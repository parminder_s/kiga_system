var config = require("./webpack.config.js"),
  ExtractTextPlugin = require("extract-text-webpack-plugin"),
  NgAnnotatePlugin = require("ng-annotate-webpack-plugin"),
  OptimizeJsPlugin = require("optimize-js-plugin"),
  UglifyJsPlugin = require("webpack/lib/optimize/UglifyJsPlugin"),
  webpackMerge = require("webpack-merge");

module.exports = webpackMerge(config, {
  module: {
    loaders: [
      {
        test: /\.css/,
        use: [
          {loader: "style-loader"},
          {loader: "css-loader", options: {minimize: false}}]
      }, {
        test: /\.less$/,
        use: [
          {loader: "style-loader"},
          {loader: "css-loader", options: {minimize: false}},
          {loader: "less-loader"}]
      }
    ]
  },
  plugins: [
    new NgAnnotatePlugin({
      add: true
    }),
    new OptimizeJsPlugin({
      sourceMap: false
    }),
    new UglifyJsPlugin({
      beautify: false,
      output: {
        comments: false,
        ascii_only: true
      },
      mangle: {
        screw_ie8: true
      },
      compress: {
        screw_ie8: true,
        warnings: false,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        dead_code: true,
        evaluate: true,
        if_return: true,
        join_vars: true,
        negate_iife: false
      }
    })
  ]
});
