import { enableProdMode } from '@angular/core';
import { ngExpressEngine } from '@nguniversal/express-engine';
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';
import * as S3 from 'aws-sdk/clients/s3';
import axios from 'axios';
import * as express from 'express';
import * as fs from 'fs';
import { join } from 'path';
import * as process from 'process';
import 'reflect-metadata';
import * as zlib from 'zlib';
// These are important and needed before anything else
import 'zone.js/dist/zone-node';

enableProdMode();

const DIST_FOLDER = join(process.cwd(), 'dist');
const PORT = process.env.KIGA_PORT || 8082;
const bucket = process.env.KIGA_BUCKET || 'kigadev';
// starts express in server mode and does not generate ssr files and upload them automatically.
const debug = process.env.KIGA_DEBUG || false;

if (!process.env.KIGA_ENDPOINTURL) {
  process.env.KIGA_ENDPOINTURL = 'http://localhost:8080/';
  process.env.KIGA_NEWBOOTSTRAPENDPOINTENABLED = '';
  process.env.KIGA_CLIENTURL = '';
  console.log('endpoint url not set, using default one: ' + process.env.KIGA_ENDPOINTURL);
}

const locale = process.argv[2];
if (!debug) {
  if (!process.env.KIGA_LEGACYURL || process.env.KIGA_LEGACYURL.endsWith('/')) {
    console.log('legacy url not set or in wrong format (must not end with "/"). Stopping');
    process.exit(1);
  }

  if (!locale) {
    console.log('locale is missing');
    process.exit(1);
  }
}

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./dist/server/main');

function upload(bucket, filename, body) {
  const credentials = {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
  };
  const s3 = new S3({ credentials });
  return new Promise((resolve, reject) =>
    s3.putObject(
      {
        Bucket: bucket,
        Key: filename,
        Body: body,
        ACL: 'public-read',
        ContentType: 'text/html; charset=utf-8',
        ContentEncoding: 'gzip'
      },
      (err, data) => {
        if (err) {
          reject(`upload failed for ${filename}`);
        } else {
          resolve();
        }
      }
    )
  );
}

function compress(body) {
  return new Promise((resolve, reject) =>
    zlib.gzip(body, { level: 9 }, (_, zipped) => resolve(zipped))
  );
}

function saveFile(filename, body) {
  return new Promise((resolve, reject) => fs.writeFile(filename, body, resolve));
}

const whenEndpointReady = () =>
  new Promise(resolve => {
    const doFetch = () =>
      axios
        .get(process.env.KIGA_ENDPOINTURL + 'healthCheck')
        .then(resolve)
        .catch(() => setTimeout(doFetch, 5 * 1000));
    doFetch();
  });

const app = express();
app
  .engine(
    'html',
    ngExpressEngine({
      bootstrap: AppServerModuleNgFactory,
      providers: [provideModuleMap(LAZY_MODULE_MAP)]
    })
  )
  .set('view engine', 'html')
  .set('views', join(DIST_FOLDER, 'kiga'))
  .get('*.*', express.static(join(DIST_FOLDER, 'kiga')))
  .get('*', (req, res) => {
    res.render('index', { req }, (err, html) => {
      res.send(html);
    });
  });

const server = app.listen(PORT, () => {
  console.log(`Node server in debug mode listening on port ${PORT}`);
});

async function getAndUploadbody() {
  const filename = `index.${locale}.html`;
  await whenEndpointReady();
  const response = await axios.get(`http://localhost:${PORT}/ng/ng6/${locale}/home`);
  const body = response.data;
  await saveFile(filename, body);
  const compressedBody = await compress(body);
  await upload(bucket, filename, compressedBody);
  console.log(`rendererd and uploaded to s3://${bucket}/${filename}`);
}

if (!debug) {
  getAndUploadbody().then(() => {
    server.close();
  });
}
