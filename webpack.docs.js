var config = require('./webpack.dev.js'),
  _ = require('lodash'),
  webpackMerge = require('webpack-merge');

config.output.publicPath = '/';
var docsConfig = webpackMerge(config, {
  entry: {
    bundle: './app/src/docs/js/docs.coffee'
  }
});

module.exports = docsConfig;
