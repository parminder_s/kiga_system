import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { storiesOf } from '@storybook/angular';
import { KigaInitData } from '../app/data/KigaInitData';
import '../app/main/css/style.css';
import { ScrollPaneComponent } from '../app/shared/scroll-pane/scroll-pane.component';
import '../styles.less';

const items = [
  {
    title: 'Kitchen Sink',
    description: 'This idea shows more or less all possible combinations an idea can have.',
    previewUrl:
      'https://s3-eu-west-1.amazonaws.com/kigadev/-fixtures/i-kitchen-sink/kitchensink_p13896.jpg',
    url: '/en/cms/ideas/new-ideas',
    queryParams: { preview: 'kitchen-sink' },
    className: '',
    useTranslation: false,
    ifRegistrationItem: ''
  },
  {
    title: 'New Idea 2',
    description:
      'This is a new idea number 2. It has no content but obviously a very long description, that should not shown in its full length.',
    previewUrl: 'https://s3-eu-west-1.amazonaws.com/kigadev/IMG_9711_p13896.jpg',
    url: '/en/cms/ideas/new-ideas',
    queryParams: { preview: 'new-idea-2' },
    className: '',
    useTranslation: false,
    ifRegistrationItem: ''
  },
  {
    title: 'New Idea 3',
    description:
      'This is a new idea number 3. It has no content but obviously a very long description, that should not shown in its full length.',
    previewUrl: 'https://s3-eu-west-1.amazonaws.com/kigadev/IMG_9711_p13896.jpg',
    url: '/en/cms/ideas/new-ideas',
    queryParams: { preview: 'new-idea-3' },
    className: '',
    useTranslation: false,
    ifRegistrationItem: ''
  },
  {
    title: 'New Idea 4',
    description:
      'This is a new idea number 4. It has no content but obviously a very long description, that should not shown in its full length.',
    previewUrl: 'https://s3-eu-west-1.amazonaws.com/kigadev/IMG_9711_p13896.jpg',
    url: '/en/cms/ideas/new-ideas',
    queryParams: { preview: 'new-idea-4' },
    className: '',
    useTranslation: false,
    ifRegistrationItem: ''
  },
  {
    title: 'New Idea 5',
    description:
      'This is a new idea number 5. It has no content but obviously a very long description, that should not shown in its full length.',
    previewUrl: 'https://s3-eu-west-1.amazonaws.com/kigadev/IMG_9711_p13896.jpg',
    url: '/en/cms/ideas/new-ideas',
    queryParams: { preview: 'new-idea-5' },
    className: '',
    useTranslation: false,
    ifRegistrationItem: ''
  },
  {
    title: 'New Idea 6',
    description:
      'This is a new idea number 6. It has no content but obviously a very long description, that should not shown in its full length.',
    previewUrl: 'https://s3-eu-west-1.amazonaws.com/kigadev/IMG_9711_p13896.jpg',
    url: '/en/cms/ideas/new-ideas',
    queryParams: { preview: 'new-idea-6' },
    className: '',
    useTranslation: false,
    ifRegistrationItem: ''
  },
  {
    title: 'New Idea 7',
    description:
      'This is a new idea number 7. It has no content but obviously a very long description, that should not shown in its full length.',
    previewUrl: 'https://s3-eu-west-1.amazonaws.com/kigadev/IMG_9711_p13896.jpg',
    url: '/en/cms/ideas/new-ideas',
    queryParams: { preview: 'new-idea-7' },
    className: '',
    useTranslation: false,
    ifRegistrationItem: ''
  },
  {
    title: 'New Idea 8',
    description:
      'This is a new idea number 8. It has no content but obviously a very long description, that should not shown in its full length.',
    previewUrl: 'https://s3-eu-west-1.amazonaws.com/kigadev/IMG_9711_p13896.jpg',
    url: '/en/cms/ideas/new-ideas',
    queryParams: { preview: 'new-idea-8' },
    className: '',
    useTranslation: false,
    ifRegistrationItem: ''
  },
  {
    title: 'New Idea 9',
    description:
      'This is a new idea number 9. It has no content but obviously a very long description, that should not shown in its full length.',
    previewUrl: 'https://s3-eu-west-1.amazonaws.com/kigadev/IMG_9711_p13896.jpg',
    url: '/en/cms/ideas/new-ideas',
    queryParams: { preview: 'new-idea-9' },
    className: '',
    useTranslation: false,
    ifRegistrationItem: ''
  },
  {
    title: 'New ideas',
    description: 'This is a new idea which is available for all age groups',
    previewUrl: 'https://s3-eu-west-1.amazonaws.com/kigadev/IMG_9711_p13896.jpg',
    url: '/en/cms/ideas/new-ideas',
    queryParams: { preview: 'new-idea-1' },
    className: '',
    useTranslation: false,
    ifRegistrationItem: ''
  },
  {
    title: 'Sorrento Peninsula',
    description: 'Free Idea on the Peninsula of Sorrento',
    previewUrl:
      'https://s3-eu-west-1.amazonaws.com/kigadev/-fixtures/i-sorrento/sorrentoqE0sz_p13896.jpg',
    url: '/en/cms/ideas/new-ideas',
    queryParams: { preview: 'sorrento' },
    className: '',
    useTranslation: false,
    ifRegistrationItem: ''
  },
  {
    title: 'Stonehenge',
    description:
      'Stonehenge is an ancient place in Great Britain which is famous for its massive stone circle.',
    previewUrl:
      'https://s3-eu-west-1.amazonaws.com/kigadev/-fixtures/i-stonehenge/Stonehenge_p13896.jpg',
    url: '/en/cms/ideas/new-ideas',
    queryParams: { preview: 'stonehenge' },
    className: '',
    useTranslation: false,
    ifRegistrationItem: ''
  }
];
storiesOf('Scroll Pane', module).add('Example', () => ({
  component: ScrollPaneComponent,
  moduleMetadata: {
    imports: [RouterModule.forRoot([]), TranslateModule.forRoot()],
    providers: [
      { provide: APP_BASE_HREF, useValue: '/' },
      {
        provide: KigaInitData,
        useValue: {
          clientUrl: '.'
        }
      }
    ]
  },
  props: {
    items
  }
}));
