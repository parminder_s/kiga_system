import { moduleMetadata, storiesOf } from '@storybook/angular';
import { withNotes } from '@storybook/addon-notes';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { KigaButtonComponent } from '../app/form/kiga-button/kiga-button.component';
import { FormGroupDirective, ReactiveFormsModule } from '@angular/forms';
import { KigaCheckboxComponent } from '../app/form/kiga-checkbox/kiga-checkbox.component';
import { KigaDateComponent } from '../app/form/kiga-date/kiga-date.component';
import { KigaNumberComponent } from '../app/form/kiga-number/kiga-number.component';
import { KigaSelectionComponent } from '../app/form/kiga-selection/kiga-selection.component';
import { KigaTextComponent } from '../app/form/kiga-text/kiga-text.component';
import { KigaTextareaComponent } from '../app/form/kiga-textarea/kiga-textarea.component';
import { KigaValidationMessagesComponent } from '../app/form/kiga-validation-messages/kiga-validation-messages.component';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material';

storiesOf('KigaForm', module)
  .addDecorator(
    moduleMetadata({
      imports: [CommonModule, ReactiveFormsModule, MatCheckboxModule],
      declarations: [KigaValidationMessagesComponent],
      providers: [FormGroupDirective]
    })
  )
  .add('KigaButton', () => ({
    component: KigaButtonComponent,
    props: {
      label: 'Orange Kiga Button',
      name: 'name'
    }
  }))
  .add('KigaCheckbox', () => ({
    component: KigaCheckboxComponent,
    props: {
      label: 'check it',
      name: 'name'
    }
  }))
  .add('KigaDate', () => ({
    component: KigaDateComponent,
    props: {
      label: 'check it',
      name: 'name'
    }
  }))
  .add('KigaNumber', () => ({
    component: KigaNumberComponent,
    props: {
      label: 'check it',
      name: 'name'
    }
  }))
  .add('KigaSelection', () => ({
    component: KigaSelectionComponent,
    props: {
      label: 'check it',
      name: 'name'
    }
  }))
  .add('KigaText', () => ({
    component: KigaTextComponent,
    props: {
      label: 'check it',
      name: 'name'
    }
  }))
  .add('KigaTextArea', () => ({
    component: KigaTextareaComponent,
    props: {
      label: 'check it',
      name: 'name'
    }
  }));
