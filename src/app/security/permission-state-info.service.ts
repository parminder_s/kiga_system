import { Injectable } from '@angular/core';
import IMember from '../../../app/src/misc/permission/js/IMember';
import { MemberInfoService } from '../main/member-info.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionStateInfoService {
  constructor(private memberInfo: MemberInfoService) {}

  public get(member: IMember, isNewIdea: boolean): string {
    return this.memberInfo.withMember(member, infoService => {
      let returner = 'root.permission.';

      if (infoService.isAnonymous()) {
        if (isNewIdea) {
          return 'root.login';
        } else {
          return 'root.permission.loginOrFull';
        }
      } else {
        if (infoService.isCommunity() && isNewIdea) {
          return returner + 'fullRequired';
        }
        if (infoService.isCommunity() && !isNewIdea) {
          return returner + 'fullRequired';
        }

        if (
          infoService.hasTestSubscription() &&
          infoService.hasExpiredSubscription() &&
          isNewIdea
        ) {
          return returner + 'testExpired';
        }
        if (
          infoService.hasTestSubscription() &&
          infoService.hasExpiredSubscription() &&
          !isNewIdea
        ) {
          return returner + 'testExpired';
        }

        if (infoService.hasCancelledSubscription()) {
          return returner + 'fullExpired';
        }
        if (infoService.hasLockedSubscription()) {
          return returner + 'invoiceOpen';
        } else if (infoService.hasTestSubscription()) {
          return returner + 'fullUpgrade';
        }
      }

      return returner;
    });
  }
}
