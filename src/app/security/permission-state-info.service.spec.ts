import { TestBed, inject } from '@angular/core/testing';

import { PermissionStateInfoService } from './permission-state-info.service';

describe('PermissionStateInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PermissionStateInfoService]
    });
  });

  it('should be created', inject(
    [PermissionStateInfoService],
    (service: PermissionStateInfoService) => {
      expect(service).toBeTruthy();
    }
  ));
});
