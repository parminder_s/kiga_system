/**
 * KiGaPortal Bootstrap
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

export interface Config {
  currentCountry?: string;
  angularCmsEnabled?: boolean;
  permissionMode?: string;
  ngLiveUrl?: string;
  planVersion?: number;
  locales?: Array<string>;
  repositoryContexts?: Array<string>;
  allowCountrySelection?: boolean;
  changeCountryViaCookie?: boolean;
  currentRepositoryContext?: string;
  feedbackEnabled?: boolean;
  googleAnalyticsEnabled?: boolean;
  jsErrorLogEnabled?: boolean;
  licenceEnabled?: boolean;
  memberAdmin?: boolean;
  newIdeasBlockViewEnabled?: boolean;
  newShopRegistrationEnabled?: boolean;
  regShowInfo?: boolean;
  regShowInfoGift?: boolean;
  regShowInfoParent?: boolean;
  regShowInfoStandardOrg?: boolean;
  regShowInfoTeacher?: boolean;
  showInvoice?: boolean;
  showMemberIdeas?: boolean;
  showNewSearchBar?: boolean;
  showQuickRegisters?: boolean;
  showVersion?: boolean;
}
