/**
 * KiGaPortal Payment
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { Breadcrumb } from './breadcrumb';
import { IdeasResponseRating } from './ideasResponseRating';
import { Locale } from './locale';
import { Metadata } from './metadata';
import { Permission } from './permission';
import { PreviewPageResponse } from './previewPageResponse';

export interface IdeasViewModelIdea {
  ageRange?: string;
  audioLink?: string;
  breadcrumbs?: Array<Breadcrumb>;
  categoryId?: number;
  categoryName?: string;
  className?: string;
  createDate?: Date;
  description?: string;
  download?: boolean;
  elasticScore?: number;
  facebookShare?: boolean;
  fileName?: string;
  filePreviewUrl?: string;
  fileProxyUrl?: string;
  fileSize?: number;
  fileUrl?: string;
  forParents?: boolean;
  id?: number;
  inPool?: boolean;
  largePreviewImageUrl?: string;
  lastEdited?: Date;
  layoutType?: IdeasViewModelIdea.LayoutTypeEnum;
  level1Title?: string;
  level2Title?: string;
  locale?: Locale;
  metadata?: Metadata;
  name?: string;
  ngUrl?: string;
  parentCategoryId?: number;
  parentCategoryName?: string;
  permission?: Permission;
  previewFileId?: number;
  previewName?: string;
  previewPages?: Array<PreviewPageResponse>;
  previewSize?: number;
  previewUrl?: string;
  published?: boolean;
  rating?: IdeasResponseRating;
  resolvedType?: string;
  separatedAgeGroups?: Array<number>;
  showBreadcrumbs?: boolean;
  sortDayMini?: number;
  sortMonth?: number;
  sortMonthMini?: number;
  sortYear?: number;
  sortYearMini?: number;
  state?: IdeasViewModelIdea.StateEnum;
  title?: string;
  uploadedFileId?: number;
  uploadedFileName?: string;
  url?: string;
  urlSegment?: string;
  videoLink?: string;
}
export namespace IdeasViewModelIdea {
  export type LayoutTypeEnum = 'STATIC_OLD' | 'DYNAMIC' | 'A4_PDF';
  export const LayoutTypeEnum = {
    STATICOLD: 'STATIC_OLD' as LayoutTypeEnum,
    DYNAMIC: 'DYNAMIC' as LayoutTypeEnum,
    A4PDF: 'A4_PDF' as LayoutTypeEnum
  };
  export type StateEnum = 'PUBLISHED' | 'UNPUBLISHED' | 'UPDATED' | 'NOT_IN_DRAFT';
  export const StateEnum = {
    PUBLISHED: 'PUBLISHED' as StateEnum,
    UNPUBLISHED: 'UNPUBLISHED' as StateEnum,
    UPDATED: 'UPDATED' as StateEnum,
    NOTINDRAFT: 'NOT_IN_DRAFT' as StateEnum
  };
}
