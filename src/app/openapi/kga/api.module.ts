import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';

import { ChildService } from './api/child.service';
import { ChildJournalService } from './api/childJournal.service';
import { CountryService } from './api/country.service';
import { EmployeeService } from './api/employee.service';
import { GroupService } from './api/group.service';
import { KindergartenService } from './api/kindergarten.service';
import { LanguageService } from './api/language.service';
import { ParentService } from './api/parent.service';
import { YearService } from './api/year.service';

@NgModule({
  imports: [],
  declarations: [],
  exports: [],
  providers: [
    ChildService,
    ChildJournalService,
    CountryService,
    EmployeeService,
    GroupService,
    KindergartenService,
    LanguageService,
    ParentService,
    YearService
  ]
})
export class ApiModule {
  public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders {
    return {
      ngModule: ApiModule,
      providers: [{ provide: Configuration, useFactory: configurationFactory }]
    };
  }

  constructor(@Optional() @SkipSelf() parentModule: ApiModule, @Optional() http: HttpClient) {
    if (parentModule) {
      throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
    }
    if (!http) {
      throw new Error(
        'You need to import the HttpClientModule in your AppModule! \n' +
          'See also https://github.com/angular/angular/issues/20575'
      );
    }
  }
}
