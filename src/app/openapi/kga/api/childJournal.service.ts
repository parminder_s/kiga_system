/**
 * KiGaPortal KGA
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpEvent } from '@angular/common/http';
import { CustomHttpUrlEncodingCodec } from '../encoder';

import { Observable } from 'rxjs';

import { ChildJournalEntry } from '../model/childJournalEntry';
import { CountryViewModel } from '../model/countryViewModel';
import { IdRequest } from '../model/idRequest';

import { BASE_PATH, COLLECTION_FORMATS } from '../variables';
import { Configuration } from '../configuration';

@Injectable({
  providedIn: 'root'
})
export class ChildJournalService {
  protected basePath = 'http://kiga:8080';
  public defaultHeaders = new HttpHeaders();
  public configuration = new Configuration();

  constructor(
    protected httpClient: HttpClient,
    @Optional() @Inject(BASE_PATH) basePath: string,
    @Optional() configuration: Configuration
  ) {
    if (configuration) {
      this.configuration = configuration;
      this.configuration.basePath = configuration.basePath || basePath || this.basePath;
    } else {
      this.configuration.basePath = basePath || this.basePath;
    }
  }

  /**
   * @param consumes string[] mime-types
   * @return true: consumes contains 'multipart/form-data', false: otherwise
   */
  private canConsumeForm(consumes: string[]): boolean {
    const form = 'multipart/form-data';
    for (const consume of consumes) {
      if (form === consume) {
        return true;
      }
    }
    return false;
  }

  /**
   * Get a file for journal.
   *
   * @param childId id of the child
   * @param fileId id of the file
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public fetchFile(
    childId: number,
    fileId: number,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<Array<CountryViewModel>>;
  public fetchFile(
    childId: number,
    fileId: number,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<Array<CountryViewModel>>>;
  public fetchFile(
    childId: number,
    fileId: number,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<Array<CountryViewModel>>>;
  public fetchFile(
    childId: number,
    fileId: number,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (childId === null || childId === undefined) {
      throw new Error('Required parameter childId was null or undefined when calling fetchFile.');
    }
    if (fileId === null || fileId === undefined) {
      throw new Error('Required parameter fileId was null or undefined when calling fetchFile.');
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];

    return this.httpClient.get<Array<CountryViewModel>>(
      `${this.configuration.basePath}/kga/childJournalTODO/file/${encodeURIComponent(
        String(childId)
      )}/${encodeURIComponent(String(fileId))}`,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Get the journal for a child
   *
   * @param idRequest
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public findForChild(
    idRequest: IdRequest,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<Array<ChildJournalEntry>>;
  public findForChild(
    idRequest: IdRequest,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<Array<ChildJournalEntry>>>;
  public findForChild(
    idRequest: IdRequest,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<Array<ChildJournalEntry>>>;
  public findForChild(
    idRequest: IdRequest,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (idRequest === null || idRequest === undefined) {
      throw new Error(
        'Required parameter idRequest was null or undefined when calling findForChild.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<Array<ChildJournalEntry>>(
      `${this.configuration.basePath}/kga/childJournal/findForChild/`,
      idRequest,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Save a child journal entry
   *
   * @param childJournalEntry
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public saveChildJournalEntry(
    childJournalEntry: ChildJournalEntry,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<number>;
  public saveChildJournalEntry(
    childJournalEntry: ChildJournalEntry,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<number>>;
  public saveChildJournalEntry(
    childJournalEntry: ChildJournalEntry,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<number>>;
  public saveChildJournalEntry(
    childJournalEntry: ChildJournalEntry,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (childJournalEntry === null || childJournalEntry === undefined) {
      throw new Error(
        'Required parameter childJournalEntry was null or undefined when calling saveChildJournalEntry.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<number>(
      `${this.configuration.basePath}/kga/childJournal/save`,
      childJournalEntry,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }
}
