/**
 * KiGaPortal KGA
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpEvent } from '@angular/common/http';
import { CustomHttpUrlEncodingCodec } from '../encoder';

import { Observable } from 'rxjs';

import { EmployeeModel } from '../model/employeeModel';
import { IdRequest } from '../model/idRequest';

import { BASE_PATH, COLLECTION_FORMATS } from '../variables';
import { Configuration } from '../configuration';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  protected basePath = 'http://kiga:8080';
  public defaultHeaders = new HttpHeaders();
  public configuration = new Configuration();

  constructor(
    protected httpClient: HttpClient,
    @Optional() @Inject(BASE_PATH) basePath: string,
    @Optional() configuration: Configuration
  ) {
    if (configuration) {
      this.configuration = configuration;
      this.configuration.basePath = configuration.basePath || basePath || this.basePath;
    } else {
      this.configuration.basePath = basePath || this.basePath;
    }
  }

  /**
   * @param consumes string[] mime-types
   * @return true: consumes contains 'multipart/form-data', false: otherwise
   */
  private canConsumeForm(consumes: string[]): boolean {
    const form = 'multipart/form-data';
    for (const consume of consumes) {
      if (form === consume) {
        return true;
      }
    }
    return false;
  }

  /**
   * Get a child
   *
   * @param idRequest
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public findOneEmployee(
    idRequest: IdRequest,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<EmployeeModel>;
  public findOneEmployee(
    idRequest: IdRequest,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<EmployeeModel>>;
  public findOneEmployee(
    idRequest: IdRequest,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<EmployeeModel>>;
  public findOneEmployee(
    idRequest: IdRequest,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (idRequest === null || idRequest === undefined) {
      throw new Error(
        'Required parameter idRequest was null or undefined when calling findOneEmployee.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<EmployeeModel>(
      `${this.configuration.basePath}/kga/employee/findOne/`,
      idRequest,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Get a list of employees via the kindergarden
   *
   * @param idRequest
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public listEmployeeByKindergarten(
    idRequest: IdRequest,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<Array<EmployeeModel>>;
  public listEmployeeByKindergarten(
    idRequest: IdRequest,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<Array<EmployeeModel>>>;
  public listEmployeeByKindergarten(
    idRequest: IdRequest,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<Array<EmployeeModel>>>;
  public listEmployeeByKindergarten(
    idRequest: IdRequest,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (idRequest === null || idRequest === undefined) {
      throw new Error(
        'Required parameter idRequest was null or undefined when calling listEmployeeByKindergarten.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<Array<EmployeeModel>>(
      `${this.configuration.basePath}/kga/employee/list/byKindergarten`,
      idRequest,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Save a child
   *
   * @param employeeModel
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public saveEmployee(
    employeeModel: EmployeeModel,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<number>;
  public saveEmployee(
    employeeModel: EmployeeModel,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<number>>;
  public saveEmployee(
    employeeModel: EmployeeModel,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<number>>;
  public saveEmployee(
    employeeModel: EmployeeModel,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (employeeModel === null || employeeModel === undefined) {
      throw new Error(
        'Required parameter employeeModel was null or undefined when calling saveEmployee.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<number>(
      `${this.configuration.basePath}/kga/employee/save`,
      employeeModel,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }
}
