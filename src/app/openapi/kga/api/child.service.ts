/**
 * KiGaPortal KGA
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpEvent } from '@angular/common/http';
import { CustomHttpUrlEncodingCodec } from '../encoder';

import { Observable } from 'rxjs';

import { AddChildToGroup } from '../model/addChildToGroup';
import { AddParentToChild } from '../model/addParentToChild';
import { ChildModel } from '../model/childModel';
import { IdRequest } from '../model/idRequest';
import { KgaParentModel } from '../model/kgaParentModel';

import { BASE_PATH, COLLECTION_FORMATS } from '../variables';
import { Configuration } from '../configuration';

@Injectable({
  providedIn: 'root'
})
export class ChildService {
  protected basePath = 'http://kiga:8080';
  public defaultHeaders = new HttpHeaders();
  public configuration = new Configuration();

  constructor(
    protected httpClient: HttpClient,
    @Optional() @Inject(BASE_PATH) basePath: string,
    @Optional() configuration: Configuration
  ) {
    if (configuration) {
      this.configuration = configuration;
      this.configuration.basePath = configuration.basePath || basePath || this.basePath;
    } else {
      this.configuration.basePath = basePath || this.basePath;
    }
  }

  /**
   * @param consumes string[] mime-types
   * @return true: consumes contains 'multipart/form-data', false: otherwise
   */
  private canConsumeForm(consumes: string[]): boolean {
    const form = 'multipart/form-data';
    for (const consume of consumes) {
      if (form === consume) {
        return true;
      }
    }
    return false;
  }

  /**
   * Add a chhild to a exsisting Group
   *
   * @param addChildToGroup
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public addChildToGroup(
    addChildToGroup: AddChildToGroup,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<string>;
  public addChildToGroup(
    addChildToGroup: AddChildToGroup,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<string>>;
  public addChildToGroup(
    addChildToGroup: AddChildToGroup,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<string>>;
  public addChildToGroup(
    addChildToGroup: AddChildToGroup,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (addChildToGroup === null || addChildToGroup === undefined) {
      throw new Error(
        'Required parameter addChildToGroup was null or undefined when calling addChildToGroup.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<string>(
      `${this.configuration.basePath}/kga/child/addToGroup/`,
      addChildToGroup,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Add a parent to a exsisting Child
   *
   * @param addParentToChild
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public addParent(
    addParentToChild: AddParentToChild,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<string>;
  public addParent(
    addParentToChild: AddParentToChild,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<string>>;
  public addParent(
    addParentToChild: AddParentToChild,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<string>>;
  public addParent(
    addParentToChild: AddParentToChild,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (addParentToChild === null || addParentToChild === undefined) {
      throw new Error(
        'Required parameter addParentToChild was null or undefined when calling addParent.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<string>(
      `${this.configuration.basePath}/kga/child/addParent/`,
      addParentToChild,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Delete a child
   *
   * @param idRequest
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public deleteOneChild(
    idRequest: IdRequest,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<string>;
  public deleteOneChild(
    idRequest: IdRequest,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<string>>;
  public deleteOneChild(
    idRequest: IdRequest,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<string>>;
  public deleteOneChild(
    idRequest: IdRequest,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (idRequest === null || idRequest === undefined) {
      throw new Error(
        'Required parameter idRequest was null or undefined when calling deleteOneChild.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<string>(
      `${this.configuration.basePath}/kga/child/deleteOne/`,
      idRequest,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Get a child
   *
   * @param idRequest
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public findOneChild(
    idRequest: IdRequest,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<ChildModel>;
  public findOneChild(
    idRequest: IdRequest,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<ChildModel>>;
  public findOneChild(
    idRequest: IdRequest,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<ChildModel>>;
  public findOneChild(
    idRequest: IdRequest,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (idRequest === null || idRequest === undefined) {
      throw new Error(
        'Required parameter idRequest was null or undefined when calling findOneChild.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<ChildModel>(
      `${this.configuration.basePath}/kga/child/findOne/`,
      idRequest,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Get a list of childs via the group
   *
   * @param idRequest
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public listChildByKgaGroup(
    idRequest: IdRequest,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<Array<ChildModel>>;
  public listChildByKgaGroup(
    idRequest: IdRequest,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<Array<ChildModel>>>;
  public listChildByKgaGroup(
    idRequest: IdRequest,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<Array<ChildModel>>>;
  public listChildByKgaGroup(
    idRequest: IdRequest,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (idRequest === null || idRequest === undefined) {
      throw new Error(
        'Required parameter idRequest was null or undefined when calling listChildByKgaGroup.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<Array<ChildModel>>(
      `${this.configuration.basePath}/kga/child/list/byKgaGroup`,
      idRequest,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Get a list of childs via the group
   *
   * @param idRequest
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public listChildByKgaYear(
    idRequest: IdRequest,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<Array<ChildModel>>;
  public listChildByKgaYear(
    idRequest: IdRequest,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<Array<ChildModel>>>;
  public listChildByKgaYear(
    idRequest: IdRequest,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<Array<ChildModel>>>;
  public listChildByKgaYear(
    idRequest: IdRequest,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (idRequest === null || idRequest === undefined) {
      throw new Error(
        'Required parameter idRequest was null or undefined when calling listChildByKgaYear.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<Array<ChildModel>>(
      `${this.configuration.basePath}/kga/child/list/byKgaYear`,
      idRequest,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Get a list of child via the kindergaren
   *
   * @param idRequest
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public listChildByKindergarten(
    idRequest: IdRequest,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<Array<ChildModel>>;
  public listChildByKindergarten(
    idRequest: IdRequest,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<Array<ChildModel>>>;
  public listChildByKindergarten(
    idRequest: IdRequest,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<Array<ChildModel>>>;
  public listChildByKindergarten(
    idRequest: IdRequest,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (idRequest === null || idRequest === undefined) {
      throw new Error(
        'Required parameter idRequest was null or undefined when calling listChildByKindergarten.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<Array<ChildModel>>(
      `${this.configuration.basePath}/kga/child/list/byKindergarten`,
      idRequest,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Removes a parent to a exsisting Child
   *
   * @param idRequest
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public removeParent(
    idRequest: IdRequest,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<string>;
  public removeParent(
    idRequest: IdRequest,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<string>>;
  public removeParent(
    idRequest: IdRequest,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<string>>;
  public removeParent(
    idRequest: IdRequest,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (idRequest === null || idRequest === undefined) {
      throw new Error(
        'Required parameter idRequest was null or undefined when calling removeParent.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<string>(
      `${this.configuration.basePath}/kga/child/removeParent/`,
      idRequest,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Save a child
   *
   * @param childModel
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public saveChild(
    childModel: ChildModel,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<number>;
  public saveChild(
    childModel: ChildModel,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<number>>;
  public saveChild(
    childModel: ChildModel,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<number>>;
  public saveChild(
    childModel: ChildModel,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (childModel === null || childModel === undefined) {
      throw new Error(
        'Required parameter childModel was null or undefined when calling saveChild.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<number>(
      `${this.configuration.basePath}/kga/child/save`,
      childModel,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Save a parent with a rolte to a child
   *
   * @param kgaParentModel
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public saveParentToChild(
    kgaParentModel: KgaParentModel,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<string>;
  public saveParentToChild(
    kgaParentModel: KgaParentModel,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<string>>;
  public saveParentToChild(
    kgaParentModel: KgaParentModel,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<string>>;
  public saveParentToChild(
    kgaParentModel: KgaParentModel,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (kgaParentModel === null || kgaParentModel === undefined) {
      throw new Error(
        'Required parameter kgaParentModel was null or undefined when calling saveParentToChild.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<string>(
      `${this.configuration.basePath}/kga/child/saveParent`,
      kgaParentModel,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }
}
