export * from './child.service';
import { ChildService } from './child.service';
export * from './childJournal.service';
import { ChildJournalService } from './childJournal.service';
export * from './country.service';
import { CountryService } from './country.service';
export * from './employee.service';
import { EmployeeService } from './employee.service';
export * from './group.service';
import { GroupService } from './group.service';
export * from './kindergarten.service';
import { KindergartenService } from './kindergarten.service';
export * from './language.service';
import { LanguageService } from './language.service';
export * from './parent.service';
import { ParentService } from './parent.service';
export * from './year.service';
import { YearService } from './year.service';
export const APIS = [
  ChildService,
  ChildJournalService,
  CountryService,
  EmployeeService,
  GroupService,
  KindergartenService,
  LanguageService,
  ParentService,
  YearService
];
