/**
 * KiGaPortal KGA
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpEvent } from '@angular/common/http';
import { CustomHttpUrlEncodingCodec } from '../encoder';

import { Observable } from 'rxjs';

import { IdRequest } from '../model/idRequest';
import { YearModel } from '../model/yearModel';

import { BASE_PATH, COLLECTION_FORMATS } from '../variables';
import { Configuration } from '../configuration';

@Injectable({
  providedIn: 'root'
})
export class YearService {
  protected basePath = 'http://kiga:8080';
  public defaultHeaders = new HttpHeaders();
  public configuration = new Configuration();

  constructor(
    protected httpClient: HttpClient,
    @Optional() @Inject(BASE_PATH) basePath: string,
    @Optional() configuration: Configuration
  ) {
    if (configuration) {
      this.configuration = configuration;
      this.configuration.basePath = configuration.basePath || basePath || this.basePath;
    } else {
      this.configuration.basePath = basePath || this.basePath;
    }
  }

  /**
   * @param consumes string[] mime-types
   * @return true: consumes contains 'multipart/form-data', false: otherwise
   */
  private canConsumeForm(consumes: string[]): boolean {
    const form = 'multipart/form-data';
    for (const consume of consumes) {
      if (form === consume) {
        return true;
      }
    }
    return false;
  }

  /**
   * Archives a year
   *
   * @param idRequest
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public archiveYear(
    idRequest: IdRequest,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<string>;
  public archiveYear(
    idRequest: IdRequest,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<string>>;
  public archiveYear(
    idRequest: IdRequest,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<string>>;
  public archiveYear(
    idRequest: IdRequest,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (idRequest === null || idRequest === undefined) {
      throw new Error(
        'Required parameter idRequest was null or undefined when calling archiveYear.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<string>(
      `${this.configuration.basePath}/kga/year/archive`,
      idRequest,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Get a Group
   *
   * @param idRequest
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public findOneYear(
    idRequest: IdRequest,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<YearModel>;
  public findOneYear(
    idRequest: IdRequest,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<YearModel>>;
  public findOneYear(
    idRequest: IdRequest,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<YearModel>>;
  public findOneYear(
    idRequest: IdRequest,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (idRequest === null || idRequest === undefined) {
      throw new Error(
        'Required parameter idRequest was null or undefined when calling findOneYear.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<YearModel>(
      `${this.configuration.basePath}/kga/year/findOne/`,
      idRequest,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Get a list of Groups by kindergarten
   *
   * @param idRequest
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public listYearByKindergarten(
    idRequest: IdRequest,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<Array<YearModel>>;
  public listYearByKindergarten(
    idRequest: IdRequest,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<Array<YearModel>>>;
  public listYearByKindergarten(
    idRequest: IdRequest,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<Array<YearModel>>>;
  public listYearByKindergarten(
    idRequest: IdRequest,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (idRequest === null || idRequest === undefined) {
      throw new Error(
        'Required parameter idRequest was null or undefined when calling listYearByKindergarten.'
      );
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<Array<YearModel>>(
      `${this.configuration.basePath}/kga/year/list/byKindergarten`,
      idRequest,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * Save a child
   *
   * @param yearModel
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public saveYear(
    yearModel: YearModel,
    observe?: 'body',
    reportProgress?: boolean
  ): Observable<number>;
  public saveYear(
    yearModel: YearModel,
    observe?: 'response',
    reportProgress?: boolean
  ): Observable<HttpResponse<number>>;
  public saveYear(
    yearModel: YearModel,
    observe?: 'events',
    reportProgress?: boolean
  ): Observable<HttpEvent<number>>;
  public saveYear(
    yearModel: YearModel,
    observe: any = 'body',
    reportProgress: boolean = false
  ): Observable<any> {
    if (yearModel === null || yearModel === undefined) {
      throw new Error('Required parameter yearModel was null or undefined when calling saveYear.');
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = ['application/json'];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(
      httpHeaderAccepts
    );
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(
      consumes
    );
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.post<number>(`${this.configuration.basePath}/kga/year/save`, yearModel, {
      withCredentials: this.configuration.withCredentials,
      headers: headers,
      observe: observe,
      reportProgress: reportProgress
    });
  }
}
