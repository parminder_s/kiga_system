import { MetaDefinition } from '@angular/platform-browser';
import { Action } from '@ngrx/store';

export enum MetaActionTypes {
  UpdateMetas = '[Meta] Update Metas',
  ResetMetas = '[Meta] Reset'
}

export class UpdateMetas implements Action {
  readonly type = MetaActionTypes.UpdateMetas;
  constructor(public payload: MetaDefinition[]) {}
}

export class ResetMetas implements Action {
  readonly type = MetaActionTypes.ResetMetas;
}

export type MetaActions = UpdateMetas | ResetMetas;
