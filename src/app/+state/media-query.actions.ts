import { BreakpointState } from '@angular/cdk/layout';
import { Action } from '@ngrx/store';

export enum MediaQueryActionTypes {
  SetMediaQuerys = '[MediaQuery] Set MediaQuerys'
}

export class SetMediaQuerys implements Action {
  readonly type = MediaQueryActionTypes.SetMediaQuerys;

  constructor(public payload: BreakpointState) {}
}

export type MediaQueryActions = SetMediaQuerys;
