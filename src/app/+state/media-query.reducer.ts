import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MediaQueryActions, MediaQueryActionTypes } from './media-query.actions';
import { State as rootState } from './selectors';

const breakpointNames = ['xxlg', 'xlg', 'lg', 'lmd', 'md', 'lsm', 'sm'];
export enum SpecialBreakpoint {
  MIN380 = 'min380'
}
const specialBreakpointNames = [SpecialBreakpoint.MIN380];

/**
 * the breakpoints in the state are based on the breakpoints defined
 * by our bootstrap versions.
 *
 * The specialBreakpoints are defined outside bootstrap's and
 * are used for special use cases / components.
 *
 * Since the observers for window resizing events are expensive,
 * we want to put them all into this single reducer, so that we
 * have an overview, what exists and what not.
 */
export interface State {
  breakpoints: {
    sm: boolean;
    lsm: boolean;
    md: boolean;
    lmd: boolean;
    lg: boolean;
    xlg: boolean;
    xxlg: boolean;
  };
  specialBreakPoints: {
    min380: boolean;
  };
}

export const initialState: State = {
  breakpoints: <any>breakpointNames.reduce((acc: object, cur: string) => {
    acc[cur] = true;
    return acc;
  }, {}),
  specialBreakPoints: { min380: true }
};

export function reducer(state = initialState, action: MediaQueryActions): State {
  switch (action.type) {
    case MediaQueryActionTypes.SetMediaQuerys: {
      const { breakpoints } = action.payload;
      return {
        ...state,
        breakpoints: {
          sm: true,
          lsm: breakpoints['(min-width: 480px)'],
          md: breakpoints['(min-width: 640px)'],
          lmd: breakpoints['(min-width: 768px)'],
          lg: breakpoints['(min-width: 960px)'],
          xlg: breakpoints['(min-width: 1120px)'],
          xxlg: breakpoints['(min-width: 1280px)']
        }
      };
    }
    default:
      return state;
  }
}

export const selectMediaQuery = createFeatureSelector<rootState, State>('mediaQuery');

export const selectBreakpointName = createSelector(
  selectMediaQuery,
  (state: State) => {
    const { breakpoints } = state;
    for (let i = 0; i < breakpointNames.length; i++) {
      const name = breakpointNames[i];
      if (breakpoints[name] === true) {
        return name;
      }
    }
    return 'sm';
  }
);
