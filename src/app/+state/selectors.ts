import { ActionReducerMap } from '@ngrx/store';
import * as fromMediaQuery from './media-query.reducer';

export interface State {
  mediaQuery: fromMediaQuery.State;
}

export const reducers: ActionReducerMap<State> = {
  mediaQuery: fromMediaQuery.reducer
};
