import { Action } from '@ngrx/store';

export enum NgrxTransterStateActionTypes {
  LoadNgrxTransterStates = '[NgrxTransterState] Load NgrxTransterStates'
}

export class LoadNgrxTransterStates implements Action {
  readonly type = NgrxTransterStateActionTypes.LoadNgrxTransterStates;
  constructor(public payload) {}
}

export type NgrxTransterStateActions = LoadNgrxTransterStates;
