import { TransferState } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { ngrxStateKey } from './ngrx-transfer-state.effects';
import { LoadNgrxTransterStates } from './ngrx-transter-state.actions';

export function initApplication(store: Store<any>, transterState: TransferState): Function {
  return () =>
    new Promise(resolve => {
      const state = transterState.get(ngrxStateKey, undefined);
      if (state) {
        store.dispatch(new LoadNgrxTransterStates(state));
      }
      resolve();
    });
}
