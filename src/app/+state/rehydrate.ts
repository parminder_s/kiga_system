import { ActionReducer } from '@ngrx/store';
import { NgrxTransterStateActionTypes } from './ngrx-transter-state.actions';

export function rehydrate(reducer: ActionReducer<any>): ActionReducer<any> {
  return function(state, action: any) {
    if (action.type === NgrxTransterStateActionTypes.LoadNgrxTransterStates) {
      return action.payload;
    }
    return reducer(state, action);
  };
}
