import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { NgrxTransferStateEffects } from './ngrx-transfer-state.effects';

describe('NgrxTransferStateEffects', () => {
  let actions$: Observable<any>;
  let effects: NgrxTransferStateEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgrxTransferStateEffects, provideMockActions(() => actions$)]
    });

    effects = TestBed.get(NgrxTransferStateEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
