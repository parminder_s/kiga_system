import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, withLatestFrom } from 'rxjs/operators';
import { BaseDataLoaded } from '../base-data/base-data.actions';
import {
  LoadNgrxTransterStates,
  NgrxTransterStateActionTypes
} from './ngrx-transter-state.actions';

export const ngrxStateKey = makeStateKey('NGRX_STATE');

@Injectable()
export class NgrxTransferStateEffects {
  @Effect({ dispatch: false })
  updateMeta$ = this.actions$.pipe(
    withLatestFrom(this.store$),
    tap(([, store]) => {
      if (isPlatformServer(this.platformId)) {
        this.transferState.set(ngrxStateKey, store);
      }
    })
  );

  loadNgrxTransferState$ = this.actions$.pipe(
    ofType<LoadNgrxTransterStates>(NgrxTransterStateActionTypes.LoadNgrxTransterStates),
    map((state: any) => {
      if (isPlatformBrowser(this.platformId)) {
        return new BaseDataLoaded(state.baseData);
      } else {
        return of({});
      }
    })
  );

  constructor(
    private actions$: Actions,
    private store$: Store<any>,
    @Inject(PLATFORM_ID) private platformId: string,
    private transferState: TransferState
  ) {}
}
