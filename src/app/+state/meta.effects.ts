import { Injectable } from '@angular/core';
import { Meta, MetaDefinition } from '@angular/platform-browser';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { BASE_DATA_LOADED, BaseDataLoaded } from '../base-data/base-data.actions';
import { LoadResolvesSuccess, ResolveActionTypes } from '../cms/actions/resolve.actions';
import { MetaActionTypes, UpdateMetas } from './meta.actions';

@Injectable()
export class MetaEffects {
  private metaElements: HTMLMetaElement[] = [];
  private fallbackMeta: MetaDefinition[];

  private clearMetaElements() {
    this.metaElements.forEach(element => this.meta.removeTagElement(element));
    this.metaElements = [];
  }

  private addMetaElements(definitions: MetaDefinition[]) {
    this.metaElements = this.meta.addTags(definitions);
  }

  private setMetadata(definitions: MetaDefinition[]): void {
    this.clearMetaElements();
    this.addMetaElements(definitions);
  }

  private resetMetadata() {
    this.clearMetaElements();
    if (this.fallbackMeta) {
      this.addMetaElements(this.fallbackMeta);
    }
  }

  @Effect({ dispatch: false })
  updateMeta$ = this.actions$.pipe(
    ofType(MetaActionTypes.UpdateMetas),
    tap((action: UpdateMetas) => {
      this.clearMetaElements();
      this.addMetaElements(action.payload);
    })
  );

  @Effect({ dispatch: false })
  loadBaseData$ = this.actions$.pipe(
    ofType(BASE_DATA_LOADED),
    tap((action: BaseDataLoaded) => {
      this.fallbackMeta = [action.payload.meta];
    })
  );

  @Effect({ dispatch: false })
  $loadResolvesSuccess = this.actions$.pipe(
    ofType(ResolveActionTypes.LoadResolvesSuccess),
    tap((action: LoadResolvesSuccess) => {
      const { metadata } = action.payload;
      if (metadata) {
        this.setMetadata([<any>metadata]);
      } else {
        this.resetMetadata();
      }
    })
  );

  @Effect({ dispatch: false })
  reset$ = this.actions$.pipe(
    ofType(MetaActionTypes.ResetMetas),
    tap(() => this.resetMetadata())
  );
  constructor(private actions$: Actions, private meta: Meta) {}
}
