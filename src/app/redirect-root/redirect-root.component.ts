import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'kiga-redirect-root',
  templateUrl: './redirect-root.component.html',
  styleUrls: ['./redirect-root.component.less']
})
export class RedirectRootComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {
    this.router.navigateByUrl('/ng6/en/home');
  }
}
