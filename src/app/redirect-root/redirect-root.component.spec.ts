import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectRootComponent } from './redirect-root.component';

describe('RedirectRootComponent', () => {
  let component: RedirectRootComponent;
  let fixture: ComponentFixture<RedirectRootComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [RedirectRootComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
