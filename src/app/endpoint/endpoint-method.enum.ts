export enum EndpointMethod {
  GET = 'GET',
  POST = 'POST'
}
