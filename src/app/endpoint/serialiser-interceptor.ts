import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SerialiserService } from './serialiser.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SerialiserInterceptor implements HttpInterceptor {
  constructor(private serialiserService: SerialiserService) {}

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const newBody = this.serialiserService.serialise(req.body);
    const newReq = req.clone({ body: newBody });

    return next.handle(newReq);
  }
}
