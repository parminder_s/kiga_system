import { EndpointModule } from './endpoint.module';

describe('EndpointModule', () => {
  let endpointModule: EndpointModule;

  beforeEach(() => {
    endpointModule = new EndpointModule();
  });

  it('should create an instance', () => {
    expect(endpointModule).toBeTruthy();
  });
});
