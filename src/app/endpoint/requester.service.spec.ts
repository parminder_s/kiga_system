import { TestBed, inject } from '@angular/core/testing';
import { KigaInitData } from '../data/KigaInitData';
import { LoaderService } from '../shared/loader.service';
import { EndpointMethod } from './endpoint-method.enum';

import { RequesterService } from './requester.service';
import { SerialiserService } from './serialiser.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('RequesterService', () => {
  const show = jest.fn();
  const hide = jest.fn();
  const request = jest.fn();
  request.mockReturnValue(of([1, 2]));

  const serialise = jest.fn();

  const options = { endpoint: true, quietMode: false, rawUrl: false };

  beforeEach(() => {
    show.mockClear();
    hide.mockClear();
    request.mockClear();
    serialise.mockClear();

    const loader = { show, hide };
    const httpClient = { request };
    const serialiser = { serialise };

    TestBed.configureTestingModule({
      providers: [
        RequesterService,
        { provide: LoaderService, useValue: loader },
        { provide: HttpClient, useValue: httpClient },
        { provide: SerialiserService, useValue: serialiser },
        {
          provide: KigaInitData,
          useValue: { endpointUrl: 'http://endpoint', legacyUrl: 'http://legacy' }
        }
      ]
    });
  });

  it('should be created', inject([RequesterService], (service: RequesterService) => {
    expect(service).toBeTruthy();
  }));

  it('should not show loader in quiet mode', inject(
    [RequesterService],
    (service: RequesterService) => {
      service.request('foobar', Object.assign(options, { quietMode: true }), EndpointMethod.GET);
      expect(show).not.toHaveBeenCalled();
    }
  ));

  it('should pass on serialised data on Post', inject(
    [RequesterService],
    (service: RequesterService) => {
      serialise.mockReturnValueOnce('serialised');
      service.request('foobar', options, EndpointMethod.POST);
      const [, , { body }] = request.mock.calls[0];

      expect(body).toBe('"serialised"');
    }
  ));

  it('should pass empty data on get', inject([RequesterService], (service: RequesterService) => {
    serialise.mockReturnValueOnce({});
    service.request('foobar', options, EndpointMethod.GET);
    const [, , { body }] = request.mock.calls[0];

    expect(body).toEqual('{}');
  }));

  it('use the rawUrl', inject([RequesterService], (service: RequesterService) => {
    service.request('foobar', Object.assign(options, { rawUrl: true }), EndpointMethod.GET);
    const [, url] = request.mock.calls[0];
    expect(url).toBe('foobar');
  }));

  it('should use the endpoint url', inject([RequesterService], (service: RequesterService) => {
    service.request(
      'foobar',
      Object.assign(options, { rawUrl: false, endpoint: true }),
      EndpointMethod.GET
    );
    const [, url] = request.mock.calls[0];
    expect(url).toBe('http://endpointfoobar');
  }));
  it('should use the legacy url', inject([RequesterService], (service: RequesterService) => {
    service.request(
      'foobar',
      Object.assign(options, { rawUrl: false, endpoint: false }),
      EndpointMethod.GET
    );
    const [, url] = request.mock.calls[0];
    expect(url).toBe('http://legacy/api/foobar');
  }));
});
