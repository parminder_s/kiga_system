import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { LocaleInfoService } from '../base-data/locale-info.service';
import { defaultEndpointOptions } from './default-endpoint-options.const';
import { EndpointMethod } from './endpoint-method.enum';
import { EndpointOptions } from './endpoint-options';
import { RequesterService } from './requester.service';
import { ReturnHandlerService } from './return-handler.service';

/**
 * This module handles the communication with our API. Internally it uses
 * Angular's HttpClient. Over that, it provides options that are specific
 * to KiGaPortal.
 *
 * In post request we automatically add the current locale to the body and
 * serialisation to JSON is done automatically.
 *
 * Because of its different signature it is not fully compatible to
 * the AngularJS endpoint.
 */

@Injectable({
  providedIn: 'root'
})
export class EndpointService {
  constructor(
    private requester: RequesterService,
    private returnHandler: ReturnHandlerService,
    private localeInfo: LocaleInfoService
  ) {}

  get<T>(url: string, options: EndpointOptions = defaultEndpointOptions): Observable<T> {
    options = Object.assign({}, defaultEndpointOptions, options);
    return this.request<T>(url, options, EndpointMethod.GET);
  }

  post<T>(
    url: string,
    data: any = {},
    options: EndpointOptions = defaultEndpointOptions
  ): Observable<T> {
    options = Object.assign({}, defaultEndpointOptions, options);
    data = Object.assign({}, { locale: this.localeInfo.getLocale() }, data);
    return this.request<T>(url, options, EndpointMethod.POST, data);
  }

  private request<T>(
    url: string,
    options: EndpointOptions,
    method: EndpointMethod,
    data?: any
  ): Observable<T> {
    const observable = this.requester.request<T>(url, options, method, data);
    return this.returnHandler.handle<T>(observable, options);
  }
}
