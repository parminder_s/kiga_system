import { of } from 'rxjs';
import { mockClass } from '../mockClass';
import { DeserialiserInterceptor } from './deserialiser-interceptor';
import { DeserialiserService } from './deserialiser.service';

describe('DeserialiserInterceptor', () => {
  it('should add a cachebust parameter', async () => {
    const deserialise = () => 'serialised data';
    const deserialiser = mockClass<DeserialiserService>({ deserialise });
    const interceptor = new DeserialiserInterceptor(deserialiser);
    const event = { body: 'serilisable data' };

    const HttpResponse = function() {
      this.body = 'serialisable data';
    };

    const serialisedBody = await interceptor
      .intercept(null, { handle: req => of(new HttpResponse()) })
      .toPromise<any>();

    expect(serialisedBody.body).toEqual('serialised data');
  });
});
