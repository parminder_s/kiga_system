import { of, throwError } from 'rxjs';
import { mockClass } from '../mockClass';
import { EndpointOptions } from './endpoint-options';
import { FailureHandlerService } from './failure-handler.service';
import { ReturnHandlerService } from './return-handler.service';
import { SuccessHandlerService } from './success-handler.service';

describe('ReturnHandlerService', () => {
  it('should handle a successful observable', () => {
    const observable = of(1);
    const options = mockClass<EndpointOptions>({});

    const successHandlerFn = jest.fn();
    const successHandler = mockClass<SuccessHandlerService>({ handle: successHandlerFn });

    new ReturnHandlerService(successHandler).handle(observable, options).subscribe();

    expect(successHandlerFn).toBeCalledWith(1);
  });

  it('should handle a unsuccessful observable', () => {
    const failureHandlerFn = jest.fn();
    const failureHandler = mockClass<FailureHandlerService>({ handle: failureHandlerFn });
    const observable = throwError('error');
    const options = mockClass<EndpointOptions>({});

    const successHandlerFn = jest.fn();
    const successHandler = mockClass<SuccessHandlerService>({ handle: successHandlerFn });

    new ReturnHandlerService(successHandler).handle(observable, options).subscribe();

    expect(successHandlerFn).not.toBeCalled();
  });
});
