import { Injectable } from '@angular/core';
import { DeserialiserService } from './deserialiser.service';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class SuccessHandlerService {
  constructor(private deserialiser: DeserialiserService, private location: Location) {}

  handle<T>(response: T): T {
    const returner = this.deserialiser.deserialise<T>(response);
    if (returner && returner['redirect']) {
      this.location.go(returner['redirect']);
    } else {
      return returner;
    }
  }
}
