import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { KigaInitData } from '../data/KigaInitData';
import { EndpointMethod } from './endpoint-method.enum';
import { EndpointOptions } from './endpoint-options';
import { SerialiserService } from './serialiser.service';

@Injectable({
  providedIn: 'root'
})
export class RequesterService {
  constructor(
    private http: HttpClient,
    private serialiser: SerialiserService,
    private kigaInitData: KigaInitData
  ) {}

  public request<T>(
    url,
    options: EndpointOptions,
    method: EndpointMethod,
    data: any = {}
  ): Observable<T> {
    const jsonData: string = JSON.stringify(this.serialiser.serialise(data));

    if (!options.rawUrl) {
      if (options.endpoint) {
        url = this.kigaInitData.endpointUrl + url;
      } else {
        url = this.kigaInitData.legacyUrl + '/api/' + url;
      }
    }

    return this.http.request<T>(method, url, {
      body: jsonData,
      withCredentials: true,
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  }
}
