import { inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { LocaleInfoService } from '../base-data/locale-info.service';
import { AngularJsStateService } from '../compat/angular-js-state.service';
import { ShowExceptionService } from '../shared/show-exception.service';
import { FailureHandlerService } from './failure-handler.service';

describe('EndpointFailureHandlerService', () => {
  const getUrl = jest.fn();
  const navigateByUrl = jest.fn();
  const show = jest.fn();

  beforeEach(() => {
    getUrl.mockClear();
    navigateByUrl.mockClear();
    show.mockClear();

    TestBed.configureTestingModule({
      providers: [
        FailureHandlerService,
        { provide: AngularJsStateService, useValue: { getUrl } },
        { provide: Router, useValue: { navigateByUrl } },
        { provide: ShowExceptionService, useValue: { show } },
        { provide: LocaleInfoService, useValue: { getLocale: () => 'de' } }
      ]
    });
  });

  const withService = function(testFn: (service: FailureHandlerService) => void) {
    return inject([FailureHandlerService], testFn);
  };

  it(
    'should be created',
    withService(service => {
      expect(service).toBeTruthy();
    })
  );

  it(
    'should immediately return on quiet mode',
    withService(service => {
      expect(() => service.handle(null)).toThrow();
      expect(getUrl).not.toHaveBeenCalled();
      expect(show).not.toHaveBeenCalled();
      expect(navigateByUrl).not.toHaveBeenCalled();
    })
  );

  it(
    'should redirect to login on login required',
    withService(service => {
      expect(() => service.handle({ error: { errorCode: 'login_required' } })).toThrowError();
      expect(getUrl).toHaveBeenCalledWith('root.login');
    })
  );

  it(
    'should be redirect on parallel log',
    withService(service => {
      expect(() =>
        service.handle({ error: { errorCode: 'parallel_login', entryId: 1 } })
      ).toThrowError();
      expect(navigateByUrl).toHaveBeenCalled();
    })
  );

  it(
    'should redirect on crm session fail',
    withService(service => {
      expect(() => service.handle({ error: { errorCode: 'crm_session_fail' } })).toThrowError();
      expect(getUrl).toHaveBeenCalledWith('root.login', {
        locale: 'de',
        forward: 'root.member.account'
      });
    })
  );

  it(
    'should simply display error message on unknown error code',
    withService(service => {
      expect(() => service.handle({ errorCode: '-' })).toThrowError();
      expect(show).toHaveBeenCalledWith('-');
    })
  );
});
