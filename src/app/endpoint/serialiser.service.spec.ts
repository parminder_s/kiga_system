import { TestBed, inject } from '@angular/core/testing';
import * as moment from 'moment';

import { SerialiserService } from './serialiser.service';

describe('SerialiserService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [SerialiserService]
    })
  );

  it(
    'should be created',
    inject([SerialiserService], (service: SerialiserService) => {
      expect(service).toBeTruthy();
    })
  );

  it(
    'should serialise a primitive type',
    inject([SerialiserService], (service: SerialiserService) => {
      expect(service.serialise('asdf')).toBe('asdf');
    })
  );

  it(
    'should serialise an array',
    inject([SerialiserService], (service: SerialiserService) => {
      expect(service.serialise([1, 2, 3])).toEqual([1, 2, 3]);
    })
  );

  it(
    'should serialise an object',
    inject([SerialiserService], (service: SerialiserService) => {
      const object = { a: 'foo', b: [1, 2, 3], c: { subC: 'bar' } };
      const serialised = service.serialise(object);
      expect(serialised).toEqual(object);
    })
  );

  it(
    'should serialise a moment',
    inject([SerialiserService], (service: SerialiserService) => {
      expect(service.serialise(moment(new Date(2018, 0, 1)))).toEqual('2018-01-01');
    })
  );

  it(
    'should serialise an object with moment and date',
    inject([SerialiserService], (service: SerialiserService) => {
      expect(
        service.serialise({
          january: moment(new Date(2018, 0, 1)),
          february: new Date(2018, 1, 1),
          march: '2018-03-01'
        })
      ).toEqual({
        january: '2018-01-01',
        february: '2018-02-01',
        march: '2018-03-01'
      });
    })
  );

  it(
    'should serialise a kitchen sink array',
    inject([SerialiserService], (service: SerialiserService) => {
      expect(
        service.serialise([
          1,
          'foo',
          moment(new Date(1982, 1, 10)),
          new Date(1983, 1, 12),
          { foo: 'bar' }
        ])
      ).toEqual([1, 'foo', '1982-02-10', '1983-02-12', { foo: 'bar' }]);
    })
  );
});
