import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DeserialiserService } from './deserialiser.service';

@Injectable({
  providedIn: 'root'
})
export class DeserialiserInterceptor implements HttpInterceptor {
  constructor(private deserialiserService: DeserialiserService) {}

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      map(event =>
        event instanceof HttpResponse
          ? event
          : ({
              ...event,
              body: this.deserialiserService.deserialise((event as any).body)
            } as any)
      )
    );
  }
}
