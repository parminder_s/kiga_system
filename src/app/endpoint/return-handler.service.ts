import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { EndpointOptions } from './endpoint-options';
import { SuccessHandlerService } from './success-handler.service';

@Injectable({
  providedIn: 'root'
})
export class ReturnHandlerService {
  constructor(private successHandler: SuccessHandlerService) {}

  handle<T>(observable: Observable<T>, options: EndpointOptions): Observable<T> {
    return observable.pipe(map(response => this.successHandler.handle(response)));
  }
}
