import { SuccessHandlerService } from './success-handler.service';
import { DeserialiserService } from './deserialiser.service';
import { mockClass } from '../mockClass';
import { Location } from '@angular/common';

describe('EndpointSuccessHandlerService', () => {
  it('should redirect if value is set', () => {
    const deserialiser = mockClass<DeserialiserService>({
      deserialise: () => ({
        redirect: 'foo'
      })
    });
    const go = jest.fn();
    const location = mockClass<Location>({ go });

    new SuccessHandlerService(deserialiser, location).handle('');

    expect(go).toHaveBeenCalledWith('foo');
  });

  it('should return the deserialsed value', () => {
    const deserialiser = mockClass<DeserialiserService>({ deserialise: () => 'foo' });
    const service = new SuccessHandlerService(deserialiser, null);

    expect(service.handle(null)).toEqual('foo');
  });

  it('should handle response of null', () => {
    const deserialiser = mockClass<DeserialiserService>({ deserialise: () => null });
    const service = new SuccessHandlerService(deserialiser, null);

    expect(service.handle(null)).toBeNull();
  });
});
