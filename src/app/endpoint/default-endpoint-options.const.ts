import { EndpointOptions } from './endpoint-options';

export const defaultEndpointOptions: EndpointOptions = {
  endpoint: true, // connect to Spring (new) backend
  rawUrl: false // don't prefix url for Spring or legacy backend
};
