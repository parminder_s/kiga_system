export interface EndpointOptions {
  endpoint?: boolean; //connect to Spring instead SilverStripe
  rawUrl?: boolean; //don't prefix the host
}
