import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class SerialiserService {
  constructor() {}

  public serialise(object: any): any {
    if (_.isArray(object)) {
      return object.map(element => this.serialise(element));
    } else if (_.isDate(object)) {
      return moment(object).format('YYYY-MM-DD');
    } else if (_.isObject(object)) {
      if (moment.isMoment(object)) {
        return object.format('YYYY-MM-DD');
      } else {
        return _.fromPairs(_.map(object, (value, key) => [key, this.serialise(value)]));
      }
    } else {
      return object;
    }
  }
}
