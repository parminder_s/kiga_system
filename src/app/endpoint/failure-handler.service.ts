import { Injectable } from '@angular/core';
import { AngularJsStateService } from '../compat/angular-js-state.service';
import { LocaleInfoService } from '../base-data/locale-info.service';
import { RedirectorService } from '../shared/redirector.service';
import { ShowExceptionService } from '../shared/show-exception.service';
import { EndpointOptions } from './endpoint-options';
import { Route, Router } from '@angular/router';
import { get } from 'lodash';
import { LoaderService } from '../shared/loader.service';

/**
 * this function handles errors from the endpoint. If the errorCode
 * is specified we handle it here, which usually is a redirection.
 *
 * In all cases an error is thrown, so that the observable doesn't
 * complete.
 */
@Injectable({
  providedIn: 'root'
})
export class FailureHandlerService {
  constructor(
    private angularJsService: AngularJsStateService,
    private router: Router,
    private showException: ShowExceptionService,
    private localeInfo: LocaleInfoService,
    private loader: LoaderService
  ) {}

  handle<T>(response: any = {}): T {
    const errorCode = get(response, 'error.errorCode');
    const locale = this.localeInfo.getLocale();
    if (errorCode === 'login_required') {
      this.router.navigateByUrl(this.angularJsService.getUrl('root.login'));
    } else if (errorCode === 'parallel_login') {
      this.router.navigateByUrl(
        this.angularJsService.getUrl('root.parallelLogInfo.entryId', { entryId: response.entryId })
      );
    } else if (errorCode === 'crm_session_fail') {
      this.angularJsService.getUrl('root.login', {
        locale: locale,
        forward: 'root.member.account'
      });
    } else {
      this.showException.show(response.errorCode);
    }
    this.loader.hide();

    throw new Error(`error in response ${JSON.stringify(response)}`);
  }
}
