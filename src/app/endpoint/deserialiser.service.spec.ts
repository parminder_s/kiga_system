import { TestBed, inject } from '@angular/core/testing';

import { DeserialiserService } from './deserialiser.service';

describe('DeserialiserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeserialiserService]
    });
  });

  it(
    'should be created',
    inject([DeserialiserService], (service: DeserialiserService) => {
      expect(service).toBeTruthy();
    })
  );

  it(
    'should deserialise a kitchen sink object',
    inject([DeserialiserService], (service: DeserialiserService) => {
      expect(
        service.deserialise({
          a: 1,
          b: 'foo',
          c: { subC: 1 },
          aDate: '1981-12-31',
          anotherDateTime: '1982-01-01',
          datetime: 'asdf',
          date: 5
        })
      ).toEqual({
        a: 1,
        b: 'foo',
        c: { subC: 1 },
        aDate: new Date(1981, 11, 31),
        anotherDateTime: new Date(1982, 0, 1),
        datetime: 'asdf',
        date: 5
      });
    })
  );

  it(
    'should deserialise a kitchen sink array',
    inject([DeserialiserService], (service: DeserialiserService) => {
      expect(service.deserialise([1, 'foo', { subC: 1 }, '1981-12-31'])).toEqual([
        1,
        'foo',
        { subC: 1 },
        '1981-12-31'
      ]);
    })
  );
});
