import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DeserialiserService {
  constructor() {}

  deserialise<T>(object: any): T {
    this.doSerialisation(object);
    return object;
  }

  private doSerialisation(object: any): any {
    if (_.isArray(object)) {
      return _.map(object, element => this.doSerialisation(element));
    } else if (_.isObject(object)) {
      for (let key in object) {
        let value = object[key];
        if (key.match(/Date$/) || key.match(/DateTime$/)) {
          object[key] = moment(value).toDate();
        } else if (_.isObject(value) || _.isArray(value)) {
          object[key] = this.doSerialisation(value);
        }
      }
      return object;
    } else {
      return object;
    }
  }
}
