import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EndpointService } from './endpoint.service';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  providers: [EndpointService],
  declarations: []
})
export class EndpointModule {}
