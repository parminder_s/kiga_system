import { inject, TestBed } from '@angular/core/testing';
import { LocaleInfoService } from '../base-data/locale-info.service';
import { EndpointMethod } from './endpoint-method.enum';
import { EndpointOptions } from './endpoint-options';
import { EndpointService } from './endpoint.service';
import { RequesterService } from './requester.service';
import { ReturnHandlerService } from './return-handler.service';

describe('EndpointService', () => {
  let requesterFn = jest.fn();

  let localeInfo = {
    getLocale: () => 'de'
  };
  let returnHandler = {
    handle: jest.fn()
  };

  beforeEach(() => {
    requesterFn.mockClear();
    TestBed.configureTestingModule({
      providers: [
        EndpointService,
        { provide: RequesterService, useValue: { request: requesterFn } },
        { provide: ReturnHandlerService, useValue: returnHandler },
        { provide: LocaleInfoService, useValue: localeInfo }
      ]
    });
  });

  it('should be created', inject([EndpointService], (service: EndpointService) => {
    expect(service).toBeTruthy();
  }));

  it('pass the current locale to the post body', inject(
    [EndpointService],
    (service: EndpointService) => {
      service.post('somewhere', { foo: 'bar' });
      const [url, , method, data] = requesterFn.mock.calls[0];
      expect(data.locale).toBe('de');
      expect(url).toBe('somewhere');
      expect(method).toBe(EndpointMethod.POST);
    }
  ));

  it('should set default options in get', inject([EndpointService], (service: EndpointService) => {
    service.get('foobar');
    const [url, options] = requesterFn.mock.calls[0];
    expect(options).toEqual({ endpoint: true, rawUrl: false });
    expect(url).toEqual('foobar');
  }));

  it('should not overwrite existing options in get', inject(
    [EndpointService],
    (service: EndpointService) => {
      service.get('foobar');
      const options = requesterFn.mock.calls[0][1] as EndpointOptions;
      expect(options).toEqual({ endpoint: true, rawUrl: false });
    }
  ));

  it('should apply defaults post', inject([EndpointService], (service: EndpointService) => {
    service.post('foobar', { locale: 'at' });
    const [url, options, , data] = requesterFn.mock.calls[0];
    expect(url).toBe('foobar');
    expect(options).toEqual({
      endpoint: true,
      rawUrl: false
    });
    expect(data).toEqual({ locale: 'at' });
  }));
});
