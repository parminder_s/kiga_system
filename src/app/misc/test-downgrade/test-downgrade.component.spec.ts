import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestDowngradeComponent } from './test-downgrade.component';

describe('TestDowngradeComponent', () => {
  let component: TestDowngradeComponent;
  let fixture: ComponentFixture<TestDowngradeComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [TestDowngradeComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestDowngradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
