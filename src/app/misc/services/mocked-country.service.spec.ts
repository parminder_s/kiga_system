import { TestBed, inject } from '@angular/core/testing';

import { MockedCountryService } from './mocked-country.service';

describe('MockedCountryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockedCountryService]
    });
  });

  it(
    'should be created',
    inject([MockedCountryService], (service: MockedCountryService) => {
      expect(service).toBeTruthy();
    })
  );
});
