import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { configSelector } from '../../base-data/base-data.selectors';
import { EndpointService } from '../../endpoint/endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class MockedCountryService {
  public countries: Array<any>;
  private changeCountryViaCookie: boolean;

  constructor(private endpoint: EndpointService, private store: Store<any>) {
    this.store
      .select(configSelector)
      .subscribe(config => (this.changeCountryViaCookie = config.changeCountryViaCookie));
  }

  isEnabled() {
    return this.changeCountryViaCookie;
  }

  isMocked(): boolean {
    if (!this.isEnabled()) {
      return false;
    }

    return !!this.getCookieValue();
  }

  getMockedCountry(): string {
    if (this.isMocked()) {
      return this.getCookieValue();
    }
  }

  // switchCountry(code) {
  //   this.endpoint({ url: 'countries/switch', data: { newCountryCode: code } }).then(() =>
  //     window.location.reload()
  //   );
  // }

  // remove() {
  //   this.endpoint({ url: 'countries/remove' }).then(() => window.location.reload());
  // }

  private getCookieValue(): string {
    let matches = document.cookie.match(/countryCode=(\w+)/);
    if (matches) {
      return matches[1];
    } else {
      return null;
    }
  }
}
