import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestDowngradeComponent } from './test-downgrade/test-downgrade.component';

import { downgradeComponent } from '@angular/upgrade/static';
import { NgUpgradeDirective } from './ng-upgrade.directive';

try {
  angular.module('misc').directive('testDowngrade', downgradeComponent({
    component: TestDowngradeComponent
  }) as angular.IDirectiveFactory);
} catch (e) {
} finally {
}

@NgModule({
  imports: [CommonModule],
  declarations: [TestDowngradeComponent, NgUpgradeDirective],
  entryComponents: [TestDowngradeComponent]
})
export class MiscModule {}
