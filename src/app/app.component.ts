import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { DOCUMENT } from '@angular/common';
import { AfterContentInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { UpgradeModule } from '@angular/upgrade/static';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Link } from '../../app/src/misc/js/services/AngularInterfaces';
import { SetMediaQuerys } from './+state/media-query.actions';
import { BaseDataLoad } from './base-data/base-data.actions';
import { linksSelector, localeSelector } from './base-data/base-data.selectors';
import { PlatformService } from './misc/services/platform.service';
import { LocationForLocaleParser } from './shared/location-for-locale-parser.service';

/**
 * The root component. It simply sets up the system by calling
 * the bootstrapper service that fetches required data from
 * the server.
 *
 * Be aware of router.events processing in the constructor.
 * We do store a static variable called localeLoaded, because
 * in the event of SSR all the data has already been loaded
 * and there is no requirement to redo the BaseLoad.
 */
@Component({
  selector: 'kiga-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements AfterContentInit, OnInit, OnDestroy {
  public isNgxRoute = false;
  private isLogin = false;
  public footerPages$: Observable<Array<Link>>;
  private currentLocale: string;
  private subscriptions: Array<Subscription> = [];

  static localeLoaded = {};

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private platform: PlatformService,
    private upgrade: UpgradeModule,
    router: Router,
    private locationForLocaleParser: LocationForLocaleParser,
    private store: Store<any>,
    private breakpointObserver: BreakpointObserver
  ) {
    this.subscriptions.push(
      router.events
        .pipe(filter(event => event instanceof NavigationStart))
        .subscribe(({ url }: NavigationStart) => {
          this.isNgxRoute = url.startsWith('/ng6/') || url === '/';
          this.isLogin = url.indexOf('login') !== -1;
          if (this.isNgxRoute) {
            const localeMatcher = url.match(/\/ng6\/(\w{2})/);
            if (localeMatcher) {
              const urlLocale = this.locationForLocaleParser.parseLocale(url);
              if (urlLocale !== this.currentLocale || !AppComponent.localeLoaded[urlLocale]) {
                this.store.dispatch(new BaseDataLoad(urlLocale));
                AppComponent.localeLoaded[urlLocale] = true;
              }
            }
          }
        })
    );
  }

  ngOnInit() {
    this.footerPages$ = this.store.pipe(
      select(linksSelector),
      map(links => links.footerPages)
    );
    this.store.pipe(select(localeSelector)).subscribe(locale => (this.currentLocale = locale));
    this.subscriptions.push(
      this.breakpointObserver
        .observe([
          '(min-width: 480px)',
          '(min-width: 640px)',
          '(min-width: 768px)',
          '(min-width: 960px)',
          '(min-width: 1120px)',
          '(min-width: 1280px)'
        ])
        .subscribe((state: BreakpointState) => this.store.dispatch(new SetMediaQuerys(state)))
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  ngAfterContentInit() {
    if (this.platform.isBrowser()) {
      this.upgrade.bootstrap(this.document.body, ['main']);
    }
  }

  public locationIsLogin(): boolean {
    return this.isLogin;
  }
}
