import { Injectable } from '@angular/core';
import { PlatformService } from './misc/services/platform.service';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsNotifierService {
  private isOnBrowser = false;
  constructor(platform: PlatformService) {
    this.isOnBrowser = platform.isBrowser();
  }

  public notify(url: string) {
    const isNg6Url = url.match(/^\/ng6/);
    if (this.isOnBrowser && isNg6Url && window['ga']) {
      const ngUrl = `/ng${url}`;
      window['ga']('send', 'pageview', `${ngUrl}`);
    }
  }
}
