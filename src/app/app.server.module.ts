import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';
import { AppComponent } from './app.component';
import * as process from 'process';

import { AppModule } from './app.module';
import { KigaInitData } from './data/KigaInitData';
import { EndpointModule } from './endpoint/endpoint.module';
import { ServerTransferStateModule } from '@angular/platform-server';

@NgModule({
  imports: [
    AppModule,
    EndpointModule,
    ServerModule,
    ModuleMapLoaderModule,
    ServerTransferStateModule
  ],
  providers: [
    {
      provide: KigaInitData,
      useFactory: () =>
        new KigaInitData(
          process.env.KIGA_ENDPOINTURL,
          process.env.KIGA_LEGACYURL,
          !!process.env.KIGA_NEWBOOTSTRAPENDPOINTENABLED,
          process.env.KIGA_CLIENTURL
        )
    }
  ],
  bootstrap: [AppComponent]
})
export class AppServerModule {}
