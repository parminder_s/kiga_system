import { TestBed } from '@angular/core/testing';

import { AnalyticsNotifierService } from './analytics-notifier.service';

describe('AnalyticsNotifierService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnalyticsNotifierService = TestBed.get(AnalyticsNotifierService);
    expect(service).toBeTruthy();
  });
});
