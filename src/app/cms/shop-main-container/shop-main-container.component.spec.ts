import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopMainContainerComponent } from './shop-main-container.component';

describe('ShopMainContainerComponent', () => {
  let component: ShopMainContainerComponent;
  let fixture: ComponentFixture<ShopMainContainerComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ShopMainContainerComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopMainContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
