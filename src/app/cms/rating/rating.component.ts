import { Component, Input, OnInit } from '@angular/core';
import { Rating } from '../rating';

@Component({
  selector: 'kiga-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.less']
})
export class RatingComponent implements OnInit {
  @Input() rating: Rating = <any>{};

  constructor() {}

  ngOnInit() {}

  getStars(): string[] {
    const returner = [];
    for (let i = 0; i < 5; ++i) {
      if (this.rating.rating <= i) {
        returner.push('fa-star-o');
      } else if (this.rating.rating === i + 0.5) {
        returner.push('fa-star-half-o');
      } else if (this.rating.rating > i + 0.5) {
        returner.push('fa-star');
      }
    }
    return returner;
  }
}
