import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { selectRouterStateUrl } from '../../router-store/router-selectors';
import { LoadResolves } from '../actions/resolve.actions';
import { selectResolveResponse } from '../reducers/resolve.selectors';

@Component({
  selector: 'kiga-cms',
  templateUrl: './cms.component.html',
  styleUrls: ['./cms.component.less']
})
export class CmsComponent implements OnInit, OnDestroy {
  public resolvedType$: Observable<string>;
  private subscriptions: Array<Subscription> = [];

  constructor(private store: Store<any>) {
    this.subscriptions = [
      this.store
        .pipe(
          select(selectRouterStateUrl),
          filter(state => state.hasNavigationEnded)
        )
        .subscribe(() => this.store.dispatch(new LoadResolves()))
    ];
  }

  ngOnInit() {
    this.resolvedType$ = this.store.pipe(select(selectResolveResponse));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
