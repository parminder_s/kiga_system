import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { DynamicModule } from 'ng-dynamic-component';
import { SharedModule } from '../shared/shared.module';
import { CmsComponent } from './cms/cms.component';
import { IdeasLoadedEffect } from './effects/ideas-loaded.effect';
import { ResolveEffects } from './effects/resolve.effects';
import { SelectIdeaEffect } from './effects/select-idea.effect';
import { SelectNextOrPreviousIdeaEffect } from './effects/select-next-or-previous.effect';
// tslint:disable-next-line:max-line-length
import { IdeaGroupContainerContainerComponent } from './idea-group-container-container/idea-group-container-container.component';
// tslint:disable-next-line:max-line-length
import { IdeaCategoriesComponent } from './idea-group-container/idea-categories/idea-categories.component';
import { IdeaGroupContainerComponent } from './idea-group-container/idea-group-container.component';
// tslint:disable-next-line:max-line-length
import { ParentCategoryComponent } from './idea-group-container/parent-category/parent-category.component';
import { IdeaMenuComponent } from './idea-menu/idea-menu.component';
// tslint:disable-next-line:max-line-length
import { IdeaPreviewContainerComponent } from './idea-preview-container/idea-preview-container.component';
import { IdeaPreviewComponent } from './idea-preview/idea-preview.component';
// tslint:disable-next-line:max-line-length
import { IdeaThumbnailContainerComponent } from './idea-thumbnail-container/idea-thumbnail-container.component';
import { IdeaThumbnailComponent } from './idea-thumbnail/idea-thumbnail.component';
// tslint:disable-next-line:max-line-length
import { ParentCategoryContainerComponent } from './parent-category-container/parent-category-container.component';
import { RatingComponent } from './rating/rating.component';
import { ideaReducer } from './reducers/idea.reducer';
import { resolveReducer } from './reducers/resolve.reducer';
import { ShopMainContainerComponent } from './shop-main-container/shop-main-container.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';

@NgModule({
  imports: [
    CommonModule,
    DynamicModule.withComponents([ParentCategoryContainerComponent]),
    RouterModule.forChild([
      {
        path: '',
        component: CmsComponent
      }
    ]),
    SharedModule,
    StoreModule.forFeature('cms', { idea: ideaReducer, resolve: resolveReducer }),
    EffectsModule.forFeature([
      IdeasLoadedEffect,
      SelectIdeaEffect,
      SelectNextOrPreviousIdeaEffect,
      ResolveEffects
    ]),
    TranslateModule.forChild()
  ],
  entryComponents: [CmsComponent, IdeaThumbnailContainerComponent, SubCategoryComponent],
  declarations: [
    CmsComponent,
    IdeaMenuComponent,
    ShopMainContainerComponent,
    IdeaGroupContainerComponent,
    ParentCategoryComponent,
    IdeaCategoriesComponent,
    IdeaThumbnailComponent,
    IdeaThumbnailContainerComponent,
    IdeaPreviewComponent,
    RatingComponent,
    IdeaPreviewContainerComponent,
    ParentCategoryContainerComponent,
    IdeaGroupContainerContainerComponent,
    SubCategoryComponent,
    IdeaThumbnailContainerComponent
  ]
})
export class CmsModule {}
