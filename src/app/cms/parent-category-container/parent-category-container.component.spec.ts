import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentCategoryContainerComponent } from './parent-category-container.component';

describe('ParentCategoryContainerComponent', () => {
  let component: ParentCategoryContainerComponent;
  let fixture: ComponentFixture<ParentCategoryContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentCategoryContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentCategoryContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
