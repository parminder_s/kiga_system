import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { selectUrlSegments } from '../../router-store/router-selectors';

@Component({
  selector: 'kiga-parent-category-container',
  templateUrl: 'parent-category-container.component.html'
})
export class ParentCategoryContainerComponent implements OnInit {
  @Input() data: any;
  @Input() isActive$: Observable<boolean>;

  constructor(private store: Store<any>, private router: Router) {}

  ngOnInit() {
    this.isActive$ = this.store.pipe(
      select(selectUrlSegments),
      map(urlSegments => this.isActive(urlSegments))
    );
  }

  onCategorySelected(category) {
    this.router.navigate(category.url);
  }

  private isActive(urlSegments: Array<string>): boolean {
    const activeCategory = urlSegments.length >= 2 ? urlSegments[1] : null;
    return this.data.name === activeCategory;
  }
}
