import { Rating } from './rating';

export interface Idea {
  ageRange: string;
  description: string;
  download: boolean;
  facebookShare: boolean;
  id: number;
  inPool: boolean;
  largePreviewImageUrl: string;
  layoutType: string;
  level1Title: string;
  level2Title: string;
  links: any;
  name: string;
  ngUrl: string;
  previewPages: any[];
  previewUrl: string;
  rating: Rating;
  title: string;
  url: string;
  favourite: boolean;
  audioLink?: string;
  videoLink?: string;
}
