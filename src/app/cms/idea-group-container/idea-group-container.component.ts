import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'kiga-idea-group-container',
  templateUrl: './idea-group-container.component.html',
  styleUrls: ['./idea-group-container.component.less']
})
export class IdeaGroupContainerComponent {
  @Input() public data: any;
  @Input() public showBreadcrumbs: boolean;
}
