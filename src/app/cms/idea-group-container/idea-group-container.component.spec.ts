import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaGroupContainerComponent } from './idea-group-container.component';

describe('IdeaGroupContainerComponent', () => {
  let component: IdeaGroupContainerComponent;
  let fixture: ComponentFixture<IdeaGroupContainerComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [IdeaGroupContainerComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaGroupContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
