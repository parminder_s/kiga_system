import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'kiga-parent-category',
  templateUrl: './parent-category.component.html',
  styleUrls: ['./parent-category.component.less']
})
export class ParentCategoryComponent {
  @Input() category: any;
  @Input() isActive: boolean;
  @Output() categorySelected = new EventEmitter();

  onCategoryClick(): void {
    this.categorySelected.emit(this.category);
  }
}
