import { Component, Input, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { ScrollPaneRowComponent } from '../../../shared/scroll-pane-row/scroll-pane-row.component';
import {
  CloseIdea,
  SelectIdea,
  SelectNextIdea,
  SelectPreviousIdea
} from '../../actions/idea.actions';
import { Idea } from '../../idea';
import { selectSelection } from '../../reducers/idea.selectors';
import { selectBreakpointName } from '../../../+state/media-query.reducer';
// tslint:disable-next-line:max-line-length
import { IdeaThumbnailContainerComponent } from '../../idea-thumbnail-container/idea-thumbnail-container.component';

@Component({
  selector: 'kiga-idea-categories',
  templateUrl: './idea-categories.component.html',
  styleUrls: ['./idea-categories.component.less']
})
export class IdeaCategoriesComponent implements OnInit, OnDestroy {
  @Input() useGrid = false;
  @Input() categories: any[];
  @Input() lastCategoryLevel: boolean;
  @Input() isGridNewIdeas: boolean;
  @ViewChildren(ScrollPaneRowComponent) scrollPaneRows: QueryList<ScrollPaneRowComponent>;
  component = IdeaThumbnailContainerComponent;
  desktopCategories = [];
  static itemsPerRowMapping = {
    xxlg: 7,
    xlg: 6,
    lg: 5,
    lmd: 4,
    md: 3
  };
  public breakpointName = '';
  public selectedIdea: Idea;

  public selectedIdeaIndex: number;
  public selectedCategoryIndex: number;
  public itemsPerRow = 0;
  private subscriptions: Subscription[] = [];

  constructor(private store: Store<any>) {}

  ngOnInit() {
    this.subscriptions = [
      this.store.pipe(select(selectBreakpointName)).subscribe(data => {
        this.breakpointName = data;
        this.itemsPerRow = IdeaCategoriesComponent.itemsPerRowMapping[this.breakpointName];
      }),
      this.store.pipe(select(selectSelection)).subscribe(({ idea, rowIx, ideaIx }) => {
        this.selectedIdea = idea;
        this.selectedIdeaIndex = ideaIx;
        this.selectedCategoryIndex = rowIx;
        if (idea && this.scrollPaneRows && !this.useGrid) {
          /**
           * On init, the scrollPaneRows is not available, which is not a problem, because
           * the scrolling required is only required on subsequent idea selections.
           */
          const row = this.scrollPaneRows.toArray()[rowIx];
          if (row) {
            row.scrollIntoView(ideaIx);
          }
        }
      })
    ];
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  onIdeaClick({ data: idea, index: ideaIx }, rowIx) {
    if (this.selectedIdea === idea) {
      this.store.dispatch(new CloseIdea());
    } else {
      this.store.dispatch(new SelectIdea(idea, rowIx, ideaIx));
    }
  }

  showNextIdea() {
    this.store.dispatch(new SelectNextIdea());
  }

  showPreviousIdea() {
    this.store.dispatch(new SelectPreviousIdea());
  }

  get selectedCategoryIdeas(): any[] {
    if (_.isNumber(this.selectedIdeaIndex) && _.isNumber(this.selectedCategoryIndex)) {
      return _.get(this.categories, `[${this.selectedCategoryIndex}].ideas`, []);
    }
    return [];
  }

  get gridIdeas(): Idea[] {
    return this.categories[0].ideas;
  }

  getIdeasForMobile(category): Array<Idea> {
    if (this.lastCategoryLevel) {
      return category.ideas;
    } else {
      return category.ideas.slice(0, 3);
    }
  }

  showPreview(index: number): boolean {
    const isIdeaSelected = this.selectedIdeaIndex !== null;
    const isRowEnd = (index + 1) % this.itemsPerRow === 0 || index === this.gridIdeas.length - 1;
    const isThisRow =
      this.selectedIdeaIndex <= index &&
      this.selectedIdeaIndex >= index - (index % this.itemsPerRow);
    return isIdeaSelected && isRowEnd && isThisRow;
  }

  getGridIdeaSpacing(): string {
    return `calc((100%/${this.itemsPerRow} - 120px)/4)`;
  }

  showSubcategoryButton(category): boolean {
    return this.lastCategoryLevel !== true && category.ideas.length >= 13;
  }
}
