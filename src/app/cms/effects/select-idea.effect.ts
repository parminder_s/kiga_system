import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { SelectIdea, IdeaActionTypes, SelectIdeaProcessed } from '../actions/idea.actions';
import { withLatestFrom, mergeMap } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { selectIdeaViewModel } from '../reducers/idea.selectors';
import { of } from 'rxjs';

@Injectable()
export class SelectIdeaEffect {
  @Effect()
  selectIdea$ = this.actions$.pipe(
    ofType<SelectIdea>(IdeaActionTypes.SelectIdea),
    withLatestFrom(this.store.pipe(select(selectIdeaViewModel))),
    mergeMap(([{ idea, ideaIx, rowIx }, { categories }]) => {
      const ideasCount = categories[rowIx].ideas.length;
      return of(
        new SelectIdeaProcessed({
          ...{ idea, ideaIx, rowIx },
          hasNext: ideasCount > ideaIx + 1,
          hasPrevious: ideaIx > 0
        })
      );
    })
  );

  constructor(private store: Store<any>, private actions$: Actions) {}
}
