import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { filter, map, mergeMap, withLatestFrom } from 'rxjs/operators';
import {
  selectLocale,
  selectQueryParams,
  selectUrlSegments
} from '../../router-store/router-selectors';
import { IdeasLoaded } from '../actions/idea.actions';
import { LoadResolvesSuccess, ResolveActionTypes } from '../actions/resolve.actions';
import { ResolveService } from '../resolve.service';

@Injectable()
export class ResolveEffects {
  @Effect()
  load$ = this.actions$.pipe(
    ofType(ResolveActionTypes.LoadResolves),
    withLatestFrom(
      this.store.pipe(select(selectLocale)),
      this.store.pipe(select(selectUrlSegments)),
      this.store.pipe(select(selectQueryParams))
    ),
    mergeMap(data => {
      const [, locale, urlSegments, queryParams] = data;
      return this.resolveService
        .getResolve(urlSegments, locale, parseInt(queryParams['ageGroupFilter'], 10) || 0)
        .pipe(map(response => new LoadResolvesSuccess(response)));
    })
  );

  @Effect()
  $loadResolvesSuccess = this.actions$.pipe(
    ofType(ResolveActionTypes.LoadResolvesSuccess),
    filter(
      (action: LoadResolvesSuccess) =>
        ['IdeaGroupContainer', 'IdeaGroupCategory', 'IdeaGroup'].indexOf(
          action.payload.resolvedType
        ) > -1
    ),
    map(action => new IdeasLoaded(action.payload))
  );

  constructor(
    private actions$: Actions,
    private store: Store<any>,
    private resolveService: ResolveService
  ) {}
}
