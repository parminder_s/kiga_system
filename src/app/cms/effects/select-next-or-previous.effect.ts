import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import {
  SelectPreviousIdea,
  SelectNextIdea,
  IdeaActionTypes,
  SelectIdea
} from '../actions/idea.actions';
import { withLatestFrom, mergeMap } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { ideaSelector } from '../reducers/idea.selectors';
import { of } from 'rxjs';

@Injectable()
export class SelectNextOrPreviousIdeaEffect {
  @Effect()
  selectNextOrPreviousIdea$ = this.actions$.pipe(
    ofType<SelectPreviousIdea | SelectNextIdea>(
      IdeaActionTypes.SelectPreviousIdea,
      IdeaActionTypes.SelectNextIdea
    ),
    withLatestFrom(this.store.pipe(select(ideaSelector))),
    mergeMap(([action, { viewModel, ideaSelection: { idea, ideaIx, rowIx } }]) => {
      if (action.type === IdeaActionTypes.SelectNextIdea) {
        ideaIx++;
      } else {
        ideaIx--;
      }
      return of(new SelectIdea(viewModel.categories[rowIx].ideas[ideaIx], rowIx, ideaIx));
    })
  );

  public constructor(private actions$: Actions, private store: Store<any>) {}
}
