import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { mergeMap, withLatestFrom } from 'rxjs/operators';
import { MemberLoaderService } from '../../main/member-loader.service';
import { IdeaLinksGeneratorService } from '../idea-links-generator.service';
import { IdeasViewModel } from '../reducers/idea.reducer';
import { LoadedIdeasProcessed, IdeaActionTypes, IdeasLoaded } from '../actions/idea.actions';
import { SetArea } from '../../area/+state/area.action';
import { AreaType } from '../../area/area-type';
import {
  selectLocale,
  selectUrlSegments,
  selectQueryParams
} from '../../router-store/router-selectors';

@Injectable()
export class IdeasLoadedEffect {
  @Effect()
  rawIdeasLoaded$ = this.actions$.pipe(
    ofType<IdeasLoaded>(IdeaActionTypes.IdeasLoaded),
    withLatestFrom(
      this.store.pipe(select(selectLocale)),
      this.store.pipe(select(selectUrlSegments)),
      this.store.pipe(select(selectQueryParams)),
      this.memberLoader.get()
    ),
    mergeMap(([action, locale, urlSegments, queryParams, member]) => {
      const parentUrlSegments = urlSegments.slice(0, -1);
      const viewModel: IdeasViewModel = action.payload;
      viewModel.parentCategories.forEach(parentCategory => {
        parentCategory.url = `/ng6/${locale}/${parentUrlSegments.join('/')}/${parentCategory.name}`;
        parentCategory.queryParams = queryParams;
      });
      viewModel.categories.forEach(category => {
        category.ideas.forEach(
          idea => (idea.links = this.ideaLinksGenerator.getLinks(member, idea))
        );
        category.url = `/ng6/${locale}/${urlSegments.join('/')}/${category.name}`;
        category.queryParams = queryParams;
      });

      viewModel.breadcrumbs.forEach((breadcrumb, ix: number) => {
        const breadcrumbSegments = urlSegments.slice(0, ix + 2);
        breadcrumb.url = `/ng6/${locale}/${breadcrumbSegments.join('/')}`;
        breadcrumb.queryParams = queryParams;
      });

      this.store.dispatch(new SetArea(AreaType.IDEAS));

      return of(new LoadedIdeasProcessed(viewModel, queryParams['preview']));
    })
  );

  constructor(
    private actions$: Actions,
    private store: Store<any>,
    private ideaLinksGenerator: IdeaLinksGeneratorService,
    private memberLoader: MemberLoaderService
  ) {}
}
