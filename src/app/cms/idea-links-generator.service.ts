import { Injectable } from '@angular/core';
import { AngularJsStateService } from '../compat/angular-js-state.service';
import { MemberInfoService } from '../main/member-info.service';
import { PermissionStateInfoService } from '../security/permission-state-info.service';
import { IdeaPermissionInfoService } from './idea-permission-info.service';
import { IdeaLinks } from './types/idea-links';
import { ngJsToNgUrl } from '../compat/ngJsToNgUrl';

@Injectable({
  providedIn: 'root'
})
export class IdeaLinksGeneratorService {
  constructor(
    private ideaPermissionInfo: IdeaPermissionInfoService,
    private memberInfo: MemberInfoService,
    private angularJsState: AngularJsStateService,
    private permissionStateInfo: PermissionStateInfoService
  ) {}

  getLinks(member, idea): IdeaLinks {
    const { planType, showEdit } = this.memberInfo.withMember(member, infoService => {
      if (infoService.isCms()) {
        return { planType: 'kiga', showEdit: true };
      } else {
        return { planType: 'member', showEdit: false };
      }
    });

    let editLink: string;
    if (idea.download === true) {
      editLink = this.angularJsState.getUrl('root.backend.cmsBackend.kiga-idea.edit', {
        ideaId: idea.id
      });
    } else {
      editLink = '/admin/show/' + idea.id;
    }

    let returner: IdeaLinks = {
      edit: { show: showEdit, link: editLink },
      planner: {
        show: true,
        link: this.angularJsState.getUrl('root.plan.planType.addTo.itemId', {
          itemId: idea.id,
          planType: planType
        }),
        state: 'root.plan.planType.addTo',
        stateParams: { itemId: idea.id, planType: planType }
      },
      view: null,
      favouritesDenied: { show: false, link: null },
      print: { show: true, link: null },
      printWithoutImage: { show: idea.layoutType === 'DYNAMIC', link: null }
    };
    let url = ngJsToNgUrl(idea.ngUrl);

    if (this.ideaPermissionInfo.hasPermission(member, idea.inPool, idea.facebookShare)) {
      returner.view = { show: true, link: url };
      returner.print.link = idea.url + '/PDF?origin=preview';
      returner.print.isExternal = true;
      returner.printWithoutImage.link = idea.url + '/PDFWOImage?origin=overview';
      returner.printWithoutImage.isExternal = true;
    } else {
      let permissionState = this.permissionStateInfo.get(member, !idea.inPool);
      let permissionLink = this.angularJsState.getUrl(permissionState, {});
      if (permissionState === 'root.login') {
        permissionLink = this.angularJsState.getUrl(permissionState, { forward: url });
      }

      returner.view = { show: true, link: permissionLink };
      returner.print.link = permissionLink;
      returner.print.isExternal = false;
      returner.printWithoutImage.link = permissionLink;
      returner.printWithoutImage.isExternal = false;
    }

    if (!this.ideaPermissionInfo.hasPermission(member, false)) {
      returner.planner.link = this.angularJsState.getUrl('root.plan.planType.detail.planId', {
        planId: 'current',
        planType: 'kiga'
      });
    }

    if (!this.ideaPermissionInfo.hasPermission(member, false)) {
      returner.favouritesDenied = {
        show: true,
        link: this.permissionStateInfo.get(member, true)
      };
    }

    return returner;
  }
}
