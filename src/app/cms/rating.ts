export interface Rating {
  count: number;
  rating: number;
  userRating: number;
}
