import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
// tslint:disable-next-line:max-line-length
import { ScrollPaneItemComponent } from '../../shared/scroll-pane-row/scroll-pane-item-component';
import { Idea } from '../idea';
import { ToggleFavourite } from '../../favourites/+state/favourites.actions';

@Component({
  selector: 'kiga-idea-thumbnail',
  templateUrl: './idea-thumbnail.component.html',
  styleUrls: ['./idea-thumbnail.component.less']
})
export class IdeaThumbnailComponent implements OnInit, ScrollPaneItemComponent<any> {
  @Input() data: Idea;
  @Input() index;
  @Input() isMobile = false;
  @Input() isFavourite: boolean;
  @Input() isActive: boolean;
  @Input() className = '';
  @Output() onClick = new EventEmitter<any>();

  constructor(private store: Store<any>) {}

  ngOnInit() {}

  onItemClick(): void {
    this.onClick.emit({ data: this.data, index: this.index });
  }

  toggleFavourite(e: Event) {
    e.stopPropagation();
    this.store.dispatch(new ToggleFavourite(this.data.id));
  }
}
