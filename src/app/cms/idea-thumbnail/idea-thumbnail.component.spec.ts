import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaThumbnailComponent } from './idea-thumbnail.component';

describe('IdeaThumbnailComponent', () => {
  let component: IdeaThumbnailComponent;
  let fixture: ComponentFixture<IdeaThumbnailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IdeaThumbnailComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaThumbnailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
