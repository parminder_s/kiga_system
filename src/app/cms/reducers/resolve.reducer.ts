import { ResolveActions, ResolveActionTypes } from '../actions/resolve.actions';
import { ResolveResponse } from '../resolve-response';

export interface State {
  resolvedType: string;
}

export const initialState: State = {
  resolvedType: ''
};

export function resolveReducer(state = initialState, action: ResolveActions): State {
  switch (action.type) {
    case ResolveActionTypes.LoadResolvesSuccess: {
      return {
        ...state,
        resolvedType: action.payload.resolvedType
      };
    }
    default:
      return state;
  }
}
