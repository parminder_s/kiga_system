import {
  IdeaActions,
  IdeaActionTypes,
  SelectIdeaProcessed,
  LoadedIdeasProcessed
} from '../actions/idea.actions';
import { Idea } from '../idea';

export type IdeasViewModel = any;

export interface IdeaSelection {
  rowIx: number;
  ideaIx: number;
  idea: Idea;
  hasNext: boolean;
  hasPrevious: boolean;
}

export interface State {
  viewModel: IdeasViewModel;
  ideaSelection: IdeaSelection;
}

const emptyIdeaSelection = {
  idea: null,
  rowIx: -1,
  ideaIx: -1,
  hasNext: false,
  hasPrevious: false
};

export const initialState: State = {
  viewModel: null,
  ideaSelection: emptyIdeaSelection
};

export function ideaReducer(state = initialState, action: IdeaActions): State {
  switch (action.type) {
    case IdeaActionTypes.LoadedIdeasProcessed: {
      const { viewModel, selectedIdeaUrlSegment } = action as LoadedIdeasProcessed;
      let ideaSelection = emptyIdeaSelection;

      if (selectedIdeaUrlSegment) {
        const categoryAndIdeaIx = findCategoryAndIdeaIxForUrlSegment(
          selectedIdeaUrlSegment,
          viewModel.categories
        );
        if (categoryAndIdeaIx) {
          const { categoryIx, ideaIx } = categoryAndIdeaIx;
          const ideas = viewModel.categories[categoryIx].ideas;
          ideaSelection = {
            rowIx: categoryIx,
            ideaIx: ideaIx,
            idea: ideas[ideaIx],
            hasPrevious: ideaIx > 0,
            hasNext: ideas.length > ideaIx + 1
          };
        }
      }

      return {
        ...state,
        ideaSelection: ideaSelection,
        viewModel: viewModel
      };
    }
    case IdeaActionTypes.CloseIdea: {
      return {
        ...state,
        ideaSelection: emptyIdeaSelection
      };
    }
    case IdeaActionTypes.SelectIdeaProcessed: {
      return {
        ...state,
        ideaSelection: (action as SelectIdeaProcessed).ideaSelection
      };
    }
    default:
      return state;
  }
}

interface CategoryAndIdeaIx {
  categoryIx: number;
  ideaIx: number;
}

function findCategoryAndIdeaIxForUrlSegment(
  urlSegment: string,
  categories: Array<any>
): CategoryAndIdeaIx {
  for (let categoryIx = 0; categoryIx < categories.length; categoryIx++) {
    const category = categories[categoryIx];
    const ideaIx = category.ideas.findIndex(idea => idea.name === urlSegment);
    if (ideaIx > -1) {
      return { categoryIx, ideaIx };
    }
  }
  return null;
}
