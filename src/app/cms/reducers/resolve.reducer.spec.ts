import { initialState, resolveReducer } from './resolve.reducer';

describe('Resolve Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = resolveReducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
