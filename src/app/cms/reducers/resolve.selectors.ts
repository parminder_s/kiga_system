import { ActionReducerMap, createSelector } from '@ngrx/store';
import * as fromIdea from './idea.reducer';
import * as fromResolve from './resolve.reducer';

interface State {
  idea: fromIdea.State;
  resolve: fromResolve.State;
}

export const cmsSelector: (state: any) => State = state => state.cms;

export const selectResolveState = createSelector(
  cmsSelector,
  (state: State) => state.resolve
);
export const selectResolveResponse = createSelector(
  selectResolveState,
  state => state.resolvedType
);
