import { createSelector } from '@ngrx/store';
import { cmsSelector } from './resolve.selectors';

export const ideaSelector = createSelector(
  cmsSelector,
  state => state.idea
);
export const selectSelection = createSelector(
  ideaSelector,
  state => state.ideaSelection
);

export const selectSelectionIdea = createSelector(
  selectSelection,
  state => state.idea
);
export const selectIdeaViewModel = createSelector(
  ideaSelector,
  state => state.viewModel
);
