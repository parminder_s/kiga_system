import { initialState, ideaReducer } from './idea.reducer';

describe('Idea Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = ideaReducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
