import { TestBed, inject } from '@angular/core/testing';

import { CmsLinkConverterService } from './cms-link-converter.service';

describe('CmsLinkConverterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CmsLinkConverterService]
    });
  });

  it('should be created', inject([CmsLinkConverterService], (service: CmsLinkConverterService) => {
    expect(service).toBeTruthy();
  }));
});
