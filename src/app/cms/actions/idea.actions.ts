import { Action } from '@ngrx/store';
import { Idea } from '../idea';
import { IdeasViewModel, IdeaSelection } from '../reducers/idea.reducer';

export enum IdeaActionTypes {
  IdeasLoaded = '[Idea] Ideas Loaded',
  LoadedIdeasProcessed = '[Idea] Loaded Ideas Processed',
  CloseIdea = '[Idea] Close Idea',
  SelectIdea = '[Idea] Select Idea',
  SelectNextIdea = '[Idea] Select Next Idea',
  SelectPreviousIdea = '[Idea] Select Previous Idea',
  SelectIdeaProcessed = '[Idea] Select Idea Processed'
}

export class IdeasLoaded implements Action {
  readonly type = IdeaActionTypes.IdeasLoaded;

  constructor(public payload: IdeasViewModel) {}
}

export class LoadedIdeasProcessed implements Action {
  readonly type = IdeaActionTypes.LoadedIdeasProcessed;

  constructor(public viewModel: IdeasViewModel, public selectedIdeaUrlSegment: string = '') {}
}

export class SelectIdea implements Action {
  readonly type = IdeaActionTypes.SelectIdea;

  constructor(public idea: Idea, public rowIx: number = 0, public ideaIx: number = 0) {}
}

export class SelectIdeaProcessed implements Action {
  readonly type = IdeaActionTypes.SelectIdeaProcessed;

  constructor(public ideaSelection: IdeaSelection) {}
}

export class CloseIdea implements Action {
  readonly type = IdeaActionTypes.CloseIdea;
  constructor() {}
}

export class SelectPreviousIdea implements Action {
  readonly type = IdeaActionTypes.SelectPreviousIdea;

  constructor() {}
}

export class SelectNextIdea implements Action {
  readonly type = IdeaActionTypes.SelectNextIdea;

  constructor() {}
}

export type IdeaActions =
  | IdeasLoaded
  | LoadedIdeasProcessed
  | CloseIdea
  | SelectIdea
  | SelectIdeaProcessed
  | SelectPreviousIdea
  | SelectNextIdea;
