import { Action } from '@ngrx/store';
import { ResolveResponse } from '../resolve-response';

export enum ResolveActionTypes {
  LoadResolves = '[Resolve] Load Resolves',
  LoadResolvesSuccess = '[Resolve] Load Resolves Success'
}

export class LoadResolves implements Action {
  readonly type = ResolveActionTypes.LoadResolves;
}

export class LoadResolvesSuccess implements Action {
  readonly type = ResolveActionTypes.LoadResolvesSuccess;
  constructor(public payload: ResolveResponse) {}
}

export type ResolveActions = LoadResolves | LoadResolvesSuccess;
