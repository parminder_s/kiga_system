import { TestBed, inject } from '@angular/core/testing';

import { IdeaPermissionInfoService } from './idea-permission-info.service';

describe('IdeaPermissionInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IdeaPermissionInfoService]
    });
  });

  it('should be created', inject(
    [IdeaPermissionInfoService],
    (service: IdeaPermissionInfoService) => {
      expect(service).toBeTruthy();
    }
  ));
});
