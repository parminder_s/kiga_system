import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { get } from 'lodash';
import { Subscription } from 'rxjs';
import { IdeasViewModel } from '../reducers/idea.reducer';
import { selectIdeaViewModel } from '../reducers/idea.selectors';

@Component({
  selector: 'kiga-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.less']
})
export class SubCategoryComponent implements OnInit, OnDestroy {
  @Input() data;
  @Input() index: number;

  subscriptions: Subscription[] = [];
  ideasViewModel: IdeasViewModel;
  constructor(store: Store<any>) {
    this.subscriptions.push(
      store.pipe(select(selectIdeaViewModel)).subscribe(data => (this.ideasViewModel = data))
    );
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  isActive(): boolean {
    return get(this.data, 'title') === get(this.ideasViewModel, 'activeCategory.title');
  }
}
