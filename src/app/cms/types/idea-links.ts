import { IdeaLink } from './idea-link';

export interface IdeaLinks {
  edit: IdeaLink;
  planner: IdeaLink;
  view: IdeaLink;
  print: IdeaLink;
  printWithoutImage: IdeaLink;
  favouritesDenied: IdeaLink;
}
