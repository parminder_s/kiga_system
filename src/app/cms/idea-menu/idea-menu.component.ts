import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
// tslint:disable-next-line:max-line-length
import { ParentCategoryContainerComponent } from '../parent-category-container/parent-category-container.component';
import { IdeasViewModel } from '../reducers/idea.reducer';
import { selectIdeaViewModel } from '../reducers/idea.selectors';
import { SubCategoryComponent } from '../sub-category/sub-category.component';
import { selectBreakpointName } from '../..//+state/media-query.reducer';

@Component({
  selector: 'kiga-idea-menu',
  templateUrl: './idea-menu.component.html',
  styleUrls: ['./idea-menu.component.less']
})
export class IdeaMenuComponent implements OnInit, OnDestroy {
  items = [];
  data: IdeasViewModel = {};
  itemsPerRowMap = {
    xxlg: 14,
    xlg: 12,
    lg: 10,
    lmd: 8,
    md: 6,
    lsm: 5,
    sm: 2
  };
  subscriptions: Subscription[] = [];
  breakpointName = 'xxlg';

  constructor(private store: Store<any>) {}

  get component() {
    if (this.useSubCategory() && this.atLeastMd()) {
      return SubCategoryComponent;
    }
    return ParentCategoryContainerComponent;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  get itemsPerRow(): number {
    let result = this.itemsPerRowMap[this.breakpointName];
    if (this.useSubCategory() && this.breakpointName !== 'sm') {
      result = Math.floor(result / 2);
    }
    return result;
  }

  ngOnInit() {
    this.subscriptions.push(
      this.store.pipe(select(selectIdeaViewModel)).subscribe(ideaViewModel => {
        this.data = ideaViewModel;
        this.items = this.getItems();
      })
    );
    this.subscriptions.push(
      this.store.pipe(select(selectBreakpointName)).subscribe(name => {
        this.breakpointName = name;
      })
    );
  }

  useSubCategory(): boolean {
    return this.data && this.data.firstLevelCategoryName;
  }

  getItems() {
    return this.useSubCategory()
      ? [
          {
            title: this.data.firstLevelCategoryName,
            url: this.getParentLevelUrl(),
            icon: 'icon-reply'
          },
          ...this.data.parentCategories
        ]
      : this.data.parentCategories;
  }

  getParentLevelUrl(): string {
    const url: string = this.data.breadcrumbs[this.data.breadcrumbs.length - 1].url;
    if (!url) {
      return '';
    }
    const splittedUrl = url.split('/');
    return splittedUrl.slice(0, splittedUrl.length - 1).join('/');
  }

  atLeastMd(): boolean {
    return this.itemsPerRowMap[this.breakpointName] >= this.itemsPerRowMap['md'];
  }
}
