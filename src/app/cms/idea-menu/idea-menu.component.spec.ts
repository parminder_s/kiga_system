import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaMenuComponent } from './idea-menu.component';

describe('IdeaMenuComponent', () => {
  let component: IdeaMenuComponent;
  let fixture: ComponentFixture<IdeaMenuComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [IdeaMenuComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
