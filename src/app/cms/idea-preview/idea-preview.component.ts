import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { ToggleFavourite } from '../../favourites/+state/favourites.actions';
import { UpdateMultimedia } from '../../shared/+state/multimedia.actions';
import { MultimediaStatus } from '../../shared/+state/multimedia.reducer';
import { selectMultimedia } from '../../shared/+state/multimedia.selectors';
import { Idea } from '../idea';

@Component({
  selector: 'kiga-idea-preview',
  templateUrl: './idea-preview.component.html',
  styleUrls: ['./idea-preview.component.less']
})
export class IdeaPreviewComponent implements OnInit, OnChanges, OnDestroy {
  @Input() idea: Idea;
  @Input() hasNext: boolean;
  @Input() hasPrevious: boolean;
  @Input() isFavourite: boolean;
  @Output() onNext = new EventEmitter();
  @Output() onPrevious = new EventEmitter();
  @Output() onClose = new EventEmitter();

  activePreviewPage = null;
  previewIndex = 0;
  showVideo = false;
  isPlayingVideo = false;
  readonly previewNumber = 3;
  private subscriptions: Subscription[] = [];

  // TODO: Define a ctrl because it is used in template.
  // Remove when template is fully updated
  ctrl = {};

  constructor(private store: Store<any>) {}

  get videoPlayerStatus(): MultimediaStatus {
    return this.isPlayingVideo ? MultimediaStatus.Play : MultimediaStatus.Pause;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.idea) {
      const { currentValue } = changes.idea;
      if (Array.isArray(currentValue.previewPages)) {
        this.activePreviewPage = currentValue.previewPages[0];
      }
    }
  }

  ngOnInit() {
    this.subscriptions.push(
      this.store.pipe(select(selectMultimedia)).subscribe(video => {
        this.isPlayingVideo =
          video.url === this.idea.videoLink && video.status === MultimediaStatus.Play;
      })
    );
  }

  onClickNext() {
    this.onNext.emit();
  }

  onClickPrevious() {
    this.onPrevious.emit();
  }

  close() {
    this.onClose.emit();
  }

  toggleFavourite(e: Event) {
    e.stopPropagation();
    this.store.dispatch(new ToggleFavourite(this.idea.id));
  }

  selectPreviewPage(page) {
    this.activePreviewPage = page;
  }

  previewUp() {
    this.previewIndex--;
  }

  previewDown() {
    this.previewIndex++;
  }

  getTransform(): string {
    return `translateY(-${94 * this.previewIndex}px)`;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  toggleVideo() {
    this.showVideo = true;
    this.store.dispatch(
      new UpdateMultimedia(
        this.idea.videoLink,
        this.isPlayingVideo ? MultimediaStatus.Pause : MultimediaStatus.Play
      )
    );
  }
}
