import { ResolveIdeaResponse } from './resolve-idea-response';

export type ResolveResponse = ResolveIdeaResponse;
