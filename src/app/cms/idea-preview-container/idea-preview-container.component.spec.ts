import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaPreviewContainerComponent } from './idea-preview-container.component';

describe('IdeaPreviewContainerComponent', () => {
  let component: IdeaPreviewContainerComponent;
  let fixture: ComponentFixture<IdeaPreviewContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdeaPreviewContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaPreviewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
