import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { getFavouritesDict } from '../../favourites/+state/favourites.selectors';
import { CloseIdea, SelectNextIdea, SelectPreviousIdea } from '../actions/idea.actions';
import { IdeaSelection } from '../reducers/idea.reducer';
import { selectSelection } from '../reducers/idea.selectors';

export interface IdeaSelectionAndIsFavourite {
  ideaSelection: IdeaSelection;
  isFavourite: boolean;
}

@Component({
  selector: 'kiga-idea-preview-container',
  templateUrl: './idea-preview-container.component.html'
})
export class IdeaPreviewContainerComponent implements OnInit {
  public ideaSelectionAndIsFavourite$: Observable<IdeaSelectionAndIsFavourite>;
  public isFavourite$: Observable<boolean>;

  constructor(private store: Store<any>) {}

  ngOnInit() {
    this.ideaSelectionAndIsFavourite$ = combineLatest(
      this.store.pipe(select(selectSelection)),
      this.store.pipe(select(getFavouritesDict))
    ).pipe(
      map(([ideaSelection, favouritesDict]) => {
        let returner: IdeaSelectionAndIsFavourite = { ideaSelection, isFavourite: false };
        if (ideaSelection.idea) {
          returner.isFavourite = !!favouritesDict[ideaSelection.idea.id];
        }
        return returner;
      })
    );
  }

  public handlePrevious() {
    this.store.dispatch(new SelectPreviousIdea());
  }

  public handleNext() {
    this.store.dispatch(new SelectNextIdea());
  }

  public handleClose() {
    this.store.dispatch(new CloseIdea());
  }
}
