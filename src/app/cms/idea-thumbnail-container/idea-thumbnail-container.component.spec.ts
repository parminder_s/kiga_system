import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaThumbnailContainerComponent } from './idea-thumbnail-container.component';

describe('IdeaThumbnailContainerComponent', () => {
  let component: IdeaThumbnailContainerComponent;
  let fixture: ComponentFixture<IdeaThumbnailContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IdeaThumbnailContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaThumbnailContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
