import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { getFavouritesDict } from '../../favourites/+state/favourites.selectors';
import { ScrollPaneItemComponent } from '../../shared/scroll-pane-row/scroll-pane-item-component';
import { Idea } from '../idea';
import { selectSelectionIdea } from '../reducers/idea.selectors';

@Component({
  selector: 'kiga-idea-thumbnail-container',
  templateUrl: './idea-thumbnail-container.component.html'
})
export class IdeaThumbnailContainerComponent implements OnInit, ScrollPaneItemComponent<any> {
  @Input() data: Idea;
  @Input() index: number;
  @Input() className: string;
  @Input() isActive$: Observable<boolean>;
  @Input() isFavourite$: Observable<boolean>;
  @Input() isMobile: boolean = false;

  @Output() onClick = new EventEmitter<any>();

  constructor(private store: Store<any>) {}

  ngOnInit() {
    this.isFavourite$ = this.store.pipe(
      select(getFavouritesDict),
      map(getFavouritesDict => !!getFavouritesDict[this.data.id])
    );
    this.isActive$ = this.store.pipe(
      select(selectSelectionIdea),
      map(idea => idea && idea.id === this.data.id)
    );
  }

  handleClick(idea: Idea) {
    this.onClick.emit(idea);
  }
}
