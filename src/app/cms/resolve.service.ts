import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EndpointService } from '../endpoint/endpoint.service';
import { ResolveResponse } from './resolve-response';

@Injectable({
  providedIn: 'root'
})
export class ResolveService {
  constructor(private endpoint: EndpointService) {}

  getResolve(pathSegments: string[], locale: string, ageGroup = 0): Observable<ResolveResponse> {
    return this.endpoint.post('cms/resolve', {
      ageGroup,
      locale,
      pathSegments
    });
  }
}
