import { Injectable } from '@angular/core';
import IMember from '../../../app/src/misc/permission/js/IMember';
import { MemberInfoService } from '../main/member-info.service';

/**
 * Returns permission info related to an idea for the current
 * logged in member.
 */
@Injectable({
  providedIn: 'root'
})
export class IdeaPermissionInfoService {
  constructor(private memberInfo: MemberInfoService) {}

  hasPermission(
    member: IMember,
    requiresFullSubscription: boolean,
    isFree: boolean = false
  ): boolean {
    return this.memberInfo.withMember<boolean>(member, infoService => {
      if (isFree) {
        return true;
      }

      if (infoService.isCms()) {
        return true;
      }

      if (infoService.isAnonymous()) {
        return false;
      }

      if (requiresFullSubscription && !infoService.hasAccessToPoolIdeas()) {
        return false;
      }

      if (!requiresFullSubscription && !infoService.hasAccessToNewIdeas()) {
        return false;
      }

      return true;
    });
  }
}
