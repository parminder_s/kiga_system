export interface ResolveIdeaResponse {
  activeCategory: object;
  breadCrumb: any[];
  categories: any[];
  className: string;
  firstLevelCategoryName: string;
  id: number;
  metadata: object;
  parentCategories: object[];
  permission: object;
  resolvedType: string;
  showBreadCrumbs: boolean;
  title: string;
  urlSegment: string;
}
