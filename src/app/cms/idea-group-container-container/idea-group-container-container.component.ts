import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { selectIdeaViewModel } from '../reducers/idea.selectors';
import { selectBreakpointName } from '../../+state/media-query.reducer';

@Component({
  selector: 'kiga-idea-group-container-container',
  templateUrl: 'idea-group-container-container.component.html'
})
export class IdeaGroupContainerContainerComponent implements OnInit {
  public viewModel$: Observable<any>;
  public showBreadcrumbs$: Observable<boolean>;

  constructor(private store: Store<any>) {}

  ngOnInit() {
    this.viewModel$ = this.store.pipe(select(selectIdeaViewModel));
    this.showBreadcrumbs$ = this.store.pipe(
      select(selectBreakpointName),
      map(breadcrumb => ['sm', 'lsm', 'md'].indexOf(breadcrumb) === -1)
    );
  }
}
