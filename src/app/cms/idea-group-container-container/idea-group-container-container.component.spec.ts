import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaGroupContainerContainerComponent } from './idea-group-container-container.component';

describe('IdeaGroupContainerContainerComponent', () => {
  let component: IdeaGroupContainerContainerComponent;
  let fixture: ComponentFixture<IdeaGroupContainerContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdeaGroupContainerContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaGroupContainerContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
