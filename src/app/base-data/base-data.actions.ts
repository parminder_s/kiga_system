import { Action } from '@ngrx/store';
import { BaseDataState } from './base-data.state';

/**
 * We have only two actions. When the website is bootstrapped or the user
 * changes the locale. In both cases, CHANGE_LOCALE is triggered which
 * is handled by the bootstrap.effects.
 */
export type BaseDataActionsUnion = BaseDataLoad | BaseDataLoaded;

export const BASE_DATA_LOADED = '[BASE_DATA] loaded';

export class BaseDataLoaded implements Action {
  readonly type = BASE_DATA_LOADED;

  constructor(public payload: BaseDataState) {}
}

export const BASE_DATA_LOAD = '[BASE_DATA] load';

export class BaseDataLoad implements Action {
  readonly type = BASE_DATA_LOAD;

  constructor(public locale: string) {}
}
