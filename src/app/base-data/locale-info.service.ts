import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { localeSelector } from './base-data.selectors';
import { ILocaleInfoService } from '../../../app/src/misc/js/services/ILocaleInfoService';

@Injectable({
  providedIn: 'root'
})
export class LocaleInfoService implements ILocaleInfoService {
  private locale: string;

  constructor(private store: Store<any>) {
    this.store.select(localeSelector).subscribe(locale => {
      this.locale = locale;
    });
  }

  getLocale(): string {
    return this.locale;
  }
}
