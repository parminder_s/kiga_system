import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { reduce } from './base-data.reducers';

/**
 * This module contains the basic data which is required by all modules.
 *
 * It contains the i18n texsts (locales), configuration parameters,
 * major links, and the user's country which is fetched from the IP address.
 *
 * We have only two actions. When the website is bootstrapped or the user
 * changes the locale. In both cases we BASEDATA_LOAD is triggered which
 * is handled by the bootstrap.effects.
 *
 * The effects themselves are defined in the main module. The reason is, that
 * we want to keep this module very thin, e.g. having as few dependencies as
 * possible. So it only defines the state, the reducers and actions. It doesn't
 * care how the data comes into the application.
 */

@NgModule({
  imports: [CommonModule, StoreModule.forFeature('baseData', reduce)],
  declarations: []
})
export class BaseDataModule {}
