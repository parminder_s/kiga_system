import { BaseDataActionsUnion, BaseDataLoaded, BASE_DATA_LOADED } from './base-data.actions';
import { BaseDataState } from './base-data.state';

const emptyLink = { url: '', inNavbar: false, code: '', label: '' };

const initialState: BaseDataState = {
  config: {
    angularCmsEnabled: false,
    kgaEnabled: false,
    changeCountryViaCookie: false,
    locales: [],
    permissionMode: '',
    showQuickRegisters: false
  },
  countries: [],
  currentCountry: '',
  locale: '',
  locales: [],
  links: {
    all: [],
    footerPages: [],
    forum: '',
    ideaGroupMainContainer: '',
    navbar: [],
    mainArticleCategory: emptyLink,
    mainArticleSearch: emptyLink,
    shop: '',
    userMenu: emptyLink
  },
  meta: { title: '', description: '', keywords: '' }
};

export function reduce(
  state: BaseDataState = initialState,
  action: BaseDataActionsUnion
): BaseDataState {
  switch (action.type) {
    case BASE_DATA_LOADED:
      return (<BaseDataLoaded>action).payload;
    default:
      return state;
  }
}
