import { MetaDefinition } from '@angular/platform-browser';
import { Link } from '../../../app/src/misc/js/services/AngularInterfaces';
import { Locales } from './model';

export interface Config {
  angularCmsEnabled: boolean;
  kgaEnabled: boolean;
  changeCountryViaCookie: boolean;
  locales: Array<string>;
  permissionMode: string;
  showQuickRegisters: boolean;
}

export interface Meta extends MetaDefinition {
  title: string;
  description: string;
  keywords: string;
}

export interface LinksState {
  readonly all: Array<Link>;
  readonly footerPages: Array<Link>;
  readonly forum: string;
  readonly ideaGroupMainContainer: string;
  readonly mainArticleCategory: Link;
  readonly mainArticleSearch: Link;
  readonly navbar: Array<Link>;
  readonly shop: string;
  readonly userMenu: Link;
}

export interface BaseDataState {
  readonly config: Config;
  readonly countries: Array<string>;
  readonly currentCountry: string;
  readonly locale: string;
  readonly locales: Locales[];
  readonly links: LinksState;
  readonly meta: Meta;
}
