import { createFeatureSelector, createSelector, State } from '@ngrx/store';
import { BaseDataState } from './base-data.state';

export const getBaseDataState = createFeatureSelector<BaseDataState>('baseData');

export const localeSelector = createSelector(getBaseDataState, state => state.locale);
export const linksSelector = createSelector(getBaseDataState, state => state.links);
export const configSelector = createSelector(getBaseDataState, state => state.config);
