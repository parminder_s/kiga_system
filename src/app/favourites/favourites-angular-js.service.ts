import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { IFavouritesService } from '../../../app/src/misc/js/services/AngularInterfaces';
import { ToggleFavourite } from './+state/favourites.actions';
import { getFavourites } from './+state/favourites.selectors';

@Injectable({
  providedIn: 'root'
})
export class FavouritesAngularJsService implements IFavouritesService {
  private cb: () => void;

  constructor(private store: Store<any>) {
    this.store.pipe(select(getFavourites)).subscribe(() => {
      if (this.cb) {
        this.cb();
      }
    });
  }

  toggle(ideaId: number) {
    this.store.dispatch(new ToggleFavourite(ideaId));
  }

  notifyForUpdate(cb: () => void) {
    this.cb = cb;
  }
}
