import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { favouritesReducer } from './+state/favourites.reducers';
import { EffectsModule } from '@ngrx/effects';
import { FavouritesEffects } from './+state/favourites.effects';
import { StoreModule, Store } from '@ngrx/store';
import { MemberLoaderService } from '../main/member-loader.service';
import { ClearFavourites, LoadFavourites } from './+state/favourites.actions';
import { downgradeInjectable } from '@angular/upgrade/static';
import { FavouritesAngularJsService } from './favourites-angular-js.service';

try {
  angular
    .module('misc')
    .factory('favouritesService', downgradeInjectable(FavouritesAngularJsService) as any);
} catch (e) {}

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('favourites', favouritesReducer),
    EffectsModule.forFeature([FavouritesEffects])
  ],
  declarations: []
})
export class FavouritesModule {
  constructor(memberLoader: MemberLoaderService, store: Store<any>) {
    memberLoader.get().subscribe(member => {
      if (member.isAnonymous) {
        store.dispatch(new ClearFavourites());
      } else {
        store.dispatch(new LoadFavourites());
      }
    });
  }
}
