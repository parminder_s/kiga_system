import { FavouriteIdea } from './favourites.reducers';
import { Action } from '@ngrx/store';

export enum FavouriteActionTypes {
  ClearFavourites = '[Favourites] Clear',
  LoadFavourites = '[Favourites] Load',
  FavouritesLoaded = '[Favourites] Loaded',
  ToggleFavourite = '[Favourites] Toggle',
  FavouriteToggled = '[Favourites] Toggled'
}

export class LoadFavourites implements Action {
  readonly type = FavouriteActionTypes.LoadFavourites;

  constructor() {}
}

export class FavouritesLoaded implements Action {
  readonly type = FavouriteActionTypes.FavouritesLoaded;

  constructor(public favourites: Array<FavouriteIdea>) {}
}

export class ClearFavourites implements Action {
  readonly type = FavouriteActionTypes.ClearFavourites;

  constructor() {}
}

export class ToggleFavourite implements Action {
  readonly type = FavouriteActionTypes.ToggleFavourite;

  constructor(public ideaId: number) {}
}

export class FavouriteToggled implements Action {
  readonly type = FavouriteActionTypes.FavouriteToggled;

  constructor(public ideaId: number) {}
}

export type FavouritesActions =
  | ClearFavourites
  | LoadFavourites
  | FavouritesLoaded
  | ToggleFavourite
  | FavouriteToggled;
