import { FavouritesActions, FavouriteActionTypes } from './favourites.actions';

export interface FavouriteIdea {
  id: number;
  ideaId: number;
}

export interface FavouritesState {
  favouriteIdeas: Array<FavouriteIdea>;
}

const initialState: FavouritesState = { favouriteIdeas: [] };

export function favouritesReducer(state = initialState, action: FavouritesActions) {
  switch (action.type) {
    case FavouriteActionTypes.FavouritesLoaded:
      return { ...state, favouriteIdeas: action.favourites };
    case FavouriteActionTypes.ClearFavourites:
      return { ...state, favouriteIdeas: [] };
    case FavouriteActionTypes.FavouriteToggled:
      const favouritesWithoutIdea = state.favouriteIdeas.filter(
        favouriteIdea => favouriteIdea.ideaId !== action.ideaId
      );
      if (favouritesWithoutIdea.length === state.favouriteIdeas.length) {
        return {
          ...state,
          favouriteIdeas: [...state.favouriteIdeas, { id: 1, ideaId: action.ideaId }]
        };
      } else {
        return { ...state, favouriteIdeas: favouritesWithoutIdea };
      }
    default:
      return state;
  }
}
