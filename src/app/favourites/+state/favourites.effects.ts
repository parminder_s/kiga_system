import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { EndpointService } from '../../endpoint/endpoint.service';
import {
  FavouriteActionTypes,
  FavouritesLoaded,
  FavouriteToggled,
  LoadFavourites,
  ToggleFavourite
} from './favourites.actions';
import { Action } from '@ngrx/store';
import { of } from 'rxjs';

@Injectable()
export class FavouritesEffects {
  @Effect()
  filterAgeGroup$ = this.actions$.pipe(
    ofType<LoadFavourites>(FavouriteActionTypes.LoadFavourites),
    mergeMap(() => this.endpoint.get<any>('favourites')),
    map(favouriteIdeas => new FavouritesLoaded(favouriteIdeas.favourites))
  );

  @Effect()
  toggleFavourite$ = this.actions$.pipe(
    ofType<ToggleFavourite>(FavouriteActionTypes.ToggleFavourite),
    mergeMap(({ ideaId }) =>
      this.endpoint.post('/favourites/toggle', { ideaId }).pipe(
        map(() => new FavouriteToggled(ideaId)),
        catchError(() => of({ type: 'ErrorAction' }))
      )
    )
  );

  constructor(private actions$: Actions, private endpoint: EndpointService) {}
}
