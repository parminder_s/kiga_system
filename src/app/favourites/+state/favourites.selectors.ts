import { FavouritesState } from './favourites.reducers';
import { createSelector } from '@ngrx/store';

export const getFavouritesState: (state: any) => FavouritesState = state => state.favourites;

export const getFavourites = createSelector(
  getFavouritesState,
  favouritesState => favouritesState.favouriteIdeas
);

export const getFavouritesDict = createSelector(
  getFavourites,
  favourites => {
    const returner: { [key: number]: number } = {};
    favourites.forEach(({ id, ideaId }) => (returner[ideaId] = id));
    return returner;
  }
);
