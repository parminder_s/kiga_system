import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { RemoveMultimedia, UpdateMultimedia } from '../+state/multimedia.actions';
import { MultimediaStatus } from '../+state/multimedia.reducer';
import { selectMultimedia } from '../+state/multimedia.selectors';

declare var require: any;

@Component({
  selector: 'kiga-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.less']
})
export class VideoPlayerComponent implements OnInit, OnDestroy {
  @Input() videoLink = '';
  @Input() previewLink = '';
  randomId = VideoPlayerComponent.generateRandomId(10);

  private video;
  private subscriptions: Subscription[] = [];

  constructor(private store: Store<any>) {}

  static generateRandomId(charLength) {
    let unId = '';
    const alfabet = 'abcdefghigklmnopqrstuvqxyzABCDEFGHIGKLMNPQRSTUVWZYZ';
    function random() {
      let max, min, rand;
      min = 0;
      max = alfabet.length - 1;
      rand = min + Math.random() * (max - min);
      return Math.round(rand);
    }
    for (let i = 0; i < charLength; i++) {
      unId += alfabet[random()];
    }
    return unId;
  }

  ngOnInit() {
    if (typeof window['jwplayer'] === 'function' && window['jwplayer'](this.randomId)['key']) {
      this.initPlayer();
    } else {
      require([
        '../../../../app/src/rawJs/jwplayer.js',
        '../../../../app/src/rawJs/jwplayer.html5.js'
      ], () => {
        window['jwplayer']['key'] = '29ODX2dQRS4ENOY6iJXeJxtKKtu8LZVSL8XxEg==';
        this.initPlayer();
      });
    }
    this.subscriptions.push(
      this.store.pipe(select(selectMultimedia)).subscribe(state => {
        if (this.video) {
          const videoState = this.video.getState();
          if (
            state.status === MultimediaStatus.Play &&
            videoState !== 'PLAYING' &&
            state.url === this.videoLink
          ) {
            this.video.play();
          } else if (state.status === MultimediaStatus.Pause && videoState === 'PLAYING') {
            this.video.pause();
          }
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscrion => subscrion.unsubscribe());
    this.store.dispatch(new RemoveMultimedia(this.videoLink));
  }

  initPlayer() {
    const self = this;
    window['jwplayer'](this.randomId).setup({
      file: this.videoLink,
      image: this.previewLink,
      width: '100%',
      aspectratio: '16:9'
    });
    this.video = window['jwplayer'](this.randomId);
    this.video.onPlay(() => {
      this.store.dispatch(new UpdateMultimedia(self.videoLink, MultimediaStatus.Play));
    });
    /*    this.video.onBuffer(() => {
      console.log('onBuffer');
      this.store.dispatch(new UpdateMultimedia(this.videoLink, MultimediaStatus.Play));
    });*/
    this.video.onPause(() => {
      this.store.dispatch(new UpdateMultimedia(this.videoLink, MultimediaStatus.Pause));
    });
    this.video.onComplete(() => {
      this.store.dispatch(new UpdateMultimedia(this.videoLink, MultimediaStatus.Pause));
    });
  }
}
