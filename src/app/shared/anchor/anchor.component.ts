import { Component, Input, OnInit } from '@angular/core';

/**
 * A component that can use useHref to toggle href and routerLink
 */
@Component({
  selector: 'kiga-anchor',
  templateUrl: './anchor.component.html',
  styleUrls: ['./anchor.component.less']
})
export class AnchorComponent implements OnInit {
  @Input() useHref: boolean;
  @Input() link: string;
  constructor() {}

  ngOnInit() {}
}
