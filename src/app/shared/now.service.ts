import { Injectable } from '@angular/core';
import * as moment from 'moment';

/**
 * This service is required for mocking the current date.
 */
@Injectable({
  providedIn: 'root'
})
export class NowService {
  constructor() {}

  date() {
    return new Date();
  }

  moment() {
    return moment();
  }
}
