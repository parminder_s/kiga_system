import { Directive, HostListener, Input } from '@angular/core';
import { EndpointService } from '../endpoint/endpoint.service';
import { KigaLinkOption } from './kiga-link-option';

@Directive({
  selector: '[kigaLink]'
})
export class LinkDirective {
  @Input('kigaLink') kigaLink: KigaLinkOption;
  constructor(private endpoint: EndpointService) {}

  @HostListener('click')
  async onClick() {
    if (this.kigaLink) {
      let url = 'log/logLink';
      if (this.kigaLink.tag === 'print') {
        url = 'log/logPrint';
      }
      await this.endpoint.post(url, this.kigaLink).toPromise();
    }
  }
}
