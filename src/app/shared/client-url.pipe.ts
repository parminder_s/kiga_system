import { Pipe, PipeTransform } from '@angular/core';
import { KigaInitData } from '../data/KigaInitData';

@Pipe({
  name: 'clientUrl'
})
export class ClientUrlPipe implements PipeTransform {
  constructor(private kigaInitData: KigaInitData) {}

  transform(value: any, args?: any): any {
    return this.kigaInitData.clientUrl + value;
  }
}
