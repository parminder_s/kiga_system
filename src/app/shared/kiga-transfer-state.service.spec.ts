import { inject, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs/internal/Observable';

import { KigaTransferStateService, TransferStateType } from './kiga-transfer-state.service';

describe('KigaTransferStateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KigaTransferStateService]
    });
  });

  it(
    'should be created',
    inject([KigaTransferStateService], (service: KigaTransferStateService) => {
      expect(service).toBeTruthy();
    })
  );

  it(
    'should insert a new observable for non existing locale',
    inject([KigaTransferStateService], (service: KigaTransferStateService) => {
      const observable = new Observable();
      const fetcherFn = () => observable;

      service.getOrFetch(TransferStateType.BOOTSTRAP, 'en', fetcherFn);

      expect(service.getOrFetch(TransferStateType.BOOTSTRAP, 'en', null)).toBe(observable);
    })
  );
});
