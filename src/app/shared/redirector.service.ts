import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RedirectorService {
  constructor() {}

  public redirectTo(path: string, params: {}) {}
}
