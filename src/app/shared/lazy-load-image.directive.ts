import { AfterContentInit, Directive, ElementRef, Input, OnDestroy } from '@angular/core';
import { PlatformService } from '../misc/services/platform.service';

export interface LazyloadImageDirectiveOptions {
  src: string;
  placeholder: string;
  intersectionObserverInit?: IntersectionObserverInit;
}
@Directive({
  selector: '[kigaLazyLoadImage]'
})
export class LazyLoadImageDirective implements AfterContentInit, OnDestroy {
  @Input() kigaLazyLoadImage: LazyloadImageDirectiveOptions;
  private observer: IntersectionObserver;
  private isSupported = false;

  constructor(private el: ElementRef, private platform: PlatformService) {
    if (platform.isBrowser()) {
      this.isSupported = 'IntersectionObserver' in window;
    }
  }

  ngAfterContentInit() {
    if (this.isSupported) {
      this.observer = new IntersectionObserver(
        this.callback.bind(this),
        this.kigaLazyLoadImage.intersectionObserverInit
      );
      this.observer.observe(this.el.nativeElement);
      if (this.kigaLazyLoadImage.placeholder) {
        this.el.nativeElement.setAttribute('src', this.kigaLazyLoadImage.placeholder);
      }
    } else {
      this.el.nativeElement.setAttribute('src', this.kigaLazyLoadImage.src);
    }
  }

  ngOnDestroy() {
    if (this.isSupported) {
      this.observer.disconnect();
    }
  }

  callback(entries: IntersectionObserverEntry[], observer: IntersectionObserver): void {
    entries.forEach(entry => {
      if (entry.isIntersecting) {
        // console.log(this.el);
        observer.unobserve(entry.target);
        entry.target.setAttribute('src', this.kigaLazyLoadImage.src);
      }
    });
  }
}
