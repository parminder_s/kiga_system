import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrollPaneContainer } from './scroll-pane-container.component';

describe('ScrollPaneContainer', () => {
  let component: ScrollPaneContainer;
  let fixture: ComponentFixture<ScrollPaneContainer>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ScrollPaneContainer]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrollPaneContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
