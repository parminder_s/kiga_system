import { Component, Input, OnInit } from '@angular/core';
import IMember from '../../../../app/src/misc/permission/js/IMember';
import { ScrollPaneItem } from '../scroll-pane/scroll-pane.component';
import { KigaInitData } from '../../data/KigaInitData';
import { MemberInfoService } from '../../main/member-info.service';
import { IdeasViewModelIdea } from '@kiga/home/api';

@Component({
  selector: 'kiga-scroll-pane-container',
  templateUrl: './scroll-pane-container.component.html',
  styleUrls: ['./scroll-pane-container.component.less']
})
export class ScrollPaneContainer implements OnInit {
  @Input() newIdeas: Array<IdeasViewModelIdea>;
  @Input() member: IMember;
  @Input() url: string;
  @Input() locale: string;

  scrollPaneItems: Array<ScrollPaneItem> = [];

  constructor(private kigaInitData: KigaInitData, private memberInfo: MemberInfoService) {}

  ngOnInit() {}

  ngOnChanges() {
    this.setScrollPaneItems(this.newIdeas, this.url, this.member, this.locale);
  }

  public setScrollPaneItems(
    newIdeas: Array<IdeasViewModelIdea>,
    url: string,
    member: IMember,
    locale: string
  ) {
    this.scrollPaneItems = [];

    this.memberInfo.withMember(member, memberInfo => {
      if (!memberInfo.hasFullSubscription() && !memberInfo.hasTestSubscription()) {
        this.scrollPaneItems.push({
          title: 'HOME_QUICK_REGISTRATION_TITLE',
          description: 'HOME_QUICK_REGISTRATION_LINK',
          previewUrl: `${this.kigaInitData.clientUrl}/assets/placeholder-home-new-idea.png`,
          url: `/${this.locale}/permission/test-required`,
          queryParams: {},
          className: 'registration',
          useTranslation: true,
          ifRegistrationItem: 'quick'
        });
      }

      if (memberInfo.hasTestSubscription()) {
        this.scrollPaneItems.push({
          title: 'HOME_FULL_REGISTRATION_TITLE',
          description: 'HOME_FULL_REGISTRATION_LINK',
          previewUrl: `${this.kigaInitData.clientUrl}/assets/placeholder-home-new-idea.png`,
          url: `/${locale}/registrierung/preSelection`,
          queryParams: {},
          className: 'registration',
          useTranslation: true,
          ifRegistrationItem: 'full'
        });
      }
    });

    this.scrollPaneItems.push(
      ...newIdeas.map(({ title, description, previewUrl, urlSegment }) => {
        const queryParams = { preview: urlSegment };
        const className = '';
        const useTranslation = false;
        const ifRegistrationItem = '';
        return {
          title,
          description,
          previewUrl,
          url,
          queryParams,
          className,
          useTranslation,
          ifRegistrationItem
        };
      })
    );
  }
}
