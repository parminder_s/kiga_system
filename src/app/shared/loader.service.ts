import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Loader } from '../../../app/src/misc/js/services/Loader';

@Injectable({
  providedIn: 'root'
})
export class LoaderService implements Loader {
  private loader: HTMLElement;

  constructor(@Inject(DOCUMENT) private document: Document) {}

  show() {
    this.getLoader().style.display = 'block';
  }

  hide() {
    this.getLoader().style.display = 'none';
  }

  private getLoader() {
    if (!this.loader) {
      this.loader = this.document.getElementById('loader');
    }

    return this.loader;
  }
}
