import { Action } from '@ngrx/store';
import { MultimediaStatus } from './multimedia.reducer';

export enum MultimediaActionTypes {
  UpdateMultimedia = '[Multimedia] Update Multimedia',
  RemoveMultimedia = '[Multimedia] Remove Multimedia'
}

export class UpdateMultimedia implements Action {
  readonly type = MultimediaActionTypes.UpdateMultimedia;
  constructor(public url: string, public status: MultimediaStatus) {}
}

export class RemoveMultimedia implements Action {
  readonly type = MultimediaActionTypes.RemoveMultimedia;
  constructor(public url: string) {}
}

export type MultimediaActions = UpdateMultimedia | RemoveMultimedia;
