import { MultimediaActions, MultimediaActionTypes } from './multimedia.actions';

export enum MultimediaStatus {
  Play = 'play',
  Pause = 'pause'
}

export interface State {
  url: string;
  status: MultimediaStatus;
}

export const initialState: State = {
  url: '',
  status: MultimediaStatus.Pause
};

export function reducer(state = initialState, action: MultimediaActions): State {
  switch (action.type) {
    case MultimediaActionTypes.UpdateMultimedia: {
      return {
        ...state,
        url: action.url,
        status: action.status
      };
    }
    case MultimediaActionTypes.RemoveMultimedia: {
      if (state.url === action.url) {
        return {
          ...state,
          url: '',
          status: MultimediaStatus.Pause
        };
      } else {
        return state;
      }
    }
    default:
      return state;
  }
}
