import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lineClamp'
})
export class LineClampPipe implements PipeTransform {
  transform(input: string, length?: number): any {
    if (!input) {
      return '';
    } else if (!length || input.length <= length) {
      return input;
    } else {
      return input.substr(0, length) + '...';
    }
  }
}
