import { Location } from '@angular/common';
import { Injectable } from '@angular/core';

/**
 * A helper class to store the current locale. This is a candidate for a
 * replace if we would have state management library like ngrx.
 *
 * We can't use the existing locale from AngularJs since that also
 * set the formatting rules for date and number and for SSR
 * we have to be AngularJS independent.
 *
 * But vice-versa this service is downgraded, so when a locale change occurs,
 * this is also stored here.
 */
@Injectable({
  providedIn: 'root'
})
export class LocationForLocaleParser {
  private defaultLocale = 'en';

  constructor(private location: Location) {}

  parseLocale(url): string {
    const matchesNg1HashBang: Array<string> = url.match(/^\/?\#\!\/(\w{2})\//);
    const matchesNgx = url.match(/^\/?ng6\/(\w{2})\//);
    const matchesNg = url.match(/^\/?(\w{2})\//);

    if (matchesNg1HashBang) {
      return matchesNg1HashBang[1];
    } else if (matchesNgx) {
      return matchesNgx[1];
    } else if (matchesNg) {
      return matchesNg[1];
    } else {
      return this.defaultLocale;
    }
  }
}
