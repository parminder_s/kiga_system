import { Component, Input, OnInit } from '@angular/core';
import { ScrollPaneItem } from '../scroll-pane/scroll-pane.component';

@Component({
  selector: 'kiga-new-ideas-mobile-view',
  templateUrl: './new-ideas-mobile-view.component.html',
  styleUrls: ['./new-ideas-mobile-view.component.less']
})
export class NewIdeasMobileView implements OnInit {
  @Input() items: Array<ScrollPaneItem>;

  constructor() {}

  ngOnInit() {}
}
