import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewIdeasMobileView } from './new-ideas-mobile-view.component';

describe('NewIdeasMobileView', () => {
  let component: NewIdeasMobileView;
  let fixture: ComponentFixture<NewIdeasMobileView>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [NewIdeasMobileView]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(NewIdeasMobileView);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
