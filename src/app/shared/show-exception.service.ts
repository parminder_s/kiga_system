import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShowExceptionService {
  constructor() {}

  show(code: string = '') {}
}
