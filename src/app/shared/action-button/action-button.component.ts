import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'kiga-action-button',
  templateUrl: './action-button.component.html',
  styleUrls: ['./action-button.component.less']
})
export class ActionButtonComponent implements OnInit {
  @Input() link: string;
  @Input() useHref: boolean;
  @Input() dataTest: string;
  @Input() icon: string;
  @Input() tooltip: string;
  @Input() colorName: string;

  constructor() {}

  ngOnInit() {}
}
