export interface KigaLinkOption {
  tag: string;
  url: string;
  articleId: string;
  title: string;
  origin: string;
}
