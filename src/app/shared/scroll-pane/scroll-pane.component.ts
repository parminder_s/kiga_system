import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { KigaInitData } from '../../data/KigaInitData';
import { LinksService } from '../../compat/links.service';

export interface ScrollPaneItem {
  title: string;
  description: string;
  previewUrl: string;
  url: string;
  queryParams: { [key: string]: string };
  className: string;
  useTranslation: boolean;
  ifRegistrationItem: string;
}

@Component({
  selector: 'kiga-scroll-pane',
  templateUrl: './scroll-pane.component.html',
  styleUrls: ['./scroll-pane.component.less']
})
export class ScrollPaneComponent {
  @Input() items: Array<ScrollPaneItem>;

  placeholder: string;
  public breakpoints = [
    {
      name: 'xlg',
      itemsPerRow: 7
    },
    {
      name: 'lg',
      itemsPerRow: 6
    },
    {
      name: 'lmd',
      itemsPerRow: 5
    },
    {
      name: 'md',
      itemsPerRow: 4
    }
  ];
  public currentIndex = 0;

  constructor(
    private linksService: LinksService,
    private router: Router,
    kigaInitData: KigaInitData
  ) {
    this.placeholder = kigaInitData.clientUrl + '/assets/placeholder-home-new-idea.png';
  }

  left(itemsPerRow: number, event: MouseEvent) {
    event.preventDefault();
    this.currentIndex = Math.max(0, this.currentIndex - itemsPerRow);
  }

  leftButtonHidden(itemsPerRow) {
    return this.currentIndex === 0;
  }

  right(itemsPerRow: number, event: MouseEvent) {
    event.preventDefault();
    this.currentIndex = this.currentIndex + itemsPerRow;
  }

  rightButtonHidden(itemsPerRow) {
    return this.currentIndex + itemsPerRow >= this.items.length;
  }

  transform(breakpoint) {
    return `translateX(-${this.currentIndex / breakpoint.itemsPerRow * 100}%)`;
  }

  itemInvisible(itemsPerRow: number, itemIndex: number): boolean {
    return itemIndex < this.currentIndex || itemIndex >= this.currentIndex + itemsPerRow;
  }

  getSrc(i: number, itemsPerRow: number) {
    const visible = !this.itemInvisible(itemsPerRow, i);
    return visible ? this.items[i].previewUrl : this.placeholder;
  }
}
