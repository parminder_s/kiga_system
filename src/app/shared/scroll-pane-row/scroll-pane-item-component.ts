import { EventEmitter } from '@angular/core';

export interface ScrollPaneItemComponent<T> {
  data: T;
  index?: number;
  onClick?: EventEmitter<{ data: T; index?: number }>;
}
