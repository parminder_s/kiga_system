import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { LinksService } from '../../compat/links.service';
import { KigaInitData } from '../../data/KigaInitData';
import { ScrollPaneItemComponent } from './scroll-pane-item-component';

@Component({
  selector: 'kiga-scroll-pane-row',
  templateUrl: './scroll-pane-row.component.html',
  styleUrls: ['./scroll-pane-row.component.less']
})
export class ScrollPaneRowComponent implements OnInit {
  @Input() items: any[];
  @Input() breakpoint;
  @Input() component: ScrollPaneItemComponent<any>;
  @Input() buttonStyle: object = {};
  @Input() noGap = false;
  @Output() onItemClick = new EventEmitter();

  currentIndex = 0;
  placeholder: string;
  constructor(
    private linksService: LinksService,
    private router: Router,
    kigaInitData: KigaInitData
  ) {
    this.placeholder = kigaInitData.clientUrl + '/assets/placeholder-home-new-idea.png';
  }

  ngOnInit() {}

  left(itemsPerRow: number, event: MouseEvent) {
    event.preventDefault();
    if (this.leftButtonHidden()) {
      return;
    }
    this.currentIndex = Math.max(0, this.currentIndex - itemsPerRow);
  }

  leftButtonHidden() {
    return this.currentIndex === 0;
  }

  right(itemsPerRow: number, event: MouseEvent) {
    event.preventDefault();
    if (this.rightButtonHidden(itemsPerRow)) {
      return;
    }
    this.currentIndex = this.currentIndex + itemsPerRow;
  }

  rightButtonHidden(itemsPerRow) {
    return this.currentIndex + itemsPerRow >= this.items.length;
  }

  transform(breakpoint) {
    return `translateX(-${(this.currentIndex / breakpoint.itemsPerRow) * 100}%)`;
  }

  itemInvisible(itemsPerRow: number, itemIndex: number): boolean {
    return itemIndex < this.currentIndex || itemIndex >= this.currentIndex + itemsPerRow;
  }

  getSrc(i: number, itemsPerRow: number) {
    const visible = !this.itemInvisible(itemsPerRow, i);
    return visible ? this.items[i].previewUrl : this.placeholder;
  }

  scrollIntoView(index: number) {
    this.currentIndex = index - (index % this.breakpoint.itemsPerRow);
  }
}
