import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrollPaneRowComponent } from './scroll-pane-row.component';

describe('ScrollPaneRowComponent', () => {
  let component: ScrollPaneRowComponent;
  let fixture: ComponentFixture<ScrollPaneRowComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ScrollPaneRowComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrollPaneRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
