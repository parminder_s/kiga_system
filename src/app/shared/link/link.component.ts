import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'kiga-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.less']
})
export class LinkComponent implements OnInit {
  @Input() href: string;
  @Input() translate: string;
  @Input() ngClass;
  constructor() {}

  ngOnInit() {}

  useRouterLink(): boolean {
    return !this.href.startsWith('/#');
  }
}
