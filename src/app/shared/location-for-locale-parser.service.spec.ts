import { Location } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { LocaleInfoService } from '../base-data/locale-info.service';

describe('LocaleInfoService', () => {
  const getServiceWithUrl: (url: string) => LocaleInfoService = (url: string) =>
    TestBed.configureTestingModule({
      providers: [LocaleInfoService, { provide: Location, useValue: { path: () => url } }]
    }).get(LocaleInfoService);

  it('should parse an ng1 url', () => expect(getServiceWithUrl('/fr/home').getLocale()).toBe('fr'));

  it('should parse an hashbang ng1 url', () =>
    expect(getServiceWithUrl('/#!/fr/home').getLocale()).toBe('fr'));

  it('should parse an hashbang ng1 url without slash', () =>
    expect(getServiceWithUrl('#!/fr/home').getLocale()).toBe('fr'));

  it('should parse an ngx url', () =>
    expect(getServiceWithUrl('ng6/at/home').getLocale()).toBe('at'));

  it('should fallback to default on empty', () =>
    expect(getServiceWithUrl('').getLocale()).toBe('en'));

  it('should support an optional slash', () =>
    expect(getServiceWithUrl('/ng6/aut/home').getLocale()).toBe('en'));

  it('should fallback to default on incomplete url', () =>
    expect(getServiceWithUrl('/ng5/de').getLocale()).toBe('en'));
});
