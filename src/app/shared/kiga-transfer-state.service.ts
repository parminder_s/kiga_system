import { Injectable } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { LocaleInfoService } from '../base-data/locale-info.service';
import { PlatformService } from '../misc/services/platform.service';

export enum TransferStateType {
  BOOTSTRAP = 'BOOTSTRAP',
  HOME = 'HOME'
}

/**
 * This is a typed wrapper for Angular's TransferService. Currently we only have
 * the bootstrap data in it and therefore only one provider.
 */
@Injectable({
  providedIn: 'root'
})
export class KigaTransferStateService {
  constructor(
    private transferState: TransferState,
    private platform: PlatformService,
    private localeInfo: LocaleInfoService
  ) {}

  getOrFetch<T>(
    transferStateType: TransferStateType,
    locale: string,
    fetcherFn: () => Observable<T>
  ): Observable<T> {
    return new Observable<T>(observer => {
      const key = makeStateKey(locale + '-' + transferStateType);
      const dataTransferState = this.transferState.get(key, null);
      if (dataTransferState) {
        observer.next(dataTransferState);
        observer.complete();
      } else {
        fetcherFn().subscribe(result => {
          if (this.platform.isServer()) {
            this.transferState.set(key, result);
          }
          observer.next(result);
          observer.complete();
        });
      }
    });
  }
}
