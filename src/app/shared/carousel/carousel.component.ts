import { Component, Input, OnChanges, OnDestroy, OnInit, ViewChildren } from '@angular/core';
import { UrlToRouteMapperService } from '../../main/url-to-route-mapper.service';
import { PlatformService } from '../../misc/services/platform.service';

export interface CarouselElement {
  alternateText: string;
  linkUrl: string;
  smallImageUrl: string;
  bigImageUrl: string;
}

interface FullCarouselElement extends CarouselElement {
  routerLink: string;
  queryParams: { [key: string]: string };
}

@Component({
  selector: 'kiga-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.less']
})
export class CarouselComponent implements OnInit, OnDestroy, OnChanges {
  @Input() elements: Array<CarouselElement>;
  public fullElements: Array<FullCarouselElement>;

  @ViewChildren('carouselItem') items;
  currentIndex = 0;
  duration = 600; // animation duration in css in ms
  interval = 5000; // default bootstrap carousel interval
  timer;

  constructor(
    private urlToRouteMapper: UrlToRouteMapperService,
    private platform: PlatformService
  ) {}

  ngOnChanges() {
    this.fullElements = this.elements.map(element =>
      Object.assign({}, element, this.urlToRouteMapper.getRouterPart(element.linkUrl))
    );
  }

  ngOnInit() {
    if (this.platform.isBrowser()) {
      const self = this;
      const addActive = setInterval(() => {
        if (self.items && self.items.toArray().length) {
          clearInterval(addActive);
          self.items.toArray()[self.currentIndex].nativeElement.classList.add('active');
        }
      }, 0);
      this.startTimer();
    }
  }

  ngOnDestroy() {
    this.pauseTimer();
  }

  showItem(i) {
    if (i < 0) {
      i += this.elements.length;
    }
    if (i >= this.elements.length) {
      i -= this.elements.length;
    }
    const itemsArray = this.items.toArray();
    const itemToHideClassList = itemsArray[this.currentIndex].nativeElement.classList;
    const itemToShowClassList = itemsArray[i].nativeElement.classList;
    itemToHideClassList.add('left');
    itemToShowClassList.add('next');
    // should have next class first, then left class
    setTimeout(() => itemToShowClassList.add('left'), 0);
    setTimeout(() => {
      itemToHideClassList.remove('active', 'left');
      itemToShowClassList.remove('next', 'left');
      itemToShowClassList.add('active');
    }, this.duration);
    this.currentIndex = i;
  }

  showPrev() {
    this.showItem(this.currentIndex + 1);
  }

  showNext() {
    this.showItem(this.currentIndex + 1);
  }

  startTimer() {
    this.timer = setInterval(() => this.showItem(this.currentIndex + 1), this.interval);
  }

  pauseTimer() {
    if (this.timer) {
      clearInterval(this.timer);
      this.timer = null;
    }
  }

  supportPicture(): boolean {
    if (this.platform.isBrowser()) {
      return !!(<any>window).HTMLPictureElement;
    } else {
      return true;
    }
  }
}
