import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { RemoveMultimedia, UpdateMultimedia } from '../+state/multimedia.actions';
import { MultimediaStatus } from '../+state/multimedia.reducer';
import { selectMultimedia } from '../+state/multimedia.selectors';
import { KigaInitData } from '../../data/KigaInitData';

@Component({
  selector: 'kiga-audio-player',
  templateUrl: './audio-player.component.html',
  styleUrls: ['./audio-player.component.less']
})
export class AudioPlayerComponent implements OnInit, OnDestroy {
  @Input() href: string;
  @Input() showOnlyButtons: boolean;
  @Input() label: string;

  playing = false;
  icon = 'icon-pause';
  endpointUrl: string;
  subscriptions: Subscription[] = [];
  @ViewChild('audio') audioPlayer: ElementRef;

  constructor(private store: Store<any>, private kigaInitData: KigaInitData) {
    this.endpointUrl = kigaInitData.endpointUrl;
  }

  ngOnInit() {
    this.subscriptions.push(
      this.store.pipe(select(selectMultimedia)).subscribe(audio => {
        this.playing = audio.url === this.href && audio.status === MultimediaStatus.Play;
        this.icon = this.playing ? 'icon-pause' : 'icon-play';
        if (this.audioPlayer) {
          const audioPlayerElement: HTMLAudioElement = this.audioPlayer.nativeElement;
          if (this.playing) {
            audioPlayerElement.play();
          } else {
            audioPlayerElement.pause();
          }
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
    this.store.dispatch(new RemoveMultimedia(this.href));
  }

  audioCtrl() {
    this.store.dispatch(
      new UpdateMultimedia(this.href, this.playing ? MultimediaStatus.Pause : MultimediaStatus.Play)
    );
  }
}
