import { TestBed, inject } from '@angular/core/testing';

import { GoBackService } from './go-back.service';

describe('GoBackService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GoBackService]
    });
  });

  it(
    'should be created',
    inject([GoBackService], (service: GoBackService) => {
      expect(service).toBeTruthy();
    })
  );
});
