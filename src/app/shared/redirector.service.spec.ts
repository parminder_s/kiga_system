import { TestBed, inject } from '@angular/core/testing';

import { RedirectorService } from './redirector.service';

describe('RedirectorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RedirectorService]
    });
  });

  it(
    'should be created',
    inject([RedirectorService], (service: RedirectorService) => {
      expect(service).toBeTruthy();
    })
  );
});
