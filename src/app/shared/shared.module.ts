import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { downgradeInjectable } from '@angular/upgrade/static';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { DynamicModule } from 'ng-dynamic-component';
import { LocaleInfoService } from '../base-data/locale-info.service';
import { reducer as multimediaReducer } from './+state/multimedia.reducer';
import { ActionButtonComponent } from './action-button/action-button.component';
import { AnchorComponent } from './anchor/anchor.component';
import { AudioPlayerComponent } from './audio-player/audio-player.component';
import { BackButtonComponent } from './back-button/back-button.component';
import { CarouselComponent } from './carousel/carousel.component';
import { ClientUrlPipe } from './client-url.pipe';
import { GoBackService } from './go-back.service';
import { LazyLoadImageDirective } from './lazy-load-image.directive';
import { LineClampPipe } from './line-clamp.pipe';
import { LinkDirective } from './link.directive';
import { LinkComponent } from './link/link.component';
import { LoaderService } from './loader.service';
import { NewIdeasMobileView } from './new-ideas-mobile-view/new-ideas-mobile-view.component';
import { RedirectorService } from './redirector.service';
import { ScrollPaneContainer } from './scroll-pane-container/scroll-pane-container.component';
import { ScrollPaneRowComponent } from './scroll-pane-row/scroll-pane-row.component';
import { ScrollPaneComponent } from './scroll-pane/scroll-pane.component';
import { VideoPlayerComponent } from './video-player/video-player.component';

try {
  angular
    .module('misc')
    .factory('goBack', downgradeInjectable(GoBackService))
    .factory('loader', downgradeInjectable(LoaderService) as any)
    .factory('localeInfo', downgradeInjectable(LocaleInfoService) as any);
} catch (e) {}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forChild(),
    DynamicModule,
    MatButtonModule,
    StoreModule.forFeature('shared', {
      multimedia: multimediaReducer
    })
  ],
  providers: [LoaderService, RedirectorService],
  declarations: [
    CarouselComponent,
    ScrollPaneComponent,
    LinkComponent,
    LineClampPipe,
    ClientUrlPipe,
    LazyLoadImageDirective,
    ScrollPaneContainer,
    NewIdeasMobileView,
    ScrollPaneRowComponent,
    AudioPlayerComponent,
    LinkDirective,
    VideoPlayerComponent,
    ActionButtonComponent,
    AnchorComponent,
    BackButtonComponent
  ],
  exports: [
    ActionButtonComponent,
    AudioPlayerComponent,
    VideoPlayerComponent,
    LinkDirective,
    CarouselComponent,
    ScrollPaneComponent,
    ScrollPaneRowComponent,
    LineClampPipe,
    LinkComponent,
    FormsModule,
    ReactiveFormsModule,
    ScrollPaneContainer,
    NewIdeasMobileView,
    BackButtonComponent
  ]
})
export class SharedModule {}
