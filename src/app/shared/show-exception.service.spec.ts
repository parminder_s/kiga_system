import { TestBed, inject } from '@angular/core/testing';

import { ShowExceptionService } from './show-exception.service';

describe('ShowExceptionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShowExceptionService]
    });
  });

  it(
    'should be created',
    inject([ShowExceptionService], (service: ShowExceptionService) => {
      expect(service).toBeTruthy();
    })
  );
});
