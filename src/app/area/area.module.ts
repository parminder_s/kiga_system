import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AreaEffects } from './+state/area.effects';
import { areaReducer } from './+state/area.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('area', areaReducer),
    EffectsModule.forFeature([AreaEffects])
  ],
  declarations: []
})
export class AreaModule {}
