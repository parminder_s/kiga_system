import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { mergeMap, withLatestFrom } from 'rxjs/operators';
import { linksSelector } from '../../base-data/base-data.selectors';
import { AreaType } from '../area-type';
import { AreaActionTypes, FilterAgeGroup } from './area.action';
import { getArea } from './area.selectors';

@Injectable()
export class AreaEffects {
  @Effect({ dispatch: false })
  filterAgeGroup$ = this.actions$.pipe(
    ofType<FilterAgeGroup>(AreaActionTypes.FilterAgeGroup),
    withLatestFrom(this.store.pipe(select(getArea)), this.store.pipe(select(linksSelector))),
    mergeMap(([{ ageGroup }, area, links]) => {
      if (area === AreaType.DEFAULT) {
        this.router.navigate([links.ideaGroupMainContainer], {
          queryParams: { ageGroupFilter: ageGroup }
        });
      } else if (area === AreaType.IDEAS) {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: { ageGroupFilter: ageGroup },
          queryParamsHandling: 'merge'
        });
      }

      return of(null);
    })
  );

  constructor(
    private actions$: Actions,
    private store: Store<any>,
    private router: Router,
    private route: ActivatedRoute
  ) {}
}
