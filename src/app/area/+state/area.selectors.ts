import { AreaState } from './area.reducer';
import { createSelector } from '@ngrx/store';

export const getAreaState: (state: any) => AreaState = state => state.area;

export const getArea = createSelector(
  getAreaState,
  areaState => areaState.area
);
export const getAgeGroup = createSelector(
  getAreaState,
  areaState => areaState.selectedAgeGroup
);
