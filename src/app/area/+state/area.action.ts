import { Action } from '@ngrx/store';
import { AreaType } from '../area-type';

export enum AreaActionTypes {
  SetArea = '[Area] Set Area',
  FilterAgeGroup = '[Area] Filter Age Group',
  AgeGroupFiltered = '[Area] Age Group Filtered'
}

export class SetArea implements Action {
  readonly type = AreaActionTypes.SetArea;

  constructor(public areaType: AreaType) {}
}

export class FilterAgeGroup implements Action {
  readonly type = AreaActionTypes.FilterAgeGroup;

  constructor(public ageGroup: number) {}
}

export type AreaActions = SetArea | FilterAgeGroup;
