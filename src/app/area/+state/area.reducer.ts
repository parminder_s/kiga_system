import { RouterActions, RouterActionTypes } from '../../router-store/router.actions';
import { AreaType } from '../area-type';
import { AreaActions, AreaActionTypes } from './area.action';

export interface AreaState {
  area: AreaType;
  selectedAgeGroup: number;
  showLocaleSwitcher: boolean;
}

const initialState: AreaState = {
  area: AreaType.DEFAULT,
  selectedAgeGroup: 0,
  showLocaleSwitcher: true
};

export function areaReducer(state = initialState, action: AreaActions | RouterActions): AreaState {
  switch (action.type) {
    case AreaActionTypes.SetArea:
      const showLanguageSwitcher = action.areaType === AreaType.DEFAULT ? true : false;
      return { ...state, showLocaleSwitcher: showLanguageSwitcher, area: action.areaType };
    case AreaActionTypes.FilterAgeGroup:
      return { ...state, selectedAgeGroup: action.ageGroup };
    case RouterActionTypes.LOAD_ROUTERS:
      const ageGroupFilter = action.payload.queryParams['ageGroupFilter'];
      if (ageGroupFilter && state.selectedAgeGroup === 0) {
        return { ...state, selectedAgeGroup: parseInt(ageGroupFilter) };
      } else if (!ageGroupFilter && state.selectedAgeGroup !== 0) {
        return { ...state, selectedAgeGroup: 0 };
      } else {
        return state;
      }
    default:
      return state;
  }
}
