import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { mockClass } from './mockClass';
import { Router } from '@angular/router';
describe('AppComponent', () => {
  it('should generate', () => {
    const subscribe = jest.fn();
    const events = { pipe: () => ({ subscribe }) };

    const router = mockClass<Router>({ events });
    expect(new AppComponent(null, null, null, router, null, null, null)).toBeTruthy();
  });
});
