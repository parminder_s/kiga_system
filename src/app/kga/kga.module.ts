import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditChildComponent } from './edit-child/edit-child.component';
import { RouterModule } from '@angular/router';
import { FormModule } from '../form/form.module';
import { EndpointModule } from '../endpoint/endpoint.module';
import { EditParentComponent } from './edit-parent/edit-parent.component';
import { EditGroupComponent } from './edit-group/edit-group.component';
import { EditKindergartenComponent } from './edit-kindergarten/edit-kindergarten.component';
import { ListKindergartenComponent } from './list-kindergarten/list-kindergarten.component';
import { ListGroupComponent } from './list-group/list-group.component';
import { ListChildComponent } from './list-child/list-child.component';
import { ListParentComponent } from './list-parent/list-parent.component';
import { ListYearComponent } from './list-year/list-year.component';
import { EditYearComponent } from './edit-year/edit-year.component';
import { YearImportComponent } from './year-import/year-import.component';
import { AcceptAdminComponent } from './accept-admin/accept-admin.component';
import { ApiModule } from '../openapi/kga';

import { FormlyMaterialModule } from '@ngx-formly/material';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMatDatepickerModule } from '@ngx-formly/material/datepicker';
import {
  MatFormFieldModule,
  MatInputModule,
  MatSortModule,
  MatTableModule,
  MatSelectModule,
  MatButtonModule,
  MatRadioModule,
  MatSnackBarModule,
  MatPaginatorModule,
  MatExpansionModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardElementComponent } from './dashboard/dashboard-element/dashboard-element.component';
import { SharedModule } from '../shared/shared.module';
import { DashboardYearComponent } from './dashboard/dashboard-year/dashboard-year.component';

import { DynamicModule } from 'ng-dynamic-component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { kgaReducer } from './+state/kga.reducer';
import { KgaEffects } from './+state/kga.effects';
import { EditChildContainerComponent } from './edit-child-container/edit-child-container.component';
import { ListGroupContainerComponent } from './list-group/list-group-container/list-group-container.component';
import { EditGroupContainerComponent } from './edit-group/edit-group-container/edit-group-container.component';
import { NewGroupContainerComponent } from './edit-group/new-group-container/new-group-container.component';
import { TranslateModule } from '@ngx-translate/core';
import { ListKindergartenContainerComponent } from './list-kindergarten-container/list-kindergarten-container.component';
import { KgaComponent } from './kga/kga.component';
import { AutocompleteComponent } from '../form/autocomplete/autocomplete.component';
import { ListEmployeeComponent } from './list-employee/list-employee.component';
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';
import { EditEmployeeContainerComponent } from './edit-employee/edit-employee-container/edit-employee-container.component';
import { ListEmployeeContainerComponent } from './list-employee/list-employee-container/list-employee-container.component';
import { ChildJournalComponent } from './child-journal/child-journal.component';
import { ChildJournalContainerComponent } from './child-journal/child-journal-container/child-journal-container.component';
import { KigaFormComponent } from '../form/kiga-form/kiga-form.component';
import { AgePipe } from './age.pipe';

@NgModule({
  imports: [
    ApiModule,
    CommonModule,
    FormModule,
    DynamicModule.withComponents([DashboardYearComponent]),
    EndpointModule,
    FormlyModule.forRoot({
      validators: [],
      types: [
        {
          name: 'autocomplete',
          component: AutocompleteComponent,
          wrappers: ['form-field']
        }
      ],
      validationMessages: [{ name: 'required', message: 'Dieses Feld muss ausgefüllt sein' }]
    }),
    ReactiveFormsModule,
    FormlyMaterialModule,
    FormlyMatDatepickerModule,
    MatSelectModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatSortModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatButtonModule,
    MatRadioModule,
    MatInputModule,
    MatTableModule,
    SharedModule,
    StoreModule.forFeature('kga', kgaReducer),
    TranslateModule,
    EffectsModule.forFeature([KgaEffects]),
    RouterModule.forChild([
      {
        path: '',
        children: [
          {
            path: 'permission/accept/:id',
            component: AcceptAdminComponent
          },
          {
            path: 'new',
            component: EditKindergartenComponent
          },
          {
            path: 'list',
            component: ListKindergartenContainerComponent
          },
          {
            path: ':kindergartenId',
            children: [
              {
                path: 'dashboard',
                component: DashboardComponent
              },
              {
                path: 'year',
                component: EditYearComponent
              },
              {
                path: ':yearId',
                children: [
                  {
                    path: 'dashboard',
                    component: DashboardComponent
                  },
                  {
                    path: 'kindergarten',
                    component: EditKindergartenComponent
                  },
                  {
                    path: 'year',
                    component: EditYearComponent
                  },
                  {
                    path: 'child',
                    children: [
                      {
                        path: 'list',
                        component: ListChildComponent
                      },
                      {
                        path: ':childId',
                        component: EditChildContainerComponent
                      },
                      {
                        path: ':childId/journal',
                        component: ChildJournalContainerComponent
                      },
                      {
                        path: 'new',
                        component: EditChildContainerComponent
                      }
                    ]
                  },
                  {
                    path: 'group',
                    children: [
                      {
                        path: 'list',
                        component: ListGroupContainerComponent
                      },
                      {
                        path: ':groupId',
                        component: EditGroupContainerComponent
                      },
                      {
                        path: 'new',
                        component: EditGroupContainerComponent
                      }
                    ]
                  },
                  {
                    path: 'employee',
                    children: [
                      {
                        path: 'list',
                        component: ListEmployeeContainerComponent
                      },
                      {
                        path: ':employeeId',
                        component: EditEmployeeContainerComponent
                      },
                      {
                        path: 'new',
                        component: EditEmployeeContainerComponent
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ])
  ],
  declarations: [
    EditChildComponent,
    EditParentComponent,
    EditGroupComponent,
    EditKindergartenComponent,
    ListKindergartenComponent,
    ListGroupComponent,
    ListChildComponent,
    ListParentComponent,
    ListYearComponent,
    EditYearComponent,
    YearImportComponent,
    AcceptAdminComponent,
    DashboardComponent,
    DashboardElementComponent,
    DashboardYearComponent,
    EditChildContainerComponent,
    ListGroupContainerComponent,
    EditGroupContainerComponent,
    NewGroupContainerComponent,
    ListEmployeeComponent,
    EditEmployeeComponent,
    KgaComponent,
    ListKindergartenContainerComponent,
    EditEmployeeContainerComponent,
    ListEmployeeContainerComponent,
    ChildJournalComponent,
    ChildJournalContainerComponent,
    AgePipe
  ],
  exports: [ReactiveFormsModule]
})
export class KgaModule {}
