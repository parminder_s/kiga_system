import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardYearComponent } from './dashboard-year.component';

describe('DashboardYearComponent', () => {
  let component: DashboardYearComponent;
  let fixture: ComponentFixture<DashboardYearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardYearComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardYearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
