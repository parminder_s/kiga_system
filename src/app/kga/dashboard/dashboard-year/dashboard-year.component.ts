import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ScrollPaneItemComponent } from '../../../shared/scroll-pane-row/scroll-pane-item-component';

@Component({
  selector: 'kiga-dashboard-year',
  templateUrl: './dashboard-year.component.html',
  styleUrls: ['./dashboard-year.component.less']
})
export class DashboardYearComponent implements OnInit, ScrollPaneItemComponent<any> {
  @Input() data: any;
  @Input() index: number;
  @Input() isActive: boolean;
  @Output() onClick = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {
    console.log(this.data);
    this.isActive = this.data.isActive;
  }

  click() {
    this.onClick.emit(this.data.id);
  }
}
