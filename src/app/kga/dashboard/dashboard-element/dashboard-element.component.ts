import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'kiga-dashboard-element',
  templateUrl: './dashboard-element.component.html',
  styleUrls: ['./dashboard-element.component.less']
})
export class DashboardElementComponent implements OnInit {
  @Input() name: string;
  @Input() nameIcon: string;
  @Input() buttonLabel: string;
  @Input() descriptionHeader: string;
  @Input() descriptionText: string;
  @Input() buttonUrl: any;

  constructor() {}

  ngOnInit() {}
}
