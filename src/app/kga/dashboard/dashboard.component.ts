import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EndpointService } from '../../endpoint/endpoint.service';
import { DashboardViewModel } from '../../openapi/kga';
import { DashboardYearComponent } from './dashboard-year/dashboard-year.component';

@Component({
  selector: 'kiga-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {
  yearId: number;
  kindergartenId: number;

  locale: string;

  dashboardData$: Observable<DashboardViewModel>;
  scrollPaneData: any[];
  public yearComopnent: any = DashboardYearComponent;

  constructor(
    private endpointService: EndpointService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private tranlsate: TranslateService
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.yearId = param['yearId'];
      this.kindergartenId = param['kindergartenId'];
      this.dashboardData$ = this.endpointService
        .post<DashboardViewModel>('/kga/kindergarten/fetchDashboard/', { id: this.kindergartenId })
        .pipe(
          map(data => {
            if (this.yearId === undefined && data.years.length > 0) {
              console.log(data.years[0]);
              this.router.navigate(['../', data.years[0].id, 'dashboard'], {
                relativeTo: this.activatedRoute
              });
            }
            this.scrollPaneData = data.years;
            this.tranlsate
              .get('KGA_DASHBOARD_CREATE_NEW_YEAR')
              .subscribe(data => this.scrollPaneData.push({ id: 0, name: data }));
            // todo check why === not working!
            // tslint:disable-next-line:triple-equals
            this.scrollPaneData.forEach(year => (year.isActive = year.id == this.yearId));
            return data;
          })
        );
    });
  }

  yearClicked(newYearId: number) {
    if (newYearId === 0) {
      if (this.yearId === undefined) {
        this.router.navigate(['../', 'year'], { relativeTo: this.activatedRoute });
      } else {
        this.router.navigate(['../../', 'year'], { relativeTo: this.activatedRoute });
      }
    } else {
      this.router.navigate(['../../', newYearId, 'dashboard'], { relativeTo: this.activatedRoute });
    }
  }
}
