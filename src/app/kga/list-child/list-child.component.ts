import { Component, OnInit, Input } from '@angular/core';
import {
  AllergyEnum,
  ChildModel,
  KgaGroupModel,
  KgaParentModel,
  VaccinationEnum
} from '../../openapi/kga';
import { MatSnackBar, Sort } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { EndpointService } from '../../endpoint/endpoint.service';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { AgePipe } from '../age.pipe';

@Component({
  selector: 'kiga-list-child',
  templateUrl: './list-child.component.html',
  styleUrls: ['./list-child.component.less']
})
export class ListChildComponent implements OnInit {
  yearId: number;
  kindergartenId: number;

  columnsToDisplay = [
    'edit',
    'name',
    'birthDate',
    'parents',
    'groupId',
    'siblings',
    'allergies',
    'delete'
  ];

  sorted: any[];
  children: any[];
  filter: any;
  groups: KgaGroupModel[];

  private datePipe: DatePipe;
  private agePipe: AgePipe;

  constructor(
    private activatedRoute: ActivatedRoute,
    private endpointService: EndpointService,
    private snackBar: MatSnackBar,
    private translate: TranslateService
  ) {
    this.datePipe = new DatePipe('en-US');
    this.agePipe = new AgePipe(translate);
  }

  ngOnInit() {
    this.filter = {};
    this.activatedRoute.params.subscribe(param => {
      this.kindergartenId = param['kindergartenId'];
      this.yearId = param['yearId'];
      this.endpointService
        .post<ChildModel[]>('/kga/child/list/byKgaYear', { id: this.yearId })
        .subscribe(data => {
          this.children = data.map(data => {
            return {
              id: data.id,
              name: data.firstname + ' ' + data.lastname,
              birthDate: this.datePipe.transform(data.birthDate, 'dd.MM.yyyy'),
              age: this.agePipe.transform(data.birthDate),
              groupId: data.groupId,
              parents: this.formatParents(data.parents),
              siblings: data.numberSiblings.toString(),
              allergies: this.formatAllergies(data.allergies)
            };
          });
          this.sorted = this.children;
        });
      this.endpointService
        .post<KgaGroupModel[]>('/kga/group/list/byYear', { id: this.yearId })
        .subscribe(data => (this.groups = data));
    });
  }

  filterData(name: string, value: string) {
    this.filter[name] = value;
    if (name == 'groupId') {
      this.sorted = this.children
        .filter(x => x.groupId !== null)
        .filter(x =>
          this.groups
            .filter(data => data.id == x.groupId)[0]
            .name.toLowerCase()
            .match(value.toLowerCase())
        );
    } else {
      this.sorted = this.children.filter(x => x[name].toLowerCase().match(value.toLowerCase()));
    }
  }

  sortData(sort: Sort) {
    const data = this.sorted.slice();
    if (!sort.active || sort.direction === '') {
      this.sorted = data;
      return;
    }

    this.sorted = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this.compare(a[sort.active], b[sort.active], isAsc);
    });
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  formatAllergies(allergies: AllergyEnum[]): string {
    return allergies.map(data => this.translate.instant('ALLERGY_' + data)).join(', ');
  }

  formatParents(parents: KgaParentModel[]) {
    return parents.map(data => this.formatParent(data)).join(', ');
  }

  formatParent(data: KgaParentModel): string {
    var returner = data.firstname + ' ' + data.lastname;
    if (data.phoneNumber != null) {
      returner += '/' + data.phoneNumber;
    }
    return returner;
  }

  save() {
    const data = this.children.map(data => {
      return { childId: data.id, groupId: data.groupId };
    });

    this.endpointService
      .post<ChildModel[]>('/kga/child/updateGroups/', { yearId: this.yearId, data: data })
      .subscribe(res => {
        this.snackBar.open(this.translate.instant('KGA_CHILD_SAVED_GROUP_ASSIGNMENT'), '', {
          duration: 1500
        });
        Object.keys(this.filter).forEach(key => this.filterData(key, this.filter[key]));
      });
  }

  delete(child) {
    console.log(child);
    let snackBarRef = this.snackBar.open(
      this.translate.instant('KGA_CHILD_CONFIRM_DELETE'),
      this.translate.instant('BUTTON_YES'),
      {
        duration: 5000
      }
    );
    snackBarRef.onAction().subscribe(() => {
      this.endpointService
        .post<ChildModel[]>('/kga/child/deleteOne/', { id: child.id })
        .subscribe(res => {
          this.snackBar.open(this.translate.instant('KGA_CHILD_INFO_DELETE'), '', {
            duration: 1500
          });
          console.log(this.children);
          this.children = this.children.filter(item => item.id !== child.id);
          this.sorted = this.sorted.filter(item => item.id !== child.id);
        });
    });
  }
}
