import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../../shared/loader.service';
import { ActivatedRoute } from '@angular/router';
import { EndpointService } from '../../endpoint/endpoint.service';
import { Observable } from 'rxjs';
import { YearModel } from '../../openapi/kga';

@Component({
  selector: 'kiga-list-year',
  templateUrl: './list-year.component.html',
  styleUrls: ['./list-year.component.less']
})
export class ListYearComponent implements OnInit {
  public yearsObservable: Observable<any>;
  public newYear: YearModel;

  public kindergartenId;

  constructor(
    private loader: LoaderService,
    private activatedRoute: ActivatedRoute,
    private endpointService: EndpointService
  ) {}

  ngOnInit() {
    this.loader.hide();
    this.activatedRoute.params.subscribe(param => {
      this.kindergartenId = param['id'];
      this.newYear = { kindergartenId: this.kindergartenId };
      this.fetchYears();
    });
  }

  fetchYears() {
    this.yearsObservable = this.endpointService.post('/kga/year/list/byKindergarten', {
      id: this.kindergartenId
    });
  }

  save(data: YearModel) {
    console.log(data);
    this.endpointService.post('/kga/year/save/', data).subscribe(res => console.log('Saved year'));
  }

  create(data: YearModel) {
    this.endpointService.post('/kga/year/save/', data).subscribe(res => this.fetchYears());
  }

  archive(data: YearModel) {
    console.log('archive');
    this.endpointService
      .post('/kga/year/archive/', { id: data.id })
      .subscribe(res => this.fetchYears());
  }
}
