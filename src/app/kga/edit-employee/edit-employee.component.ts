import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import {
  ChildModel,
  CountryViewModel,
  EmployeeModel,
  EmploymentContractEnum,
  EmploymentTypeEnum,
  GenderEnum,
  ReligionEnum
} from '../../openapi/kga';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { EndpointService } from '../../endpoint/endpoint.service';

@Component({
  selector: 'kiga-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.less']
})
export class EditEmployeeComponent implements OnInit {
  @Input() public employee: EmployeeModel;
  @Input() public locale: string;
  @Input() public kindergartenId: number;
  @Output() save = new EventEmitter<ChildModel>();
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[];
  fieldsParent: FormlyFieldConfig[];
  form = new FormGroup({});

  constructor(private translate: TranslateService, private endpointService: EndpointService) {}

  ngOnInit() {
    this.setupFieldEmployee();
    console.log(this.employee);
  }

  setupFieldEmployee() {
    this.fieldsParent = [
      {
        className: 'flex-container',
        fieldGroup: [
          {
            key: 'firstname',
            type: 'input',
            templateOptions: {
              type: 'text',
              label: this.translate.instant('FIELD_FIRSTNAME'),
              required: true
            }
          },
          {
            key: 'lastname',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_LASTNAME'),
              required: true
            }
          },
          {
            key: 'gender',
            type: 'radio',
            templateOptions: {
              label: this.translate.instant('FIELD_GENDER'),
              options: [
                { label: this.translate.instant('GENDER_F'), value: GenderEnum.F },
                { label: this.translate.instant('GENDER_M'), value: GenderEnum.M },
                { label: this.translate.instant('GENDER_O'), value: GenderEnum.O }
              ],
              required: true
            }
          },
          {
            key: 'employmentContract',
            type: 'radio',
            templateOptions: {
              label: this.translate.instant('KGA_FIELD_EMPLOYMENT_CONTRACT'),
              options: [
                {
                  label: this.translate.instant('KGA_EMPLOYMENT_CONTRACT_FULLTIME'),
                  value: EmploymentContractEnum.FULLTIME
                },
                {
                  label: this.translate.instant('KGA_EMPLOYMENT_CONTRACT_PARTTIME'),
                  value: EmploymentContractEnum.PARTTIME
                }
              ],
              required: true
            }
          },
          {
            key: 'employmentContractNote',
            type: 'input',
            hideExpression: () =>
              this.employee.employmentContract != EmploymentContractEnum.PARTTIME,
            templateOptions: {
              type: 'input',
              label: this.translate.instant('KGA_FIELD_EMPLOYMENT_CONTRACT_NOTE'),
              required: true
            }
          },
          {
            key: 'email',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_EMAIL'),
              required: true
            }
          },
          {
            key: 'phoneNumber',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_PHONE'),
              required: true
            }
          },
          {
            key: 'socialSecurityNumber',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_SOCIAL_SECURITY_NUMBER'),
              required: true
            }
          },
          {
            key: 'birthDate',
            type: 'datepicker',
            templateOptions: {
              type: 'datepicker',
              label: this.translate.instant('FIELD_BIRTHDAY'),
              required: true
            }
          },
          {
            key: 'employmentType',
            type: 'select',
            templateOptions: {
              type: 'select',
              label: this.translate.instant('KGA_EMPLOYEE_EMPLOYMENT_TYPE'),
              options: [
                {
                  value: EmploymentTypeEnum.EDUCATOR,
                  label: this.translate.instant('KGA_EMPLOYMENT_TYPE_EDUCATOR')
                },
                {
                  value: EmploymentTypeEnum.CHILDCAREWORKER,
                  label: this.translate.instant('KGA_EMPLOYMENT_TYPE_CHILD_CARE_WORKER')
                },
                {
                  value: EmploymentTypeEnum.CLEANER,
                  label: this.translate.instant('KGA_EMPLOYMENT_TYPE_CLEANER')
                },
                {
                  value: EmploymentTypeEnum.COMMUNITYSERVICE,
                  label: this.translate.instant('KGA_EMPLOYMENT_TYPE_COMMUNITY_SERVICE')
                },
                {
                  value: EmploymentTypeEnum.DAYDADDY,
                  label: this.translate.instant('KGA_EMPLOYMENT_TYPE_DAY_DADDY')
                },
                {
                  value: EmploymentTypeEnum.DAYNANNY,
                  label: this.translate.instant('KGA_EMPLOYMENT_TYPE_DAY_NANNY')
                },
                {
                  value: EmploymentTypeEnum.OTHER,
                  label: this.translate.instant('KGA_EMPLOYMENT_TYPE_OTHER')
                }
              ],
              required: true
            }
          },
          {
            key: 'address',
            templateOptions: {
              label: 'Address'
            },
            fieldGroup: [
              {
                key: 'street',
                type: 'input',
                templateOptions: {
                  type: 'input',
                  label: this.translate.instant('FIELD_STREET'),
                  required: true
                }
              },
              {
                fieldGroupClassName: 'address',
                fieldGroup: [
                  {
                    key: 'houseNumber',
                    type: 'input',
                    templateOptions: {
                      type: 'input',
                      label: this.translate.instant('FIELD_HOUSENR'),
                      required: true
                    }
                  },
                  {
                    key: 'floor',
                    type: 'input',
                    templateOptions: {
                      type: 'number',
                      label: this.translate.instant('FIELD_FLOOR')
                    }
                  },
                  {
                    key: 'doorNumber',
                    type: 'input',
                    templateOptions: {
                      type: 'number',
                      label: this.translate.instant('FIELD_DOORNR')
                    }
                  }
                ]
              },
              {
                fieldGroupClassName: 'place',
                fieldGroup: [
                  {
                    key: 'zipCode',
                    type: 'input',
                    templateOptions: {
                      type: 'number',
                      label: this.translate.instant('FIELD_ZIP'),
                      required: true
                    }
                  },
                  {
                    key: 'city',
                    type: 'input',
                    templateOptions: {
                      type: 'input',
                      label: this.translate.instant('FIELD_CITY'),
                      required: true
                    }
                  }
                ]
              },
              {
                key: 'countryId',
                type: 'autocomplete',
                templateOptions: {
                  type: 'autocomplete',
                  label: this.translate.instant('FIELD_COUNTRY'),
                  required: true,
                  autocompleteOptions: this.endpointService
                    .get<CountryViewModel[]>('/kga/country/list/' + this.locale)
                    .pipe(
                      map(data =>
                        data.map(country => {
                          return { value: country.id, label: country.title };
                        })
                      )
                    )
                }
              }
            ]
          },
          {
            key: 'parentalLeave',
            type: 'checkbox',
            templateOptions: {
              type: 'checkbox',
              label: this.translate.instant('KGA_FIELD_PARENTAL_LEAVE')
            }
          }
        ]
      }
    ];
  }

  saveClicked() {
    console.log('Here123', this.form);
    if (this.form.invalid) {
      console.log('Error');
    } else {
      this.save.emit(this.employee);
    }
  }
}
