import { Component, OnInit } from '@angular/core';
import { ChildModel, EmployeeModel } from '../../../openapi/kga';
import { Observable, of } from 'rxjs/index';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import {
  LoadEmployee,
  SaveChild,
  SaveEmployee,
  SetKindergartenId,
  SetYearId
} from '../../+state/kga.actions';
import { localeSelector } from '../../../base-data/base-data.selectors';
import { kgaQuery } from '../../+state/kga.selectors';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { selectLocale } from '../../../router-store/router-selectors';

@Component({
  selector: 'kiga-edit-employee-container',
  templateUrl: './edit-employee-container.component.html',
  styleUrls: ['./edit-employee-container.component.less']
})
export class EditEmployeeContainerComponent implements OnInit {
  public employee$: Observable<EmployeeModel>;
  public kindergartenId: number;
  public locale: string;
  constructor(
    private store: Store<any>,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.kindergartenId = param['kindergartenId'];
      this.locale = param['locale'];
      this.store.dispatch(new SetKindergartenId(this.kindergartenId));
      this.store.dispatch(new SetYearId(param['yearId']));
      if (param.employeeId !== 'new') {
        this.store.dispatch(new LoadEmployee(parseInt(param.employeeId, 10)));
        this.employee$ = this.store.pipe(select(kgaQuery.getSelectedEmployee));
      } else {
        this.employee$ = of({ parentalLeave: false });
      }
    });
  }

  save(employee: EmployeeModel) {
    employee.kindergartenId = this.kindergartenId;
    this.store.dispatch(new SaveEmployee(employee));
  }
}
