import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of as observableOf } from 'rxjs';
import { EndpointService } from '../../endpoint/endpoint.service';
import {
  CareTypeEnum,
  EmploymentContractEnum,
  KgaParentModel,
  ParentRoleEnum
} from '../../openapi/kga';
import { LoaderService } from '../../shared/loader.service';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'kiga-edit-parent',
  templateUrl: './edit-parent.component.html',
  styleUrls: ['./edit-parent.component.less']
})
export class EditParentComponent implements OnInit {
  @Input() public parent: KgaParentModel;
  @Input() public childId: number;

  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[];
  fieldsParent: FormlyFieldConfig[];
  form = new FormGroup({});

  constructor(
    private loader: LoaderService,
    private router: Router,
    private endpointService: EndpointService,
    private snackBar: MatSnackBar,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.loader.hide();
    this.setupFieldsParent();
  }

  setupFieldsParent() {
    this.fieldsParent = [
      {
        className: 'flex-container',
        fieldGroup: [
          {
            key: 'firstname',
            type: 'input',
            templateOptions: {
              type: 'text',
              label: this.translate.instant('FIELD_FIRSTNAME'),
              required: true
            }
          },
          {
            key: 'lastname',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_LASTNAME'),
              required: true
            }
          },
          {
            key: 'role',
            type: 'select',
            templateOptions: {
              type: 'select',
              label: this.translate.instant('KGA_FIELD_PARENT_ROLE'),
              labelProp: 'text',
              valueProp: 'id',
              options: Object.keys(ParentRoleEnum).map(name => {
                return {
                  text: this.translate.instant('PARENT_ROLE_' + name),
                  id: ParentRoleEnum[name]
                };
              }),
              required: true
            }
          },
          {
            key: 'phoneNumber',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_PHONE'),
              required: true
            }
          },
          {
            key: 'email',
            type: 'input',
            hideExpression: () => this.parent.legalGuardian == true,
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_EMAIL')
            }
          },
          {
            key: 'email',
            type: 'input',
            hideExpression: () => this.parent.legalGuardian != true,
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_EMAIL'),
              required: true
            }
          },
          {
            key: 'legalGuardian',
            type: 'checkbox',
            templateOptions: {
              type: 'checkbox',
              label: this.translate.instant('KGA_FIELD_LEGAL_GUARDIAN')
            }
          },
          {
            key: 'pickupAuthorized',
            type: 'checkbox',
            hideExpression: () => this.parent.legalGuardian == true,
            templateOptions: {
              type: 'checkbox',
              label: this.translate.instant('KGA_FIELD_PICKUP_AUTHORIZED')
            }
          },
          {
            key: 'emergencyAuthorized',
            type: 'checkbox',
            hideExpression: () => this.parent.legalGuardian == true,
            templateOptions: {
              type: 'checkbox',
              label: this.translate.instant('KGA_FIELD_EMERGENCY_AUTHORIZED')
            }
          },
          {
            key: 'employment',
            type: 'radio',
            hideExpression: () => this.parent.legalGuardian != true,
            templateOptions: {
              label: this.translate.instant('KGA_FIELD_EMPLOYMENT_CONTRACT'),
              options: [
                {
                  label: this.translate.instant('KGA_EMPLOYMENT_CONTRACT_FULLTIME'),
                  value: EmploymentContractEnum.FULLTIME
                },
                {
                  label: this.translate.instant('KGA_EMPLOYMENT_CONTRACT_PARTTIME'),
                  value: EmploymentContractEnum.PARTTIME
                },
                {
                  label: this.translate.instant('KGA_EMPLOYMENT_CONTRACT_NOTEMPLOYT'),
                  value: EmploymentContractEnum.NOTEMPLOYT
                }
              ],
              required: true
            }
          },
          {
            key: 'employment',
            type: 'radio',
            hideExpression: () => this.parent.legalGuardian == true,
            templateOptions: {
              label: this.translate.instant('KGA_FIELD_EMPLOYMENT_CONTRACT'),
              options: [
                {
                  label: this.translate.instant('KGA_EMPLOYMENT_CONTRACT_FULLTIME'),
                  value: EmploymentContractEnum.FULLTIME
                },
                {
                  label: this.translate.instant('KGA_EMPLOYMENT_CONTRACT_PARTTIME'),
                  value: EmploymentContractEnum.PARTTIME
                },
                {
                  label: this.translate.instant('KGA_EMPLOYMENT_CONTRACT_NOTEMPLOYT'),
                  value: EmploymentContractEnum.NOTEMPLOYT
                }
              ]
            }
          }
        ]
      }
    ];
  }

  save(data) {
    this.endpointService
      .post('/kga/parent/save/', { parent: data, childId: this.childId })
      .subscribe(res => {
        this.parent.id = <number>res;
        this.snackBar.open(this.translate.instant('BUTTON_SAVED'), '', {
          duration: 1500
        });
      });
  }
}
