import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListKindergartenContainerComponent } from './list-kindergarten-container.component';

describe('ListKindergartenContainerComponent', () => {
  let component: ListKindergartenContainerComponent;
  let fixture: ComponentFixture<ListKindergartenContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListKindergartenContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListKindergartenContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
