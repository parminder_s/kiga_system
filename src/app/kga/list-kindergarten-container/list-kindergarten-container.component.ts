import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { LoadKindergartens } from '../+state/kga.actions';
import { Observable } from 'rxjs';
import { KindergartenModel } from '../../openapi/kga';
import { kgaQuery } from '../+state/kga.selectors';
import { Router, ActivatedRoute } from '@angular/router';
import { tap, filter } from 'rxjs/operators';

@Component({
  templateUrl: './list-kindergarten-container.component.html'
})
export class ListKindergartenContainerComponent implements OnInit {
  public kindergartens$: Observable<Array<KindergartenModel>>;
  constructor(private store: Store<any>, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.store.dispatch(new LoadKindergartens());
    this.kindergartens$ = this.store.pipe(
      select(kgaQuery.getKindergartens),
      filter(kindergartens => !!kindergartens),
      tap(kindergartens => {
        if (kindergartens.length === 1) {
          this.router.navigate(['../', kindergartens[0].id, 'dashboard'], {
            relativeTo: this.route
          });
        }
      }),
      filter(kindergartens => kindergartens.length > 1)
    );
  }
}
