import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CareTypeEnum, KgaGroupModel } from '../../openapi/kga';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'kiga-edit-group',
  templateUrl: './edit-group.component.html',
  styleUrls: ['./edit-group.component.less']
})
export class EditGroupComponent implements OnInit {
  @Input() public group: KgaGroupModel;
  @Input() public yearId: number;
  @Input() public kindergartenId: number;

  @Output() save = new EventEmitter<KgaGroupModel>();

  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[];
  fieldsParent: FormlyFieldConfig[];
  form = new FormGroup({});

  constructor(private translate: TranslateService) {}

  ngOnInit() {
    this.setupFields();
  }

  saveGroup(group: KgaGroupModel) {
    this.save.emit(group);
  }

  setupFields() {
    this.fields = [
      {
        className: 'flex-container',
        fieldGroup: [
          {
            key: 'name',
            type: 'input',
            templateOptions: {
              type: 'text',
              label: this.translate.instant('FIELD_NAME'),
              required: true
            }
          },
          {
            key: 'email',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_EMAIL')
            }
          },
          {
            key: 'phoneNumber',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_PHONE')
            }
          },
          {
            key: 'room',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_ROOM')
            }
          },
          {
            key: 'notes',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_NOTES')
            }
          }
        ]
      }
    ];
  }

  scrollToError() {
    document.querySelector('form .ng-invalid').scrollIntoView({
      block: 'center'
    });
  }
}
