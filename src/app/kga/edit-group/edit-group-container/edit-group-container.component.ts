import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../../../shared/loader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { EndpointService } from '../../../endpoint/endpoint.service';
import { Observable, of } from 'rxjs/index';
import { KgaGroupModel, SetAdminRequest } from '../../../openapi/kga';

@Component({
  templateUrl: './edit-group-container.component.html'
})
export class EditGroupContainerComponent implements OnInit {
  group$: Observable<KgaGroupModel>;
  groupId: number;
  locale: string;

  constructor(
    private loader: LoaderService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private endpointService: EndpointService
  ) {}

  ngOnInit() {
    this.loader.hide();
    this.activatedRoute.params.subscribe(param => {
      this.locale = param['locale'];
      console.log(param['groupId']);
      if (param['groupId'] === 'new') {
        this.group$ = of({ yearId: param['yearId'] });
      } else {
        this.group$ = this.endpointService.post('/kga/group/findOne/', {
          id: param['groupId']
        });
      }
    });
  }

  save(data: KgaGroupModel) {
    this.endpointService.post('/kga/kgagroup/save/', data).subscribe(res => {
      this.router.navigate(['../list'], { relativeTo: this.activatedRoute });
    });
  }

  setAdmin(data: KgaGroupModel) {
    let req: SetAdminRequest = { id: data.id, email: data.adminEmail };
    this.endpointService.post('/kga/kgagroup/setAdmin', req).subscribe(res => console.log(res));
  }

  setSecondAdmin(data) {
    let req: SetAdminRequest = { id: data.id, email: data.secondAdminEmail };
    this.endpointService
      .post('/kga/kgagroup/setSecondAdmin/', req)
      .subscribe(res => console.log(res));
  }
}
