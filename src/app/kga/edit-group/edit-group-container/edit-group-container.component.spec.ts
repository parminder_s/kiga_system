import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGroupContainerComponent } from './edit-group-container.component';

describe('EditGroupContainerComponent', () => {
  let component: EditGroupContainerComponent;
  let fixture: ComponentFixture<EditGroupContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditGroupContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGroupContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
