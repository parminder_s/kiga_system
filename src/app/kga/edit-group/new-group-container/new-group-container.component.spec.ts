import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewGroupContainerComponent } from './new-group-container.component';

describe('NewGroupContainerComponent', () => {
  let component: NewGroupContainerComponent;
  let fixture: ComponentFixture<NewGroupContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewGroupContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewGroupContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
