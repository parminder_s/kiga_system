import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KgaComponent } from './kga.component';

describe('KgaComponent', () => {
  let component: KgaComponent;
  let fixture: ComponentFixture<KgaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KgaComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KgaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
