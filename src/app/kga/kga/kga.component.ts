import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { SetArea } from '../../area/+state/area.action';
import { AreaType } from '../../area/area-type';

@Component({
  templateUrl: './kga.component.html',
  styleUrls: ['./kga.component.less']
})
export class KgaComponent implements OnInit {
  constructor(private store: Store<any>) {}

  ngOnInit() {
    this.store.dispatch(new SetArea(AreaType.KGA));
  }
}
