import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptAdminComponent } from './accept-admin.component';

describe('AcceptAdminComponent', () => {
  let component: AcceptAdminComponent;
  let fixture: ComponentFixture<AcceptAdminComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [AcceptAdminComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
