import { Component, OnInit } from '@angular/core';
import { EndpointService } from '../../endpoint/endpoint.service';
import { LoaderService } from '../../shared/loader.service';
import { ActivatedRoute } from '@angular/router';
import { IdRequest } from '../../openapi/kga';

@Component({
  selector: 'kiga-accept-admin',
  templateUrl: './accept-admin.component.html',
  styleUrls: ['./accept-admin.component.less']
})
export class AcceptAdminComponent implements OnInit {
  private id: number;

  constructor(
    private loader: LoaderService,
    private activatedRoute: ActivatedRoute,
    private endpointService: EndpointService
  ) {}

  ngOnInit() {
    this.loader.hide();
    this.activatedRoute.params.subscribe(param => (this.id = param['id']));
  }

  accept(value) {
    console.log('id:', this.id);
    let req: IdRequest = { id: this.id };
    this.endpointService.post('/kga/permission/accept', req).subscribe(res => console.log(res));
  }
}
