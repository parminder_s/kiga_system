import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Input } from '@angular/core';
import { KgaGroupModel } from '../../openapi/kga';
import { Sort } from '@angular/material';

@Component({
  selector: 'kiga-list-group',
  templateUrl: './list-group.component.html',
  styleUrls: ['./list-group.component.less'],
  providers: [DatePipe]
})
export class ListGroupComponent implements OnInit {
  @Input() public groups: KgaGroupModel[];
  @Input() public yearId;
  @Input() public kindergartenId: number;

  sorted: KgaGroupModel[];

  columnsToDisplay = ['edit', 'name', 'email', 'countChildren'];

  constructor() {}

  ngOnInit() {
    this.sorted = this.groups;
  }

  filterData(name: string, value: string) {
    if (value === '') {
      this.sorted = this.groups;
    } else {
      this.sorted = this.groups.filter(
        x => x[name] !== null && ('' + x[name].toLowerCase()).match(value.toLowerCase())
      );
    }
  }

  sortData(sort: Sort) {
    const data = this.sorted.slice();
    if (!sort.active || sort.direction === '') {
      this.sorted = data;
      return;
    }

    this.sorted = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this.compare(a[sort.active], b[sort.active], isAsc);
    });
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}
