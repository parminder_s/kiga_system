import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGroupContainerComponent } from './list-group-container.component';

describe('ListGroupContainerComponent', () => {
  let component: ListGroupContainerComponent;
  let fixture: ComponentFixture<ListGroupContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListGroupContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGroupContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
