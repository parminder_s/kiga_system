import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { EndpointService } from '../../../endpoint/endpoint.service';
import { KgaGroupModel } from '../../../openapi/kga';

@Component({
  selector: 'kiga-list-group-container',
  templateUrl: './list-group-container.component.html'
})
export class ListGroupContainerComponent implements OnInit {
  public yearId: number;
  public kindergartenId: number;
  public groups$: Observable<KgaGroupModel[]>;

  constructor(private activatedRoute: ActivatedRoute, private endpointService: EndpointService) {}

  ngOnInit() {
    console.log('here123');
    this.activatedRoute.params.subscribe(param => {
      this.yearId = param['yearId'];
      this.kindergartenId = param['kindergartenId'];
      if (this.yearId !== undefined) {
        this.groups$ = this.endpointService.post('/kga/group/list/byYear', {
          id: this.yearId
        });
      } else {
        this.groups$ = of([]);
      }
    });
  }
}
