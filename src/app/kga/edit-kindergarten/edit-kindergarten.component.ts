import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { EndpointService } from '../../endpoint/endpoint.service';
import { CountryViewModel, KindergartenModel, SetAdminRequest } from '../../openapi/kga';
import { LoaderService } from '../../shared/loader.service';
import { select, Store } from '@ngrx/store';
import { localeSelector } from '../../base-data/base-data.selectors';
import { map } from 'rxjs/operators';

@Component({
  selector: 'kiga-edit-kindergarten',
  templateUrl: './edit-kindergarten.component.html',
  styleUrls: ['./edit-kindergarten.component.less']
})
export class EditKindergartenComponent implements OnInit {
  public kindergartenId: number;
  private yearId: number;
  public kindergarten$: Observable<KindergartenModel>;

  private locale: string;

  fields: FormlyFieldConfig[];
  form = new FormGroup({});

  constructor(
    private loader: LoaderService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private endpointService: EndpointService,
    private translate: TranslateService,
    private store: Store<any>
  ) {}

  ngOnInit() {
    this.loader.hide();
    this.store.pipe(select(localeSelector)).subscribe(data => (this.locale = data));
    this.activatedRoute.params.subscribe(param => {
      this.yearId = param['yearId'];
      this.kindergartenId = param['kindergartenId'];
      if (this.kindergartenId === undefined) {
        this.kindergarten$ = of({});
      } else {
        this.kindergarten$ = this.endpointService.post<KindergartenModel>(
          '/kga/kindergarten/findOne/',
          { id: this.kindergartenId }
        );
      }
    });

    this.fields = [
      {
        fieldGroup: [
          {
            key: 'name',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_NAME'),
              required: true
            }
          },
          {
            key: 'website',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_WEBSITE')
            }
          },
          {
            key: 'email',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_EMAIL')
            }
          },
          {
            key: 'phoneNumber',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_PHONE')
            }
          },

          {
            key: 'address',
            templateOptions: {
              label: 'Address'
            },
            fieldGroup: [
              {
                key: 'street',
                type: 'input',
                templateOptions: {
                  type: 'input',
                  label: this.translate.instant('FIELD_STREET'),
                  required: true
                }
              },
              {
                fieldGroupClassName: 'address',
                fieldGroup: [
                  {
                    key: 'houseNumber',
                    type: 'input',
                    templateOptions: {
                      type: 'input',
                      label: this.translate.instant('FIELD_HOUSENR'),
                      required: true
                    }
                  },
                  {
                    key: 'floor',
                    type: 'input',
                    templateOptions: {
                      type: 'number',
                      label: this.translate.instant('FIELD_FLOOR')
                    }
                  },
                  {
                    key: 'doorNumber',
                    type: 'input',
                    templateOptions: {
                      type: 'number',
                      label: this.translate.instant('FIELD_DOORNR')
                    }
                  }
                ]
              },
              {
                fieldGroupClassName: 'place',
                fieldGroup: [
                  {
                    key: 'zipCode',
                    type: 'input',
                    templateOptions: {
                      type: 'number',
                      label: this.translate.instant('FIELD_ZIP'),
                      required: true
                    }
                  },
                  {
                    key: 'city',
                    type: 'input',
                    templateOptions: {
                      type: 'input',
                      label: this.translate.instant('FIELD_CITY'),
                      required: true
                    }
                  }
                ]
              },
              {
                key: 'countryId',
                type: 'autocomplete',
                templateOptions: {
                  type: 'autocomplete',
                  label: this.translate.instant('FIELD_COUNTRY'),
                  required: true,
                  autocompleteOptions: this.endpointService
                    .get<CountryViewModel[]>('/kga/country/list/' + this.locale)
                    .pipe(
                      map(data =>
                        data.map(country => {
                          return { value: country.id, label: country.title };
                        })
                      )
                    )
                }
              }
            ]
          }
        ]
      }
    ];
  }

  save(data) {
    this.endpointService.post('/kga/kindergarten/save/', data).subscribe(res => {
      if (this.yearId === undefined) {
        this.router.navigate(['../list'], { relativeTo: this.activatedRoute });
      } else {
        this.router.navigate(['../dashboard'], { relativeTo: this.activatedRoute });
      }
    });
  }

  setAdminEmail(data) {
    console.log(data);
    let req: SetAdminRequest = { id: this.kindergartenId, email: data.email };
    this.endpointService.post('/kga/kindergarten/setAdmin', req).subscribe(res => console.log(res));
  }
}
