import { Component, Input, OnInit } from '@angular/core';
import { KindergartenModel } from '../../openapi/kga';

@Component({
  selector: 'kiga-list-kindergarten',
  templateUrl: './list-kindergarten.component.html',
  styleUrls: ['./list-kindergarten.component.less']
})
export class ListKindergartenComponent implements OnInit {
  @Input() kindergartens: Array<KindergartenModel>;

  constructor() {}

  ngOnInit() {}
}
