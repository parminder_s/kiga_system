import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'age'
})
export class AgePipe implements PipeTransform {
  constructor(private translate: TranslateService) {}

  transform(value: any, args?: any): any {
    const now = new Date(); //now as param?
    const thisYearsDate = new Date(now.getFullYear(), value.getMonth(), value.getDate());

    const yearOfBirth = value.getFullYear();
    var years = now.getFullYear() - yearOfBirth - 1;
    if (thisYearsDate.getTime() < new Date().getTime()) {
      years = years + 1;
    }

    var monthOfBirth = value.getMonth();
    var months = now.getMonth() - value.getMonth();
    if (now.getMonth() < value.getMonth) {
      months = months + 12;
    }

    var yearsWord = this.translate.instant('KGA_FIELD_YEARS');
    if (years == 1) {
      yearsWord = this.translate.instant('KGA_FIELD_YEAR');
    }

    var monthWord = this.translate.instant('KGA_FIELD_MONTHS');
    if (months == 1) {
      monthWord = this.translate.instant('KGA_FIELD_MONTH');
    }

    return years + ' ' + yearsWord + ' ' + months + ' ' + monthWord;
  }
}
