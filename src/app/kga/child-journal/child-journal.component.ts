import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ChildJournalEntry, ParentRoleEnum } from '../../openapi/kga';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { CannedAccessControlList } from '../../form/file-upload/CannedAccessControlList';

@Component({
  selector: 'kiga-child-journal',
  templateUrl: './child-journal.component.html',
  styleUrls: ['./child-journal.component.less']
})
export class ChildJournalComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Input() entries: Array<ChildJournalEntry>;
  @Input() childId: number;
  @Output() save = new EventEmitter<ChildJournalEntry>();

  fields: { [key: number]: FormlyFieldConfig[] };
  displayedColumns: string[] = ['data'];
  newIsAdded: boolean;

  dataSource: MatTableDataSource<any>;

  constructor(private translate: TranslateService) {}

  ngOnInit() {
    this.newIsAdded = false;
    this.setupFields();
    console.log(this.fields);
  }

  saveMethod(data) {
    this.save.emit(data);
  }

  filterData(value) {
    console.log(value);
  }

  addNew() {
    this.newIsAdded = true;
    const childJournalentry: ChildJournalEntry = {};
    childJournalentry.childId = this.childId;
    childJournalentry.content = null;
    childJournalentry.title = this.translate.instant('KGA_FIELD_NEW_ENTRY');
    this.entries.push(childJournalentry);
    this.setupFields();
    console.log('whaaat', this.newIsAdded);
  }

  setupFields() {
    this.dataSource = new MatTableDataSource<any>(this.entries);
    this.dataSource.paginator = this.paginator;
    this.fields = {};
    this.dataSource.data.forEach(element => {
      this.fields[element.id] = [
        {
          className: 'flex-container',
          fieldGroup: [
            {
              key: 'title',
              type: 'input',
              templateOptions: {
                type: 'text',
                label: this.translate.instant('FIELD_NAME'),
                required: true
              }
            },
            {
              key: 'content',
              type: 'textarea',
              templateOptions: {
                type: 'input',
                label: this.translate.instant('KGA_FIELD_NOTE')
              }
            },
            {
              key: 'files',
              type: 'file-upload',
              templateOptions: {
                label: this.translate.instant('FIELD_ATTACHMENT'),
                permission: CannedAccessControlList.PublicRead,
                fileClicked: file => {
                  const url =
                    window['kiga'].endPointUrl +
                    '/kga/childJournal/file/' +
                    this.childId +
                    '/' +
                    file.id;
                  window.open(url, '_blank');
                }
              }
            }
          ]
        }
      ];
    });
  }
}
