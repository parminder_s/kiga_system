import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildJournalContainerComponent } from './child-journal-container.component';

describe('ChildJournalContainerComponent', () => {
  let component: ChildJournalContainerComponent;
  let fixture: ComponentFixture<ChildJournalContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChildJournalContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildJournalContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
