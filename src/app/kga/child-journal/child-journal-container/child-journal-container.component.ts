import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs/index';
import { localeSelector } from '../../../base-data/base-data.selectors';
import {
  LoadJournalForChild,
  SaveJournalForChild,
  SetChildId,
  SetKindergartenId,
  SetYearId
} from '../../+state/kga.actions';
import { kgaQuery } from '../../+state/kga.selectors';
import { ChildJournalEntry, ChildModel } from '../../../openapi/kga';

@Component({
  selector: 'kiga-child-journal-container',
  templateUrl: './child-journal-container.component.html',
  styleUrls: ['./child-journal-container.component.less']
})
export class ChildJournalContainerComponent implements OnInit {
  public journal$: Observable<Array<ChildJournalEntry>>;
  public childId: number;
  constructor(private store: Store<any>, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.childId = parseInt(param.childId, 10);
      this.store.dispatch(new SetKindergartenId(param['kindergartenId']));
      this.store.dispatch(new SetYearId(param['yearId']));
      this.store.dispatch(new SetChildId(this.childId));
      this.store.dispatch(new LoadJournalForChild());
      this.journal$ = this.store.pipe(select(kgaQuery.getJournalForChild));
    });
  }

  save(data) {
    this.store.dispatch(new SaveJournalForChild(data));
  }
}
