import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildJournalComponent } from './child-journal.component';

describe('ChildJournalComponent', () => {
  let component: ChildJournalComponent;
  let fixture: ComponentFixture<ChildJournalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChildJournalComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildJournalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
