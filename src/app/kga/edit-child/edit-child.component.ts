import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { map } from 'rxjs/operators';
import { EndpointService } from '../../endpoint/endpoint.service';
import {
  AllergyEnum,
  CareTypeEnum,
  ChildModel,
  CountryViewModel,
  DiseasesEnum,
  GenderEnum,
  KgaParentModel,
  LanguageViewModel,
  ParentRoleEnum,
  ReligionEnum,
  VaccinationEnum
} from '../../openapi/kga';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'kiga-edit-child',
  templateUrl: './edit-child.component.html',
  styleUrls: ['./edit-child.component.less']
})
export class EditChildComponent implements OnInit {
  @Input() child: ChildModel;
  @Input() locale: string;
  @Input() languages: LanguageViewModel[];
  @Output() save = new EventEmitter<ChildModel>();

  constructor(private endpointService: EndpointService, private translate: TranslateService) {}

  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[];
  form = new FormGroup({});

  ngOnInit(): void {
    this.setupFields();
  }

  saveChild(child: ChildModel) {
    if (this.form.invalid) {
      // this.scrollToError();
    } else {
      this.save.emit(child);
    }
  }

  saveParent(data: KgaParentModel) {
    console.log(data);
  }

  scrollToError() {
    document.querySelector('form .ng-invale').scrollIntoView({
      block: 'center'
    });
  }

  setupFields() {
    console.log(this.child);
    this.fields = [
      {
        className: 'flex-container',
        fieldGroup: [
          {
            key: 'firstname',
            type: 'input',
            templateOptions: {
              type: 'text',
              label: this.translate.instant('FIELD_FIRSTNAME'),
              required: true
            }
          },
          {
            key: 'lastname',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_LASTNAME'),
              required: true
            }
          },
          {
            key: 'gender',
            type: 'radio',
            templateOptions: {
              label: this.translate.instant('FIELD_GENDER'),
              options: [
                { label: this.translate.instant('GENDER_F'), value: GenderEnum.F },
                { label: this.translate.instant('GENDER_M'), value: GenderEnum.M },
                { label: this.translate.instant('GENDER_O'), value: GenderEnum.O }
              ],
              required: true
            }
          },
          {
            key: 'socialSecurityNumber',
            type: 'input',
            templateOptions: {
              type: 'input',
              label: this.translate.instant('FIELD_SOCIAL_SECURITY_NUMBER'),
              required: true
            }
          },
          {
            key: 'birthDate',
            type: 'datepicker',
            templateOptions: {
              type: 'datepicker',
              label: this.translate.instant('FIELD_BIRTHDAY'),
              required: true
            }
          },
          {
            key: 'entryDate',
            type: 'datepicker',
            templateOptions: {
              type: 'datepicker',
              label: this.translate.instant('FIELD_ENTRY_DATE'),
              required: true
            }
          },
          {
            key: 'seperationDate',
            type: 'datepicker',
            templateOptions: {
              type: 'datepicker',
              label: this.translate.instant('FIELD_SEPERATION_DATE')
            }
          },
          {
            key: 'careType',
            type: 'select',
            templateOptions: {
              type: 'select',
              label: this.translate.instant('KGA_GROUP_CARE_TYPE'),
              options: [
                { label: this.translate.instant('CARETYPE_ALLDAY'), value: CareTypeEnum.ALLDAY },
                { label: this.translate.instant('CARETYPE_MORNING'), value: CareTypeEnum.MORNING },
                {
                  label: this.translate.instant('CARETYPE_AFTERNOON'),
                  value: CareTypeEnum.AFTERNOON
                },
                {
                  label: this.translate.instant('CARETYPE_FLEXIBLE'),
                  value: CareTypeEnum.FLEXIBLE
                },
                { label: this.translate.instant('CARETYPE_OTHER'), value: CareTypeEnum.OTHER }
              ],
              required: true
            }
          },
          {
            key: 'careTypeNote',
            type: 'input',
            hideExpression: () =>
              this.child.careType == null || this.child.careType !== CareTypeEnum.OTHER,
            templateOptions: {
              type: 'input',
              label: this.translate.instant('KGA_FIELD_CARETYPE_OTHER'),
              required: true
            }
          },
          {
            key: 'withMeal',
            type: 'checkbox',
            templateOptions: {
              type: 'checkbox',
              label: this.translate.instant('KGA_CHILD_WITH_MEAL')
            }
          },
          {
            key: 'religion',
            type: 'select',
            templateOptions: {
              type: 'select',
              label: this.translate.instant('FIELD_RELIGION'),
              options: [
                {
                  label: this.translate.instant('RELIGION_CATHOLIC'),
                  value: ReligionEnum.CATHOLIC
                },
                {
                  label: this.translate.instant('RELIGION_PROTESTANT'),
                  value: ReligionEnum.PROTESTANT
                },
                {
                  label: this.translate.instant('RELIGION_ISLAM'),
                  value: ReligionEnum.ISLAM
                },
                {
                  label: this.translate.instant('RELIGION_ATHEIST'),
                  value: ReligionEnum.ATHEIST
                },
                {
                  label: this.translate.instant('RELIGION_OTHER'),
                  value: ReligionEnum.OTHER
                }
              ],
              required: true
            }
          },
          {
            key: 'religionNote',
            type: 'input',
            hideExpression: () =>
              this.child.religion == null || this.child.religion !== ReligionEnum.OTHER,
            templateOptions: {
              type: 'input',
              label: this.translate.instant('KGA_FIELD_RELIGION_OTHER'),
              required: true
            }
          },
          {
            key: 'citizenshipCountryId',
            type: 'autocomplete',
            templateOptions: {
              label: this.translate.instant('FIELD_CITIZENSHIP'),
              autocompleteOptions: this.endpointService
                .get<CountryViewModel[]>('/kga/country/list/' + this.locale)
                .pipe(
                  map(data =>
                    data.map(country => {
                      return { value: country.id, label: country.title };
                    })
                  )
                ),
              required: true
            }
          },
          {
            key: 'nativeLanguageId',
            type: 'select',
            templateOptions: {
              type: 'select',
              label: this.translate.instant('KGA_CHILD_NATIVE_LANGUAGE'),
              options: this.endpointService.post<CountryViewModel[]>('/kga/language/list').pipe(
                map(data =>
                  data.map(language => {
                    return {
                      value: language.id,
                      label: this.translate.instant('LANGUAGE_' + language.code.toUpperCase())
                    };
                  })
                )
              ),
              required: true
            }
          },
          {
            key: 'nativeLanguageNote',
            type: 'input',
            hideExpression: () =>
              this.child.nativeLanguageId == null ||
              this.child.nativeLanguageId ===
                this.languages.filter(lang => lang.code !== 'ot')[0].id,
            templateOptions: {
              type: 'input',
              label: this.translate.instant('KGA_FIELD_NATIVE_LANGUAGE_OTHER'),
              required: true
            }
          },
          {
            key: 'secondLanguage',
            type: 'select',
            templateOptions: {
              type: 'select',
              multiple: true,
              label: this.translate.instant('KGA_CHILD_SECOND_LANGUAGE'),
              options: this.languages.map(language => {
                return {
                  value: language.id,
                  label: this.translate.instant('LANGUAGE_' + language.code.toUpperCase())
                };
              })
            }
          },
          {
            key: 'secondLanguageNote',
            type: 'input',
            hideExpression: () =>
              this.child.secondLanguage == null ||
              this.child.secondLanguage.indexOf(
                this.languages.filter(lang => lang.code === 'ot')[0].id,
                0
              ) === -1,
            templateOptions: {
              type: 'input',
              label: this.translate.instant('KGA_FIELD_SECOND_LANGUAGE_OTHER'),
              required: true
            }
          },
          {
            key: 'numberSiblings',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: this.translate.instant('KGA_CHILD_NUMBER_SIBLINGS'),
              required: true
            }
          },
          {
            key: 'allergies',
            type: 'select',
            templateOptions: {
              type: 'select',
              multiple: true,
              label: this.translate.instant('KGA_CHILD_ALLERGIES'),
              options: [
                { label: this.translate.instant('ALLERGY_BEE'), value: AllergyEnum.BEE },
                { label: this.translate.instant('ALLERGY_EGG'), value: AllergyEnum.EGG },
                { label: this.translate.instant('ALLERGY_FISH'), value: AllergyEnum.FISH },
                { label: this.translate.instant('ALLERGY_GLUTEN'), value: AllergyEnum.GLUTEN },
                { label: this.translate.instant('ALLERGY_GRASS'), value: AllergyEnum.GRASS },
                { label: this.translate.instant('ALLERGY_LATEX'), value: AllergyEnum.LATEX },
                { label: this.translate.instant('ALLERGY_LUPINEN'), value: AllergyEnum.LUPINEN },
                { label: this.translate.instant('ALLERGY_MILK'), value: AllergyEnum.MILK },
                { label: this.translate.instant('ALLERGY_NUTS'), value: AllergyEnum.NUTS },
                { label: this.translate.instant('ALLERGY_PEANUT'), value: AllergyEnum.PEANUT },
                { label: this.translate.instant('ALLERGY_POLLEN'), value: AllergyEnum.POLLEN },
                {
                  label: this.translate.instant('ALLERGY_SHELLFISH'),
                  value: AllergyEnum.SHELLFISH
                },
                { label: this.translate.instant('ALLERGY_OTHER'), value: AllergyEnum.OTHER }
              ]
            }
          },
          {
            key: 'allergiesNote',
            type: 'input',
            hideExpression: () =>
              this.child.allergies == null ||
              this.child.allergies.indexOf(AllergyEnum.OTHER, 0) === -1,
            templateOptions: {
              type: 'input',
              label: this.translate.instant('KGA_FIELD_ALLERGY_OTHER'),
              required: true
            }
          },
          {
            key: 'diseases',
            type: 'select',
            templateOptions: {
              type: 'select',
              multiple: true,
              label: this.translate.instant('KGA_CHILD_DISEASES'),
              options: [
                { label: this.translate.instant('DISEASE_DIABETES'), value: DiseasesEnum.DIABETES },
                { label: this.translate.instant('DISEASE_OTHER'), value: DiseasesEnum.OTHER }
              ]
            }
          },
          {
            key: 'diseasesNote',
            type: 'input',
            hideExpression: () =>
              this.child.diseases == null ||
              this.child.diseases.indexOf(DiseasesEnum.OTHER, 0) === -1,
            templateOptions: {
              type: 'input',
              label: this.translate.instant('KGA_FIELD_DISEASE_OTHER'),
              required: true
            }
          },
          {
            key: 'vaccinations',
            type: 'select',
            templateOptions: {
              type: 'select',
              multiple: true,
              label: this.translate.instant('KGA_CHILD_VACCINATION'),
              labelProp: 'text',
              valueProp: 'id',
              options: Object.keys(VaccinationEnum).map(name => {
                return {
                  text: this.translate.instant('VACCINATION_' + name),
                  id: VaccinationEnum[name]
                };
              })
            }
          },
          {
            key: 'vaccinationsNote',
            type: 'input',
            hideExpression: () =>
              this.child.vaccinations == null ||
              this.child.vaccinations.indexOf(VaccinationEnum.OTHER, 0) === -1,
            templateOptions: {
              type: 'input',
              label: this.translate.instant('KGA_FIELD_VACCINATION_OTHER'),
              required: true
            }
          },
          {
            key: 'notes',
            type: 'textarea',
            templateOptions: {
              type: 'textarea',
              label: this.translate.instant('FIELD_NOTES')
            }
          }
        ]
      }
    ];
  }

  createParent() {
    this.child.parents.push({});
  }

  hideSecondLanguageNote() {
    return true;
  }
}
