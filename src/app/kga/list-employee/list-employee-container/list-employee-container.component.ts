import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { EmployeeModel } from '../../../openapi/kga';
import { Observable } from 'rxjs/Observable';
import { LoadChild, LoadEmployees } from '../../+state/kga.actions';
import { kgaQuery } from '../../+state/kga.selectors';
import { ActivatedRoute } from '@angular/router';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'kiga-list-employee-container',
  templateUrl: './list-employee-container.component.html',
  styleUrls: ['./list-employee-container.component.less']
})
export class ListEmployeeContainerComponent implements OnInit {
  employees$: Observable<EmployeeModel[]>;

  constructor(private activatedRoute: ActivatedRoute, private store: Store<any>) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.store.dispatch(new LoadEmployees(parseInt(param.kindergartenId, 10)));
      this.employees$ = this.store.pipe(select(kgaQuery.getListEmployee));
    });
  }
}
