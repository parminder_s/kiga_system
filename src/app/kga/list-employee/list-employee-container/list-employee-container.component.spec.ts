import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEmployeeContainerComponent } from './list-employee-container.component';

describe('ListEmployeeContainerComponent', () => {
  let component: ListEmployeeContainerComponent;
  let fixture: ComponentFixture<ListEmployeeContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListEmployeeContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEmployeeContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
