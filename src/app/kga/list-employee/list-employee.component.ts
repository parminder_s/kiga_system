import { Component, Input, OnInit } from '@angular/core';
import { EmployeeModel, KgaGroupModel } from '../../openapi/kga';
import { Sort } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'kiga-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.less']
})
export class ListEmployeeComponent implements OnInit {
  columnsToDisplay = ['edit', 'name', 'type', 'employmentContract', 'phone'];

  sorted: any[];

  @Input() employees: EmployeeModel[];
  private data: any;
  filter: any;

  constructor(private translate: TranslateService) {}

  ngOnInit() {
    this.filter = {};
    this.data = this.employees.map(employee => {
      return {
        id: employee.id,
        name: employee.lastname + ' ' + employee.firstname,
        type: this.translate.instant('KGA_EMPLOYMENT_TYPE_' + employee.employmentType),
        employmentContract: this.translate.instant(
          'KGA_EMPLOYMENT_CONTRACT_' + employee.employmentContract
        ),
        phone: employee.phoneNumber
      };
    });
    this.sorted = this.data;
  }

  sortData(sort: Sort) {
    const data = this.sorted.slice();
    if (!sort.active || sort.direction === '') {
      this.sorted = data;
      return;
    }

    this.sorted = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this.compare(a[sort.active], b[sort.active], isAsc);
    });
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  filterData(name: string, value: string) {
    this.filter[name] = value;
    this.sorted = this.data.filter(x => x[name].toLowerCase().match(value.toLowerCase()));
  }
}
