import { KgaActions, KgaActionTypes } from './kga.actions';
import { KgaState } from './kga.state';

const initialState: KgaState = {
  kindergartenId: null,
  yearId: null,
  kindergartens: null,
  childId: null,
  children: {
    list: [],
    selected: null
  },
  employees: {
    list: null,
    selected: null
  },
  journal: {
    child: null
  }
};

export function kgaReducer(state: KgaState = initialState, action: KgaActions) {
  switch (action.type) {
    case KgaActionTypes.ChildrenLoaded:
      return { ...state, children: { list: action.children, selected: null } };
    case KgaActionTypes.ChildLoaded:
      return { ...state, children: { list: state.children.list, selected: action.child } };
    case KgaActionTypes.KindergartensLoaded:
      return { ...state, kindergartens: action.kindergartens };
    case KgaActionTypes.SetKindergartenId:
      return { ...state, kindergartenId: action.kindergartenId };
    case KgaActionTypes.SetYearId:
      return { ...state, yearId: action.yearId };
    case KgaActionTypes.EmployeesLoaded:
      return { ...state, employees: { list: action.employees, selected: null } };
    case KgaActionTypes.EmployeeLoaded:
      return { ...state, employees: { list: state.employees.list, selected: action.employee } };
    case KgaActionTypes.SaveEmployee:
      return { ...state, employees: { list: null, selected: null } };
    case KgaActionTypes.LoadJournalForChild:
      return { ...state, journal: { child: null } };
    case KgaActionTypes.JournalForChildLoaded:
      return { ...state, journal: { child: action.entries } };
    case KgaActionTypes.SetChildId:
      return { ...state, childId: action.childId };
    default:
      return state;
  }
}
