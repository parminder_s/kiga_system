import { Action } from '@ngrx/store';
import {
  ChildModel,
  KindergartenModel,
  EmployeeModel,
  ChildJournalEntry,
  KgaParentModel
} from '../../openapi/kga';

export enum KgaActionTypes {
  LoadKindergartens = '[Kga] Load Kindergartens',
  KindergartensLoaded = '[Kga] Kindergartens Loaded',
  LoadChildren = '[Kga] Load Children',
  ChildrenLoaded = '[Kga] Children Loaded',
  LoadChild = '[Kga] Load Child',
  ChildLoaded = '[Kga] Child Loaded',
  SaveChild = '[Kga] Save Child',
  SetKindergartenId = '[Kga] Set KindergartenId',
  SetYearId = '[Kga] Set YearId',
  LoadEmployees = '[Kga] Load Employees',
  LoadEmployee = '[Kga] Load Employee',
  SaveEmployee = '[Kga] Save Employee',
  EmployeesLoaded = '[Kga] Employees Loaded',
  EmployeeLoaded = '[Kga] Employee Loaded',
  LoadJournalForChild = '[Kga] LoadJournal for Child',
  JournalForChildLoaded = '[Kga] Journal for Child Loaded',
  SaveJournalForChild = '[Kga] Save Journal for Child',
  SetChildId = '[Kga] SetChildId'
}

export class LoadKindergartens implements Action {
  readonly type = KgaActionTypes.LoadKindergartens;

  constructor() {}
}

export class KindergartensLoaded implements Action {
  readonly type = KgaActionTypes.KindergartensLoaded;

  constructor(public kindergartens: Array<KindergartenModel>) {}
}

export class LoadChildren implements Action {
  readonly type = KgaActionTypes.LoadChildren;

  constructor() {}
}

export class ChildrenLoaded implements Action {
  readonly type = KgaActionTypes.ChildrenLoaded;

  constructor(public children: Array<KgaParentModel>) {}
}

export class LoadChild implements Action {
  readonly type = KgaActionTypes.LoadChild;

  constructor(public id: number) {}
}

export class ChildLoaded implements Action {
  readonly type = KgaActionTypes.ChildLoaded;

  constructor(public child: ChildModel) {}
}

export class SaveChild implements Action {
  readonly type = KgaActionTypes.SaveChild;

  constructor(public child: ChildModel) {}
}

export class SetKindergartenId implements Action {
  readonly type = KgaActionTypes.SetKindergartenId;

  constructor(public kindergartenId: number) {}
}

export class SetYearId implements Action {
  readonly type = KgaActionTypes.SetYearId;

  constructor(public yearId: number) {}
}

export class SetChildId implements Action {
  readonly type = KgaActionTypes.SetChildId;

  constructor(public childId: number) {}
}

export class LoadEmployee implements Action {
  readonly type = KgaActionTypes.LoadEmployee;

  constructor(public id: number) {}
}

export class SaveEmployee implements Action {
  readonly type = KgaActionTypes.SaveEmployee;

  constructor(public employee: EmployeeModel) {}
}

export class LoadEmployees implements Action {
  readonly type = KgaActionTypes.LoadEmployees;

  constructor(public id: number) {}
}

export class EmployeeLoaded implements Action {
  readonly type = KgaActionTypes.EmployeeLoaded;

  constructor(public employee: EmployeeModel) {}
}

export class EmployeesLoaded implements Action {
  readonly type = KgaActionTypes.EmployeesLoaded;

  constructor(public employees: Array<EmployeeModel>) {}
}

export class LoadJournalForChild implements Action {
  readonly type = KgaActionTypes.LoadJournalForChild;

  constructor() {}
}

export class JournalForChildLoaded implements Action {
  readonly type = KgaActionTypes.JournalForChildLoaded;

  constructor(public entries: Array<ChildJournalEntry>) {}
}

export class SaveJournalForChild implements Action {
  readonly type = KgaActionTypes.SaveJournalForChild;

  constructor(public entry: ChildJournalEntry) {}
}

export type KgaActions =
  | LoadKindergartens
  | KindergartensLoaded
  | LoadChildren
  | ChildrenLoaded
  | LoadChild
  | ChildLoaded
  | SaveChild
  | SetKindergartenId
  | SetYearId
  | LoadEmployee
  | SaveEmployee
  | LoadEmployees
  | EmployeesLoaded
  | EmployeeLoaded
  | LoadJournalForChild
  | JournalForChildLoaded
  | SaveJournalForChild
  | SetChildId;
