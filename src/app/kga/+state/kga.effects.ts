import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { EndpointService } from '../../endpoint/endpoint.service';
import { ChildJournalEntry, ChildModel, EmployeeModel, KindergartenModel } from '../../openapi/kga';
import {
  ChildLoaded,
  KgaActionTypes,
  LoadChild,
  LoadKindergartens,
  SaveChild,
  KindergartensLoaded,
  SaveEmployee,
  LoadEmployees,
  EmployeesLoaded,
  LoadEmployee,
  EmployeeLoaded,
  LoadJournalForChild,
  SaveJournalForChild,
  JournalForChildLoaded
} from './kga.actions';
import { selectLocale } from '../../router-store/router-selectors';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { kgaQuery } from './kga.selectors';

@Injectable()
export class KgaEffects {
  @Effect()
  loadKindergartens$ = this.actions$.pipe(
    ofType<LoadKindergartens>(KgaActionTypes.LoadKindergartens),
    mergeMap(() => this.endpoint.get<Array<KindergartenModel>>('kga/kindergarten/list')),
    map(kindergartens => new KindergartensLoaded(kindergartens))
  );

  @Effect()
  loadChild$ = this.actions$.pipe(
    ofType<LoadChild>(KgaActionTypes.LoadChild),
    mergeMap(({ id }) => this.endpoint.post<ChildModel>('kga/child/findOne/', { id })),
    map(childModel => new ChildLoaded(childModel))
  );

  @Effect()
  loadEmployee$ = this.actions$.pipe(
    ofType<LoadEmployee>(KgaActionTypes.LoadEmployee),
    mergeMap(({ id }) => this.endpoint.post<EmployeeModel>('kga/employee/findOne/', { id })),
    map(employeeModel => new EmployeeLoaded(employeeModel))
  );

  @Effect()
  loadEmployees$ = this.actions$.pipe(
    ofType<LoadEmployees>(KgaActionTypes.LoadEmployees),
    mergeMap(({ id }) =>
      this.endpoint.post<EmployeeModel[]>('/kga/employee/list/byKindergarten', { id })
    ),
    map(employeeList => new EmployeesLoaded(employeeList))
  );

  @Effect({ dispatch: false })
  saveChild$ = this.actions$.pipe(
    ofType<SaveChild>(KgaActionTypes.SaveChild),
    mergeMap(({ child }) => this.endpoint.post('kga/child/save/', child)),
    withLatestFrom(
      this.store.pipe(select(selectLocale)),
      this.store.pipe(select(kgaQuery.getKindergartenId)),
      this.store.pipe(select(kgaQuery.getYearId))
    ),
    map(([response, locale, kindergartenId, yearId]) => {
      const url = `/ng6/${locale}/kga/${kindergartenId}/${yearId}/child/list`;
      return of(this.router.navigateByUrl(url));
    })
  );

  @Effect({ dispatch: false })
  saveEmployee$ = this.actions$.pipe(
    ofType<SaveEmployee>(KgaActionTypes.SaveEmployee),
    mergeMap(({ employee }) => this.endpoint.post('kga/employee/save/', employee)),
    withLatestFrom(
      this.store.pipe(select(selectLocale)),
      this.store.pipe(select(kgaQuery.getKindergartenId)),
      this.store.pipe(select(kgaQuery.getYearId))
    ),
    map(([response, locale, kindergartenId, yearId]) => {
      const url = `/ng6/${locale}/kga/${kindergartenId}/${yearId}/employee/list`;
      return of(this.router.navigateByUrl(url));
    })
  );

  @Effect()
  loadJournalForChild$ = this.actions$.pipe(
    ofType<LoadJournalForChild>(KgaActionTypes.LoadJournalForChild),
    withLatestFrom(this.store.pipe(select(kgaQuery.getChildId))),
    mergeMap(([resp, id]) =>
      this.endpoint.post<ChildJournalEntry[]>('/kga/childJournal/findForChild/', { id })
    ),
    map(employeeList => new JournalForChildLoaded(employeeList))
  );

  @Effect({ dispatch: false })
  saveJournalForChild$ = this.actions$.pipe(
    ofType<SaveJournalForChild>(KgaActionTypes.SaveJournalForChild),
    mergeMap(({ entry }) => this.endpoint.post('kga/childJournal/save', entry)),
    map(resp => this.store.dispatch(new LoadJournalForChild()))
  );

  constructor(
    private actions$: Actions,
    private endpoint: EndpointService,
    private router: Router,
    private store: Store<any>
  ) {}
}
