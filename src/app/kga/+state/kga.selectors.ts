import { KgaState } from './kga.state';
import { createSelector } from '@ngrx/store';

const getKgaState: (state: any) => KgaState = state => state.kga;

const getSelectedChild = createSelector(
  getKgaState,
  kgaState => kgaState.children.selected
);

const getKindergartens = createSelector(
  getKgaState,
  kgaState => kgaState.kindergartens
);

const getKindergartenId = createSelector(
  getKgaState,
  kgaState => kgaState.kindergartenId
);

const getYearId = createSelector(
  getKgaState,
  kgaState => kgaState.yearId
);

const getSelectedEmployee = createSelector(
  getKgaState,
  kgaState => kgaState.employees.selected
);

const getListEmployee = createSelector(
  getKgaState,
  kgaState => kgaState.employees.list
);

const getJournalForChild = createSelector(
  getKgaState,
  kgaState => kgaState.journal.child
);

const getChildId = createSelector(
  getKgaState,
  kgaState => kgaState.childId
);

export const kgaQuery = {
  getKindergartens,
  getSelectedChild,
  getKindergartenId,
  getYearId,
  getSelectedEmployee,
  getListEmployee,
  getJournalForChild,
  getChildId
};
