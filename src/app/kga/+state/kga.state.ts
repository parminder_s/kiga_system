import {
  KgaParentModel,
  ChildModel,
  KindergartenModel,
  EmployeeModel,
  ChildJournalEntry
} from '../../openapi/kga';

export interface KgaState {
  kindergartenId: number;
  yearId: number;
  childId: number;
  kindergartens: Array<KindergartenModel>;
  children: {
    list: Array<ChildModel>;
    selected: {
      children: ChildModel;
      parents: Array<KgaParentModel>;
    };
  };
  employees: {
    list: Array<EmployeeModel>;
    selected: EmployeeModel;
  };
  journal: {
    child: Array<ChildJournalEntry>;
  };
}
