import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EndpointService } from '../../endpoint/endpoint.service';
import { LoaderService } from '../../shared/loader.service';
import { Observable } from 'rxjs';
import { KgaParentModel, KgaGroupModel, IdRequest } from '../../openapi/kga';

@Component({
  selector: 'kiga-list-parent',
  templateUrl: './list-parent.component.html',
  styleUrls: ['./list-parent.component.less']
})
export class ListParentComponent implements OnInit {
  public parentsObservable: Observable<KgaParentModel[]>;
  public parents: KgaGroupModel[];

  public kindergartenId;
  constructor(
    private loader: LoaderService,
    private activatedRoute: ActivatedRoute,
    private endpointService: EndpointService
  ) {}

  ngOnInit() {
    console.log('going to hide');
    this.loader.hide();
    console.log('after hide');
    this.activatedRoute.params.subscribe(param => {
      this.kindergartenId = param['id'];
      let req: IdRequest = { id: this.kindergartenId };
      this.parentsObservable = this.endpointService.post('/kga/parent/list/byKindergarten', req);
      this.parentsObservable.subscribe(data => (this.parents = data));
    });
  }
}
