import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';

import { Observable, of } from 'rxjs';
import { EndpointService } from '../../endpoint/endpoint.service';
import { YearModel } from '../../openapi/kga';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'kiga-edit-year',
  templateUrl: './edit-year.component.html',
  styleUrls: ['./edit-year.component.less']
})
export class EditYearComponent implements OnInit {
  public year$: Observable<YearModel>;
  private kindergartenId: number;
  private yearId: number;

  fields: FormlyFieldConfig[];
  form = new FormGroup({});

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private endpointService: EndpointService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.kindergartenId = param['kindergartenId'];
      this.yearId = param['yearId'];
      if (this.yearId !== undefined) {
        this.year$ = this.endpointService.post('/kga/year/findOne/', { id: this.yearId });
      } else {
        this.year$ = of({ kindergartenId: this.kindergartenId });
      }
    });
    this.fields = [
      {
        className: 'flex-container',
        fieldGroup: [
          {
            key: 'startDate',
            type: 'datepicker',
            templateOptions: {
              type: 'datepicker',
              label: this.translate.instant('KGA_YEAR_START'),
              required: true
            }
          },
          {
            key: 'endDate',
            type: 'datepicker',
            templateOptions: {
              type: 'datepicker',
              label: this.translate.instant('KGA_YEAR_END'),
              required: true
            }
          }
        ]
      }
    ];
  }

  saveYear() {
    const data: YearModel = {
      id: this.yearId,
      kindergartenId: this.kindergartenId,
      startDate: this.form.value.startDate,
      endDate: this.form.value.endDate
    };
    this.endpointService
      .post('/kga/year/save/', data)
      .subscribe(res =>
        this.router.navigate(['../dashboard'], { relativeTo: this.activatedRoute })
      );
  }

  archive(data: YearModel) {
    console.log('doing archive', data);
    this.endpointService
      .post('/kga/year/archive/', { id: data.id })
      .subscribe(res => console.log(res));
  }
}
