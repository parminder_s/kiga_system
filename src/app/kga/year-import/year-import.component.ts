import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EndpointService } from '../../endpoint/endpoint.service';
import { KgaGroupModel, YearModel } from '../../openapi/kga';
import { LoaderService } from '../../shared/loader.service';

@Component({
  selector: 'kiga-year-import',
  templateUrl: './year-import.component.html',
  styleUrls: ['./year-import.component.less'],
  providers: [DatePipe]
})
export class YearImportComponent implements OnInit {
  public years: Observable<any[]>;
  public groups: Observable<KgaGroupModel[]>;

  public value: any;
  public testArray: any[];
  private yearId: number;

  constructor(
    private loader: LoaderService,
    private activatedRoute: ActivatedRoute,
    private endpointService: EndpointService,
    private datePipe: DatePipe
  ) {}
  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.yearId = param['yearId'];
      this.years = this.endpointService
        .post('/kga/year/list/byKindergarten', { id: param['kindergartenId'] })
        .pipe(map(list => this.transformYears(<YearModel[]>list)));
    });
  }

  private transformYears(years: YearModel[]) {
    return years
      .filter(year => year.id != this.yearId)
      .map(year => {
        return {
          id: year.id,
          text:
            this.datePipe.transform(year.startDate, 'dd.MM.yyyy') +
            '-' +
            this.datePipe.transform(year.endDate, 'dd.MM.yyyy')
        };
      });
  }

  reloadGroups(value) {
    this.groups = this.endpointService.post('/kga/group/list/byYear', {
      id: value[0].value
    });
    console.log(this.groups);
  }

  import(value) {
    console.log('import', value);
    let groupIds = Object.keys(value)
      .filter(key => value[key] === true)
      .map(id => +id);
    console.log('doing import:', groupIds);
    console.log('doing import:', this.yearId);
    //todo type for request?!?!
    this.groups = this.endpointService.post('/kga/kgagroup/import', {
      yearId: this.yearId,
      groupIds: groupIds
    });
  }
}
