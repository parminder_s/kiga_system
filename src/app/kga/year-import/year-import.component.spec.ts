import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YearImportComponent } from './year-import.component';

describe('YearImportComponent', () => {
  let component: YearImportComponent;
  let fixture: ComponentFixture<YearImportComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [YearImportComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(YearImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
