import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { LoadChild, SaveChild, SetKindergartenId, SetYearId } from '../+state/kga.actions';
import { kgaQuery } from '../+state/kga.selectors';
import { ChildModel, CountryViewModel } from '../../openapi/kga';
import { localeSelector } from '../../base-data/base-data.selectors';
import { map } from 'rxjs/operators';
import { EndpointService } from '../../endpoint/endpoint.service';

@Component({
  templateUrl: 'edit-child-container.component.html'
})
export class EditChildContainerComponent implements OnInit {
  public child$: Observable<ChildModel>;
  public locale$: Observable<string>;
  public languages$: Observable<any>;
  public kindergartenId: number;

  constructor(
    private store: Store<any>,
    private activatedRoute: ActivatedRoute,
    private endpointService: EndpointService,
    private router: Router
  ) {}

  ngOnInit() {
    this.languages$ = this.endpointService.post<CountryViewModel[]>('/kga/language/list');

    this.activatedRoute.params.subscribe(param => {
      console.log(param);
      this.locale$ = this.store.pipe(select(localeSelector));
      this.store.dispatch(new SetKindergartenId(param['kindergartenId']));
      this.store.dispatch(new SetYearId(param['yearId']));
      if (param.childId !== 'new') {
        this.store.dispatch(new LoadChild(parseInt(param.childId, 10)));
        this.child$ = this.store.pipe(select(kgaQuery.getSelectedChild));
      } else {
        this.child$ = of({ kindergartenId: param['kindergartenId'], withMeal: false });
      }
    });
  }

  saveChild(child: ChildModel) {
    this.store.dispatch(new SaveChild(child));
  }
}
