import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChildContainerComponent } from './edit-child-container.component';

describe('EditChildContainerComponent', () => {
  let component: EditChildContainerComponent;
  let fixture: ComponentFixture<EditChildContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditChildContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChildContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
