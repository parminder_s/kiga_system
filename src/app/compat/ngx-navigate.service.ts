import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { INgxNavigateService } from '../../../app/src/misc/js/services/AngularInterfaces';
import { ngJsToNgUrl } from './ngJsToNgUrl';

/**
 * service that can be used to navigate between AngularJS and Angular.
 */

@Injectable({
  providedIn: 'root'
})
export class NgxNavigateService implements INgxNavigateService {
  constructor(private router: Router) {}

  navigateByUrl(url) {
    this.router.navigateByUrl(ngJsToNgUrl(url));
  }
}
