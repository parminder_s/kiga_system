export function ngJsToNgUrl(angularJsUrl: string) {
  return angularJsUrl.replace(/^\/ng\//, '/');
}
