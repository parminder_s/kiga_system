import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { LocaleInfoService } from '../base-data/locale-info.service';
import { Store, select } from '@ngrx/store';
import { BaseDataState } from '../base-data/base-data.state';
import { ILinksService, Link } from '../../../app/src/misc/js/services/AngularInterfaces';

@Injectable({
  providedIn: 'root'
})
export class LinksService implements ILinksService {
  links: Array<Link> = [];
  private isBrowser: boolean;
  private data$ = new BehaviorSubject<LinksService>(this);
  public isLoaded = false;
  private locale: string;

  constructor(private localeInfo: LocaleInfoService, private store: Store<BaseDataState>) {
    this.store.pipe(select('locale')).subscribe((locale: string) => (this.locale = locale));
  }

  set(links: Array<Link>) {
    this.links = links.map(link => {
      if (link.code === 'home') {
        link.url = `${link.url}${this.locale}`;
      }
      return link;
    });
    this.isLoaded = true;
    this.data$.next(this);
  }

  getTypes() {
    return {
      navbar: this.links.filter(link => link.inNavbar),
      userMenu: this.links.find(link => link.code === 'memberAdmin'),
      mainArticleCategory: this.links.find(link => link.code === 'mainArticleCategory'),
      mainArticleSearch: this.links.find(link => link.code === 'mainArticleSearch'),
      footerPages: this.links.filter(link => link.code === 'footer')
    };
  }

  findByType(type) {
    return this.getTypes()[type];
  }

  findByCode(code: string) {
    return this.links.find(link => link.code === code);
  }

  getObservable(): Observable<LinksService> {
    return this.data$;
  }
}
