import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { IBootstrapper } from '../../../app/src/misc/js/services/AngularInterfaces';
import { BaseDataLoad } from '../base-data/base-data.actions';
import { getBaseDataState } from '../base-data/base-data.selectors';
import { BaseDataState } from '../base-data/base-data.state';
import { BootstrapDataSaverService } from './bootstrap-data-saver.service';

/**
 * This service is required by AngularJS to trigger the bootstrap
 * during the initial loading phase of the website as well, when
 * the user changes the locale.
 *
 * Please note, that bootstrapping and locale changing is fully
 * handled via ngrx in Angular.
 *
 * When AngularJS requests a new bootstrap, we trigger a new
 * action to the ngrx store and create a promise that we return.
 *
 * That promise is resolved, when the store updates its data. We
 * store the resolves per Promise in an own dictionary and once
 * a Promise got resolved, we remove it from there so that we
 * don't have multiple resolves for one Promise.
 **/
@Injectable({
  providedIn: 'root'
})
export class BootstrapperService implements IBootstrapper {
  private promiseDict: { [locale: string]: Promise<BaseDataState> } = {};
  private currentLocale = '';
  private promiseResolvesDict: {
    [locale: string]: ((value: BaseDataState) => void) | boolean;
  } = {};

  constructor(private bootstrapDataSaver: BootstrapDataSaverService, private store: Store<any>) {
    this.store.select(getBaseDataState).subscribe(baseData => this.handleBaseDataChange(baseData));
  }

  public bootstrap(locale: string) {
    if (!this.promiseDict[locale]) {
      this.promiseDict[locale] = new Promise<BaseDataState>(
        resolve => (this.promiseResolvesDict[locale] = resolve)
      );
    }

    if (this.currentLocale !== locale) {
      this.store.dispatch(new BaseDataLoad(locale));
    }

    return this.promiseDict[locale];
  }

  private handleBaseDataChange(baseData: BaseDataState) {
    this.currentLocale = baseData.locale;
    this.bootstrapDataSaver.save(baseData);
    if (this.isBootstrapAlreadyRequestedAndNotResolved(baseData.locale)) {
      this.handleBootstrapWithAlreadyRequestedLocale(baseData);
    }

    if (this.isBootstrapNotYetRequested(baseData.locale)) {
      this.handleBootstrapWithNotYetRequestedLocale(baseData);
    }
  }

  private isBootstrapAlreadyRequestedAndNotResolved(locale: string): boolean {
    return (
      this.promiseResolvesDict[locale] !== undefined && this.promiseResolvesDict[locale] !== false
    );
  }

  private isBootstrapNotYetRequested(locale: string): boolean {
    return this.promiseResolvesDict[locale] === undefined;
  }

  private handleBootstrapWithAlreadyRequestedLocale(baseData: BaseDataState) {
    (<(value: BaseDataState) => void>this.promiseResolvesDict[baseData.locale])(baseData);
    this.promiseResolvesDict[baseData.locale] = false;
  }

  private handleBootstrapWithNotYetRequestedLocale(baseData: BaseDataState) {
    this.promiseDict[baseData.locale] = new Promise(resolve => resolve(baseData));
    this.promiseResolvesDict[baseData.locale] = false;
  }
}
