import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { LocaleInfoService } from '../base-data/locale-info.service';
import { IConfigService, Feature } from '../../../app/src/misc/js/services/AngularInterfaces';

@Injectable({
  providedIn: 'root'
})
export class ConfigService implements IConfigService {
  private configData = {};
  private isLoaded: Promise<void>;
  private loadedResolver: () => void;
  private data$ = new BehaviorSubject<ConfigService>(this);

  public isReady = false;

  constructor(private localeInfo: LocaleInfoService) {
    this.isLoaded = new Promise<void>((resolve, reject) => (this.loadedResolver = resolve));
  }

  getConfigData() {
    return this.configData;
  }

  get(key: string) {
    if (this.configData != null) {
      return this.configData[key];
    } else {
      return null;
    }
  }

  getObservable(): Observable<ConfigService> {
    return this.data$;
  }

  getIsLoadedPromise() {
    return this.isLoaded;
  }

  set(data) {
    this.loadedResolver();
    Object.assign(this.configData, data);
    this.isReady = true;
    this.data$.next(this);
  }

  loadNewData(data) {
    Object.assign(this.configData, data);
    this.data$.next(this);
  }

  getPermissionMode(): string {
    return this.configData['permissionMode'];
  }

  getNgLiveUrl(): string {
    return this.configData['ngLiveUrl'];
  }

  isFeatureEnabled(feature: Feature): boolean {
    return this.get(feature);
  }
}
