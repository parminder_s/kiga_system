import { Injectable } from '@angular/core';
import { LocaleInfoService } from '../base-data/locale-info.service';

@Injectable({
  providedIn: 'root'
})
export class AngularJsStateService {
  constructor(private localeInfo: LocaleInfoService) {}

  getUrl(state: string, params: { [key: string]: any } = {}): string {
    const locale = this.localeInfo.getLocale();
    if (!state.match(/^root\./)) {
      throw new Error(`unknown state: ${state}`);
    }
    if (state === 'root.home') {
      return `/ng6/${locale}/home`;
    }

    let returner = state;
    Object.keys(params).forEach(key => {
      returner = returner.replace(key, params[key]);
    });

    returner = returner
      .replace(/^root\./, `/${locale}/`)
      .replace(/([A-Z])/g, '-$1')
      .replace(/\./g, '/')
      .toLowerCase()
      .replace('/add-to/', '/addTo/');

    if (returner.endsWith('registrierung/pre-selection')) {
      returner = returner.replace('pre-selection', 'preSelection');
    }

    return returner;
  }
}
