import { inject, TestBed } from '@angular/core/testing';
import { BootstrapDataSaverService } from './bootstrap-data-saver.service';

import { BasketItemsService } from '../data/basket-items.service';
import { ConfigService } from './config.service';
import { LinksService } from './links.service';
import { LocaleDataService } from './locale-data.service';
import { MockedCountryService } from '../misc/services/mocked-country.service';
import { BaseDataState } from '../base-data/base-data.state';

describe('BootstrapDataSaverService', () => {
  const linksSet = jest.fn();
  const configSet = jest.fn();
  const mockedCountry = { countries: '' };
  const localeSet = jest.fn();
  const setNumberOfItems = jest.fn();

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BootstrapDataSaverService,
        { provide: LinksService, useValue: { set: linksSet } },
        { provide: ConfigService, useValue: { set: configSet } },
        { provide: MockedCountryService, useValue: mockedCountry },
        { provide: LocaleDataService, useValue: { set: localeSet } },
        { provide: BasketItemsService, useValue: { setNumberOfItems } }
      ]
    });
  });

  /*
  it(
    'should be created',
    inject([BootstrapDataSaverService], (service: BootstrapDataSaverService) => {
      const bootstrapData = {
        links: { all: 'a' },
        config: 'b',
        locale: 'de',
        locales: 'c',
        shopItems: 3
      };
      expect(service.save(<BaseDataState>(<unknown>bootstrapData))).toBe(bootstrapData);
      expect(linksSet.mock.calls[0][0]).toBe('a');
      expect(configSet.mock.calls[0][0]).toBe('b');
      expect(localeSet.mock.calls[0][0]).toBe('de');
      expect(localeSet.mock.calls[0][1]).toBe('c');
      expect(setNumberOfItems.mock.calls[0][0]).toBe(3);
    })
  );*/
});
