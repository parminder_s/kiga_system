import { TestBed, inject } from '@angular/core/testing';
import { Router } from '@angular/router';

import { NgxNavigateService } from './ngx-navigate.service';

describe('NgxNavigateService', () => {
  const navigateByUrl = jest.fn();

  const router = { navigateByUrl };

  beforeEach(() => navigateByUrl.mockClear());

  const getService = (): NgxNavigateService =>
    TestBed.configureTestingModule({
      providers: [
        NgxNavigateService,
        {
          provide: Router,
          useValue: router
        }
      ]
    }).get(NgxNavigateService);

  it('should be created', () => expect(getService()).toBeTruthy());

  it('should replace an ng at the beginning', () => {
    getService().navigateByUrl('/ng/de/home');
    expect(navigateByUrl.mock.calls[0][0]).toBe('/de/home');
  });

  it('it should do no changes if ng is not at the beginning', () => {
    getService().navigateByUrl('/de/ng/home');
    expect(navigateByUrl.mock.calls[0][0]).toBe('/de/ng/home');
  });

  it('it should do no changes if it is not exactly ng', () => {
    getService().navigateByUrl('/ng6/de/home');
    expect(navigateByUrl.mock.calls[0][0]).toBe('/ng6/de/home');
  });
});
