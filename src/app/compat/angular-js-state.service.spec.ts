import { TestBed, inject } from '@angular/core/testing';
import { LocaleInfoService } from '../base-data/locale-info.service';

import { AngularJsStateService } from './angular-js-state.service';

describe('AngularJsStateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AngularJsStateService,
        { provide: LocaleInfoService, useValue: { getLocale: () => 'en' } }
      ]
    });
  });

  it('should return ng home', inject([AngularJsStateService], (service: AngularJsStateService) => {
    expect(service.getUrl('root.home')).toBe('/ng6/en/home');
  }));

  it('should throw an exception when state doesn"t start with root', inject(
    [AngularJsStateService],
    (service: AngularJsStateService) => {
      expect(() => service.getUrl('foobar')).toThrow('unknown state: foobar');
    }
  ));

  it('should translate to AngularJS', inject(
    [AngularJsStateService],
    (service: AngularJsStateService) => {
      expect(service.getUrl('root.planner')).toBe('/en/planner');
    }
  ));

  it('should translate from camel to kebab case', inject(
    [AngularJsStateService],
    (service: AngularJsStateService) => {
      expect(service.getUrl('root.memberArea.favourites')).toBe('/en/member-area/favourites');
    }
  ));

  it('should not kebab case registration', inject(
    [AngularJsStateService],
    (service: AngularJsStateService) => {
      expect(service.getUrl('root.registrierung.preSelection')).toBe(
        '/en/registrierung/preSelection'
      );
    }
  ));

  it('should work with parameters', () => {
    const localeInfo = { getLocale: () => 'en' } as LocaleInfoService;
    const service = new AngularJsStateService(localeInfo);

    expect(service.getUrl('root.planType.addTo.planId', { planType: 'kiga', planId: 5 })).toBe(
      '/en/kiga/addTo/5'
    );
  });
});
