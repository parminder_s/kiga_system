import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ILocaleData, LocaleKeyPair } from '../../../app/src/misc/js/services/AngularInterfaces';

@Injectable({
  providedIn: 'root'
})
export class LocaleDataService implements ILocaleData {
  private currentLocale: string = '';
  private data: Array<LocaleKeyPair>;

  constructor(private translate: TranslateService) {}

  set(locale: string, localeData: Array<LocaleKeyPair>) {
    this.data = localeData;
    this.currentLocale = locale;
    this.translate.use(locale);
    this.translate.setTranslation(locale, localeData);
  }

  get(locale: string): Array<LocaleKeyPair> {
    if (this.currentLocale !== locale) {
      throw new Error(`locale ${locale} request, but current one if ${this.currentLocale}`);
    }

    return this.data;
  }
}
