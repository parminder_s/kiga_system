import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxUrlPipe } from './ngx-url.pipe';
import { downgradeInjectable } from '@angular/upgrade/static';
import { LocaleDataService } from './locale-data.service';
import { LinksService } from './links.service';
import { BootstrapperService } from './bootstrapper.service';
import { ConfigService } from './config.service';
import { NgxNavigateService } from './ngx-navigate.service';
import { BasketItemsService } from '../data/basket-items.service';

try {
  angular
    .module('misc')
    .factory('config', downgradeInjectable(ConfigService) as any)
    .factory('basketItemsService', downgradeInjectable(BasketItemsService) as any)
    .factory('bootstrapper', downgradeInjectable(BootstrapperService) as any)
    .factory('links', downgradeInjectable(LinksService) as any)
    .factory('localeData', downgradeInjectable(LocaleDataService) as any)
    .factory('ngxNavigate', downgradeInjectable(NgxNavigateService) as any);
} catch (e) {}
/**
 * This module stands for compatibility and provides services
 * for the AngularJS instance.
 */
@NgModule({
  imports: [CommonModule],
  declarations: [NgxUrlPipe],
  exports: [NgxUrlPipe]
})
export class CompatModule {}
