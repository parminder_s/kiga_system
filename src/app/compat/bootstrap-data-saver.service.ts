import { Injectable } from '@angular/core';
import { LinksService } from './links.service';
import { LocaleDataService } from './locale-data.service';
import { ConfigService } from './config.service';
import { MockedCountryService } from '../misc/services/mocked-country.service';
import { BaseDataState } from '../base-data/base-data.state';

/**
 * This service saves the data from the bootstrapper to the
 * services of the data module.
 *
 * It exists only for legacy reasons. Angular is managing the
 * state via ngrx.
 */
@Injectable({
  providedIn: 'root'
})
export class BootstrapDataSaverService {
  constructor(
    private linksService: LinksService,
    private configService: ConfigService,
    private localeSetter: LocaleDataService,
    private mockedCountryService: MockedCountryService
  ) {}

  public save(bootstrapData: BaseDataState) {
    this.linksService.set(bootstrapData.links.all);
    this.configService.set(bootstrapData.config);
    this.localeSetter.set(bootstrapData.locale, bootstrapData.locales);
    this.mockedCountryService.countries = bootstrapData.countries;

    return bootstrapData;
  }
}
