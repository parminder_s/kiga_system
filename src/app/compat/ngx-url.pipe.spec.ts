import { AngularJsStateService } from './angular-js-state.service';
import { NgxUrlPipe } from './ngx-url.pipe';
import { TestBed, inject } from '@angular/core/testing';

describe('NgxUrlPipe', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        NgxUrlPipe,
        { provide: AngularJsStateService, useValue: { getUrl: () => 'foobar' } }
      ]
    });
  });

  it('should call stateService', inject([NgxUrlPipe], (pipe: NgxUrlPipe) => {
    expect(pipe.transform('something')).toBe('foobar');
  }));
});
