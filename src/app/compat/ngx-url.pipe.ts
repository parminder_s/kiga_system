import { Pipe, PipeTransform } from '@angular/core';
import { AngularJsStateService } from './angular-js-state.service';

/**
 * Convert ui-router state name to ngx route url (if updated) or old angularjs route url
 */
@Pipe({
  name: 'ngxUrl'
})
export class NgxUrlPipe implements PipeTransform {
  constructor(private angularJsStateService: AngularJsStateService) {}

  transform(value: string, args?: any): string {
    return this.angularJsStateService.getUrl(value, args);
  }
}
