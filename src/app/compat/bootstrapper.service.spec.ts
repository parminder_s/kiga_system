import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { of } from 'rxjs/observable/of';
import { BootstrapDataSaverService } from './bootstrap-data-saver.service';
import { BootstrapperService } from './bootstrapper.service';
import { BaseDataLoad } from '../base-data/base-data.actions';
import { Observable } from 'rxjs';

describe('BootstrapperService', () => {
  const save = jest.fn();
  const bootstrapDataSaver = { save };
  const dispatch = jest.fn();

  const getService = (observable: Observable<any>) => {
    save.mockClear();
    return TestBed.configureTestingModule({
      providers: [
        BootstrapperService,
        {
          provide: Store,
          useValue: { dispatch: dispatch, select: () => observable }
        },
        {
          provide: BootstrapDataSaverService,
          useValue: bootstrapDataSaver
        }
      ]
    }).get(BootstrapperService);
  };

  it('should return the data when already available', () => {
    const baseData = { foo: 'bar' };
    const service = getService(of(baseData));
  });

  it('should call the finisher with the value from the observable', async () => {});

  it('should call the endpoint per locale only once', async () => {});

  it('should call the old endpoint', () => {});

  it('should call the new endpoint', () => {});
});
