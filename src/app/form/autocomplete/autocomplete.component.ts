import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatAutocompleteTrigger, MatInput } from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { FieldType } from '@ngx-formly/material';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'kiga-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.less']
})
export class AutocompleteComponent extends FieldType implements OnInit, AfterViewInit {
  @ViewChild(MatInput) formFieldControl: MatInput;
  @ViewChild(MatAutocompleteTrigger) autocomplete: MatAutocompleteTrigger;

  filteredOptions: Observable<AutocompleteElement[]>;
  values: AutocompleteElement[];
  filterFormControl: FormControl;

  ngOnInit() {
    this.filterFormControl = new FormControl();
    this.filterFormControl.validator = this.formControl.validator;
    super.ngOnInit();
    this.to.autocompleteOptions.subscribe(valueOptions => {
      this.values = valueOptions;
      if (this.formControl.value !== null) {
        console.log(this.formControl.value);
        this.filterFormControl.setValue(
          this.values.filter(
            autocompleteValue => autocompleteValue.value === this.formControl.value
          )[0].label
        );
      }
      this.filteredOptions = this.filterFormControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    });
  }

  private _filter(value: string): AutocompleteElement[] {
    const filterValue = value.toLowerCase();
    const returner = this.values
      .filter(option => option.label.toLowerCase().includes(filterValue))
      .map(entryFromInput => new AutocompleteElement(entryFromInput.value, entryFromInput.label));
    //one is selected, setting value in the actual formControl.
    returner.forEach(labelValue => {
      if (labelValue.toLowerCase() === filterValue) this.formControl.setValue(returner[0].value);
    });
    return returner;
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    // temporary fix for https://github.com/angular/material2/issues/6728
    (<any>this.autocomplete)._formField = this.formField;
  }
}

class AutocompleteElement {
  constructor(public value: string, public label: string) {}

  public toString(): string {
    return this.label;
  }

  public toLowerCase() {
    return this.label.toLowerCase();
  }
}
