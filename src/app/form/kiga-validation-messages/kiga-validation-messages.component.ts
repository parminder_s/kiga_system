import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'kiga-validation-messages',
  templateUrl: './kiga-validation-messages.component.html',
  styleUrls: ['./kiga-validation-messages.component.less']
})
export class KigaValidationMessagesComponent implements OnInit {
  @Input() control: AbstractControl;

  public required: boolean;
  public minlength: boolean;
  public maxlength: boolean;
  public max: boolean;
  public min: boolean;

  public errorExist: boolean;

  constructor() {}

  ngOnInit() {
    this.control.valueChanges.subscribe(val => {
      console.log(val);
      this.checkHide();
    });
  }

  checkHide() {
    if (this.control.errors == null) {
      this.required = false;
      this.minlength = false;
      this.maxlength = false;
      this.max = false;
      this.min = false;
    } else {
      this.required = this.control.errors['required'] !== undefined;
      this.minlength = this.control.errors['minlength'] !== undefined;
      this.maxlength = this.control.errors['maxlength'] !== undefined;
      this.max = this.control.errors['max'] !== undefined;
      this.min = this.control.errors['min'] !== undefined;
    }
    this.errorExist = this.required || this.minlength || this.maxlength || this.min || this.max;
  }
}
