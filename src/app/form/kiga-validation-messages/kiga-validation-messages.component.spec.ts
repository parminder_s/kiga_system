import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KigaValidationMessagesComponent } from './kiga-validation-messages.component';

describe('KigaValidationMessagesComponent', () => {
  let component: KigaValidationMessagesComponent;
  let fixture: ComponentFixture<KigaValidationMessagesComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [KigaValidationMessagesComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(KigaValidationMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
