import { Component, OnInit } from '@angular/core';
import { SkipSelf } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { AbstractKigaInputComponent } from '../abstract-kiga-input/abstract-kiga-input.component';

@Component({
  selector: 'kiga-textarea',
  templateUrl: './kiga-textarea.component.html',
  styleUrls: ['./kiga-textarea.component.less']
})
export class KigaTextareaComponent extends AbstractKigaInputComponent {
  type = 'textarea';

  constructor(@SkipSelf() protected parent: FormGroupDirective) {
    super(parent);
  }
}
