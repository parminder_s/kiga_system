import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KigaTextareaComponent } from './kiga-textarea.component';

describe('KigaTextareaComponent', () => {
  let component: KigaTextareaComponent;
  let fixture: ComponentFixture<KigaTextareaComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [KigaTextareaComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(KigaTextareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
