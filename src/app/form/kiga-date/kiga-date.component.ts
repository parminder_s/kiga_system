import { Component, Input } from '@angular/core';
import { AbstractKigaInputComponent } from '../abstract-kiga-input/abstract-kiga-input.component';
import { FormGroupDirective } from '@angular/forms';
import { SkipSelf } from '@angular/core';

@Component({
  selector: 'kiga-date',
  templateUrl: './kiga-date.component.html',
  styleUrls: ['./kiga-date.component.less']
})
export class KigaDateComponent extends AbstractKigaInputComponent {
  //see https://valor-software.com/ngx-bootstrap/#/datepicker
  @Input() public config: any;

  constructor(@SkipSelf() protected parent: FormGroupDirective) {
    super(parent);
  }

  ngOnInit() {
    if (this.config === undefined) {
      this.config = {};
    }
    this.config.containerClass = 'theme-kiga';
    super.ngOnInit();
  }

  onUpdate(value) {
    if (value instanceof Date) {
      this.formControl.setValue(value);
    } else {
      //assuming it is a string
      this.formControl.setValue(new Date(value));
    }
  }
}
