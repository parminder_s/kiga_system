import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KigaDateComponent } from './kiga-date.component';

describe('KigaDateComponent', () => {
  let component: KigaDateComponent;
  let fixture: ComponentFixture<KigaDateComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [KigaDateComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(KigaDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
