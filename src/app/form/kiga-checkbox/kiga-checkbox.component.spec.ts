import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KigaCheckboxComponent } from './kiga-checkbox.component';

describe('KigaCheckboxComponent', () => {
  let component: KigaCheckboxComponent;
  let fixture: ComponentFixture<KigaCheckboxComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [KigaCheckboxComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(KigaCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
