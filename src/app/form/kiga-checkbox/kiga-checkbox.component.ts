import { Component, OnInit } from '@angular/core';
import { AbstractKigaInputComponent } from '../abstract-kiga-input/abstract-kiga-input.component';
import { FormGroupDirective } from '@angular/forms';
import { SkipSelf } from '@angular/core';
import { MatCheckboxChange } from '@angular/material';

@Component({
  selector: 'kiga-checkbox',
  templateUrl: './kiga-checkbox.component.html',
  styleUrls: ['./kiga-checkbox.component.less']
})
export class KigaCheckboxComponent extends AbstractKigaInputComponent {
  type = 'checkbox';
  constructor(@SkipSelf() public parent: FormGroupDirective) {
    super(parent);
  }

  ngOnInit() {
    super.ngOnInit();
    this.formControl.valueChanges.subscribe(val => {
      if (val === false) {
        this.formControl.setValue(undefined);
      }
    });
  }
}
