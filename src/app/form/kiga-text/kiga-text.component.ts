import { Component, Host, Input, OnInit, Optional, SkipSelf } from '@angular/core';
import { AbstractControl, FormGroupDirective } from '@angular/forms';
import { AbstractKigaInputComponent } from '../abstract-kiga-input/abstract-kiga-input.component';

@Component({
  selector: 'kiga-text',
  templateUrl: './kiga-text.component.html',
  styleUrls: ['./kiga-text.component.less']
})
export class KigaTextComponent extends AbstractKigaInputComponent {
  type = 'text';

  constructor(@SkipSelf() protected parent: FormGroupDirective) {
    super(parent);
  }
}
