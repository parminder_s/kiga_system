import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KigaTextComponent } from './kiga-text.component';

describe('KigaTextComponent', () => {
  let component: KigaTextComponent;
  let fixture: ComponentFixture<KigaTextComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [KigaTextComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(KigaTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
