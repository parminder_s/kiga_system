import { Component, OnInit } from '@angular/core';
import { AbstractKigaInputComponent } from '../abstract-kiga-input/abstract-kiga-input.component';
import { SkipSelf } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'kiga-number',
  templateUrl: './kiga-number.component.html',
  styleUrls: ['./kiga-number.component.less']
})
export class KigaNumberComponent extends AbstractKigaInputComponent {
  type = 'number';

  constructor(@SkipSelf() protected parent: FormGroupDirective) {
    super(parent);
  }
}
