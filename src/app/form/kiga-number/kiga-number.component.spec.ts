import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KigaNumberComponent } from './kiga-number.component';

describe('KigaNumberComponent', () => {
  let component: KigaNumberComponent;
  let fixture: ComponentFixture<KigaNumberComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [KigaNumberComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(KigaNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
