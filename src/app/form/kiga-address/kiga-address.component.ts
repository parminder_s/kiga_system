import { Component, Self, SkipSelf } from '@angular/core';
import { AbstractKigaInputComponent } from '../abstract-kiga-input/abstract-kiga-input.component';
import { FormGroupDirective, FormBuilder } from '@angular/forms';
import { Input } from '@angular/core';
import { KigaAddress } from '../../openapi/kga';

@Component({
  selector: 'kiga-address',
  templateUrl: './kiga-address.component.html',
  styleUrls: ['./kiga-address.component.less']
})
export class KigaAddressComponent extends AbstractKigaInputComponent {
  value: any;
  type = 'address';

  @Input() public countries;

  constructor(private formBulder: FormBuilder, @SkipSelf() protected parent: FormGroupDirective) {
    super(parent);
  }

  ngOnInit() {
    let data: KigaAddress = {};
    let wasDisabled = this.parent.form.disabled;
    if (this.parent.form.controls[this.name] !== undefined) {
      data = this.parent.form.controls[this.name].value;
    }

    this.parent.form.removeControl(this.name);
    this.parent.form.addControl(
      this.name,
      this.formBulder.group({
        street: data.street,
        floor: data.floor,
        doorNumber: data.doorNumber,
        houseNumber: data.houseNumber,
        zipCode: data.zipCode,
        city: data.city,
        countryId: data.countryId
      })
    );

    if (wasDisabled) {
      this.parent.form.disable();
    }
  }
}
