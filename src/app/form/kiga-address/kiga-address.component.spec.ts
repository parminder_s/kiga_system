import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KigaAddressComponent } from './kiga-address.component';

describe('KigaAddressComponent', () => {
  let component: KigaAddressComponent;
  let fixture: ComponentFixture<KigaAddressComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [KigaAddressComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(KigaAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
