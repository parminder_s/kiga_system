import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KigaSelectionComponent } from './kiga-selection.component';

describe('KigaSelectionComponent', () => {
  let component: KigaSelectionComponent;
  let fixture: ComponentFixture<KigaSelectionComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [KigaSelectionComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(KigaSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
