import { Component, OnInit } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { SkipSelf } from '@angular/core';
import { AbstractKigaInputComponent } from '../abstract-kiga-input/abstract-kiga-input.component';
import { Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'kiga-selection',
  templateUrl: './kiga-selection.component.html',
  styleUrls: ['./kiga-selection.component.less']
})
export class KigaSelectionComponent extends AbstractKigaInputComponent {
  type = 'selection';

  items: { id: any; text: string }[];

  @Input('options') options: any;
  @Input('placeholder') placeholder: string;

  @Output('onSelectionChange') onSelectionChange: EventEmitter<any> = new EventEmitter<any>();

  constructor(@SkipSelf() protected parent: FormGroupDirective) {
    super(parent);
    this.items = [];
  }

  ngOnInit() {
    super.ngOnInit();
    if (this.options instanceof Observable) {
      this.options.subscribe(elements => this.createItems(elements));
    } else if (this.options !== undefined && this.options !== null) {
      this.createItems(this.options);
    }
  }

  private createItems(elements) {
    console.log(elements);
    if (elements instanceof Array) {
      this.items = elements;
    } else {
      Object.keys(elements).forEach(key => this.items.push({ id: key, text: elements[key] }));
    }
  }

  change(val) {
    this.onSelectionChange.emit(val);
  }
}
