import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KigaFormComponent } from './kiga-form.component';

describe('KigaFormComponent', () => {
  let component: KigaFormComponent;
  let fixture: ComponentFixture<KigaFormComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [KigaFormComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(KigaFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
