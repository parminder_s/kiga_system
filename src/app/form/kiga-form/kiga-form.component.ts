import { Component, EventEmitter, Input, OnInit, Output, Self } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective } from '@angular/forms';
import { Observable } from 'rxjs';
import { ChildModel } from '../../openapi/kga';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';

@Component({
  selector: 'kiga-form',
  templateUrl: './kiga-form.component.html',
  styleUrls: ['./kiga-form.component.less'],
  providers: [FormGroupDirective]
})
export class KigaFormComponent implements OnInit {
  @Input() model: any;
  @Input() options: FormlyFormOptions = {};
  @Input() fields: FormlyFieldConfig[];

  @Output() onSubmit = new EventEmitter<any>();

  form = new FormGroup({});

  constructor() {}

  ngOnInit() {}

  submit(data) {
    if (this.form.invalid) {
      // this.scrollToError();
    } else {
      this.onSubmit.emit(data);
    }
  }
}
