import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KigaTextComponent } from './kiga-text/kiga-text.component';
import { KigaFormComponent } from './kiga-form/kiga-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { KigaButtonComponent } from './kiga-button/kiga-button.component';
import { AbstractKigaInputComponent } from './abstract-kiga-input/abstract-kiga-input.component';
import { KigaValidationMessagesComponent } from './kiga-validation-messages/kiga-validation-messages.component';
import { KigaNumberComponent } from './kiga-number/kiga-number.component';
import { KigaSelectionComponent } from './kiga-selection/kiga-selection.component';
import { KigaCheckboxComponent } from './kiga-checkbox/kiga-checkbox.component';
import { NgxSelectModule } from 'ngx-select-ex';
import { KigaTextareaComponent } from './kiga-textarea/kiga-textarea.component';
import { KigaDateComponent } from './kiga-date/kiga-date.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { KigaAddressComponent } from './kiga-address/kiga-address.component';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginator,
  MatPaginatorModule,
  MatRadioModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { FormlyMatDatepickerModule } from '@ngx-formly/material/datepicker';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { FileUploadModule } from 'ng2-file-upload';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FileUploadModule,
    TranslateModule,
    BsDatepickerModule.forRoot(),
    FormlyModule.forRoot({
      validators: [],
      types: [
        {
          name: 'autocomplete',
          component: AutocompleteComponent,
          wrappers: ['form-field']
        },
        {
          name: 'file-upload',
          component: FileUploadComponent,
          wrappers: ['form-field']
        }
      ],
      validationMessages: [{ name: 'required', message: 'This field is required' }]
    }),
    ReactiveFormsModule,
    FormlyMaterialModule,
    FormlyMatDatepickerModule,
    MatSelectModule,
    MatNativeDateModule,
    MatSortModule,
    MatFormFieldModule,
    MatButtonModule,
    MatRadioModule,
    MatInputModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatTableModule,
    NgxSelectModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatInputModule,
    MatFormFieldModule
  ],
  declarations: [
    KigaTextComponent,
    KigaFormComponent,
    KigaButtonComponent,
    AbstractKigaInputComponent,
    KigaValidationMessagesComponent,
    KigaNumberComponent,
    KigaSelectionComponent,
    KigaCheckboxComponent,
    KigaTextareaComponent,
    KigaDateComponent,
    KigaAddressComponent,
    AutocompleteComponent,
    FileUploadComponent
  ],
  exports: [
    KigaTextComponent,
    KigaFormComponent,
    KigaButtonComponent,
    KigaNumberComponent,
    KigaSelectionComponent,
    KigaCheckboxComponent,
    KigaTextareaComponent,
    KigaDateComponent,
    KigaAddressComponent
  ]
})
export class FormModule {}
