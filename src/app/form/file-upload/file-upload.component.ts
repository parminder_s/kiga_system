import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FileItem, FileUploader, ParsedResponseHeaders } from 'ng2-file-upload';
import { FieldType } from '@ngx-formly/material';

@Component({
  selector: 'kiga-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.less']
})
export class FileUploadComponent extends FieldType implements OnInit {
  @ViewChild('fileInputElement') inputElement: ElementRef;

  public uploader: FileUploader;

  //TODO add support for other parameter (context and accessLists).
  constructor() {
    super();
  }

  ngOnInit() {
    if (this.formControl.value == undefined) {
      this.formControl.setValue([]);
    }
    this.uploader = new FileUploader({
      url: window['kiga'].endPointUrl + 'kiga-upload/push',
      additionalParameter: { cannedAccessControlList: this.to.permission },
      autoUpload: true
    });
    this.uploader.onBuildItemForm = (fileItem, form) => {
      fileItem.alias = 'files';
      if (this.to.permission !== undefined) {
        form.cannedAccessControlList = this.to.permission;
      }
      console.log(form);
      return { fileItem, form };
    };
    this.uploader.onSuccessItem = (
      item: FileItem,
      response: string,
      status: number,
      headers: ParsedResponseHeaders
    ) => {
      const resp = JSON.parse(response);
      resp.uploadedFiles.forEach(val => {
        this.formControl.value.push({ id: val.id, url: '', name: val.name });
      });
    };
  }

  onFileSelected(data: FileList) {
    //this.uploader.autoUpload(data.item(0));
  }

  btnClicked($event) {
    let el: HTMLElement = this.inputElement.nativeElement as HTMLElement;
    el.click();
  }

  removeFile(file) {
    const index = this.formControl.value.indexOf(file, 0);
    if (index > -1) {
      this.formControl.value.splice(index, 1);
    }
  }

  openFile(file) {
    this.to.fileClicked(file);
  }
}
