export type CannedAccessControlList =
  | 'AuthenticatedRead'
  | 'AwsExecRead'
  | 'BucketOwnerFullControl'
  | 'GRAND_MOTHER'
  | 'SIGNIFICANT_OTHER'
  | 'OTHER';

export const CannedAccessControlList = {
  AuthenticatedRead: 'AuthenticatedRead' as CannedAccessControlList,
  AwsExecRead: 'AwsExecRead' as CannedAccessControlList,
  BucketOwnerFullControl: 'BucketOwnerFullControl' as CannedAccessControlList,
  BucketOwnerRead: 'BucketOwnerRead' as CannedAccessControlList,
  LogDeliveryWrite: 'LogDeliveryWrite' as CannedAccessControlList,
  Private: 'Private' as CannedAccessControlList,
  PublicRead: 'PublicRead' as CannedAccessControlList,
  PublicReadWrite: 'PublicReadWrite' as CannedAccessControlList
};
