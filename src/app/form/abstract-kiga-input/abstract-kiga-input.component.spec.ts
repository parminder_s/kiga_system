import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbstractKigaInputComponent } from './abstract-kiga-input.component';

describe('AbstractKigaInputComponent', () => {
  let component: AbstractKigaInputComponent;
  let fixture: ComponentFixture<AbstractKigaInputComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [AbstractKigaInputComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractKigaInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
