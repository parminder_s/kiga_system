import { Component, Input, OnInit, SkipSelf } from '@angular/core';
import { AbstractControl, FormGroupDirective } from '@angular/forms';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'kiga-abstract-kiga-input',
  templateUrl: './abstract-kiga-input.component.html',
  styleUrls: ['./abstract-kiga-input.component.less']
})
export class AbstractKigaInputComponent implements OnInit {
  @Input() name: string;
  @Input() type: string;
  @Input() label: string;

  @Input() public disabled: boolean = false;

  formControl: AbstractControl;

  constructor(@SkipSelf() protected parent: FormGroupDirective) {}

  //call this in every inherited class
  ngOnInit() {
    if (this.name == undefined) {
      console.error('Name is undefined for kiga input component!');
    }
    //only for storybook, to prevent erros
    if (this.parent.form === null) {
      this.parent.form = new FormGroup({});
    }
    if (this.disabled || this.parent.form.disabled) {
      this.disabled = true;
    }
    let parts = this.name.split('.');
    if (parts.length === 1) {
      this.formControl = this.parent.form.controls[this.name];
      if (this.formControl === undefined) {
        this.formControl = new FormControl({ value: undefined, disabled: this.disabled });
        this.parent.form.addControl(this.name, this.formControl);
        if (this.disabled) {
          this.formControl.disable();
          this.parent.form.disable();
        }
      }
    } else if (parts.length === 2) {
      //if the name contains a dot we assume the abstract control is a formGroup.
      let formGroup = <FormGroup>this.parent.form.controls[parts[0]];
      this.formControl = formGroup.controls[parts[1]];
      if (this.disabled) {
        this.formControl.disable();
      }
    }
  }
}
