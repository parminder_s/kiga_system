import { Component, EventEmitter, Input, Output, SkipSelf } from '@angular/core';
import { AbstractKigaInputComponent } from '../abstract-kiga-input/abstract-kiga-input.component';
import { FormGroupDirective } from '@angular/forms';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'kiga-button',
  templateUrl: './kiga-button.component.html',
  styleUrls: ['./kiga-button.component.less']
})
export class KigaButtonComponent extends AbstractKigaInputComponent {
  type = 'button';

  //todo check if this approach (with ng class)
  //via provider from parent?!?!
  @Input() scheme;
  @Output('onClick') onClick: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  constructor(@SkipSelf() protected parent: FormGroupDirective) {
    super(parent);
  }

  public click() {
    this.onClick.emit(this.parent.form.value);
  }

  ngOnInit() {}
}
