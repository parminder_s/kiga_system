import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KigaButtonComponent } from './kiga-button.component';

describe('KigaButtonComponent', () => {
  let component: KigaButtonComponent;
  let fixture: ComponentFixture<KigaButtonComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [KigaButtonComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(KigaButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
