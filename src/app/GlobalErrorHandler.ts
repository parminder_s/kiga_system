import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(private injector: Injector) {}
  handleError(error) {
    const snackBar = this.injector.get(MatSnackBar);
    const translateService = this.injector.get(TranslateService);
    forkJoin([
      translateService.get('EXCEPTION_GENERAL'),
      translateService.get('BUTTON_OK')
    ]).subscribe(translateResult => {
      snackBar.open(translateResult[0], translateResult[1]);
    });
    // IMPORTANT: Rethrow the error otherwise it gets swallowed
    throw error;
  }
}
