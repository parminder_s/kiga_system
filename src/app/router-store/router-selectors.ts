import { createSelector } from '@ngrx/store';
import { RouterState } from './router.reducer';

const selectRouter: (state: any) => RouterState = state => state.router;

export const selectRouterStateUrl = createSelector(
  selectRouter,
  state => state.state
);

export const selectSplittedUrl = createSelector(
  selectRouterStateUrl,
  state => state.url.split('/').slice(1)
);
export const selectLocale = createSelector(
  selectSplittedUrl,
  (segments): string => (segments.length >= 2 ? segments[1] : '')
);
export const selectUrlSegments = createSelector(
  selectSplittedUrl,
  (segments): Array<string> => (segments.length >= 3 ? segments.slice(2) : [])
);
export const selectQueryParams = createSelector(
  selectRouterStateUrl,
  state => state.queryParams
);
