import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NavigationEnd, Router, RouterStateSnapshot, RouterEvent } from '@angular/router';
import { Store, StoreModule } from '@ngrx/store';
import { of } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { CustomSerializer } from './custom-serializer';
import { LoadRouters, RouteNavigationEnded } from './router.actions';
import { reducer } from './router.reducer';
import { RouterEffect } from './router-effects';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('router', reducer),
    EffectsModule.forFeature([RouterEffect])
  ],
  declarations: []
})
export class RouterStoreModule {
  constructor(store: Store<any>, router: Router) {
    const serializer = new CustomSerializer();
    (<any>router).hooks.beforePreactivation = (routerState: RouterStateSnapshot) => {
      store.dispatch(new LoadRouters(serializer.serialize(routerState)));
      return of(true);
    };
    router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        tap(event => store.dispatch(new RouteNavigationEnded((<RouterEvent>event).url)))
      )
      .subscribe();
  }
}
