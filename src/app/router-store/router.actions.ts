import { Action } from '@ngrx/store';
import { RouterStateUrl } from './router.reducer';

export enum RouterActionTypes {
  LOAD_ROUTERS = '[Router] Load Routers',
  ROUTE_NAVIATION_ENDED = '[Router] Navigation Ended'
}

export class LoadRouters implements Action {
  readonly type = RouterActionTypes.LOAD_ROUTERS;
  constructor(public payload: RouterStateUrl) {}
}

export class RouteNavigationEnded implements Action {
  readonly type = RouterActionTypes.ROUTE_NAVIATION_ENDED;
  constructor(public url: string) {}
}

export type RouterActions = LoadRouters | RouteNavigationEnded;
