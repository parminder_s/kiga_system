import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { AnalyticsNotifierService } from '../analytics-notifier.service';
import { RouteNavigationEnded, RouterActionTypes } from './router.actions';

@Injectable({ providedIn: 'root' })
export class RouterEffect {
  @Effect({ dispatch: false })
  routeNavigationEnded$ = this.actions$.pipe(
    ofType<RouteNavigationEnded>(RouterActionTypes.ROUTE_NAVIATION_ENDED),
    tap(({ url }) => this.analyticsNotifier.notify(url))
  );

  constructor(private actions$: Actions, private analyticsNotifier: AnalyticsNotifierService) {}
}
