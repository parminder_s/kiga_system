import { RouterStateUrl } from './router.reducer';
import { RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';

/**
 * with @ngrx/router-store use it with interface RouterStateSerializer<RouterStateUrl>
 */
export class CustomSerializer {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    let route = routerState.root;

    while (route.firstChild) {
      route = route.firstChild;
    }

    const {
      root: { queryParams }
    } = routerState;
    const { params } = route;
    const url = routerState.url.replace(/\?.*$/, '');

    return { url, params, queryParams, hasNavigationEnded: false };
  }
}
