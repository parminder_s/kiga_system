import { Params } from '@angular/router';
import { RouterActions, RouterActionTypes } from './router.actions';

/**
 * The hasNavigationEnded property is required by the cms component. It has a catch-all
 * url and has to be informed on any change.
 */
export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
  hasNavigationEnded: boolean;
}
export interface RouterState {
  state: RouterStateUrl;
}

export const initialState: RouterState = {
  state: {
    url: '/',
    params: null,
    queryParams: null,
    hasNavigationEnded: false
  }
};

export function reducer(state = initialState, action: RouterActions): RouterState {
  switch (action.type) {
    case RouterActionTypes.LOAD_ROUTERS: {
      return {
        ...state,
        state: action.payload
      };
    }
    case RouterActionTypes.ROUTE_NAVIATION_ENDED: {
      return {
        ...state,
        state: { ...state.state, hasNavigationEnded: true }
      };
    }
    default:
      return state;
  }
}
