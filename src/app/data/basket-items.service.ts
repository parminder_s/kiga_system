import { Injectable } from '@angular/core';
import { IBasketItemsService } from '../../../app/src/misc/js/services/AngularInterfaces';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BasketItemsService implements IBasketItemsService {
  private numberOfItems = 0;
  private numberOfItems$: BehaviorSubject<number>;

  constructor() {
    this.numberOfItems$ = new BehaviorSubject<number>(this.numberOfItems);
  }

  getNumberOfItems(): number {
    return this.numberOfItems;
  }

  increase(amount: number) {
    this.numberOfItems += amount;
  }

  setNumberOfItems(value: number): void {
    this.numberOfItems$.next(value);
    this.numberOfItems = value;
  }

  getObservable(): Observable<number> {
    return this.numberOfItems$;
  }
}
