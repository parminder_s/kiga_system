export class KigaInitData {
  constructor(
    public endpointUrl: string,
    public legacyUrl: string,
    public newBootstrapEndpointEnabled: boolean,
    public clientUrl: string
  ) {}
}
