import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { downgradeInjectable } from '@angular/upgrade/static';
import { BasketItemsService } from './basket-items.service';

/**
 * Legacy Data module and should be superseeded by ngrx.
 */

try {
  angular
    .module('misc')
    .factory('basketItemsService', downgradeInjectable(BasketItemsService) as any);
} catch (e) {}

@NgModule({
  imports: [CommonModule]
})
export class DataModule {}
