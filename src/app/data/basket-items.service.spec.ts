import { TestBed, inject } from '@angular/core/testing';

import { BasketItemsService } from './basket-items.service';

describe('BasketItemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BasketItemsService]
    });
  });

  it(
    'should be set to 0 by default',
    inject([BasketItemsService], (service: BasketItemsService) => {
      expect(service.getNumberOfItems()).toBe(0);
    })
  );

  it(
    'should add correctly',
    inject([BasketItemsService], (service: BasketItemsService) => {
      service.setNumberOfItems(1);
      service.increase(2);
      expect(service.getNumberOfItems()).toBe(3);
    })
  );
});
