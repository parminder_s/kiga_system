import { LayoutModule } from '@angular/cdk/layout';
import { registerLocaleData } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import {
  APP_ID,
  APP_INITIALIZER,
  ErrorHandler,
  Inject,
  LOCALE_ID,
  NgModule,
  PLATFORM_ID
} from '@angular/core';
import {
  MAT_DATE_LOCALE,
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
  MatSnackBarModule
} from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import {
  BrowserModule,
  BrowserTransferStateModule,
  TransferState
} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, UrlHandlingStrategy, UrlTree } from '@angular/router';
import { UpgradeModule } from '@angular/upgrade/static';
import { ApiModule } from '@kiga/home/api';
import { EffectsModule } from '@ngrx/effects';
import { MetaReducer, Store, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { TranslateModule } from '@ngx-translate/core';
import * as _ from 'lodash';
import { environment } from '../environments/environment';
import { initApplication } from './+state/init-application';
import { MetaEffects } from './+state/meta.effects';
import { NgrxTransferStateEffects } from './+state/ngrx-transfer-state.effects';
import { rehydrate } from './+state/rehydrate';
import { reducers } from './+state/selectors';
import { AppComponent } from './app.component';
import { AreaModule } from './area/area.module';
import { BaseDataModule } from './base-data/base-data.module';
import { DataModule } from './data/data.module';
import { KigaInitData } from './data/KigaInitData';
import { EmptyComponent } from './empty/empty.component';
import { EndpointModule } from './endpoint/endpoint.module';
import { FailureInterceptor } from './endpoint/failure-interceptor';
import { LoaderInterceptor } from './endpoint/loader-interceptor';
import { FavouritesModule } from './favourites/favourites.module';
import { GlobalErrorHandler } from './GlobalErrorHandler';
import { BootstrapperGuardService } from './main/bootstrapper-guard.service';
import { MainModule } from './main/main.module';
import { BaseDataLoadEffect } from './main/store/base-data-load.effect';
import { BaseDataLoadedEffect } from './main/store/base-data.loaded.effect';
import { MiscModule } from './misc/misc.module';
import { RedirectRootComponent } from './redirect-root/redirect-root.component';
import { RouterStoreModule } from './router-store/router-store.module';
import { SharedModule } from './shared/shared.module';

export class CustomHandlingStrategy implements UrlHandlingStrategy {
  shouldProcessUrl(url) {
    const urlString = url.toString();
    return urlString.startsWith('/ng6') || urlString === '/';
  }

  extract(url: UrlTree) {
    const segments = _.get(url, 'root.children.primary.segments', []);
    if (segments[0] && segments[0].path === '!') {
      url.root.children.primary.segments = segments.slice(1);
    }
    return url;
  }

  merge(url, whole) {
    return url;
  }
}

export const metaReducers: MetaReducer<any>[] = [rehydrate];

registerLocaleData(localeDe, 'de-AT', localeDeExtra);

@NgModule({
  declarations: [AppComponent, EmptyComponent, RedirectRootComponent],
  imports: [
    ApiModule,
    AreaModule,
    BrowserModule.withServerTransition({ appId: 'kiga' }),
    BrowserTransferStateModule,
    BaseDataModule,
    DataModule,
    EffectsModule.forRoot([
      BaseDataLoadEffect,
      BaseDataLoadedEffect,
      MetaEffects,
      NgrxTransferStateEffects
    ]),
    EndpointModule,
    FavouritesModule,
    LayoutModule,
    MatMomentDateModule,
    BrowserAnimationsModule,
    MainModule,
    MatSnackBarModule,
    MiscModule,
    RouterModule.forRoot([
      {
        path: 'ng6/:locale',
        canActivate: [BootstrapperGuardService],
        children: [
          { path: 'home', loadChildren: './main/main.module#MainModule' },
          {
            path: 'kga',
            loadChildren: './kga/kga.module#KgaModule'
          },
          {
            path: '**',
            loadChildren: './cms/cms.module#CmsModule'
          }
        ]
      }
    ]),
    RouterStoreModule,
    SharedModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    TranslateModule.forRoot(),
    UpgradeModule
  ],
  providers: [
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    { provide: UrlHandlingStrategy, useClass: CustomHandlingStrategy },
    {
      provide: KigaInitData,
      useFactory: () => {
        const kigaInitData = window['kiga'] as KigaInitData;
        kigaInitData.endpointUrl = (<any>kigaInitData).endPointUrl;
        kigaInitData.legacyUrl = kigaInitData.legacyUrl || '';
        return kigaInitData;
      }
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initApplication,
      multi: true,
      deps: [Store, TransferState]
    },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: FailureInterceptor, multi: true },
    { provide: LOCALE_ID, useValue: 'de-AT' },
    { provide: MAT_DATE_LOCALE, useValue: 'de-AT' },
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'outline' } }
  ],
  entryComponents: [EmptyComponent, RedirectRootComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string
  ) {}
}
