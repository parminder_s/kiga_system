import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { UrlToRouteMapperService } from '../url-to-route-mapper.service';

export interface BlockElement {
  title: string;
  titleShort: string;
  color: string;
  linkUrl: string;
  bigImageUrl: string;
  smallImageUrl: string;
  descriptionTitle: string;
  description: string;
}

@Component({
  selector: 'kiga-home-block',
  templateUrl: './home-block.component.html',
  styleUrls: ['./home-block.component.less']
})
export class HomeBlockComponent implements OnInit {
  @Input() public blockElement: BlockElement;
  public routerLink: string;
  public queryParams: { [key: string]: string };

  constructor(private urlToRouteMapper: UrlToRouteMapperService) {}

  ngOnInit() {}

  ngOnChanges() {
    const routeData = this.urlToRouteMapper.getRouterPart(this.blockElement.linkUrl);
    this.routerLink = routeData.routerLink;
    this.queryParams = routeData.queryParams;
  }
}
