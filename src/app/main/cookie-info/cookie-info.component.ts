import { Component, OnInit } from '@angular/core';
import { LinksService } from '../../compat/links.service';
import { CookieInfoService } from '../cookie-info.service';
import * as _ from 'lodash';

@Component({
  selector: 'kiga-cookie-info',
  templateUrl: './cookie-info.component.html',
  styleUrls: ['./cookie-info.component.less']
})
export class CookieInfoComponent implements OnInit {
  dataPrivacyLink: string;
  show: boolean;

  constructor(private links: LinksService, private cookieInfoService: CookieInfoService) {}

  ngOnInit() {
    this.links.getObservable().subscribe(links => {
      this.dataPrivacyLink = _.get(links.findByCode('dataPrivacy'), 'url', '');
    });

    this.cookieInfoService
      .getObservable()
      .subscribe(cookieUsageAgreed => (this.show = !cookieUsageAgreed));
  }

  public buttonClicked() {
    this.cookieInfoService.setUsageAccepted();
  }
}
