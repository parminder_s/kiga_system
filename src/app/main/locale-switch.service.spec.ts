import { TestBed, inject } from '@angular/core/testing';

import { LocaleSwitchService } from './locale-switch.service';

describe('LocaleSwitchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocaleSwitchService]
    });
  });

  it(
    'should be created',
    inject([LocaleSwitchService], (service: LocaleSwitchService) => {
      expect(service).toBeTruthy();
    })
  );
});
