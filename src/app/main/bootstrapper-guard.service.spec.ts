import { TestBed, inject } from '@angular/core/testing';

import { BootstrapperGuardService } from './bootstrapper-guard.service';

describe('BootstrapperGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BootstrapperGuardService]
    });
  });

  it(
    'should be created',
    inject([BootstrapperGuardService], (service: BootstrapperGuardService) => {
      expect(service).toBeTruthy();
    })
  );
});
