import { Injectable } from '@angular/core';
import IMember from '../../../app/src/misc/permission/js/IMember';

/**
 * this service offers logic on the member object. You could get
 * data about the subscription and if the member is logged in.
 */
@Injectable({
  providedIn: 'root'
})
export class MemberInfoService {
  constructor() {}

  withMember<T>(member: IMember, fn: (infoService: WithMemberInfoService) => T) {
    return fn(new WithMemberInfoService(member));
  }
}

export class WithMemberInfoService {
  constructor(private member: IMember) {}

  public isLoggedIn(): boolean {
    return !this.member.isAnonymous;
  }

  public hasTestSubscription(): boolean {
    return this.member.subscription.group === 'test';
  }

  public hasStandardSubscription(): boolean {
    return this.isCms() || this.member.subscription.group === 'standard';
  }

  public hasExpiredSubscription(): boolean {
    if (this.isCms()) {
      return false;
    } else {
      return this.member.subscription.isExpired;
    }
  }

  public hasFullSubscription(): boolean {
    return this.isCms() || !!this.member.subscription.accessPoolIdeas;
  }

  public hasAccessToNewIdeas(): boolean {
    return this.isCms() || this.member.subscription.accessNewIdeas;
  }

  public hasAccessToPoolIdeas(): boolean {
    return this.isCms() || this.member.subscription.accessPoolIdeas;
  }

  public hasActiveFullSubscription(): boolean {
    return !!this.member.subscription.accessPoolIdeas && !this.member.subscription.isExpired;
  }

  public hasActiveTestSubscription(): boolean {
    return this.member.subscription.group === 'test' && !this.member.subscription.isExpired;
  }

  public hasCancelledSubscription(): boolean {
    return !!this.member.subscription.isCancelled;
  }

  public hasLockedSubscription(): boolean {
    return !!this.member.subscription.isLocked;
  }

  public isCms(): boolean {
    return this.member.isCms;
  }

  public isAnonymous(): boolean {
    return this.member.isAnonymous;
  }

  public isCommunity(): boolean {
    return this.member.subscription.group === 'community';
  }
}
