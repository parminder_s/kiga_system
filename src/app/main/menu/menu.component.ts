import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

export interface MenuItem {
  label: string;
  translateLabel?: boolean;
  link?: string;
  callback?: () => void;
  icon?: string;
  children?: Array<MenuItem>;
  labelClass?: string;
}

@Component({
  selector: 'kiga-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less']
})
export class MenuComponent implements OnInit {
  isOpen = false;
  hasChildren: boolean;

  @Input() menuItem: MenuItem;

  constructor() {}

  ngOnInit() {
    this.hasChildren = !!this.menuItem.children;
  }

  ngOnChanges() {
    this.hasChildren = !!this.menuItem.children;
  }

  showDropdown() {
    this.isOpen = true;
  }

  hideDropdown() {
    this.isOpen = false;
  }

  callback(menuItem: MenuItem) {
    menuItem.callback();
  }
}
