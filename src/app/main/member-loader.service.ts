import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { IMemberLoader } from '../../../app/src/misc/js/services/AngularInterfaces';
import IMember from '../../../app/src/misc/permission/js/IMember';
import { BasketItemsService } from '../data/basket-items.service';
import { EndpointService } from '../endpoint/endpoint.service';
import { BehaviorSubject } from 'rxjs';

/**
 * The member loader loads the member data and provides it in the form
 * of an subject. Further more, it also provides a Promise for the
 * AngularJS application which is used by MemberLoader.ts.
 *
 * The backwards compatibiliy is implemented with the functions getPromise() and
 * invalidate().
 */
@Injectable({
  providedIn: 'root'
})
export class MemberLoaderService implements IMemberLoader {
  static anonymousMember: IMember = {
    isAnonymous: true,
    isCms: false,
    subscription: {},
    kga: { isActive: false },
    shopItems: 0
  } as IMember;
  private member$ = new BehaviorSubject<IMember>(MemberLoaderService.anonymousMember);
  private promise: Promise<IMember> = null;

  public isLoaded = false;

  constructor(private endpoint: EndpointService, private basketItems: BasketItemsService) {
    this.reload();
  }

  get(): Observable<IMember> {
    return this.member$;
  }

  logout() {
    this.endpoint.get('/Security/ajaxLogout', { rawUrl: true }).subscribe(() => {
      this.reload();
    });
  }

  getPromise(): Promise<IMember> {
    if (this.promise == null) {
      this.reload();
    }
    return this.promise;
  }

  invalidate() {
    this.promise = null;
  }

  /**
   * We can here safely wrap it with a Promise, because we always
   * get only one response back.
   */
  reload() {
    this.promise = new Promise<IMember>(resolve =>
      this.endpoint.post<IMember>('main/member', {}, { endpoint: false }).subscribe(member => {
        this.basketItems.setNumberOfItems(member.shopItems);
        this.member$.next(member);
        this.isLoaded = true;
        resolve(member);
      })
    );
  }
}
