import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { BaseDataLoad, BaseDataLoaded, BASE_DATA_LOAD } from '../../base-data/base-data.actions';
import { BaseDataState } from '../../base-data/base-data.state';
import { KigaInitData } from '../../data/KigaInitData';
import { EndpointService } from '../../endpoint/endpoint.service';
import {
  KigaTransferStateService,
  TransferStateType
} from '../../shared/kiga-transfer-state.service';
import { mergeMap, switchMap, map } from 'rxjs/operators';

/**
 * This class loads the legacy bootstrap endpoints as well as the new one (config only)
 * and merge both results into object that is afterwards transformed into the BaseData
 * type of the store's state.
 */
@Injectable({ providedIn: 'root' })
export class BaseDataLoadEffect {
  @Effect()
  loadLocales$: Observable<Action> = this.actions$.pipe(
    ofType<BaseDataLoad>(BASE_DATA_LOAD),
    mergeMap(({ locale }) => {
      const appendix = this.kigaInitData.newBootstrapEndpointEnabled ? '_info' : '';
      const url = `main/bootstrap${appendix}?locale=${locale}`;
      return this.kigaTransferState
        .getOrFetch(TransferStateType.BOOTSTRAP, locale, () =>
          this.endpoint.get<BaseDataState>(url, { endpoint: false }).pipe(
            switchMap((baseData: any) =>
              this.endpoint.get('config').pipe(
                map(config => ({
                  ...baseData,
                  config: { ...baseData.config, ...config }
                }))
              )
            )
          )
        )
        .pipe(
          map(
            ({ config, countries, currentCountry, locale, locales, links, meta }: any) =>
              new BaseDataLoaded({
                config,
                countries,
                currentCountry,
                locale,
                locales,
                links: {
                  all: links,
                  footerPages: links.filter(link => link.code === 'footer'),
                  forum: links.find(link => link.code === 'forum').url,
                  ideaGroupMainContainer: links
                    .find(link => link.code === 'ideaGroupMainContainer')
                    .url.replace('/ng/', '/'),
                  mainArticleCategory: links.find(link => link.code === 'mainArticleCategory'),
                  mainArticleSearch: links.find(link => link.code === 'mainArticleSearch'),
                  navbar: links.filter(link => link.inNavbar),
                  shop: links.find(link => link.code === 'shop').url.replace('/ng/', '/'),
                  userMenu: links.find(link => link.code === 'memberAdmin')
                },
                meta: meta
              })
          )
        );
    })
  );

  constructor(
    private actions$: Actions,
    private endpoint: EndpointService,
    private kigaTransferState: KigaTransferStateService,
    private kigaInitData: KigaInitData
  ) {}
}
