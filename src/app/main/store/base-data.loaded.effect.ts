import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { BASE_DATA_LOADED, BaseDataLoaded } from '../../base-data/base-data.actions';
import { UpdateMetas } from '../../+state/meta.actions';

@Injectable({ providedIn: 'root' })
export class BaseDataLoadedEffect {
  @Effect()
  bootstrapLoaded$: Observable<Action> = this.actions$.pipe(
    ofType(BASE_DATA_LOADED),
    map(({ payload: { locale, locales, meta } }: BaseDataLoaded) => {
      this.translate.use(locale);
      this.translate.setTranslation(locale, locales);
      return new UpdateMetas([
        { name: 'title', content: meta.title },
        { name: 'description', content: meta.description },
        { name: 'keywords', content: meta.keywords }
      ]);
    })
  );

  constructor(private actions$: Actions, private translate: TranslateService) {}
}
