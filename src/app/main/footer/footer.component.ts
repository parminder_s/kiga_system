import { Component, OnInit } from '@angular/core';
import { Link } from '../../../../app/src/misc/js/services/AngularInterfaces';
import { NowService } from '../../shared/now.service';
import { Input } from '@angular/core';
import { CookieInfoService } from '../cookie-info.service';

@Component({
  selector: 'kiga-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent implements OnInit {
  public year: number;
  public locale = 'en';
  public useCookieClassName: boolean = false;

  @Input() links: Array<Link>;

  constructor(private now: NowService, private cookieInfoService: CookieInfoService) {}

  ngOnInit() {
    this.year = this.now.date().getFullYear();

    this.cookieInfoService
      .getObservable()
      .subscribe(cookieAccepted => (this.useCookieClassName = !cookieAccepted));
  }
}
