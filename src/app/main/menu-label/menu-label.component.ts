import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../menu/menu.component';
import { Input } from '@angular/core';

@Component({
  selector: 'kiga-menu-label',
  template:
    '' +
    '<span *ngIf="menuItem.translateLabel" [className]="menuItem.labelClass">{{ menuItem.label | translate }}</span>' +
    '<span *ngIf="!menuItem.translateLabel" [className]="menuItem.labelClass">{{ menuItem.label }}</span>'
})
export class MenuLabelComponent implements OnInit {
  @Input() public menuItem: MenuItem;
  constructor() {}

  ngOnInit() {}
}
