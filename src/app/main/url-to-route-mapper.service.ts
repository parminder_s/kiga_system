import { Injectable } from '@angular/core';
import * as _ from 'lodash';

export interface RouteData {
  routerLink: string;
  queryParams: { [key: string]: string };
}

@Injectable({
  providedIn: 'root'
})
export class UrlToRouteMapperService {
  constructor() {}

  getRouterPart(url: string): RouteData {
    const urlMatcher = url.match(/([^\?]*)\??(.*)/);
    const routerLink: string = urlMatcher[1];
    let queryParams = {};
    if (urlMatcher[2]) {
      queryParams = _.fromPairs(urlMatcher[2].split(/\&/).map(param => param.split('=')));
    }

    return { routerLink: routerLink, queryParams: queryParams };
  }
}
