import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { getAreaState } from '../../area/+state/area.selectors';
import { AreaType } from '../../area/area-type';
import { AreaData } from '../header/header.component';

@Component({
  selector: 'kiga-header-container',
  template: '<kiga-header [areaData]="areaData$ | async"></kiga-header>'
})
export class HeaderContainerComponent implements OnInit {
  public areaData$: Observable<AreaData>;
  constructor(private store: Store<any>) {}

  ngOnInit() {
    this.areaData$ = this.store.pipe(
      select(getAreaState),
      map(areaState => {
        const ranges = { 0: '', 1: '1 - 3', 2: '3 - 5', 4: '5 - 6', 8: '6 - 7' };
        const returner = {
          isHomeActive: false,
          isIdeasActive: false,
          isKgaActive: false,
          ageGroup: areaState.selectedAgeGroup,
          range: ranges[areaState.selectedAgeGroup],
          showLocaleSwitcher: areaState.showLocaleSwitcher
        };
        switch (areaState.area) {
          case AreaType.DEFAULT:
            returner.isHomeActive = true;
            break;
          case AreaType.IDEAS:
            returner.isIdeasActive = true;
            break;
          case AreaType.KGA:
            returner.isKgaActive = true;
            break;
        }

        return returner;
      })
    );
  }
}
