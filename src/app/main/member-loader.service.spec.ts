import { TestBed, inject } from '@angular/core/testing';

import { MemberLoaderService } from './member-loader.service';

describe('MemberLoaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MemberLoaderService]
    });
  });

  it(
    'should be created',
    inject([MemberLoaderService], (service: MemberLoaderService) => {
      expect(service).toBeTruthy();
    })
  );
});
