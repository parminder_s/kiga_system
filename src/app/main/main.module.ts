import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { downgradeInjectable } from '@angular/upgrade/static';
import { TranslateModule } from '@ngx-translate/core';
import { CompatModule } from '../compat/compat.module';
import { SharedModule } from '../shared/shared.module';
import { CookieInfoComponent } from './cookie-info/cookie-info.component';
import { DevMenuComponent } from './dev-menu/dev-menu.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderContainerComponent } from './header-container/header-container.component';
import { HeaderComponent } from './header/header.component';
import { HomeBlockComponent } from './home-block/home-block.component';
import { HomeComponent } from './home/home.component';
import { MemberLoaderService } from './member-loader.service';
import { MenuLabelComponent } from './menu-label/menu-label.component';
import { MenuComponent } from './menu/menu.component';
import { ScrollDirective } from './scroll.directive';
import { HomeContainerComponent } from './home-container/home-container.component';

try {
  angular
    .module('misc')
    .factory('memberLoaderService', downgradeInjectable(MemberLoaderService) as any);
} catch (e) {}

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    CompatModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: HomeContainerComponent
      }
    ])
  ],
  declarations: [
    CookieInfoComponent,
    DevMenuComponent,
    FooterComponent,
    HeaderComponent,
    HeaderContainerComponent,
    HomeBlockComponent,
    HomeComponent,
    MenuComponent,
    MenuLabelComponent,
    ScrollDirective,
    HomeContainerComponent
  ],
  exports: [HeaderContainerComponent, CookieInfoComponent, FooterComponent]
})
export class MainModule {}
