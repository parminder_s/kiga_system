import { TestBed, inject } from '@angular/core/testing';

import { RouteConvertorService } from './route-convertor.service';

describe('RouteConvertorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouteConvertorService]
    });
  });

  it(
    'should be created',
    inject([RouteConvertorService], (service: RouteConvertorService) => {
      expect(service).toBeTruthy();
    })
  );
});
