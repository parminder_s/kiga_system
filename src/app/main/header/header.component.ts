import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { FilterAgeGroup } from '../../area/+state/area.action';
import { getBaseDataState } from '../../base-data/base-data.selectors';
import { BasketItemsService } from '../../data/basket-items.service';
import { NowService } from '../../shared/now.service';
import { LocaleSwitchService } from '../locale-switch.service';
import { MemberLoaderService } from '../member-loader.service';
import { MenuItem } from '../menu/menu.component';

export interface AreaData {
  isHomeActive: boolean;
  isIdeasActive: boolean;
  isKgaActive: boolean;
  ageGroup: number;
  range: string;
  showLocaleSwitcher: boolean;
}

@Component({
  selector: 'kiga-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit, OnDestroy {
  public isLangOpen = false;
  public isNavOpen: boolean;
  public isSelectorOpen: boolean;
  public isFullSubscription: boolean = true;
  public isLoggedIn: boolean = false;
  public locale: string;
  public locales: Array<any> = [];
  public showBasket = false;
  public showKga = false;
  public shopLink: string;
  public showDevMenu: boolean = false;
  public forumLink: string;
  public ideasLink: string;
  public isCmsMember: boolean = false;
  getLink = () => 'en';
  numberOfItems = 0;
  logoutCallback: () => void;
  subMenu = { cls: '', isActive: false, label: '', link: '' };
  currentYear: number;
  currentMonth: number;

  private subscriptions: Array<Subscription> = [];

  userMenu: MenuItem = { label: '' };

  @Input() public areaData: AreaData;

  constructor(
    private router: Router,
    private localeSwitch: LocaleSwitchService,
    private memberLoader: MemberLoaderService,
    private basketItemsService: BasketItemsService,
    private now: NowService,
    private store: Store<any>
  ) {}

  ngOnInit() {
    const nowValue = this.now.date();
    this.currentYear = nowValue.getFullYear();
    this.currentMonth = nowValue.getMonth() + 1;
    this.logoutCallback = () => this.memberLoader.logout();

    this.subscriptions.push(
      this.store.select(getBaseDataState).subscribe(({ config, locale, links }) => {
        this.showDevMenu = config.showQuickRegisters;
        this.locale = locale;
        this.locales = config.locales.map(locale => ({
          code: locale,
          title: `MISC_LOCALE_${locale.toUpperCase()}`
        }));

        this.forumLink = links.forum;
        this.shopLink = links.shop;
        this.showBasket = this.shopLink !== '/';
        this.ideasLink = links.ideaGroupMainContainer;

        const userMenu = links.userMenu;
        this.userMenu = {
          label: userMenu.label,
          labelClass: 'hidden-sm hidden-lsm hidden-md',
          icon: 'icon-person',
          children: [
            {
              label: userMenu.label,
              callback: () => (window.location.href = userMenu.url)
            },
            { label: 'LOGOUT', callback: () => this.logoutCallback(), translateLabel: true }
          ]
        };
      })
    );

    this.memberLoader
      .get()
      .pipe(filter(member => !!member))
      .subscribe(member => {
        this.showKga = member.kga.isActive;
        this.isLoggedIn = !member.isAnonymous;
        this.isCmsMember = member.isCms;
      });

    this.basketItemsService
      .getObservable()
      .subscribe(basketItems => (this.numberOfItems = basketItems));
  }

  changeAge(ageGroup: number) {
    this.store.dispatch(new FilterAgeGroup(ageGroup));
  }

  onSearchSubmit(tags: string) {
    if (!tags) {
      this.router.navigateByUrl(`${this.ideasLink}`);
    } else {
      this.router.navigateByUrl(`${this.locale}/search-static?query=${tags}`);
    }
  }

  showLangDropdown() {
    this.isLangOpen = true;
  }

  hideLangDropdown() {
    this.isLangOpen = false;
  }

  switchLocale(code: string) {
    this.localeSwitch.switchLocale(code);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
