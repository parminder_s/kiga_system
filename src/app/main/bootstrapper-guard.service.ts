import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { getBaseDataState } from '../base-data/base-data.selectors';

@Injectable({
  providedIn: 'root'
})
export class BootstrapperGuardService implements CanActivate {
  constructor(private store: Store<any>) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const locale = route.paramMap.get('locale');
    return this.store.pipe(
      select(getBaseDataState),
      filter(baseData => baseData.locale === locale),
      map(() => true),
      take(1)
    );
  }
}
