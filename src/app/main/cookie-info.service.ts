import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { PlatformService } from '../misc/services/platform.service';

@Injectable({
  providedIn: 'root'
})
export class CookieInfoService {
  private cookieName = 'agreedCookieUse';
  private cookieUsage$ = new BehaviorSubject<boolean>(false);

  constructor(@Inject(DOCUMENT) private document: Document, platform: PlatformService) {
    if (platform.isBrowser()) {
      let decodedCookie = decodeURIComponent(this.document.cookie);
      let splittedCookie = decodedCookie.split(';');
      for (let i = 0; i < splittedCookie.length; ++i) {
        if (splittedCookie[i].match(this.cookieName)) {
          this.cookieUsage$.next(true);
        }
      }
    } else {
      this.cookieUsage$.next(true);
    }
  }

  setUsageAccepted() {
    let expiration_date = new Date();
    expiration_date.setFullYear(expiration_date.getFullYear() + 10);
    this.document.cookie = `${
      this.cookieName
    }=true; expires=${expiration_date.toUTCString()};path=/`;
    this.cookieUsage$.next(true);
  }

  getObservable(): Observable<boolean> {
    return this.cookieUsage$;
  }
}
