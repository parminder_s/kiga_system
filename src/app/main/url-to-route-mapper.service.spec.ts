import { TestBed, inject } from '@angular/core/testing';

import { UrlToRouteMapperService } from './url-to-route-mapper.service';

describe('UrlToRouteMapperService', () => {
  it('should map a url without query parameter', () => {
    const routeInfo = new UrlToRouteMapperService().getRouterPart('/de/cms/ideas');

    expect(routeInfo.routerLink).toBe('/de/cms/ideas');
    expect(routeInfo.queryParams).toEqual({});
  });

  it('should map with query parameter', () => {
    const routeInfo = new UrlToRouteMapperService().getRouterPart('/de/cms/ideas?ageGroup=1');

    expect(routeInfo.routerLink).toBe('/de/cms/ideas');
    expect(routeInfo.queryParams).toEqual({ ageGroup: '1' });
  });

  it('should map with many query parameters', () => {
    const routeInfo = new UrlToRouteMapperService().getRouterPart(
      '/de/cms/ideas?ageGroup=1&search=test'
    );

    expect(routeInfo.routerLink).toBe('/de/cms/ideas');
    expect(routeInfo.queryParams).toEqual({ ageGroup: '1', search: 'test' });
  });
});
