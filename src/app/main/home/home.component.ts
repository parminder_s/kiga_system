import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { HomeResponse } from '@kiga/home/api';
import * as _ from 'lodash';
import { filter } from 'rxjs/operators';
import IMember from '../../../../app/src/misc/permission/js/IMember';
import { LoaderService } from '../../shared/loader.service';
import { ScrollPaneItem } from '../../shared/scroll-pane/scroll-pane.component';
import { MemberLoaderService } from '../member-loader.service';

@Component({
  selector: 'kiga-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  @Input() public homeResponse: HomeResponse;
  @Input() public locale: string;
  showEditButton = false;
  isFullSubscription: boolean = false;
  member: IMember;
  scrollPaneItems: Array<ScrollPaneItem> = [];

  constructor(private loader: LoaderService, private memberLoader: MemberLoaderService) {}

  ngOnInit() {
    this.loader.hide();

    this.memberLoader
      .get()
      .pipe(filter(member => !!member))
      .subscribe(member => {
        this.member = member;
        this.showEditButton = member.isCms;
        if (_.isFunction(member.hasActiveFullSubscription)) {
          this.isFullSubscription = member.hasActiveFullSubscription();
        }
      });
  }
}
