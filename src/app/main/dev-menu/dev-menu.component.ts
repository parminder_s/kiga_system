import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { configSelector } from '../../base-data/base-data.selectors';
import { EndpointService } from '../../endpoint/endpoint.service';
import { MemberLoaderService } from '../member-loader.service';
import { MenuItem } from '../menu/menu.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'kiga-dev-menu',
  templateUrl: './dev-menu.component.html',
  styleUrls: ['./dev-menu.component.less']
})
export class DevMenuComponent implements OnInit, OnDestroy {
  history = [];
  displayMenu = false;
  permissionMode: string;
  menuItem: MenuItem = {
    label: 'Dev  Menu',
    icon: 'icon-pencil',
    children: [
      {
        label: 'CMS Admin',
        icon: 'icon-pencil',
        callback: () => this.handleQuickRegister('cmsAdmin')
      },
      { label: 'Standard Subscription', callback: () => this.handleQuickRegister('standard') },
      {
        label: 'Ended Standard Subscription',
        callback: () => this.handleQuickRegister('endedStandard')
      },
      { label: 'Test Subscription', callback: () => this.handleQuickRegister('test') },
      { label: 'Ended Test Subscription', callback: () => this.handleQuickRegister('endedTest') },
      { label: 'NAVIGATION_AGE_LABEL', translateLabel: true },
      { label: 'Community Access', callback: () => this.handleQuickRegister('community') },
      {
        label: 'Has Children',
        children: [
          {
            label: 'Submenu 1'
          },
          {
            label: 'Submenu 2',
            children: [
              {
                label: 'Sub Sub menu'
              }
            ]
          }
        ]
      }
    ]
  };
  private subscriptions: Array<Subscription> = [];

  constructor(
    private router: Router,
    private endpoint: EndpointService,
    private store: Store<any>,
    private memberLoader: MemberLoaderService
  ) {}

  ngOnInit() {
    this.store
      .select(configSelector)
      .subscribe(config => (this.permissionMode = config.permissionMode));
  }

  async handleQuickRegister(code: string) {
    let url = '/test/quickRegisterJson?';

    if (code === 'standard') {
      url += 'permission=' + this.permissionMode;
    } else if (code === 'test') {
      url += 'product=test&';
    } else if (code === 'cmsAdmin') {
      url = '/test/CmsLoginJson?';
    } else if (code === 'endedTest') {
      url += 'product=test&isCancelled=true&';
    } else if (code === 'endedStandard') {
      url += 'isCancelled=true&';
    } else if (code === 'community') {
      url += 'product=community&';
    }
    url += `permission=${this.permissionMode}`;

    await this.endpoint.get(url, { rawUrl: true }).toPromise();
    window.location.reload();
  }

  showMenu() {
    this.displayMenu = true;
  }

  hideMenu() {
    this.displayMenu = false;
    this.history = [];
  }

  getMenu() {
    const historyLength = this.history.length;
    if (historyLength) {
      return this.history[historyLength - 1];
    }
    return this.menuItem.children;
  }

  onClick(item) {
    if (item.children) {
      this.history.push(item.children);
      return; // return to avoid hide menu
    } else if (item.callback) {
      item.callback();
    } else if (item.link) {
      this.router.navigate([item.link]);
    }
    this.hideMenu();
  }

  back() {
    this.history.pop();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
