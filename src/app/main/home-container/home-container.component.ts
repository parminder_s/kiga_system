import { Component, OnInit } from '@angular/core';
import { HomeResponse } from '@kiga/home/api';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import { ResetMetas } from '../../+state/meta.actions';
import { SetArea } from '../../area/+state/area.action';
import { AreaType } from '../../area/area-type';
import { localeSelector } from '../../base-data/base-data.selectors';
import { ngJsToNgUrl } from '../../compat/ngJsToNgUrl';
import { EndpointService } from '../../endpoint/endpoint.service';
import {
  KigaTransferStateService,
  TransferStateType
} from '../../shared/kiga-transfer-state.service';

@Component({
  templateUrl: './home-container.component.html'
})
export class HomeContainerComponent implements OnInit {
  public homeResponse$: Observable<HomeResponse>;
  public locale: string;

  constructor(
    private kigaTransferState: KigaTransferStateService,
    private endpoint: EndpointService,
    private store: Store<any>
  ) {}

  ngOnInit() {
    this.store.dispatch(new SetArea(AreaType.DEFAULT));
    this.store.dispatch(new ResetMetas());
    this.homeResponse$ = this.store.select(localeSelector).pipe(
      tap(locale => (this.locale = locale)),
      mergeMap(locale =>
        this.kigaTransferState.getOrFetch<HomeResponse>(TransferStateType.HOME, locale, () =>
          this.endpoint.post<HomeResponse>('homepage/getHome', {})
        )
      ),
      map(data => {
        data.newIdeasUrl = ngJsToNgUrl(data.newIdeasUrl);
        return data;
      })
    );
  }
}
