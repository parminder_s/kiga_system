import { Directive, ElementRef, OnDestroy, Renderer2 } from '@angular/core';
import { PlatformService } from '../misc/services/platform.service';

@Directive({
  selector: '[kigaScroll]'
})
export class ScrollDirective implements OnDestroy {
  lastScroll = 0;
  height = 124;
  constructor(
    private element: ElementRef,
    private renderer: Renderer2,
    private platform: PlatformService
  ) {
    if (this.platform.isBrowser()) {
      this.onScroll = this.onScroll.bind(this);
      window.addEventListener('scroll', this.onScroll);
    }
  }

  onScroll() {
    const currentScroll = window.scrollY;
    if (currentScroll > this.lastScroll && currentScroll > 124) {
      this.renderer.addClass(this.element.nativeElement, 'hide_header');
      this.lastScroll = currentScroll;
    } else if (currentScroll < this.lastScroll && currentScroll > 124) {
      this.renderer.removeClass(this.element.nativeElement, 'hide_header');
      this.lastScroll = currentScroll;
    } else if (currentScroll <= 124) {
      this.renderer.removeClass(this.element.nativeElement, 'hide_header');
      this.lastScroll = currentScroll;
    }
  }

  ngOnDestroy() {
    if (this.platform.isBrowser()) {
      window.removeEventListener('scroll', this.onScroll);
    }
  }
}
