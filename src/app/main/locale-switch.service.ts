import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LocaleSwitchService {
  constructor(private router: Router) {}

  public switchLocale(locale: string) {
    let url = this.router.url.split('/');
    let newUrl;
    if (url.length < 4) {
      // default route
      newUrl = `/ng6/${locale}/home`;
    } else {
      // full url
      url[2] = locale;
      newUrl = url.join('/');
    }
    this.router.navigateByUrl(newUrl, {});
  }
}
