## Upgrade User to CMS User

This task is required if a registered user should be elevated to a CMS User, which means they have full access to the content, services and can edit content, e.g. have access to the content backend.

1.  Connect to Live-DB via ssh and docker exec mysql

2.  Search for the user to upgrade

3.  Remove the user from CRM

4.  Set CustomerID and CRMID column to null

5.  Generate the update script via ./scripts/user-helper.py

6.  Execute the update script in the database

7.  Look up the required groups from table `Groups` and assign them by inserting them into `Group_Members`.

8.  Test if login works
