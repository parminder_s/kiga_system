# Knowledge Base

Right usage of DB-relations in JPA
* Include `mappedBy` to `@OneToMany`, never use `@JoinColumn`. `MappedBy` indicates that the owner is in the other table. `@JoinColumn` would create a further relation which results that JPA removes all foreign keys and adds them new during the persisting phase.
