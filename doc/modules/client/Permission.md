# Permission Module

The permission module acts as a kind of filter to each transition, e.g. state change in ui-router.
It is run before the transition and any resolving occurs.

Permissions are defined when creating the routes. For example:

````typescript
angular.module("foo", [])
  .config(function($stateProvider) {
    $stateProvider.state({
      name: "bar",
      controller: "barCtrl",
      template: "bar.html",
      params: {
        permissions: ["fullSubscription"]
      }
    })
  })
````

The code above defines that state `bar` can only be accessed if the user's rights fulfill the
`fullSubscription` permission.

Please note that it is possible to
* definie multiple permissions per state and
* permissions are inherited

**Creating own permissions**

Currently there is only permission `fullSubscription` available that requires the user to have a
subscription type of standard (not test) which is not expired. More
permissions will be added as more and more modules make usage of the permission module.

New permissions can be added by registering a class that implements `PermissionHandler` along a
name to the `PermissionHandlerRegistry`. The handler is given the transition object and has to return
`true` which means transition is granted or a TargetState if the transition is denied.

For example a new permissionHandler with the name 'loggedIn` is created like that:

````typescript
class LoggedInHandler implements PermissionHandler {
  constructor(private memberService: IMemberService, private $state: StateService) {}

  handle(transition: Transition): boolean | TargetState {
    if (this.memberService.get().isLoggedIn()) {
      return true;
    }
    else {
      this.$state.target("root.login", {locale: transition.params("to")["locale"]});
    }
  }
}
````
The registration and activation is done by
````typescript
angular.module("foo", [])
  .service("loggedInHandler", function(member, $state) {
    return new LoggedInHandler(member, $state);
  })
  .run(function(permissionHandlerRegistry, loggedInHandler) {
    permissionHandlerRegistry.registerHandler("loggedIn", loggedInHandler);
  })
  .config(function($stateProvider) {
    $stateProvider.set({
      name: "bar",
      controller: "barCtrl",
      template: "bar.html",
      params: {
        permissions: ["loggedIn"]
      }
    })
  });
````
