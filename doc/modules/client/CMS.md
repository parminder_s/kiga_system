### CMS Module

Before you start reading this, you may want to read [introduction to CMS functionality](CMS.md) first.

**How to create new module that works with CMS module**


+ Create new module and make it accessible throughout the application
+ Create a class that extends Renderer and implement required methods

```typescript
import {Renderer} from "<path to Renderer.ts>/Renderer";
export class SomeRenderer extends Renderer {
}
```

+ Define that class as an angular service

```typescript
app.service("someRenderer", function(<required dependencies>) {
    return new SomeRenderer(<required dependencies>);
});
```

+ In a run block register the renderer using cmsRenderersFactory
    + first argument is the ClassName
    + second argument is the renderer that will be responsible for that ClassName

```typescript
app.run(function(cmsRenderersFactory, someRenderer) {
    cmsRenderersFactory.register("ClassName", someRenderer);
});
```
