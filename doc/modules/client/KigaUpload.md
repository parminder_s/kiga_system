### Kiga Upload Component

`kiga-upload` component has been implemented to help with uploading files
and is integrated with other parts of the system, i.e. recognition of
the file type and uploading them to the amazon bucket by proper file
processor.

Additionally, the `kiga-upload` component is able to perform pre and
post processing when context attribute has been provided, and matching
Pre and Postprocessors can be found on the server side (see:
[Server-side part of kiga upload documentation](../server/KigaUpload.md))

The component can work both within a form or outside of it. The difference
is that placing it outside a form will only work as an uploader, whereas
using it in a form makes it possible to implement validation and
overwriting files for particular entity represented by the form.

Below is list of attributes available on the `kiga-upload` component

| Attribute        | Type     | Description |
|------------------|----------|-------------|
| name             | String   | Name of the form element that contains information about uploaded files ids |
| context          | String   | Context of this uploader. This is used by the server side to find Pre and Post processors (classes implementing `PreUploadProcessor` or `PostUploadProcessor` interface and annotated with `@UploadProcessor`) |
| ng-model         | Array    | A binding to the property holding information about uploaded files' ids |
| max-files        | Integer  | **Default: 1;** Max number of files that are allowed to be uploaded |
| on-start-upload  | Function | Callback method to be called before uploading of files has been started. |
| on-finish-upload | Function | Callback method to be called when uploading of files has been finished. |

Example:

##### view.html
```HTML
<form class="one-col" name="validationForm" ng-submit="ctrl.onSubmit()">
  <div class="panel">
    <div class="panel-heading">
      Form powered with KigaUpload component and validated with ng-fab-forms.
    </div>
    <div class="panel-body">
      <input type="text" name="someName" ng-model="ctrl.someName" required />
      <kiga-upload name="uploadedFiles"
                   ng-model="ctrl.filesIds"
                   max-files="3"
                   context="'dummy'"
                   on-start-upload="ctrl.onStartUpload"
                   on-finish-upload="ctrl.onFinishUpload"
                   required />
      <button type="submit">Submit</button>
    </div>
  </div>
</form>
```

##### controller.ts
```TypeScript
export class Controller {
  private filesIds: Array<number>;
  private someName: string;

  onStartUpload() {
    // code to be performed before upload started
  }

  onFinishUpload() {
    // code to be performed when upload has been finished
  }

  onSubmit() {
    // code to be performend on form submission
  }
}
```
