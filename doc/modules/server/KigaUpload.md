### Kiga Upload Component

This documentation page documents server-side implementation of the
`kiga-upload` module that complements the
[kiga-upload client-side](../client/KigaUpload.md)

It is possible to register Pre or/and Post processors to the flow
of uploading files. In order to do so, one must:

+ add `context` attribute to the `kiga-upload` component on client-side
([see the client side documentation](../client/KigaUpload.md))
+ create implementation of Pre or/and Post processor implementing
respectively `PreUploadProcessor` or/and `PostUploadProcessor` and annotate it with `@UploadProcessor`

Example:

##### client-side
```HTML
<kiga-upload name="uploadedFiles" context="'yourcontextname'" />
```

##### Pre Processor:
```java
import com.kiga.kigaupload.annotation.UploadProcessor;
import com.kiga.kigaupload.processors.PreUploadProcessor;
import com.kiga.security.domain.Member;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@UploadProcessor(context = "yourcontextname")
public class CustomPreUploadProcessor implements PreUploadProcessor {
  @Override
  public void process(Member member, List<MultipartFile> files) {
    // your preprocessing code goes here
  }
}
```

##### Post Processor:
```java
import com.kiga.kigaupload.annotation.UploadProcessor;
import com.kiga.kigaupload.processors.PostUploadProcessor;
import com.kiga.security.domain.Member;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@UploadProcessor(context = "yourcontextname")
public class CustomPostUploadProcessor implements PostUploadProcessor {
  @Override
  public void process(Member member, List<MultipartFile> files) {
    // your postprocessing code goes here
  }
}
```
