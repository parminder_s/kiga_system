### Cache

Caching works more or less like it is described in the Spring documentation.

There are some code guidelines:
* Do not use layered caches. Cache only endpoints (methods in Controllers).
* If possible every cached endpoint should be a one-liner method that calls a bean which
does the actual processing. The reason is that it is easier to setup Cache Actualizer jobs that are not required to call the endpoint via HTTP but in-process by the same bean the controller is executing.

Caching is relevant for the production setup. There we have different behaviours for the
red and live profile: 
1. The red profile ignores every cache request but puts a new cache entry at each request.
2. For long-running requests like the ones in ideas overview, we cannot afford a cache miss. Therefore jobs are scheduled that refresh the caches before they expire.
 
So basically cache actualization should ideally never been triggered by a customer but by a scheduled job or editors working on the red system.

#### Customizations
For the behaviour of ignoring a get request, the property `web.cache.useCache` is used. `web.cache.enabled` is set for putting caches.

The `KigaCacheManager` is a wrapper of `RedisCacheManager` and is able to ignore a cache request when configured. Further more one can also set customized cache groups if one requires a different expiration time by calling the `addKigaCache` in  `CacheConfig` when setting up the CacheManager.

