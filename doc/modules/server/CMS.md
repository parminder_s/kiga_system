### CMS Module

Before you start reading this, you may want to read [introduction to CMS functionality](CMS.md) first.

**How to create a serverside module compatible with CMS module**

* Create a new class that implements ViewModelMapper
* You can alternatively extend SimpleViewModelMapper containing basic functionality

```java
import com.kiga.cms.service.SimpleViewModelMapper;
import com.kiga.cms.service.parameters.QueryResponseParameters;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.SiteTree;

import javax.servlet.http.HttpServletRequest;

public class SomeViewModelMapper extends SimpleViewModelMapper<SomeEntity, SomeViewModel> {
  @Override
  protected SomeViewModel buildViewModel(HttpServletRequest httpServletRequest,
      QueryResponseParameters<SomeEntity> queryResponseParameters) throws Exception {
    // your code for retrieving data
  }
}
```

* Create a bean for that class and register it using ViewModelMapperFactory

```java
@Bean
public SomeViewModelMapper createSomeViewModelMapper(<dependencies>) {
  return new SomeViewModelMapper(<dependencies>);
}
```
