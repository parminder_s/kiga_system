# Jobs module
The jobs module is the central point for setting up the schedules
for all jobs that are running in the system.

This module is therefore an intermediate module coupling
Spring's task functionalities together with the job classes themselves.

The implementation of the job lies in the responsibility of the
module which it is assigned to contextually. Scheduling, triggering and logging is managed by the 
Jobs module.

**Requirements**

A job has following requirements:
* Is a Spring Bean
* Implements `com.kiga.spec.Jobable`
* Has Annotation `@Job` with set `cronProperty` property.
* The value of the cronProperty has to be a valid value of the application properties `jobs` section.

**Example**

A job `FooJob` would look like that:
 
````java
@Job(cronProperty = "fooCron")
@Service
public class FooJob implements com.kiga.spec.Jobable {
  @Override
  public void runJob() {
    LoggerFactory.getLogger(getClass()).info("job running...");
  }
}
````
And in the application[-*].yml you should put the `fooCron` property like:
````yaml
jobs:
  fooCron: 0 * * * * *
````
This will run the FooJob every minute.
