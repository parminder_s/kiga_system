# Payment System

The payment module provides functionality for native and works against the Payment Service Providers

* MPay24
* Garanti

In development and stage mode it connects to the test systems where one can use following test data:

For consumers, e.g. modules that want to use the payment system, there is a single entry point `com.kiga.payment.PaymentInitiator::initiate`.

You have to pass a config object that contains the necessary information like price, etc. The method returns a link to a PSP (Payment Service Provider) that you should use to redirect your user to. Once the user returns from the PSP the payment system will process the data and will automatically redirect the user again to the url that the consumer has passed during the initialisation.

### Garanti

1.  Visa: 4282209004348015, valid to: 05/2019, cvv: (none required)

### MPay24

1.  Visa: 4444111122223333, valid to: 06/2019, cvv: (no value)
2.  MasterCard: 5555444433331111, valid to: 06/2019, cvv: (no value)
3.  SOFORT: Bic 00000, rest is arbitrary, TAN: 12345
4.  PayPal: EMail: pp_buyer@mpay24.com, Password: mPAY24mPAY24

## Model

`Payment`'s `referenceId` and `referenceCode` link the payment to
other entities in the system.
E.g. `referenceId` can be the id of a Shop-`Purchase`.
