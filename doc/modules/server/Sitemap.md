# Sitemap

This module generates the sitemap.xml.

The default use case is that the file gets generated via an asynchronous job and uploaded to Amazon S3.

When Google's searchbot or any other bot requests the sitemap on /sitemap.xml the controller proxies the file directly from S3.

For testing purposes a content admin can request a fresh sitemap.xml (does not get uploaded to S3 automatically) via the controller.
