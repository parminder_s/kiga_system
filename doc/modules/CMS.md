# Introduction to CMS functionality

The CMS consists of two modules client and server-side. It is used for rendering any entity
of type SiteTree in Angular by providing an extensible "catch-all" controller.

It consists of following elements:
* Client-Side:
  * CmsRenderer: An angular controller listening on url `/cms/{path:nonUriEncoded}` which sends the path to server, gets a ViewModel returned and delegates the rendering to a registered Renderer of that ViewModel.
  * Renderer: Is an angular service but acts more like a typical controller since and is called by the CmsRenderer. There exists a Renderer for each viewmodel on the server-side.
  * CmsRenderersFactory: Is used during angular's run lifecycle to register the renderers for their viewmodels.
* Server-Side:
   * ViewModel: A data structure that contains all required information the renderer requires to show the Entity.
   * ViewModelMapper: Returns the ViewModel for a given subclass of SiteTree.
   * CmsController: Is called by the CmsRenderer and returns the viewmodel for the given path.


For more detailled background information, please consult original [original concept](../misc/CMS.md)
