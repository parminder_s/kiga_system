# Search

We are using ElasticSearch which is disabled by default. To enable it set the property `search.elastic.mockElasticSearch` in your `application-dev.yml` to`false`.

After that you can execute the gradle task `runSearchContainer` which makes sure, that a Docker instance is started.
Since the index is by default empty you can update it by requesting `http://localhost:8080/api/searcher/update`.

After that you can open the website under `http://localhost:8081` and simply search for ideas. The Keyword `children` returns the most results.

