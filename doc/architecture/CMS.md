# Integration of CMS-Sitemap into Angular/Spring
This document is a concept for integrating the CMS especially its SiteMap into the Angular/Spring stack.
## Routing in Angular
Angular's routing system has a very static nature. Each Url has to be mapped explicitly to a controller. There is of course the possibility to make some parts of the URL dynamic and even optional but the normal use case still foresees having an url with a defined path like:
`/en/ideas/maths/counting/counting-idea`

That url has to be coded which means it is part of the code and does not get queried by the database.
## Routing in CMS (SilverStripe)
The CMS in comparison has a very dynamic approach. The url's path is split into multiple pieces (=urlsegments). In the database each urlsegment is represented by a SiteTree element that has the value of its urlsegment stored along its parent SiteTree element.

So the Sitemap of the CMS builds itself up of recursive SiteTree entities and is completely dynamic in terms
of urlname and hierachy.

Further more each SiteTree has a specific content type that differs in terms of rendering and content fields.

Given the above example we would have following SiteTree elements that represent the url:

```json
[
 {id: 1, urlSegment: "en", parentId: 0, className: "RootPage"},
 {id: 2, urlSegment: "ideas", parentId: 1, className: "MainArticleCategory"},
 {id: 3, urlSegment: "maths", parentId: 2, className: "ArticleCategory"},
 {id: 4, urlSegment: "counting", parentId: 3, className: "ArticleCategory"},
 {id: 5, urlSegment: "counting-idea", parentId: 4, className: "Article"}
]
```

It is very easy for an editor to change the hierarchy or url names. For example we can just move counting-idea two levels lower by setting parentId to 2 or change the url by renaming counting-idea to idea-for-counting. The moving to another parent is done rarely but adding and changing url-segments is done on a daily basis.
## Selected implementation strategy
Summarized in Angular one has actually to code the sitemap whereas the CMS has a far more dynamic approach. It allows the editors to create the sitemap very freely. There are no constraints like having specific types of content on a specific node or any constraints in terms of hierarchy in general.

**We have to map the existing sitemap of our CMS 1:1 to Angular**. That is an absolute must. Having two different url systems on frontend and backend requires a mapping module and adds additional complexity that should be avoided if possible.

There are two possibilities how to reflect the sitemap in Angular:

1. The server generates the sitemap in a form that ui-router can use it directly (containing also controller classes and partials) and delivers it via a json-file or
2. ui-router sees the complete sitemap of the CMS as a single url having multiple dynamic arguments.

Given an approximate amount of SiteTree entities in all languages of 10.000 it is clear that strategy #2 "all dynamic" is selected.

That has now following consequences

### CmsController
Since the sitemap is in terms of angular a single url with multiple parameters like: :part1/:part2/:part3/:part4/:part5 that means it has also exactly one controller attached and one partial.

We call that controller `CmsController`.

### Rendering-Service and partial per content type (SiteTree subclass)
Given the certain fact that the processing of common content types like KigaIdea, KigaCategory, IdeaGroup requires an own controller and partial on its own, it is somewhat counter-intuitive having a single controller for all existing content types.

To solve that issue, one has to see the controller for the content type as an own Angular service that takes over usual controller tasks. That service is called by the `CmsController`. As a result the only task of the CmsController is to take on the Url, request from the server which actual content type is represented by the url and then pass on all further processing to an angular service that behaves as the "real" controller of the content type.

In terms of partials the situation is similiar. We have a main cms partials that has a simple ng-include or similiar directive which loads a partial defined for that particular content type.

---

The reasons for this gap between ui-router and the CMS is that Angular has been designed
for web applications where usually each url has a different functionality and therefore
requires an own controller. On the other side the CMS has usually a handful of content types
(e.g. controllers) but many url's for the same content type.
