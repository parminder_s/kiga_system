# Infrastructure

This chapter describes infrastructure.

## Logging

The project uses the sentry ([https://sentry.io/welcome/](https://sentry.io/)) service/library as a means to track errors (of the Java/Spring part of the application).
The configuration is here: *TODO*
A logging message of level `error` triggers a sentry message.
The sentry service in use is self-hosted.
