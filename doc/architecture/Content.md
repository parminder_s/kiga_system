Content Structure
=================


This section briefly describes the data structure that stores categories and content.

SiteTree
--------

There is the concept of the `SiteTree`.
`SiteTree` contains categories as well as single documents (articles) -
"Everything that has a URL, is in `SiteTree`".


The table stores instances of a class hierarchy.
Classes stored are `MainArticleCategory`, `IdeaGroupMainContainer` etc.
See below for a full list.
Besides being elements of a class hierarchy, the elements form a hierarchical
structure (tree), by parent-child relations (signified by the parent id).

Root objects:
 - `MainArticleCategory` (which contain sub categories `ArticleCategory`)
 - `IdeaGroupMainContainer` (which contain sub categories `IdeaGroupCategory`)

The `ArticleCategory` is the older concept, `IdeaGroup` is the newer concept.
The structure is as follows:

### ArticleCategory Concept

- `MainArticleCategory`: contains `ArticleCategory` or `Article`.
- `ArticleCategory`: contains `ArticleCategory` or `Article`

### IdeaGroup Concept

- `IdeaGroupMainContainer`: contains `IdeaGroupContainer`
- `IdeaGroupContainer`: contains `IdeaGroupCategory`, `IdeaGroup` or `Article`
- `IdeaGroupCategory`: contains `IdeaGroupContainer`, `IdeaGroupCategory`, `IdeaGroup`, `ArticleCategoryContainer` or `Article`
- `IdeaGroup`: contains `Article`
- `ArticleCategoryContainer`: contains `IdeaGroup`

### Downloads

Also, there are nodes of type `MainDownloadCategory`, which contain:
`AphorismusCategory`, `CalendarCategory`, `ECardCategory`, `FormularCategory`,
`GalleryCategory`, `MandalaCategory`, `CartoonCategory`


Publishing
----------

All things are in `SiteTree` - which is actually only 'drafts' (not published).
When published , the things get inserted in `SiteTree_Live` .
When unpublished, objects are deleted from that table.
The ids (primary keys) are the same in both tables.
In addition, there is `SiteTree_versions`.


Site Tree Classes
-----------------

 - Aphorismus
 - AphorismusCategory
 - Article
 - ArticleCategory
 - ArticleCategoryContainer
 - Calendar
 - CalendarCategory
 - Cartoon
 - CartoonCategory
 - CustomPage
 - DimensionPage
 - DownloadCategory
 - DraftContainer
 - DraftFolder
 - ECard
 - ECardCategory
 - ErrorPage
 - EventPage
 - FeedbackPage
 - FooterPage
 - Formular
 - FormularCategory
 - Forum
 - ForumHolder
 - Gallery
 - GalleryCategory
 - GiftActivator
 - HomePage
 - IdeaGroup
 - IdeaGroupCategory
 - IdeaGroupContainer
 - IdeaGroupMainContainer
 - JobPage
 - KigaCategory
 - KigaDownload
 - KigaDownloadFile
 - KigaDownloadImage
 - KigaPage
 - LostPassword
 - MainArticleCategory
 - MainDownloadCategory
 - MainServicePage
 - Mandala
 - MandalaCategory
 - Newsletter
 - NewsletterArchive
 - NewsletterContainer
 - NewsletterSubscribe
 - NewsletterUnsubscribe
 - NoJavaScript
 - Page
 - PaymentPage
 - PayPalPage
 - PlanPage
 - ProductPage
 - ProductPageHolder
 - RedirectorPage
 - RegistrationJob
 - RegistrationOrg
 - RegistrationPage
 - RegTypeContainer
 - RootPage
 - Service_BillAddress
 - Service_BillData
 - Service_ChangeType
 - Service_CustomerData
 - Service_EMail
 - Service_Invoices
 - Service_IP
 - Service_LicenceUser
 - Service_Locale
 - Service_MiscData
 - Service_Overview
 - Service_Password
 - Service_Profile
 - ServiceLicence
 - ServicePage
 - ShopArticlePage
 - ShopCategory
 - ShopMainContainer
 - SiteTree
 - VirtualPage


Misc / Query Helpers
--------------------

The following query yields the top level categories (there are currently four,
one for each language):

    select Title, ID, Locale, ClassName from SiteTree
    where ClassName in ('MainArticleCategory', 'IdeaGroupMainContainer');

### Get all category / container type elements

    select Title, MetaTitle, Locale, URLSegment, ID, ParentID, ClassName from SiteTree
    where ClassName in (
    'MainArticleCategory',
    'ArticleCategory',

    'IdeaGroupMainContainer',
    'IdeaGroupContainer',
    'IdeaGroupCategory',
    'IdeaGroup',
    'ArticleCategoryContainer',

    'MainDownloadCategory',
    'AphorismusCategory',
    'CalendarCategory',
    'ECardCategory',
    'FormularCategory',
    'GalleryCategory',
    'MandalaCategory',
    'CartoonCategory'
    );
