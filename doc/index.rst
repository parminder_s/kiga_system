.. KigaSystem documentation master file, created by
   sphinx-quickstart on Thu Mar 15 08:07:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KigaSystem's documentation!
======================================

At the moment, a rather loose collection of documents, which will be improved over time.

Contents:

.. toctree::
   :maxdepth: 2

   setup/Installation
   setup/DevelopmentEnvironment
   setup/installhelp
   setup/Deployment
   introduction/ServerSide
   introduction/ClientSide
   modules/CMS
   modules/Search
   modules/client/CMS
   modules/client/KigaUpload
   modules/client/Permission
   modules/server/Cache
   modules/server/CMS
   modules/server/Jobs
   modules/server/KigaUpload
   modules/server/Payment
   modules/server/Sitemap
   architecture/CMS
   architecture/Content
   architecture/Infrastructure
   misc/KnowledgeBase


.. not in a doctree currently (Misc, Modules)


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

