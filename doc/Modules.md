# Modules
This sections describes the modules on both server- and client-side:

* Server
  * [Cache](modules/server/Cache.md)
  * [CMS](modules/server/CMS.md)
  * [Jobs](modules/server/Jobs.md)
  * [Payment](modules/server/Payment.md)
  * [Sitemap](modules/server/Sitemap.md)
* Client
  * [CMS](modules/client/CMS.md)
  * [Permission](modules/client/Permission.md)

You may also be interested in this [introduction to CMS functionality](modules/CMS.md)
