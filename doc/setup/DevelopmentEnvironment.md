# Configure for Development

Various guides for how to set up the system locally for development.
Whenever a host is to be specified, use `0.0.0.0`.
It might help to define a hostname that points to 0.0.0.0, such as `kiga`.
(Some example configs in this section use `kiga` as hostname.)

## application.yml

For configuration, the application uses an `application.yml` file at startup.
The file is located at `/modules/main/src/main/resources`.
In this file, datasources (database connection or fixture-setups), cache,
search backend and many other things are configured.
Every deployment or development environment (such as production server,
red (admin) server) has its own `application.yml` file.

For a local configuration, create a `application-dev.yml` alongside the
other configurations. This file will hold the configuration for your local
development setup.

## Authentication / Sessions

To use the pre-defined dummy-users that are selectable from the DEV menu ("login
as [John Harrison, Homer Simpson, Lucy Sanders, etc.]", the `cacheType` setting
must be set to `mocked` (with setting `redis` , the real setup involving the
CRM system would be used).

## Orchestrating the Various Services Locally

As an alternative to using mocked services for all systems (cache, db etc.),
some or all systems can be run as docker images..

There are separate repositories that contain the required parts:

* `kiga-db` database migration
* `kiga-setup` docker configurations for starting up
  required systems services
* `kigaweb-server` legacy cms - a php project (silvestripe)

## Start containers for development

Inspect the file `docker-compose-dev.yml` and make any adaptions necessary
for the local development environment.
Then start the containers:

      cd kiga-setup
      docker-compose -f docker-compose-dev.yml up


      # To start deamonized in the background use -d flag:
      docker-compose -f docker-compose-dev.yml up -d

Some images will have to be pulled from the kiga registry (`ERROR: pull access denied`).
Authorization is required with `docker login`. This is a two-step process here,
first, an `aws ecr get-login` command must be issued, which returns the actual login statement.
Therefore, _aws client and the access keys_ must be configured on the system.

    # get login shell command
    aws ecr get-login --region eu-west-1

    # returns something like this (very long password token).
    docker login -u AWS -p eyJw.......2Mjg1fQ== -e none https://567748011851.dkr.ecr.eu-west-1.amazonaws.com

Paste the `docker login ...` command back into the shell!
In some cases, remove the `-e none` option, if it prevents login from working.

### Testing that all containers are running

The docker command `docker ps` should output something like:

    CONTAINER ID        IMAGE                                    COMMAND                  CREATED             STATUS              PORTS                                            NAMES
    47a91a6187c0        kiga/server:latest                       "/usr/local/bin/super"   6 months ago        Up 4 hours          0.0.0.0:10081->80/tcp                            kiga-server
    fe6b51c6692f        memcached                                "docker-entrypoint.sh"   6 months ago        Up 4 hours          0.0.0.0:11211->11211/tcp                         kiga-cache
    9ea18b4f96cf        igortimoshenko/docker-elasticsearch-hq   "/docker-entrypoint.s"   6 months ago        Up 4 hours          0.0.0.0:9200->9200/tcp, 0.0.0.0:9300->9300/tcp   kiga-search
    67beb3588cf5        nginx                                    "nginx -g 'daemon off"   6 months ago        Up 4 hours          0.0.0.0:10080->80/tcp, 0.0.0.0:10443->443/tcp    kiga-proxy
    f79e47b6e5bf        mysql:5.5                                "docker-entrypoint.sh"   6 months ago        Up 4 hours          0.0.0.0:13306->3306/tcp                          kiga-db
    4f665f1233c9        redis                                    "docker-entrypoint.sh"   6 months ago        Up 4 hours          0.0.0.0:6379->6379/tcp                           kiga-redis

## Frontend Database

This section decribes how to configure the application to use a database
with actual data.

* Docker container must be running.
* Log in to test that the service works and connection is possible: `mysql -h 0.0.0.0 -P 13306 -u root -pkiga123`
* Create the database `kiga` (`create database kiga`)
* Import a dump (see below)

Get the `dump-dev.sql.gz` (modified real-world data suitable for use
in development). Unzip it and import it.

The shell script `kiga-setup/reset-kiga-db.sh` can be used to download a suitable dump with complete schema and test content.
(Aws credentials have to be provided as environment variables).

### Contents of the database

_Note_ : in the development compose configuration, no permanent volume for
database exists, thus database content itself has to be created freshly after each up/down of docker-compose).

### Configure Kiga System

With fixtures (no database):

    datasource:
      ss:
        hsql/fixtures:
        loadFixtures: true
        jdbcUrl: jdbc:hsqldb:mem:ss;db_integration;sql.syntax_mys=true
        hbm2ddl: create-drop

With mysql database, running in the docker container (see `kiga-setup` project):

    datasource:
      ss:
        loadFixtures: false
        jdbcUrl: jdbc:mysql://0.0.0.0:13306/kiga
        username: root
        password: root
        hbm2ddl: none

### Configure CMS (kigaweb-server)

Checkout the project.

Configuration:

Create a `app/kiga/configs/dev.php` file and set up the local
configuration there. Something like the following for `dev.php`
should work:
_Note : the values for `cacheServerDefault` and `cacheServerMember` are
names of docker controllers._

    <?php
    $databaseConfig['username'] = 'root';
    Director::addRules(45, array(
    'test/$Action/$ID' => 'Test_Controller',
    'screen//$Action/$ID' => 'Screen_Controller'
    ));

    function debug($data) {
    Debug::message(print_r($data, true));
    }
    error_reporting(E_ALL ^ E_NOTICE);

    context('config')->add(
    array(
       'pdfEnabled' => false,
       'feedbackEnabled' => true,
       'licenceEnabled' => true,
       'cacheServerDefault' => 'memcached',
       'cacheServerMember' => 'redis:6379',
       'cacheMemberType' => 'redis',
       'allowCountrySelection' => true,
       'changeCountryViaCookie' => true,
       'showQuickRegisters' => true,
       'jsErrorLogEnabled' => true,
       'showVersion' => true,
       'shopEnabled' => true,
       'showNewSite' => true,
       'showNewSiteTr' => true,
       'logTransitions' => false,
       'clientUrl' => 'http://0.0.0.0:8081/',
       'permissionMode' => 'nonFullTest',
       'endPointUrl' => 'http://0.0.0.0:8080/'
    ));

    Context::register('crm', 'TestCrmClient');
    Context::register('RPCClient', 'TestCrmClient');
    Context::register('countryResolver', 'TestCountryResolver');

    Page::setJsPath('validate', 'jquery.validate');

#### !Some things to watch out for when having silverstripe in the stack!

* in `app/kiga/lang`, language files are sym-linked into the
  kiga-service project, make sure that the files are present at the expected
  relative path (`../../../../system/modules/main/src/main/resources/data/translation/`).

- _port 10081_ To use the site in conjunction with the legacy CMS, the site has to
  be accessed via the following url/port:
  [http://localhost:10081](http://localhost:10081).
  Event better, set `kiga` to point to localhost in your OS `hosts` file,
  and use [http://kiga:10081](http://kiga:10081) - some problems are mitigated with this specific
  setup.
  Similarily, update the `application.dev.yml` of kigasystem accordingly,
  E.g. set `baseUrl: http://kiga:8080` and `ngUrl: http://kiga:10081/ng/#!/`
  in the `web:` section of the config.

- If the site does not load (fully), check browser network tab to find
  some resources that cannot be served 'through' silverstripe.
  (Some JS is now served by silverstripe!).
  Things to try and check if Javascript resources are not served due to PHP errors or other issues in the kigaweb-server container:

  * Clear silverstripe temp data by doing the following:

    * 'login' to the kigaweb-server container with `docker exec -it kiga-server bash`
    * then execute `rm -rf /tmp/silverstripe-cache-usr-local-src-kiga-server-app`

  * Check / create the file at `kigaweb-server/app/kiga.log` and \_make sure
    it is writable.

  * General advice:
    Login to container, install lynx (console browser) and request site from localhost (to be able to see errors) !
    Look for errors in logs, find logs with `find -name '*.log'`, in particular
    try e.g. `cat /var/log/apache2/http_error.log` .

  * Failed to load `http://kiga:8081/bundle.js` : indicates issues with the javascript frontend code (built + served by webpack and its dev server).
    Make sure that webpack is actually running!\_ In dev setting, the bundle
    is not statically served, but served from the webpack-dev server (building and refreshing the frontend code)
    Sometimes issues arise when bundle is expected to be served from host `kiga`.+
    Try this: change the config in kigaweb-server/app/kiga/configs/dev.php.
    (See config param `clientUrl`,
    use either `0.0.0.0` as host or instead of `kiga` - it should be `kiga`, actually !)

  * Sometimes a PHP error is produced by corrupted / outdated content in the database.
    A re-import of a dev dump might help.
    See the helper scripts `reset-kiga-db.sh` and `dev-db-create.sh`
    in the `/kiga-setup` project for help with that.

- To have sessions ('logged in' status), set a cache like this:
  Set `cacheType: redis` (in the `web:` section of the config)

### A Thing About Caches

There are multiple caches and cache-systems in use. Beware.

* redis for content caching
* redis is used for sessions
* memcached is used for some legacy stuff like translations(?)

#### Clear caches

Both caches, redis and memcached, have telnet interfaces.
The cache contents can be cleared by telneting to the specific ports and
issuing `flush_all` for memcached and `flushall` for redis:

    # For Memcached
    $ telnet localhost 11211
    Trying 127.0.0.1...
    Connected to localhost.
    Escape character is '^]'.
    flush_all
    OK
    quit
    Connection closed by foreign host.

    # Redis
    $ telnet localhost 6379
    Trying 127.0.0.1...
    Connected to localhost.
    Escape character is '^]'.
    flushall
    +OK
    quit
    +OK
    Connection closed by foreign host.

### Problem with kiga-server docker

Some stuff to try:

1.  Try to log in / attach shell to container and look around:

        docker exec -it kiga-server bash

2.  Find out where the logs are written to

         docker inspect kiga-server

    This should print a JSON structure with all the info regarding the
    conatiner. See the "Mounts", "HostConfig" and similar sections.
    Currently, it is a volume mont meaning on the host machine the files
    are found somewhere in `/var/lib/docker/volumes`

#### All `bundle.js` contains is "Invalid Host header"

Acessing `http://kiga:10081/ng/#!/de/home`.
Site displays only the loading image.
Console says `Uncaught SyntaxError: Unexpected identifier` in `bundle.js`.
All `bundle.js` contains is "Invalid Host header".

Here, the error was an actual error in PHP / silverstripe.

### Crm

* Checkout repository.
* Start containers with `docker-compose`. You have two chroices:
  1.  start php app and database (recommended unless otherwise required)
      `docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d db webapp`
  2.  start everything (including crontab container that will periodically check for mails to send etc.)
      `docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d`
* Configure `application-....yml` and silverstripe `dev.php` to use
  the local crm urls

### Example application-dev.yml (using services as docker container rather than mocks)

This might be outdated.

    amazon:
      s3:
        bucketName: kigadev
        bucketUrl: https://s3-eu-west-1.amazonaws.com/kigadev
        accessKey: ***
        secretKey: ***

    ideas:
      amazon:
        s3:
          bucketName: kigadev
          bucketUrl: https://s3-eu-west-1.amazonaws.com/kigadev
          accessKey: ***
          secretKey: ***
      amountPerCategory: 20
      threeStarRating: true
      previewImage:
        small:
          width: 138
          height: 96
        large:
          width: 298
          height: 206
      printPreviewImage:
        small:
          width: 60
          height: 84
        large:
          width: 198
          height: 279

    caching:
      hostname: 127.0.0.1
      port: 6379

    crm:
      defaultCrm: 1
      testCrmApi: 0
      url: 'http://localhost:8200/service/v2/'

    content:
      siteMapCron: '0 0 3 * * ?'
      siteMapEnabled: false

    datasource:
      ss:
        # hsql/fixtures:
        # loadFixtures: true
        # jdbcUrl: jdbc:hsqldb:mem:ss;db_integration;sql.syntax_mys=true
        # hbm2ddl: create-drop

        # mysql (local on host):
        loadFixtures: false
        jdbcUrl: jdbc:mysql://0.0.0.0:13306/kiga
        username: root
        password: kiga123
        hbm2ddl: none


      zend:
        # loadFixtures: true
        # jdbcUrl: jdbc:hsqldb:mem:zend;db_legacy;sql.syntax_mys=true
        loadFixtures: false
        jdbcUrl: jdbc:mysql://192.168.1.1:33060/kiga_test
        username: kigasystem
        password: sys4321
        hbm2ddl: none

    garanti:
      mode: TEST
      version: v0.01
      securityLevel: 3D_OOS_PAY
      email: musteri.hizmetleri@kigaportal.com
      auth:
        password: ***
        terminalId: ***
        merchantId: ***
        storeKey: ***

      http:
        url: https://sanalposprovtest.garanti.com.tr/servlet/gt3dengine

      callbacks:
        protocol: http
        host: localhost
        port: 8080

    jasypt.encryptor.password:

    logging:
      level:
        root: ERROR
        com.kiga: INFO

    main:
      showNewSite: true
      showNewSiteTr: true

    mail:
      redirectMails: true
      redirectAddress: log #email address or log for log output
      mockSmtp: true
      gmail: false
      defaultSender: kiga-system@kigaportal.com

    mpay24:
      http:
        url: https://test.mpay24.com/app/bin/etpproxy_v15
        protocol: https
        host: test.mpay24.com
        port: 443
        basePath: /app/bin/etpproxy_v15
      shop:
        merchantId: 94047
        username: u94047
        password: ***
      abo:
        merchantId: 93043
        username: u93043
        password: ***

    kiga:
      asyncExceptionRecipient: log

    print:
      canSaveToDb: false
      enableValidation: false
      cronExpression: '0 0 3 * * 4'

    search:
      elastic:
        # mockElasticSearch: true
        mockElasticSearch: false
        host: 127.0.0.1
        port: 9300

    shop:
      scheduledShopPaymentChecker: false
      mpay24ShopCheckerCron: '0 0 6-20/2 * * *'
      shopEmail: KiGaPortal Shop<shop@kigaportal.com>
      bucketName: shop-dev
      accessKey: ***
      secretKey: ***
      amountPerCategory: 13
      previewImage:
        small:
          width: 138
          height: 96
        large:
          width: 298
          height: 206

    spring.jpa.show-sql: false

    system:
      kiga:
        ccSecret: ***

    translation:
      baseLocale : de
      defaultLocale : en
      locales:
        de: DE
        en: US
        it: IT
        tr: TR

    uploadModule:
      temporaryDirectory: /tmp
      optipngExecutable: optipng
      rasterizerExecutable: rasterizer
      pdf2SvgExecutable: pdf2svg
      imageVariants: # put values in format WxH where H is height and W is width
        - 49x51

    web:
      baseUrl: http://kiga:8080
      url: http://kiga:10081
      ngUrl: http://kiga:10081/ng/#!/
      # baseUrl: http://localhost:8080
      # url: http://localhost:10080
      cookieName: PHPSESSID
      exceptionRecipient: log
      memcachedUrl: 127.0.0.1:11211
      mockMemcached: false
      systemName: KiGaPortal Development
      # geoIp2CountryDatabase: GeoLite2-Country.mmdb
      enableMockedUserIpResolver: true
      cacheType: mocked
      # cacheType: mocked # memcached | redis | mocked
      # mockedUserId - if provided, will return that value. if empty, the resolver will resolve
      # the ip based on RemoteAddr.
      # mockedUserIp: 93.184.216.34 # example.com

    cache:
      url: localhost
      port: 6379
      defaultTimeout: 900
      enabled: false # true
      useCache: false
