# How to set up Environment
## What you will need to install

Firstly you should install the Ubuntu OS and in the next steps you should install:
- Intellij Ultimate
- Docker CE and Docker Compose
- npm (I'm running version 5.7.1)
- node (I'm running version v6.11.4)
- Java (I've downloaded jdk-8u162-linux-x64)
- git

##### Intellij Ultimate
Just download Intellij through following link:
https://www.jetbrains.com/idea/download/#section=linux

You will need some Plugins for TypeScript etc. but usually Intellij recognizes the missing plugins
when starting the project and asks you to install them. Just click ok and download it.

Configure Intellij to put imports into correct order:

Go to Settings -> Editor -> Code Style -> Java then click on tab Imports
then put import settings like so:

![picture of the settings window for intellijimports](importconfiguration.png)

##### Docker CE
https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce

Execute following commands to install Docker CE
(also available on the website, which you can reach with the link above)

SET UP THE REPOSITORY:
- `sudo apt-get update`

- `sudo apt-get install \
      apt-transport-https \
      ca-certificates \
      curl \
      software-properties-common`

- `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

INSTALL DOCKER CE:

- `sudo apt-get update`

- `sudo apt-get install docker-ce`

- `sudo docker run hello-world` (to check if docker was installed correctly)

THERE ARE POST-INSTALLATION STEPS FOR LINUX:

To be able to use docker without root:

- `sudo groupadd docker`

- `sudo usermod -aG docker $USER`

- `docker run hello-world` (to check if it works using docker without sudo)

- `sudo systemctl enable docker`

##### Docker Compose

-`sudo curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-`uname -s`-`uname -m`
  -o /usr/local/bin/docker-compose`


##### NodeJs

I have downloaded NodeJs via following link:

https://nodejs.org/en/download/releases/
(I took the node-v6.11.4-linux-x64.tar.gz package)

That node can work you have to make a symbolic link:
- `sudo ln -s node /usr/local/bin/node`

To check if it work just run node in the commandline.

##### Npm
- `sudo apt install npm`

- `sudo npm install npm@latest -g`

##### Java
Download the Java SE Development Kit 8:
I've downloaded the jdk-8u162-linux-x64.tar.gz on following website:
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

Then just extract the package and move the extracted folder to /usr/local/src.
In the next step create a symbolic link:
- `sudo ln -s jdk1.8.0_162 jdk`

Then you have to set the JAVA HOME path variable:
- `export JAVA_HOME=/usr/local/src/jdk`

or that command if you are using the fish shell:
- `set -U  JAVA_HOME /usr/local/src/jdk`

Maybe you also will need to create following symbolic link:
- `sudo ln -s /usr/local/src/jdk/bin/java /usr/local/bin`

##### Git
- `sudo apt-get install git`


### You need all four Git Repositories of KigaPortal

- `git clone http://git.kigaportal.com/hahnerai/kiga-system.git`
- `git clone http://git.kigaportal.com/hahnerai/kigaweb-server.git`
- `git clone http://git.kigaportal.com/hahnerai/kiga-db.git`
- `git clone http://git.kigaportal.com/hahnerai/kiga-setup.git`

Put them in a folder and make a symbolic link because of Intellij Settings
- `ln -s YourFolderWithTheRepositories idea`


### Some steps to set up the environment

- `sudo docker-compose -f docker-compose-dev.yml up -d`

You will also need access for the aws (Amazon Web Service).
There you have to create Key and Secret Key, which you will need for login.
This will make Matthias or Rainer for you and then you have to download the aws commandline interface:
- `sudo apt install awscli`

- `aws configure`
(execute this command after you have that access )

- `aws ecr get-login --region eu-west-1  `

After executing the command above you get some output like this:

docker login -u AWS -p
eyJwYXls........joxNTIwNTU0NTQyfQ== -e none https://567748011851.dkr.ecr.eu-west-1.amazonaws.com

Take that output and execute it in the commandline but without `-e none`

- `docker login -u AWS -p eyJwYXls........joxNTIwNTU0NTQyfQ https://567748011851.dkr.ecr.eu-west-1.amazonaws.com`

To see the database start kiga-db docker
- ` docker exec -it kiga-db mysql -u kiga -pkiga123`

It could be that there is a problem with kiga user, then try root instead:
- ` docker exec -it kiga-db mysql -u kiga -pkiga123`

Then you can check if kiga database exists. If not you have to download a dumb of it.
###### Download dump of the kiga-system databse.

Secret keys must be provided outside of the script (e.g. set, the
environment accordingly)
Do this e.g. by ...

- `export AWS_ACCESS_KEY_ID=<your personal aws key id>`
- `export AWS_SECRET_ACCESS_KEY=<your personal aws key secret>`
(You need the Key and Secret Key which you created in the above steps)

- `docker run \
--env AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
--env AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
--env AWS_DEFAULT_REGION=eu-west-1 \
--rm -v "$PWD:/data" \
garland/aws-cli-docker \
aws s3 cp s3://kigadbbackup/dump-dev.sql.gz /data/dump-dev.sql.gz
gunzip -f dump-dev.sql.gz`

to load the dump into the dev database, execute following commands:
- `docker cp dump-dev.sql kiga-db:/dump-dev.sql`
- `docker exec -it kiga-db mysql -u kiga -pkiga123`
- `source /dump-dev.sql`

In the next step you have to add kiga in the hosts file:
run:
- `sudo nano /etc/hosts`

and add following line in the file:

127.0.0.1       kiga


Then you have to create teh file dev.php in the kiga-webserver project. Ask Rainer or Matthias for
the settings in that file.

If you have done all these steps go in the kiga-system project and run
- `npm install`

- `npm run yarn`


If there are still problems with the environment have also a look in DevelopmentEnvironment file.
There are some steps, which you can try.
