# Deployment

# Notes About Deployment

* deployment can be done via jenkins
* to revert back, set the old AMI in ec2

## Jenkins

Jenkins is accessible via [http://ci.kigaportal.com/](http://ci.kigaportal.com/)

## Deployment

* simply select the 'Deployment' task in jenkins

## Revert Deployment (Switch to an old revision)

* The auto scaler (aws) has a launch configuration
* Choose the desired (old) ami for the launch configuration
* Then (to make the config take effect), launch current number of servers x2 (doubled)
* Then, reduce the number of servers again (auto scaler will turn off the oldest running servers, thus we end up with the new instances that we want)

[./auto-scaler-config.png]

1.  Go to EC2, "Auto Scaling Groups"
2.  choose the config / auto scaling group (probably named "CMSAutoScalingGroup") from the list
3.  Choose "Edit" to edit the config
4.  In the form, select the desired (old) config for field "Launch Configuration"
5.  Set "desired servers" to current number x 2 (e.g. , 6)
6.  Wait, then edit again and set desired servers back (e.g., 3)

## Webtests

* Webtests are a requirement for deployment.
* _But:_ it is possible to deploy without passing webtests (for 'emergency' reasons) !

If tests fail:

* select the 'webtests' job in jenkins.
* select 'Allure Report' from the job page to inspect the reasons for the failing test(s)

# Deployment setup

The deployment setup and -scripts are found in the repoistory `kiga-setup`.

# Manual Deployment

Check that all builds pass (Jenkins), and also that the staging
site (`stage.kigaportal.com`) is up and running (current build works).

Log in to stage (`kigastage`).
Use the program `screen` to be safe when console / terminal dies !
[linux screen tutorial and howto](https://www.rackaid.com/blog/linux-screen-tutorial-and-how-to/)

S3 static files (angular)
is removed and newly created

The script `kiga-setup/deploy.sh` does all the preparations for the deploymnet.
With `lein run $TAG` , the last line of that file, the deployment is actually carried out.

A side note:
For breaking changes in database (remove a column etc.)

* split this up into two deployement
* 1st, remove the code that accesses the schema
* 2nd, do the database schema migration

The script requires the `aws` command line tool
e.g `aws-cli/1.11.10 Python/2.7.12 Linux/4.4.0-70-generic botocore/1.4.67`
