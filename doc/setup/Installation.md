# Installation

## Server-side

BThe build system is Gradle. One can invoke it by simple call gradlew in the root directory of the project. Gradlew itself
is a gradle wrapper that takes care for installing and configuring the right gradle version.

As IDE IntelliJ is recommended, but it works also without it.
IntelliJ automatically recognizes that the project is using Gradle and supports the loading of the dependencies.

To run the web application run the task bootRun by executing

    $ ./gradlew :modules:main:bootRun

It starts Spring with default @Profile dev.

## Client-side

To run the frontend make sure you are on the current `npm` version and have `docker` and `yarn` installed. For the initial setup, install the node dependencies with.

    $ yarn

After that the frontend can be started via:

    $ yarn start

Make sure, that you also start the server-side application.

Do not forget to check that any `safe write` feature from your IDE is disabled.

## Verify installation and access the site

To verify the local system is up and running, point your browser to `http://localhost:8081/`
(should display a the homepage in English after a loading symbol).

For logging make sure that you have a application-dev.yml which has set the property `web.cacheType`
to value `mocked`. A dev menu should appear on the left side where you can choose
between some predefined members.
