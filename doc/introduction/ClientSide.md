# Client-side

## Basic Architecture

The application consists of two parts. A legacy AngularJS and an Angular one. Building is done via AngularCLI which also includes the AngularJS build. The npm script `start` runs first the build for AngularJS and afterwards starts AngularCLI. Both system have watchers, so you don't have to restart anything.

The most important modules in the Angular application are:

- app: The app and root module. It should include only minimal logic, which means setting up the routes and delegating them to the other modules.
- main: Is responsible for the container and represents the canvas. It loads the bootstrap css framework, renders the footer, header and is also responsible for bootstrapping the application.
- base-data: This is the global state that contains information about major links, locales and translation texts. It's data is set by the main module.
- shared: This module contains services and components that can be used by other feature modules.
- endpoint: Provides the required service to access the endpoints.
- compat: Contains services from Angular that are downgraded to AngularJS.
- data: Exists but should be slowly migrated to ngrx. Was used during the initial phase of the migration to Angular when we didn't have ngrx.

As said, the main module is responsible for loading the base data, the member and the main layout container. We have a set of global data which contains text messages for the different languages and main links. These data needs to be accessed by each module. As we don't want our feature modules to have a dependency to main, that data is part in the module `base-data`.

When the application starts, the so-called bootstrapping process is initiated by the main module. There, we load the base data from the backend. Until the loading is in process, no route is loaded. This is secured by an AuthGuard BootstrapperGuard.

If a user switches the locale, the bootstrapping process is repeated since we require new links and new translation messages. In this case the BootstrapperGuard prevents any route to be activated.

You can find more information in the module declaration file.

## Presentational and Container components

The client-side code for the webapp can be found in `src/app`.

Our application should consist of reusable components. Each component should manage its own styling. This means that we end up with a very minimised set of global styling rules. In the best case it is just the bootstrap framework.

The components should be split up into presentational and container components. The presentational components should have no dependencies to any services. The only input should come from the `@Input` injection and these types should be primitives ones. So only strings, numbers but no Observables or even Angular services.

Container components should contain all the logic and should act as kind of wrapper for presentational components. They should only prepare or transform data coming from Observable or other Angular services and pass it own to its wrapped presentational component.

Usually, container and presentational components belong together. A presentational component `foo.component` would have a `foo-container.component`:

```typescript
@Component({
  template: '<foo [title]=title></foo>'
})
export class FooContainerComponent {
  public title: string;

  constructor(private title$: Observable<string>) {}

  ngOnInit() {
    this.title$.subscripe(title => (this.title = title));
  }
}

@Component({
  template: '<h1>title</h1>',
  selector: 'foo'
})
export class FooComponent {
  @Input() title: string;
}
```

## ngrx

We use ngrx for state management. If your component has a local state, e.g. the data isn't required anywhere else, don't put it into ngrx. Huge parts of the global state can be found in module base-data.

Since no feature module should have a dependency to the app.module, this also means that we can't inject the store with AppState like `store: Store<AppState>`. Instead we simply use `store: Store<any>` and use the selectors of the various modules.

Please make sure that always unsubscribe from a Subscription, by either using the async pipe or in the ngOnDestroy method.

ngrx dev-tools runs in non-production mode.

## Testing

The presentational components should be test with the Snapshot feature of Jest. If the layout changes depending on the input, we should create a snapshot for each variety.

Container components should be tested by classical unit testing techniques which also includes mocking.

## Storybook

Each "presentational component" should be registered to Storybook. This make it easy to identify, if the components depends on some global styling rules.

## Feature Toggles

Feature Toggles are set in the backend. For a more info go to [Server-Side](ServerSide.md).

The feature toggles are loaded during angular's bootstrap and are managed via the config service. Each feature toggle gets an enum. This makes it easy to see, where a specific feature toggle is used.

## Swagger

For Swagger-enabled controllers use the request and response types generated by the code generator.

## Server-Side Rendering

We use Angular's SSR feature. The code for the server is located at `/server.ts`. This is a techology that allows to render the home on the server-side and
return the resulting html directly to the client. The browser immediately displays the finished rendered page without the need to load Angular or even JavaScript.

Once the site has been delivered to the browser, it automatically downloads the actual Angular application and reruns it on the existing page.

In order to build the SSR-version, following commands have to be executed:

1. `npm run build` // builds the normal Angular application
2. `ng run kiga:server` // builds the browserless Angular application on top of the existing one
3. `npm run webpack:server` // builds the actual server which includes an express instance and servces the static, rendered files.

This can also shortened by running `npm run build:ssr`. Executing the three commands separately makes only sense, when working on server-specific
files, where one doesn't have to build the two Angular application versions all the time.

### Running SSR in development mode

`npm run serve:ssr` starts the express server on port 8082.

`npm run server:ssr-full` should be used, when working in an environment where SilverStripe is available.

### Running SSR on stage

SSR runs in the same way as the production configuration. To run it in listening mode (like development mode) and without
the generation and upload of the html files, run following command:

`docker run -dit --env-file ./kiga-system-stage.env -e KIGA_DEBUG=1 -p 81:80 kiga-ssr node dist/server`.

This runs the express instance which is listening on port 81.

### Running SSR in production

In production, no express is listening on a port and answering requests. Instead, we use express to render the home page for
all available locales and upload the html file to Amazon S3. From there it used by our nginx instances in production.

The complete SSR version is put into a Docker container which is located along the scripts to build and run in `/containers`.

The generation of the html files is managed via a cronjob.
