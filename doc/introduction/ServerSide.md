# Server-side

The code for the server-side applications can be found in the directory `modules`. The main module is called `main` and the module containing integration tests is `webtests`.

### Module structure

If beans are dependent on conditions, like enabling or disabling feature via the configuration,
we should have an own @Configuration class that instantiates that bean per module.

When Kiga-System is booted up the Application uses MainConfig which does a component scan on com.kiga and is therefore
loading every module.

As stated below, if database entities are involved, add additional sub packages for domain and repositories.

### Swagger / OpenAPI

With the introduction of the `kga` module, we use Swagger/OpenAPI to generate our endpoints. That module also serves as reference for all other modules.

Each module has its own Swagger file which is located in `modules/main/swagger` along with its configuration file.

The code generation for both Angular and Spring is done via scripts that run the openapi generator. These scripts are located in `/scripts`.

The configuration for Spring can be copied from here:

```json
{
  "apiPackage": "com.kiga.foo.api.controller",
  "dateLibrary": "java8",
  "delegatPattern": true,
  "hideGenerationTimestamp": true,
  "interfaceOnly": true,
  "library": "spring-mvc",
  "modelPackage": "com.kiga.foo.api.model",
  "useBeanValidation": true,
  "useTags": true
}
```

We put the controller interfaces into the module's subpackage `controller` and the model (response, request) classes into `model`.

The documentation of the endpoints is available in dev profile under `http://kiga:8080/swagger-ui.html`.

### Profiles and Properties

Properties are stored within `application.yml` which allows to structure them in a hierarchical form. Use the
module's name as prefix.
The properties shouldn't be directly injected but be provided by an own Properties Bean.
For example having module `mail` with properties `email` and `password` in the `application.yml`, a bean that gets them injected should look like following:

```yaml
# application.yml
mail:
  email: kiga@kigaportal.com
  password: kigapassword
```

```java
//MailProperties.java
@Configuration
@ConfigurationProperties("mail")
@Data
public class MailProperties {
  private String email;
  private String password;
}
```

```java
public class Mailer {
  private String email;
  private String password;

  public Mailer(String email, String password) {
    this.email = email;
    this.password = password;
  }

  ///code that does something
}
```

```java
@SpringBootApplication
@Configuration
public class MailConfig {
  @Bean
  public Mailer getMailer(MailProperties mailProperties) {
    return new Mailer(mailProperties.getEmail(), mailProperties.getPassword());
  }
}
```

In default mode the application is started with dev profile. Since it should be possible for every developer to have an individual configuration there exists an application.yml which holds the default configuration and one can overwrite these values by creating an application-dev.yml . For this reason application-dev is in .gitignore.

New parameters should be added in application.yml.

As stated above dev mode should stay individual so using @Profile("dev") or using @Profile at all should be avoided whenever possible.

### Testing

The gradle task "test" runs by default all unit tests except from pacakge `integration.com.kiga`. If only a selection of test should be run like only a package, one can create a file gradle-local.properties and overwrite the value testIncluded with an individual selection.

For example if only tests from package com.kiga.test should be run following entry should be added:

    testIncluded=com/kiga/test/**

Tests should be written in JUnit and are executed by:

    ./gradlew :modules:main:test

Webtests tests are covered by the module `webtest`.

Use `MapFetcher` and `MockedPersister` for simple database related tasks.

**We require a test coverage of 100%**. That means that all getter/setter methods, controller, config and even properties
have to be tested. To see the current test coverage run:

`./gradlew :modules:main:jacocoTestReport`

The report along detailled information
can then be found in `./modules/main/build/reports/jacoco/test/html/index.html`.

- Getter/Setters: Usually one does not require explicit tests for getter and setter methods since they are already
  covered by other unit tests that use these classes.
- Controller: Testing can only be done when all beans are injected via the constructor. Try to keep the controller small by moving logic to specific services. For large controllers one should have a service per endpoint. If that is the case the only thing that needs to be tested is, if that service is called at all. Testing the service's logic is not the task of the unit test of the controller.
- Configuration: Spring beans with @Configuration annotation can only be tested if their bean methods get the dependent beans injected directly via the method itself. Autowiring on properties is not ideal. In contrast to standard beans constructor based injection does not work on Configuration beans.

#### BeanMatchers:

One can use the `FullBeanTest` that runs a full test on a Java Bean. Keep in mind that only the empty constructor is tested. So if you have further constructors you have to write your own tests for them.

All classes ending on `*Response` or `*Request` are automatically covered by `com.kiga.AllRequestResponseTest`. Empty constructor is mandatory for these classes to ensure caching is working correctly.

#### Mapping

Since we are using request and response object a lot, many code needs to be written, that deals with mapping from one object to another. To reduce the workload, you should use the library **MapStruct**.

There are also usages of **ModelMapper** in this project, which is declared as deprecated. The only exception are edge cases (Publishing of SiteTree Entities), where ModelMapper
is the better tool of choice.

For an example, just search for interfaces annotated with `@Mapper`.

#### Integration Tests

In order to run the integration tests defined in integration.kiga.com, one has to put following two lines into `gradle-local.properties`:

- `testInclude=integration/com/kiga/db/**`
- `testExclude=com/kiga/**`

### Dependencies

Dependencies are set in build.gradle. We are using the parent.pom of Spring Boot which specifies already the correct version number for the Spring dependencies and also other ones commonly used. Please make sure, that you do not override those settings.

Build.gradle contains a basics part which sets all libraries that are needed to run the system or can and should be shared by the modules.

The module part contains the dependencies which are specific to a module. If you are about to add a new library please check if there isn't already another module which provides that functionality in use. If that is the case move it to the basics/misc section.

### Database

Because of legacy reasons there are two databases. That lead to two Configuration classes which need to be extended accordingly. By default the SSDatabaseConfig should be used which is the database of the system based on SilverStripe.

#### Setup

If a new module with entities is created one has also to set them up in one of the two Database Configurations by adding at least one Entity class to the basePackageClasses of the EnableJpaRepositories annotation. Further more one Repository class also needs to be added within the EntityManagerFactory bean.

Note that only one class is needed since Spring Data will scan the complete package and all sub packages of the class.

If a module has entities in both databases the domain and repository need to get a sub package for each database. See the module accounting for a working example.

Entities should be put in the sub-package domain and their repositories into the package repository.

#### Usage of Persister and Fetcher

Spring-Data is used, but for the sake of modularity, use the Fetcher and Persister
interface from com.kiga.spec for simple database tasks. You can then use the RepositoryMapper class to map the interface from com.kiga.spec to its appropriate Repository.

The middle-term plan foresees to transform kiga-system into a multi modular application where we want keep the heavy dependency on Spring and Spring-Data out of the game as long as possible.

So given a simple service that just does finding by id and persisting:

```java
@Service
public class SimpleService {
  private Fetcher<Product> fetcher;
  private Persister<Product> persister;

  @Autowired
  public SimpleService(Fetcher<Product> fetcher, Persister<Product> persister) {
    this.fetcher = fetcher;
    this.persister = persister;
  }

  @Override
  public Product findAndPersist(Long productId) {
    Product returner = fetcher.find(productId);
    returner.setTitle("Some Title");
    persister.persist(returner);
    return returner;
  }
}
```

It is now possible to create a unit test for SimpleService since we do not require to run Spring or having a database at all. One can simply mock the fetcher and persister or use the existing ones for test usage in com.kiga.test.

#### Naming

All properties of the entites should be named according to Java standard in camelCase with first char in lowercase.

The properties are mapped to database column name by upper casing the first letter.

#### Date & Time

Every time a current date is required the `NowService` should be used.

It provides the current time as `Instant`, `ZonedDateTime`, and `Date`, although latter is deprecated.

This has the advantage that each time type is set to the same timezone (Europe/Vienna) and that the
classes are better testable.

#### Fixtures

Fixtures are used in development mode by default and are stored in the in-memory database. For each entity one can add a file which should have the same name as the entity. That file is in yaml format and needs to be registered additionally in the method loadFixturesFromFile in the class com.kiga.db.fixture.DefaultFixtureLoader.

It is not possible to set an individual id for an entity, since the ids are autogenerated. Each entity record in the yaml has a special property called _key which can be used if you want to join that entity to some other. Please note that you need to add the prefix _ to the key in the field that needs to be joined.

For example entity SiteTree has a child-parent relationship. One would setup a parent and child fixture like following:

    _key: root-page_
    title: Root Page
    ---
    _key: child-page_
    parentId: _root_page
    title: Child Page

The corresponding entity class would look like:

```java
@Entity
@Data
class SiteTree {
  private Long id;
  private String title;

  @ManyToOne
  @JoinColumn(name = "parentID", referencedColumnName = "id")
  private SiteTree parent;
}
```

For annotations `@OneToMany`, you have to keep in mind two things:

- You **must not** use `@JoinColumn`.
- You **must** add the `mappedBy` property.

Otherwise Hibernate deletes and re-creates references:

```java
@Entity
@Data
class Foo {
  @OneToMany(mappedBy = "foo")
  private List<Bar> bars;
}

@Entity
@Data
class Bar {
  @ManyToOne
  @JoinColumn(name = "barId", referencedColumnName = "id")
  private Foo foo;
}

```

Please note that in the class the property has the name **parent**, whereas in yaml it is **parentId**. The id appendix is required for all relation fields. This is due to hibernate which requires a persisted entity object for persisting the relation and is not satisfied with the id only.

### Mailing

For sending emails one does not need to have a working smtp server. Smtp server library (aspirin) can be enabled via the configuration property `mail.mockSmtp`.

It is recommended to set `mail.redirectMails` to true and `mail.redirectAddress` to one's personal email. That would send every email - regardless of the recipient - to the configured address.

By setting `mail.redirectAddress` to value "log", all emails will not be sent at all but passed the logging component.

In default mode logged output is used.

Working examples can be found in test under package com.kiga.mail.

### Logging

Log4j2 is used as logger. The configuration can be set via `application-{profile}.yml` by overwriting the default settings of logging `in application.yml`.

### Feature Toggles

We try to avoid feature branches and use feature toggles instead. So if we are working on a new feature or some modification then still push to master branch, but have different code branches. These can be if-conditions, different beans with the same interface or similar.

In the configuration file we have a section `features`. There we create a new boolean entry which enables or disables the specific feature. That property is added to the `FeatureProperties` class. A `@Config` class or a Spring Bean can then inject the `FeatureProperties` bean and adapt its behaviour depending on the feature toggle.

For example we introduce a new feature `enableGoogleLogin`.

**Step 1:** We add this entry to our application.yml and set its default value to `false`. Since we require it enabled in our dev setup, we add it also in application-dev.yml and set it to `true`.

```
features:
  ...
  enableGoogleLogin: true
  ...
```

**Step 2:** We add the same property to the class `FeatureProperties` and make "static typed".

```java
@Data
public class FeatureProperties {
  ...
  private boolean enableGoogleLogin;
  ...
}
```

**Step 3a:**
We can then inject the `FeatureProperties` in a configuration class and create a Spring bean depending on the value

```java
@Configuration
public class FooConfiguration {
  ...
  @Bean
  public Login getLogin(FeatureProperties featureProperties) {
    if (featureProperties.isEnableGoogleLogin()) {
      return new GoogleLogin();
    } else {
      return new DefaultLogin();
    }
  }
  ...
}
```

**Step 3b:**
One can also use it within a bean if the changes are not that big:

```java
@Service
public class FooBean {
  private FeatureProperties featureProperties;

  @Autowired
  public FooBean(FeatureProperties featureProperties) {
    this.featureProperties = featureProperties;
  }

  public void doSomething() {
    if (this.featureProperties.isEnableGoogleLogin()) {
      doSomethingWithGoogle();
    } else {
      doSomethingWithoutGoogle();
    }
  }
}
```
