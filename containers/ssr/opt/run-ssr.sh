#!/usr/bin/env sh

cd /
for locale in de en it
do
  node dist/server $locale
done
