#!/usr/bin/env sh

sed -ie "s|{\$ClientUrl}|$KIGA_CLIENTURL|g" /opt/index-template.html
sed -ie "s|{\$EndPointUrl}|$KIGA_ENDPOINTURL|g" /opt/index-template.html
sed -ie "s|{\$LegacyUrl}|$KIGA_LEGACYURL|g" /opt/index-template.html
sed -ie "s|{\$NoCookiesMsg}|$KIGA_NOCOOKIESMSG|g" /opt/index-template.html
sed -ie "s|{\$JsTimeoutMsg}|$KIGA_JSTIMEOUTMSG|g" /opt/index-template.html
sed -ie "s|{ \$requiresSsl }|$KIGA_REQUIRESSSL|g" /opt/index-template.html
sed -ie "s|\$NoJavascriptMsg|$KIGA_NOJAVASCRIPTMSG|g" /opt/index-template.html
sed -ie "s|\$BaseHref|$KIGA_BASEHREF|g" /opt/index-template.html

cp /opt/index-template.html /dist/kiga/index.html

sh /opt/run-ssr.sh
crond -f
