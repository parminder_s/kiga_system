#!/usr/bin/env bash

cd ..
cp -r dist containers/ssr/
cd containers/ssr
docker build -t kiga-ssr .
