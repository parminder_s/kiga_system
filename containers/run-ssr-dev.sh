#!/usr/bin/env bash

docker run -it --rm --env-file=ssr/variables.env -v $PWD:/out -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY --add-host="kiga:172.17.0.1" kiga-ssr
