module.exports = {
  module: {
    rules: [
      {
        test: /\.less$/,
        use: ['to-string-loader', 'style-loader', 'css-loader', 'less-loader']
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)$/,
        use: ['file-loader']
      }
    ]
  }
};
