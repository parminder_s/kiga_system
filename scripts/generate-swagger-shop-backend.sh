#!/usr/bin/env bash

rm -rf src/app/openapi/shopBackend/api
rm -rf /modules/main/src/main/generated/swagger/src/main/java/com/kiga/shop/backend

docker run -u `id -u $USER` --rm -v ${PWD}:/local openapitools/openapi-generator-cli:v3.2.2 generate \
  -i /local/modules/main/swagger/shopBackend.yml \
  -c /local/modules/main/swagger/shopBackendClient.json \
  -g typescript-angular \
  -o /local/src/app/openapi/shopBackend

docker run -u `id -u $USER` --rm -v ${PWD}:/local openapitools/openapi-generator-cli:v3.2.2 generate \
  -i /local/modules/main/swagger/shopBackend.yml \
  -c /local/modules/main/swagger/shopBackendServer.json \
  -g spring \
  -o /local/modules/main/src/main/generated/swagger

