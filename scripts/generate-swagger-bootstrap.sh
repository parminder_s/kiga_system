#!/usr/bin/env bash

rm -rf src/app/openapi/bootstrap/api
rm -rf modules/main/src/main/generated/swagger/src/main/java/com/kiga/bootstrap/api

docker run -u `id -u $USER` --rm -v ${PWD}:/local openapitools/openapi-generator-cli:v3.2.2 generate \
  -i /local/modules/main/swagger/bootstrap.yml \
  -c /local/modules/main/swagger/bootstrapClient.json \
  -g typescript-angular \
  -o /local/src/app/openapi/bootstrap

  docker run -u `id -u $USER` --rm -v ${PWD}:/local openapitools/openapi-generator-cli:v3.2.2 generate \
  -i /local/modules/main/swagger/bootstrap.yml \
  -c /local/modules/main/swagger/bootstrapServer.json \
  -g spring \
  -o /local/modules/main/src/main/generated/swagger
