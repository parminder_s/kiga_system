"""
Roles Helper

This outputs queries related to inserting roles / groups for a
member (specified by id).
"""

import sys

usageMessage = """Please specify user id the names of roles:
\tpython roles-helper.py 1234 publishers translator_en"""

if len(sys.argv) < 3:
    exit(usageMessage)

userId = sys.argv[1]
roles = sys.argv[2:]

print("-- Make sure you got the right user-id and don't insert duplicates!")
print("-- Current roles of user {}:".format(userId))
print("""
select gm.ID, Email, Member.ID, Code, Group.ID from Member
join Group_Members gm on Member.ID = gm.MemberID
join `Group` on Group.ID = gm.GroupID
where Member.ID = '{}';
""".format(userId))

print("-- These are the inserts for"
      " your roles for user {}: {}".format(userId, ','.join(roles)))
for r in roles:
    print("insert into Group_Members (GroupID, MemberID)"
          " select ID, {} from `Group` where Code = '{}';".format(userId, r))
