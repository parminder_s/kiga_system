docker run --rm -v ${PWD}:/local openapitools/openapi-generator-cli:v3.1.0 generate `
  -i /local/modules/main/swagger/home.yml `
  -c /local/modules/main/swagger/home.json `
  -g typescript-angular `
  -o /local/src/app/openapi/home
