rm -r ${PWD}\src\app\openapi\kga
rm -r ${PWD}\modules\main\src\main\generated\swagger\src\main\java\com\kiga\kga

docker run --rm -v ${PWD}:/local openapitools/openapi-generator-cli:v3.2.2 generate `
  -i /local/modules/main/swagger/kga.yml `
  -c /local/modules/main/swagger/kgaClient.json `
  -g typescript-angular `
  -o /local/src/app/openapi/kga

docker run --rm -v ${PWD}:/local openapitools/openapi-generator-cli:v3.2.2 generate `
  -i /local/modules/main/swagger/kga.yml `
  -c /local/modules/main/swagger/kgaServer.json `
  -g spring `
  -o /local/modules/main/src/main/generated/swagger

