"""
User Helper

For a username and password, this outputs sql queries for changing
the user record ( + calculates encrypted pass for a randomly generated salt).
"""

import sys
import hashlib
import os

usageMessage = """Please specify email and password ! e.g.:
\tpython user-helper.py some@email.com hard2guess234. For some reason you will have to execute this script twice."""

if len(sys.argv) < 3:
    exit(usageMessage)

email = sys.argv[1]
password = sys.argv[2]

salt = os.urandom(25).encode('hex')
hashedPass = hashlib.sha1(password + salt).hexdigest()

print("--- Command for creating the user")
print("""
insert into Member(FirstName, Surname, Email, Password, PasswordEncryption, Locale) 
  values('foo', 'bar', '{}', '', 'sha1_v2.4', 'de_DE');
""").format(email)
print("--- Command for setting the password")
print("""
update Member set
CRMID = NULL,
Password = '{}',
PasswordEncryption = 'sha1_v2.4',
Salt = '{}'
where Email = '{}';
""".format(hashedPass, salt, email))
