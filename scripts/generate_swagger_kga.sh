#!/usr/bin/env bash

docker run -u `id -u $USER` --rm -v ${PWD}:/local openapitools/openapi-generator-cli:v3.2.2 generate \
   -i /local/modules/main/swagger/kga.yml \
   -c /local/modules/main/swagger/kgaClient.json \
   -g typescript-angular \
   -o /local/src/app/openapi/kga

docker run -u `id -u $USER` --rm -v ${PWD}:/local openapitools/openapi-generator-cli:v3.2.2 generate \
   -i /local/modules/main/swagger/kga.yml \
   -c /local/modules/main/swagger/kgaServer.json \
   -g spring \
   -o /local/modules/main/src/main/generated/swagger

