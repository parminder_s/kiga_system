var _ = require('lodash'),
  path = require('path'),
  webpack = require('webpack');

module.exports = {
  entry: {
    bundle: './app/src/app.ts'
  },
  output: {
    path: require('path').resolve('bundle/src'),
    chunkFilename: '[name].[hash].js',
    filename: '[name].js',
    jsonpFunction: 'ng1Jsonp'
  },
  resolve: {
    modules: ['app/src', 'node_modules'],
    extensions: ['.ts', '.js', '.json', '.coffee'],
    alias: _({
      form: 'app/src/misc/form/form',
      'jquery.ui.widget': 'node_modules/jquery.ui.widget/jquery.ui.widget.js',
      main: 'app/src/main/js/main',
      member: 'app/src/member/js/main',
      misc: 'app/src/misc/js/main',
      planner: 'app/src/planner/js/main',
      registration: 'app/src/registration/js/main',
      shop: 'app/src/shop/js/main',
      translation: 'app/src/backend/translation/js/main'
    })
      .map(function(value, key) {
        return [key, path.resolve(__dirname, value)];
      })
      .fromPairs()
      .value()
  },
  plugins: [
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /de|en/),
    new webpack.ProvidePlugin({
      'window.jQuery': 'jquery',
      jQuery: 'jquery',
      $: 'jquery'
    })
  ],
  module: {
    loaders: [
      {
        test: /\.ts$/,
        use: [
          {
            loader: 'awesome-typescript-loader',
            options: {
              configFileName: 'tsconfig.old.json'
            }
          }
        ]
      },
      {
        test: /\.coffee/,
        use: [{ loader: 'coffee-loader' }]
      },
      {
        test: /\.(png|gif|jpg|html)$/,
        use: 'file-loader'
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)\??\w*$/,
        loader: 'file-loader',
        options: { name: '[name].[ext]' }
      }
    ]
  }
};
