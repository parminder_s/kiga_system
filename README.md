# Kiga-System

Kiga-System is a a web application based on Spring Boot and Angular/AngularJS.

The system exists as live, stage and dev instance:

* Live: www.kigaportal.com
* Stage: stage.kigaportal.com
* Dev: dev.kigaportal.com

The client-side code for AngularJS is located in the directory `/app`, Angular in `/src` and server-side in
`modules`. The main module is `main`.

* [Installation](doc/setup/Installation.md)
* [Server-Side](doc/introduction/ServerSide.md)
* [Client-Side](doc/introduction/ClientSide.md)

## [Code Guidelines](modules/main/src/docs/CodeGuidelines.md)

[Code Guidelines](modules/main/src/docs/CodeGuidelines.md) are not carved into stone and should be improved all the time.

_Input from each developer is highly appreciated._

##Feature Toggles
In general, we use the feature toggle approach in favour of the feature branch approach. This means that we push against the master branch.

## Programming languages

Usage of Scala and CoffeeScript is deprecated. For current development Java and TypeScript should be used. Scala and Java
can be fully mixed. So there is no need to work on Scala-only or Java-only modules.

This is accomplished by excluding the Java compiler at all and let both Java and Scala sources be compiled by Scala only.

The same goes for TypeScript and CoffeeScript.
