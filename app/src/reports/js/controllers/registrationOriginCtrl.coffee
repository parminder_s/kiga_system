define ['../app', 'moment'], (app, moment) ->
  app.controller 'registrationOriginCtrl', (now) ->
    new class RegistrationOriginCtrl
      start = now().day(1).add -4, 'week'
      end = now().day(1).add 1, 'week'

      filter:
        start: start.toDate()
        end: end.toDate()
        interval: 'weekly'
        reportType: 'overview'

      intervals: [
        {title: 'täglich', value: 'daily'}
        {title: 'wöchentlich', value: 'weekly'}
        {title: 'monatlich', value: 'monthly'}
      ]

      reportTypes: [
        {title: 'Allgemein', value: 'overview'}
        {title: 'Google AdWords', value: 'adwords'}
        {title: 'Google Suche', value: 'search'}
      ]


      query: ->
      change: ->

