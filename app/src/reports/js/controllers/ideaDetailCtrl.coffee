define ['../app', 'angular', 'lodash'], (app, angular, _) ->
  app.controller 'ideaDetailCtrl', ($state, $stateParams, now,
  reportRequests) ->
    new class Ctrl
      constructor: ->
        @sortAsc = true
        @reportId = $stateParams.reportId
        @error = $stateParams.error
        @articleId = $stateParams.articleId
        @language = $stateParams.language
        reportRequests.getIdeaDetail {
          reportId: @reportId, articleId: @articleId}, (data) =>
            @data = data
            for entry in @data.errors
              do ->
                if entry.autoCorrectable is entry.autoCorrected is false
                  entry.manual = true
                else
                  entry.manual = false

      sortAsc: null
      list: null
      reportId: null
      articleId: null
      language: null

      cancel: ->
        $state.go('root.reports.validation.idea.list',
          {reportId: @reportId, language: @language, error: @error})

      sortList: (name) ->
        @sortAsc = !@sortAsc
        @data.errors = (_.sortBy @data.errors, name)
        if (!@sortAsc) then @data.errors.reverse()

      sortTranslations: (name) ->
        @sortAsc = !@sortAsc
        @data.translation = _.sortBy @data.translation, name
        if (!@sortAsc) then  @data.translation.reverse()

      getArticleDetails: (articleId) ->
        $state.go 'root.reports.validation.idea.detail', {
          reportId: @reportId, language: @language, articleId: articleId
        }

      goToReport: ->
        $state.go 'root.reports.validation.report.detail', reportId: @reportId

      showContent:(error) ->
        console.log(error)
        alert(error.content)

      print: ->
        reportRequests.postNewPrint {articleId: @articleId}, (data) =>
          @resultPrint = data

      edit: ->
        window.location.href = "/admin/show/#{@articleId}"
