define ['../app', 'angular', 'lodash', 'moment'], (app, angular, _, moment) ->
  app.controller 'shopPaymentReportCtrl', (now, endpoint, $stateParams,
  formHelper) ->
    new class Ctrl
      constructor: ->
        @filter.orderNr = parseInt($stateParams.orderNr) || null
        console.log @filter.orderNr
        if(@filter.orderNr)
          @reloadPaymentWithOrderNr()

        @filter.time.fromDate = now().day(1).add(-2, 'week').toDate()
        @filter.time.toDate = now().add(1, 'day').toDate()
        @filter.country.list = {
          at: 'Österreich'
          de: 'Deutschland'
          ch: 'Schweiz'
          be: 'Belgien'
          it: 'Italien'
          li: 'Lichtenstein'
          lu: 'Luxemburg'
          all: 'Alle Länder'
        }
        @filter.paymentMethod.list = [
          { code: 'MP-CC', title: 'MPay Kreditkarte'}
          { code: 'MP-PAYPAL', title: 'PayPal'}
          { code: 'SOFORT', title: 'Sofort Überweisung'}
          { code:'all', title:'Alle PaymentMethoden'}
        ]
        @filter.paymentProvider.list = {
          mpay: 'Mpay'
          all: 'Alle PaymentProvider'
        }
        @filter.paymentProvider.selected = 'all'
        @filter.paymentMethod.selected = 'all'
        @filter.country.selected = 'all'
      filter:
        time:
          fromDate: null
          toDate: null
        country:
          selected: null
          list: null
        paymentMethod:
          selected: null
          list: null
        paymentProvider:
          selected: null
          list: null
        mPayId: null
        orderNr: null
        invoiceNr: null
      paymentList: null
      filteredPaymentList: null

      filterChanged: ->
        @filteredPaymentList = @paymentList

        @filterWith(@filter.country.selected, 3)
        @filterWith(@filter.paymentMethod.selected, 8)

        @filterWithContains(@filter.orderNr, 0)
        @filterWithContains(@filter.invoiceNr, 6)
        @filterWithContains(@filter.mPayId, 2)

      filterWith:(property, index) ->
        if property != 'all'
          @filteredPaymentList = _.filter(@filteredPaymentList, (o) ->
            o[index] is property)

      filterWithContains:(property, index) ->
        if(property != "" and property != null)
          @filteredPaymentList = _.filter(@filteredPaymentList, (o) ->
            if o[index] is null
              false
            else if o[index].toString().match(property)
              true
            else
              false
          )

      reloadPaymentWithOrderNr: ->
        endpoint {
          url: ('reports/shop/paymentlist/byOrderNr/' + @filter.orderNr)
        }, (data) =>
          @paymentList = data
          @filteredPaymentList = @paymentList
      reloadPayment: ->
        endpoint {
          url: 'reports/shop/paymentlist/filtered'
          data: @filter.time
        }, (data) =>
          @paymentList = data
          @filterChanged()
      getBaseUrl: -> window["kiga"].endPointUrl
      downloadCsv: ->
        window.location=@getCsvDownloadUrl
      getCsvDownloadUrl: ->
        [ @getBaseUrl(),
          'reports/shop/paymentlist/filteredAsCsv?',
          'fromDate=', moment(@filter.time.fromDate).format('YYYYMMDD'), '&',
          'toDate=', moment(@filter.time.toDate).format('YYYYMMDD')
        ].join('')
      getCsvFallbackUrl:->
        [ @getBaseUrl(),
          'reports/shop/paymentlist/allAsCsv',
        ].join('')

      cancel: ->
        formHelper()
