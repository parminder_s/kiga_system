define [ '../app', 'angular', 'lodash' ], (app, angular, _) ->
  app.controller 'reportListCtrl', ($state, now,
  formHelper, confirmerDlg, reportRequests,member) ->
    new class Ctrl
      constructor: ->
        @filter.start = now().day(1).add(-2, 'week').toDate()
        @filter.end = now().add(1, 'day').toDate()
        if member.isCms
          @getListReports()
      sortAsc: true
      list: null
      ideaId: null
      startNew: null
      filter:
        start: null
        end: null

      cancel: ->
        formHelper 'root.reports.validation'

      sortList: (name) ->
        @sortAsc = !@sortAsc
        if @sortAsc is true
          @list = _.orderBy(@list, name, 'asc')
        else
          @list = _.orderBy(@list, name, 'desc')

      getListReports: ->
        reportRequests.getReportList @filter,(data) => @list = data.reverse()

      getDetailsToReport: (report) ->
        $state.go 'root.reports.validation.report.detail', reportId: report.id

      startNewValidation: ->
        if @startNew.autoCorrect is true

          confirmerDlg 'PRINT_CONFIRM_VALIDATION', =>
            reportRequests.postNewValidation @startNew,(data) =>
              @resultStartNew = data
        else
          reportRequests.postNewValidation @startNew,(data) =>
            @resultStartNew = data

      startNewPrint: ->
        reportRequests.postNewPrint @ideaId,(data) => @resultStartNew = data

