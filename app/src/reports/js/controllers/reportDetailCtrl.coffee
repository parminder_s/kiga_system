define ['../app', 'angular', 'lodash'], (app, angular, _) ->
  app.controller 'reportDetailCtrl', ($state, $stateParams, now,
  reportRequests,localesList, member) ->
    new class Ctrl
      constructor: ->
        @errors = ['Invalid Element', 'Attribute missing', 'wrong Attribute',
                   'No element children', 'No character children',
                   'Wrong value for Attribute', 'InvalidImageUrl',
                   'Unknown', 'Xml Error', 'Quote missing', 'AllErrors',
                   'InvalidImageType', 'S3ImageMissing' ]
        @sortAsc = true
        @reportId = $stateParams.reportId
        @data = {}
        @showEndDate = 0
        @language = {}
        @language.list = []
        _.map(localesList, (title, code) =>
          @language.list.push {
            title:title,
            code:title.toLowerCase() + '_' + code.toUpperCase()
          }
        )
        @language.list.push({title:'All', code:'All'})
        @language.selected = 'All'
        if member.isCms
          reportRequests.getReportDetail {reportId: $stateParams.reportId},
            (data) =>
              @data = data
              @showEndDate = @data.report.status is 1
      data: null
      language: null

      cancel: ->
        $state.go 'root.reports.validation.report.list', reportId: @reportId

      sortList: (name) ->
        @sortAsc = !@sortAsc
        if @sortAsc is true
          @data.list = _.orderBy(@data.list, name, 'asc')
        else
          @data.list = _.orderBy(@data.list, name, 'desc')

      getLanguageDetails: (error) ->
        $state.go('root.reports.validation.idea.list',
          {reportId: @reportId, language: @language.selected, error: error})

      onLanguageSelect: () ->
        @data.list = []
        reportRequests.getReportDetail {
          reportId: $stateParams.reportId,
          language: @language.selected}, (data) => @data = data
