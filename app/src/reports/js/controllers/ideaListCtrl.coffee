define ['../app', 'angular', 'lodash'], (app, angular, _) ->
  app.controller 'ideaListCtrl', ($state, $stateParams, now, ideaListService) ->
    new class Ctrl
      constructor: ->
        @language = {}
        @language.selected = $stateParams.language
        @language.list = {
          de_DE: 'DE', it_IT: 'IT', tr_TR: 'TR', en_US: 'EN', All: 'All'
        }
        @errors = {}
        @errors.selected = $stateParams.error
        @errors.list = [
          {code: '1', title: 'Invalid Element'},
          {code: '2', title: 'Attribute missing'},
          {code: '3', title: 'wrong Attribute'},
          {code: '4', title: 'No element children'},
          {code: '5', title: 'No character children'},
          {code: '6', title: 'Wrong value for Attribute'},
          {code: '7', title: 'Invalid Image url'},
          {code: '8', title: 'Unkown'},
          {code: '9', title: 'XmlInvalid'},
          {code: '10', title: 'Quote missing'},
          {code: '11', title: 'All'},
          {code: '12', title: 'InvalidImageType'},
          {code: '13', title: 'S3ImageMissing'}]
        @sortAsc = true
        @reportId = $stateParams.reportId
        @onChangeFilter()
        @list = ideaListService.data
        @hiddenList = []
        @noManual = false
      sortAsc: null
      list: null
      hiddenList: null
      reportId: null
      language: null
      noManual: null

      cancel: ->
        $state.go 'root.reports.validation.report.detail', reportId: @reportId


      sortList: (name) ->
        @sortAsc = !@sortAsc
        if @sortAsc is true
          @list = _.orderBy(@list, name, 'asc')
        else
          @list = _.orderBy(@list, name, 'desc')

      goToReport: ->
        $state.go 'root.reports.validation.report.detail', reportId: @reportId

      getArticleDetails: (articleId) ->
        $state.go 'root.reports.validation.idea.detail', {
          reportId: @reportId,
          language: @language.selected,
          articleId: articleId,
          error: @errors.selected
        }

      checkRow: (row) ->
        row[3] is 0

      filterManualErrors: () ->
        nList = []
        nListHidden = []
        if @noManual is false
          for err in @list
            do ->
              if err[3] is 0
                nListHidden.push err
              else
                nList.push err
        else
          for err in @list
            do ->
              nList.push err
          for err in @hiddenList
            do ->
              nList.push err
        @list = nList
        @hiddenList = nListHidden

      onChangeFilter: () ->
        ideaListService.set @reportId, @language.selected,
          @errors.selected, () =>
            $state.go $state.current, {
              language: @language.selected, error: @errors.selected
            }, {reload: true}
