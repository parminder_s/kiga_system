define ['angular', '../app', 'moment'], (angular, app, moment) ->
  app.controller 'reports.plannerCtrl', ($scope, now, request) ->
    start = now().day(1).add -4, 'week'
    end = now().day(1).add 1, 'week'

    intervals =
        daily: {title: 'täglich', value: 'daily'}
        weekly: {title: 'wöchentlich', value: 'weekly'}
        monthly: {title: 'monatlich', value: 'monthly'}

    angular.extend $scope,
      filter:
        start: start.toDate()
        end: end.toDate()
        interval: 'weekly'
        reportType: 'plans'
      intervals: [
        {title: 'täglich', value: 'daily'}
        {title: 'wöchentlich', value: 'weekly'}
        {title: 'monatlich', value: 'monthly'}
      ]
      reportTypes: [
        {title: 'Pläne', value: 'plans'}
        {title: 'Ideen', value: 'ideas'}
        {title: 'Plandauer', value: 'planDurations'}
        {title: 'Individuelle Ideen', value: 'individualIdeas'}
        {title: 'Ideenquote', value: 'planQuote'}
      ]
      query: ->
        request {
          url: "reports/planner/#{$scope.filter.reportType}"
          data: $scope.filter
        }, (data) -> $scope.data = data
      change: ->
        $scope.intervals = if $scope.filter.reportType is 'planDurations'
          [intervals.weekly]
        else
          [intervals.daily, intervals.weekly, intervals.monthly]
