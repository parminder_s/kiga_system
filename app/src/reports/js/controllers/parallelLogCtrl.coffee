define ['angular', '../app', 'lodash'], (angular, app, _) ->
  app.controller 'parallelLogCtrl', (now, request) -> new class Parallelog
    constructor: -> @query()
    transactionCount: 0
    filter:
      start: now().day(1).add(-2, 'week').toDate()
      end: now().day(1).add(1, 'week').toDate()
      email: ''
      customerId: ''
      customerName: ''

    data: null
    addBgCss: (data) ->
      bgCss = 'odd'
      currentTransactionID = 0
      data.rows = for row in data.rows
        transactionId = _.last row.values
        if transactionId isnt currentTransactionID
          bgCss = if bgCss is 'odd' then 'even' else 'odd'
          currentTransactionID = transactionId
        row.bgCss = bgCss
        row

      @transactionCount = _.uniq(_.map(data.rows, (row) ->
        _.last row.values
      )).length
      data

    query: ->
      request {
        url: 'reports/parallelLog/transactions'
        data: @filter
      }, (data) => @data = @addBgCss data

