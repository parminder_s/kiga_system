define([
  'require',
  'exports',
  'angular',
  './app',
  './controllers/plannerCtrl',
  './controllers/parallelLogCtrl',
  './controllers/registrationOriginCtrl',
  './controllers/reportListCtrl',
  './controllers/reportDetailCtrl',
  './controllers/ideaListCtrl',
  './controllers/ideaDetailCtrl',
  './controllers/shopPaymentReportCtrl',
  './services/ideaListService',
  './services/reportRequests'
], function(require, exports, angular, app) {
  exports.default = true;
});
