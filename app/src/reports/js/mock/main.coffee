define ['../app'], (app) ->
  app.factory 'reportsMock', ['misc.mockHelper'].concat (mock) ->
    mock 'reports/planner', (filter) ->
      header: [
        {title: 'Anzahl', cols: 30}
        {title: 'Zeit', cols: 30}
      ]
      rows: [
        {values: [5, 'Dezember 2014'], cols: 30}
        {values: [6, 'Jänner 2015'], cols: 30}
      ]

  app.run ['reportsMock'].concat ->
