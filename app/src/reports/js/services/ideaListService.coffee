define ['../app', 'lodash'], (app, _) ->
  app.service 'ideaListService',($state, request,reportRequests) ->
    new class Model
      set: (reportId, language, error,callback) ->
        if(reportId isnt @reportId or
        language isnt @language or error isnt @error)
          @reportId = reportId
          @language = language
          @error = error
          reportRequests.getIdeaList @reportId,@language,@error,(data) =>
            @data = data
            callback()
