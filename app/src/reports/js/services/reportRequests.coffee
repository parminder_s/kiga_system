define ['../app', 'lodash'], (app, _) ->
  app.service 'reportRequests', ($state, endpoint) -> new class Links

    getIdeaList:(reportId, language, error, callback) ->
      endpoint {
        url: 'reports/validation/ideaList'
        data:
          reportId: reportId
          language: language
          error: error
      }, (data) -> callback(data)

    getReportDetail: (payload,callback) ->
      endpoint {
        url: 'reports/validation/reportDetail'
        data: payload
      }, (data) -> callback(data)

    getLanguages: ->
      endpoint {
        url:'reports/validation/languages'
      }

    getReportList: (payload,callback) ->
      endpoint {
        url: 'reports/validation/reportList'
        data: payload
      }, (data) -> callback(data)

    getIdeaDetail: (payload,callback) ->
      endpoint {
        url: 'reports/validation/ideaDetail'
        data: payload
      }, (data) -> callback(data)

    postNewValidation:(payload,callback) ->
      endpoint {
        url: 'reports/validation/startValidation'
        data: payload
      }, (data) -> callback(data)

    postNewPrint:(payload,callback) ->
      endpoint {
        url: 'reports/validation/printPreview'
        data: payload
      }, (data) -> callback(data)
