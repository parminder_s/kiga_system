import * as angular from 'angular';
import '../../misc/js/main';

// favourites are a depdendency since they define the memberArea routing.
export let app = angular.module('memberIdeas', ['misc', 'favourites']);
