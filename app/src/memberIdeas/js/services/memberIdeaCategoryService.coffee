define ['../app', 'lodash'], (app, _) ->
  app.app.service 'memberIdeaCategoryService', (endpoint) ->
    new class Model
      allCategories = null

      setParents = (list)->
        ids = _.map(list, 'id')
        aList = _.forEach list, (cat)->
          isParent = _.includes(ids, cat.parentId)
          cat['hasParent'] = isParent
          return
        aList

      getACategory: (id) ->
        if allCategories
          _.find allCategories,(cat)->
            cat.id is id
        else
          @getAll (allCategories)->
            _.find allCategories,(cat)->
              cat.id is id

      getAll: (cb) ->
        if allCategories
          allCategories
        else
          endpoint
            url: "idea/member/category/list"
          .then (list) ->
            aList = setParents(list)
            allCategories = aList
            if cb
              cb aList
            else
              aList
