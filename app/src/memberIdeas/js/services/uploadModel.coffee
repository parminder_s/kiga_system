define ['../app', 'lodash'], (app, _) ->
  app.app.service 'uploadModel', (memberIdeaCategoryService, endpoint) ->
    new class Model

      getIdea: (ideaId)->
        endpoint
          url: "idea/member/get"
          params:
            id: ideaId

      list: () ->
        endpoint {
          url: 'idea/member/list'
        }

      getPreview: (idea) ->
        if idea.previewId
        then '/api/idea/member/display/' + idea.previewId
        else null

      getFile: (idea) ->
        if idea.fileId
        then '/api/idea/member/display/' + idea.fileId
        else null

      getCategoryData: (id)->
        cat = memberIdeaCategoryService.getACategory id

      loadPreviewData : (idea, cb) ->
        returner =
          id: idea.id
          title: idea.title
          description: idea.description
          largePreviewImageUrl: @getPreview(idea)
          rating: 4
          ageGroup: 5
          fileId:idea.fileId
          previewId: idea.previewId
          previewUrl: @getPreview(idea)
          level1Title: @getCategoryData(idea.category.id).title
          level1Url: "categoryL1Link"
          level2Title: @getCategoryData(idea.category.parentId).title
          level2Url: "categoryL2Link"
          previewPages: [
            {
              small: @getPreview(idea)
              large: @getPreview(idea)
            }
          ]
        cb returner

      save: (formData, cb) ->
        files = {}
        files["preview"] = formData.preview
        files["file"] = null
        if formData.contentType isnt 'text'
          formData.content = null
          _.each formData.files, (file) ->
            files["file"] = file

        endpoint
          url: "memberidea/save"
          data:
            title: formData.title
            description: formData.description
            content: formData.content
            contentType: formData.contentType
            categoryId: formData.category.id
          files: files

      update: (formData, cb) ->
        files = {}
        files["preview"] = formData.preview
        files["file"] = null
        if formData.contentType isnt 'text'
          formData.content = null
          _.each formData.files, (file) ->
            files["file"] = file

        endpoint
          url: "member/idea/update"
          data:
            id: formData.id
            title: formData.title
            description: formData.description
            content: formData.content
            contentType: formData.contentType
            categoryId: formData.category.id
          files: files
