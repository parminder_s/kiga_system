/**
 * Created by bbs on 4/3/17.
 */

export class UploadCtrl {
  private static MAX_FILES = 1;
  private formData;
  private categories;
  private previewOptions;
  private uploadOptions;
  private parentCategories;

  constructor(
    private $state,
    private uploadModel,
    private categoriesModel: any,
    private idea,
    private memberIdeaCategoryService
  ) {
    this.formData = {
      id: idea ? idea.id : null,
      contentType: idea ? idea.contentType : 'file',
      title: idea ? idea.title : null,
      parentCategory: idea ? memberIdeaCategoryService.getACategory(idea.category.id) : null,
      category: idea ? idea.category : null,
      description: idea ? idea.description : null,
      fileUrl: idea ? uploadModel.getFile(idea) : null,
      files: [],
      previewUrl: idea ? uploadModel.getPreview(idea) : null,
      preview: null
    };

    this.previewOptions = {
      previewMaxWidth: 200,
      previewMaxHeight: 200,
      previewCrop: true,
      maxFileSize: 1000000,
      acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
    };

    this.uploadOptions = {
      previewMaxWidth: 100,
      previewMaxHeight: 100,
      previewCrop: true,
      maxFileSize: 1000000
    };

    this.categories = null;
    this.parentCategories = _.filter(categoriesModel, function(value: any) {
      return value.hasParent === false;
    });
  }

  selectChildCategory($item) {
    this.formData.category = null;
    this.categories = [];
    this.categories = _.filter(this.categoriesModel, (cat: any) => cat.parentId === $item.id);
  }

  save() {
    if (this.idea) {
      return this.uploadModel.update(this.formData).then(function() {
        return this.$state.go('root.ideas.memberIdeaList');
      });
    } else {
      return this.uploadModel.save(this.formData).then(function() {
        return this.$state.go('root.ideas.memberIdeaList');
      });
    }
  }

  onPreviewAdd(data) {
    this.formData.preview = data[0];
  }

  onFileAdd(files) {
    this.formData.files.length = 0;
    let free = Math.max(0, UploadCtrl.MAX_FILES - this.formData.files.length);
    files = files.slice(0, free);
    this.formData.files.push(files);
  }

  onRemoveFile(file) {
    this.formData.files = _.without(this.formData.files, file);
  }

  onRemovePreviewUrl(file) {
    this.formData.previewUrl = null;
  }

  onRemoveFileUrl(file) {
    this.formData.fileUrl = null;
  }

  isFormValid() {
    let data = this.formData;
    return (
      (data.contentType === 'text' && data.content.length) ||
      (data.contentType !== 'text' && data.files.length)
    );
  }
}
