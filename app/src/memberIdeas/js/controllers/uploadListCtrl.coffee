define ['../app', 'angular'], (app) ->
  app.app.controller 'uploadListCtrl', (ideasModel, $document,
    $timeout, $stateParams, $state, uploadModel, ideas) ->
    MAX_FILES = 1

    isSearchResults =
      if !$stateParams.group
        true
      else
        false

    new class IdeasCtrl
      constructor: () ->
        @types = []
        @selectedTypes = []
        @selectedType = null
        @selectedIdeaIndex = null
        @selectedIdea = null
        @previewData = null
        @activePreviewPage = null

        @ideas = {}
        @allIdeas = ideas

        for idea in ideas
          typeName = idea.contentType
          if not @ideas[typeName]?
            @ideas[typeName] = []
          @ideas[typeName].push idea
        @updateTypes()

        if isSearchResults || $stateParams.group is 'all'
          @selectAllTypes()
        else
          type = (i for i in @types when i.name is $stateParams.group)[0]
          if not type
            $state.go '.', group:'all'
            return

          @selectType type

        $timeout @refreshUi
        $(window).on('cssLoaded', @refreshUi)

      upload: ->
        $state.go '^.memberIdeaUpload'

      edit: ()->
        $state.go '^.memberIdeaEdit', ideaid:@previewData.id

      getDownloadLink: (id)->
        if id
        then '/api/idea/member/download/' + id
        else null

      getPreview: (idea) ->
        uploadModel.getPreview(idea)
#        '/api/idea/member/display/'+idea.previewId

      selectAllTypes: ->
        @selectedTypes = @types
        @selectedType = null

      selectType: (type) ->
        @selectedType = type
        @selectedTypes = [type]


      refreshUi: =>
        @selectIdeaFromParams()
        @updateScrollers()

      selectIdeaFromParams: ->
        if $stateParams.ideaId?
          id = parseInt $stateParams.ideaId
          idea = (i for i in @allIdeas when i.id is id)[0]
          if not idea?
            $state.go '.', group: $stateParams.group, ideaId:null
          for type, ideas of @ideas
            for idx, idea of ideas
              if idea.id == id
                @activateIdea(type, parseInt idx)

      updateTypes: ->
        ideasModel.typesList.forEach (type) =>
          @types.push type if @ideas[type.name] and @ideas[type.name].length

      selectIdea: (type, index) ->
        idea = @ideas[type][index]
        if @selectedIdea == idea
          @closePreview()
          return

        typeName = 'all'
        typeName = @selectedType.name if @selectedType
        $state.go '.', group:typeName, ideaId:idea.id,
          notify: false
        @activateIdea type, index

      onTypeClick: (type) ->
        id = null

        if @selectedIdea
          idea = (i for i in @ideas[type.name] when i is @selectedIdea)[0]
          id = idea.id if idea
          @closePreview() if not idea

        $state.go '.', group: type.name, ideaId: id,
          notify: false
        @selectType type

      onAllTypesClick: () ->
        id = null
        id = @selectedIdea.id if @selectedIdea

        $state.go '.', group: 'all', ideaId: id,
          notify: false
        @selectAllTypes()

      activateIdea: (type, index) ->
        if @selectedType and type isnt @selectedType.name
          $state.go '.', group:@selectedType, ideaId: null
          return
        @activePreviewPage = null
        @selectedIdeaIndex = index
        @selectedIdea = @ideas[type][index]
        @scrollToSelectedIdea(type, @selectedIdea)
        @loadPreviewData @selectedIdea

      scrollToSelectedIdea: (type, idea) ->
        el = $document.find "[data-idea-id='#{type}#{idea.id}']"
        scroll = el.parent('.ideas-list').controller('scrollPane')
        scroll.scrollToIndex @selectedIdeaIndex

      closePreview: () ->
        @selectedIdeaIndex = null
        @previewData = null
        @selectedIdea = null

      loadPreviewData: (idea) ->
        uploadModel.loadPreviewData idea,(data) =>
          @previewData = data

      selectPreviewPage: (page) ->
        if @activePreviewPage == page
          @activePreviewPage = null
        else
          @activePreviewPage = page

      prevItem: ->
        if @selectedIdeaIndex > 0
          @selectIdea @selectedIdea.contentType, @selectedIdeaIndex - 1

      nextItem: ->
        if (@selectedIdeaIndex < @ideas[@selectedIdea.contentType].length - 1)
          @selectIdea @selectedIdea.contentType, @selectedIdeaIndex + 1

      updateScrollers: ->
        scrollPanes = $document.find('.ideas [scroll-pane]')
        scrollPanes.each (idx, pane) ->
          angular.element(pane).controller('scrollPane')
          .updateButtonsVisibility()

        if @selectedType
          el = $document.find ".ideas .sort-row"
          scroll = el.controller('scrollPane')
          scroll.scrollToIndex @types.indexOf(@selectedType) + 1, true


