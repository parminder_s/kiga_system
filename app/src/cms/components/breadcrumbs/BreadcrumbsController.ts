export default class BreadcrumbsController {
  private onFirstBreadcrumbClick: () => void;
  private onBreadcrumbClick: (data: any) => void;
  public constructor() {}

  public $onInit() {}

  public clickRoot() {
    this.onFirstBreadcrumbClick();
  }

  public clickBreadcrumb(index: number) {
    this.onBreadcrumbClick({ index: index });
  }
}
