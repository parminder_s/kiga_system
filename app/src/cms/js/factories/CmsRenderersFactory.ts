import { Renderer } from '../renderers/Renderer';
import { ResolvedType } from '../ResolvedType';
/**
 * Created by bbs on 11/29/16.
 */

export class CmsRenderersFactory {
  private renderers: { [resolvedType: string]: Renderer } = {};

  register(type: ResolvedType, renderer: Renderer) {
    this.renderers[type] = renderer;
  }

  forType(type: ResolvedType) {
    return this.renderers[type];
  }
}
