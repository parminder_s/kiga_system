interface Permission {
  denied: boolean;
  code: string;
}

export default Permission;
