import Permission from './Permission';
import { ResolvedType } from './ResolvedType';
interface ViewModel {
  permission: Permission;
  resolvedType: ResolvedType;
  id: number;
  showBreadcrumbs: boolean;
  breadcrumbs: Array<any>;
  redirectTo: string;
  url: string;
}

export default ViewModel;
