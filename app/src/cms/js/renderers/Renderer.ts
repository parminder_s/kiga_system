/**
 * Created by bbs on 11/27/16.
 */
import ViewModel from '../ViewModel';

export interface Renderer {
  viewModel: ViewModel;

  breadcrumbsCssClass?: string;

  partial: string;

  initialize: (viewModel: any) => void;

  onBreadcrumbClick: (index) => void;

  onTopCategoryClick: () => void;
}
