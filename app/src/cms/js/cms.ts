import * as angular from 'angular';
import { StateParams, StateService } from 'angular-ui-router';
import * as _misc from 'misc';
import {
  IConfigService,
  INgxNavigateService,
  ILinksService
} from '../../misc/js/services/AngularInterfaces';
import IdeaRedirectService from '../../misc/js/services/IdeaRedirectService';
import IMemberService from '../../misc/permission/js/IMemberService';
import PermissionStateResolver from '../../misc/permission/js/PermissionStateResolver';
import { default as BreadcrumbsController } from '../components/breadcrumbs/BreadcrumbsController';
import CmsRenderer from './controller/CmsRenderer';
import { CmsRenderersFactory } from './factories/CmsRenderersFactory';
import { CmsModuleLoader } from './services/CmsModuleLoader';
import { CmsModuleRegistry } from './services/CmsModuleRegistry';
import CmsPermission from './services/CmsPermission';
import { IdeaResolverCache } from './services/IdeaResolverCache';
import ResolverService from './services/ResolverService';
import ViewModel from './ViewModel';

import IScope = angular.IScope;

let misc = _misc;

export let cmsModule = angular
  .module('cms', ['misc'])
  .config(($urlMatcherFactoryProvider, stateHelperProvider) => {
    let valToString = val => (val !== null ? val.toString() : val);
    $urlMatcherFactoryProvider.type('nonURIEncoded', {
      encode: valToString,
      decode: valToString,
      is: () => true
    });
    stateHelperProvider.setNestedState({
      name: 'root.cms',
      onEnter: scrollUpService => scrollUpService.scroll(),
      url: '/cms/{path:nonURIEncoded}?preview&previewCategory&ageGroupFilter',
      resolve: {
        viewModel: ($stateParams: StateParams, resolverService: ResolverService) =>
          resolverService.get($stateParams)
      },
      templateUrl: 'cms/partials/main.html',
      controller: 'cmsRenderer as ctrl'
    });
  })
  .run((resolverService: ResolverService) => resolverService.registerCache(new IdeaResolverCache()))
  .component('breadcrumbs', {
    templateUrl: 'cms/components/breadcrumbs/breadcrumbs.html',
    controller: () => new BreadcrumbsController(),
    bindings: {
      breadcrumbs: '<',
      onBreadcrumbClick: '&',
      onFirstBreadcrumbClick: '&',
      rootName: '<',
      show: '<'
    }
  })
  .controller(
    'cmsRenderer',
    (
      viewModel: ViewModel,
      cmsRenderersFactory: CmsRenderersFactory,
      cmsPermission: CmsPermission,
      ideaRedirectService: IdeaRedirectService,
      ngxNavigate: INgxNavigateService,
      config: IConfigService,
      $stateParams: StateParams,
      links: ILinksService
    ) =>
      new CmsRenderer(
        viewModel,
        cmsRenderersFactory,
        cmsPermission,
        ideaRedirectService,
        ngxNavigate,
        config,
        $stateParams,
        links
      )
  )
  .service(
    'cmsModuleLoader',
    (cmsModuleRegistry: CmsModuleRegistry, $ocLazyLoad) =>
      new CmsModuleLoader(cmsModuleRegistry, $ocLazyLoad)
  )
  .service('cmsModuleRegistry', () => new CmsModuleRegistry())
  .service(
    'cmsPermission',
    (
      member: IMemberService,
      $state: StateService,
      locale,
      permissionStateResolver: PermissionStateResolver,
      config: IConfigService
    ) => new CmsPermission(member, $state, locale, permissionStateResolver, config)
  )
  .service('cmsRenderersFactory', () => new CmsRenderersFactory())
  .service(
    'resolverService',
    (endpoint, cmsModuleLoader: CmsModuleLoader) => new ResolverService(endpoint, cmsModuleLoader)
  );
