import { ResolverRequest } from '../requests/ResolverRequest';
import ViewModel from '../ViewModel';

export interface ResolverCache {
  get(request: ResolverRequest): Promise<ViewModel>;
  put(key: ResolverRequest, value: Promise<ViewModel>): void;
  invalidate(): void;
}
