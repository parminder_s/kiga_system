import { IConfigService } from '../../../misc/js/services/AngularInterfaces';
import IMemberService from '../../../misc/permission/js/IMemberService';
import PermissionStateResolver from '../../../misc/permission/js/PermissionStateResolver';
import { StateService } from 'angular-ui-router';
/**
 * Since the default permission system is hooked before the transition starts it cannot
 * be used in the CMS module, since we only know after the url is resolved which renderer/viewModel
 * we are actually dealing with.
 *
 * Therefore this service is used after the ViewModel is resolved. If there are any permissions
 * on that ViewModel the response from the server contains the required information and this
 * service has the logic to process it properly.
 */

export default class CmsPermission {
  constructor(
    private memberService: IMemberService,
    private stateService: StateService,
    private locale,
    private permissionStateResolver: PermissionStateResolver,
    private config: IConfigService
  ) {}

  handlePermission(permission: string, forward: String) {
    let redirectState: string;
    switch (permission) {
      case 'loggedIn':
        redirectState = 'root.login';
        break;
      case 'testSubscription':
        if (this.config.getPermissionMode() === 'fullTest') {
          redirectState = this.handleFullTestSubscription();
        } else {
          redirectState = this.handleNonFullTestSubscription();
        }
        break;
      case 'subscription':
      case 'fullSubscription':
        redirectState = this.permissionStateResolver.resolvePermissionDenied(false);
    }

    if (redirectState === 'root.login') {
      this.stateService.go(
        redirectState,
        { locale: this.locale.get(), forward: forward },
        { location: 'replace' }
      );
    } else {
      this.stateService.go(redirectState, { locale: this.locale.get() }, { location: 'replace' });
    }
  }

  private handleNonFullTestSubscription() {
    let returner = 'root.permission.';
    let member = this.memberService.get();
    if (member.isLoggedIn()) {
      let subscription = member.subscription;

      console.log('cms permission' + subscription.group);
      if (subscription.group === 'community') {
        return returner + 'fullRequired';
      } else if (subscription.group === 'test' && subscription.isCancelled) {
        return returner + 'testExpired';
      } else if (subscription.isCancelled) {
        return returner + 'fullExpired';
      } else if (subscription.isLocked) {
        return returner + 'invoiceOpen';
      } else if (subscription.group === 'test') {
        return returner + 'fullRequired';
      }
    } else {
      returner = 'root.login';
    }
    return 'root.login';
  }

  private handleFullTestSubscription() {
    let returner = 'root.permission.';
    let member = this.memberService.get();
    if (member.isLoggedIn()) {
      let subscription = member.subscription;
      if (subscription.group === 'community') {
        return returner + 'testUpgrade';
      } else if (subscription.isCancelled) {
        return returner + 'fullExpired';
      } else if (subscription.isLocked) {
        return returner + 'invoiceOpen';
      } else if (subscription.group === 'test') {
        return returner + 'fullExpired';
      }
    } else {
      return returner + 'loginOrTest';
    }
    return 'root.login';
  }
}
