import { ResolverCache } from './ResolverCache';
import { ResolverRequest } from '../requests/ResolverRequest';

export class IdeaResolverCache implements ResolverCache {
  private typesToCache: string[] = [
    'IdeaGroupMainContainer',
    'IdeaGroupContainer',
    'IdeaGroupCategory',
    'IdeaGroup'
  ];

  private cache = {};

  public get(request: ResolverRequest): Promise<any> {
    let key = JSON.stringify(request);
    let promise = this.cache[key];
    return promise;
  }

  public put(request: ResolverRequest, value: Promise<any>) {
    let key = JSON.stringify(request);
    value.then(data => {
      for (let type of this.typesToCache) {
        if (type === data.resolvedType) {
          this.cache[key] = value;
          break;
        }
      }
    });
  }

  invalidate(): void {
    this.cache = {};
  }
}
