import IEndpoint from '../../../misc/js/services/IEndpoint';
import { ResolverRequest } from '../requests/ResolverRequest';
import ViewModel from '../ViewModel';
import { CmsModuleLoader } from './CmsModuleLoader';
import { ResolverCache } from './ResolverCache';

/**
 * Handles the resolvement of an CMS url, by requesting the viewmodel from
 * the endpoint. It makes also sure that the responses are cached on the
 * client and frontend modules are lazy loaded when required.
 */
export default class ResolverService {
  private caches: ResolverCache[] = [];

  public constructor(private endpoint: IEndpoint, private cmsModuleLoader: CmsModuleLoader) {}

  public registerCache(cache: ResolverCache) {
    this.caches.push(cache);
  }

  public unregisterCache(cache: ResolverCache) {
    let index = this.caches.indexOf(cache);
    if (index) {
      this.caches.slice(index, 1);
    }
  }

  public invalidateCache() {
    for (let cache of this.caches) {
      cache.invalidate();
    }
  }

  public get($stateParams): Promise<ViewModel> {
    let request = new ResolverRequest($stateParams.path, $stateParams.ageGroupFilter || 0);
    for (let cache of this.caches) {
      let hit = cache.get(request);
      if (hit) {
        return hit;
      }
    }

    const promise = this.endpoint.post<ViewModel>({ url: 'cms/resolve', data: request });
    for (let cache of this.caches) {
      cache.put(request, promise);
    }
    return promise.then(viewModel => this.cmsModuleLoader.lazyLoadIfRequired(viewModel));
  }
}
