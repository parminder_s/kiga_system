/**
 * Required to lazy load modules, which to not have a static
 * url assigned.
 */
import { ResolvedType } from '../ResolvedType';

export class CmsModuleRegistry {
  private registry: { [resolvedType: string]: string } = {};

  public register(resolvedType: ResolvedType, module: string) {
    this.registry[resolvedType] = module;
  }

  public getModuleNameForResolvedType(resolvedType: string) {
    return this.registry[resolvedType];
  }
}
