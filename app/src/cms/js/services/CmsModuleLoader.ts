import ViewModel from '../ViewModel';
import { CmsModuleRegistry } from './CmsModuleRegistry';

/**
 * ensure that the module which contains the renderer for the resolvedType
 * is loaded lazily.
 */
export class CmsModuleLoader {
  constructor(private cmsModuleRegistry: CmsModuleRegistry, private $ocLazyLoad) {}

  lazyLoadIfRequired(viewModel: ViewModel) {
    return new Promise<ViewModel>((resolve, reject) => {
      const moduleToLoad = this.cmsModuleRegistry.getModuleNameForResolvedType(
        viewModel.resolvedType
      );
      if (moduleToLoad) {
        switch (moduleToLoad) {
          case 'shop': {
            (<any>require).ensure(
              ['../../../shop/js/shop'],
              () => {
                let module = require('../../../shop/js/shop');
                this.$ocLazyLoad.load({
                  name: 'shop'
                });
                resolve(viewModel);
              },
              'shop'
            );
            break;
          }
          case 'ideas': {
            (<any>require).ensure(
              ['../../../ideas/js/ideas'],
              () => {
                let module = require('../../../ideas/js/ideas');
                this.$ocLazyLoad.load({
                  name: 'ideas'
                });
                resolve(viewModel);
              },
              'ideas'
            );
            break;
          }
          default: {
            resolve(viewModel);
          }
        }
      } else {
        resolve(viewModel);
      }
    });
  }
}
