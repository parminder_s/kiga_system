import IScope = angular.IScope;
import IdeaRedirectService from '../../../misc/js/services/IdeaRedirectService';
import { CmsRenderersFactory } from '../factories/CmsRenderersFactory';
import { Renderer } from '../renderers/Renderer';
import CmsPermission from '../services/CmsPermission';
import ViewModel from '../ViewModel';
import {
  INgxNavigateService,
  IConfigService,
  Feature,
  ILinksService
} from '../../../misc/js/services/AngularInterfaces';
import { StateParams } from 'angular-ui-router';

export default class CmsRenderer {
  constructor(
    viewModel: ViewModel,
    cmsRenderersFactory: CmsRenderersFactory,
    cmsPermission: CmsPermission,
    ideaRedirectService: IdeaRedirectService,
    ngxNavigate: INgxNavigateService,
    config: IConfigService,
    $stateParams: StateParams,
    links: ILinksService
  ) {
    if (viewModel.permission && viewModel.permission.denied) {
      if (ideaRedirectService.isDirectlyRequested() && viewModel.id) {
        ideaRedirectService.redirect(viewModel.id);
      } else {
        cmsPermission.handlePermission(viewModel.permission.code, viewModel.url);
      }
    } else if (viewModel.resolvedType === 'redirect') {
      ngxNavigate.navigateByUrl(viewModel.redirectTo);
    } else if (
      config.isFeatureEnabled(Feature.ANGULAR_CMS_ENABLED) &&
      ['IdeaGroup', 'IdeaGroupContainer', 'IdeaGroupCategory'].indexOf(viewModel.resolvedType) > -1
    ) {
      const redirectUrl = `/ng6/${$stateParams['locale']}/${$stateParams['path']}`;
      console.error(
        `requesting legacy cms module for viewType ${
          viewModel.resolvedType
        }. Redirecting to ${redirectUrl}`
      );

      ngxNavigate.navigateByUrl(redirectUrl);
    } else {
      let renderer: Renderer = cmsRenderersFactory.forType(viewModel.resolvedType);
      renderer.initialize(viewModel);
      return renderer;
    }
  }
}
