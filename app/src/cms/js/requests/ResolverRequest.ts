import { AreaTag } from '../../../area/js/AreaTag';
/**
 * Created by bbs on 6/20/16.
 */
export class ResolverRequest {
  private pathSegments: Array<string> = [];
  private ageGroup: number = null;

  constructor(path: string, ageGroup: number) {
    if (path !== '') {
      this.pathSegments = path.split('/');
    }
    this.ageGroup = ageGroup;
  }
}
