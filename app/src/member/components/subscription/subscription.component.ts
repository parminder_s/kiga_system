export default class SubscriptionComponent {
  duration: number = 0;
  translateKey: string;
  durationType: string;

  get durationTypeKey() {
    return (this.durationType || 'month').toUpperCase();
  }

  getSubscriptionDurationTranslateKey() {
    return 'MEMBER_SUBSCRIPTION_DURATION_' + this.durationTypeKey + '_' + this.duration;
  }
}
