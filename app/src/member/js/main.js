define([
  'require',
  'exports',
  './app',
  './service/LicensesHandler',
  './controllers/subscription/LicensesController',
  './controllers/subscription/DurationController',
  './controllers/profile/ProfileAdditionalController',
  './controllers/AccountDashboardController',
  './service/CheckoutService',
  './controllers/CommitController',
  './controllers/BillsSubscriptionController',
  './service/memberModel',
  './../components/subscription/subscription.component',
  './controllers/memberMainCtrl',
  './controllers/welcomeCtrl',
  './controllers/subscription/memberSubscriptionOverviewCtrl',
  './controllers/subscription/memberSubscriptionCancellationCtrl',
  './controllers/subscription/memberSubscriptionPayCtrl',
  './controllers/memberAddressCtrl',
  './controllers/memberBillsShopCtrl',
  './controllers/profile/memberProfileOverviewCtrl',
  './controllers/profile/memberProfileUserCtrl',
  './controllers/profile/memberProfileLanguageCtrl',
  './controllers/profile/memberProfilePasswordCtrl',
  './controllers/profile/memberProfileEmailCtrl',
  './controllers/memberMessageCtrl',
  './controllers/memberMsgsCtrl',
  './controllers/activation/activationCtrl',
  './controllers/activation/checkCtrl',
  './controllers/activation/mainCtrl',
  './directives/menuItem',
  './directives/payment',
  './directives/simpleFileUpload',
  './service/mainHandler',
  './service/billHandler',
  './service/subscriptionTypeService',
  './service/durationChangeService'
], function(
  require,
  exports,
  app,
  LicensesHandler,
  LicensesController,
  DurationController,
  ProfileAdditionalController,
  AccountDashboardController,
  CheckoutService,
  CommitController,
  BillsSubscriptionController,
  MemberModelService,
  SubscriptionComponent,
  MessageRedirectorService
) {
  exports.default = true;

  app.service('member.licensesHandler', [
    function() {
      return new LicensesHandler.default();
    }
  ]);

  app.service('memberModel', [
    '$log',
    'request',
    'endpoint',
    function($log, request, endpoint) {
      return new MemberModelService.default($log, request, endpoint);
    }
  ]);

  app.service('member.checkoutService', [
    'memberModel',
    '$window',
    'toastService',
    function(memberModel, $window, toastService) {
      return new CheckoutService.default(memberModel, $window, toastService);
    }
  ]);

  app.controller('member.licensesCtrl', [
    'member.mainHandler',
    'memberModel',
    'data',
    '$state',
    'scrollUpService',
    'member.billHandler',
    'member.checkoutService',
    'toastService',
    'member.licensesHandler',
    function(
      mainHandler,
      model,
      data,
      $state,
      scrollUpService,
      billHandler,
      checkoutService,
      toastService,
      licensesHandler
    ) {
      return new LicensesController.default(
        mainHandler,
        model,
        data,
        $state,
        scrollUpService,
        billHandler,
        checkoutService,
        toastService,
        licensesHandler
      );
    }
  ]);

  app.controller('member.subscriptionDurationCtrl', [
    '$translate',
    'member.mainHandler',
    'durationChangeService',
    'memberModel',
    'member.billHandler',
    'member.checkoutService',
    'toastService',
    'data',
    'availableProducts',
    'accountData',
    function(
      $translate,
      mainHandler,
      durationChangeService,
      model,
      billHandler,
      checkoutService,
      toastService,
      data,
      availableProducts,
      accountData
    ) {
      return new DurationController.default(
        $translate,
        mainHandler,
        durationChangeService,
        model,
        billHandler,
        checkoutService,
        toastService,
        data,
        availableProducts,
        accountData
      );
    }
  ]);

  app.controller('member.profileAdditionalCtrl', [
    '$log',
    '$scope',
    'memberModel',
    'member.mainHandler',
    'data',
    '$filter',
    'JobOptionsService',
    function($log, $scope, model, mainHandler, data, $filter, jobOptionsService) {
      return new ProfileAdditionalController.default(
        $log,
        $scope,
        model,
        mainHandler,
        data,
        $filter,
        jobOptionsService
      );
    }
  ]);

  app.controller('member.accountCtrl', [
    '$log',
    '$scope',
    'member.mainHandler',
    'data',
    'subscriptionTypeService',
    function($log, $scope, mainHandler, data, subscriptionTypeService) {
      return new AccountDashboardController.default(
        $log,
        $scope,
        mainHandler,
        data,
        subscriptionTypeService
      );
    }
  ]);

  app.controller('member.commitController', [
    'member.checkoutService',
    'toastService',
    '$state',
    'data',
    function(checkoutService, toastService, $state, data) {
      return new CommitController.default(checkoutService, toastService, $state, data);
    }
  ]);

  app.controller('member.billsSubscriptionCtrl', [
    'member.checkoutService',
    'member.mainHandler',
    'member.billHandler',
    'memberModel',
    'toastService',
    'data',
    'request',
    function(checkoutService, mainHandler, billHandler, model, toastService, data, request) {
      return new BillsSubscriptionController.default(
        checkoutService,
        mainHandler,
        billHandler,
        model,
        toastService,
        data,
        request
      );
    }
  ]);

  // Not yet in use
  // app.component('subscriptionTransition', {
  //   templateUrl: 'member/components/subscription-transition/subscription-transition.component.html'
  // });

  app.component('subscriptionType', {
    templateUrl: 'member/components/subscription/subscription.component.html',
    controller: function() {
      return new SubscriptionComponent.default();
    },
    bindings: {
      duration: '<',
      translateKey: '<',
      durationType: '<'
    }
  });
});
