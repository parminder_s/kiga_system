export interface TransactionResponse {
  checkout: any;
  payment_data: {
    token: string;
    checkout_url: string;
  };
  payment_option: number;
  payment_option_code: string;
  needs_checkout: number;
  transaction_id: string;
  transaction_status: string;
}
