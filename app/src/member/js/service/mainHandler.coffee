define ['../app', 'lodash', 'moment'], (app, _, moment) ->
  app.service 'member.mainHandler', ['$state', '$filter', '$locale', '$log', '$window'].
  concat ($state, $filter, $locale, $log, $window) ->
    new class MainHandler

      numberFormatter: (number) -> $filter('number')(number,2)
      dateFormatter: (date) => $filter('date')(date)
      transKey: (type,key) => "MEMBER_#{type.toUpperCase()}_#{key.replace('-','').toUpperCase()}"

      # isProductMinimumAgeReached: (birthDateAsString, minimumAge) =>
      #   minimumAge =
      #   regDataViewModel.isProductMinimumAgeReachedForAge birthDateAsString, minimumAge


      goBack: => $window.history.back()
      gotoState: (state, object) => $state.go state, object
      url: (value) => url(value)
