define ['../app', 'lodash'], (app, _) ->
  app.service 'durationChangeService', ($log, subscriptionTypeService) ->
    class DurationChangeService

      findProduct: (productId, products) -> _.filter(products, (pro) -> pro.id == productId)[0]

      getProductsForDuration: (currentProduct, products) ->
        if subscriptionTypeService.isLicenceProduct()
          (p for p in products when p.licenceCount is currentProduct.licenceCount)
        else
          products

    new DurationChangeService()
