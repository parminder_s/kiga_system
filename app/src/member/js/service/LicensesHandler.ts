export interface Subuser {
  userId: number;
  removed: boolean;
  email: string;
}

// export class LicenseUser implements Subuser {
//   userId: number;
//   removed: boolean;
//   email: string;
//   password: string;
// }

export interface LicenseData {
  aboPeriodDat: number;
  currency: string;
  licences: number;
  licenceData: {
    maxUsers: number;
    userData: Subuser[];
  };
  maxLicences: number;
  price: number;
  maxPossibleSubUsersAfterTransition: number;
  removedUsers: any[];
  showFutureRemove: boolean;
  transitionDate: Date;
}

export default class LicensesHandler {
  public userData: Subuser[];
  public data: LicenseData;

  constructor() {}

  public getLicensesAmountOptions(maxUsers: number) {
    return Array.from(Array(maxUsers).keys()).map(x => {
      return { code: x, title: '' + x };
    });
  }

  public createEmptySubuser(): any {
    return { email: undefined, removed: false };
  }

  /**
   * Create the list of open spots for currently undefined - but available -
   * subusers/liceses.
   * @param currentLicences
   * @param maxLicences
   */
  public createEmptySpots(currentLicences, maxLicences): any[] {
    let extras = Math.max(0, maxLicences - currentLicences);
    return Array.from(Array(extras).keys()).map(_ => this.createEmptySubuser());
  }
}
