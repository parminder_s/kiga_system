/**
 * Backend calls for setting and retrieving data related to member-,
 * billing- ,subscription and customer data.
 */

import { AvailableSubscription } from '../availableSubscription';

export default class MemberModelService {
  constructor(private $log, private request, private endpoint) {}
  memberAccount() {
    return this.endpoint({
      url: 'member/account'
    });
  }

  memberAddress() {
    return this.endpoint({
      url: 'member/address'
    });
  }

  memberAddressSave(data, cb, cbErr) {
    return this.endpoint(
      {
        url: 'member/updateAddress',
        data: data
      },
      cb,
      cbErr
    );
  }

  memberBillPayingPrepare(data, cb) {
    return this.endpoint(
      {
        url: 'member/billPayingPrepare',
        data: data
      },
      cb
    );
  }

  memberBillsSubscription() {
    return this.endpoint({
      url: 'member/invoicesSubscription'
    });
  }

  memberBillsShop() {
    return this.endpoint({
      url: 'member/invoicesShop'
    });
  }

  memberProfileAdditional() {
    return this.endpoint({
      url: 'member/profileAdditional'
    });
  }

  memberProfileAdditionalSave(data, cb) {
    return this.endpoint({
      url: 'member/profileAdditionalSave',
      data: data
    });
  }

  memberProfileEmail() {
    return this.endpoint({
      url: 'member/profileEmail'
    });
  }

  memberProfileEmailOk(data) {
    return this.endpoint({
      url: 'member/profileEmailOk',
      data: data
    });
  }

  memberProfileEmailSave(data, cb) {
    return this.endpoint(
      {
        url: 'member/profileEmailSave',
        data: data
      },
      cb
    );
  }

  memberProfilePasswordSave(data, cb, errcb) {
    return this.endpoint(
      {
        url: 'member/profilePasswordSave',
        data: data
      },
      cb,
      errcb
    );
  }

  memberProfileLanguage() {
    return this.endpoint({
      url: 'member/profileLanguage'
    });
  }

  memberProfileLanguageSave(data, cb) {
    return this.endpoint(
      {
        url: 'member/profileLanguageSave',
        data: data
      },
      cb
    );
  }

  memberProfileUser() {
    return this.endpoint({
      url: 'member/profileUser'
    });
  }

  memberProfileUserSave(data, files) {
    data.files = files;
    return this.endpoint({
      url: 'member/profileUserSave',
      data: data
    });
  }

  memberSubscriptionCancellation() {
    return this.endpoint({
      url: 'member/subscriptionCancellation'
    });
  }

  memberSubscriptionCancellationExecute(data, cb) {
    return this.endpoint(
      {
        url: 'member/subscriptionCancellationExecute',
        data: data
      },
      cb
    );
  }

  memberSubscriptionCancellationUndo(data, cb) {
    return this.endpoint(
      {
        url: 'member/subscriptionCancellationUndo',
        data: data
      },
      cb
    );
  }

  memberAvailableSubscriptions(): Promise<AvailableSubscription> {
    return this.endpoint({
      url: 'member/subscriptionsAvailable'
    });
  }

  memberSubscriptionDuration() {
    return this.endpoint({
      url: 'member/subscriptionDuration'
    });
  }

  memberSubscriptionDurationSave(data, cb) {
    return this.endpoint(
      {
        url: 'member/subscriptionDurationSave',
        data: data
      },
      cb
    );
  }

  memberSubscriptionLicences(data) {
    return this.endpoint({
      url: 'member/subscriptionLicences',
      data: data
    });
  }

  memberSubscriptionLicencesSaveNumberLicences(data, cb) {
    return this.endpoint(
      {
        url: 'member/subscriptionLicencesSaveNumberLicences',
        data: data
      },
      cb
    );
  }

  memberSubscriptionLicencesSave(data, cb) {
    return this.endpoint(
      {
        url: 'member/subscriptionLicencesSave',
        data: data
      },
      cb
    );
  }

  memberSubscriptionPaying() {
    return this.endpoint({
      url: 'member/subscriptionPaying'
    });
  }

  memberSubscriptionPayingSave(data, cb) {
    return this.request(
      {
        url: 'memberAdmin/setPaymentOptionsJson',
        data: data
      },
      cb
    );
  }

  memberAboPeriod() {
    return this.endpoint({
      url: 'member/aboPeriod'
    });
  }

  transactionCommit(data, cb, errcb) {
    return this.endpoint(
      {
        url: 'member/transactionCommit',
        data: data
      },
      cb,
      errcb
    );
  }

  setPaymentToCheckout(data, cb, errcb) {
    console.log(data);
    return this.endpoint(
      {
        url: 'member/setPaymentToCheckout',
        data: data
      },
      cb,
      errcb
    );
  }
}
