define ['../app', 'lodash'], (app, _) ->
  app.service 'subscriptionTypeService', ($log, member) ->
    class SubscriptionTypeService

      isLicenceAdmin: -> (member.get 'isLicenceAdmin') == 1

      isStandardAdmin: ->
        if @isTestOrCommunityMember()
         false
        else
          @isLicenceAdmin()

      getMemberSubscriptionGroup: ->
        subscription = member.get 'subscription'
        subscription?.group

      isLicenceProduct: -> 'licence' == @getMemberSubscriptionGroup()

      isStandardOrgProduct: -> 'standard-org' is member.get 'productGroup'

      isTestOrCommunityMember: -> ('test' is member.get 'productGroup') or ('forum' is member.get 'productGroup')

    new SubscriptionTypeService()
