/**
 * Service that manages the checkout-process.
 *
 * After a change has been posted, it may be necessary to redirect to
 * external payment services. This service helps with the redirects and
 * manages to state between redirects.
 *
 */

import { TransactionResponse } from '../TransactionResponse';
import ToastService from '../../../misc/toast/ToastService';

export default class CheckoutService {
  constructor(private memberModel, private $window, private toastService: ToastService) {}

  processRedirect(checkoutResponse: TransactionResponse, sendToast = true) {
    let redirectUrl = checkoutResponse.checkout.payment_data.checkout_url;
    console.log('processing redirect, to: ', redirectUrl);
    if (redirectUrl) {
      if (sendToast) {
        this.toastService.success('MEMBER_CHECKOUT_REDIRECTING');
      }
      this.$window.location = redirectUrl;
    }
  }

  commitCheckout(tid: string) {
    return new Promise((resolve, reject) => {
      const transaction = {
        transaction_id: tid,
        checkout: {
          cid: '',
          tid: ''
        }
      };

      this.memberModel.transactionCommit(
        transaction,
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
}
