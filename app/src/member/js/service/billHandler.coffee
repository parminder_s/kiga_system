define ['../app', 'lodash'], (app, _) ->
  app.service 'member.billHandler', ['$log', 'request'].concat ($log, request) ->
    class BillHandler

      isBillPaid: (bill) -> (bill.status  == 'paid' or bill.status == 'ok' or bill.status == 'pending')

      isUnpaidBillAvailable: (data) -> (item for item in data when not @isBillPaid(item)).length>0

      getDocumentUrl: (invoiceNr) ->
        kiga.endPointUrl + "member/invoicesSubscription/download?invoiceNr=" + invoiceNr;

      checkout: (paymentMethod) ->
        request(
          url: 'memberAdmin/payOpenJson'
          data:
            paymentMethod: paymentMethod
          (result) ->
            # what is `me` supposed to be ? it's not defined ...
            # $(me).trigger 'submitted', ['invoices']
            true 
        )

    new BillHandler()
