define ['../app', '../../../url'], (app, urlFn) ->
  url = urlFn.default
  app.directive 'payment', ['$log', ($log) ->
    restrict: 'E'
    scope: { paymentOptions:'=', invoiceOptions:'=', paymentMethodModel:'=', invoiceOptionModel:'='}
    templateUrl: 'member/partials/directives/payment.html'
    replace: true
    transclude: true
    controller: ['$scope'].concat ($scope) ->
      $scope.url = (value) -> url value
      $scope.paymentMethodChanged = (par) ->
        $scope.paymentMethodModel = par
        $scope.invoiceOptionModel = 'email_customer' if $scope.paymentMethodModel != 'RE'

      $scope.invoiceOptionChanged = (option) ->
        $scope.invoiceOptionModel = option

      $scope.data =
        'MP-ELV':
          images: ['registration/images/elv.png' ]
        'MP-CC':
          images: ['registration/images/visa.png', 'registration/images/mastercard.png', 'registration/images/amex.png']
        'PAYPAL':
          images: ['registration/images/paypal.png']
        'RE':
          textkey: 'MEMBER_PAY_PER_BILL'
        'email_customer':
          textkey : 'MEMBER_PAY_INVOICE_OPTION_MAIL'
        'post_customer':
          textkey : 'MEMBER_PAY_INVOICE_OPTION_POST'

  ]
