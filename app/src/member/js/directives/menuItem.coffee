define ['../app'], (app) ->
  app.directive 'menuItem', ['$log', ($log) ->
    restrict: 'E'
    scope: {icon:'@', state:'@', translateKey:'@'}
    templateUrl: 'member/partials/directives/menuItem.html'
    replace: true
  ]
