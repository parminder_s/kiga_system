define ['../app'], (app) ->
  app.directive 'simpleFileUpload', ['$log','$parse', 'requestPromise', ($log, $parse, requestPromise) ->
    restrict: 'E'
    scope: {action:'@', translateKey:'@', fileModel:'=', accept:'@'}
    templateUrl: 'member/partials/directives/simpleFileUpload.html'
    replace: true

    link:  (scope, element, atts, controllers) ->
      fileElement = element.find('input[type="file"]')
      fileElement.bind 'change', () ->
        scope.$apply () ->
          scope.fileModel =  fileElement[0].files[0]
          if element.attr('action')
            result = requestPromise
              url: element.attr('action')
              data: ''

      scope.selectFile = ->
        fileElement.click()
        true  #explicit return value is needed

  ]
