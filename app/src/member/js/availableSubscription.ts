export class AvailableSubscription {
  customer_type: string;
  discounted_price: number;
  duration: number;
  licence_price: number;
  now: number; // 0 / 1 boolean
  pre_order: number; // 0 / 1 boolean
  price: number;
  product_group_id: string;
  product_id: number;
}

export class AvailableSubscriptionInfo {
  invoice_ispaid = 0;
  invoice_willcancel = 0;
  products: AvailableSubscription[];
  remain_duration = 0;
  start_end: Date;
  start_now: Date;
}
