define ['../app'], (app) ->
  app.controller 'welcomeCtrl', ($scope, $state, $stateParams, $window, goHome) ->
    angular.extend $scope,
      goTo: (url) ->
        if $stateParams.returnTo
          $window.location.href = $stateParams.returnTo
        else if $stateParams.returnToNg
          $state.go $stateParmas.returnToNg
        else
          if url is 'root'
            goHome(true)
