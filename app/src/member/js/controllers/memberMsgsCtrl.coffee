define ['../app'], (app) ->
  app.controller 'member.msgsCtrl', ['$scope', '$stateParams', '$window', '$sce', 'request', 'member', 'endpoint', '$state', 'memberLoader'].
  concat ($scope, $stateParams, $window, $sce, request, member, endpoint, $state, memberLoader) ->
    $scope.msgIndex= 0

    angular.extend $scope,
      displayMsg: member.messages[$scope.msgIndex]
      displayMsgBody: (()->
        msgs = member.messages
        (i) ->
          $sce.trustAsHtml msgs[i].Body
      )()
      displayLeftButton: ->
        returner = true
        if $scope.msgIndex == 0
          returner = false
        returner
      displayRightButton: ->
        returner = true
        if $scope.msgIndex == member.messages.length-1
          returner = false
        returner
      showPrev: ->
        $scope.msgIndex = $scope.msgIndex - 1
        $scope.displayMsg = member.messages[$scope.msgIndex]
      showNext: ->
        $scope.msgIndex = $scope.msgIndex + 1
        $scope.displayMsg = member.messages[$scope.msgIndex]
      goTo: (id) -> endpoint.post({
          endpoint: false
          url: '/message/Confirm/' + id
          rawUrl: true
        }).then(-> memberLoader.reset().then -> $state.go 'root.home')
      cancel: ->
        $window.location.href = '/'
