define ['../../app'], (app) ->
  app.controller 'activation.activationCheckCtrl', ($scope, $state, data) ->
    if data.message is "expired"
      $state.go 'root.member.activation.expired', {id: data.user_id}
    else if data.message is "invalid"
      $state.go 'root.member.activation.notValid', {id: data.user_id}
    else if data.message is "used"
      $state.go 'root.member.activation.used', {id: data.user_id}
    else if data.message is "ok"
      if data.userstatus.product_status is 'end'
        $state.go 'root.member.activation.cancelled', {id: data.user_id}
      else
        $state.go 'root.member.activation.activated', {id: data.user_id, context: $state.params.context}
    else
      $state.go 'root.member.activation.notValid', {id: data.user_id}
