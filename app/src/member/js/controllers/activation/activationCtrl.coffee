define ['../../app', 'angular'], (app, angular) ->
  app.controller 'activation.activationCtrl', ['$scope', '$state', '$stateParams', '$window', 'requestPromise', 'config',
    'endpoint'].
  concat ($scope, $state, $stateParams, $window, requestPromise, config, endpoint) ->

    $scope.data =
      email: ''
    angular.extend $scope,
      resend: ->
        userId = ""
        if $scope.data.email
          userId = $scope.data.email
        else
          userId = $stateParams.id
        data = requestPromise
          url: 'activation/main/resend'
          data:
            user: userId
        data.then if data.message is "already verified"
            $state.go 'root.member.activation.alreadyVerified'
          else
            $state.go 'root.member.activation.sent'
      hasId: ->
        returner = false
        if $stateParams.id
          returner = true
        returner
      goTo: -> $state.go 'root'

      $scope.comingFromShopRegistration = false

      # for the activated.html partial
      # if context is set to "shop", the user comes from shopRegistration
      # and should get the button "back to shop" and "back to home"
      if $stateParams.context == "shop"
        $scope.comingFromShopRegistration = true
      if $stateParams.context == "kga"
        endpoint
          url: '/kga/permission/accept'
          data: {}
      backToHome: -> $window.location.href = '/'
      backToShop: -> $state.go 'root.cms', {path: 'shop'}

