define ['../app', 'angular'], (app, angular) ->
  app.controller 'member.billsShopCtrl', ['$scope', 'member.mainHandler', 'member.billHandler', 'memberModel', 'data', 'request'].
  concat ($scope, mainHandler, billHandler, model, data, request) ->
    angular.extend $scope,
      data: data
      isBillPaid: billHandler.isBillPaid
      unpaidBillAvailable: billHandler.isUnpaidBillAvailable(data.invoiceList)
      validationInfoRequired: false
      isValidationInfoRequired: -> $scope.validationInfoRequired
      mainHandler: mainHandler


#model.memberBillPayingPrepare requestData, -> mainHandler.goBack()
