define ['../../app', 'angular'], (app, angular) ->
  app.controller 'member.profilePasswordCtrl', ['$log', '$scope', 'memberModel',
      'member.mainHandler', 'toastService'].
  concat ($log, $scope, model, mainHandler, toastService) ->

    angular.extend $scope,
      data: {password:'', newPassword:''}
      validationInfoRequired: false
      isValidationInfoRequired: -> $scope.validationInfoRequired
      mainHandler: mainHandler
      save: (form) ->
        $scope.validationInfoRequired = true
        if form.$valid
          successCb = () ->
            toastService.success('STATUS_OK_PASSWORD_CHANGED')
            mainHandler.goBack()
          errorCb = (result) ->
            toastService.error('STATUS_ERR_OLD_PASSWORD_WRONG')
          model.memberProfilePasswordSave {
            oldPassword: $scope.data.password.value,
            newPassword: $scope.data.newPassword.value
          }, successCb, errorCb
