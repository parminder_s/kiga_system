/**
 * View and update additional member information regarding profile -
 * which job / profession.
 */
import * as _ from 'lodash';

export default class ProfileAdditionalController {
  public organizations = [];
  public jobs = [];
  public jobNames = [];
  constructor(
    private $log,
    private $scope,
    private model,
    private mainHandler,
    public data,
    private $filter,
    private jobOptionsService
  ) {
    // this.organizations = [
    //   { code: "kiga", title: this.$filter("translate")("ORG_KIGA") },
    //   { code: "hort", title: this.$filter("translate")("ORG_HORT") },
    //   { code: "elementary_school", title: this.$filter("translate")("ORG_ELEMENTARY_SCHOOL") },
    //   { code: "bakip", title: this.$filter("translate")("ORG_BAKIP") },
    //   { code: "college", title: this.$filter("translate")("ORG_COLLEGE") },
    //   { code: "polytechnic", title: this.$filter("translate")("ORG_POLYTECHNIC") },
    //   { code: "other", title: this.$filter("translate")("ORG_POLYTECHNIC") }
    // ];

    // this.jobs = [
    //   { code: "teacher", title: this.$filter("translate")("JOB_TEACHER") },
    //   { code: "educator", title: this.$filter("translate")("JOB_EDUCATOR") },
    //   { code: "pedagogue", title: this.$filter("translate")("JOB_PEDAGOGUE") },
    //   { code: "creche_pedagogue", title: this.$filter("translate")("JOB_CRECHE_PEDAGOGUE") },
    //   { code: "childminder", title: this.$filter("translate")("JOB_CHILDMINDER") },
    //   { code: "pupil", title: this.$filter("translate")("JOB_PUPIL") },
    //   { code: "student", title: this.$filter("translate")("JOB_STUDENT") },
    //   { code: "other", title: this.$filter("translate")("JOB_OTHER") }
    // ];

    this.jobNames = _(this.jobOptionsService.jobOptions)
      .map(code => {
        return { title: this.$filter('translate')('REG_JOB_' + code), code: code };
      })
      .valueOf();
  }

  save() {
    return this.model
      .memberProfileAdditionalSave({
        job: this.data.job,
        organization: this.data.organization,
        jobName: this.data.jobName
      })
      .then(() => this.mainHandler.goBack());
  }
}
