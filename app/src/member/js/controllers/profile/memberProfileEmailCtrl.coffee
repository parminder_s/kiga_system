define ['../../app', 'angular'], (app, angular) ->
  app.controller 'member.profileEmailCtrl', ['$log', '$scope', 'memberModel', 'member.mainHandler', 'data'].
  concat ($log, $scope, model, mainHandler, data) ->
    angular.extend $scope,
      oldEmail: data.email
      mainHandler: mainHandler
      input: {newEmail:''}
      validationInfoRequired: false
      isValidationInfoRequired: -> $scope.validationInfoRequired
      model: model
      save: (form) ->
        $scope.validationInfoRequired = true
        if form.$valid
          data.email =$scope.input.newEmail
          model.memberProfileEmailSave { email: $scope.input.newEmail } , ->
            mainHandler.goBack()
