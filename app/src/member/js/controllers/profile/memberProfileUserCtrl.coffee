define ['../../app', 'angular'], (app, angular) ->
  app.controller 'member.profileUserCtrl', ['$log', '$scope', 'memberModel', 'member.mainHandler', 'data', 'endpoint', 'subscriptionTypeService'].
  concat ($log, $scope, model, mainHandler, data, endpoint, subscriptionTypeService) ->
    angular.extend $scope,
      data: data
      data.birthday = new Date(data.birthday)
      mainHandler: mainHandler
      picFile: undefined
      isSubscriptionAdmin: subscriptionTypeService.isLicenceAdmin()
      isTestOrCommunityMember: subscriptionTypeService.isTestOrCommunityMember()
      isValidationInfoRequired: -> $scope.validationInfoRequired
      createFileArray: -> if $scope.picFile? then [$scope.picFile] else []
      save: (form) ->
        $scope.validationInfoRequired = true
        if form.$valid
          result = model.memberProfileUserSave $scope.data, $scope.createFileArray()
          result.then mainHandler.goBack()
      addImage:(files) ->
        endpoint({
          url: "member/uploadImage",
          files: {file: files[0]}
        }, (resp) -> data.avatar = resp)
      removeAvatar:() ->
        data.avatar = null
