define ['../../app', 'angular'], (app, angular) ->
  app.controller 'member.profileLanguageCtrl', ['$scope', '$log', 'member.mainHandler', 'memberModel', 'data', '$filter'].
  concat ($scope, $log, mainHandler, model, data, $filter) ->
    data.options = []
    for c in data.code
      data.options.push { code: c,title: $filter('translate')('MISC_LOCALE_' + c.split('_')[0].toUpperCase())}
    angular.extend $scope,
      data: data
      mainHandler: mainHandler
      save: ->
        model.memberProfileLanguageSave language: $scope.data.value.toLowerCase(), -> mainHandler.goBack()


