define ['../../app', 'angular'], (app, angular) ->
  app.controller 'member.profileOverviewCtrl', ['$log', '$scope', 'member.mainHandler','subscriptionTypeService'].
  concat ($log, $scope, mainHandler, subscriptionTypeService) ->
    angular.extend $scope,
      menu: [
        {titleKey: 'MEMBER_USER_PROFILE', icon: 'icon-person', state: 'root.member.profileuser'}
        {titleKey: 'MEMBER_LANGUAGE', icon: 'fa-globe', state: 'root.member.profilelanguage'}
        {titleKey: 'MEMBER_EMAIL', icon: 'fa-envelope', state: 'root.member.profileemail'}
        {titleKey: 'PASSWORD', icon: 'fa-lock', state: 'root.member.profilepassword'}
      ]
      mainHandler: mainHandler

    if subscriptionTypeService.isLicenceAdmin()
      $scope.menu.push {titleKey: 'MEMBER_ADDITIONAL_DATA', icon: 'fa-plus', state: 'root.member.profileadditional'}
