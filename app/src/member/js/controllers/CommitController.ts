import CheckoutService from '../service/CheckoutService';
import ToastService from '../../../misc/toast/ToastService';

/**
 * Redirects from payment checkouts go back to this controller.
 */
export default class CommitController {
  constructor(
    private checkoutService: CheckoutService,
    private toastService: ToastService,
    private $state,
    private data
  ) {}

  $onInit() {
    let tid = this.data;
    this.checkoutService.commitCheckout(tid).then(
      res => {
        // commit successfull
        console.log('commit ok');
        console.log(res);
        this.toastService.success('MEMBER_CHECKOUT_OK');
        this.$state.go('root.member.account', {}, { location: 'replace' });
      },
      err => {
        this.toastService.permanent().error('MEMBER_CHECKOUT_FAIL');
        this.$state.go('root.member.account', {}, { location: 'replace' });
      }
    );
  }
}
