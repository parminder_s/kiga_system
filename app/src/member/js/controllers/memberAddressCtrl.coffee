define ['../app', 'angular'], (app, angular) ->
  app.controller 'member.addressCtrl', ['$log', '$scope', 'memberModel',
    'member.mainHandler', 'subscriptionTypeService', 'data', 'toastService'].
  concat ($log, $scope, model, mainHandler, subscriptionTypeService, data, toastService) ->

    successCallback = () ->
      mainHandler.goBack()
      toastService.success('MEMBER_STATUS_OK_DEFAULT')
    errorCallback = () ->
      mainHandler.goBack()
      toastService.error('MEMBER_STATUS_ERR_DEFAULT')

    angular.extend $scope,
      data: data
      subscriptionTypeService: subscriptionTypeService
      mainHandler: mainHandler
      validationInfoRequired: false
      isValidationInfoRequired: -> $scope.validationInfoRequired
      canHaveShippingAddress: -> subscriptionTypeService.isStandardOrgProduct() || subscriptionTypeService.isLicenceProduct()
      canHaveOrganisation: ->
        # Indicates if input fields for organisation are to be used
        # TODO: introduce useful check
        true
      hasShippingAddressData: ->
        # Some (rather arbitrary) check on fields to determine if address is empty
        data.billingAddress && Object.keys(data.billingAddress).length > 0 && (!!data.billingAddress.lastname || !!data.billingAddress.street)
      showShipppingAddressInput: -> data.billOn && data.billOn.value
      save: (form) ->
        $scope.validationInfoRequired = true
        if form.$valid
          saveData = {shippingAddress: $scope.data.shippingAddress}
          if $scope.data.billOn.value then saveData.billingAddress = $scope.data.billingAddress
          model.memberAddressSave(saveData, successCallback, errorCallback)

    # Initialise billing address toggle
    $scope.data.billOn = {value: $scope.hasShippingAddressData()}
