export default class AccountDashboardController {
  menu: any;
  showBill: any;

  constructor(
    private $log,
    private $scope,
    public mainHandler,
    public data,
    private subscriptionTypeService
  ) {
    this.menu = [
      {
        titleKey: 'MEMBER_PROFILE',
        icon: 'icon-person',
        state: 'root.member.profileoverview'
      }
    ];
    this.showBill = subscriptionTypeService.isStandardAdmin();

    if (
      !this.subscriptionTypeService.isTestOrCommunityMember() &&
      this.subscriptionTypeService.isLicenceAdmin()
    ) {
      this.menu.push({
        titleKey: 'MEMBER_BILLS_SUBSCRIPTION',
        icon: 'fa-usd',
        state: 'root.member.bills.subscription'
      });
      this.menu.push({
        titleKey: 'MEMBER_BILLS_SHOP',
        icon: 'fa-usd',
        state: 'root.member.bills.shop'
      });
      this.menu.push({
        titleKey: 'MEMBER_SUBSCRIPTIONS',
        icon: 'icon-product-standard-outlines',
        state: 'root.member.subscriptionoverview'
      });
      this.menu.push({
        titleKey: 'MEMBER_ADDRESSES',
        icon: 'icon-location',
        state: 'root.member.address'
      });
    }
  }

  getSubscriptionTranslateKey() {
    return (
      'MEMBER_SUBSCRIPTION_DURATION_' +
      this.data.durationType.toUpperCase() +
      '_' +
      this.data.duration
    );
  }
}
