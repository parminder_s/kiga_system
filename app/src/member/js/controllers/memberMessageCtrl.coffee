define ['../app', 'angular'], (app, angular) ->
  app.controller 'memberMessageCtrl', ($state, $stateParams, goHome) ->
    new class Ctrl
      constructor: ->
        @isSuccess = $stateParams.msgVal
        @code = "MEMBER_" + $stateParams.msgKey.toUpperCase()
      back: ->
        goHome()
      isSuccess: null
      code: null

