define ['../../app', 'angular'], (app, angular) ->
  app.controller 'member.subscriptionPayCtrl', ['$scope', 'memberModel', 'member.mainHandler', 'data'].
  concat ($scope, model, mainHandler, data) ->
    angular.extend $scope,
      data: data
      mainHandler: mainHandler
      validationInfoRequired: false
      isValidationInfoRequired: -> $scope.validationInfoRequired
      save: (form) ->
        $scope.validationInfoRequired= true
        if form.$valid
          model.memberSubscriptionPayingSave $scope.data
