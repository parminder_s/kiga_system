import * as angular from 'angular';
import LicensesHandler from '../../service/LicensesHandler';
import { Subuser, LicenseData } from '../../service/LicensesHandler';
import CheckoutService from '../../service/CheckoutService';
import ToastService from '../../../../misc/toast/ToastService';

/**
 * Controller for managing licenses in a subscription for organisation.
 * The controller combines a number of actions:
 *
 * - choose and change the number of licenses (sub-users) for the subscription
 * - add license data (email + password) to "fill up" (=use) the available licenses
 * - unset license data (free up a spot)
 * - mark licenses for deletion, if a downgrade (decrement) in number of licenses is ordered
 *
 */
export default class LicensesController {
  public maxUsersSelectOptions: any[] = [];
  public showExtraValidation = false;
  public users: Subuser[] = [];
  public removedUsers: Subuser[] = [];
  public openSpots: Subuser[] = [];
  public maxUsers: number;
  public orderedMaxUsers: number;
  public currentMaxUsers: number;
  public hasTransitionOrdered: boolean;

  constructor(
    public mainHandler,
    private model,
    public data: LicenseData,
    private $state,
    private scrollUpService,
    private billHandler,
    private checkoutService: CheckoutService,
    private toastService: ToastService,
    public licensesHandler: LicensesHandler
  ) {
    scrollUpService.scroll();

    this.users = data.licenceData.userData;
    this.currentMaxUsers = data.licences;
    this.orderedMaxUsers = data.maxPossibleSubUsersAfterTransition;

    // Current select box setting
    this.maxUsers =
      data.maxPossibleSubUsersAfterTransition !== null
        ? data.maxPossibleSubUsersAfterTransition
        : this.currentMaxUsers;

    this.hasTransitionOrdered = data.maxPossibleSubUsersAfterTransition !== null;

    this.openSpots = this.licensesHandler.createEmptySpots(
      this.users.length,
      Math.max(this.maxUsers, this.currentMaxUsers)
    );

    this.maxUsersSelectOptions = this.licensesHandler.getLicensesAmountOptions(data.maxLicences);
  }

  get debugOutData() {
    return '';
    // return JSON.stringify(this.data, null, 2);
  }

  /**
   * @return how many subusers will be removed after transition.
   */
  public futureOverflowLicenses(): number {
    return Math.max(0, this.users.length - this.maxUsers);
  }

  /**
   * Whether to display the section in which the user can mark subusers
   * for deletion.
   */
  public showRemoveSelection(): boolean {
    return this.futureOverflowLicenses() > 0;
  }

  /**
   * Whether the current setting results in a transition.
   */
  public hasFutureTransition() {
    return this.orderedMaxUsers !== this.currentMaxUsers;
  }

  public hasCurrentTransition() {
    return this.hasTransitionOrdered;
  }

  public getAboPeriodEndDate() {
    return this.mainHandler.dateFormatter(this.data.aboPeriodDat);
  }

  public getFuturePrice() {
    return this.data.price * (this.maxUsers - this.currentMaxUsers);
  }

  /**
   * On change handler for the number of licenses select input..
   */
  public groupCountChanged() {
    // Un-mark the licenses that don't need to be removed
    // with the new number of licenses
    let markedForRemoval = this.licencesMarkedForRemoval();
    for (let i = 0; i < markedForRemoval.length - this.futureOverflowLicenses(); i++) {
      this.undoFutureRemove(markedForRemoval[i]);
    }

    // Re-create open spots
    this.openSpots = this.licensesHandler.createEmptySpots(
      this.users.length,
      Math.max(this.maxUsers, this.currentMaxUsers)
    );
  }

  public hasLicenseNumberIncrease(): boolean {
    return this.currentMaxUsers - this.maxUsers < 0;
  }

  public hasLicenseNumberDecrease(): boolean {
    return this.currentMaxUsers - this.maxUsers > 0;
  }

  public assignNewLicence(form, index) {
    if (form.$valid) {
      let newLicence = this.openSpots[index];
      this.users.push(newLicence);
      this.openSpots = this.openSpots.filter(v => v !== newLicence);
    } else {
      // this.openSpots[index].showErrors = true;
    }
  }

  /**
   * Test that a subuser/licene (the email) is not already in our list.
   * @param licence
   */
  public isUniqueEmail(licence: Subuser) {
    return this.users.filter(v => v.email === licence.email).length === 0;
  }

  /**
   * Unset a license - opens up a new open spot in its place.
   * @param index
   */
  public removeLicenceData(index) {
    let removedUser = this.users[index];
    this.removedUsers.push(removedUser);
    this.users.splice(index, 1);
    this.openSpots.push({ userId: null, email: undefined, removed: false });
  }

  /**
   * Un-mark use from deletion.
   * @param license
   */
  public undoFutureRemove(license: Subuser) {
    license.removed = false;
  }

  public undoRemoveLicenceData(removedUser: Subuser) {
    this.removedUsers = this.removedUsers.filter(v => v !== removedUser);
    this.users.push(removedUser);
    this.openSpots.pop();
  }

  public canUndoDeletedLicenses() {
    return this.openSpots.length > 0;
  }

  /**
   * Whether the user has marked enough licenses for future deletion.
   */
  public isEnoughLicensesRemoved() {
    let removed = this.licencesMarkedForRemoval();
    return this.futureOverflowLicenses() <= removed.length;
  }

  /**
   * The list of licenses that are marked for removal.
   */
  public licencesMarkedForRemoval(): Subuser[] {
    return this.users.filter(v => v.removed);
  }

  /**
   * Save the state to backend.
   * @param form S
   */
  public save(form) {
    this.showExtraValidation = true;
    this.scrollUpService.scroll(); // Let's scoll up (doesn't seem to work, though)

    // Do an extra check on if enough licenses have been marked for deletion
    // in case of decrease-order
    if (form.$valid && this.isEnoughLicensesRemoved()) {
      // Users without userId have not even been created, so no removal required
      let existingUsersToRemove = this.removedUsers.filter(u => !!u.userId);
      let newState = {
        users: this.users,
        removedUsers: existingUsersToRemove,
        maxUsers: this.maxUsers
      };

      this.model.memberSubscriptionLicencesSave(
        // Submit data
        newState,
        // Callback
        result => {
          console.log(result);
          if (result.needs_checkout) {
            this.checkoutService.processRedirect(result);
          } else {
            this.toastService.success('MEMBER_LICENSES_CHANGED_OK');
            this.mainHandler.goBack();
          }
        }
      );
    }
  }
}
