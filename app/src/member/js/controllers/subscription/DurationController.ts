import * as angular from 'angular';
import { AvailableSubscriptionInfo, AvailableSubscription } from '../../availableSubscription';
import ToastService from '../../../../misc/toast/ToastService';
import CheckoutService from '../../service/CheckoutService';

/**
 * Change subscription duration.
 *
 * Displays available subscriptions (durations), and lets user choose
 * a new duration (= creates transition ("abowechsel"))-
 */
export default class DurationController {
  periodChange: any;
  currentProduct: any;
  products: any[];
  subscriptionEnd: any;
  productInfo: any;
  changeTime = 'onend';
  productId: number;
  changeTimeOptions: any[] = [
    {
      key: 'onend',
      textkey: 'MEMBER_DURATION_CHANGE_TIME_ONEND',
      tv: '{date: ctrl.mainHandler.dateFormatter(ctrl.data.subscriptionEnd)}'
    },
    {
      key: 'immediate',
      textkey: 'MEMBER_DURATION_CHANGE_TIME_IMMEDIATE',
      tv: ''
    }
  ];

  constructor(
    private $translate,
    public mainHandler,
    public durationChangeService,
    private model,
    private billHandler,
    private checkoutService: CheckoutService,
    public toastService: ToastService,
    public data,
    public availableProducts: AvailableSubscriptionInfo,
    public accountData
  ) {
    this.init();
  }

  init() {
    var changeTimeKey, i, len, ref, transKey, transkey, tv;

    if (!this.data.productId) {
      this.mainHandler.goBack();
      return;
    }

    this.productId = this.data.productId;

    this.currentProduct = this.durationChangeService.findProduct(
      this.data.productId,
      this.data.productList
    );
    this.products = this.durationChangeService.getProductsForDuration(
      this.currentProduct,
      this.data.productList
    );
    this.periodChange = this.data.periodChange;
    this.subscriptionEnd = this.data.subscriptionEnd;
    this.$translate(
      'MEMBER_PRODUCT_GROUP_' + this.currentProduct.productGroupId.toUpperCase()
    ).then(v => (this.productInfo = v));
  }

  hasUpgrade() {
    let selectedProduct = this.availableProducts.products.find(
      p => p.product_id === this.productId
    );

    // TODO: should selectedProuct.discounted_price be checked, if it is set?
    //       this would contain price if a bonus exists

    // TODO: whether price is higher does not decied whether a checkout
    //       will occur ... e.g in case where payment profile is inactive
    return (
      selectedProduct && this.currentProduct && selectedProduct.price > this.currentProduct.price
    );
  }

  canSave() {
    return this.subscriptionCanOrder({ id: this.productId });
  }

  subscriptionCanOrder(product) {
    let availableIds = this.availableProducts.products
      .filter(p => this.changeTime != 'immediate' || p.now == 1)
      .map(p => p.product_id);
    return availableIds.indexOf(product.id) !== -1;
  }

  save() {
    if (this.canSave()) {
      let data = {
        changeTime: this.changeTime,
        productId: this.productId
      };
      return this.model.memberSubscriptionDurationSave(data, result => {
        if (result.needs_checkout) {
          this.checkoutService.processRedirect(result);
        } else {
          this.toastService.success('MEMBER_DURATION_CHANGED_OK');
          this.mainHandler.goBack();
        }
      });
    }
  }
}
