define ['../../app', 'angular'], (app, angular) ->
  app.controller 'member.subscriptionOverviewCtrl', ['$log', '$scope', 'member.mainHandler', 'subscriptionTypeService', 'data'].
  concat ($log, $scope, mainHandler, subscriptionTypeService, data) ->
    angular.extend $scope,
      menu: [
        {titleKey: 'MEMBER_SUBSCRIPTION_DURATION', icon: 'icon-duration-month-' + data, state: 'root.member.subscriptionduration'}
        {titleKey: 'MEMBER_SUBSCRIPTION_PAYMENT', icon: 'icon-cart', state: 'root.member.subscriptionpay'}
        {titleKey: 'MEMBER_SUBSCRIPTION_CANCELLATION', icon: 'icon-radio-off', state: 'root.member.subscriptioncancellation'}
      ]
      mainHandler: mainHandler

    if subscriptionTypeService.isStandardOrgProduct() || subscriptionTypeService.isLicenceProduct()
      $scope.menu.push {titleKey: 'MEMBER_LICENCE_MAINTENANCE', icon: 'icon-product-licence-outlines', state: 'root.member.subscriptionlicences'}




