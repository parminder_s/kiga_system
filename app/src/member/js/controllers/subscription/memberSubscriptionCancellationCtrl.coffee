define ['../../app', 'angular'], (app, angular) ->
  app.controller 'member.subscriptionCancellationCtrl', ['$scope', 'memberModel', 'member.mainHandler', 'data'].
  concat ($scope, model, mainHandler, data) ->
    data.keepUserData=true

    angular.extend $scope,
      data: data
      mainHandler: mainHandler
      cancellation: (form) ->
        if form.$valid
          model.memberSubscriptionCancellationExecute $scope.data, -> mainHandler.goBack()
      undoCancellation: ->
          model.memberSubscriptionCancellationUndo {}, -> mainHandler.goBack()
