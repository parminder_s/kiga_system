import ToastService from '../../../misc/toast/ToastService';
import CheckoutService from '../service/CheckoutService';

export default class BillsSubscriptionController {
  isBillPaid = true;
  unpaidBillAvailable = false;
  isValidationInfoRequired = false;

  constructor(
    private checkoutService: CheckoutService,
    private mainHandler,
    private billHandler,
    private model,
    private toastService: ToastService,
    private data,
    private request
  ) {}

  $onInit() {
    this.isBillPaid = this.billHandler.isBillPaid;
    this.unpaidBillAvailable = this.billHandler.isUnpaidBillAvailable(this.data.invoiceList);
  }

  getDocumentUrl(invoiceNr) {
    return this.billHandler.getDocumentUrl(invoiceNr);
  }

  payNow(form) {
    if (form.$valid) {
      let requestData = {
        paymentMethod: this.data.paymentMethod,
        invoiceOption: this.data.invoiceOption
      };

      this.model.setPaymentToCheckout(
        requestData,
        result => {
          if (result.needs_checkout) {
            this.checkoutService.processRedirect(result);
          } else {
            this.toastService.success('MEMBER_PAYMENT_CHANGED_OK');
            this.mainHandler.goBack();
          }
        },
        err => {}
      );
    }
  }
}
