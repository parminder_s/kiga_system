import IRootScopeService = angular.IRootScopeService;
import AreaFactory from './AreaFactory';
import * as _ from 'lodash';
import { IHookRegistry } from 'angular-ui-router';

export default class AreaStateChangeHandler {
  constructor($transitions: IHookRegistry, areaFactory: AreaFactory) {
    $transitions.onStart({}, () => {
      _.forEach(areaFactory.getAll(), area => {
        area.withAgeGroup(null);
        area.withSearchTags([]);
      });
    });
  }
}
