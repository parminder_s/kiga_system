/**
 * This service updates the searchbox if search tags are set via the area, which is the case
 * when a the user navigates away from the search and afterwards clicks back.
 *
 * In that case the search tags got removed and only the controller passes the new search tags
 * to the Area.
 */

export interface SearchBox {
  updateSearchTags(searchTags: Array<string>);
}

export default class AreaSearchBoxUpdater {
  private searchBox: SearchBox;
  updateSearchBox(searchTags: Array<string>) {
    if (this.searchBox) {
      this.searchBox.updateSearchTags(searchTags);
    }
  }

  registerSearchBox(searchBox: SearchBox) {
    this.searchBox = searchBox;
  }
}
