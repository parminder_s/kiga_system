import * as angular from 'angular';
import * as _uiRouter from 'angular-ui-router';
import { IHookRegistry, StateService } from 'angular-ui-router';
import * as _misc from 'misc';
import { ILinksService } from '../../misc/js/services/AngularInterfaces';
import SearchboxController from '../components/searchbox/SearchboxController';
import AreaFactory from './AreaFactory';
import AreaFetcher from './AreaFetcher';
import AreaSearchBoxUpdater from './AreaSearchBoxUpdater';
import AreaStateChangeHandler from './AreaStateChangeHandler';

let misc = _misc,
  uiRouter = _uiRouter;

export let app = angular.module('area', ['misc']);

app
  .run(
    ($transitions: IHookRegistry, areaFactory: AreaFactory) =>
      new AreaStateChangeHandler($transitions, areaFactory)
  )
  .service(
    'areaFactory',
    ($state: StateService, areaSearchBoxUpdater: AreaSearchBoxUpdater, links: ILinksService) =>
      new AreaFactory($state, areaSearchBoxUpdater, links)
  )
  .service(
    'areaFetcher',
    ($state: StateService, areaFactory: AreaFactory) => new AreaFetcher($state, areaFactory)
  )
  .service('areaSearchBoxUpdater', () => new AreaSearchBoxUpdater())
  .controller(
    'searchboxCtrl',
    (
      areaFetcher: AreaFetcher,
      $translate,
      $transitions,
      areaSearchBoxUpdater: AreaSearchBoxUpdater,
      config,
      links: ILinksService
    ) => new SearchboxController(areaFetcher, $translate, $transitions, areaSearchBoxUpdater, links)
  )
  .component('searchbox', {
    templateUrl: 'area/components/searchbox/searchbox.html',
    transclude: true,
    controller: 'searchboxCtrl'
  });
