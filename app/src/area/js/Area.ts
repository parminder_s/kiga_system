import { ILinksService } from '../../misc/js/services/AngularInterfaces';
import { SubMenu } from './SubMenu';
import { AreaCallback } from './AreaCallback';
import { AreaType } from './AreaType';
import { StateService } from 'angular-ui-router';
import AreaSearchBoxUpdater from './AreaSearchBoxUpdater';

export default class Area {
  subMenus: SubMenu[] = [];
  searchTags: Array<string> = [];
  showLocales: boolean = true;
  showCountries: boolean = false;
  showSearch: boolean = true;
  showAgeGroup: boolean = true;
  ageGroup: number = 0;
  stateName = '';
  url: string;
  stateParams: any = {};
  defaultCallback: AreaCallback = null;
  callback: AreaCallback = null;
  areaType: AreaType = null;

  constructor(
    private css: string,
    private code: string,
    private state: StateService,
    private areaSearchBoxUpdater: AreaSearchBoxUpdater,
    private links: ILinksService
  ) {}

  withCallback(callback: AreaCallback): Area {
    this.callback = callback;
    return this;
  }

  withSubMenus(subMenus: SubMenu[]): Area {
    this.subMenus = subMenus;
    return this;
  }

  withShowLocales(showLocales: boolean): Area {
    this.showLocales = showLocales;
    return this;
  }

  withShowCountries(showCountries: boolean): Area {
    this.showCountries = showCountries;
    return this;
  }

  withSearchTags(searchTags: Array<string>): Area {
    this.searchTags = searchTags;
    this.areaSearchBoxUpdater.updateSearchBox(searchTags);
    return this;
  }

  withAgeGroup(ageGroup: any): Area {
    if (ageGroup === '0' || !ageGroup) {
      this.ageGroup = 0;
    } else {
      this.ageGroup = parseInt(ageGroup);
    }
    return this;
  }

  withShowSearch(showSearch: boolean): Area {
    this.showSearch = showSearch;
    return this;
  }

  withShowAgeGroup(showAgeGroup: boolean): Area {
    this.showAgeGroup = showAgeGroup;
    return this;
  }

  withState(stateName: string, stateParams: any = {}): Area {
    this.stateName = stateName;
    this.stateParams = stateParams;
    return this;
  }

  withUrl(url: string): Area {
    this.url = url;
    return this;
  }

  getLink() {
    if (this.url) {
      return this.links.findByCode(this.url).url;
    }
    if (this.stateName) {
      return this.state.href(this.stateName, this.stateParams);
    } else {
      return '';
    }
  }

  setDefaultCallback(defaultCallback: AreaCallback) {
    this.defaultCallback = defaultCallback;
  }

  invokeCallback() {
    if (this.callback === null) {
      if (this.defaultCallback === null) {
        throw 'no callback for area defined';
      } else {
        this.defaultCallback.handle(this.searchTags, this.ageGroup);
      }
    } else {
      this.callback.handle(this.searchTags, this.ageGroup);
    }
  }
}
