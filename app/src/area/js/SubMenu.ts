export interface SubMenu {
  label: String;
  link: String;
}
