import { StateService } from 'angular-ui-router';
import * as _ from 'lodash';
import { ILinksService } from '../../misc/js/services/AngularInterfaces';
import Area from './Area';
import AreaSearchBoxUpdater from './AreaSearchBoxUpdater';
import { AreaType } from './AreaType';

export default class AreaFactory {
  cache: { [area: string]: Area } = {};
  instancesCreated = false;

  constructor(
    private state: StateService,
    private areaSearchBoxUpdate: AreaSearchBoxUpdater,
    private links: ILinksService
  ) {}

  public getInstance(areaType: AreaType) {
    if (!this.instancesCreated) {
      this.createInstances();
    }
    return this.cache[areaType];
  }

  createInstances() {
    this.cache[AreaType.DEFAULT] = new Area(
      'green',
      'default',
      this.state,
      this.areaSearchBoxUpdate,
      this.links
    ).withState('root.home');
    this.cache[AreaType.CMS] = new Area(
      'orange',
      'ideas',
      this.state,
      this.areaSearchBoxUpdate,
      this.links
    )
      .withState('root.cms', { path: 'materials' })
      .withShowLocales(false);
    this.cache[AreaType.IDEAS] = new Area(
      'orange',
      'ideas',
      this.state,
      this.areaSearchBoxUpdate,
      this.links
    )
      .withUrl('ideaGroupMainContainer')
      .withShowLocales(false);
    this.cache[AreaType.NONE] = new Area(
      'none',
      'none',
      this.state,
      this.areaSearchBoxUpdate,
      this.links
    );
    this.cache[AreaType.PLANNER] = new Area(
      'kigaBlue',
      'planner',
      this.state,
      this.areaSearchBoxUpdate,
      this.links
    )
      .withState('root.plan.planType.monthly', { planType: 'kiga' })
      .withShowAgeGroup(false)
      .withShowSearch(false);
    this.cache[AreaType.SHOP] = new Area(
      'shopColor',
      'shop',
      this.state,
      this.areaSearchBoxUpdate,
      this.links
    )
      .withState('root.cms', { path: 'shop' })
      .withShowSearch(false)
      .withShowCountries(true)
      .withShowAgeGroup(false)
      .withShowLocales(false);
    this.cache[AreaType.FAVOURITES] = new Area(
      'violet',
      'favourites',
      this.state,
      this.areaSearchBoxUpdate,
      this.links
    )
      .withShowSearch(false)
      .withShowAgeGroup(false)
      .withShowLocales(false)
      .withState('root.memberArea.favourites');
    this.cache[AreaType.MEMBERIDEAS] = new Area(
      'violet',
      'memberIdeas',
      this.state,
      this.areaSearchBoxUpdate,
      this.links
    )
      .withShowSearch(false)
      .withShowAgeGroup(false)
      .withState('root.memberArea.ideas.upload');

    this.instancesCreated = true;
  }

  getAll() {
    return _.map(this.cache, function(area: Area) {
      return area;
    });
  }
}
