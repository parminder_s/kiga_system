export enum AreaType {
  DEFAULT = 1,
  PLANNER = 2,
  IDEAS = 3,
  NONE = 4,
  SHOP = 5,
  CMS = 6,
  FAVOURITES = 7,
  MEMBERIDEAS = 8
}
