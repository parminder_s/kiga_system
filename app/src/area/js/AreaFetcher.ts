import { AreaType } from './AreaType';
import Area from './Area';
import AreaFactory from './AreaFactory';

export default class AreaFetcher {
  constructor(private state: any, private areaFactory: AreaFactory) {}

  getArea(): Area {
    return this.areaFactory.getInstance(this.getFirstAreaInState());
  }

  private getFirstAreaInState() {
    let currentState = this.state;

    while (currentState) {
      if (currentState.params && currentState.params['area']) {
        return currentState.params['area'];
      } else {
        currentState = currentState.parent;
      }
    }
    return AreaType.DEFAULT;
  }
}
