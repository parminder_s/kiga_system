export interface AreaCallback {
  handle(searchTags: Array<string>, ageGroup: number);
}
