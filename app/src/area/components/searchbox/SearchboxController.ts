import * as _ from 'lodash';
import { IHookRegistry } from 'angular-ui-router';
import { ILinksService } from '../../../misc/js/services/AngularInterfaces';
import AreaFetcher from '../../js/AreaFetcher';
import AreaSearchBoxUpdater, { SearchBox } from '../../js/AreaSearchBoxUpdater';

/**
 * Created by faxxe on 12/7/16.
 */

export default class SearchboxController implements SearchBox {
  private query: string = '';

  constructor(
    private areaFetcher: AreaFetcher,
    private $translate,
    private $transitions: IHookRegistry,
    private areaSearchBoxUpdate: AreaSearchBoxUpdater,
    private links: ILinksService
  ) {}

  $onInit() {
    let area = this.areaFetcher.getArea();
    this.query = this.searchTagsToQuery(area.searchTags);
    this.registerSearchBoxCleaner();
    this.areaSearchBoxUpdate.registerSearchBox(this);
  }

  public getPlaceHolder() {
    return this.$translate.instant('SEARCH_PLACEHOLDER');
  }

  public onSearchSubmit() {
    if (!this.query) {
      window.location.href = this.links.findByCode('ideaGroupMainContainer').url;
    } else {
      this.areaFetcher
        .getArea()
        .withSearchTags(this.queryToTags(this.query))
        .invokeCallback();
    }
  }

  public updateSearchTags(searchTags: Array<string>) {
    this.query = this.searchTagsToQuery(searchTags);
  }

  private searchTagsToQuery(searchTags: Array<string>) {
    return this.queryToTags(searchTags.join(' ')).join(' ');
  }

  private registerSearchBoxCleaner() {
    /*
      if the user navigates to another page, we want to make sure
      that the search box is empty.
     */
    this.$transitions.onBefore({}, () => {
      this.query = '';
    });
  }

  private queryToTags(query: string): Array<string> {
    let tags = query
      .replace(/</g, '')
      .replace(/>/g, '')
      .replace(/\?/g, '')
      .trim()
      .replace(/\s\s+/, ' ')
      .split(' ');

    return _(tags)
      .filter(tag => !!tag)
      .uniq()
      .value();
  }
}
