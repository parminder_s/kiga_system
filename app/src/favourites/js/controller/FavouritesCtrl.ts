import { StateService } from 'angular-ui-router';
import { AreaType } from '../../../area/js/AreaType';
import { FavouritesModel } from '../../../misc/js/model/FavouritesModel';
import PermissionStateResolver from '../../../misc/permission/js/PermissionStateResolver';
import IEndpoint from '../../../misc/js/services/IEndpoint';
import { ResolverRequest } from '../../../cms/js/requests/ResolverRequest';

/**
 * Created by bbs on 11/5/16.
 */

export class FavouritesCtrl {
  private requestedPath: string;

  private ideaCategoryIndex;
  private ideaIndex;

  private $document;
  private $scope;
  private $rootScope;
  private $timeout;
  private $state: StateService;
  private $stateParams;
  private ideasLink;

  private endpoint;

  private breadcrumbs = [];
  private selectedHierarchy: string[] = [];
  private selectedHierarchyPartsCount: number = 0;
  private isLastNavLevel: boolean = false;
  private maxIdeasPerRow = 20;
  private activePreviewPage = null;
  private ideaLinksResolver = null;
  private permissionStateResolver: PermissionStateResolver;
  public feed: any;

  private idea;
  private ideaObject;
  private member;

  private ideaLinks: any[] = [];

  private ageGroupFilter: number = null;
  private template;
  private allFavouritesVirtualCategory = null;

  constructor(
    angularProperties,
    feed,
    private $translate,
    private favouritesModel: FavouritesModel,
    endpoint: IEndpoint
  ) {
    this.loadPage(angularProperties, feed);

    const {
      $stateParams: { path, ageGroupFilter }
    } = angularProperties;

    this.favouritesModel.notifyForUpdate(() => {
      endpoint
        .post({
          url: 'favourites/resolve',
          data: new ResolverRequest(path, ageGroupFilter),
          quietMode: true
        })
        .then(feed => this.loadPage(angularProperties, feed));
    });
  }

  /**
   * This service sets up the favourites page. It is also executed, if
   * we get an update notification from the FavouritesService, which
   * comes from the Angular part.
   */
  loadPage(angularProperties, feed) {
    this.feed = feed;
    this.breadcrumbs = this.feed.breadcrumbs;
    this.member = angularProperties.member;
    this.$state = angularProperties.$state;
    this.$stateParams = angularProperties.$stateParams;
    this.ideaLinksResolver = angularProperties.ideaLinksResolver;
    this.permissionStateResolver = angularProperties.permissionStateResolver;
    this.ideasLink = angularProperties.links.findByCode('ideaGroupMainContainer').url;
    this.template = 'favourites/partials/favourites.html';

    this.allFavouritesVirtualCategory = {
      icon: 'icon-infinite',
      name: null,
      title: this.$translate.instant('FAVOURITES_ALL')
    };

    this.$document = angularProperties.$document;
    this.$scope = angularProperties.$scope;
    this.$rootScope = angularProperties.$rootScope;
    this.$timeout = angularProperties.$timeout;
    this.$state.params['area'] = AreaType.FAVOURITES;

    this.idea = angularProperties.$stateParams.preview;
    this.ageGroupFilter = angularProperties.$stateParams.ageGroupFilter;

    if (!this.idea) {
      this.$document.duScrollTo(0, 0);
    }

    this.updateScrollers();
    this.requestedPath = angularProperties.$stateParams.path;
    this.endpoint = angularProperties.endpoint;

    let selectedHierarchy = this.requestedPath.split('/');
    this.selectedHierarchy = [];
    for (let i = 0; i < selectedHierarchy.length; ++i) {
      if (selectedHierarchy[i] != null && selectedHierarchy[i].trim().length > 0) {
        this.selectedHierarchy.push(selectedHierarchy[i].trim());
      }
    }

    this.selectedHierarchyPartsCount = this.selectedHierarchy.length;
    this.insertAllFavouritesButton();
    this.init();
  }

  insertAllFavouritesButton() {
    if (!this.feed.parentCategories) {
      this.feed.parentCategories = this.feed.categories.slice(0);
    }
    if (this.selectedHierarchy.length < 2) {
      this.feed.parentCategories.unshift(this.allFavouritesVirtualCategory);
    }
  }

  toggleFavourite(idea) {
    idea.favourite = !idea.favourite;
    this.favouritesModel.toggleFavourite(idea.id);
  }

  getStars(): any[] {
    let rating = null;
    if (this.ideaObject) {
      this.ideaObject.rating.rating;
    }

    let stars = [];
    for (let i = 0; i < 5; ++i) {
      if (rating <= i) {
        stars.push('fa-star-o');
      } else if (rating === i + 0.5) {
        stars.push('fa-star-half-o');
      } else if (rating > i + 0.5) {
        stars.push('fa-star');
      }
    }

    return stars;
  }

  addAgeGroupFilter(ageGroupFilter: number) {
    this.ageGroupFilter = ageGroupFilter;
    this.updateUrl();
  }

  doForEveryIdea(callback) {
    let category, categoryIdx, idea, idx, j, k, len, len1, ref, ref1;
    this.updateScrollers();
    ref = this.feed.categories;
    for (categoryIdx = j = 0, len = ref.length; j < len; categoryIdx = ++j) {
      category = ref[categoryIdx];
      ref1 = category.ideas;
      for (idx = k = 0, len1 = ref1.length; k < len1; idx = ++k) {
        idea = ref1[idx];
        callback(categoryIdx, idx, idea);
      }
    }
  }

  init() {
    this.doForEveryIdea((categoryIdx, ideaIdx, idea) => {
      let previewCategory = parseInt(this.$stateParams.previewCategory);
      if (
        (this.idea === idea.name && previewCategory === categoryIdx) ||
        (this.idea === idea.name && !previewCategory && !this.ideaObject)
      ) {
        return this.openPreview(categoryIdx, ideaIdx);
      }
    });
    this.favouritesModel.listFavouritesForIdsAsPromise().then(data => {
      let favs = data.favourites;
      this.doForEveryIdea((cIdx, ideaIdx, idea) => {
        for (let i = 0; i < favs.length; ++i) {
          if (favs[i].ideaId === idea.id) {
            idea.favourite = true;
          }
        }
      });
    });

    if (this.isLastNavigationLevel() && !this.idea) {
      this.openPreview(0, 0);
    }
  }

  allowEdit() {
    return this.member.isCms;
  }

  planType() {
    if (this.member.isCms) {
      return 'kiga';
    } else {
      return 'member';
    }
  }

  selectPreviewPage(page) {
    return (this.activePreviewPage = page);
  }

  isLastNavigationLevel() {
    return (
      this.isLastNavLevel ||
      (this.feed.activeCategory != null && this.feed.activeCategory.lastCategoryLevel)
    );
  }

  toggleContext() {
    let reloadPage = this.$state.reload;
    this.endpoint('content/repository-context/toggle').then(function(response) {
      reloadPage();
    });
  }

  updateUrl() {
    return this.$state.go(
      'root.memberArea.favourites',
      {
        path: decodeURI(this.selectedHierarchy.join('/')),
        preview: this.idea,
        previewCategory: this.ideaCategoryIndex,
        ageGroupFilter: this.ageGroupFilter
      },
      { notify: true }
    );
  }

  findIdeaByName(ideaName) {
    let category, categoryIdx, idea, idx, j, k, len, len1, ref, ref1;
    ref = this.feed.categories;
    for (categoryIdx = j = 0, len = ref.length; j < len; categoryIdx = ++j) {
      category = ref[categoryIdx];
      ref1 = category.ideas;
      for (idx = k = 0, len1 = ref1.length; k < len1; idx = ++k) {
        idea = ref1[idx];
        if (ideaName === idea.name) {
          return idea;
        }
      }
    }
  }

  isPreviewFromCategory(categoryIndex) {
    return this.ideaCategoryIndex === categoryIndex;
  }

  generateArrowBtnClass() {
    let categoriesLength = this.feed.categories.length;
    let level = this.selectedHierarchy.length;
    let arrow_container_md = false;
    let arrow_container_small = false;

    if (level) {
      if (categoriesLength === 5 || categoriesLength === 6) {
        arrow_container_md = true;
      } else if (categoriesLength === 3 || categoriesLength === 4) {
        arrow_container_small = true;
      }
    } else {
      if (categoriesLength === 6 || categoriesLength === 7) {
        arrow_container_md = true;
      } else if (categoriesLength === 4 || categoriesLength === 5) {
        arrow_container_small = true;
      }
    }

    return {
      'arrow-container-small': arrow_container_small,
      'arrow-container-md': arrow_container_md
    };
  }

  generateBtnRowClass() {
    let categoriesLength = this.feed.categories.length;
    let level = this.selectedHierarchy.length;
    let sort_row_small = false;
    let sort_row_small_plus = false;
    let sort_row_md = false;
    let sort_row_md_plus = false;

    if (level) {
      if (categoriesLength === 3) {
        sort_row_small = true;
      } else if (categoriesLength === 4) {
        sort_row_small_plus = true;
      } else if (categoriesLength === 5) {
        sort_row_md = true;
      } else if (categoriesLength === 6) {
        sort_row_md_plus = true;
      }
    } else {
      if (categoriesLength === 4) {
        sort_row_small = true;
      } else if (categoriesLength === 5) {
        sort_row_small_plus = true;
      } else if (categoriesLength === 6) {
        sort_row_md = true;
      } else if (categoriesLength === 7) {
        sort_row_md_plus = true;
      }
    }

    return {
      'sort-row-small': sort_row_small,
      'sort-row-small-plus': sort_row_small_plus,
      'sort-row-md': sort_row_md,
      'sort-row-md-plus': sort_row_md_plus
    };
  }

  onTopCategoryClick() {
    if (this.feed.firstLevelCategoryName) {
      // take the user one layer up
      this.selectedHierarchy.pop();
    } else {
      this.selectedHierarchy = [];
    }
    this.closePreview(false);
    this.updateScrollers();
    return this.updateUrl();
  }

  onCategoryClick(categoryIndex) {
    let clickedCategoryName = this.feed.categories[categoryIndex].name;
    let clickedCategoryTitle = this.feed.categories[categoryIndex].title;
    let isNewIdeas =
      this.feed.categories[categoryIndex].ageGroup ||
      this.feed.categories[categoryIndex].ageGroupContainer;

    this.selectedHierarchy.push(clickedCategoryName);

    this.closePreview(false);
    this.updateScrollers();
    return this.updateUrl();
  }

  onCategoryButtonClick(categoryIndex) {
    if (!this.feed.parentCategories) {
      let clickedCategoryName = this.feed.categories[categoryIndex].name;
      this.selectedHierarchy.push(clickedCategoryName);
    } else {
      let clickedCategoryName = this.feed.parentCategories[categoryIndex].name;
      if (this.selectedHierarchy.length === 0) {
        this.selectedHierarchy.push(clickedCategoryName);
      } else {
        this.selectedHierarchy[this.selectedHierarchy.length - 1] = clickedCategoryName;
      }
    }

    this.closePreview(false);
    this.updateScrollers();
    return this.updateUrl();
  }

  onBreadcrumbClick(index) {
    this.selectedHierarchy = this.selectedHierarchy.slice(0, index + 1);
    this.closePreview(false);
    this.updateScrollers();
    return this.updateUrl();
  }

  closePreview(updateUrl) {
    this.ideaCategoryIndex = null;
    this.ideaIndex = null;
    this.ideaObject = null;
    this.idea = null;
    if (updateUrl) {
      return this.updateUrl();
    }
  }

  onIdeaClick(categoryIndex, idx) {
    if (categoryIndex === this.ideaCategoryIndex && idx === this.ideaIndex) {
      this.closePreview(true);
      return;
    }
    return this.openPreview(categoryIndex, idx);
  }

  scrollToPreview(categoryIndex) {
    let delay, el;
    if (this.idea !== void 0) {
      el = this.$document.find('.category-' + categoryIndex);
      if (el.length) {
        delay = 0;
      } else {
        delay = 1000;
      }

      return this.$timeout(() => {
        let elPos, firstElPos, header, navbar, i, j, ref;
        elPos = 0;
        if (categoryIndex > 0) {
          for (
            i = j = 0, ref = categoryIndex - 1;
            0 <= ref ? j <= ref : j >= ref;
            i = 0 <= ref ? ++j : --j
          ) {
            el = this.$document.find('.category-' + i);
            elPos += el.outerHeight();
          }
        }
        el = this.$document.find('.category-0');
        if (el.length) {
          header = this.$document.find('.sticky-top-nav');
          navbar = this.$document.find('.navbar-header');
          firstElPos = el.offset().top - header.outerHeight();
          return this.$document.duScrollTo(0, elPos + firstElPos - navbar.outerHeight());
        }
      }, delay);
    }
  }

  openPreview(categoryIndex, idx) {
    this.scrollToPreview(categoryIndex);
    this.ideaCategoryIndex = categoryIndex;
    this.ideaIndex = idx;
    this.ideaObject = this.feed.categories[categoryIndex].ideas[idx];
    this.idea = this.ideaObject.name;

    if (this.ideaObject.previewPages.length) {
      this.activePreviewPage = this.ideaObject.previewPages[0];
    } else {
      this.activePreviewPage = null;
    }
    this.ideaObject.previewScrollButtonsVisibility =
      this.ideaObject.previewPages.length > 3 ? 'visible' : 'hidden';
    this.updateUrl();
    this.ideaLinks = null;
    return;
  }

  getIdeaViewLink(_idea) {
    return this.ideaLinksResolver.getLinks(_idea, true).view.link;
  }

  getIdeaLinks() {
    if (!this.ideaLinks) {
      this.ideaLinks = this.ideaLinksResolver.getLinks(this.ideaObject, true);
    }
    return this.ideaLinks;
  }

  getIdeas() {
    if (!this.ideaLinks) {
      this.ideaLinks = this.ideaLinksResolver.getLinks(this.ideaObject, true);
    }
    return this.ideaLinks;
  }

  prevItem(categoryIndex) {
    this.ideaIndex -= 1;
    return this.openPreview(this.ideaCategoryIndex, this.ideaIndex);
  }

  nextItem(categoryIndex) {
    this.ideaIndex += 1;
    return this.openPreview(this.ideaCategoryIndex, this.ideaIndex);
  }

  updateScrollers() {
    let category, el, index, j, len, ref, scroll, scrollPanes;
    scrollPanes = this.$document.find('.ideas [scroll-pane]');
    scrollPanes.each(function(idx, pane) {
      if (angular.element(pane).controller('scrollPane') != null) {
        return angular
          .element(pane)
          .controller('scrollPane')
          .updateButtonsVisibility();
      }
    });
    if (this.feed.activeCategory != null) {
      el = this.$document.find('.ideas .sort-row');
      scroll = el.controller('scrollPane');
      if (scroll != null) {
        ref = this.feed.categories;
        for (index = j = 0, len = ref.length; j < len; index = ++j) {
          category = ref[index];
          if (category.name === this.feed.activeCategory) {
            return scroll.scrollToIndex(index + 1, true);
          }
        }
      }
    }
  }

  getRequestedPath(): string {
    return this.requestedPath;
  }

  isAllowedMember() {
    return !this.member.hasExpiredSubscription() && this.member.hasStandardSubscription();
  }
}
