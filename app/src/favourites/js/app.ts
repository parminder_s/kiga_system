/**
 * Created by bbs on 11/5/16.
 */
import * as angular from 'angular';
import * as _misc from 'misc';

let misc = _misc;

export let app = angular.module('favourites', ['misc']);
