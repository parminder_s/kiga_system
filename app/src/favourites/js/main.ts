import AreaFechter from '../../area/js/AreaFetcher';
import IdeaLinksResolver from '../../ideas/js/services/IdeaLinksResolver';
import { FavouritesModel } from '../../misc/js/model/FavouritesModel';
import PermissionStateResolver from '../../misc/permission/js/PermissionStateResolver';
/**
 * Created by bbs on 6/20/16.
 */
import { app } from './app';
import { FavouritesCtrl } from './controller/FavouritesCtrl';
import IEndpoint from '../../misc/js/services/IEndpoint';

app.controller('favouritesCtrl', function(
  $document,
  $scope,
  $timeout,
  $state,
  $stateParams,
  $window,
  $rootScope,
  $translate,
  areaFetcher: AreaFechter,
  feed,
  endpoint: IEndpoint,
  config,
  ideaLinksResolver: IdeaLinksResolver,
  permissionStateResolver: PermissionStateResolver,
  member,
  favouritesModel: FavouritesModel,
  links
) {
  let angularProperties = {
    $document: $document,
    $scope: $scope,
    $rootScope: $rootScope,
    $state: $state,
    $timeout: $timeout,
    $stateParams: $stateParams,
    endpoint: endpoint,
    areaFetcher: areaFetcher,
    $window: $window,
    ideaLinksResolver: ideaLinksResolver,
    permissionStateResolver: permissionStateResolver,
    config: config,
    member: member,
    links: links
  };

  return new FavouritesCtrl(angularProperties, feed, $translate, favouritesModel, endpoint);
});
