import { StateService } from 'angular-ui-router';
import * as _ from 'lodash';

export default class CheckoutHeaderController {
  public steps = ['cart', 'shipping', 'payment', 'summary'];
  public step: string;
  public translateKey: string;

  constructor(private $state: StateService) {}

  $onInit() {}

  isFirstStep(step: string) {
    return step === 'cart';
  }

  isStepActive(step: string) {
    let state = _.last(this.$state.current.name.split('.'));
    return !(this.steps.indexOf(step) > this.steps.indexOf(state));
  }

  activateStep(step: string) {
    this.$state.go('root.shop.' + step);
  }

  isCurrentStepActive() {
    return this.isStepActive(this.step);
  }
}
