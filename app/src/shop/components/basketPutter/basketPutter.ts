import { IGoBack } from '../../../misc/js/services/AngularInterfaces';

export default class BasketPutterCtrl {
  private productId: number;
  private availableInCountry: boolean;
  private countryName: string;
  private pcsSelected: string;
  private pieces: { [key: number]: string };

  constructor(private $window, private shopBasketService, private goBack: IGoBack) {}

  $onInit() {
    this.pcsSelected = '1';
    this.pieces = _.mapValues(_.keyBy(_.range(1, 100), val => val), val => val.toString());
  }

  cancel() {
    this.goBack.goBack();
  }

  addToBasket() {
    this.shopBasketService.addToBasket(this.productId, parseInt(this.pcsSelected));
  }
}
