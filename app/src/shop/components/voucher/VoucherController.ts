export default class VoucherController {
  private code: string;
  private showError: boolean;
  private submit: () => void;

  applyButtonClicked() {
    if (this.code.trim()) {
      this.submit();
    }
  }
}
