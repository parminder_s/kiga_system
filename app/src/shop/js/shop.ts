import * as angular from 'angular';
import { StateService } from 'angular-ui-router';
import * as _misc from 'misc';
import { CmsRenderersFactory } from '../../cms/js/factories/CmsRenderersFactory';
import { IConfigService, IGoBack, ILinksService } from '../../misc/js/services/AngularInterfaces';
import IEndpoint from '../../misc/js/services/IEndpoint';
import PermissionHandlerRegistry from '../../misc/permission/js/handlers/PermissionHandlerRegistry';
import ShopCheckoutPermissionHandler from '../../misc/permission/js/handlers/ShopCheckoutPermissionHandler';
import IMemberService from '../../misc/permission/js/IMemberService';
import BasketPutterCtrl from '../../shop/components/basketPutter/basketPutter';

import CheckoutHeaderController from '../components/checkoutHeader/CheckoutHeaderController';
import VoucherController from '../components/voucher/VoucherController';
import { ProductPageRenderer } from './controllers/ProductPageRenderer';
import ShopCartController from './controllers/ShopCartController';
import { ShopCategoryRenderer } from './controllers/ShopCategoryRenderer';
import { ShopFinishedCtrl } from './controllers/shopFinishedCtrl';
import ShopMainCtrl from './controllers/ShopMainCtrl';
import ShopPaymentController from './controllers/ShopPaymentController';
import ShopShippingCtrl from './controllers/ShopShippingCtrl';
import SummaryCtrl from './controllers/SummaryCtrl';
import ShopBasketService from './service/ShopBasketService';

let misc = _misc;
let shopModule = angular
  .module('shop', ['misc'])
  .component('blockItem', {
    templateUrl: 'shop/partials/blockItem.html',
    bindings: {
      item: '<'
    }
  })
  .component('basketPutter', {
    templateUrl: 'shop/components/basketPutter/basketPutter.html',
    controller: ($window, shopBasketService, goBack: IGoBack) =>
      new BasketPutterCtrl($window, shopBasketService, goBack),
    bindings: {
      productId: '<',
      availableInCountry: '<',
      countryName: '<'
    }
  })
  .component('checkoutHeader', {
    templateUrl: 'shop/components/checkoutHeader/checkoutHeader.html',
    controller: ($state: StateService) => new CheckoutHeaderController($state),
    bindings: {
      step: '@',
      translateKey: '@'
    }
  })
  .component('voucher', {
    templateUrl: 'shop/components/voucher/voucher.html',
    bindings: {
      code: '=',
      showError: '<',
      submit: '&'
    },
    controller: () => new VoucherController()
  })
  .controller(
    'shopCartCtrl',
    (
      $state,
      shopBasketService,
      basketData,
      amountList,
      canDeliver,
      context,
      $window,
      basketItemsService,
      links: ILinksService,
      goHome
    ) =>
      new ShopCartController(
        $state,
        shopBasketService,
        basketData,
        amountList,
        canDeliver,
        context,
        $window,
        basketItemsService,
        links,
        goHome
      )
  )
  .controller('shopFinishedCtrl', function(goHome) {
    return new ShopFinishedCtrl(goHome);
  })
  .controller('shopMainCtrl', function($state) {
    return new ShopMainCtrl($state);
  })
  .controller('shopPaymentCtrl', function(
    $state,
    $stateParams,
    shopBasketService,
    paymentMethods,
    basketData,
    config
  ) {
    return new ShopPaymentController(
      $state,
      $stateParams,
      shopBasketService,
      paymentMethods,
      basketData,
      config
    );
  })
  .controller(
    'shopShippingCtrl',
    ($state, shopBasketService, basketData, goHome) =>
      new ShopShippingCtrl($state, shopBasketService, basketData, goHome)
  )
  .controller(
    'shopSummaryCtrl',
    ($state, shopBasketService, basketData) =>
      new SummaryCtrl($state, shopBasketService, basketData)
  )
  .service(
    'shopBasketService',
    (request, endpoint: IEndpoint, config: IConfigService, $state: StateService) =>
      new ShopBasketService(request, endpoint, config, $state)
  )
  .service(
    'productPageRenderer',
    ($window, $state, endpoint, member, $transitions, audioHandler) =>
      new ProductPageRenderer($window, $state, endpoint, member, $transitions, audioHandler)
  )
  .service(
    'shopCategoryRenderer',
    ($document, $timeout, $state, $window, endpoint, links, member) =>
      new ShopCategoryRenderer($document, $timeout, $state, $window, endpoint, links, member)
  )
  .service(
    'shopCheckoutPermissionHandler',
    (member: IMemberService, $state: StateService) =>
      new ShopCheckoutPermissionHandler(member, $state)
  )
  .run(
    (
      cmsRenderersFactory: CmsRenderersFactory,
      productPageRenderer: ProductPageRenderer,
      shopCategoryRenderer: ShopCategoryRenderer,
      permissionHandlerRegistry: PermissionHandlerRegistry,
      shopCheckoutPermissionHandler: ShopCheckoutPermissionHandler
    ) => {
      /**
       *  do not forget to register the resolved types also at the @CmsModuleRegistry
       * in the run lifecycle of the main module.
       **/
      cmsRenderersFactory.register('ShopMainContainer', shopCategoryRenderer);
      cmsRenderersFactory.register('ShopCategory', shopCategoryRenderer);
      cmsRenderersFactory.register('ShopArticlePage', productPageRenderer);
      permissionHandlerRegistry.registerHandler('shopCheckout', shopCheckoutPermissionHandler);
    }
  );

export default shopModule;
