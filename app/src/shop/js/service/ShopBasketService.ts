import { Feature, IConfigService } from '../../../misc/js/services/AngularInterfaces';
import IEndpoint from '../../../misc/js/services/IEndpoint';
import { StateService } from 'angular-ui-router';

interface BasketProduct {
  id: number;
  productId: number;
  title: string;
  amount: number;
  price: number; // is higher then the original one, if a discount was applied.
  originalPrice: number;
  sum: number;
  originalSum: number; // same as price and originalPrice
  hasDiscount: boolean;
  minDeliveryDays: number;
  maxDeliveryDays: number;
}

interface Shipping {
  cost: number;
  to: string;
}

interface ApplyVoucherResponse {
  successful: boolean;
  basket: BasketResponse;
}

export interface BasketResponse {
  id: number;
  products: Array<BasketProduct>;
  shipping: Shipping;
  totalWithoutDelivery: number;
  total: number;
  currency: string;
  country: string;
}

export default class ShopBasketService {
  id: number = 0;
  data: any;

  constructor(
    private request,
    private endpoint: IEndpoint,
    private config: IConfigService,
    private $state: StateService
  ) {}

  find(): Promise<BasketResponse> {
    return this.endpoint.get<BasketResponse>('shop/basket/find');
  }

  resume() {
    return this.requestWithDataSet('shop/basket/resume');
  }

  canDeliver() {
    return this.endpoint.get('shop/basket/canDeliver');
  }

  findWithCheckout() {
    return this.requestWithDataSet('shop/checkout/resume');
  }

  getPaymentMethods(id: number) {
    return this.request({
      url: 'shop/checkout/getPaymentMethods',
      data: { id: id }
    });
  }

  removeItem(id, productId, cb) {
    return this.endpoint
      .post({
        url: `shop/basket/remove-product/${productId}`
      })
      .then(cb);
  }

  setAmount(product, cb) {
    return this.endpoint
      .post({
        url: 'shop/basket/set-amount',
        data: { productId: product.productId, amount: product.amount }
      })
      .then(cb);
  }

  submitCart(newData, cb) {
    return this.requestWithDataSet(
      {
        url: 'shop/checkout/submitCart',
        data: { id: newData.id }
      },
      cb
    );
  }

  submitShipping(newData, cb) {
    return this.requestWithDataSet(
      {
        url: 'shop/checkout/submitShipping',
        data: newData
      },
      cb
    );
  }

  submitPayment(method, cb) {
    return this.requestWithDataSet(
      {
        url: 'shop/checkout/submitPayment',
        data: method
      },
      cb
    );
  }

  submit(data) {
    return this.requestWithDataSet({
      url: 'shop/checkout/finish',
      data: this.data
    });
  }

  addToBasket(productId, amount) {
    this.endpoint
      .post({
        url: 'shop/basket/add-product',
        data: { productId: productId, amount: amount }
      })
      .then(() => this.$state.go('root.shop.basket'));
  }

  requestWithDataSet(requestData, cb = null) {
    return this.request(requestData).then(data => {
      this.data = data;
      if (cb) {
        cb(this.data);
      } else {
        return data;
      }
    });
  }

  applyVoucher(voucherCode: string) {
    return this.endpoint.post<ApplyVoucherResponse>({
      url: 'shop/basket/apply-voucher',
      data: { code: voucherCode }
    });
  }
}
