import { AreaType } from '../../../area/js/AreaType';
import { Renderer } from '../../../cms/js/renderers/Renderer';
import ViewModel from '../../../cms/js/ViewModel';
import IMember from '../../../misc/permission/js/IMember';
import IMemberService from '../../../misc/permission/js/IMemberService';

export class ProductPageRenderer implements Renderer {
  public rootBreadcrumbTitle: string = 'SHOP_HOMEPAGE_TITLE';

  public partial: string = 'shop/partials/product.html';
  public viewModel: ViewModel;
  private productPage;

  private showEditLink: boolean = false;
  private editLink: string;

  member: IMember;
  postContent: String;

  constructor(
    private $window,
    private $state,
    private endpoint,
    memberLoader: IMemberService,
    private $transitions,
    private audioHandler
  ) {
    this.member = memberLoader.get();
  }

  initialize(viewModel: ViewModel) {
    (<any>require)(['../../css/shop.less']);
    this.productPage = viewModel;
    this.$state.params['area'] = AreaType.SHOP;

    if (this.productPage.userRating) {
      this.productPage.ideasResponseRating.rating = this.productPage.userRating;
    }
    if (this.productPage.sliderItems.length) {
      this.productPage.preview = this.productPage.sliderItems[0].imageUrl;
      this.productPage.previewFileType = this.productPage.sliderItems[0].fileType;
      if (this.productPage.previewFileType === 'video') {
        this.productPage.videoUrl = this.productPage.sliderItems[0].videoUrl;
      }
    }

    if (this.member.isCms) {
      this.showEditLink = true;
      this.editLink = this.$state.href('root.backend.cmsBackend.sliderItems', {
        productPageId: this.productPage.id
      });
    }

    this.$transitions.onStart({ to: '**' }, () => {
      this.audioHandler.flushMusic();
    });
  }

  selectPreviewItem(index) {
    this.productPage.preview = this.productPage.sliderItems[index].imageUrl;
    this.productPage.previewFileType = this.productPage.sliderItems[index].fileType;
    if (this.productPage.previewFileType === 'video') {
      this.productPage.videoUrl = this.productPage.sliderItems[index].videoUrl;
    }
  }

  onBreadcrumbClick(index) {}

  onTopCategoryClick() {}
}
