import { StateService } from 'angular-ui-router';
import { StateParams } from 'angular-ui-router';
import { Feature, IConfigService } from '../../../misc/js/services/AngularInterfaces';

export default class ShopPaymentController {
  showError: boolean = true;
  showInvoice: boolean = true;

  constructor(
    private $state: StateService,
    private $stateParams: StateParams,
    private shopBasketService,
    private paymentMethods,
    private data,
    config: IConfigService
  ) {
    this.data.agb = true;
    this.data.security = true;

    if ($state.is('root.shop.checkout')) {
      $state.go('.payment');
    }

    this.showInvoice = config.get('showInvoice');
  }

  showFailedPayment() {
    return this.$stateParams['code'] === 'paymentFailure' && this.showError;
  }

  submit() {
    this.shopBasketService.submitPayment(this.data, () => {
      this.$state.go('root.shop.checkout.summary');
    });
  }

  moveToPreviousStep() {
    this.$state.go('root.shop.checkout.shipping');
  }

  paymentChanged() {
    this.showError = false;
  }
}
