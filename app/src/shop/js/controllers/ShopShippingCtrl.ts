import ShopBasketService from '../service/ShopBasketService';
import { StateService } from 'angular-ui-router';

export default class ShopShippingCtrl {
  data = null;
  countries = null;
  emailShown: boolean = false;
  changeAddress: boolean = false;
  changeBillingAddress: boolean = false;
  bothWereTrue: boolean = false;
  temporaryBillingAddressData: any = {};
  temporaryAddressData: any = {};
  tempDataForCheckIfChanged: any = {};

  constructor(
    private $state: StateService,
    private shopBasketService: ShopBasketService,
    private basketData,
    private goHome
  ) {
    this.$state = $state;
    this.shopBasketService = shopBasketService;
    this.data = basketData;
    this.goHome = goHome;
    this.temporaryAddressData = Object.assign({}, this.data.customer);
    this.tempDataForCheckIfChanged = Object.assign({}, this.temporaryAddressData);

    if ($state.is('root.shop.checkout')) {
      $state.go('.shipping');
    }
  }

  cancel() {
    this.goHome();
  }

  showEmail() {
    this.emailShown = !this.emailShown;
  }

  acceptBillingAddressChange() {
    this.overwriteBillingAddress();
    this.data.customer.changeBillingAddress = true;
    this.changeBillingAddress = false;

    this.shopBasketService.submitShipping(this.data, () => {
      this.$state.go('root.shop.checkout.payment', { id: this.basketData.id });
    });
  }

  overwriteBillingAddress() {
    this.data.customer.billAddressName1 = this.temporaryBillingAddressData.billAddressName1;
    this.data.customer.billAddressName2 = this.temporaryBillingAddressData.billAddressName2;
    this.data.customer.billStreet = this.temporaryBillingAddressData.billStreet;
    this.data.customer.billStreetNumber = this.temporaryBillingAddressData.billStreetNumber;
    this.data.customer.billFloorNumber = this.temporaryBillingAddressData.billFloorNumber;
    this.data.customer.billDoorNumber = this.temporaryBillingAddressData.billDoorNumber;
    this.data.customer.billZip = this.temporaryBillingAddressData.billZip;
    this.data.customer.billCity = this.temporaryBillingAddressData.billCity;
  }

  overwriteAddress() {
    this.data.customer.street = this.temporaryAddressData.street;
    this.data.customer.streetNumber = this.temporaryAddressData.streetNumber;
    this.data.customer.floorNumber = this.temporaryAddressData.floorNumber;
    this.data.customer.doorNumber = this.temporaryAddressData.doorNumber;
    this.data.customer.zip = this.temporaryAddressData.zip;
    this.data.customer.city = this.temporaryAddressData.city;
  }

  acceptAddressChange() {
    this.overwriteAddress();
    this.data.customer.changeShippingAddress = true;
    this.changeAddress = false;

    if (this.bothWereTrue) {
      this.changeBillingAddress = true;
    } else {
      this.shopBasketService.submitShipping(this.data, () => {
        this.$state.go('root.shop.checkout.payment', { id: this.basketData.id });
      });
    }
  }

  notAcceptAddressChange() {
    this.overwriteAddress();
    this.data.customer.changeShippingAddress = false;

    if (!this.bothWereTrue) {
      this.shopBasketService.submitShipping(this.data, () => {
        this.$state.go('root.shop.checkout.payment', { id: this.basketData.id });
      });
    } else {
      this.changeAddress = false;
      this.changeBillingAddress = true;
    }
  }

  notAcceptBillingAddressChange() {
    this.overwriteBillingAddress();
    this.data.customer.changeBillingAddress = false;

    this.shopBasketService.submitShipping(this.data, () => {
      this.$state.go('root.shop.checkout.payment', { id: this.basketData.id });
    });
  }

  submit() {
    if (!_.isEqual(this.tempDataForCheckIfChanged, this.temporaryAddressData)) {
      this.changeAddress = true;
    }
    if (this.basketData.customer.billOn == 1) {
      this.changeBillingAddress = true;
    }
    if (!this.changeBillingAddress && !this.changeAddress) {
      this.shopBasketService.submitShipping(this.data, () => {
        this.$state.go('root.shop.checkout.payment', { id: this.basketData.id });
      });
    }
    // if changeBillingAddress and changeAddress were both true, than the partial for accepting the
    // address change has to be visible before the partial for accepting the "Rechnungadresse" change
    if (this.changeBillingAddress && this.changeAddress) {
      this.changeBillingAddress = false;
      this.bothWereTrue = true;
    }
  }
}
