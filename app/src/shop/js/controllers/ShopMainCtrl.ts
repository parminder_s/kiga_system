export default class ShopMainCtrl {
  constructor($state) {
    (<any>require)(['../../css/shop.less']);
    if ($state.is('root.shop')) {
      $state.go('.cart');
    }
  }
}
