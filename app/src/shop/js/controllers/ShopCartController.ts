import { StateService } from 'angular-ui-router';
import { ILinksService } from '../../../misc/js/services/AngularInterfaces';
import ShopBasketService, { BasketResponse } from '../service/ShopBasketService';

export default class ShopCartController {
  private context: string;
  private data = null;
  private shopLink: string = null;
  private voucherCode = '';
  private showVoucher = false;
  private showVoucherError = false;

  constructor(
    private $state: StateService,
    private shopBasketService: ShopBasketService,
    basketData,
    private amountList,
    private canDeliver: boolean,
    context,
    private $window,
    private basketItemsService,
    links: ILinksService,
    private goHome
  ) {
    this.context = context;
    this.data = basketData;
    this.shopLink = links.findByCode('shop').url;
    if (this.data != null) {
      this.basketItemsService.setNumberOfItems(this.calcProductItemsTotal(this.data.products));
      this.setVoucherVisibility();
    }
    if ($state.is('root.shop.checkout')) {
      $state.go('.cart');
    }
  }

  submit() {
    if (this.voucherCode === '') {
      if (this.context === 'checkout') {
        this.shopBasketService.submitCart(this.data, () => {
          this.$state.go('root.shop.checkout.shipping', { id: this.data.id });
        });
      } else {
        this.$state.go('root.shop.checkout.shipping', { id: this.data.id });
      }
    } else {
      this.handleVoucherApplication();
    }
  }

  private setVoucherVisibility() {
    this.showVoucher = this.data && this.data.products.length > 0;
  }

  private handleVoucherApplication() {
    this.shopBasketService.applyVoucher(this.voucherCode).then(applyVoucherResponse => {
      this.voucherCode = '';
      if (applyVoucherResponse.successful) {
        this.data = applyVoucherResponse.basket;
        this.showVoucherError = false;
      } else {
        this.showVoucherError = true;
      }
    });
  }

  goBack() {
    if (this.context === 'checkout') {
      this.goHome();
    } else {
      this.$window.history.back();
    }
  }

  setAmount(product) {
    this.shopBasketService.setAmount(product, data => {
      this.basketItemsService.setNumberOfItems(this.calcProductItemsTotal(data.products));
      this.data = data;
    });
  }

  calcProductItemsTotal(productItems: Array<any>) {
    return _(productItems)
      .map(function(productItem: any) {
        return productItem.amount;
      })
      .sum()
      .valueOf();
  }

  removeItem(itemId, productId) {
    this.shopBasketService.removeItem(itemId, productId, data => {
      this.data = data;
      this.basketItemsService.setNumberOfItems(this.calcProductItemsTotal(this.data.products));
      if (this.basketItemsService.getNumberOfItems() === 0) {
        this.canDeliver = true;
      }
      this.setVoucherVisibility();
    });
  }

  showNoItems() {
    if (!this.data) {
      return true;
    }
    if (this.data.products.length === 0) {
      return true;
    }
    return false;
  }
}
