import { StateService } from 'angular-ui-router';
import ShopBasketService from '../service/ShopBasketService';
export default class SummaryCtrl {
  data: any;

  constructor(
    private $state: StateService,
    private shopBasketService: ShopBasketService,
    basketData
  ) {
    if (this.$state.is('root.shop.checkout')) {
      this.$state.go('root.shop.checkout.summary');
    }
    this.data = basketData;
  }

  multiply(amount, price) {
    let prod = amount * parseFloat(price);
    return prod.toFixed(2);
  }

  goTo(step) {
    this.$state.go('root.shop.checkout.' + step, { id: this.data.id });
  }

  goToPrevious() {
    this.$state.go('root.shop.checkout.payment', { id: this.data.id });
  }

  finish() {
    return this.shopBasketService.submit(this.data).then(response => {
      if (!response.redirect) {
        this.$state.go('root.shop.finished', {}, { reload: true });
      }
    });
  }
}
