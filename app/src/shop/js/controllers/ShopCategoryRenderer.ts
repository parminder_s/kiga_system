import * as _ from 'lodash';
import { AreaType } from '../../../area/js/AreaType';
import { Renderer } from '../../../cms/js/renderers/Renderer';
import ViewModel from '../../../cms/js/ViewModel';
import { ILinksService } from '../../../misc/js/services/AngularInterfaces';

export interface ShopCategoryViewModelCategory {}

export interface ShopCategoryViewModel extends ViewModel {
  subcategories: Array<ShopCategoryViewModelCategory>;
}

/**
 * Created by asv on 29.12.16.
 */
export class ShopCategoryRenderer implements Renderer {
  public rootBreadcrumbTitle: string = 'SHOP_HOMEPAGE_TITLE';
  private products: Array<any>;
  public breadcrumbsCssClass = 'shopColor';

  public viewModel: ShopCategoryViewModel;
  public partial: string = 'shop/partials/category.html';
  private sliderElements = [];

  constructor(
    private $document,
    private $timeout,
    private $state,
    private $window,
    private endpoint,
    private links: ILinksService,
    private member
  ) {}

  initialize(viewModel: ViewModel) {
    (<any>require)(['../../css/shop.less']);
    this.viewModel = viewModel as ShopCategoryViewModel;

    this.$state.params['area'] = AreaType.SHOP;
    this.endpoint('/api/shop/slider', resp => {
      this.sliderElements = resp;
    });

    this.products = this.mergeProducts(this.viewModel.subcategories);
  }

  /**
   * moves all products into the first category.
   */
  mergeProducts(subcategories: Array<any>) {
    return _(subcategories)
      .map<Array<any>>('products')
      .flatten()
      .value();
  }

  showEditButton() {
    return this.member.isCms;
  }

  onBreadcrumbClick(index) {}

  onTopCategoryClick() {}
}
