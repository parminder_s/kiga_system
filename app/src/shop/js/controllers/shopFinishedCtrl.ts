export class ShopFinishedCtrl {
  goHome: () => void;

  constructor(goHome) {
    this.goHome = goHome;
  }
  submit() {
    this.goHome();
  }
}
