import { ResolvedType } from '../../cms/js/ResolvedType';

export class ShopResolvedTypesForCms {
  public static ShopMainContainer: ResolvedType = 'ShopMainContainer';
  public static ShopCategory: ResolvedType = 'ShopCategory';
  public static ShopArticlePage: ResolvedType = 'ShopArticlePage';
}
