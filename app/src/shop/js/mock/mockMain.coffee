define ['../app', 'jquery'], (app, $) ->
  app.app.factory 'shopMock', ['misc.mockHelper'].concat (mock) ->

    countryList =[
      {code: "at" , title: 'Austria'},
      {code: "bd" , title: 'Bangladesh'},
      {code: "ch" , title: 'Switzerland'},
      {code: "cy" , title: 'Zypern'},
      {code: "de" , title: 'Germany'},
      {code: "dk" , title: 'Denmark'},
      {code: "fr" , title: 'France'},
      {code: "it" , title: 'Italiy'},
      {code: "jp" , title: '日本国'},
      {code: "no" , title: 'Norway'},
      {code: "ru" , title: 'Russia'},
      {code: "se" , title: 'Sweden'},
      {code: "tr" , title: 'Turkey'},
      {code: "ua" , title: 'Ukraine'},
      {code: "uk" , title: 'United Kingdom'},
      {code: "us" , title: 'USA'}
    ]
    shippingCost = {
      at:0.0
      bd:100.0
      ch:15.0
      cy:17.0
      de:19.0
      dk:19.5
      fr:17.5
      it:18.0
      jp:11.5
      no:15.5
      se:15.75
      ru:14.20
      tr:13.5
      ua:17.0
      uk:11.0
      us:115.15
    }

    checkoutData = null
    resetData = ->
      checkoutData = angular.copy checkoutDataRaw
      checkoutData

    updateCheckoutData = (checkoutDataUpdated)->
      checkoutData = checkoutDataUpdated
      checkoutData

    getCheckoutData = ->
      if not checkoutData
        resetData()
      else checkoutData

    basketData =
      id: 333
      customer:
        id: 1
        firstname: "Christian Johannes"
        lastname: "Tomaschitz"
        email: "chj.tom@gmail.com"
        gender: "female"
        addressName1: "Rathaus"
        addressName2: "in Eisenstadt"
        street: "Hauptstrasse"
        streetNumber: "100"
        floorNumber: "2"
        doorNumber: "6"
        zip: "7000"
        city: "Eisenstadt"
        billOn: true
        billCountry: "at"
        billAddressName1: "Kindergarten"
        billAddressName2: "Rust"
        billStreet: "Dorfmeistergasse"
        billStreetNumber: "12"
        billFloorNumber: "1"
        billDoorNumber: "3"
        billZip: "7071"
        billCity: "Rust"
        country: "at"
      products: [
        {
          id: 100
          title: "Osterhase"
          amount: 1
          price: 2.50
          sum: 2.50
        }, {
          id: 101
          title: "Weihnachtsmann"
          amount: 1
          price: 2.50
          sum: 2.50
        }, {
          id: 102
          title: "Lapbook Uhu"
          amount: 1
          price: 25.0
          sum: 25.0
        }]
      shipping:
        to: "Austria"
        cost: 12.50
      totalWithoutDelivery: 5.00
      total: 17.50
      currency: "EUR"
      paymentMethod: 'RE'

    getCountryByCode = (code) ->
      returner = _.find countryList, (country)->
        country.code is code
      returner

    getCountryByName = (name) ->
      returner = _.find countryList, (country)->
        country.title is name
      returner

    getShippingCostByCode = (code) ->
      shippingCost[code]

    mock 'shop/basket/find', -> basketData

    mock "shop/checkout/resume", -> basketData

    mock "shop/checkout/getData", (data, url) ->
      returner = getCheckoutData()

    mock "misc/getCountryList", (data, url) ->
      returner =
        countryList: countryList

    mock 'shop/checkout/getPaymentMethods', ->
      payment: ['MP-CC', 'MP-PAYPAL', 'SOFORT', 'RE']
      invoice: ['email-customer', 'post-customer']

    mock 'shop/checkout/submitCart', -> basketData

    mock 'shop/checkout/submitShipping', -> basketData

    mock "shop/checkout/submitPayment", -> basketData

    mock "shop/checkout/deleteItem", -> basketData

    mock "shop/checkout/setAmount", -> basketData

    mock "shop/checkout/submit", -> basketData

    mock "shop/payment/submit", -> basketData

    mock "shop/summary/submit", (data, url) ->
      returner = data


  app.run ['shopMock'].concat ->
