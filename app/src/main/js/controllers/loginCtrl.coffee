define ['../app', 'angular'], (app, angular) ->
  app.app.controller 'main.loginCtrl', ($state, $stateParams, request, member,
    goHome, redirectorService, $window, resolverService, backUrl, memberLoader, goBack) -> new class Ctrl
    isVerified: true
    isUsernamePasswordError: false
    shopCheckout: $stateParams.shop

    resend: ->
      request {
        url: 'activation/resend'
        data:
          user: @loginData.email
      }, -> $state.go 'root.member.activation.sent'
    loginData:
      email: '', password: ''
      remember: true, forward: decodeURIComponent $stateParams.forward
    login: =>
      request {
        url: 'security/login'
        data: @loginData
      }, (returner) =>
        @isUsernamePasswordError = false
        @isVerified = true

        if returner.status is 'not_verified'
          @isVerified = false
        else if returner.status is 'username_password_error'
          @isUsernamePasswordError = true
        else
          memberLoader.reset().then (data) ->
            resolverService.invalidateCache()
            goHome(true)

    back: -> goBack.goBack()
