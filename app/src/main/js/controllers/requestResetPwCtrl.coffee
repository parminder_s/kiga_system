define ['../app', 'angular'], (app, angular) ->
  app.app.controller 'main.requestResetPwCtrl', ($scope, $state,
  request, infoDlg,formHelper,goHome) ->
    angular.extend $scope,
      data: {}
      resetPw: ->
        request {
          url: 'security/resetPw'
          data:
            email: $scope.data.email
        }, -> infoDlg 'LOST_PASSWORD_INFO', -> goHome()

      cancel: -> formHelper()
