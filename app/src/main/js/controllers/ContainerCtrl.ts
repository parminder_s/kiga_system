import Area from '../../../area/js/Area';
import AreaFetcher from '../../../area/js/AreaFetcher';
import { IConfigService, ILinksService } from '../../../misc/js/services/AngularInterfaces';
import MockedCountryService from '../../../misc/js/services/MockedCountryService';
import IMember from '../../../misc/permission/js/IMember';
import AnalyticsLoader from '../services/AnalyticsLoader';
import ILocationService = angular.ILocationService;

export default class ContainerCtrl {
  private area: Area;

  constructor(
    $scope,
    $window,
    request,
    endpoint,
    member: IMember,
    $state,
    $stateParams,
    $translate,
    translationModeHandler,
    errorMsg,
    links: ILinksService,
    config: IConfigService,
    analyticsLoader: AnalyticsLoader,
    basketItemsService,
    mockedCountryService: MockedCountryService,
    goHome,
    areaFetcher: AreaFetcher,
    $location: ILocationService
  ) {
    if (config.get('googleAnalyticsEnabled')) {
      analyticsLoader.loadScript();
    }
    this.area = areaFetcher.getArea();
    $scope.numberOfItems = basketItemsService.getNumberOfItems();
  }
}
