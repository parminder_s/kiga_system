export default class IdeasCacheJobCtrl {
  constructor(private confirmerDlg, private infoDlg, private endpoint, private goHome) {
    this.confirmerDlg(
      '<h3>W A R N I N G</h3>' +
        '<p>Actualizing the IdeasCache can damage the system seriously.</p>' +
        '<p>Do you want to continue?</p>',
      () => {
        this.endpoint('ideasTask/runJob').then(() => {
          this.infoDlg('IdeasCacheActualizer has been started successfully', () => {
            this.goHome();
          });
        });
      },
      { useTranslation: false }
    );
  }
}
