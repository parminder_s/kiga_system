import MemberLoader from '../../../misc/permission/js/MemberLoader';
import { StateService } from 'angular-ui-router';

/**
 * development menu for switch between pre-defined user accounts.
 */
export default class DevMenuCtrl {
  menuData = {
    label: 'Dev',
    menuItems: []
  };

  menuItemsMockedUser = [
    {
      label: 'Login as',
      newType: true,
      children: [
        {
          label: 'John Harrison',
          newType: true,
          callback: () => {
            this.loginUser('john');
          }
        },
        {
          label: 'Homer Simpson',
          newType: true,
          callback: () => {
            this.loginUser('homer');
          }
        },
        {
          label: 'Lucy Sanders (Admin)',
          newType: true,
          callback: () => {
            this.loginUser('lucy');
          }
        },
        {
          label: 'No User (Logout)',
          newType: true,
          callback: () => {
            this.loginUser('none');
          }
        }
      ]
    }
  ];

  menuItemsQuickRegister = [
    {
      label: 'CMS Admin',
      newType: true,
      callback: () => {
        this.handleQuickRegister('cmsAdmin');
      }
    },
    {
      label: 'Standard Subscription',
      newType: true,
      callback: () => {
        this.handleQuickRegister('standard');
      }
    },
    {
      label: 'Ended Standard Subscription',
      newType: true,
      callback: () => {
        this.handleQuickRegister('endedStandard');
      }
    },
    {
      label: 'Test Subscription',
      newType: true,
      callback: () => {
        this.handleQuickRegister('test');
      }
    },
    {
      label: 'Ended Test Subscription',
      newType: true,
      callback: () => {
        this.handleQuickRegister('endedTest');
      }
    },
    {
      label: 'Community Access',
      newType: true,
      callback: () => {
        this.handleQuickRegister('community');
      }
    }
  ];

  constructor(
    private endpoint,
    private $state: StateService,
    private memberLoader: MemberLoader,
    private config
  ) {
    if (this.config.get('showQuickRegisters')) {
      this.menuData.menuItems = this.menuItemsQuickRegister;
    } else {
      this.menuData.menuItems = this.menuItemsMockedUser;
    }
  }

  show() {
    return window['kiga']['isDev'] || this.config.get('showQuickRegisters');
  }

  loginUser(user: string) {
    this.endpoint('test/loginUser/' + user).then(() => this.reloadMember());
  }

  handleQuickRegister(code: string) {
    let url = '/test/quickRegister?';
    let permissionMode = this.config.getPermissionMode();

    if (code === 'standard') {
      url += '?permission=' + permissionMode;
    } else if (code === 'test') {
      url += '?product=test&permission=' + permissionMode;
    } else if (code === 'cmsAdmin') {
      url = '/test/CmsLoginJson?permission=' + permissionMode;
    } else if (code === 'endedTest') {
      url += '?product=test&isCancelled=true&permission=' + permissionMode;
    } else if (code === 'endedStandard') {
      url += '?isCancelled=true&permission=' + permissionMode;
    } else if (code === 'community') {
      url += '?product=community&permission=' + permissionMode;
    }

    this.endpoint({ method: 'GET', url: url, rawUrl: true }).then(() => this.reloadMember());
  }

  reloadMember() {
    this.memberLoader.reset().then(member => this.$state.reload());
  }
}
