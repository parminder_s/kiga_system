###*
  @ngdoc controller
  @name containerCtrl
  @description
  The main controller which main task is to manage the current locale
  and generate the header. (footer now is own component).
###

define ['../app', 'angular', '../../../url', 'lodash'], (app, angular, urlFn, _) ->
  app.app.controller 'containerCtrl',
  ($scope, $window, request, endpoint, member, $state, $stateParams, $translate,
  translationModeHandler, errorMsg, links, config, analyticsLoader, basketItemsService,
    mockedCountryService, goHome, areaFetcher, $location, ngxNavigate) ->
    analyticsLoader.loadScript() if config.get 'googleAnalyticsEnabled'
    filterData = $location.search()
    $scope.tags = []
    query = filterData.query
    if query
      if not angular.isArray(query)
        query = [ query ]
      query.forEach (item) -> $scope.tags.push {text: item}

    angular.extend $scope,
      showTranslationMode: -> member.isCms
      isTranslationEnabled: false
      area: -> areaFetcher.getArea()
      areaLink: ->
        $state.href @area().state
      showFullBasket:-> $scope.numberOfItems() > 0
      numberOfItems: ->
        basketItemsService.getNumberOfItems()
      addToSelectedTags: (index) ->
      changeAge: (ageGroup) ->
        @area().withAgeGroup(ageGroup)
        @triggerAreaCallback()
      triggerAreaCallback: -> @area().invokeCallback()
      memberData: member.getAll()
      isLoggedIn: -> not member.isAnonymous
      isAllowedMember: -> !member.hasExpiredSubscription() and member.hasStandardSubscription()
      year: (new Date()).getFullYear()
      showRepositoryContext: ->
        member.isCms
      currentRepositoryContext: config.get "currentRepositoryContext"
      repositoryContexts: config.get "repositoryContexts"
      toggleRepositoryContext: ->
        endpoint("content/repository-context/toggle").then ->
          $window.location.reload()
      locale: $stateParams.locale
      location: -> $state.href '.'
      locationIsLogin: -> location.href.match('login')
      locales: _.map config.get('locales'), (locale) ->
        code: locale, title: "MISC_LOCALE_#{locale.toUpperCase()}"
      mockedCountry: mockedCountryService.getMockedCountry()
      switchLocale: (locale) ->
        $state.go '.', {locale: locale}, {reload: true}
      showQuickLogin: kiga.isDev
      showVersion: config.get 'showVersion'
      showBasket: -> links.findByCode('shop').url != '/'
      logErrors: -> config.get 'jsErrorLogEnabled'
      goHome: -> goHome()
      goToNewMessage: ->
        $state.go 'root.member.msgs'
      showMemberIdeas: -> config.get 'showMemberIdeas'
      toggleTranslation: (isEnable)->
        if isEnable is true
          $scope.isTranslationEnabled = translationModeHandler.enable()
        else
          $scope.isTranslationEnabled = translationModeHandler.disable()
        $translate.refresh()
      state: $state.current
      links: links.getTypes()
      forumLink: links.findByCode('forum').url
      shopLink: links.findByCode('shop').url
      ideasLink: links.findByCode('ideaGroupMainContainer').url
      member: member
      url: urlFn.default
      errorMsg: ""
      searchInput :  ''
      angularCmsEnabled: config.isFeatureEnabled("angularCmsEnabled")
      goToNg: (url) -> ngxNavigate.navigateByUrl(url)
      hideSubMenu: false


    errorMsg.initialize $scope
    if member.showNotVerified
      errorMsg.setMsg "LOGIN_NOT_ACTIVATED"

    require(["../../css/header.less"]);
