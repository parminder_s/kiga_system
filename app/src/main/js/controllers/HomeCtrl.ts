import url from '../../../url';
import IMember from '../../../misc/permission/js/IMember';
import { StateService } from 'angular-ui-router';

export default class HomeCtrl {
  activityForum = null;
  elementsCategory = null;
  elementsSlider = null;
  elementsThema = null;
  ideaForum = null;
  ideaIndex = null;
  newIdeas = null;
  newIdeasUrl: string = null;
  $scope = null;
  member = null;
  $stateParams = null;
  price = null;
  currency = null;
  constructor(
    $scope,
    homeData,
    private $state: StateService,
    $stateParams,
    private locale,
    member: IMember,
    request,
    private $filter
  ) {
    this.elementsSlider = homeData.slider;
    this.elementsThema = homeData.thema;
    this.newIdeas = homeData.newIdeas;
    this.ideaForum = homeData.ideaForum;
    this.newIdeasUrl = homeData.newIdeasUrl;
    this.activityForum = homeData.activityForum;
    this.ideaIndex = 0;
    this.$scope = $scope;
    this.member = member;
    this.$stateParams = $stateParams;
    this.elementsCategory = [];

    if (this.member.productGroup === 'test') {
      request('nregistration/cheapestPrice').then(({ price, currency }) => {
        this.price = $filter('number')(price, 2);
        this.currency = currency;
      });
    }
    (<any>require)(['../../css/home.less']);
  }

  emitEventItemIndexChanged(direction) {
    this.$scope.$broadcast('itemIndexChanged', {
      downloadIndex: this.$scope.ideaIndex,
      direction: direction
    });
  }

  prevItem(categoryIndex) {
    this.ideaIndex -= 1;
    this.emitEventItemIndexChanged('prev');
  }

  nextItem(categoryIndex) {
    this.ideaIndex += 1;
    this.emitEventItemIndexChanged('next');
  }

  onMoreButtonClick() {
    window.location.href = this.newIdeasUrl;
  }
  onButtonEditClick() {
    this.$state.go('root.backend.editHome');
  }
  showEditButton() {
    return this.member.isCms;
  }
  showQuickRegLink() {
    return !this.member.hasFullSubscription() && !this.member.hasTestSubscription();
  }
  showFullRegLink() {
    return this.member.hasTestSubscription();
  }
  isFullSubscription() {
    return this.member.hasFullSubscription();
  }
  url(part: string) {
    return url(part);
  }

  getDirectIdeaUrl(urlSegment: string) {
    return this.newIdeasUrl + '?preview=' + urlSegment;
  }

  showForum() {
    return true;
  }
}
