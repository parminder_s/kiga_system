export default class NewsletterSubscribeCtrl {
  private email;
  private isSubscribed: boolean = false;

  constructor(private endpoint, private $window) {}

  submit() {
    this.endpoint({
      method: 'POST',
      url: 'newsletter/subscription',
      data: { email: this.email }
    }).then(resp => {
      if (resp.actionOk) {
        this.isSubscribed = resp.actionOk;
      }
    });
  }

  cancel() {
    return this.$window.history.back();
  }
}
