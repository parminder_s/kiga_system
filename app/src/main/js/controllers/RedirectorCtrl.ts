import { INgxNavigateService } from '../../../misc/js/services/AngularInterfaces';
import RedirectorService from '../../../misc/js/services/RedirectorService';

/**
 * used by loginCtrl, forwards to a given state or url and makes sures that the container's
 * bootstrap methods (locales, member, ...) are fully reloaded by not being a child of state root.
 */

export default class RedirectorCtrl {
  constructor(
    $stateParams,
    redirectorService: RedirectorService,
    ngxNavigate: INgxNavigateService
  ) {
    if (redirectorService.toBeRedirected() && $stateParams['forward']) {
      const decodedUrl = decodeURIComponent($stateParams.forward);
      if (decodedUrl.match(/^\/ng6/)) {
        ngxNavigate.navigateByUrl(decodeURIComponent($stateParams.forward));
      } else {
        window.location.href = decodedUrl;
      }
    } else {
      ngxNavigate.navigateByUrl('/');
    }
  }
}
