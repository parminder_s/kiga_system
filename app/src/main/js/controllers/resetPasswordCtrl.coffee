define ['../app'], (app) ->
  app.app.controller 'resetPasswordCtrl', ($scope, $location,
  $stateParams, request, $state) -> new class Ctrl
    data :{newPassword : '',newPasswordConfirm : ''}
    resetPassword: ->
      request {
        url: 'security/resetPassword'
        data:
          newPassword : @data.newPassword
          t: $stateParams.token
      }, (returner) =>
        $state.go 'root.login'
