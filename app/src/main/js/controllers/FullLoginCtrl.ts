/**
 * Created by peter on 21.12.16.
 */

import { IGoBack } from '../../../misc/js/services/AngularInterfaces';
import IMemberService from '../../../misc/permission/js/IMemberService';
import { StateService } from 'angular-ui-router';

export default class FullLoginCtrl {
  private isUsernamePasswordError;
  private password;
  private email;
  constructor(
    private $state: StateService,
    private memberLoader,
    private member: IMemberService,
    private request,
    private formHelper,
    private $stateParams,
    private $window,
    private redirectorService,
    private goBack: IGoBack
  ) {
    this.isUsernamePasswordError = false;
    this.email = member.get().email;
  }

  login() {
    this.request(
      {
        url: 'security/login',
        data: { password: this.password, email: this.email }
      },
      resp => {
        this.isUsernamePasswordError = false;
        if (resp.status === 'username_password_error') {
          this.isUsernamePasswordError = true;
        } else {
          this.memberLoader.reset().then(() => {
            this.redirectorService.redirectTo(this.$stateParams.forward);
          });
        }
      }
    );
  }

  cancel() {
    this.goBack.goBack();
  }
}
