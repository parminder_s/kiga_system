define [], ->
  createMember = (userType, groupType, isLicenceAdmin, productGroup, customerType, subscriptionExpired, isLoggedIn) ->
    result =
      email: "mock.#{userType}#{groupType}@kigaportal.com"
      isCms: false
      subscription: {type: userType, expired: false}
    result.productGroup = productGroup if productGroup?
    result.subscription.group = groupType if groupType?
    result.subscription.customerType = customerType
    result.subscription.expired = subscriptionExpired if subscriptionExpired?
    result.subscription.isLicenceAdmin = isLicenceAdmin if isLicenceAdmin?
    result

  communityMember: createMember 'community'
#  testMember: createMember 'test'
  testMember: createMember 'test', 'test', false, 'test', 'test', false
  testLoginSubscription: createMember 'testLoginSubscription', 'test', false, 'test', 'test', false
  testLoginExpiredSubscription: createMember 'testLoginExpiredSubscription', 'test', false, 'test', 'test', true
  standardMember: createMember 'standard', 'standard', false, undefined, undefined, false
  standardStudentMember: createMember 'standard', 'standard', false, undefined, 'student'
  standardOrgAdmin: createMember 'standard', 'standard-org', true, 'standard-org'
  standardOrgMember: createMember 'standard', 'standard-org'
  standardLicenceAdmin: createMember 'standard', 'licence', true

