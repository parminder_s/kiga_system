import { StateParams } from 'angular-ui-router';
import * as moment from 'moment';
import { AreaType } from '../../area/js/AreaType';
import { DunningManagementService } from '../../backend/accounting/dunning/js/service/DunningManagementService';
import { OverdueInvoiceService } from '../../backend/accounting/dunning/js/service/OverdueInvoiceService';
import { PaymentTransactionService } from '../../backend/accounting/transactions/js/service/PaymentTransactionService';
import { KigaIdeaModel } from '../../backend/cmsBackend/js/services/KigaIdeaModel';
import { SliderFilesModel } from '../../backend/cmsBackend/js/services/SliderFilesModel';
import { Voucher } from '../../backend/shop/js/controllers/VoucherFormController';
import { CountryModel } from '../../backend/shop/js/services/CountryModel';
import { ProductFormModel } from '../../backend/shop/js/services/ProductFormModel';
import ShopBackendModel from '../../backend/shop/js/services/ShopBackendModel';
import { VoucherFormModel } from '../../backend/shop/js/services/VoucherFormModel';
import { ResolverRequest } from '../../cms/js/requests/ResolverRequest';
import {
  IBootstrapper,
  IConfigService,
  INgxNavigateService
} from '../../misc/js/services/AngularInterfaces';
import { ILocaleInfoService } from '../../misc/js/services/ILocaleInfoService';
import { empty } from '../../misc/js/values/templates';
import { SearchRequest } from '../../search/js/requests/searchRequest';

export default function createMainRouting(app: angular.IModule) {
  app.config(function (stateHelperProvider, $urlRouterProvider, $stateProvider) {
    stateHelperProvider.setNestedState({
      name: 'redirector',
      url: '/redirector?forward',
      controller: 'redirectorCtrl as ctrl',
      template: empty
    });

    stateHelperProvider.setNestedState({
      name: 'empty',
      url: '/empty',
      template: empty
    });

    stateHelperProvider.setNestedState({
      name: 'root',
      url: '/{locale}',
      templateUrl: 'main/partials/container.html',
      controller: 'containerCtrl',
      resolve: {
        bootstrap: (bootstrapper: IBootstrapper, locale, $translate, $stateParams: StateParams) => {
          const currentLocale = $stateParams.locale || 'en';
          return bootstrapper.bootstrap(currentLocale).then(data => {
            locale.set(currentLocale);
            $translate.use(currentLocale);
            return data;
          });
        }
      },
      children: [
        {
          name: 'home',
          url: '/home',
          template: empty,
          controller: (localeInfo: ILocaleInfoService, ngxNavigate: INgxNavigateService) =>
            ngxNavigate.navigateByUrl(`/ng6/${localeInfo.getLocale()}/home`)
        },
        {
          name: 'home-old',
          url: '/home-old',
          controller: 'homeCtrl as ctrl',
          templateUrl: 'main/partials/home.html',
          resolve: {
            homeData: endpoint => endpoint('homepage/getHome')
          }
        },
        {
          name: 'login',
          url: '/login?forward&shop',
          controller: 'main.loginCtrl as ctrl',
          templateUrl: 'main/partials/login.html',
          resolve: {
            backUrl: $state => $state.href('.')
          },
          params: {
            stateName: 'login'
          }
        },
        {
          name: 'requestResetPassword',
          url: '/request-reset-password',
          controller: 'main.requestResetPwCtrl',
          templateUrl: 'main/partials/requestResetPassword.html'
        },
        {
          name: 'resetPassword',
          url: '/reset-password/{token}',
          controller: 'resetPasswordCtrl as ctrl',
          templateUrl: 'main/partials/resetPassword.html'
        },
        {
          name: 'fullLogin',
          url: '/full-login?forward', // TODO remove ?forward if there are no forwards to legacy
          // site left(member admin)
          params: { forward: 'root.home' }, // default value for forward if nothing set is home.
          controller: 'fullLoginCtrl as ctrl',
          templateUrl: 'main/partials/fullLogin.html'
        },
        {
          name: 'parallelLogInfo',
          url: '/parallelLogInfo/:entryId',
          resolve: {
            data: function (request, $stateParams) {
              return request({
                url: 'main/parallelLog',
                data: {
                  entryId: $stateParams.entryId
                }
              });
            }
          },
          controller: 'parallelLogInfoCtrl as ctrl',
          templateUrl: 'main/partials/parallelLogInfo.html'
        },
        {
          name: 'ideasCacheJob',
          url: '/ideasCacheJob',
          controller: 'ideasCacheJobCtrl as ctrl',
          template: empty
        },
        {
          name: 'newsletterSubscribe',
          url: '/newsletter-subscribe',
          controller: 'newsletterSubscribeCtrl  as ctrl',
          templateUrl: 'main/partials/newsletter.html'
        },
        {
          name: 'angularShow',
          url: '/angular-show',
          template: '<test-downgrade></test-downgrade>'
        }
      ]
    });

    stateHelperProvider.setNestedState({
      name: 'root.ng6',
      url: '/ng6',
      template: empty,
      children: [
        {
          name: 'backend',
          url: '/backend',
          controller: 'backend.mainCtrl as backendCtrl',
          templateUrl: 'backend/partials/main.html',
          resolve: {
            lazyLoad: ($ocLazyLoad, $$animateJs, $q) => {
              let deferred = $q.defer();
              // It is webpack's require here, not requirejs
              (<any>require).ensure(
                ['../../backend/js/main'],
                function () {
                  let module = require('../../backend/js/main');
                  $ocLazyLoad.load({
                    name: 'backend'
                  });
                  deferred.resolve(module);
                },
                'backend'
              );
              return deferred.promise;
            }
          },
          children: [{
            name: 'shop',
            url: '/shop',
            template: '<div ui-view class="shopBackend"></div>',
            params: {
              area: AreaType.SHOP
            },
            children: [
              {
                name: 'purchases',
                url: '/purchases',
                controller: 'shopBackendList as ctrl',
                templateUrl: 'backend/table/partials/purchases.html',
                resolve: {
                  purchases: function (shopBackendModel: ShopBackendModel) {
                    return shopBackendModel.findFromOrdered(true);
                  }
                }
              },

            ]
          },
          ]
        }


      ]
    });

    // ------------------------------------------------------------------------------------------

    // stateHelperProvider.setNestedState({
    //   name: 'ng6',
    //   url: '/ng6',
    //   template: '<ui-view></ui-view>',
    //   children: [
    //     {
    //       name: 'locale',
    //       url: '/en',
    //       template: empty,
    //       // resolve: {
    //       //   bootstrap: (bootstrapper: IBootstrapper, locale, $translate, $stateParams: StateParams) => {
    //       //     const currentLocale = $stateParams.locale || 'en';
    //       //     return bootstrapper.bootstrap(currentLocale).then(data => {
    //       //       locale.set(currentLocale);
    //       //       $translate.use(currentLocale);
    //       //       return data;
    //       //     });
    //       //   }
    //       // },
    //       children: [
    //         {
    //           name: 'backend',
    //           url: '/backend',
    //           controller: 'backend.mainCtrl as backendCtrl',
    //           templateUrl: 'backend/partials/main.html',
    //           resolve: {
    //             lazyLoad: ($ocLazyLoad, $$animateJs, $q) => {
    //               let deferred = $q.defer();
    //               // It is webpack's require here, not requirejs
    //               (<any>require).ensure(
    //                 ['../../backend/js/main'],
    //                 function () {
    //                   let module = require('../../backend/js/main');
    //                   $ocLazyLoad.load({
    //                     name: 'backend'
    //                   });
    //                   deferred.resolve(module);
    //                 },
    //                 'backend'
    //               );
    //               return deferred.promise;
    //             }
    //           },
    //           children: [
    //             {
    //               name: 'table',
    //               url: '/table',
    //               template: '<div ui-view class="shopBackend"></div>',
    //               params: {
    //                 area: AreaType.SHOP
    //               },
    //               children: [
    //                 {
    //                   name: 'list',
    //                   url: '/list',
    //                   controller: 'shopBackendListCtrl as ctrl',
    //                   templateUrl: 'backend/table/partials/purchases.html',
    //                   resolve: {
    //                     purchases: function (shopBackendModel: ShopBackendModel) {
    //                       return shopBackendModel.findFromOrdered(true);
    //                     }
    //                   }
    //                 },

    //               ]
    //             },
    //           ]
    //         }
    //       ]
    //     },

    //   ]
    // });


    // --------------------------------------------------------------------------


    stateHelperProvider.setNestedState({
      name: 'root.backend',
      url: '/backend',
      controller: 'backend.mainCtrl as backendCtrl',
      templateUrl: 'backend/partials/main.html',
      resolve: {
        lazyLoad: ($ocLazyLoad, $$animateJs, $q) => {
          let deferred = $q.defer();
          // It is webpack's require here, not requirejs
          (<any>require).ensure(
            ['../../backend/js/main'],
            function () {
              let module = require('../../backend/js/main');
              $ocLazyLoad.load({
                name: 'backend'
              });
              deferred.resolve(module);
            },
            'backend'
          );
          return deferred.promise;
        }
      },
      children: [
        {
          name: 'accounting',
          url: '/accounting',
          template: '<div ui-view class="accounting"></div>',
          children: [
            {
              name: 'accountingImport',
              url: '/import',
              controller: 'accountingImportCtrl as ctrl',
              templateUrl: 'backend/accounting/partials/accountingTransactionImport.html'
            },
            {
              name: 'accountingTransactions',
              url: '/transactions',
              controller: 'accountingTransactionsCtrl as ctrl',
              templateUrl: 'backend/accounting/partials/accountingTransactions.html'
            },
            {
              name: 'dunning',
              url: '/dunning-management',
              template: '<div ui-view class="dunning-management"></div>',
              params: {
                area: AreaType.SHOP
              },
              children: [
                {
                  name: 'levels',
                  url: '/levels',
                  controller: 'dunningLevelsManagementCtrl as ctrl',
                  templateUrl: 'backend/accounting/dunning/partials/levels/list.html',
                  resolve: {
                    levels: function (dunningManagementService: DunningManagementService) {
                      return dunningManagementService.getAllLevels();
                    }
                  }
                },
                {
                  name: 'add-level',
                  url: '/add-level',
                  controller: 'addDunningLevelsManagementCtrl as ctrl',
                  templateUrl: 'backend/accounting/dunning/partials/levels/form.html',
                  resolve: {
                    levels: function (dunningManagementService: DunningManagementService) {
                      return dunningManagementService.getAllLevels();
                    },
                    countries: function (dunningManagementService: DunningManagementService) {
                      return dunningManagementService.getAllCountries();
                    }
                  }
                },
                {
                  name: 'update-level',
                  url: '/update-level/{dunningLevelId}',
                  controller: 'updateDunningLevelsManagementCtrl as ctrl',
                  templateUrl: 'backend/accounting/dunning/partials/levels/form.html',
                  resolve: {
                    dunningLevel: function (
                      $stateParams: StateParams,
                      dunningManagementService: DunningManagementService
                    ) {
                      let dunningLevelId = $stateParams['dunningLevelId'];
                      return dunningManagementService.getLevel(dunningLevelId);
                    },
                    countries: function (dunningManagementService: DunningManagementService) {
                      return dunningManagementService.getAllCountries();
                    }
                  }
                },
                {
                  name: 'overdue',
                  url: '/overdue',
                  controller: 'overdueInvoicesCtrl as ctrl',
                  templateUrl: 'backend/accounting/dunning/partials/overdues.html',
                  resolve: {
                    overdueInvoices: function (overdueInvoiceService: OverdueInvoiceService) {
                      return overdueInvoiceService.getOverdueInvoices();
                    }
                  }
                }
              ]
            },
            {
              name: 'transactions',
              url: '/payment-transactions',
              template: '<div ui-view class="paymentTransactions"></div>',
              params: {
                area: AreaType.SHOP
              },
              children: [
                {
                  name: 'list',
                  url: '/list/{invoiceNumber}',
                  controller: 'paymentTransactionsCtrl as ctrl',
                  templateUrl: 'backend/accounting/transactions/partials/paymentTransactions.html',
                  resolve: {
                    payments: function (
                      paymentTransactionService: PaymentTransactionService,
                      $stateParams: StateParams
                    ) {
                      return paymentTransactionService.getAll($stateParams['invoiceNumber']);
                    },
                    invoiceNumber: function ($stateParams: StateParams) {
                      return $stateParams['invoiceNumber'];
                    }
                  },
                  params: { invoiceNumber: 'all' }
                },
                {
                  name: 'add',
                  url: '/add/{invoiceNumber}',
                  controller: 'addPaymentTransactionsCtrl as ctrl',
                  templateUrl: 'backend/accounting/transactions/partials/assignPayment.html',
                  resolve: {
                    paymentTransaction: function (
                      $stateParams: StateParams,
                      paymentTransactionService: PaymentTransactionService
                    ) {
                      let invoiceNumber = $stateParams['invoiceNumber'];
                      return paymentTransactionService.getModelByInvoice(invoiceNumber);
                    },
                    bank: function (
                      $stateParams: StateParams,
                      paymentTransactionService: PaymentTransactionService
                    ) {
                      let invoiceNumber = $stateParams['invoiceNumber'];
                      return paymentTransactionService.getBanksByInvoiceNumber(invoiceNumber);
                    },
                    banks: function (paymentTransactionService: PaymentTransactionService) {
                      return paymentTransactionService.getAllBanks();
                    }
                  }
                },
                {
                  name: 'edit',
                  url: '/edit/{paymentTransactionId}',
                  controller: 'editPaymentTransactionsCtrl as ctrl',
                  templateUrl: 'backend/accounting/transactions/partials/assignPayment.html',
                  resolve: {
                    paymentTransaction: function (
                      $stateParams: StateParams,
                      paymentTransactionService: PaymentTransactionService
                    ) {
                      let paymentTransactionId = $stateParams['paymentTransactionId'];
                      return paymentTransactionService.getOne(paymentTransactionId);
                    },
                    bank: function (
                      $stateParams: StateParams,
                      paymentTransactionService: PaymentTransactionService
                    ) {
                      let paymentTransactionId = $stateParams['paymentTransactionId'];
                      return paymentTransactionService.getBanksByPaymentTransactionId(
                        paymentTransactionId
                      );
                    },
                    banks: function (paymentTransactionService: PaymentTransactionService) {
                      return paymentTransactionService.getAllBanks();
                    }
                  }
                }
              ]
            }
          ]
        },
        {
          name: 'cmsBackend',
          url: '/cms',
          template: '<ui-view></ui-view>',
          children: [
            {
              name: 'sliderItems',
              url: '/slider-items/{productPageId}',
              controller: 'sliderItemsCtrl as ctrl',
              templateUrl: 'backend/cmsBackend/partials/main.html',
              resolve: {
                productData: function (
                  sliderFilesModel: SliderFilesModel,
                  $stateParams: StateParams
                ) {
                  return sliderFilesModel.getProductPageUrl($stateParams['productPageId']);
                },
                slider: function (sliderFilesModel: SliderFilesModel, $stateParams: StateParams) {
                  return sliderFilesModel.getSliderFiles($stateParams['productPageId']);
                }
              }
            },
            {
              name: 'kiga-idea',
              url: '/kiga-idea',
              template: '<ui-view></ui-view>',
              children: [
                {
                  name: 'list',
                  url: '/list',
                  controller: 'listKigaIdeaCtrl as ctrl',
                  templateUrl: 'backend/cmsBackend/partials/kiga-idea/list.html',
                  resolve: {
                    kigaIdeas: function (kigaIdeaModel: KigaIdeaModel, $stateParams: StateParams) {
                      return kigaIdeaModel.getAll($stateParams['locale']);
                    }
                  }
                },
                {
                  name: 'add',
                  url: '/add/{selectedLocale}',
                  controller: 'modifyKigaIdeaCtrl as ctrl',
                  templateUrl: 'backend/cmsBackend/partials/kiga-idea/modify.html',
                  resolve: {
                    formData: function () {
                      return null;
                    },
                    translationGroup: function () {
                      return [];
                    }
                  }
                },
                {
                  name: 'add-by-idea',
                  url: '/add/{selectedLocale}/clone-idea/{ideaId}',
                  controller: 'modifyKigaIdeaCtrl as ctrl',
                  templateUrl: 'backend/cmsBackend/partials/kiga-idea/modify.html',
                  params: {
                    sort: null,
                    sortMini: null,
                    ageGroups: null,
                    facebookShare: null,
                    forParents: null,
                    inPool: null
                  },
                  resolve: {
                    formData: function (kigaIdeaModel: KigaIdeaModel, $stateParams: StateParams) {
                      return kigaIdeaModel.prepareIdeaForTranslation(
                        $stateParams['ideaId'],
                        $stateParams['selectedLocale']
                      );
                    },
                    translationGroup: function (
                      kigaIdeaModel: KigaIdeaModel,
                      $stateParams: StateParams
                    ) {
                      return kigaIdeaModel.getTranslationGroupByIdeaId($stateParams['ideaId']);
                    }
                  }
                },
                {
                  name: 'edit',
                  url: '/edit/{ideaId}',
                  controller: 'modifyKigaIdeaCtrl as ctrl',
                  templateUrl: 'backend/cmsBackend/partials/kiga-idea/modify.html',
                  resolve: {
                    formData: function ($stateParams, kigaIdeaModel: KigaIdeaModel) {
                      let ideaId: number = $stateParams['ideaId'];
                      return kigaIdeaModel.getIdea(ideaId);
                    },
                    translationGroup: function (
                      kigaIdeaModel: KigaIdeaModel,
                      $stateParams: StateParams
                    ) {
                      return kigaIdeaModel.getTranslationGroupByIdeaId($stateParams['ideaId']);
                    }
                  }
                }
              ]
            }
          ]
        },
        {
          name: 'editHome',
          url: '/editHome',
          controller: 'homeEditCtrl as ctrl',
          templateUrl: 'backend/homePageAdmin/partials/homeEdit.html',
          children: [
            {
              name: 'new',
              url: '/new',
              controller: 'homeEditCtrl as ctrl',
              templateUrl: 'backend/homePageAdmin/partials/homeEdit.html'
            },
            {
              name: 'slider',
              url: '/slider/:id',
              controller: 'homeEditCtrl as ctrl',
              templateUrl: 'backend/homePageAdmin/partials/homeEdit.html'
            },
            {
              name: 'topic',
              url: '/topic/:id',
              controller: 'homeEditCtrl as ctrl',
              templateUrl: 'backend/homePageAdmin/partials/homeEdit.html'
            },
            {
              name: 'publish',
              url: '/publish',
              controller: 'homeEditCtrl as ctrl',
              templateUrl: 'backend/homePageAdmin/partials/homeEdit.html'
            }
          ]
        },
        {
          name: 'shopSliderEdit',
          url: '/editShopSlider',
          controller: 'shopSliderCtrl as ctrl',
          templateUrl: 'backend/homePageAdmin/partials/homeEdit.html',
          children: [
            {
              name: 'new',
              url: '/new',
              controller: 'shopSliderCtrl as ctrl',
              templateUrl: 'backend/homePageAdmin/partials/homeEdit.html'
            },
            {
              name: 'slider',
              url: '/slider?id',
              controller: 'shopSliderCtrl as ctrl',
              templateUrl: 'backend/homePageAdmin/partials/homeEdit.html'
            },
            {
              name: 'topic',
              url: '/topic/:id',
              controller: 'shopSliderCtrl as ctrl',
              templateUrl: 'backend/homePageAdmin/partials/homeEdit.html'
            },
            {
              name: 'publish',
              url: '/publish',
              controller: 'shopSliderCtrl as ctrl',
              templateUrl: 'backend/homePageAdmin/partials/homeEdit.html'
            }
          ]
        },
        {
          name: 'newsletter',
          url: '/newsletter',
          template: `<div ui-view class="newsletter"></div>`,
          children: [
            {
              name: 'list',
              url: '/list',
              controller: 'newsletterListCtrl as ctrl',
              templateUrl: 'backend/newsletter/partials/list.html'
            },
            {
              name: 'selections',
              url: '/selections/{newsletterId}',
              controller: 'newsletterSelectionCtrl as ctrl',
              templateUrl: 'backend/newsletter/partials/selections.html'
            },
            {
              name: 'preview',
              url: '/preview/{newsletterId}',
              controller: 'newsletterPreviewCtrl as ctrl',
              templateUrl: 'backend/newsletter/partials/preview.html'
            }
          ]
        },
        {
          name: 'shop',
          url: '/shop',
          template: '<div ui-view class="shopBackend"></div>',
          params: {
            area: AreaType.SHOP
          },
          children: [
            {
              name: 'list',
              url: '/list',
              controller: 'shopBackendListCtrl as ctrl',
              templateUrl: 'backend/shop/partials/purchases.html',
              resolve: {
                purchases: function (shopBackendModel: ShopBackendModel) {
                  return shopBackendModel.findFromOrdered(true);
                }
              }
            },
            {
              name: 'detail',
              url: '/detail/{purchaseId}',
              controller: 'purchaseCtrl as ctrl',
              templateUrl: 'backend/shop/partials/purchase/purchaseDetail.html',
              resolve: {
                purchase: function (shopBackendModel: ShopBackendModel, $stateParams: StateParams) {
                  return shopBackendModel.findPurchase($stateParams['purchaseId']);
                }
              }
            },
            {
              name: 'creditVoucher',
              url: '/creditVoucher/{purchaseId}',
              controller: 'shopBackendCreditVoucherCtrl as ctrl',
              templateUrl: 'backend/shop/partials/creditVoucher.html',
              resolve: {
                purchase: function (shopBackendModel: ShopBackendModel, $stateParams: StateParams) {
                  return shopBackendModel.findPurchase($stateParams['purchaseId']);
                }
              }
            },
            {
              name: 'countries',
              url: '/countries',
              templateUrl: 'backend/shop/partials/countries.html',
              controller: 'countriesCtrl as ctrl',
              resolve: {
                importantCountries: function (countryModel: CountryModel) {
                  return countryModel.getImportantCountries();
                },
                otherCountries: function (countryModel: CountryModel) {
                  return countryModel.getOtherCountries();
                }
              }
            },
            {
              name: 'country',
              url: '/country/show/{countryId}',
              templateUrl: 'backend/shop/partials/countryForm.html',
              controller: 'countryFormCtrl as ctrl',
              resolve: {
                country: function ($stateParams: StateParams, countryModel: CountryModel) {
                  let countryId = $stateParams['countryId'];
                  return countryModel.getCountry(countryId);
                }
              }
            },
            {
              name: 'products',
              url: '/products',
              templateUrl: 'backend/shop/partials/productsOverview.html',
              controller: 'productOverviewCtrl as ctrl',
              resolve: {
                data: endpoint => endpoint({ url: 'shop/backend/products/list' })
              },
              onEnter: scrollUpService => scrollUpService.scroll()
            },
            {
              name: 'vouchers',
              url: '/vouchers',
              templateUrl: 'backend/shop/partials/vouchersOverview.html',
              controller: 'voucherOverviewCtrl as ctrl',
              resolve: {
                data: endpoint =>
                  endpoint({ url: 'shop/backend/vouchers/list', data: { page: 0, pageSize: 50 } })
              },
              onEnter: scrollUpService => scrollUpService.scroll()
            },
            {
              name: 'voucher',
              url: '/voucher/show/{voucherId}',
              templateUrl: 'backend/shop/partials/voucherForm.html',
              controller: 'voucherFormCtrl as ctrl',
              resolve: {
                voucher: (voucherFormModel: VoucherFormModel, $stateParams: StateParams) =>
                  voucherFormModel.getVoucher($stateParams['voucherId']),
                productList: (voucherFormModel: VoucherFormModel) =>
                  voucherFormModel.getProductDisplayList()
              },
              onEnter: scrollUpService => scrollUpService.scroll()
            },
            {
              name: 'add-voucher',
              url: '/voucher/add',
              templateUrl: 'backend/shop/partials/voucherForm.html',
              controller: 'voucherFormCtrl as ctrl',
              resolve: {
                voucher: (): Voucher => {
                  let today = new Date();
                  let tomorrow = new Date();
                  tomorrow.setDate(today.getDate() + 1);
                  return {
                    validTo: tomorrow,
                    validFrom: today,
                    productId: 0,
                    code: '',
                    discountRate: 0
                  };
                },
                data: function () {
                  return [];
                },
                productList: (voucherFormModel: VoucherFormModel) =>
                  voucherFormModel.getProductDisplayList()
              }
            },
            {
              name: 'product',
              url: '/product/show/{productId}',
              templateUrl: 'backend/shop/partials/productForm.html',
              controller: 'productFormCtrl as ctrl',
              resolve: {
                product: (productFormModel: ProductFormModel, $stateParams: StateParams) =>
                  productFormModel.getProduct($stateParams['productId']),
                data: (productFormModel: ProductFormModel, $stateParams: StateParams) =>
                  productFormModel.getData($stateParams['productId'])
              },
              onEnter: scrollUpService => scrollUpService.scroll()
            },
            {
              name: 'add-product',
              url: '/product/add',
              templateUrl: 'backend/shop/partials/productForm.html',
              controller: 'productFormCtrl as ctrl',
              resolve: {
                product: function () {
                  return { volumeLitre: 0, weightGram: 0 };
                },
                data: function () {
                  return [];
                }
              }
            },
            {
              name: 'product-detail',
              url: '/product-detail/show/{productDetailId}',
              templateUrl: 'backend/shop/partials/productDetailForm.html',
              controller: 'productDetailFormCtrl as ctrl',
              resolve: {
                productData: function (productDetailFormModel, $stateParams: StateParams) {
                  return productDetailFormModel.getProductDetail($stateParams['productDetailId']);
                },
                countries: function (countryModel: CountryModel) {
                  return countryModel.getCountries();
                }
              }
            },
            {
              name: 'add-product-detail',
              url: '/product-detail/add/{productId}',
              templateUrl: 'backend/shop/partials/productDetailForm.html',
              controller: 'productDetailFormCtrl as ctrl',
              resolve: {
                productData: function ($stateParams: StateParams) {
                  return {
                    productId: $stateParams['productId']
                  };
                },
                countries: function (countryModel: CountryModel) {
                  return countryModel.getCountries();
                }
              }
            }
          ]
        },
        {
          name: 'table',
          url: '/table',
          template: '<div ui-view class="shopBackend"></div>',
          params: {
            area: AreaType.SHOP
          },
          children: [
            {
              name: 'list',
              url: '/list',
              controller: 'shopBackendListCtrl as ctrl',
              templateUrl: 'backend/table/partials/purchases.html',
              resolve: {
                purchases: function (shopBackendModel: ShopBackendModel) {
                  return shopBackendModel.findFromOrdered(true);
                }
              }
            },

          ]
        },
        {
          name: 'translation',
          url: '/translation',
          templateUrl: 'backend/translation/partials/main.html',
          controller: 'translation.mainCtrl as translationCtrl',
          resolve: {
            filterObject: [
              '$stateParams',
              function ($stateParams) {
                return {
                  category: null,
                  status: null,
                  searchKey: null,
                  locale: $stateParams.locale
                };
              }
            ]
          },
          children: [
            {
              name: 'listWord',
              url: '/list',
              templateUrl: 'backend/translation/partials/list.html',
              controller: 'translation.listCtrl as listCtrl',
              resolve: {
                wordList: [
                  'translationModel',
                  '$stateParams',
                  'filterObject',
                  function (model, $stateParams, filterObject) {
                    console.log('listCtrl > filterObject >', filterObject);
                    return model.list(filterObject.locale);
                  }
                ],
                categories: [
                  'categoryService',
                  function (categories) {
                    return categories.getAll().then(function (list) {
                      list.splice(0, 0, {
                        title: 'All'
                      });
                      return list;
                    });
                  }
                ],
                locales: [
                  'localeService',
                  function (locales) {
                    return locales.getAll();
                  }
                ],
                tranStatus: [
                  'translationStatusService',
                  function (tranStatus) {
                    return tranStatus.getAll().then(function (list) {
                      list.splice(0, 0, 'All');
                      return list;
                    });
                  }
                ]
              }
            },
            {
              name: 'editWord',
              url: '/edit/{currentLocale}/{word}',
              templateUrl: 'backend/translation/partials/edit.html',
              controller: 'translation.editCtrl as ctrl',
              resolve: {
                currentWord: [
                  'translationModel',
                  '$stateParams',
                  'filterObject',
                  function (model, $stateParams, filterObject) {
                    console.log(
                      'editCtrl > filterObject >',
                      filterObject,
                      $stateParams.currentLocale,
                      $stateParams.word
                    );
                    return model.findWord($stateParams.word, filterObject.locale);
                  }
                ],
                occurrences: [
                  'occurrenceService',
                  '$stateParams',
                  function (occurrenceService, $stateParams) {
                    return occurrenceService.search($stateParams.word);
                  }
                ]
              }
            }
          ],
          permission: ['anonymous']
        }
      ]
    });

    // payment
    stateHelperProvider.setNestedState({
      name: 'root.payment',
      url: '/payment',
      template: '<ui-view></ui-view>',
      resolve: {
        lazyLoad: [
          '$ocLazyLoad',
          '$q',
          function ($ocLazyLoad, $q) {
            let deferred = $q.defer();
            // It is webpack's require here, not requirejs
            (<any>require).ensure(
              ['../../payment/js/payment'],
              function () {
                let module = require('../../payment/js/payment');
                $ocLazyLoad.load({
                  name: 'payment'
                });
                deferred.resolve(module);
              },
              'payment'
            );
            return deferred.promise;
          }
        ]
      },
      children: [
        {
          name: 'checkout',
          url: '/checkout',
          controller: 'paymentCheckoutCtrl as ctrl',
          templateUrl: 'payment/partials/payment.html'
        }
      ]
    });

    let templateUrl = function (name) {
      return 'member/partials/' + name + '.html';
    };
    const toFirstUpper = function (str) {
      return str.substr(0, 1).toUpperCase() + str.substr(1);
    };
    const createStateObject = function (name, domain, resolveFunc = null) {
      let result = {
        name: domain != null && domain !== name ? '' + domain + name : name,
        url:
          domain != null
            ? '/account/' + domain + '/' + name
            : name !== 'account'
              ? '/account/' + name
              : '/' + name,
        templateUrl:
          domain != null
            ? templateUrl(domain + '/' + domain + toFirstUpper(name))
            : templateUrl(name),
        controller:
          domain != null && domain !== name
            ? 'member.' + domain + toFirstUpper(name) + 'Ctrl'
            : 'member.' + name + 'Ctrl',
        params: {
          permissions: ['fullLogin']
        },
        resolve: {}
      };
      if (resolveFunc != null) {
        result.resolve = {
          data: ['memberModel', 'member', (model, member) => resolveFunc(model, member)]
        };
      }
      return result;
    };

    $urlRouterProvider.when(
      '/:lang/member/account/profile',
      '/:lang/member/account/profile/overview'
    );
    $urlRouterProvider.when(
      '/:lang/member/account/subscription',
      '/:lang/member/account/subscription/overview'
    );
    stateHelperProvider.setNestedState({
      name: 'root.member',
      url: '/member',
      controller: 'member.mainCtrl',
      templateUrl: templateUrl('main'),
      resolve: {
        lazyLoad: ($ocLazyLoad, $$animateJs, $q) => {
          let deferred = $q.defer();
          // It is webpack's require here, not requirejs
          (<any>require).ensure(
            ['../../member/js/main'],
            function () {
              let module = require('../../member/js/main');
              $ocLazyLoad.load({
                name: 'member'
              });
              deferred.resolve(module);
            },
            'member'
          );
          return deferred.promise;
        }
      },
      children: [
        {
          name: 'bills',
          url: '/account/bills',
          template: '<ui-view></ui-view>',
          params: {
            permissions: ['fullLogin']
          },
          children: [
            {
              name: 'subscription',
              url: '/subscription',
              templateUrl: 'member/partials/billsSubscription.html',
              controller: 'member.billsSubscriptionCtrl as ctrl',
              params: {
                permissions: ['fullLogin']
              },
              resolve: {
                data: function (memberModel) {
                  return memberModel.memberBillsSubscription();
                }
              }
            },
            {
              name: 'shop',
              url: '/shop',
              templateUrl: 'member/partials/billsShop.html',
              controller: 'member.billsShopCtrl as ctrl',
              params: {
                permissions: ['fullLogin']
              },
              resolve: {
                data: function (memberModel) {
                  return memberModel.memberBillsShop();
                }
              }
            }
          ]
        },
        createStateObject('address', null, function (model, member) {
          return model.memberAddress();
        }),
        createStateObject('overview', 'profile'),
        createStateObject('user', 'profile', function (model, member) {
          return model.memberProfileUser();
        }),
        createStateObject('language', 'profile', function (model, member) {
          return model.memberProfileLanguage();
        }),
        createStateObject('email', 'profile', function (model, member) {
          return model.memberProfileEmail();
        }),
        createStateObject('password', 'profile'),
        createStateObject('overview', 'subscription', function (model) {
          return model.memberAboPeriod();
        }),
        createStateObject('pay', 'subscription', function (model, member) {
          return model.memberSubscriptionPaying();
        }),
        createStateObject('cancellation', 'subscription', function (model, member) {
          return model.memberSubscriptionCancellation();
        }),

        {
          name: 'profileadditional',
          url: '/account/profile/additional',
          templateUrl: 'member/partials/profile/profileAdditional.html',
          controller: 'member.profileAdditionalCtrl',
          resolve: {
            data: [
              'memberModel',
              'member',
              function (model, member) {
                return model.memberProfileAdditional();
              }
            ]
          },
          controllerAs: 'ctrl'
        },
        {
          name: 'subscriptionlicences',
          url: '/account/subscription/licences',
          templateUrl: 'member/partials/subscription/subscriptionLicences.html',
          controller: 'member.licensesCtrl',
          resolve: {
            data: [
              'memberModel',
              'member',
              function (model, member) {
                return model.memberSubscriptionLicences(member);
              }
            ]
          },
          controllerAs: 'ctrl'
        },
        {
          name: 'subscriptionduration',
          url: '/account/subscription/duration',
          templateUrl: 'member/partials/subscription/subscriptionDuration.html',
          controller: 'member.subscriptionDurationCtrl',
          resolve: {
            data: [
              'memberModel',
              'member',
              function (model, member) {
                return model.memberSubscriptionDuration(member);
              }
            ],
            availableProducts: [
              'memberModel',
              function (model) {
                return model.memberAvailableSubscriptions();
              }
            ],
            accountData: [
              'memberModel',
              function (model) {
                return model.memberAccount();
              }
            ]
          },
          controllerAs: 'ctrl'
        },
        {
          name: 'account',
          url: '/account',
          templateUrl: 'member/partials/account.html',
          controller: 'member.accountCtrl',
          resolve: {
            data: [
              'memberModel',
              function (model) {
                return model.memberAccount();
              }
            ]
          },
          controllerAs: 'ctrl'
        },
        {
          name: 'commit',
          url: '/account/commit?tid"',
          controller: 'member.commitController',
          template: empty,
          resolve: {
            data: [
              '$stateParams',
              function ($stateParams) {
                return $stateParams.tid;
              }
            ]
          }
        },
        {
          name: 'msgs',
          url: '/msgs',
          templateUrl: 'member/partials/memberMsgs.html',
          controller: 'member.msgsCtrl',
          resolve: {
            member: function (member) {
              return member;
            }
          },
          params: {
            area: 4
          }
        },
        {
          name: 'message',
          url: '/message?msgVal&msgKey',
          templateUrl: 'member/partials/memberMessage.html',
          controller: 'memberMessageCtrl as ctrl'
        },
        {
          name: 'welcome',
          url: '/welcome',
          templateUrl: 'member/partials/welcome/main.html',
          children: [
            {
              name: 'test',
              url: '/test?returnTo',
              templateUrl: 'member/partials/welcome/test.html',
              controller: 'welcomeCtrl'
            },
            {
              name: 'created',
              url: '/created?returnTo',
              templateUrl: 'member/partials/welcome/created.html',
              controller: 'welcomeCtrl'
            },
            {
              name: 'ordered',
              url: '/ordered?returnTo',
              templateUrl: 'member/partials/welcome/ordered.html',
              controller: 'welcomeCtrl'
            },
            {
              name: 'new',
              url: '/new?returnTo',
              templateUrl: 'member/partials/welcome/new.html',
              controller: 'welcomeCtrl'
            },
            {
              name: 'gift',
              url: '/gift?returnTo',
              templateUrl: 'member/partials/welcome/gift.html',
              controller: 'welcomeCtrl'
            },
            {
              name: 'community',
              url: '/community?returnTo',
              templateUrl: 'member/partials/welcome/community.html',
              controller: 'welcomeCtrl'
            },
            {
              name: 'orderedGift',
              url: '/ordered-gift?returnTo',
              templateUrl: 'member/partials/welcome/ordered-gift.html',
              controller: 'welcomeCtrl'
            }
          ]
        },
        {
          name: 'activation',
          url: '/activation',
          templateUrl: 'member/partials/activation/main.html',
          controller: 'activation.activationMainCtrl',
          children: [
            {
              name: 'check',
              url: '/check?t&context',
              templateUrl: 'member/partials/activation/check.html',
              controller: 'activation.activationCheckCtrl',
              resolve: {
                data: [
                  '$stateParams',
                  'request',
                  ($stateParams, request) =>
                    request({
                      url: 'activation/main/check',
                      data: {
                        token: $stateParams.t
                      }
                    })
                ]
              }
            },
            {
              name: 'notValid',
              url: '/notValid?id',
              templateUrl: 'member/partials/activation/notValid.html',
              controller: 'activation.activationCtrl'
            },
            {
              name: 'used',
              url: '/used?id',
              templateUrl: 'member/partials/activation/used.html',
              controller: 'activation.activationCtrl'
            },
            {
              name: 'expired',
              url: '/expired?id',
              templateUrl: 'member/partials/activation/expired.html',
              controller: 'activation.activationCtrl'
            },
            {
              name: 'cancelled',
              url: '/cancelled?id',
              templateUrl: 'member/partials/activation/cancelled.html',
              controller: 'activation.activationCtrl'
            },
            {
              name: 'activated',
              url: '/activated',
              controller: 'activation.activationCtrl',
              templateUrl: 'member/partials/activation/activated.html',
              params: {
                //if context is set to "shop", user comes from ShopRegistration
                context: null
              }
            },
            {
              name: 'sent',
              url: '/sent',
              templateUrl: 'member/partials/activation/sent.html',
              controller: [
                '$scope',
                '$state',
                function ($scope, $state) {
                  return ($scope.goTo = function () {
                    return $state.go('root.login');
                  });
                }
              ]
            },
            {
              name: 'alreadyVerified',
              url: '/alreadyVerified',
              controller: 'activation.activationCtrl',
              templateUrl: 'member/partials/activation/alreadyVerified.html'
            }
          ]
        }
      ]
    });

    // reports
    stateHelperProvider.setNestedState({
      name: 'root.reports',
      url: '/reports',
      template: "<div class='reports'><ui-view></ui-view></div>",
      resolve: {
        lazyLoad: ($ocLazyLoad, $$animateJs, $q) => {
          let deferred = $q.defer();
          // It is webpack's require here, not requirejs
          (<any>require).ensure(
            ['../../reports/js/main'],
            function () {
              let module = require('../../reports/js/main');
              $ocLazyLoad.load({
                name: 'reports'
              });
              deferred.resolve(module);
            },
            'reports'
          );
          return deferred.promise;
        }
      },
      children: [
        {
          name: 'planner',
          url: '/planner',
          controller: 'reports.plannerCtrl',
          templateUrl: 'reports/partials/planner.html'
        },
        {
          name: 'registrationOrigin',
          url: '/registrationOrigin',
          controller: 'registrationOriginCtrl as ctrl',
          templateUrl: 'reports/partials/registrationOrigin.html'
        },
        {
          name: 'parallelLog',
          url: '/parallelLog',
          controller: 'parallelLogCtrl as ctrl',
          templateUrl: 'reports/partials/parallelLog.html'
        },
        {
          name: 'validation',
          url: '/validation',
          template: `<div class="reports"><ui-view></ui-view></div>`,
          children: [
            {
              name: 'report',
              url: '/report',
              template: `<div class="reports"><ui-view></ui-view></div>`,
              children: [
                {
                  name: 'list',
                  url: '/list',
                  controller: 'reportListCtrl as ctrl',
                  templateUrl: 'reports/partials/reportList.html'
                },
                {
                  name: 'detail',
                  url: '/detail/{reportId}',
                  controller: 'reportDetailCtrl as ctrl',
                  templateUrl: 'reports/partials/reportDetail.html',
                  resolve: {
                    localesList: function (reportRequests) {
                      return reportRequests.getLanguages();
                    }
                  }
                }
              ]
            },
            {
              name: 'idea',
              url: '/idea',
              template: `<div class="reports"><ui-view></ui-view></div>`,
              children: [
                {
                  name: 'list',
                  url: '/list/{reportId:int}?language&error',
                  controller: 'ideaListCtrl as ctrl',
                  templateUrl: 'reports/partials/ideaList.html'
                },
                {
                  name: 'detail',
                  url: '/{reportId:int}/{language}/{articleId}?error',
                  controller: 'ideaDetailCtrl as ctrl',
                  templateUrl: 'reports/partials/ideaDetail.html'
                }
              ]
            }
          ]
        },
        {
          name: 'payment',
          url: '/payment',
          template: `<div class="reports"><ui-view></ui-view></div>`,
          children: [
            {
              name: 'shop',
              url: '/shop/:orderNr?',
              controller: 'shopPaymentReportCtrl as ctrl',
              templateUrl: 'reports/partials/shopPaymentReport.html'
            }
          ]
        }
      ]
    });

    // planner
    (function planner() {
      const templateUrl = name => `planner/partials/${name}.html`;

      const now = moment();

      stateHelperProvider.setNestedState({
        name: 'root.plan',
        url: '/plan',
        controller: 'planCtrl as ctrl',
        template: '<ui-view></ui-view>',
        resolve: {
          lazyLoad: ($ocLazyLoad, $$animateJs, $q) => {
            let deferred = $q.defer();
            // It is webpack's require here, not requirejs
            (<any>require).ensure(
              ['../../planner/js/main'],
              function () {
                let module = require('../../planner/js/main');
                $ocLazyLoad.load({
                  name: 'planner'
                });
                deferred.resolve(module);
              },
              'planner'
            );
            return deferred.promise;
          }
        },
        params: {
          area: 2
        }
      });
      stateHelperProvider.setNestedState({
        name: 'root.plan.planType',
        url: '/{planType}',
        templateUrl: templateUrl('planContainer'),
        controller: 'planTypeCtrl as ctrl',
        children: [
          {
            name: 'detail',
            url: '/detail/{planId}',
            controller: 'planner.detailCtrl',
            templateUrl: templateUrl('detail'),
            resolve: {
              plan(planResolver, $stateParams) {
                return planResolver($stateParams.planType, $stateParams.planId);
              }
            },
            params: {
              planId: 'current'
            }
          },
          {
            name: 'add',
            url: '/add',
            templateUrl: templateUrl('planDetail'),
            controller: 'planFormCtrl as ctrl',
            resolve: {
              plan() {
                return null;
              },
              tagList(planModel) {
                return planModel.findTags();
              },
              action() {
                return 'add';
              }
            }
          },
          {
            name: 'edit',
            url: '/edit/{planId}',
            templateUrl: templateUrl('planDetail'),
            controller: 'planFormCtrl as ctrl',
            resolve: {
              plan($stateParams, planModel) {
                return planModel.find($stateParams.planId, $stateParams.planType);
              },
              tagList(planModel) {
                return planModel.findTags();
              },
              action() {
                return 'edit';
              }
            }
          },
          {
            name: 'copy',
            url: '/copy/{planId}',
            templateUrl: templateUrl('planDetail'),
            controller: 'planFormCtrl as ctrl',
            resolve: {
              plan($stateParams, planModel) {
                return planModel.find($stateParams.planId, $stateParams.planType);
              },
              tagList(planModel) {
                return planModel.findTags();
              },
              action() {
                return 'copy';
              }
            }
          },
          {
            name: 'addComment',
            url: '/comment/add/{planId:int}/{plannedDate:date}',
            templateUrl: templateUrl('comment'),
            controller: 'planCommentCtrl as ctrl',
            resolve: {
              action() {
                return 'add';
              },
              comment() {
                return null;
              },
              tinymce(tinymceLoader) {
                return tinymceLoader.load();
              }
            }
          },
          {
            name: 'editComment',
            url: '/comment/edit/{commentId:int}',
            templateUrl: templateUrl('comment'),
            controller: 'planCommentCtrl as ctrl',
            resolve: {
              action() {
                return 'edit';
              },
              comment(planModel, $stateParams) {
                return planModel.findComment($stateParams.commentId);
              }
            }
          },
          {
            name: 'addIdea',
            url: '/idea/add/{planId:int}/{plannedDate:date}',
            templateUrl: templateUrl('idea'),
            controller: 'planIdeaCtrl as ctrl'
          },
          {
            name: 'print',
            url: '/print/{planId}',
            controller: 'planPrintCtrl as ctrl',
            templateUrl: templateUrl('printSelection'),
            resolve: {
              plan(planModel, $stateParams) {
                return planModel.find($stateParams.planId, $stateParams.planType);
              }
            }
          },
          {
            name: 'yearly',
            url: '/yearly/{year:int}',
            controller: 'yearMonthController as ctrl',
            templateUrl: templateUrl('planYearMonth'),
            params: {
              year: now.year()
            }
          },
          {
            name: 'monthly',
            url: '/monthly/{year:int}/{month:int}',
            controller: 'yearMonthController as ctrl',
            templateUrl: templateUrl('planYearMonth'),
            params: {
              year: now.year(),
              month: now.month() + 1
            }
          },
          {
            name: 'weekly',
            url: '/weekly/{weekDate}',
            controller: 'planWeeklyCtrl as ctrl',
            templateUrl: templateUrl('planWeekly'),
            resolve: {
              plan(planModel, planHelper, $stateParams) {
                return planModel.findByWeek(planHelper.toMonday($stateParams.weekDate));
              }
            }
          },
          {
            name: 'addTo', // context-specific redirection
            url: '/addTo/{itemId:int}',
            controller: 'planAddToCtrl as ctrl',
            template: '<ui-view></ui-view>',
            resolve: {
              plans(planModel, $stateParams) {
                return planModel.findAll($stateParams.planType);
              }
            }
          },
          {
            name: 'addToCalendar',
            url: '/addToCalendar/{itemId:int}',
            controller: 'yearMonthController as ctrl',
            templateUrl: templateUrl('planYearMonth'),
            params: {
              year: now.year(),
              month: now.month() + 1
            }
          }
        ]
      });
    })();

    // favourites
    $stateProvider
      .state({
        name: 'root.memberArea',
        url: '/member-area',
        template: empty,
        resolve: {
          lazyLoad: ($ocLazyLoad, $$animateJs, $q) => {
            let deferred = $q.defer();
            // It is webpack's require here, not requirejs
            (<any>require).ensure(
              ['../../favourites/js/main'],
              function () {
                let module = require('../../favourites/js/main');
                $ocLazyLoad.load({
                  name: 'favourites'
                });
                deferred.resolve(module);
              },
              'favourites'
            );
            return deferred.promise;
          }
        },
        abstract: true
      })
      .state('root.memberArea.favourites', {
        url: '/favourites/{path:nonURIEncoded}?preview&previewCategory&ageGroupFilter',
        params: {
          area: AreaType.FAVOURITES,
          permissions: ['testSubscription']
        },
        resolve: {
          feed: function (endpoint, $stateParams) {
            return endpoint({
              url: 'favourites/resolve',
              data: new ResolverRequest($stateParams.path, $stateParams.ageGroupFilter)
            });
          }
        },
        template: '<div ng-include="ctrl.template"></div>',
        controller: 'favouritesCtrl as ctrl'
      });

    stateHelperProvider.setNestedState({
      name: 'root.page1',
      url: '/page-1',
      template: '<a href="http://kiga:10081/ng/de/page-2">To Page 2</a>',
      controller: foo => console.log(foo),
      resolve: {
        foo: () => new Promise(resolve => window.setTimeout(() => resolve(true)))
      }
    });
    stateHelperProvider.setNestedState({
      name: 'root.page2',
      url: '/page-2',
      template: 'At Page 2',
      controller: bar => { },
      resolve: {
        bar: () => true
      }
    });

    // search
    stateHelperProvider.setNestedState({
      name: 'root.searchStatic',
      url: '/search-static?query&ageGroup&parent&page',
      controller: 'searchCtrl as ctrl',
      params: { area: AreaType.IDEAS },
      templateUrl: 'search/partials/ideaCategories.html'
    });

    // memberIdeas
    templateUrl = name => `memberIdeas/partials/${name}.html`;
    stateHelperProvider.setNestedState({
      name: 'root.memberArea.ideas',
      url: '/ideas',
      template: '<ui-vie></ui-view>',
      params: {
        area: AreaType.MEMBERIDEAS
      },
      resolve: {
        lazyLoad: ($ocLazyLoad, $$animateJs, $q) => {
          let deferred = $q.defer();
          // It is webpack's require here, not requirejs
          (<any>require).ensure(
            ['../../memberIdeas/js/main'],
            function () {
              let module = require('../../memberIdeas/js/main');
              $ocLazyLoad.load({
                name: 'memberIdeas'
              });
              deferred.resolve(module);
            },
            'memberIdeas'
          );
          return deferred.promise;
        }
      },
      children: [
        {
          name: 'upload',
          url: '/upload',
          controller: 'uploadCtrl as ctrl',
          templateUrl: templateUrl('upload'),
          resolve: {
            idea: function () {
              return null;
            },
            categories: function (memberIdeaCategoryService) {
              return memberIdeaCategoryService.getAll();
            }
          }
        },
        {
          name: 'edit',
          url: '/edit/:ideaid',
          controller: 'uploadCtrl as ctrl',
          templateUrl: templateUrl('upload'),
          resolve: {
            idea: function (uploadModel, $stateParams) {
              return uploadModel.getIdea($stateParams.ideaid);
            },
            categories: function (memberIdeaCategoryService) {
              return memberIdeaCategoryService.getAll();
            }
          }
        },
        {
          name: 'list',
          url: '/list',
          controller: 'uploadListCtrl as ctrl',
          templateUrl: templateUrl('uploadedList'),
          resolve: {
            ideas: function (uploadModel) {
              return uploadModel.list();
            },
            categories: function (memberIdeaCategoryService) {
              return memberIdeaCategoryService.getAll();
            }
          }
        }
      ]
    });

    templateUrl = function (name) {
      return 'registration/partials/' + name + '.html';
    };
    stateHelperProvider.setNestedState({
      name: 'root.registration',
      url: '/registrierung',
      resolve: {
        lazyLoad: ($ocLazyLoad, $$animateJs, $q) => {
          let deferred = $q.defer();
          // It is webpack's require here, not requirejs
          (<any>require).ensure(
            ['../../registration/js/main'],
            function () {
              let module = require('../../registration/js/main');
              $ocLazyLoad.load({
                name: 'registration'
              });
              deferred.resolve(module);
            },
            'registration'
          );
          return deferred.promise;
        }
      },
      controller: function () {
        (<any>require)(['../../registration/css/registration.less']);
      },
      area: 4,
      templateUrl: templateUrl('main'),
      children: [
        {
          name: 'preSelection',
          url: '/preSelection?id',
          templateUrl: templateUrl('preSelection'),
          controller: 'registration.regPreselectionCtrl',
          resolve: {
            regData: [
              'registration.model',
              'registration.regDataViewModel',
              function (model, regDataViewModel) {
                if (regDataViewModel.isInitialized()) {
                  return regDataViewModel.getRegData();
                } else {
                  return model.registrationInitiate();
                }
              }
            ]
          }
        },
        {
          name: 'info',
          onEnter: function (scrollUpService) {
            return scrollUpService.scroll();
          },
          url: '/info?id',
          templateUrl: templateUrl('info'),
          controller: 'registration.regInfoCtrl',
          resolve: {
            products: [
              'registration.model',
              'registration.regDataViewModel',
              function (model, regDataViewModel) {
                if (regDataViewModel.isInitialized()) {
                  return model.getProducts(
                    regDataViewModel.getFieldValue('country'),
                    regDataViewModel.getFieldValue('productGroup')
                  );
                } else {
                  return [];
                }
              }
            ]
          }
        },
        {
          name: 'product',
          onEnter: function (scrollUpService) {
            return scrollUpService.scroll();
          },
          url: '/product?id',
          templateUrl: templateUrl('product'),
          controller: 'registration.regProductCtrl',
          resolve: {
            products: [
              'registration.model',
              'registration.regDataViewModel',
              function (model, regDataViewModel) {
                if (regDataViewModel.isInitialized()) {
                  return model.getProducts(
                    regDataViewModel.getFieldValue('country'),
                    regDataViewModel.getFieldValue('productGroup')
                  );
                } else {
                  return [];
                }
              }
            ]
          }
        },
        {
          name: 'customerData',
          onEnter: function (scrollUpService) {
            return scrollUpService.scroll();
          },
          url: '/customerData?id',
          templateUrl: templateUrl('customerData'),
          controller: 'registration.regCustomerDataCtrl'
        },
        {
          name: 'payment',
          onEnter: function (scrollUpService) {
            return scrollUpService.scroll();
          },
          url: '/payment?id&code',
          templateUrl: templateUrl('payment'),
          controller: 'registration.regPaymentCtrl'
        },
        {
          name: 'resume',
          onEnter: function (scrollUpService) {
            return scrollUpService.scroll();
          },
          url: '/resume?id&code',
          controller: 'registration.regResumeCtrl',
          template: '<ui-view></ui-view>',
          resolve: {
            regData: [
              '$state',
              '$stateParams',
              'registration.model',
              function ($state, $stateParams, model) {
                if ($stateParams.id != null && $stateParams.code != null) {
                  if ($stateParams.code === 'default') {
                    return model.registrationResumeDefault({
                      id: $stateParams.id,
                      code: $stateParams.code
                    });
                  } else {
                    return model.registrationResume({
                      id: $stateParams.id,
                      code: $stateParams.code
                    });
                  }
                } else {
                  return $state.go('preSelection');
                }
              }
            ]
          }
        },
        {
          name: 'summary',
          onEnter: function (scrollUpService) {
            return scrollUpService.scroll();
          },
          url: '/summary?id',
          templateUrl: templateUrl('summary'),
          controller: 'registration.regSummaryCtrl'
        },
        {
          name: 'welcome',
          url: '/welcome',
          templateUrl: templateUrl('welcome')
        },
        {
          name: 'giftActivation',
          url: '/giftActivation',
          templateUrl: templateUrl('gitfActivation'),
          controller: 'regGiftActivationCtrl as ctrl',
          resolve: {
            country: function (request) {
              return request('main/country');
            }
          }
        }
      ]
    });

    // shop
    stateHelperProvider.setNestedState({
      name: 'root.shop',
      url: '/shop',
      controller: 'shopMainCtrl',
      templateUrl: 'shop/partials/main.html',
      params: { themeName: 'middleRed' },
      stateName: 'shop',
      abstract: true,
      resolve: {
        lazyLoad: ($ocLazyLoad, $$animateJs, $q) => {
          let deferred = $q.defer();
          // It is webpack's require here, not requirejs
          (<any>require).ensure(
            ['../../shop/js/shop'],
            function () {
              let module = require('../../shop/js/shop');
              $ocLazyLoad.load({
                name: 'shop'
              });
              deferred.resolve(module);
            },
            'shop'
          );
          return deferred.promise;
        }
      },
      children: [
        {
          name: 'basket',
          url: '/basket',
          templateUrl: 'shop/partials/cart.html',
          controller: 'shopCartCtrl as ctrl',
          resolve: {
            basketData: shopBasketService => shopBasketService.find(),
            amountList: () => _.times(25, i => i + 1),
            context: () => 'basket',
            canDeliver: shopBasketService => shopBasketService.canDeliver()
          }
        },
        {
          name: 'checkout',
          url: '/checkout',
          template: empty,
          abstract: true,
          params: {
            permissions: ['shopCheckout']
          },
          children: [
            {
              name: 'cart',
              url: '/cart',
              templateUrl: 'shop/partials/cart.html',
              controller: 'shopCartCtrl as ctrl',
              resolve: {
                basketData: shopBasketService => shopBasketService.findWithCheckout(),
                amountList: () => _.times(25, i => i + 1),
                context: () => 'checkout',
                canDeliver: shopBasketService => shopBasketService.canDeliver()
              }
            },
            {
              name: 'shipping',
              url: '/shipping',
              templateUrl: 'shop/partials/shipping.html',
              controller: 'shopShippingCtrl as ctrl',
              resolve: {
                country: request => request('main/country'),
                basketData: shopBasketService => shopBasketService.findWithCheckout(),
                canDeliver: shopBasketService => shopBasketService.canDeliver()
              }
            },
            {
              name: 'payment',
              url: '/payment?code',
              templateUrl: 'shop/partials/payment.html',
              controller: 'shopPaymentCtrl as ctrl',
              resolve: {
                paymentMethods: shopBasketService => shopBasketService.getPaymentMethods(),
                basketData: shopBasketService => shopBasketService.findWithCheckout()
              }
            },
            {
              name: 'summary',
              url: '/summary',
              templateUrl: 'shop/partials/summary.html',
              controller: 'shopSummaryCtrl as ctrl',
              resolve: {
                basketData: shopBasketService => shopBasketService.findWithCheckout(),
                canDeliver: shopBasketService => shopBasketService.canDeliver()
              }
            }
          ]
        },
        {
          name: 'finished',
          url: '/finished',
          templateUrl: 'shop/partials/finished.html',
          controller: 'shopFinishedCtrl as ctrl'
        }
      ]
    });
  });
}
