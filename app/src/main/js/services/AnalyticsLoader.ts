/**
 * This class loads analytics.js as described by Google. Since the loading code is copied
 * directly from on https://developers.google.com/analytics/devguides/collection/analyticsjs/
 * tslinting is disabled for that part.
 */

export default class AnalyticsLoader {
  loaded = false;

  loadScript() {
    if (!this.loaded) {
      /* tslint:disable */
      (function(i: any, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        (i[r] =
          i[r] ||
          function() {
            (i[r].q = i[r].q || []).push(arguments);
          }),
          (i[r].l = new Date().getTime());
        (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m);
      })(
        window,
        document,
        'script',
        'https://www.google-analytics.com/analytics.js',
        'ga',
        null,
        null
      );

      window['ga']('create', 'UA-887465-1', 'auto');
      window['ga']('send', 'pageview');
      /* tslint:enable */
    }
  }
}
