import * as _ from 'lodash';
import { ILinksService } from '../../../misc/js/services/AngularInterfaces';

export default class Search {
  constructor(private links: ILinksService) {}

  search(term, appendix = {}) {
    window.location.href = this.getFullUrl(term, appendix);
  }

  getFullUrl(term, appendix) {
    return this.getSearchUrl + '?Search=' + term + '&' + this.createAppendix(appendix);
  }

  getSearchUrl() {
    return this.links.findByType('mainArticleCategory').url;
  }

  createAppendix(appendix) {
    _.map(appendix, function(value, key) {
      return key + '=' + value;
    }).join('&');
  }
}
