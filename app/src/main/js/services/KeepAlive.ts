import IIntervalService = angular.IIntervalService;
/**
 * call silverstripe endpoint to extend current session
 */

export default class KeepAlive {
  constructor(private interval: IIntervalService, private request) {}

  run(): void {
    this.interval(() => {
      this.request({
        method: 'POST',
        quietMode: true,
        url: 'main/keepAlive'
      });
    }, 1000 * 10 * 60);
  }
}
