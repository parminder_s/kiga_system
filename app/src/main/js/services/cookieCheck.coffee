define ['../app', 'angular'], (app, angular) ->
  app.app.service 'cookieCheck', ->
    check: ->
      cookieEnabled = navigator.cookieEnabled
      if !cookieEnabled
        bodies = document.getElementsByTagName 'body'
        b = bodies[0]
        infoBox = document.createElement 'div'
        infoBox.setAttribute 'style', 'background-color: red; text-align: center; height: 30px; font-size: 20px;margin-top:45px;'
        infoBox.appendChild document.createTextNode(kiga.noCookiesMsg)
        b.insertBefore infoBox, b.firstChild
