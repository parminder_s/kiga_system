import { StateService, Transition, TransitionService } from 'angular-ui-router';
import * as _ from 'lodash';
import IMemberService from '../../../misc/permission/js/IMemberService';

export default class MessageRedirectorService {
  constructor(
    private $transitions: TransitionService,
    private $state: StateService,
    private memberLoader: IMemberService
  ) {}

  run(): void {
    this.$transitions.onStart({ to: '**' }, transition => {
      if (this.forceRedirect() && !this.isTransitioningToMessages(transition)) {
        const locale = this.memberLoader.get().locale;
        return transition.router.stateService.target('root.member.msgs', { locale: locale });
      }
    });
  }

  forceRedirect() {
    const messages = this.memberLoader.get().messages;
    return (
      messages && messages.length > 0 && _.some(messages, message => 'blocking' === message.Type)
    );
  }

  isTransitioningToMessages(transition: Transition) {
    return transition.targetState().state().name === 'root.member.msgs';
  }
}
