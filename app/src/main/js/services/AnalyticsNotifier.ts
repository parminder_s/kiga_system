import { TransitionService } from 'angular-ui-router';
/**
 * This class notifies Google Analytics - if available - if a new transition
 * in ui-router takes place.
 *
 * Inspired by http://jasonwatmore.com/post/2015/11/07/angularjs-google-analytics-with-the-ui-router
 *
 * Also see
 * https://developers.google.com/analytics/devguides/collection/analyticsjs/single-page-applications
 */

export default class AnalyticsNotifier {
  constructor(
    private $window: angular.IWindowService,
    private $transitions: TransitionService,
    private $location: angular.ILocationService
  ) {}

  run(): void {
    this.$transitions.onSuccess({}, transition => {
      if (this.$window['ga']) {
        this.$window['ga']('send', 'pageview', '/ng' + this.$location.path());
      }
    });
  }
}
