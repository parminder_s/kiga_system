#Sticked head behaviour. Example: www.theverge.com
define ['../app'], (app) ->
  app.app.directive "scroll", ($window) ->
    (scope, element, attrs) ->
      lastScroll = 0
      height = 124
      angular.element($window).bind "scroll", () ->
        currentScroll = window.scrollY
        if(currentScroll > lastScroll && currentScroll > 124)
          element.addClass 'hide_header'
          lastScroll = currentScroll
        else if (currentScroll < lastScroll && currentScroll > 124)
          element.removeClass 'hide_header'
          lastScroll = currentScroll
        else if currentScroll <= 124
          element.removeClass 'hide_header'
          lastScroll = currentScroll


