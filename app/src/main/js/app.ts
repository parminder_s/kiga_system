import * as angular from 'angular';
import * as _misc from 'misc';

import * as _area from '../../area/js/main';
import { cmsModule } from '../../cms/js/cms';
import * as _ideas from '../../ideas/js/ideas';
import * as _kigaUpload from '../../kigaUpload/js/app';
import * as _permission from '../../misc/permission/js/permission';
import * as _search from '../../search/js/main';

let area = _area,
  cms = cmsModule,
  kigaUpload = _kigaUpload,
  misc = _misc,
  ideas = _ideas,
  permission = _permission,
  search = _search;

export let app = angular.module('main', [
  'area',
  'cms',
  'kigaUpload',
  'misc',
  'ideas',
  'kiga.permission',
  'search'
]);
