import { CmsModuleRegistry } from '../../cms/js/services/CmsModuleRegistry';
import { IdeaResolvedTypesForCms } from '../../ideas/js/IdeaResolvedTypesForCms';
import {
  IConfigService,
  IGoBack,
  ILinksService,
  INgxNavigateService
} from '../../misc/js/services/AngularInterfaces';
import { ShopResolvedTypesForCms } from '../../shop/js/ShopResolvedTypesForCms';
import { app } from './app';
import AreaFactory from '../../area/js/AreaFactory';
import { AreaType } from '../../area/js/AreaType';
import FullLoginCtrl from './controllers/FullLoginCtrl';
import IdeasAreaCallback from '../../ideas/js/services/IdeasAreaCallback';
import RedirectorCtrl from './controllers/RedirectorCtrl';
import HomeCtrl from './controllers/HomeCtrl';
import UrlPrefixChecker from './native/UrlPrefixChecker';
import SslChecker from './native/SslChecker';
import AnalyticsLoader from './services/AnalyticsLoader';
import CookieChecker from './native/CookieChecker';
import MessageRedirectorService from './services/MessageRedirectorService';
import Search from './services/Search';
import createMainRouting from './MainRouting';
import * as _loader from './jsLoader';
import IdeasCacheJobCtrl from './controllers/IdeasCacheJobCtrl';
import DevMenuCtrl from './controllers/DevMenuCtrl';
import AnalyticsNotifier from './services/AnalyticsNotifier';
import MemberLoader from '../../misc/permission/js/MemberLoader';
import { StateService, StateProvider, StateParams } from 'angular-ui-router';
import NewsletterSubscribeCtrl from './controllers/NewsletterSubscribeCtrl';
import KeepAlive from './services/KeepAlive';
import IMemberService from '../../misc/permission/js/IMemberService';
import MainMenuCtrl from '../../main/components/mainMenu/MainMenuCtrl';
import IIntervalService = angular.IIntervalService;
import MockedCountryService from '../../misc/js/services/MockedCountryService';
import RedirectorService from '../../misc/js/services/RedirectorService';
import ToastService from '../../misc/toast/ToastService';
import CookieInfoService from '../../misc/js/services/CookieInfoService';
import CookieInfoController from '../components/cookieInfo/CookieInfoController';
import FooterController from '../components/footer/FooterController';

let loader = _loader;

UrlPrefixChecker.doCheck();
SslChecker.doCheck(window, window['kiga']);
CookieChecker.check();

createMainRouting(app);

app
  .config(function($urlRouterProvider, $locationProvider, $uibTooltipProvider) {
    $locationProvider.html5Mode(true);
    // $locationProvider.hashPrefix('!');
    /* The solution for Disabling Tooltips on Touch Devices is based on the outdated
     one I get from https://github.com/angular-ui/bootstrap/issues/2525.
     It works for "angular-bootstrap": "1.3.2", the provider name and it's dependencies
     were changed. (asv 22/12/2016)
      */
    let tooltipFactory = $uibTooltipProvider.$get[$uibTooltipProvider.$get.length - 1];
    $uibTooltipProvider.$get = [
      '$window',
      '$compile',
      '$timeout',
      '$document',
      '$uibPosition',
      '$interpolate',
      '$rootScope',
      '$parse',
      '$$stackedMap',
      function(
        $window,
        $compile,
        $timeout,
        $document,
        $position,
        $interpolate,
        $rootScope,
        $parse,
        $$stackedMap
      ) {
        if ('ontouchstart' in $window) {
          return function() {
            return {
              compile: function() {}
            };
          };
        } else {
          return tooltipFactory(
            $window,
            $compile,
            $timeout,
            $document,
            $position,
            $interpolate,
            $rootScope,
            $parse,
            $$stackedMap
          );
        }
      }
    ];
  })
  .run(function(analyticsNotifier: AnalyticsNotifier) {
    analyticsNotifier.run();
  })
  .run(function(keepAlive: KeepAlive) {
    keepAlive.run();
  })
  .filter('lineClamp', function() {
    return function(input: String, length: number) {
      if (!input) {
        return '';
      } else if (!length || input.length <= length) {
        return input;
      } else {
        return input.substr(0, length) + '...';
      }
    };
  })
  .component('devMenu', {
    controller: function(endpoint, $state: StateService, memberLoader: MemberLoader, config) {
      return new DevMenuCtrl(endpoint, $state, memberLoader, config);
    },
    templateUrl: 'main/partials/devMenu.html'
  })
  .component('mainMenu', {
    controller: function(
      member: IMemberService,
      links: ILinksService,
      $state: StateService,
      mockedCountryService: MockedCountryService,
      config: IConfigService,
      endpoint,
      toastService: ToastService
    ) {
      return new MainMenuCtrl(
        member,
        links,
        $state,
        mockedCountryService,
        config,
        endpoint,
        toastService
      );
    },
    template: '<hierarchical-menu menu-data="$ctrl.menuData"></hierarchical-menu>'
  })

  .component('cookieInfo', {
    controller: function(links: ILinksService, cookieInfoService: CookieInfoService) {
      return new CookieInfoController(links, cookieInfoService);
    },
    templateUrl: 'main/components/cookieInfo/cookieinfo.html'
  })

  .component('footerComponent', {
    controller: function(
      $rootScope,
      links: ILinksService,
      cookieInfoService: CookieInfoService,
      $state
    ) {
      return new FooterController(links, cookieInfoService, $state, $rootScope);
    },
    templateUrl: 'main/components/footer/footer.html'
  })

  .controller(
    'redirectorCtrl',
    (
      $stateParams: StateParams,
      redirectorService: RedirectorService,
      ngxNavigate: INgxNavigateService
    ) => new RedirectorCtrl($stateParams, redirectorService, ngxNavigate)
  )
  .controller('fullLoginCtrl', function(
    request,
    $state: StateService,
    member,
    memberLoader: MemberLoader,
    formHelper,
    $stateParams,
    $window,
    redirectorService,
    goBack: IGoBack
  ) {
    return new FullLoginCtrl(
      $state,
      memberLoader,
      member,
      request,
      formHelper,
      $stateParams,
      $window,
      redirectorService,
      goBack
    );
  })
  .controller('homeCtrl', function(
    $scope,
    homeData,
    $state,
    $stateParams,
    locale,
    member,
    request,
    $filter
  ) {
    return new HomeCtrl($scope, homeData, $state, $stateParams, locale, member, request, $filter);
  })
  .controller('ideasCacheJobCtrl', function(confirmerDlg, infoDlg, endpoint, goHome) {
    return new IdeasCacheJobCtrl(confirmerDlg, infoDlg, endpoint, goHome);
  })
  .controller('newsletterSubscribeCtrl', function(endpoint, $window) {
    return new NewsletterSubscribeCtrl(endpoint, $window);
  })
  .service('analyticsLoader', function() {
    return new AnalyticsLoader();
  })
  .service('analyticsNotifier', function($window, $transitions, $location) {
    return new AnalyticsNotifier($window, $transitions, $location);
  })
  .service('keepAlive', function($interval: IIntervalService, request) {
    return new KeepAlive($interval, request);
  })
  .service(
    'messageRedirectorService',
    ($transitions, $state, member) => new MessageRedirectorService($transitions, $state, member)
  )
  .service('search', (links: ILinksService) => new Search(links))
  .directive('ngxLink', function($state, ngxNavigate) {
    function link(scope, element, attrs) {
      const href = attrs.ngxLink;
      attrs.$set('href', href);
      element.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $state.go('empty', null, {
          location: false
        });
        ngxNavigate.navigateByUrl(href);
      });
    }

    return {
      link: link
    };
  })
  .run(
    (
      ideasAreaCallback: IdeasAreaCallback,
      areaFactory: AreaFactory,
      messageRedirectorService: MessageRedirectorService,
      cmsModuleRegistry: CmsModuleRegistry
    ) => {
      areaFactory.getInstance(AreaType.DEFAULT).setDefaultCallback(ideasAreaCallback);
      messageRedirectorService.run();

      cmsModuleRegistry.register(ShopResolvedTypesForCms.ShopMainContainer, 'shop');
      cmsModuleRegistry.register(ShopResolvedTypesForCms.ShopCategory, 'shop');
      cmsModuleRegistry.register(ShopResolvedTypesForCms.ShopArticlePage, 'shop');

      cmsModuleRegistry.register(IdeaResolvedTypesForCms.Article, 'ideas');
      cmsModuleRegistry.register(IdeaResolvedTypesForCms.IdeaGroup, 'ideas');
      cmsModuleRegistry.register(IdeaResolvedTypesForCms.IdeaGroupContainer, 'ideas');
      cmsModuleRegistry.register(IdeaResolvedTypesForCms.IdeaGroupCategory, 'ideas');
      cmsModuleRegistry.register(IdeaResolvedTypesForCms.IdeaGroupMainContainer, 'ideas');
    }
  );
