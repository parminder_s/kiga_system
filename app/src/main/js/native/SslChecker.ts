export default class SslChecker {
  static doCheck(window, kiga) {
    let url = window.location.href;
    if (kiga) {
      let requiresSsl = kiga['requiresSsl'];
      if (requiresSsl && !url.match(/^https/)) {
        window.location.href = url.replace(/^http/, 'https');
      }
    }
  }
}
