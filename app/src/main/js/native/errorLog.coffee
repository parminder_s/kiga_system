window.onerror = (msg, file, line, position, err) ->
  console.log 'Error has been trigerred and catched'
  data = msg: msg, file: file, line: line, position: position
  if (err)
    data.stack = err.stack;
  $.ajax
    method: 'post',
    url: '/api/exceptionLogger/notify'
    data: JSON.stringify data
    contentType: 'application/json; charset=utf-8',
    dataType: 'json'

