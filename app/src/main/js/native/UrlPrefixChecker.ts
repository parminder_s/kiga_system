export default class UrlPrefixChecker {
  static doCheck() {
    let loc = window.location.href;
    if (loc.match(/#/) && !loc.match(/#!/)) {
      window.location.href = loc.replace('#', '#!');
    }
  }
}
