import * as $ from 'jquery';

export default class CookieChecker {
  static check() {
    if (!navigator.cookieEnabled) {
      let infoBox = $('<div></div>');
      infoBox.css({
        backgroundColor: 'red',
        textAlign: 'center',
        height: '30px',
        fontSize: '20px',
        marginTop: '45px'
      });
      infoBox.text(window['kiga'].noCookiesMsg);
      infoBox.appendTo('body');
    }
  }
}
