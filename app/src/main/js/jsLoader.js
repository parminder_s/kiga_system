define([
  'require',
  'exports',
  './controllers/container',
  './controllers/loginCtrl',
  './controllers/requestResetPwCtrl',
  './controllers/parallelLogInfoCtrl',
  './controllers/resetPasswordCtrl',
  './directives/scrollHeader'
], function(require, exports) {
  exports.default = true;
});
