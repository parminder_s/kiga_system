import { StateService } from 'angular-ui-router';
import * as _ from 'lodash';
import { IConfigService, ILinksService } from '../../../misc/js/services/AngularInterfaces';
import IMemberService from '../../../misc/permission/js/IMemberService';
import MockedCountryService from '../../../misc/js/services/MockedCountryService';
import ToastService from '../../../misc/toast/ToastService';

export default class MainMenuCtrl {
  menuData: any = {};

  constructor(
    private memberService: IMemberService,
    private links: ILinksService,
    private $state: StateService,
    private mockedCountryService: MockedCountryService,
    private config: IConfigService,
    private endpoint,
    private toastService: ToastService
  ) {}

  $onInit() {
    let userMenu = this.links.findByType('userMenu');
    let menuItems = [];
    if (this.links.findByType('userMenu').isLegacy) {
      menuItems.push({ label: userMenu.label, link: userMenu.url });
    } else {
      menuItems.push({ label: userMenu.label, link: this.$state.href('root.member.account', {}) });
    }

    if (this.memberService.get().isCms) {
      let countries = this.mockedCountryService.countries;
      menuItems = _.concat(menuItems, [
        {
          label: 'Content',
          children: [
            { label: 'Backend', link: '/admin' },
            { label: 'Translations', link: '/admin/translations' },
            {
              label: 'Add Idea (Download type)',
              link: this.$state.href('root.backend.cmsBackend.kiga-idea.add', {
                selectedLocale: 'de'
              })
            },
            { label: 'Refresh IdeasCache', link: this.$state.href('root.ideasCacheJob', {}) },
            {
              label: 'Refresh Search Index',
              newType: true,
              callback: () => {
                this.endpoint('api/searcher/update').then(() => {
                  this.toastService.success('Search Index Update started');
                });
              }
            }
          ]
        },
        {
          label: 'Shop',
          children: [
            { label: 'Purchases', link: this.$state.href('root.backend.shop.list', {}) },
            { label: 'Products', link: this.$state.href('root.backend.shop.products', {}) },
            { label: 'Vouchers', link: this.$state.href('root.backend.shop.vouchers', {}) },
            { label: 'Countries', link: this.$state.href('root.backend.shop.countries', {}) },
            {
              label: 'Dunning',
              link: this.$state.href('root.backend.accounting.dunning.overdue', {})
            },
            {
              label: 'Transactions Online',
              link: this.$state.href('root.backend.accounting.accountingTransactions', {})
            },
            {
              label: 'Import Transactions',
              link: this.$state.href('root.backend.accounting.accountingImport', {})
            }
          ]
        },
        //create url via url service or state (its a angular state)
        { label: 'KGA', link: 'ng6/en/kga/kindergarten/list' },
        { label: 'Newsletter', link: this.$state.href('root.backend.newsletter.list', {}) },
        {
          label: 'Select country',
          children: _(_.toPairs(countries))
            .filter(([code, name]) =>
              _.includes(['at', 'de', 'ch', 'it', 'li', 'uk', 'us', 'ru', 'tr'], code)
            )
            .map(([code, name]) => {
              return {
                label: name,
                code: code,
                newType: true,
                callback: () => {
                  this.onCountrySelect(code);
                }
              };
            })
            .value()
        },
        {
          label: 'Reports',
          children: [
            { label: 'Parallel Logs', link: this.$state.href('root.reports.parallelLog', {}) },
            { label: 'Planungstool', link: this.$state.href('root.reports.planner', {}) },
            {
              label: 'Idea Quality',
              link: this.$state.href('root.reports.validation.report.list', {})
            }
          ]
        }
      ]);
    }

    menuItems = _.concat(menuItems, [
      {
        label: 'NAVIGATION_LOCALE',
        isTranslation: true,
        children: this.getLocales(),
        type: 'locale'
      },
      { label: 'LOGOUT', isTranslation: true, link: '/Security/Logout' }
    ]);

    this.menuData = {
      label: this.links.findByType('userMenu').label,
      icon: 'icon-person',
      menuItems: menuItems
    };
  }

  onCountrySelect(code): void {
    this.mockedCountryService.switchCountry(code);
  }

  private getLocales(): Array<any> {
    return _.map(this.config.get('locales'), (locale: string) => {
      return { code: locale, title: 'MISC_LOCALE_' + locale.toUpperCase() };
    });
  }
}
