import { ILinksService } from '../../../misc/js/services/AngularInterfaces';
import CookieInfoService from '../../../misc/js/services/CookieInfoService';
import { AreaType } from '../../../area/js/AreaType';

export default class FooterController {
  private year;
  private cookieAccepted;
  private $rootScope;

  public constructor(
    private links: ILinksService,
    private cookieInfoService: CookieInfoService,
    private $state,
    $rootScope
  ) {
    this.$rootScope = $rootScope;
  }

  $onInit() {
    this.year = new Date().getFullYear();
    this.cookieAccepted = this.cookieInfoService.getClassNameForFooter();
    this.$rootScope.$on(
      'agreedToCookieUse',
      () => (this.cookieAccepted = this.cookieInfoService.getClassNameForFooter())
    );
  }

  checkAreaNone() {
    return this.$state.params['area'] == AreaType.NONE;
  }
}
