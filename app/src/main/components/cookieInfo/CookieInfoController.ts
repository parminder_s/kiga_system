import CookieInfoService from '../../../misc/js/services/CookieInfoService';

export default class CookieInfoController {
  public constructor(private links, private cookieInfoService: CookieInfoService) {}

  public $onInit() {
    this.cookieInfoService.checkIfCookieIsContained();
  }

  public buttonClicked() {
    this.cookieInfoService.agreedCookieUse = true;
    this.cookieInfoService.setClassNameForFooterToEmptyString();
    let expiration_date = new Date();
    expiration_date.setFullYear(expiration_date.getFullYear() + 10);
    document.cookie = 'agreedCookieUse=true; expires=' + expiration_date.toUTCString() + ';path=/';
    this.cookieInfoService.emitAgreementOfCookieUsage();
  }
}
