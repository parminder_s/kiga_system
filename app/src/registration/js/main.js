define([
  'require',
  'exports',
  'angular',
  './app',
  './controllers/regInfoCtrl',
  './controllers/regPreselectionCtrl',
  './controllers/regProductCtrl',
  './controllers/regCustomerDataCtrl',
  './controllers/regPaymentCtrl',
  './controllers/regResumeCtrl',
  './controllers/regSummaryCtrl',
  './controllers/regGiftActivationCtrl',
  './directives/headerEntry',
  './directives/toInteger',
  './directives/validBirthdate',
  './service/model',
  './service/regDataViewModel',
  './service/regHandlerService',
  './service/regProductHandlerService',
  './service/regScopeService',
  './service/validBirthdateService'
], function(require, exports, angular, app) {
  exports.default = true;
  return app;
});
