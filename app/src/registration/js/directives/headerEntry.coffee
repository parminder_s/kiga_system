###
  Simple header entry

  Precondition: isStepActive function exists in parent scope
###

define ['../app'], (app) ->
  app.directive 'headerEntry', ['$log', () ->
    restrict: 'E'
    scope: {href:'@', step:'@', translateKey:'@'}
    templateUrl: 'registration/partials/directives/headerEntry.html'
    controller: ['$scope','$log', ($scope) ->
      $scope.isFirstStep = $scope.$parent.isFirstStep
      $scope.isStepActive = $scope.$parent.isStepActive
      $scope.activateStep = $scope.$parent.activateStep

      $scope.isCurrentStepActive = ->
        $scope.isStepActive and  $scope.isStepActive $scope.step
    ]
  ]
