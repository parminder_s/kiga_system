###*
  @ngdoc directive
  @name validBirthdate
  @module registration

  @description
  validates the birthade, if user is older than minAge
###

define ['../app'], (app) ->
  app.directive 'validBirthday',(validBirthdateService) ->
    require: 'ngModel'
    restrict: 'A'
    scope:
      minAge: '@minAge'
    link: (scope, el, atts, ngModel) ->
      ngModel.$validators.validBirthdate = (modelValue) ->
        if modelValue
          validBirthdateService.checkAge modelValue, scope.minAge
        else false
