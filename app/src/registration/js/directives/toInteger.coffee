define ['../app'], (app) ->
  app.directive "toInteger", ->
    require: "ngModel"
    link: (scope, ele, attr, ctrl) ->
      ctrl.$parsers.unshift (viewValue) ->
        parseInt viewValue, 10

      return
