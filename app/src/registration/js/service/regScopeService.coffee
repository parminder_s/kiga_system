###*
  @ngdoc service
  @name regScopService
  @module registration
  @description

  Is responsible for enhancing a controllers scope with function references
  The goal is to restrict functionality of a controller because of better testing capabilities of services
###

define ['../app'], (app) ->
  app.service 'registration.regScopeService',
  ['$log', '$stateParams', '$locale','registration.model', 'registration.regHandlerService', 'registration.regProductHandlerService',
   'registration.regDataViewModel'].
  concat ($log, $stateParams, $locale, model, regHandlerService, regProductHandlerService, regDataViewModel) ->
    (scope) ->
      scope.activateStep =  regHandlerService.activateStep
      scope.getData =  regDataViewModel.getData
      scope.getFieldObject = regDataViewModel.getFieldObject
      scope.getFieldValue = regDataViewModel.getFieldValue
      scope.getProductGroup = regDataViewModel.getProductGroup
      scope.getProductGroupInfo = regProductHandlerService.getProductGroup
      scope.getRegData =  regDataViewModel.getRegData
      scope.getSelectedProductId =  regDataViewModel.getSelectedProductId
      scope.getSelectedProduct = regDataViewModel.getSelectedProduct
      scope.getSelectedCountry = regDataViewModel.getSelectedCountry
      scope.isFirstStep = regDataViewModel.isFirstStep
      scope.isLicenseAbo = regDataViewModel.isLicenseAbo
      scope.isParentsAbo = regDataViewModel.isParentsAbo
      scope.isStandardAbo = regDataViewModel.isStandardAbo
      scope.isStandardOrgAbo = regDataViewModel.isStandardOrgAbo
      scope.isGiftAbo = regDataViewModel.isGiftAbo
      scope.isTestAbo = regDataViewModel.isTestAbo
      scope.isStepActive = regDataViewModel.isStepActive
      scope.isValidationInfoRequired = regDataViewModel.isValidationInfoRequired
      scope.moveToPreviousStep = regHandlerService.moveToPreviousStep
      scope.setFieldValue = regDataViewModel.setFieldValue
      scope.locale = $stateParams.locale
