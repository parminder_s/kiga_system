define ['../app', 'lodash'], (app, _) ->

  ###
    Registration handling services
  ###

  app.service 'registration.regHandlerService',
  ['$log', '$state', '$window', 'registration.model', 'registration.regDataViewModel',
   'registration.regProductHandlerService', 'errorMsg',
  ($log, $state, $window, model, regDataViewModel, regProductHandlerService, errorMsg) ->

    class RegHandlerService
      activateStep: (stepName) =>
        regDataViewModel.setStepName stepName
        @gotoState @getStateForStepName(stepName)

      backToPreselection: => @gotoState 'root.registration.preSelection'

      buyNow: =>
        if regDataViewModel.getFieldValue("paymentMethod") is 'GARANTI'
          model.registrationFinishEndpoint regDataViewModel.getRegDataForEndpoint()
        else
          model.registrationFinish regDataViewModel.getRegData(), (data) =>
            if regDataViewModel.isGiftAbo()
              @executeEndOfProcess data, 'gift'
            else
              @executeEndOfProcess data

      customerData: (form) =>
        @handleFormValidationAndValidate form

      checkStepName: (stepName) =>
        # check if stepName is already active
        if not regDataViewModel.isStepActive(stepName)
          $state.go 'root.registration.preSelection'
          regDataViewModel.setStepName(regDataViewModel.getData().stepNames[0]);
          return false
        else
          #ensure correct setting of stepName (also when navigation via browser back button)
          if regDataViewModel.getStepName() != stepName
            regDataViewModel.setStepName stepName
          return true

      executeEndOfProcess: (data, subscrGroup) =>
        if data?.redirect?
          $window.location.href = data.redirect
        else
          if subscrGroup is 'gift'
            $state.go 'root.member.welcome.gift'
          else
            url = $state.href 'root.member.welcome.ordered'
            $window.location.href = url

      getStateForStepName: (stepName) => switch stepName
        when 'productFields' then 'root.registration.product'
        when 'customerDataFields' then 'root.registration.customerData'
        when 'paymentFields' then 'root.registration.payment'
        when 'summary' then 'root.registration.summary'
        else 'root.registration.preSelection' # e.g. 'preSelectionFields' or undefined

      handleFormValidationAndValidate: (form) =>
        if form? and form.$invalid
          regDataViewModel.setValidationInfoRequired true
        else
          model.registrationValidate regDataViewModel.getRegData(), @handleServerResult

      handleServerResult : (data) =>
        nextState = @getStateForStepName(data.stepName)
        regDataViewModel.setRegData data

        @gotoState nextState

      gotoState : (state) =>
        regIdObject = @getRegIdObject regDataViewModel.getRegId()
        $state.go state, regIdObject

      getRegIdObject: (regId) =>
        if regId? then 'id': regId else {}

      giftActivation: (form) =>
        if form? and form.$invalid
          regDataViewModel.setValidationInfoRequired true
        else
          model.giftActivation regDataViewModel.getRegData(), @executeEndOfProcess

      info: (productGroup) =>
        regDataViewModel.setProductGroup productGroup
        @gotoState 'root.registration.info'

      moveToPreviousStep: =>
        prevStepName = regDataViewModel.getPrevStepName()
        regDataViewModel.setStepName prevStepName
        @gotoState @getStateForStepName(prevStepName)

      order: (productGroup) =>
        if productGroup?
          regDataViewModel.setProductGroup productGroup

        if regDataViewModel.isTestAbo() || productGroup is 'test'
          $state.go 'root.permission.testRequired'
        else
          model.registrationValidate regDataViewModel.getRegData(), @handleServerResult

      payment: (form) => @handleFormValidationAndValidate form

      productGroupSelection: (productGroup) =>
        regDataViewModel.setProductGroup productGroup
        model.registrationValidate regDataViewModel.getRegData(), @handleServerResult

      summary : (form) => @handleFormValidationAndValidate form

    new RegHandlerService()
  ]
