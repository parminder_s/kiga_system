###
Handle data of registration process

Holds a data object (@data) which contains relevant data for registration
###

define ['../app', 'lodash', 'moment'], (app, _, moment) ->
  app.service 'registration.regDataViewModel', ($log) ->
    new class RegDataViewModel
      data:
        stepNames:['preSelectionFields','productFields','customerDataFields','paymentFields','summary']
        regData: fields:[]
        student: false
        validationInfoRequired: false
        products:
          standard: []
          student: []

      calcAge : (birthDate, currentDate) =>
        currentDate ?= new Date()
        years = currentDate.getFullYear() - birthDate.getFullYear()
        months = currentDate.getMonth() - birthDate.getMonth()
        if months < 0 or (months is 0 && currentDate.getDate() < birthDate.getDate()) then years--
        years


      # returns cheapest product (without products with price 0 -> which are license abo products which need price request)
      getCheapestProduct: =>
        @getProducts().reduce (a,b) -> if a.price > 0 and a.price > b.price and b.price > 0 then b else a

      getCheapestLicenseProducts: =>
        shortestDurationProduct = @getProducts().reduce (a,b) -> if a.duration < b.duration then a else b
        @getData().products.durationGroups[shortestDurationProduct.duration]



      getFieldValue: (fieldName) ->
        try
          fieldObject = @getFieldObject fieldName
#          $log.info("getFieldValue for #{fieldName} - fieldObject",fieldObject)
          fieldObject?.value
        catch error
          $log.error 'getFieldValue', error

      getFieldObject: (fieldName) =>
        field = (field for field in @getRegData().fields when field.name is fieldName)[0]
        if not field?
          field = {name: fieldName}
          @getRegData().fields.push field
        field

      getData: => @data;

      getGroupPaymentOptions: =>
        if not @data.groupPaymentOptions?
          paymentOptions = []
          for product in @getProducts()
            paymentOptions = paymentOptions.concat product.paymentOptions
          @data.groupPaymentOptions = _.uniq(paymentOptions)
        else
          @data.groupPaymentOptions


      getLicensesArray: => (''+x for x in [1..@getSelectedProduct()?.maxLicences])

      getPrevStepName: =>
        indexOfPrev = @data.stepNames.indexOf @getStepName()
        @getStepNameByIndex indexOfPrev - 1

      getProductGroup: => @getFieldObject 'productGroup'

      getProducts: => if not @data.student then @data.products.standard else @data.products.student

      getRegData: => @data.regData;

      getRegDataForEndpoint: ->
        returner = {}
        for field in @data.regData.fields
          returner[field.name] = field.value
        returner.registrationId = @data.regData.id
        returner

      getRegId: => @getRegData().id

      getSelectedCountry: =>
        countryField = @getFieldObject 'country'
        (country for country in countryField.values when country.key is countryField.value)?[0]

      getSelectedProductId: => @getFieldValue 'productId'

      getSelectedProduct: => (product for product in @getProducts() when product.id is @getSelectedProductId())?[0]

      getStepNameByIndex: (index) =>
        if index >= 0 &&  index <= (@data.stepNames.length-1)
          @data.stepNames[index]
        else
          @data.stepNames[0]

      getStepName: => @getRegData().stepName

      hasFieldValue: (fieldName) -> @getFieldObject(fieldName).value?

      isFirstStep: (stepName) => @data.stepNames[0] is stepName

      isInitialized: => @getRegData().fields?.length > 0 ? false

      isLicenseAbo: => @getProductGroup().value is 'licence'

      isParentsAbo: => @getProductGroup().value is 'parents'

      isGiftAbo: => @getProductGroup().value is 'gift'

      isTestAbo: => @getProductGroup().value is 'test'

      isProductSelected: (product) => @getFieldValue('productId') is product.id

      isProductMinimumAgeReached: (birthDateAsString) =>
        @isProductMinimumAgeReachedForAge birthDateAsString, @getSelectedProduct().minimumAge

      isProductMinimumAgeReachedForAge: (birthDateAsString, minimumAge) =>
        if birthDateAsString?
          birthDate =  moment birthDateAsString, 'YYYY-MM-DD'
          @calcAge(birthDate.toDate()) >= minimumAge
        else
          false

      isStandardAbo: => @getProductGroup().value is 'standard'

      isStandardOrgAbo: => @getProductGroup().value is 'standard-org'

      isStepActive: (step) => @data.stepNames.indexOf(step) <= @data.stepNames.indexOf(@getStepName())

      isValidationInfoRequired: => @data.validationInfoRequired

      setDefaultProductIfRequired: =>
        if not @getSelectedProductId()?
          # take first product because it is sorted by duration desc = product with max durance
          @setFieldValue 'productId', @getProducts()[0].id

      setDefaultProductForLicenseAboDuration : (duration) =>
        product = @data.products.durationGroups[duration][0] #first product of duration group
        @setFieldValue 'productId', product.id

      setFieldValue: (fieldName, value) -> @getFieldObject(fieldName).value = value

      setFieldValueWhenEmpty: (fieldName, value) -> @getFieldObject(fieldName).value ?= value

      setGroupPaymentOptions: (options) -> @getData.groupPaymentOptions = options

      setRegData: (regData) ->
        @setValidationInfoRequired false
        @data.regData = regData

      setValidationInfoRequired: (required) => @data.validationInfoRequired = required

      selectProduct: (product) => @setFieldValue 'productId', product.id

      setProducts : (data) -> @data.products = data.products

      setProductGroup: (productGroup) ->
        if @getProductGroup().value isnt productGroup
          @setFieldValue 'productId', undefined #reset selected productId when group changes
          @data.student = false

        @setFieldValue 'productGroup', productGroup
        @data.groupPaymentOptions = undefined


      setStepName : (stepName) -> @getRegData().stepName = stepName

      studentSelectionChanged: => @selectProduct @getProducts()[0]
