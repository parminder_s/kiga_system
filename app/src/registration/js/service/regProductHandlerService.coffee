define ['../app', 'lodash'], (app, _) ->

  ###
  Product handling functions
  ###

  app.service 'registration.regProductHandlerService', ->
    class RegProductHandlerService

      data: {
        productGroups: [
          { productGroup: 'free', trans: 'REG_FREE_SUBSCRIPTION' }
          { productGroup: 'standard', trans: 'REG_EDUCATIONIST_SUBSCRIPTION' }
          { productGroup: 'standard-org', trans: 'REG_INSTITUTION_SUBSCRIPTION' }
          { productGroup: 'parents', trans: 'REG_PARENTS_SUBSCRIPTION' }
          { productGroup: 'gift', trans: 'REG_GIFT_SUBSCRIPTION' }
        ]
          # { productGroup: 'licence', trans: 'REG_LICENSE_SUBSCRIPTION' }
      }

      getProductsForCustomerType = (products, customerType) ->
        result = (product for product in products when product.customerType is customerType)

      getStandardProducts : (products) =>  getProductsForCustomerType products, 'standard'

      getStudentProducts : (products) =>  getProductsForCustomerType products, 'student'

      getProductGroup: (productGroupId) => (productGroup for productGroup in @data.productGroups when productGroup.productGroup is productGroupId)?[0]

      getProductGroups: => @data.productGroups

      hasProductPaymentOption: (product, paymentOption) => @hasPaymentOption product.paymentOptions, paymentOption

      hasPaymentOption: (options, paymentOption) => options.indexOf(paymentOption) > -1

      # Create objects for different customer types
      createProductsObject: (productGroup, products) ->
        if products?
          switch productGroup
            when 'licence'
              products:
                standard: products
                durationGroups: @createLicenseDurationGroups(products)
            when 'standard'
              products:
                standard: @getStandardProducts products
                student: @getStudentProducts products
            else
              products:
                standard: products
        else []

      createLicenseDurationGroups: (products) ->
        groups={}
        products.forEach (product) ->
          group = groups[product.duration] ?= []
          group.push product
        groups

    new RegProductHandlerService()



