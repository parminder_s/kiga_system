define ['../app', 'jquery'], (app, $) ->
  ###
    Model abstracts requests
  ###

  app.service 'registration.model',
  ['$log', 'request', 'registration.regProductHandlerService', 'registration.regDataViewModel', 'endpoint'].
  concat ($log, request, regProductHandlerService, regDataViewModel, endpoint) ->
    new class Model
      getProducts: (country, productGroup) ->
        if not (country? and productGroup?)
          throw 'Model.getProducts - IllegalArguments needs country and productGroup'
        else
          request
            url: 'nregistration/subscriptions'
            data:
              country: country
              productGroup: productGroup
          .then (products) ->
            if products? then products.sort((p1,p2) -> p2.duration - p1.duration) # sort by duration desc
          .then (products) ->
            if products? then regProductHandlerService.createProductsObject productGroup, products
          .catch (error) -> $log.error 'Model.getProducts', error

      registrationInitiate: ->
        request url: "nregistration/initiate"

      registrationValidate: (data, cb) ->
        request {
          url: 'nregistration/validate'
          data: data
        }, cb

      registrationResume: (data, successFunc) ->
        regData = undefined
        request
          url: "nregistration/resume"
          data: data
        .then (data) =>
          #After data available we have to load products for country and productGroup
          regData = data
          regDataViewModel.setRegData data
          @getProducts (regDataViewModel.getFieldValue 'country'), (regDataViewModel.getFieldValue 'productGroup')
        .then (products) ->
          #Products loaded
          regDataViewModel.setProducts products
          regData
        .then successFunc

      registrationResumeDefault: (data, cb) ->
        request {
          url: 'nregistration/resume'
          data: data
        }, cb

      registrationFinish: (data, cb) ->
        request {
          url: "nregistration/finish"
          data: data
        }, cb

      registrationFinishEndpoint: (data, cb) ->
        endpoint {
          url: "registration/finish"
          data: data
        }, cb

      activateGift:  (data, cb) ->
        request {
          url: 'nregistration/giftActivation'
          data: data
        }, cb
