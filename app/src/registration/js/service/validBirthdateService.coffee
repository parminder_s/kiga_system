define ['../app', 'angular', 'moment'], (app, angular, moment) ->
  app.service 'validBirthdateService', (now)  ->
    checkAge: (modelValue, minAge) ->
      now().diff(moment(modelValue), 'years') >= minAge
