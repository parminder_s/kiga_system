define ['../app', 'angular'], (app, angular) ->
  app.controller 'registration.regPreselectionCtrl',  ['$scope', '$log',
  'registration.regScopeService', 'registration.regDataViewModel',
  'registration.regHandlerService', 'registration.regProductHandlerService',
  'regData', 'config', '$state'].
  concat ($scope, $log, regScopeService, regDataViewModel,
  regHandlerService, regProductHandlerService, regData, config, $state) ->
    regDataViewModel.setRegData regData
    if regHandlerService.checkStepName 'preSelectionFields'
      regScopeService $scope

      angular.extend $scope,
        getProductGroups: regProductHandlerService.getProductGroups
        nextStepTried: false
        onCountrySelect: () -> $scope.countryErrorRequired = false
        licenceEnabled: -> config.get 'licenceEnabled'
        allowCountrySelection: -> config.get 'allowCountrySelection'
        getSelectedCountry: ->
          countryField = $scope.getFieldObject 'country'
          _.find(countryField.values, (country) ->
            country.code is countryField.value).title

        order: (group) ->
          if $scope.getFieldValue('country')
            regHandlerService.order(group)
          else
            $scope.countryErrorRequired = true;
        info: (group) ->
          if $scope.getFieldValue('country')
            regHandlerService.info(group)
          else
            $scope.countryErrorRequired = true;
