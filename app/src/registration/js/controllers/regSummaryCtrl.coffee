define ['../app', 'angular'], (app, angular) ->
  app.controller 'registration.regSummaryCtrl', ['$scope', '$log', '$state',
  'registration.regScopeService', 'registration.regDataViewModel',
  'registration.regHandlerService', 'registration.regProductHandlerService','$locale'].
  concat ($scope, $log, $state, regScopeService, regDataViewModel, regHandlerService, regProductHandlerService,$locale) ->
    if regHandlerService.checkStepName 'summary'
      regScopeService $scope
      angular.extend $scope,
        buyNow: regHandlerService.buyNow
        getProductGroup: regProductHandlerService.getProductGroup
        getSelectedCountry: regDataViewModel.getSelectedCountry
        dateFormatBirthday: $locale.DATETIME_FORMATS.shortDate
