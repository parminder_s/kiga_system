define ['../app'], (app) ->
  regProductController = ($scope, $log, $state, regScopeService, regDataViewModel,
  regHandlerService, regProductHandlerService, products, $filter) ->
    # $log.info 'RegProductController -> products', @products

    #TODO state handling abstraction
    if not regDataViewModel.isStepActive('productFields')
      $state.go 'root.registration.preSelection'
      return

    regDataViewModel.setProducts products

    regScopeService $scope

    if regHandlerService.checkStepName 'productFields'
      $scope.getLicensesArray = regDataViewModel.getLicensesArray
      $scope.getProducts = regDataViewModel.getProducts
      $scope.isProductSelected = regDataViewModel.isProductSelected
      $scope.selectProduct = regDataViewModel.selectProduct
      $scope.setDefaultProductForLicenseAboDuration = regDataViewModel.setDefaultProductForLicenseAboDuration
      $scope.studentSelectionChanged = regDataViewModel.studentSelectionChanged
      $scope.proceed = => regHandlerService.customerData $scope.productForm
      $scope.licencePrice = ->
        product = $scope.getSelectedProduct()
        if product
          $filter('number')(product.licencePrice, 2) + ' ' + product.currency
        else ''

      regDataViewModel.setDefaultProductIfRequired()


  app.controller 'registration.regProductCtrl',  ['$scope', '$log', '$state',
                                           'registration.regScopeService',
                                           'registration.regDataViewModel',
                                           'registration.regHandlerService',
                                           'registration.regProductHandlerService',
                                           'products', '$filter', regProductController]



