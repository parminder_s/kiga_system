define ['../app'], (app) ->
  app.controller 'registration.regCustomerDataCtrl',  ['$scope', '$state', '$log', 'registration.model',
  'registration.regScopeService', 'registration.regDataViewModel', 'registration.regHandlerService', 'config'].
  concat ($scope, $state, $log, model, regScopeService, regDataViewModel, regHandlerService, config) ->
    regScopeService $scope
    # setting state to AreaType.NONE (4)
    $state.params['area'] = 4;
    if regHandlerService.checkStepName 'customerDataFields'
      $scope.model = model
      $scope.isProductMinimumAgeReached = regDataViewModel.isProductMinimumAgeReached
      $scope.payment = regHandlerService.payment
