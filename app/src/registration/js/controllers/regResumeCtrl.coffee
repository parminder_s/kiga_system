# Registration resume - forwards to registration process step based on code

app = define ['../app'], (app) ->
  app.controller 'registration.regResumeCtrl',  ['$scope', '$state' ,'$stateParams',
  '$log', 'registration.regScopeService', 'registration.regDataViewModel',
  'regData'].
  concat ($scope, $state, $stateParams, $log, regScopeService, regDataViewModel, regData) ->
    regDataViewModel.setRegData regData

    if $stateParams.code is 'paymentCancel' or $stateParams.code is 'paymentFailure'
      regDataViewModel.setStepName 'paymentFields'
      $state.go 'root.registration.payment', id: $stateParams.id, code: $stateParams.code
    else
      $state.go 'root.registration.preSelection', $stateParams.id
