app = define ['../app'], (app) ->

  regPaymentController = ($scope, $log, $state, $stateParams, regScopeService, regDataViewModel,
                          regHandlerService, regProductHandlerService, links) ->

    # $log.info 'RegPaymentCtrl'

    $scope.linkURL = links.findByCode('dataPrivacy').url;
    # set resumeCode to present a specific message on the view
    $scope.resumeCode = $stateParams.code if $stateParams.code is 'paymentCancel' or $stateParams.code is 'paymentFailure'

    if regHandlerService.checkStepName 'paymentFields'
      regScopeService $scope

      $scope.hasProductPaymentOption = regProductHandlerService.hasProductPaymentOption
      $scope.summary = regHandlerService.summary


      # check if paymentMethod fits to product (It might not fit when product selection changes which differs in payment methods)
      if not $scope.hasProductPaymentOption($scope.getSelectedProduct(),$scope.getFieldObject('paymentMethod').value)
        $scope.setFieldValue 'paymentMethod', undefined # set to undefined when doesn't fit


  app.controller 'registration.regPaymentCtrl', ['$scope', '$log', '$state', '$stateParams',
                                                 'registration.regScopeService',
                                                 'registration.regDataViewModel',
                                                 'registration.regHandlerService',
                                                 'registration.regProductHandlerService',
                                                 'links'
                                                 regPaymentController]



