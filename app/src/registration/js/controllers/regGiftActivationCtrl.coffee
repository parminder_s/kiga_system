define ['../app'], (app) ->
  app.controller 'regGiftActivationCtrl', ['country', 'member', 'formHelper',
  'registration.model', '$state'].concat (country, member, formHelper,
  model, $state) ->
    new class RegGiftActivationCtrl
      data:
        email: member.email
        email2: member.email
        firstname: member.firstname
        lastname: member.lastname
        gender: member.gender ? 'female'
        newsletter: false
        country: country.code
      countryTitle: country.title
      cancel: formHelper
      submit: -> model.activateGift @data, -> $state.go 'root.member.welcome.orderedGift'
