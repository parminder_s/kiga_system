define ['../app'], (app) ->
  regInfoController = ($scope, $log, $state, regScopeService, regDataViewModel,
                       regHandlerService, regProductHandlerService, products, config, locale) ->
    $log.info 'RegInfoCtrl'

    if not regDataViewModel.isInitialized()
      $state.go 'root.registration.preSelection'
      return

    regDataViewModel.setProducts(products)

    regScopeService $scope

    $scope.backToPreselection = regHandlerService.backToPreselection
    $scope.getCheapestProduct = regDataViewModel.getCheapestProduct
    $scope.getCheapestLicenseProducts = regDataViewModel.getCheapestLicenseProducts
    $scope.getGroupPaymentOptions = regDataViewModel.getGroupPaymentOptions
    $scope.getProducts = regDataViewModel.getProducts
    $scope.hasPaymentOption = regProductHandlerService.hasPaymentOption
    $scope.order = regHandlerService.order
    $scope.showInfo = -> config.get('regShowInfo')
    $scope.isFullTest = -> config.getPermissionMode() == 'fullTest'
    $scope.showInfoGift = -> config.get('regShowInfoGift')
    $scope.showInfoParent = -> config.get('regShowInfoParent')
    $scope.showInfoTeacher = -> config.get('regShowInfoTeacher')
    $scope.showInfoStandardOrg = -> config.get('regShowInfoStandardOrg')
    $scope.showInfoTest = -> true
    $scope.imgSmall = -> "registration/images/#{$scope.getProductGroup().value}_small_#{locale.get()}.jpg"
    $scope.imgLarge = -> "registration/images/#{$scope.getProductGroup().value}_large_#{locale.get()}.jpg"

  app.controller 'registration.regInfoCtrl', ['$scope', '$log', '$state',
                                 'registration.regScopeService',
                                 'registration.regDataViewModel',
                                 'registration.regHandlerService',
                                 'registration.regProductHandlerService',
                                 'products', 'config', 'locale', regInfoController]

