import { IConfigService } from '../../misc/js/services/AngularInterfaces';
import { app } from './app';
import { SearchService } from './service/searchService';
import { SearchRequest } from './requests/searchRequest';
import ITimeoutService = angular.ITimeoutService;
import IDocumentService = duScroll.IDocumentService;
import SearchCtrl from './controller/SearchCtrl';
import IdeaLinksResolver from '../../ideas/js/services/IdeaLinksResolver';
import { AreaType } from '../../area/js/AreaType';
import { FavouritesModel } from '../../misc/js/model/FavouritesModel';
import IMember from '../../misc/permission/js/IMember';
import IMemberService from '../../misc/permission/js/IMemberService';

app
  .service('searchService', function(endpoint) {
    return new SearchService(endpoint);
  })
  .controller('searchCtrl', function(
    $stateParams,
    areaFetcher,
    $state,
    $timeout: ITimeoutService,
    $document: IDocumentService,
    ideaLinksResolver: IdeaLinksResolver,
    favouritesModel: FavouritesModel,
    config: IConfigService,
    member: IMemberService,
    $window,
    $translate,
    scrollUpService,
    endpoint
  ) {
    return new SearchCtrl(
      $stateParams,
      areaFetcher,
      $state,
      $document,
      $timeout,
      ideaLinksResolver,
      favouritesModel,
      config,
      $window,
      member,
      $translate,
      scrollUpService,
      endpoint
    );
  });
