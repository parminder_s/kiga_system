export class SearchRequest {
  private prettyPrint: Boolean;
  private filters: String;
  private draft: Boolean;
  private pageSize: Number;

  constructor(
    private query: String[],
    private ageGroup: number,
    private parentId: number = 0,
    private page: Number = 1
  ) {
    this.prettyPrint = true;
    this.filters = '';
    this.draft = true;
    this.pageSize = 300;
  }
}
