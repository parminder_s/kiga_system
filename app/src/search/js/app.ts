import * as angular from 'angular';
import * as _misc from 'misc';

let misc = _misc;

export let app = angular.module('search', ['misc']);
