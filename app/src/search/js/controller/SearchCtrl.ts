import AreaFetcher from '../../../area/js/AreaFetcher';
import * as angular from 'angular';
import * as _ from 'lodash';
import { IConfigService } from '../../../misc/js/services/AngularInterfaces';
import url from '../../../url';
import ITimeoutService = angular.ITimeoutService;
import IDocumentService = duScroll.IDocumentService;
import IdeaLinksResolver from '../../../ideas/js/services/IdeaLinksResolver';
import { FavouritesModel } from '../../../misc/js/model/FavouritesModel';
import IMemberService from '../../../misc/permission/js/IMemberService';
import DownloadablePreviewIcon from '../../../misc/js/services/DownloadablePreviewIcon';
import IMember from '../../../misc/permission/js/IMember';
import { StateParams } from 'angular-ui-router';
import { SearchRequest } from '../requests/searchRequest';

export default class SearchCtrl {
  private ideaCategoryIndex: number;
  private ideaIndex: number;
  private ideaObject;
  private idea;
  private activeCategory;
  private activePreviewPage;
  private selectedHierarchyLevel;
  private selectedHierarchy;
  private maxIdeasPerRow = 20;
  private breadcrumbs = null;
  private ideaLinks = null;
  private query;
  private ageGroup = 0;
  private parentId = 0;
  private member: IMember = null;
  private showVideo;
  private page: number = 1;
  private ideas: any[];
  private parentName;
  private filteredCategories;
  private previewImageName;
  private breadcrumbsTitle: string;
  private feed: any = { categories: [] };

  private downloadablePreviewIconService: DownloadablePreviewIcon;
  constructor(
    private stateParams: StateParams,
    areaFetcher: AreaFetcher,
    private $state,
    private $document: IDocumentService,
    private $timeout: ITimeoutService,
    private ideaLinksResolver: IdeaLinksResolver,
    private favouritesModel: FavouritesModel,
    private config: IConfigService,
    private $window,
    memberService: IMemberService,
    private $translate,
    private scrollUpService,
    private endpoint
  ) {
    let searchQuery = stateParams.query;
    if (!angular.isArray(searchQuery)) {
      searchQuery = [searchQuery];
    }

    endpoint({
      url: 'api/searcher/searchGrid',
      data: new SearchRequest(
        searchQuery,
        stateParams.ageGroup,
        stateParams.parent,
        stateParams.page
      )
    }).then(searchResult => {
      this.feed = searchResult;
      this.downloadablePreviewIconService = new DownloadablePreviewIcon();
      if (this.stateParams['page']) {
        this.page = this.stateParams['page'];
      } else {
        this.page = 1;
      }
      this.ideas = _(this.feed.categories)
        .map(category => category.ideas)
        .flatten<any>()
        .sortBy(idea => idea.elasticScore)
        .reverse()
        .uniqBy(idea => idea.id)
        .value();
      if (this.isWithSearchResults()) {
        this.filteredCategories =
          this.feed.categories.length === 1 ? [] : (this.filteredCategories = this.feed.categories);
        this.parentName =
          this.feed.path.length === 1
            ? this.$translate.instant('SEARCH_ALL_HITS')
            : this.feed.path[this.feed.path.length - 2];
        this.activeCategory = searchResult.activeCategory;
        this.selectedHierarchy = searchResult.path;
        this.breadcrumbs = _.map(searchResult.path, label => ({ label: label }));
      }

      this.member = memberService.get();

      this.query = this.stateParams['query'];
      if (!angular.isArray(this.query)) {
        this.query = [this.query];
      }
      this.ageGroup = this.stateParams['ageGroup'];
      this.parentId = this.stateParams['parent'];

      let area = areaFetcher
        .getArea()
        .withSearchTags(this.query)
        .withAgeGroup(this.ageGroup);

      this.breadcrumbsTitle = this.getBreadcrumbsTitle();
      this.init();
    });
  }

  isWithSearchResults() {
    return this.feed.categories.length > 0;
  }

  isAllowedMember() {
    return !this.member.hasExpiredSubscription() && this.member.hasStandardSubscription();
  }

  init() {
    this.scrollUpService.scroll();
    this.updateScrollers();

    if (this.isAllowedMember()) {
      this.favouritesModel.listFavouritesForIdsAsPromise().then(data => {
        let ids = _.map(data.favourites, 'ideaId');
        for (let category of this.feed.categories) {
          for (let idea of category.ideas) {
            idea['favourite'] = _.includes(ids, idea.id);
          }
        }
      });
    }
  }

  getStars() {
    let rating = 0;
    let stars = [];
    if (this.ideaObject != null) {
      rating = this.ideaObject.rating.rating;
    }

    for (let i = 0; i < 5; i++) {
      if (rating <= i) {
        stars.push('fa-star-o');
      } else {
        if (rating === i + 0.5) {
          stars.push('fa-star-half-o');
        } else {
          stars.push('fa-star');
        }
      }
    }
    return stars;
  }

  url(part: string) {
    return url(part);
  }

  isLastNavigationLevel() {
    return this.feed.activeCategory != null && this.feed.activeCategory.lastCategoryLevel;
  }

  onIdeaClick(categoryIndex: number, idx: number) {
    this.scrollToPreview(categoryIndex);
    if (categoryIndex === this.ideaCategoryIndex && idx === this.ideaIndex) {
      this.closePreview(true);
      return;
    }
    this.openPreview(categoryIndex, idx);
  }

  onGridIdeaClick(index: number) {
    if (index === this.ideaIndex) {
      this.closePreview(false);
    } else {
      this.openPreviewByIndex(index);
    }
  }

  showPreviewPanel(ideasPerRow: number, idx: number) {
    let panelRow = Math.floor(idx / ideasPerRow);
    return (
      this.ideaIndex !== null &&
      ((idx + 1) % ideasPerRow === 0 || idx === this.ideas.length - 1) &&
      Math.floor(this.ideaIndex / ideasPerRow) === panelRow
    );
  }

  onBreadcrumbClick(index: number) {
    this.$state.go(
      '.',
      {
        parent: this.feed.pathIds[index + 1],
        query: this.query,
        ageGroup: this.ageGroup
      },
      { reload: true }
    );
  }

  onBackToParent() {
    let parentId =
      this.feed.pathIds.length > 1 ? this.feed.pathIds[this.feed.pathIds.length - 2] : 0;
    this.$state.go(
      '.',
      {
        parent: parentId,
        query: this.query,
        ageGroup: this.ageGroup
      },
      { reload: true }
    );
  }

  onTopCategoryClick() {
    this.$state.go(
      '.',
      {
        parent: 0,
        query: this.query,
        ageGroup: this.ageGroup
      },
      { reload: true }
    );
  }

  isPreviewFromCategory(categoryIndex: number) {
    return this.ideaCategoryIndex === categoryIndex;
  }

  onCategoryClick(categoryIndex) {
    this.$state.go(
      '.',
      {
        parent: this.feed.categories[categoryIndex].id,
        query: this.query,
        ageGroup: this.ageGroup,
        page: 1
      },
      { reload: true }
    );
  }

  openPreview(categoryIndex, ideaIndex) {
    this.scrollToPreview(categoryIndex);
    this.ideaCategoryIndex = categoryIndex;
    this.ideaIndex = ideaIndex;
    this.ideaObject = this.feed.categories[categoryIndex].ideas[ideaIndex];
    this.idea = this.ideaObject.name;

    if (this.ideaObject.previewPages.length > 0) {
      this.activePreviewPage = this.ideaObject.previewPages[0];
    } else {
      this.activePreviewPage = null;
    }

    if (this.ideaObject.previewPages.length > 3) {
      this.ideaObject.previewScrollButtonsVisibility = 'visible';
    } else {
      this.ideaObject.previewScrollButtonsVisibility = 'hidden';
    }
    this.updateUrl();
    this.showVideo = false;
    this.ideaLinks = null;
  }

  updateUrl() {
    return this.$state.go(
      'root.searchStatic',
      {
        parent: this.stateParams['parent'],
        query: this.query,
        ageGroup: this.ageGroup
      },
      { notify: true }
    );
  }

  openPreviewByIndex(ideaIndex) {
    this.ideaIndex = ideaIndex;
    this.ideaObject = this.ideas[ideaIndex];
    this.idea = this.ideaObject.name;

    if (this.ideaObject.download) {
      this.previewImageName = this.downloadablePreviewIconService.getImageName(
        this.ideaObject.uploadedFileName
      );
    }

    if (this.ideaObject.previewPages.length > 0) {
      this.activePreviewPage = this.ideaObject.previewPages[0];
    } else {
      this.activePreviewPage = null;
    }

    if (this.ideaObject.previewPages.length > 3) {
      this.ideaObject.previewScrollButtonsVisibility = 'visible';
    } else {
      this.ideaObject.previewScrollButtonsVisibility = 'hidden';
    }

    this.ideaLinks = null;
  }

  toggleFavourite(idea) {
    let ideaLinks = this.ideaLinksResolver.getLinks(idea);
    if (ideaLinks.favouritesDenied.show) {
      this.$state.go(ideaLinks.favouritesDenied.link);
    } else {
      idea.favourite = !idea.favourite;
      this.favouritesModel.toggleFavourite(idea.id);
    }
  }

  getIdeaViewLink(idea) {
    return this.ideaLinksResolver.getLinks(idea).view.link;
  }

  getIdeaLinks() {
    if (this.ideaLinks == null) {
      this.ideaLinks = this.ideaLinksResolver.getLinks(this.ideaObject);
    }
    return this.ideaLinks;
  }

  updateScrollers() {
    let scrollPanes = this.$document.find('.ideas [scroll-pane]');
    for (let scrollPane in scrollPanes) {
      if (angular.element(scrollPane).controller('scrollPane')) {
        angular
          .element(scrollPane)
          .controller('scrollPane')
          .updateButtonVisibility();
      }
    }
    if (this.activeCategory) {
      let el = this.$document.find('.ideas .sort-row');
      let scroll = el.controller('scrollPane');
      if (scroll) {
        this.feed.categories.forEach((category, index) => {
          if (category.name === this.activeCategory) {
            return scroll.scrollToIndex(index + 1);
          }
        });
      }
    }
  }

  selectPreviewPage(page) {
    this.activePreviewPage = page;
  }

  closePreview(updateUrl: boolean) {
    this.ideaCategoryIndex = null;
    this.ideaIndex = null;
    this.ideaObject = null;
    this.idea = null;
  }

  prevItem(categoryIndex: number) {
    this.ideaIndex -= 1;
    this.openPreviewByIndex(this.ideaIndex);
  }

  nextItem(categoryIndex: number) {
    this.ideaIndex += 1;
    this.openPreviewByIndex(this.ideaIndex);
  }

  scrollToPreview(categoryIndex: number) {
    let delay = 0;
    if (this.idea != null) {
      let electedRow = this.$document.find('.category-' + categoryIndex);
      if (electedRow.length === 0) {
        delay = 1000;
      }
      let that = this;
      this.$timeout(function() {
        let elPos = 0;
        if (categoryIndex > 0) {
          for (let i = 0; i < categoryIndex; i++) {
            electedRow = that.$document.find('.category-' + i);
            elPos += electedRow.outerHeight();
          }
        }
        electedRow = that.$document.find('.category-0');

        if (electedRow.length > 0) {
          let header = that.$document.find('.sticky-top-nav');
          let navbar = that.$document.find('.navbar-header');
          let firstElementPosition = electedRow.offset().top - header.outerHeight();
          let offset = elPos + firstElementPosition - navbar.outerHeight();
          that.$document.duScrollTo(0, offset);
        }
      }, delay);
    }
  }

  generateArrowBtnClass() {
    let categoryLength: number = 0;
    if (this.feed.categories != null) {
      categoryLength = this.feed.categories.length;
    }

    let level: number = 0;
    if (this.breadcrumbs != null) {
      level = this.breadcrumbs.length;
    }
    let arrow_container_md = false;
    let arrow_container_small = false;

    if (level > 0) {
      if (categoryLength === 5 || categoryLength === 6) {
        arrow_container_md = true;
      } else if (categoryLength === 3 || categoryLength === 4) {
        arrow_container_small = true;
      }
    } else {
      if (categoryLength === 6 || categoryLength === 7) {
        arrow_container_md = true;
      } else if (categoryLength === 4 || categoryLength === 5) {
        arrow_container_small = true;
      }
      return {
        'arrow-container-small': arrow_container_small,
        'arrow-container-md': arrow_container_md
      };
    }
  }

  generateBtnRowClass() {
    let categoriesLength: number = this.feed.categories.length;
    let level: number = 0;
    if (this.breadcrumbs != null) {
      level = this.breadcrumbs.length;
    }
    let sort_row_small = false;
    let sort_row_small_plus = false;
    let sort_row_md = false;
    let sort_row_md_plus = false;

    if (level > 0) {
      if (categoriesLength === 3) {
        sort_row_small = true;
      } else if (categoriesLength === 4) {
        sort_row_small_plus = true;
      } else if (categoriesLength === 5) {
        sort_row_md = true;
      } else if (categoriesLength === 6) {
        sort_row_md_plus = true;
      }
    } else {
      if (categoriesLength === 4) {
        sort_row_small = true;
      } else if (categoriesLength === 5) {
        sort_row_small_plus = true;
      } else if (categoriesLength === 6) {
        sort_row_md = true;
      } else if (categoriesLength === 7) {
        sort_row_md_plus = true;
      }
    }
    return {
      'sort-row-small': sort_row_small,
      'sort-row-small-plus': sort_row_small_plus,
      'sort-row-md': sort_row_md,
      'sort-row-md-plus': sort_row_md_plus
    };
  }

  back() {
    this.page--;
    this.$state.go(
      '.',
      {
        parent: this.parentId,
        query: this.query,
        ageGroup: this.ageGroup,
        page: this.page
      },
      { reload: true }
    );
  }

  nextGrid() {
    this.page++;
    this.$state.go(
      '.',
      {
        parent: this.parentId,
        query: this.query,
        ageGroup: this.ageGroup,
        page: this.page
      },
      { reload: true }
    );
  }

  downloadIdea() {
    this.$window.open(
      window['kiga'].endPointUrl + '/s3/download/' + this.ideaObject.uploadedFileId,
      '_blank'
    );
  }

  private loadSearchHits() {}

  private getBreadcrumbsTitle(): string {
    if (this.isWithSearchResults()) {
      return 'SEARCH_ALL_HITS';
    } else {
      return 'SEARCH_NO_HITS';
    }
  }
}
