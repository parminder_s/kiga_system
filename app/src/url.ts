export default function url(url: String) {
  let kiga = window['kiga'];
  if (kiga.clientUrl.match(/\/$/)) {
    return '' + kiga.clientUrl + url;
  } else {
    return kiga.clientUrl + '/' + url;
  }
}
