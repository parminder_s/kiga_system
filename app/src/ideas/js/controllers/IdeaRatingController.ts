import IdeasModel from '../../../ideas/js/services/IdeasModel';
export default class IdeaRatingController {
  ideaId: number = 0;
  rating: number = 0;
  count: number = 0;

  constructor(private ideasModel: IdeasModel) {}

  $onChanges(changeObj) {
    this.ideasModel.getRating(this.ideaId).then(response => {
      this.rating = response.rating;
      this.count = response.count;
    });
  }

  getStars() {
    let returner = [];
    for (let i = 0; i < 5; ++i) {
      if (this.rating <= i) {
        returner.push('fa-star-o');
      } else if (this.rating === i + 0.5) {
        returner.push('fa-star-half-o');
      } else if (this.rating > i + 0.5) {
        returner.push('fa-star');
      }
    }
    return returner;
  }
}
