import * as angular from 'angular';
import 'angular-clipboard';
import * as _angularSanitize from 'angular-sanitize';
import * as _angularScroll from 'angular-scroll';
import 'angular-socialshare';
import { StateService } from 'angular-ui-router';
import * as _misc from 'misc';
import AreaFactory from '../../area/js/AreaFactory';
import { AreaType } from '../../area/js/AreaType';
import { CmsRenderersFactory } from '../../cms/js/factories/CmsRenderersFactory';
import { FavouritesModel } from '../../misc/js/model/FavouritesModel';
import {
  IConfigService,
  ILinksService,
  INgxNavigateService
} from '../../misc/js/services/AngularInterfaces';
import IEndpoint from '../../misc/js/services/IEndpoint';
import ToastService from '../../misc/toast/ToastService';
import GridViewController from '../components/gridView/GridViewController';
import IdeaLinkCopyClipboardButtonController from '../components/ideaLinkCopyClipboardButton/IdeaLinkCopyClipboardButtonController';
import IdeaPreviewController from '../components/ideaPreview/IdeaPreviewController';
import IdeaThumbnailMobileController from '../components/ideaThumbnailMobile/IdeaThumbnailMobileController';
import ItemThumbnailController from '../components/itemThumbnail/ItemThumbnailController';
import IdeaRatingController from './controllers/IdeaRatingController';
import bindHtmlCompile from './directives/bindHtmlCompile';
import IdeasRenderer from './renderers/IdeasRenderer';
import { KigaIdeaRenderer } from './renderers/KigaIdeaRenderer';
import IdeaLinksResolver from './services/IdeaLinksResolver';
import IdeaPermissionHandler from './services/IdeaPermissionHandler';
import IdeasAreaCallback from './services/IdeasAreaCallback';
import IdeasModel from './services/IdeasModel';

let misc = _misc;
let angularSanitize = _angularSanitize;
let angularScroll = _angularScroll;
export let app = angular.module('ideas', [
  'angular-clipboard',
  'misc',
  'ngSanitize',
  'duScroll',
  '720kb.socialshare'
]);

app
  .config($stateProvider => {
    let redirectToIdeas = links => {
      window.location.href = links.findByCode('ideaGroupMainContainer').url;
    };

    $stateProvider
      .state({
        name: 'root.idea-groups',
        url: '/ideas-group',
        template: '<ui-view></ui-view>',
        controller: links => redirectToIdeas(links)
      })
      .state({
        name: 'root.idea-groups.path',
        url: '/{path:nonURIEncoded}',
        templateUrl: '<ui-view></ui-view>',
        controller: links => redirectToIdeas(links)
      });
  })
  .run(
    (
      ideasAreaCallback: IdeasAreaCallback,
      areaFactory: AreaFactory,
      cmsRenderersFactory: CmsRenderersFactory,
      kigaIdeaRenderer,
      ideasRenderer
    ) => {
      areaFactory.getInstance(AreaType.IDEAS).setDefaultCallback(ideasAreaCallback);
      cmsRenderersFactory.register('Article', kigaIdeaRenderer);
      cmsRenderersFactory.register('IdeaGroup', ideasRenderer);
      cmsRenderersFactory.register('IdeaGroupContainer', ideasRenderer);
      cmsRenderersFactory.register('IdeaGroupCategory', ideasRenderer);
      cmsRenderersFactory.register('IdeaGroupMainContainer', ideasRenderer);
    }
  )
  .component('gridView', {
    controller: 'gridViewController',
    templateUrl: 'ideas/components/gridView/gridView.html',
    bindings: {
      items: '<',
      activeItemId: '<',
      onClick: '&',
      onHeartClick: '&'
    }
  })
  .component('ideaLinkCopyClipboardButton', {
    controller: (toastService: ToastService, clipboard) =>
      new IdeaLinkCopyClipboardButtonController(toastService, clipboard),
    templateUrl: 'ideas/components/ideaLinkCopyClipboardButton/ideaLinkCopyClipboardButton.html',
    bindings: {
      url: '<'
    }
  })
  .component('ideaPreview', {
    controller: (ideaLinksResolver: IdeaLinksResolver) =>
      new IdeaPreviewController(ideaLinksResolver),
    templateUrl: 'ideas/components/ideaPreview/ideaPreview.html',
    bindings: {
      idea: '<',
      onPrevious: '&',
      onNext: '&',
      onClose: '&',
      showNext: '<',
      showPrevious: '<',
      showClose: '<'
    }
  })
  .component('ideaRating', {
    controller: 'ideaRatingCtrl',
    templateUrl: 'ideas/partials/ideaRating.html',
    bindings: { ideaId: '<' }
  })
  .component('itemThumbnail', {
    controller: 'itemThumbnailController',
    templateUrl: 'ideas/components/itemThumbnail/itemThumbnail.html',
    bindings: {
      item: '<',
      onClick: '&',
      onHeartClick: '&'
    }
  })
  .component('ideaThumbnailMobile', {
    controller: 'ideaThumbnailMobileController',
    templateUrl: 'ideas/components/ideaThumbnailMobile/ideaThumbnailMobile.html',
    bindings: {
      idea: '<'
    }
  })
  .component('socialButtons', {
    templateUrl: 'ideas/components/socialButtons/socialButtons.html',
    bindings: { title: '<', url: '<', imageUrl: '<' }
  })
  .controller('gridViewController', () => new GridViewController())
  .controller('ideaRatingCtrl', ideasModel => new IdeaRatingController(ideasModel))
  .controller(
    'ideaThumbnailMobileController',
    (
      ideaLinksResolver: IdeaLinksResolver,
      $state: StateService,
      favouritesModel: FavouritesModel
    ) => new IdeaThumbnailMobileController(ideaLinksResolver, $state, favouritesModel)
  )
  .controller('itemThumbnailController', () => new ItemThumbnailController())
  .directive('bindHtmlCompile', $compile => bindHtmlCompile($compile))
  .service(
    'kigaIdeaRenderer',
    (
      endpoint,
      member,
      ideaLinksResolver,
      favouritesModel,
      $state: StateService,
      scrollUpService,
      $transitions,
      audioHandler,
      ideasAreaCallback,
      $window,
      $stateParams,
      areaFetcher,
      goBack
    ) =>
      new KigaIdeaRenderer(
        endpoint,
        member,
        ideaLinksResolver,
        favouritesModel,
        $state,
        scrollUpService,
        $transitions,
        audioHandler,
        ideasAreaCallback,
        $window,
        $stateParams,
        areaFetcher,
        goBack
      )
  )
  .service(
    'ideaLinksResolver',
    (ideaPermissionHandler, permissionStateResolver, member, $state) =>
      new IdeaLinksResolver(member, ideaPermissionHandler, permissionStateResolver, $state)
  )
  .service('ideaPermissionHandler', function(member, config: IConfigService) {
    return new IdeaPermissionHandler(member, config);
  })
  .service('ideasAreaCallback', function(links, request, $location, $state, locale, config) {
    return new IdeasAreaCallback(links, request, $location, $state, locale, config);
  })
  .service('ideasModel', (endpoint, locale) => new IdeasModel(endpoint, locale))
  .service(
    'ideasRenderer',
    (
      $document,
      $timeout,
      $state: StateService,
      $rootScope,
      endpoint: IEndpoint,
      ideaLinksResolver: IdeaLinksResolver,
      member,
      favouritesModel: FavouritesModel,
      ideasModel: IdeasModel,
      ideasAreaCallback: IdeasAreaCallback,
      audioHandler,
      $transitions,
      config: IConfigService,
      $window,
      clipboard,
      toastService,
      $stateParams,
      areaFetcher,
      links: ILinksService,
      ngxNavigate: INgxNavigateService
    ) =>
      new IdeasRenderer(
        $document,
        $timeout,
        $state,
        ideaLinksResolver,
        member,
        favouritesModel,
        ideasModel,
        ideasAreaCallback,
        audioHandler,
        $transitions,
        config,
        $window,
        clipboard,
        toastService,
        $stateParams,
        areaFetcher,
        links,
        ngxNavigate
      )
  );
