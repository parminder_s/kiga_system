function bindHtmlCompile($compile) {
  return {
    restrict: 'A',
    link(scope, elem, attrs) {
      return scope.$watch(
        () => scope.$eval(attrs.bindHtmlCompile),
        value => {
          elem.html(value && value.toString());
          const compileScope = !attrs.bindHtmlScope ? scope : scope.$eval(attrs.bindHtmlScope);
          return $compile(elem.contents())(compileScope);
        }
      );
    }
  };
}
export default bindHtmlCompile;
