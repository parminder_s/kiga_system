import { ResolvedType } from '../../cms/js/ResolvedType';

export class IdeaResolvedTypesForCms {
  public static Article: ResolvedType = 'Article';
  public static IdeaGroup: ResolvedType = 'IdeaGroup';
  public static IdeaGroupContainer: ResolvedType = 'IdeaGroupContainer';
  public static IdeaGroupCategory: ResolvedType = 'IdeaGroupCategory';
  public static IdeaGroupMainContainer: ResolvedType = 'IdeaGroupMainContainer';
}
