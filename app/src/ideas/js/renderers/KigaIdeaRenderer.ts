import { StateParams, StateService } from 'angular-ui-router';
import { GoBackService } from '../../../../../src/app/shared/go-back.service';
import Area from '../../../area/js/Area';
import AreaFetcher from '../../../area/js/AreaFetcher';
import { AreaType } from '../../../area/js/AreaType';
import { Renderer } from '../../../cms/js/renderers/Renderer';
import ViewModel from '../../../cms/js/ViewModel';
import { FavouritesModel } from '../../../misc/js/model/FavouritesModel';
import { IGoBack } from '../../../misc/js/services/AngularInterfaces';
import IMember from '../../../misc/permission/js/IMember';
import IMemberService from '../../../misc/permission/js/IMemberService';
import { IdeaLinks } from '../services/IdeaLinks';
import IdeasAreaCallback from '../services/IdeasAreaCallback';
import IdeasModel from '../services/IdeasModel';

export class KigaIdeaRenderer implements Renderer {
  public rootBreadcrumbTitle = '';
  public viewModel: ViewModel;

  public partial: string = 'ideas/partials/idea.html';
  private forumThread;
  private idea;
  private member: IMember;
  private promotion;
  private ideasModel: IdeasModel;
  private isFavourite: boolean;

  links: IdeaLinks;
  postContent: String;

  constructor(
    private endpoint,
    memberService: IMemberService,
    private ideaLinksResolver,
    private favouritesModel: FavouritesModel,
    private $state: StateService,
    private scrollUpService,
    private $transitions,
    private audioHandler,
    private ideasAreaCallback: IdeasAreaCallback,
    private $window,
    private $stateParams: StateParams,
    private areaFetcher: AreaFetcher,
    private goBack: IGoBack
  ) {
    this.member = memberService.get();
  }

  initialize(viewModel: ViewModel) {
    this.viewModel = viewModel;
    this.idea = viewModel;

    this.ideasModel = new IdeasModel(this.endpoint, this.$stateParams.locale);
    this.links = this.ideaLinksResolver.getLinks(this.idea);
    if (this.idea.userRating) {
      this.idea.ideasResponseRating.rating = this.idea.userRating;
    }
    this.getForumThread();
    this.getPromotion();
    this.scrollUpService.scroll();

    this.$transitions.onStart({ to: '**' }, () => {
      this.audioHandler.flushMusic();
    });

    this.$state.params['area'] = AreaType.IDEAS;
    let area: Area = this.areaFetcher.getArea();
    area.withAgeGroup(this.$stateParams.ageGroupFilter);
    area.withCallback(this.ideasAreaCallback);

    this.loadFavourites();
    this.setIsFavourite();
    this.favouritesModel.notifyForUpdate(() => {
      console.log('hier renderer');
      this.setIsFavourite();
    });
  }

  onBreadcrumbClick(index) {}

  onTopCategoryClick() {}

  setIsFavourite() {
    this.favouritesModel.isIdeaFavourite(this.idea.id).then(isFavourite => {
      this.isFavourite = isFavourite;
    });
  }

  getForumThread() {
    this.ideasModel.getForumThread(this.idea.id).then(data => {
      this.forumThread = data;
    });
  }

  loadFavourites(): void {
    this.favouritesModel.listFavouritesForIdsAsPromise().then(data => {
      let favs = data.favourites;
      this.idea.linkedIdeas.forEach(idea => {
        idea.favourite = false;
        favs.forEach(fav => {
          if (fav.ideaId === idea.id) {
            idea.favourite = true;
          }
        });
      });
    });
  }

  getPromotion() {
    this.ideasModel.getPromotion(this.idea.id).then(data => {
      this.promotion = data;
    });
  }

  addPost() {
    if (this.postContent) {
      this.ideasModel
        .addPost(this.idea.id, this.postContent)
        .then(() => {
          this.ideasModel.getForumThread(this.idea.id).then(data => {
            this.forumThread = data;
            this.postContent = '';
          });
        })
        .catch(function(data, status, headers, config) {
          console.log('ERROR');
        });
    }
  }

  linkClicked(link: string) {
    if (link === 'favourites') {
      if (this.links.favouritesDenied.show) {
        this.$state.go(this.links.favouritesDenied.link);
      } else {
        this.favouritesModel.toggleFavourite(this.idea.id);
        this.isFavourite = !this.isFavourite;
      }
    } else if (link === 'planner') {
      this.$state.go(this.links.planner.state, this.links.planner.stateParams);
    } else {
      window.location.href = this.links[link].link;
    }
  }

  cancel() {
    this.goBack.goBack();
  }

  downloadIdea() {
    this.$window.open(
      window['kiga'].endPointUrl + '/s3/download/' + this.idea.uploadedFileId,
      '_blank'
    );
  }
}
