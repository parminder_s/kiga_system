import { StateParams, StateService } from 'angular-ui-router';
import Area from '../../../area/js/Area';
import AreaFetcher from '../../../area/js/AreaFetcher';
import { AreaType } from '../../../area/js/AreaType';
import { Renderer } from '../../../cms/js/renderers/Renderer';
import ViewModel from '../../../cms/js/ViewModel';
import IdeasModel from '../../../ideas/js/services/IdeasModel';
import { FavouritesModel } from '../../../misc/js/model/FavouritesModel';
import {
  Feature,
  IConfigService,
  ILinksService,
  INgxNavigateService
} from '../../../misc/js/services/AngularInterfaces';
import DownloadablePreviewIcon from '../../../misc/js/services/DownloadablePreviewIcon';
import IMember from '../../../misc/permission/js/IMember';
import IMemberService from '../../../misc/permission/js/IMemberService';
import ToastService from '../../../misc/toast/ToastService';
import IdeasAreaCallback from '../services/IdeasAreaCallback';

interface IdeasState {
  path: string;
  preview: string;
  previewCategory: number;
  ageGroupFilter: number;
}

interface IdeaViewModelIdea {}

interface IdeasViewModelCategory {
  ageGroupContainer: boolean;
  ideas: Array<IdeaViewModelIdea>;
  lastCategoryLevel: boolean;
  name: string;
}

interface IdeasViewModel extends ViewModel {
  activeCategory: IdeasViewModelCategory;
  categories: Array<IdeasViewModelCategory>;
  firstLevelCategoryName: string;
  parentCategories: Array<IdeasViewModelCategory>;
}

/**
 * TOOD: clarify when `this.ideas` is  used (ageGroupContainer?) and when
 * `currentCategoryIdeas()` is required (mutliple categories) etc.
 */
export default class IdeasRenderer implements Renderer {
  static currentState: IdeasState = null;

  public rootBreadcrumbTitle: string = 'IDEAS_ALL_IDEAS';
  public breadcrumbsCssClass;
  public partial: string;
  public viewModel: IdeasViewModel;
  public previewId: number;

  private requestedPath: string;
  private ideaCategoryIndex;
  private ideaIndex;
  private activeCategory;
  private isLastNavLevel: boolean = false;
  private maxIdeasPerRow = 20;
  private activePreviewPage = null;
  private idea;
  private ideaLink: string;
  private ideaObject;
  private ideas: any[] = [];
  private ideaLinks: any[] = [];
  protected selectedHierarchy: Array<string> = [];
  private ageGroupFilter: number = null;
  private member: IMember;
  private showVideo;
  private parentName;
  private previewImageName;
  private showBlockView = false;
  private useComponents: boolean;
  private isNewIdeas = false;
  private downloadablePreviewIconService: DownloadablePreviewIcon;

  private currentShownCategories;
  private shownCategories: Array<any> = [];
  private showLoadMore: boolean = false;

  constructor(
    private $document,
    private $timeout,
    private $state: StateService,
    private ideaLinksResolver,
    memberService: IMemberService,
    private favouritesModel: FavouritesModel,
    private ideasModel: IdeasModel,
    private ideasAreaCallback: IdeasAreaCallback,
    private audioHandler,
    private $transitions,
    private config: IConfigService,
    private $window,
    private clipboard: any,
    private toastService: ToastService,
    private $stateParams: StateParams,
    private areaFetcher: AreaFetcher,
    private links: ILinksService,
    private ngxNavigate: INgxNavigateService
  ) {
    this.member = memberService.get();
  }

  initialize(viewModel: ViewModel) {
    this.currentShownCategories = 2;
    this.viewModel = viewModel as IdeasViewModel;
    this.useComponents = this.config.isFeatureEnabled(Feature.GRIDVIEWWITHCOMPONENTNS);
    this.shownCategories = [];

    IdeasRenderer.currentState = this.getIdeasState(this.$stateParams);
    this.downloadablePreviewIconService = new DownloadablePreviewIcon();
    this.isLastNavLevel = this.viewModel.activeCategory.lastCategoryLevel;

    if (this.viewModel.activeCategory.ageGroupContainer) {
      this.showBlockView = true;
      this.ideas = this.viewModel.categories[0].ideas;
      this.showLoadMore = false;
    } else {
      this.shownCategories = _.take(this.viewModel.categories, this.currentShownCategories);
      this.showLoadMore = this.shownCategories.length < this.viewModel.categories.length;
      this.showBlockView = false;
    }

    this.activeCategory = null;
    this.ideaCategoryIndex = null;
    this.ideaIndex = null;
    this.previewId = parseInt(this.$stateParams.preview) || -1;
    if (this.viewModel.breadcrumbs.length > 1) {
      this.parentName = this.viewModel.breadcrumbs[this.viewModel.breadcrumbs.length - 2].label;
    }
    this.ideaObject = null;
    this.partial = 'ideas/partials/ideaCategories.html';

    this.$state.params['area'] = AreaType.IDEAS;
    this.idea = this.$stateParams.preview;
    this.ageGroupFilter = this.$stateParams.ageGroupFilter;
    this.requestedPath = this.$stateParams.path;
    this.isNewIdeas = this.viewModel.activeCategory.ageGroupContainer;
    this.breadcrumbsCssClass = this.isNewIdeas ? 'green' : 'orange';

    let selectedHierarchy = this.requestedPath.split('/');
    this.selectedHierarchy = [];
    for (let i = 0; i < selectedHierarchy.length; ++i) {
      if (selectedHierarchy[i] != null && selectedHierarchy[i].trim().length > 0) {
        this.selectedHierarchy.push(selectedHierarchy[i].trim());
      }
    }

    let area: Area = this.areaFetcher.getArea();
    area.withAgeGroup(this.$stateParams.ageGroupFilter);
    area.withCallback(this.ideasAreaCallback);

    if (!this.idea) {
      this.$document.duScrollTo(0, 0);
    }

    this.updateScrollers();
    this.init();

    this.$transitions.onStart({ to: '**' }, () => {
      this.audioHandler.flushMusic();
    });
    this.showVideo = false;
  }

  handleFavouritesClick(id: number) {
    this.toggleFavourite(this.find(id));
  }

  find(id: number) {
    return this.ideas.filter(idea => idea.id === id);
  }

  toggleFavourite(idea) {
    let ideaLinks = this.ideaLinksResolver.getLinks(idea, true);
    if (ideaLinks.favouritesDenied.show) {
      this.$state.go(ideaLinks.favouritesDenied.link);
    } else {
      idea.favourite = !idea.favourite;
      this.favouritesModel.toggleFavourite(idea.id);
    }
  }

  addAgeGroupFilter(ageGroupFilter: number) {
    this.ageGroupFilter = ageGroupFilter;
    this.updateUrl();
  }

  doForEveryIdea(callback) {
    let category, categoryIdx, idea, idx, j, k, len, len1, ref, ref1;
    this.updateScrollers();
    ref = this.viewModel.categories;

    for (categoryIdx = j = 0, len = ref.length; j < len; categoryIdx = ++j) {
      category = ref[categoryIdx];
      ref1 = category.ideas;
      for (idx = k = 0, len1 = ref1.length; k < len1; idx = ++k) {
        idea = ref1[idx];
        callback(categoryIdx, idx, idea);
      }
    }
  }

  isAllowedMember() {
    return !this.member.hasExpiredSubscription() && this.member.hasStandardSubscription();
  }

  init() {
    let previewCategory = parseInt(this.$stateParams.previewCategory);
    this.doForEveryIdea((categoryIdx, ideaIdx, idea) => {
      if (
        (this.idea === idea.name && previewCategory === categoryIdx) ||
        (this.idea === idea.name && !previewCategory)
      ) {
        return this.openPreview(categoryIdx, ideaIdx);
      }
    });

    this.favouritesModel.listFavouritesForIdsAsPromise().then(data => {
      let favs = data.favourites;
      this.doForEveryIdea((cIdx, ideaIdx, idea) => {
        idea.favourite = false;
        for (let i = 0; i < favs.length; ++i) {
          if (favs[i].ideaId === idea.id) {
            idea.favourite = true;
          }
        }
      });
    });

    if (this.isLastNavLevel && !this.idea) {
      this.openPreview(0, 0);
    }
  }

  allowEdit() {
    return this.member.isCms;
  }

  planType() {
    if (this.member.isCms) {
      return 'kiga';
    } else {
      return 'member';
    }
  }

  selectPreviewPage(page) {
    return (this.activePreviewPage = page);
  }

  updateUrl() {
    if (!this.hasOnlyChangedPreview(this.getUrl())) {
      this.$state.go('root.cms', this.getUrl());
    }
  }

  hasOnlyChangedPreview(stateParams: IdeasState) {
    let formerState = IdeasRenderer.currentState;
    if (formerState === null) {
      return false;
    }

    if (
      formerState.path === stateParams['path'] &&
      formerState.ageGroupFilter === stateParams['ageGroupFilter']
    ) {
      return true;
    } else {
      return false;
    }
  }

  getIdeasState($stateParams: StateParams): IdeasState {
    return {
      path: $stateParams['path'],
      preview: $stateParams['preview'],
      previewCategory: $stateParams['previewCategory'],
      ageGroupFilter: $stateParams['ageGroupFilter']
    };
  }

  getUrl(): IdeasState {
    return {
      path: decodeURI(this.selectedHierarchy.join('/')),
      preview: this.idea,
      previewCategory: this.ideaCategoryIndex,
      ageGroupFilter: this.ageGroupFilter
    };
  }

  findIdeaByName(ideaName) {
    let category, categoryIdx, idea, idx, j, k, len, len1, ref, ref1;
    ref = this.viewModel.categories;
    for (categoryIdx = j = 0, len = ref.length; j < len; categoryIdx = ++j) {
      category = ref[categoryIdx];
      ref1 = category.ideas;
      for (idx = k = 0, len1 = ref1.length; k < len1; idx = ++k) {
        idea = ref1[idx];
        if (ideaName === idea.name) {
          return idea;
        }
      }
    }
  }

  isPreviewFromCategory(categoryIndex) {
    return this.ideaCategoryIndex === categoryIndex;
  }

  generateArrowBtnClass() {
    let categoriesLength = this.viewModel.categories.length;
    let level = this.selectedHierarchy.length;
    let arrow_container_md = false;
    let arrow_container_small = false;

    if (level) {
      if (categoriesLength === 5 || categoriesLength === 6) {
        arrow_container_md = true;
      } else if (categoriesLength === 3 || categoriesLength === 4) {
        arrow_container_small = true;
      }
    } else {
      if (categoriesLength === 6 || categoriesLength === 7) {
        arrow_container_md = true;
      } else if (categoriesLength === 4 || categoriesLength === 5) {
        arrow_container_small = true;
      }
    }

    return {
      'arrow-container-small': arrow_container_small,
      'arrow-container-md': arrow_container_md
    };
  }

  generateBtnRowClass() {
    let categoriesLength = this.viewModel.categories.length;
    let level = this.selectedHierarchy.length;
    let sort_row_small = false;
    let sort_row_small_plus = false;
    let sort_row_md = false;
    let sort_row_md_plus = false;

    if (level) {
      if (categoriesLength === 3) {
        sort_row_small = true;
      } else if (categoriesLength === 4) {
        sort_row_small_plus = true;
      } else if (categoriesLength === 5) {
        sort_row_md = true;
      } else if (categoriesLength === 6) {
        sort_row_md_plus = true;
      }
    } else {
      if (categoriesLength === 4) {
        sort_row_small = true;
      } else if (categoriesLength === 5) {
        sort_row_small_plus = true;
      } else if (categoriesLength === 6) {
        sort_row_md = true;
      } else if (categoriesLength === 7) {
        sort_row_md_plus = true;
      }
    }

    return {
      'sort-row-small': sort_row_small,
      'sort-row-small-plus': sort_row_small_plus,
      'sort-row-md': sort_row_md,
      'sort-row-md-plus': sort_row_md_plus
    };
  }

  onBackToParent() {
    this.selectedHierarchy.pop();
    this.closePreview(false);
    this.updateScrollers();
    this.updateUrl();
  }

  onBreadcrumbClick(index: number) {
    this.selectedHierarchy = this.selectedHierarchy.slice(0, index + 2);
    this.closePreview(false);
    this.updateScrollers();
    this.updateUrl();
  }

  onTopCategoryClick() {
    const url = this.links.findByCode('ideaGroupMainContainer').url;
    const [, path] = url.match(/cms\/(.*)$/);
    this.$state.go('root.cms', { path });
  }

  onCategoryClick(categoryIndex) {
    let clickedCategoryName = this.viewModel.categories[categoryIndex].name;
    this.selectedHierarchy.push(clickedCategoryName);

    this.closePreview(false);
    this.updateScrollers();
    this.updateUrl();
  }

  onCategoryButtonClick(categoryIndex) {
    if (!this.viewModel.parentCategories) {
      let clickedCategoryName = this.viewModel.categories[categoryIndex].name;
      this.selectedHierarchy.push(clickedCategoryName);
    } else {
      let clickedCategoryName = this.viewModel.parentCategories[categoryIndex].name;
      this.selectedHierarchy[this.selectedHierarchy.length - 1] = clickedCategoryName;
    }

    this.closePreview(false);
    this.updateScrollers();
    this.updateUrl();
  }

  currentCategoryHasNext() {
    return this.ideaIndex < this.currentCategoryIdeas.length - 1;
  }

  /**
   * Get ideas of current category.
   * There are two modes to this: either we have ideas in the
   * active category, then take these, otherwise we take the ideas
   * from the categories at the active category index.
   */
  get currentCategoryIdeas(): any[] {
    if (!!this.viewModel.activeCategory.ideas) {
      return this.viewModel.activeCategory.ideas;
    } else {
      let category = this.viewModel.categories[this.ideaCategoryIndex];
      return category ? category.ideas : [];
    }
  }

  openPreviewByIndex(ideaIndex) {
    this.ideaIndex = ideaIndex;
    this.ideaObject = this.currentCategoryIdeas[ideaIndex]; // TODO : should check
    // for undefined / index out-of-bounds
    this.idea = this.ideaObject.name;
    this.ideaLink = this.config.getNgLiveUrl() + this.$state.href('root.cms', this.getUrl());

    if (this.ideaObject.download) {
      this.previewImageName = this.downloadablePreviewIconService.getImageName(
        this.ideaObject.uploadedFileName
      );
    }

    if (this.ideaObject.previewPages.length > 0) {
      this.activePreviewPage = this.ideaObject.previewPages[0];
    } else {
      this.activePreviewPage = null;
    }

    if (this.ideaObject.previewPages.length > 3) {
      this.ideaObject.previewScrollButtonsVisibility = 'visible';
    } else {
      this.ideaObject.previewScrollButtonsVisibility = 'hidden';
    }

    this.ideaLinks = null;
  }

  onGridIdeaClick(index: number) {
    if (index === this.ideaIndex) {
      this.closePreview(false);
    } else {
      this.openPreviewByIndex(index);
    }
  }

  onBlockViewNewIdeaClick(index: number) {
    if (index === this.ideaIndex) {
      this.closePreview(false);
    } else {
      this.ideaCategoryIndex = 0; // because in newIdeaBlockView we always have one single category
      this.openPreviewByIndex(index);
    }
  }

  showPreviewPanel(ideasPerRow: number, idx: number) {
    let panelRow = Math.floor(idx / ideasPerRow);
    return (
      this.ideaIndex !== null &&
      ((idx + 1) % ideasPerRow === 0 || idx === this.ideas.length - 1) &&
      Math.floor(this.ideaIndex / ideasPerRow) === panelRow
    );
  }

  copyLinkToClipboard() {
    this.clipboard.copyText(this.ideaLink);
    this.toastService.success('Link in Zwischenablage kopiert');
  }

  closePreview(updateUrl) {
    this.ideaCategoryIndex = null;
    this.ideaIndex = null;
    this.ideaObject = null;
    this.idea = null;
    if (updateUrl) {
      this.updateUrl();
    }
  }

  onIdeaClick(categoryIndex, idx) {
    if (categoryIndex === this.ideaCategoryIndex && idx === this.ideaIndex) {
      this.closePreview(true);
      return;
    }
    this.openPreview(categoryIndex, idx);
  }

  scrollToPreview(categoryIndex) {
    let delay, el;
    if (this.idea !== void 0) {
      el = this.$document.find('.category-' + categoryIndex);
      if (el.length) {
        delay = 0;
      } else {
        delay = 1000;
      }

      return this.$timeout(() => {
        let elPos, firstElPos, header, navbar, i, j, ref;
        elPos = 0;
        if (categoryIndex > 0) {
          for (
            i = j = 0, ref = categoryIndex - 1;
            0 <= ref ? j <= ref : j >= ref;
            i = 0 <= ref ? ++j : --j
          ) {
            el = this.$document.find('.category-' + i);
            elPos += el.outerHeight();
          }
        }
        el = this.$document.find('.category-0');
        if (el.length) {
          header = this.$document.find('.sticky-top-nav');
          navbar = this.$document.find('.navbar-header');
          firstElPos = el.offset().top - header.outerHeight();
          return this.$document.duScrollTo(0, elPos + firstElPos - navbar.outerHeight());
        }
      }, delay);
    }
  }

  openPreview(categoryIndex, idx) {
    this.scrollToPreview(categoryIndex);
    this.ideaCategoryIndex = categoryIndex;
    this.ideaIndex = idx;
    this.ideaObject = this.viewModel.categories[categoryIndex].ideas[idx];
    this.idea = this.ideaObject.name;
    this.ideaLink = this.config.getNgLiveUrl() + this.$state.href('root.cms', this.getUrl());

    if (this.ideaObject.previewPages.length) {
      this.activePreviewPage = this.ideaObject.previewPages[0];
    } else {
      this.activePreviewPage = null;
    }
    if (this.ideaObject.download) {
      if (this.ideaObject.download) {
        this.previewImageName = this.downloadablePreviewIconService.getImageName(
          this.ideaObject.uploadedFileName
        );
      }
    }

    this.ideaObject.previewScrollButtonsVisibility =
      this.ideaObject.previewPages.length > 3 ? 'visible' : 'hidden';
    this.updateUrl();
    this.ideaLinks = null;
  }

  getIdeaViewLink(_idea) {
    return this.ideaLinksResolver.getLinks(_idea, true).view.link;
  }

  getIdeaLinks() {
    if (!this.ideaLinks) {
      this.ideaLinks = this.ideaLinksResolver.getLinks(this.ideaObject, true);
    }
    return this.ideaLinks;
  }

  getIdeas() {
    if (!this.ideaLinks) {
      this.ideaLinks = this.ideaLinksResolver.getLinks(this.ideaObject, true);
    }
    return this.ideaLinks;
  }

  prevItem(categoryIndex) {
    this.ideaIndex -= 1;
    this.openPreviewByIndex(this.ideaIndex);
  }

  nextItem(categoryIndex) {
    this.ideaIndex += 1;
    this.openPreviewByIndex(this.ideaIndex);
  }

  updateScrollers() {
    let category, el, index, j, len, ref, scroll, scrollPanes;
    scrollPanes = this.$document.find('.ideas [scroll-pane]');
    scrollPanes.each(function(idx, pane) {
      if (angular.element(pane).controller('scrollPane') != null) {
        return angular
          .element(pane)
          .controller('scrollPane')
          .updateButtonsVisibility();
      }
    });
    if (this.activeCategory != null) {
      el = this.$document.find('.ideas .sort-row');
      scroll = el.controller('scrollPane');
      if (scroll != null) {
        ref = this.viewModel.categories;
        for (index = j = 0, len = ref.length; j < len; index = ++j) {
          category = ref[index];
          if (category.name === this.activeCategory) {
            return scroll.scrollToIndex(index + 1, true);
          }
        }
      }
    }
  }

  getRequestedPath(): string {
    return this.requestedPath;
  }

  downloadIdea() {
    this.$window.open(
      window['kiga'].endPointUrl + '/s3/download/' + this.ideaObject.uploadedFileId,
      '_blank'
    );
  }

  showMore() {
    this.currentShownCategories += 2;
    this.shownCategories = _.take(this.viewModel.categories, this.currentShownCategories);
    this.showLoadMore = this.shownCategories.length < this.viewModel.categories.length;
  }
}
