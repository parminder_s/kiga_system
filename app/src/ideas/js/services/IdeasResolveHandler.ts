import { IHookRegistry } from 'angular-ui-router';

/**
 * <p>If a user clicks on an Ideas renderer page on a thumbnail the preview pane opens. This is
 * done by setting a parameter responsible for the preview in the url. Changing the url triggers
 * ui-router to request the viewModel from the endpoint again, which causes some delay, whichs is
 * not required, since the endpoint returns the same result and we already have the data
 * available.</p>
 *
 * <p>This service handles that use case by hooking into a transition of the cms module
 * and storing the result. If a new transition takes place which has exaclty the same
 * parameters (except the preview data) and the former resolved data identifies it as
 * IdeasViewModel, then we do not call the endpoint, but return the stored data.</p>
 *
 */
export default class IdeasResolveHandler {
  constructor(private $transitions: IHookRegistry) {}
}
