import IdeaPermissionHandler from './IdeaPermissionHandler';
import { IdeaLinks } from './IdeaLinks';
import { StateService } from 'angular-ui-router';
import PermissionStateResolver from '../../../misc/permission/js/PermissionStateResolver';

export default class IdeaLinksResolver {
  constructor(
    private member,
    private ideaPermissionHandler: IdeaPermissionHandler,
    private permissionStateResolver: PermissionStateResolver,
    private $state: StateService
  ) {}

  getLinks(idea): IdeaLinks {
    let planType = 'member';
    let showEdit = false;

    if (this.member.getAll().isCms) {
      planType = 'kiga';
      showEdit = true;
    }

    let editLink = '/admin/show/' + idea.id;
    if (idea.download === true) {
      editLink = this.$state.href('root.backend.cmsBackend.kiga-idea.edit', { ideaId: idea.id });
    }

    let returner: IdeaLinks = {
      edit: { show: showEdit, link: editLink },
      planner: {
        show: true,
        link: this.$state.href('root.plan.planType.addTo', { itemId: idea.id, planType: planType }),
        state: 'root.plan.planType.addTo',
        stateParams: { itemId: idea.id, planType: planType }
      },
      view: null,
      favouritesDenied: { show: false, link: null },
      print: { show: true, link: null },
      printWithoutImage: { show: idea.layoutType === 'DYNAMIC', link: null }
    };
    let url = idea.ngUrl;

    if (this.ideaPermissionHandler.hasPermission(idea.inPool, idea.facebookShare)) {
      returner.view = { show: true, link: url };
      returner.print.link = idea.url + '/PDF';
      returner.printWithoutImage.link = idea.url + '/PDFWOImage';
    } else {
      let permissionState = this.permissionStateResolver.resolvePermissionDenied(!idea.inPool);
      let permissionLink = this.$state.href(permissionState, {});
      if (permissionState === 'root.login') {
        permissionLink = this.$state.href(permissionState, { forward: url });
      }

      returner.view = { show: true, link: permissionLink };
      returner.print.link = permissionLink;
      returner.printWithoutImage.link = permissionLink;
    }

    if (!this.ideaPermissionHandler.hasPermission(false)) {
      returner.planner.link = this.$state.href('root.plan.planType.detail', { planType: 'kiga' });
    }

    if (!this.ideaPermissionHandler.hasPermission(false)) {
      returner.favouritesDenied = {
        show: true,
        link: this.permissionStateResolver.resolvePermissionDenied(true)
      };
    }

    return returner;
  }
}
