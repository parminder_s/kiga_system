export interface IdeaLink {
  show: boolean;
  link: string;
  state?: string; // required for compatibility with Angular/AngularJS
  stateParams?: any; // required for compatibility with Angular/AngularJS
}
