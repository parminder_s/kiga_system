export default class IdeasModel {
  cache = {};

  constructor(private endpoint, private locale) {}

  getGroupFeed(urlSegment, categoryDepth, ageGroupFilter, isLastCategoryLevel) {
    let cacheKey;
    ageGroupFilter = ageGroupFilter || 0;
    if (!urlSegment) {
      urlSegment = 'all';
    }
    if (isLastCategoryLevel == null) {
      isLastCategoryLevel = false;
    }
    cacheKey =
      urlSegment + isLastCategoryLevel + categoryDepth + this.locale.get() + ageGroupFilter;
    if (this.cache[cacheKey]) {
      return this.cache[cacheKey];
    } else {
      return this.endpoint({
        url: 'ideas/groups/list',
        data: {
          urlSegment: urlSegment,
          lastCategory: isLastCategoryLevel,
          level: categoryDepth,
          ageGroupFilter: ageGroupFilter
        }
      }).then(
        (function(_this) {
          return function(data) {
            return (_this.cache[cacheKey] = data);
          };
        })(this)
      );
    }
  }

  getIdeaFeed(id) {
    let cacheKey = id + this.locale.get();
    if (this.cache[cacheKey]) {
      return this.cache[cacheKey];
    } else {
      return this.endpoint({
        url: 'idea/',
        data: {
          id: id
        }
      }).then(
        (function(_this) {
          return function(data) {
            return (_this.cache[cacheKey] = data);
          };
        })(this)
      );
    }
  }

  getForumThread(id) {
    return this.endpoint({
      url: 'forum/idea/get/' + id,
      method: 'GET',
      quietMode: true
    });
  }

  addPost(ideaId, content) {
    return this.endpoint({
      url: 'forum/idea/addPost/' + ideaId,
      data: {
        kigaIdeaId: ideaId,
        content: content
      }
    });
  }

  setRating(id, rating) {
    return this.endpoint({
      url: 'idea/updateRating/',
      data: {
        ideaId: id,
        userRating: rating
      },
      quietMode: true
    });
  }

  clearRating(id) {
    return this.endpoint({
      url: 'idea/clearRating/',
      data: {
        ideaId: id
      },
      quietMode: true
    });
  }

  getPromotion(id) {
    return this.endpoint({
      url: 'promotion/',
      data: {
        pageId: id
      },
      quietMode: true
    });
  }

  getRating(id) {
    return this.endpoint({
      method: 'GET',
      url: `idea/rating/${id}`,
      quietMode: true
    });
  }
}
