import { StateService } from 'angular-ui-router';
import { IConfigService } from '../../../misc/js/services/AngularInterfaces';
/**
 * this service retrieves the url for an idea with opened ideaPreview
 * in a ideas overview screen.
 */

export default class IdeaOverviewIdeaUrlRetriever {
  constructor(private config: IConfigService, private $state: StateService) {}

  public getUrl(title: string) {
    this.config.getNgLiveUrl() + this.$state.href('root.cms', this.getParameters(title));
  }

  private getParameters(title) {
    return {
      path: '',
      preview: title
    };
  }
}
