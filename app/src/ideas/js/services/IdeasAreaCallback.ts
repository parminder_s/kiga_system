/**
 * The IdeasAreaCallback automatically goes to the search module if the searchbar contains
 * text, otherwise redirects to the browsing functionality.
 */
import { AreaCallback } from '../../../area/js/AreaCallback';
import { StateService } from 'angular-ui-router';

export default class IdeasAreaCallback implements AreaCallback {
  constructor(
    private links,
    private request,
    private $location,
    private $state: StateService,
    private locale,
    private config
  ) {}

  handle(searchTags: Array<string>, ageGroup: number) {
    if (searchTags != null && searchTags.length !== 0) {
      let params = { query: searchTags, parent: 0 };

      if (this.$state.includes('root.searchStatic')) {
        params['parent'] = this.$state.params['parent'] || 0;
      }

      if (ageGroup || ageGroup === 0) {
        params['ageGroup'] = ageGroup;
      }
      params['page'] = 1;
      this.$state.go('root.searchStatic', params, { reload: true });
    } else {
      let ideaLink = this.links.findByCode('ideaGroupMainContainer').url;
      let ideas = window.location.href.indexOf(ideaLink);
      if (ideas >= 0) {
        this.$state.params['ageGroupFilter'] = ageGroup;
        this.$state.params['preview'] = null;
        this.$state.go('.', this.$state.params, { reload: true });
      } else {
        window.location.href = ideaLink + '?ageGroupFilter=' + ageGroup;
      }
    }
  }
}
