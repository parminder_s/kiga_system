import { IConfigService } from '../../../misc/js/services/AngularInterfaces';

export default class IdeaPermissionHandler {
  constructor(private member, private config: IConfigService) {}

  hasPermission(requiresFullSubscription: boolean, isFree: boolean = false) {
    let memberData = this.member.getAll();
    if (isFree) {
      return true;
    }
    if (memberData.isCms) {
      return true;
    }

    if (memberData.isAnonymous) {
      return false;
    }

    if (requiresFullSubscription && !memberData.subscription.accessPoolIdeas) {
      return false;
    }

    if (!requiresFullSubscription && !memberData.subscription.accessNewIdeas) {
      return false;
    }

    return true;
  }
}
