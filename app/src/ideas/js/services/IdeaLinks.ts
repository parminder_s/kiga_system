import { IdeaLink } from './IdeaLink';
export interface IdeaLinks {
  edit: IdeaLink;
  planner: IdeaLink;
  view: IdeaLink;
  print: IdeaLink;
  printWithoutImage: IdeaLink;
  favouritesDenied: IdeaLink;
}
