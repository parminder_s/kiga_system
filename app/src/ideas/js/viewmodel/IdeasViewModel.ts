import ViewModel from '../../../cms/js/ViewModel';
/**
 * Created by bbs on 12/3/16.
 */

export interface IdeasViewModel extends ViewModel {
  categories: Array<any>;
}
