import ToastService from '../../../misc/toast/ToastService';

export default class IdeaLinkCopyClipboardButtonController {
  private url: string;

  constructor(private toastService: ToastService, private clipboard: any) {}

  copyLinkToClipboard() {
    this.clipboard.copyText(this.url);
    this.toastService.success('Link in Zwischenablage kopiert');
  }
}
