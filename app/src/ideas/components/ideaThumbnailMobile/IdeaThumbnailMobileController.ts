import { StateService } from 'angular-ui-router';
import { FavouritesModel } from '../../../misc/js/model/FavouritesModel';
import IdeaLinksResolver from '../../js/services/IdeaLinksResolver';

export default class IdeaThumbnailMobileController {
  private idea: any;
  private viewLink: string;

  constructor(
    private ideaLinksResolver: IdeaLinksResolver,
    private $state: StateService,
    private favouritesModel: FavouritesModel
  ) {}

  $onInit() {
    this.viewLink = this.ideaLinksResolver.getLinks(this.idea).view.link;
  }

  toggleFavourite() {
    let ideaLinks = this.ideaLinksResolver.getLinks(this.idea);
    if (ideaLinks.favouritesDenied.show) {
      this.$state.go(ideaLinks.favouritesDenied.link);
    } else {
      this.idea.favourite = !this.idea.favourite;
      this.favouritesModel.toggleFavourite(this.idea.id);
    }
  }
}
