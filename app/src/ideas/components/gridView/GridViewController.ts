import * as _ from 'lodash';
import { ItemThumbnail } from '../itemThumbnail/ItemThumbnailController';

interface GridViewItem {
  id: number;
  title: string;
}

/**
 * this component displays items in a grid. As we currently
 * can't have automatic line-breaks, we have to implement 5 different
 * views with 3 to 7 items per row.
 * We create all 5 views and are showing only one view mode by using bootstrap.
 * The item-preview is placed below each row (in each view mode).
 */
export default class GridViewController {
  private items: Array<any>;
  private itemThumbnails: Array<ItemThumbnail> = [];
  private onClick: (value: { id: number }) => void;
  private onHeartClick: (value: { id: number }) => void;
  private activeItem: GridViewItem = null;
  private activeIndex: number = -1;
  private activeItemId: number = -1; // input parameter

  private viewModes: Array<{ itemsPerRow: number; cssClass: string }> = [
    { itemsPerRow: 3, cssClass: 'visible-md' },
    { itemsPerRow: 4, cssClass: 'visible-lmd' },
    { itemsPerRow: 5, cssClass: 'visible-lg' },
    { itemsPerRow: 6, cssClass: 'visible-xlg' },
    { itemsPerRow: 7, cssClass: 'visible-xxlg' }
  ];

  constructor() {}

  $onChanges() {
    this.itemThumbnails = _.map(this.items, this.mapToItemThumbnail);

    if (this.activeItemId === -1) {
      this.closePreview();
    } else {
      this.openPreview(this.activeItemId);
    }
  }

  public handleClick(id: number) {
    if (this.activeItem && this.activeItem.id === id) {
      this.closePreview();
    } else {
      this.openPreview(id);
    }
  }

  public openNextPreview() {
    if (this.activeIndex < this.items.length - 1) {
      this.activeIndex++;
      this.activeItem = this.items[this.activeIndex];
    }
  }

  public openPreviousPreview() {
    if (this.activeIndex > 0) {
      this.activeIndex--;
      this.activeItem = this.items[this.activeIndex];
    }
  }

  public handleHeartClick(id: number) {
    this.onHeartClick({ id: id });
  }

  /**
   * used by the partial as criteria for enabling
   * the different view modes.
   */
  public showPreviewPanel(itemsPerRow: number, index: number) {
    return (
      this.showLineBreak(itemsPerRow, index) &&
      this.isActiveIndex() &&
      this.isActiveItemInIndexRow(itemsPerRow, index)
    );
  }

  public showLineBreak(itemsPerRow: number, index: number) {
    return this.isLastItemInRow(itemsPerRow, index) || this.isLastIndex(index);
  }

  private mapToItemThumbnail(item): ItemThumbnail {
    return {
      id: item.id,
      title: item.title,
      description: item.description,
      imageUrl: item.previewUrl,
      hasHeart: item.favourite
    };
  }

  private isActiveIndex() {
    return this.activeIndex > -1;
  }

  private isLastItemInRow(itemsPerRow: number, index: number) {
    return (index + 1) % itemsPerRow === 0;
  }

  private isActiveItemInIndexRow(itemsPerRow: number, index: number) {
    let indexRow = Math.floor(index / itemsPerRow);
    let activeItemRow = Math.floor(this.activeIndex / itemsPerRow);
    return indexRow === activeItemRow;
  }

  private isLastIndex(index: number) {
    return index === this.items.length;
  }

  private find(id: number) {
    return this.items.find(item => item.id === id);
  }

  private findIndex(id: number) {
    return _.findIndex(this.items, item => item.id === id);
  }

  private openPreview(id: number) {
    this.activeItem = this.find(id);
    this.activeIndex = this.findIndex(id);
  }

  public closePreview() {
    this.activeItem = null;
    this.activeIndex = -1;
  }
}
