import { IdeaLinks } from '../../js/services/IdeaLinks';
import IdeaLinksResolver from '../../js/services/IdeaLinksResolver';

interface PreviewPage {
  small: string;
  large: string;
  pageNumber: number;
}

interface Idea {
  id: number;
  previewPages: Array<PreviewPage>;
}

export default class IdeaPreviewController {
  private links: IdeaLinks;
  private idea: Idea;

  private onClose: () => void;
  private onNext: () => void;
  private onPrevious: () => void;

  private showNext: boolean;
  private showPrevious: boolean;
  private showClose: boolean;

  private activePreviewPage: PreviewPage;

  constructor(private ideaLinksResolver: IdeaLinksResolver) {}

  $onChanges() {
    this.links = this.ideaLinksResolver.getLinks(this.idea);
    this.activePreviewPage = this.idea.previewPages[0];
  }

  public selectPreviewPage(previewPage: PreviewPage) {
    this.activePreviewPage = previewPage;
  }

  public handleClose() {
    this.onClose();
  }

  public handleNext() {
    this.onNext();
  }

  public handlePrevious() {
    this.onPrevious();
  }

  private toggleFavourite() {}
}
