export interface ItemThumbnail {
  id: number;
  title: string;
  description: string;
  imageUrl: string;
  hasHeart: boolean;
}

export default class ItemThumbnailController {
  private onHeartClick: (value: { id: number }) => void;
  private onClick: (value: { id: number }) => void;
  private item: ItemThumbnail;

  constructor() {}

  $onInit() {}

  handleHeartClick() {
    this.onHeartClick({ id: this.item.id });
  }

  handleClick() {
    this.onClick({ id: this.item.id });
  }
}
