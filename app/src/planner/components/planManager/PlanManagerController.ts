import * as moment from 'moment';
import PermissionStateResolver from '../../../misc/permission/js/PermissionStateResolver';

export default class PlanManagerController {
  private plan;
  private week;
  private idOfItem: number;
  private today: number;
  private currentWeek: number;
  private allowNewPlan: boolean;
  private isKigaPlan: string;
  private isImportKigaPlan: boolean;
  private dateIsInPast: boolean;

  private ItemCountPerDay = {};
  private color;

  constructor(
    private planPermission,
    private request,
    private $stateParams,
    private $state,
    private planModel,
    private reservedPlanDate,
    private permissionStateResolver: PermissionStateResolver
  ) {}

  $onInit() {
    this.color = this.plan.plan.color;
    const now = moment();
    this.today = now.date();
    this.currentWeek = now.week();
    this.allowNewPlan = this.getAllowNewPlan();
    this.calcItemsPerDay();
    this.dateIsInPast = this.planDateInPast();
    if (
      this.$state.is('root.plan.planType.addToCalendar') &&
      this.$stateParams.planType == 'kiga'
    ) {
      this.isImportKigaPlan = true;
    }
  }

  planDateInPast() {
    const weekStart = parseInt(this.week.weekNr);
    return weekStart < this.currentWeek;
  }

  calcItemsPerDay() {
    function getClassesForItems(numberOfIdeas) {
      const activated = 'activated';
      const deactivated = 'deactivated';
      const classesForItems = {
        '1': [activated, deactivated, deactivated, deactivated, deactivated, deactivated],
        '2': [activated, activated, deactivated, deactivated, deactivated, deactivated],
        '3': [activated, activated, activated, deactivated, deactivated, deactivated],
        '4': [activated, activated, activated, activated, deactivated, deactivated],
        '5': [activated, activated, activated, activated, activated, deactivated],
        '6': [activated, activated, activated, activated, activated, activated],
        default: [deactivated, deactivated, deactivated, deactivated, deactivated, deactivated]
      };
      return classesForItems[numberOfIdeas];
    }

    if (this.plan.plan != 'empty') {
      let tempPlannedDays = [];
      for (let itemIndex in this.plan.plan.items) {
        if (this.plan.plan.items.hasOwnProperty(itemIndex)) {
          tempPlannedDays[itemIndex] = moment(this.plan.plan.items[itemIndex].plannedDate).date();
        }
      }
      for (let day in this.plan.days) {
        if (this.plan.days.hasOwnProperty(day)) {
          const date = this.plan.days[day].dayNr;
          let counter = 0;
          this.ItemCountPerDay[date] = getClassesForItems('default');
          tempPlannedDays.forEach(value => {
            if (date == value) {
              counter++;
              this.ItemCountPerDay[date] = getClassesForItems(counter);
            }
          });
        }
      }
    }
  }

  private getAllowNewPlan() {
    const planType = this.planPermission.planType();
    if (planType === 'member') {
      return this.planPermission.isAllowedMember();
    } else {
      this.isKigaPlan = this.planPermission.planType();
      return this.planPermission.isCmsMember();
    }
  }

  addKigaIdea(day: number) {
    if (this.allowNewPlan && this.idOfItem && this.plan.plan.id) {
      Object.keys(this.plan.days).forEach((key, index) => {
        if (this.plan.days[key].dayNr == day) {
          let weekNr = this.week.weekNr;
          if (index == 6) {
            ++weekNr;
          }
          let date = moment()
            .isoWeekday(index + 1)
            .week(weekNr);
          let url = 'planner/' + this.$stateParams.planType + '/addItem';
          let data = {
            planId: this.plan.plan.id,
            itemId: this.idOfItem,
            plannedDate: date
          };
          return this.request({ url: url, data: data }).then(() => {
            this.$state.go('root.plan.planType.detail', { planId: this.plan.plan.id });
          });
        }
      });
    } else if (this.plan.plan.id) {
      this.goToDetail();
    }
  }

  goToDetail() {
    if (!this.isImportKigaPlan) {
      this.$state.go('root.plan.planType.detail', { planId: this.plan.plan.id });
    }
  }

  redirectToLogin() {
    let loginState = this.permissionStateResolver.resolvePermissionDenied(true);
    if (loginState === 'root.login') {
      this.$state.go(
        loginState,
        { forward: this.$state.href(this.$state.current.name, this.$state.params) },
        { location: 'replace' }
      );
    } else {
      this.$state.go(loginState, {}, { location: 'replace' });
    }
  }

  addPlan() {
    this.reservedPlanDate.setDateForPlan(this.week.date.startOf('isoWeek').toDate());
    this.$state.go('root.plan.planType.add');
  }

  importKigaPlan() {
    if (this.isImportKigaPlan) {
      let kigaPlanToImportPromise = this.planModel.find(
        this.$stateParams.itemId,
        this.$stateParams.planType
      );
      kigaPlanToImportPromise.then(response => {
        response.planDate = this.week.date.startOf('isoWeek').toDate();
        response.color = 'lightGreen';
        response.icon = 'sticker-leaf6';
        let copyPromise = this.planModel.copy(response, 'kiga');
        copyPromise.then(newResponse => {
          this.$state.go('root.plan.planType.detail', {
            planId: newResponse.id,
            planType: 'member'
          });
        });
      });
    }
  }
}
