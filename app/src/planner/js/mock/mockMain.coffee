define ['../app', 'jquery', './mockConfigs'], (app, $, configs) ->
  app.factory 'plannerMock', ['misc.mockHelper'].concat (mock) ->
    getUrl = (method) -> new RegExp "planner/\\w+/#{method}$"

    _.each configs, (val, key) ->
      if key.match /\//
        mock key, val
      else
        mock getUrl(key), val

  app.run ['plannerMock'].concat ->
