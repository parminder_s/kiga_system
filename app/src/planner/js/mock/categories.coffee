define [], ->
  [
    {id: 1, title: 'Mathematik', parentId: 0}
    {id: 2, title: 'Reisen', parentId: 0}
    {id: 3, title: 'Algebra', parentId: 1}
    {id: 4, title: 'Trigonometrie', parentId: 1}
    {id: 5, title: 'Abenteuer', parentId: 2}
    {id: 6, title: 'Nautik', parentId: 2}
  ]
