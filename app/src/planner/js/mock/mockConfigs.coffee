define ['jquery', 'lodash', 'angular', './fixtures', './categories', './tags',
'moment'], ($, _, angular, fixtures, categories, tags, moment) ->
  getPlans = (url) ->
    if url.match /kiga/
      fixtures.kigaPlans
    else
      fixtures.memberPlans

  returner =
    find: (data, url) ->
      plans = getPlans url
      returner = if data.planId is 'current'
        plans[0]
      else _.find plans, (p) ->
        p.id is parseInt(data.planId)
      ix = _.indexOf(plans, returner)
      if returner
        returner.nextPlanId = if ix >= plans.length - 1 then 0
        else plans[ix + 1].id
        returner.prevPlanId = if ix is 0 then 0 else plans[ix - 1].id
      returner

    findByYear: (data, url) ->
      plans = getPlans(url)
      year = data.year
      yearStart = moment [year, 0, 1]
      yearEnd = moment [year, 11, 31]
      _.filter plans, (plan) ->
        moment(plan.startDate).isBefore(yearEnd) and
          moment(plan.endDate).isAfter(yearStart)

    findByMonth: (data, url) ->
      plans = getPlans(url)
      year = data.year
      monthStart = moment [year, data.month - 1, 1]
      monthEnd = moment(monthStart).endOf 'month'
      _.filter plans, (plan) ->
        moment(plan.startDate).isBefore(monthEnd) and
          moment(plan.endDate).isAfter(monthStart)

    findAll: (data, url) -> getPlans url

    add: (data, url) ->
      plans = getPlans url
      maxId = _.max _.map plans, (plan) -> plan.id
      maxId = 0 unless maxId

      plannedDate = moment data.plannedDate
      plan = $.extend data,
        id: maxId + 1
        startDate: moment(plannedDate).startOf('isoWeek').format 'YYYY-MM-DD'
        endDate: moment(plannedDate).startOf('isoWeek').add('days', 7)
          .format 'YYYY-MM-DD'
        type: if url.match /kiga/ then 'kiga' else 'member'
        comments: []
        items: []

      plans.push plan
      status: true, result: plan

    addItem: (data, url) -> planId: data.planId

    save: (data, url) ->
      plans = getPlans(url)
      ix = 0
      _.each plans, (plan, i) ->
        ix = i if plan.id is data.id
      plans[ix] = data
      plans[ix]

    remove: (data, url) ->
      plans = getPlans url
      plans = _.filter plans, (plan) ->
        plan.id isnt parseInt(data.planId)
      if __matches = url.match(/kiga/)
        fixtures.kigaPlans = plans
      else
        fixtures.memberPlans = plans
      true

    changeDate: (data, url) ->
      newDate = moment data.newDate
      plans = getPlans url
      plan = _.find (plans), (plan) -> plan.id is data.planId
      item = _.find (plan.items), (item) -> item.id is data.itemId
      if newDate.isAfter moment plan.endDate
        plan.endDate = moment(newDate).endOf 'isoWeek'
      item.plannedDate = newDate.format 'YYYY-MM-DD'
      plan

    removeItem: (data, url) ->
      plans = getPlans url
      plan = _.find (plans), (plan) -> plan.id is data.planId
      plan.items = _.filter plan.items, (item) -> item.id isnt data.itemId
      plan

    copy: (data, url) ->
      kigaPlan = _.find fixtures.kigaPlans, (plan) ->
        plan.id is data.id
      maxId = _.max _.map fixtures.memberPlans, (plan) ->
        plan.id
      plan = angular.copy(kigaPlan)

      $.extend plan,
        id: maxId + 1
        type: 'member'
        color: data.color
        icon: data.icon
        startDate: data.planDate
        endDate: moment(data.planDate).add(moment(plan.endDate).
        diff(plan.startDate)).format 'YYYY-MM-DD'
        title: data.title

      fixtures.memberPlans.push plan
      plan

    'planner/member/saveComment': (data, url) ->
      plan = _.find fixtures.memberPlans, (p) -> p.id is parseInt(data.planId)
      plan

    'planner/planPageUrl': '/de/meine-planung'
    'main/findCategories': categories
    'main/findTags': tags

    'planner/member/addWeek': (data, url) ->
      plan = _.find fixtures.memberPlans, (p) -> p.id is data.planId
      plan.endDate = moment(plan.endDate).add('days', 7).format 'YYYY-MM-DD'
      plan

    'planner/member/removeWeek': (data, url) ->
      plan = _.find fixtures.memberPlans, (p) -> p.id is data.planId
      plan.endDate = moment(plan.endDate).add('days', -7).format 'YYYY-MM-DD'
      plan
