define [], ->
  1:
    kigaPageId: 1
    type: 'article'
    title: 'Ice Scating on the ocean'
    description: 'During winter one can slide slowly and safely'
    previewUrl: 'planner/js/mock/preview1.jpg'
    ageGroup: 4
    link: '/articles/1'
    categoryL1Name: 'Sports'
    categoryL1Link: 'http://irgendwo'
    categoryL2Name: 'Activity'
    categoryL2Link: 'http://irgendwo'

  2:
    kigaPageId: 2
    type: 'article'
    title: 'Überlebenstraining'
    description: "Für mutige Kinder gibt es ein
      1-wöchiges Überlebenstraining in der Wüste Gobi."
    previewUrl: 'planner/js/mock/preview2.jpg'
    ageGroup: 5
    link: '/articles/1'
    categoryL1Name: 'Mathematik'
    categoryL1Link: 'http://irgendwo'
    categoryL2Name: 'Zählen'
    categoryL2Link: 'http://irgendwo'

  3:
    kigaPageId: 3
    type: 'article'
    title: 'Tulpenspiel'
    description: 'We are doing an excursion in the Netherlands during May'
    previewUrl: 'planner/js/mock/preview3.jpg'
    ageGroup: 3
    link: '/articles/1'
    categoryL1Name: 'Reisen'
    categoryL1Link: 'http://irgendwo'
    categoryL2Name: 'Europa'
    categoryL2Link: 'http://irgendwo'

  4:
    kigaPageId: 4
    type: 'article'
    title: 'Segeltörn Nordwestpassage - Grönland nach Alaska'
    previewUrl: 'planner/js/mock/preview4.jpg'
    ageGroup: 3
    link: '/articles/1'
    categoryL1Name: 'Reisen'
    categoryL1Link: 'http://irgendwo'
    categoryL2Name: 'Europa'
    categoryL2Link: 'http://irgendwo'

  5:
    kigaPageId: 5
    type: 'article'
    title: 'Programming game in Assembler Z80'
    description: 'Children should make first experiences with programming'
    previewUrl: 'planner/js/mock/preview5.jpg'
    ageGroup: 6
    link: '/articles/1'
    categoryL1Name: 'Mathematik'
    categoryL1Link: 'http://irgendwo'
    categoryL2Name: 'Programmieren'
    categoryL2Link: 'http://irgendwo'
