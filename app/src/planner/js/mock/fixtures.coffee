define ['moment','jquery', './articles'], (moment, $, articles ) ->
  getDate = (interval, number) ->
    interval = 'days'  unless interval?
    number = 0  unless number?
    moment(weekStart).add(interval, number).format 'YYYY-MM-DD'
  createKigaPlanItems = (week, articleIds) ->
    _.map articleIds, (articleId, ix) ->
      $.extend {}, articles[articleId],
        id: ix + 1
        plannedDate: moment(week).add('days', ix).format('YYYY-MM-DD')


  sortFn = (p1, p2) ->
    p1End = moment(p1.endDate)
    p2End = moment(p2.endDate)
    if p1End.isBefore(p2End)
      -1
    else if p1End.isAfter(p2End)
      1
    else
      p1Start = moment(p1.startDate)
      p2Start = moment(p2.startDate)
      if p1Start.isBefore(p2.startDate)
        -1
      else
        1

  weekStart = moment().startOf 'isoWeek'
  memberPlans = [
    {
      id: 1
      title: 'Mein erster Plan'
      description: 'Das machen wir jetzt mal'
      color: 'lightGreen'
      icon: 'sticker-drop'
      startDate: getDate 'weeks', 1
      endDate: getDate 'weeks', 2
      items: [
        $.extend({}, articles['1'],
          id: 1
          position: 2
          itemType: 'kigaIdea',
          plannedDate: getDate 'weeks', 1
        )
        $.extend({}, articles['2'],
          id: 2
          itemType: 'kigaIdea'
          plannedDate: getDate 'days', 8
        )
        $.extend({}, articles['3'],
          id: 3
          itemType: 'kigaIdea',
          plannedDate: getDate 'days', 10
        )
        $.extend({}, articles['1'],
          id: 4
          itemType: 'kigaIdea'
          plannedDate: getDate 'days', 10
        )
        {
          id: 5
          itemType: 'comment'
          position: 1
          plannedDate: getDate 'weeks', 1
          comment: 'This is a very long comment, which has also a very long
                    word that comes right now:
                    thisisaverylongwordjusttoseeiftherowdoesntexpand'
        }

      ]
    }, {
      id: 2
      title: 'Zweiwöchiger Plan'
      description: 'Dieser Plan dauert zwei Wochen'
      color: 'amber'
      icon: 'sticker-banner'
      startDate: getDate('weeks', -1)
      endDate: getDate('weeks', 1)
      items: [
        $.extend({}, articles['1'],
          id:1
          itemType: 'kigaIdea'
          plannedDate: getDate 'days', -7
        )
        $.extend({}, articles['1'],
          id:2
          itemType: 'kigaIdea'
          plannedDate: getDate 'days', 4
        )
        $.extend({}, articles['2'],
          id:3
          itemType: 'kigaIdea'
          plannedDate: getDate 'days', 4
        )
        {
          id: 4
          itemType: 'comment'
          plannedDate: getDate 'days', 6
          comment: 'This is a very long comment, which has also a very long
                    word that comes right now:
                    thisisaverylongwordjusttoseeiftherowdoesntexpand'
        }
      ]
    }, {
      id: 3
      title: 'Jahresplan'
      color: 'deepPurple'
      icon: 'sticker-leaf'
      startDate: getDate 'weeks', -2 * 4
      endDate: getDate 'weeks', 5 * 4
      items: []
    }
  ]
  kigaPlans = [
    {
      id: 1
      title: 'KW vorbei'
      previewItemId: 4
      description: 'KiGaPlan der ersten Woche'
      published: true
      startDate: getDate('weeks', -1)
      endDate: getDate()
      items: createKigaPlanItems getDate('weeks', -1), [1, 2, 3, 4]
    }, {
      id: 2
      title: 'KW aktuell'
      previewItemId: 0
      description: 'aktueller KiGaPlan'
      published: true
      startDate: getDate()
      endDate: getDate('weeks', 1)
      items: createKigaPlanItems getDate(), [5, 4, 3, 2, 1]
    }, {
      id: 3
      title: 'KW Zukunft'
      description: 'KiGaPlan für die darauffolgende Woche'
      published: true
      startDate: getDate('weeks', 1)
      endDate: getDate('weeks', 2)
      items: createKigaPlanItems getDate('weeks', 1), [5, 1, 3, 4, 2]
    }, {
      id: 4
      title: 'KW leer'
      description: 'Leerer KiGa Plan'
      published: false
      startDate: getDate 'weeks', 2
      endDate: getDate 'weeks', 3
      items: []
    }
  ]
  _.each kigaPlans, (kp) ->
    kp.type = 'kiga'

  _.each memberPlans, (kp) ->
    kp.type = 'member'

  memberPlans.sort sortFn
  kigaPlans.sort sortFn

  memberPlans: memberPlans
  kigaPlans: kigaPlans
