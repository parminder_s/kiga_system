###*
  @ngdoc controller
  @name planner.detailCtrl
  @description
  The controller for plan detailed view.
###

define ['../app', 'angular', 'lodash', 'moment'], (app, angular, _, moment) ->
  app.controller 'planner.detailCtrl', ($scope, $stateParams, $state,
  planModel, member, infoDlg, confirmerDlg, planMenu,
  planPermission, planConfig, planDragDrop, planUiLogic, plan,
  planNlDecorator, permissionStateResolver, reserveIdeaDateForPlan) ->
    planMenu.show()
    angular.extend $scope, planUiLogic,
      plan: plan
      planType: $stateParams.planType
      noPlanAvailable: !plan and planPermission.allowMemberDetail()
      showCopy: -> planPermission.showCopy()
      $scope.$parent.$parent.$parent.hideSubMenu = false
      allowPrintSelection: -> planPermission.allowPrintSelection()
      cmsOrSubscription: -> planPermission.cmsOrSubscription()
      isCmsMember: -> planPermission.isCmsMember()
      isMemberPlan: -> $scope.planType is 'member'
      showNlSelect: -> planPermission.showNlSelect()
      updateNlIndicator: -> planNlDecorator.updateNlIndicator(plan)
      linkItemToIdea: -> planPermission.linkItemToIdea()
      isDropping: false
      printOverview: ->
        if @allowPrintSelection()
          url = "#{location.protocol}//#{window.location.hostname}"
          url += "/api/planner/#{$scope.planType}/printOverview"
          url += "?planId=#{$scope.plan.id}&locale=#{$stateParams.locale}"
          document.location.href = url
        else
          @showPrintSelectionInfo()
      addKigaIdea: (day) ->
        reserveIdeaDateForPlan.setReservedDate(day.date.toDate(), plan.id)
        reserveIdeaDateForPlan.redirectToKigaIdeas()

      editComment: (comment) ->
        if !$scope.isDropping
          $state.go 'root.plan.planType.editComment',
            commentId: comment.id
      allowCopy: -> planPermission.allowCopy()
      showCopyInfo: -> $state.go permissionStateResolver.resolvePermissionDenied true
      copyPlanToCalendar: ->
        promise = planModel.find $stateParams.planId, $stateParams.planType
        promise.then (response) =>
          $state.go 'root.plan.planType.addToCalendar',
            {itemId: response.id}, {location: 'replace'}
      showPrintSelectionInfo: ->
        $state.go permissionStateResolver.resolvePermissionDenied false
      allowDrop: planDragDrop.allowDrop
      allowDrag: planDragDrop.allowDrag
      itemDrop: (item, day, position) ->
        position = position ? -1
        if planDragDrop.checkDrop item, day, position
          $scope.isDropping = true
          changeData =
            itemId: item.id, planId: plan.id, newDate: day
            position: position, itemType: item.itemType,
            planType: $scope.planType
          planModel.changeItemDate(changeData).then (plan) ->
            $scope.plan = plan
      addWeek: ->
        planModel.addWeek $scope.plan.id, (plan) ->
          $scope.plan = plan
      removeWeek: ->
        planModel.removeWeek $scope.plan.id, (plan) ->
          $scope.plan = plan
      removeItem: (item) ->
        confirmerDlg 'PLAN_ITEM_REMOVE_QUESTION', ->
          planModel.removeItem($scope.plan.id, item.id, $scope.planType).then ->
            $state.reload()
      addPlan: ->
        $state.go 'root.plan.planType.add'
      viewItem: (item) -> window.location.href = item.link
      showIdeaOrComment: (day) ->
        $scope.showAddIdea(day) and $scope.showAddComment(day)
      showAddIdea: (day) ->
        $scope.cmsOrSubscription() and
        !day.isInPast and
        day.items.length < planConfig.maxItemsPerDay
      showAddComment: (day) ->
        $scope.planType is 'member' and
        !day.isInPast and
        day.items.length < planConfig.maxItemsPerDay
      showRemoveWeek: ->
        $scope.plan.weeks?.length > 1 and
        _.last($scope.plan.weeks).maxItems is 0
      setNlPosition: (item, position) ->
        if plan.selectedNl
          planModel.setNlPosition(item.kigaPageId, position, plan.id,
          plan.selectedNl).then (data) ->
            plan.selectedNl.ideas = data
            plan.newsletters[plan.selectedNl.index].ideas = data
            planNlDecorator.updateNlIndicator(plan)

    findItemById = (id) ->
      _.head _.filter(plan.items, (item) -> item.id == id)

    if planPermission.allowMemberDetail()
      if !$scope.plan
        $scope.noPlanAvailable = true

    actualizeView = (result) ->
      if result is 'saved'
        $scope.plan = {}
        $state.reload()
      else if result is 'removed'
        if $stateParams.planId is 'current'
          $scope.plan = {}
          $state.reload()
        else
          $state.go '.', planId: 'current'


