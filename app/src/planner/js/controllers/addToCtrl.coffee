define ['../app', 'lodash', 'moment'], (app, _, moment) ->
  app.controller 'planAddToCtrl', ($stateParams, $state, plans,
    reserveIdeaDateForPlan, request, config) ->
    new class PlanAddToCtrl
      constructor: ->
        if reserveIdeaDateForPlan.checkIfDateIsReserved()
          reservedData = reserveIdeaDateForPlan.getKigaIdeaAndPlan()
          reserveIdeaDateForPlan.removeReservation();
          request({
            url: 'planner/' + $stateParams.planType + '/addItem'
            data:
              planId: reservedData.planId
              itemId: $stateParams.itemId
              plannedDate: moment reservedData.date
          }).then ->
           $state.go 'root.plan.planType.detail',
              {planId: reserveIdeaDateForPlan.planId}, {location: 'replace'}
        else
          $state.go 'root.plan.planType.addToCalendar',
            {itemId: $stateParams.itemId}, {location: 'replace'}
