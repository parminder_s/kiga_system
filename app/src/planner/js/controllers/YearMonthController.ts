import { StateParams, StateService } from 'angular-ui-router';
import * as moment from 'moment';
import { Feature, IConfigService } from '../../../misc/js/services/AngularInterfaces';

export default class YearMonthController {
  private allowPrev: boolean;
  private allowNext: boolean;
  private allowNewPlan: boolean;
  private data: any;
  private title: string;

  constructor(
    private $stateParams: StateParams,
    private $state: StateService,
    private planModel: any,
    private planPermission,
    private config: IConfigService,
    private $scope
  ) {}

  private $onInit() {
    this.allowNewPlan = this.getAllowNewPlan();

    let planType = this.planPermission.planType();
    let year = this.$stateParams.year;
    let currentYear = moment().year();
    if (planType === 'member' && !this.planPermission.allowMemberDetail()) {
      if (this.$state.is('root.plan.planType.monthly')) {
        this.handleMonth(this.$stateParams.month, year, currentYear, planType);
      }
    } else if (planType === 'kiga' && this.$state.is('root.plan.planType.addToCalendar')) {
      this.handleMonth(this.$stateParams.month, year, currentYear, 'member');
      this.$scope.$parent.$parent.$parent.hideSubMenu = true;
    } else {
      if (this.$state.is('root.plan.planType.monthly')) {
        this.handleMonth(this.$stateParams.month, year, currentYear, planType);
      }
      if (this.$state.is('root.plan.planType.yearly')) {
        this.handleYear(year, planType);
      }
      if (this.$state.is('root.plan.planType.addToCalendar')) {
        this.handleMonth(this.$stateParams.month, year, currentYear, planType);
      }
    }
  }

  private handleYear(year, planType) {
    if (this.$stateParams.year) {
      this.allowNext = year <= moment().year();
      this.allowPrev = year > 2015;
      this.planModel.findByYear(year, planType, data => {
        this.data = data;
        this.title = year;
      });
    }
  }

  private getAllowNewPlan() {
    let planType = this.planPermission.planType();

    if (planType === 'member') {
      return this.planPermission.isAllowedMember();
    } else {
      return this.planPermission.isCmsMember();
    }
  }

  private handleMonth(month, year, currentYear, planType) {
    this.allowNext = year <= currentYear || (year === currentYear + 1 && month < 12);
    this.allowPrev = year > 2015 || (year === 2015 && month > 1);
    this.planModel.findByMonth(month, year, planType, data => {
      this.data = data;
      this.title = data.months[0].name;
    });
  }
}
