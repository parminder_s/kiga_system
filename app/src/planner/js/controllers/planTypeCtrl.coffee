define ['../app', 'angular', 'moment'], (app, angular, moment) ->
  app.controller 'planTypeCtrl', ($stateParams, $state, member,
  planMenu, config, areaFetcher, planPermission) ->
    new class PlanTypeCtrl
      constructor: ->
        @setSubMenu()
        if $state.is 'root.plan.planType'
          if @showWeekly()
            $state.go '.weekly', {}, {location: 'replace'}
          else
            $state.go '.monthly',
              {month: @month, year: @year}, {location: 'replace'}
      showWeekly: -> config.get('planVersion') is 4
      isSevenColumns: -> $state.is 'root.plan.planType.detail'
      isCmsMember: ->  planPermission.isCmsMember()
      isMemberPlan: -> $state.params.planType is 'member'
      showMenu: ->
        if ($state.is 'root.plan.planType.addToCalendar')
          !planMenu.isShown()
        else
          planMenu.isShown()
      showPlanTypeTab: -> !member.isCms
      planType: $state.params.planType
      month: moment().month() + 1
      year: moment().year()
      setSubMenu: ->
        memberLink = $state.href 'root.plan.planType.monthly',
          {planType: 'member', month: @month, year: @year}

        kigaLink = $state.href 'root.plan.planType.detail',
          {planType: 'kiga', planId: 'current'}

        cls = 'sub-menu-planner'

        if @showPlanTypeTab() and @showMenu()
          areaFetcher.getArea().withSubMenus [
            {label: 'PLAN_PLURAL_MEMBER', link: memberLink,
            cls: cls, isActive: $stateParams.planType is 'member'}
            {label: 'PLAN_PLURAL_KIGA', link: kigaLink,
            cls: cls, isActive: $stateParams.planType is 'kiga'}
          ]
