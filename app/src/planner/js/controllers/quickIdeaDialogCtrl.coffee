define ['../app', 'angular'], (app, angular) ->
  MAX_FILES = 5
  ($stateParams, $state, $scope) ->
    angular.extend $scope,
      showAllErrors: false
      formData:
        id: null
        title: ''
        description: ''
        tags: null
        educationSector: ''
        files: []
      educationSectors:
        Sector1:
          ['S1 category1', 'S1 category2', 'S1 category3', 'S1 category4']
        Sector2:
          ['S2 category1', 'S2 category2']

      previewOptions:
        previewMaxWidth: 125
        previewMaxHeight: 110
        previewCrop: true
        maxFileSize: 1000000
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
      uniqueCode: "quick_idea_editor"

      onFileAdd: (files) ->
        free = Math.max 0, MAX_FILES - $scope.formData.files.length
        files = files[0...free]
        $scope.formData.files.push files

      onPreviewAdd: (file) ->

    onRemoveFile: (file) ->

