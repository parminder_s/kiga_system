define ['../app', 'angular'], (app, angular) ->
  app.controller 'planCtrl', ($state) ->
    new class PlanCtrl
      constructor: ->
        if $state.is 'root.plan'
          $state.go '.planType', {planType: 'kiga'}, {location: 'replace'}
