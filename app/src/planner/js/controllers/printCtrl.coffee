define ['../app', 'lodash', '../../../misc/js/customCheckbox'], (app) ->
  app.controller 'planPrintCtrl', ($scope, $stateParams, plan) ->
    $scope.plan = plan
    $scope.plan.items = _.filter $scope.plan.items, (item) -> item.kigaPageId?

    uniqueItems = _.uniq $scope.plan.items, (item) ->
      item.kigaPageId.toString()

    $scope.items = _.fromPairs _.map uniqueItems, (item) ->
      [item.kigaPageId, true]

    $scope.itemGroups = _.groupBy uniqueItems, (item, ix) ->
      if ix % 2 is 0 then 'left' else 'right'

    $scope.click = (kigaPageId)->
      $scope.items[kigaPageId] = !$scope.items[kigaPageId]

    $scope.printSelection = ->
      itemIds = []
      for own itemId, selected of $scope.items
        if selected
          itemIds.push parseInt itemId, 10

      if itemIds.length
        ids = itemIds.join ','
        url = "#{location.protocol}//#{window.location.host}"
        url += "/api/planner/#{$scope.plan.type}/printSelection?ids=#{ids}"
        url += "&locale=#{$stateParams.locale}"
        document.location.href = url
