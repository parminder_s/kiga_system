define ['../app', 'angular'], (app, angular) ->
  app.controller 'planCommentCtrl', ($stateParams, $state, formHelper,
    planModel, action, comment, confirmerDlg, planMenu, planEditorProfile) ->
    new class CommentCtrl
      constructor: ->
        planMenu.hide()
        if action is 'edit'
          @comment = comment.comment
          @editMode = true
          @planId = comment.planId
        else
          @editMode = false
          @planId = $stateParams.planId
      comment: ''
      options: planEditorProfile
      submit: ->
        promise = if action is 'edit'
          planModel.saveComment comment.id, @comment
        else
          planModel.addComment(@planId, $stateParams.plannedDate, @comment)
        promise.then =>
          $state.go 'root.plan.planType.detail', planId: @planId
      cancel: -> formHelper 'root.plan.planType.detail', planId: @planId
      remove: ->
        confirmerDlg 'PLAN_COMMENT_REMOVE_QUESTION', =>
          planModel.removeComment(comment.id).then =>
            $state.go 'root.plan.planType.detail', planId: @planId
