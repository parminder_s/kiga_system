define ['../app', 'angular', 'lodash', 'moment'], (app, angular, _, moment) ->
  app.controller 'planFormCtrl', ($stateParams, $state, planModel, $q,
  colorSelector, iconSelector, confirmerDlg, now, $window, planMenu,
  previewSelector, plan, tagList, action, goBack, reservedPlanDate) ->
    new class PlanFormCtrl
      constructor: ->
        planMenu.hide()

        if plan
          if moment(plan.startDate).isBefore now(), 'isoWeek'
            @plan.planDate = new Date()
          else
            @plan.planDate = plan.startDate
          if @action is 'copy'
            angular.extend plan,
              color: 'lightGreen'
              icon: 'sticker-flower1'
        else
          if reservedPlanDate.getPlanDate()
            @planDate = reservedPlanDate.getPlanDate();
            reservedPlanDate.removePlannedDate();
          else
            @planDate = new Date();
          @plan =
            planDate: @planDate
            color: 'lightGreen'
            icon: 'sticker-flower1'
            syncEnabled: false
            isPublished: false
            atHomePage: false
            tags: []

      action: action
      plan: plan
      planType: $stateParams.planType

      selectColor: ->
        colorSelector (color) => @plan.color = color

      selectIcon: ->
        iconSelector (icon) => @plan.icon = icon

      showSyncEnabled: ->
        @planType is 'kiga' and $stateParams.locale isnt 'de'

      previewImageSrc: ->
        if @plan.previewItemId
          previewItem = _.find @plan.items, (item) =>
            item.kigaPageId is @plan.previewItemId
          previewItem.previewUrl if previewItem

      selectPreview: ->
        previewSelector(@plan).then (previewItemId) =>
          @plan.previewItemId = previewItemId

      save: ->
        @plan.tags = _.map @plan.tags, (tag) -> tag.text
        promise = if @action is 'edit'
          planModel.save @plan, @planType
        else if @action is 'copy'
          @planType = 'member'
          planModel.copy @plan, 'kiga'
        else
          planModel.add @plan, @planType

        promise.then (response) =>
          $state.go 'root.plan.planType.detail',
            planId: response.id
            planType: @planType

      cancel: -> goBack.goBack()

      deletePlan: ->
        confirmerDlg 'PLAN_REMOVE_QUESTION', =>
          planModel.remove @plan.id, @planType, =>
            $state.go 'root.plan.planType.detail', planType: @planType

      copyPlan: ->
        $state.go 'root.plan.copyPlan',
          planId: plan.id
          planType: plan.type

      completeTag: (query) ->
        deferred = $q.defer()
        deferred.resolve _.filter tagList, (tag) ->
          tag.toLowerCase().indexOf(query.toLowerCase()) > -1
        deferred.promise
