###*
  @ngdoc directive
  @name heightSetter
  @description

  This directive sets the css-attribute min-height of a div-element by using
  the actual height.

  This is needed for the drag & drop functionality since it does not have a
  cloning effect and would remove the complete idea/comment on dragging in
  the detailView which destroys the layout.

  For getting the height the first child is used as reference, since
  the actual element can get shrink during runtime which would not be
  tracked if min-height is already.
###

define ['../app', 'jquery'], (app, $) ->
  app.directive 'heightSetter', ($timeout) ->
    link: ($scope, element) ->
      imgRegistered = false
      setHeight = ->
        if element.is ':visible'
          child = $ element.children(':first')
          child.css 'overflow', 'auto'
          element.css 'min-height', "#{child.outerHeight(true)}px"

      $(window).resize -> setHeight()

      $scope.$on 'draggable:preStart', (event, obj) ->
        if element.has obj.element
          setHeight()
