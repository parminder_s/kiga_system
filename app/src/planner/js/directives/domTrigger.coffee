###*
  @ngdoc directive
  @name domTrigger
  @description

  This plugin just triggers the event domChanged on the window object.

  This is useful in cases of angularJs where it cannot be declared when
  partials are finally loaded and there are some other plugins that
  depend on them.
###

define ['../app', 'jquery'], (app, $) ->
  app.directive 'domTrigger', ->
    link: ($scope, element) ->
      $(window).trigger 'domChanged', element
