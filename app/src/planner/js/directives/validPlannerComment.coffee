###*
  @ngdoc directive
  @name validPlannerComment
  @module planner

  @description
  validator for plan comments. Uses @planCommentLength which takes
  line breaks into consideration.
###

define ['../app'], (app) ->
  app.directive 'validPlannerComment', (planCommentLength) ->
    require: 'ngModel'
    restrict: 'A'
    link: (scope, el, atts, ngModel) ->
      ngModel.$validators.validPlannerComment = (modelValue) ->
        planCommentLength(modelValue) <= 196
