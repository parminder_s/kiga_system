define ['angular',
  './services/PlanIdeaLinkResolver', './services/TinymceLoader', './services/ReservedPlanDate', './services/ReserveIdeaDateForPlan', './controllers/YearMonthController',
  '../components/planManager/PlanManagerController', '../../misc/js/main', 'angular-sanitize'],
  (angular, PlanIdeaLinkResolver, TinymceLoader, ReservedPlanDate, ReserveIdeaDateForPlan,
  YearMonthController, MonthPlanManagerController) ->
   angular
      .module('planner', ['misc', 'ngSanitize'])
      .component('planManager', {
        templateUrl: "planner/components/planManager/planManager.html",
        controller: (planPermission, request, $stateParams, $state, planModel, reservedPlanDate, permissionStateResolver, locale) ->
          new MonthPlanManagerController.default(planPermission, request, $stateParams, $state, planModel, reservedPlanDate, permissionStateResolver, locale)
        bindings: {
          plan: "<",
          week: "<",
          idOfItem: "<"
        }
      })
      .controller('yearMonthController', ($stateParams, $state, planModel, planPermission, config, $scope) ->
        new YearMonthController.default($stateParams, $state, planModel, planPermission, config, $scope)
      )
      .service('planIdeaLinkResolver',
        ($state, ideaPermissionHandler, permissionStateResolver, locale) ->
          new PlanIdeaLinkResolver.default(
            $state, locale, ideaPermissionHandler, permissionStateResolver)
      )
      .service("tinymceLoader", -> new TinymceLoader.default())
      .service('reservedPlanDate', () -> new ReservedPlanDate.default())
      .service('reserveIdeaDateForPlan', (links, $state, config, ngxNavigate) -> new ReserveIdeaDateForPlan.default(links, $state, config, ngxNavigate))


