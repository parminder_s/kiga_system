###*
  @ngdoc value
  @name planEditorProfile
  @description

  Config for TinyMCE editor for adding comments.
  The last three parameters are used for converting
  an empty p tag in a br.
###

define ['../app'], (app) ->
  app.value 'planEditorProfile',
    menu: {}
    plugins : 'lists'
    toolbar: 'bold italic | bullist numlist'
    #content_css: 'https://s3-eu-west-1.amazonaws.com/kiga-client/tinymce-1/skins/default/content.css'
    resize: false
    statusbar: false
    valid_elements: 'strong,em,p,br,li,ul,ol'
    element_format: 'html'
    entity_encoding: 'raw'
    force_br_newlines: true
    force_p_newlines: false
    forced_root_block: ''

