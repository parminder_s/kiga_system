###*
  @ngdoc value
  @name planConfig
  @descripition

  Configuration  parameters for planner tool
###

define ['../app'], (app) ->
  app.value 'planConfig',
    maxItemsPerDay: 6
