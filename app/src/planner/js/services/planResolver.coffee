###*
  @ngdoc service
  @name planResolver
  @description

  Resolves the plan according the url and takes create
  of permission issues and potential redirections.
###

define ['../app'], (app) ->
  app.service 'planResolver', (planModel, infoDlg, $state,
    planPermission, permissionStateResolver) ->
    (planType, planId) ->
      if planType is 'kiga'
        planModel.find planId, planType
      else
        if planPermission.allowMemberDetail()
          planId ?= 'current'
          planModel.find planId, planType
        else
          $state.go permissionStateResolver.resolvePermissionDenied true
