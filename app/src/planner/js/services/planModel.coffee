###*
  @ngdoc service
  @name planModel

  @description

  This service contains all methods in the planner module that require a server
  communication.
###

define ['../app', 'moment'], (app, moment) ->
  app.service 'planModel', (request, planViewModel, planIdeaLinkResolver,
  planKigaDetailViewModel, planYearMonthViewModel, planNlDecorator, $q) ->
    new class Model
      find: (planId, planType) ->
        @withPlanPromise planType,
          url: "planner/#{planType}/find"
          data: planId: planId

      findByMonth: (month, year, planType, cb) ->
        request {
          url: "planner/#{planType}/findByMonth"
          data:
            year: year, month: month
        }, (plans) ->
          if plans
            selected = moment [year, month - 1, 1]
            prev = moment(selected).add -1,  'months'
            next = moment(selected).add 1, 'months'

            prevMonth = prev.month() + 1
            cb
              prevMonth: prev.month() + 1
              prevYear: prev.year()
              nextMonth: next.month() + 1
              nextYear: next.year()
              showMonthHeader: false
              type: 'monthly'
              months: [planYearMonthViewModel.getModelForMonth year,
                month, plans]

      findByYear: (year, planType, cb) ->
        request {
          url: "planner/#{planType}/findByYear"
          data:
            year: year
        }, (plans) ->
          if plans then cb
            showMonthHeader: true
            prevYear: year - 1
            nextYear: year + 1
            type: 'yearly'
            months: planYearMonthViewModel.getModelForYear year, plans

      findByWeek: (weekDate) -> {}

      findAll: (planType) -> request "planner/#{planType}/findAll"

      addComment: (planId, plannedDate, comment) ->
        request url: 'planner/member/addComment', data:
          planId: planId, plannedDate: plannedDate, comment: comment

      saveComment: (id, comment) ->
        request url: 'planner/member/saveComment', data:
          id: id, comment: comment

      removeComment: (id) ->
        request url: 'planner/member/removeComment', data:
          id: id

      add: (plan, planType) ->
        request url: "planner/#{planType}/add", data: plan

      addItem: (item, planType) ->
        request url: "planner/#{planType}/addItem", data: item

      addWithItem: (plan, planType) ->
        request url: "planner/#{planType}/addWithItem", data: plan

      save: (plan, planType) ->
        @withPlanPromise planType,
          url: "planner/#{planType}/save"
          data: plan

      remove: (planId, planType, cb) ->
        request {
          url: "planner/#{planType}/remove"
          data:
            planId: planId
        }, cb

      copy: (plan, planType) ->
        request url: "planner/#{planType}/copy", data: plan

      findItem: (planId, itemId, planType, cb) ->
        @find planId, planType, (plan) ->
          cb _.find plan.items, (item) -> parseInt(item.id) is parseInt(itemId)

      changeItemDate: (changeData) ->
        @withPlanPromise changeData.planType,
          url: "planner/#{changeData.planType}/changeItemDate"
          data: changeData

      removeItem: (planId, itemId, planType) ->
        request url: "planner/#{planType}/removeItem", data:
          itemId: itemId, planId: planId

      findCategories: -> request 'main/findCategories'

      findComment: (id) -> request url: 'planner/member/findComment', data:
        id: id

      addWeek: (planId, cb) ->
        @withPlanReturn cb, 'member',
          url: "planner/member/addWeek"
          data: planId: planId

      removeWeek: (planId, cb) ->
        @withPlanReturn cb, 'member',
          url: 'planner/member/removeWeek'
          data: planId: planId

      withPlanPromise: (planType, requestConfig) ->
        request(requestConfig).then (plan) =>
          @decoratePlan plan, planType

      withPlanReturn: (cb, planType, requestConfig) ->
        request requestConfig, (plan) =>
          @decoratePlan(plan, planType).then cb

      decoratePlan: (plan, planType) ->
        deferred = $q.defer()
        if plan and plan isnt 'null'
          if planType is 'member'
            deferred.resolve planViewModel.getModel plan
          else
            @decorateNl(planKigaDetailViewModel.getModel plan).then (plan) ->
              plan.items =
                planIdeaLinkResolver.getItemsWithResolvedLinks plan.items
              deferred.resolve plan
        else deferred.resolve null
        deferred.promise

      decorateNl: (plan) -> planNlDecorator.decorate plan

      setNlPosition: (ideaId, position, planId, newsletter) ->
        request
          url: 'newsletter/setPosition',
          data: type: 'plan', planId: planId, ideaId: ideaId,
          position: position, newsletterId: newsletter.id

      findTags: ->
        request {url: "main/findTags", data: category: 'Planner'}



