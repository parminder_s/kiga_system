###*
  @ngDoc service
  @name planUiLogic
  @module planner
  @description

  Utility service for the planner's detailCtrl that provides
  UI logic functions that are called throughout the partials.
###

define ['../app'], (app) ->
  app.service 'planUiLogic', ->
    toggleItemMenu: (item) ->
      item.showMenu = !item.showMenu
    hideWeek: (plan, week) ->
      not plan.isInPast and week.isInPast and not plan.showPastDays
    showDay: (plan, day) ->
      plan.isInPast or not day.isInPast or plan.showPastDays
    showItemsInPastShowBtn: (plan, week) ->
      not plan.isInPast and week.isNow and not plan.showPastDays
    showItemsInPastHideBtn: (plan, week) ->
      not plan.isInPast and
        plan.showPastDays and
        plan.hasPastDays and
        week is plan.weeks[0]
    showItemsInPast: (plan) -> plan.showPastDays = true
    hideItemsInPast: (plan) -> plan.showPastDays = false
