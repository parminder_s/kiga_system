###*
  @ngdoc service
  @name planNlDecorator
  @description

  Provides newsletter functions for the planner, e.g. setting
  newsletter position attribute to ideas in kigaplan
###

define ['../app', 'angular', 'lodash'], (app, angular, _) ->
  app.service 'planNlDecorator', (planPermission, request, $q) ->
    new class PlanNlDecorator
      decorate: (plan) ->
        deferred = $q.defer()
        if @isDecoratable plan.planType
          @findNewsletterIdeas(plan).then (data) =>
            deferred.resolve @addNlIndicator plan, data
        else
          deferred.resolve plan
        deferred.promise

      addNlIndicator: (plan, data) ->
        return plan unless data.length
        plan.newsletters = data
        plan.selectedNl = data[0] unless plan.selectedNl
        #plan.selectedNl = if plan.selectedNl then plan.selectedNl else data[0]
        @updateNlIndicator plan

      updateNlIndicator: (plan) ->
        _.forEach plan.weeks, (week) ->
          _.forEach week.days, (day) ->
            _.forEach day.items, (item) ->
              if item.itemType is 'kigaIdea'
                item.nlPosition = _.indexOf plan.selectedNl.ideas,
                  parseInt item.kigaPageId
                item.nlPosition += 1
        plan

      findNewsletterIdeas: (plan) ->
        request
          url: 'newsletter/getPlanNewsletters'


      isDecoratable: (planType) ->
        (planType is 'kiga') or planPermission.isCmsMember()
