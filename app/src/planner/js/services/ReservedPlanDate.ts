/**
 * This class is used for storing state for creating a new plan.
 */

export default class ReservedPlanDate {
  private plannerDate: Date;

  constructor() {}

  setDateForPlan(date) {
    this.plannerDate = date;
  }

  getPlanDate() {
    return this.plannerDate;
  }

  removePlannedDate() {
    this.plannerDate = null;
  }
}
