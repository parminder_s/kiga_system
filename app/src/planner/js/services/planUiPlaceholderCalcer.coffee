###*
  @ngDoc service
  @name planUiPlaceholderCalcer
  @module planner
  @description

  Logic component for ui which calculates the amount of placeholders per day.
  There are two relevant view modes which need to be calculated.
  1: For the default calendar view on desktop mode where 7 days are shown within
     an row.
  2: For smaller displays the weekdays (monday - friday) and the weekend build
     an own row.

  calc has as argument week which is a structure of
   {days, workDays, weekendDays}. The latter three contain again days
  where each day holds the items and the ui property where the placeholders
  will be stored.

  Each day should also have a date property which is used to check if adding
  for that day is allowed or not.

  Placeholders in ui is an array of boolean whereas each element stands for a
  placeholder. Droppable in ui is the same as the placeholder except that an
  item can be dropped on it.

  All smaller displays are handled by bootstrap/css mechanisms.
###

define ['../app', 'lodash', 'moment'], (app, _, moment) ->
  app.service 'planUiPlaceholderCalcer', (planConfig) ->
    new class PlanUiPlaceholderCalcer
      calc: (weeks, now) ->
        maxItemsPerDay = planConfig.maxItemsPerDay
        _.map weeks, (week) =>
          week.days = @perDays week.days, now, maxItemsPerDay
          week.workDays = @perDays week.workDays, now, maxItemsPerDay
          week.weekendDays = @perDays week.weekendDays, now, maxItemsPerDay
          week

      perDays: (days, now, maxItemsPerDay) ->
        maxItems = @getMaxItems days
        _.map days, (day) =>
          itemsLength = day.items.length
          angular.extend day.ui,
            isDroppable: @isDroppable now, day.date
            placeholders: @setPlaceholders maxItems, itemsLength
            placeholders3Col: _.times @getPlaceholders3Col(itemsLength),
              (i) -> i
            has1ColPlaceholder: @has1ColPlaceholder now, day.date,
              maxItemsPerDay, itemsLength
          day

      getMaxItems: (days) ->
        _.max(_.map(days, (day) -> day.items.length)) ? 0

      isDroppable: (now, date) ->
        !moment(date).isBefore now, 'day'

      has1ColPlaceholder: (now, date, maxItemsPerDay, itemsLength) ->
        if maxItemsPerDay > itemsLength
          !moment(date).isBefore now, 'day'
        else false

      setPlaceholders: (maxItems, itemsLength) ->
        length = maxItems - itemsLength

        if itemsLength is 0 and maxItems is 0
          [true]
        else
          if length > 0
            _.times length, -> true
          else []

      getPlaceholders3Col: (itemsLength) -> [3, 2, 1, 0, 2, 1, 0][itemsLength]
