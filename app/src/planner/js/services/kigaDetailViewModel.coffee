###*
  @ngdoc service

  @description
  Creates the view model for a KiGaPlan.
  It uses the existing detailViewModel which does the main work
  and simply changes / reduces some fields
###

define ['../app', 'angular'], (app, angular) ->
  app.service 'planKigaDetailViewModel', (planViewModel, now) ->
    new class KigaDetailViewModel
      getModel: (plan) ->
        plan = planViewModel.getModel plan
        angular.extend plan,
          type: 'kiga'
          color: 'kigaPlan'
          days: @addWeekParts @extendDays(plan.weeks[0].days)

      extendDays: (days) ->
        _.map days, (day) ->
          day.item = day.items[0] if day.items.length
          day.canAddItem = not day.date.isBefore now().startOf 'day'
          day

      addWeekParts: (days) ->
        _.groupBy days, (day) ->
          if day.date.isoWeekday() < 6 then 'weekday' else 'weekend'
