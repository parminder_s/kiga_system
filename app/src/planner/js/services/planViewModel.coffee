###*
  @ngdoc service
  @name planViewModel
  @description

  Creates a view model from a given plan record
  retrieved from the server

  The structure of the view model is
  type: string - kiga or member
  isInPast: boolean - if plan is completely in the past
  hasPastDays: boolean - if plan starts in the past
  showPastDays: boolean - if plan should show even past days (-4 col rwd)
  weeks: [{
   isInPast: boolean - if that week is in the past
   isNow: boolean - if this week is now, e.g. current day is part of week
   maxItems: int - maximal amount of planned items per day
   weekNr: int - calendar week number
   workDays: [days]
   weekendDays [days]
   days: [
     {day: week: (parent),
            prettyDate,
            dayCode,
            isToday,
            items,
            date,
            ui: see @planUiPlaceholderCalcer
       }
      ...
    ]
}
 ...
]
###

define ['../app', 'angular', 'moment', 'lodash'], (app, angular, moment, _) ->
  app.service 'planViewModel', (now, planUiPlaceholderCalcer) ->
    new class ViewModel
      dayCodes: [
        'SUNDAY','MONDAY',
        'TUESDAY', 'WEDNESDAY',
        'THURSDAY', 'FRIDAY', 'SATURDAY']

      getModel: (plan) ->
        @plan = plan
        angular.extend plan,
          isInPast: moment(plan.endDate).isBefore now()
          hasPastDays: moment(plan.startDate).isBefore now(), 'day'
          showPastDays: false
          type: 'member'

        days = @generatePlainDays plan.startDate, plan.endDate
        plan.weeks = @groupInPlainWeeks days
        plan.weeks = @assignData plan.weeks, plan.id, plan.items, now()
        plan.weeks = @addWeekendAndWorkDays plan.weeks
        plan.weeks = planUiPlaceholderCalcer.calc plan.weeks, now()
        plan

      generatePlainDays: (startDate, endDate) ->
        returner = []
        currentDay = moment startDate
        while currentDay.isBefore moment endDate
          returner.push moment currentDay
          currentDay = currentDay.add 1 , 'days'
        returner

      groupInPlainWeeks: (days) ->
        _.map _.groupBy(days, (day) -> moment(day).startOf 'isoWeek'),
          (week) -> week

      assignData: (weeks, planId, items, now) ->
        _.map weeks, (days) =>
          extendedDays = _.map days, (day) =>
            returner = @setupDay day, planId
            returner.items = @addItems day, items
            returner

          isInPast: now.isAfter _.last(days), 'day'
          isNow: now.isSame days[0], 'isoWeek'
          days: extendedDays
          maxItems: @findMaxItems extendedDays
          maxWeekendItems: @findMaxItems extendedDays, true
          weekNr: _.padStart days[0].isoWeek(), 2, '0'

      formatMonth: (day) ->
        _.padStart day.month() + 1, 2, '0'

      findMaxItems: (days, weekend) ->
        returner = 0
        _.each days, (day, idx) ->
          if (weekend and idx >= 6 or not weekend) and
          (day.items.length > returner)
            returner = day.items.length
        returner

      setupDay: (day, planId) ->
        prettyDate: "#{day.date()}.#{@formatMonth day}."
        iso: day.format 'YYYY-MM-DD'
        dayCode: "PLAN_#{@dayCodes[day.day()]}"
        isToday: day.isSame now(), 'day'
        isInPast: day.isBefore now(), 'day'
        date: day
        ui:
          uniqueCode: "#{planId}_#{day.format('YYYY-MM-DD')}"
          itemCode: day.format 'YYYY-MM-DD'

      addItems: (day, items) ->
        day.items = @sortItems @filterItems day, items

      filterItems: (day, items) ->
        _.filter items, (item) -> day.isSame moment(item.plannedDate), 'day'

      sortItems: (items) -> items.sort (a, b) ->
        if !a.position and !b.position
          if a.itemType is 'kigaIdea' then -1 else 1
        else if !b.position
          -1
        else if !a.position
          1
        else a.position - b.position

      addWeekendAndWorkDays: (weeks) ->
        _.map weeks, (week) ->
          angular.extend week, _.groupBy week.days, (day) ->
            if day.date.isoWeekday() > 5
              'weekendDays'
            else 'workDays'
          week.workDays = _.map week.workDays, (d) -> angular.copy d
          week.weekendDays = _.map week.weekendDays, (d) -> angular.copy d
          week
