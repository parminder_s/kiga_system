define ['../app', 'lodash', 'angular'], (app, _, angular) ->
  app.service 'iconSelector', ($uibModal) ->
    (cb) -> $uibModal.open
      templateUrl: 'planner/partials/modals/iconSelector.html'
      controller: ($scope, $uibModalInstance) ->
        angular.extend $scope,
          close: -> $uibModalInstance.close()
          icons: [
            'wave', 'leaf4', 'star2', 'flower1', 'leaf3', 'shirt',
            'drop', 'leaf2', 'star3', 'leaf6', 'flower2', 'moon',
            'speach', 'heart', 'music2', 'questionmark', 'music1',
            'leaf', 'star1', 'banner', 'marker', 'pattern1', 'tree',
            'construction', 'leaf5', 'lines', 'flag', 'crown', 'ok',
            'lightning', 'book', 'car', 'rocket', 'phone',
            'magnet', 'bird', 'headset', 'hourglass', 'wheel', 'star4'
          ]
          selectIcon: (icon) ->
            cb icon
            $uibModalInstance.close()
          ok: -> $uibModalInstance.close()
