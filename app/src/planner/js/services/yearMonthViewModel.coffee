define ['../app', 'moment', 'lodash'], (app, moment, _) ->
  app.service 'planYearMonthViewModel', ($sce, now) ->
    new class ViewModel
      monthNames: [
        'JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY',
        'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'
      ]

      getModelForYear: (year, plans) ->
        _.map [1..12], (month) => @getModelForMonth year, month, plans

      getModelForMonth: (year, month, plans) ->
        name: "MISC_#{@monthNames[month-1]}"
        isNow: moment([year, month - 1, 1]).isSame now(), 'month'
        weeks: @createMonth year, month, plans

      createMonth: (year, month, plans) ->
        monthDate = moment [year, month - 1, 1]
        _.map @getWeeks(month, year), (week) =>
          week.plans = @addPlansToWeek week, plans, monthDate
          @addOptionalEmptyPlan week, monthDate

      addPlansToWeek: (week, plans, month) ->
        weekStart = week.date
        weekEnd = moment(weekStart).endOf 'isoWeek'
        filteredPlans = @filterRelevantPlans week.date, plans
        _.map filteredPlans, (plan) =>
          days = []
          currentDay = moment weekStart
          while currentDay.isBefore weekEnd
            days.push
              dayNr: _.padStart currentDay.date(), 2, '0'
              style: @getStyle plan, currentDay, month
            currentDay.add 1, 'days'

          plan: $.extend plan, color: @getPlanColor plan
          days: days
          prettyDuration: @prettyDuration plan

      filterRelevantPlans: (weekDate, plans) ->
        _.filter plans, (plan) =>
          @isPlanWithinWeek weekDate, plan

      getStyle: (plan, day, month) ->
        if !day.isSame month, 'month'
          'notInMonth'
        else
          planColor = @getPlanColor plan
          if @hasPlannedItemOnDay day, plan
            'plannedOnDay'
          else
            'notPlannedOnDay'

      getPlanColor: (plan) ->
        if plan.type is 'kiga' then 'kigaPlan' else plan.color

      isPlanWithinWeek: (weekDate, plan) ->
        weekStart = weekDate
        weekEnd = moment(weekDate).endOf 'isoWeek'
        moment(plan.startDate).isBefore(weekEnd) and
          moment(plan.endDate).isAfter weekStart

      hasPlannedItemOnDay: (day, plan) ->
        _.some plan.items, (item) -> moment(item.plannedDate).isSame day, 'day'

      addOptionalEmptyPlan: (week, month) ->
        if week.plans.length is 0
          currentDay = moment week.date
          weekEnd = moment week.date.endOf 'isoWeek'
          days = []
          while currentDay.isBefore weekEnd
            days.push
              dayNr: _.padStart currentDay.date(), 2, '0'
              style: @getEmptyPlanStyle currentDay, month
              innerStyle: @getEmptyPlanInnerStyle currentDay, month

            currentDay.add 1, 'days'
          week.plans = [days: days, plan: 'empty']
        week

      prettyDuration: (plan) ->
        start = moment(plan.startDate).format 'DD.MM.YY'
        end = moment(plan.endDate).format 'DD.MM.YY'
        "#{start} - #{end}"

      getEmptyPlanStyle: (day, month) ->
        if day.isSame month, 'month'
          'noPlanInMonth'
        else
          'noPlanOutMonth'

      getEmptyPlanInnerStyle: (day, month) ->
        if day.isSame month, 'month'
          backgroundColor: 'white'

      getWeeks: (month, year) ->
        returner = []
        limits = @calcMonthLimits month, year
        currentWeek = moment limits.start

        while currentWeek.isBefore limits.end
          returner.push
            date: currentWeek
            weekNr: _.padStart currentWeek.isoWeek(), 2, '0'
            month: month
            year: year
            plans: []

          currentWeek = moment(currentWeek).add 1, 'weeks'
        returner

      calcMonthLimits: (month, year) ->
        returner = []
        monthDate = moment [year, month - 1, 1]
        start = moment(monthDate).startOf 'isoWeek'
        end = moment(monthDate).endOf('month').endOf 'isoWeek'
        start: start, end: end
