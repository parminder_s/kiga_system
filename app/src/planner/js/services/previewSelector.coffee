define ['../app', 'lodash'], (app, _) ->
  app.service 'previewSelector', ($uibModal, $q) ->
    (plan) ->
      deferred = $q.defer()
      $uibModal.open
        templateUrl: 'planner/partials/modals/previewSelector.html'
        controller: ($scope, $uibModalInstance) ->
          $scope.plan = plan
          $scope.close = -> $uibModalInstance.dismiss()

          uniques = _.uniq plan.items, (i) -> i.previewUrl
          $scope.items = _.map uniques, (item) ->
            kigaPageId: item.kigaPageId, url: item.previewUrl

          $scope.selectImage = (item) ->
            $uibModalInstance.close()
            deferred.resolve item.kigaPageId
      deferred.promise
