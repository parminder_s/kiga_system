import PermissionHandler from '../../../ideas/js/services/IdeaPermissionHandler';
import * as _ from 'lodash';
import { StateService } from 'angular-ui-router';
import PermissionStateResolver from '../../../misc/permission/js/PermissionStateResolver';

export default class PlanIdeaLinkResolver {
  constructor(
    private $state: StateService,
    private locale,
    private permissionHandler: PermissionHandler,
    private permissionStateResolver: PermissionStateResolver
  ) {}

  getItemsWithResolvedLinks(ideas) {
    return _(ideas)
      .map(idea => {
        if (!this.permissionHandler.hasPermission(idea.inPool, idea.facebookShare)) {
          let permissionState = this.permissionStateResolver.resolvePermissionDenied(!idea.inPool);
          idea.link = this.$state.href(permissionState, {
            locale: this.locale.get(),
            forward: idea.link
          });
        }
        return idea;
      })
      .valueOf();
  }
}
