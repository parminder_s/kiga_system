###*
  @ngdoc service
  @name planCommmentLength

  @description

  This service calculates the length of a comment. It calculates 15 chars per
  empty line.
  This service is needed since we have a print version of the comments in
  planner, where the comment is written into a box which has a fixed height.
  Consumer of this service is the directive @validPlannerComment
###

define ['../app', 'lodash'], (app, _) ->
  maxChars = 15
  app.service 'planCommentLength', -> (html) ->
    html = "<p>#{html}" if !html.match /^<p>/
    _.sum (_(html.replace('</p>', '').split '<p>')
      .drop()
      .map((line) -> line.replace /<\/li>/g, '<br>')
      .map((line) -> line.split /<br\/?>/)
      .flatten()
      .map((line) ->
        line.replace /<[^>]*>/g, '')
      .map((line) ->
        _.max [Math.ceil(line.length / maxChars) * maxChars, maxChars])
      .value())





