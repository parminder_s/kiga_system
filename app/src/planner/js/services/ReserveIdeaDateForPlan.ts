import {
  IConfigService,
  Feature,
  INgxNavigateService
} from '../../../misc/js/services/AngularInterfaces';

/**
 * This class is used for storing state for adding an idea to a plan
 */

export default class ReserveIdeaDateForPlan {
  private date: Date;
  private planId: number = 0;

  constructor(
    private links,
    private $state,
    private config: IConfigService,
    private navigate: INgxNavigateService
  ) {}

  setReservedDate(date, planId) {
    this.date = date;
    this.planId = planId;
  }

  getKigaIdeaAndPlan() {
    return {
      date: this.date,
      planId: this.planId
    };
  }

  checkIfDateIsReserved() {
    return this.date && this.planId != 0;
  }

  removeReservation() {
    this.date = null;
  }

  redirectToKigaIdeas() {
    const url = this.links.findByCode('ideaGroupMainContainer').url;
    if (this.config.isFeatureEnabled(Feature.ANGULAR_CMS_ENABLED)) {
      this.navigate.navigateByUrl(url);
    } else {
      let splittedUrl = url.split('cms');
      let path = splittedUrl[1].replace('/', '');
      this.$state.go('root.cms', { path: path });
    }
  }
}
