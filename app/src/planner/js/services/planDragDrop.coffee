###*
  @ngdoc service
  @name planDragDrop
  @description

  Provides logic for drag and drop in planner
###

define ['../app', 'moment'], (app, moment) ->
  app.service 'planDragDrop', (planPermission) -> new class PlanDragDrop
    allowDrag: (day) ->
      !day.isInPast and planPermission.cmsOrSubscription()
    allowDrop: (day) ->
      day.ui.isDroppable and planPermission.cmsOrSubscription()
    checkDrop: (item, day, position) ->
      !moment(item.plannedDate).isSame(moment(day), 'day') or
      (
        moment(item.plannedDate).isSame(moment(day), 'day') and
        (item.position isnt position) and
        (item.position isnt (position - 1))
      )
