export default class TinymceLoader {
  loaded = false;

  load(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      if (this.loaded) {
        resolve(true);
      } else {
        (<any>require)(['tinymce/tinymce', 'tinymce/themes/modern/theme'], (tinymce, theme) => {
          /* we have to overwrite the baseURL since tinymce scans the loaded scripts files
           and takes the first one it matches with regex tinymce as baseUrl.
           Due to the lazy loading of tinymce, the first match is angular-ui-tinymce.
           */

          /*
           window["tinymce"].baseURL =
           window["tinymce"].baseURL.replace("angular-ui-tinymce/src", "tinymce");
           */
          window['tinymce'].baseURL = 'https://d1v5g5yve3hx29.cloudfront.net/tinymce-1';
          (<any>resolve)(true);
          this.loaded = true;
        });
      }
    });
  }
}
