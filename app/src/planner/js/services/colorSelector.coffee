define ['../app', 'angular'], (app, angular) ->
  app.service 'colorSelector', ($uibModal) -> (cb) -> $uibModal.open
    templateUrl: 'planner/partials/modals/colorSelector.html'
    controller: ($scope, $uibModalInstance) ->
      angular.extend $scope,
        close: -> $uibModalInstance.close()
        colors: [
          'cyan', 'teal', 'green', 'lightGreen', 'lime',
          'deepOrange', 'red', 'pink', 'purple', 'deepPurple',
          'orange', 'amber', 'blue', 'lightBlue', 'indigo'
          ]
        selectColor: (color) ->
          cb color
          $uibModalInstance.close()
        ok: -> $uibModalInstance.close()
