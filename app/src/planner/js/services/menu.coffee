define ['../app', 'angular'], (app, angular) ->
  app.service 'planMenu', ->
    new class Menu
      constructor: -> @showMenu = true
      show: -> @showMenu = true
      hide: -> @showMenu = false
      isShown: -> @showMenu
