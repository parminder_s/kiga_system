define ['../app', 'angular', 'lodash'], (app, angular, _) ->
  app.service 'planPermission', ($stateParams, member, $state, config) ->
    new class Permission
      isCmsMember: -> member.isCms
      planType: -> $stateParams.planType
      isKigaPlan: -> @planType() is 'kiga'
      isMemberPlan: -> @planType() is 'member'
      isAllowedMember: -> member.hasActiveFullSubscription() or member.hasActiveTestSubscription()
      cmsOrSubscription: ->
        (@isKigaPlan() and @isCmsMember()) or
          (@isMemberPlan() and @isAllowedMember())
      showCopy: -> @isKigaPlan() and !@isCmsMember()
      linkItemToIdea: -> @isKigaPlan() and !@isCmsMember()
      allowCopy: -> @isKigaPlan() and @isAllowedMember()
      allowPrintSelection: ->
        member.hasActiveFullSubscription() or @isCmsMember()
      allowMemberDetail: -> @isAllowedMember()
      showNlSelect: ->
        @isKigaPlan() and @isCmsMember()

