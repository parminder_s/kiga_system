define [
  './app'
  './controllers/addToCtrl'
  './controllers/commentCtrl'
  './controllers/detailCtrl'
  './controllers/planFormCtrl'
  './controllers/planCtrl'
  './controllers/planIdeaCtrl'
  './controllers/planTypeCtrl'
  './controllers/planWeeklyCtrl'
  './controllers/printCtrl'
  './controllers/quickIdeaDialogCtrl'
], (app) -> return true
