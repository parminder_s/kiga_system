import * as angular from 'angular';
import * as _misc from 'misc';

import { DunningManagementService } from './service/DunningManagementService';
import { DunningLevelsManagementCtrl } from './controller/DunningLevelsManagementCtrl';
import { OverdueInvoiceService } from './service/OverdueInvoiceService';
import { OverdueInvoicesCtrl } from './controller/OverdueInvoicesCtrl';
import { AddDunningLevelsManagementCtrl } from './controller/AddDunningLevelsManagementCtrl';
import { UpdateDunningLevelsManagementCtrl } from './controller/UpdateDunningLevelsManagementCtrl';
/**
 * Created by bbs on 2/14/17.
 */

let misc = _misc;

export let app = angular
  .module('dunningManagement', ['misc'])
  .controller('dunningLevelsManagementCtrl', function(
    levels,
    dunningManagementService: DunningManagementService,
    $scope
  ) {
    return new DunningLevelsManagementCtrl(levels, dunningManagementService, $scope);
  })
  .controller('overdueInvoicesCtrl', function(
    overdueInvoices,
    $scope,
    dunningManagementService: DunningManagementService,
    overdueInvoiceService: OverdueInvoiceService,
    infoDlg,
    confirmerDlg,
    $q,
    $window
  ) {
    return new OverdueInvoicesCtrl(
      overdueInvoices,
      $scope,
      dunningManagementService,
      overdueInvoiceService,
      infoDlg,
      confirmerDlg,
      $q,
      $window
    );
  })
  .controller('addDunningLevelsManagementCtrl', function(
    countries: Array<any>,
    dunningManagementService: DunningManagementService,
    $state
  ) {
    return new AddDunningLevelsManagementCtrl(countries, dunningManagementService, $state);
  })
  .controller('updateDunningLevelsManagementCtrl', function(
    dunningLevel,
    countries: Array<any>,
    dunningManagementService: DunningManagementService,
    $state
  ) {
    return new UpdateDunningLevelsManagementCtrl(
      dunningLevel,
      countries,
      dunningManagementService,
      $state
    );
  })
  .service('overdueInvoiceService', function(endpoint) {
    return new OverdueInvoiceService(endpoint);
  })
  .service('dunningManagementService', function(endpoint) {
    return new DunningManagementService(endpoint);
  });
