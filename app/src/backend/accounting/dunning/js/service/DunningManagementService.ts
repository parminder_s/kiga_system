/**
 * Created by bbs on 2/14/17.
 */

export class DunningManagementService {
  constructor(private endpoint) {}

  getAll(): Array<any> {
    return null;
  }

  getAllLevels() {
    return this.endpoint('backend/dunning-management/levels');
  }

  sendEmail(invoiceNumbers: Array<string>) {
    return this.endpoint({
      url: 'backend/dunning-management/send-email',
      data: { invoiceNumbers: invoiceNumbers }
    });
  }

  increaseLevel(invoiceIds: Array<number>) {
    return this.endpoint({
      url: 'backend/dunning-management/increase-level',
      data: { invoiceIds: invoiceIds }
    });
  }

  decreaseLevel(invoiceIds: Array<number>) {
    return this.endpoint({
      url: 'backend/dunning-management/decrease-level',
      data: { invoiceIds: invoiceIds }
    });
  }

  getAllCountries() {
    return this.endpoint('backend/dunning-management/levels/countries');
  }

  saveLevel(dunningLevel) {
    let url = 'backend/dunning-management/levels/update';
    if (dunningLevel.id == null || dunningLevel.id < 1) {
      url = 'backend/dunning-management/levels/add';
    }

    return this.endpoint({
      url: url,
      data: {
        id: dunningLevel.id,
        code: dunningLevel.code,
        days: dunningLevel.days,
        feesAmount: dunningLevel.feesAmount,
        dunningLevel: dunningLevel.dunningLevel,
        countryId: dunningLevel.countryId
      }
    });
  }

  removeLevels(ids: Array<number>) {
    return this.endpoint({
      url: 'backend/dunning-management/levels/remove',
      data: { ids: ids }
    });
  }

  getLevel(dunningLevelId: number) {
    return this.endpoint({
      url: 'backend/dunning-management/levels/get',
      data: { id: dunningLevelId }
    });
  }
}
