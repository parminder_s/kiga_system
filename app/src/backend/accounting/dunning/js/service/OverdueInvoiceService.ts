/**
 * Created by bbs on 2/19/17.
 */

export class OverdueInvoiceService {
  constructor(private endpoint) {}

  getOverdueInvoices() {
    return this.endpoint('backend/dunning-management/overdue/list');
  }
}
