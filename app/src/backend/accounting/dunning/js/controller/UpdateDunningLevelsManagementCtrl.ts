import { AddDunningLevelsManagementCtrl } from './AddDunningLevelsManagementCtrl';
import { DunningManagementService } from '../service/DunningManagementService';
/**
 * Created by bbs on 2/14/17.
 */

export class UpdateDunningLevelsManagementCtrl extends AddDunningLevelsManagementCtrl {
  constructor(
    dunningLevel,
    countries: Array<any>,
    dunningManagementService: DunningManagementService,
    $state
  ) {
    super(countries, dunningManagementService, $state);
    this.dunningLevel = dunningLevel;
    this.dunningLevel.countryId = dunningLevel.country.id;
  }

  onDunningLevelsDelete(id: number) {
    this.dunningManagementService.removeLevels([id]).then(() => {
      this.$state.go('root.backend.accounting.dunning.levels');
    });
  }
}
