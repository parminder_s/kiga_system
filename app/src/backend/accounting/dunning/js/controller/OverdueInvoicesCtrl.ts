import moment = require('moment');
import { DunningManagementService } from '../service/DunningManagementService';
import { OverdueInvoiceService } from '../service/OverdueInvoiceService';
import IQService = angular.IQService;
/**
 * Created by bbs on 2/19/17.
 */

let template = html => {
  return `backend/accounting/dunning/partials/${html}.html`;
};

export class OverdueInvoicesCtrl {
  filterDunnables: boolean = false;
  private gridConfiguration: any = {};
  private data: any;

  private gridApi = null;

  private gridOptions = {
    rowHeight: 46
  };

  private columnsDefinition = [
    { name: 'Level', maxWidth: 50, field: 'dunningLevel', enableFiltering: true },
    { name: 'Country', maxWidth: 50, field: 'countryCode', enableFiltering: true },
    { name: 'Due Days', maxWidth: 110, cellTemplate: template('cellDueDays') },
    { name: 'Order', maxWidth: 110, field: 'order', cellTemplate: template('cellOrder') },
    { name: 'Invoice', maxWidth: 110, field: 'invoice', cellTemplate: template('cellInvoices') },
    { name: 'Name', cellTemplate: template('cellName') },
    { name: 'Amount', maxWidth: 100, cellTemplate: template('cellAmount') },
    { name: 'Open', maxWidth: 100, cellTemplate: template('cellDueAmount') },
    { name: 'Email sent', maxWidth: 100, field: 'isEmailSent' },
    { name: 'Actions', maxWidth: 100, cellTemplate: template('cellTransactions') }
  ];

  private rowsSelected: Array<any> = [];
  private baseUrl;

  constructor(
    private overdueInvoices: Array<any>,
    $scope,
    private dunningManagementService: DunningManagementService,
    private overdueInvoiceService: OverdueInvoiceService,
    private infoDlg,
    private confirmerDlg,
    private $q: IQService,
    private $window
  ) {
    this.data = this.formatOverdueInvoicesForUiGrid(overdueInvoices);
    this.gridConfiguration = angular.extend(
      {},
      {
        onRegisterApi: gridApi => {
          this.gridApi = gridApi;
          this.gridApi.selection.on.rowSelectionChanged($scope, () => this.onRowSelectionChanged());
          this.gridApi.selection.on.rowSelectionChangedBatch($scope, () =>
            this.onRowSelectionChanged()
          );
        },
        data: 'ctrl.data',
        enableFiltering: true,
        columnDefs: this.columnsDefinition
      },
      this.gridOptions
    );
    this.baseUrl = window['kiga'].endPointUrl;
  }

  onRowSelectionChanged() {
    this.rowsSelected = [];

    _.each(this.gridApi.selection.getSelectedRows(), row => {
      this.rowsSelected.push(row);
    });
  }

  sendEmail() {
    let selectedRows = this.gridApi.selection.getSelectedRows();
    if (this.checkIfInvoiceshasDunningLevel(selectedRows)) {
      let invoiceNumber = _.map(this.rowsSelected, row => this.getInvoiceNumber(row));
      this.dunningManagementService.sendEmail(invoiceNumber).then(() =>
        this.overdueInvoiceService.getOverdueInvoices().then(data => {
          this.overdueInvoices = data;
          this.gridConfiguration.data = this.formatOverdueInvoicesForUiGrid(this.overdueInvoices);
          this.gridApi.core.refresh();
          this.reselectRows(this.gridConfiguration.data, invoiceNumber, this.getInvoiceNumber);
        })
      );
    }
  }

  clickFilterDunnables() {
    this.data = _(this.formatOverdueInvoicesForUiGrid(this.overdueInvoices))
      .filter(entity => (this.filterDunnables ? true : entity.dueDays < 1))
      .value();
  }

  private checkIfInvoiceshasDunningLevel(selectedRows) {
    let invoicesWithoutDunningLevel = _.filter(selectedRows, (row: any) => {
      return row.dunningLevel === 'N/A';
    });

    if (invoicesWithoutDunningLevel.length > 0) {
      // show dialog for invoice not due
      this.infoDlg(
        'Following invoices has no dunning level hence cannot send dunning email: ' +
          _.join(_.map(invoicesWithoutDunningLevel, row => row.invoiceNumber), ', ')
      );
      return false;
    }

    return true;
  }

  private checkIfInvoicesNotDue(selectedRows): Promise<any> {
    return new Promise((resolve, reject) => {
      let invoicesNotDue = _.filter(selectedRows, (row: any) => {
        return row.dueDays >= 0;
      });

      if (invoicesNotDue.length > 0) {
        // show dialog for invoice not due
        this.confirmerDlg(
          'Following invoices are NOT due: ' +
            _.join(_.map(invoicesNotDue, row => row.invoiceNumber), ', ') +
            '<br>Still proceed?',
          () => {
            resolve();
          }
        );
      } else {
        resolve();
      }
    });
  }

  increaseLevel() {
    let selectedRows = this.gridApi.selection.getSelectedRows();
    let invoicesAreDue = this.checkIfInvoicesNotDue(selectedRows);
    invoicesAreDue.then(() => {
      let invoiceIds = _.map(this.rowsSelected, row => this.getInvoiceId(row));
      this.dunningManagementService.increaseLevel(invoiceIds).then(() =>
        this.overdueInvoiceService.getOverdueInvoices().then(data => {
          this.overdueInvoices = data;
          this.gridConfiguration.data = this.formatOverdueInvoicesForUiGrid(this.overdueInvoices);
          this.gridApi.core.refresh();
          this.reselectRows(this.gridConfiguration.data, invoiceIds, this.getInvoiceId);
        })
      );
    });
  }

  decreaseLevel() {
    let selectedRows = this.gridApi.selection.getSelectedRows();
    let invoicesAreDue = this.checkIfInvoicesNotDue(selectedRows);
    invoicesAreDue.then(() => {
      let invoiceIds = _.map(this.rowsSelected, row => this.getInvoiceId(row));
      this.dunningManagementService.decreaseLevel(invoiceIds).then(() =>
        this.overdueInvoiceService.getOverdueInvoices().then(data => {
          this.overdueInvoices = data;
          this.gridConfiguration.data = this.formatOverdueInvoicesForUiGrid(this.overdueInvoices);
          this.gridApi.core.refresh();
          this.reselectRows(this.gridConfiguration.data, invoiceIds, this.getInvoiceId);
        })
      );
    });
  }

  downloadPdf() {
    let selectedRows = this.gridApi.selection.getSelectedRows();
    let invoicesAreDue = this.checkIfInvoicesNotDue(selectedRows);
    invoicesAreDue.then(() => {
      let invoiceIds = _.map(this.rowsSelected, row => this.getInvoiceId(row));
      this.$window.open(
        `${this.baseUrl}/backend/dunning-management/download-pdf/${invoiceIds}`,
        '_blank'
      );
    });
  }

  reselectRows(gridData, selectedProperties, propertyGetterCallback: (row: any) => any) {
    this.gridApi.grid.modifyRows(gridData);
    _.map(gridData, (row: any) => {
      if (_.indexOf(selectedProperties, propertyGetterCallback(row)) > -1) {
        this.gridApi.selection.selectRow(row);
      }
    });
  }

  private getInvoiceNumber(row) {
    return row.invoiceNumber;
  }

  private getInvoiceId(row) {
    return row.invoiceId;
  }

  private formatOverdueInvoicesForUiGrid(overdueInvoices: Array<any>) {
    return _.map(overdueInvoices, overdueInvoice => {
      return {
        dunningLevel:
          overdueInvoice.dunningLevel == null ? '0' : overdueInvoice.dunningLevel.dunningLevel,
        countryCode: overdueInvoice.countryCode,
        dueDays: overdueInvoice.dueDays,
        dueDate: overdueInvoice.dueDate,
        isEmailSent: overdueInvoice.dunningEmailSent,
        orderNumber: overdueInvoice.orderNumber,
        orderDate: overdueInvoice.orderDate,
        invoice: overdueInvoice.invoiceNumber,
        invoiceDate: overdueInvoice.invoiceDate,
        invoiceNumber: overdueInvoice.invoiceNumber,
        invoiceId: overdueInvoice.invoiceId,
        customerName: overdueInvoice.customerName == null ? '' : overdueInvoice.customerName,
        customerEmail: overdueInvoice.customerEmail == null ? '' : overdueInvoice.customerEmail,
        amount: overdueInvoice.amount,
        currency: overdueInvoice.currency,
        dueAmount: overdueInvoice.dueAmount
      };
    });
  }
}
