import { DunningManagementService } from '../service/DunningManagementService';
/**
 * Created by bbs on 2/14/17.
 */

export class AddDunningLevelsManagementCtrl {
  protected dunningLevel: any;
  protected countries;

  constructor(
    countries: Array<any>,
    protected dunningManagementService: DunningManagementService,
    protected $state
  ) {
    this.countries = _.map(countries, country => {
      return {
        code: country.id,
        title: country.title + ' (' + country.currency + ')'
      };
    });
  }

  saveDunningLevel() {
    this.dunningManagementService
      .saveLevel(this.dunningLevel)
      .then(() => this.$state.go('root.backend.accounting.dunning.levels'));
  }
}
