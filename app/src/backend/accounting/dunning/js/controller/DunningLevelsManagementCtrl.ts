import { DunningManagementService } from '../service/DunningManagementService';
/**
 * Created by bbs on 2/14/17.
 */

export class DunningLevelsManagementCtrl {
  private gridApi;
  private gridConfiguration: any = {};
  private rowsSelected: Array<any> = [];

  private columnsDefinition = [
    {
      name: ' ',
      cellTemplate: 'backend/accounting/dunning/partials/levels/cellAction.html',
      maxWidth: 30
    },
    { name: 'ID', field: 'id' },
    { name: 'Code', field: 'code' },
    { name: 'Days', field: 'days' },
    { name: 'Fees', field: 'feesAmount' },
    { name: 'Currency', field: 'country.currency' },
    { name: 'Dunning Level', field: 'dunningLevel' },
    { name: 'Country', field: 'country.title' }
  ];

  constructor(
    private dunningLevels: Array<any>,
    private dunningManagementService: DunningManagementService,
    $scope
  ) {
    this.gridConfiguration = angular.extend(
      {},
      {
        onRegisterApi: gridApi => {
          this.gridApi = gridApi;
          this.gridApi.selection.on.rowSelectionChanged($scope, () => this.onRowsSelected());
          this.gridApi.selection.on.rowSelectionChangedBatch($scope, () => this.onRowsSelected());
        },
        data: this.dunningLevels
      },
      { columnDefs: this.columnsDefinition }
    );
  }

  onRowsSelected() {
    this.rowsSelected = [];
    _.each(this.gridApi.selection.getSelectedRows(), row => this.rowsSelected.push(row));
  }

  onDunningLevelsDelete() {
    let idsToRemove = _.map(this.gridApi.selection.getSelectedRows(), (row: any) => {
      return row.id;
    });
    this.dunningManagementService.removeLevels(idsToRemove).then(() => {
      this.rowsSelected = [];
      this.dunningManagementService.getAllLevels().then(data => {
        this.dunningLevels = data;
        this.gridConfiguration.data = this.dunningLevels;
      });
    });
  }
}
