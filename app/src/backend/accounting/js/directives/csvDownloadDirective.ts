import * as _ from 'lodash';
import * as angular from 'angular';

export class CsvDownloadDirective {
  $interpolate;
  rowtemplate;
  rowDelimiter;

  constructor($interpolate) {
    this.$interpolate = $interpolate;
    this.rowtemplate = null;
    this.rowDelimiter = '\n';
  }

  encodeCSV(data, mapfunc, rowAs) {
    if (!data) return '';
    let rowmap = rowAs
      ? function(r) {
          let rowScope = {};
          rowScope[rowAs] = r;
          return mapfunc(rowScope);
        }
      : mapfunc;
    return data.map(rowmap).join(this.rowDelimiter);
  }

  downloadCsv(csvdata, filename) {
    csvdata = 'data:text/csv;charset=utf-8,' + csvdata;
    csvdata = encodeURI(csvdata);
    let link = document.createElement('a');
    link.setAttribute('href', csvdata);
    link.setAttribute('download', filename);
    link.click();
  }

  public link(scope, element, attrs) {
    this.rowtemplate = this.$interpolate(scope.row);
    scope.download = () => {
      let csv = this.encodeCSV(scope.data, this.rowtemplate, scope.rowAs);
      if (scope.header) {
        csv = scope.header + this.rowDelimiter + csv;
      }
      this.downloadCsv(csv, scope.filename || 'export.csv');
    };

    scope.getRowCount = function getRowCount() {
      return scope.data ? scope.data.length : 0;
    };
  }
  public template = '<div><button class="btn btn-default" ng-click="download()">' +
    '<i class="glyphicon glyphicon-download">' +
    '</i> Download {{getRowCount()}} rows </button></div>';
  public scope = {
    data: '=',
    filename: '=',
    row: '=',
    rowAs: '@',
    header: '@'
  };
}
