import * as _ from 'lodash';
import * as angular from 'angular';

export class TextFileReaderDirective {
  public link(scope, element, attrs, ctrl) {
    let input = element.find('input')[0];
    input.addEventListener('change', handleFileSelect);
    scope.charsets = ['UTF-8', 'ISO-8859-1'];
    scope.charset = 'ISO-8859-1'; // "UTF-8";

    function handleFileSelect(evt) {
      let files = evt.target.files;
      for (let i = 0, f; (f = files[i]); i += 1) {
        let reader = new FileReader();
        reader.onload = function() {
          scope.$apply(function() {
            scope.data.content = reader.result;
            scope.data.file = f;
          });
        };
        reader.readAsText(f, scope.charset);
      }
    }
  }

  public scope = {
    data: '='
  };

  public template = `
<div class="row">
  <div class="col-md-30">
    <input type="file" name="files[]" class="form-control"/>
  </div>
  <div class="col-md-30">
    <select class="form-control" ng-model="charset" ng-options="opt for opt in charsets"></select>\
  </div>
</div>
`;
}
