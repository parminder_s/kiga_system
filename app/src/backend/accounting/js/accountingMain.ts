import * as angular from 'angular';
import * as _misc from 'misc';

import { AccountingImportCtrl } from './controllers/accountingImportCtrl';
import { AccountingTransactionsCtrl } from './controllers/accountingTransactionsCtrl';
import { TextFileReaderDirective } from './directives/textFileReaderDirective';
import { CsvDownloadDirective } from './directives/csvDownloadDirective';
import ToastService from '../../../misc/toast/ToastService';

import '../accounting-backend.less';

let misc = _misc;

export let app = angular
  .module('accounting', ['misc'])
  .controller('accountingImportCtrl', function(endpoint, toastService: ToastService) {
    return new AccountingImportCtrl(endpoint, toastService);
  })
  .controller('accountingTransactionsCtrl', function(endpoint) {
    return new AccountingTransactionsCtrl(endpoint);
  })
  .directive('textFileReader', function() {
    return new TextFileReaderDirective();
  })
  .directive('csvDownload', function($interpolate) {
    return new CsvDownloadDirective($interpolate);
  });
