import moment = require('moment');
import * as _ from 'lodash';
import * as angular from 'angular';

export class AccountingTransactionsFilter {
  fromDate: Date;
  toDate: Date;
}

export class AccountingTransactionsCtrl {
  endpoint = null;
  transactions = [];
  filter = new AccountingTransactionsFilter();

  constructor(endpoint) {
    this.endpoint = endpoint;
    this.transactions = [];
    this.fetchTransactions();
  }

  reload() {
    this.fetchTransactions();
  }

  fetchTransactions() {
    let dateParameters = [];
    if (this.filter.fromDate) {
      dateParameters.push('fromDate=' + moment(this.filter.fromDate).format('YYYY-MM-DD'));
    }
    if (this.filter.toDate) {
      dateParameters.push('toDate=' + moment(this.filter.toDate).format('YYYY-MM-DD'));
    }
    let qstring = '?' + dateParameters.join('&');
    this.endpoint({
      url: 'backend/accounting/transactions' + qstring,
      method: 'GET'
    }).then(data => {
      this.transactions = data.data;
    });
  }
}
