import * as _ from 'lodash';
import * as angular from 'angular';

import ToastService from '../../../../misc/toast/ToastService';

export class AccountingImportCtrl {
  endpoint = null;
  fileFormats = {
    CC: 'CardComplete',
    SOFORT: 'Sofort',
    HOBEX: 'Hobex',
    PAYPAL: 'Paypal',
    OTHERPAYPAL: 'Paypal (old)'
  };

  importRows = [];
  data = {
    format: undefined,
    content: '',
    doImport: false,
    file: {
      name: ''
    }
  };
  importStatus = false;
  importResult = {
    rowsParsed: 0,
    rowsError: 0,
    rowsPersisted: 0
  };

  constructor(endpoint, private toastService: ToastService) {
    this.endpoint = endpoint;
  }

  formchange() {
    this.importStatus = false;
    this.data.doImport = false;
  }

  canImport() {
    return this.importStatus;
  }

  resultHasWarnings() {
    return this.importResult.rowsError > 0;
  }

  submit() {
    this.endpoint({
      url: 'backend/accounting/import',
      data: {
        csv: this.data.content,
        formatCode: this.data.format,
        doImport: !!this.data.doImport,
        filename: this.data.file.name
      }
    }).then(data => {
      this.importRows = data.data;
      this.importResult = {
        rowsParsed: data.rowsParsed,
        rowsError: data.rowsError,
        rowsPersisted: data.rowsPersisted
      };
      if (data.rowsParsed > 0) {
        this.importStatus = true;
      }
      if (data.rowsPersisted > 0) {
        this.toastService.default().success(data.rowsPersisted + ' Rows imported');
      }
    });
  }
}
