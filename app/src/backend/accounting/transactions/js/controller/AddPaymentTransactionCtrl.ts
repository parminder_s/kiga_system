import { IGoBack } from '../../../../../misc/js/services/AngularInterfaces';
import { PaymentTransactionService } from '../service/PaymentTransactionService';
/**
 * Created by bbs on 2/15/17.
 */

export class AddPaymentTransactionCtrl {
  protected paymentTransaction;
  protected banks;
  protected paymentSettled = false;

  constructor(
    paymentTransaction: any,
    banks: Array<any>,
    protected bank,
    protected $state,
    protected paymentTransactionService: PaymentTransactionService,
    private confirmerDlg,
    private goBack: IGoBack
  ) {
    this.banks = _.map(banks, bank => {
      return {
        title: bank.bankName,
        code: bank.bankName
      };
    });

    let isEmptyBankName: boolean =
      paymentTransaction.bank == null || _.trim(paymentTransaction.bank) === '';
    let isDefaultBankNameForCountry =
      bank != null && bank.bankName != null && _.trim(bank.bankName) !== '';

    if (!isEmptyBankName) {
      this.banks = _.filter(this.banks, (bankItem: any) => {
        return bankItem.title !== paymentTransaction.bank;
      });
      this.banks = _.concat(
        [{ title: paymentTransaction.bank, code: paymentTransaction.bank }],
        this.banks
      );
    } else if (isDefaultBankNameForCountry) {
      paymentTransaction.bank = bank.bankName;
    }

    this.paymentTransaction = paymentTransaction;
    this.paymentTransaction.totalAmountLeft = this.paymentTransaction.amount;
    if (this.paymentTransaction.transferDate == null) {
      this.paymentTransaction.transferDate = new Date();
    }
  }

  onSubmit() {
    if (
      this.paymentTransaction.amount < this.paymentTransaction.totalAmountLeft ||
      this.paymentTransaction.invoiceModel.settled === true
    ) {
      this.savePayment();
    } else {
      this.confirmerDlg(
        "The full amount has been paid, but invoice wasn't marked as settled." +
          '<br><br>Still Proceed?',
        () => this.savePayment()
      );
    }
  }

  savePayment() {
    this.paymentTransactionService
      .save({
        id: this.paymentTransaction.id,
        paidInvoiceNumber: this.paymentTransaction.paidInvoiceNumber,
        amount: this.paymentTransaction.amount,
        transferDate: this.paymentTransaction.transferDate,
        bank: this.paymentTransaction.bank,
        bankTransactionId: this.paymentTransaction.bankTransactionId,
        settle: this.paymentTransaction.invoiceModel.settled
      })
      .then(() => {
        this.$state.go('root.backend.accounting.transactions.list', {
          invoiceNumber: this.paymentTransaction.paidInvoiceNumber
        });
      });
  }

  back() {
    this.goBack.goBack();
  }
}
