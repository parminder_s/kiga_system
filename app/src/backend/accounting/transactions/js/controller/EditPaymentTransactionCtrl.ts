import { IGoBack } from '../../../../../misc/js/services/AngularInterfaces';
import { AddPaymentTransactionCtrl } from './AddPaymentTransactionCtrl';
import { PaymentTransactionService } from '../service/PaymentTransactionService';
/**
 * Created by bbs on 2/15/17.
 */

export class EditPaymentTransactionCtrl extends AddPaymentTransactionCtrl {
  constructor(
    paymentTransaction: any,
    banks,
    bank,
    $state,
    paymentTransactionService: PaymentTransactionService,
    confirmerDlg,
    goBack: IGoBack
  ) {
    super(paymentTransaction, banks, bank, $state, paymentTransactionService, confirmerDlg, goBack);
    this.paymentSettled = paymentTransaction.invoiceModel.settled;
  }

  onPaymentDelete(paymentTransactionId) {
    this.paymentTransactionService.remove([paymentTransactionId]).then(() => {
      this.$state.go('root.backend.accounting.transactions.all');
    });
  }
}
