import { PaymentTransactionService } from '../service/PaymentTransactionService';
/**
 * Created by bbs on 2/14/17.
 */

export class PaymentTransactionCtrl {
  private gridApi;
  private gridConfiguration: any = {};
  private rowsSelected: Array<any> = [];

  private columnsDefinition = [
    { name: 'Action', cellTemplate: 'backend/accounting/transactions/partials/cellAction.html' },
    { name: 'invoice', field: 'invoiceModel.invoiceNumber' },
    { name: 'amount', field: 'amount' },
    { name: 'transferDate', field: 'transferDate', cellFilter: "date:'LLLL d, yyyy'" },
    { name: 'bank', field: 'bank' },
    { name: 'bankTransactionId', field: 'bankTransactionId' }
  ];

  constructor(
    private payments,
    private paymentTransactionService: PaymentTransactionService,
    private $state,
    private infoDlg,
    $scope,
    private invoiceNumber: string
  ) {
    this.gridConfiguration = angular.extend(
      {},
      {
        onRegisterApi: gridApi => {
          this.gridApi = gridApi;
          this.gridApi.selection.on.rowSelectionChanged($scope, () => this.onRowsSelected());
          this.gridApi.selection.on.rowSelectionChangedBatch($scope, () => this.onRowsSelected());
        },
        data: this.payments
      },
      { columnDefs: this.columnsDefinition }
    );
  }

  onRowsSelected() {
    this.rowsSelected = [];
    _.each(this.gridApi.selection.getSelectedRows(), row => {
      this.rowsSelected.push(row);
    });
  }

  onPaymentDelete() {
    let selectedRows = this.gridApi.selection.getSelectedRows();
    if (this.checkIfInvoiceshasDunningLevel(selectedRows)) {
      let paymentTransactionIds = _.map(selectedRows, (row: any) => {
        return row.id;
      });
      this.paymentTransactionService.remove(paymentTransactionIds).then(() => {
        this.paymentTransactionService.getAll().then(data => {
          this.payments = data;
          this.gridConfiguration.data = this.payments;
          this.gridApi.grid.modifyRows(this.gridConfiguration.data);
          _.map(this.gridConfiguration.data, (row: any) => {
            if (_.indexOf(paymentTransactionIds, row.id) > -1) {
              this.gridApi.selection.selectRow(row);
            }
          });
        });
      });
    }
  }

  private checkIfInvoiceshasDunningLevel(selectedRows) {
    let paymentsWithSettledInvoices = _.filter(selectedRows, (row: any) => {
      return row.invoiceModel.settled === true;
    });

    if (paymentsWithSettledInvoices.length > 0) {
      // show dialog for invoice not due
      this.infoDlg(
        'Payments with following IDs has been settled therefore cannot be removed or modified: ' +
          _.join(_.map(paymentsWithSettledInvoices, row => row.id), ', ')
      );
      return false;
    }

    return true;
  }
}
