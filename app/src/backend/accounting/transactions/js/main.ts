import * as angular from 'angular';
import * as _misc from 'misc';
import { IGoBack } from '../../../../misc/js/services/AngularInterfaces';

import { PaymentTransactionCtrl } from './controller/PaymentTransactionCtrl';
import { PaymentTransactionService } from './service/PaymentTransactionService';
import { AddPaymentTransactionCtrl } from './controller/AddPaymentTransactionCtrl';
import { EditPaymentTransactionCtrl } from './controller/EditPaymentTransactionCtrl';
import { InvoiceService } from './service/InvoiceService';
/**
 * Created by bbs on 2/14/17.
 */

let misc = _misc;

export let app = angular
  .module('paymentTransactions', ['misc'])
  .controller('paymentTransactionsCtrl', function(
    payments,
    paymentTransactionService: PaymentTransactionService,
    $state,
    infoDlg,
    $scope,
    invoiceNumber
  ) {
    return new PaymentTransactionCtrl(
      payments,
      paymentTransactionService,
      $state,
      infoDlg,
      $scope,
      invoiceNumber
    );
  })
  .controller('addPaymentTransactionsCtrl', function(
    paymentTransaction,
    banks,
    bank,
    $state,
    paymentTransactionService: PaymentTransactionService,
    confirmerDlg,
    goBack: IGoBack
  ) {
    return new AddPaymentTransactionCtrl(
      paymentTransaction,
      banks,
      bank,
      $state,
      paymentTransactionService,
      confirmerDlg,
      goBack
    );
  })
  .controller('editPaymentTransactionsCtrl', function(
    paymentTransaction,
    banks,
    bank,
    $state,
    paymentTransactionService: PaymentTransactionService,
    confirmerDlg,
    goBack: IGoBack
  ) {
    return new EditPaymentTransactionCtrl(
      paymentTransaction,
      banks,
      bank,
      $state,
      paymentTransactionService,
      confirmerDlg,
      goBack
    );
  })
  .service('paymentTransactionService', function(endpoint) {
    return new PaymentTransactionService(endpoint);
  })
  .service('invoiceService', function(endpoint) {
    return new InvoiceService(endpoint);
  })
  .config(function(ngFabFormProvider) {
    let customInsertFn = function(compiledAlert, el, attrs) {
      if (el.hasClass('transferdate-picker')) {
        el.parent().after(compiledAlert);
      } else {
        el.after(compiledAlert);
      }
    };
    ngFabFormProvider.setInsertErrorTplFn(customInsertFn);
  });
