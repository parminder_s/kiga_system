/**
 * Created by bbs on 2/16/17.
 */

export class InvoiceService {
  constructor(private endpoint) {}

  getOne(invoiceNumber: string) {
    return this.endpoint({ url: 'backend/invoices/get', data: { invoiceNumber: invoiceNumber } });
  }
}
