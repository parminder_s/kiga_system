import IPromise = angular.IPromise;
/**
 * Created by bbs on 2/14/17.
 */

export class PaymentTransactionService {
  constructor(private endpoint) {}

  getAll(invoiceNumber: string = 'all') {
    return this.endpoint({ url: `backend/payment-transactions/${invoiceNumber}`, method: 'GET' });
  }

  private update(paymentTransaction, endpointUrl) {
    return this.endpoint({ url: endpointUrl, data: paymentTransaction });
  }

  save(paymentTransaction) {
    if (paymentTransaction.id == null) {
      return this.update(paymentTransaction, 'backend/payment-transactions/add');
    } else {
      return this.update(paymentTransaction, 'backend/payment-transactions/update');
    }
  }

  getOne(paymentTransactionId) {
    return this.endpoint({
      url: 'backend/payment-transactions/get',
      data: { id: paymentTransactionId }
    });
  }

  remove(paymentTransactionIds) {
    return this.endpoint({
      url: 'backend/payment-transactions/remove',
      data: { ids: paymentTransactionIds }
    });
  }

  getModelByInvoice(invoiceNumber: string) {
    return this.endpoint({
      url: 'backend/payment-transactions/get-model',
      data: { invoiceNumber: invoiceNumber }
    });
  }

  getBanksByInvoiceNumber(invoiceNumber: string) {
    return this.endpoint({
      url: 'backend/payment-transactions/banks/by-invoice',
      data: { invoiceNumber: invoiceNumber }
    });
  }

  getBanksByPaymentTransactionId(paymentTransactionId: number) {
    return this.endpoint({
      url: 'backend/payment-transactions/banks/by-transaction',
      data: { paymentTransactionId: paymentTransactionId }
    });
  }

  getAllBanks() {
    return this.endpoint('backend/payment-transactions/banks');
  }
}
