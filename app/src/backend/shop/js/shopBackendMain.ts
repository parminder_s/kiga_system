import * as angular from 'angular';
import * as _misc from 'misc';

import PurchasesController from './controllers/PurchasesController';
import { ShopBackendCreditVoucherCtrl } from './controllers/shopBackendCreditVoucherCtrl';
import ShopBackendModel from './services/ShopBackendModel';
import { ProductOverviewController } from './controllers/ProductOverviewController';
import { VoucherOverviewController } from './controllers/VoucherOverviewController';
import { ProductFormController } from './controllers/ProductFormController';
import { Voucher, VoucherFormController } from './controllers/VoucherFormController';
import { ProductDetailFormModel } from './services/ProductDetailFormModel';
import { ProductFormModel } from './services/ProductFormModel';
import { VoucherFormModel } from './services/VoucherFormModel';
import { CountryModel } from './services/CountryModel';
import PurchaseController from './controllers/PurchaseController';
import { CountriesCtrl } from './controllers/CountriesCtrl';
import { CountryFormCtrl } from './controllers/CountryFormCtrl';
import ProductDetailFormController from './controllers/ProductDetailFormController';

let misc = _misc;

export let app = angular
  .module('shopBackend', ['misc'])
  .service('countryModel', endpoint => new CountryModel(endpoint))
  .service('shopBackendModel', endpoint => new ShopBackendModel(endpoint))
  .controller(
    'countriesCtrl',
    (importantCountries, otherCountries) => new CountriesCtrl(importantCountries, otherCountries)
  )
  .controller(
    'countryFormCtrl',
    (country, countryModel: CountryModel) => new CountryFormCtrl(country, countryModel)
  )
  .controller(
    'voucherFormCtrl',
    ($state, confirmerDlg, voucherFormModel, voucher, productList) =>
      new VoucherFormController($state, confirmerDlg, voucherFormModel, voucher, productList)
  )
  .controller(
    'productOverviewCtrl',
    (productFormModel, uiGridConstants, data) =>
      new ProductOverviewController(productFormModel, uiGridConstants, data)
  )
  .service('productFormModel', endpoint => new ProductFormModel(endpoint))
  .controller(
    'voucherOverviewCtrl',
    (voucherFormModel, uiGridConstants, data) =>
      new VoucherOverviewController(voucherFormModel, uiGridConstants, data)
  )
  .service('voucherFormModel', endpoint => new VoucherFormModel(endpoint))
  .controller(
    'productFormCtrl',
    ($state, confirmerDlg, productFormModel, productDetailFormModel, product, data) =>
      new ProductFormController(
        $state,
        confirmerDlg,
        productFormModel,
        productDetailFormModel,
        product,
        data
      )
  )
  .service('productDetailFormModel', endpoint => new ProductDetailFormModel(endpoint))
  .controller(
    'productDetailFormCtrl',
    ($state, productDetailFormModel, productData, countries, confirmerDlg) =>
      new ProductDetailFormController(
        $state,
        productDetailFormModel,
        productData,
        countries,
        confirmerDlg
      )
  )
  .controller(
    'shopBackendListCtrl',
    ($window, purchases, shopBackendModel, uiGridConstants, $location, confirmerDlg) =>
      new PurchasesController(
        $window,
        purchases,
        shopBackendModel,
        uiGridConstants,
        $location,
        confirmerDlg
      )
  )
  .controller('purchaseCtrl', function($window, purchase, confirmerDlg, shopBackendModel, $state) {
    return new PurchaseController($window, purchase, confirmerDlg, shopBackendModel, $state.go);
  })
  .controller('shopBackendCreditVoucherCtrl', function(
    $window,
    $state,
    purchase,
    shopBackendModel,
    formHelper
  ) {
    return new ShopBackendCreditVoucherCtrl(
      $window,
      $state.go,
      purchase,
      shopBackendModel,
      formHelper,
      angular.isDate
    );
  });
