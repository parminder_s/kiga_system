/**
 * Created by bbs on 10/16/16.
 */

export class CountryGroupModel {
  private endpoint;

  constructor(endpoint) {
    this.endpoint = endpoint;
  }

  getCountryGroups() {
    return this.endpoint({ url: 'countries/groups/list' });
  }
}
