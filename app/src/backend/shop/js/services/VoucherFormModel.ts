/**
 * Created by Florian Schneider on 20/11/17.
 */
export class VoucherFormModel {
  private endpoint;

  constructor(endpoint) {
    this.endpoint = endpoint;
  }

  getVoucher(voucherId: number) {
    return this.endpoint({ url: 'shop/backend/vouchers/get', data: { voucherId: voucherId } });
  }

  save(voucher) {
    return this.endpoint({ url: 'shop/backend/vouchers/save', data: voucher });
  }

  getProductDisplayList() {
    return this.endpoint({ url: 'shop/backend/vouchers/products' });
  }

  // promoteProduct(productId: number): Promise<any> {
  //   return this.endpoint({url: "setPromotionOnMainArticleCategory",
  //     data: {id: productId}});
  // }

  // cloneProduct(productId: number) {
  //   return this.endpoint({url: "shop/backend/products/clone", data: {productId: productId}});
  // }

  deleteById(id: number): Promise<any> {
    return this.endpoint({ url: 'shop/backend/vouchers/deactivate', data: { id: id } });
  }
}
