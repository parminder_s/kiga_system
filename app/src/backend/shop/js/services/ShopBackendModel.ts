import IPromise = angular.IPromise;
import Purchase from '../spec/Purchase';
import PurchaseViewModel from '../spec/PurchaseViewModel';
import * as _ from 'lodash';
import PurchaseViewModelMapper from './PurchaseViewModelMapper';

export default class ShopBackendModel {
  endpoint = null;

  constructor(endpoint) {
    this.endpoint = endpoint;
  }

  callEndpoint(data: Array<number>, url: string): IPromise<boolean> {
    return this.endpoint({ url: url, data: { ids: data } });
  }

  findFromOrdered(hideFinished: boolean): IPromise<Array<PurchaseViewModel>> {
    let returner = this.endpoint.get(`shop/backend/listPurchases?hideFinished=${hideFinished}`);
    returner.then((purchases: Array<Purchase>) =>
      _(purchases)
        .map(PurchaseViewModelMapper.map)
        .value()
    );
    return returner;
  }

  createCreditVoucher(purchase: Purchase): IPromise<Boolean> {
    return this.endpoint({ url: 'shop/backend/createCreditVoucher', data: purchase });
  }

  finishCreditVoucher(id: number): IPromise<Boolean> {
    return this.endpoint({ url: 'shop/backend/finishCreditVoucher', data: { id: id } });
  }

  findPurchase(id: number): IPromise<Purchase> {
    return this.endpoint({ url: 'shop/backend/find', data: { id: id } });
  }

  findByInvoiceNr(invoiceNr: string): IPromise<Purchase> {
    return this.endpoint({ url: 'shop/backend/findByInvoiceNr/' + invoiceNr });
  }

  findByOrderNr(orderNr: string): IPromise<Purchase> {
    return this.endpoint({ url: 'shop/backend/findByOrderNr/' + orderNr });
  }

  createPickingNote(id: number): IPromise<boolean> {
    return this.endpoint({ url: 'shop/backend/createPickingNote', data: { id: id } });
  }

  createDocuments(id: number): IPromise<boolean> {
    return this.endpoint({ url: 'shop/backend/createDocuments', data: { id: id } });
  }

  createSelectedDocuments(selected): IPromise<boolean> {
    return this.endpoint({
      url: 'shop/backend/createSelectedDocuments',
      data: { ids: selected }
    });
  }

  setStatusShipped(id: number): IPromise<boolean> {
    return this.endpoint({ url: 'shop/backend/setStatusShipped', data: { id: id } });
  }

  printSelected(selected): IPromise<boolean> {
    return this.endpoint({ url: 'shop/backend/printSelected', data: { ids: selected } });
  }

  setSelectedStatusShipped(selected): any {
    return this.endpoint({ url: 'shop/backend/setSelectedStatusShipped', data: { ids: selected } });
  }
  resendInvoice(id): any {
    return this.endpoint({ url: 'shop/backend/resendInvoice', data: { id: id } });
  }

  recreateDocuments(id): any {
    return this.endpoint({ url: 'shop/backend/recreateDocuments', data: { id: id } });
  }

  savePurchase(purchase: Purchase): any {
    return this.endpoint({ url: 'shop/backend/savePurchase', data: purchase });
  }

  cancelPurchase(id: number): Promise<PurchaseViewModel> {
    return this.endpoint.post(`shop/backend/cancelPurchase/${id}`);
  }
}
