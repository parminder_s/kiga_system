/**
 * Created by bbs on 10/12/16.
 */
export class ProductFormModel {
  private endpoint;

  constructor(endpoint) {
    this.endpoint = endpoint;
  }

  getProduct(productId: number) {
    return this.endpoint({ url: 'shop/backend/products/get', data: { productId: productId } });
  }

  getData(productId: number) {
    return this.endpoint({
      url: 'shop/backend/products/details/list',
      data: { productId: productId }
    });
  }

  save(productDetail) {
    return this.endpoint({ url: 'shop/backend/products/save', data: productDetail });
  }

  promoteProduct(productId: number): Promise<any> {
    return this.endpoint({
      url: 'setPromotionOnMainArticleCategory',
      data: { id: productId }
    });
  }

  cloneProduct(productId: number) {
    return this.endpoint({ url: 'shop/backend/products/clone', data: { productId: productId } });
  }

  deleteById(id: number): Promise<any> {
    return this.endpoint({ url: 'shop/backend/products/deactivate', data: { id: id } });
  }
}
