import * as _ from 'lodash';
import * as angular from 'angular';
import { columnDefs } from '../mainList/columnDefs';
import { mainGridOptions } from '../mainList/mainGridOptions';
import { Toolbar } from '../mainList/toolbar';
import ShopBackendModel from '../services/ShopBackendModel';
import Purchase from '../spec/Purchase';
import IWindowService = angular.IWindowService;
import ILocationService = angular.ILocationService;

export default class PurchasesController {
  $location: ILocationService;
  $window: IWindowService;
  confirmer;
  data: Array<Purchase>;
  hideFinished = true;
  shopBackendModel: ShopBackendModel;
  gridApi = null;
  gridOptions = null;
  toolbar: Toolbar;

  constructor($window, purchases, shopBackendModel, uiGridConstants, $location, confirmer) {
    this.$location = $location;
    this.$window = $window;
    this.confirmer = confirmer;
    this.data = purchases;
    this.gridOptions = this.createGridOptions(
      columnDefs(function(name) {
        return `backend/shop/partials/${name}.html`;
      }, uiGridConstants),
      mainGridOptions,
      this.getBaseUrl()
    );
    this.shopBackendModel = shopBackendModel;
    this.toolbar = this.createToolbar($window);
  }

  createToolbar($window): Toolbar {
    let getSelected = (): Array<string> => {
      let rows: Array<any> = this.gridApi.selection.getSelectedRows();
      return _(rows)
        .map<number>('id')
        .map(id => id.toString())
        .value();
    };

    return new Toolbar(getSelected, this.refresh, this.callEndpoint, $window, this.getBaseUrl());
  }

  getBaseUrl(): string {
    return window['kiga'].endPointUrl;
  }

  onHideFinished() {
    this.$window.setTimeout(() => this.refresh(), 0);
  }

  refresh: any = () => {
    this.shopBackendModel.findFromOrdered(this.hideFinished).then(purchases => {
      this.data = purchases;
    });
  };

  callEndpoint: any = (data, url) => {
    this.shopBackendModel.callEndpoint(data, url).then(this.refresh);
  };

  createShippingNote(purchase: Purchase): any {
    this.confirmer('Create Shipping Note and Invoice', () => {
      this.shopBackendModel.createDocuments(purchase.id).then(this.refresh);
    });
  }

  setStatusShipped(purchase: Purchase): any {
    this.confirmer("Change Status to 'shipped'?", () => {
      this.shopBackendModel.setStatusShipped(purchase.id).then(this.refresh);
    });
  }

  createGridOptions(columnDefs, mainGridOptions, baseUrl): { [key: string]: any } {
    return angular.extend(mainGridOptions, {
      clearAllFilters: test => {
        this.gridApi.clearAllFilters();
      },
      onRegisterApi: gridApi => {
        this.gridApi = gridApi;
      },
      data: 'ctrl.data',
      columnDefs: columnDefs,
      appScopeProvider: {
        downloadInvoice: id => {
          this.$window.open(`${baseUrl}/shop/backend/downloadInvoice/${id}`, '_blank');
        },

        downloadShippingNote: id => {
          this.$window.open(`${baseUrl}/shop/backend/downloadShippingNote/${id}`, '_blank');
        },

        downloadShippingLabel: id => {
          this.$window.open(`${baseUrl}/shop/backend/downloadShippingLabel/${id}`, '_blank');
        },

        downloadOrderNote: id => {
          this.$window.open(`${baseUrl}/shop/backend/downloadOrderNote`, '_blank');
        },

        createShippingNote: (purchase: Purchase) => {
          this.createShippingNote(purchase);
        },

        setStatusShipped: (purchase: Purchase) => {
          this.setStatusShipped(purchase);
        }
      }
    });
  }
}
