import { CountryModel } from '../services/CountryModel';
/**
 * Created by bbs on 3/14/17.
 */

export class CountryFormCtrl {
  constructor(private country, private countryModel: CountryModel) {}

  saveCountry() {
    this.countryModel.save(this.country);
  }
}
