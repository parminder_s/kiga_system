import { VoucherFormModel } from '../services/VoucherFormModel';
import * as _ from 'lodash';
/**
 * Created by Florian Schneider on 20/11/17.
 */
interface ProductOption {
  code: number;
  title: string;
}

export interface Voucher {
  validFrom: Date;
  validTo: Date;
  productId: number;
  code: string;
  discountRate: number;
}

export class VoucherFormController {
  private productOptions: Array<ProductOption>;

  constructor(
    private $state,
    private confirmerDlg,
    private voucherFormModel: VoucherFormModel,
    private voucher: Voucher,
    productList: any
  ) {
    this.voucherFormModel = voucherFormModel;
    this.voucher = voucher;

    this.productOptions = _.map(productList, (key: string, value: string) => {
      return { code: parseInt(value), title: key };
    });
  }

  saveVoucher() {
    this.voucherFormModel
      .save(this.voucher)
      .then(() => this.$state.go('root.backend.shop.vouchers'));
  }

  onVoucherDelete(id: number) {
    this.confirmerDlg('CONFIRM_DELETE', () =>
      this.voucherFormModel.deleteById(id).then(() => this.$state.go('root.backend.shop.vouchers'))
    );
  }
}
