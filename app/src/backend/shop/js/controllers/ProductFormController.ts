import { ProductFormModel } from '../services/ProductFormModel';
import { ProductDetailFormModel } from '../services/ProductDetailFormModel';
/**
 * Created by bbs on 10/9/16.
 */
export class ProductFormController {
  private productDetailsGrid: any;
  private columnsDefinition = [
    { name: 'Action', cellTemplate: `backend/shop/partials/productForm/cellAction.html` },
    { name: 'Country', field: 'country' },
    { name: 'Currency', field: 'currencyCode' },
    {
      name: 'Price (Gross)',
      field: 'priceGross',
      cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.priceGross | number:2}}</div>'
    },
    {
      name: 'Product Vat',
      field: 'vatProduct',
      cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.vatProduct * 100}}%</div>'
    }
  ];

  constructor(
    private $state,
    private confirmerDlg,
    private productFormModel: ProductFormModel,
    private productDetailFormModel: ProductDetailFormModel,
    private product: any,
    private data: any
  ) {
    if (product !== null) {
      this.productDetailsGrid = angular.extend(
        {},
        { data: this.data },
        { columnDefs: this.columnsDefinition }
      );
    } else {
      this.$state.go('root.backend.shop.products');
    }
  }

  saveProduct() {
    this.productFormModel
      .save(this.product)
      .then(() => this.$state.go('root.backend.shop.products'));
  }

  onProductDelete(id: number) {
    this.confirmerDlg('CONFIRM_DELETE', () =>
      this.productFormModel.deleteById(id).then(() => this.$state.go('root.backend.shop.products'))
    );
  }

  promoteProduct() {
    this.productFormModel
      .promoteProduct(this.product.id)
      .then(() => this.$state.go('root.backend.shop.products'));
  }

  cloneProduct() {
    this.productFormModel
      .cloneProduct(this.product.id)
      .then(({ clonedProductId }) =>
        this.$state.go('root.backend.shop.product', { productId: clonedProductId })
      );
  }
}
