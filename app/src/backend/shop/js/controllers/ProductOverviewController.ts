import { ProductFormModel } from '../services/ProductFormModel';
import { mainGridOptions } from '../mainList/mainGridOptions';

/**
 * Created by bbs on 10/9/16.
 */

export class ProductOverviewController {
  private gridConfiguration: any = {};
  private productFormModel: ProductFormModel;

  constructor(productFormModel: ProductFormModel, uiGridConstants, private data) {
    this.productFormModel = productFormModel;
    this.gridConfiguration = angular.extend(
      mainGridOptions,
      { data: this.data },
      {
        columnDefs: [
          {
            name: 'Action',
            cellTemplate: `backend/shop/partials/productsOverview/cellAction.html`
          },
          {
            name: 'Product Title',
            field: 'productTitle',
            sort: { direction: uiGridConstants.ASC, priority: 1 }
          },
          { name: 'Code', field: 'code' },
          { name: 'Weight', field: 'weightGram' },
          { name: 'Volume', field: 'volumeLitre' }
        ]
      }
    );
  }
}
