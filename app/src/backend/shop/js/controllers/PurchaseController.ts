import ShopBackendModel from '../services/ShopBackendModel';
import * as _ from 'lodash';
import IWindowService = angular.IWindowService;

export default class PurchaseController {
  public data = null;
  public baseUrl = null;
  public shippingPartnerOptions = null;
  public showCancelPurchase: boolean;
  public showCreateCreditVoucher: boolean;
  public showFinishCreditVoucher: boolean;

  constructor(private $window, purchase, private confirmer, private shopBackendModel, private go) {
    this.data = purchase;
    this.$window = $window;
    this.shopBackendModel = shopBackendModel;
    this.baseUrl = PurchaseController.getBaseUrl();
    this.confirmer = confirmer;
    this.shippingPartnerOptions = [
      { title: 'Post', value: 'POST' },
      { title: 'DHL', value: 'DHL' }
    ];
    this.go = go;
    this.showCancelPurchase =
      this.data.paymentMethod === 'INVOICE' &&
      ['CreditVoucherPending', 'CreditVoucherFinalized', 'Cancelled'].indexOf(this.data.status) ===
        -1;
    this.showFinishCreditVoucher = this.data.status === 'CreditVoucherPending';
    this.showCreateCreditVoucher =
      ['CreditVoucherPending', 'CreditVoucherFinalized', 'Cancelled'].indexOf(this.data.status) ===
      -1;
  }

  submit() {
    this.confirmer('Change Purchase?', () => this.shopBackendModel.savePurchase(this.data));
  }

  downloadShippingNote(id: Number) {
    this.$window.open(`${this.baseUrl}/shop/backend/downloadShippingNote/${id}`, '_blank');
  }

  resendInvoice(id: Number) {
    this.confirmer('Resend Invoice to Customer?', () => this.shopBackendModel.resendInvoice(id));
  }

  recreateDocuments(id: Number) {
    this.confirmer('Create new Invoice and ShippingNote?', () =>
      this.shopBackendModel.recreateDocuments(id)
    );
  }
  createCreditVoucher() {
    this.go('root.backend.shop.creditVoucher', { orderNr: this.data.id });
  }

  finishCreditVoucher() {
    this.shopBackendModel.finishCreditVoucher(this.data.id);
  }

  async cancelPurchase() {
    this.confirmer('Do you really want to cancel this purchase?', async () => {
      this.data = await this.shopBackendModel.cancelPurchase(this.data.id);
    });
  }

  getInvoiceVersion(id: Number) {
    this.$window.open(`${this.baseUrl}/shop/backend/getInvoiceVersion/${id}`, '_blank');
  }

  static getBaseUrl(): string {
    return window['kiga'].endPointUrl;
  }
}
