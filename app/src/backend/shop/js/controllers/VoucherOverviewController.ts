import { VoucherFormModel } from '../services/VoucherFormModel';

/**
 * Created by Florian Schneider on 20/11/17.
 */

export class VoucherOverviewController {
  private gridConfiguration: any = {};
  private voucherFormModel: VoucherFormModel;

  constructor(voucherFormModel: VoucherFormModel, uiGridConstants, private data) {
    this.voucherFormModel = voucherFormModel;
    this.gridConfiguration = angular.extend(
      {},
      { data: this.data },
      {
        columnDefs: [
          {
            name: 'Action',
            cellTemplate: `backend/shop/partials/vouchersOverview/cellAction.html`
          },
          { name: 'Code', field: 'code' },
          { name: 'Discount Rate', field: 'discountRate' },
          {
            name: 'Product',
            cellTemplate: `backend/shop/partials/vouchersOverview/productAction.html`
          }
        ]
      }
    );
  }
}
