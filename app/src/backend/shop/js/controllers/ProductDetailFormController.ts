import { ProductDetailFormModel } from '../services/ProductDetailFormModel';
import * as _ from 'lodash';
/**
 * Created by bbs on 10/10/16.
 */
export default class ProductDetailFormController {
  private country: any;

  constructor(
    private $state,
    private productDetailFormModel: ProductDetailFormModel,
    private productDetail: any,
    private countries: any,
    private confirmerDlg
  ) {
    this.country = _.find(
      this.countries,
      country => country['id'] === this.productDetail.countryId
    );
  }

  saveProductDetail() {
    this.productDetailFormModel.save(this.productDetail).then(() => {
      this.$state.go('root.backend.shop.product', { productId: this.productDetail.productId });
    });
  }

  changeCountry() {
    this.productDetail.countryId = this.country.id;
  }

  onProductDetailDeactivate(id: number) {
    this.confirmerDlg('CONFIRM_DEACTIVATE', () => {
      this.productDetailFormModel.deactivateById(id).then(() => {
        this.$state.go('root.backend.shop.product', { productId: this.productDetail.productId });
      });
    });
  }

  onProductDetailActivate(id: number) {
    this.confirmerDlg('CONFIRM_ACTIVATE', () => {
      this.productDetailFormModel.activateById(id).then(() => {
        this.$state.go('root.backend.shop.product', { productId: this.productDetail.productId });
      });
    });
  }
}
