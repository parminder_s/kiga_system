import Purchase from '../spec/Purchase';
import ShopBackendModel from '../services/ShopBackendModel';

export class Toolbar {
  getSelected: () => Array<string>;
  $window: Window;
  baseUrl: string;
  refresh: () => any;
  callback: (url, data) => any;
  urls = {
    csv: 'shop/backend/getSelectedCsv',
    orders: 'shop/backend/printSelectedOrders',
    shippingNotes: 'shop/backend/printSelectedShippingNotes',

    setreadyforshipping: 'shop/backend/setSelectedReadyForShipping',
    setexportfin: 'shop/backend/setSelectedCsvFin',
    setshippingfin: 'shop/backend/setSelectedStatusShipped',
    reset: 'shop/backend/resetSelected'
  };

  constructor(
    getSelected: () => Array<string>,
    refreshFn: () => any,
    callbackFn: (url, data) => any,
    $window: Window,
    baseUrl: string
  ) {
    this.getSelected = getSelected;
    this.$window = $window;
    this.baseUrl = baseUrl;
    this.refresh = refreshFn;
    this.callback = callbackFn;
  }

  getSelectedShippingNotes() {
    this.openIfSelected(this.urls.shippingNotes);
  }

  setSelectedReadyForShipping() {
    this.postIfSelected(this.urls.setreadyforshipping);
  }

  setSelectedExportFin() {
    this.postIfSelected(this.urls.setexportfin);
  }

  setSelectedShippingFin() {
    this.postIfSelected(this.urls.setshippingfin);
  }

  resetSelected() {
    this.postIfSelected(this.urls.reset);
  }

  getSelectedCsv() {
    this.openIfSelected(this.urls.csv);
  }

  printSelectedOrder() {
    this.openIfSelected(this.urls.orders);
  }

  postIfSelected(urlPart: string) {
    let selected = this.getSelected();
    if (selected.length > 0) {
      this.callback(selected, urlPart);
    }
  }

  openIfSelected(urlPart: string) {
    let selected = this.getSelected();
    if (selected.length > 0) {
      this.$window.open(`${this.baseUrl}/${urlPart}/${selected}`, '_blank');
    }
  }
}
