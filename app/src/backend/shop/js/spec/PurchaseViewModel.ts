import Purchase from './Purchase';
interface PurchaseViewModel extends Purchase {
  showAccounting: boolean;
  showPspId: boolean;
}

export default PurchaseViewModel;
