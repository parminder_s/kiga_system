import PurchaseItem from './PurchaseItem';
interface Purchase {
  id: number;
  orderNr: number;
  firstName: string;
  lastName: string;
  country: string;
  priceNetTotal: string;
  priceGrossTotal: string;
  currency: string;
  status: string;
  amount: number;
  shippingNoteNumber: string;
  shippingPartner: string;
  invoiceNumber: string;
  memberId: string;
  memberEmail: string;
  purchaseDate: Date;
  paymentMethod: string;

  street: string;
  streetNumber: string;
  floorNumber: string;
  doorNumber: string;
  city: string;
  zip: string;

  billStreet: string;
  billStreetNumber: string;
  billFloorNumber: string;
  billDoorNumber: string;
  billCity: string;
  billZip: string;
  billAddressName1: string;
  billAddressName2: string;
  purchaseItems: PurchaseItem[];
  countryCode: string;
  comment: string;
}

export default Purchase;
