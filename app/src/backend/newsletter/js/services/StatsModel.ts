export interface StatsModel {
  total: number;
  pending: number;
  done: number;
  skipped: number;
  error: number;
}
