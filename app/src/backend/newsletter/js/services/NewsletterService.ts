import * as _ from 'lodash';
import * as angular from 'angular';

import IPromise = angular.IPromise;
import { NewsletterModel } from './NewsletterModel';
import { RecipientSelectionModel } from './RecipientSelectionModel';
import { SendTaskModel } from './SendTaskModel';

interface FetchNewsletterResult {
  newsletters: Array<NewsletterModel>;
  selections: Array<RecipientSelectionModel>;
}

interface PreviewResult {
  content: string;
}

declare var kiga: any;

export class NewsletterService {
  constructor(private endpoint) {}

  /**
   * Get all newsletters.
   */
  getNewsletters(): IPromise<Array<NewsletterModel>> {
    return this.endpoint({
      url: 'backend/newsletter/newsletters',
      method: 'GET'
    }).then((res: FetchNewsletterResult) => {
      // Adding selections to the newsletter ...
      return _.map(res.newsletters, (newsletter: NewsletterModel) => {
        newsletter.selections = [];
        return newsletter;
      });
    });
  }

  /**
   * Get detailed data of a newsletter by id.
   */
  getNewsletter(id: number): IPromise<NewsletterModel> {
    return this.endpoint({
      url: 'backend/newsletter/newsletter/' + id,
      method: 'GET'
    });
  }

  /**
   * Get contents (html) of a newsletter.
   */
  getNewsletterPreview(newsletter: NewsletterModel): IPromise<any> {
    return this.endpoint({
      url: 'backend/newsletter/newsletter/' + newsletter.id + '/preview',
      method: 'GET'
    }).then((res: PreviewResult) => res.content);
  }

  /**
   * Get contents (html) of a newsletter (actual html).
   */
  getNewsletterPreviewUrl(newsletter: NewsletterModel): string {
    return kiga.endPointUrl + 'backend/newsletter/newsletter/' + newsletter.id + '/previewRaw';
  }

  /**
   * Create a new selection for a newsletter.
   */
  createSelection(newsletter: NewsletterModel, selection?: RecipientSelectionModel) {
    return this.endpoint({
      url: 'backend/newsletter/newsletter/' + newsletter.id + '/selections',
      method: 'POST'
    });
  }

  /**
   * Update an existing selection.
   */
  updateSelection(selection: RecipientSelectionModel) {
    return this.endpoint({
      url: 'backend/newsletter/selection/' + selection.id + '/update',
      method: 'POST',
      data: selection
    });
  }

  /**
   * Remove an existing selection.
   */
  removeSelection(selection: RecipientSelectionModel) {
    return this.endpoint({
      url: 'backend/newsletter/selection/' + selection.id + '/delete',
      method: 'POST'
    });
  }

  /**
   * Send a test mail of a newsletter to a single email address.
   */
  sendTestNewsletter(newsletter: NewsletterModel, email: string): IPromise<SendTaskModel> {
    return this.endpoint({
      url: 'backend/newsletter/newsletter/' + newsletter.id + '/testsend',
      method: 'POST',
      data: { email: email }
    });
  }

  /**
   * Send newsletter
   */
  sendNewsletter(newsletter: NewsletterModel, sendDate?: Date): IPromise<SendTaskModel> {
    let data = {};
    if (sendDate) {
      data['sendDate'] = sendDate.toISOString();
    }
    return this.endpoint({
      url: 'backend/newsletter/newsletter/' + newsletter.id + '/send',
      method: 'POST',
      data: data
    });
  }

  /**
   * Fetch send task status
   */
  getSendTaskStatus(id: number): IPromise<SendTaskModel> {
    return this.endpoint({
      url: 'backend/newsletter/sendtask/' + id,
      method: 'GET'
    });
  }
}
