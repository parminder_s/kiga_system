import { StatsModel } from './StatsModel';

export interface RecipientSelectionModel {
  id: number;
  locale: string;
  country: string;
  countryParent: string;
  subscriptionType: string;
  shopFlag: boolean;
  subscriptionFlag: boolean;
  subscriptionStatus: string;
  subscriptionDuration: number;
  recipients: number;
  stats: StatsModel;
}
