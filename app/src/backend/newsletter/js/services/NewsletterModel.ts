import { RecipientSelectionModel } from './RecipientSelectionModel';
import { StatsModel } from './StatsModel';

export interface NewsletterModel {
  id: number;
  locale: string;
  name: string;
  publishingDate: Date;
  subject: string;
  url: string;
  previewUrl: string;
  selections: Array<RecipientSelectionModel>;
  stats: StatsModel;
}
