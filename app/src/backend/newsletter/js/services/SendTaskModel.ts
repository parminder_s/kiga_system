import { StatsModel } from './StatsModel';

export interface SendTaskModel {
  taskId: number;
  created: Date;
  finished: Date;
  isFinished: boolean;
  stats: StatsModel;
  sendDate: Date;
}
