import * as _ from 'lodash';
import * as angular from 'angular';
import { StateService } from 'angular-ui-router';
import { NewsletterModel } from '../services/NewsletterModel';

export class NewsletterHeaderCtrl {
  newsletter: NewsletterModel;
  constructor(public $state: StateService) {}
}
