import * as _ from 'lodash';
import * as angular from 'angular';
import { StateParams } from 'angular-ui-router';
import ToastService from '../../../../misc/toast/ToastService';
import { NewsletterService } from '../services/NewsletterService';
import { NewsletterModel } from '../services/NewsletterModel';
import { SendTaskModel } from '../services/SendTaskModel';

export class NewsletterPreviewCtrl {
  newsletter: NewsletterModel;
  testmail: string;
  includeNewsletterUrl: any;

  constructor(
    private newsletterService: NewsletterService,
    private toastService: ToastService,
    $sce: any,
    $stateParams: StateParams
  ) {
    this.newsletterService
      .getNewsletter($stateParams['newsletterId'])
      .then((newsletter: NewsletterModel) => {
        // Newsletter
        this.newsletter = newsletter;
        this.newsletter.previewUrl = this.newsletterService.getNewsletterPreviewUrl(newsletter);

        this.includeNewsletterUrl = $sce.trustAsResourceUrl(this.newsletter.previewUrl);
      });
  }

  /**
   * Send test mail.
   * @param newsletter
   * @param email
   */
  testSend(newsletter: NewsletterModel, email: string) {
    this.newsletterService
      .sendTestNewsletter(newsletter, email)
      .then((taskStatus: SendTaskModel) => {
        this.toastService.success('Testmail to ' + email + ' was sent.');
      });
  }
}
