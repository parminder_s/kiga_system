import * as _ from 'lodash';
import * as angular from 'angular';
import { NewsletterService } from '../services/NewsletterService';
import { NewsletterModel } from '../services/NewsletterModel';
import { SendTaskModel } from '../services/SendTaskModel';

export class NewsletterSendCtrl {
  public statusSend = false;
  public statusDone = false;
  public interval: any;
  public sendResult: any;

  public dummyProgressStatus = 0;

  constructor(
    private $uibModalInstance,
    private newsletterService: NewsletterService,
    public newsletter: NewsletterModel,
    public sendDate: Date
  ) {
    this.newsletterService.getNewsletter(newsletter.id).then(detail => {
      this.newsletter.selections = detail.selections;
    });
  }

  close() {
    this.$uibModalInstance.close(this.statusDone);
  }

  getTotals(newsletter: NewsletterModel) {
    return _.sum(_.map(newsletter.selections, 'stats.pending'));
  }

  getProgressBarWidth() {
    return 200 * (this.dummyProgressStatus / 10);
  }

  /**
   * Submit a send task to the backend ("send a newsletter").
   */
  send() {
    this.statusSend = true;
    this.statusDone = false;
    this.stopTrackSendStatus();
    this.newsletterService
      .sendNewsletter(this.newsletter, this.sendDate)
      .then((taskStatus: SendTaskModel) => {
        this.sendResult = taskStatus;
        this.trackSendStatus(taskStatus.taskId);
      });
  }

  /**
   * For a sendTask-id, query backend once.
   */
  updateSendStatus(sendTaskId: number) {
    return this.newsletterService
      .getSendTaskStatus(sendTaskId)
      .then((taskStatus: SendTaskModel) => {
        this.sendResult = taskStatus;

        this.dummyProgressStatus = (this.dummyProgressStatus + 1) % 9 + 1;

        if (taskStatus.isFinished) {
          this.stopTrackSendStatus();
          this.statusSend = false;
          this.statusDone = true;
          this.dummyProgressStatus = 0;
        }
      });
  }

  /**
   * Set up an interval to periodically poll the status of a sendTask.
   */
  trackSendStatus(sendTaskId: number) {
    this.interval = setInterval(() => this.updateSendStatus(sendTaskId), 1000);
  }

  /**
   * Clears / ends the current interval.
   */
  stopTrackSendStatus() {
    clearInterval(this.interval);
  }
}
