import * as _ from 'lodash';
import * as angular from 'angular';
import { NewsletterService } from '../services/NewsletterService';
import { NewsletterModel } from '../services/NewsletterModel';
import { RecipientSelectionModel } from '../services/RecipientSelectionModel';

export class NewsletterListCtrl {
  public newsletters: Array<NewsletterModel>;

  constructor(private newsletterService: NewsletterService) {
    this.loadList();
  }

  loadList() {
    this.newsletterService.getNewsletters().then(v => (this.newsletters = v));
  }
}
