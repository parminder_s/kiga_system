import * as _ from 'lodash';
import * as angular from 'angular';
import { StateParams } from 'angular-ui-router';
import { NewsletterService } from '../services/NewsletterService';
import { NewsletterModel } from '../services/NewsletterModel';
import { RecipientSelectionModel } from '../services/RecipientSelectionModel';

export class NewsletterSelectionCtrl {
  public newsletter: NewsletterModel;
  public sendDate: Date;
  public sendHour: number;

  public localeOptions = [
    { code: 'de', title: 'de' },
    { code: 'it', title: 'it' },
    { code: 'en', title: 'en' },
    { code: 'tr', title: 'tr' }
  ];

  public countryOptions = [
    { code: null, title: '*' },
    { code: 'at', title: 'at' },
    { code: 'de', title: 'de' },
    { code: 'ch', title: 'ch' },
    { code: 'it', title: 'it' },
    { code: 'lu', title: 'lu' },
    { code: 'tr', title: 'tr' },
    { code: 'be', title: 'be' },
    { code: 'li', title: 'li' },
    { code: 'us', title: 'us' },
    { code: 'hu', title: 'hu' },
    { code: 'es', title: 'es' },
    { code: 'fr', title: 'fr' },
    { code: 'gb', title: 'gb' }
  ];

  public parentCountryOptions = [
    { code: null, title: '*' },
    { code: 'EU1', title: 'EU1' },
    { code: 'WRLD', title: 'WRLD' }
    // {code: "", title: "none"}
  ];

  public binaryOptions = [
    { code: null, title: '*' },
    { code: true, title: 'Yes' },
    { code: false, title: 'No' }
  ];

  public subscriptionOptions = [
    { code: null, title: '*' },
    { code: 'paed', title: 'Paed' },
    { code: 'test', title: 'Test' },
    { code: 'parent', title: 'Parents' },
    { code: 'gift', title: 'Gift' }
  ];

  public durationOptions = [
    { code: null, title: '*' },
    { code: 1, title: '1' },
    { code: 3, title: '3' },
    { code: 6, title: '6' },
    { code: 12, title: '12' }
  ];

  public subscriptionStatusOptions = [
    { code: null, title: '*' },
    { code: 'active', title: 'active' },
    { code: 'end', title: 'end' },
    { code: 'new', title: 'new' },
    { code: 'locked', title: 'locked' }
  ];

  constructor(
    private newsletterService: NewsletterService,
    private $stateParams: StateParams,
    private $uibModal
  ) {
    this.loadDetail($stateParams['newsletterId']);
  }

  loadDetail(newsletterId: number) {
    this.newsletterService.getNewsletter(newsletterId).then(newsletter => {
      this.newsletter = newsletter;
    });
  }

  createSelection(nl: NewsletterModel) {
    return this.newsletterService.createSelection(nl).then(v => this.loadDetail(nl.id));
  }

  updateSelection(nl: NewsletterModel, sel: RecipientSelectionModel) {
    return this.newsletterService.updateSelection(sel).then(response => {
      // Updating the view by reloading the newsletter.
      // TODO : better to refresh only the one selection that was updated.
      this.loadDetail(nl.id);
    });
  }

  removeSelection(nl: NewsletterModel, sel: RecipientSelectionModel) {
    return this.newsletterService.removeSelection(sel).then(v => this.loadDetail(nl.id));
  }

  /**
   * Test that selections fit the newsletter locale.
   */
  testSelectionLocales() {
    if (this.newsletter) {
      let languageCode = this.newsletter.locale.slice(0, 2);
      for (let selection of this.newsletter.selections) {
        if (selection.locale !== languageCode) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Provide a Datetime from the date and hour values for sendDate.
   */
  getSendDateTime(): Date {
    if (this.sendDate) {
      let returner = this.sendDate ? new Date(this.sendDate.toString()) : new Date();
      returner.setHours(this.sendHour || 0);
      returner.setMinutes(0);
      returner.setSeconds(0);
      return returner;
    } else {
      return new Date();
    }
  }

  send(nl: NewsletterModel) {
    let modalInstance = this.$uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'backend/newsletter/partials/send-modal.html',
      controller: 'newsletterSendCtrl',
      controllerAs: 'ctrl',
      windowClass: 'fullscreen',
      resolve: {
        newsletter: () => {
          return nl;
        },
        sendDate: () => this.getSendDateTime()
      }
    });

    modalInstance.result.then(result => {
      if (result) {
        // reload
      }
    });
  }
}
