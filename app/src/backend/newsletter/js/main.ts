import * as angular from 'angular';
import * as _misc from 'misc';
import { StateService, StateParams } from 'angular-ui-router';
import { ISCEService } from 'angular';
import ToastService from '../../../misc/toast/ToastService';
import { NewsletterSelectionCtrl } from './controllers/NewsletterSelectionCtrl';
import { NewsletterHeaderCtrl } from './controllers/NewsletterHeaderCtrl';
import { NewsletterListCtrl } from './controllers/NewsletterListCtrl';
import { NewsletterPreviewCtrl } from './controllers/NewsletterPreviewCtrl';
import { NewsletterSendCtrl } from './controllers/NewsletterSendCtrl';
import { NewsletterModel } from './services/NewsletterModel';
import { NewsletterService } from './services/NewsletterService';
let misc = _misc;

import '../newsletter-backend.less';

export let app = angular
  .module('newsletter', ['misc'])
  .service('newsletterService', function(endpoint) {
    return new NewsletterService(endpoint);
  })
  .controller('newsletterListCtrl', function(newsletterService: NewsletterService) {
    return new NewsletterListCtrl(newsletterService);
  })
  .controller('newsletterSelectionCtrl', function(
    newsletterService: NewsletterService,
    $stateParams: StateParams,
    $uibModal
  ) {
    return new NewsletterSelectionCtrl(newsletterService, $stateParams, $uibModal);
  })
  .controller('newsletterHeaderCtrl', function($state: StateService) {
    return new NewsletterHeaderCtrl($state);
  })
  .controller('newsletterPreviewCtrl', function(
    newsletterService: NewsletterService,
    toastService: ToastService,
    $sce: ISCEService,
    $stateParams: StateParams
  ) {
    return new NewsletterPreviewCtrl(newsletterService, toastService, $sce, $stateParams);
  })
  .controller('newsletterSendCtrl', function(
    $uibModalInstance,
    newsletterService: NewsletterService,
    newsletter: NewsletterModel,
    sendDate: Date
  ) {
    return new NewsletterSendCtrl($uibModalInstance, newsletterService, newsletter, sendDate);
  })
  .component('newsletterHeader', {
    templateUrl: 'backend/newsletter/partials/header.html',
    controller: 'newsletterHeaderCtrl',
    bindings: {
      newsletter: '='
    }
  });
