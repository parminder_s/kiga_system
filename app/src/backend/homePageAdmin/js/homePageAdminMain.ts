import * as angular from 'angular';
import * as _misc from 'misc';

import { HomeEditCtrl } from './controllers/homeEditCtrl';
import { StateService } from 'angular-ui-router';

let misc = _misc;

export let app = angular
  .module('homePageAdmin', ['misc'])
  .controller('homeEditCtrl', function(
    endpoint,
    config,
    $state: StateService,
    confirmerDlg,
    $stateParams,
    formHelper
  ) {
    return new HomeEditCtrl(endpoint, $state, confirmerDlg, $stateParams, formHelper, 'homepage');
  })
  .controller('shopSliderCtrl', function(
    endpoint,
    config,
    $state: StateService,
    confirmerDlg,
    $stateParams,
    formHelper
  ) {
    return new HomeEditCtrl(endpoint, $state, confirmerDlg, $stateParams, formHelper, 'shop');
  });
