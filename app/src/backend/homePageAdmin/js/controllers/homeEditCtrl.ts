export class HomeEditCtrl {
  slider = null;
  topic = null;
  endpoint = null;
  config = null;
  types = null;
  colors = null;
  previewOptions = null;
  $parent = null;
  $state = null;
  new = null;
  positions = null;
  $stateParams;
  confirmerDlg = null;
  formHelper = null;
  bigUploadedFiles = null;
  bigFile;
  bigFileId: Array<number>;
  constructor(endpoint, $state, confirmerDlg, $stateParams, formHelper, private context) {
    this.endpoint = endpoint;
    this.$state = $state;
    this.confirmerDlg = confirmerDlg;
    this.$stateParams = $stateParams;
    endpoint('homepage/list/slider/' + context, resp => {
      this.slider = resp;
    });
    endpoint('homepage/list/thema/' + context, resp => {
      this.topic = resp;
      console.log(this.topic.length);
    });
    this.previewOptions = {
      previewMaxWidth: 200,
      previewMaxHeight: 200,
      previewCrop: true,
      maxFileSize: 1000000,
      acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
    };
    this.types = { slider: 'slider' };
    this.new = {};
    this.new.context = this.context;
    this.positions = { slider: [], thema: [] };
    this.formHelper = formHelper;
    this.colors = {
      orange: 'orange',
      blue: 'blue',
      pink: 'pink',
      red: 'red',
      green: 'green'
    };
  }
  addBigImage(files) {
    this.$parent.$parent.ctrl.endpoint(
      {
        url: 'homepage/uploadImage',
        files: { file: files[0] }
      },
      resp => (this.$parent.elem.big = resp)
    );
  }

  addNewBigImage(files) {
    this.$parent.$parent.ctrl.endpoint(
      {
        url: 'homepage/uploadImage',
        files: { file: files[0] }
      },
      resp => (this.$parent.$parent.ctrl.new.big = resp)
    );
  }

  addSmallImage(files) {
    this.$parent.$parent.ctrl.endpoint(
      {
        url: 'homepage/uploadImage',
        files: { file: files[0] }
      },
      resp => (this.$parent.elem.small = resp)
    );
  }
  addNewSmallImage(files) {
    this.$parent.$parent.ctrl.endpoint(
      {
        url: 'homepage/uploadImage',
        files: { file: files[0] }
      },
      resp => (this.$parent.$parent.ctrl.new.small = resp)
    );
  }

  save(elem) {
    if (elem.bigUploaded) {
      elem.big.id = elem.bigUploaded[0];
    }
    if (elem.smallUploaded) {
      elem.small.id = elem.smallUploaded[0];
    }
    this.endpoint(
      {
        url: 'homepage/update',
        data: { homePageElement: elem }
      },
      resp => this.$state.reload()
    );
  }

  add() {
    this.endpoint(
      {
        url: 'homepage/add',
        data: { homePageElement: this.new }
      },
      resp => this.$state.reload()
    );
  }

  delete(elem) {
    this.confirmerDlg('FORM_CONFIRM_DELETE', () =>
      this.endpoint(
        {
          url: 'homepage/delete/' + elem.id
        },
        resp => this.deleteCallback(resp, elem)
      )
    );
  }

  cancel() {
    this.formHelper('root.home');
  }

  deleteCallback(resp, elem) {
    elem = null;
    this.$state.reload();
  }

  removeBigImage(elem) {
    elem.big = null;
  }
  removeSmallImage(elem) {
    elem.small = null;
  }

  publish(elem) {
    this.confirmerDlg('CONFIRM_PUBLISH', () =>
      this.endpoint({
        url: 'homepage/publish/' + elem.id
      })
    );
  }

  publishAll() {
    this.confirmerDlg('CONFIRM_PUBLISH_ALL', () =>
      this.endpoint({ url: 'homepage/publish/all', data: { context: this.context } })
    );
  }

  maxPosition(elem) {
    if (elem.type === 'slider') {
      return this.slider.length;
    } else {
      return this.topic.length;
    }
  }
}
