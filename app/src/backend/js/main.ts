import * as angular from 'angular';
import '../translation/js/main';
import '../shop/js/shopBackendMain';
import '../table/js/tableBackendMain';
import '../accounting/js/accountingMain';
import '../newsletter/js/main';
import '../cmsBackend/js/cmsBackendMain';
import '../accounting/dunning/js/main';
import '../accounting/transactions/js/main';
import '../homePageAdmin/js/homePageAdminMain';
import 'angular-ui-grid/ui-grid';
import 'ui-grid-draggable-rows';

angular
  .module('backend', [
    'translation',
    'shopBackend',
    'tableBackend',
    'accounting',
    'newsletter',
    'cmsBackend',
    'paymentTransactions',
    'dunningManagement',
    'homePageAdmin',
    'ui.grid',
    'ui.grid.selection',
    'ui.grid.pagination'
  ])
  .controller('backend.mainCtrl', () => {});
