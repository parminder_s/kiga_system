define [
  'angular'
  '../app'
], (angular, app) ->

  mainCtrl = ($scope) ->

  app.controller 'backend.mainCtrl', ['$scope', mainCtrl]