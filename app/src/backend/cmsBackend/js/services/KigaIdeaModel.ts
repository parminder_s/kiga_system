/**
 * Created by bbs on 4/1/17.
 */

export class KigaIdeaModel {
  constructor(private endpoint) {}

  getCategories(locale) {
    return this.endpoint('idea-category/list/' + locale);
  }

  save(formData) {
    let url = 'idea/downloadable/add';
    if (formData.id != null) {
      url = 'idea/downloadable/update';
    }
    return this.endpoint({ url: url, data: formData });
  }

  getIdea(ideaId: number) {
    return this.endpoint('idea/downloadable/get/' + ideaId);
  }

  getAll(locale) {
    return this.endpoint('idea/downloadable/list/' + locale);
  }

  publish(idsToPublish: Array<any>) {
    return this.endpoint({ url: 'idea/downloadable/publish', data: { ids: idsToPublish } });
  }

  unpublish(idsToUnpublish: Array<any>) {
    return this.endpoint({ url: 'idea/downloadable/unpublish', data: { ids: idsToUnpublish } });
  }

  remove(idsToRemove: Array<any>) {
    return this.endpoint({ url: 'idea/downloadable/remove', data: { ids: idsToRemove } });
  }

  isValidUrlSegment(id: number, categoryId: number, urlSegment: string) {
    return this.endpoint({
      url: 'idea/downloadable/validate/',
      data: {
        id: id,
        categoryId: categoryId,
        urlSegment: urlSegment
      }
    });
  }

  getUnassignedIdeaGroups(ideaId: number, locale) {
    return this.endpoint({
      url: 'idea/groups/unassigned/',
      data: { id: ideaId, locale: locale }
    });
  }

  getAssignedIdeaGroups(ideaId: number) {
    return this.endpoint({
      url: 'idea/groups/assigned',
      data: { id: ideaId }
    });
  }

  getTranslatedAssignedIdeaGroups(ideaId: number, locale: string) {
    return this.endpoint({
      url: 'idea/groups/assigned/translated',
      data: { id: ideaId, locale: locale }
    });
  }

  getTranslationGroup(translationGroupId: number) {
    return this.endpoint({
      url: 'idea/translation-group/by/translation-group',
      data: { id: translationGroupId }
    });
  }

  getTranslationGroupByIdeaId(ideaId: number) {
    return this.endpoint({
      url: 'idea/translation-group/by/idea',
      data: { id: ideaId }
    });
  }

  prepareIdeaForTranslation(ideaId: number, locale: string) {
    return this.endpoint({
      url: 'idea/translation-group/prepare-idea',
      data: { id: ideaId, locale: locale }
    });
  }
}
