/**
 * Created by asv 24.11.2016.
 */

export class SliderFilesModel {
  private endpoint;

  constructor(endpoint) {
    this.endpoint = endpoint;
  }

  getSliderFiles(productId) {
    return this.endpoint({ url: 'cms/backend/product/slider', data: { productId: productId } });
  }

  getProductPageUrl(productId) {
    return this.endpoint({
      url: 'cms/backend/product/getProductUrl',
      data: { productId: productId }
    });
  }
}
