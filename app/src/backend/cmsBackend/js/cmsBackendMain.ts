import * as angular from 'angular';
import * as _misc from 'misc';
import { SliderItemsCtrl } from './controllers/sliderItemsCtrl';
import { SliderFilesModel } from './services/SliderFilesModel';
import { StateParams } from 'angular-ui-router';
import { KigaIdeaModel } from './services/KigaIdeaModel';
import { ModifyKigaIdeaCtrl } from './controllers/ModifyKigaIdeaCtrl';
import { ListKigaIdeaCtrl } from './controllers/ListKigaIdeaCtrl';
import { UploadCtrl } from '../../../memberIdeas/js/controllers/UploadCtrl';

let misc = _misc;

export let app = angular.module('cmsBackend', ['misc']);
app
  .service('sliderFilesModel', function(endpoint) {
    return new SliderFilesModel(endpoint);
  })
  .controller('sliderItemsCtrl', function(endpoint, slider, productData, $scope, $state) {
    return new SliderItemsCtrl(endpoint, slider, productData, $scope, $state);
  })
  .controller('modifyKigaIdeaCtrl', function(
    formData,
    kigaIdeaModel: KigaIdeaModel,
    infoDlg,
    $state,
    $scope,
    translationGroup
  ) {
    return new ModifyKigaIdeaCtrl(
      formData,
      kigaIdeaModel,
      infoDlg,
      $state,
      $scope,
      translationGroup
    );
  })
  .controller('listKigaIdeaCtrl', function(
    kigaIdeaModel: KigaIdeaModel,
    kigaIdeas,
    $scope,
    $stateParams: StateParams
  ) {
    return new ListKigaIdeaCtrl(kigaIdeaModel, kigaIdeas, $scope, $stateParams);
  })
  .controller('uploadCtrl', function(
    $state,
    uploadModel,
    categories,
    idea,
    memberIdeaCategoryService
  ) {
    return new UploadCtrl($state, uploadModel, categories, idea, memberIdeaCategoryService);
  })
  .service('kigaIdeaModel', function(endpoint) {
    return new KigaIdeaModel(endpoint);
  })
  .config(function(ngFabFormProvider) {
    let customScrollToFn = function(targetElement, duration, scrollOffset) {
      let closestTabContent = targetElement.closest('.tab-content');
      if (closestTabContent !== null) {
        let tabPanes = closestTabContent.getElementsByClassName('tab-pane');
        for (let i = 0; i < tabPanes.length; ++i) {
          let newClasses = tabPanes[i]
            .getAttribute('class')
            .replace('active', '')
            .trim();
          tabPanes[i].setAttribute('class', newClasses);
        }

        let invalidPanel = targetElement.closest('.tab-pane');
        let activeTabPaneNewClasses = invalidPanel.getAttribute('class') + ' active';
        invalidPanel.setAttribute('class', activeTabPaneNewClasses);

        let invalidPaneId = invalidPanel.getAttribute('id');
        let tabsContainers = targetElement.closest('form').getElementsByClassName('nav-tabs');
        for (let i = 0; i < tabsContainers.length; ++i) {
          let tabs = tabsContainers[i].children;
          for (let j = 0; j < tabs.length; ++j) {
            let attribute = tabs[j].getAttribute('class');
            if (attribute != null) {
              tabs[j].setAttribute('class', attribute.replace('active', '').trim());
            }
            let child = tabs[j].children[0];
            if (child.getAttribute('href') === '#' + invalidPaneId) {
              child
                .closest('li')
                .setAttribute('class', child.closest('li').getAttribute('class') + ' active');
            }
          }
        }
      }

      targetElement.scrollIntoView(true);
      targetElement.focus();
    };
    ngFabFormProvider.setScrollToFn(customScrollToFn);
  });
