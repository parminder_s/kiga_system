import { KigaIdeaModel } from '../services/KigaIdeaModel';
import { StateParams } from 'angular-ui-router';
/**
 * Created by bbs on 4/6/17.
 */

export class ListKigaIdeaCtrl {
  private isPublishedSelected = false;
  private publishSelectedBtnEnabled = false;
  private unpublishSelectedBtnEnabled = false;
  private rowsSelected: Array<any> = [];
  private gridApi: any;
  private columnsDefinition: any = [
    { name: 'Action', cellTemplate: `backend/cmsBackend/partials/kiga-idea/actionCell.html` },
    { name: 'ID', field: 'id' },
    { name: 'State', cellTemplate: `backend/cmsBackend/partials/kiga-idea/state.html` },
    { name: 'Title', field: 'title' },
    { name: 'Category', field: 'parentCategoryName' },
    { name: 'Subcategory', field: 'categoryName' }
  ];
  private gridConfiguration: any = {};
  private currentUserLocale;

  constructor(
    private kigaIdeaModel: KigaIdeaModel,
    private kigaIdeas: Array<any>,
    $scope,
    private $stateParams: StateParams
  ) {
    this.currentUserLocale = $stateParams['locale'];
    this.gridConfiguration = angular.extend(
      {},
      {
        onRegisterApi: gridApi => {
          this.gridApi = gridApi;
          this.gridApi.selection.on.rowSelectionChanged($scope, () =>
            this.onRowsSelectionChanged()
          );
          this.gridApi.selection.on.rowSelectionChangedBatch($scope, () =>
            this.onRowsSelectionChanged()
          );
        },
        data: this.kigaIdeas
      },
      { columnDefs: this.columnsDefinition }
    );
  }

  onRowsSelectionChanged() {
    this.rowsSelected = [];
    this.isPublishedSelected = false;
    this.publishSelectedBtnEnabled = false;
    this.unpublishSelectedBtnEnabled = false;

    _.each(this.gridApi.selection.getSelectedRows(), row => {
      this.rowsSelected.push(row);
      this.setupButtonsState(row);
    });
  }

  private setupButtonsState(row: any) {
    if (row.published == null) {
      this.publishSelectedBtnEnabled = true;
      this.unpublishSelectedBtnEnabled = true;
    }
    if (row.published === true) {
      this.unpublishSelectedBtnEnabled = true;
      this.isPublishedSelected = true;
    }
    if (row.published === false) {
      this.publishSelectedBtnEnabled = true;
    }
  }

  removeSelected() {
    let idsToRemove = _.map(this.gridApi.selection.getSelectedRows(), (row: any) => {
      return row.id;
    });
    this.kigaIdeaModel.remove(idsToRemove).then(() => {
      this.rowsSelected = [];
      this.publishSelectedBtnEnabled = false;
      this.unpublishSelectedBtnEnabled = false;
      this.isPublishedSelected = false;
      this.kigaIdeaModel.getAll(this.$stateParams['locale']).then(data => {
        this.kigaIdeas = data;
        this.gridConfiguration.data = this.kigaIdeas;
      });
    });
  }

  publishSelected() {
    let idsToPublish = _.map(this.gridApi.selection.getSelectedRows(), (row: any) => {
      return row.id;
    });
    this.kigaIdeaModel.publish(idsToPublish).then(() => {
      _.forEach(this.gridApi.selection.getSelectedRows(), (row: any) => {
        row.published = true;
        this.publishSelectedBtnEnabled = false;
        this.unpublishSelectedBtnEnabled = false;
        this.isPublishedSelected = false;
        _.each(this.gridApi.selection.getSelectedRows(), row => {
          this.setupButtonsState(row);
        });
      });
    });
  }

  unpublishSelected() {
    let idsToUnpublish = _.map(this.gridApi.selection.getSelectedRows(), (row: any) => {
      return row.id;
    });
    this.kigaIdeaModel.unpublish(idsToUnpublish).then(() => {
      _.forEach(this.gridApi.selection.getSelectedRows(), (row: any) => {
        row.published = false;
        this.publishSelectedBtnEnabled = false;
        this.unpublishSelectedBtnEnabled = false;
        this.isPublishedSelected = false;
        _.each(this.gridApi.selection.getSelectedRows(), row => {
          this.setupButtonsState(row);
        });
      });
    });
  }
}
