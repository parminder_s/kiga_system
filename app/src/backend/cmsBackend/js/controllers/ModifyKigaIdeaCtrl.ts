import { KigaIdeaModel } from '../services/KigaIdeaModel';
/**
 * Created by bbs on 4/1/17.
 */

export class ModifyKigaIdeaCtrl {
  private urlSegment;
  private ageGroupsList = {
    1: { label: '1 - 3', checked: false },
    2: { label: '3 - 5', checked: false },
    4: { label: '5 - 6', checked: false },
    8: { label: '6 - 7', checked: false },
    16: { label: '7 - 8', checked: false }
  };
  private activeTab = 'general-info';
  private subcategories = [];
  private previewFilesIds: Array<number> = null;
  private uploadedFilesIds: Array<number> = null;
  private uploadedFiles: Array<any> = null;
  private previewFiles: Array<any> = null;
  private lastSelectedCategoryId = null;
  private urlSegmentLastValidationMessage = null;
  private controllerInitialized = false;
  private assignedIdeaGroupsGridApi: any;
  private unassignedIdeaGroupsGridApi: any;

  private assignedIdeaGroupsGridColumns = [
    {
      name: 'Action',
      field: 'id',
      cellTemplate: `backend/cmsBackend/partials/kiga-idea/custom-grid-cells/selected-idea-groups-action.html`
    },
    {
      name: 'Path',
      field: 'fullPathTitles',
      cellTemplate: `backend/cmsBackend/partials/kiga-idea/custom-grid-cells/idea-group-path.html`
    }
  ];

  private unassignedIdeaGroupsGridColumns = [
    {
      name: 'Action',
      field: 'id',
      enableFiltering: false,
      cellTemplate: `backend/cmsBackend/partials/kiga-idea/custom-grid-cells/other-idea-groups-action.html`
    },
    {
      name: 'Path',
      field: 'fullPathTitles',
      cellTemplate: `backend/cmsBackend/partials/kiga-idea/custom-grid-cells/idea-group-path.html`
    }
  ];

  private selectedIdeaGroupsGrid: any = {
    columnDefs: [],
    data: []
  };

  private otherIdeaGroupsGrid: any = {
    data: [],
    columnDefs: []
  };

  private localesList = [
    { locale: 'de', label: 'MISC_LOCALE_DE' },
    { locale: 'en', label: 'MISC_LOCALE_EN' },
    { locale: 'it', label: 'MISC_LOCALE_IT' },
    { locale: 'tr', label: 'MISC_LOCALE_TR' }
  ];

  private selectedIdeaLocale;
  private existingIdeasInTranslationGroup: Array<any> = [];
  private categories: Array<any>;
  private assignedIdeaGroups = [];

  private modelIdeaId;

  constructor(
    private formData: any,
    private kigaIdeaModel: KigaIdeaModel,
    private infoDlg,
    private $state,
    private $scope,
    private translationGroup
  ) {
    this.existingIdeasInTranslationGroup = _.map(
      this.translationGroup.ideas,
      (idea: any) => idea.locale
    );

    let ideaLocale = $state.params['selectedLocale'] || $state.params['locale'];

    this.modelIdeaId = $state.params['ideaId'];

    if (this.formData != null && this.formData.id != null) {
      ideaLocale = this.formData.locale;
      this.modelIdeaId = this.formData.id;
    }

    this.selectedIdeaGroupsGrid = angular.extend(
      {},
      {
        rowTemplate:
          "<div grid='grid' class='ui-grid-draggable-row' draggable='true'>\
          <div ng-repeat='(colRenderIndex, col) in colContainer.renderedColumns track by \
          col.colDef.name' class='ui-grid-cell' ng-class='{ \"ui-grid-row-header-cell\": \
          col.isRowHeader, \"custom\": true }' ui-grid-cell></div></div>",
        columnDefs: this.assignedIdeaGroupsGridColumns,
        appScopeProvider: this,
        data: this.assignedIdeaGroups,
        onRegisterApi: gridApi => {
          this.assignedIdeaGroupsGridApi = gridApi;
          gridApi.draggableRows.on.rowDropped($scope, function(info, rowElement) {
            $scope.ctrl.slider.forEach(function(row, index) {
              row.newSort = index + 1;
            });
          });
        }
      }
    );

    if (this.formData != null && this.formData.id != null) {
      kigaIdeaModel
        .getAssignedIdeaGroups(this.modelIdeaId)
        .then(
          assignedIdeaGroups =>
            (this.selectedIdeaGroupsGrid.data = this.appendPathAndTitles(assignedIdeaGroups))
        );
    } else {
      kigaIdeaModel
        .getTranslatedAssignedIdeaGroups(this.modelIdeaId, ideaLocale)
        .then(
          assignedIdeaGroups =>
            (this.selectedIdeaGroupsGrid.data = this.appendPathAndTitles(assignedIdeaGroups))
        );
    }
    this.otherIdeaGroupsGrid = {
      columnDefs: this.unassignedIdeaGroupsGridColumns,
      enableFiltering: true,
      data: null,
      appScopeProvider: this,
      onRegisterApi: gridApi => (this.unassignedIdeaGroupsGridApi = gridApi)
    };

    kigaIdeaModel
      .getUnassignedIdeaGroups(this.modelIdeaId, ideaLocale)
      .then(
        unassignedIdeaGroups =>
          (this.otherIdeaGroupsGrid.data = this.appendPathAndTitles(unassignedIdeaGroups))
      );

    if (this.formData == null) {
      this.formData = {};
    }

    if (this.formData.previewFileId) {
      this.previewFilesIds = [this.formData.previewFileId];

      this.previewFiles = [
        {
          url: this.formData.largePreviewImageUrl,
          name: this.formData.previewName,
          size: this.formData.previewSize
        }
      ];
    }

    if (this.formData.uploadedFileId) {
      this.uploadedFilesIds = [this.formData.uploadedFileId];

      this.uploadedFiles = [
        {
          url: this.formData.filePreviewUrl,
          name: this.formData.fileName,
          size: this.formData.fileSize
        }
      ];
    }

    if (this.formData.id == null) {
      this.formData.sortingDate = new Date(this.formData.sortYear, this.formData.sortMonth);
      this.formData.sortingDateMini = new Date(
        this.formData.sortYearMini,
        this.formData.sortMonthMini,
        this.formData.sortDayMini
      );

      _.forEach(this.formData.separatedAgeGroups, element => {
        if (this.ageGroupsList[element] != null) {
          this.ageGroupsList[element].checked = true;
        }
      });

      this.formData.translationGroupId = this.translationGroup.translationGroupId;

      this.formData.locale = ideaLocale;
      this.controllerInitialized = true;
      this.selectedIdeaLocale = this.convertCodeToLocale(ideaLocale);

      this.lastSelectedCategoryId = this.formData.categoryId;
      kigaIdeaModel.getCategories(ideaLocale).then(categories => {
        this.categories = _.map(categories, (element: any) => {
          let category = {
            id: element.subCategoryId,
            title: element.categoryTitle + ' / ' + element.subCategoryTitle
          };

          if (category.id === this.lastSelectedCategoryId) {
            this.formData.category = category;
          }

          return category;
        });
      });
    } else {
      this.selectedIdeaLocale = this.convertCodeToLocale(this.formData.locale);

      this.formData.sortingDateMini = new Date(
        this.formData.sortYearMini,
        this.formData.sortMonthMini,
        this.formData.sortDayMini
      );

      this.formData.sortingDate = new Date(this.formData.sortYear, this.formData.sortMonth);

      _.forEach(this.formData.separatedAgeGroups, element => {
        if (this.ageGroupsList[element] != null) {
          this.ageGroupsList[element].checked = true;
        }
      });

      this.lastSelectedCategoryId = this.formData.categoryId;

      this.urlSegment = this.formData.urlSegment;

      kigaIdeaModel.getCategories(this.selectedIdeaLocale.locale).then(categories => {
        this.categories = _.map(categories, (element: any) => {
          let category = {
            id: element.subCategoryId,
            title: element.categoryTitle + ' / ' + element.subCategoryTitle
          };

          if (category.id === this.lastSelectedCategoryId) {
            this.formData.category = category;
          }

          return category;
        });
      });

      if (!this.formData.inPool) {
        this.formData.inPool = false;
      }
    }
  }

  onIdeaGroupsTabClick() {
    setTimeout(function() {
      angular.element(window).resize();
    }, 300);
  }

  private appendPathAndTitles(feedData) {
    _.forEach(feedData, function(row) {
      row.fullPath = _.reverse(_.concat([], [{ id: row.id, title: row.title }], row.parents));
      row.fullPathTitles = _.join(_.map(row.fullPath, (item: any) => item.title), ' / ');
    });

    return feedData;
  }

  addGroupToIdea($row: any) {
    let i = $row.grid.options.data.indexOf($row.entity);
    this.assignedIdeaGroupsGridApi.grid.options.data.push($row.entity);
    $row.grid.options.data.splice(i, 1);
  }

  removeGroupFromIdea($row) {
    let i = $row.grid.options.data.indexOf($row.entity);
    this.unassignedIdeaGroupsGridApi.grid.options.data.push($row.entity);
    $row.grid.options.data.splice(i, 1);
  }

  onCategorySelection($item) {
    if (
      this.formData.category != null &&
      this.formData.category.id != null &&
      this.formData.category.id > 0
    ) {
      this.lastSelectedCategoryId = this.formData.category.id;
    }
  }

  save() {
    if (this.formData.sortingDate != null) {
      this.formData.sortMonth = this.formData.sortingDate.getMonth() + 1;
      this.formData.sortYear = this.formData.sortingDate.getFullYear();
    }

    if (this.formData.sortingDateMini != null) {
      this.formData.sortDayMini = this.formData.sortingDateMini.getDate();
      this.formData.sortMonthMini = this.formData.sortingDateMini.getMonth() + 1;
      this.formData.sortYearMini = this.formData.sortingDateMini.getFullYear();
    }

    if (this.previewFilesIds && this.previewFilesIds.length === 1) {
      this.formData.previewFileId = this.previewFilesIds[0];
    }

    if (this.uploadedFilesIds && this.uploadedFilesIds.length === 1) {
      this.formData.uploadedFileId = this.uploadedFilesIds[0];
    }

    if (this.formData.category != null) {
      this.formData.categoryId = this.formData.category.id;
    }

    this.formData.ageGroup = 0;
    _.forEach(this.ageGroupsList, (ageGroupProperties, ageGroupValue) => {
      if (ageGroupProperties.checked) {
        this.formData.ageGroup += parseInt(ageGroupValue);
      }
    });

    this.formData.ideaGroups = this.assignedIdeaGroupsGridApi.grid.options.data;

    this.kigaIdeaModel
      .save(this.formData)
      .then(data => this.$state.go('root.backend.cmsBackend.kiga-idea.edit', { ideaId: data.id }));
  }

  validateUrlSegment(attrs) {
    if (this.controllerInitialized) {
      let value = this.formData.urlSegment;
      let categoryId = this.formData.categoryId;
      if (this.formData.category != null) {
        categoryId = this.formData.category.id;
      }
      return this.kigaIdeaModel
        .isValidUrlSegment(this.formData.id, categoryId, value)
        .then(data => {
          attrs.urlSegmentValidationMessage = data.message;
          this.urlSegmentLastValidationMessage = data.message;
          return data.valid;
        });
    } else {
      // workaround to disable asyncValidateFn before controller gets fully initialized
      return new Promise((resolve, reject) => resolve(null)).then(() => {
        return true;
      });
    }
  }

  asyncValidateFn(value, attrs) {
    let ctrl = (<any>this).$parent.ctrl;
    ctrl.formData.urlSegment = value;
    return ctrl.validateUrlSegment(attrs);
  }

  publish() {
    this.kigaIdeaModel.publish([this.formData.id]).then(() => (this.formData.published = true));
  }

  unpublish() {
    this.kigaIdeaModel.unpublish([this.formData.id]).then(() => (this.formData.published = false));
  }

  remove() {
    this.kigaIdeaModel
      .remove([this.formData.id])
      .then(() => this.$state.go('root.backend.cmsBackend.kiga-idea.list'));
  }

  onTitleInputLoosesFocus() {
    if (this.formData.title && !this.urlSegment) {
      this.urlSegment = this.formData.title
        .toLowerCase()
        .replace(/[^A-Za-z0-9]+/g, '-')
        .replace(/\-+/g, '-')
        .replace(/^-*([^-].*[^-])-*$/g, '$1');
    }
  }

  convertCodeToLocale(localeString) {
    return { locale: localeString, label: 'MISC_LOCALE_' + _.toUpper(localeString) };
  }

  hasTranslation(locale) {
    return this.existingIdeasInTranslationGroup.indexOf(locale) > -1;
  }

  onLocaleChange(locale) {
    if (this.hasTranslation(locale.locale)) {
      let idea = this.translationGroup.ideas[
        this.existingIdeasInTranslationGroup.indexOf(locale.locale)
      ];
      return this.$state.go('root.backend.cmsBackend.kiga-idea.edit', {
        ideaId: idea.id
      });
    } else {
      return this.$state.go('root.backend.cmsBackend.kiga-idea.add-by-idea', {
        selectedLocale: locale.locale,
        ideaId: this.modelIdeaId,
        sort: this.formData.sortingDate,
        sortMini: this.formData.sortingDateMini,
        ageGroups: this.ageGroupsList,
        facebookShare: this.formData.facebookShare,
        forParents: this.formData.forParents,
        inPool: this.formData.inPool
      });
    }
  }
}
