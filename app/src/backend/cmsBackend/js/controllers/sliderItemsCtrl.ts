import * as angular from 'angular';
import url from '../../../../url';

export class SliderItemsCtrl {
  endpoint = null;
  slider = null;
  productData = null;
  gridOptions = null;
  $scope = null;
  $state = null;
  previewOptions = null;
  $parent = null;
  new = null;
  newVideo = null;
  uploadedFiles = null;

  constructor(endpoint, slider, productData, $scope, $state) {
    this.endpoint = endpoint;
    this.productData = productData;
    this.slider = slider;
    this.$scope = $scope;
    this.$state = $state;
    this.new = {};
    this.uploadedFiles = [];
    this.newVideo = {};
    this.slider.forEach(function(row, index) {
      row.newSort = index + 1;
    });
    this.gridOptions = {
      rowTemplate:
        "<div grid='grid' class='ui-grid-draggable-row' draggable='true'>\
      <div ng-repeat='(colRenderIndex, col) in colContainer.renderedColumns track by \
      col.colDef.name' class='ui-grid-cell' ng-class='{ \"ui-grid-row-header-cell\": \
      col.isRowHeader, \"custom\": true }' ui-grid-cell></div></div>",
      enableSorting: false,
      enableHorizontalScrollbar: 0,
      enableVerticalScrollbar: 0,
      rowHeight: 100,
      columnDefs: [
        { name: 'newSort', width: '15%', displayName: 'New Sort Order' },
        {
          name: 'imageUrl',
          visible: true,
          displayName: 'Image',
          cellClass: 'img-cell',
          cellTemplate: '<img width="120px" ng-src="{{grid.getCellValue(row, col)}}" lazy-src>'
        },
        { name: 'fileType' },
        { name: 'sort', displayName: 'Sort Order', width: '15%' },
        {
          name: 'id',
          displayName: 'Action',
          width: '10%',
          cellTemplate:
            "<i class='icon icon-close delete-btn' \
          ng-click='grid.appScope.ctrl.removeFile(grid.getCellValue(row, col))'></i>"
        }
      ],
      data: this.slider
    };
    this.gridOptions.onRegisterApi = function(gridApi) {
      gridApi.draggableRows.on.rowDropped($scope, function(info, rowElement) {
        $scope.ctrl.slider.forEach(function(row, index) {
          row.newSort = index + 1;
        });
      });
    };
    this.previewOptions = {
      previewMaxWidth: 200,
      previewMaxHeight: 200,
      previewCrop: true,
      maxFileSize: 10000000,
      acceptFileTypes: /(\.|\/)(gif|jpe?g|png|mp4)$/i
    };
  }

  submit() {
    this.slider.forEach(function(row, index) {
      row.sort = row.newSort;
    });
    this.endpoint({
      url: 'cms/backend/product/save-slider',
      data: {
        sliderFiles: this.slider,
        productId: this.$scope.$resolve.$stateParams.productPageId
      }
    }).then(data => {
      console.log('saved!');
    });
  }

  addImage(files) {
    this.$parent.ctrl
      .endpoint({
        url: 'cms/backend/product/uploadImage',
        files: { file: files[0] }
      })
      .then(data => {
        if (data.className === 'KigaS3File') {
          this.$parent.ctrl.newVideo = data;
        } else {
          this.$parent.ctrl.new.big = data;
        }
      });
  }
  cancel() {
    this.new = {};
    this.newVideo = {};
  }
  addFile() {
    this.endpoint({
      url: 'cms/backend/product/addFile',
      data: {
        s3FileIds: this.uploadedFiles,
        productId: this.$scope.$resolve.$stateParams.productPageId
      }
    }).then(data => {
      this.$state.reload();
    });
  }

  removeFile(sliderFileId) {
    this.endpoint({
      url: 'cms/backend/product/deleteSliderFile',
      data: { sliderFileId: sliderFileId }
    }).then(data => {
      this.$state.reload();
    });
  }
}
