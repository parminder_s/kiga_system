interface PurchaseItem {
  id: number;
  title: string;
  amount: number;
  priceGrossTotal: number;
  creditVoucherTotal: number;
}

export default PurchaseItem;
