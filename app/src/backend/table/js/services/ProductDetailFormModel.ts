/**
 * Created by bbs on 10/11/16.
 */

export class ProductDetailFormModel {
  private endpoint;

  constructor(endpoint) {
    this.endpoint = endpoint;
  }

  getProductDetail(productDetailId: number) {
    return this.endpoint({
      url: 'shop/backend/products/details/get',
      data: { productDetailId: productDetailId }
    });
  }

  save(productDetail) {
    return this.endpoint({ url: 'shop/backend/products/details/save', data: productDetail });
  }

  deactivateById(id: number) {
    return this.endpoint({ url: 'shop/backend/products/details/deactivate', data: { id: id } });
  }

  activateById(id: number) {
    return this.endpoint({ url: 'shop/backend/products/details/activate', data: { id: id } });
  }
}
