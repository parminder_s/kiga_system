/**
 * Created by bbs on 10/16/16.
 */

import * as _ from 'lodash';

export class CountryModel {
  private endpoint;

  constructor(endpoint) {
    this.endpoint = endpoint;
  }

  getCountries() {
    return this.endpoint({ url: 'shop/backend/countries' }).then(countries =>
      _.orderBy(countries, 'title')
    );
  }

  getCountry(countryId: any) {
    return this.endpoint({ url: 'shop/backend/countries/get', data: { id: countryId } });
  }

  save(country: any) {
    return this.endpoint({ url: 'shop/backend/countries/update', data: country });
  }

  getOtherCountries() {
    return this.endpoint({ url: 'shop/backend/countries/other' }).then(countries =>
      _.orderBy(countries, 'title')
    );
  }

  getImportantCountries() {
    return this.endpoint({ url: 'shop/backend/countries/important' }).then(countries =>
      _.orderBy(countries, 'title')
    );
  }
}
