import Purchase from '../spec/Purchase';
import PurchaseViewModel from '../spec/PurchaseViewModel';

export default class PurchaseViewModelMapper {
  static map(purchase: Purchase): PurchaseViewModel {
    let returner: PurchaseViewModel = purchase as PurchaseViewModel;
    returner.showAccounting = purchase.paymentMethod === 'INVOICE' && !!purchase.invoiceNumber;
    returner.showPspId = !returner.showAccounting;
    return returner;
  }
}
