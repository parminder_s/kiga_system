export let mainGridOptions = {
  enableRowSelect: true,
  enableSelection: true,
  enableSelectAll: true,
  multiSelect: true,
  enableSorting: true,
  enableFiltering: true,

  paginationPageSizes: [10, 25, 50, 75],
  paginationPageSize: 12
};
