export let columnDefs = function(url: (url: string) => string, uiGridConstants) {
  return [
    { name: ' ', cellTemplate: url('cellDetail'), width: 30, enableFiltering: false },
    { name: '#.', field: 'amount', width: 35, enableFiltering: false },
    {
      name: 'Order',
      field: 'orderNr',
      sort: {
        direction: uiGridConstants.DESC,
        priority: 1
      },
      width: 50
    },
    { name: 'Date', field: 'purchaseDate', cellFilter: 'date:"dd.MM.yyyy"', width: 80 },
    { name: 'Member', field: 'memberId', width: 75 },
    { name: 'email', field: 'memberEmail', width: 160 },
    { name: 'Customer', field: 'name', width: 160 },
    { name: 'Country', field: 'country', width: 70, enableFiltering: true },
    {
      name: 'Invoice',
      field: 'invoiceNumber',
      cellTemplate: url('cellInvoice'),
      enableFiltering: true,
      width: 100
    },
    {
      name: 'Shipping',
      cellTemplate: url('cellShippingNote'),
      enableFiltering: true,
      width: 100,
      filter: {
        condition: (searchTerm: string, cellValue, row, column) => {
          let entity = row.entity;
          let shippingPartner = entity.shippingPartner || '';
          let shippingNoteNumber = entity.shippingNoteNumber || '';
          let regex = new RegExp(searchTerm, 'i');
          return shippingPartner.match(regex) || shippingNoteNumber.match(regex);
        }
      }
    },
    {
      name: 'Payment',
      cellTemplate: url('cellPayment'),
      enableFiltering: true,
      width: 100,
      filter: {
        condition: (searchTerm: string, cellValue, row) => {
          let entity = row.entity;
          let paymentMethod = entity.paymentMethod || '';
          let paymentPspId = entity.paymentPspId || '';
          let regex = new RegExp(searchTerm, 'i');
          return paymentMethod.match(regex) || paymentPspId.match(regex);
        }
      }
    },
    {
      name: 'Status',
      cellTemplate: url('cellStatus'),
      field: 'status',
      enableFiltering: true,
      width: 160
    }
  ];
};
