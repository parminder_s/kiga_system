/**
 * Created by bbs on 3/14/17.
 */

export class CountriesCtrl {
  private columnsDefinition = [
    { name: 'Action', cellTemplate: `backend/shop/partials/countries/cellAction.html` },
    { name: 'Code', field: 'code', sort: { direction: 'desc', priority: 1 } },
    { name: 'Code3', field: 'code3' },
    { name: 'Currency', field: 'currency' },
    { name: 'Shipping Price', field: 'priceShippingNet', sort: { direction: 'desc', priority: 2 } },
    { name: 'VAT %', field: 'vat', sort: { direction: 'desc', priority: 0 } },

    { name: 'Account Number', field: 'accountNr' },
    { name: 'Bank name', field: 'bankName' },
    { name: 'BIC', field: 'bic' },
    { name: 'IBAN', field: 'iban' },
    { name: 'Due days', field: 'daysToPayInvoice' },
    { name: 'Max purchase price', field: 'maxPurchasePrice' },
    { name: 'Shipping net price', field: 'priceShippingNet' },
    { name: 'Shipping vat rate', field: 'vatRateShipping' },
    { name: 'Free shipping limit', field: 'freeShippingLimit' }
  ];
  private importantCountriesGrid: any = {};
  private otherCountriesGrid: any = {};

  constructor(importantCountries, otherCountries) {
    this.importantCountriesGrid = angular.extend(
      {},
      { data: importantCountries },
      { columnDefs: this.columnsDefinition }
    );
    this.otherCountriesGrid = angular.extend(
      {},
      { data: otherCountries },
      { columnDefs: this.columnsDefinition }
    );
  }
}
