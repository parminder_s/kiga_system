import ShopBackendModel from '../services/ShopBackendModel';
import PurchaseItem from '../spec/PurchaseItem';
import * as _ from 'lodash';
import IWindowService = angular.IWindowService;
import Purchase from '../spec/Purchase';

export class ShopBackendCreditVoucherCtrl {
  data = null;
  shopBackendModel: ShopBackendModel;
  creditVoucherSum;
  paymentMethod = null;
  selectedPaymentMethod = null;
  $window: IWindowService;
  go;
  formHelper;
  orderNr: number;
  constructor($window, go, purchase, shopBackendModel, formHelper, isDate) {
    this.data = purchase;
    if (isDate(this.data.invoiceDate)) {
      this.data.invoiceDate = null;
    }
    this.shopBackendModel = shopBackendModel;
    this.creditVoucherSum = 0;
    this.$window = $window;
    this.formHelper = formHelper;
    this.go = go;
    this.orderNr = null;
    this.paymentMethod = [
      { code: 'MP-CC', title: 'MPay Kreditkarte' },
      { code: 'MP-PAYPAL', title: 'PayPal' },
      { code: 'SOFORT', title: 'Sofort Überweisung' }
    ];

    this.selectedPaymentMethod = purchase.paymentMethod;
  }

  submit() {
    this.shopBackendModel.createCreditVoucher(this.data);
  }

  recalcSum() {
    this.creditVoucherSum = _.reduce(
      this.data.purchaseItems,
      (sum: number, entry: PurchaseItem) => {
        return sum + parseFloat(entry.creditVoucherTotal.toString());
      },
      0
    );
  }

  search() {
    if (this.data.invoiceNumber) {
      this.shopBackendModel.findByInvoiceNr(this.data.invoiceNumber).then((data: Purchase) => {
        this.go('root.backend.shop.creditVoucher', { purchaseId: data.id });
      });
    } else {
      if (this.data.orderNr) {
        this.shopBackendModel.findByOrderNr(this.data.orderNr).then((data: Purchase) => {
          this.go('root.backend.shop.creditVoucher', { purchaseId: data.id });
        });
      }
    }
  }

  setMax() {
    this.data.purchaseItems.map((item: PurchaseItem) => {
      item.creditVoucherTotal = item.priceGrossTotal;
    });
    this.recalcSum();
  }

  clearInvoiceNr() {
    this.data.invoiceNumber = null;
  }

  clearOrderNr() {
    this.data.orderNr = null;
  }

  cancel() {
    this.formHelper();
  }
}
