import * as _ from 'lodash';
import * as angular from 'angular';
import { columnDefs } from '../mainList/columnDefs';
import { mainGridOptions } from '../mainList/mainGridOptions';
import { Toolbar } from '../mainList/toolbar';
import ShopBackendModel from '../services/ShopBackendModel';
import Purchase from '../spec/Purchase';
import IWindowService = angular.IWindowService;
import ILocationService = angular.ILocationService;

export default class PurchasesController {
  $location: ILocationService;
  $window: IWindowService;
  confirmer;
  data: Array<Purchase>;
  hideFinished = true;
  shopBackendModel: ShopBackendModel;
  gridApi = null;
  gridOptions = null;
  toolbar: Toolbar;
  dataSource = []
  checked = true;
  amount = ''
  orderNr = ''
  purchaseDate = ''
  memberId = ''
  memberEmail = ''
  name = ''
  country = ''
  invoiceNumber = ''
  shippingPartner = ''
  paymentMethod = ''
  status = ''

  constructor($window, purchases, shopBackendModel, uiGridConstants, $location, confirmer) {
    this.$location = $location;
    this.$window = $window;
    this.confirmer = confirmer;
    this.data = purchases;
    this.shopBackendModel = shopBackendModel;
    this.refresh()
  }

  search($event: Event, value) {
    console.log($event)
  }

  onHideFinished() {
    this.$window.setTimeout(() => this.refresh(), 0);
  }

  refresh: any = () => {
    this.shopBackendModel.findFromOrdered(this.hideFinished).then(purchases => {
      this.dataSource = purchases
    });
  };

}
