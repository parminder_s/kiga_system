define [
  'angular'
  './app'
#  './filter/wordByAnyKey'
#  './filter/wordByCategory'
#  './filter/wordListByStatus'
  './services/filter/filter'
  './services/model'
  './services/occurrence'
  './services/locale'
  './services/history'
  './services/comment'
  './services/translationStatus'
  './services/category'
  './controllers/main'
  './controllers/list'
  './controllers/edit'
],(angular, app)->


