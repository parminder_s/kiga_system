define [
  '../app'
  './search/stateTemplate'
], (app) ->
  app.service 'occurrenceService', (stateTemplateService) ->

    search : (searchKey, cb) ->
      result = stateTemplateService.getSearchResult searchKey
      if cb
        cb result
      else result
