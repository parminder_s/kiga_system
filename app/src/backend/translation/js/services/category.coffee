define [
  '../app'
], (app) ->
  app.service 'categoryService', (request) ->
    groupCategory = (categories)->
      _.groupBy categories, (val)->
        if val.parent then val.parent.title else "Main"
    processCategory = (groupedCategory, pickedCat, arr)->
      if arr.length is 0
        pickedCat = _.sortBy groupedCategory['Main'], 'title'
      _.forEach pickedCat, (cat)->
        arr.push cat
        childCat = groupedCategory[cat.title]
        if childCat
          childCat = _.sortBy childCat, 'title'
          processCategory groupedCategory, childCat, arr
      arr
    reformCategory = (categories)->
      groupedCategory = groupCategory categories
      #      categories = processCategory groupedCategory, null, []
      processCategory groupedCategory, null, []

    getAll: (cb) ->
      request
        url: "translation/getCategories"
      .then (categories) ->
        reformCategory categories
#        if cb
#          cb categories
#        else
#          categories
