define [
  '../app'
], (app) ->
  app.service 'localeService', (request) ->
    getAll: (cb) ->
      request
        url: "translation/getLocale"
      .then (locales) ->
        if cb
          cb locales
        else
          locales
