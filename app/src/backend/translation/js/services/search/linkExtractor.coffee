
define [
  '../../app'
  'lodash'
], (app, _) ->
  app.service 'linkExtractorService', -> (html)->
    matches = html.match /ng-include\s*(src\s*)?=\s*("([^"]*")|'[^']*'|([^'">\s]+))/ig
    _.uniq _.map matches, (inc)->
      inc.replace(/ng-include\s*(src\s*)?=\s*/, "").replace /["']/g, ""
