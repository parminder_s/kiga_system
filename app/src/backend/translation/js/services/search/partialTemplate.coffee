
define [
  '../../app'
  'lodash'
  './linkExtractor'
], (app, _) ->
  app.service 'partialTemplateService', ['$q', '$state', '$templateFactory', '$timeout',
  'misc.loader', 'linkExtractorService', 'recursivePromise'].
  concat ($q, $state, $templateFactory, $timeout,
  loader, linkExtractorService, recursivePromise)->

    allTemplates = null
    saveTemplates = (data)->
      includes = linkExtractorService(data)
      if includes.length > 0
        _.map includes, (urlInc)->
          $templateFactory.fromUrl urlInc
      else null

    insert = (url, data)->
      if not allTemplates
        allTemplates = {}
      allTemplates[url] = data

    httpRecurForTemplate = (urlCollection) ->
      _.map urlCollection, (url)->
        aPromise = $templateFactory.fromUrl url
        result = recursivePromise(aPromise, saveTemplates).then (data)-> _.uniq data
        insert url, result
      $q.all _.values allTemplates

    getTemplate : (url)->
      allTemplates[url]

    getAllTemplates : ->
      allTemplates

    getResolvedUrl : ->
      _.keys allTemplates

    loadAll : (cb)->
      states = _.filter $state.get(),
        (state)-> _.isString state.templateUrl
      templateUrls = _.map states, 'templateUrl'
      loader.show()
      httpRecurForTemplate(templateUrls)
      .then ()->
        _.forEach _.keys(allTemplates), (key)->
          allTemplates[key] = allTemplates[key].$$state.value
        cb()
      .finally ->
        loader.hide()
