# get all words
# get all the keys
# loop through the keys and find the occurrences from the $state
# count the occurrences
# build searchResults
# preserve it by key,val


define [
  '../../app'
  'lodash'
  './searchObject'
  './linkExtractor'
  './partialTemplate'
], (app, _) ->
  app.service 'stateTemplateService',
  ($state, $templateFactory, translationModel, partialTemplateService, searchObjectService) ->

    allSearchResult = null
    allKeys = null

    saveAllOccurrence : ()->
      allSearchResult = {}
      aState = _.filter $state.get(), (state)->
        (_.isString state.templateUrl)
      _.forEach allKeys, (wordKey)->
        allSearchResult[wordKey] = searchObjectService.build(wordKey,aState)
      allSearchResult

    loadTranslation : ->
      if not allKeys
        translationModel.list()

    buildData : ()->
      @loadTranslation().then (data)=>
        allKeys = _.keys data
        partialTemplateService.loadAll(@saveAllOccurrence)

    getSearchResult : (wordKey)->
      if not allSearchResult
        @buildData().then ->
          allSearchResult[wordKey]
      else allSearchResult[wordKey]
