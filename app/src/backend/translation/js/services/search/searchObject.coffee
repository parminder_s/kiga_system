
# get all words
# get all the keys
# loop through the keys and find the occurrences from the $state
# count the occurrences
# build searchResults
# preserve it by key,val


define [
  '../../app'
  'lodash'
], (app, _) ->
  app.service 'searchObjectService', (partialTemplateService)->

    create = (wordKey, state, matched)->
      templateUrl = state.templateUrl
      returner =
        name : state.name
        templateUrl : templateUrl
        count: matched.length
      returner

    build : (wordKey, states)->
      searchResult = []
      _.forEach states, (state)=>
        matched = partialTemplateService.getTemplate(state.templateUrl).join("")
        .match(new RegExp(wordKey, 'ig'))
        if matched
          searchResult.push create(wordKey, state, matched)

      totalCount = _.sum(_.map(searchResult, "count"))
      finalSearchObj =
        states : searchResult
        totalCount : totalCount

      finalSearchObj
