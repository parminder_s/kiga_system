define [
  '../app'
], (app) ->
  app.service 'backendHistoryService', (request) ->
    getAll: (key, cb) ->
      request
        url: "translation/getHistory"
        data: key:key
      .then (locales) ->
        if cb
          cb locales
        else
          locales
