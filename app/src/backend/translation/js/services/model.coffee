define [
  '../app'
], (app)->

  app.service 'translationModel', (request, translatedWord) ->

    getNew: ->
      translatedWord.getNew()

    list: (locale, cb) ->
      request
        url: 'translation/findWithMetaData'
        data:
          'locale':locale
          'isTranslationEnabled': true
      .then (words) ->
        if cb
          cb words
        else
          words

    saveEntity: (originalWord, word, cb)->
      request
        url: "translation/update"
        data:
          'word': word
#          'key': originalWord.key
      .then (updatedWord) ->
        if cb
          cb updatedWord
        else
          updatedWord

    replaceWord : (collection, key, word) ->
      delete collection[key]
      collection[word.key] = word

    checkRedundant: (word, currentLocale, cb) ->
      request
        url: "translation/checkRedundant"
        data:
          'word': word
      .then (word) ->
        if cb
          cb word
        else
          word

    findWord : (key, currentLocale, cb) ->
      request
        url: "translation/getWord"
        data:
          'translationKey': key
          'currentLocale': currentLocale
      .then (word) ->
        if cb
          cb word
        else
          word
