define [
  '../app'
], (app) ->
  app.service 'backendCommentService', (request) ->
    getAll: (key, cb) ->
      request
        url: "translation/getComment"
        data: key:key
      .then (locales) ->
        if cb
          cb locales
        else
          locales
