define [
  '../app'
], (app) ->
  app.service 'translationStatusService', (request) ->
    getAll: (cb) ->
      request
        url: "translation/getTranslationStatus"
      .then (statusList) ->
        if cb
          cb statusList
        else
          statusList
