define [
  '../../app'
  'lodash'
  './filterByCategory'
  './filterBySearchKey'
  './filterByStatus'
], (app, _) ->
  app.service 'translationFilter', (filterBySearchKey, filterByCategory, filterByStatus)->
    (words, filterObject) ->
      newWords = filterBySearchKey words, filterObject
      newWords = filterByCategory newWords, filterObject
      newWords = filterByStatus newWords, filterObject
      newWords
