define [
  '../../app'
  'lodash'
], (app, _) ->
  app.service 'filterByCategory', ->
    (words, filterObject) ->
      selectedCategory = filterObject.category
      recurCat = (category, arr)->
        if not (category.parent is null)
          recurCat category.parent, arr
        arr.push category.title
        arr

      if selectedCategory and not (selectedCategory.title is 'All')
        catArr = recurCat selectedCategory, []
        catArrStr = catArr.join("/")

        _.filter words, (word)->
          word.category?.join("/") is catArrStr
      else words
