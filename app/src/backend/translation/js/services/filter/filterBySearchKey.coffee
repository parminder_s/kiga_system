define [
  '../../app'
  'lodash'
], (app, _) ->
  app.service 'filterBySearchKey', ->
    (words, filterObject)->
      searchKey = filterObject.searchKey
      if searchKey
        _.filter words, (word, key)->
          match = _.filter _.values(word), (val)->
            (val?) and (_.includes(val.toString().toLowerCase(), searchKey.toLowerCase()))
          !(_.isLoaded match)
      else words
