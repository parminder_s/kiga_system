define [
  '../../app'
  'lodash'
], (app, _) ->
  app.service 'filterByStatus', ->
    (words, filterObject) ->
      selectedStatus = filterObject.status
      if selectedStatus and not (selectedStatus is 'All')
        _.filter words, (word)->
          not (word.status is null) and word.status.toLowerCase() is selectedStatus.toLowerCase()
      else words
