define [
  '../app'
  'angular'
], (app, angular) ->

  app.controller 'translation.listCtrl',
  ($state, $stateParams, $filter, $translate,
   translationFilter, translationModel, wordList,
   locales, categories, tranStatus, filterObject) ->

    url = (name) -> "backend/translation/partials/template/cell/#{name}.html"

    new class Ctrl
      constructor: () ->
        @wordList = wordList
        @wordListValues = _.values wordList
        @locales = locales
        @translationStatus = tranStatus
        @categories = categories
        @filterObject = filterObject
        @applyFilter()

        @gridOptions =
          data : 'listCtrl.wordListValues'
          appScopeProvider:
            categoryTree:(entity) ->
              if entity.category is null
                ''
              else  entity.category.join(' / ')
            filterObject: @filterObject

          rowTemplate: url 'row'
          columnDefs : [
            {name: 'Locale', width:100, field: 'locale', enableSorting: true, headerCellFilter:"translate" },
            {name: 'Category', field: 'category', cellTemplate: url 'cellForCategory'},
            {name: 'Status', field: 'status', cellTemplate: url 'cellForStatus' },
            {name: 'Translation', field: 'translation' },
            {name: 'Original Translation', field:'originalTranslation', cellTemplate: url 'cellForOriginalTranslation'},
            {name: 'KEY', field:'key'}
          ]
          enableSorting : true
          paginationPageSizes : [10, 25, 50, 75]
          paginationPageSize : 10

      getCssClass: (category) ->
        '' if category is null
        recurCat = (category, count)->
          return '' if category.title is 'All'
          if category.parent is null
            count
          else
            count = count + 1
            recurCat category.parent, count
        "category-level-" + recurCat(category,0)

      resetFilter : =>
        @filterObject.category = null
        @filterObject.status = null
        @filterObject.searchKey = null
        @applyFilter()

      applyFilter : =>
        @wordListValues = _.values translationFilter @wordList, @filterObject

      changeLocale : =>
        translationModel.list @filterObject.locale, (result)=>
          @wordList = result
          @applyFilter()
