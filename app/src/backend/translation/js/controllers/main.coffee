define [
  '../app'
], (app) ->

  app.controller 'translation.mainCtrl', ($state) ->
    if not ($state.is('root.backend.translation.listWord') or $state.is('root.backend.translation.editWord'))
      $state.go '.listWord'
