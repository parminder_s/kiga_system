define [
  '../app'
  'angular'
], (app, angular) ->
  app.controller 'translation.editCtrl',
  ($state, occurrences, translationModel, currentWord, confirmerDlg) ->
    console.log "translation.editCtrl > ", currentWord
    new class Ctrl
      constructor: () ->
        @isEditMode = (currentWord != null) && angular.isDefined(currentWord)
        @editedWord = angular.copy currentWord.word
        @allComments = currentWord.comment
        @allHistories = currentWord.history
        @occurrences = occurrences
        @word = currentWord

      gotoHome : ->
        $state.go 'root.backend.translation.listWord'

      checkRedundant : =>
        console.log "translation.editCtrl > ", currentWord.word.translation , @editedWord.translation
        if currentWord.word.translation is @editedWord.translation and @editedWord.latestComment is null
          @gotoHome()
        else if currentWord.word.translation is @editedWord.translation
          @saveWord()
        else
          translationModel.checkRedundant @editedWord, @editedWord.locale, (matched)=>
            if matched
              confirmerDlg 'Translation already exists! Are you sure to save?', =>
                @saveWord()
            else
              @saveWord()


      saveWord : =>
        translationModel.saveEntity currentWord, @editedWord, (word) =>
          currentWord = word
          @editedWord = word
          @gotoHome()

      resetForm : =>
        @editedWord = if @isEditMode then currentWord else model.getNew()
        @gotoHome()

      getLink : (state)->
        state.name
