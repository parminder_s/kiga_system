###*

  @ngdoc filter
  @name wordListByStatus
  @module backend

  @description
  A custom filter to sort out all the data
  where the given 'status' matches to 'status' property
  value of the objects.

###

define [
  '../app'
  'lodash'], (app, _)->

  app.filter 'wordListByStatus', ->
    (words, selectedStatus) ->
      if selectedStatus and not (selectedStatus is 'All')
        _.filter words, (word)->
          word.status.toLowerCase() is selectedStatus.toLowerCase()
      else words
