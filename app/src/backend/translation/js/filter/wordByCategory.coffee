###*

  @ngdoc filter
  @name wordByCategory
  @module backend
  @description

  A custom filter to sort out all the data where the given 'category' matches to 'category' property value of the objects.

###

define [
  '../app'
  'lodash'], (app, _)->

  app.filter 'wordByCategory', ->
    (words, selectedCategory) ->
      recurCat = (category, arr)->
        if not (category.parent is null)
          recurCat category.parent, arr
        arr.push category.title
        arr

      if selectedCategory and not (selectedCategory.title is 'All')
        catArr = recurCat selectedCategory, []
        catArrStr = catArr.join("/")

        _.filter words, (word)->
          word.category.join("/") is catArrStr
      else words
