###*

  @ngdoc filter
  @name wordByAnyKey
  @module backend
  @description

  A custom filter to sort out all the data where the given 'search key' matches to any property value of the objects.

###

define [
  '../app'
  'lodash'], (app, _)->

  app.filter 'wordByAnyKey', ->
    (words, searchKey) ->

      if searchKey
        _.filter words, (word, key)->
          match = _.filter _.values(word), (val)->
            (val?) and (_.includes(val.toString().toLowerCase(), searchKey.toLowerCase()))
          !(_.isLoaded match)
      else words
