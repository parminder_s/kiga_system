define ["angular", "misc", "angular-sanitize", "angular-scroll"], (angular) ->
  angular
    .module('docs', ['misc', 'ngAnimate', 'ngSanitize', "duScroll"])
