define ['angular', './app'], (angular, app) ->
  app.controller 'docsMenuController', ['$scope', ($scope, $animate) ->
    $scope.dataMenu = [
      {
        'name': 'main'
        'items': [
          'Fashion'
          'Electronics'
          'Furniture'
          'Jewelry_Watches'
        ]
      }
      {
        'name': 'Fashion'
        'items': [
          'Men'
          'Women'
          'Children'
        ]
      }
      {
        'name': 'Electronics'
        'items': [
          'Camera&Photo'
          'TV&Home_Cinema'
          'Phones'
          'PC&Video_Games'
          'Community2'
        ]
      }
      {
        'name': 'Furniture'
        'items': [
          'Living_Room'
          'Bedroom'
          'Home_Offise'
          'Dining&Bar'
          'Patio'
        ]
      }
      {
        'name': 'Jewelry_Watches'
        'items': [
          'Fine_Jewelry'
          'Fasion_Jewelry'
          'Watches'
          'Wedding_Jewelry'
        ]
      }
      {
        'name': 'Living_Room'
        'items': [
          'Sofas&Loveseats'
          'Coffee&Accent_Tables'
          'Chairs&Recliners'
          'Bookshelves'
        ]
      }
      {
        'name': 'Bedroom'
        'items': [
          'Beds'
          'Bedroom_Sets'
          'Chests&Dressers'
        ]
      }
      {
        'name': 'Beds'
        'items': [
          'Upholstered_Beds'
          'Divans'
          'Metal_Beds'
          'Storage_Beds'
          'Wooden_Beds'
          'Children\'s_Beds'
        ]
      }
      {
        'name': 'Wedding_Jewelry'
        'items': [
          'Engagement_Rings'
          'Bridal_Sets'
          'Women\'s_Wedding_Bands'
          'Men\'s_Wedding_Bands'
        ]
      }
      {
        'name': 'Men'
        'items': [
          'Shirts'
          'Jackets'
          'Chinos&Trousers'
          'Jeans'
          'T-Shirts'
          'Underwear'
        ]
      }
      {
        'name': 'Women'
        'items': [
          'Jackets'
          'Knits'
          'Jeans'
          'Dresses'
          'Blouses'
          'T-Shirts'
          'Underwear'
        ]
      }
      {
        'name': 'Children'
        'items': [
          'Boys'
          'Girls'
        ]
      }
    ]

    $scope.menuData = {
        'label': 'Menu'
        'menuItems': [
          {
            'label': 'Fashion'
            'children': [
              {
                'label': 'Men'
                'icon': 'icon-reg-why'
                'children': [
                  {'label':'Shirts'}
                  {'label':'Jackets'}
                  {'label':'Chinos&Trousers'}
                  {'label':'Jeans'}
                  {'label':'T-Shirts'}
                  {'label':'Underwear'}
                ]  
              }
              {
                'label': 'Women'
                'children': [
                  {'label':'Shirts'}
                  {'label':'Jackets'}
                  {'label':'Chinos&Trousers'}
                  {'label':'Jeans'}
                  {'label':'T-Shirts'}
                  {'label':'Underwear'}
                ]
              }
              {'label': 'Children'}
            ]
          }
          {
            'label': 'Electronics'
            'children': [
              {'label': 'Camera&Photo'}
              {'label':'TV&Home_Cinema'}
              {'label':'Phones'}
              {'label':'PC&Video_Games'}
              {'label':'Community2'}
            ]
          }
          {
            'label': 'Furniture'
            'icon': 'icon-reg-why'
          }
          {label: 'Jewelry Watches'}
        ]
      } 

    $scope.showBack = false
    $scope.states = {}
    routMenu = ['', $scope.dataMenu[0].name]
    $scope.isIcon = (item) ->
      i = 0
      while i < $scope.dataMenu.length
        if $scope.dataMenu[i].name == item
          return true
        i++
      false
    $scope.switchMenu = (flag) ->
      $scope.showX = false
      $scope.hideX = false
      $scope.closeBack = false
      $scope.showBack = false
      if flag
        if routMenu.length == 2 and flag
          $scope.states.activeItem = routMenu[1]
        else
          length = routMenu.length - 2
          routMenu.splice 2, length
          $scope.states.activeItem = routMenu[0]
      else
        $scope.states.activeItem = routMenu[0]
        $scope.showX = false
        $scope.hideX = false
        $scope.closeBack = false
        $scope.showBack = false
      return
    $scope.selectClassLeft = (item) ->
      $scope.showX = true
      $scope.showBack = false
      routMenu.push item
      $scope.states.activeItem = item
      return
    $scope.backMenu = ->
      if routMenu.length > 2
        $scope.hideX = $scope.showX = false
        routMenu.pop()
        $scope.states.activeItem = routMenu[routMenu.length - 1]
        $scope.showBack = true
      else
        $scope.states.activeItem = routMenu[routMenu.length - 1]
      return
    $scope.selectData = (myName) ->
      if myName != undefined
        for k of $scope.dataMenu
          if $scope.dataMenu[k].name == myName
            $scope.choiseData = $scope.dataMenu[k].items
      return
  ]