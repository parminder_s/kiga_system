define ['./app', 'angular'], (app, angular) ->
  app.controller 'scrollCtrl', ($scope) ->
    $scope.ideaIndex = 0
    $scope.elementsCategory = [
      {
        img: 'http://lorempixel.com/140/95/abstract',
        text: 'Geheimnis-Stern für kleine Botschaften mit Videoanleitung Zum V'
      },
      {img: 'http://lorempixel.com/140/95', text: 'Du bist die/der Beste auf der Welt - ein Malgedicht'},
      {img: 'http://lorempixel.com/140/95/city', text: 'Bedruckte Küchentücher - ein Familiengeschenk'},
      {img: 'http://lorempixel.com/140/95', text: 'Die Frühlingssonne - Malen mit Gelbtönen'},
      {
        img: 'http://lorempixel.com/140/95/sports',
        text: 'Geheimnis-Stern für kleine Botschaften mit Videoanleitung Zum V'
      },
      {img: 'http://lorempixel.com/140/95/people', text: 'Projektplan: Hühner-Lapbook'},
      {img: 'http://lorempixel.com/140/95', text: 'Frühblüher-Würfelpuzzle'},
      {
        img: 'http://lorempixel.com/140/95/nature',
        text: 'Geheimnis-Stern für kleine Botschaften mit Videoanleitung Zum V'
      },
      {img: 'http://lorempixel.com/140/95', text: 'Projektplan: Hühner-Lapbook'},
      {img: 'http://lorempixel.com/140/95/cats', text: 'Frühblüher-Würfelpuzzle'}
    ]
    angular.extend $scope,
      emitEventItemIndexChanged: (direction) ->
        $scope.$broadcast 'itemIndexChanged',
          downloadIndex: @ideaIndex
          direction: direction
      prevItem: (categoryIndex) ->
        @ideaIndex -= 1
        @emitEventItemIndexChanged('prev')
      nextItem: (categoryIndex) ->
        @ideaIndex += 1
        @emitEventItemIndexChanged('next')
