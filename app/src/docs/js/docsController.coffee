define ['angular', './app'], (angular, app) ->
  app.controller 'docsController', ($scope, $location, $anchorScroll, $locale,
    localizer, $filter, $q, $timeout) ->
    angular.extend $scope,
      goto: (link) ->
        $location.hash link
        $anchorScroll()
      localizer 'de'
      filter: $filter
      formats:
        current: $locale
        date: $locale.DATETIME_FORMATS
        numbers: $locale.NUMBER_FORMATS
      data:
        date2: new Date()
        birthday: new Date(1981, 11, 31)
        number: 5125
      validators:
        text1: ''
        productId: 1
      select:
        selected: ''
        preSelected: 'AT'
        countries:
          [
            {title: 'Afghanistan', code: 'AF'}
            {title: 'Åland Islands', code: 'AX'}
            {title: 'Albania', code: 'AL'}
            {title: 'Algeria', code: 'DZ'}
            {title: 'American Samoa', code: 'AS'}
            {title: 'Andorra', code: 'AD'}
            {title: 'Angola', code: 'AO'}
            {title: 'Anguilla', code: 'AI'}
            {title: 'Antarctica', code: 'AQ'}
            {title: 'Antigua and Barbuda', code: 'AG'}
            {title: 'Argentina', code: 'AR'}
            {title: 'Armenia', code: 'AM'}
            {title: 'Aruba', code: 'AW'}
            {title: 'Australia', code: 'AU'}
            {title: 'Austria', code: 'AT'}
          ]
        monthSelectionOptions:
          [
            {code: 0, title: 'MISC_JANUARY'}
            {code: 1, title: 'MISC_FEBRUARY'}
            {code: 2, title: 'MISC_MARCH'}
            {code: 3, title: 'MISC_APRIL'}
            {code: 4, title: 'MISC_MAY'}
            {code: 5, title: 'MISC_JUNE'}

          ]
        countriesNameIdx:
          [
            {name: 'Afghanistan', idx: 'AF'}
            {name: 'Åland Islands', idx: 'AX'}
            {name: 'Albania', idx: 'AL'}
            {name: 'Algeria', idx: 'DZ'}
            {name: 'American Samoa', idx: 'AS'}
            {name: 'Andorra', idx: 'AD'}
            {name: 'Angola', idx: 'AO'}
            {name: 'Anguilla', idx: 'AI'}
            {name: 'Antarctica', idx: 'AQ'}
            {name: 'Antigua and Barbuda', idx: 'AG'}
            {name: 'Argentina', idx: 'AR'}
            {name: 'Armenia', idx: 'AM'}
            {name: 'Aruba', idx: 'AW'}
            {name: 'Australia', idx: 'AU'}
            {name: 'Austria', idx: 'AT'}
          ]
      gridOptions :
        columnDefs : [
          { name:'firstName', field: 'text' },
          { name:'1stFriend', field: 'comment' }
        ]
        data : [
          {text:"123", comment: "Some Comments"}
          {text:"54", comment: "Some More Comment"}
        ]
      dragStart: ->
        console.log 'starting dragging'
      dragData: [{name: 'one'}]
      draggableObjects: [
        {name: 'one'}
        {name: 'two'}
        {name: 'three'}
      ],
      onDropComplete: (obj, evt) -> console.log 'dropped'
      asyncValidateFn: (value) -> $timeout(( -> value is 'ok'), 1000)
      asyncSubmit: ->
        console.log("about to submit")
        console.log("submitted")


  app.config ['$translateProvider'].concat ($translateProvider) ->
    $translateProvider.use 'en'
