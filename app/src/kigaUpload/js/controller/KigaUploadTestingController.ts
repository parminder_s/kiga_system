/**
 * Created by bbs on 1/14/17.
 */
export class KigaUploadTestingController {
  private filesIds: Array<number>;
  private someName: string;

  onStartUpload() {
    // This whole controller is only for manual testing of kiga-uploader
    // please leave this console.log
    console.log('onStartUpload');
  }

  onFinishUpload() {
    // This whole controller is only for manual testing of kiga-uploader
    // please leave this console.log
    console.log('onFinishUpload');
  }

  onSubmit() {
    // This whole controller is only for manual testing of kiga-uploader
    // please leave this console.log
    console.log('submitted => ' + this.someName + ' => ' + _.join(this.filesIds, ', '));
  }
}
