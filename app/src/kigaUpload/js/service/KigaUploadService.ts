/**
 * Created by bbs on 1/15/17.
 */

export class KigaUploadService {
  constructor(private endpoint) {}

  save(
    context: string,
    acceptedTypes: Array<string>,
    files: Array<File>,
    cannedAccessControlList: string
  ) {
    return this.endpoint({
      url: 'kiga-upload/push',
      data: {
        acceptedTypes: acceptedTypes,
        contentType: 'file',
        context: context,
        cannedAccessControlList: cannedAccessControlList
      },
      files: { files: files }
    });
  }
}
