/**
 * Created by bbs on 1/14/17.
 */
import * as angular from 'angular';
import * as _misc from 'misc';
import { KigaUploadService } from './service/KigaUploadService';
import { KigaUploadTestingController } from './controller/KigaUploadTestingController';

let misc = _misc;

export let app = angular.module('kigaUpload', ['misc']);

app
  .controller('kigaUploadTestingController', function() {
    return new KigaUploadTestingController();
  })
  .service('kigaUploadService', function(endpoint) {
    return new KigaUploadService(endpoint);
  })
  .config(function(stateHelperProvider) {
    stateHelperProvider.setNestedState({
      name: 'root.kigaUploadTest',
      url: '/kiga-upload/test',
      templateUrl: 'kigaUpload/partials/test.html',
      controller: 'kigaUploadTestingController as ctrl'
    });
  });
