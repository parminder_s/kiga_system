import * as angular from 'angular';
import PaymentCheckoutDirective from './paymentCheckoutDirective';
import PaymentCheckoutCtrl from './paymentCheckoutCtrl';

export default angular
  .module('payment', ['ngSanitize'])
  .controller('paymentCheckoutCtrl', function($sce, $location) {
    return new PaymentCheckoutCtrl($sce, $location);
  })
  .directive('paymentCheckoutDirective', function($timeout) {
    return new PaymentCheckoutDirective($timeout);
  });
