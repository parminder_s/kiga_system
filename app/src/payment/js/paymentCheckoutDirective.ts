/* taken from https://plnkr.co/edit/lVflCxdL9RslGH1N74fR?p=preview */

import ITimeoutService = angular.ITimeoutService;

export default class PaymentCheckoutDirective {
  constructor(private $timeout: ITimeoutService) {}

  restrict = 'E';
  replace = true;
  templateUrl = 'payment/partials/initiate.html';
  link = ($scope, $element) => {
    this.$timeout(function() {
      $element.submit();
    });
  };
}
