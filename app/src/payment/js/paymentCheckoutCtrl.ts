import ISCEService = angular.ISCEService;
import ILocationService = angular.ILocationService;
import * as _ from 'lodash';

export default class PaymentCheckoutCtrl {
  data = {};

  constructor($sce: ISCEService, $location: ILocationService) {
    const formData = $location.search();
    const url = formData.submitUrl;

    this.data = {
      action: $sce.trustAsResourceUrl(url),
      method: 'POST',
      fields: formData
    };

    (<any>require)(['../css/main.less']);
  }
}
