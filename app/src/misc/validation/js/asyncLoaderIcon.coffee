###*
  @ngDoc service
  @name asyncLoaderIcon
  @module validation
  @description

  Companion service for asyncValidation. Adds a wrapper div around the input
  element that needs to be validated asynchronous and while waiting for the
  response it shouws the loader icon next to the input on the right side.
###

define ['./app', 'jquery', '../../../url'], (app, $, urlFn, $timeout) ->
  url = urlFn.default
  app.service 'asyncLoaderIcon', -> (element) -> new class AsyncLoaderIcon
    constructor: ->
      @element = element
    wrapper: null
    loader: null
    create: ->
      modelName = element.attr('data-test') ? ''
      imgSrc = url('misc/images/loadingSmall.gif')
      @loader = $ '<img class="loading" src="' + imgSrc + '">'
      @wrapper = @element.wrap '<div class="wrapper"></div>'
      @element.after @loader
      @loader.hide()

    show: ->
      if @loader == null
        @create()
      if @messages == null
        @messages = @element.next()
      @element.after @loader
      @element.parent().after @messages
      @loader.show()

    hide: ->
      @loader.hide()
