###*
  @ngdoc module
  @name app
  @module kiga.validation

  @description

  The validation module adapts ng-fab-form and adds additional capabilities
  like customized messages or the asyncValidator for emails.

  Further more it also customizes the configuration of ng-fab-form.

  It is possible to add new validators by creating a directive that
  adds a new validator to the  controller's $validators object but
  one has to add the directive's name as message in the
  validationMessages.html in this module even if customized messages
  are used.
###

define ['angular', 'angular-messages', 'ng-fab-form'], (angular) ->
  angular
    .module('kiga.validation', ['ngFabForm', 'ngMessages'])

