
###*

  @ngdoc Decorator
  @name noFabFormValidation
  @module misc
  @description
  decorates ngFabForm directive to add ability to disable validation with
  no-fab-form-validation
###

define ['./app'], (app) ->
  app.config ($provide) ->
    $provide.decorator 'ngFabFormValidationsDirective', ($delegate) ->
      oldCompile = $delegate.compile
      oldCtrl = $delegate.controller
      isRequireArray = Array.isArray $delegate.require
      oldLink = null
      icheck = null

      onICheckCompile = (scope, el, attrs, controllers) =>
        el = el.parent()
        oldLink.call this, scope, el, attrs, controllers

      link = (scope, el, attrs, controllers) ->
        icheck = controllers.shift()
        controllers = controllers[0] if not isRequireArray
        if (icheck?)
          icheck.getCompilePromise().then onICheckCompile.bind this, scope,
            el, attrs, controllers
        else
          oldLink.call this, scope, el, attrs, controllers
      $delegate.require = ['?icheck'].concat($delegate.require)
      $delegate.compile = (el, attrs) ->
        if (not attrs.noFabFormValidation?)
          oldLink = oldCompile.call this, el, attrs
          link
      $delegate
