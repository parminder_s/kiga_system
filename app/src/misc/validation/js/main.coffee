define ['./app', './asyncValidator', './validationConfig', './fabFormValidationDecorator', './asyncValidation',
'./asyncLoaderIcon','./uiValidateFabForm', './validationMessages',
'./serverValidation','./provider/valueEmailRegex'], (app, asyncValidator) ->
  asyncValidator.default app

