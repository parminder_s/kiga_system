import IPromise = angular.IPromise;
import ITimeoutService = angular.ITimeoutService;
/**
 This is in contrast to asyncValidation a more generic
 */

export default function createAsyncValidator(app: angular.IModule) {
  app.directive('asyncValidator', function($timeout: ITimeoutService, asyncLoaderIcon) {
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: {
        asyncFn: '=',
        asyncMsg: '@',
        dependendModel: '@'
      },
      link: {
        pre: function(scope: any, element, attr, controller: any) {
          attr.$observe('dependendModel', function() {
            controller.$validate();
          });

          let loader = asyncLoaderIcon(element);
          let timer = null;

          let validate = value => {
            loader.show();
            return new Promise<boolean>((resolve, reject) => {
              let resolveFn = (result: boolean) => {
                loader.hide();
                if (result) {
                  resolve(result);
                } else {
                  reject();
                }
              };

              let rejectFn = () => {
                loader.hide();
                reject();
              };
              let asyncPromise: Promise<boolean> = scope.asyncFn(value, attr);
              asyncPromise.then(resolveFn, rejectFn);
            });
          };

          controller.$asyncValidators[scope.asyncMsg] = value => {
            if (timer) {
              $timeout.cancel(timer);
            }
            timer = $timeout(() => validate(value));
            return timer;
          };
        }
      }
    };
  });
}
