###*
  @ngdoc service
  @name misc.serverValidation

  @description
  service to perform arbitrary value validation on server-side.
  supposed to work well with angular validation.
###

define ['./app'], (app) ->
  app.service 'serverValidation', (request, endpoint, $q) ->
    checkValue: (type, value, context, url, useEndpoint) ->
      url = url || 'validateValue'
      def = $q.defer()
      if(useEndpoint)
        promise = endpoint
          quietMode: true
          url: url
          data:
            type: type
            value: value
            context: context
      else
        promise = request
          quietMode: true
          url: url
          data:
            type: type
            value: value
            context: context
      promise.then(
        (res) ->
          def.resolve(res) if res
          def.reject(res) if not res
        (res) -> def.reject(false)
      )
      def.promise
