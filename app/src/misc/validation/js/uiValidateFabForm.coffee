
###*

  @ngdoc directive
  @name forcedValidation
  @module misc
  @description

  adds a dummy validator to have ng-fab-form logic enabled
  for input in case there is uiValidator
  uiValidator does not use angular's v1.3 $validators

###

define ['./app'], (app) ->
  app.directive 'uiValidate', () ->
    restrict: 'A'
    require: 'ngModel'
    link:
      pre:
        (scope, element, attrs, controller) ->
          # ngfab forms require sync validator exists to apply, so we put dummy
          controller.$validators["uiValidateFabFormConnector"] = () -> true
