###
  @ngdoc directive
  @name validationMessages
  @module kiga.validation

  @description
  This serves as real validation info directive -> the "element" is generated
  dynamically from validationElement directive
###

define ['./app'], (app) ->
  app.directive 'validationMessages', ($log, $translate, ngFabForm) ->
    restrict: 'E'
    require: '^form'
    scope:
      inputElement: '='
    templateUrl: 'misc/validation/partials/validationMessages.html'
    compile: (element, attrs, transclude) ->
      container = ngFabForm.addCustomValidations(element.html(), attrs)
      element.html(container.html())
      (scope, element, atts, formCtrl) ->
        scope.field = scope.inputElement
        scope.form = formCtrl
