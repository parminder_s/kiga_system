###*

  @ngdoc directive
  @name asyncValidation
  @module misc
  @description

  Asynchronous validation for a input value.
  uses standard angular validation

  Expects type as a value of directive attribute.
    [async-validation-context]: POJO with arbitrary contextual
    information sent to server


  validation messages are in validationMessages.html with validation key
  'asyncValidation{type}' there type is validation type passed
  to directive attribute


  @usage
  <input name="email" type="email" ng-model="email"
      async-validation="email" async-validation-context="{{ productId }}" />

###

define ['./app', 'jquery'], (app, $) ->
  debounceDelay = 500

  app.directive 'asyncValidation', ($q, serverValidation, $timeout,
  asyncLoaderIcon) ->
    restrict: 'A'
    require: 'ngModel'
    scope:
      type: '@asyncValidation'
      context: '=?asyncValidationContext'
      url: '@asyncUrl'
      useEndpoint: '@useEndpoint'
    link:
      pre:
        (scope, element, attrs, controller) ->
          loader = asyncLoaderIcon element
          context = scope.context or null
          type = scope.type
          timer = null
          checkValue = null
          validate = ->
            loader.show()
            timer = null
            def = $q.defer()

            resolveFn = ->
              loader.hide()
              def.resolve()

            rejectFn = ->
              loader.hide()
              def.reject()

            serverValidation
              .checkValue(type, checkValue, context, scope.url, scope.useEndpoint)
              .then(resolveFn, rejectFn)
            def.promise

          controller.$asyncValidators["asyncValidation#{type}"] = (value) ->
            checkValue = value
            $timeout.cancel timer if timer
            timer = $timeout validate, debounceDelay
