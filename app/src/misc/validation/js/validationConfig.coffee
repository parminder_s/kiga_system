###
  @ngdoc overview

  @name misc.validation

  @description
  configures ngFabForm to use our custom validation messages
  and could contain additional config parameters

  added emailRegex with case insensitive
###

define ['./app'], (app) ->
  app.config (ngFabFormProvider, valueEmailRegexProvider) -> ngFabFormProvider.extendConfig
    validationsTemplate : 'misc/validation/partials/validationMessages.html'
    emailRegex: valueEmailRegexProvider.$get()

