import './toast.less';
import ConfiguredToast from './ConfiguredToast';

/**
 * Facade for ng-toast.
 * The service hands out ConfiguredToast objects from which to
 * create toast messages.
 *
 * Create a message with the default settings:
 *
 *    toastService.default().success("my message");
 *
 *    // or shorter:
 *    toastService.sucess("my message");
 *
 * Using specific configuration:
 *
 *    let myConfig = toastService.getConfiguredToast({...config...});
 *    myConfig.success("my configured message");
 *
 *
 */
export default class ToastService {
  defaultConfiguredToast: ConfiguredToast;

  constructor(private ngToast: any, private translate: any) {
    this.defaultConfiguredToast = new ConfiguredToast(ngToast);
    this.defaultConfiguredToast.configure({
      additionalClasses: 'toast-anim container',
      dismissOnTimeout: 'true',
      timeout: 5500,
      dismissButton: true,
      dismissOnClick: true
    });
  }

  /**
   * Directly pass through.
   * @param configuration object
   */
  create(toastConfig: any) {
    this.ngToast.create(toastConfig);
  }

  /**
   * Programmatically dismiss one (or all) toasts.
   * @param toast the toast to be dismissed or empty
   */
  dismiss(toast?: any) {
    this.ngToast.dismiss(toast);
  }

  /**
   * Look up translation, then show the toast.
   * Returns promise for the created toast.
   * @param toastCreateFunc the callable that produces the toast
   * @param translatableString text part to be translated
   */
  private showTranslated(toastCreateFunc, translatableString: string): any {
    return this.translate(translatableString)
      .then(translated => {
        return toastCreateFunc(translated);
      })
      .catch(reason => {
        return toastCreateFunc(translatableString);
      });
  }

  success(content: string) {
    return this.showTranslated(v => this.default().success(v), content);
  }

  warning(content: string) {
    return this.showTranslated(v => this.default().warning(v), content);
  }

  error(content: string) {
    return this.showTranslated(v => this.default().error(v), content);
  }

  /**
   * Create a new configuration from which to create toasts.
   * @param configuration
   */
  getConfiguredToast(configuration: any) {
    let configuredToast = new ConfiguredToast(this.ngToast);
    configuredToast.configure(this.defaultConfiguredToast.getConfig());
    configuredToast.configure(configuration);
    return configuredToast;
  }

  default() {
    return this.defaultConfiguredToast;
  }

  permanent() {
    return this.getConfiguredToast({ dismissOnTimeout: false });
  }
}
