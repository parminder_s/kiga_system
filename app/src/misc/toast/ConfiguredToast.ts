/**
 * Keeps a configuration for toasts and passes through calls
 * to ngToast.
 */
export default class ConfiguredToast {
  configuration: any = {};

  constructor(private ngToast: any) {}

  /**
   * Set configuration.
   * @param config
   */
  configure(config: any) {
    this.configuration = Object.assign(this.configuration, config);
    return this;
  }

  getConfig() {
    return this.configuration;
  }

  /**
   * Create a "success"-themed message.
   * @param content
   */
  success(content: string) {
    let config = Object.assign({}, this.configuration);
    config.content = content;
    config.className = 'success';
    return this.ngToast.create(config);
  }

  /**
   * Create a "warning"-themed message.
   * @param content
   */
  warning(content: string) {
    let config = Object.assign({}, this.configuration);
    config.content = content;
    config.className = 'warning';
    return this.ngToast.create(config);
  }

  /**
   * Create an "error"-themed message.
   * @param content
   */
  error(content: string) {
    let config = Object.assign({}, this.configuration);
    config.content = content;
    config.className = 'danger';
    return this.ngToast.create(config);
  }
}
