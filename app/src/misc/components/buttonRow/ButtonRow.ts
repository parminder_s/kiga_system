export default class ButtonRowCtrl {
  private categories;
  private selectedHierarchy;
  private selectedHierarchyLimit;
  private limit;
  private prevBtn;
  private nextBtn;
  private el;
  private isLastNavigationLevel;
  private firstButtonName;
  private isFavorites;

  constructor() {}

  $onInit() {
    this.prevBtn = angular.element('#idea-types-prev');
    this.nextBtn = angular.element('#idea-types-next');
    this.el = angular.element('.buttons-container');
    this.limit = parseInt(this.selectedHierarchyLimit);
    this.setStartPosition();
    this.updateButtonsVisibility();
  }

  updateButtonsVisibility() {
    setTimeout(() => {
      if (this.getPos() === 0) {
        if (this.el[0].scrollWidth - this.el.innerWidth()) {
          this.prevBtn.addClass('inactive-btn');
        } else {
          this.prevBtn.hide();
        }
      } else {
        this.prevBtn.show();
        this.prevBtn.removeClass('inactive-btn');
      }

      if (this.getPos() === this.getMaxPos() || this.getMaxPos() < 10) {
        this.nextBtn.hide();
      } else {
        this.nextBtn.show();
      }
    }, 0);
  }

  setStartPosition() {
    if (this.categories) {
      setTimeout(() => {
        let activeCategory = 0;
        this.categories.forEach((category, i, arr) => {
          if (this.selectedHierarchy[this.selectedHierarchy.length - 1] === category.name) {
            activeCategory = i;
            if (this.selectedHierarchy.length > this.limit) activeCategory++;
          }
        });
        let pos = this.getPos();
        let idx = activeCategory;
        let sumWidth = this.getChildrenSize(0) * (idx + 1);
        let pageWidth = this.getScrollPage();
        let page = Math.floor(sumWidth / pageWidth - 0.1);
        if (page) this.el.duScrollTo(page * pageWidth);
        else if (pos) {
          let offset = Math.round((this.getPos() + this.getChildrenSize(0)) / pageWidth);
          this.el.duScrollTo(offset * pageWidth);
        }
      }, 0);
    }
  }

  getPos() {
    return Math.ceil(this.el[0].scrollLeft);
  }

  getWindowSize() {
    return this.el.innerWidth();
  }

  getMaxPos() {
    let windowSize = this.getWindowSize();
    return this.el[0].scrollWidth - windowSize;
  }

  onPrevClick() {
    this.scrollTo(this.getPos() - this.getScrollPage());
  }

  onNextClick() {
    this.scrollTo(this.getPos() + this.getScrollPage());
  }

  scrollTo(pos) {
    if (this.getMaxPos() - pos < this.getChildrenSize(0)) {
      pos = this.getMaxPos();
    }
    if (pos < this.getChildrenSize(0) && this.getChildrenSize(0) - pos > 10) {
      pos = 0;
    }
    this.el.animate({ scrollLeft: pos }, () => this.updateButtonsVisibility());
  }

  getScrollPage() {
    return this.el.innerWidth();
  }

  getChildrenSize(idx) {
    let children = this.el.children();
    if (idx === -1) {
      idx = children.length - 1;
    }
    let size = angular.element(children[idx]).outerWidth(true);

    return Math.ceil(size);
  }

  generateArrowBtnClass() {
    if (this.categories) {
      let categoriesLength = this.categories.length;
      let level = this.selectedHierarchy.length;
      let arrow_container_md = false;
      let arrow_container_small = false;
      if (level) {
        if (categoriesLength === 5 || categoriesLength === 6) {
          arrow_container_md = true;
        } else if (categoriesLength === 3 || categoriesLength === 4) {
          arrow_container_small = true;
        }
      } else {
        if (categoriesLength === 6 || categoriesLength === 7) {
          arrow_container_md = true;
        } else if (categoriesLength === 4 || categoriesLength === 5) {
          arrow_container_small = true;
        }
      }
      return {
        'arrow-container-small': arrow_container_small,
        'arrow-container-md': arrow_container_md
      };
    }
  }

  generateBtnRowClass() {
    if (this.categories) {
      let categoriesLength = this.categories.length;
      let level = this.selectedHierarchy.length;
      let sort_row_small = false;
      let sort_row_small_plus = false;
      let sort_row_md = false;
      let sort_row_md_plus = false;
      if (level) {
        if (categoriesLength === 3) {
          sort_row_small = true;
        } else if (categoriesLength === 4) {
          sort_row_small_plus = true;
        } else if (categoriesLength === 5) {
          sort_row_md = true;
        } else if (categoriesLength === 6) {
          sort_row_md_plus = true;
        }
      } else {
        if (categoriesLength === 4) {
          sort_row_small = true;
        } else if (categoriesLength === 5) {
          sort_row_small_plus = true;
        } else if (categoriesLength === 6) {
          sort_row_md = true;
        } else if (categoriesLength === 7) {
          sort_row_md_plus = true;
        }
      }
      return {
        'sort-row-small': sort_row_small,
        'sort-row-small-plus': sort_row_small_plus,
        'sort-row-md': sort_row_md,
        'sort-row-md-plus': sort_row_md_plus
      };
    }
  }
}
