import MockedCountryService from '../../js/services/MockedCountryService';
export default class MockedCountryCtrl {
  constructor(private mockedCountryService: MockedCountryService) {}

  remove() {
    this.mockedCountryService.remove();
  }
}
