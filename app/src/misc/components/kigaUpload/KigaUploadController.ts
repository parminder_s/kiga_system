/**
 * Created by bbs on 1/14/17.
 */

import { KigaUploadService } from '../../../kigaUpload/js/service/KigaUploadService';

export class KigaUploadController {
  private _maxFiles = null;
  private filesCollection: Array<any> = [];
  private uploadedFiles: Array<any> = null;
  private ngModel: Array<number> = null;
  private acceptedTypes;
  private context: string;
  private onStartUpload;
  private onFinishUpload;
  private name;
  private hidePreview;
  private cannedAccessControlList;

  constructor(private kigaUploadService: KigaUploadService, private isRequired) {}

  $onInit() {
    if (this.uploadedFiles != null) {
      this.filesCollection = _.concat(this.filesCollection, this.uploadedFiles);
    }
  }

  /**
   * This method is used by the jquery uploader component and therefore the "this" instance
   * available inside this method is not an instance of the KigaUploadController. Hence, we extract
   * KigaUploadController instance to the "that" variable in the first lines of the method.
   *
   * @param files
   */
  onFileAdd(files: Array<any>) {
    if (files == null) {
      return;
    }

    // noinspection TypeScriptUnresolvedVariable
    let that = (<any>this).$parent.$ctrl;

    if (typeof that.onStartUpload === 'function') {
      that.onStartUpload();
    }

    let itemsToBeInserted = Math.min(
      files.length,
      Math.max(0, that._maxFiles - that.filesCollection.length)
    );

    let filesToBeSaved: Array<any> = [];

    let filesToBeAddedToCollection = [];
    for (let i = 0; i < itemsToBeInserted; ++i) {
      let file = files[i];
      let previewFileDataUrl = null;
      if (file.preview != null) {
        previewFileDataUrl = file.preview.toDataURL(file.type);
      }
      filesToBeAddedToCollection.push({
        name: file.name,
        size: file.size,
        url: previewFileDataUrl,
        error: null
      });
      filesToBeSaved.push({ file: file });
    }

    let acceptedTypesAsArray = [];
    if (this.acceptedTypes != null && this.acceptedTypes.trim() !== '') {
      acceptedTypesAsArray = this.acceptedTypes.trim().split(',');
      acceptedTypesAsArray = _.map(acceptedTypesAsArray, function(acceptedType) {
        return acceptedType != null ? acceptedType.trim().toUpperCase() : '';
      });
      acceptedTypesAsArray = _.compact(acceptedTypesAsArray);
    }

    that.kigaUploadService
      .save(
        that.context,
        acceptedTypesAsArray,
        _.map(filesToBeSaved, function(item) {
          return item.file;
        }),
        that.cannedAccessControlList
      )
      .then(
        data => {
          if (that.ngModel == null) {
            that.ngModel = [];
          }

          _.each(data.uploadedFiles, (fileUpload, idx) => {
            if (fileUpload.isFailed) {
              filesToBeAddedToCollection[idx].error = fileUpload.error;
            } else {
              filesToBeAddedToCollection[idx].url = fileUpload.url;
              that.ngModel.push(fileUpload.id);
            }
          });

          that.filesCollection = _.concat(that.filesCollection, filesToBeAddedToCollection);

          if (typeof that.onFinishUpload === 'function') {
            that.onFinishUpload();
          }
        },
        () => {
          _.each(filesToBeAddedToCollection, file => {
            file.error = 'Unexpected error - could not upload file.';
          });
          that.filesCollection = _.concat(that.filesCollection, filesToBeAddedToCollection);
          if (typeof that.onFinishUpload === 'function') {
            that.onFinishUpload();
          }
        }
      );
  }

  onRemoveFile(idx) {
    this.ngModel.splice(idx, 1);
    this.filesCollection.splice(idx, 1);

    if (this.ngModel.length === 0) {
      this.ngModel = null;
    }
  }

  get maxFiles(): number {
    if (this._maxFiles == null) {
      this._maxFiles = 1;
    }
    return this._maxFiles;
  }

  set maxFiles(value: number) {
    this._maxFiles = value;
  }
}
