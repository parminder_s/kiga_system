export default class StarRatingCtrl {
  private rating;
  private kigaPageId;

  constructor(private endpoint) {}

  $onInit() {
    this.endpoint({
      method: 'GET',
      url: 'starRating/' + this.kigaPageId,
      quietMode: true
    }).then(data => {
      this.rating = data;
    });
  }

  addRating(rating) {
    if (!this.rating.userRating) {
      this.rating.userRating = rating;
      this.endpoint({
        url: 'starRating/updateRating/',
        data: {
          kigaPageId: this.kigaPageId,
          userRating: rating
        },
        quietMode: true
      }).then(data => {
        this.rating.count = _.get(data, 'count');
      });
    }
  }

  clearRating() {
    this.endpoint({
      url: 'starRating/clearRating/',
      data: {
        kigaPageId: this.kigaPageId
      },
      quietMode: true
    }).then(data => {
      this.rating.userRating = 0;
      this.rating.rating = _.get(data, 'rating');
      this.rating.count = _.get(data, 'count');
    });
  }
}
