import IScope = angular.IScope;
import AudioHandler from '../../js/services/AudioHandler';

export default class AudioPlayerCtrl {
  private audio: HTMLAudioElement;
  private endpointUrl: string;
  private icon = 'icon-play';
  private cssClass = 'paused';
  private href: string;
  private showOnlyButtons: boolean = false;

  private previousHref;

  constructor(private $scope: IScope, private audioHandler: AudioHandler) {}

  $onInit() {
    this.endpointUrl = window['kiga']['endPointUrl'];
    this.audio = new Audio(this.href);
    this.audio.volume = 0.8;

    this.$scope.$on('otherPlaying', () => {
      if (this.audio.paused === false) {
        this.pause();
      }
    });
    this.audioHandler.addAudio(this.audio);
    this.previousHref = this.href;
  }

  $onChanges() {
    if (this.previousHref != this.href && this.previousHref != null) {
      this.pause();
      this.audio = new Audio(this.href);
      this.audio.volume = 0.8;
      this.audioHandler.addAudio(this.audio);
    }
    this.previousHref = this.href;
  }

  audioCtrl() {
    this.audio.onended = () => (this.icon = 'icon-play');
    if (this.audio.paused) {
      this.play();
    } else {
      this.pause();
    }
  }

  play() {
    this.$scope.$parent.$broadcast('otherPlaying', () => this.pause());
    this.audio.play();
    this.icon = 'icon-pause';
    this.cssClass = 'playing';
  }

  pause() {
    this.audio.pause();
    this.icon = 'icon-play';
    this.cssClass = 'paused';
  }
}
