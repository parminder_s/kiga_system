import * as angular from 'angular';

export default class VideoPlayerCtrl {
  private videoLink;
  private previewLink;
  private randomId;
  private button;
  private playButton;
  private video;

  constructor() {}

  $onInit() {
    this.randomId = randomInteger(10);
    if (typeof window['jwplayer'] === 'function' && window['jwplayer'](this.randomId)['key']) {
      window['jwplayer'](this.randomId).setup({
        file: this.videoLink,
        image: this.previewLink,
        width: '100%',
        aspectratio: '16:9'
      });
      this.video = window['jwplayer'](this.randomId);
    } else {
      (<any>require)(['../../../rawJs/jwplayer', '../../../rawJs/jwplayer.html5'], () => {
        let jwplayer = window['jwplayer'];
        jwplayer['key'] = '29ODX2dQRS4ENOY6iJXeJxtKKtu8LZVSL8XxEg==';
        jwplayer(this.randomId).setup({
          file: this.videoLink,
          image: this.previewLink,
          width: '100%',
          aspectratio: '16:9'
        });
        this.video = jwplayer(this.randomId);
      });
    }
    this.playButton = angular.element(this.button);
    this.playButton.on('click', this.playVideo.bind(this));
  }

  $onChanges() {
    if (this.video) {
      if (this.video.getState() === 'PLAYING') {
        this.video.pause();
        this.playButton.addClass('paused').removeClass('playing');
      }
      this.video.load({ image: this.previewLink, sources: [{ file: this.videoLink }] });
      this.setupEventHandlers();
    }
  }

  playVideo() {
    this.setupEventHandlers();
    if (this.video.getState() !== 'PLAYING') {
      this.video.play();
    } else {
      this.video.pause();
    }
  }

  setupEventHandlers() {
    this.video.onComplete(e => {
      this.playButton.addClass('paused').removeClass('playing');
    });
    this.video.onPlay(e => {
      this.playButton.addClass('playing').removeClass('paused');
    });
    this.video.onBuffer(e => {
      this.playButton.addClass('playing').removeClass('paused');
    });
    this.video.onPause(e => {
      this.playButton.addClass('paused').removeClass('playing');
    });
  }
}

function randomInteger(charLength) {
  let unId = '';
  const alfabet = 'abcdefghigklmnopqrstuvqxyzABCDEFGHIGKLMNPQRSTUVWZYZ';
  function random() {
    let max, min, rand;
    min = 0;
    max = alfabet.length - 1;
    rand = min + Math.random() * (max - min);
    return (rand = Math.round(rand));
  }
  for (let i = 0; i < charLength; i++) {
    unId += alfabet[random()];
  }
  return unId;
}
