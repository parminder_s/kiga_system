import IMember from './IMember';

interface IMemberService {
  get(): IMember;
  set(member: IMember);
}

export default IMemberService;
