import { IConfigService } from '../../js/services/AngularInterfaces';
import nonFullTestResolvePermissionDenied from './NonFullTestPermissionStateResolver';
import fullTestResolvePermissionDenied from './FullTestPermissionStateResolver';

export default class PermissionStateResolver {
  constructor(private member, private config: IConfigService) {}

  resolvePermissionDenied(isNewIdea: boolean = false) {
    if (this.config.getPermissionMode() === 'fullTest') {
      return fullTestResolvePermissionDenied(isNewIdea);
    } else {
      return nonFullTestResolvePermissionDenied(this.member, isNewIdea);
    }
  }
}
