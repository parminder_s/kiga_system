import * as angular from 'angular';
import * as _misc from 'misc';
import { IConfigService, IMemberLoader } from '../../js/services/AngularInterfaces';
import PermissionStateChangeHandler from './PermissionStateChangeHandler';
import createPermissionRouting from './PermissionRouting';
import PermissionHandlerRegistry from './handlers/PermissionHandlerRegistry';
import MemberLoader from './MemberLoader';
import IMemberService from './IMemberService';
import FullSubscriptionHandler from './handlers/FullSubscriptionHandler';
import FullController from './controllers/FullController';
import LoginOrTestCommunityController from './controllers/LoginOrTestCommunityController';
import QuickRegister from './services/QuickRegister';
import FullLoginHandler from './handlers/FullLoginHandler';
import IdeaRedirectService from '../../js/services/IdeaRedirectService';
import { StateService, TransitionService } from 'angular-ui-router';
import TestSubscriptionHandler from './handlers/TestSubscriptionHandler';
import PermissionStateResolver from './PermissionStateResolver';
import InvoiceOpenCtrl from './controllers/invoiceOpenCtrl';
import ShopRegistrationController from './controllers/ShopRegistrationController';

let misc = _misc;

let app = angular.module('kiga.permission', ['misc']);

app
  .service('permissionStateChangeHandler', function(
    $transitions: TransitionService,
    permissionHandlerRegistry: PermissionHandlerRegistry,
    $state: StateService,
    $q: ng.IQService,
    memberLoader: MemberLoader,
    ideaRedirectService: IdeaRedirectService
  ) {
    return new PermissionStateChangeHandler(
      $transitions,
      permissionHandlerRegistry,
      $state,
      $q,
      memberLoader,
      ideaRedirectService
    );
  })
  .service('fullSubscriptionHandler', function(
    member: IMemberService,
    permissionStateResolver: PermissionStateResolver,
    $state: StateService
  ) {
    return new FullSubscriptionHandler(member, $state, permissionStateResolver);
  })
  .service('testSubscriptionHandler', function(
    member: IMemberService,
    permissionStateResolver: PermissionStateResolver,
    $state: StateService
  ) {
    return new TestSubscriptionHandler(member, $state, permissionStateResolver);
  })
  .service('fullLoginHandler', function(member: IMemberService, $state: StateService) {
    return new FullLoginHandler(member, $state);
  })
  .service(
    'memberLoader',
    (member: IMemberService, memberLoaderService: IMemberLoader) =>
      new MemberLoader(member, memberLoaderService)
  )
  .service('permissionHandlerRegistry', () => new PermissionHandlerRegistry())
  .service('permissionStateResolver', function(member, config: IConfigService) {
    return new PermissionStateResolver(member, config);
  })
  .service('quickRegister', function(request) {
    return new QuickRegister(request);
  })
  .controller('fullCtrl', function(
    $filter,
    price,
    request,
    formHelper,
    expiredType,
    withLogin,
    $state,
    $stateParams,
    $window
  ) {
    return new FullController(
      $filter,
      price,
      request,
      formHelper,
      expiredType,
      withLogin,
      $state,
      $stateParams,
      $window
    );
  })
  .controller('invoiceOpenCtrl', function($window, goHome, links, $state) {
    return new InvoiceOpenCtrl($window, goHome, links, $state);
  })
  .controller('loginOrTestCommunityCtrl', function(
    $state,
    $stateParams,
    $window,
    request,
    quickRegister,
    formHelper,
    member,
    country,
    subscription,
    config,
    productId,
    $translate,
    JobOptionsService,
    links
  ) {
    return new LoginOrTestCommunityController(
      $state,
      $stateParams,
      $window,
      request,
      quickRegister,
      formHelper,
      member,
      country,
      subscription,
      config,
      productId,
      $translate,
      JobOptionsService,
      links
    );
  })
  .controller('shopRegistrationCtrl', function(
    $state,
    $window,
    quickRegister,
    member,
    country,
    subscription,
    config,
    productId,
    $translate,
    JobOptionsService,
    links
  ) {
    return new ShopRegistrationController(
      $state,
      $window,
      quickRegister,
      member,
      country,
      subscription,
      config,
      productId,
      $translate,
      JobOptionsService,
      links,
      'shop'
    );
  })
  .controller('kgaRegistrationCtrl', function(
    $state,
    $window,
    quickRegister,
    member,
    country,
    subscription,
    config,
    productId,
    $translate,
    JobOptionsService,
    links
  ) {
    return new ShopRegistrationController(
      $state,
      $window,
      quickRegister,
      member,
      country,
      subscription,
      config,
      productId,
      $translate,
      JobOptionsService,
      links,
      'kga'
    );
  })

  .run(function(
    permissionHandlerRegistry: PermissionHandlerRegistry,
    fullSubscriptionHandler: FullSubscriptionHandler,
    testSubscriptionHandler: TestSubscriptionHandler,
    permissionStateChangeHandler: PermissionStateChangeHandler
  ) {
    permissionHandlerRegistry.registerHandler('fullSubscription', fullSubscriptionHandler);
    permissionHandlerRegistry.registerHandler('testSubscription', testSubscriptionHandler);
    permissionStateChangeHandler.start();
  });

createPermissionRouting(app);
