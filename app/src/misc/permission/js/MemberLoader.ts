import { IMemberLoader } from '../../js/services/AngularInterfaces';
import IMember from './IMember';
import IMemberService from './IMemberService';

export default class MemberLoader {
  constructor(private memberService: IMemberService, private memberLoader: IMemberLoader) {}

  /**
   * returns the member and loads it if it hasn't already.
   */
  getOrLoad(): Promise<IMember> {
    return this.memberLoader.getPromise().then(member => {
      this.memberService.set(member);
      return member;
    });
  }

  reset(): Promise<IMember> {
    this.memberLoader.invalidate();
    return this.getOrLoad();
  }
}
