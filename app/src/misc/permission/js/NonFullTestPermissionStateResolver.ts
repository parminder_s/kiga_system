import PermissionStateResolver from './PermissionStateResolver';
export default function nonFullTestResolvePermissionDenied(member, isNewIdea: boolean) {
  let memberData = member.getAll();
  let subscription = memberData.subscription;
  let returner = 'root.permission.';
  if (memberData.isAnonymous) {
    if (isNewIdea) {
      return 'root.login';
    } else {
      return 'root.permission.loginOrFull';
    }
  } else {
    if (subscription.group === 'community' && isNewIdea) {
      return returner + 'fullRequired';
    }
    if (subscription.group === 'community' && !isNewIdea) {
      return returner + 'fullRequired';
    }

    if (subscription.group === 'test' && subscription.isExpired && isNewIdea) {
      return returner + 'testExpired';
    }
    if (subscription.group === 'test' && subscription.isExpired && !isNewIdea) {
      return returner + 'testExpired';
    }

    if (subscription.isCancelled) {
      return returner + 'fullExpired';
    }
    if (subscription.isLocked) {
      return returner + 'invoiceOpen';
    } else if (subscription.group === 'test') {
      return returner + 'fullUpgrade';
    }
  }
  return 'root.login';
}
