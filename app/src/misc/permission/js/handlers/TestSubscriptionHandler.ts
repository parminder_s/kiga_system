import IMemberService from '../IMemberService';
import PermissionStateResolver from '../PermissionStateResolver';
import { StateService, Transition, TargetState } from 'angular-ui-router';

export default class TestSubscriptionHandler {
  constructor(
    private memberService: IMemberService,
    private $state: StateService,
    private permissionStateResolver: PermissionStateResolver
  ) {}

  handle(transition: Transition): boolean | TargetState {
    if (
      this.memberService.get().hasActiveTestSubscription() ||
      this.memberService.get().hasActiveFullSubscription()
    ) {
      return true;
    } else {
      let targetState = this.permissionStateResolver.resolvePermissionDenied(true);
      return this.$state.target(
        targetState,
        { locale: transition.params('to')['locale'] },
        { location: 'replace' }
      );
    }
  }
}
