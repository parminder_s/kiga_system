import IMemberService from '../IMemberService';
import PermissionStateResolver from '../PermissionStateResolver';
import { StateService, Transition, TargetState } from 'angular-ui-router';

export default class FullSubscriptionHandler {
  constructor(
    private memberService: IMemberService,
    private $state: StateService,
    private permissionStateResolver: PermissionStateResolver
  ) {}

  handle(transition: Transition): boolean | TargetState {
    if (this.memberService.get().hasFullSubscription()) {
      return true;
    } else {
      return this.$state.target(this.permissionStateResolver.resolvePermissionDenied(false), {
        locale: transition.params('to')['locale']
      });
    }
  }
}
