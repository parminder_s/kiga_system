import PermissionHandler from './PermissionHandler';
import * as _ from 'lodash';

export default class PermissionHandlerRegistry {
  handlers: { [name: string]: PermissionHandler } = {};

  registerHandler(name: string, handler: PermissionHandler) {
    this.handlers[name] = handler;
  }

  getHandler(name: string) {
    return this.handlers[name];
  }

  hasHandler(name: string) {
    return _.has(this.handlers, name);
  }
}
