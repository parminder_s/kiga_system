import { Transition, TargetState } from 'angular-ui-router';

interface PermissionHandler {
  handle(transition: Transition): boolean | TargetState;
}

export default PermissionHandler;
