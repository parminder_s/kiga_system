import IMemberService from '../IMemberService';
import { StateService, Transition, TargetState } from 'angular-ui-router';

export default class ShopCheckoutPermissionHandler {
  constructor(private memberService: IMemberService, private $state: StateService) {}

  handle(transition: Transition): boolean | TargetState {
    if (this.memberService.get().hasFullLogin) {
      return true;
    } else {
      if (this.memberService.get().isLoggedIn()) {
        return this.$state.target('root.fullLogin', {
          forward: transition.targetState().name()
        });
      } else {
        return this.$state.target('root.login', { shop: true });
      }
    }
  }
}
