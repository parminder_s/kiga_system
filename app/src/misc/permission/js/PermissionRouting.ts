import IModule = angular.IModule;
import { AreaType } from '../../../area/js/AreaType';
let template = function(partial) {
  return 'misc/permission/partials/' + partial + '.html';
};

export default function createPermissionRouting(app: IModule) {
  app.config(function(stateHelperProvider) {
    return stateHelperProvider.setNestedState({
      name: 'root.permission',
      url: '/permission',
      template: '<div ui-view></div>',
      abstract: true,
      params: {
        area: AreaType.NONE
      },
      children: [
        {
          name: 'loginOrCommunity',
          url: '/login-or-community?forward',
          templateUrl: template('loginOrTestCommunity'),
          controller: 'loginOrTestCommunityCtrl as ctrl',
          resolve: {
            country: function(request) {
              return request('main/country');
            },
            subscription: function() {
              return 'community';
            },
            productId: function(request) {
              return request({
                url: 'nregistration/productGroupIdToProductId',
                data: {
                  country: 'at',
                  productGroup: 'community'
                }
              });
            }
          }
        },
        {
          name: 'shopRegistration',
          url: '/shop-registration?forward',
          templateUrl: template('shopRegistration'),
          controller: 'shopRegistrationCtrl as ctrl',
          resolve: {
            country: function(request) {
              return request('main/country');
            },
            subscription: function() {
              return 'community';
            },
            productId: function(request) {
              return request({
                url: 'nregistration/productGroupIdToProductId',
                data: {
                  country: 'at',
                  productGroup: 'community'
                }
              });
            }
          }
        },
        {
          name: 'kgaRegistration',
          url: '/kga-registration?forward',
          templateUrl: template('shopRegistration'),
          controller: 'kgaRegistrationCtrl as ctrl',
          resolve: {
            country: function(request) {
              return request('main/country');
            },
            subscription: function() {
              return 'community';
            },
            productId: function(request) {
              return request({
                url: 'nregistration/productGroupIdToProductId',
                data: {
                  country: 'at',
                  productGroup: 'community'
                }
              });
            }
          }
        },
        {
          name: 'loginOrTest',
          url: '/login-or-test?forward',
          templateUrl: template('loginOrTestCommunity'),
          controller: 'loginOrTestCommunityCtrl as ctrl',
          resolve: {
            country: function(request) {
              return request('main/country');
            },
            subscription: function() {
              return 'test';
            },
            productId: function(request) {
              return request({
                url: 'nregistration/productGroupIdToProductId',
                data: {
                  country: 'at',
                  productGroup: 'test'
                }
              });
            }
          }
        },
        {
          name: 'testRequired',
          url: '/test-required?forward',
          templateUrl: template('loginOrTestCommunity'),
          controller: 'loginOrTestCommunityCtrl as ctrl',
          resolve: {
            country: function(request) {
              return request('main/country');
            },
            subscription: function() {
              return 'testOnly';
            },
            productId: function(request) {
              return request({
                url: 'nregistration/productGroupIdToProductId',
                data: {
                  country: 'at',
                  productGroup: 'test'
                }
              });
            }
          }
        },
        {
          name: 'testUpgrade',
          url: '/test-upgrade?forward',
          templateUrl: template('testUpgrade'),
          controller: 'fullCtrl as ctrl',
          resolve: {
            price: function(request) {
              return request('nregistration/cheapestPrice');
            },
            expiredType: function() {
              return 'none';
            },
            withLogin: function() {
              return false;
            }
          }
        },
        {
          name: 'fullExpired',
          url: '/full-expired?forward',
          templateUrl: template('fullRequired'),
          controller: 'fullCtrl as ctrl',
          resolve: {
            price: function(request) {
              return request('nregistration/cheapestPrice');
            },
            expiredType: function() {
              return 'full';
            },
            withLogin: function() {
              return false;
            }
          }
        },
        {
          name: 'testExpired',
          url: '/test-expired?forward',
          templateUrl: template('fullRequired'),
          controller: 'fullCtrl as ctrl',
          resolve: {
            price: function(request) {
              return request('nregistration/cheapestPrice');
            },
            expiredType: function() {
              return 'test';
            },
            withLogin: function() {
              return false;
            }
          }
        },
        {
          name: 'fullUpgrade',
          url: '/full-upgrade?forward',
          templateUrl: template('fullUpgrade'),
          controller: 'fullCtrl as ctrl',
          resolve: {
            price: function(request) {
              return request('nregistration/cheapestPrice');
            },
            expiredType: function() {
              return 'test';
            },
            withLogin: function() {
              return false;
            }
          }
        },
        {
          name: 'fullRequired',
          url: '/full-required?forward',
          templateUrl: template('fullRequired'),
          controller: 'fullCtrl as ctrl',
          resolve: {
            price: function(request) {
              return request('nregistration/cheapestPrice');
            },
            expiredType: function() {
              return 'none';
            },
            withLogin: function() {
              return false;
            }
          }
        },
        {
          name: 'loginOrFull',
          url: '/login-or-full?forward',
          templateUrl: template('loginOrFull'),
          controller: 'fullCtrl as ctrl',
          resolve: {
            price: function(request) {
              return request('nregistration/cheapestPrice');
            },
            expiredType: function() {
              return 'none';
            },
            withLogin: function() {
              return true;
            }
          }
        },
        {
          name: 'adminLogin',
          url: '/admin-login',
          templateUrl: template('admin-login')
        },
        {
          name: 'invoiceOpen',
          url: '/invoice-open?forward',
          templateUrl: template('invoiceOpen'),
          controller: 'invoiceOpenCtrl as ctrl'
        }
      ]
    });
  });
}
