import * as _ from 'lodash';

export default class QuickRegister {
  constructor(private request) {}

  register(data, type, final) {
    let url = 'nregistration/finish' + _.capitalize(type);
    return this.request({ url: url, data: data }, final);
  }
}
