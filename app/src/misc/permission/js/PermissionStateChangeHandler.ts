import PermissionHandlerRegistry from './handlers/PermissionHandlerRegistry';
import MemberLoader from './MemberLoader';
import { IHookRegistry, StateService, TargetState, Transition, State } from 'angular-ui-router';
import IdeaRedirectService from '../../js/services/IdeaRedirectService';

export default class PermissionStateChangeHandler {
  constructor(
    private $transitions: IHookRegistry,
    private permissionHandlerRegistry: PermissionHandlerRegistry,
    private $state: StateService,
    private $q: ng.IQService,
    private memberLoader: MemberLoader,
    private ideaRedirectService: IdeaRedirectService
  ) {}

  start(): void {
    this.$transitions.onBefore({}, transition => {
      return new Promise((resolve, reject) => {
        this.memberLoader.getOrLoad().then(() => resolve(this.handleTransition(transition)));
      });
    });
    this.$transitions.onSuccess({}, transition => {
      this.ideaRedirectService.setFirstRequestOver();
    });
  }

  private handleTransition(transition: Transition): boolean | TargetState {
    let toState = transition.$to();
    let results = this.getDeniedResults(this.collectPermissions(toState), transition);
    if (results.length > 0) {
      return results[0];
    } else {
      return true;
    }
  }

  private collectPermissions(toState: State): Array<string> {
    let currentPermissions = [];
    if (toState.params['permissions']) {
      currentPermissions = toState.params['permissions'].value();
    }

    if (toState.parent) {
      return currentPermissions.concat(this.collectPermissions(toState.parent));
    } else {
      return currentPermissions;
    }
  }

  private getDeniedResults(
    permissions: Array<string>,
    transition: Transition
  ): Array<boolean | TargetState> {
    return _(permissions)
      .map(permission => this.getResult(permission, transition))
      .filter((result: boolean | TargetState) => result !== true)
      .value();
  }

  private getResult(permission: string, transition: Transition): boolean | TargetState {
    if (this.permissionHandlerRegistry.hasHandler(permission)) {
      return this.permissionHandlerRegistry.getHandler(permission).handle(transition);
    } else {
      console.error('Unknown permission ' + permission + '. Allowing transition.');
      return true;
    }
  }
}
