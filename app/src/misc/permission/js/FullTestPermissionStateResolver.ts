export default function fullTestResolvePermissionDenied(member) {
  let memberData = member.getAll();
  let subscription = memberData.subscription;
  let returner = 'root.permission.';
  if (memberData.isAnonymous) {
    return 'root.login';
  } else {
    if (subscription.group === 'community') {
      return returner + 'fullRequired';
    } else if (subscription.group === 'test' && subscription.isExpired) {
      return returner + 'testExpired';
    } else if (subscription.isCancelled) {
      return returner + 'fullExpired';
    } else if (subscription.isLocked) {
      return returner + 'invoiceOpen';
    } else if (subscription.group === 'test') {
      return returner + 'fullUpgrade';
    }
  }
  return 'root.login';
}
