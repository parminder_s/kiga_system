/**
 * Created by peter on 12.06.17.
 */
export default class InvoiceOpenCtrl {
  constructor(private $window, private goHome, private links, private $state) {}

  payNow() {
    if (this.links.findByType('userMenu').isLegacy) {
      console.log('paying now..: ' + this.links.findByType('userMenu').url);
      window.location.href = this.links.findByType('userMenu').url;
    } else {
      this.$state.go('root.member.account', {});
    }
  }

  back() {
    this.goHome();
  }
}
