import { IConfigService } from '../../../js/services/AngularInterfaces';
import JobOptionsService from '../../../js/services/JobOptionsService';

export default class ShopRegistrationController {
  data: any;
  countries: any;
  state: string;
  subscriptionCode: string;
  jobs: any;

  private linkURL;

  constructor(
    private $state,
    private $window,
    private quickRegister,
    member,
    country,
    private subscription,
    private config: IConfigService,
    private productId,
    $translate,
    private jobOptionsService: JobOptionsService,
    private links,
    context
  ) {
    this.data = {
      context: context,
      email: member.email,
      email2: member.email,
      firstname: member.firstname,
      lastname: member.lastname,
      phone: '',
      gender: member.gender != null ? member.gender : 'female',
      newsletter: false,
      country: country.code,
      street: member.street,
      streetNumber: member.streetNumber,
      floorNumber: member.floorNumber,
      doorNumber: member.doorNumber,
      zip: member.zip,
      city: member.city,
      birthday: member.birthday
    };
    this.countries = country.values;

    this.jobs = _(this.jobOptionsService.jobOptions)
      .map(code => {
        return { title: $translate.instant('REG_JOB_' + code), code: code };
      })
      .valueOf();

    this.subscriptionCode = subscription;
    this.productId = productId;
    this.state = $state.current.name;
    this.linkURL = links.findByCode('dataPrivacy').url;
  }

  allowCountrySelection() {
    return this.config.get('allowCountrySelection');
  }

  register() {
    this.quickRegister.register(this.data, this.subscriptionCode, () =>
      this.$state.go('root.member.welcome.' + this.subscriptionCode)
    );
  }

  cancel() {
    return this.$window.history.back();
  }
}
