import * as _ from 'lodash';
import JobOptionsService from '../../../../misc/js/services/JobOptionsService';
import { IConfigService } from '../../../js/services/AngularInterfaces';

export default class LoginOrTestCommunityController {
  data: any;
  countries: any;
  forward: string;
  state: string;
  subscriptionCode: string;
  jobs: any;
  fullTest: boolean;

  private linkURL;

  constructor(
    private $state,
    $stateParams,
    private $window,
    request,
    private quickRegister,
    formHelper,
    member,
    country,
    private subscription,
    private config: IConfigService,
    private productId,
    $translate,
    private jobOptionsService: JobOptionsService,
    private links
  ) {
    this.fullTest = config.getPermissionMode() === 'fullTest';

    this.data = {
      email: member.email,
      email2: member.email,
      firstname: member.firstname,
      lastname: member.lastname,
      phone: '',
      gender: member.gender != null ? member.gender : 'female',
      newsletter: false,
      country: country.code,
      street: member.street,
      streetNumber: member.streetNumber,
      floorNumber: member.floorNumber,
      doorNumber: member.doorNumber,
      zip: member.zip,
      city: member.city,
      birthday: member.birthday
    };
    this.countries = country.values;

    this.jobs = _(this.jobOptionsService.jobOptions)
      .map(code => {
        return { title: $translate.instant('REG_JOB_' + code), code: code };
      })
      .valueOf();

    if (subscription === 'testOnly') {
      this.subscriptionCode = 'test';
    } else {
      this.subscriptionCode = subscription;
    }

    this.productId = productId;
    this.state = $state.current.name;
    this.forward = $stateParams.forward;
    this.linkURL = links.findByCode('dataPrivacy').url;
  }

  allowCountrySelection() {
    return this.config.get('allowCountrySelection');
  }

  isTestOnly() {
    return this.subscription === 'testOnly';
  }

  register() {
    this.quickRegister.register(this.data, this.subscriptionCode, () =>
      this.$state.go('root.member.welcome.' + this.subscriptionCode)
    );
  }

  cancel() {
    return this.$window.history.back();
  }
}
