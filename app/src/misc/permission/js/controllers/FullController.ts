/**
 * Created by rainerh on 27.11.16.
 */
export default class FullController {
  price: any;
  forward: string;
  state: string;
  duration: () => string;

  constructor(
    private $filter,
    price,
    private request,
    formHelper,
    private expiredType,
    private withLogin,
    private $state,
    $stateParams,
    private $window
  ) {
    this.price = $filter('currency')(price.price, price.currency);
    this.duration = () => {
      let key = 'MISC_' + (price.duration === 1 ? 'MONTH_SINGULAR' : 'MONTH_PLURAL');
      return price.duration + ' ' + this.$filter('translate')(key);
    };
    this.forward = $stateParams.forward;
    this.state = this.$state.current.name;
  }

  cancel() {
    this.$window.history.back();
  }

  submit() {
    this.$state.go('root.registration.preSelection');
  }

  changeToTest() {
    this.request(
      {
        url: 'nregistration/changeToTest'
      },
      () => (window.location.href = decodeURIComponent(this.forward))
    );
  }
}
