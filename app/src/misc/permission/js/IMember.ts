interface MemberMessage {
  Body: string;
  ButtonCancel: string;
  ButtonOK: string;
  Header: string;
  ID: string;
  Redirect: string;
  Type: string;
}

interface MemberKga {
  isActive: boolean;
}

interface IMember {
  email: string;
  hasFullLogin: boolean;
  isCms: boolean;
  isAnonymous: boolean;
  kga: MemberKga;
  locale: string;
  messages: Array<MemberMessage>;
  nickname: string;
  shopItems: number;
  subscription: any;
  s3Avatar: string;

  isLoggedIn(): boolean;
  hasTestSubscription(): boolean;
  hasActiveTestSubscription(): boolean;
  hasStandardSubscription(): boolean;
  hasExpiredSubscription(): boolean;
  hasFullSubscription(): boolean;
  hasActiveFullSubscription(): boolean;
}

export default IMember;
