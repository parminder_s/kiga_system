import * as angular from 'angular';
import PercentageController from './components/percentage/PercentageController';
import KigaInputController from './components/kigainput/KigaInputController';
import KigaLabelController from './components/kigalabel/KigaLabelController';
import KigaTextAreaController from './components/kigatextarea/KigaTextAreaController';
import KigaMultipleCheckboxController from './components/kigamultiplecheckbox/KigaMultipleCheckboxController';
import KigaRadioButtonController from './components/kigaradiobutton/KigaRadioButtonController';
import KigaButtonFormGroupController from './components/kigabuttongroups/kigabuttonformgroup/KigaButtonFormGroupController';
import KigaButtonController from './components/kigabutton/KigaButtonController';
import KigaButtonsController from './components/kigabuttongroups/kigabuttonsgroup/KigaButtonsController';
import KigaMultipleInputsController from './components/kigamultipleinputs/KigaMultipleInputsController';
import KigaSelectionController from './components/kigaselection/KigaSelectionController';
import KigaBirthday from './components/kigabirthday/KigaBirthday';
import KigaCheckboxController from './components/kigacheckbox/KigaCheckboxController';

angular
  .module('kiga.form', [])
  .component('kigaInputPercentage', {
    bindings: {
      value: '=?',
      label: '@'
    },
    controller: function() {
      return new PercentageController();
    },
    templateUrl: 'misc/form/components/percentage/percentage.html'
  })

  /*
  * inputType is an optional attribute, the default value for it is text.
  * value = ng-model
  * value-options = ng-model-options
  * inputClass is also optional -> by default class="form-control"
  * inputName is also an optional attribute
  * pattern is an optional attribute (ng-pattern) for pattern validation
  * If you have a pattern you MUST use the validation-message-pattern attribute
  *
  * validation = "required" for required input field
  * validation = "required && disable" for required input field but disabled validation
  *
  * validation = "async-validator" for async validator
  * asyncFunction -> async-fn
  * asyncMessage -> async-msg
  *
  * validation = "async-validation/emailExists": -> async-validation = "emailExists"
  * validation="required && async-validation/emailExists"> -> async-validation = "emailExists"
  *                                                           and required input
  * async-validation-url -> async-url
  * asyncContext -> async-validation-context
  *
  * validation-message-pattern -> validation-msg-pattern
  * */

  .component('kigaInput', {
    bindings: {
      label: '@?',
      placeHolder: '@?',
      value: '=?',
      inputType: '@?',
      validation: '@?',
      valueOptions: '<?',
      inputClass: '@?',
      inputName: '@?',
      pattern: '@?',
      asyncFunction: '<?',
      asyncMessage: '@?',
      asyncValidationUrl: '@?',
      asyncContext: '@?',
      validationMessagePattern: '@?',
      onChange: '&?'
    },
    controller: function() {
      return new KigaInputController();
    },
    templateUrl: 'misc/form/components/kigainput/kigainput.html'
  })

  /*
  * value = ng-model
  * large is a boolean and optional -> true/ false for large label or not
  * */
  .component('kigaLabel', {
    bindings: {
      label: '@?',
      labelContent: '@?',
      value: '=?',
      large: '<?',
      placeHolder: '@?'
    },
    controller: function() {
      return new KigaLabelController();
    },
    templateUrl: 'misc/form/components/kigalabel/kigalabel.html'
  })

  /*
  * value = ng-model
  * */
  .component('kigaTextArea', {
    bindings: {
      value: '=?',
      label: '@?'
    },
    controller: function() {
      return new KigaTextAreaController();
    },
    templateUrl: 'misc/form/components/kigatextarea/kigatextarea.html'
  })

  .component('kigaCheckbox', {
    bindings: {
      value: '=',
      checkboxName: '@?',
      trueValue: '@?',
      falseValue: '@?',
      uiValidation: '@?',
      msgValidator: '@?'
    },
    controller: function() {
      return new KigaCheckboxController();
    },
    templateUrl: 'misc/form/components/kigacheckbox/kigacheckbox.html'
  })

  /*
  * value = ng-model
  * alignment ="vertical" for vertical alignment -> by default horizontal
  * options use: {at: 'Österreich', de: 'Deutschland'} or [['at', 'Österreich'], ['ch', 'Schweiz']]
  * value="['at']" for setting a selected checkbox
  * */
  .component('kigaMultipleCheckbox', {
    bindings: {
      label: '@?',
      value: '=?',
      alignment: '@?',
      options: '<'
    },
    controller: function() {
      return new KigaMultipleCheckboxController();
    },
    templateUrl: 'misc/form/components/kigamultiplecheckbox/kigamultiplecheckbox.html'
  })

  /*
  * value = ng-model
  * alignment ="vertical" for vertical alignment -> by default horizontal
  * options use: {at: 'Österreich', de: 'Deutschland'} or [['at', 'Österreich'], ['ch', 'Schweiz']]
  * value="['at']" for setting a selected checkbox
  * */
  .component('kigaRadioButton', {
    bindings: {
      label: '@?',
      alignment: '@?',
      value: '=?',
      options: '<?',
      option1: '@?',
      option2: '@?',
      name: '@',
      translation: '@?'
    },
    controller: function($filter) {
      return new KigaRadioButtonController($filter);
    },
    templateUrl: 'misc/form/components/kigaradiobutton/kigaradiobutton.html'
  })

  .component('kigaButtonFormGroup', {
    bindings: {
      label: '@'
    },
    controller: function() {
      return new KigaButtonFormGroupController();
    },
    templateUrl:
      'misc/form/components/kigabuttongroups/kigabuttonformgroup/' + 'kigabuttonformgroup.html'
  })

  /*
  * value = ng-model
  * test-data -> is data-test
  * button-click -> is ng-click
  * */
  .component('kigaButton', {
    bindings: {
      buttonText: '@?',
      buttonType: '@?',
      buttonClass: '@',
      buttonHref: '@?',
      buttonClick: '&',
      value: '=?',
      translatedText: '@?'
    },
    controller: function() {
      return new KigaButtonController();
    },
    templateUrl: 'misc/form/components/kigabutton/kigabutton.html'
  })

  /*
  * firstValue -> ng model for first input field
  * secondValue -> ng model for second input field
  * thirdValue -> ng model for third input field
  * threeFields is optional;
  *   -> set to false at default (so 2 fields at default)
  *   -> you can set three input fields: three-fields="true"
  *  asyncMsgEmailExists -> validation-msg-async-validationemail-exists
  *  minLength="8/8/8" or minLength="8/8" or minLength="8"
  * */
  .component('kigaMultipleInputs', {
    bindings: {
      label: '@?',
      placeHolder1: '@?',
      placeHolder2: '@?',
      placeHolder3: '@?',
      validation: '@?',
      fabMatch: '<?',
      firstValue: '=?',
      secondValue: '=?',
      thirdValue: '=?',
      threeFields: '@?',
      valueOptions: '<?',
      inputType: '@?',
      inputNames: '<?',
      fabMatchMsg: '@?',
      asyncValidationUrl: '@?',
      asyncMsgEmailExists: '@?',
      asyncContext: '@?',
      minLength: '@?'
    },
    controller: function() {
      return new KigaMultipleInputsController();
    },
    templateUrl: 'misc/form/components/kigamultipleinputs/kigamultipleinputs.html'
  })

  .component('kigaButtonsGroup', {
    controller: function() {
      return new KigaButtonsController();
    },
    templateUrl: 'misc/form/components/kigabuttongroups/kigabuttonsgroup/kigabuttons.html'
  })

  /*
  * value = ng-model
  * validation = "required" for required selection field
  * options use: {'hu': 'Lapbag Huhn', 'hun': 'Lapbag Hund' }
  *           or [{title: 'Apfel', code: 'AQ'}, {title: 'Melone', code: 'AR'}]
  * */
  .component('kigaSelection', {
    bindings: {
      inputName: '@?',
      label: '@?',
      placeHolder: '@?',
      test: '@?',
      options: '=',
      onSelect: '&?',
      value: '=?',
      validation: '@?',
      selectionClass: '@?',
      translation: '@?'
    },
    controller: function($filter) {
      return new KigaSelectionController($filter);
    },
    templateUrl: 'misc/form/components/kigaselection/kigaselection.html'
  })

  .component('kigaBirthday', {
    bindings: {
      name: '@',
      value: '='
    },
    controller: () => new KigaBirthday(),
    templateUrl: 'misc/form/components/kigabirthday/kigabirthday.html'
  });
