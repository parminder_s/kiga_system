export default class KigaMultipleCheckboxController {
  private label: string;
  private alignment: string;
  private horizontal: boolean;
  private value: string[] = [];
  private options = {};
  private list = [];

  $onInit() {
    // for setting checkbox alignment to horizontal (by default) or vertical
    if (this.alignment === 'horizontal' || this.alignment === undefined) {
      this.horizontal = true;
    } else if (this.alignment === 'vertical') {
      this.horizontal = false;
    }

    // pushing options to own array to show in view
    for (let key in this.options) {
      let optionsIsArray = Array.isArray(this.options);
      if (optionsIsArray) {
        this.list.push({ key: this.options[key][0], name: this.options[key][1], value: false });
      } else {
        this.list.push({ key: key, name: this.options[key], value: false });
      }
    }

    // for setting the corresponding checkbox as selected if stated
    for (let listEntry in this.list) {
      for (let selectedEntry in this.value) {
        if (this.list[listEntry].key === this.value[selectedEntry]) {
          this.list[listEntry].value = true;
        }
      }
    }
  }
}
