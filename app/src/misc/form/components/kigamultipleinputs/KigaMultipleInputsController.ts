export default class KigaMultipleInputsController {
  private label: string;
  private firstValue: string;
  private secondValue: string;
  private thirdValue: string;
  private validation = '';
  private fabMatch: string;
  private placeHolder1: string;
  private placeHolder2: string;
  private placeHolder3: string;
  private inputNames = {
    name1: '',
    name2: '',
    name3: ''
  };
  private threeFields: string;
  private inputType = '';
  private valueOptions = {};
  private fabMatchMsg: string;
  private asyncValidationUrl: string;
  private asyncMsgEmailExists: string;
  private asyncContext: string;
  private minLength = '';

  private setThreeFields: boolean;
  private is_required: boolean;
  private asyncValidation = false;
  private async_validation_attributes: string[];
  private length1 = 0;
  private length2 = 0;
  private length3 = 0;

  $onInit() {
    this.setThreeFields = this.threeFields === 'true';

    // min-length for individual fields:
    let tmp_mins = this.minLength.split('/');
    let lengths = tmp_mins.length;
    if (lengths === 3) {
      this.length1 = Number(tmp_mins[0]);
      this.length2 = Number(tmp_mins[1]);
      this.length3 = Number(tmp_mins[2]);
    } else if (lengths === 2) {
      this.length1 = Number(tmp_mins[0]);
      this.length2 = Number(tmp_mins[1]);
    } else if (lengths === 1) {
      this.length1 = Number(tmp_mins[0]);
    }

    // default value of type="text"
    if (!this.inputType) this.inputType = 'text';

    // does validation contain required
    if (this.validation.includes('required')) this.is_required = true;

    // does validation contain async-validation
    if (this.validation.includes('async-validation')) {
      this.asyncValidation = true;
      this.async_validation_attributes = this.validation.split('/');
    }

    // default value of ng-model-options = {updateOnDefault: "true"}
    if (_.isEmpty(this.valueOptions) && this.inputType === 'text')
      this.valueOptions = { updateOnDefault: 'true' };
  }
}
