import * as _ from 'lodash';
import { InputChangeCallbackObject } from '../../interfaces';

export default class KigaInputController {
  private label = '';
  private placeHolder: string;
  private value: string;
  private inputType = '';
  private validation = '';
  private pattern: string;
  private valueOptions = {};
  private inputClass = '';
  private inputName = '';
  private asyncFunction: any;
  private asyncMessage: string;
  private asyncValidationUrl: string;
  private asyncContext: any;
  private validationMessagePattern: string;
  private onChange: (value: InputChangeCallbackObject) => void;

  private isRequired: boolean;
  private noFarbFormValidation: boolean;
  private asyncValidation = false;
  private asyncValidationAttributes: string[];
  private asyncValidator: boolean;
  private formattedPattern: string;

  public constructor() {}

  public $onInit() {
    this.noFarbFormValidation = this.validation === 'disable';
    this.asyncValidator = this.validation === 'async-validator';

    // does validation contain async-validation
    if (this.validation.includes('async-validation')) {
      this.asyncValidation = true;
      this.asyncValidationAttributes = this.validation.split('/');
    }

    // does validation contain required
    if (this.validation.includes('required')) {
      this.isRequired = true;
    }

    // default value of type="text"
    if (!this.inputType) {
      this.inputType = 'text';
    }

    // default value of ng-model-options = {updateOnDefault: "true"}
    if (_.isEmpty(this.valueOptions) && this.inputType === 'text') {
      this.valueOptions = { updateOnDefault: 'true' };
    }

    // default value of class="form-control"
    if (!this.inputClass) {
      this.inputClass = 'form-control';
    }

    // remove slashes from pattern input
    if (this.pattern) {
      this.formattedPattern = this.pattern.replace(/\//g, '');
    }
  }

  public change() {
    if (this.onChange) {
      this.onChange({ value: this.value });
    }
  }
}
