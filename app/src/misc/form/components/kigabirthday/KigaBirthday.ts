import * as moment from 'moment';
import { SelectOption } from '../kigaselection/KigaSelectionController';
import { Moment } from 'moment';

interface DateParts {
  day: number;
  month: number;
  year: number;
}

export default class KigaBirthday {
  private name: string;
  private months: any;
  private monthSelectOption: SelectOption;
  private value: Date;
  private momentValue: Moment;
  private dateParts: DateParts;

  public constructor() {}

  public $onInit() {
    this.months = [
      'MISC_JANUARY',
      'MISC_FEBRUARY',
      'MISC_MARCH',
      'MISC_APRIL',
      'MISC_MAY',
      'MISC_JUNE',
      'MISC_JULY',
      'MISC_AUGUST',
      'MISC_SEPTEMBER',
      'MISC_OCTOBER',
      'MISC_NOVEMBER',
      'MISC_DECEMBER'
    ].map((month, idx) => ({
      name: month,
      idx: idx
    }));

    if (this.value) {
      this.momentValue = moment(this.value);
      this.dateParts = {
        day: this.momentValue.date(),
        month: this.momentValue.month(),
        year: this.momentValue.year()
      };
    } else {
      this.dateParts = {
        day: null,
        month: this.months[0].idx,
        year: null
      };
    }
    this.monthSelectOption = _.find(
      this.months,
      selectOption => selectOption.idx === this.dateParts.month
    );
  }

  public setViewValueIfDateIsValid() {
    if (
      this.dateParts.day === null ||
      this.dateParts.year === null ||
      isNaN(this.dateParts.day) ||
      isNaN(this.dateParts.year)
    ) {
      this.value = null;
    } else {
      const tmpValue = moment(this.dateParts);
      if (this.checkIfDateIsValid(tmpValue)) {
        this.momentValue = tmpValue;
        this.value = this.momentValue.toDate();
      } else {
        this.value = null;
      }
    }
  }

  public isInputNumber(value): boolean {
    return angular.isNumber(value);
  }

  public checkIfDateIsValid(date: Moment): boolean {
    return date.isValid() && date.year() > 1900 && date.year() <= 2100;
  }

  public changeYear(year: string) {
    const yearInNumber = parseInt(year);
    if (this.isInputNumber(yearInNumber)) {
      this.dateParts.year = parseInt(year);
      this.setViewValueIfDateIsValid();
    }
  }

  public changeDay(day: string) {
    const dayInNumber = parseInt(day);
    if (this.isInputNumber(dayInNumber)) {
      this.dateParts.day = dayInNumber;
      this.setViewValueIfDateIsValid();
    }
  }

  public changeMonth(month: string) {
    this.dateParts.month = parseInt(month);
    this.setViewValueIfDateIsValid();
  }
}
