export default class KigaButtonController {
  private buttonText: string;
  private buttonType: string;
  private buttonClass: string;
  private buttonHref = '';
  private value: any;
  private buttonClick: () => void;
  private translatedText: string;

  private buttonIsIcon: boolean;
  private buttonIsLink: boolean;

  $onInit() {
    this.buttonIsIcon = this.buttonClass === 'icon';
    this.buttonIsLink = this.buttonHref !== '';
  }
}
