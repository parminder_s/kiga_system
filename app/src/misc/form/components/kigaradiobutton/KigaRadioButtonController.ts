export default class KigaRadioButtonController {
  private label: string;
  private value: string;
  private alignment: string;
  private horizontal: boolean;
  private options: any;
  private radioOptionsList = [];
  private name: string;
  private translation: string;

  constructor(private $filter) {}

  $onInit() {
    // for setting radio button alignment to horizontal (by default) or vertical
    if (this.alignment === 'horizontal' || this.alignment === undefined) {
      this.horizontal = true;
    } else if (this.alignment === 'vertical') {
      this.horizontal = false;
    }

    for (let key in this.options) {
      const optionsIsArray = Array.isArray(this.options);
      if (optionsIsArray && this.translation === 'true') {
        this.radioOptionsList.push({
          key: this.options[key][0],
          name: this.$filter('translate')(this.options[key][1])
        });
      } else if (optionsIsArray && !this.translation) {
        this.radioOptionsList.push({ key: this.options[key][0], name: this.options[key][1] });
      } else if (!optionsIsArray && this.translation === 'true') {
        this.radioOptionsList.push({
          key: key,
          name: this.$filter('translate')(this.options[key])
        });
      } else if (!optionsIsArray && !this.translation) {
        this.radioOptionsList.push({ key: key, name: this.options[key] });
      }
    }
  }
}
