export default class PercentageController {
  value: number;
  formValue: number;

  $onInit() {
    this.formValue = this.value * 100;
  }

  onChange() {
    this.value = this.formValue / 100;
  }
}
