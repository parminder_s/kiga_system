import { InputChangeCallbackObject } from '../../interfaces';

export interface SelectOption {
  name: string;
  idx: number;
}

export default class KigaSelectionController {
  private label: string;
  private placeHolder: string;
  private options: any;
  private value: any;
  private validation: string;
  private isRequired: boolean;
  private inputName = '';
  private selectionClass: string;
  private formattedInput: Array<SelectOption>;
  private translation = 'false';

  private onSelect: (value: InputChangeCallbackObject) => void;

  constructor(private $filter) {}

  public $onInit() {
    let tmpInput: Array<SelectOption>;
    this.isRequired = this.validation === 'required';

    // default value of class="form-group"
    if (!this.selectionClass) {
      this.selectionClass = 'form-group';
    }

    // to format input that objects always consist of name and idx: [{"name": "", "idx": ""}, {..}..]
    tmpInput = this.formatInputToNameAndIdx(this.options);
    if (this.translation === 'true') {
      this.translateInput(tmpInput);
    } else {
      this.formattedInput = tmpInput;
    }
  }

  private formatInputToNameAndIdx(inputOptions: any): Array<SelectOption> {
    let outputArray = [];
    const inputIsArray = Array.isArray(inputOptions);
    if (!inputIsArray) {
      outputArray = _.map(inputOptions, (key: string, value: number) => {
        return { name: key, idx: value };
      });
    } else if (inputIsArray && JSON.stringify(inputOptions).includes('title')) {
      const allValues = _.map(inputOptions, 'title');
      const allKeys = _.map(inputOptions, 'code');
      allValues.forEach((item, i) => {
        outputArray.push({ name: allValues[i], idx: allKeys[i] });
      });
    } else {
      outputArray = inputOptions;
    }
    return outputArray;
  }

  private translateInput(input: Array<SelectOption>) {
    // for translating the placeholder if needed
    this.placeHolder = this.$filter('translate')(this.placeHolder);
    input.forEach(element => {
      element.name = this.$filter('translate')(element.name);
    });
    this.formattedInput = input;
  }

  public select() {
    if (this.onSelect) {
      this.onSelect({ value: this.value.toString() });
    }
  }
}
