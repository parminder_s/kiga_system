export default class KigaLinkCtrl {
  tag: string;
  url: string;
  articleId: string;
  title: string;
  origin: string;

  constructor(private endpoint) {}

  public submit() {
    let url = 'log/logLink';
    if (this.tag === 'print') {
      url = 'log/logPrint';
    }

    this.endpoint({
      method: 'POST',
      quietMode: true,
      url: url,
      data: {
        articleId: this.articleId,
        title: this.title,
        origin: this.origin,
        tag: this.tag,
        url: this.url
      }
    });
  }
}
