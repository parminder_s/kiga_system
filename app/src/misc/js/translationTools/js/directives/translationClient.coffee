###*

  @ngdoc directive
  @name translationClient
  @module misc
  @description

  A custom directive which makes a 'translateable' word to be wrapped around
  by a special element. This element shows the translation information of a
  translateable word in the <b>contexmenu</b> - right click event.

  It does not require any parameter to pass in it, rather, it requires to
  add 3 more attr - <b>status</b>, <b>key</b> and <b>value</b> and all
  of these attr accept only string values.

  This directive is not used in the html partials directly. The
  <b>translationTranformer</b> service injects the directive on
  <b>translationModeHandler.enable()</b> where a translateable word exist.

  @example
  '`<span translation-client=\"\" status=\"translated\"
    key=\"ADDRESS\" value=\"Adresse\"></span>`
###


define ['../../../app', 'angular', 'lodash'], (app, angular, _) ->
  app.directive 'translationClient', ($compile, popupHandler, translatedWord, occurrenceService) ->
    restrict: 'A'
    replace: true
    terminal: true
    transclude: true
    #  priority: 1000
    scope:
      key: '@key'
      value: '@value'
      status: '@status'

    template: "<span ng-bind='value'></span>"

    controller: ['$scope', '$q'].concat ($scope, $q) ->
      $scope.translationPopup = ()->
        $scope.key = $scope.key.trim()
        $scope.word = translatedWord.findByKey($scope.key)
        $scope.occurrences = []
        occurrenceService.search $scope.key, (result)->
          $q.when(result).then (data)->
            popupHandler $scope.word, data, {}
      return


    link: (scope, element, attr)->
      element.addClass scope.status
      element.attr
        key: scope.key.trim()
      element.removeAttr "value status"
      element.bind 'contextmenu', (event) ->
        scope.$apply ->
          event.preventDefault()
          scope.translationPopup()
          return
        return
      return
