###*

  @ngdoc service
  @name translatedData
  @module misc

  @description
  This service works as a data repository. It stores the transformed data
  from translationTransformer and serves these data to both front-end
  and back-end translation tools.

  @requires
  request, requestPromise

###

define ['../../../app'], (app) ->
  app.service 'translatedData', (request)->
    new class Model
      allWords: {}

      update: (word)->
        @allWords[word.key] = word

      isSet: ->
        @allWords.length > 0

      set: (data)->
        if @isSet() is true
          @allWords
        else @allWords = data

      findByKey: (key)->
        @allWords[key]

      get: ()->
        @allWords

      save: (word, cb)->
        request
          url: "translation/update"
          data:
            'word': word
        .then (result) =>
          @update result
          if cb
            cb result
          else
            result
