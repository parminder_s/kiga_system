define [
  '../../../app'
], (app) ->
  app.service 'historyService', (request) ->
    getAll: (key, cb) ->
      request
        url: "translation/getHistory"
        data:
          translationKey: key
      .then (locales) ->
        if cb
          cb locales
        else
          locales
