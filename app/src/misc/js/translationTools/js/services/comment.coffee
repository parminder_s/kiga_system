define [
  '../../../app'
], (app) ->
  app.service 'commentService', (request) ->
    getAll: (key, cb) ->
      request
        url: "translation/getComment"
        data:
          translationKey: key
      .then (locales) ->
        if cb
          cb locales
        else
          locales
