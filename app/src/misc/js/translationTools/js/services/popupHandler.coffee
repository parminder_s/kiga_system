###*

  @ngdoc service
  @name popupHandler
  @module misc
  @description
  This service shows a popup on contextmenu i.e. right-click event on a
  translate-able word.
  The popup shows all the information regarding the translation of a word.

  This handler is only used by the front-end translationTools.

  Before showing any popup, it dismisses all other popup that is shown
  previously.

  @requires
  $uibModal, $uibModalStack

###

define ['../../../app', 'angular', 'lodash'], (app, angular, _) ->
  app.service 'popupHandler',
  ($uibModal, $uibModalStack, historyService, commentService) ->
    (word, occurrences, cb) ->
      $uibModalStack.dismissAll()
      $uibModal.open
        templateUrl: 'misc/js/translationTools/partial/popup.html'
        windowClass: 'translation-popup'
        resolve:
          allHistories: ['historyService', (historyService) ->
            historyService.getAll word.key
          ]
          allComments: ['commentService', (commentService) ->
            commentService.getAll word.key
          ]
          word : ()->
            word
          occurrences : ()->
            occurrences
        controller: 'translationModalCtrl as ctrl'
