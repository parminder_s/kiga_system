###*

  @ngdoc service
  @name translatedData
  @module misc
  @description
  This service works as a data model. All the translation data is transformed
  into this model  and it is used by front-end and back-end translation tools.

  @requires
  translatedData

###

define ['../../../app'], (app) ->
  app.service 'translatedWord', (translatedData) ->
    new class Model
      getNew: ->
        new class Model
          version: 1
          key: ""
          translation: ""
          html: ""
          locale: ""
          status: ""
          category: []
          comments: []
          history: []
          occurrences:[]
          type: ""

          toString: ->
            @translation

      create: ()->
        tempWord = @getNew()
        tempWord.version = 0
        tempWord.key = arguments[0]
        tempWord.translation = arguments[1]
        tempWord.locale = arguments[2]
        tempWord.status = arguments[3]
        tempWord.comments = []
        tempWord.history = []
        occurrences:[]
        category: []
        tempWord

      findByKey: (key)->
        translatedData.findByKey key

      save: (word, cb)->
        translatedData.save(word, cb)
