###*

  @ngdoc service
  @name translationModeHandler
  @module misc
  @description
  This service enables or disables the translation tools for the
  front-end translation.

###

define ['../../../app', 'lodash'], (app, _) ->
  app.service 'translationModeHandler', ->
    new class Model
      @makeEnabled: false

      enable: ()->
        @makeEnabled = true

      disable: ()->
        @makeEnabled = false

      isEnabled: ()->
        @makeEnabled == true
