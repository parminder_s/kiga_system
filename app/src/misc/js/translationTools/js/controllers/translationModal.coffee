define ['../../../app', 'angular', 'lodash'], (app, angular, _) ->
  app.controller 'translationModalCtrl', ($uibModalInstance,
  $translate, translatedWord, allComments, allHistories, word, occurrences) ->
    new class Ctrl
      constructor: () ->
        @word = angular.copy word
        @occurrences = occurrences
        @editedWord = angular.copy word
        @allHistories = allHistories
        @allComments = allComments

      cancel: =>
        @word = angular.copy word
        @editedWord = angular.copy word
        @close()

      changeTranslation: =>
        translatedWord.save @editedWord, (updatedWord)=>
          $translate.refresh()
          @close()

      close: ->
        $uibModalInstance.dismiss()
