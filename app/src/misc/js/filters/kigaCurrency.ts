import { IFilterService } from 'angular';

export default function($filter: IFilterService) {
  return (input: number, symbol: string): string =>
    $filter('currency')(input, symbol).replace(/([^\d]*)(.*)$/, '$2 $1');
}
