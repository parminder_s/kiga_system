import url from '../../../url';
import IRequestConfig = angular.IRequestConfig;

export default class PartialInterceptor {
  request(config: IRequestConfig) {
    if (config.url.match(/partials/) || config.url.match(/components.*\.html$/)) {
      config.url = url(config.url);
    }
    return config;
  }
}
