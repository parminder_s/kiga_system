define ['../app', 'jquery'], (app, $) ->
  app.factory 'quickRegisterMock', ['misc.mockHelper'].concat (mock) ->

    mock "nregistration/finishTest", (data, url) ->
      console.log 'Testing the quickRegister'

  app.run ['quickRegisterMock'].concat ->
