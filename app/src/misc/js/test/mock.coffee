define ['../app', 'jquery', 'angular-mocks'], (app, $) ->
  app.requires.push 'ngMockE2E'

  app.factory 'miscMock', ['$httpBackend'].concat ($httpBackend) ->
    for locale in ['de', 'en', 'it', 'tr']
      $httpBackend.when('GET', "/#{locale}.json").respond {}

  app.factory 'kigaLoader', ['$http', 'translationTransformer', 'translationModeHandler', 'translatedWord'].
  concat ($http, translationTransformer, translationModeHandler, translatedWord) -> (options) ->
    isTranslationEnabled = translationModeHandler.isEnabled()
    returner = $http
      method: 'GET'
      url: "/#{options.key}.json"
      data:
        isTranslationEnabled: isTranslationEnabled
    returner.then (response) ->
      console.log("kigaLoader >> Mock", isTranslationEnabled)
      parse = (keys, data, translations) ->
        _.map data, (val, key) ->
          if $.type(val) is 'object'
            parse keys.concat(key), val, translations
          else
            fullKey = keys.concat(key).join '_'
            fullKey = fullKey.trim()
            if isTranslationEnabled is true
              translations[fullKey] = translatedWord.create fullKey, val, options.key, "translated"
            else
              translations[fullKey] = val

        translations
      data = parse [], response.data, {}
      translationTransformer data

  app.run ['miscMock'].concat ->
