###*
  @ngdoc service

  @name misc.translationTransformer

  @description

  It provides a complex {KEY : VALUE} pairs where values are objects of translatedWord.

  Replaces variables defined by '%...' with their predefined vales.
  Most of the time those are icons as images which can now be seen
  as deprecated since we are using an icon font.

  Further more this service adds support for aliases to the translation
  module, whereas an alias in a translation value starting with ':' and
  the key to the original translation.
###

define ['../app', 'lodash'], (app, _) ->
  app.service 'objectDataTransformer', ['transformerHelper', 'translatedData'].
  concat (transformerHelper, translatedData) -> (data) ->
    replaceBrandFree = (obj) ->
      obj.translation = transformerHelper.replaceBrandFree data, obj.translation
      obj

    replaceBrandDefault = (obj) ->
      obj.translation = transformerHelper.replaceBrandDefault data, obj.translation
      obj

    replaceMyPlan = (obj) ->
      obj.translation = transformerHelper.replaceMyPlan data, obj.translation
      obj

    replaceMyPlanInverted = (obj) ->
      obj.translation = transformerHelper.replaceMyPlanInverted obj.translation
      obj

    replaceKigaPlan = (obj) ->
      obj.translation = transformerHelper.replaceKigaPlan obj.translation
      obj

    handleAliases = (data, code) ->
      if data[code] and data[code].translation
        if data[code].translation.match "^:"
          data[code].translation = data[code].translation.substring 1
          handleAliases data, data[code].translation
        else data[code]

    setTranslation = (data) ->
      translatedData.set(data)
      returner = _.fromPairs _.map data, (val, key) ->
        [key.trim(), val.html]
      returner

    handleTranslation = (key, obj) ->
      if not obj.html or obj.html is null
        obj.html = "<span translation-client=\"\" status=\"#{obj.status}\" key=\"#{key}\" value=\"#{obj.translation}\"></span>"
      obj

    returner = _.fromPairs _.map data, (val, key) ->
      val = handleAliases data, key
      if val
        for fn in [replaceBrandFree, replaceBrandDefault,
                   replaceMyPlanInverted, replaceMyPlan, replaceKigaPlan]
          val = fn val
          val

        for fn in [handleTranslation]
          val = fn key, val

        # TODO : _.trim is not working
        [key.trim(), val]

      else [key.trim(), {}]

    setTranslation returner
