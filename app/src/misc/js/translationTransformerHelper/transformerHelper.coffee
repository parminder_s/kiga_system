###*
  @ngdoc service

  @name misc.transformerHelper

  @description

  Provides the common methods for replacing specific text.
  This common methods are used both in objectDataTransformer and jsonDataTransformer

###

define ['../app', 'lodash'], (app, _) ->
  app.service 'transformerHelper', ->
    replaceBrandFree = (data, text) ->
      name = data.BRAND_FREE
      title = "Kiga #{name}"
      imgSrc = "#{kiga.staticUrl}kiga/images/kigaSpotGreenInverse.png"
      replace = "
        <span class=\"freeBrand\">
        <img src=\"#{imgSrc}\" alt=\"#{title}\" title=\"#{title}\">#{name}</span>"
      text.replace /%brandFree/g, replace

    replaceBrandDefault = (data, text) ->
      name = data.BRAND_DEFAULT
      title = "Kiga #{name}"
      imgSrc = "#{kiga.staticUrl}kiga/images/kigaSpotInverse.png"
      replace = "
        <span class=\"defaultBrand\">
        <img src=\"#{imgSrc}\" alt=\"#{title}\" title=\"#{title}\">#{name}</span>"
      text.replace /%brandDefault/g, replace

    replaceMyPlan = (data, text) ->
      title = 'Kiga MyPlan'
      replace = """
        <strong class=\"ico-box\">
          <i class=\"ico meine\">&nbsp;</i>
        </strong>
        """
      text.replace /%myPlan/g, replace


    replaceMyPlanInverted = (text) ->
      title = 'Kiga MyPlan'
      replace = "<i class=\"ico meine\">&nbsp;</i>"
      text.replace /%myPlanInverted/g, replace

    replaceKigaPlan = (text) ->
      title = 'Kiga MyPlan'
      replace = """
        <strong class=\"ico-box\">
          <i class=\"ico kiga\">&nbsp;</i>
        </strong>
        """
      text.replace /%kigaPlan/g, replace

    replaceBrandFree: replaceBrandFree
    replaceBrandDefault: replaceBrandDefault
    replaceMyPlan: replaceMyPlan
    replaceMyPlanInverted: replaceMyPlanInverted
    replaceKigaPlan: replaceKigaPlan