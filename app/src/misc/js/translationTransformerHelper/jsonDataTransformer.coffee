###*
  @ngdoc service

  @name misc.jsonDataTransformer

  @description

  It provides simple {KEY : VALUE} pairs where values are plain text.

  Replaces variables defined by '%...' with their predefined vales.
  Most of the time those are icons as images which can now be seen
  as deprecated since we are using an icon font.

  Further more this service adds support for aliases to the translation
  module, whereas an alias in a translation value starting with ':' and
  the key to the original translation.
###

define ['../app', 'lodash'], (app, _) ->
  app.service 'jsonDataTransformer', ['transformerHelper'].concat (transformerHelper) -> (data) ->
    replaceBrandFree = (text) ->
      transformerHelper.replaceBrandFree data, text

    replaceBrandDefault = (text) ->
      transformerHelper.replaceBrandDefault data, text

    replaceMyPlan = (text) ->
      transformerHelper.replaceMyPlan data, text

    replaceMyPlanInverted = (text) ->
      transformerHelper.replaceMyPlanInverted text

    replaceKigaPlan = (text) ->
      transformerHelper.replaceKigaPlan text

    handleAliases = (data, code) ->
      text = data[code]
      if text
        if data[code].match "^:"
          handleAliases data, data[code].substring 1
        else data[code]

    returner = _.fromPairs _.map data, (val, key) ->
      val = handleAliases data, key
      if val
        for fn in [replaceBrandFree, replaceBrandDefault,
                   replaceMyPlanInverted, replaceMyPlan, replaceKigaPlan]
          val = fn val
          val

        [key.trim(), val]

      else [key.trim(), ""]
