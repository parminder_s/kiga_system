###*
  @ngdoc service

  @name main.locales

  @description
  holds the formatting data for the enabled locales
  english is empty since not alter of default $locale required
###

define ['../app'], (app) ->
  app.value 'locales',
    de:
      DATETIME_FORMATS:
        fullDate: "EEEE, d. MMMM y"
        longDate: "d. MMMM y"
        medium: "dd.MM.y HH:mm:ss"
        mediumDate: "dd.MM.y"
        mediumTime: "HH:mm:ss"
        short: "dd.MM.yy HH:mm"
        shortDate: "dd.MM.yy"
        shortTime: "HH:mm"
      NUMBER_FORMATS:
        DECIMAL_SEP: ','
        GROUP_SEP: '.'
    it:
      DATETIME_FORMATS:
        fullDate: "EEEE d MMMM y"
        longDate: "dd MMMM y"
        medium: "dd/MMM/y HH:mm:ss"
        mediumDate: "dd/MMM/y"
        mediumTime: "HH:mm:ss"
        short: "dd/MM/yy HH:mm"
        shortDate: "dd/MM/yy"
        shortTime: "HH:mm"
      NUMBER_FORMATS:
        DECIMAL_SEP: ','
        GROUP_SEP: '.'
    tr:
      DATETIME_FORMATS:
        fullDate: "d MMMM y EEEE"
        longDate: "d MMMM y"
        medium: "d MMM y HH:mm:ss"
        mediumDate: "d MMM y"
        mediumTime: "HH:mm:ss"
        short: "dd MM yyyy HH:mm"
        shortDate: "dd MM yyyy"
        shortTime: "HH:mm"
      NUMBER_FORMATS:
        DECIMAL_SEP: ","
        GROUP_SEP: "."
    en:
      DATETIME_FORMATS:
        fullDate: 'EEEE, MMMM d, y'
        longDate: 'MMMM d, y'
        medium: 'MMM d, y h:mm:ss a'
        mediumDate: 'MMM d, y'
        mediumTime: 'h:mm:ss a'
        short: 'M/d/yy h:mm a'
        shortDate: 'M/d/yy'
        shortTime: 'h:mm a'
      NUMBER_FORMATS:
        DECIMAL_SEP: "."
        GROUP_SEP: ","
