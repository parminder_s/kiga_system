###*
  @ngdoc module
  @name misc
  @module misc
  @description

  Misc module which holds every components that are needed by one
  than more module.

  Please not the additional loading of tinymce although its set
  as dependency in requirejs. This is because it is not included
  in the bundle when deploying.

  Further more bootstrap is also set to be loaded, although it is
  a dependency of angular-bootstrap. For some reasons it turned out
  that it does not load correctly when run through the requirejs
  optimizer.

  The loader for blueimp was found on
  http://stackoverflow.com/questions/31849476/how-to-use-blueimp-file-upload-with-webpack
###

define [
  'angular'
  './filters/kigaCurrency'
  '../../misc/js/model/FavouritesModel'
  './services/MockedCountryService'
  './services/CustomTranslationTable'
  './services/IdeaRedirectService'
  './services/TransitionHistoryService'
  './services/HistoryLogService'
  './services/RedirectorService'
  './services/AudioHandler',
  './services/CookieInfoService'
  './services/JobOptionsService',
  './components/KigaLinkCtrl'
  '../../misc/components/audioPlayer/AudioPlayerCtrl'
  '../../misc/components/mockedCountry/MockedCountryCtrl'
  '../../misc/components/starRating/StarRatingCtrl'
  '../../misc/components/toolTipInfoIcon/ToolTipInfoIcon'
  '../../misc/components/buttonRow/ButtonRow'
  '../../misc/components/kigaUpload/KigaUploadController'
  '../../misc/components/videoPlayer/VideoPlayer'
  '../../misc/toast/ToastService'
  './factories/PartialInterceptor'
  '../../misc/components/scroller/ScrollerController'
  './services/Loader'
  './services/ngDraggable'
  'form'
  'bootstrap/dist/js/bootstrap'
  'angular-ui-bootstrap'
  'angular-touch'
  '../../rawJs/modernizr'
  'angular-translate'
  'ui-select'
  'angular-ui-router'
  '../../rawJs/ui-utils-jq'
  '../../rawJs/statehelper'
  'angular-ui-validate',
  'angular-animate'
  'ng-toast'
  'oclazyload'
  'ng-tags-input'
  'ng-material-floating-button/src/mfb-directive'
  'imports-loader?define=>false&exports=>false!blueimp-file-upload/js/vendor/jquery.ui.widget.js'
  'imports-loader?define=>false&exports=>false!blueimp-file-upload/js/jquery.iframe-transport.js'
  'imports-loader?define=>false&exports=>false!blueimp-file-upload/js/jquery.fileupload.js'
  'imports-loader?define=>false&exports=>false!blueimp-file-upload/js/jquery.fileupload-process.js'
  'imports-loader?define=>false&exports=>false!blueimp-file-upload/js/jquery.fileupload-validate.js'
  'imports-loader?define=>false&exports=>false!blueimp-file-upload/js/jquery.fileupload-angular.js'
  './services/ngDraggable'
  'angular-ui-tinymce'
  '../validation/js/main'
  './values/templates'], (angular, kigaCurrency, favouritesModel, MockedCountryService, CustomTranslationTable, IdeaRedirectService, transitionService,
  historyLogService, redirectorService, AudioHandler, CookieInfoService, JobOptionsService, KigaLinkCtrl,
  AudioPlayerCtrl, MockedCountryCtrl, StarRatingCtrl, ToolTipIcon, ButtonRowCtrl, KigaUploadController,
  VideoPlayer, ToastService, PartialInterceptor, ScrollerController, Loader) ->
  app = angular.module 'misc', ['ui.bootstrap', 'ngTouch', 'pascalprecht.translate', 'kiga.form'
    'ui.jq', 'ui.select', 'ui.router', 'ngDraggable',
    'ui.router.stateHelper', 'ui.validate', 'kiga.validation'
    'ngTagsInput', 'ng-mfb', 'ui.tinymce', 'ngAnimate',
    'ngToast', 'oc.lazyLoad']

  app.service("redirectorService", ($state, $stateParams) ->
    return new redirectorService.default($state, $stateParams);
  )

  app.service 'mockedCountryService', (endpoint, config) ->
    return new MockedCountryService.default(endpoint, config)

  app.service 'ideaRedirectService', (endpoint) ->
    return new IdeaRedirectService.default(endpoint)

  app.service("transitionHistoryLogService", (transitionHistoryService, endpoint, $interval, config) ->
    return new historyLogService.default(transitionHistoryService, endpoint, $interval, config)
  )

  app.service("transitionHistoryService", ($transitions, $state) ->
    return new transitionService.default($transitions, $state)
  )

  app.service("toastService", (ngToast, $translate) ->
    return new ToastService.default(ngToast, $translate)
  )

  app.service "audioHandler", -> new AudioHandler.default()

  app.service "JobOptionsService", -> return new JobOptionsService.default()

  app.service "cookieInfoService", ($rootScope) -> return new CookieInfoService.default($rootScope)

  app.service "customTranslationTable", -> return new CustomTranslationTable.default()

  app.run((transitionHistoryLogService) ->
    transitionHistoryLogService.startHistoryLogging()
  )

  app.run(($transitions, $state, transitionHistoryService) ->
    $transitions.onSuccess(true, (transition) =>
      toUrl = $state.href(transition.to().name, transition.params("to"))
      fromUrl = $state.href(transition.from().name, transition.params("from"))
      transitionHistoryService.pushState($state.get("."), $state.params, fromUrl, toUrl)
    )
  )

  app.controller("kigaLinkCtrl", (endpoint) ->
    return new KigaLinkCtrl.default(endpoint)
  )

  app.component("buttonRow", {
    templateUrl: "misc/components/buttonRow/buttonRow.html",
    controller: () -> new ButtonRowCtrl.default(),
    bindings: {
      categories: "<",
      selectedHierarchy: "<",
      onTopCategoryClick: "&",
      onCategoryClick: "&",
      isLastNavigationLevel: "<",
      selectedHierarchyLimit: "@",
      firstButtonName: "@",
      isFavorites: "<"
    }
  })

  app.component "audioPlayer", {
    bindings:
      href: "@"
      showOnlyButtons: "<"
      label: "@"
    controller: ($scope, audioHandler) ->
      new AudioPlayerCtrl.default($scope, audioHandler)
    templateUrl: "misc/components/audioPlayer/audioPlayer.html"
  }

  app.component "scroller", {
    controller: ($element) -> new ScrollerController.default($element),
    template: '<ng-transclude></ng-transclude>',
    transclude: true
  }

  app.component "slider", {
    templateUrl: 'misc/components/slider/slider.html',
    bindings:
      elements: '<'
  }
  app.component "videoPlayer", {
    bindings:
      videoLink: "@"
      previewLink: "@"
      button: "@"
    controller: -> new VideoPlayer.default()
    template: '<div id="{{$ctrl.randomId}}"></div>'
  }

  app.component "mockedCountry", {
    bindings:
      country: "<"
    templateUrl: "misc/components/mockedCountry/mockedCountry.html"
    controller: (mockedCountryService) -> new MockedCountryCtrl.default(mockedCountryService)
  }

  app.component("kigaLink", {
    controller: "kigaLinkCtrl"
    template: '<a ng-href="{{$ctrl.url}}" ng-click="$ctrl.submit()" data-test="{{$ctrl.tag}}" ng-transclude></a>'
    transclude: true
    bindings:
      url: '@'
      tag: '@'
      id: '@'
      title: '@'
      origin: '@'
  })

  app.component("starRating", {
    templateUrl: "misc/components/starRating/starRating.html",
    controller: (endpoint) -> new StarRatingCtrl.default(endpoint),
    bindings: {
      kigaPageId: "<"
    }
  })

  app.component("ngUpgrade", {
    template: "<div>Angularjs component</div>",
    controller: (endpoint) -> new StarRatingCtrl.default(endpoint)
  })

  app.component("toolTipInfoIcon", {
    templateUrl: "misc/components/toolTipInfoIcon/toolTipInfoIcon.html",
    controller: () -> new ToolTipIcon.default(),
    bindings: {
      toolTipText: "@"
    }
  })

  app.component("kigaUpload", {
    controller: (kigaUploadService, $attrs) ->
      isRequired = $attrs.hasOwnProperty "required"
      new KigaUploadController.KigaUploadController(kigaUploadService, isRequired)
    templateUrl: "misc/components/kigaUpload/kigaUpload.html"
    bindings:
      ngModel: "="
      uploadedFiles: "<"
      context: "<"
      acceptedTypes: "<"
      maxFiles: "<"
      onStartUpload: "<"
      onFinishUpload: "<"
      name: "<"
      hidePreview: "<"
      cannedAccessControlList: "@"
  })

  app.config ($translateProvider, $httpProvider, tagsInputConfigProvider
    uiSelectConfig) ->
    $translateProvider.useLoader 'kigaLoader', {}
    $translateProvider.useSanitizeValueStrategy null
    $httpProvider.interceptors.push 'partialInterceptor'

    tagsInputConfigProvider.setActiveInterpolation('tagsInput', {placeholder: true});
    tagsInputConfigProvider.setDefaults 'tagsInput', placeholder: ''


    angular.extend uiSelectConfig,
      theme: 'bootstrap'
      searchEnabled: false
      resetSearchInput: true

  #adds space between currency symbol and number
  app.filter 'kigaCurrency', ["$filter", kigaCurrency.default]

  app.filter 'percentage', ($filter) ->
    (input, decimals = 0) -> $filter('number')(input * 100, decimals)

  app.factory 'kigaLoader', ($http, $q, translationTransformer, translationModeHandler, customTranslationTable, localeData) -> (options) ->

    if typeof customTranslationTable.getEntry(options.key) isnt 'undefined'
      return $q.when(translationTransformer(customTranslationTable.getEntry(options.key)))

    locale = options.key
    locales = translationTransformer localeData.get(locale)
    new Promise((resolve) -> resolve(locales))

  app.factory 'partialInterceptor', -> new PartialInterceptor.default()

  app.service 'favouritesModel', (endpoint, $q, favouritesService) ->
    new favouritesModel.FavouritesModel(endpoint, $q, favouritesService)

  app
