define ['jquery', './app', '../../rawJs/icheck'], ($, app) ->
  app.value 'uiJqConfig',
    datepicker:
      showOn: 'button'
      buttonImage: "#{kiga?.nStaticUrl}misc/images/calendar.png"
      buttonImageOnly: true
      dateFormat: 'dd.mm.yy'
    iCheck:
      checkboxClass: 'icon iCheck'
      radioClass: 'icon iCheck'
      checkedCheckboxClass: 'icon-checkbox-on'
      uncheckedCheckboxClass: 'icon-checkbox-off'
      checkedRadioClass: 'icon-radio-on'
      uncheckedRadioClass: 'icon-radio-off'
      cursor: true
