#http://stackoverflow.com/questions/19224028/add-directives-from-directive-in-angularjs

define ['../app', 'jquery'], (app, $) ->
  app.directive 't', ($compile) ->
    restrict: 'A'
    replace: false
    terminal: true
    priority: 1000
    compile: (element, attr) ->
      element.attr
        translate: $(element).attr 't'
  #      'translate-compile': true
      element.removeAttr 't'

      post: (scope, iElement, iAttrs, controller) ->
        $compile(iElement)(scope)
