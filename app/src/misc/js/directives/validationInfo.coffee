###
  Simple validation info

  An "element" is generated dynamically which provides validation messages

  It can customized via an object which contains specific messages for certain "validation error key" (required, min, max etc.)

  e.g.

  <div validation-info-required="isValidationInfoRequired">
    <form>
      <input ng-model="model.name" validation-info  required></input>
      <input ng-model="model.email" validation-info="{'validator':'REG_EMAIL_CONFIRMATION_ERROR'}"  required></input>
    </form>
  </div>

###

###
  master directive for providing information if validation is required
###
define ['../app'], (app) ->
  app.directive 'validationInfoRequired', ($log) ->
    scope: validationInfoRequired: '='
    restrict: 'A'
    controller: ['$scope', ($scope) ->
      this.validationInfoRequired = $scope.validationInfoRequired
      return
    ]


  ###
    Should be used on an input element with ng-model attribute inside a form (or ng-form).
    It provides a dynamic way of show validation information
  ###
  app.directive 'validationInfo', ($log, $compile) ->
    restrict: 'A'
    require: ['?^form', '?^ngForm', 'ngModel' ]
    link:  (scope, element, atts, controllers) ->
      formCtrl = controllers[0]
      ngFormCtrl = controllers[1]
      modelCtrl = controllers[2]

      unless modelCtrl.$name
        $log.error 'element must have a name attribute!!'

      name = ngFormCtrl?.$name # take ng-form first when existent
      name ?= formCtrl?.$name # or form
      name = "#{name}.#{modelCtrl.$name}"

      options = atts.validationInfo ?= ""

      params = {}
      for validation in ['required','max','min','minlength','ngMinlength','ngMaxlength','ngRequired','ngPattern','emailValidation']
        if atts[validation]? then params[validation] = atts[validation]

      if atts['uiValidate'] then params['validator'] = ''
      if atts.type? and atts.type is 'email' then params['email'] = ''

      validations = if params? then JSON.stringify(params) else ""

      elementAsString = "<validation-info-element class=\"validation-info\" validations=\'#{validations}\' options=\"#{options}\" formelement=\"#{name}\"></validation-info-element>"

      newElement = angular.element(elementAsString)

      compiled = $compile(newElement)

      element.after(newElement)

      compiled(scope)


  ###
    This serves as real validation info directive -> the "element" is generated dynamically
    from validationElement directive
  ###
  app.directive 'validationInfoElement', ($log, $translate) ->
    restrict: 'E'
    scope:
      formelement: '='
      forceValidationMessage: '@'
    require: '?^validationInfoRequired'
    templateUrl: 'misc/partials/directives/validationInfo.html'
    replace: true
    controller: ($scope) ->
      $scope.data = {}
      $scope.validationKeys=[]

      $scope.getMessage = (key) ->
        if not $scope.data[key]?
          $scope.data[key] = key
        $scope.data[key]

      $scope.resolveKeys = ->
        if $scope.validations
          for own key, value of $scope.validations
            key = key[2..].toLowerCase() if key[0..1] is 'ng'  #cut ng - because validation keys are without 'ng'
            key = 'validator' if key is 'uiValidate'
            $scope.validationKeys.push key
            realKey = if $scope.options?[key]? then $scope.options?[key] else "VALIDATION_#{key.toUpperCase()}"
            $scope.translate key, realKey

        return

      $scope.translate = (key, realKey) ->
        $translate(realKey).then( (value) ->
          $scope.data[key] = value)

    link: (scope, element, atts, validationInfoRequiredCtrl) ->
      #check if parent element availabe which provides validationInfoRequired function
      scope.isValidationInfoRequired =
        if scope.forceValidationMessage?
          -> scope.forceValidationMessage == 'true'
        else if validationInfoRequiredCtrl?.validationInfoRequired?
          validationInfoRequiredCtrl.validationInfoRequired
        else
          -> true
      scope.options = if atts.options? then  scope.$eval atts.options else {}
      scope.validations = if atts.validations? then  scope.$eval atts.validations else {}

      scope.resolveKeys()

      return


