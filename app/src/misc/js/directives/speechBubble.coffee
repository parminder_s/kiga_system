define ['../app'], (app) ->
  app.directive 'speechBubble', ($compile) ->
    restrict: 'A'
    scope: true
    link: (scope, elem, attrs) ->
      elem.replaceWith $compile("
        <div class=\"speech-bubble\">
          <div class=\"#{attrs.speechBubble}\">#{elem.html()}</div>
        </div>
      ") scope
