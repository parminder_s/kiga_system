define ['../app', 'jquery', 'url'], (app, $, urlFn) ->
  url = urlFn.default

  app.directive 'hierarchicalMenu', ->
    scope:
      menuData: "="
    templateUrl: 'misc/partials/directives/hierarchicalMenu.html'
    replace: true
    restrict: 'E'
    controller: ($scope, $state, $stateParams, $translate) ->
      $scope.currentMenu = angular.copy($scope.menuData.menuItems)
      $scope.path = []
      $scope.flag = false
      $scope.localeMenu = false
      $scope.countryMenu = false

      $scope.selectItem = (item) ->
        if(item.children instanceof Array)
          $scope.currentMenu = item.children
          $scope.path.push(item)
          if(item.type == 'locale')
            $scope.localeMenu = true
          else
            $scope.localeMenu = false
          if (item.type == 'country')
            $scope.countryMenu = true
          else
            $scope.countryMenu = false

      $scope.goBack = () ->
        $scope.localeMenu = false
        $scope.countryMenu = false
        $scope.path.pop()
        if($scope.path.length)
          $scope.currentMenu = $scope.path[$scope.path.length - 1].children
        else
          $scope.currentMenu = angular.copy($scope.menuData.menuItems)

      $scope.switchLocale = (locale) ->
        $state.go '.', locale: locale

      $scope.$on('$stateChangeStart', () ->
        $scope.cleanUp()
      )

      $scope.cleanUp = () ->
        $scope.currentMenu = angular.copy($scope.menuData.menuItems)
        $scope.path = []
        $scope.flag = false
        $scope.localeMenu = false
        $scope.countryMenu = false
