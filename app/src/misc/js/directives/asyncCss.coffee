define ['../app', 'jquery', '../../../url'], (app, $, urlFn) ->
  url = urlFn.default
  app.directive 'asyncCss', ->
    loadedStyles = {}

    restrict: 'E'
    link: (scope, element, atts) ->
      stylePath = atts.href
      stylePath = url stylePath unless stylePath.match(/^http/) or stylePath.match(/^\//)
      return if loadedStyles.stylePath?

      link = document.createElement 'link'
      linkAttr =
        type: 'text/css'
        rel: 'stylesheet'
        media: 'all'
        href: stylePath

      for own key, val of atts
        unless key is 'href' or key.match /^\$/
          linkAttr[key] = val

      $(link).attr linkAttr
      document.getElementsByTagName('head')[0].appendChild link

      loadedStyles[stylePath] = true
      link.onload = () ->
        $(window).trigger 'cssLoaded'
