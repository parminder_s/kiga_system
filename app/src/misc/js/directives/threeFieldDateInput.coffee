###
  Input component which provides 3 input areas (day, month, year)
  To be able to work with a date value in form of YYYY-MM-DD
  Uses ui-validation to trigger isDayValid and isYearValid
###

define ['../app', 'moment'], (app, moment) ->
  app.directive 'threeFieldDateInput',($log)  ->
    restrict: 'E'
    require: 'ngModel'
    replace: true
    templateUrl: 'misc/partials/directives/threeFieldDateInput.html'
    link: (scope, element, atts, ngModelController) ->
      if not ngModelController?
        return

      scope.months = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY',
        'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER']
        .map (month, idx) ->
          name: month
          idx: idx

      dateFormat = 'YYYY-MM-DD'
      scope.date = month: scope.months[0]
      scope.change = ->
        if scope.isDateValid parseInt(scope.date.year), scope.date.month.idx, parseInt(scope.date.day)
          scope.ngModelController.$setViewValue [scope.date.year, scope.date.month.idx, scope.date.day]
          scope.setValid true
        else
          scope.ngModelController.$setViewValue null
          scope.setValid false

      scope.setValid = (value) -> scope.ngModelController.$setValidity 'dateinput', value

      scope.isDateValid = (yyyy, mm, dd) ->
        mm = parseInt mm,10
        valid = false
        if angular.isNumber(dd) and angular.isNumber(yyyy)
          yearValid = yyyy > 1900 and yyyy <= 2100
          date = new Date(yyyy, mm, dd, 0, 0, 0, 0)
          valid = mm is date.getMonth() and dd is date.getDate() and yyyy is date.getFullYear()
        yearValid and valid

      scope.ngModelController = ngModelController
      ngModelController.$formatters.unshift (value) ->
        if value?
          current = moment value, dateFormat
          scope.date =
            day: current.date()
            month: scope.months[current.month()]
            year: current.year()
          scope.date
        else
          scope.date = month: scope.months[0]

      ngModelController.$parsers.unshift (value) ->
        if value?
          moment(value).format dateFormat
        else
          value
