# Uses the bootstrap datepicker directive and add kiga specific
# options.

#http://stackoverflow.com/questions/19224028/add-directives-from-directive-in-angularjs

define ['../app', 'jquery'], (app, $) ->
  app.directive 'kigaDate', ($compile) ->
    counter = 1

    restrict: 'A'
    replace: false
    terminal: true
    priority: 1000
    compile: (element, attr) ->
      element.attr
        'datepicker-options': "{startingDay: 1}"
        'show-button-bar': 'false'
        'uib-datepicker-popup': 'shortDate'
        readonly: true
        'is-open': "Opened#{counter}"
        'ng-click': "Opened=true"
        'data-role': 'datepicker'

      element.wrap '<div class="input-group"></div>'
      btn = $ """
        <span class="input-group-addon" data-test="openCalendar" ng-click="Opened#{counter}=true;$event.stopPropagation();">
          <i class="glyphicon glyphicon-calendar"></i>
        </span>
      """

      element.after btn.wrap '<span class="input-group-addon"></span>'

      counter++
      element.removeAttr 'kiga-date'

      post: (scope, iElement, iAttrs, controller) ->
        $compile(iElement)(scope)

  app.directive 'datepickerPopup', ->
    restrict: 'EAC'
    require: 'ngModel'
    link: (scope, element, attr, controller) ->
      controller.$formatters.shift()
