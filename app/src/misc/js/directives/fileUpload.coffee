define ['../app'], (app) ->
  app.directive 'fileUpload', () ->
    restrict: 'A'
    template: '<input type="file" name="files[]">'
    scope:
      onAdd: '=onAdd'
      options: '=options'
    link: ($scope, $element, $attrs) ->
      $element.find('input').attr('multiple','multiple') if $attrs.multiple?
      $element.fileupload( angular.extend
        autoUpload: false
        $scope.options
      ).on 'fileuploadprocessdone', (e, data) ->
        $scope.$apply $scope.onAdd data.files
      .on '$destroy', () ->
        try
          $element.fileupload('destroy');
        catch
          console.info('fileupload destory bug catch!! TODO: fix it')

  .directive 'fileUploadPreview', () ->
    restrict: 'A'
    scope:
      uploadFile: '=uploadFile'
    link: ($scope, $element, $attrs) ->
      $scope.$watch 'uploadFile', (file) ->
        $element.empty()
        $element.append file.preview if file?.preview
