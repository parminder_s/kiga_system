define ['../app'], (app) ->
  app.directive 'orWidget', ->
    requires: 'E'
    replace: true
    templateUrl: 'misc/partials/directives/orWidget.html'
