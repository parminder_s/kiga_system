###
  scroll panel directive - allows to add scrolling functionality
  to any block with children size exceed panel size
###

class ScrollPanelController
  constructor: ($element, $window, $timeout) ->
    @el = $element
    @window = angular.element $window
    @initialized = false
    @$timeout = $timeout;

  setOptions: (options) -> @options = options

  init: ->
    @$timeout(
      ()=>
        return if initialised
        initialised = true
        @updateValueOnResize()
        @setStartPosition()
        if (@options.autoHideButtons)
          @window.on 'resize', @updateValueOnResize
          @window.on 'resize', () => @updateButtonsVisibility(0)
          @window.on 'resize', @setStartPosition

        @prevBtn = angular.element @options.prevButton
        @nextBtn = angular.element @options.nextButton

        @prevBtn.on 'click', @onPrevClick
        @nextBtn.on 'click', @onNextClick
      , 0
    )

  updateValueOnResize:() =>
    @getPosValue = @getPos()
    @getScrollPageValue = @getScrollPage()
    @elInnerWidth = @el.innerWidth()
    @getWindowSizeValue = @getWindowSize()
    @getMaxPosValue = @getMaxPos()
    @el0ScrollWidth = @el[0].scrollWidth

  updateButtonsVisibility: (timeoutInMs) =>
    return if (not @options) or (not @options.autoHideButtons) or (!@prevBtn)
    @$timeout(
      ()=>
        pos = @getPosValue
        maxPos = @getMaxPosValue
        if (pos == 0)
          if @options.showInactivePrevBtn && (@el0ScrollWidth - @elInnerWidth)
            @prevBtn.addClass('inactive-btn')
          else
            @prevBtn.hide()
        else
          @prevBtn.show()
          @prevBtn.removeClass('inactive-btn')

        if (pos == maxPos || maxPos < 10)
          @nextBtn.hide()
        else
          @nextBtn.show()
    , timeoutInMs
    )

  setStartPosition: =>
    @$timeout(
      ()=>
        pos = @getPosValue
        childrenSize = @getChildrenSize(0)
        idx = @options.idx || 0
        sumWidth = childrenSize * (idx + 1)
        pageWidth = @getScrollPageValue
        page = Math.floor(sumWidth / pageWidth - 0.1)
        if (page)
          @el.duScrollTo(page * pageWidth)
        else if(pos)
          offset = Math.round((pos + childrenSize) / pageWidth)
          @el.duScrollTo(offset * pageWidth)
      , 0
      )


  getWindowSize: ->
    if (@options.vertical)
      @el.innerHeight()
    else
      @el.innerWidth()


  getMaxPos: ->
    windowSize = @getWindowSizeValue
    if (@options.vertical)
      @el[0].scrollHeight - windowSize
    else
      @el[0].scrollWidth - windowSize

  getScrollPage: ->
    if (@options.vertical)
      @el.innerHeight()
    else
      @el.innerWidth()

  scrollToIndex: (idx, useOffset = false) ->
    elPos = 0
    if (idx > 0)
      elPos = elPos + @getChildrenSize(i) for i in [0..idx-1]

    if @options.vertical
      offset = @el.children()[idx].offsetTop
    else
      offset = @el.children()[idx].offsetLeft
    if useOffset
      elPos = Math.max(elPos, offset)
    if (elPos < @getPosValue)
      @scrollTo(elPos)
    else if (elPos > @getPosValue + @getWindowSizeValue - @getChildrenSize(idx))
      @scrollTo(elPos + @getChildrenSize(idx) - @getWindowSizeValue)

  scrollTo: (pos) ->
    if (@getMaxPosValue - pos < @getChildrenSize(0))
      pos = @getMaxPosValue
    if (pos < @getChildrenSize(0) and @getChildrenSize(0) - pos > 10)
      pos = 0
    if (@options.vertical)
      @el.animate {scrollTop: pos}, @onScrollEnd
    else
      @el.animate {scrollLeft: pos}, @onScrollEnd

  onScrollEnd: =>
    @getPosValue = @getPos()
    @updateButtonsVisibility 0

  getChildrenSize: (idx) ->
    children = @el.children()
    if (idx == -1)
      idx = children.length-1

    size = if (@options.vertical)
      angular.element(children[idx]).outerHeight(true)
    else
      angular.element(children[idx]).outerWidth(true)

    Math.ceil size

  getPos: ->
    if (@options.vertical)
      Math.ceil @el[0].scrollTop
    else
      Math.ceil @el[0].scrollLeft

  onPrevClick: =>
    if (@options.pageScroll)
      @scrollTo @getPosValue - @getScrollPageValue
    else
      @scrollTo @getPosValue - @getChildrenSize(0)

  onNextClick: =>
    if (@options.pageScroll)
      @scrollTo @getPosValue + @getScrollPageValue
    else
      @scrollTo @getPosValue + @getChildrenSize(0)


define ['../app'], (app) ->
  app.directive 'scrollPane',()  ->
    restrict: 'A',
    controller: ($element, $window, $timeout) ->
      new ScrollPanelController($element, $window, $timeout)
    link: (scope, element, attrs, ctrl) ->
      attrs.$observe 'scrollPane', (newOptions) ->
        newOptions = scope.$eval newOptions

        if (newOptions.watch)
          watcher = scope.$watch newOptions.watch, (data) ->
            ctrl.updateButtonsVisibility(0)
          scope.$on '$destroy', -> watcher()
        ctrl.setOptions newOptions
        ctrl.init()

        getMaxPos = () -> ctrl.getMaxPos()
        sizeWatcher = scope.$watch getMaxPos, (data) ->
          ctrl.updateButtonsVisibility(0)
        scope.$on '$destroy', -> sizeWatcher()
