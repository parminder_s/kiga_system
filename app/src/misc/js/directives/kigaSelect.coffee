define ['../app','angular','lodash'], (app,angular,_) ->
  app.directive 'kigaSelect',(kigaSelectService) ->
    bindToController: true
    scope:
      placeholder: '@'
      test: '@' # data-test - ng removes 'data'
      options: '='
      onSelect: '&'
      ngModel: '='
      inputName: '@?'

    controllerAs: 'ctrl'
    controller: ($element) ->
      @isRequired = $element.is '[required]'
      @filteredOptions = kigaSelectService.prepareInput @options
      null

    templateUrl: 'misc/partials/directives/kigaSelect.html'


