###*

@ngdoc directive
@name emailValidation
@module misc
@description

Simple email validation Attribute (Async)

Requires a function parameter as String.

The function will then be called with an object parameter in form of {email: 'emailaddres@test.com'}
and has to return a promise. The promises result should be an object in this form: {data:{error:true|false}}

@usage
<input name="email" type="email" ng-model="input.newEmail" required
                       ng-model-options="{ updateOn: 'default blur', debounce: {'default': 300, 'blur': 0}}"
                       email-validation="model.memberProfileEmailOk" />

###

app = define ['../app'], (app) ->
  app.directive 'emailValidation', ($log,$q) ->
    restrict: 'A'
    require: 'ngModel'
    link:  (scope, element, atts, controller) ->
      if not atts.emailValidation?
        $log.error 'no email validation functions defined'
      else
        validationFunction = scope.$eval atts.emailValidation

        #define 'emailValidation' validator
        controller.$asyncValidators.emailValidation = (email) ->
          deferred = $q.defer()
          #call of validation function
          validationFunction({email:email})
            .then (result) ->
              #result evaluation
              if result.data?.error then deferred.reject() else deferred.resolve()
          deferred.promise
