define ['../app', '../../../rawJs/icheck'], (app) ->
  app.directive 'icheck', ($timeout, $q) ->
    require: ['ngModel', 'icheck']
    controller: ->
      def = $q.defer();
      @getCompilePromise = => def.promise
      @resolveCompilePromise = => def.resolve()
      null
    link: ($scope, element, $attrs, controllers) ->
      ngModel = controllers[0]
      icheck = controllers[1]
      $timeout ->
        value = $attrs['value']
        $scope.$watch $attrs['ngModel'], (newValue) ->
          $(element).iCheck 'update'

        parent = $(element).parent()
        checkElement = if parent.prop('tagName').toLowerCase() is 'label'
          parent[0]
        else element

        iCheck = $(checkElement).iCheck
          checkboxClass: 'icon iCheck'
          radioClass: 'icon iCheck'
          checkedCheckboxClass: 'icon-checkbox-on'
          uncheckedCheckboxClass: 'icon-checkbox-off'
          checkedRadioClass: 'icon-radio-on'
          uncheckedRadioClass: 'icon-radio-off'
          cursor: true

        iCheck.on 'ifChanged', (event) ->
          if $(element).attr('type') is 'checkbox' and $attrs['ngModel']
            $scope.$apply -> ngModel.$setViewValue event.target.checked
          if $(element).attr('type') is 'radio' and $attrs['ngModel']
            $scope.$apply -> ngModel.$setViewValue value

        iCheck.on 'ifClicked', (event) ->
          $scope.$apply $(element).attr('ng-click')

        icheck.resolveCompilePromise()
