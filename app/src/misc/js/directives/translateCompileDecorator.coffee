###*

  @ngdoc directive
  @name translateCompileDecorator
  @module misc
  @description
  It is an decoration for extending angular "translate" directive. It injects attr translate-compile='true' in any element
  where translate directive is used. As a result, the contents of an element is compiled by the translate service.
  It helps us to achieve the same result as $translate.usePostCompile(true)

  @requires
  $delegate

###

define ['../app'], (app) ->
  app.config ($provide) ->
    $provide.decorator 'translateDirective', ($delegate) ->
      directive = $delegate[0]
      compile = directive.compile
      directive.compile = (tElement, tAttrs) ->
        link = compile.apply this, arguments
        pre : (scope, elem, attrs) ->
          tElement.attr
            'translate-compile': 'true'
          tAttrs['translateCompile'] = true

        post : (scope, elem, attrs) ->
          link.apply this, arguments

      $delegate
