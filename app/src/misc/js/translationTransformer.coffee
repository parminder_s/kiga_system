###*
  @ngdoc service

  @name misc.translationTransformer

  @description
  Provides the appropriate data set needed for the application.
  Depending on if translation is enabled or not, it provides simple plain text data or a complex dataset.

###

define ['./app', 'lodash'], (app, _) ->
  app.service 'translationTransformer', (translationModeHandler, objectDataTransformer, jsonDataTransformer) -> (data) ->
    if translationModeHandler.isEnabled() is true
      objectDataTransformer data
    else
      jsonDataTransformer data