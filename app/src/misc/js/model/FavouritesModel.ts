import IPromise = angular.IPromise;
import { IFavouritesService } from '../services/AngularInterfaces';

/**
 * Created by bbs on 11/5/16.
 */

export class FavouritesModel {
  private cache = null;

  constructor(
    private endpoint,
    private $q: ng.IQService,
    private favouritesService: IFavouritesService
  ) {}

  listFavouritesForIdsAsPromise(): IPromise<any> {
    let returner = this.$q.defer<any>();
    let data = this.endpoint({
      method: 'POST',
      url: 'favourites',
      quietMode: true
    });
    data.then(data => {
      if (data !== null) {
        this.cache = data;
      } else {
        this.cache = { 'favourites: ': [] };
      }
      if (this.cache.favourites === null) {
        this.cache.favourites = [];
      }
      returner.resolve(this.cache);
    });
    return returner.promise;
  }

  isIdeaFavourite(id: number): IPromise<boolean> {
    return this.endpoint({
      method: 'GET',
      quietMode: true,
      url: 'favourites/isFavourite/' + id
    });
  }

  toggleFavourite(id: number): void {
    this.favouritesService.toggle(id);
  }

  notifyForUpdate(cb: () => void) {
    this.favouritesService.notifyForUpdate(cb);
  }
}
