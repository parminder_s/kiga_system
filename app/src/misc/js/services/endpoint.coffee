###*
  * @ngdoc service
  * @name endpoint

  * @description
  * Wrapper method for request with endpoint parameter set to true.
  * Endpoint means that the request goes always to Spring application
  * instead of legacy SilverStripe
###

define ['../app', 'lodash'], (app, _) ->
  app.service 'endpoint', (request) ->
    returner = ->
      if angular.isString arguments[0]
        arguments[0] = url: arguments[0], endpoint: true
      else if (not _.has(arguments[0], 'endpoint'))
        arguments[0].endpoint = true

      request.apply this, arguments
    returner.get = (request) ->
      request.method = 'GET'
      if angular.isString request
        request = url: request, endpoint: true, method: 'GET'
      else
        request.method = 'GET'
      returner request
    returner.post = (request) ->
      request.method = 'GET'
      if angular.isString request
        request = url: request, endpoint: true, method: 'POST'
      else
        request.method = 'POST'
      returner request
    returner
