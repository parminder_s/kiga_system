###*
  @ngdoc service

  @name localizer

  @description
  Sets the $locale formats for the current language and also allows
  dynamic switching.

  We are not using the locale formats from official angular but
  are using just a selection of them for the enabled locales.

  The formats are loaded from {@link main.locales} and the loading itself
  is called from {@link main.containerCtrl}

  We are storing the current locale directly to $locale, since $locale
  is implemented as provider and resets to default on angular upstart,
  whereas localizer keeps its data the whole time -> troubles in unit tests.
###

define ['../app', 'angular'], (app, angular) ->
  app.service 'localizer', ($locale, locales) -> (currentLocale) ->
    if currentLocale isnt $locale.current
      $locale.current = currentLocale
      localeData = locales[currentLocale]
      angular.extend $locale.DATETIME_FORMATS, localeData.DATETIME_FORMATS
      angular.extend $locale.NUMBER_FORMATS, localeData.NUMBER_FORMATS
