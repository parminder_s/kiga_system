/**
 * Created by peter on 07.08.17.
 */

export default class {
  private lastIndex = 0;

  constructor() {}

  getImageName(fileName: string) {
    let extension = fileName.split('.').pop();
    if (extension === 'docx' || extension === 'doc') {
      return 'word.png';
    }
    if (extension === 'xslx' || extension === 'xsl') {
      return 'excel.png';
    }
    if (extension === 'png' || extension === 'jpg' || extension === 'gif') {
      return 'image.png';
    }
    if (extension === 'mp3' || extension === 'wav') {
      return 'music.png';
    }
    if (extension === 'pdf') {
      return 'pdf.png';
    }
    if (extension === 'ppt' || extension === 'pptx') {
      return 'powerpoint.png';
    }
  }
}
