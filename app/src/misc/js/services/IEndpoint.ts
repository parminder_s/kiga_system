export enum IEndpointMethod {
  GET = 'GET',
  POST = 'POST'
}

export interface IEndpointOptions {
  endpoint?: boolean; //connect to Spring instead SilverStripe
  quietMode?: boolean; //don't show loader
  rawUrl?: boolean; //don't prefix the host
}

export interface IPostEndpointOptions extends IEndpointOptions {
  url: string;
  locale?: string;
  data?: any;
}

export interface IEndpoint {
  get<T>(url: string, options?: IEndpointOptions): Promise<T>;
  post<T>(options: IPostEndpointOptions): Promise<T>;
}

export default IEndpoint;
