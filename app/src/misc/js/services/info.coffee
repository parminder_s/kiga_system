define ['../app'], (app) ->
  app.service 'infoDlg', ($uibModal) ->
    (text, cb) -> $uibModal.open
      templateUrl: 'misc/partials/info.html'
      controller: ($scope, $uibModalInstance) ->
        unless cb
          cb = ->
        $scope.text = text
        $scope.close = ->
          $uibModalInstance.close()
          cb()
        $scope.ok = ->
          $uibModalInstance.close()
          cb()
