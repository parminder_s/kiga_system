define ['../app'], (app) ->
  app.service 'misc.showException', (infoDlg) ->
    (exceptionCode = 'EXCEPTION_GENERAL') ->
      infoDlg exceptionCode.toUpperCase()
