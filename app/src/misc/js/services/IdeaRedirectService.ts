/**
 * This service redirects a user without permission to view an idea to the ideas overview
 * page with opened preview panel.
 * This is done only, if the idea is called directly, e. g. it is the first request.
 * Reason is, that we don't want to show the permission screen on first sight. Same goes
 * for SEO.
 *
 * The notification when the first request is over is done by PermissionStateChangeHandler.
 */
export default class IdeaRedirectService {
  private directlyRequested = true;
  constructor(private endpoint) {}

  setFirstRequestOver() {
    this.directlyRequested = false;
  }

  isDirectlyRequested(): boolean {
    return this.directlyRequested;
  }

  redirect(ideaId: number) {
    this.endpoint({ url: `idea/getPreviewUrl/${ideaId}`, method: 'GET' }, url => {
      window.location.assign(url);
    });
  }
}
