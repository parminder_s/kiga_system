define ['../app','angular','lodash'], (app,angular,_) ->
  app.service 'kigaSelectService',  ->
    prepareInput: (input) ->
      #alert input.toSource()
      if angular.isArray input
        _.map input, (option) ->
          option.code = option.value if !option.code? and option.value?
          option.title = option.title.replace '&#039;', '\''
          option
      else
        returner = []
        #in for loop, title is the value and code is a index
        for code, title of input
          title = title.replace '&#039;', '\''
          returner.push {title:title, code:code}
        returner
