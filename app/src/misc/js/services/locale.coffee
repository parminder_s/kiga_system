###*
  * @ngdoc service
  * @name locale

  * @description
  * Using resolve together with $stateParams misses some values ($state).
  * For this reason this service stores the current locale and can be used
  * to retrieve it.
###

define ['../app'], (app) ->
  app.service 'locale', (localizer, localeInfo) -> new class Locale
    constructor: ->
      @locale = localeInfo.getLocale()
    set: (locale) ->
      @locale = locale
      localizer locale
    get: -> @locale
