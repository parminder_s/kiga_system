import KigaTransition from './KigaTransition';

export default class TransitionHistoryService {
  private _history: KigaTransition[] = [];

  constructor() {}

  public pushState(state, params, fromUrl: string, toUrl: string) {
    this._history.push(new KigaTransition(state, params, fromUrl, toUrl));
  }

  public getLast(): KigaTransition[] {
    return this._history.slice(-1);
  }

  public getN(fromIndex: number, count: number): KigaTransition[] {
    return this._history.slice(fromIndex, count);
  }

  public get(index: number): KigaTransition {
    return this._history[index];
  }

  public getFromIndex(index: number): KigaTransition[] {
    return this._history.slice(index);
  }

  public getCurrentSize(): number {
    return this._history.length;
  }
}
