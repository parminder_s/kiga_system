define ['../app'], (app) ->
  app.service 'errorMsg',  ->
    msgBox = null
    initialize: (msgBoxScope) ->
      @msgBox = msgBoxScope
    setMsg: (info) ->
      @msgBox.errorMsg = info
