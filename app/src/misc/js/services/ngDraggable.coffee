###*
  @ngdoc module
  @name ngDraggable
  @description

  https://github.com/fatlinesofcode/ngDraggable

  This is a customized verision of ngDraggable depending on v 0.2.0
  since it is last version that supported jQuery.
  ngDragScroll directive was added and adapted to jQuery.
###

define ['angular'], (angular) ->
  angular.module('ngDraggable', []).directive('ngDrag', [

    '$rootScope'
    '$parse'
    ($rootScope, $parse) ->
      {
        restrict: 'A'
        link: (scope, element, attrs) ->
          scope.value = attrs.ngDrag
          #  return;
          offset = undefined
          _centerAnchor = false
          _mx = undefined
          _my = undefined
          _tx = undefined
          _ty = undefined
          _mrx = undefined
          _mry = undefined
          _hasTouch = 'ontouchstart' of document.documentElement
          _pressEvents = 'touchstart mousedown'
          _moveEvents = 'touchmove mousemove'
          _releaseEvents = 'touchend mouseup'
          $document = $(document)
          $window = $(window)
          _data = null
          _dragEnabled = false
          _pressTimer = null
          onDragSuccessCallback = $parse(attrs.ngDragSuccess) or null

          initialize = ->
            element.attr 'draggable', 'false'
            # prevent native drag
            toggleListeners true
            return

          toggleListeners = (enable) ->
            # remove listeners
            if !enable
              return
            # add listeners.
            scope.$on '$destroy', onDestroy
            #attrs.$observe("ngDrag", onEnableChange);
            scope.$watch attrs.ngDrag, onEnableChange
            #attrs.$observe('ngCenterAnchor', onCenterAnchor);
            scope.$watch attrs.ngCenterAnchor, onCenterAnchor
            scope.$watch attrs.ngDragData, onDragDataChange
            element.on _pressEvents, onpress
            if !_hasTouch and element[0].nodeName.toLowerCase() is 'img'
              element.on 'mousedown', ->
                false
              # prevent native drag for images
            return

          onDestroy = (enable) ->
            toggleListeners false
            return

          onDragDataChange = (newVal, oldVal) ->
            _data = newVal
            return

          onEnableChange = (newVal, oldVal) ->
            _dragEnabled = newVal
            return

          onCenterAnchor = (newVal, oldVal) ->
            if angular.isDefined(newVal)
              _centerAnchor = newVal or 'true'
            return

          isClickableElement = (evt) ->
            angular.isDefined($(evt.target).attr('ng-click')) or angular.isDefined($(evt.target).attr('ng-dblclick')) or angular.isDefined($(evt.target).attr('ng-cancel-drag'))

          ###
          # When the element is clicked start the drag behaviour
          # On touch devices as a small delay so as not to prevent native window scrolling
          ###

          onpress = (evt) ->
            if !_dragEnabled
              return
            # disable drag on clickable element
            if isClickableElement(evt)
              return
            if _hasTouch
              cancelPress()
              _pressTimer = setTimeout((->
                cancelPress()
                onlongpress evt
                return
              ), 100)
              $document.on _moveEvents, cancelPress
              $document.on _releaseEvents, cancelPress
            else
              onlongpress evt
            return

          cancelPress = ->
            clearTimeout _pressTimer
            $document.off _moveEvents, cancelPress
            $document.off _releaseEvents, cancelPress
            return

          onlongpress = (evt) ->
            if !_dragEnabled
              return
            evt.preventDefault()
            $rootScope.$broadcast 'draggable:preStart',
              element: element
              event: evt
            offset = element.offset()
            element.centerX = element.width() / 2
            element.centerY = element.height() / 2
            element.addClass 'dragging'
            _mx = evt.pageX or evt.originalEvent.touches[0].pageX
            _my = evt.pageY or evt.originalEvent.touches[0].pageY
            _mrx = _mx - (offset.left)
            _mry = _my - (offset.top)
            if _centerAnchor
              _tx = _mx - (element.centerX) - $window.scrollLeft()
              _ty = _my - (element.centerY) - $window.scrollTop()
            else
              _tx = offset.left - $window.scrollLeft()
              _ty = offset.top - $window.scrollTop()
            moveElement _tx, _ty
            $document.on _moveEvents, onmove
            $document.on _releaseEvents, onrelease
            $rootScope.$broadcast 'draggable:start',
              x: _mx
              y: _my
              tx: _tx
              ty: _ty
              event: evt
              element: element
              data: _data
            return

          onmove = (evt) ->
            if !_dragEnabled
              return
            evt.preventDefault()
            _mx = evt.pageX or evt.originalEvent.touches[0].pageX
            _my = evt.pageY or evt.originalEvent.touches[0].pageY
            if _centerAnchor
              _tx = _mx - (element.centerX) - $window.scrollLeft()
              _ty = _my - (element.centerY) - $window.scrollTop()
            else
              _tx = _mx - _mrx - $window.scrollLeft()
              _ty = _my - _mry - $window.scrollTop()
            moveElement _tx, _ty
            $rootScope.$broadcast 'draggable:move',
              x: _mx
              y: _my
              tx: _tx
              ty: _ty
              clientX: _mx - $window.scrollLeft()
              clientY: _my - $window.scrollTop()
              event: evt
              element: element
              data: _data
            return

          onrelease = (evt) ->
            if !_dragEnabled
              return
            evt.preventDefault()
            $rootScope.$broadcast 'draggable:end',
              x: _mx
              y: _my
              tx: _tx
              ty: _ty
              event: evt
              element: element
              data: _data
              callback: onDragComplete
            element.removeClass 'dragging'
            reset()
            $document.off _moveEvents, onmove
            $document.off _releaseEvents, onrelease
            return

          onDragComplete = (evt) ->
            if !onDragSuccessCallback
              return
            scope.$apply ->
              onDragSuccessCallback scope,
                $data: _data
                $event: evt
              return
            return

          reset = ->
            element.css
              left: ''
              top: ''
              position: ''
              'z-index': ''
              margin: ''
            return

          moveElement = (x, y) ->
            element.css
              left: x
              top: y
              position: 'fixed'
              'z-index': 99999
              margin: '0'
            return

          initialize()
          return

      }
  ]).directive('ngDrop', [
    '$parse'
    '$timeout'
    ($parse, $timeout) ->
      {
        restrict: 'A'
        link: (scope, element, attrs) ->
          scope.value = attrs.ngDrop
          _dropEnabled = false
          onDropCallback = $parse(attrs.ngDropSuccess)
          # || function(){};

          initialize = ->
            toggleListeners true
            return

          toggleListeners = (enable) ->
            # remove listeners
            if !enable
              return
            # add listeners.
            attrs.$observe 'ngDrop', onEnableChange
            scope.$on '$destroy', onDestroy
            #scope.$watch(attrs.uiDraggable, onDraggableChange);
            scope.$on 'draggable:start', onDragStart
            scope.$on 'draggable:move', onDragMove
            scope.$on 'draggable:end', onDragEnd
            return

          onDestroy = (enable) ->
            toggleListeners false
            return

          onEnableChange = (newVal, oldVal) ->
            _dropEnabled = scope.$eval(newVal)
            return

          onDragStart = (evt, obj) ->
            if !_dropEnabled
              return
            isTouching obj.x, obj.y, obj.element
            return

          onDragMove = (evt, obj) ->
            if !_dropEnabled
              return
            isTouching obj.x, obj.y, obj.element
            return

          onDragEnd = (evt, obj) ->
            if !_dropEnabled
              return
            if isTouching(obj.x, obj.y, obj.element)
              element.removeClass 'drag-enter'
              # call the ngDraggable ngDragSuccess element callback
              if obj.callback
                obj.callback obj
              # call the ngDrop element callback
              #   scope.$apply(function () {
              #       onDropCallback(scope, {$data: obj.data, $event: evt});
              #   });
              onDropCallback scope,
                $data: obj.data
                $event: obj
              return
            updateDragStyles false, obj.element
            return

          isTouching = (mouseX, mouseY, dragElement) ->
            touching = hitTest(mouseX, mouseY)
            updateDragStyles touching, dragElement
            touching

          updateDragStyles = (touching, dragElement) ->
            if touching
              element.addClass 'drag-enter'
              dragElement.addClass 'drag-over'
            else
              element.removeClass 'drag-enter'
              dragElement.removeClass 'drag-over'
            return

          hitTest = (x, y) ->
            bounds = element.offset()
            bounds.right = bounds.left + element.outerWidth()
            bounds.bottom = bounds.top + element.outerHeight()
            x >= bounds.left and x <= bounds.right and y <= bounds.bottom and y >= bounds.top

          initialize()
          return

      }
  ]).directive('ngPreventDrag', [
    '$parse'
    '$timeout'
    ($parse, $timeout) ->
      {
        restrict: 'A'
        link: (scope, element, attrs) ->

          initialize = ->
            element.attr 'draggable', 'false'
            toggleListeners true
            return

          toggleListeners = (enable) ->
            # remove listeners
            if !enable
              return
            # add listeners.
            element.on 'mousedown touchstart touchmove touchend touchcancel', absorbEvent_
            return

          absorbEvent_ = (event) ->
            e = event.originalEvent
            e.preventDefault and e.preventDefault()
            e.stopPropagation and e.stopPropagation()
            e.cancelBubble = true
            e.returnValue = false
            false

          initialize()
          return

      }
  ]).directive('ngCancelDrag', [ ->
    {
      restrict: 'A'
      link: (scope, element, attrs) ->
        element.find('*').attr 'ng-cancel-drag', 'ng-cancel-drag'
        return

    }
   ]).directive 'ngDragScroll', ($window, $interval, $timeout, $document, $rootScope) ->
      {
        restrict: 'A'
        link: (scope, element, attrs) ->
          intervalPromise = null
          lastMouseEvent = null
          config =
            verticalScroll: attrs.verticalScroll or true
            horizontalScroll: attrs.horizontalScroll or true
            activationDistance: attrs.activationDistance or 75
            scrollDistance: attrs.scrollDistance or 50
            scrollInterval: attrs.scrollInterval or 250

          createInterval = ->
            intervalPromise = $interval((->
              if !lastMouseEvent
                return
              viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth or 0)
              viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight or 0)
              scrollX = 0
              scrollY = 0

              if config.horizontalScroll
                # If horizontal scrolling is active.
                if lastMouseEvent.clientX < config.activationDistance
                  # If the mouse is on the left of the viewport within the activation distance.
                  scrollX = -config.scrollDistance
                else if lastMouseEvent.clientX > viewportWidth - (config.activationDistance)
                  # If the mouse is on the right of the viewport within the activation distance.
                  scrollX = config.scrollDistance
              if config.verticalScroll
                # If vertical scrolling is active.
                if lastMouseEvent.clientY < config.activationDistance
                  # If the mouse is on the top of the viewport within the activation distance.
                  scrollY = -config.scrollDistance
                else if lastMouseEvent.clientY > viewportHeight - (config.activationDistance)
                  # If the mouse is on the bottom of the viewport within the activation distance.
                  scrollY = config.scrollDistance
              if scrollX != 0 or scrollY != 0
                # Record the current scroll position.
                currentScrollLeft = $document[0].documentElement.scrollLeft
                currentScrollTop = $document[0].documentElement.scrollTop
                # Remove the transformation from the element, scroll the window by the scroll distance
                # record how far we scrolled, then reapply the element transformation.
                elementTransform = element.css('transform')
                element.css 'transform', 'initial'
                $window.scrollBy scrollX, scrollY
                horizontalScrollAmount = $document[0].documentElement.scrollLeft - currentScrollLeft
                verticalScrollAmount = $document[0].documentElement.scrollTop - currentScrollTop
                element.css 'transform', elementTransform
                # On the next digest cycle, trigger a mousemove event equal to the amount we scrolled so
                # the element moves correctly.
                $timeout ->
                  lastMouseEvent.pageX += horizontalScrollAmount
                  lastMouseEvent.pageY += verticalScrollAmount
                  $rootScope.$emit 'draggable:_triggerHandlerMove', lastMouseEvent
                  return
              return
            ), config.scrollInterval)
            return

          clearInterval = ->
            $interval.cancel intervalPromise
            intervalPromise = null
            return

          scope.$on 'draggable:start', (event, obj) ->
            # Ignore this event if it's not for this element.
            if obj.element[0] != element[0]
              return
            if !intervalPromise
              createInterval()
            return
          scope.$on 'draggable:end', (event, obj) ->
            # Ignore this event if it's not for this element.
            if obj.element[0] != element[0]
              return
            if intervalPromise
              clearInterval()
            return
          scope.$on 'draggable:move', (event, obj) ->
            # Ignore this event if it's not for this element.
            if obj.element[0] != element[0]
              return
            lastMouseEvent =
              clientX: obj.clientX
              clientY: obj.clientY
            return
          return

      }
