define ['../app', 'angular'], (app, angular) ->
  app.service 'confirmerDlg', ($uibModal) ->
    (text, cb, options = {}) -> $uibModal.open
      templateUrl: 'misc/partials/confirmer.html'
      controller: ($scope, $uibModalInstance) ->
        options = angular.extend options,
          useTranslation: true

        angular.extend $scope,
          options: options
          text: text
          close: ->
            $uibModalInstance.close()
          no: ->
            $uibModalInstance.close()
          yes: ->
            cb()
            $uibModalInstance.close()
