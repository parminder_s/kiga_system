define ['../app'], (app) ->
  app.service 'userService', (request) ->
    userList = null
    getAll: (cb) ->
      request
        url: "user/list"
      .then (users)->
        userList = users
        if cb
          cb users
        else
          users
    get: (id)->
      if not userList
        @getAll()
      else
        userList
