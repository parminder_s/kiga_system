define ['../app'], (app) ->
  app.service 'goHome', ($state, config) -> (reload = false) ->
    $state.go('root.home', {}, {reload: reload})
