###
  This is the serializer function which is used by the request service and
  serializes javascript objects, e. g. momentJs objects and dates to strings.
###

define ['../../app', 'lodash', 'moment'], (app, _, moment) ->
  app.service 'deserialize', ->
    returner = (obj) ->
      if $.isArray obj
        _.map obj, returner
      else if $.type(obj) is 'object'
        for own key, value of obj
          if key.match(/Date$/) or key.match(/DateTime$/)
            obj[key] = moment(value).toDate()
          else if $.type(value) is 'object' or $.isArray value
            obj[key] = returner value
        obj
      else
        obj

    returner
