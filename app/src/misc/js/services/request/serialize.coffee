###
  This is the serializer function which is used by the request service and
  serializes javascript objects, e. g. momentJs objects and dates to strings.
###

define ['../../app', 'lodash', 'moment'], (app, _, moment) ->
  app.service 'serialize', ->
    returner = (obj) ->
      if $.isArray obj
        _.map obj, returner
      else if $.type(obj) is 'date'
        moment(obj).format 'YYYY-MM-DD'
      else if $.type(obj) is 'object'
        if obj._isAMomentObject?
          obj.format 'YYYY-MM-DD'
        else
          _.fromPairs _.map obj, (value, key) ->
            [key, returner value]
      else
        obj

    returner
