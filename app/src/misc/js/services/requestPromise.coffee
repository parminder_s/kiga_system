###

@author Rainer Hahnekamp

Request which should be used instead of $http. Internally it comes down
to a $http call but it has some additional features:
It de- and serializes date types and also momentJs objects. Deserialization
works that it checks each property name if it ends with a Date or DateTime.

It prepends the mandatory '/api' to the url, sends the current locale and
sets the correct content type if POST is used.

POST is the default method but can be changed. All data is transformed to
json. JSON has the big advantage over generic HTTP parameters that one
can send hierarchical data structures and also data types.

If an error is returned, by default the user is shown an information
dialog with a localized user-friendly error message.

A loading gif is shown during the request and the user is virtually
blocked by the user interface by default.

If a redirection should take place, request also handles that automatically.

It returns the success promise, but shouldn't be used where the caller
requires a real promise. For promise returns use requestPromise
or $http directly.

Request expects as second and third argument callbacks which stand
for success and failed response respectively.
###

define ['../app', 'jquery'], (app, $) ->
  app.service 'requestPromise', ['$q', 'request', '$log'].concat ($q, request, $log) ->
    (options) ->
      $log.warn 'usage of requestPromise is deprecated, use request only'
      deferred = $q.defer()
      success = (result) -> deferred.resolve(result)
      error = (error) -> deferred.resolve(error)
      request(options, success, error)
      deferred.promise
