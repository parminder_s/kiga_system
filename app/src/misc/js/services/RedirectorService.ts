import { StateService } from 'angular-ui-router';
export default class RedirectorService {
  private _toBeRedirected = true;

  constructor(private $state: StateService, private $stateParams) {}

  setRedirected(): void {
    this._toBeRedirected = false;
  }

  toBeRedirected(): boolean {
    return this._toBeRedirected;
  }

  redirectTo(forward, params = {}): void {
    this._toBeRedirected = true;
    if (forward.indexOf('.') === -1) {
      this.$state.go('redirector', {
        forward: forward
      });
    } else {
      params['locale'] = this.$stateParams.locale;
      this.$state.go('redirector', {
        forward: this.$state.href(forward, params)
      });
    }
  }
}
