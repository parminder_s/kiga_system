define ['../app', 'angular', 'lodash'], (app,angular,_) ->
  app.service 'scrollUpService', ($anchorScroll) ->
    scroll: () ->
      $anchorScroll()
