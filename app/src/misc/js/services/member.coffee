define ['../app', 'angular'], (app, angular) ->
  app.service 'member', (config) ->
    new class Member
      memberData: null
      set: (data) ->
        @memberData = data
        angular.extend this, data
      get: (field) ->
        if field
          this[field]
        else
          this
      getAll: -> @memberData
      isLoggedIn: -> not @isAnonymous
      hasTestSubscription: -> @subscription?.group is 'test'
      hasStandardSubscription: -> @subscription?.group is 'standard'
      hasExpiredSubscription: -> @subscription?.isExpired
      hasFullSubscription: -> !!@subscription?.accessPoolIdeas
      hasActiveFullSubscription: -> !!@subscription?.accessPoolIdeas && !@subscription.isExpired
      hasActiveTestSubscription: -> @subscription?.group is 'test' && !@subscription.isExpired


