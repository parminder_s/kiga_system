###*
  @ngdoc service

  @name formHelper

  @description

  Is a helper service for controllers that serve up a standard form.

  It extends it with cancel functionality.
###

define ['../app', 'angular'], (app, angular) ->
  app.service 'formHelper', ($state, $window) ->
    (defaultState = '', params = {}) ->
      if defaultState
        $state.go defaultState, params
      else
        $window.location.href = '/'
