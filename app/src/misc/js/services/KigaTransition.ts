export default class KigaTransition {
  private date: Date = new Date();

  constructor(private state, private params, private fromUrl, private toUrl) {}

  public getFromUrl() {
    return this.fromUrl;
  }

  public getToUrl() {
    return this.toUrl;
  }
}
