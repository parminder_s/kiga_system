export interface Loader {
  show();

  hide();
}
