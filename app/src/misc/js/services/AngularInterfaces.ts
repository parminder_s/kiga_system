import IMember from '../../permission/js/IMember';

export interface Link {
  label: string;
  code: string;
  url: string;
  inNavbar: boolean;
}

export interface ILinksService {
  set(links: Array<Link>): void;

  getTypes(): {
    navbar: any[];
    userMenu: any;
    mainArticleCategory: any;
    mainArticleSearch: any;
    footerPages: any[];
  };

  findByType(type): any;

  findByCode(code: string): any;
}

export enum Feature {
  GRIDVIEWWITHCOMPONENTNS = 'gridViewWithComponentsEnabled',
  SHOW_QUICK_REGISTERS = 'showQuickRegisters',
  ANGULAR_CMS_ENABLED = 'angularCmsEnabled'
}

export interface IConfigService {
  getConfigData(): {};

  get(key: string): any;

  set(data): void;

  loadNewData(data): void;

  getPermissionMode(): string;

  getNgLiveUrl(): string;

  isFeatureEnabled(feature: Feature): boolean;

  getIsLoadedPromise(): Promise<void>;
}

export interface IBasketItemsService {
  setNumberOfItems(value: number): void;

  increase(amount: number): void;

  getNumberOfItems(): number;
}

export type LocaleKeyPair = { [key: string]: string };

export interface BootstrapResponse {
  locale: string;
  locales: Array<LocaleKeyPair>;
}

export interface IBootstrapper {
  bootstrap(locale: string): Promise<BootstrapResponse>;
}

export interface ILocaleData {
  get(locale: string): Array<LocaleKeyPair>;
}

export interface IMemberLoader {
  getPromise(): Promise<IMember>;
  invalidate(): void;
}

export interface IGoBack {
  goBack(): void;
}

export interface INgxNavigateService {
  navigateByUrl(url: string);
}

export interface IFavouritesService {
  toggle(ideaId: number);
  notifyForUpdate(cb: () => void);
}
