export default class AudioHandler {
  private list: Array<HTMLAudioElement> = [];

  addAudio(audioObj: HTMLAudioElement) {
    this.list.push(audioObj);
  }

  flushMusic() {
    this.list.forEach(audioObj => {
      audioObj.pause();
      audioObj.currentTime = 0;
    });
    this.list = [];
  }
}
