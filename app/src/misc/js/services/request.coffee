###*
  * @ngdoc service
  * @name misc.request
  * @description
  * Wrapper method for $http which has some additional features:
  * It de- and serializes date types and also momentJs objects. Deserialization
  * works that it checks each property name if it ends with a Date or DateTime.
  *
  * It prepends the mandatory '/api' to the url, sends the current locale and
  * sets the correct content type if POST is used.
  *
  * POST is the default method but can be changed. All data is transformed to
  * json. JSON has the big advantage over generic HTTP parameters that one
  * can send hierarchical data structures and also data types.
  *
  * If an error is returned, by default the user is shown an information
  * dialog with a localized user-friendly error message.
  *
  * A loading gif is shown during the request and the user is virtually
  * blocked by the user interface by default.
  * if quietMode parameter passed, then loader not shown
  *
  * If a redirection should take place, request also handles that automatically.
  *
  * If no success and error parameter are passed it returns a promise.
  * Otherwise it executes success and error callbacks on $http's success and
  * error methods.
###

define ['../app', 'angular', 'moment'], (app, angular, moment) ->
  app.service 'request', ['$http', 'locale', 'misc.showException',
  'loader', 'deserialize', 'serialize', '$state', 'redirectorService'].
  concat ($http, locale, showException, loader, deserialize, serialize, $state, redirectorService) ->
    (options, success, error) ->
      quietMode = options.quietMode or false
      loader.show() if not quietMode

      if $.type(options) is 'string'
        options = url: options

      if !options.rawUrl
        if options.endpoint
          options.url = "#{kiga.endPointUrl}#{options.url}"
        else
          options.url = "/api/#{options.url}"

      options.method = 'POST' unless options.method
      options.withCredentials = true
      options.data = $.extend locale: locale.get(),
        serialize options.data

      if options.method is 'POST'
        options.responseType = 'json'
        if options.files
          options.headers = 'Content-Type': undefined
          data = new FormData
          options.transformRequest = angular.indentity
          data.append key,value for key, value of options.data

          typeIsArray = Array.isArray || (value) ->
            return {}.toString.call(value) is '[object Array]'

          _.each options.files, (filesContainer, name) ->
            if typeIsArray filesContainer
              _.each filesContainer, (singleFile) ->
                data.append name, singleFile
            else
              data.append name, filesContainer
        else
          options.headers = 'Content-Type': 'application/json'
          data = angular.toJson options.data
        options.data = data

      promise = $http options
      promise.finally -> loader.hide() if not quietMode
      if error
        promise.error error
      else promise.error (data) ->
        return if quietMode
        if data? and data.errorCode?
          if data.errorCode == "login_required"
            $state.go 'root.login', locale: locale.get()
          else if data.errorCode == "parallel_login"
            redirectorService.redirectTo('root.parallelLogInfo', {entryId: data.entryId});
          else if data.errorCode == "crm_session_fail"
            $state.go 'root.login', { locale: locale.get(), forward: 'root.member.account' }
          else
            showException data.errorCode
        else
          showException()

      parse = (response) ->
        if response
          data = deserialize response
          if data.redirect?
            window.location.href = data.redirect
          data
        else response

      if success
        promise.success(parse).success success
      else
        promise.then (response) ->
          parse response.data
