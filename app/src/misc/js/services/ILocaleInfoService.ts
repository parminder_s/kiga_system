export interface ILocaleInfoService {
  getLocale(): string;
}
