export default class CustomTranslationTable {
  private table: object;

  constructor() {
    this.table = {};
  }

  getEntry(key: string) {
    return this.table[key];
  }

  addEntry(key: string, value: object) {
    this.table[key] = value;
  }
}
