export default class CookieInfoService {
  agreedCookieUse = false;
  footerClassName = 'cookie-not-accepted';

  constructor(private $rootScope) {}

  getCookie() {
    let decodedCookie = decodeURIComponent(document.cookie);
    let splittedCookie = decodedCookie.split(';');
    for (let i = 0; i < splittedCookie.length; ++i) {
      if (splittedCookie[i].match('agreedCookieUse')) {
        return splittedCookie[i];
      }
    }
    return '';
  }

  checkIfCookieIsContained() {
    let agreedCookie = this.getCookie();
    if (agreedCookie != '') {
      this.agreedCookieUse = true;
      this.setClassNameForFooterToEmptyString();
    } else {
      this.footerClassName = 'cookie-not-accepted';
    }
  }

  getAgreedCookieUse() {
    return this.agreedCookieUse;
  }

  setClassNameForFooterToEmptyString() {
    this.footerClassName = '';
  }

  getClassNameForFooter() {
    this.checkIfCookieIsContained();
    return this.footerClassName;
  }

  emitAgreementOfCookieUsage() {
    this.$rootScope.$emit('agreedToCookieUse', {});
  }
}
