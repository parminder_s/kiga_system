import { IConfigService } from './AngularInterfaces';
import TransitionHistoryService from './TransitionHistoryService';
import KigaTransition from './KigaTransition';
import IIntervalService = angular.IIntervalService;

export default class {
  private lastIndex = 0;

  constructor(
    private transitionHistoryService: TransitionHistoryService,
    private endpoint,
    private $interval: IIntervalService,
    private config: IConfigService
  ) {}

  public startHistoryLogging() {
    this.config.getIsLoadedPromise().then(() => {
      if (this.config.get('logTransitions')) {
        this.$interval(() => {
          this.submitRemaining();
        }, 10000);
      }
    });
  }

  public submitRemaining() {
    let data = this.transitionHistoryService.getFromIndex(this.lastIndex);
    this.lastIndex += data.length;
    this.doSubmit(data);
  }

  private doSubmit(data: KigaTransition[]) {
    if (!data || data.length === 0) {
      return;
    }
    let newData = [];

    for (let trans of data) {
      newData.push({ toUrl: trans.getToUrl(), fromUrl: trans.getFromUrl() });
    }

    this.endpoint({
      url: 'log/logTransition',
      data: { transitions: newData },
      quietMode: true
    });
  }
}
