import { IConfigService } from './AngularInterfaces';

export default class MockedCountryService {
  public countries: Array<any>;

  constructor(private endpoint, private config: IConfigService) {}

  isEnabled() {
    return this.config.get('changeCountryViaCookie');
  }

  isMocked(): boolean {
    if (!this.isEnabled()) {
      return false;
    }

    return !!this.getCookieValue();
  }

  getMockedCountry(): string {
    if (this.isMocked()) {
      return this.getCookieValue();
    }
  }

  switchCountry(code) {
    this.endpoint({ url: 'countries/switch', data: { newCountryCode: code } }).then(() =>
      window.location.reload()
    );
  }

  remove() {
    this.endpoint({ url: 'countries/remove' }).then(() => window.location.reload());
  }

  private getCookieValue(): string {
    let matches = document.cookie.match(/countryCode=(\w+)/);
    if (matches) {
      return matches[1];
    } else {
      return null;
    }
  }
}
