define ['../app', 'moment'], (app, moment) ->
  app.service 'now', -> -> moment()
