###*
  @ngdoc service
  @name recursivePromise
  @description

  This service provides a loader for a or multiple promises that call
  promises on their resolves again, e.g. recursively.

  It needs a callback function which it executes on every promise. The return
  value of this callback function can be one or multiple promises which causes
  the service to load those again.
  If the callback function returns null the final promise is resolved with
  all results from the former promises within an array.

  It does so by creating a dictionary which keys are the promises and its
  boolean value indicates if it has been already resolved.

  For an example check the unit tests.
###

define ['../app', 'angular'], (app, angular) ->
  app.service 'recursivePromise', ($q) -> (initPromise, callback) ->
    deferred = $q.defer()
    resolvedPromises = {}
    results = []
    counter = 0

    handlePromise = (promise) ->
      counter++
      doResolve promise, counter


    doResolve = (promise, counter) ->
      resolvedPromises[counter] = false
      promise.then (data) ->
        results.push data
        resolvedPromises[counter] = true
        promises = callback data
        if promises
          promises = [promises] if !angular.isArray promises
          _.forEach promises, (promise) ->
            handlePromise promise
        else if promisesResolved()
          deferred.resolve results
      , (error) ->
        deferred.resolve ''

    promisesResolved = -> _.every resolvedPromises

    handlePromise initPromise
    deferred.promise
