define ['jquery'], ($) ->

  #--- custom checkbox's ---
  (($) ->
    CustomCheckbox = (thisDOMObj, config) ->
      @checkbox = jQuery(thisDOMObj)
      if @checkbox.data('CustomCheckbox') and typeof @checkbox.data('CustomCheckbox')[config] is 'function' # call api function
        @checkbox.data('CustomCheckbox')[config]()
      else unless typeof config is 'string' # init custom checkbox
        # default options
        @options = jQuery.extend(
          checkboxStructure: '<div></div>' # HTML struct for custom checkbox
          checkboxDisabled: 'disabled' # disabled class name
          checkboxDefault: 'checkboxArea' # default class name
          checkboxChecked: 'checkboxAreaChecked' # checked class name
          hideClass: 'outtaHere' # hide class for checkbox
          onInit: null # oninit callback
          onChange: null # onchage callback
        , config)
        @init()
      this
    'use strict'
    CustomCheckbox:: =

      # init function
      init: ->

        # add api in data checkbox
        @checkbox.data 'CustomCheckbox', this
        @createElements()
        @createStructure()
        @attachEvents()
        @checkbox.addClass @options.hideClass

        # init callback
        @options.onInit @getUI()  if typeof @options.onInit is 'function'

      getUI: ->
        checkbox: @checkbox[0]
        fakecheckbox: @fakecheckbox


      # attach events and listeners
      attachEvents: ->
        @clickEvent = @bindScope((event) ->
          unless event.target is @checkbox[0]
            if @checkbox[0].checked
              @checkbox.removeAttr 'checked'
              @checkbox[0].checked = false
            else
              @checkbox.attr 'checked', 'checked'
              @checkbox[0].checked = true
          @toggleState()

          # change callback
          @options.onChange event, @getUI()  if typeof @options.onChange is 'function'
        )
        @fakeCheckbox.on click: @clickEvent
        @checkbox.on click: @clickEvent


      # checked or disabled checkbox
      toggleState: ->
        @fakeCheckbox.removeAttr('class').addClass @options[(if @checkbox[0].checked then 'checkboxChecked' else 'checkboxDefault')]


      # create api elements
      createElements: ->
        @fakeCheckbox = jQuery(@options.checkboxStructure)


      # create custom checkbox struct
      createStructure: ->
        if @checkbox.is(':disabled')
          @fakeCheckbox.addClass @options.checkboxDisabled
        else if @checkbox.is(':checked')
          @fakeCheckbox.addClass @options.checkboxChecked
        else
          @fakeCheckbox.addClass @options.checkboxDefault
        @fakeCheckbox.insertBefore @checkbox


      # api update function
      update: ->
        @fakeCheckbox.detach()
        @fakeCheckbox = jQuery(@options.checkboxStructure)
        @checkbox.off 'click', @clickEvent
        @createStructure()
        @attachEvents()

        # init callback
        @options.onInit @getUI(), true  if typeof @options.onInit is 'function'


      # api destroy function
      destroy: ->
        @fakeCheckbox.detach()
        @checkbox.removeClass @options.hideClass
        @checkbox.off 'click', @clickEvent
        @checkbox.removeData 'CustomCheckbox'

      bindScope: (func, scope) ->
        jQuery.proxy func, scope or this

    jQuery.fn.customCheckbox = (config) ->
      @each ->
        new CustomCheckbox(this, config)

  ) jQuery
