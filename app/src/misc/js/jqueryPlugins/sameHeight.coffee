###

@author Rainer Hahnekamp

This jquery plugin adapts sets the height of a block element
to the same height of a reference block element.
A typical use case is in RWD where you have a div next to an image.
On resizing the image grows in height and the div stays the same. This doesn't
look nice when the div has borders. This plugins would also resize the div so
that the height is always the same.

Usage:
Since the reference element is not a child or direct parent one has the
possibility to select a parent element which includes both the reference and
also the element to be resized.
The refParent is a selector string that selects the parent element.
The refElement is the selector string for the reference element which
height should be adapted. The selector is called on the refParent element.
The extender is the selector for child element which is used to adapt the
height, e. g. setting the height.

Example:

Here we resize the div which is right to an image which can change its
height when the screen is resized.

HTML:
<div class="row">
  <div class="img-holder" style="float: left;">
    <img src="some.jpg">
  </div>
  <div style="border: 1px solid black;" data-item="theDiv">
    <p>Some text here</p>
    <div class="extender"></div>
  </div>
</div>
JS:
$('.theDiv).sameHeight({
  refParent: '.row',
  refElement: '.img-holder',
  extender: '.extender'
});

###

define ['jquery', 'lodash'], ($, _) ->
  class SameHeight
    constructor: (options) ->
      if options.selector?
        options.selectors = [options.selector]
      if options.extender?
        options.extender = options.holder.find options.extender
      else
        options.extender = options.holder

      $.extend this, options

      $(window).resize =>
        @loadReference options
        @adaptHeight()

      #event which is triggerd if partials are loaded afterwards
      $(window).on 'domChanged', =>
        @loadReference options
        @adaptHeight()

      @loadReference options
      @adaptHeight()

    loadReference: (options) ->
      @references = options.holder
        .parents(options.refParent)
        .find "#{options.refElement}:visible"

      #if img is part of reference adapt if img is loaded
      if @references.length
        @references.find('img').load => @adaptHeight()

    existingHeight: (el) ->
      height = el.css 'height'
      if height
        match = height.match /(\d+)/
        parseInt match[0]
      else 0

    adaptHeight: ->
      if @references and @references.length
        diff = @referenceHeight() - @holder.outerHeight()
        extenders = @visibleExtenders()
        if extenders.length is 1
          if @adaptSingleExtender diff, extenders
            @extender.trigger 'resized'
        else
          throw new Exception 'multiple extenders'

    adaptSingleExtender: (diff, extender) ->
      existingHeight = @existingHeight $ extender
      newHeight = Math.max 0, diff + existingHeight
      if $(extender).css('height') isnt "#{newHeight}px"
        $(extender).css 'height', "#{newHeight}px"
        true
      else
        false

    visibleExtenders: ->
      _.filter @extender, (e) -> $(e).is ':visible'

    referenceHeight: ->
      _.max _.map(@references, (ref) -> $(ref).outerHeight())

  $.fn.sameHeight = (opt) -> $(this).data 'SameHeight',
    new SameHeight $.extend opt, holder: this
