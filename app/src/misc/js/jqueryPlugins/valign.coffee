###

@author Rainer Hahnekamp

This jquery plugin vertically aligns block elements which
is not possible in generic HTML.

The plugin needs to be called on a container element. One of the container's
children should have a data-valign=el attribute. That child is centered
vertically by setting a padding-top value.

If the total height should not be calculated from the container element
one can set one of its child elements as container. This is done by
setting data-valign=ctr.

Example:
1) Vertically aligns an image within a div:
<div>
  <img src="some.jpg" data-valign="el"
</div>

2) Vertically aligns an text within a div, but uses
a link element inside the div as container:

<div>
  <a href="#" data-valign="ctr">
    <h2 data-valign="el">Valign me</h2>
  </a>
</div>

###

define ['jquery'], ($) ->
  class Valign
    constructor: (options) ->
      $.extend this, options
      @el = @holder.find '[data-valign=el]' || @holder
      @ctr = @holder.find '[data-valign=ctr]' || @holder

      @holder.on 'resized', => @middleHolder()
      @middleHolder()

    middleHolder: ->
      padding = Math.floor((@holder.height() - @el.height()) / 2)
      @ctr.css
        paddingTop: "#{padding}px"
        paddingBottom: 0

  $.fn.valign = (opt) -> $(this).data 'Valign',
    new Valign $.extend opt, holder: this
