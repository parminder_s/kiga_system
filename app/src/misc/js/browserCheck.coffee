define ['jquery', 'jquery.browser'], ($) ->
  $ -> if $.browser.name is 'msie' && $.browser.versionNumber <= 10
    language = location.href.match /#\!\/(\w+)\/?/
    locale = if language
      language[1]
    else
      'de'

    $.ajax
      type: 'POST'
      url:  '/api/main/browserCheck'
      contentType: 'application/json'
      dataType: 'json'
      data: JSON.stringify({"locale": locale})
      success: (texts) ->
        infoBox = $ """
          <div id='browsercheck' style=\"background-color: grey; max-width: 650px; margin: auto; margin-top: 130px;\">
            <p style=\"font-size: 25px; color: white; text-align: center; padding: 20px;\">#{texts.info}</p>
            <p id= "bcclose" style=\"font-size: 25px; color: white; text-align: center; padding: 20px;\">#{texts.close}</p>
           </div>
        """
        infoBox.prependTo 'body'
        $("#bcclose").click () ->
          $("#browsercheck").remove()
        $('body').removeAttr 'ng-cloak'
        $('img.splash').hide();
