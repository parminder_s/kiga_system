package com.kiga.db.fixture

/**
  * Created by rainerh on 17.03.16.
  */
case class EntityJoinByKey
(
  entity: Entity,
  originalEntity: OriginalEntity
)
