package com.kiga.db.fixture

/**
  * Created by rainerh on 13.03.16.
  */
class MissingEntityKeyException(msg: String) extends RuntimeException {
  override def getMessage(): String = {
    msg
  }
}
