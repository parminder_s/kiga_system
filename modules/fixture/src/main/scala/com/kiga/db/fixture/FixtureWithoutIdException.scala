package com.kiga.db.fixture

/**
  * Created by rainerh on 20.12.15.
  */
class FixtureWithoutIdException(msg: String) extends RuntimeException {
  override def getMessage(): String = {
    msg
  }
}
