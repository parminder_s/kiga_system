package com.kiga.db.fixture

/**
  * Created by rainerh on 19.12.15.
  */
trait FixtureParser {
  def loadFixtureFromFile[T](clazz: Class[T], filename: String): Iterable[ParserResult[T]]
  def loadFixtureFromFile[T](clazz: Class[T]): Iterable[ParserResult[T]]
  def parse[T](yaml: String, clazz: Class[T]): ParserResult[T]
  def parseMultiple[T](yaml: String, clazz: Class[T]): Iterable[ParserResult[T]]
}
