package com.kiga.db.fixture

trait Entity {
  def getClassName(): java.lang.String
  def getId(): java.lang.Long
  def setId(id: java.lang.Long)
}
