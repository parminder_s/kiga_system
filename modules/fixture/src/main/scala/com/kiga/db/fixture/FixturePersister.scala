package com.kiga.db.fixture

/**
  * Created by rainerh on 19.12.15.
  */
trait FixturePersister {
  @throws(classOf[FixtureWithoutIdException])
  def persist[T <: Entity](parserResult: ParserResult[T]): T
  def persistEntityJoinsByKey(): Unit
}
