package com.kiga.db.fixture

import java.lang.reflect.Field

/**
  * Created by rainerh on 30.12.15.
  */

case class OriginalEntity
(
  field: Field,
  isKey: Boolean,
  id: Long,
  key: String
)
