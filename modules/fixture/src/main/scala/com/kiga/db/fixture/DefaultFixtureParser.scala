package com.kiga.db.fixture

import org.slf4j.LoggerFactory
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.BaseConstructor

import scala.collection.JavaConversions._
import scala.io.Source

/**
  * Created by rainerh on 19.12.15.
  */
class DefaultFixtureParser(constructor: BaseConstructor) extends FixtureParser {
  val logger = LoggerFactory.getLogger(getClass)

  val loader = if(constructor == null) new Yaml() else new Yaml(constructor)
  val entityJoinHelper = new EntityJoinHelper

  def loadFixtureFromFile[T](clazz: Class[T], filename: String): Iterable[ParserResult[T]] = {
    val resourceName = if (filename.equals("")) {
      s"/fixtures/${clazz.getSimpleName}.yml"
    }
    else {
      s"/fixtures/${filename}.yml"
    }
    logger.debug("Loading file " + resourceName)
    parseMultiple[T](
      Source.fromInputStream(
        getClass.getResourceAsStream(resourceName), "UTF-8"
      ).mkString, clazz)
  }

  def loadFixtureFromFile[T](clazz: Class[T]): Iterable[ParserResult[T]] = {
    loadFixtureFromFile[T](clazz, "")
  }

  def parse[T](yaml: String, clazz: Class[T]) = {
    yamlToParserResult[T](yaml, clazz)
  }

  def parseMultiple[T](yaml: String, clazz: Class[T]): Iterable[ParserResult[T]] = {
    loader
      .loadAll(yaml)
      .map(entity => loader.dump(entity))
      .map(yaml => yamlToParserResult[T](yaml, clazz))
  }

  def yamlToParserResult[T](yaml: String, clazz: Class[T]): ParserResult[T] = {
    val intermediateYaml = entityJoinHelper.getIntermediateYaml(yaml, clazz)
    ParserResult(
      loader.loadAs[T](intermediateYaml.yaml, clazz),
      intermediateYaml.entities,
      intermediateYaml.key
    )
  }
}
