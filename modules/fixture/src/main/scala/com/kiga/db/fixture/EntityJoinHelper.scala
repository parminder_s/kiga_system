package com.kiga.db.fixture

import java.lang.reflect.Field

import org.apache.commons.lang3.reflect.FieldUtils
import org.yaml.snakeyaml.Yaml

import scala.collection.JavaConversions
import scala.collection.JavaConversions._

/**
  * Created by rainerh on 29.12.15.
  */
class EntityJoinHelper {
  val parser = new Yaml
  val keyIndictator = "_"

  def getIntermediateYaml[T](yaml: String, clazz: Class[T]): YamlWithEntityPropMetadata = {
    findFieldsInYaml(yaml, findEntityFields(clazz), findJoinableFields(clazz))
  }

  def findFieldsInYaml(yaml: String, fieldsForId: List[Field], fieldsForKey: List[Field]): YamlWithEntityPropMetadata = {
    YamlWithEntityPropMetadata(
      yamlify(nullifyEntities(toYamlMap(yaml), fieldsMap(fieldsForId), fieldsMap(fieldsForKey))),
      fetchOriginalEntities(toYamlMap(yaml), fieldsMap(fieldsForId), fieldsMap(fieldsForKey)),
      getKey(toYamlMap(yaml))
    )
  }

  def toYamlMap(yaml: String): Map[String, Any] = {
    parser
      .loadAs(yaml, classOf[java.util.HashMap[String, Any]])
      .toMap
  }

  def getKey(yamlMap: Map[String, Any]): String = {
    if (yamlMap.contains("_key")) {
      yamlMap("_key").toString
    }
    else {
      ""
    }
  }

  def nullifyEntities(yamlMap: Map[String, Any], fieldsMapForId: Map[String, Field], fieldsMapForKey: Map[String, Field]): Map[String, Any] = {
    val returner = yamlMap
      .filter({
        case (key, value) => !fieldsMapForId.contains(key.toLowerCase)
      })
      .filter({
        case (key, value) => !value.toString.startsWith(keyIndictator)
      })
      .filter({
        case (key, value) => !key.equals("_key")
      })

    returner
  }

  def yamlify(yamlMap: Map[String, Any]): String = {
    new Yaml().dump(JavaConversions.mapAsJavaMap(yamlMap))
  }

  def fetchOriginalEntities(yamlMap: Map[String, Any], fieldsMapForId: Map[String, Field], fieldsMapForKey: Map[String, Field]) = {
    fetchOriginalEntitiesForId(yamlMap, fieldsMapForId).toList ++ fetchOriginalEntitiesForKey(yamlMap, fieldsMapForKey).toList
  }

  def fetchOriginalEntitiesForId(yamlMap: Map[String, Any], fieldsMap: Map[String, Field]): Iterable[OriginalEntity] = {
    yamlMap
      .filter({
        case (key, value) =>
          fieldsMap.keySet.contains(key.toLowerCase) && value != "" && !value.toString.startsWith(keyIndictator)
      })
      .map({
        case (key, value) =>
          OriginalEntity(
            fieldsMap.get(key.toLowerCase).get, false,
            value.asInstanceOf[java.lang.Integer].toLong, "")
      })
  }

  def fetchOriginalEntitiesForKey(yamlMap: Map[String, Any], fieldsMap: Map[String, Field]): Iterable[OriginalEntity] = {
    yamlMap
      .filter({
        case (key, value) =>
          fieldsMap.keySet.contains(key.toLowerCase) && value.toString.startsWith(keyIndictator)
      })
      .map({
        case (key, value) =>
          OriginalEntity(
            fieldsMap.get(key.toLowerCase).get, true,
            0L, value.toString.substring(1)
          )
      })
  }

  def fieldsMap(fields: List[Field]): Map[String, Field] = {
    fields.map(field => ((field.getName + "id").toLowerCase, field)).toMap
  }

  def findEntityFields[T](clazz: Class[T]): List[Field] = {
    FieldUtils.getAllFieldsList(clazz).filter(field => {
      Option(field.getType.getDeclaredAnnotation(classOf[javax.persistence.Entity])).isDefined
    }).toList
  }

  def findJoinableFields[T](clazz: Class[T]): List[Field] = {
    FieldUtils.getAllFieldsList(clazz).filter(field => {
      classOf[Entity].isAssignableFrom(field.getType)
    }).toList
  }
}
