package com.kiga.db.fixture

import org.slf4j.LoggerFactory

/**
  * Created by rainerh on 17.03.16.
  */
class EntityJoinerByKey(entityJoinsByKey: List[EntityJoinByKey], entityKeyMap: java.util.HashMap[String, Entity]) {
  def joinEntities(): Iterable[Entity] = {
    val logger = LoggerFactory.getLogger(getClass)
    entityJoinsByKey.map(entityJoinByKey => {
      val originalEntity = entityJoinByKey.originalEntity
      if (entityKeyMap.containsKey(originalEntity.key)) {
        val joinedEntity = entityKeyMap.get(originalEntity.key)
        logger.debug(s"Joining entity ${joinedEntity.getClassName} - ${joinedEntity.getId} to ${entityJoinByKey.entity.getClassName} - ${entityJoinByKey.entity.getId}")
        originalEntity.field.setAccessible(true)
        originalEntity.field.set(entityJoinByKey.entity, joinedEntity)
        entityJoinByKey.entity
      }
      else {
        throw new MissingEntityKeyException(s"Entity with Key ${originalEntity.key} is not available for ${entityJoinByKey.entity.getClassName} with id ${entityJoinByKey.entity.getId}")
      }
    })
  }
}
