package com.kiga.db.fixture

/**
  * Created by rainerh on 30.12.15.
  */
case class YamlWithEntityPropMetadata
(
  yaml: String,
  entities: Iterable[OriginalEntity],
  key: String
)
