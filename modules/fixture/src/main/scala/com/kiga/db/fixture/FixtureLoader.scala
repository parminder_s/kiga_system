package com.kiga.db.fixture

/**
 * Created by rainerh on 13.04.15.
 */
trait FixtureLoader {
  def loadFixturesFromFiles(): Unit
  def persist[T <: Entity](parserResult: ParserResult[T]): T
}
