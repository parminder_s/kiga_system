package com.kiga.db.fixture

/**
  * Created by rainerh on 30.12.15.
  */
case class ParserResult[T]
(
  entity: T,
  originalEntities: Iterable[OriginalEntity],
  key: String
)
