package com.kiga.db.fixture.fixture.domain

import javax.persistence.Entity

import scala.beans.BeanProperty

/**
  * Created by rainerh on 29.12.15.
  */
@Entity class Instrument extends com.kiga.db.fixture.Entity {
  this.setClassName("instrument")

  @BeanProperty var i: java.lang.Long = _
  @BeanProperty var title: String = _
  @BeanProperty var className: String = _

  def getId(): java.lang.Long = {
    this.i
  }

  def setId(id: java.lang.Long) = {
    this.i = id
  }

}
