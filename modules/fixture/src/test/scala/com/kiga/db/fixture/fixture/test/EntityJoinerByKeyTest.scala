package com.kiga.db.fixture.fixture.test

import com.kiga.db.fixture._
import com.kiga.db.fixture.fixture.domain.{Instrument, Musician}
import org.junit.{Assert, Test}

/**
  * Created by rainerh on 17.03.16.
  */
class EntityJoinerByKeyTest {
  @Test def defaultJoin(): Unit = {
    val zimerman = new Musician()
    val piano = new Instrument()
    piano.setTitle("Bösendorfer")
    val entityKeyMap = new java.util.HashMap[String, Entity]()
    entityKeyMap.put("piano", piano)

    val joinField = classOf[Musician].getDeclaredField("instrument")
    val originalEntity = OriginalEntity(joinField, true, 0, "piano")
    val entityJoinsByKey = List[EntityJoinByKey](EntityJoinByKey(zimerman, originalEntity))
    new EntityJoinerByKey(entityJoinsByKey, entityKeyMap).joinEntities()

    Assert.assertEquals("Bösendorfer", zimerman.getInstrument().getTitle())
  }

  @Test(expected = classOf[MissingEntityKeyException]) def nonExistingKey(): Unit = {
    val zimerman = new Musician()
    val piano = new Instrument()
    piano.setTitle("Bösendorfer")
    val entityKeyMap = new java.util.HashMap[String, Entity]()
    entityKeyMap.put("piano", piano)

    val joinField = classOf[Musician].getDeclaredField("instrument")
    val originalEntity = OriginalEntity(joinField, true, 0, "cello")
    val entityJoinsByKey = List[EntityJoinByKey](EntityJoinByKey(zimerman, originalEntity))
    new EntityJoinerByKey(entityJoinsByKey, entityKeyMap).joinEntities()
  }
}
