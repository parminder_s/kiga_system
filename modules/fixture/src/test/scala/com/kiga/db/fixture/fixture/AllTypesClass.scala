package com.kiga.db.fixture.fixture

import java.util.Date

import scala.beans.BeanProperty

/**
  * Created by rainerh on 05.01.16.
  */
class AllTypesClass {
  @BeanProperty var intType: Int = _
  @BeanProperty var doubleType: Double = _
  @BeanProperty var stringType: String = _
  @BeanProperty var datetimeType: Date = _
}
