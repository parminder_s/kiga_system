package com.kiga.db.fixture.fixture.repository

import com.kiga.db.fixture.fixture.domain.Musician
import org.springframework.data.repository.CrudRepository

/**
 * Created by rainerh on 01.12.15.
 */

trait MusicianRepository extends CrudRepository[Musician, java.lang.Long]
