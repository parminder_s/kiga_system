package com.kiga.db.fixture.fixture

import com.kiga.db.fixture._
import javax.persistence.{EntityManager, PersistenceContext}
import javax.transaction.Transactional
import org.springframework.beans.factory.annotation.Autowired

/**
  * Created by rainerh on 19.12.15.
  */
@Autowired @Transactional
class TestFixtureLoader extends FixtureLoader {
  @PersistenceContext(unitName = "fixture") var em: EntityManager = _
  var fixturePersister: FixturePersister = _

  def persist[T <: Entity](parserResult: ParserResult[T]): T = {
    getPersister.persist(parserResult)
  }

  def getPersister(): FixturePersister = {
    if (fixturePersister == null) {
      fixturePersister = new DefaultFixturePersister(em)
    }
    fixturePersister
  }

  override def loadFixturesFromFiles(): Unit = {}
}
