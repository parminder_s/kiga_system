package com.kiga.db.fixture.fixture

import com.kiga.db.fixture.fixture.domain.Musician
import com.kiga.db.fixture.fixture.repository.MusicianRepository
import javax.sql.DataSource
import org.springframework.beans.factory.annotation.{Autowired, Qualifier}
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.{Bean, ComponentScan, Primary}
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.{JpaTransactionManager, LocalContainerEntityManagerFactoryBean}

import scala.collection.JavaConversions._

/**
  * Created by rainerh on 19.12.15.
  */
@EnableAutoConfiguration
@SpringApplication
@ComponentScan
@EnableJpaRepositories(
  basePackageClasses = Array(
    classOf[MusicianRepository]
  ),
  entityManagerFactoryRef = "emfFixture",
  transactionManagerRef = "txFixture"
)
class FixtureConfig {
  @Primary @Bean def dataSource(): DataSource = {
    val jdbcUrl = "jdbc:hsqldb:mem:fixture;db_integration;sql.syntax_mys=true"
    DataSourceBuilder.create.url(jdbcUrl).build
  }

  @Primary @Bean(name = Array("emfFixture"))
  def emf(builder: EntityManagerFactoryBuilder): LocalContainerEntityManagerFactoryBean = {
    builder.
      dataSource(dataSource()).
      properties(Map("hibernate.hbm2ddl.auto" -> "create-drop")).
      packages(classOf[Musician]).
      persistenceUnit("fixture").
      build()
  }

  @Primary @Bean(name = Array("txFixture"))
  def tx(@Autowired @Qualifier("emfFixture") emf: LocalContainerEntityManagerFactoryBean) = {
    new JpaTransactionManager(emf.getObject)
  }
}

