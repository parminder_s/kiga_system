package com.kiga.db.fixture.fixture.domain

import javax.persistence._

import scala.beans.BeanProperty

/**
 * Created by rainerh on 01.12.15.
 */

@Entity @PrimaryKeyJoinColumn(name = "ID")
class Musician extends Person {
  this.setClassName("Musician")

  @BeanProperty var trainingYears: java.lang.Long = _

  @ManyToOne
  @JoinColumn(name = "teacherID", referencedColumnName = "id")
  @BeanProperty var teacher: Musician = _

  @ManyToOne
  @JoinColumn(name = "instrumentID", referencedColumnName = "id")
  @BeanProperty var instrument: Instrument = _
}

