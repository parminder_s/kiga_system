package com.kiga.db.fixture.fixture.test

import java.lang.reflect.Field

import com.kiga.db.fixture.fixture.domain.{Instrument, Musician}
import com.kiga.db.fixture.{DefaultFixtureParser, EntityJoinHelper}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by rainerh on 30.12.15.
  */
@RunWith(classOf[JUnitRunner])
class EntityJoinHelperTest extends FlatSpec with Matchers {
  var fixtureParser = new DefaultFixtureParser(null)
  var entityJoinHelper = new EntityJoinHelper

  "it" should "find the two join properties in musician entity" in {
    val fields = entityJoinHelper.findEntityFields(classOf[Musician])
    fields.length should be(3)
  }

  "it" should "convert to a map structure" in {
    val pollini = "firstname: Maurizio\nlastname: Pollini\nteacherId: 60"
    val hashMap = entityJoinHelper.toYamlMap(pollini)
    hashMap.keySet should contain allOf ("firstname", "lastname", "teacherId")
    hashMap.get("firstname").get shouldBe "Maurizio"
    hashMap.get("lastname").get shouldBe "Pollini"
    hashMap.get("teacherId").get shouldBe 60
  }

  "it" should "reconvert a yaml" in {
    entityJoinHelper.yamlify(
      Map("firstname" -> "Maurizio", "lastname" -> "Pollini")
    ) shouldBe "{firstname: Maurizio, lastname: Pollini}\n"
  }


  "it" should "nullify only set entities" in {
    val fieldsMap = Map("teacherid" -> null, "instrumentid" -> null).asInstanceOf[Map[String, Field]]
    val yamlMap = Map("firstname" -> "Maurizio", "teacherid" -> "", "instrumentID" -> "2")
    val nullifiedMap = entityJoinHelper.nullifyEntities(yamlMap, fieldsMap, Map())
    nullifiedMap.keys.size shouldBe 1
    nullifiedMap.get("firstname").get shouldBe "Maurizio"
    nullifiedMap.contains("teacherid") shouldBe false
    nullifiedMap.contains("instrumentID") shouldBe false
  }

  "it" should "fetch the original entities" in {
    val yamlMap = Map("firstname" -> "Maurizio", "teacherid" -> "", "instrumentID" -> 2)
    val fieldsMap = entityJoinHelper.fieldsMap(
      entityJoinHelper.findEntityFields(classOf[Musician])
    )
    val entities = entityJoinHelper.fetchOriginalEntities(yamlMap, fieldsMap, Map()).toList
    entities.size shouldBe 1
    val instrumentEntity = entities(0)
    instrumentEntity.field.getType shouldBe classOf[Instrument]
    instrumentEntity.id shouldBe 2
  }

  "it" should "find joinable fields for Musician" in {
    val fields = entityJoinHelper.findJoinableFields[Musician](classOf[Musician])
    fields.size shouldBe 3
  }

  "it" should "distinguish between entity by id and by key" in {
    val yaml = "firstname: Maurizio\nteacherid: _lonati\ninstrumentID: 2"
    val intermediateYaml = entityJoinHelper.getIntermediateYaml[Musician](yaml, classOf[Musician])

    intermediateYaml.entities.size shouldBe 2
    val entities = intermediateYaml.entities.toList
    val teacherEntity = entities.find(p => p.field.getName == "teacher").head
    val instrumentEntity = entities.find(p => p.field.getName == "instrument").head

    teacherEntity.isKey shouldBe true
    teacherEntity.key shouldBe "lonati"
    instrumentEntity.isKey shouldBe false
    instrumentEntity.id shouldBe 2
  }
}
