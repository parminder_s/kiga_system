package com.kiga.db.fixture.fixture.test

import com.kiga.db.fixture.DefaultFixtureParser
import com.kiga.db.fixture.fixture.AllTypesClass
import com.kiga.db.fixture.fixture.domain.{Musician, Person}
import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FlatSpec, Matchers}
import org.slf4j.LoggerFactory

/**
 * Created by rainerh on 31.07.15.
 */
@RunWith(classOf[JUnitRunner])
class ParserTest extends FlatSpec with Matchers {
  val logger = LoggerFactory.getLogger(classOf[ParserTest])
  val parser = new DefaultFixtureParser(null)

  "it" should "load a person entity from a yaml string" in {
    val personString = "firstname: John\nlastname: Harrison\nbirthdate: 1980-05-12"

    val person = parser.parse[Person](personString, classOf[Person]).entity
    person.firstname shouldBe "John"
    person.lastname shouldBe "Harrison"
    val birthdate = new DateTime(person.birthdate)
    birthdate.getYear shouldBe 1980
    birthdate.getMonthOfYear shouldBe 5
    birthdate.getDayOfMonth shouldBe 12
  }

  "it" should "load inherited entity" in {
    val musicianString = "firstname: Jason\nlastname: Long\ntrainingYears: 5"

    val musician = parser.parse[Musician](musicianString, classOf[Musician]).entity
    musician.firstname shouldBe "Jason"
    musician.trainingYears shouldBe 5
  }

  "it" should "load multiple entities" in {
    val personsString = "firstname: Jason\nlastname: Long\n---\nfirstname: John\nlastname: Harrison"
    val persons = parser.parseMultiple[Person](personsString, classOf[Person]).toList
    val jason = persons.head.entity
    val john = persons.last.entity

    jason.firstname shouldBe "Jason"
    jason.lastname shouldBe "Long"
    john.firstname shouldBe "John"
    john.lastname shouldBe "Harrison"
  }

  "it" should "load an object containing all available types" in {
    val allTypesString = "intType: 5\ndoubleType: 2.5\nstringType: test\ndatetimeType: 2010-05-21T00:50:28"
    parser.parse[AllTypesClass](allTypesString, classOf[AllTypesClass])
  }

  "it" should "set the _key properly" in {
    val personsString = "_key: p-harrison\nfirstname: Jason\nlastname: Long"
    val result = parser.parse[Person](personsString, classOf[Person])

    result.key shouldBe "p-harrison"
  }
}
