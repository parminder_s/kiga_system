package com.kiga.db.fixture.fixture.domain

import javax.persistence._

import scala.beans.BeanProperty

/**
  * Created by rainerh on 01.12.15.
  */
@Inheritance(strategy=InheritanceType.JOINED)
@Entity
class Person extends com.kiga.db.fixture.Entity {
  this.setClassName("Person")

  @BeanProperty var i: java.lang.Long = _
  @BeanProperty var firstname: String = _
  @BeanProperty var lastname: String = _
  @BeanProperty var birthdate: java.util.Date = _
  @BeanProperty var className: String = _

  @ManyToOne
  @JoinColumn(name = "spouseID", referencedColumnName = "id")
  @BeanProperty var spouse: Person = _

  def getId(): java.lang.Long = {
    this.i
  }

  def setId(id: java.lang.Long) = {
    this.i = id
  }
}
