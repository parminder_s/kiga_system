#Goal
Testing of [KiGa portal](http://stage.kigaportal.com/) 

#How to run from command line
You have to create a file `gradle-local.properties` in the projects root. There you add following line to enable the webtests in gradle:

`runWebtest=true`
* without details
    * `gradlew :modules/kiga-webtest:test`
        * tests for default environment will be run
* using environment option
    * `gradlew :modules/kiga-webtest:test -Ptest_env=dev`
        * tests with specified environment will be run
* using parallelization options
    * `gradlew :modules/kiga-webtest:test -PmaxParallelForks=2 -PforkEvery=2`    
* using environment option & system properties
    * `gradlew :modules/kiga-webtest:test -Ptest_env=dev -Dtest.browser=firefox`
        * tests with environment properties overridden by system properties will be executed
        * [example](https://drive.google.com/file/d/0B2UFaKOpHq_Mc1d1VVhDTmVQQzA/view?usp=sharing) 
##Additional options
* some tests
    * `gradlew :modules/kiga-webtest:test --tests com.kiga.webtest.registration.standart.RegistrationFreeTest.registerFree`  
* with logging from Java code
    * `gradlew :modules/kiga-webtest:test -Dtest.logging=true`
* with Success result even if test was failed
    * `gradlew :modules/kiga-webtest:test -Dtest.ignoreFailures=true`
        * this option is needed to correct result of build in Jenkins (to get Unstable instead of Failure result)
#"Done" list   
## the "core" part of test framework
  * based on open source [Automician template for Web UI automation](https://github.com/automician/snippets/tree/master/java/widgetsapproach) 
  * environment options and properties 
    * ensure running tests for different environments 
    * java properties files are linked to project property 'env' (`-Ptest_env=...`)
    * default value of project property 'env': test
      * if environment is not specified by user, default value will be used
    * there can be as many java properties files as needed
    * each property file describes their own set of values for the same list of properties
    * property values can be overridden using system property with the same name (`-Dsome.property=...`) 
    * so tests can be configured by
        * changing property file
        * using different environment options
        * using default environment option
        * specifying some properties through system properties 
  * tests can be run in chrome and firefox
    * for firefox the "older" firefox driver is used (for possible versions of the browser <=47.0.1) to avoid [some problems in geckodriver](https://github.com/mozilla/geckodriver/issues/233)
 * included: wait for Angular 
     * to finish async "loading" activity in case UI is not ready for corresponding actions and there is no other way to wait (e.g. via Selenide)
     * based on open source [ngWebDriver's](https://github.com/paul-hammant/ngWebDriver) implementation
     * for smart waiting of finishing async "loading" used ExpectedCondition based on wait for Angular
     * to use element's or collection's methods with these waits realized wrappers around SelenideElement element and ElementsCollection  
  * Methods to access to elements with 'data-test' attribute
  * Unique data generator 
    * each generated text has a name
    * each text can be get by its name
    * only the "name" is used in tests, without directly specifying the "unique" prefix of postfix of data
    * and the "unique" part is hidden under the hood... 
    * so everything just works
  * One entry point to access core features
## The Test Model part
  * based on Widgets pattern (aka [PageObjects by Martin Fowler](http://martinfowler.com/bliki/PageObject.html))
  * implemented in object-oriented style for convenient handy usage in tests
  * widget describes some complex element, usually containing a group of other nested elements
  * widget can reflect the part of a page or a whole page
  * widget represents a behavior of the user on the "UI Part" that it represents
    * implemented in widget methods
    * returning widget(the same or new one - it depends on application logic)
      * so it can be conveniently used without much knowledge of its internal structure:
        `WHEN().topBar().startLogin().startSubscribe();`
  * in tests widgets have one entry point
    * entry point is implemented in GIVEN(), WHEN(), THEN() methods of BaseTest
    * so each widget call begins from call one of these methods
    * thus, this approach can provide more readable code in BDD style ([reported correspondingly](https://drive.google.com/file/d/0B2UFaKOpHq_MM1pXRzR4SFoyNzA/view?usp=sharing)):
``` 
           GIVEN().visitHome().ensureLoggedOut();
   
           WHEN().topBar().startLogin().login(
                   KigaConfiguration.userEmail,
                   KigaConfiguration.userPassword
           );
           THEN().assertLoggedIn();
```                       
#Environment options
* `dev` 
* `test`(by default)
* `jenkins`
* `saucelabs`
* `browserstack`
#Settings
* `app.url`
    * base url of tested application
* `data.user.email` and `data.user.password`
    * credentials of user - to be used in tests
* `test.timeout`
    * timeout in milliseconds 
       * for smart waits 
          * "smart" means - the "wait implementation" will wait only the minimum needed amount of time, not greater than specified timeout
       * during
          * asserts / checking test steps results
          * access to elements 
* `test.browser`
    * firefox
    * chrome
         * __!!! to use it the additional actions are needed: download chromedriver and include path to chromedriver into environment variable PATH__
* `test.fastSetValue`
    * true - sets text by javascript instead of using Selenium built-in "sendKey" function (by default)
    * false
         * both options work correctly, `test.fastSetValue = true` gives faster setting text in inputs. Despite of that, the test speed increase did not happen because of Angular waits.
* `test.gridUrl`
    * URL for node of Selenium Grid, or URL for cloud service
* `capabilities.*`
    * [custom capabilities for the browser](http://selenide.org/2016/12/30/selenide-4.2/)
#[Parallelization options](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.testing.Test.html)
* maxParallelForks
* forkEvery         
   
#How to get Allure report
* command line command to generate a report (based on test results of previously executed `gradlew :modules/kiga-webtest:test`)
    * `gradlew :modules/kiga-webtest:allureReport`
* to open
    * open in firefox - project\modules\kiga-webtest\build\allure-report\index.html
    * [example](https://drive.google.com/file/d/0B2UFaKOpHq_MZTVkM2t0eWRHSW8/view?usp=sharing)
        * The report can be opened in more detail - down to the method steps (follow the arrows)
        * Each new "sentence" - starts with GIVEN / WHEN / THEN
        * The continuation of this sentence (methods of widgets) come after GIVEN / WHEN / THEN
    * [Example of Allure report](https://drive.google.com/file/d/0B2UFaKOpHq_MZTVkM2t0eWRHSW8/view?usp=sharing)
##Coverage of registration tests
* [Mind Map - registration](https://drive.google.com/file/d/0B2UFaKOpHq_MR1p4dmQtNTR1VjQ/view?usp=sharing)
* [Mind Map - shop](https://drive.google.com/file/d/0B2UFaKOpHq_MTkJrb1pDTkg5U0E/view?usp=sharing)
    * to review [Mind Mup addon needed](https://chrome.google.com/webstore/detail/mindmup-legacy-version/dnenaecjcgeppfpaokiifokeieopppej)
* [Mind Map picture](https://drive.google.com/file/d/0B2UFaKOpHq_McDZCQ2xEb0Z3QlU/view?usp=sharing)



  
