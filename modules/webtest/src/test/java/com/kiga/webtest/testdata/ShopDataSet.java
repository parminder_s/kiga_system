package com.kiga.webtest.testdata;

import com.kiga.webtest.datastructure.Countries;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.datastructure.ShopData.ShopDataBuilder;
import ru.yandex.qatools.allure.annotations.Step;

public class ShopDataSet {

  @Step
  public static ShopDataSet SHOP_DATA() {
    return new ShopDataSet();
  }

  @Step
  public ShopData huehnerMiniLappokInLuxembourg(int quantity) {
    return
      new ShopDataBuilder(quantity + " X HÜHNER MINI-LAPBOOK")
        .inCountry(Countries.LUXEMBOURG)
        .addProducts("HÜHNER MINI-LAPBOOK", 9.90, quantity, "Mini-Lapbook Hühner / Hülle grün")
        .build();
  }
  @Step
  public ShopData cdZeitDesWartensinGermany(int quantity){
    return new ShopDataBuilder(quantity + "CD Zeit des Wartens")
      .inCountry(Countries.GERMANY)
      .addProducts("CD ZEIT DES WARTENS", 18.90, quantity, "something")
      .build();
  }
  @Step
  public ShopData schmetterlingMiniLapbook(int quantity){
    return new ShopDataBuilder(quantity + "Schmetterling Mini-Lapbook")
      .inCountry(Countries.GERMANY)
      .addProducts("SCHMETTERLING MINI-LAPBOOK", 9.90, quantity, "Mini-Lapbook Schmetterling / Hülle grün")
      .build();
  }
  @Step
  public ShopData winterwaldLapbag(int quantity){
    return new ShopDataBuilder(quantity + "Winterwald Lapbag")
      .inCountry(Countries.GERMANY)
      .addProducts("WINTERWALD LAPBAG", 39.90, quantity, "something")
      .build();
  }
}
