package com.kiga.webtest.permission.user.fulltest;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.IdeaData;
import com.kiga.webtest.datastructure.settings.IdeasSection;
import com.kiga.webtest.datastructure.settings.Permission;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Permission")
@Stories("Full test permission & New ideas")
@Ignore
public class NewIdeasAvailabilityForFullTestPermissionTest
  extends BaseTest {

  @Test
  public void forFreeSubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission
        (SubscriptionType.FREE, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);
    AND().navigation().visitIdeas();

    WHEN().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    String title = AND().ideas().ideasForm().categories().category(0).selectIdea(1).getIdeaTitle();
    AND().ideas().ideaOverview().viewPreview();
    THEN().ideas().ideaForm().assertOpened(title);
  }

  @Test
  public void forExpiredSubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .expiredSubscriptionWithPermission(
        SubscriptionType.FREE, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);
    AND().navigation().visitIdeas();

    WHEN().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    AND().ideas().ideasForm().categories().category(0).selectIdea(1);
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertTestExpiredErrorUrl();
  }

  @Test
  public void forCommunitySubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(
        SubscriptionType.COMMUNITY, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);
    AND().navigation().visitIdeas();

    WHEN().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    AND().ideas().ideasForm().categories().category(0).selectIdea(1);
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertFullRequiredErrorUrl();
  }

  @Test
  public void forStandardSubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(
        SubscriptionType.TEACHERS, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);
    AND().navigation().visitIdeas();

    WHEN().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    String title = AND().ideas().ideasForm().categories().category(0).selectIdea(2).getIdeaTitle();
    AND().ideas().ideaOverview().viewPreview();
    THEN().ideas().ideaForm().assertOpened(title);
  }

}
