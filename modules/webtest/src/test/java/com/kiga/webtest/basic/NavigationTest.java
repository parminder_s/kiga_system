package com.kiga.webtest.basic;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.kiga.webtest.config.KigaConfiguration;
import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.Countries;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;

@Features("Navigation")
public class NavigationTest extends BaseTest {
  @Test
  public void appIsLoadedInDefaultLanguage() {
    WHEN().navigation().visit();
    THEN().navigation().assertHomeUrl(Language.getDefault());
  }

  @Test
  public void nonDefaultLanguageCanBeSelected() {
    GIVEN().navigation().visitHome();

    WHEN().topBar().selectLanguage(Language.ITALIANO);
    THEN().navigation().assertHomeUrl(Language.ITALIANO);
  }

  @Test
  public void startSubscription() {
    GIVEN().navigation().visitHome();
    AND().assertHomeAndLoggedOut();

    WHEN().topBar().startSubscribe();
    THEN().navigation().assertSubscriptionUrl();
  }

  @Test
  public void defaultCountryIsChosen() {
    GIVEN().endPoints().country().set(Countries.BELGIUM);
    AND().ensureLoggedOut();

    WHEN().navigation().visitSubscription();
    THEN().subscriptionFor().assertCountry(Countries.BELGIUM);
  }

  @Test
  public void loginAndLogout() {
    GIVEN().ensureLoggedOut();

    WHEN()
        .topBar()
        .login()
        .userEMail(KigaConfiguration.userEmail)
        .userPassword(KigaConfiguration.userPassword)
        .submit();

    THEN().topBar().assertLoggedIn();

    WHEN().topBar().logout();
    THEN().topBar().assertLoggedOut();
  }

  @Test
  public void startShopInGerman() {
    GIVEN(Language.DEUTSCH).navigation().visitHome();

    WHEN().topBar().startShop();
    THEN().navigation().assertShopUrl();
  }

  @Ignore
  @Test
  public void assertCookiesMessage() {
    GIVEN().endPoints().createNewSession(null);
    getWebDriver().manage().deleteAllCookies();
    // Selenide.executeJavaScript("ctrl.cookieInfoService.getAgreedCookieUse()");

    WHEN().navigation().visitHome();
    THEN().cookies().assertCookiesMessageIs(Condition.visible);
    Selenide.refresh();
    THEN().cookies().assertCookiesMessageIs(Condition.hidden);
  }
}
