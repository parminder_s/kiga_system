package com.kiga.webtest.permission.user.nonfulltest;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.IdeaData;
import com.kiga.webtest.datastructure.settings.IdeasSection;
import com.kiga.webtest.datastructure.settings.Permission;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Permission")
@Stories("Non full test permission & favourites")
public class FavouritesAvailabilityForNonFullTestPermissionTest extends BaseTest {
  @Ignore
  @Test
  public void favouriteOperationsForFreeSubscriptionThroughIdeas() {
    CustomerData customerData =
        CUSTOMER_DATA().subscriptionWithPermission(SubscriptionType.FREE, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    IdeaData ideaData = WHEN().ideas().startIdeas().addToFavourites(IdeasSection.ACTIVITIES, 0, 1);
    THEN().ideas().ideasForm().assertIsFavourite(ideaData);

    WHEN()
        .ideas()
        .startFavourites()
        .sections()
        .select(ideaData.section())
        .category(ideaData.categoryTitle());
    AND().ideas().favouritesForm().removeFromFavourites(ideaData);
    THEN().ideas().favouritesForm().assertIsNotFavourite(ideaData);
  }

  @Test
  public void addToFavouritesForExpiredSubscriptionThroughIdeaOverview() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .expiredSubscriptionWithPermission(SubscriptionType.FREE, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);
    AND().navigation().visitIdeas();

    WHEN().ideas().ideasForm().sections().select(IdeasSection.RESOURCES);
    AND().ideas().ideasForm().categories().category(1).selectIdea(1);
    AND().cookies().acceptCookiesInfo();
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertTestExpiredErrorUrl();
  }

  @Test
  public void switchToFavouritesForExpiredSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .expiredSubscriptionWithPermission(SubscriptionType.FREE, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startFavourites();
    THEN().navigation().assertTestExpiredErrorUrl();
  }

  @Ignore
  @Test
  public void addToFavouritesForCommunitySubscriptionThroughIdeas() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.COMMUNITY, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startIdeas().addToFavourites(IdeasSection.THEMES, 0, 1);
    THEN().navigation().assertFullRequiredErrorUrl();
  }

  @Test
  public void switchToFavouritesForCommunitySubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.COMMUNITY, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startFavourites();
    THEN().navigation().assertFullRequiredErrorUrl();
  }

  @Ignore
  @Test
  public void favouriteOperationsForStandardSubscriptionThroughIdeaOverview() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.TEACHERS, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    IdeaData ideaData = WHEN().ideas().startIdeas().selectIdea(IdeasSection.ACTIVITIES, 0, 1);
    AND().cookies().acceptCookiesInfo();
    String ideaTitle = ideaData.ideaTitle();
    AND().ideaOverview().actionButtons().viewIdea();
    // AND().ideaOverview().addToFavourites();                       !!!1  reuse this add method to
    // other test
    AND().ideas().ideaOverview().toFavourites();
    AND().ideas().startFavourites().categories();
    AND().ideas().ideasForm().assertIsToFavourites(ideaTitle);

    WHEN()
        .ideas()
        .startFavourites()
        .sections()
        .select(ideaData.section())
        .category(ideaData.categoryTitle());
    AND().ideas().favouritesForm().selectIdea(ideaTitle);
    AND().ideas().ideaOverview().viewPreview();
    AND().ideas().ideaOverview().outFavourites();
    AND().ideas().startFavourites().categories();
    AND().ideas().ideasForm().assertNotToFavourites(ideaTitle);
  }

  @Ignore
  @Test
  public void addToFavouritesForStandardExpiredSubscriptionThroughIdeas() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .expiredSubscriptionWithPermission(SubscriptionType.TEACHERS, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startIdeas().addToFavourites(IdeasSection.RESOURCES, 1, 1);
    THEN().navigation().assertFullExpiredErrorUrl();
  }

  @Test
  public void switchToFavouritesForStandardExpiredSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .expiredSubscriptionWithPermission(SubscriptionType.TEACHERS, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startFavourites();
    THEN().navigation().assertFullExpiredErrorUrl();
  }
}
