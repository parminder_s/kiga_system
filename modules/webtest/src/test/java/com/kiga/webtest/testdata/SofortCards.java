package com.kiga.webtest.testdata;

import com.kiga.webtest.datastructure.SofortCard;

import java.text.DecimalFormat;
import java.util.Random;

public class SofortCards {

  public final static SofortCard SOFORT_CARD_1 = new SofortCard(
    "00000",
    "12345",
    new DecimalFormat("0000").format(new Random().nextInt(9999)),
    new DecimalFormat("00000").format(new Random().nextInt(99999)),
    0
  );
}
