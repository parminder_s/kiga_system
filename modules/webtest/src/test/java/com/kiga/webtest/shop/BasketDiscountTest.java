package com.kiga.webtest.shop;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.open;
import static com.kiga.webtest.testdata.ShopDataSet.SHOP_DATA;

import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.Countries;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Shop")
@Stories("Basket")
public class BasketDiscountTest extends BaseTest {

  private static String validVoucher = "selenium";

  @BeforeClass
  public static void setUpVoucher() {
    open(
        "http://stage.kigaportal.com/endpoint/testurl/shop/voucher?code="
            + validVoucher
            + "&discount=0.2&productcode=mlap-3-mh");
  }

  @Test
  public void validVoucherInput() {
    GIVEN(Language.DEUTSCH).shop().prepare(Countries.GERMANY);
    ShopData shopData17 = SHOP_DATA().schmetterlingMiniLapbook(1);

    WHEN().shop().forData(shopData17).addToBasket(0);
    AND().shop().basket().inputVoucher(validVoucher);
    THEN().shop().basket().table().totalRow().assertDiscountAdded(visible);
    AND().shop().basket().table().totalRow().assertDiscountCounter();

    WHEN().shop().basket().table().row(0).increaseTo(2);
    THEN().shop().basket().table().totalRow().assertDiscountAdded(visible);
    AND().shop().basket().table().totalRow().assertDiscountCounter();
  }

  @Test
  public void voucherCantAppyedOnOtherProduct() {
    GIVEN(Language.DEUTSCH).shop().prepare(Countries.LUXEMBOURG);
    ShopData shopData16 = SHOP_DATA().huehnerMiniLappokInLuxembourg(1);

    WHEN().shop().forData(shopData16).addToBasket(0);
    AND().shop().basket().inputVoucher(validVoucher);
    THEN().shop().basket().table().totalRow().assertErrorMessage();
    AND().shop().basket().table().totalRow().assertDiscountAdded(hidden);
  }

  @Test
  public void emptyVoucherInput() {
    GIVEN(Language.DEUTSCH).shop().prepare(Countries.GERMANY);
    ShopData shopData17 = SHOP_DATA().schmetterlingMiniLapbook(2);

    WHEN().shop().forData(shopData17).addToBasket(0);
    AND().shop().basket().inputVoucher("");
    THEN().navigation().assertLoginUrl();
  }
}
