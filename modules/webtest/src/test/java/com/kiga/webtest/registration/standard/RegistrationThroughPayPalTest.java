package com.kiga.webtest.registration.standard;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.testconfigs.BaseTest;
import com.kiga.webtest.testdata.*;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Registration")
@Stories("Payment through PayPal")
public class RegistrationThroughPayPalTest extends BaseTest {
  // @Ignore
  @Test
  public void registerCustomerFor1MonthInSwitzerland() {

    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    CustomerData customerData = CUSTOMER_DATA().inSwitzerlandAsTeacherFor1Month();

    WHEN().subscriptionFor(customerData).start();
    AND().cookies().acceptCookiesInfo();
    AND()
        .registrationFor(customerData)
        .fillCustomerInformationReg()
        .submitCustomerInformation()
        .selectPayment()
        .viaPayPal()
        .accept()
        .next();
    THEN().registrationFor(customerData).summary().assertRegistrationData("Pay with PayPal");

    WHEN().registrationFor(customerData).summary().confirm();
    AND().paymentFor(customerData).using(PayPalAccounts.PAY_PAL_1).payAndApprove();
    THEN().welcome().assertSuccessAndGoHome();
    AND().assertHomeAndLoggedOut();
  }
}
