package com.kiga.webtest.registration.validation;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Registration")
@Stories("Validation")
public class ValidationForBirthDateTest extends BaseTest {
  @Test
  public void checkOnCorrectDate() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    CustomerData customerData = CUSTOMER_DATA().inBelgiumAsGiftFor6Month();
    AND().subscriptionFor(customerData).start();
    AND().registrationFor(customerData).fillCustomerInformationReg();

    WHEN().registrationFor(customerData).fillBirthDate(58, 5, 1987).submit();
    THEN().registrationFor(customerData).validation()
      .assertDayInBirthDateIsMandatory();
  }
}
