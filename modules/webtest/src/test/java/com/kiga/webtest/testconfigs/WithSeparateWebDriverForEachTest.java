package com.kiga.webtest.testconfigs;

import org.junit.After;
import org.junit.Before;

import java.io.IOException;

import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class WithSeparateWebDriverForEachTest extends BaseTest {

  @Before
  public void restartWebDriverBefore() {
    closeWebDriver();
    getWebDriver();
  }

  @After
  public void closeWebDriverAfter() throws IOException {
    screenshot();
    closeWebDriver();
  }

}
