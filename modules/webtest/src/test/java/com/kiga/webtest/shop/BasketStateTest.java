package com.kiga.webtest.shop;

import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.Countries;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.codeborne.selenide.Configuration.browser;
import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Shop")
@Stories("Basket")
public class BasketStateTest extends BaseTest {
  @Test
  public void basketIsTheSameAfterLogin() {
    CustomerData customerData =
      CUSTOMER_DATA().paidSubscriptionIn(Countries.BELGIUM);
    GIVEN(Language.DEUTSCH)
      .endPoints().quickRegisterFor(customerData).registerAndGetEmail();
    AND().ensureLoggedOut();
    AND().shop().prepare(Countries.BELGIUM);
    ShopData shopData = AND().shop()
      .getDataForProducts("Product", Countries.BELGIUM, 5);

    WHEN().shop().forData(shopData).selectProduct(0).addToBasket();
    THEN().shop().forData(shopData)
      .assertBasketRows().assertBasketDelivery().assertBasketTotal();
    AND().topBar().basketButton().shouldHaveProductsCount(1);

    WHEN().shop().basket().startPay();
    AND().login(customerData);
    AND().navigation().visitShop();
    THEN().topBar().basketButton().shouldHaveProductsCount(1);

    WHEN().topBar().basketButton().click();
    THEN().shop().forData(shopData)
      .assertBasketRows().assertBasketDelivery().assertBasketTotal();
    AND().topBar().basketButton().shouldHaveProductsCount(1);
  }

}
