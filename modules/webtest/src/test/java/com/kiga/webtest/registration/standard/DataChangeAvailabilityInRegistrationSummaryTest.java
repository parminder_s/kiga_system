package com.kiga.webtest.registration.standard;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Registration")
@Stories("Data change availability")
public class DataChangeAvailabilityInRegistrationSummaryTest extends BaseTest {
  @Test
  public void checkSubscriptionInformationAfterEditSubscriptionPrice() {
    CustomerData customerData = CUSTOMER_DATA().inAustriaAsParentFor12Month();
    GIVEN().registrationFor(customerData).fillDataReg();
    WHEN().registrationFor(customerData).summary().editSubscriptionPrice();
    THEN().subscriptionFor(customerData)
      .assertDuration(customerData.duration())
      .assertAmIStudent(customerData.isStudent());
  }
  @Test
  public void checkCustomerInformationAfterEditContact() {
    CustomerData customerData = CUSTOMER_DATA().inAustriaAsParentFor12Month();
    GIVEN().registrationFor(customerData).fillDataReg();

    WHEN().registrationFor(customerData)
      .summary().editContact();
    THEN().registrationFor(customerData)
      .assertCustomerInformation();
  }
  @Test
  public void checkCustomerInformationAfterEditPaymentMethod() {
    CustomerData customerData = CUSTOMER_DATA().inAustriaAsParentFor12Month();
    GIVEN().registrationFor(customerData).fillDataReg();

    WHEN().registrationFor(customerData)
      .summary().editPaymentMethod();
    THEN().registrationFor(customerData)
      .paymentMethods().assertInvoiceViaEmail();
  }

}
