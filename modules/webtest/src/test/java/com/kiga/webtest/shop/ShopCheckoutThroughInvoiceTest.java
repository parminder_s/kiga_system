package com.kiga.webtest.shop;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.Countries;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.datastructure.settings.ShopPaymentMethod;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Shop")
@Stories("Checkout through Invoice")
public class ShopCheckoutThroughInvoiceTest extends BaseTest {
  @Test
  public void checkout() {
    CustomerData customerData = CUSTOMER_DATA().paidSubscriptionIn(Countries.BELGIUM);
    ShopData shopData =
        GIVEN(Language.DEUTSCH).shop().prepareForCustomerAndProduct(customerData, 11, 3);

    WHEN().cookies().acceptCookiesInfo();
    AND()
        .shop()
        .forData(shopData, customerData, ShopPaymentMethod.INVOICE)
        .addToBasket()
        .selectPaymentMethod()
        .assertSummaryAndStartPay();

    THEN().shop().assertFinished();
  }
}
