package com.kiga.webtest.permission.user.fulltest;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.IdeaData;
import com.kiga.webtest.datastructure.settings.IdeasSection;
import com.kiga.webtest.datastructure.settings.Permission;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Permission")
@Stories("Full test permission & Archive ideas")
@Ignore
public class ArchiveIdeasAvailabilityForFullTestPermissionTest
  extends BaseTest {

  @Test
  public void forFreeSubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(SubscriptionType.FREE, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    IdeaData ideaData =
      WHEN().ideas().startIdeas().selectIdea(IdeasSection.ACTIVITIES, 0, 1);
    AND().ideas().ideaOverview().viewPreview();
    THEN().ideas().ideaForm().assertOpened(ideaData);
  }

  @Test
  public void forExpiredSubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .expiredSubscriptionWithPermission(
        SubscriptionType.FREE, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startIdeas().selectIdea(IdeasSection.RESOURCES, 0, 1);
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertTestExpiredErrorUrl();
  }

  @Test
  public void forCommunitySubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(
        SubscriptionType.COMMUNITY, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startIdeas().selectIdea(IdeasSection.THEMES, 0, 1);
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertFullRequiredErrorUrl();
  }

  @Test
  public void forStandardSubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(
        SubscriptionType.TEACHERS, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    IdeaData ideaData =
      WHEN().ideas().startIdeas().selectIdea(IdeasSection.ACTIVITIES, 0, 2);
    AND().ideas().ideaOverview().viewPreview();
    THEN().ideas().ideaForm().assertOpened(ideaData);
  }
}
