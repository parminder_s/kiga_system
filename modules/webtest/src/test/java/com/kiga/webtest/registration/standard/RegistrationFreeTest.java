package com.kiga.webtest.registration.standard;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.codeborne.selenide.Configuration.browser;
import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Registration")
@Stories("Free")
public class RegistrationFreeTest extends BaseTest {
  @Test
  public void registerFree() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    CustomerData customerData = CUSTOMER_DATA().inSwitzerlandForFree();

    WHEN().subscriptionFor(customerData).start();
    AND().cookies().acceptCookiesInfo();
    AND().registrationFor(customerData).fillCustomerInformation();
    THEN().welcome().assertSuccessAndGoHome();
    AND().assertHomeAndLoggedOut();
  }

}
