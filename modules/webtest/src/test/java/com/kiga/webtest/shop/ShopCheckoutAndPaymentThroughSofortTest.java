package com.kiga.webtest.shop;

import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.Countries;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.datastructure.settings.ShopPaymentMethod;
import com.kiga.webtest.testconfigs.WithSeparateWebDriverForEachTest;
import com.kiga.webtest.testdata.SofortCards;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Shop")
@Stories("Checkout and payment through Sofort")
public class ShopCheckoutAndPaymentThroughSofortTest extends WithSeparateWebDriverForEachTest {
/*
* separate webdriver for each test is needed for Sofort payment
* Sofort service remembers previous state and starts payment from the different steps
* (it depends on the previous actions in browser)
* separate webdriver ensures the ame start for the Sofort service
*/
  @Test
  public void checkoutAndPay() {
    CustomerData customerData =
      CUSTOMER_DATA().paidSubscriptionIn(Countries.GERMANY);
    ShopData shopData = GIVEN(Language.DEUTSCH).shop()
      .prepareForCustomerAndProduct(customerData, 8, 2);

    WHEN().cookies().acceptCookiesInfo();
    WHEN().shop()
      .forData(shopData, customerData, ShopPaymentMethod.SOFORT)
      .addToBasketAndStartPay();
    AND().shop().paymentFor(customerData, shopData)
      .using(SofortCards.SOFORT_CARD_1)
      .confirmSumAndPaymentMethod()
      .payAndApprove();
    THEN().shop().assertFinished();
  }
}
