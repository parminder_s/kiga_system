package com.kiga.webtest.permission.common;

import com.kiga.webtest.datastructure.settings.IdeasSection;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Permission")
@Stories("Logged out")
public class PermissionWhenLoggedOutTest extends BaseTest {
  // results are different for fullTest and nonFullTest
  // @Ignore or comment assertion
  @Test
  public void archiveIdeas() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitHome();

    WHEN().ideas().startIdeas().selectIdea(IdeasSection.ACTIVITIES, 0, 0);
    AND().ideas().ideaOverview().viewPreview();
    // THEN().navigation().assertLoginUrl();//forFullTest
    THEN().navigation().assertLoginOrFullUrl(); // for nonFullTest
  }

  @Test
  public void newIdeas() {
    // GIVEN().ensureLoggedOut();// getting timeOutExeption
    GIVEN().navigation().visitIdeas();
    AND().cookies().acceptCookiesInfo();

    WHEN().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    AND().ideas().ideasForm().selectNewIdea(1);
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertLoginUrl();
  }

  @Test
  public void addToFavourites() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitHome();
    AND().cookies().acceptCookiesInfo();

    WHEN().ideas().startIdeas().selectIdea(IdeasSection.RESOURCES, 1, 0);
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertLoginOrFullUrl();
  }

  @Test
  public void switchToFavourites() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitHome();

    WHEN().ideas().startFavourites();
    THEN().navigation().assertLoginUrl();
  }

  @Ignore
  @Test
  public void switchToMyPlans() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitHome();

    WHEN().topBar().startPlanner().switchToMyPlans();
    THEN().navigation().assertLoginUrl();
  }

  @Test
  public void importToMyPlans() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitHome();

    WHEN().planner().startImportToMyPlans();
    THEN().navigation().assertLoginUrl();
  }
}
