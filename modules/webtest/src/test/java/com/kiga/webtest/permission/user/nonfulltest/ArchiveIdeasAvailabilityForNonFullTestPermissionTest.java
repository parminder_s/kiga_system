package com.kiga.webtest.permission.user.nonfulltest;

import static com.codeborne.selenide.Selenide.open;
import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

import com.codeborne.selenide.WebDriverRunner;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.IdeaData;
import com.kiga.webtest.datastructure.settings.IdeasSection;
import com.kiga.webtest.datastructure.settings.Permission;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Permission")
@Stories("Non full test permission & Archive ideas")
public class ArchiveIdeasAvailabilityForNonFullTestPermissionTest extends BaseTest {
  @Test
  public void redirectLinkOnFirstRequest() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.TEACHERS, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startIdeas().selectIdea(IdeasSection.ACTIVITIES, 0, 2);
    String ideaTitle = AND().ideas().ideaOverview().getIdeaTitle();

    AND().ideas().ideaOverview().viewPreview();
    String previewUrl = WebDriverRunner.url();
    WebDriverRunner.closeWebDriver();

    GIVEN().ideas().prepareFor(customerData);
    open(previewUrl);
    THEN().ideas().ideaOverview().assertPreview(ideaTitle);
    WebDriverRunner.closeWebDriver();

    open(previewUrl);
    THEN().navigation().assertLoginOrFullUrl();
  }

  @Test
  public void forFreeSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA().subscriptionWithPermission(SubscriptionType.FREE, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startIdeas().selectIdea(IdeasSection.ACTIVITIES, 0, 0);
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertFullUpgradeErrorUrl();
  }

  @Test
  public void forExpiredSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .expiredSubscriptionWithPermission(SubscriptionType.FREE, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startIdeas().selectIdea(IdeasSection.THEMES, 0, 1);
    AND().cookies().acceptCookiesInfo();
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertTestExpiredErrorUrl();
  }

  @Test
  public void forCommunitySubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.COMMUNITY, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startIdeas().selectIdea(IdeasSection.RESOURCES, 1, 0);
    AND().cookies().acceptCookiesInfo();
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertFullRequiredErrorUrl();
  }

  @Test
  public void forStandardSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.TEACHERS, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    IdeaData ideaData = WHEN().ideas().startIdeas().selectIdea(IdeasSection.ACTIVITIES, 1, 1);
    AND().ideas().ideaOverview().viewPreview();
    THEN().ideas().ideaForm().assertOpened(ideaData);
  }

  @Test
  public void forStandardExpiredSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .expiredSubscriptionWithPermission(SubscriptionType.TEACHERS, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startIdeas().selectIdea(IdeasSection.THEMES, 0, 1);
    AND().cookies().acceptCookiesInfo();
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertFullExpiredErrorUrl();
  }
}
