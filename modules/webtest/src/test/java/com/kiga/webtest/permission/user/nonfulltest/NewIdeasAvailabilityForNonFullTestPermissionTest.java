package com.kiga.webtest.permission.user.nonfulltest;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.settings.IdeasSection;
import com.kiga.webtest.datastructure.settings.Permission;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Permission")
@Stories("Non full test permission & New ideas")
public class NewIdeasAvailabilityForNonFullTestPermissionTest extends BaseTest {
  @Test
  public void forFreeSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA().subscriptionWithPermission(SubscriptionType.FREE, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);
    AND().navigation().visitIdeas();
    int ideaIndex = 1;

    WHEN().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    String title = AND().ideas().ideasForm().selectNewIdea(ideaIndex).getIdeaTitle();

    AND().cookies().acceptCookiesInfo();
    AND().ideas().ideaOverview().viewPreview();
    THEN().ideas().ideaForm().assertOpened(title);
  }

  @Test
  public void forExpiredSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .expiredSubscriptionWithPermission(SubscriptionType.FREE, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);
    AND().navigation().visitIdeas();
    int ideaIndex = 2;

    WHEN().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    AND().ideas().ideasForm().selectNewIdea(ideaIndex);
    AND().cookies().acceptCookiesInfo();
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertTestExpiredErrorUrl();
  }

  @Test
  public void forCommunitySubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.COMMUNITY, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);
    AND().navigation().visitIdeas();
    int ideaIndex = 0;

    WHEN().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    AND().ideas().ideasForm().selectNewIdea(ideaIndex);
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertFullRequiredErrorUrl();
  }

  @Test
  public void forStandardSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.TEACHERS, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);
    AND().navigation().visitIdeas();
    int ideaIndex = 1;

    WHEN().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    String title = AND().ideas().ideasForm().selectNewIdea(ideaIndex).getIdeaTitle();
    AND().cookies().acceptCookiesInfo();
    AND().ideas().ideaOverview().viewPreview();
    THEN().ideas().ideaForm().assertOpened(title);
  }

  @Test
  public void forStandardExpiredSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .expiredSubscriptionWithPermission(SubscriptionType.TEACHERS, Permission.NON_FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);
    AND().navigation().visitIdeas();
    int ideaIndex = 1;

    WHEN().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    AND().ideas().ideasForm().selectNewIdea(ideaIndex);
    AND().cookies().acceptCookiesInfo();
    AND().ideas().ideaOverview().viewPreview();
    THEN().navigation().assertFullExpiredErrorUrl();
  }
}
