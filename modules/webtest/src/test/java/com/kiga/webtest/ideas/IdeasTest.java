package com.kiga.webtest.ideas;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.settings.IdeasSection;
import com.kiga.webtest.datastructure.settings.Permission;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;

@Features("ideas")
public class IdeasTest extends BaseTest {
  @Test
  public void activitiesAndNewIdeasMode() {
    GIVEN().navigation().visitIdeas();
    THEN().ideas().ideasForm().categories().assertRowMode();
    AND().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    THEN().ideas().ideasForm().categories().assertGridMode();
  }

  @Ignore
  @Test
  public void ageGroupFilter() {
    GIVEN().navigation().visitIdeas();

    String idea = WHEN().ideas().ideasForm().categories().category(0).selectIdea(5).getTitle();
    AND().topBar().ageRange(1);
    THEN().ideas().ideasForm().categories().category(0).assertAgeRange(idea, hidden);

    WHEN().topBar().ageRange(0);
    THEN().ideas().ideasForm().categories().category(0).assertAgeRange(idea, visible);

    idea = WHEN().ideas().ideasForm().categories().category(0).selectIdea(0).getTitle();
    AND().topBar().ageRange(4);
    THEN().ideas().ideasForm().categories().category(0).assertAgeRange(idea, hidden);
    AND().topBar().ageRange(0);

    idea = WHEN().ideas().ideasForm().categories().category(1).selectIdea(0).getTitle();
    AND().topBar().ageRange(3);
    THEN().ideas().ideasForm().categories().category(0).assertAgeRange(idea, hidden);
    AND().topBar().ageRange(0);

    idea = WHEN().ideas().ideasForm().categories().category(9).selectIdea(6).getTitle();
    AND().topBar().ageRange(2);
    THEN().ideas().ideasForm().categories().category(9).assertAgeRange(idea, hidden);
  }

  @Test
  public void ageGroupFilterFromHome() {
    GIVEN().navigation().visitHome();
    int ageGroup = 2;
    AND().topBar().ageRange(ageGroup);
    THEN().navigation().asssertIdeasAndAgeRangeUrl(ageGroup);
  }

  @Test
  public void ideaScrollsAndOpensCategoryPage() {
    GIVEN().navigation().visitHome();
    WHEN().topBar().startIdeas();
    THEN().breadcrumbs().assertBreadcrumbAmount(2);
    WHEN().ideas().ideasForm().categories().category(0).scrollAndOpen();
    THEN().breadcrumbs().assertBreadcrumbAmount(3);
  }

  @Test
  public void lastLevelIdeasOverview() {
    GIVEN().navigation().visitIdeas();
    WHEN().ideas().ideasForm().categories().category(0).navigateToLastLevel();
    WHEN().ideas().ideasForm().selectNewIdea(0);
    AND().ideas().ideaOverview().assertOverview();
  }

  @Test
  public void lastLevelIdeasGridMode() {
    GIVEN().navigation().visitIdeas();
    WHEN().ideas().ideasForm().categories().category(0).navigateToLastLevel();
    THEN().ideas().ideasForm().categories().assertGridMode();
  }

  @Test
  public void previewOpenCloseForBothModes() {
    GIVEN().navigation().visitIdeas();
    int ideaIndex = 1;
    AND().ideas().ideasForm().categories().category(0).selectIdea(ideaIndex);

    THEN().ideas().ideaOverview().assertOverview(ideaIndex);
    AND().ideas().ideasForm().categories().category(0).selectIdea(ideaIndex);
    THEN().ideas().ideaOverview().assertNoOverview();

    WHEN().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    AND().ideas().ideasForm().selectNewIdea(ideaIndex);
    THEN().ideas().ideaOverview().assertOverview(ideaIndex);
    AND().ideas().ideasForm().selectNewIdea(ideaIndex);
    THEN().ideas().ideaOverview().assertNoOverview();
  }

  @Test
  public void printButtonWorks() {
    CustomerData customerData =
        CUSTOMER_DATA().subscriptionWithPermission(SubscriptionType.FREE, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);
    WHEN().ideas().startIdeas().selectIdea(IdeasSection.ACTIVITIES, 0, 1);
    THEN().ideaOverview().actionButtons().assertPrintButton();
  }

  @Test
  public void scrollingIdeasOverviewInNewIdeas() {
    GIVEN().navigation().visitIdeas();
    int startIdeaIndex = 5;
    int scrollElementsAmount = 3;
    AND().ideas().ideasForm().sections().select(IdeasSection.NEW_IDEAS);
    WHEN().ideas().ideasForm().selectNewIdea(startIdeaIndex);
    AND().cookies().acceptCookiesInfo();
    THEN().ideaOverview().assertScrollOverview(startIdeaIndex, scrollElementsAmount);
  }

  @Ignore
  @Test
  public void socialMediaButtons() {
    GIVEN().navigation().visitIdeas();
    WHEN().ideas().ideasForm().categories().category(0).selectIdea(1);
    THEN().ideaOverview().socialMedia().assertFacebook();
    THEN().ideaOverview().socialMedia().assertTwitter();
    THEN().ideaOverview().socialMedia().assertGoogle();
    THEN().ideaOverview().socialMedia().assertPinterest();
  }

  @Test
  public void thumbnailSwitchesAreWorking() {
    GIVEN().navigation().visitIdeas();
    AND().cookies().acceptCookiesInfo();
    WHEN().ideas().ideasForm().categories().category(0).selectIdea(1);
    THEN().ideaOverview().thumbnails().assertThumbnailSwitches();
  }
}
