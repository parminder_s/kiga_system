package com.kiga.webtest.testdata;

import com.kiga.webtest.datastructure.CreditCard;
import com.kiga.webtest.datastructure.settings.CreditCardType;

public class CreditCards {

  public final static CreditCard VISA_1 = new CreditCard(
      CreditCardType.VISA, "4444111122223333",
      12, 2020, "");
  public final static CreditCard VISA_WITH_WRONG_CREDENTIALS = new CreditCard(
    CreditCardType.VISA, "4444111122223333",
    01, 2020, "");

  public final static CreditCard MASTERCARD_1 = new CreditCard(
    CreditCardType.MASTERCARD, "5555444433331111",
    12, 2020, "");
  public final static CreditCard MASTERCARD_WITH_WRONG_CREDENTIALS = new CreditCard(
    CreditCardType.MASTERCARD, "5555444433331111",
    1, 2020, "");

  public final static CreditCard GARANTI_1 = new CreditCard(
    CreditCardType.VISA, "4282209027132016",
    5,2020, "358");

}
