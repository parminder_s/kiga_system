package com.kiga.webtest.permission.user.fulltest;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.IdeaData;
import com.kiga.webtest.datastructure.settings.IdeasSection;
import com.kiga.webtest.datastructure.settings.Permission;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.codeborne.selenide.Configuration.browser;
import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Permission")
@Stories("Full test permission & Favourites")
@Ignore
public class FavouritesAvailabilityForFullTestPermissionTest
  extends BaseTest {

  @Test
  public void favouriteOperationsForFreeSubscriptionThroughIdeaOverview() {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(SubscriptionType.FREE, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    IdeaData ideaData =
      WHEN().ideas().startIdeas().addToFavourites(IdeasSection.ACTIVITIES, 0, 1);
    THEN().ideas().ideasForm().assertIsFavourite(ideaData);

    WHEN().ideas().startFavourites().removeFromFavourites(ideaData);
    THEN().ideas().favouritesForm().assertIsNotFavourite(ideaData);
  }

  @Test
  public void addToFavouritesForExpiredSubscriptionThroughIdeas() {
    CustomerData customerData = CUSTOMER_DATA()
      .expiredSubscriptionWithPermission(
        SubscriptionType.FREE, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startIdeas().addToFavourites(IdeasSection.RESOURCES, 1, 1);
    THEN().navigation().assertTestExpiredErrorUrl();
  }

  @Test
  public void switchToFavouritesForExpiredSubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .expiredSubscriptionWithPermission(
        SubscriptionType.FREE, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startFavourites();
    THEN().navigation().assertTestExpiredErrorUrl();
  }


  @Test
  public void addToFavouritesForCommunitySubscriptionThroughIdeaOverview() {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(
        SubscriptionType.COMMUNITY, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startIdeas().addToFavourites(IdeasSection.THEMES, 0, 0);
    THEN().navigation().assertFullRequiredErrorUrl();
  }

  @Test
  public void switchToFavouritesForCommunitySubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(
        SubscriptionType.COMMUNITY, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    WHEN().ideas().startFavourites();
    THEN().navigation().assertFullRequiredErrorUrl();
  }

  @Test
  public void favouriteOperationsForStandardSubscriptionThroughIdeas() {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(
        SubscriptionType.TEACHERS, Permission.FULL_TEST);
    GIVEN().ideas().prepareFor(customerData);

    IdeaData ideaData =
      WHEN().ideas().startIdeas().addToFavourites(IdeasSection.ACTIVITIES, 0, 1);
    THEN().ideas().ideasForm().assertIsFavourite(ideaData);

    WHEN().ideas().startFavourites().removeFromFavourites(ideaData);
    THEN().ideas().favouritesForm().assertIsNotFavourite(ideaData);
  }
}
