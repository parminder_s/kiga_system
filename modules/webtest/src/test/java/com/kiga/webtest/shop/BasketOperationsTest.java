package com.kiga.webtest.shop;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

import com.kiga.webtest.config.KigaConfiguration;
import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.Countries;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Shop")
@Stories("Basket")
public class BasketOperationsTest extends BaseTest {
  @Test
  public void loginPageOpensAfterStartPayment() {
    GIVEN(Language.DEUTSCH).shop().prepare(Countries.SWITZERLAND);
    ShopData shopData = AND().shop().getDataForProducts("Product", Countries.SWITZERLAND, 0);

    WHEN().shop().forData(shopData).selectProduct(0);
    THEN().shop().forData(shopData).assertProductData(0);

    WHEN().shop().product().addToBasket();
    THEN().shop().forData(shopData).assertBasketRows(0);

    WHEN().shop().basket().startPay();
    THEN().navigation().assertLoginUrl();
  }

  @Test
  public void shopRegistrationTest() {
    GIVEN(Language.DEUTSCH).shop().prepare(Countries.SWITZERLAND);
    ShopData shopData = AND().shop().getDataForProducts("Product", Countries.SWITZERLAND, 0);
    CustomerData customerData = CUSTOMER_DATA().inSwitzerlandForFree();
    AND().cookies().acceptCookiesInfo();

    WHEN().shop().forData(shopData).selectProduct(0);
    THEN().shop().forData(shopData).assertProductData(0);

    WHEN().shop().product().addToBasket();
    THEN().shop().forData(shopData).assertBasketRows(0);

    WHEN().shop().basket().startPay();
    THEN().navigation().assertLoginUrl();

    WHEN().login().assertRegistrationButtons();
    AND().login().startRegistration();
    AND().registrationFor(customerData).registrationShop();
    THEN().welcome().assertSuccessAndGoHomeShop();
    AND().assertHomeAndLoggedOut();

    WHEN()
        .topBar()
        .login()
        .userEMail(KigaConfiguration.userEmail)
        .userPassword(KigaConfiguration.userPassword)
        .submit();

    THEN().topBar().assertLoggedIn();
    AND().topBar().basketButton().shouldHaveProductsCount(1);
  }

  @Test
  public void addOneProductTwice() {
    GIVEN(Language.DEUTSCH).shop().prepare(Countries.ITALY);
    ShopData shopData1 =
        AND().shop().getDataForProduct("Product added once", Countries.ITALY, 1, 1);
    ShopData shopData2 =
        AND().shop().getDataForProduct("Product added twice", Countries.ITALY, 1, 2);

    WHEN().shop().forData(shopData1).selectProduct(0).addToBasket();
    THEN().shop().forData(shopData1).assertBasketRows(0);
    AND().topBar().basketButton().shouldHaveProductsCount(1);

    WHEN().topBar().startShop();
    AND().shop().forData(shopData1).selectProduct(0).addToBasket();
    THEN().shop().forData(shopData2).assertBasketRows(0);
    AND().topBar().basketButton().shouldHaveProductsCount(2);
  }

  @Test
  public void addEditAndDeleteProducts() {
    GIVEN(Language.DEUTSCH).shop().prepare(Countries.BELGIUM);
    ShopData shopData1 = AND().shop().getDataForProducts("Products added", Countries.BELGIUM, 2, 4);
    ShopData shopData2 =
        AND().shop().getDataForProducts("Product#1 increased", Countries.BELGIUM, 2, 2, 4);
    ShopData shopData3 =
        AND().shop().getDataForProducts("Product#2 deleted", Countries.BELGIUM, 2, 2);

    WHEN().shop().forData(shopData1).selectProduct(0).addToBasket();
    THEN()
        .shop()
        .forData(shopData1)
        .assertBasketRows(0)
        .assertBasketDelivery(0)
        .assertBasketTotal(0);
    AND().topBar().basketButton().shouldHaveProductsCount(1);

    WHEN().topBar().startShop();
    AND().shop().forData(shopData1).selectProduct(1).addToBasket();
    THEN().shop().forData(shopData1).assertBasketRows(1).assertBasketDelivery().assertBasketTotal();
    AND().topBar().basketButton().shouldHaveProductsCount(2);

    WHEN().shop().basket().table().row(0).increaseTo(2);
    THEN().shop().forData(shopData2).assertBasketRows().assertBasketDelivery().assertBasketTotal();
    AND().topBar().basketButton().shouldHaveProductsCount(3);

    WHEN().shop().basket().table().row(1).delete();
    THEN().shop().forData(shopData3).assertBasketRows().assertBasketDelivery().assertBasketTotal();
    AND().topBar().basketButton().shouldHaveProductsCount(2);
  }
}
