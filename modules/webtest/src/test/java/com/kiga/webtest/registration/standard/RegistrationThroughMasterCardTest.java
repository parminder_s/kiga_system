package com.kiga.webtest.registration.standard;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.testconfigs.BaseTest;
import com.kiga.webtest.testdata.CreditCards;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.codeborne.selenide.Configuration.browser;
import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Registration")
@Stories("Payment through MasterCard")
public class RegistrationThroughMasterCardTest extends BaseTest {
  @Test
  public void registerCustomerFor1MonthInGermany() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    CustomerData customerData = CUSTOMER_DATA().inGermanyAsTeacherFor1Month();

    WHEN().subscriptionFor(customerData).start();
    AND().cookies().acceptCookiesInfo();
    AND().registrationFor(customerData)
      .fillCustomerInformationReg()
      .submitCustomerInformation()
      .selectPayment().viaCreditCard().accept().next();
    THEN().registrationFor(customerData).summary()
      .assertRegistrationData("Pay with credit card");

    WHEN().registrationFor(customerData).summary().confirm();
    AND().paymentFor(customerData).using(CreditCards.MASTERCARD_1)
      .payAndApprove();
    THEN().welcome().assertSuccessAndGoHome();
  }
  @Test
  public void registerGiftFor6MonthWithFailedPaymentInBelgium() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    CustomerData customerData = CUSTOMER_DATA().inBelgiumAsGiftFor6Month();

    WHEN().subscriptionFor(customerData).start();
    AND().cookies().acceptCookiesInfo();
    AND().registrationFor(customerData)
      .fillCustomerInformationReg()
      .submitCustomerInformation()
      .selectPayment().viaCreditCard().accept().next();
    THEN().registrationFor(customerData).summary()
      .assertRegistrationData("Pay with credit card");

    WHEN().registrationFor(customerData).summary().confirm();
    AND().paymentFor(customerData)
      .using(CreditCards.MASTERCARD_WITH_WRONG_CREDENTIALS).pay();
    THEN().registrationFor(customerData)
      .assertWarning(
        "This payment method failed. Please choose another payment method or try again.");

    WHEN().registrationFor(customerData).submit();
    THEN().registrationFor(customerData).summary()
      .assertRegistrationData("Pay with credit card");

    WHEN().registrationFor(customerData).summary().confirm();
    AND().paymentFor(customerData).using(CreditCards.MASTERCARD_1)
      .payAndApprove();
    THEN().welcome().assertSuccessAndGoHome();
    AND().assertHomeAndLoggedOut();
  }

}
