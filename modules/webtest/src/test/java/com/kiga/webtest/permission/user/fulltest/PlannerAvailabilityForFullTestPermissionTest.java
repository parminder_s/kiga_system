package com.kiga.webtest.permission.user.fulltest;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.SessionData;
import com.kiga.webtest.datastructure.settings.Permission;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.testconfigs.BaseTest;
import org.apache.http.HttpResponse;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Permission")
@Stories("Full test permission & Planner")
@Ignore
public class PlannerAvailabilityForFullTestPermissionTest extends BaseTest {

  @Test
  public void importToMyPlanAndPrintSelectionForFreeSubscription() throws URISyntaxException, IOException {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(SubscriptionType.FREE, Permission.FULL_TEST);
    SessionData session =
      GIVEN().planner().prepareFor(customerData);

    List<String> ideaIds =
      WHEN().planner().importToMyPlan().startPrintSelection().getIdeaIds();
    HttpResponse response =
      AND().requests().printSelection(ideaIds, session.sessionID());
    THEN().requests().assertStatusCode(response, 200);
  }

  @Test
  public void createMyPlanAndPrintOverviewForFreeSubscription() throws URISyntaxException, IOException {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(SubscriptionType.FREE, Permission.FULL_TEST);
    SessionData session =
      GIVEN().planner().prepareFor(customerData);

    WHEN().planner().addFirstPlan();
    String planID =
      AND().planner().getMyWeekPlanID();
    HttpResponse response =
      AND().requests().printOverview(planID, session.sessionID());
    THEN().requests().assertStatusCode(response, 200);
  }

  @Test
  public void importToMyPlanAndPrintSelectionForStandardSubscription() throws URISyntaxException, IOException {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(SubscriptionType.TEACHERS, Permission.FULL_TEST);
    SessionData session =
      GIVEN().planner().prepareFor(customerData);

    List<String> ideaIds =
      WHEN().planner().importToMyPlan().startPrintSelection().getIdeaIds();
    HttpResponse response =
      AND().requests().printSelection(ideaIds, session.sessionID());
    THEN().requests().assertStatusCode(response, 200);

  }

  @Test
  public void createMyPlanAndPrintOverviewForStandardSubscription() throws URISyntaxException, IOException {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(SubscriptionType.TEACHERS, Permission.FULL_TEST);
    SessionData session =
      GIVEN().planner().prepareFor(customerData);

    WHEN().planner().addFirstPlan();
    String planID =
      AND().planner().getMyWeekPlanID();
    HttpResponse response =
      AND().requests().printOverview(planID, session.sessionID());
    THEN().requests().assertStatusCode(response, 200);
  }

  @Test
  public void importToMyPlanForTestExpiredSubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .expiredSubscriptionWithPermission(
        SubscriptionType.FREE, Permission.FULL_TEST);

    GIVEN().planner().prepareFor(customerData);

    WHEN().planner().startImportToMyPlan();
    THEN().navigation().assertTestExpiredErrorUrl();
  }

  @Test
  public void switchToMyPlanForTestExpiredSubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .expiredSubscriptionWithPermission(
        SubscriptionType.FREE, Permission.FULL_TEST);

    GIVEN().planner().prepareFor(customerData);

    WHEN().topBar().startPlanner().switchToMyPlans();
    THEN().navigation().assertTestExpiredErrorUrl();
  }

  @Test
  public void importToMyPlanForCommunitySubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(
        SubscriptionType.COMMUNITY, Permission.FULL_TEST);

    GIVEN().planner().prepareFor(customerData);

    WHEN().planner().startImportToMyPlan();
    THEN().navigation().assertFullRequiredErrorUrl();
  }

  @Test
  public void switchToMyPlanForCommunitySubscription() {
    CustomerData customerData = CUSTOMER_DATA()
      .subscriptionWithPermission(
        SubscriptionType.COMMUNITY, Permission.FULL_TEST);

    GIVEN().planner().prepareFor(customerData);

    WHEN().topBar().startPlanner().switchToMyPlans();
    THEN().navigation().assertFullRequiredErrorUrl();
  }


}
