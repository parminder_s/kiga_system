package com.kiga.webtest.testdata;

import com.kiga.webtest.datastructure.Countries;
import com.kiga.webtest.datastructure.Country;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.settings.Duration;
import com.kiga.webtest.datastructure.settings.Job;
import com.kiga.webtest.datastructure.settings.Permission;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.datastructure.CustomerData.CustomerDataBuilder;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.Core.core;

public class CustomerDataSet {

  @Step
  public static CustomerDataSet CUSTOMER_DATA() {
    return new CustomerDataSet();
  }

  @Step
  public CustomerData inSwitzerlandForFree() {
    return customerData("Nursery to register for free")
      .inCountry(Countries.SWITZERLAND)
      .forSubscription(SubscriptionType.FREE)
      .forCustomer(Customers.CUSTOMERS[0])
      .withEmail(core().uniqueEmail())
      .withJob(Job.NURSERY)
      .build();
  }

  @Step
  public CustomerData inSwitzerlandAsTeacherFor1Month() {
    return customerData("Teacher for 1 month in Switzerland")
      .inCountry(Countries.SWITZERLAND)
      .forSubscription(SubscriptionType.TEACHERS)
      .forDuration(Duration.MONTH_1)
      .withPrice(9.99)
      .forCustomer(Customers.CUSTOMERS[0])
      .withBirthDate(BirthDates.BIRTH_DATES[0])
      .withEmail(core().uniqueEmail())
      .withPhone("11333444666")
      .build();
  }

  @Step
  public CustomerData inGermanyAsStudentFor3Month() {
    return inGermanyAsStudentFor3Month(
      "Student for 3 month in Germany",
      core().uniqueEmail()
    );
  }

  @Step
  public CustomerData inGermanyAsStudentFor3Month(String title, String email) {
    return customerData(title)
      .inCountry(Countries.GERMANY)
      .forSubscription(SubscriptionType.TEACHERS)
      .forDuration(Duration.MONTH_3)
      .asAStudent()
      .withPrice(14.90)
      .forCustomer(Customers.CUSTOMERS[0])
      .withBirthDate(BirthDates.BIRTH_DATES[0])
      .withEmail(email)
      .withPhone("31333444666")
      .build();
  }

  @Step
  public CustomerData inGermanyAsTeacherFor1Month() {
    return customerData("Teacher for 1 month in Germany")
      .inCountry(Countries.GERMANY)
      .forSubscription(SubscriptionType.TEACHERS)
      .forDuration(Duration.MONTH_1)
      .withPrice(7.90)
      .forCustomer(Customers.CUSTOMERS[0])
      .withBirthDate(BirthDates.BIRTH_DATES[0])
      .withEmail(core().uniqueEmail())
      .withPhone("112333444666")
      .build();
  }

  @Step
  public CustomerData inGermanyAsParentFor3Month() {
    return inGermanyAsParentFor3Month("Parent for 3 month in Germany");
  }

  @Step
  public CustomerData inGermanyAsParentFor3Month(String title) {
    return inGermanyAsParentFor3Month(
      title,
      core().uniqueEmail()
    );
  }

  @Step
  public CustomerData inGermanyAsParentFor3Month(String title, String email) {
    return customerData(title)
      .inCountry(Countries.GERMANY)
      .forSubscription(SubscriptionType.PARENTS)
      .forDuration(Duration.MONTH_3)
      .withPrice(19.90)
      .forCustomer(Customers.HUGO)
      .withBirthDate(BirthDates.BIRTH_DATES[0])
      .withEmail(email)
      .withPhone("312333444666")
      .build();
  }

  @Step
  public CustomerData inBelgiumAsGiftFor6Month() {
    return customerData("Gift for 6 month in Belgium")
      .inCountry(Countries.BELGIUM)
      .forSubscription(SubscriptionType.GIFT)
      .forDuration(Duration.MONTH_6)
      .withPrice(35.90)
      .forCustomer(Customers.CUSTOMERS[0])
      .withBirthDate(BirthDates.BIRTH_DATES[0])
      .withEmail(core().uniqueEmail())
      .withPhone("61333444666")
      .build();
  }

  @Step
  public CustomerData inAustriaAsTeacherFor12Month() {
    return customerData("Teacher for 12 month in Austria")
      .inCountry(Countries.AUSTRIA)
      .forSubscription(SubscriptionType.TEACHERS)
      .forDuration(Duration.MONTH_12)
      .withPrice(59.90)
      .forCustomer(Customers.CUSTOMERS[0])
      .withBirthDate(BirthDates.BIRTH_DATES[0])
      .withEmail(core().uniqueEmail())
      .withPhone("121333444666")
      .build();
  }

  @Step
  public CustomerData inAustriaAsParentFor12Month() {
    return inAustriaAsParentFor12Month("Parent for 12 month in Austria");
  }

  @Step
  public CustomerData inAustriaAsParentFor12Month(String title) {
    return customerData(title)
      .inCountry(Countries.AUSTRIA)
      .forSubscription(SubscriptionType.PARENTS)
      .forDuration(Duration.MONTH_12)
      .withPrice(59.90)
      .forCustomer(Customers.CUSTOMERS[0])
      .withBirthDate(BirthDates.BIRTH_DATES[0])
      .withEmail(core().uniqueEmail())
      .withPhone("121333444666")
      .build();
  }

  @Step
  public CustomerData inAustriaAsSchoolWith3AdditionalLicencesFor12Month() {
    return customerData(
      "School for 12 month with 3 additional licences in Austria")
      .inCountry(Countries.AUSTRIA)
      .forSubscription(SubscriptionType.SCHOOLS)
      .forDuration(Duration.MONTH_12)
      .withAdditionalLicences(3)
      .withPrice(104.60)
      .forCustomer(Customers.SCHOOL)
      .withBirthDate(BirthDates.BIRTH_DATES[0])
      .withEmail(core().uniqueEmail())
      .withPhone("123333444666")
      .build();
  }

  @Step
  public CustomerData inTurkeyAsTeacherFor12Month() {
    return customerData("Teacher for 12 month in Turkey")
      .inCountry(Countries.TURKEY)
      .forSubscription(SubscriptionType.TEACHERS)
      .forDuration(Duration.MONTH_12)
      .withPrice(199.90)
      .forCustomer(Customers.CUSTOMERS[0])
      .withBirthDate(BirthDates.BIRTH_DATES[0])
      .withEmail(core().uniqueEmail())
      .withPhone("540123456")
      .withPassportNumber("CI 78963")
      .build();
  }

  @Step
  public CustomerData paidSubscription() {
    String email = core().uniqueEmail();
    return customerData("Paid subscribed customer with email " + email)
      .withEmail(email)
      .build();
  }

  @Step
  public CustomerData paidSubscriptionInLiechtenstein() {
    String email = core().uniqueEmail();
    return customerData(
      "Paid subscribed customer in Liechtenstein with email " + email)
      .inCountry(Countries.LIECHTENSTEIN)
      .withEmail(email)
      .forCustomer(Customers.HUGO)
      .build();
  }

  @Step
  public CustomerData paidSubscriptionIn(Country country) {
    String email = core().uniqueEmail();
    return customerData(
      "Paid subscribed customer in " + country + " with email " + email)
      .inCountry(country)
      .forCustomer(Customers.HUGO)
      .withEmail(email)
      .withPhone("789654123")
      .build();
  }

  @Step
  public CustomerData freeSubscription() {
    String email = core().uniqueEmail();
    return customerData("Free subscribed customer with email " + email)
      .forSubscription(SubscriptionType.FREE)
      .withEmail(email)
      .build();
  }

  @Step
  public CustomerData subscriptionWithPermission(
    SubscriptionType subscriptionType, Permission permission) {
    return customerData(
      "Subscription " + subscriptionType + " with permission " + permission)
      .forSubscription(subscriptionType)
      .withPermission(permission)
      .build();
  }

  @Step
  public CustomerData expiredSubscriptionWithPermission(
    SubscriptionType subscriptionType, Permission permission) {
    return customerData(
      "Subscription " + subscriptionType + " with permission " + permission)
      .forSubscription(subscriptionType)
      .withPermission(permission)
      .asExpired()
      .build();
  }

  @Step
  private CustomerDataBuilder customerData(String title) {
    return new CustomerDataBuilder(title);
  }

}
