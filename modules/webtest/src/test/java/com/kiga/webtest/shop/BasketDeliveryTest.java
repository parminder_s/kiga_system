package com.kiga.webtest.shop;

import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.Countries;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Before;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.codeborne.selenide.Configuration.browser;
import static com.kiga.webtest.testdata.ShopDataSet.SHOP_DATA;

@Features("Shop")
@Stories("Delivery")
public class BasketDeliveryTest extends BaseTest {
  @Test
  public void deliverySumDependsOnTotal() {
    GIVEN(Language.DEUTSCH).shop().prepare(Countries.LUXEMBOURG);
    ShopData shopData15 = SHOP_DATA().huehnerMiniLappokInLuxembourg(15);
    ShopData shopData16 = SHOP_DATA().huehnerMiniLappokInLuxembourg(16);

    WHEN().shop().forData(shopData15).addToBasket(0);
    THEN().shop().forData(shopData15)
      .assertBasketRows().assertBasketDelivery().assertBasketTotal();

    WHEN().shop().basket().table().row(0).increaseTo(16);
    AND().shop().forData(shopData16)
      .assertBasketRows().assertBasketDelivery().assertBasketTotal();
  }
}
