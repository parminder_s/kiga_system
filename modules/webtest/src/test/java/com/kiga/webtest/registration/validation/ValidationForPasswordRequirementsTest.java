package com.kiga.webtest.registration.validation;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.codeborne.selenide.Configuration.browser;
import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Registration")
@Stories("Validation")
public class ValidationForPasswordRequirementsTest extends BaseTest {
  @Test
  public void checkOnPasswordLength() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    CustomerData customerData = CUSTOMER_DATA().inGermanyAsParentFor3Month();
    AND().subscriptionFor(customerData).start();
    AND().registrationFor(customerData).fillCustomerInformationReg();

    WHEN().registrationFor(customerData).fields()
      .password().setValue("short");
    THEN().registrationFor(customerData).validation()
      .assertPasswordIsTooShort();
  }
  @Test
  public void checkOnPasswordConfirmationLength() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    CustomerData customerData = CUSTOMER_DATA().inAustriaAsTeacherFor12Month();
    AND().subscriptionFor(customerData).start();
    AND().registrationFor(customerData).fillCustomerInformationReg();

    WHEN().registrationFor(customerData).fields()
      .password().clear().setValue("short");
    AND().registrationFor(customerData).fields()
      .passwordConfirmation().clear().setValue("short");
    THEN().registrationFor(customerData).validation()
      .assertPasswordIsTooShort()
      .assertPasswordConfirmationIsTooShort();
  }
}
