package com.kiga.webtest.testdata;

import com.kiga.webtest.datastructure.KiGaDate;

import java.util.Random;

public class BirthDates {

  public final static KiGaDate[] BIRTH_DATES = {
    new KiGaDate(23, 2, 1989),
    new KiGaDate(26, 05, 2000),
    new KiGaDate(1, 3, 2001),
    new KiGaDate(5, 1, 1903),
    new KiGaDate(16, 9, 1956)
  };

  public static KiGaDate random() {
    return BIRTH_DATES[new Random().nextInt(BIRTH_DATES.length)];
  }

}
