package com.kiga.webtest.registration.validation;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.kiga.webtest.core.Core.core;
import static com.kiga.webtest.pages.helpers.RegistrationFieldsHelper.registrationFields;
import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Registration")
@Stories("Validation")
public class ValidationForCredentialsConfirmationTest extends BaseTest {

  @Test
  public void checkOnSameEMail() {
    CustomerData customerData = CUSTOMER_DATA().inGermanyAsParentFor3Month();
    String emailConfirmationField =
      registrationFields(customerData).confirmEmail();

    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    AND().subscriptionFor(customerData).start();
    AND().registrationFor(customerData).fillCustomerInformationReg();

    WHEN().registrationFor(customerData).fieldReg(emailConfirmationField)
      .setValue(core().unique("newemail", "@k.tt4.at")).pressEnter();
    THEN().registrationFor(customerData).validation()
      .assertConfirmationDoNotMatch("emails", emailConfirmationField);
  }

  @Test
  public void checkOnSamePassword() {
    CustomerData customerData = CUSTOMER_DATA().inGermanyAsParentFor3Month();
    String passwordConfirmationField =
      registrationFields(customerData).confirmPassword();

    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    AND().subscriptionFor(customerData).start();
    AND().registrationFor(customerData).fillCustomerInformationReg();

    WHEN().registrationFor(customerData).fieldReg(passwordConfirmationField)
      .setValue("not the same password");
    AND().registrationFor(customerData).submitCustomerInformation();
    THEN().registrationFor(customerData).validation()
      .assertConfirmationDoNotMatch("passwords", passwordConfirmationField);
  }

}
