package com.kiga.webtest.testconfigs;

import com.codeborne.selenide.WebDriverRunner;
import com.kiga.webtest.config.Language;
import com.kiga.webtest.core.angular.wait.AngularWait;
import com.kiga.webtest.config.KigaConfiguration;
import com.codeborne.selenide.Screenshots;
import com.google.common.io.Files;
import org.junit.After;
import org.junit.Before;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.File;
import java.io.IOException;

import com.kiga.webtest.pages.App;

import static com.kiga.webtest.core.Core.core;

public class BaseTest {

  static {
    KigaConfiguration.init();
  }

  @After
  public void tearDown() throws IOException {
    if (WebDriverRunner.hasWebDriverStarted()) {
      screenshot();
    }
    System.out.println(AngularWait.getStatistic());
  }

  @Attachment(type = "image/png")
  public byte[] screenshot() throws IOException {
    File screenshot = Screenshots.takeScreenShotAsFile();
    if (screenshot == null) {
      return new byte[0];
    }
    return Files.toByteArray(screenshot);
  }

  /*
   * methods for
   * - access to the one entry point for all widgets
   * - good reporting and readability of tests in BDD-style
   */

  private App app;

  private App newApp(Language language) {
    app = new App(language);
    return app;
  }

  private App app() {
    if (app == null) {
      return newApp(Language.getDefault());
    }
    return app;
  }

  @Step
  public App GIVEN(Language language) {
    return newApp(language);
  }

  @Step
  public App GIVEN() {
    return newApp(Language.getDefault());
  }

  @Step
  public App WHEN() {
    return app();
  }

  @Step
  public App THEN() {
    return app();
  }

  @Step
  public App AND() {
    return app();
  }

}
