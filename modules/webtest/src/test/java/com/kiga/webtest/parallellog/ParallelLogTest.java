package com.kiga.webtest.parallellog;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Configuration.browser;
import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import com.kiga.webtest.datastructure.SessionData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.Cookie;

public class ParallelLogTest extends BaseTest {

  @After
  public void closeDriverAfterEachTest() {
    closeWebDriver();
  }

  @Test
  public void simpleSessionSwitchingTest() {
    GIVEN().endPoints()
      .quickRegisterFor(null)
      .registerAndGetEmail();
    int userId = AND().endPoints().memberInfo().identifyUserId();
    Cookie session = getWebDriver().manage().getCookieNamed("PHPSESSID");
    String firstSession = session.getValue();
    getWebDriver().manage().deleteCookie(session);
    AND().endPoints()
      .quickRegisterFor(null)
      .registerAndGetEmail();
    int secondUserId = AND().endPoints().memberInfo().identifyUserId();
    assertNotEquals(userId, secondUserId);
    getWebDriver().manage().addCookie(new Cookie("PHPSESSID", firstSession, "/"));
    assertEquals(userId, AND().endPoints().memberInfo().identifyUserId());
  }
  @Test
  public void parallelLoginErrorAfterOpeningSecondSessionForTheSameUser() {
    //session 1
    SessionData session1 =
      GIVEN().endPoints().createNewSession(null);

    //session 2
    WHEN().browser().deleteAllCookies();
    AND().navigation().visitHome();
    AND().topBar().login(session1.email(), "test1234");
    THEN().topBar().assertLoggedIn();

    //restore session 1
    WHEN().browser().switchToSession(session1);
    //AND().navigation().visit();
    String errorCode = AND().endPoints().memberInfo().identifyErrorCode();
    assertEquals("parallel_login", errorCode);
  }
  @Ignore
  @Test
  public void multipleLoginShouldBeOpenedAfterOpeningSecondSessionForTheSameUser() {

    //session 1
    SessionData session1 =
      GIVEN().endPoints().createNewSession(null);

    //session 2
    WHEN().browser().deleteAllCookies();
    AND().navigation().visitHome();
    AND().topBar().login(session1.email(), "test1234");
    THEN().topBar().assertLoggedIn();

    //restore session 1
    WHEN().browser().switchToSession(session1);
    AND().topBar().navBarLeft().button("Favorites").click();
    $(".kiga-container .panel-heading").shouldHave(exactText("Multiple Login"));
    $(".kiga-container .panel-body").shouldHave(
      text(session1.email() + " is currently used by another member. Due to security reasons you have been logged out automatically."),
      text("The safety of your KiGaPortal account is very important for us. Please report any potential abuse of your account to service@kigaportal.com."),
      text("To proceed, please login again.")
    );

    $(".kiga-container .form-group [ui-sref='root.login']").click();
    THEN().navigation().assertLoginUrl();
  }
}
