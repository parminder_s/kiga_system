package com.kiga.webtest.testdata;


import java.util.Random;

import com.kiga.webtest.datastructure.Customer;

import static com.kiga.webtest.datastructure.settings.Gender.*;

public class Customers {

  public final static Customer[] CUSTOMERS = {
    new Customer(FEMALE, "TestFirstname", "Test Last Name",
      "test1234",
      "TestStreet", "7654", "", "",
      "4567-TEST", "TestCITY"),

    new Customer(FEMALE, "!TestFirstname!!! ", "!TestLastname!!!! ",
      "TestPassword123!!! ",
      "TestStreet ", "7654??? ", "", "",
      "4567-CH ", "TestCITY!()! "),

    new Customer(FEMALE, "testfirstname", "testlastname",
      "testpassword",
      "CHEESEWAY", "EIGHTYTWO", "", "",
      "SWIZERLAND", "BERN"),

    new Customer(FEMALE, "TESTFIRSTNAME", "TESTLASTNAME",
      "TESTPASSWORD",
      "cheeseway", "eightytwo", "", "",
      "swizerland", "bern"),

    new Customer(FEMALE, "$!!&/)()(", "$!!&/)()(",
      "$!!&/)()(",
      "-TestStreet-8", "-9-", "", "",
      "4567-CH", "TEST town!"),

    new Customer(FEMALE, "TestFirstname001", "TestLastname001",
      "TESTpassword001", "TestStreet", "7654", "", "",
      "4567-CH", "TestCITY")
  };

  public final static Customer HUGO = new Customer(MALE, "Hugo", "Hugenberger",
    "test1234",
    "Under den Linden", "1", "2", "3",
    "4444", "Berlin");

  public final static Customer SCHOOL = new Customer(MALE, "Hugo", "Hugenberger",
    "test1234",
    "Under den Linden", "1", "2", "3",
    "4444", "Berlin",
    "Berlin Elementary School", "GMBH");


  public static Customer random() {
    return CUSTOMERS[new Random().nextInt(CUSTOMERS.length)];
  }

}
