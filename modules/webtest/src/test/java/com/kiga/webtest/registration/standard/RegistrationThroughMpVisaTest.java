package com.kiga.webtest.registration.standard;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.testconfigs.BaseTest;
import com.kiga.webtest.testdata.CreditCards;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Registration")
@Stories("Payment through Visa")
public class RegistrationThroughMpVisaTest extends BaseTest {
  @Test
  public void registerCustomerFor1MonthInSwitzerland() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    CustomerData customerData = CUSTOMER_DATA().inSwitzerlandAsTeacherFor1Month();

    WHEN().subscriptionFor(customerData).start();
    AND().cookies().acceptCookiesInfo();
    AND()
        .registrationFor(customerData)
        .fillCustomerInformationReg()
        .submitCustomerInformation()
        .selectPayment()
        .viaCreditCard()
        .accept()
        .next();
    THEN().registrationFor(customerData).summary().assertRegistrationData("Pay with credit card");

    WHEN().registrationFor(customerData).summary().confirm();
    AND().paymentFor(customerData).using(CreditCards.VISA_1).payAndApprove();
    THEN().welcome().assertSuccessAndGoHome();
  }

  @Test
  public void registerParentsFor3MonthInGermany() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    CustomerData customerData = CUSTOMER_DATA().inGermanyAsParentFor3Month();

    WHEN().subscriptionFor(customerData).start();
    AND().cookies().acceptCookiesInfo();
    AND()
        .registrationFor(customerData)
        .fillCustomerInformationReg()
        .submitCustomerInformation()
        .selectPayment()
        .viaCreditCard()
        .accept()
        .next();
    THEN().registrationFor(customerData).summary().assertRegistrationData("Pay with credit card");

    WHEN().registrationFor(customerData).summary().confirm();
    AND().paymentFor(customerData).using(CreditCards.VISA_1).payAndApprove("3-Monate");
    THEN().welcome().assertSuccessAndGoHome();
    AND().assertHomeAndLoggedOut();
  }
}
