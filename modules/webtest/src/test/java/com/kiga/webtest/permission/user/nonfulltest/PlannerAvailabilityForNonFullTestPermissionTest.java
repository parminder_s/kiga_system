package com.kiga.webtest.permission.user.nonfulltest;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.SessionData;
import com.kiga.webtest.datastructure.settings.Permission;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.testconfigs.BaseTest;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import org.apache.http.HttpResponse;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Permission")
@Stories("Non full test permission & Planner")
public class PlannerAvailabilityForNonFullTestPermissionTest extends BaseTest {
  @Ignore
  @Test
  public void plannerAddToSelectedDate() throws URISyntaxException, IOException {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.PARENTS, Permission.NON_FULL_TEST);
    SessionData session = GIVEN().planner().prepareFor(customerData);

    WHEN().planner().addFirstPlan();
    String planID = AND().planner().addFirstPlanAndGetId(); // fail here
  }

  @Test
  public void importToMyPlanAndPrintSelectionForFreeSubscription()
      throws URISyntaxException, IOException {
    CustomerData customerData =
        CUSTOMER_DATA().subscriptionWithPermission(SubscriptionType.FREE, Permission.NON_FULL_TEST);
    GIVEN().planner().prepareFor(customerData);
    WHEN().planner().importToMyPlan().startPrintSelection();
    THEN().navigation().assertFullUpgradeErrorUrl();
  }

  @Test
  public void createMyPlanAndPrintOverviewForFreeSubscription()
      throws URISyntaxException, IOException {
    CustomerData customerData =
        CUSTOMER_DATA().subscriptionWithPermission(SubscriptionType.FREE, Permission.NON_FULL_TEST);
    SessionData session = GIVEN().planner().prepareFor(customerData);

    WHEN().planner().addFirstPlan();
    String planID = AND().planner().getMyWeekPlanID();
    HttpResponse response = AND().requests().printOverview(planID, session.sessionID());
    THEN().requests().assertStatusCode(response, 200);
  }

  @Test
  public void importToMyPlanAndPrintSelectionForStandardSubscription()
      throws URISyntaxException, IOException {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.TEACHERS, Permission.NON_FULL_TEST);
    SessionData session = GIVEN().planner().prepareFor(customerData);

    List<String> ideaIds = WHEN().planner().importToMyPlan().startPrintSelection().getIdeaIds();
    HttpResponse response = AND().requests().printSelection(ideaIds, session.sessionID());
    THEN().requests().assertStatusCode(response, 200);
  }

  @Test
  public void createMyPlanAndPrintOverviewForStandardSubscription()
      throws URISyntaxException, IOException {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.TEACHERS, Permission.NON_FULL_TEST);
    SessionData session = GIVEN().planner().prepareFor(customerData);

    WHEN().planner().addFirstPlan();
    String planID = AND().planner().getMyWeekPlanID();
    HttpResponse response = AND().requests().printOverview(planID, session.sessionID());
    THEN().requests().assertStatusCode(response, 200);
  }

  @Test
  public void importToMyPlanForTestExpiredSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .expiredSubscriptionWithPermission(SubscriptionType.FREE, Permission.NON_FULL_TEST);

    GIVEN().planner().prepareFor(customerData);

    WHEN().planner().startImportToMyPlans();
    THEN().navigation().assertTestExpiredErrorUrl();
  }

  @Ignore
  @Test
  public void switchToMyPlanForTestExpiredSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .expiredSubscriptionWithPermission(SubscriptionType.FREE, Permission.NON_FULL_TEST);

    GIVEN().planner().prepareFor(customerData);

    WHEN().topBar().startPlanner().switchToMyPlans();
    THEN().navigation().assertTestExpiredErrorUrl();
  }

  @Test
  public void importToMyPlanForCommunitySubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.COMMUNITY, Permission.NON_FULL_TEST);

    GIVEN().planner().prepareFor(customerData);

    WHEN().planner().startImportToMyPlans();
    THEN().navigation().assertFullRequiredErrorUrl();
  }

  @Ignore
  @Test
  public void switchToMyPlanForCommunitySubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .subscriptionWithPermission(SubscriptionType.COMMUNITY, Permission.NON_FULL_TEST);

    GIVEN().planner().prepareFor(customerData);

    WHEN().topBar().startPlanner().switchToMyPlans();
    THEN().navigation().assertFullRequiredErrorUrl();
  }

  @Test
  public void importToMyPlanForStandardExpiredSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .expiredSubscriptionWithPermission(SubscriptionType.TEACHERS, Permission.NON_FULL_TEST);

    GIVEN().planner().prepareFor(customerData);

    WHEN().planner().startImportToMyPlans();
    THEN().navigation().assertFullExpiredErrorUrl();
  }

  @Ignore
  @Test
  public void switchToMyPlanForStandardExpiredSubscription() {
    CustomerData customerData =
        CUSTOMER_DATA()
            .expiredSubscriptionWithPermission(SubscriptionType.TEACHERS, Permission.NON_FULL_TEST);

    GIVEN().planner().prepareFor(customerData);

    WHEN().topBar().startPlanner().switchToMyPlans();
    THEN().navigation().assertFullExpiredErrorUrl();
  }
}
