package com.kiga.webtest.registration.standard;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.testconfigs.BaseTest;
import com.kiga.webtest.testdata.ELVs;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Registration")
@Stories("Payment through Elv")
public class RegistrationThroughElvTest extends BaseTest {
  @Test
  public void registerStudentFor3MonthInGermany() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    CustomerData customerData = CUSTOMER_DATA().inGermanyAsStudentFor3Month();

    WHEN().subscriptionFor(customerData).start();
    AND().cookies().acceptCookiesInfo();
    AND().registrationFor(customerData)
      .fillCustomerInformationReg()
      .submitCustomerInformation()
      .selectPayment().viaElv().accept().next();
    THEN().registrationFor(customerData).summary()
      .assertRegistrationData("Payment via electronic direct debit");

    WHEN().registrationFor(customerData).summary().confirm();
    AND().paymentFor(customerData).using(ELVs.ELV_0).payAndApprove();
    THEN().welcome().assertSuccessAndGoHome();
    AND().assertHomeAndLoggedOut();
  }

}
