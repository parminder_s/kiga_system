package com.kiga.webtest.registration.standard;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.codeborne.selenide.Configuration.browser;
import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Registration")
@Stories("Payment through Invoice")
public class RegistrationThroughInvoiceTest extends BaseTest {
  @Test
  public void registerCustomerFor12MonthViaPostInAustria() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    CustomerData customerData = CUSTOMER_DATA().inAustriaAsTeacherFor12Month();

    WHEN().subscriptionFor(customerData).start();
    AND().cookies().acceptCookiesInfo();
    WHEN().registrationFor(customerData)
      .fillCustomerInformationReg()
      .submitCustomerInformation()
      .selectPayment().invoiceViaPost().accept().next();
    THEN().registrationFor(customerData).summary()
      .assertRegistrationData("Pay with invoice");

    WHEN().registrationFor(customerData).summary().confirm();
    THEN().welcome().assertSuccessAndGoHome();
  }
  @Test
  public void registerParentsFor12MonthViaEmailInAustria() {
    GIVEN().ensureLoggedOut();
    AND().navigation().visitSubscription();
    CustomerData customerData = CUSTOMER_DATA().inAustriaAsParentFor12Month();

    WHEN().subscriptionFor(customerData).start();
    AND().cookies().acceptCookiesInfo();
    AND().registrationFor(customerData)
      .fillCustomerInformationReg()
      .submitCustomerInformation()
      .selectPayment().invoiceViaEmail().accept().next();
    THEN().registrationFor(customerData).summary()
      .assertRegistrationData("Pay with invoice");

    WHEN().registrationFor(customerData).summary().confirm();
    THEN().welcome().assertSuccessAndGoHome();
    AND().assertHomeAndLoggedOut();
  }

}
