package com.kiga.webtest.registration.standard;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Registration")
@Stories("Standard")
public class RegistrationWithDataChangeTest extends BaseTest {
  @Test
  public void changeInformationAfterEditChosenSubscription() {
    CustomerData oldCustomerData = CUSTOMER_DATA()
      .inAustriaAsParentFor12Month("Old Customer data");
    CustomerData newCustomerData = CUSTOMER_DATA()
      .inGermanyAsParentFor3Month("New customer data");
    WHEN().navigation().visitHome();
    GIVEN().registrationFor(oldCustomerData).fillDataReg();

    WHEN().registrationFor(oldCustomerData).summary().editChosenSubscription();
    THEN().subscriptionFor(oldCustomerData)
      .assertCountry(oldCustomerData.country());

    WHEN().subscriptionFor(newCustomerData).selectCountryAndSubscriptionType();
    THEN().subscriptionFor(oldCustomerData)
      .assertDuration(oldCustomerData.duration())
      .assertAmIStudent(oldCustomerData.isStudent());

    WHEN().subscriptionFor(newCustomerData).continueSubscription();
    THEN().registrationFor(oldCustomerData).assertCustomerInformation();

    WHEN().registrationFor(newCustomerData)
      .fillCustomerInformationReg()
      .submitCustomerInformation();
    THEN().registrationFor(oldCustomerData)
      .paymentMethods().assertInvoiceViaEmail();

    WHEN().registrationFor(newCustomerData).selectPayment().viaElv().next();
    THEN().registrationFor(newCustomerData).summary()
      .assertRegistrationData("Payment via electronic direct debit");
  }

}
