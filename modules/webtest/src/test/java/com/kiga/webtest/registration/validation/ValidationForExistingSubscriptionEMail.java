package com.kiga.webtest.registration.validation;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.codeborne.selenide.Configuration.browser;
import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Registration")
@Stories("Validation")
public class ValidationForExistingSubscriptionEMail extends BaseTest {
  @Test
  public void registerFailedWithExistingSubscriptionEMail() {
    CustomerData customerData1 = CUSTOMER_DATA().paidSubscription();
    GIVEN().endPoints().quickRegisterFor(customerData1).registerAndGetEmail();
    AND().ensureLoggedOut();
    CustomerData customerData2 = CUSTOMER_DATA().inGermanyAsParentFor3Month(
      "Customer with the same email", customerData1.email());
    AND().navigation().visitSubscription();
    AND().subscriptionFor(customerData2).start();
    AND().registrationFor(customerData2).fillCustomerInformationReg();

    WHEN().registrationFor(customerData2).submitCustomerInformation();
    THEN().registrationFor(customerData2).
      validation().assertEmailIsUsed();
  }
  @Test
  public void registerPassedWithExistingTestSubscriptionEMail() {
    CustomerData customerData1 = CUSTOMER_DATA().freeSubscription();
    GIVEN().endPoints().quickRegisterFor(customerData1).registerAndGetEmail();
    AND().ensureLoggedOut();
    CustomerData customerData2 = CUSTOMER_DATA().inGermanyAsStudentFor3Month(
      "Customer with the same email", customerData1.email());
    AND().navigation().visitSubscription();
    AND().subscriptionFor(customerData2).start();
    AND().cookies().acceptCookiesInfo();
    AND().registrationFor(customerData2).fillCustomerInformationReg();

    WHEN().registrationFor(customerData2).submitCustomerInformation();
    THEN().registrationFor(customerData2)
      .selectPayment().invoiceViaEmail().accept().next();
    THEN().registrationFor(customerData2).summary()
      .assertRegistrationData("Pay with invoice");

    WHEN().registrationFor(customerData2).summary().confirm();
    THEN().welcome().assertSuccessAndGoHome();
    AND().assertHomeAndLoggedOut();
  }

}
