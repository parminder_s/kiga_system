package com.kiga.webtest.shop;

import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.Countries;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.datastructure.settings.ShopPaymentMethod;
import com.kiga.webtest.testconfigs.BaseTest;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Shop")
@Stories("Availability")
public class ShopAvailabilityTest extends BaseTest {
  @Ignore
  @Test
  public void shopPurchaseAddressDataSubmitAndResume() {
    CustomerData oldCustomerData = CUSTOMER_DATA().paidSubscriptionIn(Countries.GERMANY);
    CustomerData newCustomerData = CUSTOMER_DATA().inTurkeyAsTeacherFor12Month();
    ShopData shopData =
        GIVEN(Language.DEUTSCH).shop().prepareForCustomerAndProduct(oldCustomerData, 1, 1);

    WHEN().cookies().acceptCookiesInfo();
    AND().shop().forData(shopData).addToBasket(0).startPay();
    AND()
        .shop()
        .basket()
        .startPay()
        .fillPhone(newCustomerData.phone())
        .fillAddress(newCustomerData)
        // .changeData(newCustomerData)
        .next();
    // GIVEN().registrationFor(newCustomerData).fillDataReg();

    AND()
        .shop()
        .forData(shopData, oldCustomerData, ShopPaymentMethod.INVOICE)
        .selectPaymentMethod();

    THEN().shop().summary().assertCustomerAddress(newCustomerData);
  }

  @Test
  public void shopIsNotAvailableForGermanyAndEnglishLanguage() {
    GIVEN(Language.ENGLISH).endPoints().country().set(Countries.GERMANY);

    WHEN().navigation().visitHome();
    THEN().topBar().navBarLeft().shouldNotExist("Products");
  }

  @Test
  public void purchasesAreAvailableForGermanyAndGermanLanguage() {
    GIVEN(Language.DEUTSCH).endPoints().country().set(Countries.GERMANY);
    AND().navigation().visitHome();

    WHEN().topBar().startShop().selectProduct(0);
    THEN().shop().product().addToBasketShouldBeAvailable();
  }
}
