package com.kiga.webtest.shop;

import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.Countries;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.datastructure.settings.ShopPaymentMethod;
import com.kiga.webtest.testconfigs.BaseTest;
import com.kiga.webtest.testdata.CreditCards;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.codeborne.selenide.Configuration.browser;
import static com.kiga.webtest.testdata.CustomerDataSet.CUSTOMER_DATA;

@Features("Shop")
@Stories("Checkout and payment through Visa")
public class ShopCheckoutAndPaymentThroughVisaTest extends BaseTest {
  @Test
  public void checkoutAndPay() {
    CustomerData customerData =
      CUSTOMER_DATA().paidSubscriptionIn(Countries.AUSTRIA);
    ShopData shopData = GIVEN(Language.DEUTSCH).shop()
      .prepareForCustomerAndProduct(customerData, 7, 5);

    WHEN().cookies().acceptCookiesInfo();
    AND().shop()
      .forData(shopData, customerData, ShopPaymentMethod.CREDIT_CARD)
      .addToBasketAndStartPay();
    AND().shop()
      .paymentFor(customerData, shopData).using(CreditCards.VISA_1)
      .payAndApprove();
    THEN().shop().assertFinished();
  }
  @Test
  public void checkoutAndPayUsing2Attempts() {
    CustomerData customerData =
      CUSTOMER_DATA().paidSubscriptionIn(Countries.AUSTRIA);
    ShopData shopData = GIVEN(Language.DEUTSCH).shop()
      .prepareForCustomerAndProduct(customerData, 7, 5);

    WHEN().cookies().acceptCookiesInfo();
    AND().shop().forData(shopData, customerData, ShopPaymentMethod.CREDIT_CARD)
      .addToBasketAndStartPay();
    AND().shop().paymentFor(customerData, shopData)
      .using(CreditCards.VISA_WITH_WRONG_CREDENTIALS)
      .pay();
    THEN().shop().paymentFor(customerData, shopData)
      .using(CreditCards.VISA_WITH_WRONG_CREDENTIALS)
      .redirect().assertDeclined();

    WHEN().shop().paymentFor(customerData, shopData)
      .using(CreditCards.VISA_WITH_WRONG_CREDENTIALS)
      .redirect().next();
    THEN().shop().paymentMethod().assertWarning();

    WHEN().shop().paymentMethod()
      .acceptTermsConditions().next();
    AND().shop()
      .forData(shopData, customerData, ShopPaymentMethod.CREDIT_CARD)
      .assertSummaryAndStartPay();
    AND().shop()
      .paymentFor(customerData, shopData).using(CreditCards.VISA_1)
      .payAndApprove();
    THEN().shop().assertFinished();
  }

}
