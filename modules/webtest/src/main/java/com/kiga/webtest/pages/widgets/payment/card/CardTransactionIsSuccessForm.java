package com.kiga.webtest.pages.widgets.payment.card;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.*;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.helpers.LocaleHelpers.decimalFormat;

public class CardTransactionIsSuccessForm extends BasePage {

  private final SelenideElement container = $("body");

  public CardTransactionIsSuccessForm(App app) {
    super(app);
  }

  @Step
  public CardTransactionIsSuccessForm assertTotal(double sum, Currency currency) {
    this.container.find("#total>td").shouldHave(
      exactText(
        currency + " " +
          decimalFormat(language, sum, "#.00")
      )
    );
    return this;
  }

  @Step
  public CardTransactionIsSuccessForm assertOpened() {
    this.container.find("#success_title")
      .shouldHave(exactText("Transaktion erfolgreich!"));
    return this;
  }

  @Step
  public void next() {
    new Buttons(this.container, "#buttons>.btn")
      .clickBy(value(language.term("creditcard.redirect.continue")));
  }

}
