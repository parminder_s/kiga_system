package com.kiga.webtest.pages.widgets.payment.paypal;

import static com.kiga.webtest.core.angular.AngularHelpers.$;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.PayPalAccount;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class PayPalLogin extends BasePage {

  private final SelenideElement container;

  public PayPalLogin(App app, SelenideElement container) {
    super(app);
    this.container = container;
  }

  @Step
  public PayPalReviewInformation login(PayPalAccount payPalAccount) {
    this.container.find("#email").setValue(payPalAccount.email()).pressEnter();
    this.container.find("#password").setValue(payPalAccount.password()).pressEnter();
    $(".continueButton").click();
    $("#confirmButtonTop").click();
    return new PayPalReviewInformation(app, this.container);
  }
}
