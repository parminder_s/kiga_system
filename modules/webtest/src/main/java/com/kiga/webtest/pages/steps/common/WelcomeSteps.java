package com.kiga.webtest.pages.steps.common;

import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.common.WelcomeForm;
import ru.yandex.qatools.allure.annotations.Step;

public class WelcomeSteps extends BasePage {

  public WelcomeSteps(App app) {
    super(app);
  }

  @Step
  public void assertSuccessAndGoHome() {
    welcomeForm()
      .assertSuccess()
      .goToHomePage();
  }
  @Step
  public void assertSuccessAndGoHomeShop() {
    welcomeForm()
      .assertSuccessShop()
      .goToHomePageShop();
  }
  //**********************Steps implementation************

  @Step
  private WelcomeForm welcomeForm() {
    return new WelcomeForm(app);
  }

}
