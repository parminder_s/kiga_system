package com.kiga.webtest.pages.widgets.registration;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.settings.Gender;
import com.kiga.webtest.datastructure.settings.Job;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.helpers.basic.DataForGroup;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.*;
import com.kiga.webtest.pages.widgets.common.WelcomeForm;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.checks.CustomConditions.dt;

public class FreeRegistrationForm extends BasePage {

  private final SelenideElement container = $("[role='form']");
  private final SelenideElement jobs = this.container.find("kiga-select[options='ctrl.jobs']");

  public FreeRegistrationForm(App app) {
    super(app);
  }

  @Step
  public FreeRegistrationForm fillCommonInformation(
    Gender gender, DataForGroup dataForGroup) {

    new RadioGroup(this.container).
      clickOnGender(gender.toString());
    new TextFieldGroup(this.container, dataForGroup.attrDataTestValues())
      .fill(dataForGroup.texts());
    return this;
  }

  @Step
  public FreeRegistrationForm assertCommonInformation(
    Gender gender, DataForGroup dataForGroup) {

    new RadioGroup(this.container).
      shouldBeSelected(gender.toString());
    new TextFieldGroup(this.container, dataForGroup.attrDataTestValues())
      .shouldHaveValues(dataForGroup.texts());
    return this;
  }

  @Step
  public FreeRegistrationForm assertCommonInformationReg(
    Gender gender, DataForGroup dataForGroup) {

    new RadioGroup(this.container).
      shouldBeSelected(gender.toString());
    new TextFieldGroupReg(this.container, dataForGroup.attrDataTestValues())
      .shouldHaveValues(dataForGroup.texts());
    return this;
  }

  @Step
  public FreeRegistrationForm fillJob(Job job) {
    new DropDownList(
      jobs, ".ui-select-choices-row div")
      .open().selectBy(dt(job.getAttrDataTestValue()));
    return this;
  }

  @Step
  public FreeRegistrationForm assertJob(Language language, Job job) {
    this.jobs.shouldHave(exactText(
        language.term("job." + job.getAttrDataTestValue())
      ));
    return this;
  }

  @Step
  public WelcomeForm register() {
    new Buttons(this.container).click("Register");
    return new WelcomeForm(app);
  }

  @Step
  public FreeRegistrationForm acceptTermsConditionsAndSecurity() {
    new CheckBoxGroup(this.container, "agb", "security", "datenschutz").checkAll();
    return this;
  }

}
