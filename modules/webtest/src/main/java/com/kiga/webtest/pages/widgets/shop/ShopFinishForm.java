package com.kiga.webtest.pages.widgets.shop;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;

public class ShopFinishForm extends BasePage {

  private final SelenideElement container = $(".shop .container");
  private final Buttons buttons = new Buttons(this.container);

  public ShopFinishForm(App app) {
    super(app);
  }

  @Step
  public ShopFinishForm assertOpened() {
    this.container.find("[translate='SHOP_FINISHED_TITLE']")
      .shouldHave(Condition.exactText("Vielen Dank für Ihre Bestellung"));
    return this;
  }

  @Step
  public void goHome() {
    buttons.click("Zurück zur Startseite");
  }
}
