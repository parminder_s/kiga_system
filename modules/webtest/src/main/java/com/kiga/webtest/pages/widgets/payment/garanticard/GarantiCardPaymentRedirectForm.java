package com.kiga.webtest.pages.widgets.payment.garanticard;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;

import static com.kiga.webtest.core.angular.AngularHelpers.$;

public class GarantiCardPaymentRedirectForm extends BasePage{

  private final SelenideElement container  = $("#anaAlan");

  public GarantiCardPaymentRedirectForm(App app) {
    super(app);
  }

  public void continuePayment() {
    this.container.find("input[type='submit']").click();
  }
}
