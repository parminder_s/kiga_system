package com.kiga.webtest.core;

import com.kiga.webtest.core.angular.wait.AngularWait;
import com.kiga.webtest.core.checks.Assertions;
import com.kiga.webtest.core.checks.CustomConditions;
import com.kiga.webtest.core.locators.Locators;
import com.kiga.webtest.core.properties.PropertiesHelper;
import com.codeborne.selenide.*;

import java.util.Random;

public class Core {

  private static Core core = new Core();

  private AngularWait angular = new AngularWait();

  public static Core core() {
    return core;
  }

  public Locators by() {
    return new Locators();
  }

  public Assertions asserts() {
    return new Assertions();
  }

  public AngularWait angular() {
    return this.angular;
  }

  public void open(String url) {
    Selenide.open(url);
  }

  public PropertiesHelper properties() {
    return new PropertiesHelper();
  }

  public CustomConditions conditions() {
    return new CustomConditions();
  }

  public String unique(String name) {
    return unique(name, "");
  }

  public String unique(String name, String suffix) {
    return name + (new Random().nextInt(Integer.MAX_VALUE)) + suffix;
  }

  public String uniqueEmail() {
    return unique("email", "@k.tt4.at");
  }

}
