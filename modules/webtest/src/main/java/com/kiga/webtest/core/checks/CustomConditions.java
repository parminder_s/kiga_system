package com.kiga.webtest.core.checks;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.util.Arrays;

public class CustomConditions {

  public static Condition dt(String attrDataTestValue) {
    return Condition.attribute("data-test", attrDataTestValue);
  }

  public static Condition dtStarts(final String attrDataTestValue) {
    return attrValueStarts("data-test", attrDataTestValue);
  }

  public static Condition oneOfDt(final String... attrDataTestValues) {
    return oneOfAttrValues("data-test", attrDataTestValues);
  }

  public static Condition exactTitle(final String title) {
    return exactTitle(".title", title);
  }

  public static Condition exactTitle(final String titleSelector, final String title) {
    return new Condition("exact title for (" + titleSelector + ")") {
      @Override
      public boolean apply(WebElement element) {
        return element.findElement(
          By.cssSelector(titleSelector)
        ).getText().equals(title);
      }
    };
  }

  public static Condition hRefContains(final String href) {
    return new Condition("href contains") {
      @Override
      public boolean apply(WebElement element) {
        return element.findElement(By.cssSelector("a"))
          .getAttribute("href").contains(href);
      }
    };
  }

  public static Condition attrValueStarts(final String attrName, final String attrDataTestValue) {
    return new Condition("attrValueStarts") {
      @Override
      public boolean apply(WebElement element) {
        return getAttribute(attrName, element).startsWith(attrDataTestValue);
      }

      @Override
      public String toString() {
        return "Attribute " + attrName + " starts with " + attrDataTestValue;
      }
    };
  }

  public static Condition oneOfAttrValues(final String attrName, final String... attrDataTestValues) {
    return new Condition("oneOfAttrValues") {
      @Override
      public boolean apply(WebElement element) {
        for (String attrDataTestValue : attrDataTestValues) {
          if (getAttribute(attrName, element).equals(attrDataTestValue)) {
            return true;
          }
        }
        return false;
      }

      @Override
      public String toString() {
        return "Attribute " + attrName + " should be equal one of " + Arrays.toString(attrDataTestValues);
      }
    };
  }

  private static String getAttribute(String attrName, WebElement element) {
    String attr = element.getAttribute("data-test");
    return attr == null ? "" : attr.trim();
  }

}
