package com.kiga.webtest.pages.steps.shop.payment;

import com.kiga.webtest.datastructure.CreditCard;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePaymentPage;
import com.kiga.webtest.pages.widgets.payment.card.creditcard.CreditCardPaymentDetailsForm;
import com.kiga.webtest.pages.widgets.payment.card.CardPaymentRedirectForm;
import com.kiga.webtest.pages.widgets.payment.card.creditcard.CreditCardVerificationSimulatorForm;
import com.kiga.webtest.pages.widgets.payment.card.CardTransactionIsSuccessForm;
import ru.yandex.qatools.allure.annotations.Step;

public class ShopCreditCardPaymentSteps extends BasePaymentPage {

  private final CreditCard creditCard;

  public ShopCreditCardPaymentSteps(App app, double total, Currency currency, CreditCard creditCard) {
    super(app, total, currency);
    this.creditCard = creditCard;
  }

  @Step
  public ShopCreditCardPaymentSteps payAndApprove() {
    return pay().approvePayment();
  }

  @Step
  public ShopCreditCardPaymentSteps pay() {
    creditCardPaymentDetails()
      .assertSelectedCardType(creditCard.creditCardType())
      .assertTotal(this.total(), this.currency())
      .fill(
        creditCard.identifier(),
        creditCard.expiryMm(), creditCard.expiryYy())
      .pay();
    return this;
  }

  @Step
  public ShopCreditCardPaymentSteps approvePayment() {
    redirect()
      .assertTotal(this.total(), this.currency())
      .assertContains(creditCard.creditCardType().toString())
      .next();
    creditCardVerificationSimulator()
      .assertOpened(creditCard)
      .approveAuthentication();
    transactionIsSuccess()
      .assertTotal(this.total(), this.currency())
      .assertOpened()
      .next();
    return this;
  }

  //**********************Steps implementation************

  @Step
  private CreditCardPaymentDetailsForm creditCardPaymentDetails() {
    return new CreditCardPaymentDetailsForm(app);
  }

  @Step
  public CardPaymentRedirectForm redirect() {
    return new CardPaymentRedirectForm(app);
  }

  @Step
  private CreditCardVerificationSimulatorForm creditCardVerificationSimulator() {
    return new CreditCardVerificationSimulatorForm(app);
  }

  @Step
  private CardTransactionIsSuccessForm transactionIsSuccess() {
    return new CardTransactionIsSuccessForm(app);
  }

}
