package com.kiga.webtest.pages.steps.common;

import com.codeborne.selenide.Selenide;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.common.Breadcrumbs;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.size;

public class BreadcrumbsSteps extends BasePage {
  public BreadcrumbsSteps(App app){super(app);}
  @Step
  private Breadcrumbs breadcrumbs() {return new Breadcrumbs(app);}
  @Step
  public BreadcrumbsSteps startIdeas(){
    breadcrumbs().selectIdeas();
    return this;
  }
  @Step
  public BreadcrumbsSteps assertBreadcrumbAmount(int expectedSize){
    breadcrumbs().assertBreadcrumbAmount(expectedSize);
    return this;
  }
}
