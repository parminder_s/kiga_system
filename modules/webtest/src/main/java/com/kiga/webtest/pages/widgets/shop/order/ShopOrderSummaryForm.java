package com.kiga.webtest.pages.widgets.shop.order;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.datastructure.settings.ShopPaymentMethod;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.kiga.webtest.core.angular.AngularHelpers.$;

public class ShopOrderSummaryForm extends BasePage {

  private final SelenideElement container = $(".shop .container");
  private final Buttons buttons = new Buttons(this.container);
  private final SelenideElement customerDataContainer =
    this.container.find(".customer-data-left");
  private final SelenideElement paymentContainer =
    this.container.find("[translate='SHOP_PAYMENT']").parent();

  public ShopOrderSummaryForm(App app) {
    super(app);
  }

  @Step
  public ShopCartInSummary shopCart() {
    return new ShopCartInSummary(app, this.container);
  }

  @Step
  public ShopOrderSummaryForm assertCustomerData(CustomerData customerData) {
    this.customerDataContainer.shouldHave(
      text(customerData.email()),
      text("Tel-Nr.: " + customerData.phone()),
      text(customerData.customer().commonInformation().gender().title(language)),
      text(
        customerData.customer().commonInformation().firstName() + " " +
          customerData.customer().commonInformation().lastName()
      ),
      text(
        customerData.customer().address().street() + " " +
          customerData.customer().address().streetNumber()
      ),
      text(
        customerData.customer().address().zip() + " " +
          customerData.customer().address().city()
      ),
      text(
        language.term(
          "shop.basket.country." + customerData.country().isoCode()
        )
      )
    );
    return this;
  }

  @Step
  public ShopOrderSummaryForm assertCustomerAddress(CustomerData customerData) {
    this.customerDataContainer.shouldHave(
      text(
        customerData.customer().address().street() + " " +
          customerData.customer().address().streetNumber()
      ),
      text(
        customerData.customer().address().zip() + " " +
          customerData.customer().address().city()
      )
    );
    return this;
  }

  @Step
  public ShopOrderSummaryForm assertPaymentMethod(ShopPaymentMethod paymentMethod) {
    this.container.find(
      "[ng-if=\"ctrl.data.paymentMethod == '" +
        paymentMethod.value() +
        "'\"]"
    ).shouldBe(visible);
    return this;
  }

  @Step
  public ShopOrderSummaryForm assertOrder(ShopData shopData) {
    shopCart().assertData(shopData);
    return this;
  }

  @Step
  public void startPay() {
    buttons.click(language.term("shop.topay"));
  }
}
