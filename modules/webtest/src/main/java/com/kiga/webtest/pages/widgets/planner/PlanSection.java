package com.kiga.webtest.pages.widgets.planner;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.DropDownList;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.locators.Locators.dt;

public class PlanSection extends BasePage {

  private final SelenideElement container;
  private final SelenideElement printButton;
  private final DropDownList printOptions;

  public PlanSection(App app, SelenideElement parent) {
    super(app);
    this.container = parent.find(".plan-section");
    this.printButton = $(dt("printPlan"));
    this.printOptions = new DropDownList(
      this.printButton,
      this.printButton.parent().findAll(".dropdown-menu li"));
  }

  @Step
  public void prevPeriod() {
    this.container.find(".btn-prev").click();
  }

  @Step
  public void nextPeriod() {
    this.container.find(".btn-next").click();
  }

  @Step
  public PrintSelectionForm startPrintSelection() {
    printOptions.open().select("Print Selection");
    return new PrintSelectionForm(app);
  }

  @Step
  public SelenideElement button(String attrDataTestValue) {
    return this.container.find(dt(attrDataTestValue));
  }

}
