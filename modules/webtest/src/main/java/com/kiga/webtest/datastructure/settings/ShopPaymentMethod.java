package com.kiga.webtest.datastructure.settings;

public enum ShopPaymentMethod {
  CREDIT_CARD("MP-CC"),
  PAY_PAL("MP-PAYPAL"),
  SOFORT("SOFORT"),
  INVOICE("INVOICE");

  private final String value;

  ShopPaymentMethod(String value) {
    this.value = value;
  }

  public String value() {
    return this.value;
  }

  @Override
  public String toString() {
    return this.name();
  }
}

