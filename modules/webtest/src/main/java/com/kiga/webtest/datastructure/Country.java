package com.kiga.webtest.datastructure;

import com.kiga.webtest.datastructure.settings.Currency;

public class Country {

  private final String title;
  private final Currency currency;
  private final String isoCode;
  private final double delivery;
  private final double totalLimitForDelivery;

  public Country(
    String title,
    Currency currency,
    String isoCode,
    double delivery,
    double totalLimitForDelivery) {

    this.title = title;
    this.currency = currency;
    this.isoCode = isoCode;
    this.delivery = delivery;
    this.totalLimitForDelivery = totalLimitForDelivery;
  }

  public String title() {
    return this.title;
  }

  public Currency currency() {
    return this.currency;
  }

  public String isoCode() {
    return this.isoCode;
  }

  public double delivery() {
    return this.delivery;
  }

  public double totalLimitForDelivery() {
    return this.totalLimitForDelivery;
  }

  @Override
  public String toString() {
    return this.title;
  }
}
