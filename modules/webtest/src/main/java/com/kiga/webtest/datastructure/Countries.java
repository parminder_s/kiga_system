package com.kiga.webtest.datastructure;

import static com.kiga.webtest.datastructure.settings.Currency.*;

public class Countries {

  public final static Country AUSTRIA =
    new Country("Österreich", EUR, "at", 4, 150);
  public final static Country BELGIUM =
    new Country("Belgium", EUR, "be", 6, 150);
  public final static Country GERMANY =
    new Country("Deutschland", EUR, "de", 6, 150);
  public final static Country ITALY =
    new Country("Italy", EUR, "it", 6, 150);
  public final static Country LIECHTENSTEIN =
    new Country("Liechtenstein", EUR, "li", 5, 150);
  public final static Country LUXEMBOURG =
    new Country("Luxembourg", EUR, "lu", 6, 150);
  public final static Country SWITZERLAND =
    new Country("Schweiz", CHF, "ch", 9, 200);

  public final static Country TURKEY =
    new Country("Turkey", TL, "tr", 0, 0);

}
