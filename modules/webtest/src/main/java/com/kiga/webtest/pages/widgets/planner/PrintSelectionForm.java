package com.kiga.webtest.pages.widgets.planner;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.config.KigaConfiguration;
import com.kiga.webtest.config.Url;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.kiga.webtest.core.angular.AngularHelpers.$;


public class PrintSelectionForm extends BasePage {

  private final SelenideElement container = $("#main>.container");
  private final ElementsCollection items = this.container.findAll(".programs .row .event");
  private final Url url;


  public PrintSelectionForm(App app) {
    super(app);
    url = new Url(app.language);
  }

  @Step
  public List<String> getIdeaIds() {
    assertLoaded();
    List<String> returner = new ArrayList<>();
    for (SelenideElement item : items) {
      String srcValue = item.find(".img-holder img").getAttribute("src");
      //from src="https://kiga.s3.amazonaws.com/251-500/323/KindergartenPutzmaterial-Druck2097X20650ebfbdf67d0a_h14250ebfc0d99b1c.jpg"
      //get ID of idea = 323
      srcValue = srcValue.replaceFirst(url.srcUrl(), "");
      srcValue = srcValue.substring(srcValue.indexOf("/") + 1);
      srcValue = srcValue.substring(0, srcValue.indexOf("/"));
      returner.add(srcValue);
    }
    return returner;
  }

  @Step
  public PrintSelectionForm assertLoaded() {
    $(".loaderDialog").shouldNotBe(visible);
    return this;
  }
}
