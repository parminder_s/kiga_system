package com.kiga.webtest.pages.widgets.payment.card.sofort;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.exactText;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.helpers.LocaleHelpers.decimalFormat;

public class SofortPaymentStep1Form extends BasePage {

  private final SelenideElement container = $("[name='pageform']");
  private final Buttons buttons =
    new Buttons(container, "#buttons>[type='submit']");

  public SofortPaymentStep1Form(App app) {
    super(app);
  }

  @Step
  public SofortPaymentStep1Form assertTotal(double sum, Currency currency) {
    this.container.find(".priceResponsive").shouldHave(exactText(
      language.term("creditcard.total") + ": " +
        currency.name() + " " +
        decimalFormat(language, sum, "#.00"))
    );
    return this;
  }

  @Step
  public SofortPaymentStep1Form assertOpened() {
    this.container.find("#ptype_img").shouldHave(attribute("alt", "SOFORT"));
    return this;
  }

  @Step
  public void pay() {
    this.buttons.clickBy(attribute("value", language.term("creditcard.pay")));
  }

}
