package com.kiga.webtest.datastructure;

import com.kiga.webtest.datastructure.settings.Duration;
import com.kiga.webtest.datastructure.settings.Job;
import com.kiga.webtest.datastructure.settings.Permission;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import ru.yandex.qatools.allure.annotations.Step;

public class CustomerData {

  private final Customer customer;
  private final String email;
  private final KiGaDate birthDate;
  private final Country country;
  private final SubscriptionType subscriptionType;
  private final Duration duration;
  private final double price;
  private final String title;
  private final String phone;
  private final String passportNumber;
  private final boolean isStudent;
  private final Job job;
  private int additionalLicencesCount;
  private Permission permission;
  private boolean expired;

  private CustomerData(CustomerDataBuilder customerDataBuilder) {
    this.customer = customerDataBuilder.customer;
    this.email = customerDataBuilder.email;
    this.birthDate = customerDataBuilder.birthDate;
    this.country = customerDataBuilder.country;
    this.subscriptionType = customerDataBuilder.subscriptionType;
    this.duration = customerDataBuilder.duration;
    this.price = customerDataBuilder.price;
    this.title = customerDataBuilder.title;
    this.phone = customerDataBuilder.phone;
    this.passportNumber = customerDataBuilder.passportNumber;
    this.isStudent = customerDataBuilder.isStudent;
    this.job = customerDataBuilder.job;
    this.additionalLicencesCount = customerDataBuilder.additionalLicencesCount;
    this.permission = customerDataBuilder.permission;
    this.expired = customerDataBuilder.expired;
  }

  @Override
  public String toString() {
    return this.title;
  }

  public Customer customer() {
    return this.customer;
  }

  public String email() {
    return this.email;
  }

  public KiGaDate birthDate() {
    return this.birthDate;
  }

  public Country country() {
    return this.country;
  }

  public SubscriptionType subscriptionType() {
    return this.subscriptionType;
  }

  public Duration duration() {
    return this.duration;
  }

  public double price() {
    return this.price;
  }

  public String phone() {
    return this.phone;
  }

  public String passportNumber() {
    return this.passportNumber;
  }

  public boolean isStudent() {
    return this.isStudent;
  }

  public Job job() {
    return this.job;
  }

  public int additionalLicencesCount() {
    return this.additionalLicencesCount;
  }

  public Permission permission() {
    return permission;
  }

  public boolean expired() {
    return expired;
  }

  public static class CustomerDataBuilder {
    private Customer customer;
    private String email;
    private KiGaDate birthDate;
    private Country country;
    private SubscriptionType subscriptionType;
    private Duration duration;
    private double price;
    private String title;
    private String phone;
    private String passportNumber;
    private boolean isStudent;
    private Job job;
    private int additionalLicencesCount;
    private Permission permission;
    private boolean expired;

    public CustomerDataBuilder(String title) {
      this.title = title;
    }

    @Step
    public CustomerDataBuilder forCustomer(Customer customer) {
      this.customer = customer;
      return this;
    }

    @Step
    public CustomerDataBuilder withEmail(String email) {
      this.email = email;
      return this;
    }

    @Step
    public CustomerDataBuilder withBirthDate(KiGaDate birthDate) {
      this.birthDate = birthDate;
      return this;
    }

    @Step
    public CustomerDataBuilder forSubscription(SubscriptionType subscriptionType) {
      this.subscriptionType = subscriptionType;
      return this;
    }

    @Step
    public CustomerDataBuilder inCountry(Country country) {
      this.country = country;
      return this;
    }

    @Step
    public CustomerDataBuilder forDuration(Duration duration) {
      this.duration = duration;
      return this;
    }

    @Step
    public CustomerDataBuilder withPrice(double price) {
      this.price = price;
      return this;
    }

    @Step
    public CustomerDataBuilder withPhone(String phone) {
      this.phone = phone;
      return this;
    }

    @Step
    public CustomerDataBuilder withPassportNumber(String passportNumber) {
      this.passportNumber = passportNumber;
      return this;
    }

    @Step
    public CustomerDataBuilder asAStudent() {
      this.isStudent = true;
      return this;
    }

    @Step
    public CustomerDataBuilder withJob(Job job) {
      this.job = job;
      return this;
    }

    @Step
    public CustomerDataBuilder withAdditionalLicences(int additionalLicencesCount) {
      this.additionalLicencesCount = additionalLicencesCount;
      return this;
    }

    @Step
    public CustomerDataBuilder withPermission(Permission permission) {
      this.permission = permission;
      return this;
    }

    @Step
    public CustomerDataBuilder asExpired() {
      this.expired = true;
      return this;
    }

    public CustomerData build() {
      return new CustomerData(this);
    }
  }
}
