package com.kiga.webtest.pages.widgets.payment.elv;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.ELV;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.visible;

public class ElvPaymentDetailsForm extends BasePage {

  private final SelenideElement container = $("[name='pageform']");

  private final SelenideElement iban = container.find("[name='iban']");
  private final SelenideElement bic = container.find("[name='bic']");

  public ElvPaymentDetailsForm(App app) {
    super(app);
  }

  @Step
  public ElvPaymentDetailsForm fill(String IbanValue, String BicValue) {
    this.iban.setValue(IbanValue);
    this.bic.setValue(BicValue);
    return this;
  }

  @Step
  public ElvPaymentDetailsForm fill(ELV elv) {
    return fill(elv.iban(), elv.bic());
  }

  @Step
  public ElvPaymentDetailsForm agreeSepaDirectDebitTermsAndAuthorizeTheMandate() {
    $("[name='conditions']").click();
    return this;
  }

  @Step
  public void pay() {
    new Buttons(
      this.container,
      "#buttons>.btn"
    ).clickBy(attribute("name", "pay"));
  }

  @Step
  public void assertOpened() {
    this.iban.shouldBe(visible);
  }
}
