package com.kiga.webtest.pages.requests;

import com.kiga.webtest.config.Url;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RequestManager extends BasePage {

  private final Url url;

  public RequestManager(App app) {
    super(app);
    this.url = new Url(app.language);
  }

  @Step
  public HttpResponse printOverview(String planId, String sessionId)
    throws IOException, URISyntaxException {
    return get(
      this.url.myWeekPlanPrintOverviewUrl(planId),
      sessionId);
  }

  @Step
  public HttpResponse printSelection(List<String> ideaIds, String sessionId)
    throws IOException, URISyntaxException {
    return get(
      this.url.myWeekPlanPrintSelectionUrl(ideaIds),
      sessionId);
  }


  @Step
  public void assertStatusCode(HttpResponse response, int statusCode) {
    assertEquals(response.getStatusLine().getStatusCode(), statusCode);
  }

  @Step
  private HttpResponse get(String uri, String sessionID)
    throws IOException, URISyntaxException {
    CookieStore cookieStore = new BasicCookieStore();
    BasicClientCookie cookie = new BasicClientCookie("PHPSESSID", sessionID);
    String domain = new URI(uri).getHost();
    cookie.setDomain(domain);
    cookie.setPath("/");
    cookieStore.addCookie(cookie);

    return Executor.newInstance()
      .use(cookieStore).execute(Request.Get(uri)).returnResponse();
  }

}
