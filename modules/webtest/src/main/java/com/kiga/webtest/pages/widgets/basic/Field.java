package com.kiga.webtest.pages.widgets.basic;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.impl.Events.events;
import static java.lang.String.format;

public class Field {

  private SelenideElement container;

  public Field(SelenideElement container) {
    this.container = container;
  }

  @Step
  public Field followingSiblingByTag(String tag, int index) {
    return new Field(
      this.container.find(
        By.xpath(
          format("following-sibling::%s[%s]", tag, (index + 1))
        )
      )
    );
  }

  @Step
  public Field followingSiblingByClass(String cssClass, int index) {
    return new Field(
      this.container.find(
        By.xpath(
          format("following-sibling::*[contains(concat(' ', normalize-space(@class), ' '), ' %s ')][%s]", cssClass, (index + 1))
        )
      )
    );
  }

  @Step
  public Field parent() {
    return new Field(container.parent());
  }

  @Step
  public Field validation() {
    if (container.attr("async-validation") != null) {
      return parent().followingSiblingByClass("validation", 0);
    } else {
      return followingSiblingByClass("validation", 0);
    }
  }

  @Step
  public Field validator() {
    return this;
  }

  @Step
  public Field clear() {
    this.container.clear();
    return this;
  }

  @Step
  public Field setValue(String value) {
    this.container.setValue(value);
    return this;
  }

  @Step
  public Field pressEnter() {
    this.container.pressEnter();
    return this;
  }

  @Step
  public Field blur() {
    events.fireEvent(this.container, "blur");
    return this;
  }

  @Step
  public Field should(Condition condition) {
    this.container.should(condition);
    return this;
  }

  @Override
  public String toString() {
    return this.container.toString();
  }

}
