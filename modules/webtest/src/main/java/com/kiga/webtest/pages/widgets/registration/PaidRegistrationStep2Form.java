package com.kiga.webtest.pages.widgets.registration;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.*;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.helpers.basic.DataForGroup;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.*;
import com.kiga.webtest.datastructure.settings.Gender;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.locators.Locators.dt;
import static com.kiga.webtest.core.locators.Locators.dtReg;

public class PaidRegistrationStep2Form extends BasePage {

  private final SelenideElement container = $(".registration>.container");

  private final DateGroup birthDayGroup =
    new DateGroup(this.container.find(By.name("birthday")));


  public PaidRegistrationStep2Form(App app) {
    super(app);
  }

  @Step
  public PaidRegistrationStep2Form fillCommonInformation(
    Gender gender, DataForGroup dataForGroup) {

    new RadioGroup(this.container).
      clickOn(gender.toString());
    new TextFieldGroup(this.container,
      dataForGroup.attrDataTestValues()
    ).fill(dataForGroup.texts());
    return this;
  }

  @Step
  public PaidRegistrationStep2Form fillCommonInformationReg(
    Gender gender, DataForGroup dataForGroup) {

    new RadioGroup(this.container).
      clickOnGender(gender.toString());
    new TextFieldGroupReg(this.container,
      dataForGroup.attrDataTestValues()
    ).fill(dataForGroup.texts());
    return this;
  }

  @Step
  public PaidRegistrationStep2Form assertCommonInformation(
    Gender gender, DataForGroup dataForGroup) {

    new RadioGroup(this.container).
      shouldBeSelected(gender.toString());
    new TextFieldGroup(this.container,
      dataForGroup.attrDataTestValues()
    ).shouldHaveValues(dataForGroup.texts());
    return this;
  }
  @Step
  public PaidRegistrationStep2Form assertCommonInformationReg(
    Gender gender, DataForGroup dataForGroup) {

    new RadioGroup(this.container).
      shouldBeSelected(gender.toString());
    new TextFieldGroupReg(this.container,
      dataForGroup.attrDataTestValues()
    ).shouldHaveValues(dataForGroup.texts());
    return this;
  }
  @Step
  public PaidRegistrationStep2Form fillAddressInformation(
    DataForGroup addressData) {

    new TextFieldGroup(this.container,
      addressData.attrDataTestValues()
    ).fill(addressData.texts());
    return this;
  }

  @Step
  public PaidRegistrationStep2Form fillAddressInformationReg(
    DataForGroup addressData) {

    new TextFieldGroupReg(this.container,
      addressData.attrDataTestValues()
    ).fill(addressData.texts());
    return this;
  }

  @Step
  public PaidRegistrationStep2Form assertAddressInformation(
    DataForGroup addressData) {

    new TextFieldGroupReg(this.container,
      addressData.attrDataTestValues()
    ).shouldHaveValues(addressData.texts());
    return this;
  }

  @Step
  public PaidRegistrationStep2Form fillBirthDate(
    int day, int month, int year) {

    birthDayGroup.set(day, month, year);
    return this;
  }

  @Step
  public PaidRegistrationStep2Form fillBirthDate(KiGaDate date) {
    return fillBirthDate(date.day(), date.month(), date.year());
  }

  @Step
  public PaidRegistrationStep2Form assertBirthDate(KiGaDate date) {
    birthDayGroup.assertDate(language, date.day(), date.month(), date.year());
    return this;
  }

  @Step
  public PaidRegistrationStep3Form next() {
    new Buttons(this.container).click(language.term("next"));
    return new PaidRegistrationStep3Form(app);
  }

  @Step
  public Field field(String attrDataTestValue) {
    return field(dt(attrDataTestValue));
  }

  @Step
  public Field fieldReg(String attrDataTestValue) {
    return field(dtReg(attrDataTestValue));
  }

  @Step
  public Field field(By locator) {
    return new Field(this.container.find(locator));
  }

}
