package com.kiga.webtest.pages.widgets.payment.card.sofort;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import com.kiga.webtest.pages.widgets.payment.card.CardTransactionIsSuccessForm;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;

public class SofortPaymentStep5Form extends BasePage{

  private final SelenideElement container = $("#ProvideTanPage .main");
  private final Buttons buttons =
    new Buttons(this.container, "button");

  public SofortPaymentStep5Form(App app) {
    super(app);
  }

  @Step
  public SofortPaymentStep5Form setTan(String tan) {
    //todo replace with selector on <input type="text", now only working in Germany,
    // but not in Austria.
    SelenideElement elem = this.container.find("#BackendFormTan");
    elem.parent().click();
    elem.setValue(tan);
    return this;
  }

  @Step
  public CardTransactionIsSuccessForm next() {
    buttons.click(language.term("next")); //todo from language.term!
    return new CardTransactionIsSuccessForm(app);
  }

}
