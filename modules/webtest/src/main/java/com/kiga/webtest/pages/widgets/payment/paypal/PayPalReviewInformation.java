package com.kiga.webtest.pages.widgets.payment.paypal;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.PayPalAccount;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;

public class PayPalReviewInformation extends BasePage {

  private final SelenideElement parent;
  private final SelenideElement container;

  public PayPalReviewInformation(App app, SelenideElement parent) {
    super(app);
    this.parent = parent;
    this.container = this.parent.find("#sliders");
  }

  @Step
  public PayPalReviewInformation assertAccount(PayPalAccount payPalAccount) {
    this.container.find("#contactInfo .inset").shouldHave(exactText(payPalAccount.email()));
    return this;
  }

  @Step
  public void agreeAndContinue() {
    this.container.find("#continue").click();
  }

}
