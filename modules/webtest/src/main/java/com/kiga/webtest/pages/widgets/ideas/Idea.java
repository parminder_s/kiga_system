package com.kiga.webtest.pages.widgets.ideas;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.IdeaData;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.text;
import static com.kiga.webtest.core.angular.AngularHelpers.$;

public class Idea extends BasePage {

  private final SelenideElement container = $(".kiga-container");
  private final SelenideElement idea = this.container.find(".ideas .external-container");
  private final Buttons ideaButtons = new Buttons(this.idea, ".mfb-menu [mfb-button]");
  private final SelenideElement closeButton = this.idea.find(".mfb-component__button--main");
  private final SelenideElement forum = this.container.find(".ideas .forum-container");

  public Idea(App app) {
    super(app);
  }

  @Step
  public void assertOpened(IdeaData ideaData) {
    idea.find("h1").shouldHave(exactText(ideaData.ideaTitle()));
  }
  @Step
  public void assertOpened(String title) {
    idea.find("h1").shouldHave(text(title));
  }
}
