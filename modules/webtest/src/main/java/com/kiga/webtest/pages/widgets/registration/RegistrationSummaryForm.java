package com.kiga.webtest.pages.widgets.registration;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.CustomerAddress;
import com.kiga.webtest.datastructure.CustomerCommonInformation;
import com.kiga.webtest.datastructure.KiGaDate;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.datastructure.settings.Duration;
import com.kiga.webtest.datastructure.settings.Gender;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.helpers.LocaleHelpers.dateFormat;
import static com.kiga.webtest.core.helpers.LocaleHelpers.decimalFormat;
import static com.kiga.webtest.core.locators.Locators.dt;
import static com.codeborne.selenide.Condition.text;

public class RegistrationSummaryForm extends BasePage {

  private final SelenideElement container = $(".registration>.container");

  private final Buttons buttons = new Buttons(container);
  private final ElementsCollection blocks =
    container.find(dt("summary")).findAll("[class^='col-xlg']");

  public RegistrationSummaryForm(App app) {
    super(app);
  }

  private RegistrationSummaryBlock block(String termTitle) {
    return new RegistrationSummaryBlock(app, blocks, termTitle);
  }

  @Step
  public RegistrationSummaryBlock chosenSubscriptionBlock() {
    return block("summary.chosensubscription");
  }

  @Step
  public RegistrationSummaryBlock subscriptionPriceBlock() {
    return block("summary.subscriptionprice");
  }

  @Step
  public RegistrationSummaryBlock contactBlock() {
    return block("summary.contact");
  }

  @Step
  public RegistrationSummaryBlock paymentMethodBlock() {
    return block("summary.paymentmethod");
  }

  @Step
  public RegistrationSummaryForm assertChosenSubscription(
    SubscriptionType subscriptionType) {

    this.chosenSubscriptionBlock()
      .shouldHave(text(subscriptionType.title(language)));
    return this;
  }

  @Step
  public RegistrationSummaryForm assertSubscriptionPriceAndDuration(
    Duration duration, double sum, Currency currency) {

    this.subscriptionPriceBlock().shouldHave(
      text(duration.title(language)),
      text(decimalFormat(language, sum, "#.00") + " " + currency)
    );
    return this;
  }

  @Step
  public RegistrationSummaryForm assertCommonInformation(
    Gender gender, String firstName, String lastName, String email) {

    this.contactBlock().shouldHave(
      text(gender.title(language)),
      text(firstName + " " + lastName),
      text(email)
    );
    return this;
  }

  @Step
  public RegistrationSummaryForm assertPassportNumber(String passportNumber) {
    this.contactBlock().shouldHave(
      text(passportNumber)
    );
    return this;
  }

  @Step
  public RegistrationSummaryForm assertCommonInformation(
    CustomerCommonInformation commonInformation, String email) {

    return assertCommonInformation(
      commonInformation.gender(),
      commonInformation.firstName(), commonInformation.lastName(),
      email
    );

  }

  @Step
  public RegistrationSummaryForm assertAddressInformation(
    CustomerAddress address) {

    this.contactBlock().shouldHave(
      text(address.street() + ", " + address.streetNumber() +
        (address.floorNumber().isEmpty() ? "" : (", " + address.floorNumber())) +
        (address.doorNumber().isEmpty() ? "" : (", " + address.doorNumber()))
      ),
      text(address.zip() + " " + address.city()),
      text(address.institutionName1()),
      text(address.institutionName2()));
    return this;

  }

  @Step
  public RegistrationSummaryForm assertPaymentMethodBlock(
    String paymentMethodText) {

    this.paymentMethodBlock().shouldHave(text(paymentMethodText));
    return this;
  }

  @Step
  public RegistrationSummaryForm assertBirthDate(KiGaDate birthDate) {
    this.contactBlock().shouldHave(text(dateFormat(language, birthDate)));
    return this;
  }

  @Step
  public void confirm() {
    this.buttons.click(language.term("confirm"));
  }

}
