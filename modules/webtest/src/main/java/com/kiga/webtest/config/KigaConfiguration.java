package com.kiga.webtest.config;

import com.kiga.webtest.core.properties.PropertiesHelper;
import com.codeborne.selenide.Configuration;

import java.util.Properties;

import static com.kiga.webtest.core.properties.PropertiesHelper.configureRemoteAndCapabilities;

public class KigaConfiguration {

  public static String appBaseUrl;

  /*
  * User email & password - existing account for testing
  */
  public static String userEmail;
  public static String userPassword;

  public static int maxParallelForks;

  public static void init(Properties properties) {

    appBaseUrl = properties.getProperty("app.url");

    userEmail = properties.getProperty("data.user.email");
    userPassword = properties.getProperty("data.user.password");

    maxParallelForks = Integer.valueOf(System.getProperty("maxParallelForks", "1"));

    Configuration.timeout = Long.valueOf(properties.getProperty("test.timeout"));
    Configuration.collectionsTimeout = Long.valueOf(properties.getProperty("test.timeout"));
    Configuration.pollingInterval = Long.valueOf(properties.getProperty("test.pollingInterval"));

    Configuration.browser = properties.getProperty("test.browser");
    Configuration.fastSetValue = Boolean.valueOf(properties.getProperty("test.fastSetValue"));

    configureRemoteAndCapabilities(properties);
  }

  public static void init() {
    KigaConfiguration.init(PropertiesHelper.readEnvironmentProperties());
    PropertiesHelper.readLocalizations();
  }

}
