package com.kiga.webtest.pages.widgets.planner;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.DropDownList;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.locators.Locators.dt;

public class WeekPlan extends BasePage {

  private final SelenideElement container = $(".planner .kiga-plan");
  private final PlanSection planSection = new PlanSection(app, this.container);

  public WeekPlan(App app) {
    super(app);
  }

  @Step
  public WeekPlan prevPeriod() {
    this.planSection.prevPeriod();
    return this;
  }

  @Step
  public WeekPlan nextPeriod() {
    this.planSection.nextPeriod();
    return this;
  }

  @Step
  public PlanSection planSection() {
    return this.planSection;
  }

  @Step
  public WeekKigaPlanDetails kigaPlan() {
    return new WeekKigaPlanDetails(app, this);
  }

  @Step
  public WeekMyPlanDetails myPlan() {
    return new WeekMyPlanDetails(app, this, this.container);
  }

  @Step
  public PrintSelectionForm startPrintSelection() {
    return this.planSection.startPrintSelection();
  }
}
