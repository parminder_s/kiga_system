package com.kiga.webtest.pages.widgets.shop;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.ShopProductData;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThanOrEqual;
import static com.codeborne.selenide.Condition.visible;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.checks.CustomConditions.exactTitle;
import static com.kiga.webtest.core.helpers.AdditionalSelenideAPI.scrollWithOffsetOn;

public class Products extends BasePage {

  private final SelenideElement container = $(".body.shop");
  private final ElementsCollection products = this.container.findAll("[item='product']>div");

  public Products(App app) {
    super(app);
  }

  @Step
  public Product selectProduct(int index) {
    product(index).click();
    return new Product(app);
  }

  @Step
  public Product selectProduct(String title) {
    //without scroll error for firefox in Xvfb run - header shows over element
    scrollWithOffsetOn(product(title), 0, -150);

    product(title).click();
    return new Product(app);
  }

  @Step
  public ShopProductData getProductData(int index) {
    this.products.shouldHave(sizeGreaterThanOrEqual(index + 1));
    String title = title(index).getText();
    String price = price(index).getText();
    String productTitle = productTitle(index).attr("content");
    Currency currency = Currency.valueOf(price.split(" ")[1]);
    double priceSum = Double.parseDouble(price.split(" ")[0].replace(",", "."));
    return new ShopProductData(title, priceSum, currency, productTitle);
  }

  private SelenideElement product(int index) {
    return this.products.get(index);
  }

  private SelenideElement product(String title) {
    return this.products.find(exactTitle(title));
  }

  private SelenideElement title(int index) {
    return product(index).findAll(".title").find(visible);
  }

  private SelenideElement productTitle(int index) {
    return product(index).findAll("meta[name='productTitle'").first();
  }

  private SelenideElement price(int index) {
    return product(index).find(".price");
  }

}
