package com.kiga.webtest.pages.widgets.registration;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;

public class RegistrationSummaryBlock extends BasePage {

  private final SelenideElement container;

  private final ElementsCollection buttons;

  public RegistrationSummaryBlock(App app, SelenideElement container) {
    super(app);
    this.container = container;
    this.buttons = container.findAll(".button");
  }

  public RegistrationSummaryBlock(App app, ElementsCollection blocks, String termTitle) {
    this(
      app,
      blocks.find(text(app.language.term(termTitle)))
    );
  }

  @Step
  public RegistrationSummaryBlock edit() {
    buttons.find(visible).click();
    return this;
  }

  @Step
  public RegistrationSummaryBlock shouldHave(Condition... conditions) {
    this.container.should(conditions);
    return this;
  }

}
