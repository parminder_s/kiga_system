package com.kiga.webtest.pages.endpoints.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorCodeResponse {
  private String errorCode;
  private boolean status;
  private String entryID;

  public String getErrorCode() {
    return errorCode;
  }

  public boolean isStatus() {
    return status;
  }

  public String getEntryID() {
    return entryID;
  }
}
