package com.kiga.webtest.pages.widgets.shop.basket;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.kiga.webtest.core.helpers.LocaleHelpers.decimalFormat;

public class BasketTableSumAndCurrencyCell extends BasePage {

  private final SelenideElement container;

  private final SelenideElement currency;
  private final SelenideElement sum;

  public BasketTableSumAndCurrencyCell(App app, SelenideElement container) {
    super(app);
    this.container = container;
    this.currency = this.container.find(".currency-left");
    this.sum = this.container.find(".currency-right");
  }

  @Step
  public void shouldEqual(double sum, Currency currency) {
    this.currency.shouldHave(exactText(currency.toString()));
    this.sum.shouldHave(exactText(decimalFormat(language, sum, "0.00")));
  }
}
