package com.kiga.webtest.pages.widgets.payment.card.creditcard;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.CreditCard;
import com.kiga.webtest.datastructure.settings.CreditCardType;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import com.kiga.webtest.pages.widgets.basic.DropDownList;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.kiga.webtest.core.helpers.LocaleHelpers.decimalFormat;

public class CreditCardPaymentDetailsForm extends BasePage {

  private final SelenideElement container = $("[name='pageform']");

  private final SelenideElement cardTypeElement = container.find("#cardtype");
  private final SelenideElement cardNumberElement = container.find("#cardnumber");

  private final DropDownList creditCardTypeList =
    new DropDownList(cardTypeElement, "option");
  private final DropDownList expiryMontList =
    new DropDownList(container.find("#expiry-month"), "option");
  private final DropDownList expiryYearList =
    new DropDownList(container.find("#expiry-year"), "option");

  private final Buttons buttons = new Buttons(container, "#buttons>.btn");

  public CreditCardPaymentDetailsForm(App app) {
    super(app);
  }

  @Step
  public CreditCardPaymentDetailsForm fill(CreditCardType creditCardType, String identifier, int expiryMm, int expiryYy) {
    this.creditCardTypeList.open().select(creditCardType.toString());
    return fill(identifier, expiryMm, expiryYy);
  }

  @Step
  public CreditCardPaymentDetailsForm fill(String identifier, int expiryMm, int expiryYy) {
    this.cardNumberElement.setValue(identifier);
    this.expiryMontList.open().select(String.format("%02d", expiryMm));
    this.expiryYearList.open().select(String.valueOf(expiryYy));
    return this;
  }

  @Step
  public CreditCardPaymentDetailsForm fill(CreditCard creditCard) {
    return fill(
      creditCard.creditCardType(),
      creditCard.identifier(),
      creditCard.expiryMm(),
      creditCard.expiryYy()
    );
  }

  @Step
  public CreditCardPaymentDetailsForm assertSelectedCardType(CreditCardType creditCardType) {
    this.container.find("h4").shouldHave(exactText(creditCardType.toString()));
    return this;
  }

  @Step
  public CreditCardPaymentDetailsForm assertTotal(double sum, Currency currency) {
    this.container.find(".priceResponsive").shouldHave(exactText(
      language.term("creditcard.total") + ": " +
        currency.name() + " " +
        decimalFormat(language, sum, "#.00"))
    );
    return this;
  }

  @Step
  public void pay() {
    this.buttons.clickBy(attribute("name", "pay"));
  }

  @Step
  public CreditCardPaymentDetailsForm assertOpened() {
    this.cardTypeElement.shouldBe(visible);
    return this;
  }

}
