package com.kiga.webtest.pages.widgets.ideas;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.kiga.webtest.core.checks.CustomConditions.exactTitle;

import com.codeborne.selenide.*;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class IdeasCategoryOverview extends BasePage {

  public static final String headerSelector = ".type-title a";

  private final SelenideElement container;
  private final ElementsCollection ideas;
  private final String ideaTitleSelector = ".desc";

  public IdeasCategoryOverview(App app, SelenideElement container, String ideasSelector) {
    super(app);
    this.container = container;
    this.ideas = this.container.findAll(ideasSelector);
  }

  @Step
  public IdeaOverview selectIdea(int index) {
    return selectIdea(ideas.get(index));
  }

  @Step
  public IdeaOverview selectIdea(String title) {
    return selectIdea(idea(title));
  }

  @Step
  public String addToFavourites(int index) {
    return addToFavourites(ideas.get(index));
  }

  @Step
  public String removeFromFavourites(int index) {
    return removeFromFavourites(ideas.get(index));
  }

  @Step
  public String removeFromFavourites(String ideaTitle) {
    return removeFromFavourites(idea(ideaTitle));
  }

  @Step
  public void assertIsNotFavourite(String ideaTitle) {
    assertIsNotFavourite(idea(ideaTitle));
  }

  @Step
  public void assertIsFavourite(String ideaTitle) {
    assertIsFavourite(idea(ideaTitle));
  }

  @Step
  private SelenideElement idea(String ideaTitle) {
    return ideas.find(exactTitle(ideaTitleSelector, ideaTitle));
  }

  @Step
  private IdeaOverview selectIdea(SelenideElement idea) {
    idea.click();
    return new IdeaOverview(app);
  }

  @Step
  public void clickTitle() {
    container.find(headerSelector).click();
  }

  @Step
  public void scrollAndOpen() {
    SelenideElement moreIdeasBtn = container.find(".has-subcategories");
    SelenideElement chevronRight = container.find(".fa-chevron-right");
    while (true) {
      if (chevronRight.is(visible)) {
        chevronRight.click();
      }
      if (moreIdeasBtn.is(visible)) {
        moreIdeasBtn.click();
        break;
      }
    }
  }

  @Step
  public void navigateToLastLevel() {
    SelenideElement headerTitle = $(headerSelector);
    for (int i = 0; i < 5; i++) {
      if (headerTitle.is(visible)) {
        headerTitle.click();
      } else break;
    }
  }

  @Step
  private String addToFavourites(SelenideElement idea) {
    assertIsNotFavourite(idea);
    String returner = getIdeaTitle(idea);
    favourite(idea).find("i").shouldBe(enabled).click();
    return returner;
  }

  @Step
  private String removeFromFavourites(SelenideElement idea) {
    assertIsFavourite(idea);
    String returner = getIdeaTitle(idea);
    favourite(idea).shouldBe(visible).click();
    return returner;
  }

  @Step
  private String getIdeaTitle(SelenideElement idea) {
    return idea.find(ideaTitleSelector).shouldBe(visible).getText();
  }

  @Step
  private SelenideElement favourite(SelenideElement idea) {
    return idea.find(".info-heart");
  }

  @Step
  private void assertIsNotFavourite(SelenideElement idea) {
    favourite(idea).find(".fa-heart-o").should(exist);
  }

  @Step
  private void assertIsFavourite(SelenideElement idea) {
    favourite(idea).find(".fa-heart").should(exist);
  }

  @Step
  public String getTitle() {
    return this.container.find(headerSelector).shouldBe(visible).getText();
  }

  @Step
  public void assertAgeRange(String title, Condition condition) {
    $(byText(title)).shouldBe(condition);
  }
}
