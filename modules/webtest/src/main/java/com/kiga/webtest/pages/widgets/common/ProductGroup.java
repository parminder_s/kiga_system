package com.kiga.webtest.pages.widgets.common;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.text;

public class ProductGroup extends BasePage {

  private final SelenideElement parent;
  private final SelenideElement container;

  public ProductGroup(App app, SelenideElement parent, String title) {
    super(app);
    this.parent = parent;
    this.container = parent.findAll(".product-group").find(text(title));
  }

  @Step
  public void subscribe() {
    this.container.find(".order-btn").click();
  }

}
