package com.kiga.webtest.pages.steps.shop.payment;

import com.kiga.webtest.datastructure.Country;
import com.kiga.webtest.datastructure.SofortCard;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePaymentPage;
import com.kiga.webtest.pages.widgets.payment.card.CardPaymentRedirectForm;
import com.kiga.webtest.pages.widgets.payment.card.CardTransactionIsSuccessForm;
import com.kiga.webtest.pages.widgets.payment.card.sofort.*;
import ru.yandex.qatools.allure.annotations.Step;

public class ShopSofortCardPaymentSteps extends BasePaymentPage {

  private final SofortCard sofortCard;

  private final Country country;

  public ShopSofortCardPaymentSteps(
    App app,
    double total,
    Country country,
    SofortCard sofortCard) {

    super(app, total, country.currency());
    this.sofortCard = sofortCard;
    this.country = country;
  }

  @Step
  public ShopSofortCardPaymentSteps payAndApprove() {
    setCountryBicNumberAndPin();
    selectAccountAndTan();
    assertSuccessAndApprove();
    return this;
  }

  @Step
  public ShopSofortCardPaymentSteps confirmSumAndPaymentMethod() {
    step1()
      .assertOpened()
      .assertTotal(this.total(), this.currency())
      .pay();
    redirect()
      .assertTotal(this.total(), this.currency())
      .assertContains("SOFORT")
      .next();
    return this;
  }

  @Step
  public SofortPaymentStep4Form setCountryBicNumberAndPin() {
    step2()
      .assertOpened()
      .assertTotal(this.total(), this.currency())
      .setCountry(country)
      .setBic(sofortCard.bic());
    return step3()
      .setNumber(sofortCard.number())
      .setPin(sofortCard.pin())
      .next();
  }

  @Step
  public CardTransactionIsSuccessForm selectAccountAndTan() {
    step4()
      .selectAccount(sofortCard.accountIndex())
      .next();
    return step5()
      .setTan(sofortCard.tan())
      .next();
  }

  @Step
  public ShopSofortCardPaymentSteps assertSuccessAndApprove() {
    transactionIsSuccess()
      .assertOpened()
      .assertTotal(this.total(), this.currency())
      .next();
    return this;
  }

  //**********************Steps implementation************

  @Step
  private SofortPaymentStep1Form step1() {
    return new SofortPaymentStep1Form(app);
  }

  @Step
  private CardPaymentRedirectForm redirect() {
    return new CardPaymentRedirectForm(app);
  }

  @Step
  public SofortPaymentStep2Form step2() {
    return new SofortPaymentStep2Form(app);
  }

  @Step
  public SofortPaymentStep3Form step3() {
    return new SofortPaymentStep3Form(app);
  }

  @Step
  public SofortPaymentStep4Form step4() {
    return new SofortPaymentStep4Form(app);
  }

  @Step
  public SofortPaymentStep5Form step5() {
    return new SofortPaymentStep5Form(app);
  }

  @Step
  private CardTransactionIsSuccessForm transactionIsSuccess() {
    return new CardTransactionIsSuccessForm(app);
  }

}
