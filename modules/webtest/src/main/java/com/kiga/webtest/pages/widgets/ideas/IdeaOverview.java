package com.kiga.webtest.pages.widgets.ideas;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$$;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static org.junit.Assert.assertEquals;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

public class IdeaOverview extends BasePage {
  private String previewContainer = ".preview-container";
  private final SelenideElement container = $$(previewContainer).filter(visible).first();
  private final Buttons buttons =
      new Buttons(this.container, ".action-buttons .action-btn[data-test^='preview']");
  private final SelenideElement favouritesButton = this.buttons.button(cssClass("favourite-btn"));
  private String previewTitleSelector = ".info-title";
  private String previewImgSelector = ".info-img";
  private String ideaTitleSelector = ".desc";

  public IdeaOverview(App app) {
    super(app);
  }

  @Step
  public String getTitle() {
    return $(previewTitleSelector).shouldBe(visible).getText();
  }

  @Step
  public String getIdeaTitle() {
    return $$(previewTitleSelector).filter(visible).first().getText();
  }

  @Step
  public void viewPreview() {
    $$(previewImgSelector).filter(visible).first().click();
  }

  @Step
  public void toFavourites() {
    $("[label=\"add to Favorites\"]").click();
  }

  @Step
  public void outFavourites() {
    $("[label=\"remove from Favorites\"]").click();
  }

  @Step
  public void assertScrollToNextOverview(int ideaIndex, int scrollElementsAmount) {
    scrollTo(".next-btn", ideaIndex, scrollElementsAmount, 1);
  }

  @Step
  public void assertScrollToPrevOverview(int ideaIndex, int scrollElementsAmount) {
    scrollTo(".prev-btn", ideaIndex, scrollElementsAmount, 2);
  }

  private void scrollTo(
      String cssSelector, int ideaIndex, int scrollElementsAmount, int scrollCase) {

    String ideaTitle = null;
    String ideaTitlePreview;
    ElementsCollection collection = $$(ideaTitleSelector).filter(visible);

    for (int i = 1; i <= scrollElementsAmount; i++) {

      $$(cssSelector).filter(visible).first().click();
      ideaTitlePreview = $$(previewTitleSelector).filter(visible).first().getText();

      switch (scrollCase) {
        case 1:
          ideaTitle = collection.get(ideaIndex + i).getText();
          break;
        case 2:
          ideaTitle = collection.get(ideaIndex + scrollElementsAmount - i).getText();
          break;
      }
      assertEquals(ideaTitlePreview, ideaTitle);
    }
  }

  @Step
  public void assertNoOverview() {
    $$(this.previewContainer).filter(visible).first().shouldNotBe(visible);
  }

  @Step
  public void assertOverview() {
    $$(this.previewContainer).filter(visible).first().shouldBe(visible);
  }

  @Step
  public void assertOverview(int ideaIndex) {
    String ideaPreviewTitle = $$(previewTitleSelector).filter(visible).first().getText();
    $$(ideaTitleSelector).filter(visible).get(ideaIndex).shouldHave(text(ideaPreviewTitle));
  }

  @Step
  public void assertOverview(String ideaTitle) {
    String ideaTitlePreview = $$(previewTitleSelector).filter(visible).first().getText();
    assertEquals(ideaTitle, ideaTitlePreview);
  }

  @Step
  public void assertPreview(String ideaTitle) {
    $("h1").shouldHave(text(ideaTitle));
  }

  @Step
  public void assertIsNotFavourite() {
    this.favouritesButton.shouldHave(attribute("uib-tooltip", "add to Favorites"));
  }

  @Step
  public void assertIsFavourite() {
    this.favouritesButton.shouldHave(attribute("uib-tooltip", "remove from Favorites"));
  }
}
