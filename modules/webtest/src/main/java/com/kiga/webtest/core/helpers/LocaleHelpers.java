package com.kiga.webtest.core.helpers;

import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.KiGaDate;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class LocaleHelpers {

  public static String decimalFormat(Language language, double value, String pattern) {
    DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(language.locale());
    decimalFormat.applyPattern(pattern);

    return decimalFormat.format(value);
  }

  public static String dateFormat(Language language, KiGaDate date) {
    LocalDate localDate = LocalDate.of(date.year(), date.month(), date.day());
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(language.locale());
    String returner = localDate.format(dateTimeFormatter);
    if (language.equals(Language.TURKISH)) {
      returner = returner.replaceAll(".", " ");
    }
    return returner;
  }

}
