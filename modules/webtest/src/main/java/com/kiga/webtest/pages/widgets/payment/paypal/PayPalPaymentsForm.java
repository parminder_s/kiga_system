package com.kiga.webtest.pages.widgets.payment.paypal;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.PayPalAccount;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.datastructure.settings.Duration;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;

public class PayPalPaymentsForm extends BasePage {

  private final SelenideElement container = $("#login");
  private final SelenideElement payWithAccountElement = container.find("#loadLogin");
  private final SelenideElement summaryElement = container.find("#miniCart");

  public PayPalPaymentsForm(App app) {
    super(app);
  }

  @Step
  public PayPalPaymentsForm assertOpened() {
    this.payWithAccountElement.shouldBe(visible);
    return this;
  }

  @Step
  public PayPalPaymentsForm assertPrice(double sum, Currency currency) {
    DecimalFormat decimalFormat = (DecimalFormat)NumberFormat.getNumberInstance(Locale.UK);
    decimalFormat.applyPattern("#.00");
    this.summaryElement.shouldHave(text(currency.name() + " " + decimalFormat.format(sum)));
    return this;
  }

  @Step
  public PayPalPaymentsForm assertDuration(Duration duration) {
    this.summaryElement.shouldHave(text(duration.toString()));
    return this;
  }

  @Step
  public PayPalReviewInformation payWithAccount(PayPalAccount payPalAccount) {
    return new PayPalLogin(app, this.container).login(payPalAccount);
  }

  @Step
  public PayPalReviewInformation payPalReviewInformation() {
    return new PayPalReviewInformation(app, this.container);
  }

}
