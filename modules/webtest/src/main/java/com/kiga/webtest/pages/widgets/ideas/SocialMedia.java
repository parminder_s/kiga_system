package com.kiga.webtest.pages.widgets.ideas;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.switchTo;

public class SocialMedia extends BasePage {

  public SocialMedia(App app){
    super(app);
  }
  @Step
  public void assertFacebook(){
    $(".fa.fa-facebook.clickable").scrollTo().click();
    switchTo().window("Facebook").close();
    switchTo().window(0);
  }
  @Step
  public void assertTwitter(){
    $(".fa.fa-twitter.clickable").scrollTo().click();
    switchTo().window("Share a link on Twitter").close();
    switchTo().window(0);
  }
  @Step
  public void assertGoogle(){
    $(".fa.fa-google.clickable").scrollTo().click();
    switchTo().window("Google+").close();
    switchTo().window(0);
  }
  @Step
  public void assertPinterest(){
    $(".fa.fa-pinterest.clickable").scrollTo().click();
    switchTo().window("Pinterest").close();
  }
}
