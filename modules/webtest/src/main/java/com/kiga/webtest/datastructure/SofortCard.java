package com.kiga.webtest.datastructure;

public class SofortCard {

  private final String bic;
  private final String tan;
  private final String number;
  private final String pin;
  private final int accountIndex;

  public SofortCard(
    String bic, String tan,
    String number, String pin,
    int accountIndex) {
    this.bic = bic;
    this.tan = tan;
    this.number = number;
    this.pin = pin;
    this.accountIndex = accountIndex;
  }

  public String bic() {
    return this.bic;
  }

  public String tan() {
    return this.tan;
  }

  public String number() {
    return this.number;
  }

  public String pin() {
    return this.pin;
  }

  public int accountIndex() {
    return accountIndex;
  }

  @Override
  public String toString() {
    return "SofortCard{" +
      "bic='" + bic + '\'' +
      ", tan='" + tan + '\'' +
      ", number='" + number + '\'' +
      ", pin='" + pin + '\'' +
      ", account index='" + accountIndex +
      '}';
  }

}
