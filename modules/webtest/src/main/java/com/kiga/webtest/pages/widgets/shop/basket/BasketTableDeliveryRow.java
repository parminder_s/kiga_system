package com.kiga.webtest.pages.widgets.shop.basket;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.Country;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.kiga.webtest.core.checks.Assertions.waitingIs;

public class BasketTableDeliveryRow extends BasePage {

  private final SelenideElement container;

  private final ElementsCollection items;
  private final SelenideElement title;
  private final SelenideElement country;
  private final BasketTableSumAndCurrencyCell sum;

  public BasketTableDeliveryRow(App app, SelenideElement parent) {
    super(app);
    this.container = parent.find(".product-table-delivery");
    this.items = this.container.findAll(".product-table-delivery-item");
    this.title = this.items.get(0);
    this.country = this.items.get(2);
    this.sum = new BasketTableSumAndCurrencyCell(app, this.items.get(3));
  }

  @Step
  public BasketTableDeliveryRow assertData(ShopData shopData, int maxIndex) {
    assertTitle(
      shopData.delivery(maxIndex).price() == 0 ?
        language.term("shop.basket.shippingfree") :
        language.term("shop.basket.shipping")
    )
      .assertCountry(shopData.country())
      .assertSum(shopData.delivery(maxIndex).price(), shopData.currency());
    return this;
  }

  @Step
  public BasketTableDeliveryRow assertTitle(String title) {
    this.title.shouldHave(exactText(title));
    return this;
  }

  @Step
  public BasketTableDeliveryRow assertCountry(Country country) {
    if (waitingIs(this.country, visible)) {
      this.country.shouldHave(
        exactText(
          language.term("shop.basket.country." + country.isoCode())
        )
      );
    }
    return this;
  }

  @Step
  public BasketTableDeliveryRow assertSum(double sum, Currency currency) {
    this.sum.shouldEqual(sum, currency);
    return this;
  }

}
