package com.kiga.webtest.pages.widgets.common;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.visible;
import static com.kiga.webtest.core.angular.AngularHelpers.$;


public class LoginForm extends BasePage {

  public final SelenideElement container = $(".container.login");
  public final String shopRegistration = "[translate=\"LOGIN_SHOP_REGISTRATION\"]";

  public LoginForm(App app) {
    super(app);
  }

  @Step
  public SubscriptionForm startSubscribe() {
    this.container.find("[translate='LOGIN_KIGA_SUBSCRIPTION']").click();
    return new SubscriptionForm(app);
  }
  @Step
  public void login(String email, String password) {
    this.container.find("[name='email']").setValue(email);
    this.container.find("[name='password']").setValue(password);
    new Buttons(this.container).click("Login");
  }
  @Step
  public void assertRegistrationButtons() {
    this.container.find(shopRegistration).shouldBe(visible);
    this.container.find("[translate=\"LOGIN_FREE_SUBSCRIPTION\"]").shouldBe(not(visible));
    this.container.find("[translate=\"LOGIN_KIGA_SUBSCRIPTION\"]").shouldBe(not(visible));
  }
  @Step
  public void startRegistration() {
    this.container.find(shopRegistration).click();
  }
}
