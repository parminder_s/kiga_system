package com.kiga.webtest.pages.pageconfigs;

import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.pages.App;

public class BasePaymentPage extends BasePage {

  private final double total;
  private final Currency currency;

  public BasePaymentPage(App app, double total, Currency currency) {
    super(app);
    this.total = total;
    this.currency = currency;
  }

  public double total() {
    return total;
  }

  public Currency currency() {
    return currency;
  }

}
