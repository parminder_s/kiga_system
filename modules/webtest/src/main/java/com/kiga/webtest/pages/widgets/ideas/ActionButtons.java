package com.kiga.webtest.pages.widgets.ideas;

import static com.codeborne.selenide.Configuration.reportsFolder;
import static com.codeborne.selenide.Selenide.$$;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static org.junit.Assert.assertEquals;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import ru.yandex.qatools.allure.annotations.Step;

public class ActionButtons extends BasePage {

  private String previewContainer = ".preview-container";
  private String printButton = ".theme-darkGrey a";
  private final SelenideElement container = $(previewContainer);
  private final Buttons buttons =
      new Buttons(this.container, ".action-buttons .action-btn[data-test^='preview']");
  private final SelenideElement favouritesButton = this.buttons.button($$("div.favourite-btn"));
  private final SelenideElement previewView = this.buttons.button($$(".fa-eye"));

  public ActionButtons(App app) {
    super(app);
  }

  @Step
  public void clickFavourites() {
    this.favouritesButton.click();
  }

  @Step
  public Idea viewIdea() {
    this.previewView.click();
    return new Idea(app);
  }

  @Step
  public void assertPrintButton() {
    reportsFolder = "./build/reports/download";
    try {
      $(printButton).download();
    } catch (FileNotFoundException | NoSuchElementException e) {
      deleteDownloadDir();
      e.printStackTrace();
    }
    int downloadsCount = new File(reportsFolder).list().length;
    assertEquals(1, downloadsCount);
    deleteDownloadDir();
  }

  private void deleteDownloadDir() {
    try {
      FileUtils.deleteDirectory(new File(reportsFolder));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
