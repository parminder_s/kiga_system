package com.kiga.webtest.pages.widgets.payment.card.sofort;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.kiga.webtest.core.angular.AngularHelpers.$;

public class SofortPaymentStep3Form extends BasePage {

  private final SelenideElement container = $("#LoginPage .main");
  private final Buttons buttons = new Buttons(this.container, "button");

  public SofortPaymentStep3Form(App app) {
    super(app);
  }

  @Step
  public SofortPaymentStep3Form assertError() {
    this.container.find(".alert-box.error h2")
      .shouldHave(exactText("Das Login zu Ihrer Bank ist fehlgeschlagen "));
    return this;
  }

  @Step
  public SofortPaymentStep2Form goToPreviousStep() {
    this.container.find(".return-action").click();
    return new SofortPaymentStep2Form(app);
  }

  @Step
  public SofortPaymentStep4Form next() {
    buttons.click(language.term("next"));
    return new SofortPaymentStep4Form(app);
  }

  @Step
  public SofortPaymentStep3Form setNumber(String number) {
    SelenideElement elem = this.container.find("#BackendFormLOGINNAMEUSERID");
    elem.parent().click(); //needed to update content of input!
    elem.setValue(number);
    return this;
  }

  @Step
  public SofortPaymentStep3Form setPin(String pin) {
    SelenideElement elem = this.container.find("#BackendFormUSERPIN");
    elem.parent().click(); //needed to update content of input!
    elem.setValue(pin);
    return this;
  }
}
