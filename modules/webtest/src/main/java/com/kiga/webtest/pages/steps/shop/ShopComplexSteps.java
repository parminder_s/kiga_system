package com.kiga.webtest.pages.steps.shop;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.datastructure.settings.ShopPaymentMethod;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class ShopComplexSteps extends BasePage {

  private final ShopData shopData;
  private final CustomerData customerData;
  private final ShopPaymentMethod paymentMethod;

  public ShopComplexSteps(
    App app,
    ShopData shopData, CustomerData customerData,
    ShopPaymentMethod paymentMethod) {
    super(app);
    this.shopData = shopData;
    this.customerData = customerData;
    this.paymentMethod = paymentMethod;
  }

  @Step
  public ShopComplexSteps addToBasketAndStartPay() {
    return addToBasket()
      .selectPaymentMethod()
      .assertSummaryAndStartPay();
  }

  @Step
  public ShopComplexSteps addToBasket() {
    app.shop().forData(shopData)
      .addToBasket(0);
    app.shop().forData(shopData)
      .assertBasketRows()
      .assertBasketDelivery()
      .assertBasketTotal();
    app.shop()
      .basket().startPay()
      .fillPhone(customerData.phone())
      .next();
    return this;
  }

  @Step
  public ShopComplexSteps selectPaymentMethod() {
    app.shop()
      .paymentMethod()
      .assertOpened()
      .selectPaymentMethod(paymentMethod)
      .acceptTermsConditions()
      .acceptSecurity()
      .next();
    return this;
  }

  @Step
  public ShopComplexSteps assertSummaryAndStartPay() {
    app.shop()
      .summary()
      .assertOrder(shopData).assertCustomerData(customerData)
      .assertPaymentMethod(paymentMethod)
      .startPay();
    return this;
  }

}
