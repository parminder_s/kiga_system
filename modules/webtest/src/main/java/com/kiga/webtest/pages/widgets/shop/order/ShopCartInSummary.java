package com.kiga.webtest.pages.widgets.shop.order;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.shop.basket.BasketTable;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.helpers.AdditionalSelenideAPI.scrollWithOffsetOn;

public class ShopCartInSummary extends BasePage {

  public final SelenideElement container;
  private final BasketTable table;

  public ShopCartInSummary(App app, SelenideElement parent) {
    super(app);
    this.container = parent.find("[translate='SHOP_CART_HEADER']").parent();
    this.table = new BasketTable(app, this.container);
  }

  @Step
  public ShopCartInSummary assertData(ShopData shopData) {
    //without scroll error for firefox in Xvfb run - header shows over element
    scrollWithOffsetOn(this.container, 0, -150);

    int maxIndex = shopData.products().size() - 1;
    this.table.rows().shouldHaveData(
      shopData,
      maxIndex
    );
    this.table.deliveryRow().assertData(
      shopData,
      maxIndex
    );
    this.table.totalRow().assertData(
      shopData,
      maxIndex
    );
    return this;
  }
}
