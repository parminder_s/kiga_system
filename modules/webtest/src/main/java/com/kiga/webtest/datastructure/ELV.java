package com.kiga.webtest.datastructure;

public class ELV {

  private String iban;
  private String bic;

  public ELV(String iban, String bic) {
    this.iban = iban;
    this.bic = bic;
  }

  public String iban() {
    return this.iban;
  }

  public String bic() {
    return this.bic;
  }

  @Override
  public String toString() {
    return "ELV{" +
      "iban='" + this.iban + '\'' +
      ", bic='" + this.bic + '\'' +
      '}';
  }
}
