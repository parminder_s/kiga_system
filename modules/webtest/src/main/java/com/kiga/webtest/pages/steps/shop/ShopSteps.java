package com.kiga.webtest.pages.steps.shop;

import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.shop.basket.Basket;
import com.kiga.webtest.pages.widgets.shop.Product;
import ru.yandex.qatools.allure.annotations.Step;


public class ShopSteps extends BasePage {

  private final ShopData shopData;

  public ShopSteps(App app, ShopData shopData) {
    super(app);
    this.shopData = shopData;
  }

  @Step
  public Product selectProduct(int index) {
    return app.shop().products()
      .selectProduct(this.shopData.product(index).title());
  }

  @Step
  public ShopSteps assertBasketRows(int maxIndex) {
    app.shop().basket().table().rows()
      .shouldHaveData(this.shopData, maxIndex);
    return this;
  }

  @Step
  public ShopSteps assertBasketRows() {
    return assertBasketRows(this.shopData.products().size() - 1);
  }

  @Step
  public ShopSteps assertBasketDelivery(int maxIndex) {
    app.shop().basket()
      .table().deliveryRow().assertData(this.shopData, maxIndex);
    return this;
  }

  @Step
  public ShopSteps assertBasketTotal(int maxIndex) {
    app.shop().basket()
      .table().totalRow().assertData(this.shopData, maxIndex);
    return this;
  }

  @Step
  public ShopSteps assertBasketDelivery() {
    return assertBasketDelivery(this.shopData.products().size() - 1);
  }

  @Step
  public ShopSteps assertBasketTotal() {
    return assertBasketTotal(this.shopData.products().size() - 1);
  }

  @Step
  public ShopSteps assertProductData(int index) {
    app.shop().product().assertShopData(this.shopData.product(index));
    return this;
  }

  @Step
  public Basket addToBasket(int index) {
    return selectProduct(index)
      .addToBasket(this.shopData.quantity(index));
  }

}
