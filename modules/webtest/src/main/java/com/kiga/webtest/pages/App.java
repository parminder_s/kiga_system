package com.kiga.webtest.pages;

import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.manager.DataManager;
import com.kiga.webtest.pages.endpoints.EndPointsManager;
import com.kiga.webtest.pages.helpers.BrowserHelper;
import com.kiga.webtest.pages.requests.RequestManager;
import com.kiga.webtest.pages.steps.common.*;
import com.kiga.webtest.pages.steps.ideas.IdeaOverviewSteps;
import com.kiga.webtest.pages.steps.planner.PlannerSteps;
import com.kiga.webtest.pages.steps.ideas.IdeasSteps;
import com.kiga.webtest.pages.steps.registration.RegistrationSteps;
import com.kiga.webtest.pages.steps.registration.StartSubscriptionSteps;
import com.kiga.webtest.pages.steps.registration.payment.PaymentSteps;
import com.kiga.webtest.pages.steps.shop.ShopCommonSteps;
import com.kiga.webtest.pages.widgets.common.LoginForm;
import com.kiga.webtest.pages.widgets.ideas.IdeaOverview;
import ru.yandex.qatools.allure.annotations.Step;

public class App {

  public final Language language;

  public App(Language language) {
    this.language = language;
  }

  @Step
  public Cookies cookies(){
    return new Cookies(this);
  }

  @Step
  public DataManager data() {
    return new DataManager();
  }

  @Step
  public RegistrationSteps registrationFor(CustomerData customerData) {
    return new RegistrationSteps(this, customerData);
  }

  @Step
  public WelcomeSteps welcome() {
    return new WelcomeSteps(this);
  }

  @Step
  public PaymentSteps paymentFor(CustomerData customerData) {
    return new PaymentSteps(this, customerData);
  }

  @Step
  public NavigationSteps navigation() {
    return new NavigationSteps(this);
  }

  @Step
  public TopBarSteps topBar() {
    return new TopBarSteps(this);
  }

  @Step
  public BreadcrumbsSteps breadcrumbs() {return  new BreadcrumbsSteps(this);}

  @Step
  public ShopCommonSteps shop() {
    return new ShopCommonSteps(this);
  }

  @Step
  public IdeasSteps ideas() {
    return new IdeasSteps(this);
  }

  @Step
  public IdeaOverviewSteps ideaOverview(){
    return new IdeaOverviewSteps(this);
  }

  @Step
  public PlannerSteps planner() {
    return new PlannerSteps(this);
  }

  @Step
  public StartSubscriptionSteps subscriptionFor(CustomerData customerData) {
    return new StartSubscriptionSteps(this, customerData);
  }

  @Step
  public StartSubscriptionSteps subscriptionFor() {
    return new StartSubscriptionSteps(this);
  }

  @Step
  public EndPointsManager endPoints() {
    return new EndPointsManager();
  }

  @Step
  public BrowserHelper browser() {
    return new BrowserHelper();
  }

  @Step
  public RequestManager requests() {
    return new RequestManager(this);
  }

  @Step
  public App ensureLoggedOut() {
    endPoints().security().ensureLogOut();
    return this;
  }

  @Step
  public App assertHomeAndLoggedOut() {
    navigation().assertHomeUrl();
    topBar().assertLoggedOut();
    return this;
  }

  @Step
  public App login(CustomerData customerData) {
    new LoginForm(this)
      .login(customerData.email(), customerData.customer().password());
    topBar().assertLoggedIn();//for exact detect - logged in and page is loaded
    return this;
  }

  @Step
  public LoginForm login() {
    return new LoginForm(this);
  }

}
