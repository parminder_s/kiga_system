package com.kiga.webtest.pages.steps.shop;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

import com.kiga.webtest.datastructure.Country;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.datastructure.ShopData.ShopDataBuilder;
import com.kiga.webtest.datastructure.ShopProductData;
import com.kiga.webtest.datastructure.settings.ShopPaymentMethod;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.steps.shop.payment.ShopPaymentSteps;
import com.kiga.webtest.pages.widgets.shop.Product;
import com.kiga.webtest.pages.widgets.shop.Products;
import com.kiga.webtest.pages.widgets.shop.ShopFinishForm;
import com.kiga.webtest.pages.widgets.shop.basket.Basket;
import com.kiga.webtest.pages.widgets.shop.order.ShopOrderCustomerInformationForm;
import com.kiga.webtest.pages.widgets.shop.order.ShopOrderPaymentMethodForm;
import com.kiga.webtest.pages.widgets.shop.order.ShopOrderSummaryForm;
import java.util.Arrays;
import ru.yandex.qatools.allure.annotations.Step;

public class ShopCommonSteps extends BasePage {

  public ShopCommonSteps(App app) {
    super(app);
  }

  @Step
  public ShopCommonSteps prepare(Country country) {
    getWebDriver().manage().deleteAllCookies();
    app.endPoints().country().set(country);
    app.endPoints().shop().emptyBasket();
    app.navigation().visitShop();
    return this;
  }

  @Step
  public Product selectProduct(int index) {
    return products().selectProduct(index);
  }

  @Step
  public ShopData getDataForProducts(String prefix, Country country, int... indexes) {
    ShopDataBuilder returnerBuilder =
        app.data()
            .shop(prefix + " " + Arrays.toString(indexes) + " in " + country)
            .inCountry(country);
    for (int i = 0; i < indexes.length; i++) {
      returnerBuilder.addProduct(this.getProductData(indexes[i]));
    }
    return returnerBuilder.build();
  }

  @Step
  public ShopData getDataForProduct(String prefix, Country country, int index, int quantity) {
    ShopDataBuilder returnerBuilder =
        app.data().shop(prefix + " " + index + " in " + country).inCountry(country);
    ShopProductData productData = this.getProductData(index);
    returnerBuilder.addProducts(
        productData.title(), productData.price(), quantity, productData.productTitle());
    return returnerBuilder.build();
  }

  @Step
  public ShopData prepareForCustomerAndProduct(
      CustomerData customerData, int productIndex, int productQuantity) {
    app.endPoints().prepareShopFor(customerData);
    app.navigation().visitShop();
    ShopData shopData =
        getDataForProduct("Product", customerData.country(), productIndex, productQuantity);
    return shopData;
  }

  @Step
  public ShopSteps forData(ShopData shopData) {
    return new ShopSteps(app, shopData);
  }

  @Step
  public ShopComplexSteps forData(
      ShopData shopData, CustomerData customerData, ShopPaymentMethod paymentMethod) {
    return new ShopComplexSteps(app, shopData, customerData, paymentMethod);
  }

  @Step
  public ShopPaymentSteps paymentFor(CustomerData customerData, ShopData shopData) {
    return new ShopPaymentSteps(app, customerData, shopData);
  }

  @Step
  public Products products() {
    return new Products(app);
  }

  @Step
  public Product product() {
    return new Product(app);
  }

  @Step
  public ShopProductData getProductData(int index) {
    return products().getProductData(index);
  }

  @Step
  public Basket basket() {
    return new Basket(app);
  }

  @Step
  public ShopOrderCustomerInformationForm customerInformation() {
    return new ShopOrderCustomerInformationForm(app);
  }

  @Step
  public ShopOrderPaymentMethodForm paymentMethod() {
    return new ShopOrderPaymentMethodForm(app);
  }

  @Step
  public ShopOrderSummaryForm summary() {
    return new ShopOrderSummaryForm(app);
  }

  @Step
  public ShopFinishForm finish() {
    return new ShopFinishForm(app);
  }

  @Step
  public void assertFinished() {
    app.shop().finish().assertOpened();
    // app.topBar().basketButton().shouldHaveProductsCount(0);

    app.shop().finish().goHome();
    app.navigation().assertHomeUrl();
  }
}
