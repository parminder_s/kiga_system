package com.kiga.webtest.pages.widgets.registration;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.settings.Duration;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import com.kiga.webtest.pages.widgets.basic.DropDownList;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.selected;
import static com.codeborne.selenide.Selectors.byValue;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.locators.Locators.dt;
import static com.codeborne.selenide.Condition.exactText;

public class PaidRegistrationStep1Form extends BasePage {

  private final SelenideElement container = $(".registration>.container");

  private final ElementsCollection durations =
    container.findAll("[data-test^='duration'] .product-duration-label");
  private final SelenideElement amIStudent =
    this.container.find(dt("student")).parent();

  public PaidRegistrationStep1Form(App app) {
    super(app);
  }

  @Step
  public PaidRegistrationStep1Form selectDuration(Duration duration) {
    this.durations.find(exactText(duration.title(language))).click();
    return this;
  }

  @Step
  public PaidRegistrationStep1Form setStudentStatus(boolean isStudent) {
    amIStudent.shouldNotBe(selected);
    if (isStudent) {
      amIStudent.click();
    }
    return this;
  }

  @Step
  public PaidRegistrationStep1Form assertStudentStatus(boolean isStudent) {
    if (isStudent) {
      amIStudent.shouldBe(selected);
    } else {
      amIStudent.shouldNotBe(selected);
    }
    return this;
  }


  @Step
  public PaidRegistrationStep1Form selectAdditionalLicences(int count) {
    if (count > 0) {
      SelenideElement additionalLicences = this.container.find(dt("licences"));
      new DropDownList(additionalLicences, "option")
        .open()
        .select(byValue(String.valueOf(count)));
    }
    return this;
  }

  @Step
  public PaidRegistrationStep1Form assertDuration(Duration duration) {
    this.container.find(".text-duration>.product-duration-label")
      .shouldHave(exactText(duration.title(language)));
    return this;
  }

  @Step
  public PaidRegistrationStep2Form next() {
    new Buttons(this.container).click(language.term("next"));
    return new PaidRegistrationStep2Form(app);
  }

}
