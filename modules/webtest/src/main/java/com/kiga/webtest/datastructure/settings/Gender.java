package com.kiga.webtest.datastructure.settings;

import com.kiga.webtest.config.Language;

public enum Gender {
  MALE("male"), FEMALE("female");

  private String attrDataTestValue;

  Gender(String attrDataTestValue) {
    this.attrDataTestValue = attrDataTestValue;
  }

  public String title(Language language) {
    return language.term(this.attrDataTestValue);
  }

  @Override
  public String toString() {
    return this.attrDataTestValue;
  }
}
