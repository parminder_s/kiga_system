package com.kiga.webtest.pages.widgets.basic;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Selectors.byText;
import static com.kiga.webtest.core.locators.Locators.childHasOneOfDt;
import static com.kiga.webtest.core.locators.Locators.childHasOneOfDtReg;

public class RadioGroup {

  private final SelenideElement container;

  public RadioGroup(SelenideElement container) {
    this.container = container;
  }

  public void clickOn(By itemLocator) {
    this.container.find(itemLocator).click();
  }

  public void clickOnGender(String attrDataTestValue) {
    clickOn(childHasOneOfDtReg(attrDataTestValue));
  }

  public void clickOn(String attrDataTestValue) {
    clickOn(childHasOneOfDt(attrDataTestValue));
  }

  public void setRadio(String gender) {Selenide.$(byText(gender)).setSelected(true);}

  public RadioGroup shouldBeSelected(String attrDataTestValue) {
    return shouldBeSelected(childHasOneOfDtReg(attrDataTestValue));
  }

  public RadioGroup shouldNotBeSelected(String attrDataTestValue) {
    return shouldNotBeSelected(childHasOneOfDt(attrDataTestValue));
  }

  public RadioGroup shouldBeSelected(By itemLocator) {
    this.container.find(itemLocator).shouldHave(cssClass("icon-radio-on"));
    return this;
  }

  public RadioGroup shouldNotBeSelected(By itemLocator) {
    this.container.find(itemLocator).shouldHave(cssClass("icon-radio-off"));
    return this;
  }
}
