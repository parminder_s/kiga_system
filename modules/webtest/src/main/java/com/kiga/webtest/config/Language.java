package com.kiga.webtest.config;

import java.util.Locale;
import java.util.Properties;

public enum Language {

  ENGLISH("en", "English"),
  ITALIANO("it", "Italiano"),
  DEUTSCH("de", "Deutsch"),
  TURKISH("tr", "Türkçe");

  public String shortValue() {
    return this.shortValue;
  }

  public String description() {
    return this.description;
  }

  public Locale locale() {
    return locale;
  }

  public static Language getDefault() {
    return ENGLISH;
  }

  private String shortValue;
  private String description;
  private Properties terms;
  private Locale locale;

  Language(String shortValue, String description) {
    this.shortValue = shortValue;
    this.description = description;
    this.locale = new Locale(shortValue, "");
  }

  public void setTerms(Properties terms) {
    this.terms = terms;
  }

  public String term(String termTitle) {
    return String.valueOf(terms.getProperty(termTitle));
  }

}
