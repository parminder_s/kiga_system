package com.kiga.webtest.pages.widgets.registration;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import com.kiga.webtest.pages.widgets.basic.CheckBoxGroup;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.locators.Locators.dt;

public class PaidRegistrationStep3Form extends BasePage {

  private final SelenideElement container = $(".registration>.container");

  public PaidRegistrationStep3Form(App app) {
    super(app);
  }

  @Step
  public PaidRegistrationStep3Form acceptTermsConditionsAndSecurity() {
    new CheckBoxGroup(this.container, "agb", "security", "datenschutz").toggleAll();//.checkAll();
    return this;
  }

  @Step
  public PaymentMethods paymentMethods() {
    return new PaymentMethods(app, this.container);
  }

  @Step
  public PaidRegistrationStep3Form assertOpened() {
    this.container.find(dt("payment")).shouldBe(visible);
    return this;
  }

  @Step
  public PaidRegistrationStep3Form next() {
    new Buttons(this.container).click(language.term("next"));
    return this;
  }

  @Step
  public PaidRegistrationStep3Form acceptAndNext() {
    acceptTermsConditionsAndSecurity();
    next();
    return this;
  }

  @Step
  public PaidRegistrationStep3Form assertWarning(String message) {
    this.container.find(".alert-warning").shouldHave(exactText(message));
    return this;
  }
}
