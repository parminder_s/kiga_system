package com.kiga.webtest.datastructure;

import com.kiga.webtest.datastructure.settings.CreditCardType;

public class CreditCard {

  private CreditCardType creditCardType;
  private String identifier;
  private String cvv;
  private int expiryMm;
  private int expiryYy;

  public CreditCard(
    CreditCardType creditCardType,
    String identifier,
    int expiryMm, int expiryYy,
    String cvv) {

    this.creditCardType = creditCardType;
    this.identifier = identifier;
    this.expiryMm = expiryMm;
    this.expiryYy = expiryYy;
    this.cvv = cvv;
  }

  public CreditCardType creditCardType() {
    return this.creditCardType;
  }

  public String identifier() {
    return this.identifier;
  }

  public int expiryMm() {
    return this.expiryMm;
  }

  public int expiryYy() {
    return this.expiryYy;
  }

  public String cvv() {
    return cvv;
  }

  @Override
  public String toString() {
    return "CreditCard{" +
      this.creditCardType +
      ", " + this.identifier +
      ", " + this.expiryMm +
      "/ " + this.expiryYy +
      ", cvv=" + this.cvv +
      '}';
  }
}
