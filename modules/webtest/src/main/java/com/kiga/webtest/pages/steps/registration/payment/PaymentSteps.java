package com.kiga.webtest.pages.steps.registration.payment;

import com.kiga.webtest.datastructure.CreditCard;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.ELV;
import com.kiga.webtest.datastructure.PayPalAccount;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.WithCustomerDataPage;
import ru.yandex.qatools.allure.annotations.Step;

public class PaymentSteps extends WithCustomerDataPage {

  public PaymentSteps(App app, CustomerData customerData) {
    super(app, customerData);
  }

  @Step
  public CreditCardPaymentSteps using(CreditCard creditCard) {
    return new CreditCardPaymentSteps(app, customerData, creditCard);
  }

  @Step
  public PayPalPaymentSteps using(PayPalAccount payPalAccount) {
    return new PayPalPaymentSteps(app, customerData, payPalAccount);
  }

  @Step
  public ElvPaymentSteps using(ELV elv) {
    return new ElvPaymentSteps(app, customerData, elv);
  }

  @Step
  public GarantiPaymentSteps usingGaranti(CreditCard creditCard) {
    return new GarantiPaymentSteps(app, customerData, creditCard);
  }

}
