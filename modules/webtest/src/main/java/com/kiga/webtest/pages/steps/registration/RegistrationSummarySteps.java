package com.kiga.webtest.pages.steps.registration;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.WithCustomerDataPage;
import com.kiga.webtest.pages.widgets.registration.RegistrationSummaryForm;
import ru.yandex.qatools.allure.annotations.Step;

public class RegistrationSummarySteps extends WithCustomerDataPage {

  public RegistrationSummarySteps(App app, CustomerData customerData) {
    super(app, customerData);
  }

  @Step
  public RegistrationSummarySteps confirm() {
    registrationSummary().confirm();
    return this;
  }

  @Step
  public RegistrationSummarySteps assertRegistrationData(String paymentMethodDescription) {
    assertChosenSubscription()
      .assertSubscriptionPriceAndDuration()
      .assertContactData()
      .assertPaymentMethodData(paymentMethodDescription);
    return this;
  }

  @Step
  public RegistrationSummarySteps assertSubscriptionPriceAndDuration() {
    registrationSummary()
      .assertSubscriptionPriceAndDuration(
        customerData.duration(),
        customerData.price(), customerData.country().currency()
      );
    return this;
  }

  @Step
  public RegistrationSummarySteps assertChosenSubscription() {
    registrationSummary()
      .assertChosenSubscription(customerData.subscriptionType());
    return this;
  }


  @Step
  public RegistrationSummarySteps assertContactData() {
    registrationSummary()
      .assertCommonInformation(
        customerData.customer().commonInformation(),
        customerData.email()
      )
      .assertBirthDate(customerData.birthDate())
      .assertAddressInformation(customerData.customer().address());
    return this;
  }

  @Step
  public RegistrationSummarySteps assertPaymentMethodData(String paymentMethodDescription) {
    registrationSummary()
      .assertPaymentMethodBlock(paymentMethodDescription);
    return this;
  }

  @Step
  public RegistrationSummarySteps assertPassportNumber() {
    registrationSummary().assertPassportNumber(customerData.passportNumber());
    return this;
  }

  @Step
  public RegistrationSummarySteps editChosenSubscription() {
    registrationSummary().chosenSubscriptionBlock().edit();
    return this;
  }

  @Step
  public RegistrationSummarySteps editSubscriptionPrice() {
    registrationSummary().subscriptionPriceBlock().edit();
    return this;
  }

  @Step
  public RegistrationSummarySteps editContact() {
    registrationSummary().contactBlock().edit();
    return this;
  }

  @Step
  public RegistrationSummarySteps editPaymentMethod() {
    registrationSummary().paymentMethodBlock().edit();
    return this;
  }
  //**********************Steps implementation*****************************

  @Step
  private RegistrationSummaryForm registrationSummary() {
    return new RegistrationSummaryForm(app);
  }

}
