package com.kiga.webtest.pages.steps.registration;

import com.kiga.webtest.pages.pageconfigs.WithCustomerDataPage;
import ru.yandex.qatools.allure.annotations.Step;

public class RegistrationSelectPaymentSteps extends WithCustomerDataPage {

  private final RegistrationSteps registration;

  public RegistrationSelectPaymentSteps(RegistrationSteps registration) {
    super(registration.app, registration.customerData);
    this.registration = registration;
  }

  @Step
  public RegistrationSelectPaymentSteps accept() {
    registration.paidRegistrationStep3().acceptTermsConditionsAndSecurity();
    return this;
  }

  @Step
  public RegistrationSelectPaymentSteps next() {
    registration.paidRegistrationStep3().next();
    return this;
  }

  @Step
  public RegistrationSelectPaymentSteps viaCreditCard() {
    registration.paymentMethods()
      .selectCreditCard();
    return this;
  }

  @Step
  public RegistrationSelectPaymentSteps invoiceViaPost() {
    registration.paymentMethods()
      .selectInvoice()
      .viaPost();
    return this;
  }

  @Step
  public RegistrationSelectPaymentSteps invoiceViaEmail() {
    registration.paymentMethods()
      .selectInvoice()
      .viaEmail();
    return this;
  }

  @Step
  public RegistrationSelectPaymentSteps viaElv() {
    registration.paymentMethods()
      .selectElv();
    return this;
  }

  @Step
  public RegistrationSelectPaymentSteps viaPayPal() {
    registration.paymentMethods()
      .selectPayPal();
    return this;
  }

  @Step
  public RegistrationSelectPaymentSteps viaGarantiCard() {
    registration.paymentMethods()
      .selectGarantiCard();
    return this;
  }

}
