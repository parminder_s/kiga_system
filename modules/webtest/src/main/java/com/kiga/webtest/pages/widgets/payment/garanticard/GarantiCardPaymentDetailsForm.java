package com.kiga.webtest.pages.widgets.payment.garanticard;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.core.checks.Assertions;
import com.kiga.webtest.datastructure.CreditCard;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.datastructure.settings.Duration;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.table.Table;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.kiga.webtest.core.angular.AngularHelpers.$;

public class GarantiCardPaymentDetailsForm extends BasePage {

  private final SelenideElement container = $("#anaAlan");

  private final SelenideElement cardNumberElement =
    container.find("#cardnumber");

  private final SelenideElement expiryMonthElement =
    container.find("#cardexpiredatemonth");
  private final SelenideElement expiryYearElement =
    container.find("#cardexpiredateyear");
  private final SelenideElement cvvElement =
    container.find("#cardcvv2");

  private final SelenideElement submitButton = container.find("#imageField");

  public GarantiCardPaymentDetailsForm(App app) {
    super(app);
  }

  @Step
  public GarantiCardPaymentDetailsForm fill(CreditCard creditCard) {
    this.cardNumberElement.setValue(creditCard.identifier());
    this.expiryMonthElement.setValue(
      String.format("%02d", creditCard.expiryMm()));
    this.expiryYearElement.setValue(
      String.valueOf(creditCard.expiryYy()).substring(2));
    this.cvvElement.setValue(creditCard.cvv());
    return this;
  }

  @Step
  public GarantiCardPaymentDetailsForm pay() {
    this.submitButton.click();
    return this;
  }

  private GarantiCardPaymentDetailsForm assertThat(String title, String text) {
    new Table(this.container)
      .row(title)
      .cell(1)
      .shouldHave(exactText(text));
    return this;
  }

  @Step
  public GarantiCardPaymentDetailsForm assertTotal(double sum, Currency currency) {
    DecimalFormat decimalFormat = (DecimalFormat)NumberFormat.getNumberInstance(Locale.UK);
    decimalFormat.applyPattern("#.0");

    return assertThat(
      language.term("garanti.amount"),
      decimalFormat.format(sum) + " " + currency.name()
    );
  }

  @Step
  public GarantiCardPaymentDetailsForm assertDuration(Duration duration) {
    return assertThat(
      language.term("garanti.recurringperiod"),
      duration.countMonth() + " " + language.term("garanti.month")
    );
  }

  @Step
  public GarantiCardPaymentDetailsForm assertOpened() {
    Assertions.urlToBe(
      "https://sanalposprovtest.garanti.com.tr/servlet/gt3dengine");
    this.cardNumberElement.shouldBe(visible);
    return this;
  }

}
