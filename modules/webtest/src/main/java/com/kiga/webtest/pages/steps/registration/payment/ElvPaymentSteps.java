package com.kiga.webtest.pages.steps.registration.payment;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.ELV;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.WithCustomerDataPage;
import com.kiga.webtest.pages.widgets.payment.elv.ElvPaymentDetailsForm;
import ru.yandex.qatools.allure.annotations.Step;

public class ElvPaymentSteps extends WithCustomerDataPage {

  private final ELV elv;

  public ElvPaymentSteps(App app, CustomerData customerData, ELV elv) {
    super(app, customerData);
    this.elv = elv;
  }

  @Step
  public ElvPaymentSteps payAndApprove() {
    elvPaymentDetails().assertOpened();

    elvPaymentDetails()
      .fill(elv)
      .agreeSepaDirectDebitTermsAndAuthorizeTheMandate()
      .pay();

    return this;
  }

  //**********************Steps implementation************

  @Step
  private ElvPaymentDetailsForm elvPaymentDetails() {
    return new ElvPaymentDetailsForm(app);
  }

}
