package com.kiga.webtest.datastructure.settings;

public enum Job {
  PEDAGOGUE("-PEDAGOGUE"), NURSERY("-NURSERY"), SCHOOL("-SCHOOL"), PARENTS("-PARENTS");

  private String attrDataTestValue;

  Job(String attrDataTestValue) {
    this.attrDataTestValue = attrDataTestValue;
  }

  public String getAttrDataTestValue() {
    return attrDataTestValue;
  }

  @Override
  public String toString() {
    return this.attrDataTestValue;
  }
}
