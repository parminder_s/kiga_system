package com.kiga.webtest.datastructure;

import com.kiga.webtest.datastructure.settings.Currency;

public class ShopProductData {

  private final String title;
  private final String productTitle;
  private final double price;
  private final Currency currency;

  public ShopProductData(String title, double price, Currency currency, String productTitle) {
    this.title = title;
    this.price = price;
    this.currency = currency;
    this.productTitle = productTitle;
  }

  public String title() {
    return this.title;
  }

  public double price() {
    return this.price;
  }

  public String productTitle()  {
    return this.productTitle;
  }
  public Currency currency() {
    return this.currency;
  }

  @Override
  public String toString() {
    return title + ", " + price + " " + currency;
  }
}
