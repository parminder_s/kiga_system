package com.kiga.webtest.pages.widgets.basic.table;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.text;

public class Table {

  private final SelenideElement container;

  public Table(SelenideElement container) {
    this.container = container;
  }

  private ElementsCollection rowElements() {
    return this.container.findAll("tr");
  }

  public Row row(int index) {
    return new Row(this.rowElements().get(index));
  }

  public Row row(String rowText) {
    return new Row(this.rowElements().find(text(rowText)));
  }
}
