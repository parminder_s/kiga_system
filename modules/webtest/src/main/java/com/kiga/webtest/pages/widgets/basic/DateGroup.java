package com.kiga.webtest.pages.widgets.basic;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.config.Language;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.exactValue;
import static com.kiga.webtest.core.helpers.AdditionalSelenideAPI.scrollWithOffsetOn;
import static com.kiga.webtest.core.locators.Locators.dt;
import static com.kiga.webtest.core.locators.Locators.dtReg;

public class DateGroup {

  private final SelenideElement container;
  private final SelenideElement dayElement;
  private final SelenideElement monthElement;
  private final SelenideElement yearElement;

  public DateGroup(SelenideElement container, SelenideElement dayElement, SelenideElement monthElement, SelenideElement yearElement) {
    this.container = container;
    this.dayElement = dayElement;
    this.monthElement = monthElement;
    this.yearElement = yearElement;
  }

  public DateGroup(SelenideElement container) {
    this(
      container,
      container.find(dtReg("bday")),
      container.find(dtReg("bmonth")).parent(),
      container.find(dtReg("byear"))
    );
  }

  private DateGroup setDay(int day) {
    this.dayElement.setValue(String.valueOf(day));
    return this;
  }

  private DateGroup setMonth(int month) {
    //without scroll error for firefox in Xvfb run - header shows over month element
    scrollWithOffsetOn(this.monthElement, 0, -150);

    new DropDownList(this.monthElement).open().select(month - 1);
    return this;
  }

  private DateGroup setYear(int year) {
    this.yearElement.setValue(String.valueOf(year));
    return this;
  }

  @Step
  public void set(int day, int month, int year) {
    setDay(day).setMonth(month).setYear(year);
  }

  public void assertDate(Language language, int day, int month, int year) {
    this.dayElement.shouldHave(exactValue(String.valueOf(day)));
    this.monthElement.shouldHave(exactText(language.term("monthtitle." + month)));
    this.yearElement.shouldHave(exactValue(String.valueOf(year)));
  }
}
