package com.kiga.webtest.pages.endpoints;

import com.kiga.webtest.config.KigaConfiguration;
import com.kiga.webtest.datastructure.CustomerData;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
import static com.kiga.webtest.core.angular.AngularHelpers.$$;

public class QuickRegisterEndPoint {

  private final CustomerData customerData;

  public QuickRegisterEndPoint(CustomerData customerData) {
    this.customerData = customerData;
  }

  @Step
  public String registerAndGetEmail() {
    String url = getUrl();
    openUrl(url);
    return $$(".email,body").find(text("@")).shouldBe(visible).getText();
  }

  @Step
  private void openUrl(String url) {
    //to report step in allure
    open(url);
  }

  private String getUrl() {
    String parameters = getParameters();
    return KigaConfiguration.appBaseUrl +
      "/test/quickRegister" +
      (parameters.isEmpty() ? "" : "?") +
      parameters;
  }

  private String getParameters() {
    List<String> parameters = new ArrayList<>();
    if (customerData == null) {
      return "";
    }
    if (customerData.country() != null) {
      parameters.add("country=" + customerData.country().isoCode());
    }
    if (customerData.subscriptionType() != null) {
      parameters.add("product=" + customerData.subscriptionType().productId());
    }
    if (customerData.duration() != null) {
      parameters.add("duration=" + customerData.duration().countMonth());
    }
    if (customerData.customer() != null) {
      if (customerData.customer().commonInformation() != null) {
        parameters.add("firstname=" + customerData.customer().commonInformation().firstName());
        parameters.add("lastname=" + customerData.customer().commonInformation().lastName());
      }
      if (customerData.customer().address() != null) {
        parameters.add("city=" + customerData.customer().address().city());
        parameters.add("street=" + customerData.customer().address().street());
        parameters.add("streetNumber=" + customerData.customer().address().streetNumber());
        if (customerData.customer().address().zip() != null) {
          parameters.add("zip=" + customerData.customer().address().zip());
        }
      }
    }
    if (customerData.email() != null) {
      parameters.add("email=" + customerData.email());
    }
    if (customerData.phone() != null) {
      parameters.add("phone=" + customerData.phone());
    }
    if (customerData.permission() != null) {
      parameters.add("permission=" + customerData.permission());
    }
    if (customerData.expired()) {
      parameters.add("isCancelled=1");
    }
    return String.join("&", parameters);
  }
}
