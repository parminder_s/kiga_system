package com.kiga.webtest.pages.widgets.payment.card.creditcard;


import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.CreditCard;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.DropDownList;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.value;

public class CreditCardVerificationSimulatorForm extends BasePage {

  private final SelenideElement container = $("body");

  private final DropDownList optionsList =
    new DropDownList(this.container.find("[name='PaRes']"), "option");

  public CreditCardVerificationSimulatorForm(App app) {
    super(app);
  }

  @Step
  public CreditCardVerificationSimulatorForm assertOpened(CreditCard creditCard) {
    this.container.find("h4").shouldHave(text(creditCard.creditCardType().toString()));
    return this;
  }

  @Step
  public void approveAuthentication() {
    chooseAndSubmit("OK");
  }

  @Step
  public void refuseToPay() {
    chooseAndSubmit("NOK");
  }

  @Step
  public void systemError() {
    chooseAndSubmit("ERR");
  }

  public void chooseAndSubmit(String optionValue) {
    optionsList.open().selectBy(value(optionValue));
    this.container.find("[type='submit']").click();
  }
}
