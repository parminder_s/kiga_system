package com.kiga.webtest.datastructure;

import com.kiga.webtest.datastructure.settings.Gender;

public class CustomerCommonInformation {

  private Gender gender;
  private String firstName;
  private String lastName;

  public CustomerCommonInformation(Gender gender, String firstName, String lastName) {
    this.gender = gender;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public Gender gender() {
    return this.gender;
  }

  public String firstName() {
    return this.firstName;
  }

  public String lastName() {
    return this.lastName;
  }

  @Override
  public String toString() {
    return this.gender +
      ", " + this.firstName +
      ", " + this.lastName;
  }
}
