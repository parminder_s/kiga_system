package com.kiga.webtest.datastructure.manager;

import ru.yandex.qatools.allure.annotations.Step;
import com.kiga.webtest.datastructure.CustomerData.CustomerDataBuilder;
import com.kiga.webtest.datastructure.ShopData.ShopDataBuilder;

public class DataManager {

  @Step
  public CustomerDataBuilder customer(String title) {
    return new CustomerDataBuilder(title);
  }

  @Step
  public ShopDataBuilder shop(String title) {
    return new ShopDataBuilder(title);
  }

}
