package com.kiga.webtest.pages.pageconfigs;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.pages.App;

public class WithCustomerDataPage extends BasePage {

  public final CustomerData customerData;

  public WithCustomerDataPage(App app, CustomerData customerData) {
    super(app);
    this.customerData = customerData;
  }
}
