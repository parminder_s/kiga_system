package com.kiga.webtest.pages.steps.registration;

import com.kiga.webtest.datastructure.Country;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.settings.Duration;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.WithCustomerDataPage;
import com.kiga.webtest.pages.widgets.common.SubscriptionForm;
import com.kiga.webtest.pages.widgets.registration.PaidRegistrationStep1Form;
import ru.yandex.qatools.allure.annotations.Step;

public class StartSubscriptionSteps extends WithCustomerDataPage {

  public StartSubscriptionSteps(App app, CustomerData customerData) {
    super(app, customerData);
  }

  public StartSubscriptionSteps(App app) {
    this(app,
      app.data().customer("customer is not used").build()
    );
  }

  @Step
  public StartSubscriptionSteps start() {
    if (customerData.subscriptionType().equals(SubscriptionType.FREE)) {
      form().subscribeKiGaFree();
    } else {
      selectCountryAndSubscriptionType();
      continueSubscription();
    }
    return this;
  }

  @Step
  public StartSubscriptionSteps selectCountryAndSubscriptionType() {
    form()
      .selectCountry(customerData.country())
      .as(customerData.subscriptionType());
    return this;
  }

  @Step
  public StartSubscriptionSteps continueSubscription() {
    paidRegistrationStep1()
      .setStudentStatus(customerData.isStudent())
      .selectDuration(customerData.duration())
      .selectAdditionalLicences(customerData.additionalLicencesCount());
    paidRegistrationStep1().assertDuration(customerData.duration());
    paidRegistrationStep1().next();
    return this;
  }

  @Step
  public StartSubscriptionSteps assertCountry(Country selectedCountry) {
    form().assertCountry(selectedCountry);
    return this;
  }

  @Step
  public StartSubscriptionSteps assertAmIStudent(boolean isStudent) {
    paidRegistrationStep1().assertStudentStatus(isStudent);
    return this;
  }

  @Step
  public StartSubscriptionSteps assertDuration(Duration duration) {
    paidRegistrationStep1().assertDuration(duration);
    return this;
  }

  //**********************Steps implementation************

  @Step
  private PaidRegistrationStep1Form paidRegistrationStep1() {
    return new PaidRegistrationStep1Form(app);
  }

  @Step
  private SubscriptionForm form() {
    return new SubscriptionForm(app);
  }

}
