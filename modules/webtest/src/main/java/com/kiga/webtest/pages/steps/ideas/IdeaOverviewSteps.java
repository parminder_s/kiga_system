package com.kiga.webtest.pages.steps.ideas;

import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.ideas.*;
import ru.yandex.qatools.allure.annotations.Step;

public class IdeaOverviewSteps extends BasePage {
  public IdeaOverviewSteps(App app) {super(app);}
  @Step
  public ActionButtons actionButtons(){return new ActionButtons(app);}
  @Step
  public SocialMedia socialMedia(){return new SocialMedia(app);}
  @Step
  public Thumbnails thumbnails(){return new Thumbnails(app);}
  @Step
  public IdeaOverviewSteps assertScrollOverview(int ideaIndex, int scrollElementsAmount){
    ideaOverview().assertScrollToNextOverview(ideaIndex, scrollElementsAmount);
    ideaOverview().assertScrollToPrevOverview(ideaIndex, scrollElementsAmount);
    return this;
  }
  @Step
  public IdeaOverviewSteps addToFavourites(){
    ideaOverview().assertIsNotFavourite();
    actionButtons().clickFavourites();
    return this;
  }
  @Step
  public IdeaOverviewSteps removeFromFavourites(){
    ideaOverview().assertIsFavourite();
    actionButtons().clickFavourites();
    return this;
  }

  //**********************Steps implementation*************************
  @Step
  private IdeaOverview ideaOverview(){return new IdeaOverview(app);}

}
