package com.kiga.webtest.pages.steps.ideas;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.ideas.*;
import ru.yandex.qatools.allure.annotations.Step;

public class IdeasSteps extends BasePage {

  public IdeasSteps(App app) {
    super(app);
  }

  @Step
  public Idea ideaForm() {
    return new Idea(app);
  }

  @Step
  public Ideas ideasForm() {
    return new Ideas(app, ".container.overview-row", ".element-container .element");
  }

  @Step
  public Ideas favouritesForm() {
    return new Ideas(app, ".favourites-overview>.type-row", "div.favourite");
  }

  @Step
  public Ideas startIdeas() {
    app.topBar().startIdeas();
    return ideasForm();
  }

  @Step
  public Ideas startFavourites() {
    app.topBar().startFavourites();
    return favouritesForm();
  }

  @Step
  public IdeaOverview ideaOverview() {
    return new IdeaOverview(app);
  }

  @Step
  public void prepareFor(CustomerData customerData) {
    app.endPoints().quickRegisterFor(customerData).registerAndGetEmail();
    app.navigation().visitHome();
  }
}
