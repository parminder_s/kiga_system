package com.kiga.webtest.datastructure.exceptions;

import org.openqa.selenium.WebDriverException;

public class TestDataMismatch extends WebDriverException {

  public TestDataMismatch(String message) {
    super(message);
  }
}
