package com.kiga.webtest.pages.steps.registration.payment;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.PayPalAccount;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.WithCustomerDataPage;
import com.kiga.webtest.pages.widgets.payment.paypal.PayPalPaymentsForm;
import ru.yandex.qatools.allure.annotations.Step;

public class PayPalPaymentSteps extends WithCustomerDataPage {

  private final PayPalAccount payPalAccount;

  public PayPalPaymentSteps(App app, CustomerData customerData, PayPalAccount payPalAccount) {
    super(app, customerData);
    this.payPalAccount = payPalAccount;
  }

  @Step
  public PayPalPaymentSteps payAndApproveOld() {
    payPalPaymentsForm()
      .assertOpened()
      .assertDuration(customerData.duration())
      .assertPrice(customerData.price(), customerData.country().currency())
      .payWithAccount(payPalAccount)
      .assertAccount(payPalAccount)
      .agreeAndContinue();
    return this;
  }
  @Step
  public PayPalPaymentSteps payAndApprove() {
    payPalPaymentsForm()
    .payWithAccount(payPalAccount);
    return this;
  }

  //**********************Steps implementation************

  @Step
  private PayPalPaymentsForm payPalPaymentsForm() {
    return new PayPalPaymentsForm(app);
  }
}
