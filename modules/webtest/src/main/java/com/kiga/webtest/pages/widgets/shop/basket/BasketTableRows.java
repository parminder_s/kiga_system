package com.kiga.webtest.pages.widgets.shop.basket;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class BasketTableRows extends BasePage {

  private final SelenideElement container;
  private final ElementsCollection rows;

  public BasketTableRows(App app, SelenideElement container) {
    super(app);
    this.container = container;
    this.rows = container.findAll(".products.row");
  }

  @Step
  public BasketTableRow get(int index) {
    return new BasketTableRow(app, rows.get(index));
  }

  @Step
  public BasketTableRows shouldHaveSize(int count) {
    rows.shouldHaveSize(count);
    return this;
  }

  @Step
  public BasketTableRows shouldHaveData(ShopData shopData, int maxIndex) {
    shouldHaveSize(maxIndex + 1);
    for (int i = 0; i <= maxIndex; i++) {
      this.get(i).shouldHaveData(shopData.product(i), shopData.quantity(i));
    }
    return this;
  }
}
