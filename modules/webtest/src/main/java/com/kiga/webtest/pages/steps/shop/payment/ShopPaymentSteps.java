package com.kiga.webtest.pages.steps.shop.payment;

import com.kiga.webtest.datastructure.CreditCard;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.datastructure.SofortCard;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.WithCustomerDataPage;
import ru.yandex.qatools.allure.annotations.Step;

public class ShopPaymentSteps extends WithCustomerDataPage {

  private final ShopData shopData;

  public ShopPaymentSteps(App app, CustomerData customerData, ShopData shopData) {
    super(app, customerData);
    this.shopData = shopData;
  }

  @Step
  public ShopCreditCardPaymentSteps using(CreditCard creditCard) {
    return new ShopCreditCardPaymentSteps(
      app,
      shopData.total() + shopData.delivery().price(),
      shopData.currency(),
      creditCard
    );
  }

  @Step
  public ShopSofortCardPaymentSteps using(SofortCard sofortCard) {
    return new ShopSofortCardPaymentSteps(
      app, shopData.total() + shopData.delivery().price(),
      shopData.country(),
      sofortCard
    );
  }
}
