package com.kiga.webtest.datastructure.settings;

public enum SubscriptionPaymentMethod {
  CREDIT_CARD("MP-CC"),
  GARANTI_CARD("GARANTI"),
  PAY_PAL("PAYPAL"),
  ELV("MP-ELV"),
  INVOICE("RE");

  private final String value;

  SubscriptionPaymentMethod(String value) {
    this.value = value;
  }

  public String value() {
    return this.value;
  }

  @Override
  public String toString() {
    return this.name();
  }
}
