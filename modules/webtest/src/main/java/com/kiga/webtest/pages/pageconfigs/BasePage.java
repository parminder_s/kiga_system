package com.kiga.webtest.pages.pageconfigs;

import com.kiga.webtest.config.Language;
import com.kiga.webtest.pages.App;
import ru.yandex.qatools.allure.annotations.Step;

public class BasePage {

  public final App app;
  public final Language language;

  public BasePage(App app) {
    this.app = app;
    this.language = app.language;
  }

}
