package com.kiga.webtest.pages.widgets.ideas;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.settings.IdeasSection;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

public class IdeasSections extends BasePage {

  private final SelenideElement container;
  private final Buttons sections;
  private final String categoriesSelector;
  private final String ideasSelector;

  public IdeasSections(
      App app, SelenideElement container, String categoriesSelector, String ideasSelector) {
    super(app);
    this.container = container;
    this.sections = new Buttons(this.container.find(".element-container"), ".button-name");
    this.categoriesSelector = categoriesSelector;
    this.ideasSelector = ideasSelector;
  }

  @Step
  public IdeasCategories select(IdeasSection ideasSection) {
    sections.click(ideasSection.toString());
    this.container.find(".loader").shouldNotBe(Condition.visible);
    return new IdeasCategories(app, this.container, this.categoriesSelector, this.ideasSelector);
  }
}
