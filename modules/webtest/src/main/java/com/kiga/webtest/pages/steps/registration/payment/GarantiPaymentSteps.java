package com.kiga.webtest.pages.steps.registration.payment;

import com.kiga.webtest.core.checks.Assertions;
import com.kiga.webtest.datastructure.CreditCard;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.WithCustomerDataPage;
import com.kiga.webtest.pages.widgets.payment.garanticard.GarantiCardPaymentDetailsForm;
import com.kiga.webtest.pages.widgets.payment.garanticard.GarantiCardPaymentRedirectForm;
import ru.yandex.qatools.allure.annotations.Step;

public class GarantiPaymentSteps extends WithCustomerDataPage {

  private final CreditCard creditCard;

  public GarantiPaymentSteps(App app, CustomerData customerData, CreditCard creditCard) {
    super(app, customerData);
    this.creditCard = creditCard;
  }

  @Step
  public GarantiPaymentSteps pay() {
    garantiCardPaymentDetails()
      .assertOpened()
      .assertTotal(customerData.price(), customerData.country().currency())
      .assertDuration(customerData.duration())
      .fill(creditCard)
      .pay();
    return this;
  }

  @Step
  public GarantiPaymentSteps assertMDPayDefaultResponseOpened() {
    Assertions.urlToBe("https://sanalposprovtest.garanti.com.tr/servlet/gt3dengine");
    return this;
  }

  //**********************Steps implementation************

  @Step
  private GarantiCardPaymentRedirectForm garantiCardPaymentRedirect() {
    return new GarantiCardPaymentRedirectForm(app);
  }

  @Step
  private GarantiCardPaymentDetailsForm garantiCardPaymentDetails() {
    return new GarantiCardPaymentDetailsForm(app);
  }

}
