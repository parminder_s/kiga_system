package com.kiga.webtest.pages.widgets.common;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.visible;
import static com.kiga.webtest.core.angular.AngularHelpers.$;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class Breadcrumbs extends BasePage {
  private final SelenideElement container = $(".breadcrumbs-container");

  public Breadcrumbs(App app) {
    super(app);
  }

  @Step
  public Breadcrumbs selectIdeas() {
    this.container.shouldBe(visible);
    this.container.find(".breadcrumb-cat.first").click();
    return this;
  }

  @Step
  public void assertBreadcrumbAmount(int expectedSize) {
    Selenide.$$(".breadcrumbs-cat-block").shouldHave(size(expectedSize));
  }
}
