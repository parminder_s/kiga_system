package com.kiga.webtest.pages.steps.common;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by johnbunky on 10.05.18.
 */
public class Cookies extends BasePage {
  public Cookies(App app){super(app);}

  private static final SelenideElement cookiesAlert = $(".cookie-info-close-button");

  @Step
  public void acceptCookiesInfo(){
    if (cookiesAlert.isDisplayed()){ cookiesAlert.click();}
  }
  @Step
  public void  assertCookiesMessageIs(Condition condition){
    cookiesAlert.waitUntil(Condition.visible, 30000).shouldBe(condition);
  }
}
