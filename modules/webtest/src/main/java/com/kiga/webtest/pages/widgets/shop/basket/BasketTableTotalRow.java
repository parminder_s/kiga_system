package com.kiga.webtest.pages.widgets.shop.basket;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.ShopData;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.junit.Assert.assertEquals;

public class BasketTableTotalRow extends BasePage {

  private final SelenideElement container;

  private final BasketTableSumAndCurrencyCell sum;

  public BasketTableTotalRow(App app, SelenideElement parent) {
    super(app);
    this.container = parent.find(".product-table-result");
    this.sum = new BasketTableSumAndCurrencyCell(
      app,
      this.container.find(".currency-left").parent()
    );
  }

  @Step
  public BasketTableTotalRow assertData(ShopData shopData, int maxIndex) {
    assertSum(
      shopData.total(maxIndex) + shopData.delivery(maxIndex).price(),
      shopData.currency()
    );
    return this;
  }

  @Step
  public BasketTableTotalRow assertSum(double sum, Currency currency) {
    this.sum.shouldEqual(sum, currency);
    return this;
  }
  @Step
  public void assertDiscountAdded(Condition visible) {
    $(".product-table-item.total.with-discount").shouldBe(visible);
  }
  @Step
  public void assertDiscountCounter() throws NumberFormatException{
    float worthWithDiscount = getWorth(1);
    float worthWitouthDiscount = getWorth(2);
    assertEquals(worthWitouthDiscount / worthWithDiscount, 1.2, 0.01);
  }
  private float getWorth(int s) {
    String worth = $(".product-table-item.total.with-discount div.currency-right span:nth-child("+ s +")")
      .getText()
      .replaceAll("\\D+","");
    return new Float(worth);
  }

  @Step
  public void assertErrorMessage(){
    $$(".list-unstyled.validation-errors.fade-in").filter(visible).first().shouldBe(exist);
  }
}
