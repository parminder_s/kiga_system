package com.kiga.webtest.datastructure.settings;

public enum IdeasSection {
  NEW_IDEAS("New Ideas"),
  ACTIVITIES("Activities"),
  RESOURCES("Resources"),
  THEMES("Themes");

  private String title;

  IdeasSection(String title) {
    this.title = title;
  }

  @Override
  public String toString() {
    return this.title;
  }
}
