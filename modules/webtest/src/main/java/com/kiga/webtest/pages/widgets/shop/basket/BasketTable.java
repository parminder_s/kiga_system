package com.kiga.webtest.pages.widgets.shop.basket;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class BasketTable extends BasePage {

  private final SelenideElement container;
  private final BasketTableRows rows;

  public BasketTable(App app, SelenideElement parent) {
    super(app);
    this.container = parent.find(".product-selection");
    this.rows = new BasketTableRows(app, container);
  }

  @Step
  public BasketTableRows rows() {
    return this.rows;
  }

  @Step
  public BasketTableRow row(int index) {
    return this.rows.get(index);
  }

  @Step
  public BasketTableDeliveryRow deliveryRow() {
    return new BasketTableDeliveryRow(app, this.container);
  }

  @Step
  public BasketTableTotalRow totalRow() {
    return new BasketTableTotalRow(app, this.container);
  }

}
