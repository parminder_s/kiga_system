package com.kiga.webtest.pages.widgets.shop.basket;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.ShopProductData;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.DropDownList;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.kiga.webtest.core.checks.Assertions.waitingIs;

public class BasketTableRow extends BasePage {

  private final SelenideElement container;

  private final ElementsCollection items;
  private final SelenideElement title;
  private final SelenideElement quantity;
  private final DropDownList quantityList;
  private final SelenideElement price;
  private final BasketTableSumAndCurrencyCell priceCell;
  private final BasketTableSumAndCurrencyCell sumCell;

  public BasketTableRow(App app, SelenideElement container) {
    super(app);
    this.container = container;
    this.items = this.container.findAll(".product-table-item");
    this.title = this.items.get(0).find(".ng-binding");
    this.quantity = this.items.get(1);
    this.quantityList = new DropDownList(
      this.quantity.find(".ui-select-toggle"),
      this.quantity.findAll(".dropdown-menu .ui-select-choices-row")
    );
    this.price = this.items.get(2);
    this.priceCell = new BasketTableSumAndCurrencyCell(app, this.price);
    this.sumCell = new BasketTableSumAndCurrencyCell(app, this.items.get(3));
  }


  public BasketTableRow shouldHaveData(ShopProductData productData, int quantity) {
    this.title.shouldHave(exactText(productData.productTitle()));
    if (waitingIs(this.quantity, visible)) {
      this.quantity.shouldHave(exactText(String.valueOf(quantity)));
    }
    if (waitingIs(this.price, visible)) {
      this.priceCell.shouldEqual(productData.price(), productData.currency());
    }
    this.sumCell.shouldEqual(
      productData.price() * quantity,
      productData.currency()
    );
    return this;
  }

  @Step
  public BasketTableRow increaseTo(int newQuantity) {
    this.quantityList.open().select(String.valueOf(newQuantity));
    return this;
  }

  @Step
  public void delete() {
    this.items.get(1).find("[ng-click='ctrl.removeItem(product.id, product.productId)']").click();
  }
}
