package com.kiga.webtest.pages.widgets.registration;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.settings.SubscriptionPaymentMethod;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.RadioGroup;
import com.kiga.webtest.pages.widgets.payment.elv.ElvPaymentDetailsForm;
import com.kiga.webtest.pages.widgets.payment.invoice.InvoiceDelivery;
import ru.yandex.qatools.allure.annotations.Step;

public class PaymentMethods extends BasePage {

  private final SelenideElement container;

  private final SelenideElement invoiceOption;

  public PaymentMethods(App app, SelenideElement container) {
    super(app);
    this.container = container;
    this.invoiceOption = container.find(".invoiceOption");
  }

  @Step
  public void selectCreditCard() {
    new RadioGroup(this.container)
      .clickOn(SubscriptionPaymentMethod.CREDIT_CARD.value());
  }

  @Step
  public void selectGarantiCard() {
    new RadioGroup(this.container)
      .clickOn(SubscriptionPaymentMethod.GARANTI_CARD.value());
  }

  @Step
  public void selectPayPal() {
    new RadioGroup(this.container)
      .clickOn(SubscriptionPaymentMethod.PAY_PAL.value());
  }

  @Step
  public ElvPaymentDetailsForm selectElv() {
    new RadioGroup(this.container)
      .clickOn(SubscriptionPaymentMethod.ELV.value());
    return new ElvPaymentDetailsForm(app);
  }

  @Step
  public InvoiceDelivery selectInvoice() {
    new RadioGroup(this.container)
      .clickOn(SubscriptionPaymentMethod.INVOICE.value());
    return new InvoiceDelivery(app, invoiceOption);
  }

  @Step
  public void assertCreditCard() {
    new RadioGroup(this.container)
      .shouldBeSelected(SubscriptionPaymentMethod.CREDIT_CARD.value());
  }

  @Step
  public void assertGarantiCard() {
    new RadioGroup(this.container)
      .shouldBeSelected(SubscriptionPaymentMethod.GARANTI_CARD.value());
  }

  @Step
  public void assertPayPal() {
    new RadioGroup(this.container)
      .shouldBeSelected(SubscriptionPaymentMethod.PAY_PAL.value());
  }

  @Step
  public void assertElv() {
    new RadioGroup(this.container)
      .shouldBeSelected(SubscriptionPaymentMethod.ELV.value());
  }

  @Step
  public void assertInvoiceViaEmail() {
    new RadioGroup(this.container)
      .shouldBeSelected(SubscriptionPaymentMethod.INVOICE.value());
    new InvoiceDelivery(app, invoiceOption).assertViaEmail();
  }

  @Step
  public void assertInvoiceViaPost() {
    new RadioGroup(this.container)
      .shouldBeSelected(SubscriptionPaymentMethod.INVOICE.value());
    new InvoiceDelivery(app, invoiceOption).assertViaPost();
  }

}
