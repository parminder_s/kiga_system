package com.kiga.webtest.pages.steps.registration;

import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Field;
import ru.yandex.qatools.allure.annotations.Step;

public class RegisterFieldsSteps extends BasePage {

  private final RegistrationSteps registration;

  public RegisterFieldsSteps(RegistrationSteps registration) {
    super(registration.app);
    this.registration = registration;
  }

  @Step
  public Field phone() {
    return field(registration.fieldsHelper().phone());
  }

  @Step
  public Field passportNumber() {
    return field(registration.fieldsHelper().passportNumber());
  }

  @Step
  public Field email() {
    return field(registration.fieldsHelper().email());
  }

  @Step
  public Field confirmEmail() {
    return field(registration.fieldsHelper().confirmEmail());
  }

  @Step
  public Field password() {
    return fieldReg(registration.fieldsHelper().password());
  }

  @Step
  public Field passwordConfirmation() {
    return fieldReg(registration.fieldsHelper().confirmPassword());
  }

  @Step
  public Field field(String attrDataTestValue) {
    return registration.paidRegistrationStep2().field(
      attrDataTestValue
    );
  }
  @Step
  public Field passwordConfirmationReg() {
    return fieldReg(registration.fieldsHelper().confirmPassword());
  }

  @Step
  public Field fieldReg(String attrDataTestValue) {
    return registration.paidRegistrationStep2().fieldReg(
      attrDataTestValue
    );
  }
}
