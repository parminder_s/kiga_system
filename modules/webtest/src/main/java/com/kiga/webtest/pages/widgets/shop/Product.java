package com.kiga.webtest.pages.widgets.shop;

import static com.codeborne.selenide.Condition.*;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.helpers.AdditionalSelenideAPI.scrollWithOffsetOn;
import static com.kiga.webtest.core.helpers.LocaleHelpers.decimalFormat;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.ShopProductData;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import com.kiga.webtest.pages.widgets.basic.DropDownList;
import com.kiga.webtest.pages.widgets.shop.basket.Basket;
import ru.yandex.qatools.allure.annotations.Step;

public class Product extends BasePage {

  private final SelenideElement container = $(".product-container").waitUntil(visible, 60000);

  private final Buttons basketButtons = new Buttons(container, "[ng-click='$ctrl.addToBasket()']");

  private final SelenideElement addToBasketButton =
      basketButtons.button(language.term("shop.product.putintobasket"));
  private final DropDownList quantityList =
      new DropDownList(
          this.container.find(".pcs-select .ui-select-toggle"),
          this.container.findAll(".pcs-select .ui-select-choices-row-inner"));

  private final SelenideElement title = this.container.find("h1");
  private final SelenideElement priceBlock =
      this.container.find(".metadata-row-normal .price-block");

  public Product(App app) {
    super(app);
  }

  @Step
  public Product addToBasketShouldBeAvailable() {
    this.addToBasketButton.shouldBe(enabled);
    return this;
  }

  @Step
  public Basket addToBasket() {
    // without scroll error for firefox in Xvfb run - header shows over element
    scrollWithOffsetOn(this.addToBasketButton, 0, -150);

    this.addToBasketButton.click();
    return new Basket(app);
  }

  @Step
  public Basket addToBasket(int quantity) {
    // without scroll error for firefox in Xvfb run - header shows over element
    scrollWithOffsetOn(this.addToBasketButton, 0, -150);

    this.quantityList.open().select(String.valueOf(quantity));
    return addToBasket();
  }

  @Step
  public Product addToBasketShouldNotBeAvailable() {
    addToBasketButton.shouldNot(exist);
    return this;
  }

  public Product assertShopData(ShopProductData shopProductData) {
    this.title.shouldHave(exactText(shopProductData.title()));
    this.priceBlock.should(
        matchesText(
            shopProductData.currency()
                + " "
                + decimalFormat(language, shopProductData.price(), "#.00").replace(",", ".")
                + "*"));
    return this;
  }
}
