package com.kiga.webtest.pages.widgets.basic;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$;

public class Buttons {

  private final SelenideElement container;
  private final ElementsCollection items;

  public Buttons(SelenideElement container, String itemsCssSelector) {
    this.container = container;
    this.items = this.container.findAll(itemsCssSelector);
  }

  public Buttons(SelenideElement container) {
    this(container, ".buttons button");
  }

  public void clickBy(Condition condition) {
    button(condition).click();
  }

  public void click(String buttonText) {
    button(buttonText).click();
  }

  public SelenideElement button(Condition condition) {
    return this.items.find(condition);
  }

  public SelenideElement button(String buttonText) {
    return button(exactText(buttonText));
  }

  public SelenideElement button(ElementsCollection button){ return button.filter(visible).first();}
}
