package com.kiga.webtest.pages.endpoints;

import com.kiga.webtest.config.KigaConfiguration;
import com.kiga.webtest.core.checks.Assertions;
import com.kiga.webtest.datastructure.Country;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.open;
import static com.kiga.webtest.core.angular.AngularHelpers.$;

public class CountryEndPoint {

  @Step
  public void set(Country country) {
    open(KigaConfiguration.appBaseUrl + "/test/setCountry?code=" + country.isoCode());
  }

  @Step
  public String getIsoCode() {
    open(KigaConfiguration.appBaseUrl + "/endpoint/main/getCountry");
    Assertions.urlToBe(KigaConfiguration.appBaseUrl + "/endpoint/main/getCountry");
    return $("body").getText();
  }

}
