package com.kiga.webtest.pages.endpoints;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.SessionData;
import com.kiga.webtest.pages.helpers.BrowserHelper;
import org.openqa.selenium.Cookie;
import ru.yandex.qatools.allure.annotations.Step;

public class EndPointsManager {

  @Step
  public CountryEndPoint country() {
    return new CountryEndPoint();
  }

  @Step
  public QuickRegisterEndPoint quickRegisterFor(CustomerData customerData) {
    return new QuickRegisterEndPoint(customerData);
  }

  @Step
  public MemberInfoEndPoint memberInfo() {
    return new MemberInfoEndPoint();
  }

  @Step
  public SecurityEndPoint security() {
    return new SecurityEndPoint();
  }

  @Step
  public BrowserHelper browserHelper() {
    return new BrowserHelper();
  }

  @Step
  public ShopEndPoint shop() {
    return new ShopEndPoint();
  }

  @Step
  public void prepareShopFor(CustomerData customerData) {
    browserHelper().deleteAllCookies();
    country().set(customerData.country());
    quickRegisterFor(customerData).registerAndGetEmail();
    shop().emptyBasket();
  }

  @Step
  public SessionData createNewSession(CustomerData customerData) {
    String email = quickRegisterFor(customerData).registerAndGetEmail();
    int userId = memberInfo().identifyUserId();
    Cookie session = getWebDriver().manage().getCookieNamed("PHPSESSID");
    String sessionID = session.getValue();
    return new SessionData(sessionID, userId, email);
  }
}
