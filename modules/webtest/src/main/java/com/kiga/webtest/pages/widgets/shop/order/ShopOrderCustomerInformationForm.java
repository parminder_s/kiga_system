package com.kiga.webtest.pages.widgets.shop.order;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.helpers.RegistrationFieldsHelper;
import com.kiga.webtest.pages.helpers.basic.DataForGroup;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import com.kiga.webtest.pages.widgets.basic.TextFieldGroupReg;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;

public class ShopOrderCustomerInformationForm extends BasePage {

  private final SelenideElement container = $(".panel");
  private final SelenideElement phone = this.container.find(By.name("ctrl.data.customer.phoneNr"));
  private final SelenideElement firstname = $("[name='firstname']");
  private final Buttons buttons = new Buttons(this.container);

  public ShopOrderCustomerInformationForm(App app) {
    super(app);
  }

  @Step
  public ShopOrderCustomerInformationForm changeData(CustomerData customerData) {
    this.firstname.setValue(customerData.customer().commonInformation().firstName());
    return this;
  }

  @Step
  public ShopOrderCustomerInformationForm fillAddress(CustomerData customerData){
    DataForGroup addressData = fieldsHelper(customerData).getDataForAddressInformation();

    new TextFieldGroupReg(this.container,
      addressData.attrDataTestValues()
    ).fill(addressData.texts());
    return this;
  }
  public RegistrationFieldsHelper fieldsHelper(CustomerData customerData) {
    return new RegistrationFieldsHelper(customerData);
  }
  @Step
  public ShopOrderCustomerInformationForm fillPhone(String phone) {
    this.phone.setValue(phone);
    return this;
  }

  @Step
  public ShopOrderPaymentMethodForm next() {
    this.buttons.click(language.term("next"));
    return new ShopOrderPaymentMethodForm(app);
  }

}
