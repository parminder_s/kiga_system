package com.kiga.webtest.pages.steps.registration;

import com.kiga.webtest.pages.pageconfigs.WithCustomerDataPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.kiga.webtest.core.locators.Locators.dt;
import static com.kiga.webtest.core.locators.Locators.dtReg;

public class RegisterValidationSteps extends WithCustomerDataPage {

  private final RegistrationSteps registration;

  public RegisterValidationSteps(RegistrationSteps registration) {
    super(registration.app, registration.customerData);
    this.registration = registration;
  }

  @Step
  public RegisterValidationSteps assertPassportNumberIsMandatory() {
    return assertMandatoryField(registration.fieldsHelper().passportNumber());
  }

  @Step
  public RegisterValidationSteps assertPhoneIsMandatory() {
    return assertMandatoryField(registration.fieldsHelper().phone());
  }

  @Step
  public RegisterValidationSteps assertDayInBirthDateIsMandatory() {
    return assertMandatoryField(By.name("birthday"));
  }

  @Step
  public RegisterValidationSteps assertEmailIsUsed() {
    return assertValidationReg(
      registration.fieldsHelper().email(),
      "validation.emailalreadyused"
    );
  }

  @Step
  public RegisterValidationSteps assertPasswordDoNotMatch() {
    return assertConfirmationDoNotMatch(
      "passwords",
      registration.fieldsHelper().confirmPassword()
    );
  }

  @Step
  public RegisterValidationSteps assertPasswordIsTooShort() {
    return assertValidationReg(
      registration.fieldsHelper().password(),
      "validation.passwordistooshort"
    );
  }

  @Step
  public RegisterValidationSteps assertPasswordConfirmationIsTooShort() {
    return assertValidationReg(
      registration.fieldsHelper().confirmPassword(),
      "validation.passwords.donotmatch"
    );
  }

  @Step
  public RegisterValidationSteps assertMandatoryField(String attrDataTestValue) {
    return assertMandatoryField(dt(attrDataTestValue));
  }

  @Step
  public RegisterValidationSteps assertMandatoryFieldReg(String attrDataTestValue) {
    return assertMandatoryField(dtReg(attrDataTestValue));
  }

  @Step
  public RegisterValidationSteps assertConfirmationDoNotMatch(String parameters, String attrDataTestValue) {
    return assertValidationReg(
      attrDataTestValue,
      "validation." + parameters + ".donotmatch"
    );
  }

  //**********************Steps implementation************

  @Step
  private RegisterValidationSteps assertValidation(String attrDataTestValue, String term) {
    return assertValidation(dt(attrDataTestValue), term);
  }

  @Step
  private RegisterValidationSteps assertValidationReg(String attrDataTestValue, String term) {
    return assertValidation(dtReg(attrDataTestValue), term);
  }
  @Step
  private RegisterValidationSteps assertValidation(By locator, String term) {
    registration.paidRegistrationStep2()
      .field(locator)
      .validation()
      .should(exactText(language.term(term)));
    return this;
  }

  @Step
  public RegisterValidationSteps assertMandatoryField(By locator) {
    return assertValidation(locator, "validation.mandatoryfield");
  }

}
