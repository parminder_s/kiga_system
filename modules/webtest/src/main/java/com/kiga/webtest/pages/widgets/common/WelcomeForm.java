package com.kiga.webtest.pages.widgets.common;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.codeborne.selenide.Condition.exactText;

public class WelcomeForm extends BasePage {

  private final SelenideElement container = $(".panel");

  public WelcomeForm(App app) {
    super(app);
  }

  @Step
  public WelcomeForm assertSuccess() {
    this.container.find(".panel-heading").shouldHave(exactText("Welcome"));
    return this;
  }

  @Step
  public void goToHomePage() {
    new Buttons(this.container).click("Continue to homepage");
  }

  @Step
  public WelcomeForm assertSuccessShop() {
    this.container.find(".panel-heading").shouldHave(exactText("Herzlich Willkommen"));
    return this;
  }

  @Step
  public void goToHomePageShop() {
    new Buttons(this.container).click("Zur Startseite");
  }

}
