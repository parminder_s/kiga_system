package com.kiga.webtest.pages.widgets.planner;

import static com.codeborne.selenide.Condition.exist;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.checks.Assertions.waitingIs;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class MonthPlan extends BasePage {

  private final String weekLocator = ".desc.empty";//".lightGreen"; // ".kigaPlan";
  private final SelenideElement week = $(weekLocator);
  private final SelenideElement container = $(".planner .kiga-plan");
  private final ElementsCollection weeks = this.container.findAll(weekLocator);
  private final PlanSection planSection = new PlanSection(app, this.container);

  public MonthPlan(App app) {
    super(app);
  }

  @Step
  public MonthPlan prevPeriod() {
    this.planSection.prevPeriod();
    return this;
  }

  @Step
  public MonthPlan prevPeriodWithPlans() {
    do {
      this.planSection.prevPeriod();
    } while (waitingIs(this.week, exist));
    return this;
  }

  @Step
  public MonthPlan nextPeriod() {
    this.planSection.nextPeriod();
    return this;
  }

  @Step
  public AddPlanForm selectWeek(int index) {
    this.weeks.get(index).click();
    return new AddPlanForm(app);
  }
}
