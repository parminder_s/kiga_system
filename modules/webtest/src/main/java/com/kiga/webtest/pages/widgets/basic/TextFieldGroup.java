package com.kiga.webtest.pages.widgets.basic;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.exactValue;
import static com.codeborne.selenide.impl.Events.events;
import static com.kiga.webtest.core.locators.Locators.oneOfDt;

public class TextFieldGroup {

  private final SelenideElement container;
  private final String[] attrDataTestValues;
  private final ElementsCollection items;

  public TextFieldGroup(SelenideElement container, ElementsCollection items, String... attrDataTestValues) {
    this.container = container;
    this.attrDataTestValues = attrDataTestValues;
    this.items = items;
  }


  public TextFieldGroup(SelenideElement container, String... attrDataTestValues) {
    this(
      container,
      container.findAll(oneOfDt(attrDataTestValues)),
      attrDataTestValues
    );
  }

  public void fill(String... texts) {
    fillAndFireEvents("", "", texts);
  }

  public void fillAndBlur(String... texts) {
    fillAndFireEvents("", "blur", texts);
  }

  private void fillAndFireEvents(String beforeEvent, String afterEvent, String[] texts) {
    //items.shouldHave(CollectionCondition.sizeGreaterThanOrEqual(texts.length));
    for (int i = 0; i < Math.min(texts.length, items.size()); i++) {
      if (! beforeEvent.isEmpty()) {
        events.fireEvent(items.get(i), beforeEvent);
      }
      items.get(i).click();
      items.get(i).setValue(texts[i]);
      if (! afterEvent.isEmpty()) {
        events.fireEvent(items.get(i), afterEvent);
      }
    }
  }

  public void shouldHaveValues(String... texts) {
    items.shouldHave(CollectionCondition.size(texts.length));
    for (int i = 0; i < texts.length; i++) {
      items.get(i).shouldHave(exactValue(texts[i]));
    }
  }
}
