package com.kiga.webtest.pages.widgets.ideas;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.kiga.webtest.core.checks.CustomConditions.exactTitle;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class IdeasCategories extends BasePage {

  private final SelenideElement container;
  private final ElementsCollection categories;
  private final String ideasSelector;

  public IdeasCategories(
      App app, SelenideElement container, String categoriesSelector, String ideasSelector) {

    super(app);
    this.container = container;
    this.categories = this.container.findAll(categoriesSelector);
    this.ideasSelector = ideasSelector;
  }

  @Step
  public IdeasCategoryOverview category(int index) {
    return new IdeasCategoryOverview(app, categories.get(index), ideasSelector);
  }

  @Step
  public IdeasCategoryOverview category(String title) {
    return new IdeasCategoryOverview(
        app,
        categories.find(exactTitle(IdeasCategoryOverview.headerSelector, title)),
        ideasSelector);
  }

  @Step
  public void assertGridMode() {
    String gridModeSelector = ".grid-container";
    $(gridModeSelector).should(exist);
  }

  @Step
  public void assertRowMode() {
    String rowModeSelector = ".type-row";
    $(rowModeSelector).should(exist);
  }
}
