package com.kiga.webtest.pages.helpers.basic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataForGroup {

  private List<String> attrDataTestValues;
  private List<String> texts;

  public DataForGroup(String[] attrDataTestValues, String[] texts) {
    this.attrDataTestValues = new ArrayList<>();
    this.attrDataTestValues.addAll(Arrays.asList(attrDataTestValues));
    this.texts = new ArrayList<>();
    this.texts.addAll(Arrays.asList(texts));
  }

  public DataForGroup() {
    this(new String[0], new String[0]);
  }

  public DataForGroup add(String attrDataTestValue, String text) {
    this.attrDataTestValues.add(attrDataTestValue);
    this.texts.add(text);
    return this;
  }

  public DataForGroup addIf(boolean isTrue, String attrDataTestValue, String text) {
    if (isTrue) {
      this.add(attrDataTestValue, text);
    }
    return this;
  }

  public DataForGroup addIfDefined(String attrDataTestValue, String text) {
    return addIf(!attrDataTestValue.isEmpty(), attrDataTestValue, text);
  }

  public DataForGroup addIfNotEmpty(String attrDataTestValue, String text) {
    return addIf(!text.isEmpty(), attrDataTestValue, text);
  }

  public String[] attrDataTestValues() {
    return attrDataTestValues.toArray(new String[0]);
  }

  public String[] texts() {
    return texts.toArray(new String[0]);
  }

  @Override
  public String toString() {
    return "fields: " + this.attrDataTestValues +
      "texts: " + this.texts;
  }
}
