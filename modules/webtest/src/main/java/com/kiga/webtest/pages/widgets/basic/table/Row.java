package com.kiga.webtest.pages.widgets.basic.table;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

public class Row {

  private final SelenideElement container;

  public Row(SelenideElement container) {
    this.container = container;
  }

  public ElementsCollection cells() {
    return this.container.findAll("td");
  }

  public SelenideElement cell(int index) {
    return cells().get(index);
  }

}
