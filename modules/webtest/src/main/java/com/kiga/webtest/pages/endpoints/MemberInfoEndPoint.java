package com.kiga.webtest.pages.endpoints;

import static com.codeborne.selenide.Condition.matchText;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.getElement;
import static com.codeborne.selenide.Selenide.open;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.angular.AngularHelpers.$$;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.webtest.config.KigaConfiguration;
import com.kiga.webtest.pages.endpoints.response.ErrorCodeResponse;
import com.kiga.webtest.pages.endpoints.response.IdentifierResponse;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by rainerh on 14.04.17.
 */
public class MemberInfoEndPoint {

  @Step
  public int identifyUserId() {
    IdentifierResponse identifierResponse = getValue(IdentifierResponse.class);
    return (identifierResponse == null) ? 0 : identifierResponse.getId();
  }

  @Step
  public String identifyErrorCode() {
    ErrorCodeResponse errorCodeResponse = getValue(ErrorCodeResponse.class);
    return (errorCodeResponse == null) ? "" : errorCodeResponse.getErrorCode();
  }

  private <T> T getValue(Class<T> valueType) {
    open(KigaConfiguration.appBaseUrl + "/endpoint/test/memberInfo");
    String text = $("body").shouldBe(visible).getText();
//    for firefox & geckodriver
//    $(By.linkText("Raw Data")).click();
//    String text = $(".data").shouldBe(visible).getText();
    if ("No user logged in".equalsIgnoreCase(text)) {
      return null;
    } else {
      try {
        return new ObjectMapper().readValue(text, valueType);
      } catch (IOException ioException) {
        return null;
      }
    }
  }

}
