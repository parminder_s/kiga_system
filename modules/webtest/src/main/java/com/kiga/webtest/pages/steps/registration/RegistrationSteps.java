package com.kiga.webtest.pages.steps.registration;

import com.kiga.webtest.datastructure.*;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.helpers.RegistrationFieldsHelper;
import com.kiga.webtest.pages.helpers.basic.DataForGroup;
import com.kiga.webtest.pages.pageconfigs.WithCustomerDataPage;
import com.kiga.webtest.pages.widgets.basic.Field;
import com.kiga.webtest.pages.widgets.registration.*;
import ru.yandex.qatools.allure.annotations.Step;

public class RegistrationSteps extends WithCustomerDataPage {

  public RegistrationSteps(App app, CustomerData customerData) {
    super(app, customerData);
  }

  @Step
  public RegistrationSteps fillCustomerInformation() {
    DataForGroup commonData = fieldsHelper().getDataForCommonInformation();
    DataForGroup addressData = fieldsHelper().getDataForAddressInformation();
    if (!customerData.subscriptionType().equals(SubscriptionType.FREE)) {
      paidRegistrationStep2()
        .fillCommonInformation(
          customerData.customer().commonInformation().gender(),
          commonData
        )
        .fillBirthDate(customerData.birthDate())
        .fillAddressInformation(addressData);
    } else {
      freeRegisterForm()
        .fillCommonInformation(
          customerData.customer().commonInformation().gender(),
          commonData
        )
        .fillJob(customerData.job())
        .acceptTermsConditionsAndSecurity()
        .register();
    }
    return this;
  }
  @Step
  public RegistrationSteps fillCustomerInformationReg() {
    DataForGroup commonData = fieldsHelper().getDataForCommonInformation();
    DataForGroup addressData = fieldsHelper().getDataForAddressInformation();
    if (!customerData.subscriptionType().equals(SubscriptionType.FREE)) {
      paidRegistrationStep2()
        .fillCommonInformationReg(
          customerData.customer().commonInformation().gender(),
          commonData
        )
        .fillBirthDate(customerData.birthDate())
        .fillAddressInformationReg(addressData);
    } else {
      freeRegisterForm()
        .fillCommonInformation(
          customerData.customer().commonInformation().gender(),
          commonData
        )
        .fillJob(customerData.job())
        .acceptTermsConditionsAndSecurity()
        .register();
    }
    return this;
  }
  @Step
  public RegistrationSteps registrationShop(){
    DataForGroup commonData = fieldsHelper().getDataForCommonInformation();
    DataForGroup addressData = fieldsHelper().getDataForAddressInformation();

    shopRegistration()
      .fillCommonInformationShop(
        customerData.customer().commonInformation().gender(),
        commonData
      )
      .fillBirthDateShop(1, 1, 1981)
      .fillAddressInformationShop(addressData)
      .fillJob()
      .acceptTermsConditionsAndSecurity()
      .register();
    return this;
  }
  @Step
  public RegistrationSteps assertCustomerInformation() {
    DataForGroup commonData = fieldsHelper().getDataForCommonInformation();
    DataForGroup addressData = fieldsHelper().getDataForAddressInformation();
    if (!customerData.subscriptionType().equals(SubscriptionType.FREE)) {
      paidRegistrationStep2()
        .assertCommonInformationReg(
          customerData.customer().commonInformation().gender(),
          commonData
        )
        .assertBirthDate(customerData.birthDate())
        .assertAddressInformation(addressData);
    } else {
      freeRegisterForm()
        .assertCommonInformation(
          customerData.customer().commonInformation().gender(),
          commonData
        )
        .assertJob(language, customerData.job());
    }
    return this;
  }

  @Step
  public RegistrationSummarySteps summary() {
    return new RegistrationSummarySteps(app, customerData);
  }

  @Step
  public RegistrationSelectPaymentSteps selectPayment() {
    return new RegistrationSelectPaymentSteps(this);
  }

  @Step
  public RegistrationSteps fillBirthDate(int day, int month, int year) {
    paidRegistrationStep2().fillBirthDate(day, month, year);
    return this;
  }

  @Step
  public RegistrationSteps submitCustomerInformation() {
    paidRegistrationStep2().next();
    return this;
  }

  @Step
  public RegistrationSteps assertWarning(String message) {
    paidRegistrationStep3().assertWarning(message);
    return this;
  }

  @Step
  public RegistrationSteps submit() {
    paidRegistrationStep3().next();
    return this;
  }

  @Step
  public PaymentMethods paymentMethods() {
    return paidRegistrationStep3().paymentMethods();
  }

  @Step
  public RegistrationFieldsHelper fieldsHelper() {
    return new RegistrationFieldsHelper(customerData);
  }

  @Step
  public RegisterValidationSteps validation() {
    return new RegisterValidationSteps(this);
  }

  @Step
  public RegisterFieldsSteps fields() {
    return new RegisterFieldsSteps(this);
  }

  @Step
  public Field field(String attrDataTestValue) {
    return fields().field(attrDataTestValue);
  }

  @Step
  public Field fieldReg(String attrDataTestValue) {
    return fields().fieldReg(attrDataTestValue);
  }

  @Step
  public void fillData() {
    app.ensureLoggedOut();
    app.navigation().visitSubscription();
    app.subscriptionFor(customerData).start();
    app.registrationFor(customerData)
      .fillCustomerInformation()
      .submitCustomerInformation()
      .selectPayment().invoiceViaEmail().accept().next();
    app.registrationFor(customerData).summary()
      .assertRegistrationData("Pay with invoice");
  }
  @Step
  public void fillDataReg() {
    app.ensureLoggedOut();
    app.navigation().visitSubscription();
    app.subscriptionFor(customerData).start();
    app.cookies().acceptCookiesInfo();
    app.registrationFor(customerData)
      .fillCustomerInformationReg()
      .submitCustomerInformation()
      .selectPayment().invoiceViaEmail().accept().next();
    app.registrationFor(customerData).summary()
      .assertRegistrationData("Pay with invoice");
  }
  //**********************Steps implementation************

  @Step
  private FreeRegistrationForm freeRegisterForm() {
    return new FreeRegistrationForm(app);
  }

  @Step
  public PaidRegistrationStep2Form paidRegistrationStep2() {
    return new PaidRegistrationStep2Form(app);
  }

  @Step
  public PaidRegistrationStep3Form paidRegistrationStep3() {
    return new PaidRegistrationStep3Form(app);
  }

  @Step
  public  ShopRegistrationForm shopRegistration(){ return new ShopRegistrationForm(app); }

}
