package com.kiga.webtest.pages.widgets.payment.invoice;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.RadioGroup;
import ru.yandex.qatools.allure.annotations.Step;

public class InvoiceDelivery extends BasePage {

  private final SelenideElement container;

  private final String emailAttrDataTestValue = "email_customer";
  private final String postAttrDataTestValue = "post_customer";

  public InvoiceDelivery(App app, SelenideElement container) {
    super(app);
    this.container = container;
  }

  @Step
  public void viaEmail() {
    new RadioGroup(this.container).clickOn(emailAttrDataTestValue);
  }

  @Step
  public void assertViaEmail() {
    new RadioGroup(this.container).shouldBeSelected(emailAttrDataTestValue);
  }

  @Step
  public void viaPost() {
    new RadioGroup(this.container).clickOn(postAttrDataTestValue);
  }

  @Step
  public void assertViaPost() {
    new RadioGroup(this.container).shouldBeSelected(postAttrDataTestValue);
  }

}
