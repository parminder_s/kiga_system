package com.kiga.webtest.pages.steps.common;

import com.codeborne.selenide.Selenide;
import com.kiga.webtest.config.Language;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.common.LoginPage;
import com.kiga.webtest.pages.widgets.common.topbar.NavBarLeft;
import com.kiga.webtest.pages.widgets.common.topbar.TopBar;
import com.kiga.webtest.pages.widgets.common.topbar.BasketButton;
import com.kiga.webtest.pages.widgets.planner.Planner;
import com.kiga.webtest.pages.widgets.shop.Products;
import ru.yandex.qatools.allure.annotations.Step;

public class TopBarSteps extends BasePage {

  public TopBarSteps(App app) {
    super(app);
  }

  @Step
  public TopBarSteps selectLanguage(Language language) {
    topBar().selectLanguage(language);
    return this;
  }

  @Step
  public void assertLoggedIn() {
    topBar().assertLoggedIn();
  }

  @Step
  public void assertLoggedOut() {
    topBar().assertLoggedOut();
  }

  @Step
  public TopBarSteps startSubscribe() {
    topBar().startLogin().startSubscribe();
    return this;
  }

  @Step
  public TopBarSteps login(String email, String password) {
    topBar().startLogin().login(email, password);
    return this;
  }

  @Step
  public LoginPage login(){
    topBar().startLogin();
    return new LoginPage(app);
  }

  @Step
  public TopBarSteps logout() {
    topBar().logout();
    return this;
  }

  @Step
  public TopBarSteps ensureLoggedOut() {
    topBar().ensureLoggedOut();
    return this;
  }

  @Step
  public NavBarLeft navBarLeft() {
    return topBar().navBarLeft();
  }

  @Step
  public Products startShop() {
    navBarLeft().clickOn("Shop");
    return new Products(app);
  }

  @Step
  public void startIdeas() {
    navBarLeft().clickOn("ideas");
  }

  @Step
  public void ageRange(int index){
    topBar().ageRange(index);
  }

  @Step
  public void startFavourites() {
    navBarLeft().clickOn("Favorites");
  }

  @Step
  public Planner startPlanner() {
    navBarLeft().clickOn("Planner");
    return new Planner(app);
  }

  @Step
  public BasketButton basketButton() {
    return topBar().basket();
  }

  //**********************Steps implementation*************************

  @Step
  private TopBar topBar() {
    return new TopBar(app);
  }

}
