package com.kiga.webtest.pages.widgets.common;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.Country;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.DropDownList;
import com.kiga.webtest.pages.widgets.registration.FreeRegistrationForm;
import com.kiga.webtest.pages.widgets.registration.PaidRegistrationStep1Form;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.locators.Locators.dt;


public class SubscriptionForm extends BasePage {

  private final SelenideElement container = $(".registration");

  private final SelenideElement country = this.container.find(dt("country"));

  public SubscriptionForm(App app) {
    super(app);
  }

  private final ProductGroup productGroup(String title) {
    return new ProductGroup(app, this.container, title);
  }

  @Step
  public FreeRegistrationForm subscribeKiGaFree() {
    productGroup("KiGaFree").subscribe();
    return new FreeRegistrationForm(app);
  }

  @Step
  public PaidRegistrationStep1Form as(SubscriptionType subscriptionType) {
    productGroup(subscriptionType.title(language)).subscribe();
    return new PaidRegistrationStep1Form(app);
  }

  @Step
  public SubscriptionForm selectCountry(String title) {
    new DropDownList(country, ".ui-select-choices-row-inner")
      .open()
      .select(title);
    return this;
  }

  @Step
  public SubscriptionForm selectCountry(Country country) {
    return selectCountry(country.title());
  }

  @Step
  public void assertCountry(Country selectedCountry) {
    country.shouldHave(Condition.exactText(selectedCountry.title()));
  }
}
