package com.kiga.webtest.pages.widgets.payment.card.sofort;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.core.checks.Assertions;
import com.kiga.webtest.datastructure.Country;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import com.kiga.webtest.pages.widgets.basic.DropDownList;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.helpers.LocaleHelpers.decimalFormat;

public class SofortPaymentStep2Form extends BasePage {

  private final SelenideElement container =
    $("#SelectCountryPage .main");
  private final Buttons buttons =
    new Buttons(this.container, ".form-actions button");
  private final SelenideElement country =
    this.container.find("#MultipaysSessionSenderCountryId").parent();

  private final SelenideElement countryD =
    this.container.find("#MultipaysSessionSenderCountryId");
  private final DropDownList countryList = new DropDownList(
    this.country,
    this.country.findAll("option")
  );

  public SofortPaymentStep2Form(App app) {
    super(app);
  }

  @Step
  public SofortPaymentStep2Form assertTotal(double total, Currency currency) {
    this.container.find(".sidebar-top .amount")
      .shouldHave(exactText(
        decimalFormat(language, total, "#.00") + " " +
          currency.symbol()
      ));
    return this;
  }

  @Step
  public SofortPaymentStep2Form assertOpened() {
    Assertions.urlExcludingRedirect(
      "https://www.sofort.com/payment/multipay/go/select_country");
    return this;
  }

  @Step
  public SofortPaymentStep2Form setCountry(Country country) {
    this.countryList.open()
      .select(language.term("shop.basket.country." + country.isoCode()));
    return this;
  }

  @Step
  public SofortPaymentStep2Form setBic(String bic) {
    SelenideElement textInput = this.container.find("#BankCodeSearch");
    textInput.parent().click(); //needed to update content of input!
    textInput.setValue(bic);

    SelenideElement searchResult = this.container
      .find("#BankSearcherResultsContent ul li a");
    searchResult.click();
    return this;
  }

  @Step
  public SofortPaymentStep3Form next() {
    buttons.click(language.term("next"));
    return new SofortPaymentStep3Form(app);
  }

}
