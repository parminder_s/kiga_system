package com.kiga.webtest.pages.widgets.payment.card.sofort;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;

public class SofortPaymentStep4Form extends BasePage {

  private final SelenideElement container = $("#SelectAccountPage .main");
  private final Buttons buttons = new Buttons(this.container, "button");

  public SofortPaymentStep4Form(App app) {
    super(app);
  }

  @Step
  public SofortPaymentStep5Form next() {
    buttons.click(language.term("next"));
    return new SofortPaymentStep5Form(app);
  }

  @Step
  public SofortPaymentStep4Form selectAccount(int index) {
    this.container.findAll(".account-selector input").get(index).click();
    return this;
  }

}
