package com.kiga.webtest.pages.widgets.common;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;

public class LoginPage extends BasePage{

  public final SelenideElement container = $(".container.login");

  public LoginPage(App app) {
    super(app);
  }

  @Step
  public LoginPage userEMail(String email){
    this.container.find("[name='email']").setValue(email);
    return this;
  }
  @Step
  public LoginPage userPassword(String password){
    this.container.find("[name='password']").setValue(password);
    return this;
  }
  @Step
  public void submit(){
    new Buttons(this.container).click("Login");
  }
}
