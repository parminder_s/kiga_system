package com.kiga.webtest.datastructure.settings;

public enum CreditCardType {

  AMEX("AMEX"), DINERS_CLUB("Diners Club"), JSB("JCB"), MASTERCARD("MasterCard"), VISA("VISA");

  private String text;

  CreditCardType(String text) {
    this.text = text;
  }

  @Override
  public String toString() {
    return this.text;
  }
}
