package com.kiga.webtest.datastructure.settings;

import com.kiga.webtest.config.Language;

public enum SubscriptionType {
  TEACHERS("standard"),
  SCHOOLS("standard-org"),
  PARENTS("parents"),
  GIFT("gift"),
  FREE("test"),
  COMMUNITY("community");

  private final String productId;

  SubscriptionType(String productId) {
    this.productId = productId;
  }

  public String productId() {
    return this.productId;
  }

  public String title(Language language) {
    return language.term("subscription." + this.name().toLowerCase());
  }

  @Override
  public String toString() {
    return this.name();
  }
}
