package com.kiga.webtest.pages.widgets.planner;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.locators.Locators.dt;

public class AddPlanForm extends BasePage {

  private final SelenideElement container = $(".add-new-plan");
  private final Buttons buttons = new Buttons(
    this.container, ".buttons div");

  public AddPlanForm(App app) {
    super(app);
  }

  @Step
  public AddPlanForm assertOpened() {
    this.container.shouldBe(visible);
    return this;
  }

  @Step
  public AddPlanForm fillTitle(String title) {
    this.container.find(dt("title")).setValue(title);
    return this;
  }

  @Step
  public WeekPlan submit() {
    buttons.click("OK");
    return new WeekPlan(app);
  }

}
