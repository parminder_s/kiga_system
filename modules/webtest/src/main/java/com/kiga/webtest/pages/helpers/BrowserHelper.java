package com.kiga.webtest.pages.helpers;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

import com.kiga.webtest.datastructure.SessionData;
import org.openqa.selenium.Cookie;
import ru.yandex.qatools.allure.annotations.Step;

public class BrowserHelper {
  @Step
  public void switchToSession(SessionData session) {
    getWebDriver().manage().addCookie(new Cookie("PHPSESSID", session.sessionID(), "/"));
  }

  @Step
  public void deleteAllCookies() {
    getWebDriver().manage().deleteAllCookies();
  }
}
