package com.kiga.webtest.pages.endpoints.response;

import lombok.Data;

import java.util.List;

/**
 * Created by rainerh on 14.04.17.
 */
@Data
public class IdentifierResponse {
  private int id;
  private String roles;

  public int getId() {
    return id;
  }

  public String getRoles() {
    return roles;
  }
}
