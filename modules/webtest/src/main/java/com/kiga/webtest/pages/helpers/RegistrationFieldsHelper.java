package com.kiga.webtest.pages.helpers;

import com.kiga.webtest.datastructure.Countries;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.settings.SubscriptionType;
import com.kiga.webtest.pages.helpers.basic.DataForGroup;

public class RegistrationFieldsHelper {

  public static RegistrationFieldsHelper registrationFields(CustomerData customerData) {
    return new RegistrationFieldsHelper(customerData);
  }

  private final CustomerData customerData;

  public RegistrationFieldsHelper(CustomerData customerData) {
    this.customerData = customerData;
  }

  public DataForGroup getDataForCommonInformation() {

    DataForGroup returner = new DataForGroup();

    returner
      .add("firstname", customerData.customer().commonInformation().firstName())
      .add("lastname", customerData.customer().commonInformation().lastName())
      .addIfDefined(passportNumber(), customerData.passportNumber())
      .addIfDefined(phone(), customerData.phone())
      .addIfDefined(email(), customerData.email())
      .addIfDefined(confirmEmail(), customerData.email())
      .addIfDefined(password(), customerData.customer().password())
      .addIfDefined(confirmPassword(), customerData.customer().password());

    return returner;

  }

  public DataForGroup getDataForAddressInformation() {
    DataForGroup returner = new DataForGroup();

    returner
      .addIf(
        customerData.subscriptionType().equals(SubscriptionType.SCHOOLS),
        "addressName1",
        customerData.customer().address().institutionName1())
      .addIf(
        customerData.subscriptionType().equals(SubscriptionType.SCHOOLS),
        "addressName2",
        customerData.customer().address().institutionName2())
      .add("street", customerData.customer().address().street())
      .add("streetNumber", customerData.customer().address().streetNumber())
      .add("floorNumber", customerData.customer().address().floorNumber())
      .add("doorNumber", customerData.customer().address().doorNumber())
      .add("zip", customerData.customer().address().zip())
      .add("city", customerData.customer().address().city());

    return returner;
  }

  public String confirmPassword() {
    if (!customerData.subscriptionType().equals(SubscriptionType.GIFT) &&
      !customerData.subscriptionType().equals(SubscriptionType.FREE)) {
      return "passwordConfirmation";
    } else if (!customerData.subscriptionType().equals(SubscriptionType.GIFT) &&
      customerData.subscriptionType().equals(SubscriptionType.FREE)) {
      return "password2";
    }
    return "";
  }

  public String password() {
    if (!customerData.subscriptionType().equals(SubscriptionType.GIFT)) {
      return "password";
    }
    return "";
  }

  public String email() {
    return "email";
  }


  public String confirmEmail() {
    if (!customerData.subscriptionType().equals(SubscriptionType.FREE)) {
      return "email2";
    }
    return "";
  }

  public String phone() {
    if (!customerData.subscriptionType().equals(SubscriptionType.FREE)) {
      return "phone";
    }
    return "";
  }

  public String passportNumber() {
    if (customerData.country().equals(Countries.TURKEY) &&
      !(customerData.subscriptionType().equals(SubscriptionType.FREE) ||
        customerData.subscriptionType().equals(SubscriptionType.GIFT))) {
      return "passportnumber";
    }
    return "";
  }

}
