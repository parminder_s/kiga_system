package com.kiga.webtest.datastructure;


public class PayPalAccount {

  private String email;
  private String password;

  public PayPalAccount(String email, String password) {
    this.email = email;
    this.password = password;
  }

  public String email() {
    return this.email;
  }

  public String password() {
    return this.password;
  }

  @Override
  public String toString() {
    return "PayPalAccount{" +
      this.email + ", " +
      this.password +
      '}';
  }
}
