package com.kiga.webtest.core.webdriverconfig;

import com.codeborne.selenide.WebDriverProvider;
import com.kiga.webtest.core.webdriverconfig.exceptions.WebDriverProviderInitialisationError;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class AppiumDriverProvider implements WebDriverProvider {

  private static DesiredCapabilities capabilities;
  private static String remote;

  public static void init(String remote, DesiredCapabilities capabilities) {
    AppiumDriverProvider.remote = remote;
    AppiumDriverProvider.capabilities = capabilities;
  }

  @Override
  public WebDriver createDriver(DesiredCapabilities capabilities) {
    try {
      String platform = String.valueOf(AppiumDriverProvider.capabilities.getCapability("platformName"));
      if ("Android".equals(platform)) {
        return new AndroidDriver(new URL(remote), AppiumDriverProvider.capabilities);
      }
      if ("iOS".equals(platform)) {
        return new IOSDriver<WebElement>(new URL(remote), AppiumDriverProvider.capabilities);
      } else {
        throw new WebDriverProviderInitialisationError("Unknown platform " + platform);
      }
    } catch (MalformedURLException e) {
      throw new WebDriverProviderInitialisationError(e);
    }
  }
}
