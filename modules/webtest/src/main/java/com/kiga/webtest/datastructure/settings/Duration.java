package com.kiga.webtest.datastructure.settings;

import com.kiga.webtest.config.Language;

public enum Duration {

  MONTH_12(12), MONTH_11(11), MONTH_6(6), MONTH_3(3), MONTH_1(1);

  public int countMonth() {
    return this.monthCount;
  }

  private int monthCount;

  Duration(int monthCount) {
    this.monthCount = monthCount;
  }

  public String title(Language language) {
    return this.monthCount + " " + language.term(monthCount == 1 ? "month" : "months");
  }

  @Override
  public String toString() {
    return this.title(Language.ENGLISH);
  }
}
