package com.kiga.webtest.config;

import java.util.List;

public class Url {

  private static String getHomeUrlFor(Language language) {
    return (language != Language.TURKISH)
        ? (KigaConfiguration.appBaseUrl + "/ng/ng6/" + language.shortValue() + "/home")
        : (KigaConfiguration.appBaseUrl + "/tr");
  }

  private static String getSubscriptionUrlFor(Language language) {
    return (KigaConfiguration.appBaseUrl
        + "/ng/"
        + language.shortValue()
        + "/registrierung/preSelection");
  }

  private static String getWelcomeUrlFor(Language language) {
    return (KigaConfiguration.appBaseUrl
        + "/ng/#!/"
        + language.shortValue()
        + "/member/welcome/test");
  }

  private static String getLoginUrlFor(Language language) {
    return (KigaConfiguration.appBaseUrl + "/ng/" + language.shortValue() + "/login");
  }

  private static String getBillsUrlFor(Language language) {
    return (KigaConfiguration.appBaseUrl
        + "/ng/#!/"
        + language.shortValue()
        + "/member/account/bills");
  }

  private static String getIdeasActivitiesFor(Language language) {
    return (KigaConfiguration.appBaseUrl
        + "/ng/ng6/"
        + language.shortValue()
        + "/ideas/activities");
  }

  private static String getShopUrl(Language language) {
    return (KigaConfiguration.appBaseUrl + "/ng/" + language.shortValue() + "/cms/shop");
  }

  public Url(Language language) {
    this.language = language;
    this.homeUrl = getHomeUrlFor(language);
    this.subscriptionUrl = getSubscriptionUrlFor(language);
    this.welcomeUrl = getWelcomeUrlFor(language);
    this.loginUrl = getLoginUrlFor(language);
    this.shopUrl = getShopUrl(language);
    this.billsUrl = getBillsUrlFor(language);
    this.ideasUrl = getIdeasActivitiesFor(language);
  }

  public String ideasUrl() {
    return this.ideasUrl;
  }

  public String ideasAndAgeRangeUrl(int ageRange) {
    return (KigaConfiguration.appBaseUrl
        + "/ng/ng6/"
        + language.shortValue()
        + "/ideas/activities?ageGroupFilter="
        + ageRange);
  }

  public String homeUrl() {
    return this.homeUrl;
  }

  public String subscriptionUrl() {
    return this.subscriptionUrl;
  }

  public String welcomeUrl() {
    return this.welcomeUrl;
  }

  public String loginUrl() {
    return this.loginUrl;
  }

  public String shopUrl() {
    return this.shopUrl;
  }

  public String billsUrl() {
    return this.billsUrl;
  }

  public String testExpiredErrorUrl() {
    return (KigaConfiguration.appBaseUrl
        + "/ng/"
        + language.shortValue()
        + "/permission/test-expired");
  }

  public String fullExpiredErrorUrl() {
    return (KigaConfiguration.appBaseUrl
        + "/ng/"
        + language.shortValue()
        + "/permission/full-expired");
  }

  public String fullRequiredErrorUrl() {
    return (KigaConfiguration.appBaseUrl
        + "/ng/"
        + language.shortValue()
        + "/permission/full-required");
  }

  public String fullUpgradeErrorUrl() {
    return (KigaConfiguration.appBaseUrl
        + "/ng/"
        + language.shortValue()
        + "/permission/full-upgrade");
  }

  public String loginOrFullUrl() {
    return (KigaConfiguration.appBaseUrl
        + "/ng/"
        + language.shortValue()
        + "/permission/login-or-full");
  }

  public String myWeekPlanPrintOverviewUrl(String planId) {
    return KigaConfiguration.appBaseUrl
        + "/api/planner/member/printOverview?planId="
        + planId
        + "&locale="
        + language.shortValue();
  }

  public String myWeekPlanPrintSelectionUrl(List<String> ideaIds) {
    return KigaConfiguration.appBaseUrl
        + "/api/planner/member/printSelection?ids="
        + String.join(",", ideaIds)
        + "&locale="
        + language.shortValue();
  }

  public String myWeekPlanPart1Url() {
    return KigaConfiguration.appBaseUrl + "/ng/" + language.shortValue() + "/plan/member/detail/";
  }

  public String srcUrl() {
    return "https://kiga.s3.amazonaws.com/";
  }

  private final Language language;
  private final String homeUrl;
  private final String subscriptionUrl;
  private final String welcomeUrl;
  private final String loginUrl;
  private final String shopUrl;
  private final String billsUrl;
  private final String ideasUrl;
}
