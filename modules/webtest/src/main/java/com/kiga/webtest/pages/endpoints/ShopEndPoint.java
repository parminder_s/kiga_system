package com.kiga.webtest.pages.endpoints;

import com.kiga.webtest.config.KigaConfiguration;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.open;

public class ShopEndPoint {

  @Step
  public void emptyBasket() {
    open(KigaConfiguration.appBaseUrl + "/test/emptyBasket");
  }
}
