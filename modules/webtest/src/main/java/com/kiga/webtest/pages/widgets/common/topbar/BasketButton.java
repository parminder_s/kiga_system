package com.kiga.webtest.pages.widgets.common.topbar;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.shop.basket.Basket;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$;

public class BasketButton extends BasePage {

  private final SelenideElement container;

  public BasketButton(App app, SelenideElement parent) {
    super(app);
    //this.container = parent.find("[ui-sref='root.shop.basket']");
    this.container = parent.$$(".baket-btn>a").filter(visible).first();
  }

  @Step
  public Basket click() {
    this.container.click();
    return new Basket(app);
  }

  @Step
  public void shouldHaveProductsCount(int count) {
    this.container.find(".basket-number")
      .shouldHave(exactText(String.valueOf(count)));
  }

}
