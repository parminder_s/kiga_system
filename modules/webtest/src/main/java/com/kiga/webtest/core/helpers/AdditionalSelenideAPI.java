package com.kiga.webtest.core.helpers;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.config.KigaConfiguration;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class AdditionalSelenideAPI {

  //scrollTo works not always
  //scrollWithOffset(0, -150); - this variant doesn't help in situations when something is shown under the header
  public static void scrollWithOffsetOn(SelenideElement element, int x, int y) {
    element.shouldBe(visible);
    ensureWindowIsActive();

    String code = "window.scroll(" + (element.getLocation().x + x) + "," + (element.getLocation().y + y) + ");";
    executeJavaScript(code, element, x, y);
  }

  public static void ensureWindowIsActive() {
    if (KigaConfiguration.maxParallelForks > 1) {
      getWebDriver().switchTo().window(getWebDriver().getWindowHandle());
      executeJavaScript("window.focus();");
    }
  }

}
