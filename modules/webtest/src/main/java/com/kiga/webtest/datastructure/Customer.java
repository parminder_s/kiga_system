package com.kiga.webtest.datastructure;

import com.kiga.webtest.datastructure.settings.Gender;

public class Customer {

  private CustomerCommonInformation commonInformation;
  private CustomerAddress address;
  private String password;

  public Customer(Gender gender, String firstName, String lastName,
                  String password,
                  String street, String streetNumber,
                  String floorNumber, String doorNumber,
                  String zip, String city,
                  String institutionName1, String institutionName2) {

    this.commonInformation = new CustomerCommonInformation(
      gender, firstName, lastName
    );
    this.address = new CustomerAddress(
      street, streetNumber,
      floorNumber, doorNumber,
      zip, city,
      institutionName1, institutionName2
    );
    this.password = password;

  }

  public Customer(Gender gender, String firstName, String lastName,
                  String password,
                  String street, String streetNumber,
                  String floorNumber, String doorNumber,
                  String zip, String city) {

    this(
      gender, firstName, lastName, password,
      street, streetNumber, floorNumber, doorNumber,
      zip, city,
      "", ""
    );

  }

  public CustomerCommonInformation commonInformation() {
    return this.commonInformation;
  }

  public CustomerAddress address() {
    return this.address;
  }

  public String password() {
    return this.password;
  }

  @Override
  public String toString() {
    return this.commonInformation.toString();
  }

}
