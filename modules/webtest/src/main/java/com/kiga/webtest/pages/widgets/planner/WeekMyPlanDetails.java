package com.kiga.webtest.pages.widgets.planner;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.config.Url;
import com.kiga.webtest.core.checks.Assertions;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.WebDriverRunner.url;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.locators.Locators.dt;

public class WeekMyPlanDetails extends BasePage {

  private final SelenideElement container;
  private final WeekPlan weekPlan;
  private final String urlPart1 = new Url(language).myWeekPlanPart1Url();

  public WeekMyPlanDetails(App app, WeekPlan weekPlan, SelenideElement container) {
    super(app);
    this.container = container;
    this.weekPlan = weekPlan;
  }

  @Step
  public WeekMyPlanDetails assertOpened() {
    Assertions.urlStarts(urlPart1);
    return this;
  }

  @Step
  public String getPlanID() {
    assertOpened();
    return url().replaceFirst(urlPart1, "");
  }

  public AddPlanForm addFirstPlan() {
    $(dt("addPlan")).click();
    return new AddPlanForm(app);
  }
}
