package com.kiga.webtest.pages.steps.planner;

import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.datastructure.SessionData;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.planner.*;
import ru.yandex.qatools.allure.annotations.Step;

public class PlannerSteps extends BasePage {

  public PlannerSteps(App app) {
    super(app);
  }

  @Step
  public AddPlanForm addPlanForm() {
    return new AddPlanForm(app);
  }

  @Step
  public WeekPlan weekPlan() {
    return new WeekPlan(app);
  }

  @Step
  public AddPlanForm startImportToMyPlan() {
    return app.topBar()
        .startPlanner()
        .importToMyPlans()
        // .switchToMonthPeriod()
        //.prevPeriodWithPlans()
        .selectWeek(0);
       // .kigaPlan()
        //.importToMyPlans();
  }

  @Step
  public MonthPlan startImportToMyPlans() {
    return app.topBar().startPlanner().importToMyPlans();
  }

  @Step
  public PlannerSteps importToMyPlan() {
    startImportToMyPlan();//.submit();
    return this;
  }

  @Step
  public PrintSelectionForm startPrintSelection() {
    return weekPlan().startPrintSelection();
  }

  @Step
  public String addFirstPlanAndGetId() {
    addFirstPlan();
    return weekPlan().myPlan().getPlanID();
  }

  @Step
  public SessionData prepareFor(CustomerData customerData) {
    SessionData returner = app.endPoints().createNewSession(customerData);
    app.navigation().visitHome();
    return returner;
  }

  @Step
  public WeekPlan addFirstPlan() {
    return app.topBar()
        .startPlanner()
        .switchToMyPlans()
        .switchToWeekPeriod()
        .myPlan()
        .addFirstPlan()
        .fillTitle("My First Plan")
        .submit();
  }

  @Step
  public PlannerSteps assertMyWeekPlanOpened() {
    weekPlan().myPlan().assertOpened();
    return this;
  }

  @Step
  public String getMyWeekPlanID() {
    return weekPlan().myPlan().getPlanID();
  }
}
