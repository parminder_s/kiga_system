package com.kiga.webtest.datastructure;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class KiGaDate {

  private int day;
  private int month;
  private int year;

  public KiGaDate(int day, int month, int year) {
    this.day = day;
    this.month = month;
    this.year = year;
  }

  public int day() {
    return this.day;
  }

  public int month() {
    return this.month;
  }

  public int year() {
    return this.year;
  }

  @Override
  public String toString() {
    return LocalDate.of(this.year, this.month, this.day).format(DateTimeFormatter.ofPattern("M/d/yy"));
  }
}
