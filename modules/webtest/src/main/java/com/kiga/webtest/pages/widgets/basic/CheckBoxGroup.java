package com.kiga.webtest.pages.widgets.basic;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.checked;
import static com.kiga.webtest.core.locators.Locators.*;

public class CheckBoxGroup {

  private final SelenideElement container;
  private final ElementsCollection items;

  public CheckBoxGroup(SelenideElement container, ElementsCollection items) {
    this.container = container;
    this.items = items;
  }

  public CheckBoxGroup(SelenideElement container, String... attrDataTestValues) {
    this(
      container,
      getItems(container, attrDataTestValues)
    );
  }

  private static ElementsCollection getItems(SelenideElement container, String... attrDataTestValues) {
    return container.findAll(childHasOneOfDt(attrDataTestValues));
  }

  public void toggleAll() {
    for (SelenideElement element : items) {
      element.click();
    }
  }

  public void checkAll() {
    for (SelenideElement element : items) {
      element.shouldNotBe(checked).click();
    }
  }
}
