package com.kiga.webtest.pages.widgets.ideas;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Selectors.byText;
import static com.kiga.webtest.core.angular.AngularHelpers.$;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.IdeaData;
import com.kiga.webtest.datastructure.settings.IdeasSection;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class Ideas extends BasePage {

  private final SelenideElement container =
      $("[id=\"theme-area\"]"); // $("kiga-idea-group-container");
  private final SelenideElement buttonContainer = $(".button-row");
  private final String categoriesSelector;
  private final String ideasSelector;

  public Ideas(App app, String categoriesSelector, String ideasSelector) {
    super(app);
    this.categoriesSelector = categoriesSelector;
    this.ideasSelector = ideasSelector;
  }

  @Step
  public IdeasSections sections() {
    return new IdeasSections(app, this.container, categoriesSelector, ideasSelector);
  }

  @Step
  public IdeasCategories categories() {
    return new IdeasCategories(app, this.container, categoriesSelector, ideasSelector);
  }

  @Step
  public void assertIsToFavourites(String text) {
    $(byText(text)).should(exist);
  }

  @Step
  public void assertNotToFavourites(String text) {
    $(byText(text)).should(not(exist));
  }

  @Step
  public void assertIsFavourite(IdeaData ideaData) {
    categories().category(ideaData.categoryTitle()).assertIsFavourite(ideaData.ideaTitle());
  }

  @Step
  public void assertIsNotFavourite(IdeaData ideaData) {
    categories().category(ideaData.categoryTitle()).assertIsNotFavourite(ideaData.ideaTitle());
  }

  @Step
  public IdeaData selectIdea(IdeasSection ideasSection, int categoryIndex, int ideaIndex) {

    IdeasCategories categories = sections().select(ideasSection);
    IdeasCategoryOverview categoryOverview = categories.category(categoryIndex);
    IdeaOverview ideaOverview = categoryOverview.selectIdea(ideaIndex);

    return new IdeaData(
        ideasSection,
        categoryIndex,
        categoryOverview.getTitle(),
        ideaIndex,
        ideaOverview.getTitle());
  }

  @Step
  public void selectIdea(IdeaData ideaData) {
    sections()
        .select(ideaData.section())
        .category(ideaData.categoryTitle())
        .selectIdea(ideaData.ideaTitle());
  }

  @Step
  public IdeaOverview selectNewIdea(int index) {
    return selectIdea(container.findAll(".grid-container .grid").get(index));
  }

  @Step
  private IdeaOverview selectIdea(SelenideElement idea) {
    idea.click();
    return new IdeaOverview(app);
  }

  @Step
  public void selectIdea(String ideaTitle) {
    $(byText(ideaTitle)).click();
  }

  @Step
  public IdeaData addToFavourites(IdeasSection ideasSection, int categoryIndex, int ideaIndex) {

    IdeasCategories categories = sections().select(ideasSection);
    IdeasCategoryOverview categoryOverview = categories.category(categoryIndex);
    String categoryTitle = categoryOverview.getTitle();
    String ideaTitle = categoryOverview.addToFavourites(ideaIndex);

    return new IdeaData(ideasSection, categoryIndex, categoryTitle, ideaIndex, ideaTitle);
  }

  @Step
  public void removeFromFavourites(IdeaData ideaData) {
    sections()
        .select(ideaData.section())
        .category(ideaData.categoryTitle())
        .removeFromFavourites(ideaData.ideaTitle());
  }
}
