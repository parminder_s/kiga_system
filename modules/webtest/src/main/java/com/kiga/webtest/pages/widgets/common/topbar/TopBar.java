package com.kiga.webtest.pages.widgets.common.topbar;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Menu;
import com.kiga.webtest.config.Language;
import com.kiga.webtest.pages.widgets.common.LoginForm;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$$;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.checks.Assertions.waitingIs;
import static com.codeborne.selenide.Condition.visible;

public class TopBar extends BasePage {

  private final SelenideElement container = $$("#top").filter(visible).first();

  private final String accountMenuItemsLocator = "ul.dropdown-menu>li>a>kiga-menu-label>span";
  private final SelenideElement loginElement = container.find(".navbar-ctx");
  private final SelenideElement accountElement = container.find("kiga-menu");
  private final SelenideElement languageElement = container.find(".lang-selector");
  private final SelenideElement accountMenuElement = container.findAll(accountMenuItemsLocator).first();
  private final SelenideElement ageRangeDropMenu = $("#dropdownMenu1");
  private final ElementsCollection ageRangeDropMenuItems = $$("ul[aria-labelledby=\"dropdownMenu1\"] li");

  private final Menu accountMenu =
    new Menu(this.accountElement, container.findAll(accountMenuItemsLocator));
  private final Menu languageMenu =
    new Menu(this.languageElement, container.findAll(".lang_sub a"));

  public TopBar(App app) {
    super(app);
  }

  @Step
  public LoginForm startLogin() {
    this.container.shouldBe(visible);
    this.loginElement.click();
    return new LoginForm(app);
  }

  @Step
  public void ageRange(int index){
    ageRangeDropMenu.click();
    ageRangeDropMenuItems.get(index).click();
  }


  @Step
  public boolean isLoggedOut() {
    this.container.shouldBe(visible);//for exact detect difference - not logged out vs top Bar is not loaded
    return waitingIs(this.loginElement, visible);
  }

  @Step
  public TopBar logout() {
    this.container.shouldBe(visible);
    this.accountMenu.open().select("Logout");
    return this;
  }

  @Step
  public TopBar selectLanguage(Language language) {
    this.container.shouldBe(visible);
    this.languageMenu.open().select(language.description());
    return this;
  }

  @Step
  public void ensureLoggedOut() {
    if (!isLoggedOut()) {
      logout();
    }
  }

  @Step
  public void assertLoggedIn() {
    this.container.shouldBe(visible);//for exact detect difference - not logged out vs top Bar is not loaded
    this.accountMenuElement.should(exist);
  }

  @Step
  public void assertLoggedOut() {
    this.container.shouldBe(visible);//for exact detect difference - not logged out vs top Bar is not loaded
    this.accountMenuElement.shouldNot(exist);
  }

  @Step
  public NavBarLeft navBarLeft() {
    return new NavBarLeft(this.container);
  }

  @Step
  public BasketButton basket() {
    return new BasketButton(app, this.container);
  }
}
