package com.kiga.webtest.pages.steps.registration.payment;

import com.kiga.webtest.datastructure.CreditCard;
import com.kiga.webtest.datastructure.CustomerData;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.WithCustomerDataPage;
import com.kiga.webtest.pages.widgets.payment.card.creditcard.CreditCardPaymentDetailsForm;
import com.kiga.webtest.pages.widgets.payment.card.CardPaymentRedirectForm;
import com.kiga.webtest.pages.widgets.payment.card.creditcard.CreditCardVerificationSimulatorForm;
import ru.yandex.qatools.allure.annotations.Step;

public class CreditCardPaymentSteps extends WithCustomerDataPage {

  private final CreditCard creditCard;

  public CreditCardPaymentSteps(App app, CustomerData customerData, CreditCard creditCard) {
    super(app, customerData);
    this.creditCard = creditCard;
  }

  @Step
  public CreditCardPaymentSteps pay() {
    creditCardPaymentDetails()
      .assertOpened()
      .assertTotal(customerData.price(), customerData.country().currency())
      .fill(creditCard)
      .pay();
    return this;
  }

  @Step
  public CreditCardPaymentSteps payAndApprove(String durationTitle) {
    return pay().approvePayment(durationTitle);
  }

  @Step
  public CreditCardPaymentSteps payAndApprove() {
    return payAndApprove(" " + customerData.duration());
  }

  @Step
  public CreditCardPaymentSteps approvePayment(String durationTitle) {
    redirect()
      .assertHeader(durationTitle)
      .assertTotal(customerData.price(), customerData.country().currency());
      //.assertContains(creditCard.creditCardType().toString());

    redirect().next();
    creditCardVerificationSimulator().assertOpened(creditCard);

    creditCardVerificationSimulator().approveAuthentication();

    return this;
  }

  //**********************Steps implementation************

  @Step
  private CreditCardPaymentDetailsForm creditCardPaymentDetails() {
    return new CreditCardPaymentDetailsForm(app);
  }

  @Step
  private CardPaymentRedirectForm redirect() {
    return new CardPaymentRedirectForm(app);
  }

  @Step
  private CreditCardVerificationSimulatorForm creditCardVerificationSimulator() {
    return new CreditCardVerificationSimulatorForm(app);
  }

}
