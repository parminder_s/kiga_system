package com.kiga.webtest.core.properties;


import com.codeborne.selenide.Configuration;
import com.kiga.webtest.config.Language;
import com.kiga.webtest.core.webdriverconfig.AppiumDriverProvider;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertiesHelper {

  public static Properties readEnvironmentProperties() {

    String path = "environments";
    String ext = "properties";
    String env = System.getProperty("test_env", "test").toLowerCase();
    System.out.println("environment: " + env);
    Properties profileProperties = readProperties(path, ext, env);

    Properties systemProperties = System.getProperties();

    System.out.println("\n[Properties reading] ---------------------------------------------------------");

    for (Map.Entry entry : profileProperties.entrySet()) {
      String key = String.valueOf(entry.getKey());
      System.out.println(key + " = " + entry.getValue());
      if (systemProperties.containsKey(key)) {
        String value = systemProperties.getProperty(key);

        if (!value.isEmpty()) {
          profileProperties.setProperty(key, value);
          System.out.println(key + " = " + entry.getValue() + " !!! corrected");
        }
      }
    }
    System.out.println("[Properties reading] ---------------------------------------------------------\n");

    return profileProperties;
  }

  public static Properties readProperties(String path, String ext, String name) {

    Properties properties = new Properties();
    String fullName = path + "/" + name + "." + ext;

    try {
      properties.load(
        new InputStreamReader(
          PropertiesHelper.class.getClassLoader()
            .getResourceAsStream(fullName)
          , "UTF-8"
        )
      );

    } catch (IOException e) {
      System.out.println("Error : " + fullName + " is not exist");
      e.printStackTrace();
    }

    return properties;
  }

  public static void readLocalizations() {
    for (Language language : Language.values()) {
      language.setTerms(readProperties("localizations", "properties", language.shortValue()));
    }
  }

  public static void configureRemoteAndCapabilities(Properties properties) {
    String remote = properties.getProperty("test.gridUrl", "");
    PropertiesHelper.fillSystemPropertiesFromCapabilityProperties(properties);
    if (!remote.isEmpty()) {
      Configuration.remote = remote;
      if (remote.contains("browserstack")) {
        System.setProperty("capabilities.browserstack.debug", "true");
      } else if (remote.contains("saucelabs") && (properties.getProperty("capabilities.appiumVersion") != null)) {
        Configuration.remote = null;
        AppiumDriverProvider.init(remote, getCapabilities(System.getProperties()));
        Configuration.browser = AppiumDriverProvider.class.getName();
      } else {
        //for FireFox<=47.0.1 usage
        //!!!to run node - use -Dwebdriver.firefox.marionette=false in the command line
        if ("firefox".equals(Configuration.browser)) {
          System.setProperty("capabilities.marionette", "false");
        }
      }
    }
  }

  public static Map<String, String> readCapabilities(Properties properties) {
    Map<String, String> returner = new HashMap<>();

    for (Map.Entry entry : properties.entrySet()) {
      String key = String.valueOf(entry.getKey());
      if (key.startsWith("capabilities")) {
        String value = String.valueOf(entry.getValue());
        returner.put(key, value);
        System.out.println(key + " = " + value);
      }
    }
    return returner;
  }

  public static void fillSystemPropertiesFromCapabilityProperties(Properties properties) {
    Map<String, String> capabilities = readCapabilities(properties);
    for (Map.Entry entry : capabilities.entrySet()) {
      System.setProperty(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
    }
  }

  public static DesiredCapabilities getCapabilities(Properties properties) {
    DesiredCapabilities returner = new DesiredCapabilities();
    Map<String, String> capabilities = readCapabilities(properties);
    for (Map.Entry entry : capabilities.entrySet()) {
      String capabilityName = String.valueOf(entry.getKey()).replaceFirst("capabilities.", "");
      returner.setCapability(capabilityName, String.valueOf(entry.getValue()));
    }
    return returner;
  }
}
