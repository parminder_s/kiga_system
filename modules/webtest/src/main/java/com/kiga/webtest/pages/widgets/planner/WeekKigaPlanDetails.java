package com.kiga.webtest.pages.widgets.planner;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class WeekKigaPlanDetails extends BasePage {

  private final WeekPlan weekPlan;

  public WeekKigaPlanDetails(App app, WeekPlan weekPlan) {
    super(app);
    this.weekPlan = weekPlan;
  }

  @Step
  public AddPlanForm importToMyPlans() {
    weekPlan.planSection().button("copyPlan").click();
    return new AddPlanForm(app);
  }
}
