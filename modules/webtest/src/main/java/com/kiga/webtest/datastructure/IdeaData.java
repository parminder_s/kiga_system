package com.kiga.webtest.datastructure;

import com.kiga.webtest.datastructure.settings.IdeasSection;

public class IdeaData {

  private final IdeasSection section;
  private final int categoryIndex;
  private final String categoryTitle;
  private final int ideaIndex;
  private final String ideaTitle;

  public IdeaData(IdeasSection section, int categoryIndex, String categoryTitle, int ideaIndex, String ideaTitle) {
    this.section = section;
    this.categoryIndex = categoryIndex;
    this.categoryTitle = categoryTitle;
    this.ideaIndex = ideaIndex;
    this.ideaTitle = ideaTitle;
  }

  public IdeasSection section() {
    return section;
  }

  public int categoryIndex() {
    return categoryIndex;
  }

  public String categoryTitle() {
    return categoryTitle;
  }

  public int ideaIndex() {
    return ideaIndex;
  }

  public String ideaTitle() {
    return ideaTitle;
  }

  @Override
  public String toString() {
    return section + "\\ " + categoryTitle + "\\ " + ideaTitle;
  }
}
