package com.kiga.webtest.pages.widgets.planner;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.checks.CustomConditions.hRefContains;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

public class Planner extends BasePage {

  private final SelenideElement container = $(".kiga-container");
  private final Buttons plans = new Buttons(this.container, "#header .nav-tabs .sub-menu-planner");
  private final Buttons periods = new Buttons(this.container, ".main-nav .btn");

  public Planner(App app) {
    super(app);
  }

  @Step
  public Planner switchToMyPlans() {
    plans.clickBy(hRefContains("plan/member"));
    return this;
  }

  @Step
  public Planner switchToKigaPlans() {
    plans.clickBy(hRefContains("plan/kiga"));
    return this;
  }

  @Step
  public WeekPlan switchToWeekPeriod() {
    periods.click("Week");
    return new WeekPlan(app);
  }

  @Step
  public MonthPlan switchToMonthPeriod() {
    periods.click("Month");
    return new MonthPlan(app);
  }

  @Step
  public Planner switchToYearPeriod() {
    periods.click("Year");
    return this;
  }

  @Step
  public MonthPlan importToMyPlans() {
    container.$("[data-test=\"copyPlan\"]").click();
    return new MonthPlan(app);
  }
}
