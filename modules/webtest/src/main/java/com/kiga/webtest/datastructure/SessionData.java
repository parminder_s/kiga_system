package com.kiga.webtest.datastructure;

public class SessionData {
  private String sessionID;
  private int userId;
  private String email;

  public SessionData(String sessionID, int userId, String email) {
    this.sessionID = sessionID;
    this.userId = userId;
    this.email = email;
  }

  public String sessionID() {
    return sessionID;
  }

  public int userId() {
    return userId;
  }

  public String email() {
    return email;
  }

}
