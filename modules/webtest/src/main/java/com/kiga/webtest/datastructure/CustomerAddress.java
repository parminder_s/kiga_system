package com.kiga.webtest.datastructure;

public class CustomerAddress {
  private String street;
  private String streetNumber;
  private String floorNumber;
  private String doorNumber;
  private String zip;
  private String city;
  private String institutionName1;
  private String institutionName2;

  public CustomerAddress(
    String street, String streetNumber,
    String floorNumber, String doorNumber,
    String zip, String city,
    String institutionName1, String institutionName2) {

    this.street = street;
    this.streetNumber = streetNumber;
    this.floorNumber = floorNumber;
    this.doorNumber = doorNumber;
    this.zip = zip;
    this.city = city;
    this.institutionName1 = institutionName1;
    this.institutionName2 = institutionName2;
  }


  public CustomerAddress(
    String street, String streetNumber,
    String floorNumber, String doorNumber,
    String zip, String city) {

    this(street, streetNumber, floorNumber, doorNumber, zip, city, "", "");
  }

  public String street() {
    return this.street;
  }

  public String streetNumber() {
    return this.streetNumber;
  }

  public String floorNumber() {
    return this.floorNumber;
  }

  public String doorNumber() {
    return this.doorNumber;
  }

  public String zip() {
    return this.zip;
  }

  public String city() {
    return this.city;
  }

  public String institutionName1() {
    return institutionName1;
  }

  public String institutionName2() {
    return institutionName2;
  }

  @Override
  public String toString() {
    return this.institutionName1 +
      " " + this.institutionName2 +
      " " + this.street +
      ",  " + this.streetNumber +
      ",  " + this.floorNumber +
      ",  " + this.doorNumber +
      ",  " + this.zip +
      ",  " + this.city;
  }
}
