package com.kiga.webtest.pages.widgets.shop.order;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.settings.ShopPaymentMethod;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import com.kiga.webtest.pages.widgets.basic.CheckBoxGroup;
import com.kiga.webtest.pages.widgets.basic.RadioGroup;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.text;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.helpers.AdditionalSelenideAPI.scrollWithOffsetOn;
import static com.kiga.webtest.core.locators.Locators.childHasOneOfAttrValues;

public class ShopOrderPaymentMethodForm extends BasePage {

  public final SelenideElement container = $(".shop .container");
  public final Buttons buttons = new Buttons(this.container);

  public ShopOrderPaymentMethodForm(App app) {
    super(app);
  }

  @Step
  public ShopOrderPaymentMethodForm assertOpened() {
    this.container.find(".panel-heading")
      .shouldHave(exactText(language.term("shop.paymenttypes")));
    return this;
  }

  @Step
  public ShopOrderPaymentMethodForm assertWarning() {
    this.container.find(".alert-warning").shouldHave(text(
      "Die Zahlung ist fehlgeschlagen. " +
        "Bitte wählen Sie ein anderes Zahlungsmittel oder " +
        "probieren Sie es noch einmal."
    ));
    return this;
  }

  @Step
  public ShopOrderPaymentMethodForm acceptTermsConditions() {
    return accept("agb");
  }

  @Step
  public ShopOrderPaymentMethodForm acceptSecurity() {
    return accept("security");
  }

  @Step
  private ShopOrderPaymentMethodForm accept(String attrValue) {
    new CheckBoxGroup(
      this.container,
      this.container.findAll(childHasOneOfAttrValues("name", attrValue))
    ).checkAll();
    return this;
  }

  @Step
  public ShopOrderPaymentMethodForm selectPaymentMethod(ShopPaymentMethod paymentMethod) {
    //without scroll error for firefox in Xvfb run - header shows over element
    scrollWithOffsetOn(this.container, 0, -150);

    By checkBoxLocator =
      childHasOneOfAttrValues("value", paymentMethod.value());
    new RadioGroup(this.container)
      .clickOn(checkBoxLocator);
    return this;
  }

  @Step
  public void next() {
    this.buttons.click(language.term("next"));
  }

}
