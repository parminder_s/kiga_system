package com.kiga.webtest.pages.widgets.registration;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.settings.Gender;
import com.kiga.webtest.datastructure.settings.Job;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.helpers.basic.DataForGroup;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.*;
import com.kiga.webtest.pages.widgets.common.WelcomeForm;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.kiga.webtest.core.locators.Locators.childHasOneOfDtShop;

public class ShopRegistrationForm extends BasePage {

  private final SelenideElement container = $(".panel-body");
  private final SelenideElement buttonsContainer = $(".buttons");
  private final SelenideElement jobs = this.container.find("[name=\"jobs\"]");


  private final DateGroup birthDayGroup =
    new DateGroup(this.container.find(By.name("birthday")));

  public ShopRegistrationForm(App app) {
    super(app);
  }

  @Step
  public ShopRegistrationForm fillCommonInformationShop(
    Gender gender, DataForGroup dataForGroup) {

    new RadioGroup(this.container).
      clickOnGender(gender.toString());
    new TextFieldGroupReg(this.container,
      dataForGroup.attrDataTestValues()
    ).fill(dataForGroup.texts());
    return this;
  }

  @Step
  public ShopRegistrationForm fillAddressInformationShop(
    DataForGroup addressData) {

    new TextFieldGroupReg(this.container,
      addressData.attrDataTestValues()
    ).fill(addressData.texts());
    return this;
  }
  @Step
  public ShopRegistrationForm fillBirthDateShop(
    int day, int month, int year) {
    birthDayGroup.set(day, month, year);
    return this;
  }
  @Step
  public ShopRegistrationForm acceptTermsConditionsAndSecurity() {
    new CheckBoxGroup(
      this.container,
      this.container.findAll(childHasOneOfDtShop("agb", "security", "datenschutz")))
      .checkAll();
    return this;
  }
  @Step
  public ShopRegistrationForm fillJob() {
    new DropDownList(
      jobs, ".ui-select-choices-row div")
      .open().select(2);
    return this;
  }
  @Step
  public WelcomeForm register() {
    new Buttons(this.buttonsContainer).click("Registrieren");
    return new WelcomeForm(app);
  }
}
