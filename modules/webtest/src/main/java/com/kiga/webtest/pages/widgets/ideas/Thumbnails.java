package com.kiga.webtest.pages.widgets.ideas;

import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$;
import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static org.junit.Assert.assertEquals;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class Thumbnails extends BasePage {

  private final SelenideElement moreThumbnails = $("#preview-list-down-");
  private final ElementsCollection thumbnails = $$(".list-prev-sm");
  private final String smallThumbnailSelector = ".list-prev-sm.active img";
  private final String largeThumbnailSelector = ".preview.hidden-lmd img";

  public Thumbnails(App app) {
    super(app);
  }

  @Step
  public void assertThumbnailSwitches() {

    for (SelenideElement thumbnail : thumbnails) {
      if (thumbnail.is(not(visible))) {
        moreThumbnails.click();
      }
      thumbnail.click();
      String smallThumbnail = getImageAttribute(smallThumbnailSelector);
      String largeThumbnail = getImageAttribute(largeThumbnailSelector);

      assertEquals(smallThumbnail, largeThumbnail);
    }
  }

  private String getImageAttribute(String thumbnailSelector) {
    String attribute = $(thumbnailSelector).getAttribute("src");
    String[] attributePart = attribute.split("\\.");
    return attributePart[3];
  }
}
