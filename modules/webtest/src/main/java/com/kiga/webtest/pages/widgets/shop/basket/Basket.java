package com.kiga.webtest.pages.widgets.shop.basket;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import com.kiga.webtest.pages.widgets.shop.order.ShopOrderCustomerInformationForm;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;

public class Basket extends BasePage {

  private final SelenideElement container = $(".shop .container");
  private final Buttons buttons = new Buttons(container);

  public Basket(App app) {
    super(app);
  }

  @Step
  public ShopOrderCustomerInformationForm startPay() {
    buttons.click(language.term("shop.basket.pay"));
    return new ShopOrderCustomerInformationForm(app);
  }

  @Step
  public BasketTable table() {
    return new BasketTable(app, container);
  }
  @Step
  public void inputVoucher(String voucher){
    Selenide.$("input[name='voucher']").setValue(voucher).pressEnter();
  }
}
