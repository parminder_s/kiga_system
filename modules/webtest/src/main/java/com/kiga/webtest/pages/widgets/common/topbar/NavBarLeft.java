package com.kiga.webtest.pages.widgets.common.topbar;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.*;

public class NavBarLeft {

  private final SelenideElement container;
  private final ElementsCollection buttons;

  public NavBarLeft(SelenideElement parent) {
    this.container = parent.find(".navbar-left.btn-group");
    this.buttons = this.container.findAll("li");
  }

  @Step
  public SelenideElement button(String title) {
    return this.buttons.find(exactText(title));
  }

  @Step
  public NavBarLeft clickOn(String title) {
    button(title).click();
    return this;
  }

  @Step
  public NavBarLeft shouldNotExist(String title) {
    button(title).shouldNot(exist);
    return this;
  }

}
