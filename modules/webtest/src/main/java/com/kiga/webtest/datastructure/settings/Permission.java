package com.kiga.webtest.datastructure.settings;

public enum Permission {
  FULL_TEST("fullTest"), NON_FULL_TEST("nonFullTest");

  private String title;

  Permission(String title) {
    this.title = title;
  }

  @Override
  public String toString() {
    return this.title;
  }
}
