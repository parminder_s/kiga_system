package com.kiga.webtest.pages.widgets.payment.card;

import com.codeborne.selenide.SelenideElement;
import com.kiga.webtest.datastructure.settings.Currency;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import com.kiga.webtest.pages.widgets.basic.Buttons;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.angular.AngularHelpers.$;
import static com.codeborne.selenide.Condition.*;
import static com.kiga.webtest.core.helpers.LocaleHelpers.decimalFormat;

public class CardPaymentRedirectForm extends BasePage {

  private final SelenideElement container = $("body");

  public CardPaymentRedirectForm(App app) {
    super(app);
  }

  @Step
  public CardPaymentRedirectForm assertHeader(String text) {
    this.container.find("#page_header").shouldHave(text(text));
    return this;
  }

  @Step
  public CardPaymentRedirectForm assertDeclined() {
    this.container.find("#error_msg").shouldHave(exactText("DECLINED"));
    return this;
  }

  @Step
  public CardPaymentRedirectForm assertTotal(double sum, Currency currency) {
    this.container.find("#total>td").shouldHave(
      exactText(
        currency + " " +
          decimalFormat(language, sum, "#.00")
      )
    );
    return this;
  }

  @Step
  public CardPaymentRedirectForm assertContains(String text) {
    this.container.find("#page").shouldHave(text(text));
    return this;
  }

  @Step
  public void next() {
    new Buttons(this.container, "#buttons>.btn")
      .clickBy(name("continue"));
  }

}
