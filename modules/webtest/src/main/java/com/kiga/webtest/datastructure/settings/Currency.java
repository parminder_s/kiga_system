package com.kiga.webtest.datastructure.settings;

public enum Currency {
  EUR("€"), USD("$"), CHF("CHF"), TL("TL");

  private final String symbol;

  Currency(String symbol) {
    this.symbol = symbol;
  }

  public String symbol() {
    return this.symbol;
  }

  @Override
  public String toString() {
    return this.name();
  }
}
