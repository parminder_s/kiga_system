package com.kiga.webtest.datastructure;

import com.kiga.webtest.config.Language;
import com.kiga.webtest.datastructure.exceptions.TestDataMismatch;
import com.kiga.webtest.datastructure.settings.Currency;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.HashMap;
import java.util.Map;

public class ShopData {

  private final String title;
  private final Country country;
  private final Map<String, ShopProductData> products;
  private final Map<String, Integer> quantities;

  private ShopData(ShopDataBuilder builder) {
    this.title = builder.title;
    this.country = builder.country;
    this.products = builder.products;
    this.quantities = builder.quantities;
  }

  public String title() {
    return this.title;
  }

  public Country country() {
    return this.country;
  }

  public Currency currency() {
    return this.country.currency();
  }

  public Map<String, ShopProductData> products() {
    return this.products;
  }

  public ShopProductData product(String title) {
    return this.products.get(title);
  }

  public int quantity(String title) {
    return this.quantities.get(title);
  }

  public ShopProductData product(int index) {
    return this.products.values().toArray(new ShopProductData[0])[index];
  }

  public int quantity(int index) {
    return this.quantities.values().toArray(new Integer[0])[index];
  }

  public ShopProductData delivery(int maxIndex) {
    double total = total(maxIndex);
    double delivery =
      total < this.country.totalLimitForDelivery() ?
        this.country.delivery() : 0;

    return new ShopProductData(
      Language.DEUTSCH.term("shop.basket.shipping"),
      delivery, country.currency(), Language.DEUTSCH.term("shop.basket.shipping")
    );
  }

  public double total(int maxIndex) {
    double returner = 0;
    for (int i = 0; i <= maxIndex; i++) {
      returner += this.product(i).price() * this.quantity(i);
    }
    return returner;
  }

  public double total() {
    return total(products().size() - 1);
  }

  public ShopProductData delivery() {
    return delivery(products().size() - 1);
  }

  @Override
  public String toString() {
    return this.title;
  }

  public static class ShopDataBuilder {

    private String title;
    private Country country;
    private Map<String, ShopProductData> products = new HashMap<>();
    private Map<String, Integer> quantities = new HashMap<>();

    public ShopDataBuilder(String title) {
      this.title = title;
    }

    @Step
    public ShopDataBuilder inCountry(Country country) {
      this.country = country;
      return this;
    }

    @Step
    public ShopDataBuilder addProducts(String title, double price, int quantity,
                                       String productTitle) {
      if (products.containsKey(title)) {
        quantities.put(title, quantities.get(title) + quantity);
      } else {
        products.put(title, new ShopProductData(title, price, country.currency(), productTitle));
        quantities.put(title, quantity);
      }
      return this;
    }

    @Step
    public ShopDataBuilder addProduct(ShopProductData shopProductData) {
      if (!shopProductData.currency().equals(this.country.currency())) {
        throw new TestDataMismatch(
          "\nCurrency for product is " + shopProductData.currency() +
            "\n while Currency of country is " + this.country.currency()
        );
      }
      return addProducts(
        shopProductData.title(),
        shopProductData.price(),
        1,
        shopProductData.productTitle()
      );
    }

    public ShopData build() {
      return new ShopData(this);
    }

  }
}
