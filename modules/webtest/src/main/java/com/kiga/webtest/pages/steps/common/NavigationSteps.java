package com.kiga.webtest.pages.steps.common;

import com.codeborne.selenide.WebDriverRunner;
import com.kiga.webtest.config.KigaConfiguration;
import com.kiga.webtest.config.Language;
import com.kiga.webtest.config.Url;
import com.kiga.webtest.core.checks.Assertions;
import com.kiga.webtest.pages.App;
import com.kiga.webtest.pages.pageconfigs.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.kiga.webtest.core.Core.core;

public class NavigationSteps extends BasePage {

  private final Url url;

  public NavigationSteps(App app) {
    super(app);
    this.url = new Url(language);
  }

  @Step
  public NavigationSteps visit() {
    core().open(KigaConfiguration.appBaseUrl);
    assertHomeUrl(); //for exact detect difference - url is not opened vs top Bar is not loaded
    return this;
  }

  @Step
  public NavigationSteps visitHome(Language language) {
    core().open(new Url(language).homeUrl());
    return this;
  }

  @Step
  public NavigationSteps visitHome() {
    core().open(url.homeUrl());
    return this;
  }

  @Step
  public NavigationSteps visitIdeas() {
    core().open(url.ideasUrl());
    return this;
  }

  @Step
  public NavigationSteps visitSubscription() {
    core().open(url.subscriptionUrl());
    return this;
  }

  @Step
  public NavigationSteps visitShop() {
    core().open(new Url(Language.DEUTSCH).shopUrl());
    return this;
  }

  @Step
  public NavigationSteps visitBills() {
    core().open(new Url(Language.DEUTSCH).billsUrl());
    return this;
  }

  @Step
  public NavigationSteps ensureOnKigaHome(Language language) {
    if (!WebDriverRunner.url().equals(new Url(language).homeUrl())) {
      visitHome(language);
    }
    return this;
  }

  @Step
  public NavigationSteps ensureOnKigaHome() {
    return ensureOnKigaHome(language);
  }

  @Step
  public void assertSubscriptionUrl(Language language) {
    assertUrl(new Url(language).subscriptionUrl());
  }

  @Step
  public void assertSubscriptionUrl() {
    assertUrl(this.url.subscriptionUrl());
  }

  @Step
  public void assertLoginUrl(Language language) {
    Assertions.urlExcludingRedirect(new Url(language).loginUrl());
  }

  @Step
  public void assertLoginUrl() {
    Assertions.urlExcludingRedirect(this.url.loginUrl());
  }

  @Step
  public void assertHomeUrl(Language language) {
    assertUrl(new Url(language).homeUrl());
  }

  @Step
  public void assertHomeUrl() {
    assertUrl(this.url.homeUrl());
  }

  @Step
  public void assertShopUrl() {
    assertUrl(this.url.shopUrl());
  }

  @Step
  public void assertWelcomeUrl() {
    assertUrl(this.url.welcomeUrl());
  }

  @Step
  public void assertIdeasActivitiesUrl(){
    assertUrl(this.url.ideasUrl());
  }

  @Step
  public void asssertIdeasAndAgeRangeUrl(int ageRange){
    double rangeIndex = Math.pow(2, ageRange-1);
    assertUrl(this.url.ideasAndAgeRangeUrl((int) rangeIndex));
  }

  @Step
  public void assertTestExpiredErrorUrl() {
    assertUrl(this.url.testExpiredErrorUrl());
  }

  @Step
  public void assertFullRequiredErrorUrl() {
    assertUrl(this.url.fullRequiredErrorUrl());
  }

  @Step
  public void assertFullUpgradeErrorUrl() {
    assertUrl(this.url.fullUpgradeErrorUrl());
  }

  @Step
  public void assertFullExpiredErrorUrl() {
    assertUrl(this.url.fullExpiredErrorUrl());
  }

  @Step
  public void assertLoginOrFullUrl() {
    assertUrl(this.url.loginOrFullUrl());
  }

  //**********************Steps implementation************

  @Step
  private void assertUrl(String url) {
    Assertions.urlToBe(url);
  }

}
