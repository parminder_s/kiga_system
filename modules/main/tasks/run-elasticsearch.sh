#!/usr/bin/env bash

if [[ $(docker images -q kiga-search | wc -l) == 0 ]]; then
  echo "Docker image not available, building it..."
  cd ../containers
  ./build-search.sh
fi

if [[ $(docker ps -q -f status=running -f ancestor=kiga-search | wc -l) > 0 ]]; then
  echo "Search instance is already running"
else
  docker run -dit -p 9200:9200 -p 9300:9300 kiga-search
  echo "Started Search instance"
fi
