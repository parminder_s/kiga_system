#!/usr/bin/env bash

docker run -it --rm \
  -p 8081:8081 \
  -v "$PWD:/data" \
  -w "/data" \
  node:6.9 npm run start

