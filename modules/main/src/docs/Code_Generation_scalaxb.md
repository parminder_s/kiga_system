## Code Generation with scalaxb

### Intro

[scalaxb](http://scalaxb.org/) is a tool that can generate scala classes and services based on an
xml schema (xsd) or a service descriptor (wsdl).

In this project, scalaxb is used to generate scala code for the mpay24 MDXI schema, located at 
[https://www.mpay24.com/schemas/MDXI/v3.0/MDXI.xsd](https://www.mpay24.com/schemas/MDXI/v3.0/MDXI.xsd), 
and for the SOAP services as described in [https://www.mpay24.com/soap/etp/1.5/ETP.wsdl](https://www.mpay24.com/soap/etp/1.5/ETP.wsdl).

### Installation

Follow the guide [here](http://scalaxb.org/setup). Basically, there are a number of ways to be able to use 
scalaxb: command line, sbt, maven, and the [online API](http://scalaxb.org/online). Choose the steps with the
method that is most comfortable for you.

### Generating the Code

#### MDXI

After installing, make sure to have the MDXI xsd available to scalaxb according to the method you use. For example,
for maven, put the xsd file in `src/main/xsd`. For the online tool, simply upload the xsd to generate the code.
Make sure to change the package name to something appropriate. I used `com.kiga.payment.gateway.mpay24`.
 
Generate the code by running (example using maven):

    mvn scalaxb:generate
 
For more details, see [http://scalaxb.org/mvn-scalaxb](http://scalaxb.org/mvn-scalaxb), or the pages related to
the other methods.

After successfully generating the code, copy over the files to this project's designated directory for generated
code, `src/main/generated/scala`.

The file `MDXI.scala` defines a class named 'Package'. This will cause conflicts and compile errors so you should
refactor this class by renaming it to 'MPackage'.

There will be a few compile warnings here and there, depending on the scalaxb/scala version used, but these are
generally ok, though they can be cleaned up by updating the generated code one by one.

#### SOAP

For generating the SOAP services, make sure to have the wsdl available, for example in `src/main/wsdl`. 

Make sure to change the package name to something appropriate. To generate, run:

    mvn scalaxb:generate
    
As with the MDXI steps, also copy over the generated files to `src/main/generated/scala`.

scalaxb uses [Dispatch](http://dispatch.databinder.net/Dispatch.html) as http client. In this project we are using
Apache HC so remove the dispatch-based client and implement an Apache HC-based one. See `scalaxb.ApacheHttpClientsAsync`.

The generated code doesn't allow users of the service to add http headers to the request. I modified it so that 
basic authorization, and other headers, can be sent. See `scalaxb.Soap11ClientsAsync.Soap11ClientAsync#soapRequest`, 
`#requestResponse`, and `#soapResponse`.

For more details, see [http://scalaxb.org/wsdl-support](http://scalaxb.org/wsdl-support).

### Notes

* Both xsd and wsdl code generation will create a `scalaxb` package, so you'll have to separate them. I placed the
MDXI classes (generated from xsd) under com.kiga whereas the SOAP classes are in the top-level package so that MDXI 
classes are under `com.kiga.scalaxb` while SOAP classes are under `scalaxb`.

