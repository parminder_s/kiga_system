## Code Guidelines

### General

* Indent with 2 spaces, don’t use tabs
* Name of the variable which is returned within a function with the name “returner”.
* In a foreach collection use the plural and single name of the iterated elements
* A line should only have a maximum of 80 chars
* A function should not have more than 30 lines
* A class/module/service should not have more than 200 lines
* Usage of singletons is preferred
* Date datatypes: Should always end with ...Date and if time is also relevant, it should end with DateTime
* Use Title instead of Name for title/name fields
* Adding commands should have the name add instead new
* Editing existing entities should be named save instead edit or update
* Deleting commands should have the name remove
* Calculate time for permanent refactoring
* Calculate time for writing test cases
* CamelCase as default naming convention

### LESS

* Make sure a tag has no 0x0 dimensions (cannot be clicked in test cases)
* SnakeCase as naming convention
* Don’t use id attributes, check for .class

### Forms

* Use one of the predefined form classes
* Do not set a ng-click on the submit but ng-submit in form tag
* For cancel use formhelper
