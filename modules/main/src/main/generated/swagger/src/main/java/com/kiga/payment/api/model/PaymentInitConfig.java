package com.kiga.payment.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.kiga.payment.api.model.Money;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PaymentInitConfig
 */
@Validated

public class PaymentInitConfig   {
  /**
   * Gets or Sets paymentType
   */
  public enum PaymentTypeEnum {
    GARANTI("GARANTI"),
    
    MP_AMEX("MP_AMEX"),
    
    MP_CC("MP_CC"),
    
    MP_ELV("MP_ELV"),
    
    MP_MC("MP_MC"),
    
    MP_PAYPAL("MP_PAYPAL"),
    
    MP_VISA("MP_VISA"),
    
    PAYPAL("PAYPAL"),
    
    RE("RE"),
    
    REZS("REZS"),
    
    SOFORT("SOFORT");

    private String value;

    PaymentTypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static PaymentTypeEnum fromValue(String text) {
      for (PaymentTypeEnum b : PaymentTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("paymentType")
  private PaymentTypeEnum paymentType = null;

  @JsonProperty("successUrl")
  private String successUrl = null;

  @JsonProperty("failureUrl")
  private String failureUrl = null;

  @JsonProperty("price")
  private Money price = null;

  @JsonProperty("countryCode")
  private String countryCode = null;

  @JsonProperty("locale")
  private String locale = null;

  @JsonProperty("referenceId")
  private String referenceId = null;

  @JsonProperty("referenceCode")
  private String referenceCode = null;

  @JsonProperty("description")
  private String description = null;

  public PaymentInitConfig paymentType(PaymentTypeEnum paymentType) {
    this.paymentType = paymentType;
    return this;
  }

  /**
   * Get paymentType
   * @return paymentType
  **/
  @ApiModelProperty(example = "GARANTI", value = "")


  public PaymentTypeEnum getPaymentType() {
    return paymentType;
  }

  public void setPaymentType(PaymentTypeEnum paymentType) {
    this.paymentType = paymentType;
  }

  public PaymentInitConfig successUrl(String successUrl) {
    this.successUrl = successUrl;
    return this;
  }

  /**
   * Get successUrl
   * @return successUrl
  **/
  @ApiModelProperty(example = "http://localhost/payment/success", value = "")


  public String getSuccessUrl() {
    return successUrl;
  }

  public void setSuccessUrl(String successUrl) {
    this.successUrl = successUrl;
  }

  public PaymentInitConfig failureUrl(String failureUrl) {
    this.failureUrl = failureUrl;
    return this;
  }

  /**
   * Get failureUrl
   * @return failureUrl
  **/
  @ApiModelProperty(example = "http://localhost/payment/success", value = "")


  public String getFailureUrl() {
    return failureUrl;
  }

  public void setFailureUrl(String failureUrl) {
    this.failureUrl = failureUrl;
  }

  public PaymentInitConfig price(Money price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Money getPrice() {
    return price;
  }

  public void setPrice(Money price) {
    this.price = price;
  }

  public PaymentInitConfig countryCode(String countryCode) {
    this.countryCode = countryCode;
    return this;
  }

  /**
   * Get countryCode
   * @return countryCode
  **/
  @ApiModelProperty(example = "at", value = "")


  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public PaymentInitConfig locale(String locale) {
    this.locale = locale;
    return this;
  }

  /**
   * Get locale
   * @return locale
  **/
  @ApiModelProperty(example = "de", value = "")


  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public PaymentInitConfig referenceId(String referenceId) {
    this.referenceId = referenceId;
    return this;
  }

  /**
   * Get referenceId
   * @return referenceId
  **/
  @ApiModelProperty(example = "123", value = "")


  public String getReferenceId() {
    return referenceId;
  }

  public void setReferenceId(String referenceId) {
    this.referenceId = referenceId;
  }

  public PaymentInitConfig referenceCode(String referenceCode) {
    this.referenceCode = referenceCode;
    return this;
  }

  /**
   * Get referenceCode
   * @return referenceCode
  **/
  @ApiModelProperty(example = "subscription", value = "")


  public String getReferenceCode() {
    return referenceCode;
  }

  public void setReferenceCode(String referenceCode) {
    this.referenceCode = referenceCode;
  }

  public PaymentInitConfig description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(example = "checkout for a full subscription", value = "")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentInitConfig paymentInitConfig = (PaymentInitConfig) o;
    return Objects.equals(this.paymentType, paymentInitConfig.paymentType) &&
        Objects.equals(this.successUrl, paymentInitConfig.successUrl) &&
        Objects.equals(this.failureUrl, paymentInitConfig.failureUrl) &&
        Objects.equals(this.price, paymentInitConfig.price) &&
        Objects.equals(this.countryCode, paymentInitConfig.countryCode) &&
        Objects.equals(this.locale, paymentInitConfig.locale) &&
        Objects.equals(this.referenceId, paymentInitConfig.referenceId) &&
        Objects.equals(this.referenceCode, paymentInitConfig.referenceCode) &&
        Objects.equals(this.description, paymentInitConfig.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentType, successUrl, failureUrl, price, countryCode, locale, referenceId, referenceCode, description);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentInitConfig {\n");
    
    sb.append("    paymentType: ").append(toIndentedString(paymentType)).append("\n");
    sb.append("    successUrl: ").append(toIndentedString(successUrl)).append("\n");
    sb.append("    failureUrl: ").append(toIndentedString(failureUrl)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    countryCode: ").append(toIndentedString(countryCode)).append("\n");
    sb.append("    locale: ").append(toIndentedString(locale)).append("\n");
    sb.append("    referenceId: ").append(toIndentedString(referenceId)).append("\n");
    sb.append("    referenceCode: ").append(toIndentedString(referenceCode)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

