package com.kiga.shop.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.kiga.shop.api.model.BasketResponse;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ApplyVoucherResponse
 */
@Validated

public class ApplyVoucherResponse   {
  @JsonProperty("successful")
  private Boolean successful = null;

  @JsonProperty("basket")
  private BasketResponse basket = null;

  public ApplyVoucherResponse successful(Boolean successful) {
    this.successful = successful;
    return this;
  }

  /**
   * Get successful
   * @return successful
  **/
  @ApiModelProperty(value = "")


  public Boolean isSuccessful() {
    return successful;
  }

  public void setSuccessful(Boolean successful) {
    this.successful = successful;
  }

  public ApplyVoucherResponse basket(BasketResponse basket) {
    this.basket = basket;
    return this;
  }

  /**
   * Get basket
   * @return basket
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BasketResponse getBasket() {
    return basket;
  }

  public void setBasket(BasketResponse basket) {
    this.basket = basket;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApplyVoucherResponse applyVoucherResponse = (ApplyVoucherResponse) o;
    return Objects.equals(this.successful, applyVoucherResponse.successful) &&
        Objects.equals(this.basket, applyVoucherResponse.basket);
  }

  @Override
  public int hashCode() {
    return Objects.hash(successful, basket);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApplyVoucherResponse {\n");
    
    sb.append("    successful: ").append(toIndentedString(successful)).append("\n");
    sb.append("    basket: ").append(toIndentedString(basket)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

