package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.kiga.kga.api.model.UpdateChildInGroupEntry;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * UpdateChildInGroup
 */

public class UpdateChildInGroup   {
  @JsonProperty("yearId")
  private Integer yearId = null;

  @JsonProperty("data")
  @Valid
  private List<UpdateChildInGroupEntry> data = null;

  public UpdateChildInGroup yearId(Integer yearId) {
    this.yearId = yearId;
    return this;
  }

  /**
   * Get yearId
   * @return yearId
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getYearId() {
    return yearId;
  }

  public void setYearId(Integer yearId) {
    this.yearId = yearId;
  }

  public UpdateChildInGroup data(List<UpdateChildInGroupEntry> data) {
    this.data = data;
    return this;
  }

  public UpdateChildInGroup addDataItem(UpdateChildInGroupEntry dataItem) {
    if (this.data == null) {
      this.data = new ArrayList<>();
    }
    this.data.add(dataItem);
    return this;
  }

  /**
   * Get data
   * @return data
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<UpdateChildInGroupEntry> getData() {
    return data;
  }

  public void setData(List<UpdateChildInGroupEntry> data) {
    this.data = data;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateChildInGroup updateChildInGroup = (UpdateChildInGroup) o;
    return Objects.equals(this.yearId, updateChildInGroup.yearId) &&
        Objects.equals(this.data, updateChildInGroup.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(yearId, data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UpdateChildInGroup {\n");
    
    sb.append("    yearId: ").append(toIndentedString(yearId)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

