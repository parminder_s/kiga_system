package com.kiga.bootstrap.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.kiga.bootstrap.api.model.BootstrapLink;
import com.kiga.bootstrap.api.model.Config;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * BootstrapResponse
 */

public class BootstrapResponse   {
  @JsonProperty("countries")
  @Valid
  private Map<String, String> countries = null;

  @JsonProperty("locale")
  private String locale = null;

  @JsonProperty("links")
  @Valid
  private List<BootstrapLink> links = null;

  @JsonProperty("locales")
  @Valid
  private Map<String, String> locales = null;

  @JsonProperty("config")
  private Config config = null;

  @JsonProperty("meta")
  @Valid
  private Map<String, String> meta = null;

  public BootstrapResponse countries(Map<String, String> countries) {
    this.countries = countries;
    return this;
  }

  public BootstrapResponse putCountriesItem(String key, String countriesItem) {
    if (this.countries == null) {
      this.countries = new HashMap<>();
    }
    this.countries.put(key, countriesItem);
    return this;
  }

  /**
   * Get countries
   * @return countries
  **/
  @ApiModelProperty(value = "")


  public Map<String, String> getCountries() {
    return countries;
  }

  public void setCountries(Map<String, String> countries) {
    this.countries = countries;
  }

  public BootstrapResponse locale(String locale) {
    this.locale = locale;
    return this;
  }

  /**
   * Get locale
   * @return locale
  **/
  @ApiModelProperty(value = "")


  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public BootstrapResponse links(List<BootstrapLink> links) {
    this.links = links;
    return this;
  }

  public BootstrapResponse addLinksItem(BootstrapLink linksItem) {
    if (this.links == null) {
      this.links = new ArrayList<>();
    }
    this.links.add(linksItem);
    return this;
  }

  /**
   * Get links
   * @return links
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<BootstrapLink> getLinks() {
    return links;
  }

  public void setLinks(List<BootstrapLink> links) {
    this.links = links;
  }

  public BootstrapResponse locales(Map<String, String> locales) {
    this.locales = locales;
    return this;
  }

  public BootstrapResponse putLocalesItem(String key, String localesItem) {
    if (this.locales == null) {
      this.locales = new HashMap<>();
    }
    this.locales.put(key, localesItem);
    return this;
  }

  /**
   * Get locales
   * @return locales
  **/
  @ApiModelProperty(value = "")


  public Map<String, String> getLocales() {
    return locales;
  }

  public void setLocales(Map<String, String> locales) {
    this.locales = locales;
  }

  public BootstrapResponse config(Config config) {
    this.config = config;
    return this;
  }

  /**
   * Get config
   * @return config
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Config getConfig() {
    return config;
  }

  public void setConfig(Config config) {
    this.config = config;
  }

  public BootstrapResponse meta(Map<String, String> meta) {
    this.meta = meta;
    return this;
  }

  public BootstrapResponse putMetaItem(String key, String metaItem) {
    if (this.meta == null) {
      this.meta = new HashMap<>();
    }
    this.meta.put(key, metaItem);
    return this;
  }

  /**
   * Get meta
   * @return meta
  **/
  @ApiModelProperty(value = "")


  public Map<String, String> getMeta() {
    return meta;
  }

  public void setMeta(Map<String, String> meta) {
    this.meta = meta;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BootstrapResponse bootstrapResponse = (BootstrapResponse) o;
    return Objects.equals(this.countries, bootstrapResponse.countries) &&
        Objects.equals(this.locale, bootstrapResponse.locale) &&
        Objects.equals(this.links, bootstrapResponse.links) &&
        Objects.equals(this.locales, bootstrapResponse.locales) &&
        Objects.equals(this.config, bootstrapResponse.config) &&
        Objects.equals(this.meta, bootstrapResponse.meta);
  }

  @Override
  public int hashCode() {
    return Objects.hash(countries, locale, links, locales, config, meta);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BootstrapResponse {\n");
    
    sb.append("    countries: ").append(toIndentedString(countries)).append("\n");
    sb.append("    locale: ").append(toIndentedString(locale)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    locales: ").append(toIndentedString(locales)).append("\n");
    sb.append("    config: ").append(toIndentedString(config)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

