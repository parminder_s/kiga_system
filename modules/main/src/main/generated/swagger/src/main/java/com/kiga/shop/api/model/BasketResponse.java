package com.kiga.shop.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.kiga.shop.api.model.BasketProduct;
import com.kiga.shop.api.model.Shipping;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * BasketResponse
 */
@Validated

public class BasketResponse   {
  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("products")
  private BasketProduct products = null;

  @JsonProperty("shipping")
  private Shipping shipping = null;

  public BasketResponse id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public BasketResponse products(BasketProduct products) {
    this.products = products;
    return this;
  }

  /**
   * Get products
   * @return products
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BasketProduct getProducts() {
    return products;
  }

  public void setProducts(BasketProduct products) {
    this.products = products;
  }

  public BasketResponse shipping(Shipping shipping) {
    this.shipping = shipping;
    return this;
  }

  /**
   * Get shipping
   * @return shipping
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Shipping getShipping() {
    return shipping;
  }

  public void setShipping(Shipping shipping) {
    this.shipping = shipping;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BasketResponse basketResponse = (BasketResponse) o;
    return Objects.equals(this.id, basketResponse.id) &&
        Objects.equals(this.products, basketResponse.products) &&
        Objects.equals(this.shipping, basketResponse.shipping);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, products, shipping);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BasketResponse {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    products: ").append(toIndentedString(products)).append("\n");
    sb.append("    shipping: ").append(toIndentedString(shipping)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

