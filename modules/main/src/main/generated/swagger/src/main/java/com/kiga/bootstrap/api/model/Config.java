package com.kiga.bootstrap.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Config
 */

public class Config   {
  @JsonProperty("currentCountry")
  private String currentCountry = null;

  @JsonProperty("angularCmsEnabled")
  private Boolean angularCmsEnabled = null;

  @JsonProperty("permissionMode")
  private String permissionMode = null;

  @JsonProperty("ngLiveUrl")
  private String ngLiveUrl = null;

  @JsonProperty("planVersion")
  private BigDecimal planVersion = null;

  @JsonProperty("locales")
  @Valid
  private List<String> locales = null;

  @JsonProperty("repositoryContexts")
  @Valid
  private List<String> repositoryContexts = null;

  @JsonProperty("allowCountrySelection")
  private Boolean allowCountrySelection = null;

  @JsonProperty("changeCountryViaCookie")
  private Boolean changeCountryViaCookie = null;

  @JsonProperty("currentRepositoryContext")
  private String currentRepositoryContext = null;

  @JsonProperty("feedbackEnabled")
  private Boolean feedbackEnabled = null;

  @JsonProperty("googleAnalyticsEnabled")
  private Boolean googleAnalyticsEnabled = null;

  @JsonProperty("jsErrorLogEnabled")
  private Boolean jsErrorLogEnabled = null;

  @JsonProperty("licenceEnabled")
  private Boolean licenceEnabled = null;

  @JsonProperty("memberAdmin")
  private Boolean memberAdmin = null;

  @JsonProperty("newIdeasBlockViewEnabled")
  private Boolean newIdeasBlockViewEnabled = null;

  @JsonProperty("newShopRegistrationEnabled")
  private Boolean newShopRegistrationEnabled = null;

  @JsonProperty("regShowInfo")
  private Boolean regShowInfo = null;

  @JsonProperty("regShowInfoGift")
  private Boolean regShowInfoGift = null;

  @JsonProperty("regShowInfoParent")
  private Boolean regShowInfoParent = null;

  @JsonProperty("regShowInfoStandardOrg")
  private Boolean regShowInfoStandardOrg = null;

  @JsonProperty("regShowInfoTeacher")
  private Boolean regShowInfoTeacher = null;

  @JsonProperty("showInvoice")
  private Boolean showInvoice = null;

  @JsonProperty("showMemberIdeas")
  private Boolean showMemberIdeas = null;

  @JsonProperty("showNewSearchBar")
  private Boolean showNewSearchBar = null;

  @JsonProperty("showQuickRegisters")
  private Boolean showQuickRegisters = null;

  @JsonProperty("showVersion")
  private Boolean showVersion = null;

  public Config currentCountry(String currentCountry) {
    this.currentCountry = currentCountry;
    return this;
  }

  /**
   * Get currentCountry
   * @return currentCountry
  **/
  @ApiModelProperty(value = "")


  public String getCurrentCountry() {
    return currentCountry;
  }

  public void setCurrentCountry(String currentCountry) {
    this.currentCountry = currentCountry;
  }

  public Config angularCmsEnabled(Boolean angularCmsEnabled) {
    this.angularCmsEnabled = angularCmsEnabled;
    return this;
  }

  /**
   * Get angularCmsEnabled
   * @return angularCmsEnabled
  **/
  @ApiModelProperty(value = "")


  public Boolean getAngularCmsEnabled() {
    return angularCmsEnabled;
  }

  public void setAngularCmsEnabled(Boolean angularCmsEnabled) {
    this.angularCmsEnabled = angularCmsEnabled;
  }

  public Config permissionMode(String permissionMode) {
    this.permissionMode = permissionMode;
    return this;
  }

  /**
   * Get permissionMode
   * @return permissionMode
  **/
  @ApiModelProperty(value = "")


  public String getPermissionMode() {
    return permissionMode;
  }

  public void setPermissionMode(String permissionMode) {
    this.permissionMode = permissionMode;
  }

  public Config ngLiveUrl(String ngLiveUrl) {
    this.ngLiveUrl = ngLiveUrl;
    return this;
  }

  /**
   * Get ngLiveUrl
   * @return ngLiveUrl
  **/
  @ApiModelProperty(value = "")


  public String getNgLiveUrl() {
    return ngLiveUrl;
  }

  public void setNgLiveUrl(String ngLiveUrl) {
    this.ngLiveUrl = ngLiveUrl;
  }

  public Config planVersion(BigDecimal planVersion) {
    this.planVersion = planVersion;
    return this;
  }

  /**
   * Get planVersion
   * @return planVersion
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getPlanVersion() {
    return planVersion;
  }

  public void setPlanVersion(BigDecimal planVersion) {
    this.planVersion = planVersion;
  }

  public Config locales(List<String> locales) {
    this.locales = locales;
    return this;
  }

  public Config addLocalesItem(String localesItem) {
    if (this.locales == null) {
      this.locales = new ArrayList<>();
    }
    this.locales.add(localesItem);
    return this;
  }

  /**
   * Get locales
   * @return locales
  **/
  @ApiModelProperty(value = "")


  public List<String> getLocales() {
    return locales;
  }

  public void setLocales(List<String> locales) {
    this.locales = locales;
  }

  public Config repositoryContexts(List<String> repositoryContexts) {
    this.repositoryContexts = repositoryContexts;
    return this;
  }

  public Config addRepositoryContextsItem(String repositoryContextsItem) {
    if (this.repositoryContexts == null) {
      this.repositoryContexts = new ArrayList<>();
    }
    this.repositoryContexts.add(repositoryContextsItem);
    return this;
  }

  /**
   * Get repositoryContexts
   * @return repositoryContexts
  **/
  @ApiModelProperty(value = "")


  public List<String> getRepositoryContexts() {
    return repositoryContexts;
  }

  public void setRepositoryContexts(List<String> repositoryContexts) {
    this.repositoryContexts = repositoryContexts;
  }

  public Config allowCountrySelection(Boolean allowCountrySelection) {
    this.allowCountrySelection = allowCountrySelection;
    return this;
  }

  /**
   * Get allowCountrySelection
   * @return allowCountrySelection
  **/
  @ApiModelProperty(value = "")


  public Boolean getAllowCountrySelection() {
    return allowCountrySelection;
  }

  public void setAllowCountrySelection(Boolean allowCountrySelection) {
    this.allowCountrySelection = allowCountrySelection;
  }

  public Config changeCountryViaCookie(Boolean changeCountryViaCookie) {
    this.changeCountryViaCookie = changeCountryViaCookie;
    return this;
  }

  /**
   * Get changeCountryViaCookie
   * @return changeCountryViaCookie
  **/
  @ApiModelProperty(value = "")


  public Boolean getChangeCountryViaCookie() {
    return changeCountryViaCookie;
  }

  public void setChangeCountryViaCookie(Boolean changeCountryViaCookie) {
    this.changeCountryViaCookie = changeCountryViaCookie;
  }

  public Config currentRepositoryContext(String currentRepositoryContext) {
    this.currentRepositoryContext = currentRepositoryContext;
    return this;
  }

  /**
   * Get currentRepositoryContext
   * @return currentRepositoryContext
  **/
  @ApiModelProperty(value = "")


  public String getCurrentRepositoryContext() {
    return currentRepositoryContext;
  }

  public void setCurrentRepositoryContext(String currentRepositoryContext) {
    this.currentRepositoryContext = currentRepositoryContext;
  }

  public Config feedbackEnabled(Boolean feedbackEnabled) {
    this.feedbackEnabled = feedbackEnabled;
    return this;
  }

  /**
   * Get feedbackEnabled
   * @return feedbackEnabled
  **/
  @ApiModelProperty(value = "")


  public Boolean getFeedbackEnabled() {
    return feedbackEnabled;
  }

  public void setFeedbackEnabled(Boolean feedbackEnabled) {
    this.feedbackEnabled = feedbackEnabled;
  }

  public Config googleAnalyticsEnabled(Boolean googleAnalyticsEnabled) {
    this.googleAnalyticsEnabled = googleAnalyticsEnabled;
    return this;
  }

  /**
   * Get googleAnalyticsEnabled
   * @return googleAnalyticsEnabled
  **/
  @ApiModelProperty(value = "")


  public Boolean getGoogleAnalyticsEnabled() {
    return googleAnalyticsEnabled;
  }

  public void setGoogleAnalyticsEnabled(Boolean googleAnalyticsEnabled) {
    this.googleAnalyticsEnabled = googleAnalyticsEnabled;
  }

  public Config jsErrorLogEnabled(Boolean jsErrorLogEnabled) {
    this.jsErrorLogEnabled = jsErrorLogEnabled;
    return this;
  }

  /**
   * Get jsErrorLogEnabled
   * @return jsErrorLogEnabled
  **/
  @ApiModelProperty(value = "")


  public Boolean getJsErrorLogEnabled() {
    return jsErrorLogEnabled;
  }

  public void setJsErrorLogEnabled(Boolean jsErrorLogEnabled) {
    this.jsErrorLogEnabled = jsErrorLogEnabled;
  }

  public Config licenceEnabled(Boolean licenceEnabled) {
    this.licenceEnabled = licenceEnabled;
    return this;
  }

  /**
   * Get licenceEnabled
   * @return licenceEnabled
  **/
  @ApiModelProperty(value = "")


  public Boolean getLicenceEnabled() {
    return licenceEnabled;
  }

  public void setLicenceEnabled(Boolean licenceEnabled) {
    this.licenceEnabled = licenceEnabled;
  }

  public Config memberAdmin(Boolean memberAdmin) {
    this.memberAdmin = memberAdmin;
    return this;
  }

  /**
   * Get memberAdmin
   * @return memberAdmin
  **/
  @ApiModelProperty(value = "")


  public Boolean getMemberAdmin() {
    return memberAdmin;
  }

  public void setMemberAdmin(Boolean memberAdmin) {
    this.memberAdmin = memberAdmin;
  }

  public Config newIdeasBlockViewEnabled(Boolean newIdeasBlockViewEnabled) {
    this.newIdeasBlockViewEnabled = newIdeasBlockViewEnabled;
    return this;
  }

  /**
   * Get newIdeasBlockViewEnabled
   * @return newIdeasBlockViewEnabled
  **/
  @ApiModelProperty(value = "")


  public Boolean getNewIdeasBlockViewEnabled() {
    return newIdeasBlockViewEnabled;
  }

  public void setNewIdeasBlockViewEnabled(Boolean newIdeasBlockViewEnabled) {
    this.newIdeasBlockViewEnabled = newIdeasBlockViewEnabled;
  }

  public Config newShopRegistrationEnabled(Boolean newShopRegistrationEnabled) {
    this.newShopRegistrationEnabled = newShopRegistrationEnabled;
    return this;
  }

  /**
   * Get newShopRegistrationEnabled
   * @return newShopRegistrationEnabled
  **/
  @ApiModelProperty(value = "")


  public Boolean getNewShopRegistrationEnabled() {
    return newShopRegistrationEnabled;
  }

  public void setNewShopRegistrationEnabled(Boolean newShopRegistrationEnabled) {
    this.newShopRegistrationEnabled = newShopRegistrationEnabled;
  }

  public Config regShowInfo(Boolean regShowInfo) {
    this.regShowInfo = regShowInfo;
    return this;
  }

  /**
   * Get regShowInfo
   * @return regShowInfo
  **/
  @ApiModelProperty(value = "")


  public Boolean getRegShowInfo() {
    return regShowInfo;
  }

  public void setRegShowInfo(Boolean regShowInfo) {
    this.regShowInfo = regShowInfo;
  }

  public Config regShowInfoGift(Boolean regShowInfoGift) {
    this.regShowInfoGift = regShowInfoGift;
    return this;
  }

  /**
   * Get regShowInfoGift
   * @return regShowInfoGift
  **/
  @ApiModelProperty(value = "")


  public Boolean getRegShowInfoGift() {
    return regShowInfoGift;
  }

  public void setRegShowInfoGift(Boolean regShowInfoGift) {
    this.regShowInfoGift = regShowInfoGift;
  }

  public Config regShowInfoParent(Boolean regShowInfoParent) {
    this.regShowInfoParent = regShowInfoParent;
    return this;
  }

  /**
   * Get regShowInfoParent
   * @return regShowInfoParent
  **/
  @ApiModelProperty(value = "")


  public Boolean getRegShowInfoParent() {
    return regShowInfoParent;
  }

  public void setRegShowInfoParent(Boolean regShowInfoParent) {
    this.regShowInfoParent = regShowInfoParent;
  }

  public Config regShowInfoStandardOrg(Boolean regShowInfoStandardOrg) {
    this.regShowInfoStandardOrg = regShowInfoStandardOrg;
    return this;
  }

  /**
   * Get regShowInfoStandardOrg
   * @return regShowInfoStandardOrg
  **/
  @ApiModelProperty(value = "")


  public Boolean getRegShowInfoStandardOrg() {
    return regShowInfoStandardOrg;
  }

  public void setRegShowInfoStandardOrg(Boolean regShowInfoStandardOrg) {
    this.regShowInfoStandardOrg = regShowInfoStandardOrg;
  }

  public Config regShowInfoTeacher(Boolean regShowInfoTeacher) {
    this.regShowInfoTeacher = regShowInfoTeacher;
    return this;
  }

  /**
   * Get regShowInfoTeacher
   * @return regShowInfoTeacher
  **/
  @ApiModelProperty(value = "")


  public Boolean getRegShowInfoTeacher() {
    return regShowInfoTeacher;
  }

  public void setRegShowInfoTeacher(Boolean regShowInfoTeacher) {
    this.regShowInfoTeacher = regShowInfoTeacher;
  }

  public Config showInvoice(Boolean showInvoice) {
    this.showInvoice = showInvoice;
    return this;
  }

  /**
   * Get showInvoice
   * @return showInvoice
  **/
  @ApiModelProperty(value = "")


  public Boolean getShowInvoice() {
    return showInvoice;
  }

  public void setShowInvoice(Boolean showInvoice) {
    this.showInvoice = showInvoice;
  }

  public Config showMemberIdeas(Boolean showMemberIdeas) {
    this.showMemberIdeas = showMemberIdeas;
    return this;
  }

  /**
   * Get showMemberIdeas
   * @return showMemberIdeas
  **/
  @ApiModelProperty(value = "")


  public Boolean getShowMemberIdeas() {
    return showMemberIdeas;
  }

  public void setShowMemberIdeas(Boolean showMemberIdeas) {
    this.showMemberIdeas = showMemberIdeas;
  }

  public Config showNewSearchBar(Boolean showNewSearchBar) {
    this.showNewSearchBar = showNewSearchBar;
    return this;
  }

  /**
   * Get showNewSearchBar
   * @return showNewSearchBar
  **/
  @ApiModelProperty(value = "")


  public Boolean getShowNewSearchBar() {
    return showNewSearchBar;
  }

  public void setShowNewSearchBar(Boolean showNewSearchBar) {
    this.showNewSearchBar = showNewSearchBar;
  }

  public Config showQuickRegisters(Boolean showQuickRegisters) {
    this.showQuickRegisters = showQuickRegisters;
    return this;
  }

  /**
   * Get showQuickRegisters
   * @return showQuickRegisters
  **/
  @ApiModelProperty(value = "")


  public Boolean getShowQuickRegisters() {
    return showQuickRegisters;
  }

  public void setShowQuickRegisters(Boolean showQuickRegisters) {
    this.showQuickRegisters = showQuickRegisters;
  }

  public Config showVersion(Boolean showVersion) {
    this.showVersion = showVersion;
    return this;
  }

  /**
   * Get showVersion
   * @return showVersion
  **/
  @ApiModelProperty(value = "")


  public Boolean getShowVersion() {
    return showVersion;
  }

  public void setShowVersion(Boolean showVersion) {
    this.showVersion = showVersion;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Config config = (Config) o;
    return Objects.equals(this.currentCountry, config.currentCountry) &&
        Objects.equals(this.angularCmsEnabled, config.angularCmsEnabled) &&
        Objects.equals(this.permissionMode, config.permissionMode) &&
        Objects.equals(this.ngLiveUrl, config.ngLiveUrl) &&
        Objects.equals(this.planVersion, config.planVersion) &&
        Objects.equals(this.locales, config.locales) &&
        Objects.equals(this.repositoryContexts, config.repositoryContexts) &&
        Objects.equals(this.allowCountrySelection, config.allowCountrySelection) &&
        Objects.equals(this.changeCountryViaCookie, config.changeCountryViaCookie) &&
        Objects.equals(this.currentRepositoryContext, config.currentRepositoryContext) &&
        Objects.equals(this.feedbackEnabled, config.feedbackEnabled) &&
        Objects.equals(this.googleAnalyticsEnabled, config.googleAnalyticsEnabled) &&
        Objects.equals(this.jsErrorLogEnabled, config.jsErrorLogEnabled) &&
        Objects.equals(this.licenceEnabled, config.licenceEnabled) &&
        Objects.equals(this.memberAdmin, config.memberAdmin) &&
        Objects.equals(this.newIdeasBlockViewEnabled, config.newIdeasBlockViewEnabled) &&
        Objects.equals(this.newShopRegistrationEnabled, config.newShopRegistrationEnabled) &&
        Objects.equals(this.regShowInfo, config.regShowInfo) &&
        Objects.equals(this.regShowInfoGift, config.regShowInfoGift) &&
        Objects.equals(this.regShowInfoParent, config.regShowInfoParent) &&
        Objects.equals(this.regShowInfoStandardOrg, config.regShowInfoStandardOrg) &&
        Objects.equals(this.regShowInfoTeacher, config.regShowInfoTeacher) &&
        Objects.equals(this.showInvoice, config.showInvoice) &&
        Objects.equals(this.showMemberIdeas, config.showMemberIdeas) &&
        Objects.equals(this.showNewSearchBar, config.showNewSearchBar) &&
        Objects.equals(this.showQuickRegisters, config.showQuickRegisters) &&
        Objects.equals(this.showVersion, config.showVersion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currentCountry, angularCmsEnabled, permissionMode, ngLiveUrl, planVersion, locales, repositoryContexts, allowCountrySelection, changeCountryViaCookie, currentRepositoryContext, feedbackEnabled, googleAnalyticsEnabled, jsErrorLogEnabled, licenceEnabled, memberAdmin, newIdeasBlockViewEnabled, newShopRegistrationEnabled, regShowInfo, regShowInfoGift, regShowInfoParent, regShowInfoStandardOrg, regShowInfoTeacher, showInvoice, showMemberIdeas, showNewSearchBar, showQuickRegisters, showVersion);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Config {\n");
    
    sb.append("    currentCountry: ").append(toIndentedString(currentCountry)).append("\n");
    sb.append("    angularCmsEnabled: ").append(toIndentedString(angularCmsEnabled)).append("\n");
    sb.append("    permissionMode: ").append(toIndentedString(permissionMode)).append("\n");
    sb.append("    ngLiveUrl: ").append(toIndentedString(ngLiveUrl)).append("\n");
    sb.append("    planVersion: ").append(toIndentedString(planVersion)).append("\n");
    sb.append("    locales: ").append(toIndentedString(locales)).append("\n");
    sb.append("    repositoryContexts: ").append(toIndentedString(repositoryContexts)).append("\n");
    sb.append("    allowCountrySelection: ").append(toIndentedString(allowCountrySelection)).append("\n");
    sb.append("    changeCountryViaCookie: ").append(toIndentedString(changeCountryViaCookie)).append("\n");
    sb.append("    currentRepositoryContext: ").append(toIndentedString(currentRepositoryContext)).append("\n");
    sb.append("    feedbackEnabled: ").append(toIndentedString(feedbackEnabled)).append("\n");
    sb.append("    googleAnalyticsEnabled: ").append(toIndentedString(googleAnalyticsEnabled)).append("\n");
    sb.append("    jsErrorLogEnabled: ").append(toIndentedString(jsErrorLogEnabled)).append("\n");
    sb.append("    licenceEnabled: ").append(toIndentedString(licenceEnabled)).append("\n");
    sb.append("    memberAdmin: ").append(toIndentedString(memberAdmin)).append("\n");
    sb.append("    newIdeasBlockViewEnabled: ").append(toIndentedString(newIdeasBlockViewEnabled)).append("\n");
    sb.append("    newShopRegistrationEnabled: ").append(toIndentedString(newShopRegistrationEnabled)).append("\n");
    sb.append("    regShowInfo: ").append(toIndentedString(regShowInfo)).append("\n");
    sb.append("    regShowInfoGift: ").append(toIndentedString(regShowInfoGift)).append("\n");
    sb.append("    regShowInfoParent: ").append(toIndentedString(regShowInfoParent)).append("\n");
    sb.append("    regShowInfoStandardOrg: ").append(toIndentedString(regShowInfoStandardOrg)).append("\n");
    sb.append("    regShowInfoTeacher: ").append(toIndentedString(regShowInfoTeacher)).append("\n");
    sb.append("    showInvoice: ").append(toIndentedString(showInvoice)).append("\n");
    sb.append("    showMemberIdeas: ").append(toIndentedString(showMemberIdeas)).append("\n");
    sb.append("    showNewSearchBar: ").append(toIndentedString(showNewSearchBar)).append("\n");
    sb.append("    showQuickRegisters: ").append(toIndentedString(showQuickRegisters)).append("\n");
    sb.append("    showVersion: ").append(toIndentedString(showVersion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

