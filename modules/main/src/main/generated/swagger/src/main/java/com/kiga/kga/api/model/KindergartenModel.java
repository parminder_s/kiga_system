package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.kiga.kga.api.model.KigaAddress;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * KindergartenModel
 */

public class KindergartenModel   {
  @JsonProperty("address")
  private KigaAddress address = null;

  @JsonProperty("adminCustomerNumber")
  private String adminCustomerNumber = null;

  @JsonProperty("currentYearId")
  private Integer currentYearId = null;

  @JsonProperty("email")
  private String email = null;

  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("phoneNumber")
  private String phoneNumber = null;

  @JsonProperty("website")
  private String website = null;

  public KindergartenModel address(KigaAddress address) {
    this.address = address;
    return this;
  }

  /**
   * Get address
   * @return address
  **/
  @ApiModelProperty(value = "")

  @Valid

  public KigaAddress getAddress() {
    return address;
  }

  public void setAddress(KigaAddress address) {
    this.address = address;
  }

  public KindergartenModel adminCustomerNumber(String adminCustomerNumber) {
    this.adminCustomerNumber = adminCustomerNumber;
    return this;
  }

  /**
   * Get adminCustomerNumber
   * @return adminCustomerNumber
  **/
  @ApiModelProperty(example = "123123", value = "")


  public String getAdminCustomerNumber() {
    return adminCustomerNumber;
  }

  public void setAdminCustomerNumber(String adminCustomerNumber) {
    this.adminCustomerNumber = adminCustomerNumber;
  }

  public KindergartenModel currentYearId(Integer currentYearId) {
    this.currentYearId = currentYearId;
    return this;
  }

  /**
   * Get currentYearId
   * @return currentYearId
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getCurrentYearId() {
    return currentYearId;
  }

  public void setCurrentYearId(Integer currentYearId) {
    this.currentYearId = currentYearId;
  }

  public KindergartenModel email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(example = "office@flohzirkus.at", value = "")


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public KindergartenModel id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public KindergartenModel name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "Kindergarten Flohzirkus", value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public KindergartenModel phoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

  /**
   * Get phoneNumber
   * @return phoneNumber
  **/
  @ApiModelProperty(example = "123456789", value = "")


  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public KindergartenModel website(String website) {
    this.website = website;
    return this;
  }

  /**
   * Get website
   * @return website
  **/
  @ApiModelProperty(example = "flohzirkus.at", value = "")


  public String getWebsite() {
    return website;
  }

  public void setWebsite(String website) {
    this.website = website;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KindergartenModel kindergartenModel = (KindergartenModel) o;
    return Objects.equals(this.address, kindergartenModel.address) &&
        Objects.equals(this.adminCustomerNumber, kindergartenModel.adminCustomerNumber) &&
        Objects.equals(this.currentYearId, kindergartenModel.currentYearId) &&
        Objects.equals(this.email, kindergartenModel.email) &&
        Objects.equals(this.id, kindergartenModel.id) &&
        Objects.equals(this.name, kindergartenModel.name) &&
        Objects.equals(this.phoneNumber, kindergartenModel.phoneNumber) &&
        Objects.equals(this.website, kindergartenModel.website);
  }

  @Override
  public int hashCode() {
    return Objects.hash(address, adminCustomerNumber, currentYearId, email, id, name, phoneNumber, website);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class KindergartenModel {\n");
    
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    adminCustomerNumber: ").append(toIndentedString(adminCustomerNumber)).append("\n");
    sb.append("    currentYearId: ").append(toIndentedString(currentYearId)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
    sb.append("    website: ").append(toIndentedString(website)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

