package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets CareTypeEnum
 */
public enum CareTypeEnum {
  
  MORNING("MORNING"),
  
  AFTERNOON("AFTERNOON"),
  
  ALLDAY("ALLDAY"),
  
  FLEXIBLE("FLEXIBLE"),
  
  OTHER("OTHER");

  private String value;

  CareTypeEnum(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static CareTypeEnum fromValue(String text) {
    for (CareTypeEnum b : CareTypeEnum.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + text + "'");
  }
}

