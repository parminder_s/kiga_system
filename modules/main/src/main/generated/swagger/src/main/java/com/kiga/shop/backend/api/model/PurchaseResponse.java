package com.kiga.shop.backend.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PurchaseResponse
 */

public class PurchaseResponse   {
  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("orderNr")
  private Integer orderNr = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("country")
  private String country = null;

  @JsonProperty("priceNetTotal")
  private String priceNetTotal = null;

  @JsonProperty("priceGrossTotal")
  private String priceGrossTotal = null;

  @JsonProperty("currency")
  private String currency = null;

  @JsonProperty("status")
  private String status = null;

  @JsonProperty("amount")
  private Integer amount = null;

  @JsonProperty("shippingNoteNumber")
  private String shippingNoteNumber = null;

  @JsonProperty("shippingPartner")
  private String shippingPartner = null;

  @JsonProperty("invoiceNumber")
  private String invoiceNumber = null;

  @JsonProperty("invoiceId")
  private Long invoiceId = null;

  @JsonProperty("memberId")
  private String memberId = null;

  @JsonProperty("memberEmail")
  private String memberEmail = null;

  @JsonProperty("paymentPspId")
  private String paymentPspId = null;

  @JsonProperty("purchaseDate")
  private LocalDate purchaseDate = null;

  @JsonProperty("comment")
  private String comment = null;

  @JsonProperty("voucherNumber")
  private String voucherNumber = null;

  @JsonProperty("paymentMethod")
  private String paymentMethod = null;

  @JsonProperty("invoiceSettled")
  private Boolean invoiceSettled = null;

  public PurchaseResponse id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public PurchaseResponse orderNr(Integer orderNr) {
    this.orderNr = orderNr;
    return this;
  }

  /**
   * Get orderNr
   * @return orderNr
  **/
  @ApiModelProperty(value = "")


  public Integer getOrderNr() {
    return orderNr;
  }

  public void setOrderNr(Integer orderNr) {
    this.orderNr = orderNr;
  }

  public PurchaseResponse name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PurchaseResponse country(String country) {
    this.country = country;
    return this;
  }

  /**
   * Get country
   * @return country
  **/
  @ApiModelProperty(value = "")


  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public PurchaseResponse priceNetTotal(String priceNetTotal) {
    this.priceNetTotal = priceNetTotal;
    return this;
  }

  /**
   * Get priceNetTotal
   * @return priceNetTotal
  **/
  @ApiModelProperty(value = "")


  public String getPriceNetTotal() {
    return priceNetTotal;
  }

  public void setPriceNetTotal(String priceNetTotal) {
    this.priceNetTotal = priceNetTotal;
  }

  public PurchaseResponse priceGrossTotal(String priceGrossTotal) {
    this.priceGrossTotal = priceGrossTotal;
    return this;
  }

  /**
   * Get priceGrossTotal
   * @return priceGrossTotal
  **/
  @ApiModelProperty(value = "")


  public String getPriceGrossTotal() {
    return priceGrossTotal;
  }

  public void setPriceGrossTotal(String priceGrossTotal) {
    this.priceGrossTotal = priceGrossTotal;
  }

  public PurchaseResponse currency(String currency) {
    this.currency = currency;
    return this;
  }

  /**
   * Get currency
   * @return currency
  **/
  @ApiModelProperty(value = "")


  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public PurchaseResponse status(String status) {
    this.status = status;
    return this;
  }

  /**
   * Get status
   * @return status
  **/
  @ApiModelProperty(value = "")


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public PurchaseResponse amount(Integer amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Get amount
   * @return amount
  **/
  @ApiModelProperty(value = "")


  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public PurchaseResponse shippingNoteNumber(String shippingNoteNumber) {
    this.shippingNoteNumber = shippingNoteNumber;
    return this;
  }

  /**
   * Get shippingNoteNumber
   * @return shippingNoteNumber
  **/
  @ApiModelProperty(value = "")


  public String getShippingNoteNumber() {
    return shippingNoteNumber;
  }

  public void setShippingNoteNumber(String shippingNoteNumber) {
    this.shippingNoteNumber = shippingNoteNumber;
  }

  public PurchaseResponse shippingPartner(String shippingPartner) {
    this.shippingPartner = shippingPartner;
    return this;
  }

  /**
   * Get shippingPartner
   * @return shippingPartner
  **/
  @ApiModelProperty(value = "")


  public String getShippingPartner() {
    return shippingPartner;
  }

  public void setShippingPartner(String shippingPartner) {
    this.shippingPartner = shippingPartner;
  }

  public PurchaseResponse invoiceNumber(String invoiceNumber) {
    this.invoiceNumber = invoiceNumber;
    return this;
  }

  /**
   * Get invoiceNumber
   * @return invoiceNumber
  **/
  @ApiModelProperty(value = "")


  public String getInvoiceNumber() {
    return invoiceNumber;
  }

  public void setInvoiceNumber(String invoiceNumber) {
    this.invoiceNumber = invoiceNumber;
  }

  public PurchaseResponse invoiceId(Long invoiceId) {
    this.invoiceId = invoiceId;
    return this;
  }

  /**
   * Get invoiceId
   * @return invoiceId
  **/
  @ApiModelProperty(value = "")


  public Long getInvoiceId() {
    return invoiceId;
  }

  public void setInvoiceId(Long invoiceId) {
    this.invoiceId = invoiceId;
  }

  public PurchaseResponse memberId(String memberId) {
    this.memberId = memberId;
    return this;
  }

  /**
   * Get memberId
   * @return memberId
  **/
  @ApiModelProperty(value = "")


  public String getMemberId() {
    return memberId;
  }

  public void setMemberId(String memberId) {
    this.memberId = memberId;
  }

  public PurchaseResponse memberEmail(String memberEmail) {
    this.memberEmail = memberEmail;
    return this;
  }

  /**
   * Get memberEmail
   * @return memberEmail
  **/
  @ApiModelProperty(value = "")


  public String getMemberEmail() {
    return memberEmail;
  }

  public void setMemberEmail(String memberEmail) {
    this.memberEmail = memberEmail;
  }

  public PurchaseResponse paymentPspId(String paymentPspId) {
    this.paymentPspId = paymentPspId;
    return this;
  }

  /**
   * Get paymentPspId
   * @return paymentPspId
  **/
  @ApiModelProperty(value = "")


  public String getPaymentPspId() {
    return paymentPspId;
  }

  public void setPaymentPspId(String paymentPspId) {
    this.paymentPspId = paymentPspId;
  }

  public PurchaseResponse purchaseDate(LocalDate purchaseDate) {
    this.purchaseDate = purchaseDate;
    return this;
  }

  /**
   * Get purchaseDate
   * @return purchaseDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getPurchaseDate() {
    return purchaseDate;
  }

  public void setPurchaseDate(LocalDate purchaseDate) {
    this.purchaseDate = purchaseDate;
  }

  public PurchaseResponse comment(String comment) {
    this.comment = comment;
    return this;
  }

  /**
   * Get comment
   * @return comment
  **/
  @ApiModelProperty(value = "")


  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public PurchaseResponse voucherNumber(String voucherNumber) {
    this.voucherNumber = voucherNumber;
    return this;
  }

  /**
   * Get voucherNumber
   * @return voucherNumber
  **/
  @ApiModelProperty(value = "")


  public String getVoucherNumber() {
    return voucherNumber;
  }

  public void setVoucherNumber(String voucherNumber) {
    this.voucherNumber = voucherNumber;
  }

  public PurchaseResponse paymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
    return this;
  }

  /**
   * Get paymentMethod
   * @return paymentMethod
  **/
  @ApiModelProperty(value = "")


  public String getPaymentMethod() {
    return paymentMethod;
  }

  public void setPaymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  public PurchaseResponse invoiceSettled(Boolean invoiceSettled) {
    this.invoiceSettled = invoiceSettled;
    return this;
  }

  /**
   * Get invoiceSettled
   * @return invoiceSettled
  **/
  @ApiModelProperty(value = "")


  public Boolean getInvoiceSettled() {
    return invoiceSettled;
  }

  public void setInvoiceSettled(Boolean invoiceSettled) {
    this.invoiceSettled = invoiceSettled;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PurchaseResponse purchaseResponse = (PurchaseResponse) o;
    return Objects.equals(this.id, purchaseResponse.id) &&
        Objects.equals(this.orderNr, purchaseResponse.orderNr) &&
        Objects.equals(this.name, purchaseResponse.name) &&
        Objects.equals(this.country, purchaseResponse.country) &&
        Objects.equals(this.priceNetTotal, purchaseResponse.priceNetTotal) &&
        Objects.equals(this.priceGrossTotal, purchaseResponse.priceGrossTotal) &&
        Objects.equals(this.currency, purchaseResponse.currency) &&
        Objects.equals(this.status, purchaseResponse.status) &&
        Objects.equals(this.amount, purchaseResponse.amount) &&
        Objects.equals(this.shippingNoteNumber, purchaseResponse.shippingNoteNumber) &&
        Objects.equals(this.shippingPartner, purchaseResponse.shippingPartner) &&
        Objects.equals(this.invoiceNumber, purchaseResponse.invoiceNumber) &&
        Objects.equals(this.invoiceId, purchaseResponse.invoiceId) &&
        Objects.equals(this.memberId, purchaseResponse.memberId) &&
        Objects.equals(this.memberEmail, purchaseResponse.memberEmail) &&
        Objects.equals(this.paymentPspId, purchaseResponse.paymentPspId) &&
        Objects.equals(this.purchaseDate, purchaseResponse.purchaseDate) &&
        Objects.equals(this.comment, purchaseResponse.comment) &&
        Objects.equals(this.voucherNumber, purchaseResponse.voucherNumber) &&
        Objects.equals(this.paymentMethod, purchaseResponse.paymentMethod) &&
        Objects.equals(this.invoiceSettled, purchaseResponse.invoiceSettled);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, orderNr, name, country, priceNetTotal, priceGrossTotal, currency, status, amount, shippingNoteNumber, shippingPartner, invoiceNumber, invoiceId, memberId, memberEmail, paymentPspId, purchaseDate, comment, voucherNumber, paymentMethod, invoiceSettled);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PurchaseResponse {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    orderNr: ").append(toIndentedString(orderNr)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    priceNetTotal: ").append(toIndentedString(priceNetTotal)).append("\n");
    sb.append("    priceGrossTotal: ").append(toIndentedString(priceGrossTotal)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    shippingNoteNumber: ").append(toIndentedString(shippingNoteNumber)).append("\n");
    sb.append("    shippingPartner: ").append(toIndentedString(shippingPartner)).append("\n");
    sb.append("    invoiceNumber: ").append(toIndentedString(invoiceNumber)).append("\n");
    sb.append("    invoiceId: ").append(toIndentedString(invoiceId)).append("\n");
    sb.append("    memberId: ").append(toIndentedString(memberId)).append("\n");
    sb.append("    memberEmail: ").append(toIndentedString(memberEmail)).append("\n");
    sb.append("    paymentPspId: ").append(toIndentedString(paymentPspId)).append("\n");
    sb.append("    purchaseDate: ").append(toIndentedString(purchaseDate)).append("\n");
    sb.append("    comment: ").append(toIndentedString(comment)).append("\n");
    sb.append("    voucherNumber: ").append(toIndentedString(voucherNumber)).append("\n");
    sb.append("    paymentMethod: ").append(toIndentedString(paymentMethod)).append("\n");
    sb.append("    invoiceSettled: ").append(toIndentedString(invoiceSettled)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

