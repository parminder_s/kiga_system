package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.kiga.kga.api.model.EmploymentContractEnum;
import com.kiga.kga.api.model.EmploymentTypeEnum;
import com.kiga.kga.api.model.GenderEnum;
import com.kiga.kga.api.model.KigaAddress;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * EmployeeModel
 */

public class EmployeeModel   {
  @JsonProperty("email")
  private String email = null;

  @JsonProperty("firstname")
  private String firstname = null;

  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("kindergartenId")
  private Integer kindergartenId = null;

  @JsonProperty("gender")
  private GenderEnum gender = null;

  @JsonProperty("employmentContract")
  private EmploymentContractEnum employmentContract = null;

  @JsonProperty("employmentContractNote")
  private String employmentContractNote = null;

  @JsonProperty("address")
  private KigaAddress address = null;

  @JsonProperty("birthDate")
  private String birthDate = null;

  @JsonProperty("lastname")
  private String lastname = null;

  @JsonProperty("socialSecurityNumber")
  private String socialSecurityNumber = null;

  @JsonProperty("phoneNumber")
  private String phoneNumber = null;

  @JsonProperty("employmentType")
  private EmploymentTypeEnum employmentType = null;

  @JsonProperty("parentalLeave")
  private Boolean parentalLeave = null;

  public EmployeeModel email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(example = "Kohout", value = "")


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public EmployeeModel firstname(String firstname) {
    this.firstname = firstname;
    return this;
  }

  /**
   * Get firstname
   * @return firstname
  **/
  @ApiModelProperty(example = "Evelyn", value = "")


  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public EmployeeModel id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public EmployeeModel kindergartenId(Integer kindergartenId) {
    this.kindergartenId = kindergartenId;
    return this;
  }

  /**
   * Get kindergartenId
   * @return kindergartenId
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getKindergartenId() {
    return kindergartenId;
  }

  public void setKindergartenId(Integer kindergartenId) {
    this.kindergartenId = kindergartenId;
  }

  public EmployeeModel gender(GenderEnum gender) {
    this.gender = gender;
    return this;
  }

  /**
   * Get gender
   * @return gender
  **/
  @ApiModelProperty(value = "")

  @Valid

  public GenderEnum getGender() {
    return gender;
  }

  public void setGender(GenderEnum gender) {
    this.gender = gender;
  }

  public EmployeeModel employmentContract(EmploymentContractEnum employmentContract) {
    this.employmentContract = employmentContract;
    return this;
  }

  /**
   * Get employmentContract
   * @return employmentContract
  **/
  @ApiModelProperty(value = "")

  @Valid

  public EmploymentContractEnum getEmploymentContract() {
    return employmentContract;
  }

  public void setEmploymentContract(EmploymentContractEnum employmentContract) {
    this.employmentContract = employmentContract;
  }

  public EmployeeModel employmentContractNote(String employmentContractNote) {
    this.employmentContractNote = employmentContractNote;
    return this;
  }

  /**
   * Get employmentContractNote
   * @return employmentContractNote
  **/
  @ApiModelProperty(example = "50%", value = "")


  public String getEmploymentContractNote() {
    return employmentContractNote;
  }

  public void setEmploymentContractNote(String employmentContractNote) {
    this.employmentContractNote = employmentContractNote;
  }

  public EmployeeModel address(KigaAddress address) {
    this.address = address;
    return this;
  }

  /**
   * Get address
   * @return address
  **/
  @ApiModelProperty(value = "")

  @Valid

  public KigaAddress getAddress() {
    return address;
  }

  public void setAddress(KigaAddress address) {
    this.address = address;
  }

  public EmployeeModel birthDate(String birthDate) {
    this.birthDate = birthDate;
    return this;
  }

  /**
   * Get birthDate
   * @return birthDate
  **/
  @ApiModelProperty(example = "1994-01-01", value = "")


  public String getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(String birthDate) {
    this.birthDate = birthDate;
  }

  public EmployeeModel lastname(String lastname) {
    this.lastname = lastname;
    return this;
  }

  /**
   * Get lastname
   * @return lastname
  **/
  @ApiModelProperty(example = "Kohout", value = "")


  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public EmployeeModel socialSecurityNumber(String socialSecurityNumber) {
    this.socialSecurityNumber = socialSecurityNumber;
    return this;
  }

  /**
   * Get socialSecurityNumber
   * @return socialSecurityNumber
  **/
  @ApiModelProperty(example = "123123123", value = "")


  public String getSocialSecurityNumber() {
    return socialSecurityNumber;
  }

  public void setSocialSecurityNumber(String socialSecurityNumber) {
    this.socialSecurityNumber = socialSecurityNumber;
  }

  public EmployeeModel phoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

  /**
   * Get phoneNumber
   * @return phoneNumber
  **/
  @ApiModelProperty(example = "0660123123", value = "")


  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public EmployeeModel employmentType(EmploymentTypeEnum employmentType) {
    this.employmentType = employmentType;
    return this;
  }

  /**
   * Get employmentType
   * @return employmentType
  **/
  @ApiModelProperty(value = "")

  @Valid

  public EmploymentTypeEnum getEmploymentType() {
    return employmentType;
  }

  public void setEmploymentType(EmploymentTypeEnum employmentType) {
    this.employmentType = employmentType;
  }

  public EmployeeModel parentalLeave(Boolean parentalLeave) {
    this.parentalLeave = parentalLeave;
    return this;
  }

  /**
   * Get parentalLeave
   * @return parentalLeave
  **/
  @ApiModelProperty(example = "true", value = "")


  public Boolean getParentalLeave() {
    return parentalLeave;
  }

  public void setParentalLeave(Boolean parentalLeave) {
    this.parentalLeave = parentalLeave;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EmployeeModel employeeModel = (EmployeeModel) o;
    return Objects.equals(this.email, employeeModel.email) &&
        Objects.equals(this.firstname, employeeModel.firstname) &&
        Objects.equals(this.id, employeeModel.id) &&
        Objects.equals(this.kindergartenId, employeeModel.kindergartenId) &&
        Objects.equals(this.gender, employeeModel.gender) &&
        Objects.equals(this.employmentContract, employeeModel.employmentContract) &&
        Objects.equals(this.employmentContractNote, employeeModel.employmentContractNote) &&
        Objects.equals(this.address, employeeModel.address) &&
        Objects.equals(this.birthDate, employeeModel.birthDate) &&
        Objects.equals(this.lastname, employeeModel.lastname) &&
        Objects.equals(this.socialSecurityNumber, employeeModel.socialSecurityNumber) &&
        Objects.equals(this.phoneNumber, employeeModel.phoneNumber) &&
        Objects.equals(this.employmentType, employeeModel.employmentType) &&
        Objects.equals(this.parentalLeave, employeeModel.parentalLeave);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, firstname, id, kindergartenId, gender, employmentContract, employmentContractNote, address, birthDate, lastname, socialSecurityNumber, phoneNumber, employmentType, parentalLeave);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EmployeeModel {\n");
    
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    firstname: ").append(toIndentedString(firstname)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    kindergartenId: ").append(toIndentedString(kindergartenId)).append("\n");
    sb.append("    gender: ").append(toIndentedString(gender)).append("\n");
    sb.append("    employmentContract: ").append(toIndentedString(employmentContract)).append("\n");
    sb.append("    employmentContractNote: ").append(toIndentedString(employmentContractNote)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    birthDate: ").append(toIndentedString(birthDate)).append("\n");
    sb.append("    lastname: ").append(toIndentedString(lastname)).append("\n");
    sb.append("    socialSecurityNumber: ").append(toIndentedString(socialSecurityNumber)).append("\n");
    sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
    sb.append("    employmentType: ").append(toIndentedString(employmentType)).append("\n");
    sb.append("    parentalLeave: ").append(toIndentedString(parentalLeave)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

