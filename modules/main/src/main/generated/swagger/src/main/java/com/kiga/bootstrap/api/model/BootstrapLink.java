package com.kiga.bootstrap.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * BootstrapLink
 */

public class BootstrapLink   {
  @JsonProperty("label")
  private String label = null;

  @JsonProperty("url")
  private String url = null;

  @JsonProperty("code")
  private String code = null;

  @JsonProperty("isLegacy")
  private Boolean isLegacy = null;

  @JsonProperty("inNavbar")
  private Boolean inNavbar = null;

  public BootstrapLink label(String label) {
    this.label = label;
    return this;
  }

  /**
   * Get label
   * @return label
  **/
  @ApiModelProperty(value = "")


  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public BootstrapLink url(String url) {
    this.url = url;
    return this;
  }

  /**
   * Get url
   * @return url
  **/
  @ApiModelProperty(value = "")


  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public BootstrapLink code(String code) {
    this.code = code;
    return this;
  }

  /**
   * Get code
   * @return code
  **/
  @ApiModelProperty(value = "")


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public BootstrapLink isLegacy(Boolean isLegacy) {
    this.isLegacy = isLegacy;
    return this;
  }

  /**
   * Get isLegacy
   * @return isLegacy
  **/
  @ApiModelProperty(value = "")


  public Boolean getIsLegacy() {
    return isLegacy;
  }

  public void setIsLegacy(Boolean isLegacy) {
    this.isLegacy = isLegacy;
  }

  public BootstrapLink inNavbar(Boolean inNavbar) {
    this.inNavbar = inNavbar;
    return this;
  }

  /**
   * Get inNavbar
   * @return inNavbar
  **/
  @ApiModelProperty(value = "")


  public Boolean getInNavbar() {
    return inNavbar;
  }

  public void setInNavbar(Boolean inNavbar) {
    this.inNavbar = inNavbar;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BootstrapLink bootstrapLink = (BootstrapLink) o;
    return Objects.equals(this.label, bootstrapLink.label) &&
        Objects.equals(this.url, bootstrapLink.url) &&
        Objects.equals(this.code, bootstrapLink.code) &&
        Objects.equals(this.isLegacy, bootstrapLink.isLegacy) &&
        Objects.equals(this.inNavbar, bootstrapLink.inNavbar);
  }

  @Override
  public int hashCode() {
    return Objects.hash(label, url, code, isLegacy, inNavbar);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BootstrapLink {\n");
    
    sb.append("    label: ").append(toIndentedString(label)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    isLegacy: ").append(toIndentedString(isLegacy)).append("\n");
    sb.append("    inNavbar: ").append(toIndentedString(inNavbar)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

