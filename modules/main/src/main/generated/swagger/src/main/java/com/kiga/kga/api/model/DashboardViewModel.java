package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.kiga.kga.api.model.YearViewModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DashboardViewModel
 */

public class DashboardViewModel   {
  @JsonProperty("kindergartenId")
  private Integer kindergartenId = null;

  @JsonProperty("kindergartenDescription")
  private String kindergartenDescription = null;

  @JsonProperty("years")
  @Valid
  private List<YearViewModel> years = null;

  public DashboardViewModel kindergartenId(Integer kindergartenId) {
    this.kindergartenId = kindergartenId;
    return this;
  }

  /**
   * Get kindergartenId
   * @return kindergartenId
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getKindergartenId() {
    return kindergartenId;
  }

  public void setKindergartenId(Integer kindergartenId) {
    this.kindergartenId = kindergartenId;
  }

  public DashboardViewModel kindergartenDescription(String kindergartenDescription) {
    this.kindergartenDescription = kindergartenDescription;
    return this;
  }

  /**
   * Get kindergartenDescription
   * @return kindergartenDescription
  **/
  @ApiModelProperty(example = "Kindergarten Poggersdorf", value = "")


  public String getKindergartenDescription() {
    return kindergartenDescription;
  }

  public void setKindergartenDescription(String kindergartenDescription) {
    this.kindergartenDescription = kindergartenDescription;
  }

  public DashboardViewModel years(List<YearViewModel> years) {
    this.years = years;
    return this;
  }

  public DashboardViewModel addYearsItem(YearViewModel yearsItem) {
    if (this.years == null) {
      this.years = new ArrayList<>();
    }
    this.years.add(yearsItem);
    return this;
  }

  /**
   * Get years
   * @return years
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<YearViewModel> getYears() {
    return years;
  }

  public void setYears(List<YearViewModel> years) {
    this.years = years;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DashboardViewModel dashboardViewModel = (DashboardViewModel) o;
    return Objects.equals(this.kindergartenId, dashboardViewModel.kindergartenId) &&
        Objects.equals(this.kindergartenDescription, dashboardViewModel.kindergartenDescription) &&
        Objects.equals(this.years, dashboardViewModel.years);
  }

  @Override
  public int hashCode() {
    return Objects.hash(kindergartenId, kindergartenDescription, years);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DashboardViewModel {\n");
    
    sb.append("    kindergartenId: ").append(toIndentedString(kindergartenId)).append("\n");
    sb.append("    kindergartenDescription: ").append(toIndentedString(kindergartenDescription)).append("\n");
    sb.append("    years: ").append(toIndentedString(years)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

