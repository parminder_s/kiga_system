package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AddChildToGroup
 */

public class AddChildToGroup   {
  @JsonProperty("childId")
  private Integer childId = null;

  @JsonProperty("groupId")
  private Integer groupId = null;

  public AddChildToGroup childId(Integer childId) {
    this.childId = childId;
    return this;
  }

  /**
   * Get childId
   * @return childId
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getChildId() {
    return childId;
  }

  public void setChildId(Integer childId) {
    this.childId = childId;
  }

  public AddChildToGroup groupId(Integer groupId) {
    this.groupId = groupId;
    return this;
  }

  /**
   * Get groupId
   * @return groupId
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getGroupId() {
    return groupId;
  }

  public void setGroupId(Integer groupId) {
    this.groupId = groupId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AddChildToGroup addChildToGroup = (AddChildToGroup) o;
    return Objects.equals(this.childId, addChildToGroup.childId) &&
        Objects.equals(this.groupId, addChildToGroup.groupId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(childId, groupId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AddChildToGroup {\n");
    
    sb.append("    childId: ").append(toIndentedString(childId)).append("\n");
    sb.append("    groupId: ").append(toIndentedString(groupId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

