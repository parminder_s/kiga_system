package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.kiga.kga.api.model.AllergyEnum;
import com.kiga.kga.api.model.CareTypeEnum;
import com.kiga.kga.api.model.DiseasesEnum;
import com.kiga.kga.api.model.GenderEnum;
import com.kiga.kga.api.model.KgaParentModel;
import com.kiga.kga.api.model.ReligionEnum;
import com.kiga.kga.api.model.VaccinationEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ChildModel
 */

public class ChildModel   {
  @JsonProperty("allergies")
  @Valid
  private List<AllergyEnum> allergies = null;

  @JsonProperty("birthDate")
  private String birthDate = null;

  @JsonProperty("entryDate")
  private String entryDate = null;

  @JsonProperty("seperationDate")
  private String seperationDate = null;

  @JsonProperty("citizenshipCountryId")
  private BigDecimal citizenshipCountryId = null;

  @JsonProperty("diseases")
  @Valid
  private List<DiseasesEnum> diseases = null;

  @JsonProperty("secondLanguage")
  @Valid
  private List<BigDecimal> secondLanguage = null;

  @JsonProperty("vaccinations")
  @Valid
  private List<VaccinationEnum> vaccinations = null;

  @JsonProperty("firstname")
  private String firstname = null;

  @JsonProperty("gender")
  private GenderEnum gender = null;

  @JsonProperty("groupId")
  private Integer groupId = null;

  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("kindergartenId")
  private Integer kindergartenId = null;

  @JsonProperty("lactoseIntolerant")
  private Boolean lactoseIntolerant = null;

  @JsonProperty("lastname")
  private String lastname = null;

  @JsonProperty("nativeLanguageId")
  private BigDecimal nativeLanguageId = null;

  @JsonProperty("notes")
  private String notes = null;

  @JsonProperty("numberSiblings")
  private BigDecimal numberSiblings = null;

  @JsonProperty("parents")
  @Valid
  private List<KgaParentModel> parents = null;

  @JsonProperty("religion")
  private ReligionEnum religion = null;

  @JsonProperty("withMeal")
  private Boolean withMeal = null;

  @JsonProperty("careType")
  private CareTypeEnum careType = null;

  @JsonProperty("religionNote")
  private String religionNote = null;

  @JsonProperty("careTypeNote")
  private String careTypeNote = null;

  @JsonProperty("nativeLanguageNote")
  private String nativeLanguageNote = null;

  @JsonProperty("allergiesNote")
  private String allergiesNote = null;

  @JsonProperty("diseasesNote")
  private String diseasesNote = null;

  @JsonProperty("vaccinationsNote")
  private String vaccinationsNote = null;

  @JsonProperty("secondLanguageNote")
  private String secondLanguageNote = null;

  @JsonProperty("socialSecurityNumber")
  private String socialSecurityNumber = null;

  public ChildModel allergies(List<AllergyEnum> allergies) {
    this.allergies = allergies;
    return this;
  }

  public ChildModel addAllergiesItem(AllergyEnum allergiesItem) {
    if (this.allergies == null) {
      this.allergies = new ArrayList<>();
    }
    this.allergies.add(allergiesItem);
    return this;
  }

  /**
   * Get allergies
   * @return allergies
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<AllergyEnum> getAllergies() {
    return allergies;
  }

  public void setAllergies(List<AllergyEnum> allergies) {
    this.allergies = allergies;
  }

  public ChildModel birthDate(String birthDate) {
    this.birthDate = birthDate;
    return this;
  }

  /**
   * Get birthDate
   * @return birthDate
  **/
  @ApiModelProperty(example = "1994-01-01", value = "")


  public String getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(String birthDate) {
    this.birthDate = birthDate;
  }

  public ChildModel entryDate(String entryDate) {
    this.entryDate = entryDate;
    return this;
  }

  /**
   * Get entryDate
   * @return entryDate
  **/
  @ApiModelProperty(example = "2004-01-01", value = "")


  public String getEntryDate() {
    return entryDate;
  }

  public void setEntryDate(String entryDate) {
    this.entryDate = entryDate;
  }

  public ChildModel seperationDate(String seperationDate) {
    this.seperationDate = seperationDate;
    return this;
  }

  /**
   * Get seperationDate
   * @return seperationDate
  **/
  @ApiModelProperty(example = "2014-01-01", value = "")


  public String getSeperationDate() {
    return seperationDate;
  }

  public void setSeperationDate(String seperationDate) {
    this.seperationDate = seperationDate;
  }

  public ChildModel citizenshipCountryId(BigDecimal citizenshipCountryId) {
    this.citizenshipCountryId = citizenshipCountryId;
    return this;
  }

  /**
   * Get citizenshipCountryId
   * @return citizenshipCountryId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getCitizenshipCountryId() {
    return citizenshipCountryId;
  }

  public void setCitizenshipCountryId(BigDecimal citizenshipCountryId) {
    this.citizenshipCountryId = citizenshipCountryId;
  }

  public ChildModel diseases(List<DiseasesEnum> diseases) {
    this.diseases = diseases;
    return this;
  }

  public ChildModel addDiseasesItem(DiseasesEnum diseasesItem) {
    if (this.diseases == null) {
      this.diseases = new ArrayList<>();
    }
    this.diseases.add(diseasesItem);
    return this;
  }

  /**
   * Get diseases
   * @return diseases
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<DiseasesEnum> getDiseases() {
    return diseases;
  }

  public void setDiseases(List<DiseasesEnum> diseases) {
    this.diseases = diseases;
  }

  public ChildModel secondLanguage(List<BigDecimal> secondLanguage) {
    this.secondLanguage = secondLanguage;
    return this;
  }

  public ChildModel addSecondLanguageItem(BigDecimal secondLanguageItem) {
    if (this.secondLanguage == null) {
      this.secondLanguage = new ArrayList<>();
    }
    this.secondLanguage.add(secondLanguageItem);
    return this;
  }

  /**
   * Get secondLanguage
   * @return secondLanguage
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<BigDecimal> getSecondLanguage() {
    return secondLanguage;
  }

  public void setSecondLanguage(List<BigDecimal> secondLanguage) {
    this.secondLanguage = secondLanguage;
  }

  public ChildModel vaccinations(List<VaccinationEnum> vaccinations) {
    this.vaccinations = vaccinations;
    return this;
  }

  public ChildModel addVaccinationsItem(VaccinationEnum vaccinationsItem) {
    if (this.vaccinations == null) {
      this.vaccinations = new ArrayList<>();
    }
    this.vaccinations.add(vaccinationsItem);
    return this;
  }

  /**
   * Get vaccinations
   * @return vaccinations
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<VaccinationEnum> getVaccinations() {
    return vaccinations;
  }

  public void setVaccinations(List<VaccinationEnum> vaccinations) {
    this.vaccinations = vaccinations;
  }

  public ChildModel firstname(String firstname) {
    this.firstname = firstname;
    return this;
  }

  /**
   * Get firstname
   * @return firstname
  **/
  @ApiModelProperty(example = "Peter", value = "")


  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public ChildModel gender(GenderEnum gender) {
    this.gender = gender;
    return this;
  }

  /**
   * Get gender
   * @return gender
  **/
  @ApiModelProperty(value = "")

  @Valid

  public GenderEnum getGender() {
    return gender;
  }

  public void setGender(GenderEnum gender) {
    this.gender = gender;
  }

  public ChildModel groupId(Integer groupId) {
    this.groupId = groupId;
    return this;
  }

  /**
   * Get groupId
   * @return groupId
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getGroupId() {
    return groupId;
  }

  public void setGroupId(Integer groupId) {
    this.groupId = groupId;
  }

  public ChildModel id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public ChildModel kindergartenId(Integer kindergartenId) {
    this.kindergartenId = kindergartenId;
    return this;
  }

  /**
   * Get kindergartenId
   * @return kindergartenId
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getKindergartenId() {
    return kindergartenId;
  }

  public void setKindergartenId(Integer kindergartenId) {
    this.kindergartenId = kindergartenId;
  }

  public ChildModel lactoseIntolerant(Boolean lactoseIntolerant) {
    this.lactoseIntolerant = lactoseIntolerant;
    return this;
  }

  /**
   * Get lactoseIntolerant
   * @return lactoseIntolerant
  **/
  @ApiModelProperty(example = "false", value = "")


  public Boolean getLactoseIntolerant() {
    return lactoseIntolerant;
  }

  public void setLactoseIntolerant(Boolean lactoseIntolerant) {
    this.lactoseIntolerant = lactoseIntolerant;
  }

  public ChildModel lastname(String lastname) {
    this.lastname = lastname;
    return this;
  }

  /**
   * Get lastname
   * @return lastname
  **/
  @ApiModelProperty(example = "Kohout", value = "")


  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public ChildModel nativeLanguageId(BigDecimal nativeLanguageId) {
    this.nativeLanguageId = nativeLanguageId;
    return this;
  }

  /**
   * Get nativeLanguageId
   * @return nativeLanguageId
  **/
  @ApiModelProperty(example = "1.0", value = "")

  @Valid

  public BigDecimal getNativeLanguageId() {
    return nativeLanguageId;
  }

  public void setNativeLanguageId(BigDecimal nativeLanguageId) {
    this.nativeLanguageId = nativeLanguageId;
  }

  public ChildModel notes(String notes) {
    this.notes = notes;
    return this;
  }

  /**
   * Get notes
   * @return notes
  **/
  @ApiModelProperty(example = "Peter is from carinthia", value = "")


  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public ChildModel numberSiblings(BigDecimal numberSiblings) {
    this.numberSiblings = numberSiblings;
    return this;
  }

  /**
   * Get numberSiblings
   * @return numberSiblings
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getNumberSiblings() {
    return numberSiblings;
  }

  public void setNumberSiblings(BigDecimal numberSiblings) {
    this.numberSiblings = numberSiblings;
  }

  public ChildModel parents(List<KgaParentModel> parents) {
    this.parents = parents;
    return this;
  }

  public ChildModel addParentsItem(KgaParentModel parentsItem) {
    if (this.parents == null) {
      this.parents = new ArrayList<>();
    }
    this.parents.add(parentsItem);
    return this;
  }

  /**
   * Get parents
   * @return parents
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<KgaParentModel> getParents() {
    return parents;
  }

  public void setParents(List<KgaParentModel> parents) {
    this.parents = parents;
  }

  public ChildModel religion(ReligionEnum religion) {
    this.religion = religion;
    return this;
  }

  /**
   * Get religion
   * @return religion
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ReligionEnum getReligion() {
    return religion;
  }

  public void setReligion(ReligionEnum religion) {
    this.religion = religion;
  }

  public ChildModel withMeal(Boolean withMeal) {
    this.withMeal = withMeal;
    return this;
  }

  /**
   * Get withMeal
   * @return withMeal
  **/
  @ApiModelProperty(example = "true", value = "")


  public Boolean getWithMeal() {
    return withMeal;
  }

  public void setWithMeal(Boolean withMeal) {
    this.withMeal = withMeal;
  }

  public ChildModel careType(CareTypeEnum careType) {
    this.careType = careType;
    return this;
  }

  /**
   * Get careType
   * @return careType
  **/
  @ApiModelProperty(value = "")

  @Valid

  public CareTypeEnum getCareType() {
    return careType;
  }

  public void setCareType(CareTypeEnum careType) {
    this.careType = careType;
  }

  public ChildModel religionNote(String religionNote) {
    this.religionNote = religionNote;
    return this;
  }

  /**
   * Get religionNote
   * @return religionNote
  **/
  @ApiModelProperty(example = "Peter is from carinthia", value = "")


  public String getReligionNote() {
    return religionNote;
  }

  public void setReligionNote(String religionNote) {
    this.religionNote = religionNote;
  }

  public ChildModel careTypeNote(String careTypeNote) {
    this.careTypeNote = careTypeNote;
    return this;
  }

  /**
   * Get careTypeNote
   * @return careTypeNote
  **/
  @ApiModelProperty(example = "Peter is from carinthia", value = "")


  public String getCareTypeNote() {
    return careTypeNote;
  }

  public void setCareTypeNote(String careTypeNote) {
    this.careTypeNote = careTypeNote;
  }

  public ChildModel nativeLanguageNote(String nativeLanguageNote) {
    this.nativeLanguageNote = nativeLanguageNote;
    return this;
  }

  /**
   * Get nativeLanguageNote
   * @return nativeLanguageNote
  **/
  @ApiModelProperty(example = "Peter is from carinthia", value = "")


  public String getNativeLanguageNote() {
    return nativeLanguageNote;
  }

  public void setNativeLanguageNote(String nativeLanguageNote) {
    this.nativeLanguageNote = nativeLanguageNote;
  }

  public ChildModel allergiesNote(String allergiesNote) {
    this.allergiesNote = allergiesNote;
    return this;
  }

  /**
   * Get allergiesNote
   * @return allergiesNote
  **/
  @ApiModelProperty(example = "Peter is from carinthia", value = "")


  public String getAllergiesNote() {
    return allergiesNote;
  }

  public void setAllergiesNote(String allergiesNote) {
    this.allergiesNote = allergiesNote;
  }

  public ChildModel diseasesNote(String diseasesNote) {
    this.diseasesNote = diseasesNote;
    return this;
  }

  /**
   * Get diseasesNote
   * @return diseasesNote
  **/
  @ApiModelProperty(example = "Peter is from carinthia", value = "")


  public String getDiseasesNote() {
    return diseasesNote;
  }

  public void setDiseasesNote(String diseasesNote) {
    this.diseasesNote = diseasesNote;
  }

  public ChildModel vaccinationsNote(String vaccinationsNote) {
    this.vaccinationsNote = vaccinationsNote;
    return this;
  }

  /**
   * Get vaccinationsNote
   * @return vaccinationsNote
  **/
  @ApiModelProperty(example = "Peter is from carinthia", value = "")


  public String getVaccinationsNote() {
    return vaccinationsNote;
  }

  public void setVaccinationsNote(String vaccinationsNote) {
    this.vaccinationsNote = vaccinationsNote;
  }

  public ChildModel secondLanguageNote(String secondLanguageNote) {
    this.secondLanguageNote = secondLanguageNote;
    return this;
  }

  /**
   * Get secondLanguageNote
   * @return secondLanguageNote
  **/
  @ApiModelProperty(example = "Peter is from carinthia", value = "")


  public String getSecondLanguageNote() {
    return secondLanguageNote;
  }

  public void setSecondLanguageNote(String secondLanguageNote) {
    this.secondLanguageNote = secondLanguageNote;
  }

  public ChildModel socialSecurityNumber(String socialSecurityNumber) {
    this.socialSecurityNumber = socialSecurityNumber;
    return this;
  }

  /**
   * Get socialSecurityNumber
   * @return socialSecurityNumber
  **/
  @ApiModelProperty(example = "123123123", value = "")


  public String getSocialSecurityNumber() {
    return socialSecurityNumber;
  }

  public void setSocialSecurityNumber(String socialSecurityNumber) {
    this.socialSecurityNumber = socialSecurityNumber;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChildModel childModel = (ChildModel) o;
    return Objects.equals(this.allergies, childModel.allergies) &&
        Objects.equals(this.birthDate, childModel.birthDate) &&
        Objects.equals(this.entryDate, childModel.entryDate) &&
        Objects.equals(this.seperationDate, childModel.seperationDate) &&
        Objects.equals(this.citizenshipCountryId, childModel.citizenshipCountryId) &&
        Objects.equals(this.diseases, childModel.diseases) &&
        Objects.equals(this.secondLanguage, childModel.secondLanguage) &&
        Objects.equals(this.vaccinations, childModel.vaccinations) &&
        Objects.equals(this.firstname, childModel.firstname) &&
        Objects.equals(this.gender, childModel.gender) &&
        Objects.equals(this.groupId, childModel.groupId) &&
        Objects.equals(this.id, childModel.id) &&
        Objects.equals(this.kindergartenId, childModel.kindergartenId) &&
        Objects.equals(this.lactoseIntolerant, childModel.lactoseIntolerant) &&
        Objects.equals(this.lastname, childModel.lastname) &&
        Objects.equals(this.nativeLanguageId, childModel.nativeLanguageId) &&
        Objects.equals(this.notes, childModel.notes) &&
        Objects.equals(this.numberSiblings, childModel.numberSiblings) &&
        Objects.equals(this.parents, childModel.parents) &&
        Objects.equals(this.religion, childModel.religion) &&
        Objects.equals(this.withMeal, childModel.withMeal) &&
        Objects.equals(this.careType, childModel.careType) &&
        Objects.equals(this.religionNote, childModel.religionNote) &&
        Objects.equals(this.careTypeNote, childModel.careTypeNote) &&
        Objects.equals(this.nativeLanguageNote, childModel.nativeLanguageNote) &&
        Objects.equals(this.allergiesNote, childModel.allergiesNote) &&
        Objects.equals(this.diseasesNote, childModel.diseasesNote) &&
        Objects.equals(this.vaccinationsNote, childModel.vaccinationsNote) &&
        Objects.equals(this.secondLanguageNote, childModel.secondLanguageNote) &&
        Objects.equals(this.socialSecurityNumber, childModel.socialSecurityNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(allergies, birthDate, entryDate, seperationDate, citizenshipCountryId, diseases, secondLanguage, vaccinations, firstname, gender, groupId, id, kindergartenId, lactoseIntolerant, lastname, nativeLanguageId, notes, numberSiblings, parents, religion, withMeal, careType, religionNote, careTypeNote, nativeLanguageNote, allergiesNote, diseasesNote, vaccinationsNote, secondLanguageNote, socialSecurityNumber);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChildModel {\n");
    
    sb.append("    allergies: ").append(toIndentedString(allergies)).append("\n");
    sb.append("    birthDate: ").append(toIndentedString(birthDate)).append("\n");
    sb.append("    entryDate: ").append(toIndentedString(entryDate)).append("\n");
    sb.append("    seperationDate: ").append(toIndentedString(seperationDate)).append("\n");
    sb.append("    citizenshipCountryId: ").append(toIndentedString(citizenshipCountryId)).append("\n");
    sb.append("    diseases: ").append(toIndentedString(diseases)).append("\n");
    sb.append("    secondLanguage: ").append(toIndentedString(secondLanguage)).append("\n");
    sb.append("    vaccinations: ").append(toIndentedString(vaccinations)).append("\n");
    sb.append("    firstname: ").append(toIndentedString(firstname)).append("\n");
    sb.append("    gender: ").append(toIndentedString(gender)).append("\n");
    sb.append("    groupId: ").append(toIndentedString(groupId)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    kindergartenId: ").append(toIndentedString(kindergartenId)).append("\n");
    sb.append("    lactoseIntolerant: ").append(toIndentedString(lactoseIntolerant)).append("\n");
    sb.append("    lastname: ").append(toIndentedString(lastname)).append("\n");
    sb.append("    nativeLanguageId: ").append(toIndentedString(nativeLanguageId)).append("\n");
    sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
    sb.append("    numberSiblings: ").append(toIndentedString(numberSiblings)).append("\n");
    sb.append("    parents: ").append(toIndentedString(parents)).append("\n");
    sb.append("    religion: ").append(toIndentedString(religion)).append("\n");
    sb.append("    withMeal: ").append(toIndentedString(withMeal)).append("\n");
    sb.append("    careType: ").append(toIndentedString(careType)).append("\n");
    sb.append("    religionNote: ").append(toIndentedString(religionNote)).append("\n");
    sb.append("    careTypeNote: ").append(toIndentedString(careTypeNote)).append("\n");
    sb.append("    nativeLanguageNote: ").append(toIndentedString(nativeLanguageNote)).append("\n");
    sb.append("    allergiesNote: ").append(toIndentedString(allergiesNote)).append("\n");
    sb.append("    diseasesNote: ").append(toIndentedString(diseasesNote)).append("\n");
    sb.append("    vaccinationsNote: ").append(toIndentedString(vaccinationsNote)).append("\n");
    sb.append("    secondLanguageNote: ").append(toIndentedString(secondLanguageNote)).append("\n");
    sb.append("    socialSecurityNumber: ").append(toIndentedString(socialSecurityNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

