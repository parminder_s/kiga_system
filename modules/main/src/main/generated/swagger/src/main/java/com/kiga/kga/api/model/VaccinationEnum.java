package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets VaccinationEnum
 */
public enum VaccinationEnum {
  
  ROTAVIRUS("ROTAVIRUS"),
  
  DIPHTHERIE("DIPHTHERIE"),
  
  TETANUS("TETANUS"),
  
  PERTUSSIS("PERTUSSIS"),
  
  POLIOMYELITIS("POLIOMYELITIS"),
  
  HAEMOHILUS_INFLUENZA_B("HAEMOHILUS_INFLUENZA_B"),
  
  HEPATITIS_B("HEPATITIS_B"),
  
  PNEUMOKOKKEN("PNEUMOKOKKEN"),
  
  MASERN_MUMPS_ROTELN("MASERN_MUMPS_ROTELN"),
  
  FSME("FSME"),
  
  ACWY("ACWY"),
  
  HUMANE_PAPILLONMAVIREN("HUMANE_PAPILLONMAVIREN"),
  
  MENINGOKOKKEN_B("MENINGOKOKKEN_B"),
  
  MENINGOKOKKEN_C("MENINGOKOKKEN_C"),
  
  VARIZELLEN("VARIZELLEN"),
  
  HEPATITIS_A("HEPATITIS_A"),
  
  INFLUENZA("INFLUENZA"),
  
  HERPES_ZOSTA("HERPES_ZOSTA"),
  
  OTHER("OTHER");

  private Object value;

  VaccinationEnum(Object value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static VaccinationEnum fromValue(String text) {
    for (VaccinationEnum b : VaccinationEnum.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + text + "'");
  }
}

