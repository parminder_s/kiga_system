package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets ReligionEnum
 */
public enum ReligionEnum {
  
  CATHOLIC("CATHOLIC"),
  
  PROTESTANT("PROTESTANT"),
  
  ISLAM("ISLAM"),
  
  ATHEIST("ATHEIST"),
  
  OTHER("OTHER");

  private String value;

  ReligionEnum(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ReligionEnum fromValue(String text) {
    for (ReligionEnum b : ReligionEnum.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + text + "'");
  }
}

