package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * YearModel
 */

public class YearModel   {
  @JsonProperty("archived")
  private Boolean archived = null;

  @JsonProperty("endDate")
  private String endDate = null;

  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("kindergartenId")
  private BigDecimal kindergartenId = null;

  @JsonProperty("startDate")
  private String startDate = null;

  public YearModel archived(Boolean archived) {
    this.archived = archived;
    return this;
  }

  /**
   * Get archived
   * @return archived
  **/
  @ApiModelProperty(example = "false", value = "")


  public Boolean getArchived() {
    return archived;
  }

  public void setArchived(Boolean archived) {
    this.archived = archived;
  }

  public YearModel endDate(String endDate) {
    this.endDate = endDate;
    return this;
  }

  /**
   * Get endDate
   * @return endDate
  **/
  @ApiModelProperty(example = "2017-12-31", value = "")


  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public YearModel id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public YearModel kindergartenId(BigDecimal kindergartenId) {
    this.kindergartenId = kindergartenId;
    return this;
  }

  /**
   * Get kindergartenId
   * @return kindergartenId
  **/
  @ApiModelProperty(example = "1.0", value = "")

  @Valid

  public BigDecimal getKindergartenId() {
    return kindergartenId;
  }

  public void setKindergartenId(BigDecimal kindergartenId) {
    this.kindergartenId = kindergartenId;
  }

  public YearModel startDate(String startDate) {
    this.startDate = startDate;
    return this;
  }

  /**
   * Get startDate
   * @return startDate
  **/
  @ApiModelProperty(example = "2017-01-01", value = "")


  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    YearModel yearModel = (YearModel) o;
    return Objects.equals(this.archived, yearModel.archived) &&
        Objects.equals(this.endDate, yearModel.endDate) &&
        Objects.equals(this.id, yearModel.id) &&
        Objects.equals(this.kindergartenId, yearModel.kindergartenId) &&
        Objects.equals(this.startDate, yearModel.startDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(archived, endDate, id, kindergartenId, startDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class YearModel {\n");
    
    sb.append("    archived: ").append(toIndentedString(archived)).append("\n");
    sb.append("    endDate: ").append(toIndentedString(endDate)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    kindergartenId: ").append(toIndentedString(kindergartenId)).append("\n");
    sb.append("    startDate: ").append(toIndentedString(startDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

