package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.kiga.kga.api.model.ChildModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * KgaGroupModel
 */

public class KgaGroupModel   {
  @JsonProperty("adminEmail")
  private String adminEmail = null;

  @JsonProperty("children")
  @Valid
  private List<ChildModel> children = null;

  @JsonProperty("email")
  private String email = null;

  @JsonProperty("phoneNumber")
  private String phoneNumber = null;

  @JsonProperty("notes")
  private String notes = null;

  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("room")
  private String room = null;

  @JsonProperty("secondAdminEmail")
  private String secondAdminEmail = null;

  @JsonProperty("yearId")
  private Integer yearId = null;

  @JsonProperty("countChildren")
  private Integer countChildren = null;

  public KgaGroupModel adminEmail(String adminEmail) {
    this.adminEmail = adminEmail;
    return this;
  }

  /**
   * Get adminEmail
   * @return adminEmail
  **/
  @ApiModelProperty(example = "admin@website.at", value = "")


  public String getAdminEmail() {
    return adminEmail;
  }

  public void setAdminEmail(String adminEmail) {
    this.adminEmail = adminEmail;
  }

  public KgaGroupModel children(List<ChildModel> children) {
    this.children = children;
    return this;
  }

  public KgaGroupModel addChildrenItem(ChildModel childrenItem) {
    if (this.children == null) {
      this.children = new ArrayList<>();
    }
    this.children.add(childrenItem);
    return this;
  }

  /**
   * Get children
   * @return children
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ChildModel> getChildren() {
    return children;
  }

  public void setChildren(List<ChildModel> children) {
    this.children = children;
  }

  public KgaGroupModel email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(example = "group@website.at", value = "")


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public KgaGroupModel phoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

  /**
   * Get phoneNumber
   * @return phoneNumber
  **/
  @ApiModelProperty(example = "group@website.at", value = "")


  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public KgaGroupModel notes(String notes) {
    this.notes = notes;
    return this;
  }

  /**
   * Get notes
   * @return notes
  **/
  @ApiModelProperty(example = "group@website.at", value = "")


  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public KgaGroupModel id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public KgaGroupModel name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "Schmetterlinge", value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public KgaGroupModel room(String room) {
    this.room = room;
    return this;
  }

  /**
   * Get room
   * @return room
  **/
  @ApiModelProperty(example = "Room 123", value = "")


  public String getRoom() {
    return room;
  }

  public void setRoom(String room) {
    this.room = room;
  }

  public KgaGroupModel secondAdminEmail(String secondAdminEmail) {
    this.secondAdminEmail = secondAdminEmail;
    return this;
  }

  /**
   * Get secondAdminEmail
   * @return secondAdminEmail
  **/
  @ApiModelProperty(example = "admin1@website.at", value = "")


  public String getSecondAdminEmail() {
    return secondAdminEmail;
  }

  public void setSecondAdminEmail(String secondAdminEmail) {
    this.secondAdminEmail = secondAdminEmail;
  }

  public KgaGroupModel yearId(Integer yearId) {
    this.yearId = yearId;
    return this;
  }

  /**
   * Get yearId
   * @return yearId
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getYearId() {
    return yearId;
  }

  public void setYearId(Integer yearId) {
    this.yearId = yearId;
  }

  public KgaGroupModel countChildren(Integer countChildren) {
    this.countChildren = countChildren;
    return this;
  }

  /**
   * Get countChildren
   * @return countChildren
  **/
  @ApiModelProperty(example = "15", value = "")


  public Integer getCountChildren() {
    return countChildren;
  }

  public void setCountChildren(Integer countChildren) {
    this.countChildren = countChildren;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KgaGroupModel kgaGroupModel = (KgaGroupModel) o;
    return Objects.equals(this.adminEmail, kgaGroupModel.adminEmail) &&
        Objects.equals(this.children, kgaGroupModel.children) &&
        Objects.equals(this.email, kgaGroupModel.email) &&
        Objects.equals(this.phoneNumber, kgaGroupModel.phoneNumber) &&
        Objects.equals(this.notes, kgaGroupModel.notes) &&
        Objects.equals(this.id, kgaGroupModel.id) &&
        Objects.equals(this.name, kgaGroupModel.name) &&
        Objects.equals(this.room, kgaGroupModel.room) &&
        Objects.equals(this.secondAdminEmail, kgaGroupModel.secondAdminEmail) &&
        Objects.equals(this.yearId, kgaGroupModel.yearId) &&
        Objects.equals(this.countChildren, kgaGroupModel.countChildren);
  }

  @Override
  public int hashCode() {
    return Objects.hash(adminEmail, children, email, phoneNumber, notes, id, name, room, secondAdminEmail, yearId, countChildren);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class KgaGroupModel {\n");
    
    sb.append("    adminEmail: ").append(toIndentedString(adminEmail)).append("\n");
    sb.append("    children: ").append(toIndentedString(children)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
    sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    room: ").append(toIndentedString(room)).append("\n");
    sb.append("    secondAdminEmail: ").append(toIndentedString(secondAdminEmail)).append("\n");
    sb.append("    yearId: ").append(toIndentedString(yearId)).append("\n");
    sb.append("    countChildren: ").append(toIndentedString(countChildren)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

