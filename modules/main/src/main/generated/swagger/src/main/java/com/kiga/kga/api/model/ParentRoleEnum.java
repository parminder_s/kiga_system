package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets ParentRoleEnum
 */
public enum ParentRoleEnum {
  
  MOTHER("MOTHER"),
  
  FATHER("FATHER"),
  
  GRAND_FATHER("GRAND_FATHER"),
  
  GRAND_MOTHER("GRAND_MOTHER"),
  
  SIGNIFICANT_OTHER("SIGNIFICANT_OTHER"),
  
  OTHER("OTHER");

  private String value;

  ParentRoleEnum(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ParentRoleEnum fromValue(String text) {
    for (ParentRoleEnum b : ParentRoleEnum.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + text + "'");
  }
}

