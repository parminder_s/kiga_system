package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets EmployeeTypeEnum
 */
public enum EmployeeTypeEnum {
  
  EDUCATOR("EDUCATOR"),
  
  CHILD_CARE_WORKER("CHILD_CARE_WORKER"),
  
  OTHER("OTHER");

  private String value;

  EmployeeTypeEnum(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static EmployeeTypeEnum fromValue(String text) {
    for (EmployeeTypeEnum b : EmployeeTypeEnum.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + text + "'");
  }
}

