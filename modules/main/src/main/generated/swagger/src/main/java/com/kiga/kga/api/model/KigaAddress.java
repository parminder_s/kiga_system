package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * KigaAddress
 */

public class KigaAddress   {
  @JsonProperty("city")
  private String city = null;

  @JsonProperty("countryId")
  private BigDecimal countryId = null;

  @JsonProperty("doorNumber")
  private BigDecimal doorNumber = null;

  @JsonProperty("floor")
  private BigDecimal floor = null;

  @JsonProperty("houseNumber")
  private String houseNumber = null;

  @JsonProperty("street")
  private String street = null;

  @JsonProperty("zipCode")
  private BigDecimal zipCode = null;

  public KigaAddress city(String city) {
    this.city = city;
    return this;
  }

  /**
   * Get city
   * @return city
  **/
  @ApiModelProperty(example = "Poggersdorf", value = "")


  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public KigaAddress countryId(BigDecimal countryId) {
    this.countryId = countryId;
    return this;
  }

  /**
   * Get countryId
   * @return countryId
  **/
  @ApiModelProperty(example = "1.0", value = "")

  @Valid

  public BigDecimal getCountryId() {
    return countryId;
  }

  public void setCountryId(BigDecimal countryId) {
    this.countryId = countryId;
  }

  public KigaAddress doorNumber(BigDecimal doorNumber) {
    this.doorNumber = doorNumber;
    return this;
  }

  /**
   * Get doorNumber
   * @return doorNumber
  **/
  @ApiModelProperty(example = "17.0", value = "")

  @Valid

  public BigDecimal getDoorNumber() {
    return doorNumber;
  }

  public void setDoorNumber(BigDecimal doorNumber) {
    this.doorNumber = doorNumber;
  }

  public KigaAddress floor(BigDecimal floor) {
    this.floor = floor;
    return this;
  }

  /**
   * Get floor
   * @return floor
  **/
  @ApiModelProperty(example = "3.0", value = "")

  @Valid

  public BigDecimal getFloor() {
    return floor;
  }

  public void setFloor(BigDecimal floor) {
    this.floor = floor;
  }

  public KigaAddress houseNumber(String houseNumber) {
    this.houseNumber = houseNumber;
    return this;
  }

  /**
   * Get houseNumber
   * @return houseNumber
  **/
  @ApiModelProperty(example = "4", value = "")


  public String getHouseNumber() {
    return houseNumber;
  }

  public void setHouseNumber(String houseNumber) {
    this.houseNumber = houseNumber;
  }

  public KigaAddress street(String street) {
    this.street = street;
    return this;
  }

  /**
   * Get street
   * @return street
  **/
  @ApiModelProperty(example = "Schleusenweg", value = "")


  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public KigaAddress zipCode(BigDecimal zipCode) {
    this.zipCode = zipCode;
    return this;
  }

  /**
   * Get zipCode
   * @return zipCode
  **/
  @ApiModelProperty(example = "9130.0", value = "")

  @Valid

  public BigDecimal getZipCode() {
    return zipCode;
  }

  public void setZipCode(BigDecimal zipCode) {
    this.zipCode = zipCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KigaAddress kigaAddress = (KigaAddress) o;
    return Objects.equals(this.city, kigaAddress.city) &&
        Objects.equals(this.countryId, kigaAddress.countryId) &&
        Objects.equals(this.doorNumber, kigaAddress.doorNumber) &&
        Objects.equals(this.floor, kigaAddress.floor) &&
        Objects.equals(this.houseNumber, kigaAddress.houseNumber) &&
        Objects.equals(this.street, kigaAddress.street) &&
        Objects.equals(this.zipCode, kigaAddress.zipCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(city, countryId, doorNumber, floor, houseNumber, street, zipCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class KigaAddress {\n");
    
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    countryId: ").append(toIndentedString(countryId)).append("\n");
    sb.append("    doorNumber: ").append(toIndentedString(doorNumber)).append("\n");
    sb.append("    floor: ").append(toIndentedString(floor)).append("\n");
    sb.append("    houseNumber: ").append(toIndentedString(houseNumber)).append("\n");
    sb.append("    street: ").append(toIndentedString(street)).append("\n");
    sb.append("    zipCode: ").append(toIndentedString(zipCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

