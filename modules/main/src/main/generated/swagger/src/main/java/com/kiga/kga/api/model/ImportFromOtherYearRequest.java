package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ImportFromOtherYearRequest
 */

public class ImportFromOtherYearRequest   {
  @JsonProperty("groupIds")
  @Valid
  private List<Integer> groupIds = null;

  @JsonProperty("yearId")
  private Integer yearId = null;

  public ImportFromOtherYearRequest groupIds(List<Integer> groupIds) {
    this.groupIds = groupIds;
    return this;
  }

  public ImportFromOtherYearRequest addGroupIdsItem(Integer groupIdsItem) {
    if (this.groupIds == null) {
      this.groupIds = new ArrayList<>();
    }
    this.groupIds.add(groupIdsItem);
    return this;
  }

  /**
   * Get groupIds
   * @return groupIds
  **/
  @ApiModelProperty(value = "")


  public List<Integer> getGroupIds() {
    return groupIds;
  }

  public void setGroupIds(List<Integer> groupIds) {
    this.groupIds = groupIds;
  }

  public ImportFromOtherYearRequest yearId(Integer yearId) {
    this.yearId = yearId;
    return this;
  }

  /**
   * Get yearId
   * @return yearId
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getYearId() {
    return yearId;
  }

  public void setYearId(Integer yearId) {
    this.yearId = yearId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ImportFromOtherYearRequest importFromOtherYearRequest = (ImportFromOtherYearRequest) o;
    return Objects.equals(this.groupIds, importFromOtherYearRequest.groupIds) &&
        Objects.equals(this.yearId, importFromOtherYearRequest.yearId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(groupIds, yearId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ImportFromOtherYearRequest {\n");
    
    sb.append("    groupIds: ").append(toIndentedString(groupIds)).append("\n");
    sb.append("    yearId: ").append(toIndentedString(yearId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

