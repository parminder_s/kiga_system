package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.kiga.kga.api.model.EmploymentContractEnum;
import com.kiga.kga.api.model.ParentRoleEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * KgaParentModel
 */

public class KgaParentModel   {
  @JsonProperty("firstname")
  private String firstname = null;

  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("lastname")
  private String lastname = null;

  @JsonProperty("phoneNumber")
  private String phoneNumber = null;

  @JsonProperty("email")
  private String email = null;

  @JsonProperty("legalGuardian")
  private Boolean legalGuardian = null;

  @JsonProperty("pickupAuthorized")
  private Boolean pickupAuthorized = null;

  @JsonProperty("emergencyAuthorized")
  private Boolean emergencyAuthorized = null;

  @JsonProperty("employment")
  private EmploymentContractEnum employment = null;

  @JsonProperty("role")
  private ParentRoleEnum role = null;

  public KgaParentModel firstname(String firstname) {
    this.firstname = firstname;
    return this;
  }

  /**
   * Get firstname
   * @return firstname
  **/
  @ApiModelProperty(example = "Evelyn", value = "")


  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public KgaParentModel id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "0", value = "")


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public KgaParentModel lastname(String lastname) {
    this.lastname = lastname;
    return this;
  }

  /**
   * Get lastname
   * @return lastname
  **/
  @ApiModelProperty(example = "Kohout", value = "")


  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public KgaParentModel phoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

  /**
   * Get phoneNumber
   * @return phoneNumber
  **/
  @ApiModelProperty(example = "0660123123", value = "")


  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public KgaParentModel email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(example = "0660123123", value = "")


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public KgaParentModel legalGuardian(Boolean legalGuardian) {
    this.legalGuardian = legalGuardian;
    return this;
  }

  /**
   * Get legalGuardian
   * @return legalGuardian
  **/
  @ApiModelProperty(example = "false", value = "")


  public Boolean getLegalGuardian() {
    return legalGuardian;
  }

  public void setLegalGuardian(Boolean legalGuardian) {
    this.legalGuardian = legalGuardian;
  }

  public KgaParentModel pickupAuthorized(Boolean pickupAuthorized) {
    this.pickupAuthorized = pickupAuthorized;
    return this;
  }

  /**
   * Get pickupAuthorized
   * @return pickupAuthorized
  **/
  @ApiModelProperty(example = "false", value = "")


  public Boolean getPickupAuthorized() {
    return pickupAuthorized;
  }

  public void setPickupAuthorized(Boolean pickupAuthorized) {
    this.pickupAuthorized = pickupAuthorized;
  }

  public KgaParentModel emergencyAuthorized(Boolean emergencyAuthorized) {
    this.emergencyAuthorized = emergencyAuthorized;
    return this;
  }

  /**
   * Get emergencyAuthorized
   * @return emergencyAuthorized
  **/
  @ApiModelProperty(example = "false", value = "")


  public Boolean getEmergencyAuthorized() {
    return emergencyAuthorized;
  }

  public void setEmergencyAuthorized(Boolean emergencyAuthorized) {
    this.emergencyAuthorized = emergencyAuthorized;
  }

  public KgaParentModel employment(EmploymentContractEnum employment) {
    this.employment = employment;
    return this;
  }

  /**
   * Get employment
   * @return employment
  **/
  @ApiModelProperty(value = "")

  @Valid

  public EmploymentContractEnum getEmployment() {
    return employment;
  }

  public void setEmployment(EmploymentContractEnum employment) {
    this.employment = employment;
  }

  public KgaParentModel role(ParentRoleEnum role) {
    this.role = role;
    return this;
  }

  /**
   * Get role
   * @return role
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ParentRoleEnum getRole() {
    return role;
  }

  public void setRole(ParentRoleEnum role) {
    this.role = role;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KgaParentModel kgaParentModel = (KgaParentModel) o;
    return Objects.equals(this.firstname, kgaParentModel.firstname) &&
        Objects.equals(this.id, kgaParentModel.id) &&
        Objects.equals(this.lastname, kgaParentModel.lastname) &&
        Objects.equals(this.phoneNumber, kgaParentModel.phoneNumber) &&
        Objects.equals(this.email, kgaParentModel.email) &&
        Objects.equals(this.legalGuardian, kgaParentModel.legalGuardian) &&
        Objects.equals(this.pickupAuthorized, kgaParentModel.pickupAuthorized) &&
        Objects.equals(this.emergencyAuthorized, kgaParentModel.emergencyAuthorized) &&
        Objects.equals(this.employment, kgaParentModel.employment) &&
        Objects.equals(this.role, kgaParentModel.role);
  }

  @Override
  public int hashCode() {
    return Objects.hash(firstname, id, lastname, phoneNumber, email, legalGuardian, pickupAuthorized, emergencyAuthorized, employment, role);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class KgaParentModel {\n");
    
    sb.append("    firstname: ").append(toIndentedString(firstname)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    lastname: ").append(toIndentedString(lastname)).append("\n");
    sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    legalGuardian: ").append(toIndentedString(legalGuardian)).append("\n");
    sb.append("    pickupAuthorized: ").append(toIndentedString(pickupAuthorized)).append("\n");
    sb.append("    emergencyAuthorized: ").append(toIndentedString(emergencyAuthorized)).append("\n");
    sb.append("    employment: ").append(toIndentedString(employment)).append("\n");
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

