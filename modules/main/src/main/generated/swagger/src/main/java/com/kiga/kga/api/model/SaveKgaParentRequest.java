package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.kiga.kga.api.model.KgaParentModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SaveKgaParentRequest
 */

public class SaveKgaParentRequest   {
  @JsonProperty("childId")
  private BigDecimal childId = null;

  @JsonProperty("parent")
  private KgaParentModel parent = null;

  public SaveKgaParentRequest childId(BigDecimal childId) {
    this.childId = childId;
    return this;
  }

  /**
   * Get childId
   * @return childId
  **/
  @ApiModelProperty(example = "1.0", value = "")

  @Valid

  public BigDecimal getChildId() {
    return childId;
  }

  public void setChildId(BigDecimal childId) {
    this.childId = childId;
  }

  public SaveKgaParentRequest parent(KgaParentModel parent) {
    this.parent = parent;
    return this;
  }

  /**
   * Get parent
   * @return parent
  **/
  @ApiModelProperty(value = "")

  @Valid

  public KgaParentModel getParent() {
    return parent;
  }

  public void setParent(KgaParentModel parent) {
    this.parent = parent;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SaveKgaParentRequest saveKgaParentRequest = (SaveKgaParentRequest) o;
    return Objects.equals(this.childId, saveKgaParentRequest.childId) &&
        Objects.equals(this.parent, saveKgaParentRequest.parent);
  }

  @Override
  public int hashCode() {
    return Objects.hash(childId, parent);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SaveKgaParentRequest {\n");
    
    sb.append("    childId: ").append(toIndentedString(childId)).append("\n");
    sb.append("    parent: ").append(toIndentedString(parent)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

