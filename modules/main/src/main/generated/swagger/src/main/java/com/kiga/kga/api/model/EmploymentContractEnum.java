package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets EmploymentContractEnum
 */
public enum EmploymentContractEnum {
  
  FULLTIME("FULLTIME"),
  
  PARTTIME("PARTTIME"),
  
  NOTEMPLOYT("NOTEMPLOYT");

  private String value;

  EmploymentContractEnum(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static EmploymentContractEnum fromValue(String text) {
    for (EmploymentContractEnum b : EmploymentContractEnum.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + text + "'");
  }
}

