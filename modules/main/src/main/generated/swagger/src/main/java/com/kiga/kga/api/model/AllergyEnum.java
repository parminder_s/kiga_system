package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets AllergyEnum
 */
public enum AllergyEnum {
  
  PEANUT("PEANUT"),
  
  EGG("EGG"),
  
  FISH("FISH"),
  
  GLUTEN("GLUTEN"),
  
  SHELLFISH("SHELLFISH"),
  
  LUPINEN("LUPINEN"),
  
  MILK("MILK"),
  
  NUTS("NUTS"),
  
  BEE("BEE"),
  
  POLLEN("POLLEN"),
  
  GRASS("GRASS"),
  
  LATEX("LATEX"),
  
  OTHER("OTHER");

  private Object value;

  AllergyEnum(Object value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static AllergyEnum fromValue(String text) {
    for (AllergyEnum b : AllergyEnum.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + text + "'");
  }
}

