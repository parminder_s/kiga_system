package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ChildJournalFileRequest
 */

public class ChildJournalFileRequest   {
  @JsonProperty("childId")
  private Integer childId = null;

  @JsonProperty("fileId")
  private Integer fileId = null;

  public ChildJournalFileRequest childId(Integer childId) {
    this.childId = childId;
    return this;
  }

  /**
   * Get childId
   * @return childId
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getChildId() {
    return childId;
  }

  public void setChildId(Integer childId) {
    this.childId = childId;
  }

  public ChildJournalFileRequest fileId(Integer fileId) {
    this.fileId = fileId;
    return this;
  }

  /**
   * Get fileId
   * @return fileId
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getFileId() {
    return fileId;
  }

  public void setFileId(Integer fileId) {
    this.fileId = fileId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChildJournalFileRequest childJournalFileRequest = (ChildJournalFileRequest) o;
    return Objects.equals(this.childId, childJournalFileRequest.childId) &&
        Objects.equals(this.fileId, childJournalFileRequest.fileId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(childId, fileId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChildJournalFileRequest {\n");
    
    sb.append("    childId: ").append(toIndentedString(childId)).append("\n");
    sb.append("    fileId: ").append(toIndentedString(fileId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

