package com.kiga.kga.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets EmploymentTypeEnum
 */
public enum EmploymentTypeEnum {
  
  EDUCATOR("EDUCATOR"),
  
  CLEANER("CLEANER"),
  
  CHILD_CARE_WORKER("CHILD_CARE_WORKER"),
  
  DAY_NANNY("DAY_NANNY"),
  
  DAY_DADDY("DAY_DADDY"),
  
  COMMUNITY_SERVICE("COMMUNITY_SERVICE"),
  
  OTHER("OTHER");

  private String value;

  EmploymentTypeEnum(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static EmploymentTypeEnum fromValue(String text) {
    for (EmploymentTypeEnum b : EmploymentTypeEnum.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + text + "'");
  }
}

