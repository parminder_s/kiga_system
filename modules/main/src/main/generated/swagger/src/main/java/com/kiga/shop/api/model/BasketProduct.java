package com.kiga.shop.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * BasketProduct
 */
@Validated

public class BasketProduct   {
  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("productId")
  private Integer productId = null;

  @JsonProperty("title")
  private String title = null;

  @JsonProperty("amount")
  private Integer amount = null;

  @JsonProperty("price")
  private BigDecimal price = null;

  @JsonProperty("originalPrice")
  private BigDecimal originalPrice = null;

  @JsonProperty("sum")
  private BigDecimal sum = null;

  @JsonProperty("originalSum")
  private BigDecimal originalSum = null;

  @JsonProperty("minDeliveryDays")
  private Integer minDeliveryDays = null;

  @JsonProperty("maxDeliveryDays")
  private Integer maxDeliveryDays = null;

  @JsonProperty("hasDiscount")
  private Boolean hasDiscount = null;

  public BasketProduct id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public BasketProduct productId(Integer productId) {
    this.productId = productId;
    return this;
  }

  /**
   * Get productId
   * @return productId
  **/
  @ApiModelProperty(value = "")


  public Integer getProductId() {
    return productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public BasketProduct title(String title) {
    this.title = title;
    return this;
  }

  /**
   * Get title
   * @return title
  **/
  @ApiModelProperty(value = "")


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public BasketProduct amount(Integer amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Get amount
   * @return amount
  **/
  @ApiModelProperty(value = "")


  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public BasketProduct price(BigDecimal price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BasketProduct originalPrice(BigDecimal originalPrice) {
    this.originalPrice = originalPrice;
    return this;
  }

  /**
   * Get originalPrice
   * @return originalPrice
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getOriginalPrice() {
    return originalPrice;
  }

  public void setOriginalPrice(BigDecimal originalPrice) {
    this.originalPrice = originalPrice;
  }

  public BasketProduct sum(BigDecimal sum) {
    this.sum = sum;
    return this;
  }

  /**
   * Get sum
   * @return sum
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getSum() {
    return sum;
  }

  public void setSum(BigDecimal sum) {
    this.sum = sum;
  }

  public BasketProduct originalSum(BigDecimal originalSum) {
    this.originalSum = originalSum;
    return this;
  }

  /**
   * Get originalSum
   * @return originalSum
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getOriginalSum() {
    return originalSum;
  }

  public void setOriginalSum(BigDecimal originalSum) {
    this.originalSum = originalSum;
  }

  public BasketProduct minDeliveryDays(Integer minDeliveryDays) {
    this.minDeliveryDays = minDeliveryDays;
    return this;
  }

  /**
   * Get minDeliveryDays
   * @return minDeliveryDays
  **/
  @ApiModelProperty(value = "")


  public Integer getMinDeliveryDays() {
    return minDeliveryDays;
  }

  public void setMinDeliveryDays(Integer minDeliveryDays) {
    this.minDeliveryDays = minDeliveryDays;
  }

  public BasketProduct maxDeliveryDays(Integer maxDeliveryDays) {
    this.maxDeliveryDays = maxDeliveryDays;
    return this;
  }

  /**
   * Get maxDeliveryDays
   * @return maxDeliveryDays
  **/
  @ApiModelProperty(value = "")


  public Integer getMaxDeliveryDays() {
    return maxDeliveryDays;
  }

  public void setMaxDeliveryDays(Integer maxDeliveryDays) {
    this.maxDeliveryDays = maxDeliveryDays;
  }

  public BasketProduct hasDiscount(Boolean hasDiscount) {
    this.hasDiscount = hasDiscount;
    return this;
  }

  /**
   * Get hasDiscount
   * @return hasDiscount
  **/
  @ApiModelProperty(value = "")


  public Boolean isHasDiscount() {
    return hasDiscount;
  }

  public void setHasDiscount(Boolean hasDiscount) {
    this.hasDiscount = hasDiscount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BasketProduct basketProduct = (BasketProduct) o;
    return Objects.equals(this.id, basketProduct.id) &&
        Objects.equals(this.productId, basketProduct.productId) &&
        Objects.equals(this.title, basketProduct.title) &&
        Objects.equals(this.amount, basketProduct.amount) &&
        Objects.equals(this.price, basketProduct.price) &&
        Objects.equals(this.originalPrice, basketProduct.originalPrice) &&
        Objects.equals(this.sum, basketProduct.sum) &&
        Objects.equals(this.originalSum, basketProduct.originalSum) &&
        Objects.equals(this.minDeliveryDays, basketProduct.minDeliveryDays) &&
        Objects.equals(this.maxDeliveryDays, basketProduct.maxDeliveryDays) &&
        Objects.equals(this.hasDiscount, basketProduct.hasDiscount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, productId, title, amount, price, originalPrice, sum, originalSum, minDeliveryDays, maxDeliveryDays, hasDiscount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BasketProduct {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    productId: ").append(toIndentedString(productId)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    originalPrice: ").append(toIndentedString(originalPrice)).append("\n");
    sb.append("    sum: ").append(toIndentedString(sum)).append("\n");
    sb.append("    originalSum: ").append(toIndentedString(originalSum)).append("\n");
    sb.append("    minDeliveryDays: ").append(toIndentedString(minDeliveryDays)).append("\n");
    sb.append("    maxDeliveryDays: ").append(toIndentedString(maxDeliveryDays)).append("\n");
    sb.append("    hasDiscount: ").append(toIndentedString(hasDiscount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

