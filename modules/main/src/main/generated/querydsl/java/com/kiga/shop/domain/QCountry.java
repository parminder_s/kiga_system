package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QCountry is a Querydsl query type for Country
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCountry extends EntityPathBase<Country> {

    private static final long serialVersionUID = 1606847963L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCountry country = new QCountry("country");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    public final StringPath code = createString("code");

    public final StringPath code3 = createString("code3");

    public final QCountryGroup countryGroup;

    public final QCountryShop countryShop;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath currency = createString("currency");

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<java.math.BigDecimal> vat = createNumber("vat", java.math.BigDecimal.class);

    public QCountry(String variable) {
        this(Country.class, forVariable(variable), INITS);
    }

    public QCountry(Path<? extends Country> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCountry(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCountry(PathMetadata<?> metadata, PathInits inits) {
        this(Country.class, metadata, inits);
    }

    public QCountry(Class<? extends Country> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.countryGroup = inits.isInitialized("countryGroup") ? new QCountryGroup(forProperty("countryGroup"), inits.get("countryGroup")) : null;
        this.countryShop = inits.isInitialized("countryShop") ? new QCountryShop(forProperty("countryShop"), inits.get("countryShop")) : null;
    }

}

