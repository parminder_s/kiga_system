package com.kiga.content.domain.ss;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QSiteTreeModel is a Querydsl query type for SiteTreeModel
 */
@Generated("com.mysema.query.codegen.SupertypeSerializer")
public class QSiteTreeModel extends EntityPathBase<SiteTreeModel<? extends SiteTree>> {

    private static final long serialVersionUID = 24400386L;

    public static final QSiteTreeModel siteTreeModel = new QSiteTreeModel("siteTreeModel");

    public final com.kiga.db.QKigaEntityModelWithoutId _super = new com.kiga.db.QKigaEntityModelWithoutId(this);

    public final StringPath canEditType = createString("canEditType");

    public final StringPath canPublishType = createString("canPublishType");

    public final StringPath canViewType = createString("canViewType");

    public final ListPath<SiteTree<?>, SimplePath<SiteTree<?>>> children = this.<SiteTree<?>, SimplePath<SiteTree<?>>>createList("children", SiteTree.class, SimplePath.class, PathInits.DIRECT2);

    //inherited
    public final StringPath className = _super.className;

    public final StringPath content = createString("content");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final DateTimePath<java.util.Date> expiryDate = createDateTime("expiryDate", java.util.Date.class);

    public final StringPath extraMeta = createString("extraMeta");

    public final NumberPath<Integer> hasBrokenFile = createNumber("hasBrokenFile", Integer.class);

    public final NumberPath<Integer> hasBrokenLink = createNumber("hasBrokenLink", Integer.class);

    public final StringPath homePageForDomain = createString("homePageForDomain");

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final SimplePath<com.kiga.main.locale.Locale> locale = createSimple("locale", com.kiga.main.locale.Locale.class);

    public final StringPath menuTitle = createString("menuTitle");

    public final StringPath metaDescription = createString("metaDescription");

    public final StringPath metaKeywords = createString("metaKeywords");

    public final StringPath metaTitle = createString("metaTitle");

    public final SimplePath<SiteTree> parent = createSimple("parent", SiteTree.class);

    public final StringPath priority = createString("priority");

    public final NumberPath<Integer> provideComments = createNumber("provideComments", Integer.class);

    public final StringPath reportClass = createString("reportClass");

    public final NumberPath<Integer> showInMenus = createNumber("showInMenus", Integer.class);

    public final NumberPath<Integer> showInSearch = createNumber("showInSearch", Integer.class);

    public final NumberPath<Integer> sort = createNumber("sort", Integer.class);

    public final StringPath sortTree = createString("sortTree");

    public final StringPath status = createString("status");

    public final StringPath title = createString("title");

    public final StringPath toDo = createString("toDo");

    public final StringPath urlSegment = createString("urlSegment");

    public final NumberPath<Integer> version = createNumber("version", Integer.class);

    @SuppressWarnings("all")
    public QSiteTreeModel(String variable) {
        super((Class)SiteTreeModel.class, forVariable(variable));
    }

    @SuppressWarnings("all")
    public QSiteTreeModel(Path<? extends SiteTreeModel> path) {
        super((Class)path.getType(), path.getMetadata());
    }

    @SuppressWarnings("all")
    public QSiteTreeModel(PathMetadata<?> metadata) {
        super((Class)SiteTreeModel.class, metadata);
    }

}

