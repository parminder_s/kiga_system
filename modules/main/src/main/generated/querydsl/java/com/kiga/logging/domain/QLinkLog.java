package com.kiga.logging.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QLinkLog is a Querydsl query type for LinkLog
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QLinkLog extends EntityPathBase<LinkLog> {

    private static final long serialVersionUID = -1538935528L;

    public static final QLinkLog linkLog = new QLinkLog("linkLog");

    public final QLogModel _super = new QLogModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final StringPath countryCode = _super.countryCode;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    //inherited
    public final SimplePath<com.kiga.main.locale.Locale> locale = _super.locale;

    //inherited
    public final StringPath remoteIp = _super.remoteIp;

    //inherited
    public final StringPath sessionId = _super.sessionId;

    public final StringPath tag = createString("tag");

    public final StringPath url = createString("url");

    public QLinkLog(String variable) {
        super(LinkLog.class, forVariable(variable));
    }

    public QLinkLog(Path<? extends LinkLog> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLinkLog(PathMetadata<?> metadata) {
        super(LinkLog.class, metadata);
    }

}

