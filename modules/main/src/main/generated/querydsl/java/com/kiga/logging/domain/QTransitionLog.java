package com.kiga.logging.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QTransitionLog is a Querydsl query type for TransitionLog
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QTransitionLog extends EntityPathBase<TransitionLog> {

    private static final long serialVersionUID = -253174947L;

    public static final QTransitionLog transitionLog = new QTransitionLog("transitionLog");

    public final QLogModel _super = new QLogModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final StringPath countryCode = _super.countryCode;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath fromUrl = createString("fromUrl");

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    //inherited
    public final SimplePath<com.kiga.main.locale.Locale> locale = _super.locale;

    //inherited
    public final StringPath remoteIp = _super.remoteIp;

    //inherited
    public final StringPath sessionId = _super.sessionId;

    public final StringPath toUrl = createString("toUrl");

    public QTransitionLog(String variable) {
        super(TransitionLog.class, forVariable(variable));
    }

    public QTransitionLog(Path<? extends TransitionLog> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTransitionLog(PathMetadata<?> metadata) {
        super(TransitionLog.class, metadata);
    }

}

