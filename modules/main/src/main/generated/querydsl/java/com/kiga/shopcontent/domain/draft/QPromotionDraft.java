package com.kiga.shopcontent.domain.draft;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPromotionDraft is a Querydsl query type for PromotionDraft
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPromotionDraft extends EntityPathBase<PromotionDraft> {

    private static final long serialVersionUID = -1348997855L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPromotionDraft promotionDraft = new QPromotionDraft("promotionDraft");

    public final com.kiga.shopcontent.domain.QPromotion _super = new com.kiga.shopcontent.domain.QPromotion(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final com.kiga.content.domain.ss.draft.QPageDraft page;

    public final QProductPageDraft productPage;

    public QPromotionDraft(String variable) {
        this(PromotionDraft.class, forVariable(variable), INITS);
    }

    public QPromotionDraft(Path<? extends PromotionDraft> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPromotionDraft(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPromotionDraft(PathMetadata<?> metadata, PathInits inits) {
        this(PromotionDraft.class, metadata, inits);
    }

    public QPromotionDraft(Class<? extends PromotionDraft> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.page = inits.isInitialized("page") ? new com.kiga.content.domain.ss.draft.QPageDraft(forProperty("page"), inits.get("page")) : null;
        this.productPage = inits.isInitialized("productPage") ? new QProductPageDraft(forProperty("productPage"), inits.get("productPage")) : null;
    }

}

