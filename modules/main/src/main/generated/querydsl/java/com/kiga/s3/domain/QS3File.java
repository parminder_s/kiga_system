package com.kiga.s3.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QS3File is a Querydsl query type for S3File
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QS3File extends EntityPathBase<S3File> {

    private static final long serialVersionUID = -244448063L;

    public static final QS3File s3File = new QS3File("s3File");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final StringPath bucket = createString("bucket");

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath folder = createString("folder");

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final StringPath name = createString("name");

    public final NumberPath<Long> ownerId = createNumber("ownerId", Long.class);

    public final StringPath url = createString("url");

    public QS3File(String variable) {
        super(S3File.class, forVariable(variable));
    }

    public QS3File(Path<? extends S3File> path) {
        super(path.getType(), path.getMetadata());
    }

    public QS3File(PathMetadata<?> metadata) {
        super(S3File.class, metadata);
    }

}

