package com.kiga.subscription.domain.zend;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QSubscription is a Querydsl query type for Subscription
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSubscription extends EntityPathBase<Subscription> {

    private static final long serialVersionUID = -434151224L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSubscription subscription = new QSubscription("subscription");

    public final com.kiga.db.QKigaLegacyDbEntityModel _super = new com.kiga.db.QKigaLegacyDbEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    public final StringPath countryCode = createString("countryCode");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final DateTimePath<java.util.Date> periodEndDate = createDateTime("periodEndDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> periodStartDate = createDateTime("periodStartDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> productStartDate = createDateTime("productStartDate", java.util.Date.class);

    public final EnumPath<SubscriptionStatus> status = createEnum("status", SubscriptionStatus.class);

    public final QSubscriptionType subscriptionType;

    public QSubscription(String variable) {
        this(Subscription.class, forVariable(variable), INITS);
    }

    public QSubscription(Path<? extends Subscription> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSubscription(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSubscription(PathMetadata<?> metadata, PathInits inits) {
        this(Subscription.class, metadata, inits);
    }

    public QSubscription(Class<? extends Subscription> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.subscriptionType = inits.isInitialized("subscriptionType") ? new QSubscriptionType(forProperty("subscriptionType")) : null;
    }

}

