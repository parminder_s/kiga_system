package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QCountryGroup is a Querydsl query type for CountryGroup
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCountryGroup extends EntityPathBase<CountryGroup> {

    private static final long serialVersionUID = -1591940764L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCountryGroup countryGroup1 = new QCountryGroup("countryGroup1");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    public final QCountryGroup countryGroup;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final StringPath title = createString("title");

    public QCountryGroup(String variable) {
        this(CountryGroup.class, forVariable(variable), INITS);
    }

    public QCountryGroup(Path<? extends CountryGroup> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCountryGroup(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCountryGroup(PathMetadata<?> metadata, PathInits inits) {
        this(CountryGroup.class, metadata, inits);
    }

    public QCountryGroup(Class<? extends CountryGroup> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.countryGroup = inits.isInitialized("countryGroup") ? new QCountryGroup(forProperty("countryGroup"), inits.get("countryGroup")) : null;
    }

}

