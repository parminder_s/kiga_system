package com.kiga.content.domain.ss.draft;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QLinkedKigaPageGroupDraft is a Querydsl query type for LinkedKigaPageGroupDraft
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QLinkedKigaPageGroupDraft extends EntityPathBase<LinkedKigaPageGroupDraft> {

    private static final long serialVersionUID = -1248374479L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QLinkedKigaPageGroupDraft linkedKigaPageGroupDraft = new QLinkedKigaPageGroupDraft("linkedKigaPageGroupDraft");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath description = createString("description");

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final ListPath<KigaPageDraft, QKigaPageDraft> kigaPages = this.<KigaPageDraft, QKigaPageDraft>createList("kigaPages", KigaPageDraft.class, QKigaPageDraft.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final ListPath<LinkedKigaPageGroupDraft, QLinkedKigaPageGroupDraft> linkedKigaPageGroupDrafts = this.<LinkedKigaPageGroupDraft, QLinkedKigaPageGroupDraft>createList("linkedKigaPageGroupDrafts", LinkedKigaPageGroupDraft.class, QLinkedKigaPageGroupDraft.class, PathInits.DIRECT2);

    public final SimplePath<com.kiga.main.locale.Locale> locale = createSimple("locale", com.kiga.main.locale.Locale.class);

    public final QLinkedKigaPageGroupDraft referenceLinkedKigaPageGroup;

    public final StringPath title = createString("title");

    public QLinkedKigaPageGroupDraft(String variable) {
        this(LinkedKigaPageGroupDraft.class, forVariable(variable), INITS);
    }

    public QLinkedKigaPageGroupDraft(Path<? extends LinkedKigaPageGroupDraft> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QLinkedKigaPageGroupDraft(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QLinkedKigaPageGroupDraft(PathMetadata<?> metadata, PathInits inits) {
        this(LinkedKigaPageGroupDraft.class, metadata, inits);
    }

    public QLinkedKigaPageGroupDraft(Class<? extends LinkedKigaPageGroupDraft> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.referenceLinkedKigaPageGroup = inits.isInitialized("referenceLinkedKigaPageGroup") ? new QLinkedKigaPageGroupDraft(forProperty("referenceLinkedKigaPageGroup"), inits.get("referenceLinkedKigaPageGroup")) : null;
    }

}

