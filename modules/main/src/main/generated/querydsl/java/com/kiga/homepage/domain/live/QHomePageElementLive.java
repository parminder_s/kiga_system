package com.kiga.homepage.domain.live;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QHomePageElementLive is a Querydsl query type for HomePageElementLive
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QHomePageElementLive extends EntityPathBase<HomePageElementLive> {

    private static final long serialVersionUID = -1242872405L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QHomePageElementLive homePageElementLive = new QHomePageElementLive("homePageElementLive");

    public final com.kiga.homepage.domain.QHomePageElement _super;

    // inherited
    public final com.kiga.s3.domain.QS3Image big;

    //inherited
    public final StringPath className;

    //inherited
    public final StringPath color;

    //inherited
    public final EnumPath<com.kiga.homepage.domain.HomePageElementContext> context;

    //inherited
    public final DateTimePath<java.util.Date> created;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited;

    //inherited
    public final SimplePath<com.kiga.main.locale.Locale> locale;

    //inherited
    public final NumberPath<Integer> position;

    // inherited
    public final com.kiga.s3.domain.QS3Image small;

    //inherited
    public final StringPath title;

    //inherited
    public final StringPath titleShort;

    //inherited
    public final EnumPath<com.kiga.homepage.domain.HomePageElementType> type;

    //inherited
    public final StringPath url;

    public QHomePageElementLive(String variable) {
        this(HomePageElementLive.class, forVariable(variable), INITS);
    }

    public QHomePageElementLive(Path<? extends HomePageElementLive> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QHomePageElementLive(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QHomePageElementLive(PathMetadata<?> metadata, PathInits inits) {
        this(HomePageElementLive.class, metadata, inits);
    }

    public QHomePageElementLive(Class<? extends HomePageElementLive> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new com.kiga.homepage.domain.QHomePageElement(type, metadata, inits);
        this.big = _super.big;
        this.className = _super.className;
        this.color = _super.color;
        this.context = _super.context;
        this.created = _super.created;
        this.lastEdited = _super.lastEdited;
        this.locale = _super.locale;
        this.position = _super.position;
        this.small = _super.small;
        this.title = _super.title;
        this.titleShort = _super.titleShort;
        this.type = _super.type;
        this.url = _super.url;
    }

}

