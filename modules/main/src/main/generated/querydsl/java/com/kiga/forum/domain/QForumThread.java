package com.kiga.forum.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QForumThread is a Querydsl query type for ForumThread
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QForumThread extends EntityPathBase<ForumThread> {

    private static final long serialVersionUID = -1216268041L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QForumThread forumThread = new QForumThread("forumThread");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final QForum forum;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final BooleanPath isGlobalSticky = createBoolean("isGlobalSticky");

    public final BooleanPath isReadOnly = createBoolean("isReadOnly");

    public final BooleanPath isSticky = createBoolean("isSticky");

    public final com.kiga.content.domain.ss.live.QKigaPageLive kigaPage;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<Long> numViews = createNumber("numViews", Long.class);

    public final ListPath<Post, QPost> posts = this.<Post, QPost>createList("posts", Post.class, QPost.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> realLastEdited = createDateTime("realLastEdited", java.util.Date.class);

    public final StringPath title = createString("title");

    public QForumThread(String variable) {
        this(ForumThread.class, forVariable(variable), INITS);
    }

    public QForumThread(Path<? extends ForumThread> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QForumThread(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QForumThread(PathMetadata<?> metadata, PathInits inits) {
        this(ForumThread.class, metadata, inits);
    }

    public QForumThread(Class<? extends ForumThread> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.forum = inits.isInitialized("forum") ? new QForum(forProperty("forum"), inits.get("forum")) : null;
        this.kigaPage = inits.isInitialized("kigaPage") ? new com.kiga.content.domain.ss.live.QKigaPageLive(forProperty("kigaPage"), inits.get("kigaPage")) : null;
    }

}

