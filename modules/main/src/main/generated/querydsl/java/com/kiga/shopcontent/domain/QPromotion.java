package com.kiga.shopcontent.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPromotion is a Querydsl query type for Promotion
 */
@Generated("com.mysema.query.codegen.SupertypeSerializer")
public class QPromotion extends EntityPathBase<Promotion<? extends com.kiga.content.domain.ss.Page, ? extends ProductPage>> {

    private static final long serialVersionUID = -808609811L;

    public static final QPromotion promotion = new QPromotion("promotion");

    public final com.kiga.db.QKigaEntityModelWithoutId _super = new com.kiga.db.QKigaEntityModelWithoutId(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final SimplePath<com.kiga.content.domain.ss.Page> page = createSimple("page", com.kiga.content.domain.ss.Page.class);

    public final SimplePath<ProductPage> productPage = createSimple("productPage", ProductPage.class);

    @SuppressWarnings("all")
    public QPromotion(String variable) {
        super((Class)Promotion.class, forVariable(variable));
    }

    @SuppressWarnings("all")
    public QPromotion(Path<? extends Promotion> path) {
        super((Class)path.getType(), path.getMetadata());
    }

    @SuppressWarnings("all")
    public QPromotion(PathMetadata<?> metadata) {
        super((Class)Promotion.class, metadata);
    }

}

