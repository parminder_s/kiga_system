package com.kiga.content.domain.ss;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QKigaPageImage is a Querydsl query type for KigaPageImage
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QKigaPageImage extends EntityPathBase<KigaPageImage> {

    private static final long serialVersionUID = 1596371858L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QKigaPageImage kigaPageImage = new QKigaPageImage("kigaPageImage");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final NumberPath<Long> imageId = createNumber("imageId", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final StringPath locale = createString("locale");

    public final com.kiga.s3.domain.QS3Image s3Image;

    public final StringPath title = createString("title");

    public QKigaPageImage(String variable) {
        this(KigaPageImage.class, forVariable(variable), INITS);
    }

    public QKigaPageImage(Path<? extends KigaPageImage> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QKigaPageImage(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QKigaPageImage(PathMetadata<?> metadata, PathInits inits) {
        this(KigaPageImage.class, metadata, inits);
    }

    public QKigaPageImage(Class<? extends KigaPageImage> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.s3Image = inits.isInitialized("s3Image") ? new com.kiga.s3.domain.QS3Image(forProperty("s3Image")) : null;
    }

}

