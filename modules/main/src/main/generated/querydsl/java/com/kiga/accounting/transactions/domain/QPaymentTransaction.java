package com.kiga.accounting.transactions.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPaymentTransaction is a Querydsl query type for PaymentTransaction
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPaymentTransaction extends EntityPathBase<PaymentTransaction> {

    private static final long serialVersionUID = 284185081L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPaymentTransaction paymentTransaction = new QPaymentTransaction("paymentTransaction");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final NumberPath<java.math.BigDecimal> amount = createNumber("amount", java.math.BigDecimal.class);

    public final StringPath bank = createString("bank");

    public final StringPath bankTransactionId = createString("bankTransactionId");

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final com.kiga.accounting.domain.QInvoice invoice;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final DateTimePath<java.util.Date> transferDate = createDateTime("transferDate", java.util.Date.class);

    public QPaymentTransaction(String variable) {
        this(PaymentTransaction.class, forVariable(variable), INITS);
    }

    public QPaymentTransaction(Path<? extends PaymentTransaction> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPaymentTransaction(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPaymentTransaction(PathMetadata<?> metadata, PathInits inits) {
        this(PaymentTransaction.class, metadata, inits);
    }

    public QPaymentTransaction(Class<? extends PaymentTransaction> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.invoice = inits.isInitialized("invoice") ? new com.kiga.accounting.domain.QInvoice(forProperty("invoice"), inits.get("invoice")) : null;
    }

}

