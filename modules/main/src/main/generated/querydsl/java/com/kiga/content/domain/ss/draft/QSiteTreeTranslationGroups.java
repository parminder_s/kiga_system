package com.kiga.content.domain.ss.draft;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QSiteTreeTranslationGroups is a Querydsl query type for SiteTreeTranslationGroups
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSiteTreeTranslationGroups extends EntityPathBase<SiteTreeTranslationGroups> {

    private static final long serialVersionUID = 1653695665L;

    public static final QSiteTreeTranslationGroups siteTreeTranslationGroups = new QSiteTreeTranslationGroups("siteTreeTranslationGroups");

    public final com.kiga.content.domain.ss.QSiteTreeTranslationGroupsModel _super = new com.kiga.content.domain.ss.QSiteTreeTranslationGroupsModel(this);

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final NumberPath<Long> originalId = _super.originalId;

    //inherited
    public final NumberPath<Long> translationGroupId = _super.translationGroupId;

    public QSiteTreeTranslationGroups(String variable) {
        super(SiteTreeTranslationGroups.class, forVariable(variable));
    }

    public QSiteTreeTranslationGroups(Path<? extends SiteTreeTranslationGroups> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSiteTreeTranslationGroups(PathMetadata<?> metadata) {
        super(SiteTreeTranslationGroups.class, metadata);
    }

}

