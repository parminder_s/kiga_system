package com.kiga.subscription.domain.zend;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QSubscriptionType is a Querydsl query type for SubscriptionType
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSubscriptionType extends EntityPathBase<SubscriptionType> {

    private static final long serialVersionUID = 312066082L;

    public static final QSubscriptionType subscriptionType = new QSubscriptionType("subscriptionType");

    public final com.kiga.db.QKigaLegacyDbEntityModel _super = new com.kiga.db.QKigaLegacyDbEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> durationPeriod = createNumber("durationPeriod", Integer.class);

    public final NumberPath<Integer> durationPeriodDays = createNumber("durationPeriodDays", Integer.class);

    public final StringPath durationType = createString("durationType");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final StringPath name = createString("name");

    public final StringPath typeCode = createString("typeCode");

    public QSubscriptionType(String variable) {
        super(SubscriptionType.class, forVariable(variable));
    }

    public QSubscriptionType(Path<? extends SubscriptionType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSubscriptionType(PathMetadata<?> metadata) {
        super(SubscriptionType.class, metadata);
    }

}

