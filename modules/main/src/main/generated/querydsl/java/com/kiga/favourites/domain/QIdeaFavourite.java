package com.kiga.favourites.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QIdeaFavourite is a Querydsl query type for IdeaFavourite
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QIdeaFavourite extends EntityPathBase<IdeaFavourite> {

    private static final long serialVersionUID = -964452541L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QIdeaFavourite ideaFavourite = new QIdeaFavourite("ideaFavourite");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final com.kiga.content.domain.ss.live.QKigaIdeaLive kigaIdeaLive;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final com.kiga.security.domain.QMember member;

    public QIdeaFavourite(String variable) {
        this(IdeaFavourite.class, forVariable(variable), INITS);
    }

    public QIdeaFavourite(Path<? extends IdeaFavourite> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QIdeaFavourite(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QIdeaFavourite(PathMetadata<?> metadata, PathInits inits) {
        this(IdeaFavourite.class, metadata, inits);
    }

    public QIdeaFavourite(Class<? extends IdeaFavourite> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.kigaIdeaLive = inits.isInitialized("kigaIdeaLive") ? new com.kiga.content.domain.ss.live.QKigaIdeaLive(forProperty("kigaIdeaLive"), inits.get("kigaIdeaLive")) : null;
        this.member = inits.isInitialized("member") ? new com.kiga.security.domain.QMember(forProperty("member"), inits.get("member")) : null;
    }

}

