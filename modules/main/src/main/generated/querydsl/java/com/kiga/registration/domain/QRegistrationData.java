package com.kiga.registration.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QRegistrationData is a Querydsl query type for RegistrationData
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QRegistrationData extends EntityPathBase<RegistrationData> {

    private static final long serialVersionUID = 1267706657L;

    public static final QRegistrationData registrationData = new QRegistrationData("registrationData");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath data = createString("data");

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public QRegistrationData(String variable) {
        super(RegistrationData.class, forVariable(variable));
    }

    public QRegistrationData(Path<? extends RegistrationData> path) {
        super(path.getType(), path.getMetadata());
    }

    public QRegistrationData(PathMetadata<?> metadata) {
        super(RegistrationData.class, metadata);
    }

}

