package com.kiga.homepage.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QHomePageElement is a Querydsl query type for HomePageElement
 */
@Generated("com.mysema.query.codegen.SupertypeSerializer")
public class QHomePageElement extends EntityPathBase<HomePageElement> {

    private static final long serialVersionUID = 1461803227L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QHomePageElement homePageElement = new QHomePageElement("homePageElement");

    public final com.kiga.db.QKigaEntityModelWithoutId _super = new com.kiga.db.QKigaEntityModelWithoutId(this);

    public final com.kiga.s3.domain.QS3Image big;

    //inherited
    public final StringPath className = _super.className;

    public final StringPath color = createString("color");

    public final EnumPath<HomePageElementContext> context = createEnum("context", HomePageElementContext.class);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final SimplePath<com.kiga.main.locale.Locale> locale = createSimple("locale", com.kiga.main.locale.Locale.class);

    public final NumberPath<Integer> position = createNumber("position", Integer.class);

    public final com.kiga.s3.domain.QS3Image small;

    public final StringPath title = createString("title");

    public final StringPath titleShort = createString("titleShort");

    public final EnumPath<HomePageElementType> type = createEnum("type", HomePageElementType.class);

    public final StringPath url = createString("url");

    public QHomePageElement(String variable) {
        this(HomePageElement.class, forVariable(variable), INITS);
    }

    public QHomePageElement(Path<? extends HomePageElement> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QHomePageElement(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QHomePageElement(PathMetadata<?> metadata, PathInits inits) {
        this(HomePageElement.class, metadata, inits);
    }

    public QHomePageElement(Class<? extends HomePageElement> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.big = inits.isInitialized("big") ? new com.kiga.s3.domain.QS3Image(forProperty("big")) : null;
        this.small = inits.isInitialized("small") ? new com.kiga.s3.domain.QS3Image(forProperty("small")) : null;
    }

}

