package com.kiga.survey.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QSurvey is a Querydsl query type for Survey
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSurvey extends EntityPathBase<Survey> {

    private static final long serialVersionUID = -1545788519L;

    public static final QSurvey survey = new QSurvey("survey");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final StringPath urlSegment = createString("urlSegment");

    public QSurvey(String variable) {
        super(Survey.class, forVariable(variable));
    }

    public QSurvey(Path<? extends Survey> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSurvey(PathMetadata<?> metadata) {
        super(Survey.class, metadata);
    }

}

