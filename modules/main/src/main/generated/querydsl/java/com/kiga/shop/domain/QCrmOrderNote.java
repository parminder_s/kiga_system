package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QCrmOrderNote is a Querydsl query type for CrmOrderNote
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCrmOrderNote extends EntityPathBase<CrmOrderNote> {

    private static final long serialVersionUID = -183953155L;

    public static final QCrmOrderNote crmOrderNote = new QCrmOrderNote("crmOrderNote");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath crmId = createString("crmId");

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final DateTimePath<java.time.Instant> invoiceDate = createDateTime("invoiceDate", java.time.Instant.class);

    public final StringPath invoiceNr = createString("invoiceNr");

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public QCrmOrderNote(String variable) {
        super(CrmOrderNote.class, forVariable(variable));
    }

    public QCrmOrderNote(Path<? extends CrmOrderNote> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCrmOrderNote(PathMetadata<?> metadata) {
        super(CrmOrderNote.class, metadata);
    }

}

