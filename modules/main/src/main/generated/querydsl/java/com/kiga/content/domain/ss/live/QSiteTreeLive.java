package com.kiga.content.domain.ss.live;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QSiteTreeLive is a Querydsl query type for SiteTreeLive
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSiteTreeLive extends EntityPathBase<SiteTreeLive> {

    private static final long serialVersionUID = 1666374769L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSiteTreeLive siteTreeLive = new QSiteTreeLive("siteTreeLive");

    public final com.kiga.content.domain.ss.QSiteTreeModel _super = new com.kiga.content.domain.ss.QSiteTreeModel(this);

    //inherited
    public final StringPath canEditType = _super.canEditType;

    //inherited
    public final StringPath canPublishType = _super.canPublishType;

    //inherited
    public final StringPath canViewType = _super.canViewType;

    //inherited
    public final ListPath<com.kiga.content.domain.ss.SiteTree<?>, SimplePath<com.kiga.content.domain.ss.SiteTree<?>>> children = _super.children;

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final StringPath content = _super.content;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final DateTimePath<java.util.Date> expiryDate = _super.expiryDate;

    //inherited
    public final StringPath extraMeta = _super.extraMeta;

    //inherited
    public final NumberPath<Integer> hasBrokenFile = _super.hasBrokenFile;

    //inherited
    public final NumberPath<Integer> hasBrokenLink = _super.hasBrokenLink;

    //inherited
    public final StringPath homePageForDomain = _super.homePageForDomain;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    //inherited
    public final SimplePath<com.kiga.main.locale.Locale> locale = _super.locale;

    //inherited
    public final StringPath menuTitle = _super.menuTitle;

    //inherited
    public final StringPath metaDescription = _super.metaDescription;

    //inherited
    public final StringPath metaKeywords = _super.metaKeywords;

    //inherited
    public final StringPath metaTitle = _super.metaTitle;

    public final QSiteTreeLive parent;

    //inherited
    public final StringPath priority = _super.priority;

    //inherited
    public final NumberPath<Integer> provideComments = _super.provideComments;

    //inherited
    public final StringPath reportClass = _super.reportClass;

    //inherited
    public final NumberPath<Integer> showInMenus = _super.showInMenus;

    //inherited
    public final NumberPath<Integer> showInSearch = _super.showInSearch;

    //inherited
    public final NumberPath<Integer> sort = _super.sort;

    //inherited
    public final StringPath sortTree = _super.sortTree;

    //inherited
    public final StringPath status = _super.status;

    //inherited
    public final StringPath title = _super.title;

    //inherited
    public final StringPath toDo = _super.toDo;

    //inherited
    public final StringPath urlSegment = _super.urlSegment;

    //inherited
    public final NumberPath<Integer> version = _super.version;

    public QSiteTreeLive(String variable) {
        this(SiteTreeLive.class, forVariable(variable), INITS);
    }

    public QSiteTreeLive(Path<? extends SiteTreeLive> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSiteTreeLive(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSiteTreeLive(PathMetadata<?> metadata, PathInits inits) {
        this(SiteTreeLive.class, metadata, inits);
    }

    public QSiteTreeLive(Class<? extends SiteTreeLive> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.parent = inits.isInitialized("parent") ? new QSiteTreeLive(forProperty("parent"), inits.get("parent")) : null;
    }

}

