package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QCountryLocale is a Querydsl query type for CountryLocale
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCountryLocale extends EntityPathBase<CountryLocale> {

    private static final long serialVersionUID = -1965524971L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCountryLocale countryLocale = new QCountryLocale("countryLocale");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    public final QCountry country;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final SimplePath<com.kiga.main.locale.Locale> locale = createSimple("locale", com.kiga.main.locale.Locale.class);

    public final StringPath title = createString("title");

    public QCountryLocale(String variable) {
        this(CountryLocale.class, forVariable(variable), INITS);
    }

    public QCountryLocale(Path<? extends CountryLocale> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCountryLocale(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCountryLocale(PathMetadata<?> metadata, PathInits inits) {
        this(CountryLocale.class, metadata, inits);
    }

    public QCountryLocale(Class<? extends CountryLocale> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.country = inits.isInitialized("country") ? new QCountry(forProperty("country"), inits.get("country")) : null;
    }

}

