package com.kiga.security.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QMember is a Querydsl query type for Member
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QMember extends EntityPathBase<Member> {

    private static final long serialVersionUID = -983998977L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMember member = new QMember("member1");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    public final StringPath country = createString("country");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath crmId = createString("crmId");

    public final StringPath customerId = createString("customerId");

    public final StringPath email = createString("email");

    public final StringPath firstname = createString("firstname");

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final StringPath lastname = createString("lastname");

    public final StringPath locale = createString("locale");

    public final StringPath nickname = createString("nickname");

    public final StringPath password = createString("password");

    public final ListPath<com.kiga.shop.domain.Purchase, com.kiga.shop.domain.QPurchase> purchases = this.<com.kiga.shop.domain.Purchase, com.kiga.shop.domain.QPurchase>createList("purchases", com.kiga.shop.domain.Purchase.class, com.kiga.shop.domain.QPurchase.class, PathInits.DIRECT2);

    public final ListPath<com.kiga.shop.domain.PurchaseWorkflow, com.kiga.shop.domain.QPurchaseWorkflow> purchaseWorkflows = this.<com.kiga.shop.domain.PurchaseWorkflow, com.kiga.shop.domain.QPurchaseWorkflow>createList("purchaseWorkflows", com.kiga.shop.domain.PurchaseWorkflow.class, com.kiga.shop.domain.QPurchaseWorkflow.class, PathInits.DIRECT2);

    public final StringPath rememberLoginToken = createString("rememberLoginToken");

    public final ListPath<Role, QRole> roles = this.<Role, QRole>createList("roles", Role.class, QRole.class, PathInits.DIRECT2);

    public final com.kiga.s3.domain.QS3Image s3Avatar;

    public QMember(String variable) {
        this(Member.class, forVariable(variable), INITS);
    }

    public QMember(Path<? extends Member> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QMember(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QMember(PathMetadata<?> metadata, PathInits inits) {
        this(Member.class, metadata, inits);
    }

    public QMember(Class<? extends Member> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.s3Avatar = inits.isInitialized("s3Avatar") ? new com.kiga.s3.domain.QS3Image(forProperty("s3Avatar")) : null;
    }

}

