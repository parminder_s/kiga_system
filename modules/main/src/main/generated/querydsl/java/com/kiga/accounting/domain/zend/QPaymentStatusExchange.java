package com.kiga.accounting.domain.zend;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPaymentStatusExchange is a Querydsl query type for PaymentStatusExchange
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPaymentStatusExchange extends EntityPathBase<PaymentStatusExchange> {

    private static final long serialVersionUID = -1851355576L;

    public static final QPaymentStatusExchange paymentStatusExchange = new QPaymentStatusExchange("paymentStatusExchange");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final StringPath acquirerName = createString("acquirerName");

    public final StringPath acquirerTid = createString("acquirerTid");

    public final NumberPath<Double> amount = createNumber("amount", Double.class);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath currencyCode = createString("currencyCode");

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<Integer> paymentType = createNumber("paymentType", Integer.class);

    public final StringPath pspName = createString("pspName");

    public final StringPath pspTid = createString("pspTid");

    public final DateTimePath<java.util.Date> rcptDate = createDateTime("rcptDate", java.util.Date.class);

    public final NumberPath<Long> rcptId = createNumber("rcptId", Long.class);

    public final StringPath rcptStatus = createString("rcptStatus");

    public final StringPath rcptStatusLong = createString("rcptStatusLong");

    public final DateTimePath<java.util.Date> requestDate = createDateTime("requestDate", java.util.Date.class);

    public final StringPath requestStatus = createString("requestStatus");

    public final DateTimePath<java.util.Date> requestUpdate = createDateTime("requestUpdate", java.util.Date.class);

    public final StringPath tid1 = createString("tid1");

    public final StringPath tid2 = createString("tid2");

    public final DateTimePath<java.util.Date> transactionDate = createDateTime("transactionDate", java.util.Date.class);

    public final StringPath transactionId = createString("transactionId");

    public final NumberPath<Double> transferAmount = createNumber("transferAmount", Double.class);

    public final DateTimePath<java.util.Date> transferDate = createDateTime("transferDate", java.util.Date.class);

    public QPaymentStatusExchange(String variable) {
        super(PaymentStatusExchange.class, forVariable(variable));
    }

    public QPaymentStatusExchange(Path<? extends PaymentStatusExchange> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPaymentStatusExchange(PathMetadata<?> metadata) {
        super(PaymentStatusExchange.class, metadata);
    }

}

