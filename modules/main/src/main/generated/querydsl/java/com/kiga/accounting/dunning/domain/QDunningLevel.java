package com.kiga.accounting.dunning.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QDunningLevel is a Querydsl query type for DunningLevel
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDunningLevel extends EntityPathBase<DunningLevel> {

    private static final long serialVersionUID = -99235632L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDunningLevel dunningLevel1 = new QDunningLevel("dunningLevel1");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    public final StringPath code = createString("code");

    public final com.kiga.shop.domain.QCountry country;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> days = createNumber("days", Integer.class);

    public final NumberPath<Integer> dunningLevel = createNumber("dunningLevel", Integer.class);

    public final NumberPath<java.math.BigDecimal> feesAmount = createNumber("feesAmount", java.math.BigDecimal.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final ListPath<com.kiga.accounting.domain.Invoice, com.kiga.accounting.domain.QInvoice> invoices = this.<com.kiga.accounting.domain.Invoice, com.kiga.accounting.domain.QInvoice>createList("invoices", com.kiga.accounting.domain.Invoice.class, com.kiga.accounting.domain.QInvoice.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public QDunningLevel(String variable) {
        this(DunningLevel.class, forVariable(variable), INITS);
    }

    public QDunningLevel(Path<? extends DunningLevel> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QDunningLevel(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QDunningLevel(PathMetadata<?> metadata, PathInits inits) {
        this(DunningLevel.class, metadata, inits);
    }

    public QDunningLevel(Class<? extends DunningLevel> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.country = inits.isInitialized("country") ? new com.kiga.shop.domain.QCountry(forProperty("country"), inits.get("country")) : null;
    }

}

