package com.kiga.s3.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QKigaS3File is a Querydsl query type for KigaS3File
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QKigaS3File extends EntityPathBase<KigaS3File> {

    private static final long serialVersionUID = 18626329L;

    public static final QKigaS3File kigaS3File = new QKigaS3File("kigaS3File");

    public final QS3File _super = new QS3File(this);

    //inherited
    public final StringPath bucket = _super.bucket;

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final StringPath folder = _super.folder;

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    //inherited
    public final StringPath name = _super.name;

    //inherited
    public final NumberPath<Long> ownerId = _super.ownerId;

    public final NumberPath<Integer> size = createNumber("size", Integer.class);

    //inherited
    public final StringPath url = _super.url;

    public QKigaS3File(String variable) {
        super(KigaS3File.class, forVariable(variable));
    }

    public QKigaS3File(Path<? extends KigaS3File> path) {
        super(path.getType(), path.getMetadata());
    }

    public QKigaS3File(PathMetadata<?> metadata) {
        super(KigaS3File.class, metadata);
    }

}

