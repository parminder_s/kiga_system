package com.kiga.content.domain.ss.live;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QIdeaGroupToIdeaLive is a Querydsl query type for IdeaGroupToIdeaLive
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QIdeaGroupToIdeaLive extends EntityPathBase<IdeaGroupToIdeaLive> {

    private static final long serialVersionUID = -316636762L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QIdeaGroupToIdeaLive ideaGroupToIdeaLive = new QIdeaGroupToIdeaLive("ideaGroupToIdeaLive");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final QKigaIdeaLive article;

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final QIdeaGroupLive ideaGroup;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<Integer> sortOrder = createNumber("sortOrder", Integer.class);

    public final StringPath sortTree = createString("sortTree");

    public QIdeaGroupToIdeaLive(String variable) {
        this(IdeaGroupToIdeaLive.class, forVariable(variable), INITS);
    }

    public QIdeaGroupToIdeaLive(Path<? extends IdeaGroupToIdeaLive> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QIdeaGroupToIdeaLive(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QIdeaGroupToIdeaLive(PathMetadata<?> metadata, PathInits inits) {
        this(IdeaGroupToIdeaLive.class, metadata, inits);
    }

    public QIdeaGroupToIdeaLive(Class<? extends IdeaGroupToIdeaLive> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.article = inits.isInitialized("article") ? new QKigaIdeaLive(forProperty("article"), inits.get("article")) : null;
        this.ideaGroup = inits.isInitialized("ideaGroup") ? new QIdeaGroupLive(forProperty("ideaGroup"), inits.get("ideaGroup")) : null;
    }

}

