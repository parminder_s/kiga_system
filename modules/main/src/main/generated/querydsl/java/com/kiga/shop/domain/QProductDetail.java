package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QProductDetail is a Querydsl query type for ProductDetail
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QProductDetail extends EntityPathBase<ProductDetail> {

    private static final long serialVersionUID = 499241125L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProductDetail productDetail = new QProductDetail("productDetail");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final BooleanPath activated = createBoolean("activated");

    //inherited
    public final StringPath className = _super.className;

    public final QCountry country;

    public final QCountryGroup countryGroup;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> freeShippingAmount = createNumber("freeShippingAmount", Integer.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<java.math.BigDecimal> oldPriceGross = createNumber("oldPriceGross", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> priceNet = createNumber("priceNet", java.math.BigDecimal.class);

    public final QProduct product;

    public final NumberPath<java.math.BigDecimal> shippingCosts = createNumber("shippingCosts", java.math.BigDecimal.class);

    public final ListPath<ShippingDiscount, QShippingDiscount> shippingDiscounts = this.<ShippingDiscount, QShippingDiscount>createList("shippingDiscounts", ShippingDiscount.class, QShippingDiscount.class, PathInits.DIRECT2);

    public final StringPath shippingDiscountType = createString("shippingDiscountType");

    public final NumberPath<java.math.BigDecimal> vatProduct = createNumber("vatProduct", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> vatShipping = createNumber("vatShipping", java.math.BigDecimal.class);

    public QProductDetail(String variable) {
        this(ProductDetail.class, forVariable(variable), INITS);
    }

    public QProductDetail(Path<? extends ProductDetail> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QProductDetail(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QProductDetail(PathMetadata<?> metadata, PathInits inits) {
        this(ProductDetail.class, metadata, inits);
    }

    public QProductDetail(Class<? extends ProductDetail> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.country = inits.isInitialized("country") ? new QCountry(forProperty("country"), inits.get("country")) : null;
        this.countryGroup = inits.isInitialized("countryGroup") ? new QCountryGroup(forProperty("countryGroup"), inits.get("countryGroup")) : null;
        this.product = inits.isInitialized("product") ? new QProduct(forProperty("product")) : null;
    }

}

