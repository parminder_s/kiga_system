package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPurchaseItem is a Querydsl query type for PurchaseItem
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPurchaseItem extends EntityPathBase<PurchaseItem> {

    private static final long serialVersionUID = -600803185L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPurchaseItem purchaseItem = new QPurchaseItem("purchaseItem");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final NumberPath<Integer> amount = createNumber("amount", Integer.class);

    public final NumberPath<java.math.BigDecimal> appliedDiscountGrossTotal = createNumber("appliedDiscountGrossTotal", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> appliedDiscountRate = createNumber("appliedDiscountRate", java.math.BigDecimal.class);

    //inherited
    public final StringPath className = _super.className;

    public final StringPath code = createString("code");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<java.math.BigDecimal> priceGrossTotal = createNumber("priceGrossTotal", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> priceNetSingle = createNumber("priceNetSingle", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> priceShippingNetSingle = createNumber("priceShippingNetSingle", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> priceShippingNetTotal = createNumber("priceShippingNetTotal", java.math.BigDecimal.class);

    public final QProduct product;

    public final QPurchase purchase;

    public final StringPath title = createString("title");

    public final NumberPath<java.math.BigDecimal> vatRate = createNumber("vatRate", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> vatShippingRate = createNumber("vatShippingRate", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> vatShippingTotal = createNumber("vatShippingTotal", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> vatTotal = createNumber("vatTotal", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> volumeLitreSingle = createNumber("volumeLitreSingle", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> volumeLitreTotal = createNumber("volumeLitreTotal", java.math.BigDecimal.class);

    public final QVoucher voucher;

    public final StringPath voucherCode = createString("voucherCode");

    public final NumberPath<Integer> weightGramSingle = createNumber("weightGramSingle", Integer.class);

    public final NumberPath<Integer> weightGramTotal = createNumber("weightGramTotal", Integer.class);

    public QPurchaseItem(String variable) {
        this(PurchaseItem.class, forVariable(variable), INITS);
    }

    public QPurchaseItem(Path<? extends PurchaseItem> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPurchaseItem(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPurchaseItem(PathMetadata<?> metadata, PathInits inits) {
        this(PurchaseItem.class, metadata, inits);
    }

    public QPurchaseItem(Class<? extends PurchaseItem> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.product = inits.isInitialized("product") ? new QProduct(forProperty("product")) : null;
        this.purchase = inits.isInitialized("purchase") ? new QPurchase(forProperty("purchase"), inits.get("purchase")) : null;
        this.voucher = inits.isInitialized("voucher") ? new QVoucher(forProperty("voucher"), inits.get("voucher")) : null;
    }

}

