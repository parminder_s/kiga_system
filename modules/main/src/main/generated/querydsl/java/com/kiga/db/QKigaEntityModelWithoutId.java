package com.kiga.db;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QKigaEntityModelWithoutId is a Querydsl query type for KigaEntityModelWithoutId
 */
@Generated("com.mysema.query.codegen.SupertypeSerializer")
public class QKigaEntityModelWithoutId extends EntityPathBase<KigaEntityModelWithoutId> {

    private static final long serialVersionUID = 1611235484L;

    public static final QKigaEntityModelWithoutId kigaEntityModelWithoutId = new QKigaEntityModelWithoutId("kigaEntityModelWithoutId");

    public final StringPath className = createString("className");

    public final DateTimePath<java.util.Date> created = createDateTime("created", java.util.Date.class);

    public final DateTimePath<java.util.Date> lastEdited = createDateTime("lastEdited", java.util.Date.class);

    public QKigaEntityModelWithoutId(String variable) {
        super(KigaEntityModelWithoutId.class, forVariable(variable));
    }

    public QKigaEntityModelWithoutId(Path<? extends KigaEntityModelWithoutId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QKigaEntityModelWithoutId(PathMetadata<?> metadata) {
        super(KigaEntityModelWithoutId.class, metadata);
    }

}

