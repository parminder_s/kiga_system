package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QCountryShop is a Querydsl query type for CountryShop
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCountryShop extends EntityPathBase<CountryShop> {

    private static final long serialVersionUID = -605194383L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCountryShop countryShop = new QCountryShop("countryShop");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final StringPath accountNr = createString("accountNr");

    public final StringPath bankName = createString("bankName");

    public final StringPath bic = createString("bic");

    //inherited
    public final StringPath className = _super.className;

    public final QCountry country;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> daysToPayInvoice = createNumber("daysToPayInvoice", Integer.class);

    public final NumberPath<java.math.BigDecimal> freeShippingLimit = createNumber("freeShippingLimit", java.math.BigDecimal.class);

    public final StringPath iban = createString("iban");

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<Long> maxDeliveryDays = createNumber("maxDeliveryDays", Long.class);

    public final NumberPath<java.math.BigDecimal> maxPurchasePrice = createNumber("maxPurchasePrice", java.math.BigDecimal.class);

    public final NumberPath<Long> minDeliveryDays = createNumber("minDeliveryDays", Long.class);

    public final NumberPath<java.math.BigDecimal> priceShippingNet = createNumber("priceShippingNet", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> vatRateShipping = createNumber("vatRateShipping", java.math.BigDecimal.class);

    public QCountryShop(String variable) {
        this(CountryShop.class, forVariable(variable), INITS);
    }

    public QCountryShop(Path<? extends CountryShop> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCountryShop(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCountryShop(PathMetadata<?> metadata, PathInits inits) {
        this(CountryShop.class, metadata, inits);
    }

    public QCountryShop(Class<? extends CountryShop> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.country = inits.isInitialized("country") ? new QCountry(forProperty("country"), inits.get("country")) : null;
    }

}

