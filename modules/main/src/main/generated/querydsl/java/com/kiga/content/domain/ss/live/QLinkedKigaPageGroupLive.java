package com.kiga.content.domain.ss.live;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QLinkedKigaPageGroupLive is a Querydsl query type for LinkedKigaPageGroupLive
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QLinkedKigaPageGroupLive extends EntityPathBase<LinkedKigaPageGroupLive> {

    private static final long serialVersionUID = 1781567211L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QLinkedKigaPageGroupLive linkedKigaPageGroupLive = new QLinkedKigaPageGroupLive("linkedKigaPageGroupLive");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath description = createString("description");

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final ListPath<KigaPageLive, QKigaPageLive> kigaPages = this.<KigaPageLive, QKigaPageLive>createList("kigaPages", KigaPageLive.class, QKigaPageLive.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final ListPath<LinkedKigaPageGroupLive, QLinkedKigaPageGroupLive> linkedKigaPageGroupLives = this.<LinkedKigaPageGroupLive, QLinkedKigaPageGroupLive>createList("linkedKigaPageGroupLives", LinkedKigaPageGroupLive.class, QLinkedKigaPageGroupLive.class, PathInits.DIRECT2);

    public final SimplePath<com.kiga.main.locale.Locale> locale = createSimple("locale", com.kiga.main.locale.Locale.class);

    public final QLinkedKigaPageGroupLive referenceLinkedKigaPageGroup;

    public final StringPath title = createString("title");

    public QLinkedKigaPageGroupLive(String variable) {
        this(LinkedKigaPageGroupLive.class, forVariable(variable), INITS);
    }

    public QLinkedKigaPageGroupLive(Path<? extends LinkedKigaPageGroupLive> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QLinkedKigaPageGroupLive(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QLinkedKigaPageGroupLive(PathMetadata<?> metadata, PathInits inits) {
        this(LinkedKigaPageGroupLive.class, metadata, inits);
    }

    public QLinkedKigaPageGroupLive(Class<? extends LinkedKigaPageGroupLive> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.referenceLinkedKigaPageGroup = inits.isInitialized("referenceLinkedKigaPageGroup") ? new QLinkedKigaPageGroupLive(forProperty("referenceLinkedKigaPageGroup"), inits.get("referenceLinkedKigaPageGroup")) : null;
    }

}

