package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QProduct is a Querydsl query type for Product
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QProduct extends EntityPathBase<Product> {

    private static final long serialVersionUID = 339542836L;

    public static final QProduct product = new QProduct("product");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final BooleanPath activated = createBoolean("activated");

    //inherited
    public final StringPath className = _super.className;

    public final StringPath code = createString("code");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final ListPath<ProductDetail, QProductDetail> productDetails = this.<ProductDetail, QProductDetail>createList("productDetails", ProductDetail.class, QProductDetail.class, PathInits.DIRECT2);

    public final StringPath productTitle = createString("productTitle");

    public final NumberPath<java.math.BigDecimal> volumeLitre = createNumber("volumeLitre", java.math.BigDecimal.class);

    public final NumberPath<Integer> weightGram = createNumber("weightGram", Integer.class);

    public QProduct(String variable) {
        super(Product.class, forVariable(variable));
    }

    public QProduct(Path<? extends Product> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProduct(PathMetadata<?> metadata) {
        super(Product.class, metadata);
    }

}

