package com.kiga.s3.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QS3Image is a Querydsl query type for S3Image
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QS3Image extends EntityPathBase<S3Image> {

    private static final long serialVersionUID = 1014923958L;

    public static final QS3Image s3Image = new QS3Image("s3Image");

    public final QKigaS3File _super = new QKigaS3File(this);

    //inherited
    public final StringPath bucket = _super.bucket;

    public final StringPath cachedData = createString("cachedData");

    public final MapPath<String, String, StringPath> cachedDataJson = this.<String, String, StringPath>createMap("cachedDataJson", String.class, String.class, StringPath.class);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final StringPath folder = _super.folder;

    public final NumberPath<Integer> height = createNumber("height", Integer.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    //inherited
    public final StringPath name = _super.name;

    //inherited
    public final NumberPath<Long> ownerId = _super.ownerId;

    public final StringPath pdfUrl = createString("pdfUrl");

    //inherited
    public final NumberPath<Integer> size = _super.size;

    //inherited
    public final StringPath url = _super.url;

    public final NumberPath<Integer> width = createNumber("width", Integer.class);

    public QS3Image(String variable) {
        super(S3Image.class, forVariable(variable));
    }

    public QS3Image(Path<? extends S3Image> path) {
        super(path.getType(), path.getMetadata());
    }

    public QS3Image(PathMetadata<?> metadata) {
        super(S3Image.class, metadata);
    }

}

