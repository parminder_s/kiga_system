package com.kiga.shopcontent.domain.draft;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QProductPageDraft is a Querydsl query type for ProductPageDraft
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QProductPageDraft extends EntityPathBase<ProductPageDraft> {

    private static final long serialVersionUID = 2044116422L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProductPageDraft productPageDraft = new QProductPageDraft("productPageDraft");

    public final com.kiga.content.domain.ss.draft.QKigaPageDraft _super;

    //inherited
    public final StringPath author;

    //inherited
    public final NumberPath<Integer> automaticMetaDescription;

    //inherited
    public final StringPath canEditType;

    //inherited
    public final StringPath canPublishType;

    //inherited
    public final StringPath canViewType;

    //inherited
    public final ListPath<com.kiga.content.domain.ss.SiteTree<?>, SimplePath<com.kiga.content.domain.ss.SiteTree<?>>> children;

    //inherited
    public final StringPath className;

    //inherited
    public final StringPath code;

    //inherited
    public final StringPath content;

    //inherited
    public final DateTimePath<java.util.Date> created;

    //inherited
    public final StringPath description;

    //inherited
    public final NumberPath<Integer> dirty;

    //inherited
    public final DateTimePath<java.util.Date> expiryDate;

    //inherited
    public final StringPath extraMeta;

    //inherited
    public final NumberPath<Integer> facebookShare;

    //inherited
    public final NumberPath<Integer> forParents;

    //inherited
    public final NumberPath<Integer> hasBrokenFile;

    //inherited
    public final NumberPath<Integer> hasBrokenLink;

    //inherited
    public final StringPath homePageForDomain;

    //inherited
    public final NumberPath<Long> id;

    public final com.kiga.content.domain.ss.QKigaPageImage ideaImage;

    public final StringPath ideaTeaser = createString("ideaTeaser");

    public final StringPath ideaText = createString("ideaText");

    //inherited
    public final NumberPath<Integer> inPool;

    //inherited
    public final NumberPath<Integer> inSync;

    //inherited
    public final ListPath<com.kiga.content.domain.ss.KigaPageImage, com.kiga.content.domain.ss.QKigaPageImage> kigaPageImages;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited;

    //inherited
    public final NumberPath<Integer> legacyId;

    //inherited
    public final ListPath<com.kiga.content.domain.ss.draft.LinkedKigaPageGroupDraft, com.kiga.content.domain.ss.draft.QLinkedKigaPageGroupDraft> linkedKigaPageGroups;

    //inherited
    public final SimplePath<com.kiga.main.locale.Locale> locale;

    //inherited
    public final StringPath menuTitle;

    //inherited
    public final StringPath metaDescription;

    //inherited
    public final StringPath metaKeywords;

    //inherited
    public final StringPath metaTitle;

    //inherited
    public final NumberPath<Integer> noRobotIndex;

    //inherited
    public final StringPath oldId;

    //inherited
    public final NumberPath<Integer> outOfSync;

    // inherited
    public final com.kiga.content.domain.ss.draft.QSiteTreeDraft parent;

    //inherited
    public final NumberPath<Integer> pdfFileId;

    // inherited
    public final com.kiga.s3.domain.QKigaS3File pdfWoImage;

    // inherited
    public final com.kiga.content.domain.ss.QKigaPageImage previewImage;

    //inherited
    public final StringPath priority;

    public final com.kiga.shop.domain.QProduct product;

    //inherited
    public final NumberPath<Integer> provideComments;

    //inherited
    public final NumberPath<Integer> rating1;

    //inherited
    public final NumberPath<Integer> rating2;

    //inherited
    public final NumberPath<Integer> rating3;

    //inherited
    public final NumberPath<Integer> rating4;

    //inherited
    public final NumberPath<Integer> rating5;

    //inherited
    public final NumberPath<Float> ratingComplete;

    //inherited
    public final ListPath<com.kiga.content.domain.ss.Rating, com.kiga.content.domain.ss.QRating> ratings;

    //inherited
    public final StringPath reportClass;

    //inherited
    public final NumberPath<Integer> screenshotId;

    //inherited
    public final NumberPath<Integer> showInMenus;

    //inherited
    public final NumberPath<Integer> showInSearch;

    public final ListPath<SliderFileDraft, QSliderFileDraft> sliderFiles = this.<SliderFileDraft, QSliderFileDraft>createList("sliderFiles", SliderFileDraft.class, QSliderFileDraft.class, PathInits.DIRECT2);

    //inherited
    public final NumberPath<Integer> sort;

    //inherited
    public final NumberPath<Integer> sortDayMini;

    //inherited
    public final NumberPath<Integer> sortIndex;

    //inherited
    public final NumberPath<Integer> sortIndexMini;

    //inherited
    public final NumberPath<Integer> sortMonth;

    //inherited
    public final NumberPath<Integer> sortMonthMini;

    //inherited
    public final StringPath sortTree;

    //inherited
    public final NumberPath<Integer> sortYear;

    //inherited
    public final NumberPath<Integer> sortYearMini;

    //inherited
    public final StringPath status;

    //inherited
    public final StringPath subTitle;

    //inherited
    public final StringPath tagsSummarized;

    //inherited
    public final StringPath tagsText;

    //inherited
    public final StringPath title;

    //inherited
    public final StringPath toDo;

    //inherited
    public final StringPath urlSegment;

    //inherited
    public final NumberPath<Integer> version;

    public QProductPageDraft(String variable) {
        this(ProductPageDraft.class, forVariable(variable), INITS);
    }

    public QProductPageDraft(Path<? extends ProductPageDraft> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QProductPageDraft(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QProductPageDraft(PathMetadata<?> metadata, PathInits inits) {
        this(ProductPageDraft.class, metadata, inits);
    }

    public QProductPageDraft(Class<? extends ProductPageDraft> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new com.kiga.content.domain.ss.draft.QKigaPageDraft(type, metadata, inits);
        this.author = _super.author;
        this.automaticMetaDescription = _super.automaticMetaDescription;
        this.canEditType = _super.canEditType;
        this.canPublishType = _super.canPublishType;
        this.canViewType = _super.canViewType;
        this.children = _super.children;
        this.className = _super.className;
        this.code = _super.code;
        this.content = _super.content;
        this.created = _super.created;
        this.description = _super.description;
        this.dirty = _super.dirty;
        this.expiryDate = _super.expiryDate;
        this.extraMeta = _super.extraMeta;
        this.facebookShare = _super.facebookShare;
        this.forParents = _super.forParents;
        this.hasBrokenFile = _super.hasBrokenFile;
        this.hasBrokenLink = _super.hasBrokenLink;
        this.homePageForDomain = _super.homePageForDomain;
        this.id = _super.id;
        this.ideaImage = inits.isInitialized("ideaImage") ? new com.kiga.content.domain.ss.QKigaPageImage(forProperty("ideaImage"), inits.get("ideaImage")) : null;
        this.inPool = _super.inPool;
        this.inSync = _super.inSync;
        this.kigaPageImages = _super.kigaPageImages;
        this.lastEdited = _super.lastEdited;
        this.legacyId = _super.legacyId;
        this.linkedKigaPageGroups = _super.linkedKigaPageGroups;
        this.locale = _super.locale;
        this.menuTitle = _super.menuTitle;
        this.metaDescription = _super.metaDescription;
        this.metaKeywords = _super.metaKeywords;
        this.metaTitle = _super.metaTitle;
        this.noRobotIndex = _super.noRobotIndex;
        this.oldId = _super.oldId;
        this.outOfSync = _super.outOfSync;
        this.parent = _super.parent;
        this.pdfFileId = _super.pdfFileId;
        this.pdfWoImage = _super.pdfWoImage;
        this.previewImage = _super.previewImage;
        this.priority = _super.priority;
        this.product = inits.isInitialized("product") ? new com.kiga.shop.domain.QProduct(forProperty("product")) : null;
        this.provideComments = _super.provideComments;
        this.rating1 = _super.rating1;
        this.rating2 = _super.rating2;
        this.rating3 = _super.rating3;
        this.rating4 = _super.rating4;
        this.rating5 = _super.rating5;
        this.ratingComplete = _super.ratingComplete;
        this.ratings = _super.ratings;
        this.reportClass = _super.reportClass;
        this.screenshotId = _super.screenshotId;
        this.showInMenus = _super.showInMenus;
        this.showInSearch = _super.showInSearch;
        this.sort = _super.sort;
        this.sortDayMini = _super.sortDayMini;
        this.sortIndex = _super.sortIndex;
        this.sortIndexMini = _super.sortIndexMini;
        this.sortMonth = _super.sortMonth;
        this.sortMonthMini = _super.sortMonthMini;
        this.sortTree = _super.sortTree;
        this.sortYear = _super.sortYear;
        this.sortYearMini = _super.sortYearMini;
        this.status = _super.status;
        this.subTitle = _super.subTitle;
        this.tagsSummarized = _super.tagsSummarized;
        this.tagsText = _super.tagsText;
        this.title = _super.title;
        this.toDo = _super.toDo;
        this.urlSegment = _super.urlSegment;
        this.version = _super.version;
    }

}

