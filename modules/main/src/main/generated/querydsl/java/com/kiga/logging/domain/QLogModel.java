package com.kiga.logging.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QLogModel is a Querydsl query type for LogModel
 */
@Generated("com.mysema.query.codegen.SupertypeSerializer")
public class QLogModel extends EntityPathBase<LogModel> {

    private static final long serialVersionUID = 340616151L;

    public static final QLogModel logModel = new QLogModel("logModel");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    public final StringPath countryCode = createString("countryCode");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final SimplePath<com.kiga.main.locale.Locale> locale = createSimple("locale", com.kiga.main.locale.Locale.class);

    public final StringPath remoteIp = createString("remoteIp");

    public final StringPath sessionId = createString("sessionId");

    public QLogModel(String variable) {
        super(LogModel.class, forVariable(variable));
    }

    public QLogModel(Path<? extends LogModel> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLogModel(PathMetadata<?> metadata) {
        super(LogModel.class, metadata);
    }

}

