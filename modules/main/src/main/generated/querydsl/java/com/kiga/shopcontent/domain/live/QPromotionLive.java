package com.kiga.shopcontent.domain.live;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPromotionLive is a Querydsl query type for PromotionLive
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPromotionLive extends EntityPathBase<PromotionLive> {

    private static final long serialVersionUID = 1704911459L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPromotionLive promotionLive = new QPromotionLive("promotionLive");

    public final com.kiga.shopcontent.domain.QPromotion _super = new com.kiga.shopcontent.domain.QPromotion(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final com.kiga.content.domain.ss.live.QPageLive page;

    public final QProductPageLive productPage;

    public QPromotionLive(String variable) {
        this(PromotionLive.class, forVariable(variable), INITS);
    }

    public QPromotionLive(Path<? extends PromotionLive> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPromotionLive(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPromotionLive(PathMetadata<?> metadata, PathInits inits) {
        this(PromotionLive.class, metadata, inits);
    }

    public QPromotionLive(Class<? extends PromotionLive> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.page = inits.isInitialized("page") ? new com.kiga.content.domain.ss.live.QPageLive(forProperty("page"), inits.get("page")) : null;
        this.productPage = inits.isInitialized("productPage") ? new QProductPageLive(forProperty("productPage"), inits.get("productPage")) : null;
    }

}

