package com.kiga.homepage.domain.draft;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QHomePageElementDraft is a Querydsl query type for HomePageElementDraft
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QHomePageElementDraft extends EntityPathBase<HomePageElementDraft> {

    private static final long serialVersionUID = 1955885427L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QHomePageElementDraft homePageElementDraft = new QHomePageElementDraft("homePageElementDraft");

    public final com.kiga.homepage.domain.QHomePageElement _super;

    // inherited
    public final com.kiga.s3.domain.QS3Image big;

    //inherited
    public final StringPath className;

    //inherited
    public final StringPath color;

    //inherited
    public final EnumPath<com.kiga.homepage.domain.HomePageElementContext> context;

    //inherited
    public final DateTimePath<java.util.Date> created;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited;

    //inherited
    public final SimplePath<com.kiga.main.locale.Locale> locale;

    //inherited
    public final NumberPath<Integer> position;

    // inherited
    public final com.kiga.s3.domain.QS3Image small;

    //inherited
    public final StringPath title;

    //inherited
    public final StringPath titleShort;

    //inherited
    public final EnumPath<com.kiga.homepage.domain.HomePageElementType> type;

    //inherited
    public final StringPath url;

    public QHomePageElementDraft(String variable) {
        this(HomePageElementDraft.class, forVariable(variable), INITS);
    }

    public QHomePageElementDraft(Path<? extends HomePageElementDraft> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QHomePageElementDraft(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QHomePageElementDraft(PathMetadata<?> metadata, PathInits inits) {
        this(HomePageElementDraft.class, metadata, inits);
    }

    public QHomePageElementDraft(Class<? extends HomePageElementDraft> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new com.kiga.homepage.domain.QHomePageElement(type, metadata, inits);
        this.big = _super.big;
        this.className = _super.className;
        this.color = _super.color;
        this.context = _super.context;
        this.created = _super.created;
        this.lastEdited = _super.lastEdited;
        this.locale = _super.locale;
        this.position = _super.position;
        this.small = _super.small;
        this.title = _super.title;
        this.titleShort = _super.titleShort;
        this.type = _super.type;
        this.url = _super.url;
    }

}

