package com.kiga.content.domain.ss;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QRating is a Querydsl query type for Rating
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QRating extends EntityPathBase<Rating> {

    private static final long serialVersionUID = -1105758209L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRating rating = new QRating("rating");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final com.kiga.content.domain.ss.live.QKigaPageLive kigaPage;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final com.kiga.security.domain.QMember member;

    public final NumberPath<Integer> ratingValue = createNumber("ratingValue", Integer.class);

    public QRating(String variable) {
        this(Rating.class, forVariable(variable), INITS);
    }

    public QRating(Path<? extends Rating> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QRating(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QRating(PathMetadata<?> metadata, PathInits inits) {
        this(Rating.class, metadata, inits);
    }

    public QRating(Class<? extends Rating> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.kigaPage = inits.isInitialized("kigaPage") ? new com.kiga.content.domain.ss.live.QKigaPageLive(forProperty("kigaPage"), inits.get("kigaPage")) : null;
        this.member = inits.isInitialized("member") ? new com.kiga.security.domain.QMember(forProperty("member"), inits.get("member")) : null;
    }

}

