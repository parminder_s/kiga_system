package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QCreditVoucherItem is a Querydsl query type for CreditVoucherItem
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCreditVoucherItem extends EntityPathBase<CreditVoucherItem> {

    private static final long serialVersionUID = -357567955L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCreditVoucherItem creditVoucherItem = new QCreditVoucherItem("creditVoucherItem");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final NumberPath<Long> amount = createNumber("amount", Long.class);

    //inherited
    public final StringPath className = _super.className;

    public final StringPath code = createString("code");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<java.math.BigDecimal> priceGrossTotal = createNumber("priceGrossTotal", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> priceNetTotal = createNumber("priceNetTotal", java.math.BigDecimal.class);

    public final QProduct product;

    public final QPurchase purchase;

    public final StringPath title = createString("title");

    public final NumberPath<java.math.BigDecimal> vatRate = createNumber("vatRate", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> vatTotal = createNumber("vatTotal", java.math.BigDecimal.class);

    public QCreditVoucherItem(String variable) {
        this(CreditVoucherItem.class, forVariable(variable), INITS);
    }

    public QCreditVoucherItem(Path<? extends CreditVoucherItem> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCreditVoucherItem(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCreditVoucherItem(PathMetadata<?> metadata, PathInits inits) {
        this(CreditVoucherItem.class, metadata, inits);
    }

    public QCreditVoucherItem(Class<? extends CreditVoucherItem> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.product = inits.isInitialized("product") ? new QProduct(forProperty("product")) : null;
        this.purchase = inits.isInitialized("purchase") ? new QPurchase(forProperty("purchase"), inits.get("purchase")) : null;
    }

}

