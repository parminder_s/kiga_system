package com.kiga.content.domain.ss.draft;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QLinkedKigaPageGroupToKigaPageDraft is a Querydsl query type for LinkedKigaPageGroupToKigaPageDraft
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QLinkedKigaPageGroupToKigaPageDraft extends EntityPathBase<LinkedKigaPageGroupToKigaPageDraft> {

    private static final long serialVersionUID = -67747505L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QLinkedKigaPageGroupToKigaPageDraft linkedKigaPageGroupToKigaPageDraft = new QLinkedKigaPageGroupToKigaPageDraft("linkedKigaPageGroupToKigaPageDraft");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final QKigaIdeaDraft kigaPage;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final QLinkedKigaPageGroupDraft linkedKigaPageGroup;

    public QLinkedKigaPageGroupToKigaPageDraft(String variable) {
        this(LinkedKigaPageGroupToKigaPageDraft.class, forVariable(variable), INITS);
    }

    public QLinkedKigaPageGroupToKigaPageDraft(Path<? extends LinkedKigaPageGroupToKigaPageDraft> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QLinkedKigaPageGroupToKigaPageDraft(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QLinkedKigaPageGroupToKigaPageDraft(PathMetadata<?> metadata, PathInits inits) {
        this(LinkedKigaPageGroupToKigaPageDraft.class, metadata, inits);
    }

    public QLinkedKigaPageGroupToKigaPageDraft(Class<? extends LinkedKigaPageGroupToKigaPageDraft> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.kigaPage = inits.isInitialized("kigaPage") ? new QKigaIdeaDraft(forProperty("kigaPage"), inits.get("kigaPage")) : null;
        this.linkedKigaPageGroup = inits.isInitialized("linkedKigaPageGroup") ? new QLinkedKigaPageGroupDraft(forProperty("linkedKigaPageGroup"), inits.get("linkedKigaPageGroup")) : null;
    }

}

