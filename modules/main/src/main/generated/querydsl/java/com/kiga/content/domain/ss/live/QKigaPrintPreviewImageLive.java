package com.kiga.content.domain.ss.live;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QKigaPrintPreviewImageLive is a Querydsl query type for KigaPrintPreviewImageLive
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QKigaPrintPreviewImageLive extends EntityPathBase<KigaPrintPreviewImageLive> {

    private static final long serialVersionUID = 519454868L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QKigaPrintPreviewImageLive kigaPrintPreviewImageLive = new QKigaPrintPreviewImageLive("kigaPrintPreviewImageLive");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final QKigaIdeaLive kigaIdea;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<Integer> pageNumber = createNumber("pageNumber", Integer.class);

    public final com.kiga.s3.domain.QS3Image s3Image;

    public QKigaPrintPreviewImageLive(String variable) {
        this(KigaPrintPreviewImageLive.class, forVariable(variable), INITS);
    }

    public QKigaPrintPreviewImageLive(Path<? extends KigaPrintPreviewImageLive> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QKigaPrintPreviewImageLive(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QKigaPrintPreviewImageLive(PathMetadata<?> metadata, PathInits inits) {
        this(KigaPrintPreviewImageLive.class, metadata, inits);
    }

    public QKigaPrintPreviewImageLive(Class<? extends KigaPrintPreviewImageLive> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.kigaIdea = inits.isInitialized("kigaIdea") ? new QKigaIdeaLive(forProperty("kigaIdea"), inits.get("kigaIdea")) : null;
        this.s3Image = inits.isInitialized("s3Image") ? new com.kiga.s3.domain.QS3Image(forProperty("s3Image")) : null;
    }

}

