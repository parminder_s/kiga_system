package com.kiga.content.domain.ss.live;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPageLive is a Querydsl query type for PageLive
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPageLive extends EntityPathBase<PageLive> {

    private static final long serialVersionUID = -653521285L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPageLive pageLive = new QPageLive("pageLive");

    public final QSiteTreeLive _super;

    //inherited
    public final StringPath canEditType;

    //inherited
    public final StringPath canPublishType;

    //inherited
    public final StringPath canViewType;

    //inherited
    public final ListPath<com.kiga.content.domain.ss.SiteTree<?>, SimplePath<com.kiga.content.domain.ss.SiteTree<?>>> children;

    //inherited
    public final StringPath className;

    public final StringPath code = createString("code");

    //inherited
    public final StringPath content;

    //inherited
    public final DateTimePath<java.util.Date> created;

    //inherited
    public final DateTimePath<java.util.Date> expiryDate;

    //inherited
    public final StringPath extraMeta;

    //inherited
    public final NumberPath<Integer> hasBrokenFile;

    //inherited
    public final NumberPath<Integer> hasBrokenLink;

    //inherited
    public final StringPath homePageForDomain;

    //inherited
    public final NumberPath<Long> id;

    public final NumberPath<Integer> inSync = createNumber("inSync", Integer.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited;

    public final NumberPath<Integer> legacyId = createNumber("legacyId", Integer.class);

    //inherited
    public final SimplePath<com.kiga.main.locale.Locale> locale;

    //inherited
    public final StringPath menuTitle;

    //inherited
    public final StringPath metaDescription;

    //inherited
    public final StringPath metaKeywords;

    //inherited
    public final StringPath metaTitle;

    public final NumberPath<Integer> noRobotIndex = createNumber("noRobotIndex", Integer.class);

    public final NumberPath<Integer> outOfSync = createNumber("outOfSync", Integer.class);

    // inherited
    public final QSiteTreeLive parent;

    //inherited
    public final StringPath priority;

    //inherited
    public final NumberPath<Integer> provideComments;

    //inherited
    public final StringPath reportClass;

    //inherited
    public final NumberPath<Integer> showInMenus;

    //inherited
    public final NumberPath<Integer> showInSearch;

    //inherited
    public final NumberPath<Integer> sort;

    //inherited
    public final StringPath sortTree;

    //inherited
    public final StringPath status;

    public final StringPath subTitle = createString("subTitle");

    //inherited
    public final StringPath title;

    //inherited
    public final StringPath toDo;

    //inherited
    public final StringPath urlSegment;

    //inherited
    public final NumberPath<Integer> version;

    public QPageLive(String variable) {
        this(PageLive.class, forVariable(variable), INITS);
    }

    public QPageLive(Path<? extends PageLive> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPageLive(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPageLive(PathMetadata<?> metadata, PathInits inits) {
        this(PageLive.class, metadata, inits);
    }

    public QPageLive(Class<? extends PageLive> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new QSiteTreeLive(type, metadata, inits);
        this.canEditType = _super.canEditType;
        this.canPublishType = _super.canPublishType;
        this.canViewType = _super.canViewType;
        this.children = _super.children;
        this.className = _super.className;
        this.content = _super.content;
        this.created = _super.created;
        this.expiryDate = _super.expiryDate;
        this.extraMeta = _super.extraMeta;
        this.hasBrokenFile = _super.hasBrokenFile;
        this.hasBrokenLink = _super.hasBrokenLink;
        this.homePageForDomain = _super.homePageForDomain;
        this.id = _super.id;
        this.lastEdited = _super.lastEdited;
        this.locale = _super.locale;
        this.menuTitle = _super.menuTitle;
        this.metaDescription = _super.metaDescription;
        this.metaKeywords = _super.metaKeywords;
        this.metaTitle = _super.metaTitle;
        this.parent = _super.parent;
        this.priority = _super.priority;
        this.provideComments = _super.provideComments;
        this.reportClass = _super.reportClass;
        this.showInMenus = _super.showInMenus;
        this.showInSearch = _super.showInSearch;
        this.sort = _super.sort;
        this.sortTree = _super.sortTree;
        this.status = _super.status;
        this.title = _super.title;
        this.toDo = _super.toDo;
        this.urlSegment = _super.urlSegment;
        this.version = _super.version;
    }

}

