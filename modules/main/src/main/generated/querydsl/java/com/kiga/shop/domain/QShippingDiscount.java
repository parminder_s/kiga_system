package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QShippingDiscount is a Querydsl query type for ShippingDiscount
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QShippingDiscount extends EntityPathBase<ShippingDiscount> {

    private static final long serialVersionUID = 965554282L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QShippingDiscount shippingDiscount = new QShippingDiscount("shippingDiscount");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<java.math.BigDecimal> discountValue = createNumber("discountValue", java.math.BigDecimal.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final QProductDetail productDetail;

    public final NumberPath<Integer> threshold = createNumber("threshold", Integer.class);

    public QShippingDiscount(String variable) {
        this(ShippingDiscount.class, forVariable(variable), INITS);
    }

    public QShippingDiscount(Path<? extends ShippingDiscount> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QShippingDiscount(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QShippingDiscount(PathMetadata<?> metadata, PathInits inits) {
        this(ShippingDiscount.class, metadata, inits);
    }

    public QShippingDiscount(Class<? extends ShippingDiscount> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.productDetail = inits.isInitialized("productDetail") ? new QProductDetail(forProperty("productDetail"), inits.get("productDetail")) : null;
    }

}

