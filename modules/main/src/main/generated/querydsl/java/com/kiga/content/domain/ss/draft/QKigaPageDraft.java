package com.kiga.content.domain.ss.draft;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QKigaPageDraft is a Querydsl query type for KigaPageDraft
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QKigaPageDraft extends EntityPathBase<KigaPageDraft> {

    private static final long serialVersionUID = 298136523L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QKigaPageDraft kigaPageDraft = new QKigaPageDraft("kigaPageDraft");

    public final QPageDraft _super;

    public final StringPath author = createString("author");

    public final NumberPath<Integer> automaticMetaDescription = createNumber("automaticMetaDescription", Integer.class);

    //inherited
    public final StringPath canEditType;

    //inherited
    public final StringPath canPublishType;

    //inherited
    public final StringPath canViewType;

    //inherited
    public final ListPath<com.kiga.content.domain.ss.SiteTree<?>, SimplePath<com.kiga.content.domain.ss.SiteTree<?>>> children;

    //inherited
    public final StringPath className;

    //inherited
    public final StringPath code;

    //inherited
    public final StringPath content;

    //inherited
    public final DateTimePath<java.util.Date> created;

    public final StringPath description = createString("description");

    public final NumberPath<Integer> dirty = createNumber("dirty", Integer.class);

    //inherited
    public final DateTimePath<java.util.Date> expiryDate;

    //inherited
    public final StringPath extraMeta;

    public final NumberPath<Integer> facebookShare = createNumber("facebookShare", Integer.class);

    public final NumberPath<Integer> forParents = createNumber("forParents", Integer.class);

    //inherited
    public final NumberPath<Integer> hasBrokenFile;

    //inherited
    public final NumberPath<Integer> hasBrokenLink;

    //inherited
    public final StringPath homePageForDomain;

    //inherited
    public final NumberPath<Long> id;

    public final NumberPath<Integer> inPool = createNumber("inPool", Integer.class);

    //inherited
    public final NumberPath<Integer> inSync;

    public final ListPath<com.kiga.content.domain.ss.KigaPageImage, com.kiga.content.domain.ss.QKigaPageImage> kigaPageImages = this.<com.kiga.content.domain.ss.KigaPageImage, com.kiga.content.domain.ss.QKigaPageImage>createList("kigaPageImages", com.kiga.content.domain.ss.KigaPageImage.class, com.kiga.content.domain.ss.QKigaPageImage.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited;

    //inherited
    public final NumberPath<Integer> legacyId;

    public final ListPath<LinkedKigaPageGroupDraft, QLinkedKigaPageGroupDraft> linkedKigaPageGroups = this.<LinkedKigaPageGroupDraft, QLinkedKigaPageGroupDraft>createList("linkedKigaPageGroups", LinkedKigaPageGroupDraft.class, QLinkedKigaPageGroupDraft.class, PathInits.DIRECT2);

    //inherited
    public final SimplePath<com.kiga.main.locale.Locale> locale;

    //inherited
    public final StringPath menuTitle;

    //inherited
    public final StringPath metaDescription;

    //inherited
    public final StringPath metaKeywords;

    //inherited
    public final StringPath metaTitle;

    //inherited
    public final NumberPath<Integer> noRobotIndex;

    public final StringPath oldId = createString("oldId");

    //inherited
    public final NumberPath<Integer> outOfSync;

    // inherited
    public final QSiteTreeDraft parent;

    public final NumberPath<Integer> pdfFileId = createNumber("pdfFileId", Integer.class);

    public final com.kiga.s3.domain.QKigaS3File pdfWoImage;

    public final com.kiga.content.domain.ss.QKigaPageImage previewImage;

    //inherited
    public final StringPath priority;

    //inherited
    public final NumberPath<Integer> provideComments;

    public final NumberPath<Integer> rating1 = createNumber("rating1", Integer.class);

    public final NumberPath<Integer> rating2 = createNumber("rating2", Integer.class);

    public final NumberPath<Integer> rating3 = createNumber("rating3", Integer.class);

    public final NumberPath<Integer> rating4 = createNumber("rating4", Integer.class);

    public final NumberPath<Integer> rating5 = createNumber("rating5", Integer.class);

    public final NumberPath<Float> ratingComplete = createNumber("ratingComplete", Float.class);

    public final ListPath<com.kiga.content.domain.ss.Rating, com.kiga.content.domain.ss.QRating> ratings = this.<com.kiga.content.domain.ss.Rating, com.kiga.content.domain.ss.QRating>createList("ratings", com.kiga.content.domain.ss.Rating.class, com.kiga.content.domain.ss.QRating.class, PathInits.DIRECT2);

    //inherited
    public final StringPath reportClass;

    public final NumberPath<Integer> screenshotId = createNumber("screenshotId", Integer.class);

    //inherited
    public final NumberPath<Integer> showInMenus;

    //inherited
    public final NumberPath<Integer> showInSearch;

    //inherited
    public final NumberPath<Integer> sort;

    public final NumberPath<Integer> sortDayMini = createNumber("sortDayMini", Integer.class);

    public final NumberPath<Integer> sortIndex = createNumber("sortIndex", Integer.class);

    public final NumberPath<Integer> sortIndexMini = createNumber("sortIndexMini", Integer.class);

    public final NumberPath<Integer> sortMonth = createNumber("sortMonth", Integer.class);

    public final NumberPath<Integer> sortMonthMini = createNumber("sortMonthMini", Integer.class);

    //inherited
    public final StringPath sortTree;

    public final NumberPath<Integer> sortYear = createNumber("sortYear", Integer.class);

    public final NumberPath<Integer> sortYearMini = createNumber("sortYearMini", Integer.class);

    //inherited
    public final StringPath status;

    //inherited
    public final StringPath subTitle;

    public final StringPath tagsSummarized = createString("tagsSummarized");

    public final StringPath tagsText = createString("tagsText");

    //inherited
    public final StringPath title;

    //inherited
    public final StringPath toDo;

    //inherited
    public final StringPath urlSegment;

    //inherited
    public final NumberPath<Integer> version;

    public QKigaPageDraft(String variable) {
        this(KigaPageDraft.class, forVariable(variable), INITS);
    }

    public QKigaPageDraft(Path<? extends KigaPageDraft> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QKigaPageDraft(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QKigaPageDraft(PathMetadata<?> metadata, PathInits inits) {
        this(KigaPageDraft.class, metadata, inits);
    }

    public QKigaPageDraft(Class<? extends KigaPageDraft> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new QPageDraft(type, metadata, inits);
        this.canEditType = _super.canEditType;
        this.canPublishType = _super.canPublishType;
        this.canViewType = _super.canViewType;
        this.children = _super.children;
        this.className = _super.className;
        this.code = _super.code;
        this.content = _super.content;
        this.created = _super.created;
        this.expiryDate = _super.expiryDate;
        this.extraMeta = _super.extraMeta;
        this.hasBrokenFile = _super.hasBrokenFile;
        this.hasBrokenLink = _super.hasBrokenLink;
        this.homePageForDomain = _super.homePageForDomain;
        this.id = _super.id;
        this.inSync = _super.inSync;
        this.lastEdited = _super.lastEdited;
        this.legacyId = _super.legacyId;
        this.locale = _super.locale;
        this.menuTitle = _super.menuTitle;
        this.metaDescription = _super.metaDescription;
        this.metaKeywords = _super.metaKeywords;
        this.metaTitle = _super.metaTitle;
        this.noRobotIndex = _super.noRobotIndex;
        this.outOfSync = _super.outOfSync;
        this.parent = _super.parent;
        this.pdfWoImage = inits.isInitialized("pdfWoImage") ? new com.kiga.s3.domain.QKigaS3File(forProperty("pdfWoImage")) : null;
        this.previewImage = inits.isInitialized("previewImage") ? new com.kiga.content.domain.ss.QKigaPageImage(forProperty("previewImage"), inits.get("previewImage")) : null;
        this.priority = _super.priority;
        this.provideComments = _super.provideComments;
        this.reportClass = _super.reportClass;
        this.showInMenus = _super.showInMenus;
        this.showInSearch = _super.showInSearch;
        this.sort = _super.sort;
        this.sortTree = _super.sortTree;
        this.status = _super.status;
        this.subTitle = _super.subTitle;
        this.title = _super.title;
        this.toDo = _super.toDo;
        this.urlSegment = _super.urlSegment;
        this.version = _super.version;
    }

}

