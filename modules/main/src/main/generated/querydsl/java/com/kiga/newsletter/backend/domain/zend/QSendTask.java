package com.kiga.newsletter.backend.domain.zend;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QSendTask is a Querydsl query type for SendTask
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSendTask extends EntityPathBase<SendTask> {

    private static final long serialVersionUID = 1959440662L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSendTask sendTask = new QSendTask("sendTask");

    public final DateTimePath<java.util.Date> created = createDateTime("created", java.util.Date.class);

    public final NumberPath<Long> done = createNumber("done", Long.class);

    public final NumberPath<Long> error = createNumber("error", Long.class);

    public final DateTimePath<java.util.Date> finished = createDateTime("finished", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final BooleanPath isFinished = createBoolean("isFinished");

    public final QNewsletter newsletter;

    public final NumberPath<Long> recipients = createNumber("recipients", Long.class);

    public final DateTimePath<java.util.Date> sendDate = createDateTime("sendDate", java.util.Date.class);

    public final NumberPath<Long> skipped = createNumber("skipped", Long.class);

    public QSendTask(String variable) {
        this(SendTask.class, forVariable(variable), INITS);
    }

    public QSendTask(Path<? extends SendTask> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSendTask(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSendTask(PathMetadata<?> metadata, PathInits inits) {
        this(SendTask.class, metadata, inits);
    }

    public QSendTask(Class<? extends SendTask> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.newsletter = inits.isInitialized("newsletter") ? new QNewsletter(forProperty("newsletter")) : null;
    }

}

