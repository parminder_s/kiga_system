package com.kiga.newsletter.backend.domain.zend;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QQueueEntry is a Querydsl query type for QueueEntry
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QQueueEntry extends EntityPathBase<QueueEntry> {

    private static final long serialVersionUID = -1066670198L;

    public static final QQueueEntry queueEntry = new QQueueEntry("queueEntry");

    public final com.kiga.db.QKigaLegacyDbEntityModel _super = new com.kiga.db.QKigaLegacyDbEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath email = createString("email");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final StringPath newsletter = createString("newsletter");

    public final DateTimePath<java.util.Date> sendDate = createDateTime("sendDate", java.util.Date.class);

    public final NumberPath<Long> sendTaskId = createNumber("sendTaskId", Long.class);

    public final EnumPath<com.kiga.newsletter.backend.domain.QueueEntryStatus> status = createEnum("status", com.kiga.newsletter.backend.domain.QueueEntryStatus.class);

    public QQueueEntry(String variable) {
        super(QueueEntry.class, forVariable(variable));
    }

    public QQueueEntry(Path<? extends QueueEntry> path) {
        super(path.getType(), path.getMetadata());
    }

    public QQueueEntry(PathMetadata<?> metadata) {
        super(QueueEntry.class, metadata);
    }

}

