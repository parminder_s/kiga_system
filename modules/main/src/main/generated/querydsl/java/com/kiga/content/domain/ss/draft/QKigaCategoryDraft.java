package com.kiga.content.domain.ss.draft;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QKigaCategoryDraft is a Querydsl query type for KigaCategoryDraft
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QKigaCategoryDraft extends EntityPathBase<KigaCategoryDraft> {

    private static final long serialVersionUID = -1558928836L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QKigaCategoryDraft kigaCategoryDraft = new QKigaCategoryDraft("kigaCategoryDraft");

    public final QPageDraft _super;

    //inherited
    public final StringPath canEditType;

    //inherited
    public final StringPath canPublishType;

    //inherited
    public final StringPath canViewType;

    //inherited
    public final ListPath<com.kiga.content.domain.ss.SiteTree<?>, SimplePath<com.kiga.content.domain.ss.SiteTree<?>>> children;

    //inherited
    public final StringPath className;

    //inherited
    public final StringPath code;

    //inherited
    public final StringPath content;

    //inherited
    public final DateTimePath<java.util.Date> created;

    //inherited
    public final DateTimePath<java.util.Date> expiryDate;

    //inherited
    public final StringPath extraMeta;

    //inherited
    public final NumberPath<Integer> hasBrokenFile;

    //inherited
    public final NumberPath<Integer> hasBrokenLink;

    //inherited
    public final StringPath homePageForDomain;

    //inherited
    public final NumberPath<Long> id;

    public final StringPath infoText = createString("infoText");

    //inherited
    public final NumberPath<Integer> inSync;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited;

    //inherited
    public final NumberPath<Integer> legacyId;

    //inherited
    public final SimplePath<com.kiga.main.locale.Locale> locale;

    //inherited
    public final StringPath menuTitle;

    //inherited
    public final StringPath metaDescription;

    //inherited
    public final StringPath metaKeywords;

    //inherited
    public final StringPath metaTitle;

    //inherited
    public final NumberPath<Integer> noRobotIndex;

    //inherited
    public final NumberPath<Integer> outOfSync;

    // inherited
    public final QSiteTreeDraft parent;

    //inherited
    public final StringPath priority;

    //inherited
    public final NumberPath<Integer> provideComments;

    //inherited
    public final StringPath reportClass;

    //inherited
    public final NumberPath<Integer> showInMenus;

    //inherited
    public final NumberPath<Integer> showInSearch;

    //inherited
    public final NumberPath<Integer> sort;

    //inherited
    public final StringPath sortTree;

    //inherited
    public final StringPath status;

    //inherited
    public final StringPath subTitle;

    //inherited
    public final StringPath title;

    //inherited
    public final StringPath toDo;

    //inherited
    public final StringPath urlSegment;

    //inherited
    public final NumberPath<Integer> version;

    public QKigaCategoryDraft(String variable) {
        this(KigaCategoryDraft.class, forVariable(variable), INITS);
    }

    public QKigaCategoryDraft(Path<? extends KigaCategoryDraft> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QKigaCategoryDraft(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QKigaCategoryDraft(PathMetadata<?> metadata, PathInits inits) {
        this(KigaCategoryDraft.class, metadata, inits);
    }

    public QKigaCategoryDraft(Class<? extends KigaCategoryDraft> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new QPageDraft(type, metadata, inits);
        this.canEditType = _super.canEditType;
        this.canPublishType = _super.canPublishType;
        this.canViewType = _super.canViewType;
        this.children = _super.children;
        this.className = _super.className;
        this.code = _super.code;
        this.content = _super.content;
        this.created = _super.created;
        this.expiryDate = _super.expiryDate;
        this.extraMeta = _super.extraMeta;
        this.hasBrokenFile = _super.hasBrokenFile;
        this.hasBrokenLink = _super.hasBrokenLink;
        this.homePageForDomain = _super.homePageForDomain;
        this.id = _super.id;
        this.inSync = _super.inSync;
        this.lastEdited = _super.lastEdited;
        this.legacyId = _super.legacyId;
        this.locale = _super.locale;
        this.menuTitle = _super.menuTitle;
        this.metaDescription = _super.metaDescription;
        this.metaKeywords = _super.metaKeywords;
        this.metaTitle = _super.metaTitle;
        this.noRobotIndex = _super.noRobotIndex;
        this.outOfSync = _super.outOfSync;
        this.parent = _super.parent;
        this.priority = _super.priority;
        this.provideComments = _super.provideComments;
        this.reportClass = _super.reportClass;
        this.showInMenus = _super.showInMenus;
        this.showInSearch = _super.showInSearch;
        this.sort = _super.sort;
        this.sortTree = _super.sortTree;
        this.status = _super.status;
        this.subTitle = _super.subTitle;
        this.title = _super.title;
        this.toDo = _super.toDo;
        this.urlSegment = _super.urlSegment;
        this.version = _super.version;
    }

}

