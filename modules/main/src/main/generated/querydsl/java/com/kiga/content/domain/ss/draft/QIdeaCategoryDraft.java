package com.kiga.content.domain.ss.draft;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QIdeaCategoryDraft is a Querydsl query type for IdeaCategoryDraft
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QIdeaCategoryDraft extends EntityPathBase<IdeaCategoryDraft> {

    private static final long serialVersionUID = -433013667L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QIdeaCategoryDraft ideaCategoryDraft = new QIdeaCategoryDraft("ideaCategoryDraft");

    public final QKigaCategoryDraft _super;

    //inherited
    public final StringPath canEditType;

    //inherited
    public final StringPath canPublishType;

    //inherited
    public final StringPath canViewType;

    //inherited
    public final ListPath<com.kiga.content.domain.ss.SiteTree<?>, SimplePath<com.kiga.content.domain.ss.SiteTree<?>>> children;

    //inherited
    public final StringPath className;

    //inherited
    public final StringPath code;

    //inherited
    public final StringPath content;

    //inherited
    public final DateTimePath<java.util.Date> created;

    public final BooleanPath disableForFavourites = createBoolean("disableForFavourites");

    //inherited
    public final DateTimePath<java.util.Date> expiryDate;

    //inherited
    public final StringPath extraMeta;

    //inherited
    public final NumberPath<Integer> hasBrokenFile;

    //inherited
    public final NumberPath<Integer> hasBrokenLink;

    public final NumberPath<Integer> homepageCol = createNumber("homepageCol", Integer.class);

    //inherited
    public final StringPath homePageForDomain;

    public final StringPath iconCode = createString("iconCode");

    //inherited
    public final NumberPath<Long> id;

    //inherited
    public final StringPath infoText;

    //inherited
    public final NumberPath<Integer> inSync;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited;

    //inherited
    public final NumberPath<Integer> legacyId;

    //inherited
    public final SimplePath<com.kiga.main.locale.Locale> locale;

    //inherited
    public final StringPath menuTitle;

    //inherited
    public final StringPath metaDescription;

    //inherited
    public final StringPath metaKeywords;

    //inherited
    public final StringPath metaTitle;

    //inherited
    public final NumberPath<Integer> noRobotIndex;

    //inherited
    public final NumberPath<Integer> outOfSync;

    // inherited
    public final QSiteTreeDraft parent;

    public final NumberPath<Integer> position = createNumber("position", Integer.class);

    //inherited
    public final StringPath priority;

    //inherited
    public final NumberPath<Integer> provideComments;

    //inherited
    public final StringPath reportClass;

    //inherited
    public final NumberPath<Integer> showInMenus;

    //inherited
    public final NumberPath<Integer> showInSearch;

    //inherited
    public final NumberPath<Integer> sort;

    public final EnumPath<com.kiga.content.domain.ss.property.SortStrategy> sortStrategy = createEnum("sortStrategy", com.kiga.content.domain.ss.property.SortStrategy.class);

    //inherited
    public final StringPath sortTree;

    //inherited
    public final StringPath status;

    //inherited
    public final StringPath subTitle;

    //inherited
    public final StringPath title;

    //inherited
    public final StringPath toDo;

    //inherited
    public final StringPath urlSegment;

    //inherited
    public final NumberPath<Integer> version;

    public QIdeaCategoryDraft(String variable) {
        this(IdeaCategoryDraft.class, forVariable(variable), INITS);
    }

    public QIdeaCategoryDraft(Path<? extends IdeaCategoryDraft> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QIdeaCategoryDraft(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QIdeaCategoryDraft(PathMetadata<?> metadata, PathInits inits) {
        this(IdeaCategoryDraft.class, metadata, inits);
    }

    public QIdeaCategoryDraft(Class<? extends IdeaCategoryDraft> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new QKigaCategoryDraft(type, metadata, inits);
        this.canEditType = _super.canEditType;
        this.canPublishType = _super.canPublishType;
        this.canViewType = _super.canViewType;
        this.children = _super.children;
        this.className = _super.className;
        this.code = _super.code;
        this.content = _super.content;
        this.created = _super.created;
        this.expiryDate = _super.expiryDate;
        this.extraMeta = _super.extraMeta;
        this.hasBrokenFile = _super.hasBrokenFile;
        this.hasBrokenLink = _super.hasBrokenLink;
        this.homePageForDomain = _super.homePageForDomain;
        this.id = _super.id;
        this.infoText = _super.infoText;
        this.inSync = _super.inSync;
        this.lastEdited = _super.lastEdited;
        this.legacyId = _super.legacyId;
        this.locale = _super.locale;
        this.menuTitle = _super.menuTitle;
        this.metaDescription = _super.metaDescription;
        this.metaKeywords = _super.metaKeywords;
        this.metaTitle = _super.metaTitle;
        this.noRobotIndex = _super.noRobotIndex;
        this.outOfSync = _super.outOfSync;
        this.parent = _super.parent;
        this.priority = _super.priority;
        this.provideComments = _super.provideComments;
        this.reportClass = _super.reportClass;
        this.showInMenus = _super.showInMenus;
        this.showInSearch = _super.showInSearch;
        this.sort = _super.sort;
        this.sortTree = _super.sortTree;
        this.status = _super.status;
        this.subTitle = _super.subTitle;
        this.title = _super.title;
        this.toDo = _super.toDo;
        this.urlSegment = _super.urlSegment;
        this.version = _super.version;
    }

}

