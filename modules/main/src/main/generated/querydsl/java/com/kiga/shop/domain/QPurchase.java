package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;

/**
 * QPurchase is a Querydsl query type for Purchase
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPurchase extends EntityPathBase<Purchase> {

    private static final long serialVersionUID = 388011868L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPurchase purchase = new QPurchase("purchase");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final StringPath addressName1 = createString("addressName1");

    public final StringPath addressName2 = createString("addressName2");

    public final BooleanPath agb = createBoolean("agb");

    public final StringPath billAddressName1 = createString("billAddressName1");

    public final StringPath billAddressName2 = createString("billAddressName2");

    public final StringPath billCity = createString("billCity");

    public final QCountry billCountry;

    public final StringPath billDoorNumber = createString("billDoorNumber");

    public final StringPath billFloorNumber = createString("billFloorNumber");

    public final BooleanPath billOn = createBoolean("billOn");

    public final StringPath billStreet = createString("billStreet");

    public final StringPath billStreetNumber = createString("billStreetNumber");

    public final StringPath billZip = createString("billZip");

    public final StringPath birthday = createString("birthday");

    public final StringPath city = createString("city");

    // inherited
    public final StringPath className = _super.className;

    public final StringPath comment = createString("comment");

    public final QCountry country;

    public final StringPath countryTitle = createString("countryTitle");

    // inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final DateTimePath<java.time.Instant> creditVoucherDate = createDateTime("creditVoucherDate",
            java.time.Instant.class);

    public final StringPath creditVoucherNr = createString("creditVoucherNr");

    public final StringPath creditVoucherS3Bucket = createString("creditVoucherS3Bucket");

    public final StringPath creditVoucherS3Url = createString("creditVoucherS3Url");

    public final StringPath currency = createString("currency");

    public final StringPath currentStep = createString("currentStep");

    public final com.kiga.security.domain.QMember customer;

    public final StringPath doorNumber = createString("doorNumber");

    public final StringPath email = createString("email");

    public final StringPath firstname = createString("firstname");

    public final StringPath floorNumber = createString("floorNumber");

    public final StringPath gender = createString("gender");

    // inherited
    public final NumberPath<Long> id = _super.id;

    public final DateTimePath<java.time.Instant> invoiceDate = createDateTime("invoiceDate", java.time.Instant.class);

    public final StringPath invoiceHash = createString("invoiceHash");

    public final NumberPath<Integer> invoiceId = createNumber("invoiceId", Integer.class);

    public final StringPath invoiceNr = createString("invoiceNr");

    public final StringPath invoiceOption = createString("invoiceOption");

    public final StringPath invoiceS3Url = createString("invoiceS3Url");

    // inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final StringPath lastname = createString("lastname");

    public final SimplePath<com.kiga.main.locale.Locale> locale = createSimple("locale",
            com.kiga.main.locale.Locale.class);

    public final BooleanPath newsletter = createBoolean("newsletter");

    public final DateTimePath<java.time.Instant> orderDate = createDateTime("orderDate", java.time.Instant.class);

    public final NumberPath<Long> orderNr = createNumber("orderNr", Long.class);

    public final EnumPath<com.kiga.main.paymentmethod.PaymentMethod> paymentMethod = createEnum("paymentMethod",
            com.kiga.main.paymentmethod.PaymentMethod.class);

    public final StringPath paymentPspId = createString("paymentPspId");

    public final StringPath phoneNr = createString("phoneNr");

    public final NumberPath<Integer> pickingNoteId = createNumber("pickingNoteId", Integer.class);

    public final NumberPath<java.math.BigDecimal> priceGrossTotal = createNumber("priceGrossTotal",
            java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> priceNetTotal = createNumber("priceNetTotal",
            java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> priceShippingNet = createNumber("priceShippingNet",
            java.math.BigDecimal.class);

    public final ListPath<PurchaseItem, QPurchaseItem> purchaseItems = this.<PurchaseItem, QPurchaseItem>createList(
            "purchaseItems", PurchaseItem.class, QPurchaseItem.class, PathInits.DIRECT2);

    public final ListPath<PurchaseWorkflow, QPurchaseWorkflow> purchaseWorkflows = this
            .<PurchaseWorkflow, QPurchaseWorkflow>createList("purchaseWorkflows", PurchaseWorkflow.class,
                    QPurchaseWorkflow.class, PathInits.DIRECT2);

    public final BooleanPath security = createBoolean("security");

    public final StringPath sessionId = createString("sessionId");

    public final StringPath shippingAddressName1 = createString("shippingAddressName1");

    public final StringPath shippingAddressName2 = createString("shippingAddressName2");

    public final StringPath shippingCity = createString("shippingCity");

    public final StringPath shippingDoorNumber = createString("shippingDoorNumber");

    public final StringPath shippingFloorNumber = createString("shippingFloorNumber");

    public final NumberPath<Integer> shippingNoteId = createNumber("shippingNoteId", Integer.class);

    public final StringPath shippingNoteNr = createString("shippingNoteNr");

    public final StringPath shippingNoteS3Url = createString("shippingNoteS3Url");

    public final BooleanPath shippingOn = createBoolean("shippingOn");

    public final EnumPath<ShippingPartner> shippingPartner = createEnum("shippingPartner", ShippingPartner.class);

    public final StringPath shippingStreet = createString("shippingStreet");

    public final StringPath shippingStreetNumber = createString("shippingStreetNumber");

    public final StringPath shippingZip = createString("shippingZip");

    public final EnumPath<PurchaseStatus> status = createEnum("status", PurchaseStatus.class);

    public final StringPath street = createString("street");

    public final StringPath streetNumber = createString("streetNumber");

    public final NumberPath<java.math.BigDecimal> vatRateShipping = createNumber("vatRateShipping",
            java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> vatShipping = createNumber("vatShipping", java.math.BigDecimal.class);

    public final StringPath zip = createString("zip");

    public QPurchase(String variable) {
        this(Purchase.class, forVariable(variable), INITS);
    }

    public QPurchase(Path<? extends Purchase> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPurchase(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPurchase(PathMetadata<?> metadata, PathInits inits) {
        this(Purchase.class, metadata, inits);
    }

    public QPurchase(Class<? extends Purchase> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.billCountry = inits.isInitialized("billCountry")
                ? new QCountry(forProperty("billCountry"), inits.get("billCountry"))
                : null;
        this.country = inits.isInitialized("country") ? new QCountry(forProperty("country"), inits.get("country"))
                : null;
        this.customer = inits.isInitialized("customer")
                ? new com.kiga.security.domain.QMember(forProperty("customer"), inits.get("customer"))
                : null;
    }

}
