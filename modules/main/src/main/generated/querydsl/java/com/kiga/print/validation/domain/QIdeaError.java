package com.kiga.print.validation.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QIdeaError is a Querydsl query type for IdeaError
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QIdeaError extends EntityPathBase<IdeaError> {

    private static final long serialVersionUID = 660237554L;

    public static final QIdeaError ideaError = new QIdeaError("ideaError");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final NumberPath<Long> articleId = createNumber("articleId", Long.class);

    public final BooleanPath autoCorrectable = createBoolean("autoCorrectable");

    public final BooleanPath autoCorrected = createBoolean("autoCorrected");

    //inherited
    public final StringPath className = _super.className;

    public final NumberPath<Integer> codeColumn = createNumber("codeColumn", Integer.class);

    public final NumberPath<Integer> codeLine = createNumber("codeLine", Integer.class);

    public final StringPath content = createString("content");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath errorDetail = createString("errorDetail");

    public final NumberPath<Integer> errorGroup = createNumber("errorGroup", Integer.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final NumberPath<Long> ideaValidationReportId = createNumber("ideaValidationReportId", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final SimplePath<com.kiga.main.locale.Locale> locale = createSimple("locale", com.kiga.main.locale.Locale.class);

    public QIdeaError(String variable) {
        super(IdeaError.class, forVariable(variable));
    }

    public QIdeaError(Path<? extends IdeaError> path) {
        super(path.getType(), path.getMetadata());
    }

    public QIdeaError(PathMetadata<?> metadata) {
        super(IdeaError.class, metadata);
    }

}

