package com.kiga.newsletter.backend.domain.zend;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QRecipient is a Querydsl query type for Recipient
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QRecipient extends EntityPathBase<Recipient> {

    private static final long serialVersionUID = 1417196368L;

    public static final QRecipient recipient = new QRecipient("recipient");

    public final com.kiga.db.QKigaLegacyDbEntityModel _super = new com.kiga.db.QKigaLegacyDbEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    public final StringPath countryCode = createString("countryCode");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath email = createString("email");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final BooleanPath isActiveSubscription = createBoolean("isActiveSubscription");

    public final BooleanPath isShop = createBoolean("isShop");

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final StringPath locale = createString("locale");

    public final StringPath parentCountryCode = createString("parentCountryCode");

    public final NumberPath<Integer> subscriptionDuration = createNumber("subscriptionDuration", Integer.class);

    public final StringPath subscriptionStatus = createString("subscriptionStatus");

    public final StringPath subscriptionType = createString("subscriptionType");

    public QRecipient(String variable) {
        super(Recipient.class, forVariable(variable));
    }

    public QRecipient(Path<? extends Recipient> path) {
        super(path.getType(), path.getMetadata());
    }

    public QRecipient(PathMetadata<?> metadata) {
        super(Recipient.class, metadata);
    }

}

