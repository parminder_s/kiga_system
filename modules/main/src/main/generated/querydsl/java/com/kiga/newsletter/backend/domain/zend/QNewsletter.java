package com.kiga.newsletter.backend.domain.zend;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QNewsletter is a Querydsl query type for Newsletter
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QNewsletter extends EntityPathBase<Newsletter> {

    private static final long serialVersionUID = -1861686686L;

    public static final QNewsletter newsletter = new QNewsletter("newsletter");

    public final StringPath body = createString("body");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath locale = createString("locale");

    public final StringPath name = createString("name");

    public final DateTimePath<java.util.Date> publishingDate = createDateTime("publishingDate", java.util.Date.class);

    public final ListPath<RecipientSelection, QRecipientSelection> selections = this.<RecipientSelection, QRecipientSelection>createList("selections", RecipientSelection.class, QRecipientSelection.class, PathInits.DIRECT2);

    public final StringPath subject = createString("subject");

    public final StringPath url = createString("url");

    public QNewsletter(String variable) {
        super(Newsletter.class, forVariable(variable));
    }

    public QNewsletter(Path<? extends Newsletter> path) {
        super(path.getType(), path.getMetadata());
    }

    public QNewsletter(PathMetadata<?> metadata) {
        super(Newsletter.class, metadata);
    }

}

