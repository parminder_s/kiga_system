package com.kiga.survey.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QQuestionnaire is a Querydsl query type for Questionnaire
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QQuestionnaire extends EntityPathBase<Questionnaire> {

    private static final long serialVersionUID = 846077060L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QQuestionnaire questionnaire = new QQuestionnaire("questionnaire");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final BooleanPath isCompleted = createBoolean("isCompleted");

    public final BooleanPath isNotifiedByEMail = createBoolean("isNotifiedByEMail");

    public final BooleanPath isNotifiedByMessage = createBoolean("isNotifiedByMessage");

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final com.kiga.security.domain.QMember member;

    public final MapPath<String, String, StringPath> questionnaireData = this.<String, String, StringPath>createMap("questionnaireData", String.class, String.class, StringPath.class);

    public final StringPath referenceCode = createString("referenceCode");

    public final StringPath referenceId = createString("referenceId");

    public final StringPath salt = createString("salt");

    public final QSurvey survey;

    public QQuestionnaire(String variable) {
        this(Questionnaire.class, forVariable(variable), INITS);
    }

    public QQuestionnaire(Path<? extends Questionnaire> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QQuestionnaire(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QQuestionnaire(PathMetadata<?> metadata, PathInits inits) {
        this(Questionnaire.class, metadata, inits);
    }

    public QQuestionnaire(Class<? extends Questionnaire> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.member = inits.isInitialized("member") ? new com.kiga.security.domain.QMember(forProperty("member"), inits.get("member")) : null;
        this.survey = inits.isInitialized("survey") ? new QSurvey(forProperty("survey")) : null;
    }

}

