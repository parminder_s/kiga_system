package com.kiga.ideas.member.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QMemberIdea is a Querydsl query type for MemberIdea
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QMemberIdea extends EntityPathBase<MemberIdea> {

    private static final long serialVersionUID = 516969282L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMemberIdea memberIdea = new QMemberIdea("memberIdea");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    public final StringPath content = createString("content");

    public final StringPath contentType = createString("contentType");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath description = createString("description");

    public final NumberPath<Long> fileId = createNumber("fileId", Long.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final com.kiga.content.domain.ss.live.QIdeaCategoryLive ideaCategory;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<Long> previewId = createNumber("previewId", Long.class);

    public final StringPath title = createString("title");

    public QMemberIdea(String variable) {
        this(MemberIdea.class, forVariable(variable), INITS);
    }

    public QMemberIdea(Path<? extends MemberIdea> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QMemberIdea(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QMemberIdea(PathMetadata<?> metadata, PathInits inits) {
        this(MemberIdea.class, metadata, inits);
    }

    public QMemberIdea(Class<? extends MemberIdea> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.ideaCategory = inits.isInitialized("ideaCategory") ? new com.kiga.content.domain.ss.live.QIdeaCategoryLive(forProperty("ideaCategory"), inits.get("ideaCategory")) : null;
    }

}

