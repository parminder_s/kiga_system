package com.kiga.accounting.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QAcquirerTransaction is a Querydsl query type for AcquirerTransaction
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAcquirerTransaction extends EntityPathBase<AcquirerTransaction> {

    private static final long serialVersionUID = 1186809192L;

    public static final QAcquirerTransaction acquirerTransaction = new QAcquirerTransaction("acquirerTransaction");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final StringPath acquirerName = createString("acquirerName");

    public final StringPath acquirerTransactionId = createString("acquirerTransactionId");

    public final NumberPath<java.math.BigDecimal> amount = createNumber("amount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> amountOrig = createNumber("amountOrig", java.math.BigDecimal.class);

    public final StringPath batchNr = createString("batchNr");

    public final NumberPath<java.math.BigDecimal> batchSum = createNumber("batchSum", java.math.BigDecimal.class);

    public final StringPath cardType = createString("cardType");

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath currency = createString("currency");

    public final StringPath currencyOrig = createString("currencyOrig");

    public final StringPath destinationBankBic = createString("destinationBankBic");

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<Integer> parseStatus = createNumber("parseStatus", Integer.class);

    public final DateTimePath<java.util.Date> processingDate = createDateTime("processingDate", java.util.Date.class);

    public final StringPath pspName = createString("pspName");

    public final StringPath pspTransactionId = createString("pspTransactionId");

    public final DateTimePath<java.util.Date> settlementDate = createDateTime("settlementDate", java.util.Date.class);

    public final StringPath sourceContent = createString("sourceContent");

    public final StringPath sourceFileName = createString("sourceFileName");

    public final DateTimePath<java.util.Date> transactionDate = createDateTime("transactionDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> transactionDateAcquirer = createDateTime("transactionDateAcquirer", java.util.Date.class);

    public QAcquirerTransaction(String variable) {
        super(AcquirerTransaction.class, forVariable(variable));
    }

    public QAcquirerTransaction(Path<? extends AcquirerTransaction> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAcquirerTransaction(PathMetadata<?> metadata) {
        super(AcquirerTransaction.class, metadata);
    }

}

