package com.kiga.content.domain.ss;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QSiteTreeTranslationGroupsModel is a Querydsl query type for SiteTreeTranslationGroupsModel
 */
@Generated("com.mysema.query.codegen.SupertypeSerializer")
public class QSiteTreeTranslationGroupsModel extends EntityPathBase<SiteTreeTranslationGroupsModel> {

    private static final long serialVersionUID = -373483381L;

    public static final QSiteTreeTranslationGroupsModel siteTreeTranslationGroupsModel = new QSiteTreeTranslationGroupsModel("siteTreeTranslationGroupsModel");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> originalId = createNumber("originalId", Long.class);

    public final NumberPath<Long> translationGroupId = createNumber("translationGroupId", Long.class);

    public QSiteTreeTranslationGroupsModel(String variable) {
        super(SiteTreeTranslationGroupsModel.class, forVariable(variable));
    }

    public QSiteTreeTranslationGroupsModel(Path<? extends SiteTreeTranslationGroupsModel> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSiteTreeTranslationGroupsModel(PathMetadata<?> metadata) {
        super(SiteTreeTranslationGroupsModel.class, metadata);
    }

}

