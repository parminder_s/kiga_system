package com.kiga.content.domain.ss.draft;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QIdeaGroupToIdeaDraft is a Querydsl query type for IdeaGroupToIdeaDraft
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QIdeaGroupToIdeaDraft extends EntityPathBase<IdeaGroupToIdeaDraft> {

    private static final long serialVersionUID = -1339431594L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QIdeaGroupToIdeaDraft ideaGroupToIdeaDraft = new QIdeaGroupToIdeaDraft("ideaGroupToIdeaDraft");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final QKigaIdeaDraft article;

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final QIdeaGroupDraft ideaGroup;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<Integer> sortOrder = createNumber("sortOrder", Integer.class);

    public final StringPath sortTree = createString("sortTree");

    public QIdeaGroupToIdeaDraft(String variable) {
        this(IdeaGroupToIdeaDraft.class, forVariable(variable), INITS);
    }

    public QIdeaGroupToIdeaDraft(Path<? extends IdeaGroupToIdeaDraft> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QIdeaGroupToIdeaDraft(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QIdeaGroupToIdeaDraft(PathMetadata<?> metadata, PathInits inits) {
        this(IdeaGroupToIdeaDraft.class, metadata, inits);
    }

    public QIdeaGroupToIdeaDraft(Class<? extends IdeaGroupToIdeaDraft> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.article = inits.isInitialized("article") ? new QKigaIdeaDraft(forProperty("article"), inits.get("article")) : null;
        this.ideaGroup = inits.isInitialized("ideaGroup") ? new QIdeaGroupDraft(forProperty("ideaGroup"), inits.get("ideaGroup")) : null;
    }

}

