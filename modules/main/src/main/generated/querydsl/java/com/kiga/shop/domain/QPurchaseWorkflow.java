package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.forVariable;

import com.mysema.query.types.Path;
import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.EntityPathBase;
import com.mysema.query.types.path.EnumPath;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathInits;
import com.mysema.query.types.path.StringPath;

import javax.annotation.Generated;

/** QPurchaseWorkflow is a Querydsl query type for PurchaseWorkflow */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPurchaseWorkflow extends EntityPathBase<PurchaseWorkflow> {

    public static final QPurchaseWorkflow purchaseWorkflow = new QPurchaseWorkflow("purchaseWorkflow");
    private static final long serialVersionUID = -1557678277L;
    private static final PathInits INITS = PathInits.DIRECT2;
    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final DateTimePath<java.time.Instant> changeDate = createDateTime("changeDate", java.time.Instant.class);

    // inherited
    public final StringPath className = _super.className;

    // inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    // inherited
    public final NumberPath<Long> id = _super.id;

    // inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final com.kiga.security.domain.QMember member;

    public final EnumPath<PurchaseStatus> prevStatus = createEnum("prevStatus", PurchaseStatus.class);

    public final QPurchase purchase;

    public final EnumPath<PurchaseStatus> status = createEnum("status", PurchaseStatus.class);

    public QPurchaseWorkflow(String variable) {
        this(PurchaseWorkflow.class, forVariable(variable), INITS);
    }

    public QPurchaseWorkflow(Path<? extends PurchaseWorkflow> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPurchaseWorkflow(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPurchaseWorkflow(PathMetadata<?> metadata, PathInits inits) {
        this(PurchaseWorkflow.class, metadata, inits);
    }

    public QPurchaseWorkflow(Class<? extends PurchaseWorkflow> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.member = inits.isInitialized("member")
                ? new com.kiga.security.domain.QMember(forProperty("member"), inits.get("member"))
                : null;
        this.purchase = inits.isInitialized("purchase") ? new QPurchase(forProperty("purchase"), inits.get("purchase"))
                : null;
    }
}
