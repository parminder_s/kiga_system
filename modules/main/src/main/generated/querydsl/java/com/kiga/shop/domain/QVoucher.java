package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QVoucher is a Querydsl query type for Voucher
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QVoucher extends EntityPathBase<Voucher> {

    private static final long serialVersionUID = 1289209075L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QVoucher voucher = new QVoucher("voucher");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final BooleanPath activated = createBoolean("activated");

    //inherited
    public final StringPath className = _super.className;

    public final StringPath code = createString("code");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<java.math.BigDecimal> discountRate = createNumber("discountRate", java.math.BigDecimal.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final QProduct product;

    public final ListPath<PurchaseItem, QPurchaseItem> purchaseItems = this.<PurchaseItem, QPurchaseItem>createList("purchaseItems", PurchaseItem.class, QPurchaseItem.class, PathInits.DIRECT2);

    public final DateTimePath<java.time.Instant> validFromDate = createDateTime("validFromDate", java.time.Instant.class);

    public final DateTimePath<java.time.Instant> validToDate = createDateTime("validToDate", java.time.Instant.class);

    public QVoucher(String variable) {
        this(Voucher.class, forVariable(variable), INITS);
    }

    public QVoucher(Path<? extends Voucher> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QVoucher(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QVoucher(PathMetadata<?> metadata, PathInits inits) {
        this(Voucher.class, metadata, inits);
    }

    public QVoucher(Class<? extends Voucher> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.product = inits.isInitialized("product") ? new QProduct(forProperty("product")) : null;
    }

}

