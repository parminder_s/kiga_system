package com.kiga.content.domain.ss.draft;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QKigaPrintPreviewImageDraft is a Querydsl query type for KigaPrintPreviewImageDraft
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QKigaPrintPreviewImageDraft extends EntityPathBase<KigaPrintPreviewImageDraft> {

    private static final long serialVersionUID = -1667160856L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QKigaPrintPreviewImageDraft kigaPrintPreviewImageDraft = new QKigaPrintPreviewImageDraft("kigaPrintPreviewImageDraft");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final QKigaIdeaDraft kigaIdea;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final NumberPath<Integer> pageNumber = createNumber("pageNumber", Integer.class);

    public final com.kiga.s3.domain.QS3Image s3Image;

    public QKigaPrintPreviewImageDraft(String variable) {
        this(KigaPrintPreviewImageDraft.class, forVariable(variable), INITS);
    }

    public QKigaPrintPreviewImageDraft(Path<? extends KigaPrintPreviewImageDraft> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QKigaPrintPreviewImageDraft(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QKigaPrintPreviewImageDraft(PathMetadata<?> metadata, PathInits inits) {
        this(KigaPrintPreviewImageDraft.class, metadata, inits);
    }

    public QKigaPrintPreviewImageDraft(Class<? extends KigaPrintPreviewImageDraft> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.kigaIdea = inits.isInitialized("kigaIdea") ? new QKigaIdeaDraft(forProperty("kigaIdea"), inits.get("kigaIdea")) : null;
        this.s3Image = inits.isInitialized("s3Image") ? new com.kiga.s3.domain.QS3Image(forProperty("s3Image")) : null;
    }

}

