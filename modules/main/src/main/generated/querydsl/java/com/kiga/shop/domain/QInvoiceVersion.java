package com.kiga.shop.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QInvoiceVersion is a Querydsl query type for InvoiceVersion
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QInvoiceVersion extends EntityPathBase<InvoiceVersion> {

    private static final long serialVersionUID = 2105808646L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QInvoiceVersion invoiceVersion1 = new QInvoiceVersion("invoiceVersion1");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final StringPath invoiceS3Bucket = createString("invoiceS3Bucket");

    public final StringPath invoiceS3Url = createString("invoiceS3Url");

    public final NumberPath<Long> invoiceVersion = createNumber("invoiceVersion", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final QPurchase purchase;

    public QInvoiceVersion(String variable) {
        this(InvoiceVersion.class, forVariable(variable), INITS);
    }

    public QInvoiceVersion(Path<? extends InvoiceVersion> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QInvoiceVersion(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QInvoiceVersion(PathMetadata<?> metadata, PathInits inits) {
        this(InvoiceVersion.class, metadata, inits);
    }

    public QInvoiceVersion(Class<? extends InvoiceVersion> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.purchase = inits.isInitialized("purchase") ? new QPurchase(forProperty("purchase"), inits.get("purchase")) : null;
    }

}

