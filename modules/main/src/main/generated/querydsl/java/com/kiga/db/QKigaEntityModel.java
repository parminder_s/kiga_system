package com.kiga.db;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QKigaEntityModel is a Querydsl query type for KigaEntityModel
 */
@Generated("com.mysema.query.codegen.SupertypeSerializer")
public class QKigaEntityModel extends EntityPathBase<KigaEntityModel> {

    private static final long serialVersionUID = 1003172647L;

    public static final QKigaEntityModel kigaEntityModel = new QKigaEntityModel("kigaEntityModel");

    public final QKigaEntityModelWithoutId _super = new QKigaEntityModelWithoutId(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public QKigaEntityModel(String variable) {
        super(KigaEntityModel.class, forVariable(variable));
    }

    public QKigaEntityModel(Path<? extends KigaEntityModel> path) {
        super(path.getType(), path.getMetadata());
    }

    public QKigaEntityModel(PathMetadata<?> metadata) {
        super(KigaEntityModel.class, metadata);
    }

}

