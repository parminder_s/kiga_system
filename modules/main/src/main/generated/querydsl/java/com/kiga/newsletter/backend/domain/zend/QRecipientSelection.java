package com.kiga.newsletter.backend.domain.zend;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QRecipientSelection is a Querydsl query type for RecipientSelection
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QRecipientSelection extends EntityPathBase<RecipientSelection> {

    private static final long serialVersionUID = -188835812L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRecipientSelection recipientSelection = new QRecipientSelection("recipientSelection");

    public final com.kiga.db.QKigaLegacyDbEntityModel _super = new com.kiga.db.QKigaLegacyDbEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    public final StringPath country = createString("country");

    public final StringPath countryGroup = createString("countryGroup");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final StringPath locale = createString("locale");

    public final QNewsletter newsletter;

    public final StringPath parentCountry = createString("parentCountry");

    public final BooleanPath shopFlag = createBoolean("shopFlag");

    public final NumberPath<Integer> subscriptionDuration = createNumber("subscriptionDuration", Integer.class);

    public final BooleanPath subscriptionFlag = createBoolean("subscriptionFlag");

    public final StringPath subscriptionStatus = createString("subscriptionStatus");

    public final EnumPath<com.kiga.newsletter.backend.domain.SubscriptionTypeGroup> subscriptionType = createEnum("subscriptionType", com.kiga.newsletter.backend.domain.SubscriptionTypeGroup.class);

    public QRecipientSelection(String variable) {
        this(RecipientSelection.class, forVariable(variable), INITS);
    }

    public QRecipientSelection(Path<? extends RecipientSelection> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QRecipientSelection(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QRecipientSelection(PathMetadata<?> metadata, PathInits inits) {
        this(RecipientSelection.class, metadata, inits);
    }

    public QRecipientSelection(Class<? extends RecipientSelection> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.newsletter = inits.isInitialized("newsletter") ? new QNewsletter(forProperty("newsletter")) : null;
    }

}

