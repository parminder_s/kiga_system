package com.kiga.accounting.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QInvoice is a Querydsl query type for Invoice
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QInvoice extends EntityPathBase<Invoice> {

    private static final long serialVersionUID = 410235571L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QInvoice invoice = new QInvoice("invoice");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final NumberPath<java.math.BigDecimal> amount = createNumber("amount", java.math.BigDecimal.class);

    //inherited
    public final StringPath className = _super.className;

    public final StringPath countryCode = createString("countryCode");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath currency = createString("currency");

    public final StringPath customerEmail = createString("customerEmail");

    public final StringPath customerName = createString("customerName");

    public final DateTimePath<java.util.Date> dueDate = createDateTime("dueDate", java.util.Date.class);

    public final BooleanPath dunningEmailSent = createBoolean("dunningEmailSent");

    public final com.kiga.accounting.dunning.domain.QDunningLevel dunningLevel;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final DateTimePath<java.util.Date> invoiceDate = createDateTime("invoiceDate", java.util.Date.class);

    public final StringPath invoiceNumber = createString("invoiceNumber");

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final DateTimePath<java.util.Date> orderDate = createDateTime("orderDate", java.util.Date.class);

    public final StringPath orderNumber = createString("orderNumber");

    public final ListPath<com.kiga.accounting.transactions.domain.PaymentTransaction, com.kiga.accounting.transactions.domain.QPaymentTransaction> paymentTransactions = this.<com.kiga.accounting.transactions.domain.PaymentTransaction, com.kiga.accounting.transactions.domain.QPaymentTransaction>createList("paymentTransactions", com.kiga.accounting.transactions.domain.PaymentTransaction.class, com.kiga.accounting.transactions.domain.QPaymentTransaction.class, PathInits.DIRECT2);

    public final BooleanPath settled = createBoolean("settled");

    public QInvoice(String variable) {
        this(Invoice.class, forVariable(variable), INITS);
    }

    public QInvoice(Path<? extends Invoice> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QInvoice(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QInvoice(PathMetadata<?> metadata, PathInits inits) {
        this(Invoice.class, metadata, inits);
    }

    public QInvoice(Class<? extends Invoice> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.dunningLevel = inits.isInitialized("dunningLevel") ? new com.kiga.accounting.dunning.domain.QDunningLevel(forProperty("dunningLevel"), inits.get("dunningLevel")) : null;
    }

}

