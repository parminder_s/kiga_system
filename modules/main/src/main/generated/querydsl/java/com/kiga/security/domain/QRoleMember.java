package com.kiga.security.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QRoleMember is a Querydsl query type for RoleMember
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QRoleMember extends EntityPathBase<RoleMember> {

    private static final long serialVersionUID = 987815829L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRoleMember roleMember = new QRoleMember("roleMember");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final QMember member;

    public final QRole role;

    public QRoleMember(String variable) {
        this(RoleMember.class, forVariable(variable), INITS);
    }

    public QRoleMember(Path<? extends RoleMember> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QRoleMember(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QRoleMember(PathMetadata<?> metadata, PathInits inits) {
        this(RoleMember.class, metadata, inits);
    }

    public QRoleMember(Class<? extends RoleMember> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.member = inits.isInitialized("member") ? new QMember(forProperty("member"), inits.get("member")) : null;
        this.role = inits.isInitialized("role") ? new QRole(forProperty("role")) : null;
    }

}

