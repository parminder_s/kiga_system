package com.kiga.shopcontent.domain.draft;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QSliderFileDraft is a Querydsl query type for SliderFileDraft
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSliderFileDraft extends EntityPathBase<SliderFileDraft> {

    private static final long serialVersionUID = -2024599647L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSliderFileDraft sliderFileDraft = new QSliderFileDraft("sliderFileDraft");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath fileType = createString("fileType");

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final com.kiga.s3.domain.QKigaS3File kigaS3File;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final com.kiga.s3.domain.QS3Image previewImage;

    public final QProductPageDraft productPage;

    public final NumberPath<Integer> sort = createNumber("sort", Integer.class);

    public QSliderFileDraft(String variable) {
        this(SliderFileDraft.class, forVariable(variable), INITS);
    }

    public QSliderFileDraft(Path<? extends SliderFileDraft> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSliderFileDraft(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSliderFileDraft(PathMetadata<?> metadata, PathInits inits) {
        this(SliderFileDraft.class, metadata, inits);
    }

    public QSliderFileDraft(Class<? extends SliderFileDraft> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.kigaS3File = inits.isInitialized("kigaS3File") ? new com.kiga.s3.domain.QKigaS3File(forProperty("kigaS3File")) : null;
        this.previewImage = inits.isInitialized("previewImage") ? new com.kiga.s3.domain.QS3Image(forProperty("previewImage")) : null;
        this.productPage = inits.isInitialized("productPage") ? new QProductPageDraft(forProperty("productPage"), inits.get("productPage")) : null;
    }

}

