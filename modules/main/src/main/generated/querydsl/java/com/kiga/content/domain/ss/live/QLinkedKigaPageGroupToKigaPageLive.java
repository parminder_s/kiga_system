package com.kiga.content.domain.ss.live;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QLinkedKigaPageGroupToKigaPageLive is a Querydsl query type for LinkedKigaPageGroupToKigaPageLive
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QLinkedKigaPageGroupToKigaPageLive extends EntityPathBase<LinkedKigaPageGroupToKigaPageLive> {

    private static final long serialVersionUID = 1418673229L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QLinkedKigaPageGroupToKigaPageLive linkedKigaPageGroupToKigaPageLive = new QLinkedKigaPageGroupToKigaPageLive("linkedKigaPageGroupToKigaPageLive");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final QKigaPageLive kigaPage;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final QLinkedKigaPageGroupLive linkedKigaPageGroup;

    public QLinkedKigaPageGroupToKigaPageLive(String variable) {
        this(LinkedKigaPageGroupToKigaPageLive.class, forVariable(variable), INITS);
    }

    public QLinkedKigaPageGroupToKigaPageLive(Path<? extends LinkedKigaPageGroupToKigaPageLive> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QLinkedKigaPageGroupToKigaPageLive(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QLinkedKigaPageGroupToKigaPageLive(PathMetadata<?> metadata, PathInits inits) {
        this(LinkedKigaPageGroupToKigaPageLive.class, metadata, inits);
    }

    public QLinkedKigaPageGroupToKigaPageLive(Class<? extends LinkedKigaPageGroupToKigaPageLive> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.kigaPage = inits.isInitialized("kigaPage") ? new QKigaPageLive(forProperty("kigaPage"), inits.get("kigaPage")) : null;
        this.linkedKigaPageGroup = inits.isInitialized("linkedKigaPageGroup") ? new QLinkedKigaPageGroupLive(forProperty("linkedKigaPageGroup"), inits.get("linkedKigaPageGroup")) : null;
    }

}

