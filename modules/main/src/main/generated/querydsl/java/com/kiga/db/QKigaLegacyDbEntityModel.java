package com.kiga.db;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QKigaLegacyDbEntityModel is a Querydsl query type for KigaLegacyDbEntityModel
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QKigaLegacyDbEntityModel extends BeanPath<KigaLegacyDbEntityModel> {

    private static final long serialVersionUID = -467964544L;

    public static final QKigaLegacyDbEntityModel kigaLegacyDbEntityModel = new QKigaLegacyDbEntityModel("kigaLegacyDbEntityModel");

    public final StringPath className = createString("className");

    public final DateTimePath<java.util.Date> created = createDateTime("created", java.util.Date.class);

    public final DateTimePath<java.util.Date> lastEdited = createDateTime("lastEdited", java.util.Date.class);

    public QKigaLegacyDbEntityModel(String variable) {
        super(KigaLegacyDbEntityModel.class, forVariable(variable));
    }

    public QKigaLegacyDbEntityModel(Path<? extends KigaLegacyDbEntityModel> path) {
        super(path.getType(), path.getMetadata());
    }

    public QKigaLegacyDbEntityModel(PathMetadata<?> metadata) {
        super(KigaLegacyDbEntityModel.class, metadata);
    }

}

