package com.kiga.logging.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QSearchTerm is a Querydsl query type for SearchTerm
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSearchTerm extends EntityPathBase<SearchTerm> {

    private static final long serialVersionUID = 999259974L;

    public static final QSearchTerm searchTerm1 = new QSearchTerm("searchTerm1");

    public final QLogModel _super = new QLogModel(this);

    public final NumberPath<Long> ageGroup = createNumber("ageGroup", Long.class);

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final StringPath countryCode = _super.countryCode;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    //inherited
    public final SimplePath<com.kiga.main.locale.Locale> locale = _super.locale;

    public final NumberPath<Long> parentId = createNumber("parentId", Long.class);

    //inherited
    public final StringPath remoteIp = _super.remoteIp;

    public final NumberPath<Long> resultCount = createNumber("resultCount", Long.class);

    public final StringPath searchTerm = createString("searchTerm");

    //inherited
    public final StringPath sessionId = _super.sessionId;

    public QSearchTerm(String variable) {
        super(SearchTerm.class, forVariable(variable));
    }

    public QSearchTerm(Path<? extends SearchTerm> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSearchTerm(PathMetadata<?> metadata) {
        super(SearchTerm.class, metadata);
    }

}

