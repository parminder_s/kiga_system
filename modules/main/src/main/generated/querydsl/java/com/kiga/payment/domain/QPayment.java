package com.kiga.payment.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPayment is a Querydsl query type for Payment
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPayment extends EntityPathBase<Payment> {

    private static final long serialVersionUID = 1376631981L;

    public static final QPayment payment = new QPayment("payment");

    public final com.kiga.db.QKigaEntityModel _super = new com.kiga.db.QKigaEntityModel(this);

    public final StringPath acquirer = createString("acquirer");

    public final StringPath cid = createString("cid");

    //inherited
    public final StringPath className = _super.className;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath crmTransactionId = createString("crmTransactionId");

    public final StringPath currency = createString("currency");

    public final StringPath failureUrl = createString("failureUrl");

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final StringPath initRequestData = createString("initRequestData");

    public final StringPath initResponseData = createString("initResponseData");

    public final BooleanPath isAutomatic = createBoolean("isAutomatic");

    public final BooleanPath isCrmCommitted = createBoolean("isCrmCommitted");

    public final BooleanPath isPostCommitted = createBoolean("isPostCommitted");

    //inherited
    public final DateTimePath<java.util.Date> lastEdited = _super.lastEdited;

    public final SimplePath<com.kiga.main.locale.Locale> locale = createSimple("locale", com.kiga.main.locale.Locale.class);

    public final StringPath paymentMethod = createString("paymentMethod");

    public final NumberPath<java.math.BigDecimal> price = createNumber("price", java.math.BigDecimal.class);

    public final StringPath psp = createString("psp");

    public final StringPath pspReturnData = createString("pspReturnData");

    public final StringPath referenceCode = createString("referenceCode");

    public final StringPath referenceId = createString("referenceId");

    public final StringPath remoteTransactionId = createString("remoteTransactionId");

    public final StringPath sessionId = createString("sessionId");

    public final StringPath status = createString("status");

    public final StringPath successUrl = createString("successUrl");

    public final StringPath transactionId = createString("transactionId");

    public QPayment(String variable) {
        super(Payment.class, forVariable(variable));
    }

    public QPayment(Path<? extends Payment> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPayment(PathMetadata<?> metadata) {
        super(Payment.class, metadata);
    }

}

