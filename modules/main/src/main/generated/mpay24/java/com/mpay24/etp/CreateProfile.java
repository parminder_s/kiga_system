
package com.mpay24.etp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateProfile complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateProfile">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="merchantID" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *         &lt;element name="mdxi" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateProfile", propOrder = {
    "merchantID",
    "mdxi"
})
public class CreateProfile {

    @XmlSchemaType(name = "unsignedInt")
    protected long merchantID;
    @XmlElement(required = true)
    protected String mdxi;

    /**
     * Gets the value of the merchantID property.
     * 
     */
    public long getMerchantID() {
        return merchantID;
    }

    /**
     * Sets the value of the merchantID property.
     * 
     */
    public void setMerchantID(long value) {
        this.merchantID = value;
    }

    /**
     * Gets the value of the mdxi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdxi() {
        return mdxi;
    }

    /**
     * Sets the value of the mdxi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdxi(String value) {
        this.mdxi = value;
    }

}
