
package com.mpay24.etp;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * CreditCard specific profile payment parameters
 * 
 * <p>Java class for ProfilePaymentCC complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfilePaymentCC">
 *   &lt;complexContent>
 *     &lt;extension base="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}Payment">
 *       &lt;sequence>
 *         &lt;element name="cvc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="auth3DS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfilePaymentCC", propOrder = {
    "cvc",
    "auth3DS"
})
public class ProfilePaymentCC
    extends Payment
{

    @XmlElementRef(name = "cvc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cvc;
    @XmlElement(defaultValue = "false")
    protected Boolean auth3DS;

    /**
     * Gets the value of the cvc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCvc() {
        return cvc;
    }

    /**
     * Sets the value of the cvc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCvc(JAXBElement<String> value) {
        this.cvc = value;
    }

    /**
     * Gets the value of the auth3DS property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAuth3DS() {
        return auth3DS;
    }

    /**
     * Sets the value of the auth3DS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAuth3DS(Boolean value) {
        this.auth3DS = value;
    }

}
