
package com.mpay24.etp;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SortField.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SortField">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MPAYTID"/>
 *     &lt;enumeration value="PTYPE"/>
 *     &lt;enumeration value="BRAND"/>
 *     &lt;enumeration value="CURRENCY"/>
 *     &lt;enumeration value="TID"/>
 *     &lt;enumeration value="ORDERDESC"/>
 *     &lt;enumeration value="ORDERNUMBER"/>
 *     &lt;enumeration value="CURRDATE"/>
 *     &lt;enumeration value="CURRTIME"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SortField")
@XmlEnum
public enum SortField {

    MPAYTID,
    PTYPE,
    BRAND,
    CURRENCY,
    TID,
    ORDERDESC,
    ORDERNUMBER,
    CURRDATE,
    CURRTIME;

    public String value() {
        return name();
    }

    public static SortField fromValue(String v) {
        return valueOf(v);
    }

}
