
package com.mpay24.etp;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NOTFOUND"/>
 *     &lt;enumeration value="FAILED"/>
 *     &lt;enumeration value="RESERVED"/>
 *     &lt;enumeration value="BILLED"/>
 *     &lt;enumeration value="REVERSED"/>
 *     &lt;enumeration value="CREDITED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TStatus")
@XmlEnum
public enum TStatus {

    NOTFOUND,
    FAILED,
    RESERVED,
    BILLED,
    REVERSED,
    CREDITED;

    public String value() {
        return name();
    }

    public static TStatus fromValue(String v) {
        return valueOf(v);
    }

}
