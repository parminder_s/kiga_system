
package com.mpay24.etp;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PaymentType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CC"/>
 *     &lt;enumeration value="CB"/>
 *     &lt;enumeration value="MAESTRO"/>
 *     &lt;enumeration value="EPS"/>
 *     &lt;enumeration value="PB"/>
 *     &lt;enumeration value="PSC"/>
 *     &lt;enumeration value="CASH-TICKET"/>
 *     &lt;enumeration value="ELV"/>
 *     &lt;enumeration value="QUICK"/>
 *     &lt;enumeration value="GIROPAY"/>
 *     &lt;enumeration value="PAYPAL"/>
 *     &lt;enumeration value="MPASS"/>
 *     &lt;enumeration value="BILLPAY"/>
 *     &lt;enumeration value="INVOICE"/>
 *     &lt;enumeration value="HP"/>
 *     &lt;enumeration value="SAFETYPAY"/>
 *     &lt;enumeration value="KLARNA"/>
 *     &lt;enumeration value="SOFORT"/>
 *     &lt;enumeration value="TOKEN"/>
 *     &lt;enumeration value="PROFILE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PaymentType")
@XmlEnum
public enum PaymentType {

    CC("CC"),
    CB("CB"),
    MAESTRO("MAESTRO"),
    EPS("EPS"),
    PB("PB"),
    PSC("PSC"),
    @XmlEnumValue("CASH-TICKET")
    CASH_TICKET("CASH-TICKET"),
    ELV("ELV"),
    QUICK("QUICK"),
    GIROPAY("GIROPAY"),
    PAYPAL("PAYPAL"),
    MPASS("MPASS"),
    BILLPAY("BILLPAY"),
    INVOICE("INVOICE"),
    HP("HP"),
    SAFETYPAY("SAFETYPAY"),
    KLARNA("KLARNA"),
    SOFORT("SOFORT"),
    TOKEN("TOKEN"),
    PROFILE("PROFILE");
    private final String value;

    PaymentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PaymentType fromValue(String v) {
        for (PaymentType c: PaymentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
