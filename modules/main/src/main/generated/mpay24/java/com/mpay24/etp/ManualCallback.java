
package com.mpay24.etp;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ManualCallback complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ManualCallback">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="merchantID" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *         &lt;element name="mpayTID" type="{http://www.w3.org/2001/XMLSchema}unsignedLong"/>
 *         &lt;element name="paymentCallback" type="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}Callback"/>
 *         &lt;element name="order" type="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}Order" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ManualCallback", propOrder = {
    "merchantID",
    "mpayTID",
    "paymentCallback",
    "order"
})
public class ManualCallback {

    @XmlSchemaType(name = "unsignedInt")
    protected long merchantID;
    @XmlElement(required = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger mpayTID;
    @XmlElement(required = true, nillable = true)
    protected Callback paymentCallback;
    @XmlElementRef(name = "order", type = JAXBElement.class, required = false)
    protected JAXBElement<Order> order;

    /**
     * Gets the value of the merchantID property.
     * 
     */
    public long getMerchantID() {
        return merchantID;
    }

    /**
     * Sets the value of the merchantID property.
     * 
     */
    public void setMerchantID(long value) {
        this.merchantID = value;
    }

    /**
     * Gets the value of the mpayTID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMpayTID() {
        return mpayTID;
    }

    /**
     * Sets the value of the mpayTID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMpayTID(BigInteger value) {
        this.mpayTID = value;
    }

    /**
     * Gets the value of the paymentCallback property.
     * 
     * @return
     *     possible object is
     *     {@link Callback }
     *     
     */
    public Callback getPaymentCallback() {
        return paymentCallback;
    }

    /**
     * Sets the value of the paymentCallback property.
     * 
     * @param value
     *     allowed object is
     *     {@link Callback }
     *     
     */
    public void setPaymentCallback(Callback value) {
        this.paymentCallback = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Order }{@code >}
     *     
     */
    public JAXBElement<Order> getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Order }{@code >}
     *     
     */
    public void setOrder(JAXBElement<Order> value) {
        this.order = value;
    }

}
