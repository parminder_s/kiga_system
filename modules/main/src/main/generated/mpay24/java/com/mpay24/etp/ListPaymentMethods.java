
package com.mpay24.etp;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ListPaymentMethods complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ListPaymentMethods">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="merchantID" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *         &lt;element name="pType" type="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}PaymentType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListPaymentMethods", propOrder = {
    "merchantID",
    "pType"
})
public class ListPaymentMethods {

    @XmlSchemaType(name = "unsignedInt")
    protected long merchantID;
    @XmlElementRef(name = "pType", type = JAXBElement.class, required = false)
    protected JAXBElement<PaymentType> pType;

    /**
     * Gets the value of the merchantID property.
     * 
     */
    public long getMerchantID() {
        return merchantID;
    }

    /**
     * Sets the value of the merchantID property.
     * 
     */
    public void setMerchantID(long value) {
        this.merchantID = value;
    }

    /**
     * Gets the value of the pType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PaymentType }{@code >}
     *     
     */
    public JAXBElement<PaymentType> getPType() {
        return pType;
    }

    /**
     * Sets the value of the pType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PaymentType }{@code >}
     *     
     */
    public void setPType(JAXBElement<PaymentType> value) {
        this.pType = value;
    }

}
