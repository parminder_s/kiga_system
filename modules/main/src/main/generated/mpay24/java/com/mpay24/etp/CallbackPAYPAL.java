
package com.mpay24.etp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * PayPal specific payment callback parameters
 * 
 * <p>Java class for CallbackPAYPAL complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CallbackPAYPAL">
 *   &lt;complexContent>
 *     &lt;extension base="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}Callback">
 *       &lt;sequence>
 *         &lt;element name="cancel" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallbackPAYPAL", propOrder = {
    "cancel"
})
public class CallbackPAYPAL
    extends Callback
{

    @XmlElement(defaultValue = "false")
    protected boolean cancel;

    /**
     * Gets the value of the cancel property.
     * 
     */
    public boolean isCancel() {
        return cancel;
    }

    /**
     * Sets the value of the cancel property.
     * 
     */
    public void setCancel(boolean value) {
        this.cancel = value;
    }

}
