
package com.mpay24.etp;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Carte Bleue specific payment parameters
 * 
 * <p>Java class for PaymentCB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentCB">
 *   &lt;complexContent>
 *     &lt;extension base="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}Payment">
 *       &lt;sequence>
 *         &lt;element name="identifier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="expiry" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *         &lt;element name="cvc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentCB", propOrder = {
    "identifier",
    "expiry",
    "cvc"
})
public class PaymentCB
    extends Payment
{

    @XmlElement(required = true)
    protected String identifier;
    @XmlSchemaType(name = "unsignedInt")
    protected long expiry;
    @XmlElementRef(name = "cvc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cvc;

    /**
     * Gets the value of the identifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Sets the value of the identifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentifier(String value) {
        this.identifier = value;
    }

    /**
     * Gets the value of the expiry property.
     * 
     */
    public long getExpiry() {
        return expiry;
    }

    /**
     * Sets the value of the expiry property.
     * 
     */
    public void setExpiry(long value) {
        this.expiry = value;
    }

    /**
     * Gets the value of the cvc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCvc() {
        return cvc;
    }

    /**
     * Sets the value of the cvc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCvc(JAXBElement<String> value) {
        this.cvc = value;
    }

}
