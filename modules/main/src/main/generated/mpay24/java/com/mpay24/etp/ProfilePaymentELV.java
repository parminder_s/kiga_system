
package com.mpay24.etp;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Direct debit specific profile payment parameters
 * 
 * <p>Java class for ProfilePaymentELV complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfilePaymentELV">
 *   &lt;complexContent>
 *     &lt;extension base="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}Payment">
 *       &lt;sequence>
 *         &lt;element name="mandateID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateOfSignature" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfilePaymentELV", propOrder = {
    "mandateID",
    "dateOfSignature"
})
public class ProfilePaymentELV
    extends Payment
{

    @XmlElementRef(name = "mandateID", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mandateID;
    @XmlElementRef(name = "dateOfSignature", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dateOfSignature;

    /**
     * Gets the value of the mandateID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMandateID() {
        return mandateID;
    }

    /**
     * Sets the value of the mandateID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMandateID(JAXBElement<String> value) {
        this.mandateID = value;
    }

    /**
     * Gets the value of the dateOfSignature property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDateOfSignature() {
        return dateOfSignature;
    }

    /**
     * Sets the value of the dateOfSignature property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDateOfSignature(JAXBElement<XMLGregorianCalendar> value) {
        this.dateOfSignature = value;
    }

}
