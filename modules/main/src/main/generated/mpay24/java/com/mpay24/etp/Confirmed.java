
package com.mpay24.etp;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Confirmed.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Confirmed">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="RESERVED"/>
 *     &lt;enumeration value="BILLED"/>
 *     &lt;enumeration value="REVERSED"/>
 *     &lt;enumeration value="CREDITED"/>
 *     &lt;enumeration value="SUSPENDED"/>
 *     &lt;enumeration value="ERROR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Confirmed")
@XmlEnum
public enum Confirmed {

    RESERVED,
    BILLED,
    REVERSED,
    CREDITED,
    SUSPENDED,
    ERROR;

    public String value() {
        return name();
    }

    public static Confirmed fromValue(String v) {
        return valueOf(v);
    }

}
