
package com.mpay24.etp;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ListNotCleared complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ListNotCleared">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="merchantID" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *         &lt;element name="begin" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="size" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="sortField" type="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}SortField" minOccurs="0"/>
 *         &lt;element name="sortType" type="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}SortType" minOccurs="0"/>
 *         &lt;element name="listInProgress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListNotCleared", propOrder = {
    "merchantID",
    "begin",
    "size",
    "sortField",
    "sortType",
    "listInProgress"
})
public class ListNotCleared {

    @XmlSchemaType(name = "unsignedInt")
    protected long merchantID;
    @XmlElementRef(name = "begin", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> begin;
    @XmlElementRef(name = "size", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> size;
    @XmlElementRef(name = "sortField", type = JAXBElement.class, required = false)
    protected JAXBElement<SortField> sortField;
    @XmlElementRef(name = "sortType", type = JAXBElement.class, required = false)
    protected JAXBElement<SortType> sortType;
    @XmlElementRef(name = "listInProgress", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> listInProgress;

    /**
     * Gets the value of the merchantID property.
     * 
     */
    public long getMerchantID() {
        return merchantID;
    }

    /**
     * Sets the value of the merchantID property.
     * 
     */
    public void setMerchantID(long value) {
        this.merchantID = value;
    }

    /**
     * Gets the value of the begin property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getBegin() {
        return begin;
    }

    /**
     * Sets the value of the begin property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setBegin(JAXBElement<Long> value) {
        this.begin = value;
    }

    /**
     * Gets the value of the size property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setSize(JAXBElement<Long> value) {
        this.size = value;
    }

    /**
     * Gets the value of the sortField property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SortField }{@code >}
     *     
     */
    public JAXBElement<SortField> getSortField() {
        return sortField;
    }

    /**
     * Sets the value of the sortField property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SortField }{@code >}
     *     
     */
    public void setSortField(JAXBElement<SortField> value) {
        this.sortField = value;
    }

    /**
     * Gets the value of the sortType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SortType }{@code >}
     *     
     */
    public JAXBElement<SortType> getSortType() {
        return sortType;
    }

    /**
     * Sets the value of the sortType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SortType }{@code >}
     *     
     */
    public void setSortType(JAXBElement<SortType> value) {
        this.sortType = value;
    }

    /**
     * Gets the value of the listInProgress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getListInProgress() {
        return listInProgress;
    }

    /**
     * Sets the value of the listInProgress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setListInProgress(JAXBElement<Boolean> value) {
        this.listInProgress = value;
    }

}
