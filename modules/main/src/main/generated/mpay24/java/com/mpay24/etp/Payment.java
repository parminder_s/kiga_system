
package com.mpay24.etp;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Common payment parameters
 * 
 * <p>Java class for Payment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Payment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="manualClearing" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="useProfile" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="profileID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Payment", propOrder = {
    "amount",
    "currency",
    "manualClearing",
    "useProfile",
    "profileID"
})
@XmlSeeAlso({
    PaymentCB.class,
    PaymentBILLPAY.class,
    PaymentCC.class,
    PaymentPB.class,
    PaymentEPS.class,
    PaymentKLARNA.class,
    ProfilePaymentCC.class,
    PaymentELV.class,
    PaymentGIROPAY.class,
    ProfilePaymentPB.class,
    PaymentPAYPAL.class,
    PaymentTOKEN.class,
    PaymentMAESTRO.class,
    ProfilePaymentELV.class
})
public class Payment {

    @XmlSchemaType(name = "unsignedInt")
    protected long amount;
    @XmlElement(required = true)
    protected String currency;
    @XmlElementRef(name = "manualClearing", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> manualClearing;
    @XmlElementRef(name = "useProfile", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> useProfile;
    @XmlElementRef(name = "profileID", type = JAXBElement.class, required = false)
    protected JAXBElement<String> profileID;

    /**
     * Gets the value of the amount property.
     * 
     */
    public long getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAmount(long value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the manualClearing property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getManualClearing() {
        return manualClearing;
    }

    /**
     * Sets the value of the manualClearing property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setManualClearing(JAXBElement<Boolean> value) {
        this.manualClearing = value;
    }

    /**
     * Gets the value of the useProfile property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getUseProfile() {
        return useProfile;
    }

    /**
     * Sets the value of the useProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setUseProfile(JAXBElement<Boolean> value) {
        this.useProfile = value;
    }

    /**
     * Gets the value of the profileID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProfileID() {
        return profileID;
    }

    /**
     * Sets the value of the profileID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProfileID(JAXBElement<String> value) {
        this.profileID = value;
    }

}
