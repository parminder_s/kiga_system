
package com.mpay24.etp;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mpay24.etp package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ManualCallbackResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ManualCallbackResponse");
    private final static QName _ManualCreditResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ManualCreditResponse");
    private final static QName _TransactionStatus_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "TransactionStatus");
    private final static QName _DeleteProfileResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "DeleteProfileResponse");
    private final static QName _AcceptPayment_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "AcceptPayment");
    private final static QName _AcceptPaymentResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "AcceptPaymentResponse");
    private final static QName _CreatePaymentToken_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "CreatePaymentToken");
    private final static QName _SelectPayment_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "SelectPayment");
    private final static QName _ListProfilesResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ListProfilesResponse");
    private final static QName _ManualCredit_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ManualCredit");
    private final static QName _TransactionConfirmationResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "TransactionConfirmationResponse");
    private final static QName _ListNotCleared_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ListNotCleared");
    private final static QName _SelectPaymentResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "SelectPaymentResponse");
    private final static QName _ManualClear_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ManualClear");
    private final static QName _ManualReverse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ManualReverse");
    private final static QName _TransactionConfirmation_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "TransactionConfirmation");
    private final static QName _ManualClearResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ManualClearResponse");
    private final static QName _DeleteProfile_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "DeleteProfile");
    private final static QName _ListPaymentMethodsResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ListPaymentMethodsResponse");
    private final static QName _CreateProfileResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "CreateProfileResponse");
    private final static QName _ListProfiles_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ListProfiles");
    private final static QName _TransactionStatusResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "TransactionStatusResponse");
    private final static QName _ManualCallback_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ManualCallback");
    private final static QName _ListPaymentMethods_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ListPaymentMethods");
    private final static QName _ManualReverseResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ManualReverseResponse");
    private final static QName _CreatePaymentTokenResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "CreatePaymentTokenResponse");
    private final static QName _CreateProfile_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "CreateProfile");
    private final static QName _ListNotClearedResponse_QNAME = new QName("https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", "ListNotClearedResponse");
    private final static QName _PaymentCCCvc_QNAME = new QName("", "cvc");
    private final static QName _CallbackAmount_QNAME = new QName("", "amount");
    private final static QName _ListPaymentMethodsPType_QNAME = new QName("", "pType");
    private final static QName _PaymentPAYPALCustom_QNAME = new QName("", "custom");
    private final static QName _OrderIndustrySpecific_QNAME = new QName("", "industrySpecific");
    private final static QName _OrderShipping_QNAME = new QName("", "shipping");
    private final static QName _OrderClientIP_QNAME = new QName("", "clientIP");
    private final static QName _OrderUserField_QNAME = new QName("", "userField");
    private final static QName _OrderShoppingCart_QNAME = new QName("", "shoppingCart");
    private final static QName _OrderDescription_QNAME = new QName("", "description");
    private final static QName _OrderBilling_QNAME = new QName("", "billing");
    private final static QName _ProfilePaymentPBReserveDays_QNAME = new QName("", "reserveDays");
    private final static QName _ProfilePaymentPBPayDays_QNAME = new QName("", "payDays");
    private final static QName _ListProfilesSize_QNAME = new QName("", "size");
    private final static QName _ListProfilesExpiredBy_QNAME = new QName("", "expiredBy");
    private final static QName _ListProfilesCustomerID_QNAME = new QName("", "customerID");
    private final static QName _ListProfilesBegin_QNAME = new QName("", "begin");
    private final static QName _PaymentELVMandateID_QNAME = new QName("", "mandateID");
    private final static QName _PaymentELVDateOfSignature_QNAME = new QName("", "dateOfSignature");
    private final static QName _PaymentELVBic_QNAME = new QName("", "bic");
    private final static QName _PaymentGIROPAYIban_QNAME = new QName("", "iban");
    private final static QName _PaymentKLARNAPClass_QNAME = new QName("", "pClass");
    private final static QName _PaymentProfileID_QNAME = new QName("", "profileID");
    private final static QName _PaymentManualClearing_QNAME = new QName("", "manualClearing");
    private final static QName _PaymentUseProfile_QNAME = new QName("", "useProfile");
    private final static QName _CreatePaymentTokenTemplateSet_QNAME = new QName("", "templateSet");
    private final static QName _CreatePaymentTokenDomain_QNAME = new QName("", "domain");
    private final static QName _CreatePaymentTokenStyle_QNAME = new QName("", "style");
    private final static QName _CreatePaymentTokenLanguage_QNAME = new QName("", "language");
    private final static QName _ManualCallbackResponseMpayTID_QNAME = new QName("", "mpayTID");
    private final static QName _ManualCallbackResponseErrNo_QNAME = new QName("", "errNo");
    private final static QName _ManualCallbackResponseLocation_QNAME = new QName("", "location");
    private final static QName _ManualCallbackResponseErrText_QNAME = new QName("", "errText");
    private final static QName _PaymentEPSBankID_QNAME = new QName("", "bankID");
    private final static QName _ManualCallbackOrder_QNAME = new QName("", "order");
    private final static QName _ItemNumber_QNAME = new QName("", "number");
    private final static QName _ItemPackage_QNAME = new QName("", "package");
    private final static QName _ItemQuantity_QNAME = new QName("", "quantity");
    private final static QName _ItemTax_QNAME = new QName("", "tax");
    private final static QName _ItemProductNr_QNAME = new QName("", "productNr");
    private final static QName _CreatePaymentTokenResponseApiKey_QNAME = new QName("", "apiKey");
    private final static QName _CreatePaymentTokenResponseToken_QNAME = new QName("", "token");
    private final static QName _ShoppingCartShippingTax_QNAME = new QName("", "shippingTax");
    private final static QName _ShoppingCartDiscount_QNAME = new QName("", "discount");
    private final static QName _ShoppingCartShippingCosts_QNAME = new QName("", "shippingCosts");
    private final static QName _TransactionStatusTid_QNAME = new QName("", "tid");
    private final static QName _ListNotClearedSortType_QNAME = new QName("", "sortType");
    private final static QName _ListNotClearedListInProgress_QNAME = new QName("", "listInProgress");
    private final static QName _ListNotClearedSortField_QNAME = new QName("", "sortField");
    private final static QName _AddressBirthday_QNAME = new QName("", "birthday");
    private final static QName _AddressGender_QNAME = new QName("", "gender");
    private final static QName _AddressPhone_QNAME = new QName("", "phone");
    private final static QName _AddressStreet2_QNAME = new QName("", "street2");
    private final static QName _AddressState_QNAME = new QName("", "state");
    private final static QName _AddressEmail_QNAME = new QName("", "email");
    private final static QName _PaymentProfileExpires_QNAME = new QName("", "expires");
    private final static QName _PaymentProfileAdress_QNAME = new QName("", "adress");
    private final static QName _PaymentProfilePMethodID_QNAME = new QName("", "pMethodID");
    private final static QName _AcceptPaymentErrorURL_QNAME = new QName("", "errorURL");
    private final static QName _AcceptPaymentConfirmationURL_QNAME = new QName("", "confirmationURL");
    private final static QName _AcceptPaymentSuccessURL_QNAME = new QName("", "successURL");
    private final static QName _AcceptPaymentCustomerName_QNAME = new QName("", "customerName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mpay24.etp
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ManualCallbackResponse }
     * 
     */
    public ManualCallbackResponse createManualCallbackResponse() {
        return new ManualCallbackResponse();
    }

    /**
     * Create an instance of {@link ManualCreditResponse }
     * 
     */
    public ManualCreditResponse createManualCreditResponse() {
        return new ManualCreditResponse();
    }

    /**
     * Create an instance of {@link TransactionStatus }
     * 
     */
    public TransactionStatus createTransactionStatus() {
        return new TransactionStatus();
    }

    /**
     * Create an instance of {@link DeleteProfileResponse }
     * 
     */
    public DeleteProfileResponse createDeleteProfileResponse() {
        return new DeleteProfileResponse();
    }

    /**
     * Create an instance of {@link CreatePaymentToken }
     * 
     */
    public CreatePaymentToken createCreatePaymentToken() {
        return new CreatePaymentToken();
    }

    /**
     * Create an instance of {@link AcceptPayment }
     * 
     */
    public AcceptPayment createAcceptPayment() {
        return new AcceptPayment();
    }

    /**
     * Create an instance of {@link AcceptPaymentResponse }
     * 
     */
    public AcceptPaymentResponse createAcceptPaymentResponse() {
        return new AcceptPaymentResponse();
    }

    /**
     * Create an instance of {@link SelectPayment }
     * 
     */
    public SelectPayment createSelectPayment() {
        return new SelectPayment();
    }

    /**
     * Create an instance of {@link ListProfilesResponse }
     * 
     */
    public ListProfilesResponse createListProfilesResponse() {
        return new ListProfilesResponse();
    }

    /**
     * Create an instance of {@link ListNotCleared }
     * 
     */
    public ListNotCleared createListNotCleared() {
        return new ListNotCleared();
    }

    /**
     * Create an instance of {@link ManualCredit }
     * 
     */
    public ManualCredit createManualCredit() {
        return new ManualCredit();
    }

    /**
     * Create an instance of {@link TransactionConfirmationResponse }
     * 
     */
    public TransactionConfirmationResponse createTransactionConfirmationResponse() {
        return new TransactionConfirmationResponse();
    }

    /**
     * Create an instance of {@link SelectPaymentResponse }
     * 
     */
    public SelectPaymentResponse createSelectPaymentResponse() {
        return new SelectPaymentResponse();
    }

    /**
     * Create an instance of {@link ManualClear }
     * 
     */
    public ManualClear createManualClear() {
        return new ManualClear();
    }

    /**
     * Create an instance of {@link ManualReverse }
     * 
     */
    public ManualReverse createManualReverse() {
        return new ManualReverse();
    }

    /**
     * Create an instance of {@link TransactionConfirmation }
     * 
     */
    public TransactionConfirmation createTransactionConfirmation() {
        return new TransactionConfirmation();
    }

    /**
     * Create an instance of {@link ManualClearResponse }
     * 
     */
    public ManualClearResponse createManualClearResponse() {
        return new ManualClearResponse();
    }

    /**
     * Create an instance of {@link DeleteProfile }
     * 
     */
    public DeleteProfile createDeleteProfile() {
        return new DeleteProfile();
    }

    /**
     * Create an instance of {@link ListPaymentMethodsResponse }
     * 
     */
    public ListPaymentMethodsResponse createListPaymentMethodsResponse() {
        return new ListPaymentMethodsResponse();
    }

    /**
     * Create an instance of {@link CreateProfileResponse }
     * 
     */
    public CreateProfileResponse createCreateProfileResponse() {
        return new CreateProfileResponse();
    }

    /**
     * Create an instance of {@link TransactionStatusResponse }
     * 
     */
    public TransactionStatusResponse createTransactionStatusResponse() {
        return new TransactionStatusResponse();
    }

    /**
     * Create an instance of {@link ListProfiles }
     * 
     */
    public ListProfiles createListProfiles() {
        return new ListProfiles();
    }

    /**
     * Create an instance of {@link ManualCallback }
     * 
     */
    public ManualCallback createManualCallback() {
        return new ManualCallback();
    }

    /**
     * Create an instance of {@link CreatePaymentTokenResponse }
     * 
     */
    public CreatePaymentTokenResponse createCreatePaymentTokenResponse() {
        return new CreatePaymentTokenResponse();
    }

    /**
     * Create an instance of {@link CreateProfile }
     * 
     */
    public CreateProfile createCreateProfile() {
        return new CreateProfile();
    }

    /**
     * Create an instance of {@link ListPaymentMethods }
     * 
     */
    public ListPaymentMethods createListPaymentMethods() {
        return new ListPaymentMethods();
    }

    /**
     * Create an instance of {@link ManualReverseResponse }
     * 
     */
    public ManualReverseResponse createManualReverseResponse() {
        return new ManualReverseResponse();
    }

    /**
     * Create an instance of {@link ListNotClearedResponse }
     * 
     */
    public ListNotClearedResponse createListNotClearedResponse() {
        return new ListNotClearedResponse();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link PaymentCB }
     * 
     */
    public PaymentCB createPaymentCB() {
        return new PaymentCB();
    }

    /**
     * Create an instance of {@link PaymentBILLPAY }
     * 
     */
    public PaymentBILLPAY createPaymentBILLPAY() {
        return new PaymentBILLPAY();
    }

    /**
     * Create an instance of {@link PaymentCC }
     * 
     */
    public PaymentCC createPaymentCC() {
        return new PaymentCC();
    }

    /**
     * Create an instance of {@link PaymentPB }
     * 
     */
    public PaymentPB createPaymentPB() {
        return new PaymentPB();
    }

    /**
     * Create an instance of {@link Item }
     * 
     */
    public Item createItem() {
        return new Item();
    }

    /**
     * Create an instance of {@link PaymentEPS }
     * 
     */
    public PaymentEPS createPaymentEPS() {
        return new PaymentEPS();
    }

    /**
     * Create an instance of {@link PaymentKLARNA }
     * 
     */
    public PaymentKLARNA createPaymentKLARNA() {
        return new PaymentKLARNA();
    }

    /**
     * Create an instance of {@link ProfilePaymentCC }
     * 
     */
    public ProfilePaymentCC createProfilePaymentCC() {
        return new ProfilePaymentCC();
    }

    /**
     * Create an instance of {@link PaymentELV }
     * 
     */
    public PaymentELV createPaymentELV() {
        return new PaymentELV();
    }

    /**
     * Create an instance of {@link PaymentProfile }
     * 
     */
    public PaymentProfile createPaymentProfile() {
        return new PaymentProfile();
    }

    /**
     * Create an instance of {@link AirlineTicket }
     * 
     */
    public AirlineTicket createAirlineTicket() {
        return new AirlineTicket();
    }

    /**
     * Create an instance of {@link Order }
     * 
     */
    public Order createOrder() {
        return new Order();
    }

    /**
     * Create an instance of {@link CallbackPAYPAL }
     * 
     */
    public CallbackPAYPAL createCallbackPAYPAL() {
        return new CallbackPAYPAL();
    }

    /**
     * Create an instance of {@link PaymentGIROPAY }
     * 
     */
    public PaymentGIROPAY createPaymentGIROPAY() {
        return new PaymentGIROPAY();
    }

    /**
     * Create an instance of {@link ProfilePaymentPB }
     * 
     */
    public ProfilePaymentPB createProfilePaymentPB() {
        return new ProfilePaymentPB();
    }

    /**
     * Create an instance of {@link PaymentPAYPAL }
     * 
     */
    public PaymentPAYPAL createPaymentPAYPAL() {
        return new PaymentPAYPAL();
    }

    /**
     * Create an instance of {@link Payment }
     * 
     */
    public Payment createPayment() {
        return new Payment();
    }

    /**
     * Create an instance of {@link PaymentTOKEN }
     * 
     */
    public PaymentTOKEN createPaymentTOKEN() {
        return new PaymentTOKEN();
    }

    /**
     * Create an instance of {@link Transaction }
     * 
     */
    public Transaction createTransaction() {
        return new Transaction();
    }

    /**
     * Create an instance of {@link PaymentMethod }
     * 
     */
    public PaymentMethod createPaymentMethod() {
        return new PaymentMethod();
    }

    /**
     * Create an instance of {@link PaymentMAESTRO }
     * 
     */
    public PaymentMAESTRO createPaymentMAESTRO() {
        return new PaymentMAESTRO();
    }

    /**
     * Create an instance of {@link Confirmation }
     * 
     */
    public Confirmation createConfirmation() {
        return new Confirmation();
    }

    /**
     * Create an instance of {@link IndustrySpecific }
     * 
     */
    public IndustrySpecific createIndustrySpecific() {
        return new IndustrySpecific();
    }

    /**
     * Create an instance of {@link Profile }
     * 
     */
    public Profile createProfile() {
        return new Profile();
    }

    /**
     * Create an instance of {@link ProfilePaymentELV }
     * 
     */
    public ProfilePaymentELV createProfilePaymentELV() {
        return new ProfilePaymentELV();
    }

    /**
     * Create an instance of {@link ClearingDetails }
     * 
     */
    public ClearingDetails createClearingDetails() {
        return new ClearingDetails();
    }

    /**
     * Create an instance of {@link TransactionDetails }
     * 
     */
    public TransactionDetails createTransactionDetails() {
        return new TransactionDetails();
    }

    /**
     * Create an instance of {@link Callback }
     * 
     */
    public Callback createCallback() {
        return new Callback();
    }

    /**
     * Create an instance of {@link ShoppingCart }
     * 
     */
    public ShoppingCart createShoppingCart() {
        return new ShoppingCart();
    }

    /**
     * Create an instance of {@link Parameter }
     * 
     */
    public Parameter createParameter() {
        return new Parameter();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ManualCallbackResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ManualCallbackResponse")
    public JAXBElement<ManualCallbackResponse> createManualCallbackResponse(ManualCallbackResponse value) {
        return new JAXBElement<ManualCallbackResponse>(_ManualCallbackResponse_QNAME, ManualCallbackResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ManualCreditResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ManualCreditResponse")
    public JAXBElement<ManualCreditResponse> createManualCreditResponse(ManualCreditResponse value) {
        return new JAXBElement<ManualCreditResponse>(_ManualCreditResponse_QNAME, ManualCreditResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "TransactionStatus")
    public JAXBElement<TransactionStatus> createTransactionStatus(TransactionStatus value) {
        return new JAXBElement<TransactionStatus>(_TransactionStatus_QNAME, TransactionStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "DeleteProfileResponse")
    public JAXBElement<DeleteProfileResponse> createDeleteProfileResponse(DeleteProfileResponse value) {
        return new JAXBElement<DeleteProfileResponse>(_DeleteProfileResponse_QNAME, DeleteProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcceptPayment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "AcceptPayment")
    public JAXBElement<AcceptPayment> createAcceptPayment(AcceptPayment value) {
        return new JAXBElement<AcceptPayment>(_AcceptPayment_QNAME, AcceptPayment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcceptPaymentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "AcceptPaymentResponse")
    public JAXBElement<AcceptPaymentResponse> createAcceptPaymentResponse(AcceptPaymentResponse value) {
        return new JAXBElement<AcceptPaymentResponse>(_AcceptPaymentResponse_QNAME, AcceptPaymentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePaymentToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "CreatePaymentToken")
    public JAXBElement<CreatePaymentToken> createCreatePaymentToken(CreatePaymentToken value) {
        return new JAXBElement<CreatePaymentToken>(_CreatePaymentToken_QNAME, CreatePaymentToken.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectPayment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "SelectPayment")
    public JAXBElement<SelectPayment> createSelectPayment(SelectPayment value) {
        return new JAXBElement<SelectPayment>(_SelectPayment_QNAME, SelectPayment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListProfilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ListProfilesResponse")
    public JAXBElement<ListProfilesResponse> createListProfilesResponse(ListProfilesResponse value) {
        return new JAXBElement<ListProfilesResponse>(_ListProfilesResponse_QNAME, ListProfilesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ManualCredit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ManualCredit")
    public JAXBElement<ManualCredit> createManualCredit(ManualCredit value) {
        return new JAXBElement<ManualCredit>(_ManualCredit_QNAME, ManualCredit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionConfirmationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "TransactionConfirmationResponse")
    public JAXBElement<TransactionConfirmationResponse> createTransactionConfirmationResponse(TransactionConfirmationResponse value) {
        return new JAXBElement<TransactionConfirmationResponse>(_TransactionConfirmationResponse_QNAME, TransactionConfirmationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListNotCleared }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ListNotCleared")
    public JAXBElement<ListNotCleared> createListNotCleared(ListNotCleared value) {
        return new JAXBElement<ListNotCleared>(_ListNotCleared_QNAME, ListNotCleared.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectPaymentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "SelectPaymentResponse")
    public JAXBElement<SelectPaymentResponse> createSelectPaymentResponse(SelectPaymentResponse value) {
        return new JAXBElement<SelectPaymentResponse>(_SelectPaymentResponse_QNAME, SelectPaymentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ManualClear }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ManualClear")
    public JAXBElement<ManualClear> createManualClear(ManualClear value) {
        return new JAXBElement<ManualClear>(_ManualClear_QNAME, ManualClear.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ManualReverse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ManualReverse")
    public JAXBElement<ManualReverse> createManualReverse(ManualReverse value) {
        return new JAXBElement<ManualReverse>(_ManualReverse_QNAME, ManualReverse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionConfirmation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "TransactionConfirmation")
    public JAXBElement<TransactionConfirmation> createTransactionConfirmation(TransactionConfirmation value) {
        return new JAXBElement<TransactionConfirmation>(_TransactionConfirmation_QNAME, TransactionConfirmation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ManualClearResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ManualClearResponse")
    public JAXBElement<ManualClearResponse> createManualClearResponse(ManualClearResponse value) {
        return new JAXBElement<ManualClearResponse>(_ManualClearResponse_QNAME, ManualClearResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "DeleteProfile")
    public JAXBElement<DeleteProfile> createDeleteProfile(DeleteProfile value) {
        return new JAXBElement<DeleteProfile>(_DeleteProfile_QNAME, DeleteProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListPaymentMethodsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ListPaymentMethodsResponse")
    public JAXBElement<ListPaymentMethodsResponse> createListPaymentMethodsResponse(ListPaymentMethodsResponse value) {
        return new JAXBElement<ListPaymentMethodsResponse>(_ListPaymentMethodsResponse_QNAME, ListPaymentMethodsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "CreateProfileResponse")
    public JAXBElement<CreateProfileResponse> createCreateProfileResponse(CreateProfileResponse value) {
        return new JAXBElement<CreateProfileResponse>(_CreateProfileResponse_QNAME, CreateProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListProfiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ListProfiles")
    public JAXBElement<ListProfiles> createListProfiles(ListProfiles value) {
        return new JAXBElement<ListProfiles>(_ListProfiles_QNAME, ListProfiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "TransactionStatusResponse")
    public JAXBElement<TransactionStatusResponse> createTransactionStatusResponse(TransactionStatusResponse value) {
        return new JAXBElement<TransactionStatusResponse>(_TransactionStatusResponse_QNAME, TransactionStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ManualCallback }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ManualCallback")
    public JAXBElement<ManualCallback> createManualCallback(ManualCallback value) {
        return new JAXBElement<ManualCallback>(_ManualCallback_QNAME, ManualCallback.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListPaymentMethods }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ListPaymentMethods")
    public JAXBElement<ListPaymentMethods> createListPaymentMethods(ListPaymentMethods value) {
        return new JAXBElement<ListPaymentMethods>(_ListPaymentMethods_QNAME, ListPaymentMethods.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ManualReverseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ManualReverseResponse")
    public JAXBElement<ManualReverseResponse> createManualReverseResponse(ManualReverseResponse value) {
        return new JAXBElement<ManualReverseResponse>(_ManualReverseResponse_QNAME, ManualReverseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePaymentTokenResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "CreatePaymentTokenResponse")
    public JAXBElement<CreatePaymentTokenResponse> createCreatePaymentTokenResponse(CreatePaymentTokenResponse value) {
        return new JAXBElement<CreatePaymentTokenResponse>(_CreatePaymentTokenResponse_QNAME, CreatePaymentTokenResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "CreateProfile")
    public JAXBElement<CreateProfile> createCreateProfile(CreateProfile value) {
        return new JAXBElement<CreateProfile>(_CreateProfile_QNAME, CreateProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListNotClearedResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://www.mpay24.com/soap/etp/1.5/ETP.wsdl", name = "ListNotClearedResponse")
    public JAXBElement<ListNotClearedResponse> createListNotClearedResponse(ListNotClearedResponse value) {
        return new JAXBElement<ListNotClearedResponse>(_ListNotClearedResponse_QNAME, ListNotClearedResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cvc", scope = PaymentCC.class)
    public JAXBElement<String> createPaymentCCCvc(String value) {
        return new JAXBElement<String>(_PaymentCCCvc_QNAME, String.class, PaymentCC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "amount", scope = Callback.class)
    public JAXBElement<Long> createCallbackAmount(Long value) {
        return new JAXBElement<Long>(_CallbackAmount_QNAME, Long.class, Callback.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pType", scope = ListPaymentMethods.class)
    public JAXBElement<PaymentType> createListPaymentMethodsPType(PaymentType value) {
        return new JAXBElement<PaymentType>(_ListPaymentMethodsPType_QNAME, PaymentType.class, ListPaymentMethods.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "custom", scope = PaymentPAYPAL.class)
    public JAXBElement<String> createPaymentPAYPALCustom(String value) {
        return new JAXBElement<String>(_PaymentPAYPALCustom_QNAME, String.class, PaymentPAYPAL.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndustrySpecific }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "industrySpecific", scope = Order.class)
    public JAXBElement<IndustrySpecific> createOrderIndustrySpecific(IndustrySpecific value) {
        return new JAXBElement<IndustrySpecific>(_OrderIndustrySpecific_QNAME, IndustrySpecific.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "shipping", scope = Order.class)
    public JAXBElement<Address> createOrderShipping(Address value) {
        return new JAXBElement<Address>(_OrderShipping_QNAME, Address.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "clientIP", scope = Order.class)
    public JAXBElement<String> createOrderClientIP(String value) {
        return new JAXBElement<String>(_OrderClientIP_QNAME, String.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "userField", scope = Order.class)
    public JAXBElement<String> createOrderUserField(String value) {
        return new JAXBElement<String>(_OrderUserField_QNAME, String.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShoppingCart }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "shoppingCart", scope = Order.class)
    public JAXBElement<ShoppingCart> createOrderShoppingCart(ShoppingCart value) {
        return new JAXBElement<ShoppingCart>(_OrderShoppingCart_QNAME, ShoppingCart.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "description", scope = Order.class)
    public JAXBElement<String> createOrderDescription(String value) {
        return new JAXBElement<String>(_OrderDescription_QNAME, String.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "billing", scope = Order.class)
    public JAXBElement<Address> createOrderBilling(Address value) {
        return new JAXBElement<Address>(_OrderBilling_QNAME, Address.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "reserveDays", scope = ProfilePaymentPB.class)
    public JAXBElement<Long> createProfilePaymentPBReserveDays(Long value) {
        return new JAXBElement<Long>(_ProfilePaymentPBReserveDays_QNAME, Long.class, ProfilePaymentPB.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "payDays", scope = ProfilePaymentPB.class)
    public JAXBElement<Long> createProfilePaymentPBPayDays(Long value) {
        return new JAXBElement<Long>(_ProfilePaymentPBPayDays_QNAME, Long.class, ProfilePaymentPB.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "size", scope = ListProfiles.class)
    public JAXBElement<Long> createListProfilesSize(Long value) {
        return new JAXBElement<Long>(_ListProfilesSize_QNAME, Long.class, ListProfiles.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "expiredBy", scope = ListProfiles.class)
    public JAXBElement<XMLGregorianCalendar> createListProfilesExpiredBy(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ListProfilesExpiredBy_QNAME, XMLGregorianCalendar.class, ListProfiles.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "customerID", scope = ListProfiles.class)
    public JAXBElement<String> createListProfilesCustomerID(String value) {
        return new JAXBElement<String>(_ListProfilesCustomerID_QNAME, String.class, ListProfiles.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "begin", scope = ListProfiles.class)
    public JAXBElement<Long> createListProfilesBegin(Long value) {
        return new JAXBElement<Long>(_ListProfilesBegin_QNAME, Long.class, ListProfiles.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mandateID", scope = PaymentELV.class)
    public JAXBElement<String> createPaymentELVMandateID(String value) {
        return new JAXBElement<String>(_PaymentELVMandateID_QNAME, String.class, PaymentELV.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dateOfSignature", scope = PaymentELV.class)
    public JAXBElement<XMLGregorianCalendar> createPaymentELVDateOfSignature(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PaymentELVDateOfSignature_QNAME, XMLGregorianCalendar.class, PaymentELV.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "bic", scope = PaymentELV.class)
    public JAXBElement<String> createPaymentELVBic(String value) {
        return new JAXBElement<String>(_PaymentELVBic_QNAME, String.class, PaymentELV.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "iban", scope = PaymentGIROPAY.class)
    public JAXBElement<String> createPaymentGIROPAYIban(String value) {
        return new JAXBElement<String>(_PaymentGIROPAYIban_QNAME, String.class, PaymentGIROPAY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pClass", scope = PaymentKLARNA.class)
    public JAXBElement<Long> createPaymentKLARNAPClass(Long value) {
        return new JAXBElement<Long>(_PaymentKLARNAPClass_QNAME, Long.class, PaymentKLARNA.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "profileID", scope = Payment.class)
    public JAXBElement<String> createPaymentProfileID(String value) {
        return new JAXBElement<String>(_PaymentProfileID_QNAME, String.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "manualClearing", scope = Payment.class)
    public JAXBElement<Boolean> createPaymentManualClearing(Boolean value) {
        return new JAXBElement<Boolean>(_PaymentManualClearing_QNAME, Boolean.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "useProfile", scope = Payment.class)
    public JAXBElement<Boolean> createPaymentUseProfile(Boolean value) {
        return new JAXBElement<Boolean>(_PaymentUseProfile_QNAME, Boolean.class, Payment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cvc", scope = PaymentCB.class)
    public JAXBElement<String> createPaymentCBCvc(String value) {
        return new JAXBElement<String>(_PaymentCCCvc_QNAME, String.class, PaymentCB.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "templateSet", scope = CreatePaymentToken.class)
    public JAXBElement<String> createCreatePaymentTokenTemplateSet(String value) {
        return new JAXBElement<String>(_CreatePaymentTokenTemplateSet_QNAME, String.class, CreatePaymentToken.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "profileID", scope = CreatePaymentToken.class)
    public JAXBElement<String> createCreatePaymentTokenProfileID(String value) {
        return new JAXBElement<String>(_PaymentProfileID_QNAME, String.class, CreatePaymentToken.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "domain", scope = CreatePaymentToken.class)
    public JAXBElement<String> createCreatePaymentTokenDomain(String value) {
        return new JAXBElement<String>(_CreatePaymentTokenDomain_QNAME, String.class, CreatePaymentToken.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "customerID", scope = CreatePaymentToken.class)
    public JAXBElement<String> createCreatePaymentTokenCustomerID(String value) {
        return new JAXBElement<String>(_ListProfilesCustomerID_QNAME, String.class, CreatePaymentToken.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "style", scope = CreatePaymentToken.class)
    public JAXBElement<String> createCreatePaymentTokenStyle(String value) {
        return new JAXBElement<String>(_CreatePaymentTokenStyle_QNAME, String.class, CreatePaymentToken.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "language", scope = CreatePaymentToken.class)
    public JAXBElement<String> createCreatePaymentTokenLanguage(String value) {
        return new JAXBElement<String>(_CreatePaymentTokenLanguage_QNAME, String.class, CreatePaymentToken.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mpayTID", scope = ManualCallbackResponse.class)
    public JAXBElement<BigInteger> createManualCallbackResponseMpayTID(BigInteger value) {
        return new JAXBElement<BigInteger>(_ManualCallbackResponseMpayTID_QNAME, BigInteger.class, ManualCallbackResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errNo", scope = ManualCallbackResponse.class)
    public JAXBElement<Integer> createManualCallbackResponseErrNo(Integer value) {
        return new JAXBElement<Integer>(_ManualCallbackResponseErrNo_QNAME, Integer.class, ManualCallbackResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "location", scope = ManualCallbackResponse.class)
    public JAXBElement<String> createManualCallbackResponseLocation(String value) {
        return new JAXBElement<String>(_ManualCallbackResponseLocation_QNAME, String.class, ManualCallbackResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errText", scope = ManualCallbackResponse.class)
    public JAXBElement<String> createManualCallbackResponseErrText(String value) {
        return new JAXBElement<String>(_ManualCallbackResponseErrText_QNAME, String.class, ManualCallbackResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "amount", scope = ManualCredit.class)
    public JAXBElement<Long> createManualCreditAmount(Long value) {
        return new JAXBElement<Long>(_CallbackAmount_QNAME, Long.class, ManualCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "bankID", scope = PaymentEPS.class)
    public JAXBElement<Long> createPaymentEPSBankID(Long value) {
        return new JAXBElement<Long>(_PaymentEPSBankID_QNAME, Long.class, PaymentEPS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "reserveDays", scope = PaymentPB.class)
    public JAXBElement<Long> createPaymentPBReserveDays(Long value) {
        return new JAXBElement<Long>(_ProfilePaymentPBReserveDays_QNAME, Long.class, PaymentPB.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "payDays", scope = PaymentPB.class)
    public JAXBElement<Long> createPaymentPBPayDays(Long value) {
        return new JAXBElement<Long>(_ProfilePaymentPBPayDays_QNAME, Long.class, PaymentPB.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Order }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "order", scope = ManualCallback.class)
    public JAXBElement<Order> createManualCallbackOrder(Order value) {
        return new JAXBElement<Order>(_ManualCallbackOrder_QNAME, Order.class, ManualCallback.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "number", scope = Item.class)
    public JAXBElement<String> createItemNumber(String value) {
        return new JAXBElement<String>(_ItemNumber_QNAME, String.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "package", scope = Item.class)
    public JAXBElement<String> createItemPackage(String value) {
        return new JAXBElement<String>(_ItemPackage_QNAME, String.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "quantity", scope = Item.class)
    public JAXBElement<Long> createItemQuantity(Long value) {
        return new JAXBElement<Long>(_ItemQuantity_QNAME, Long.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "description", scope = Item.class)
    public JAXBElement<String> createItemDescription(String value) {
        return new JAXBElement<String>(_OrderDescription_QNAME, String.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tax", scope = Item.class)
    public JAXBElement<Integer> createItemTax(Integer value) {
        return new JAXBElement<Integer>(_ItemTax_QNAME, Integer.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "productNr", scope = Item.class)
    public JAXBElement<String> createItemProductNr(String value) {
        return new JAXBElement<String>(_ItemProductNr_QNAME, String.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errNo", scope = SelectPaymentResponse.class)
    public JAXBElement<Integer> createSelectPaymentResponseErrNo(Integer value) {
        return new JAXBElement<Integer>(_ManualCallbackResponseErrNo_QNAME, Integer.class, SelectPaymentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errText", scope = SelectPaymentResponse.class)
    public JAXBElement<String> createSelectPaymentResponseErrText(String value) {
        return new JAXBElement<String>(_ManualCallbackResponseErrText_QNAME, String.class, SelectPaymentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mandateID", scope = ProfilePaymentELV.class)
    public JAXBElement<String> createProfilePaymentELVMandateID(String value) {
        return new JAXBElement<String>(_PaymentELVMandateID_QNAME, String.class, ProfilePaymentELV.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dateOfSignature", scope = ProfilePaymentELV.class)
    public JAXBElement<XMLGregorianCalendar> createProfilePaymentELVDateOfSignature(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PaymentELVDateOfSignature_QNAME, XMLGregorianCalendar.class, ProfilePaymentELV.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errNo", scope = CreatePaymentTokenResponse.class)
    public JAXBElement<Integer> createCreatePaymentTokenResponseErrNo(Integer value) {
        return new JAXBElement<Integer>(_ManualCallbackResponseErrNo_QNAME, Integer.class, CreatePaymentTokenResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "apiKey", scope = CreatePaymentTokenResponse.class)
    public JAXBElement<String> createCreatePaymentTokenResponseApiKey(String value) {
        return new JAXBElement<String>(_CreatePaymentTokenResponseApiKey_QNAME, String.class, CreatePaymentTokenResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "location", scope = CreatePaymentTokenResponse.class)
    public JAXBElement<String> createCreatePaymentTokenResponseLocation(String value) {
        return new JAXBElement<String>(_ManualCallbackResponseLocation_QNAME, String.class, CreatePaymentTokenResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errText", scope = CreatePaymentTokenResponse.class)
    public JAXBElement<String> createCreatePaymentTokenResponseErrText(String value) {
        return new JAXBElement<String>(_ManualCallbackResponseErrText_QNAME, String.class, CreatePaymentTokenResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "token", scope = CreatePaymentTokenResponse.class)
    public JAXBElement<String> createCreatePaymentTokenResponseToken(String value) {
        return new JAXBElement<String>(_CreatePaymentTokenResponseToken_QNAME, String.class, CreatePaymentTokenResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "shippingTax", scope = ShoppingCart.class)
    public JAXBElement<Integer> createShoppingCartShippingTax(Integer value) {
        return new JAXBElement<Integer>(_ShoppingCartShippingTax_QNAME, Integer.class, ShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "discount", scope = ShoppingCart.class)
    public JAXBElement<Integer> createShoppingCartDiscount(Integer value) {
        return new JAXBElement<Integer>(_ShoppingCartDiscount_QNAME, Integer.class, ShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tax", scope = ShoppingCart.class)
    public JAXBElement<Integer> createShoppingCartTax(Integer value) {
        return new JAXBElement<Integer>(_ItemTax_QNAME, Integer.class, ShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "shippingCosts", scope = ShoppingCart.class)
    public JAXBElement<Integer> createShoppingCartShippingCosts(Integer value) {
        return new JAXBElement<Integer>(_ShoppingCartShippingCosts_QNAME, Integer.class, ShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errNo", scope = CreateProfileResponse.class)
    public JAXBElement<Integer> createCreateProfileResponseErrNo(Integer value) {
        return new JAXBElement<Integer>(_ManualCallbackResponseErrNo_QNAME, Integer.class, CreateProfileResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "location", scope = CreateProfileResponse.class)
    public JAXBElement<String> createCreateProfileResponseLocation(String value) {
        return new JAXBElement<String>(_ManualCallbackResponseLocation_QNAME, String.class, CreateProfileResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errText", scope = CreateProfileResponse.class)
    public JAXBElement<String> createCreateProfileResponseErrText(String value) {
        return new JAXBElement<String>(_ManualCallbackResponseErrText_QNAME, String.class, CreateProfileResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mpayTID", scope = TransactionStatus.class)
    public JAXBElement<BigInteger> createTransactionStatusMpayTID(BigInteger value) {
        return new JAXBElement<BigInteger>(_ManualCallbackResponseMpayTID_QNAME, BigInteger.class, TransactionStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tid", scope = TransactionStatus.class)
    public JAXBElement<String> createTransactionStatusTid(String value) {
        return new JAXBElement<String>(_TransactionStatusTid_QNAME, String.class, TransactionStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "amount", scope = ClearingDetails.class)
    public JAXBElement<Long> createClearingDetailsAmount(Long value) {
        return new JAXBElement<Long>(_CallbackAmount_QNAME, Long.class, ClearingDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Order }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "order", scope = ClearingDetails.class)
    public JAXBElement<Order> createClearingDetailsOrder(Order value) {
        return new JAXBElement<Order>(_ManualCallbackOrder_QNAME, Order.class, ClearingDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "size", scope = ListNotCleared.class)
    public JAXBElement<Long> createListNotClearedSize(Long value) {
        return new JAXBElement<Long>(_ListProfilesSize_QNAME, Long.class, ListNotCleared.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SortType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sortType", scope = ListNotCleared.class)
    public JAXBElement<SortType> createListNotClearedSortType(SortType value) {
        return new JAXBElement<SortType>(_ListNotClearedSortType_QNAME, SortType.class, ListNotCleared.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "listInProgress", scope = ListNotCleared.class)
    public JAXBElement<Boolean> createListNotClearedListInProgress(Boolean value) {
        return new JAXBElement<Boolean>(_ListNotClearedListInProgress_QNAME, Boolean.class, ListNotCleared.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SortField }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sortField", scope = ListNotCleared.class)
    public JAXBElement<SortField> createListNotClearedSortField(SortField value) {
        return new JAXBElement<SortField>(_ListNotClearedSortField_QNAME, SortField.class, ListNotCleared.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "begin", scope = ListNotCleared.class)
    public JAXBElement<Long> createListNotClearedBegin(Long value) {
        return new JAXBElement<Long>(_ListProfilesBegin_QNAME, Long.class, ListNotCleared.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "birthday", scope = Address.class)
    public JAXBElement<XMLGregorianCalendar> createAddressBirthday(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AddressBirthday_QNAME, XMLGregorianCalendar.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Gender }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "gender", scope = Address.class)
    public JAXBElement<Gender> createAddressGender(Gender value) {
        return new JAXBElement<Gender>(_AddressGender_QNAME, Gender.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "phone", scope = Address.class)
    public JAXBElement<String> createAddressPhone(String value) {
        return new JAXBElement<String>(_AddressPhone_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "street2", scope = Address.class)
    public JAXBElement<String> createAddressStreet2(String value) {
        return new JAXBElement<String>(_AddressStreet2_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "state", scope = Address.class)
    public JAXBElement<String> createAddressState(String value) {
        return new JAXBElement<String>(_AddressState_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "email", scope = Address.class)
    public JAXBElement<String> createAddressEmail(String value) {
        return new JAXBElement<String>(_AddressEmail_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "expires", scope = PaymentProfile.class)
    public JAXBElement<XMLGregorianCalendar> createPaymentProfileExpires(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_PaymentProfileExpires_QNAME, XMLGregorianCalendar.class, PaymentProfile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "adress", scope = PaymentProfile.class)
    public JAXBElement<Address> createPaymentProfileAdress(Address value) {
        return new JAXBElement<Address>(_PaymentProfileAdress_QNAME, Address.class, PaymentProfile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pMethodID", scope = PaymentProfile.class)
    public JAXBElement<Long> createPaymentProfilePMethodID(Long value) {
        return new JAXBElement<Long>(_PaymentProfilePMethodID_QNAME, Long.class, PaymentProfile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errorURL", scope = AcceptPayment.class)
    public JAXBElement<String> createAcceptPaymentErrorURL(String value) {
        return new JAXBElement<String>(_AcceptPaymentErrorURL_QNAME, String.class, AcceptPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "confirmationURL", scope = AcceptPayment.class)
    public JAXBElement<String> createAcceptPaymentConfirmationURL(String value) {
        return new JAXBElement<String>(_AcceptPaymentConfirmationURL_QNAME, String.class, AcceptPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "customerID", scope = AcceptPayment.class)
    public JAXBElement<String> createAcceptPaymentCustomerID(String value) {
        return new JAXBElement<String>(_ListProfilesCustomerID_QNAME, String.class, AcceptPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "successURL", scope = AcceptPayment.class)
    public JAXBElement<String> createAcceptPaymentSuccessURL(String value) {
        return new JAXBElement<String>(_AcceptPaymentSuccessURL_QNAME, String.class, AcceptPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "language", scope = AcceptPayment.class)
    public JAXBElement<String> createAcceptPaymentLanguage(String value) {
        return new JAXBElement<String>(_CreatePaymentTokenLanguage_QNAME, String.class, AcceptPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "customerName", scope = AcceptPayment.class)
    public JAXBElement<String> createAcceptPaymentCustomerName(String value) {
        return new JAXBElement<String>(_AcceptPaymentCustomerName_QNAME, String.class, AcceptPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Order }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "order", scope = AcceptPayment.class)
    public JAXBElement<Order> createAcceptPaymentOrder(Order value) {
        return new JAXBElement<Order>(_ManualCallbackOrder_QNAME, Order.class, AcceptPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cvc", scope = ProfilePaymentCC.class)
    public JAXBElement<String> createProfilePaymentCCCvc(String value) {
        return new JAXBElement<String>(_PaymentCCCvc_QNAME, String.class, ProfilePaymentCC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mpayTID", scope = AcceptPaymentResponse.class)
    public JAXBElement<BigInteger> createAcceptPaymentResponseMpayTID(BigInteger value) {
        return new JAXBElement<BigInteger>(_ManualCallbackResponseMpayTID_QNAME, BigInteger.class, AcceptPaymentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errNo", scope = AcceptPaymentResponse.class)
    public JAXBElement<Integer> createAcceptPaymentResponseErrNo(Integer value) {
        return new JAXBElement<Integer>(_ManualCallbackResponseErrNo_QNAME, Integer.class, AcceptPaymentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "location", scope = AcceptPaymentResponse.class)
    public JAXBElement<String> createAcceptPaymentResponseLocation(String value) {
        return new JAXBElement<String>(_ManualCallbackResponseLocation_QNAME, String.class, AcceptPaymentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errText", scope = AcceptPaymentResponse.class)
    public JAXBElement<String> createAcceptPaymentResponseErrText(String value) {
        return new JAXBElement<String>(_ManualCallbackResponseErrText_QNAME, String.class, AcceptPaymentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "profileID", scope = DeleteProfile.class)
    public JAXBElement<String> createDeleteProfileProfileID(String value) {
        return new JAXBElement<String>(_PaymentProfileID_QNAME, String.class, DeleteProfile.class, value);
    }

}
