
package com.mpay24.etp;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AddressMode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="READONLY"/>
 *     &lt;enumeration value="READWRITE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AddressMode")
@XmlEnum
public enum AddressMode {

    READONLY,
    READWRITE;

    public String value() {
        return name();
    }

    public static AddressMode fromValue(String v) {
        return valueOf(v);
    }

}
