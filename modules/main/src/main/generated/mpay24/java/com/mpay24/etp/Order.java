
package com.mpay24.etp;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Customer order definition
 * 
 * <p>Java class for Order complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Order">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clientIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userField" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shoppingCart" type="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}ShoppingCart" minOccurs="0"/>
 *         &lt;element name="industrySpecific" type="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}IndustrySpecific" minOccurs="0"/>
 *         &lt;element name="billing" type="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}Address" minOccurs="0"/>
 *         &lt;element name="shipping" type="{https://www.mpay24.com/soap/etp/1.5/ETP.wsdl}Address" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Order", propOrder = {
    "clientIP",
    "description",
    "userField",
    "shoppingCart",
    "industrySpecific",
    "billing",
    "shipping"
})
public class Order {

    @XmlElementRef(name = "clientIP", type = JAXBElement.class, required = false)
    protected JAXBElement<String> clientIP;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "userField", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userField;
    @XmlElementRef(name = "shoppingCart", type = JAXBElement.class, required = false)
    protected JAXBElement<ShoppingCart> shoppingCart;
    @XmlElementRef(name = "industrySpecific", type = JAXBElement.class, required = false)
    protected JAXBElement<IndustrySpecific> industrySpecific;
    @XmlElementRef(name = "billing", type = JAXBElement.class, required = false)
    protected JAXBElement<Address> billing;
    @XmlElementRef(name = "shipping", type = JAXBElement.class, required = false)
    protected JAXBElement<Address> shipping;

    /**
     * Gets the value of the clientIP property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClientIP() {
        return clientIP;
    }

    /**
     * Sets the value of the clientIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClientIP(JAXBElement<String> value) {
        this.clientIP = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Gets the value of the userField property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserField() {
        return userField;
    }

    /**
     * Sets the value of the userField property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserField(JAXBElement<String> value) {
        this.userField = value;
    }

    /**
     * Gets the value of the shoppingCart property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ShoppingCart }{@code >}
     *     
     */
    public JAXBElement<ShoppingCart> getShoppingCart() {
        return shoppingCart;
    }

    /**
     * Sets the value of the shoppingCart property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ShoppingCart }{@code >}
     *     
     */
    public void setShoppingCart(JAXBElement<ShoppingCart> value) {
        this.shoppingCart = value;
    }

    /**
     * Gets the value of the industrySpecific property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link IndustrySpecific }{@code >}
     *     
     */
    public JAXBElement<IndustrySpecific> getIndustrySpecific() {
        return industrySpecific;
    }

    /**
     * Sets the value of the industrySpecific property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link IndustrySpecific }{@code >}
     *     
     */
    public void setIndustrySpecific(JAXBElement<IndustrySpecific> value) {
        this.industrySpecific = value;
    }

    /**
     * Gets the value of the billing property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public JAXBElement<Address> getBilling() {
        return billing;
    }

    /**
     * Sets the value of the billing property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public void setBilling(JAXBElement<Address> value) {
        this.billing = value;
    }

    /**
     * Gets the value of the shipping property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public JAXBElement<Address> getShipping() {
        return shipping;
    }

    /**
     * Sets the value of the shipping property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public void setShipping(JAXBElement<Address> value) {
        this.shipping = value;
    }

}
