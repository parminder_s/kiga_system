package com.kiga.integration.amazon.s3.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Component

import scala.beans.BeanProperty

/**
 * @author robert.klodzinski@treevert.com
 * @since 5/23/15.
 */
@Component
@Primary
@ConfigurationProperties("amazon.s3")
class AmazonS3Parameters {
    @BeanProperty
    var bucketName: String = _
    @BeanProperty
    var bucketUrl: String = _
    @BeanProperty
    var accessKey: String = _
    @BeanProperty
    var secretKey: String = _
}
