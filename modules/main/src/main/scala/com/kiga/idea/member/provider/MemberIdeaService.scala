package com.kiga.idea.member.provider

import java.nio.file.{Files, Path, Paths}
import javax.inject.{Inject, Named}

import com.kiga.idea.member.web.request.MemberIdeaRequest
import com.kiga.idea.member.web.responses.MemberIdeaResponse
import com.kiga.idea.member.web.responses.model.{FileDTO, MemberIdeaDTO}
import com.kiga.ideas.member.domain.MemberIdea
import com.kiga.ideas.member.repository.MemberIdeaRepository
import com.kiga.security.services.SecurityService
import com.kiga.types.JLong
import com.kiga.upload.factory.UploadFactory
import org.springframework.web.multipart.MultipartFile

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer

@Named
class MemberIdeaService {
  @Inject var memberIdeaRepo: MemberIdeaRepository = _
  @Inject var memberIdeaCategoryService: MemberIdeaCategoryService = _
  @Inject var securityService: SecurityService = _

  def list(): List[MemberIdeaDTO] = {

    val memberIdeaList = memberIdeaRepo.findAll().asScala.toList
    var newMemberIdeaList = new ListBuffer[MemberIdeaDTO]()
    memberIdeaList foreach { case memberIdea =>
      newMemberIdeaList += MemberIdeaDTO.toMemberIdeaDTO(memberIdea)
    }
    newMemberIdeaList.toList
  }

  def get(id: JLong): MemberIdeaDTO ={
    val idea = memberIdeaRepo.findOne(id)
    MemberIdeaDTO.toMemberIdeaDTO(idea)
  }

  def saveMemberIdea(memberIdeaRequest: MemberIdeaRequest): MemberIdeaResponse = {

    var memberIdea : MemberIdea = null
    if(memberIdeaRequest.id != null){
      memberIdea = memberIdeaRepo.findOne(memberIdeaRequest.id)
    } else {
      memberIdea = new MemberIdea()
    }

    memberIdea.setTitle(memberIdeaRequest.title)
    memberIdea.setDescription(memberIdeaRequest.description)
    var previewSummaryVar : Any= null

    // there may not be updated preview file,
    // so old previewId will not changed
    if(memberIdeaRequest.preview != null){
      // so new preview is added, so previewId must be updated
      val previewSummary = uploadFile(memberIdeaRequest.preview)
      previewSummary match {
        case error: MemberIdeaResponse =>
          return error
        case success: FileDTO =>
          memberIdea.setPreviewId(success.getId)
      }
      previewSummaryVar = previewSummary.asInstanceOf[FileDTO]
    }

    if (memberIdeaRequest.contentType == "text") {
      // contentType may have changed to text
      // so, the fileId must be null
      memberIdea.setFileId(null)
      memberIdea.setContent(memberIdeaRequest.content)
    }

    memberIdea.setIdeaCategory(memberIdeaCategoryService.findOne(memberIdeaRequest.categoryId))
    memberIdea.setContentType(memberIdeaRequest.contentType)

    val fileSummary = null
    if (memberIdeaRequest.contentType == "file" && memberIdeaRequest.file != null) {
      // new file is there, so fileId must be updated
      val fileSummary = uploadFile(memberIdeaRequest.file)
      fileSummary match {
        case error: MemberIdeaResponse =>
          return error
        case success: FileDTO =>
          memberIdea.setFileId(success.getId)
      }
    }

    memberIdeaRepo.save(memberIdea)
    new MemberIdeaResponse(MemberIdeaDTO.toMemberIdeaDTO(memberIdea), fileSummary.asInstanceOf[FileDTO], previewSummaryVar.asInstanceOf[FileDTO])
  }

  def uploadFile(file: MultipartFile) = {
    val validatedFile = validateFile(file)
    if (validatedFile != null) {
      new MemberIdeaResponse(null, null, validatedFile)
    }
    processSingleFile(file)
  }

  def validateFile(file: MultipartFile): FileDTO = {
    if (file.isEmpty) {
      return FileDTO.toFileDTO("error", "Uploaded file was empty")
    }
    null
  }

  @Inject
  var uploadFactory: UploadFactory = _

  private def processSingleFile(file: MultipartFile): Any = {

    try {
      val fileUploader = uploadFactory.getUploaderInstance(file, null)// TODO get real member
      fileUploader.upload

      val path: Path = Paths.get(fileUploader.getOriginalFile.getAbsolutePath)
      val contentType = Files.probeContentType(path)

      FileDTO.toFileDTO("OK", fileUploader, file, contentType)
    } catch {
      case e: IllegalArgumentException => FileDTO.toFileDTO("error", "Type of uploaded file wasn't recognized")
      case e: Exception => {
        e.printStackTrace()
        FileDTO.toFileDTO("error", "Unexpected exception occurred")
      }
    }
  }

}
