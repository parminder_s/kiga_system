package com.kiga.idea.member.provider

import java.nio.file.{Files, Path, Paths}
import javax.inject.{Inject, Named}

import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.{GetObjectRequest, S3Object}
import com.amazonaws.{AmazonClientException, AmazonServiceException}
import com.kiga.ideas.member.repository.MemberIdeaRepository
import com.kiga.integration.amazon.s3.config.AmazonS3Parameters
import com.kiga.s3.domain.S3File
import com.kiga.s3.repository.ss.S3FileRepository
import com.kiga.types.JLong
import com.kiga.upload.factory.UploadFactory
import org.springframework.web.multipart.MultipartFile

@Named
class MemberIdeaFileService {

//  @Inject val logger = LoggerFactory.getLogger(classOf[MemberIdeaFileService])
  @Inject var s3Config: AmazonS3Parameters = _
  @Inject var memberIdeaRepo: MemberIdeaRepository = _
  @Inject var memberIdeaCategoryService: MemberIdeaCategoryService = _
  @Inject var s3FileRepository: S3FileRepository = _
  @Inject var uploadFactory: UploadFactory = _

  def downloadFile(fileId: JLong) = {
    val s3File = s3FileRepository.findOne(fileId)
    // check needed if s3File exist or not
    val downloadedFile = downloadFromAmazon(s3File)
    downloadedFile
  }


  protected def getAmazonS3Client = {
    val awsCredentials: BasicAWSCredentials = new BasicAWSCredentials(s3Config.getAccessKey, s3Config.getSecretKey)
    val amazonS3Client = new AmazonS3Client(awsCredentials)
    amazonS3Client
  }

  final protected def downloadFromAmazon(file: S3File): S3Object = {
    try {
      val fileDownloaded = getAmazonS3Client.getObject(
        new GetObjectRequest(s3Config.getBucketName, file.getName))
      fileDownloaded
    } catch {
      case e: AmazonClientException => {
        throw e
      }
      case e: AmazonServiceException => {
        throw e
      }
    }
  }


  private def processSingleFile(file: MultipartFile): Map[String, Any] = {

    try {
      val fileUploader = uploadFactory.getUploaderInstance(file, null)// TODO get real member
      fileUploader.upload

      val path: Path = Paths.get(fileUploader.getOriginalFile.getAbsolutePath)
      val contentType = Files.probeContentType(path)

      Map(
        "status" -> "OK",
        "name" -> fileUploader.getOriginalFile.getName,
        "size" -> file.getSize,
        "thumbnailUrl" -> fileUploader.getFileUrl,
        "type" -> contentType,
        "id" -> fileUploader.getSavedEntity.getId,
        "created" -> fileUploader.getSavedEntity.getCreated,
        "url" -> fileUploader.getFileUrl
      )
    } catch {
      case e: IllegalArgumentException => Map("status" -> "error", "message" -> "Type of uploaded file wasn't recognized")
      case e: Exception => {
        e.printStackTrace()
        Map("status" -> "error", "message" -> "Unexpected exception occurred")
      }
    }
  }

}
