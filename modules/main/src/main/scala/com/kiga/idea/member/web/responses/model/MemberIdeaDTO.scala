package com.kiga.idea.member.web.responses.model

import com.kiga.idea.member.web.responses.MemberCategoryResponse
import com.kiga.ideas.member.domain.MemberIdea
import com.kiga.types.JLong

import scala.beans.BeanProperty

/**
  * Created by aynul on 2/29/16.
  */
class MemberIdeaDTO() {

  @BeanProperty var id: JLong = _
  @BeanProperty var title: String = _
  @BeanProperty var description: String = _
  @BeanProperty var fileId: JLong = _
  @BeanProperty var previewId: JLong = _
  @BeanProperty var content: String = _
  @BeanProperty var category: MemberCategoryResponse= _
  @BeanProperty var contentType: String = _

}

object MemberIdeaDTO {

  def toMemberIdeaDTO(memberIdea: MemberIdea): MemberIdeaDTO = {
    val returner = new MemberIdeaDTO()
    returner.id = memberIdea.getId
    returner.title = memberIdea.getTitle
    returner.description = memberIdea.getDescription
    if(memberIdea.getFileId != null){
      returner.setFileId(memberIdea.getFileId)
    }
    returner.previewId = memberIdea.getPreviewId
    if(memberIdea.getContent != null){
      returner.content = memberIdea.getContent
    }
    returner.category = new MemberCategoryResponse(memberIdea.getIdeaCategory.getId,
      memberIdea.getIdeaCategory.getTitle, memberIdea.getIdeaCategory.getParent.getId)
    returner.contentType = memberIdea.getContentType
    returner
  }

}
