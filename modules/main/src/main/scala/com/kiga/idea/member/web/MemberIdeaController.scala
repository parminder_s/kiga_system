package com.kiga.idea.member.web

import java.io.InputStream
import javax.inject.Inject
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import com.kiga.idea.member.provider.{MemberIdeaFileService, MemberIdeaService}
import com.kiga.idea.member.web.request.MemberIdeaRequest
import com.kiga.s3.repository.ss.S3FileRepository
import com.kiga.security.services.SecurityService
import com.kiga.types.JLong
import org.apache.commons.io.IOUtils
//import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation._
import org.springframework.web.multipart.MultipartFile

@Controller
class MemberIdeaController {
  @Inject var securityService: SecurityService = _
  @Inject var memberIdeaService: MemberIdeaService = _
  @Inject var s3FileRepository: S3FileRepository= _
  @Inject var memberIdeaFileService : MemberIdeaFileService = _
  @RequestMapping(
    value = Array("idea/member/list"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def list() = {
    securityService.requiresLogin()
    memberIdeaService.list()
  }

  @RequestMapping(
    value = Array("idea/member/get"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def get(@RequestParam("id") id: JLong) = {
    securityService.requiresLogin()
    memberIdeaService.get(id)
  }

  @RequestMapping(
    value = Array("/api/idea/member/file"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def filelist() = {
    securityService.requiresLogin()
    val x = s3FileRepository.findAll()
    x
  }

  @RequestMapping(
    value = Array("/api/idea/member/display/{fileId}"),
    method = Array(RequestMethod.GET)
  )
  def display( @PathVariable("fileId") fileId : JLong, response : HttpServletResponse) {
    securityService.requiresLogin()
    val file = memberIdeaFileService.downloadFile(fileId)
    response.setContentType(file.getObjectMetadata.getContentType)
    response.setContentLengthLong(file.getObjectMetadata.getContentLength)
    val is : InputStream = file.getObjectContent
    IOUtils.copy(is, response.getOutputStream())
    response.flushBuffer()
  }

  @RequestMapping(
    value = Array("/api/idea/member/download/{fileId}"),
    method = Array(RequestMethod.GET)
  )
  def download( @PathVariable("fileId") fileId : JLong, response : HttpServletResponse) {
    securityService.requiresLogin()
    val file = memberIdeaFileService.downloadFile(fileId)
    response.setContentType(file.getObjectMetadata.getContentType)
    response.setContentLengthLong(file.getObjectMetadata.getContentLength)
    response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getKey))
    val is : InputStream = file.getObjectContent
    IOUtils.copy(is, response.getOutputStream())
    response.flushBuffer()
  }


  @RequestMapping(
    value = Array("member/idea/update", "member/idea/save", "memberidea/save"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def saveMemberIdea
  (
    @RequestParam(required = false, value = "file") file: MultipartFile,
    @RequestParam(required = false, value = "preview") preview: MultipartFile,
    @RequestParam(required = false, value = "id") id: JLong,
    @RequestParam("categoryId") categoryId: JLong,
    @RequestParam("content") content: String,
    @RequestParam("contentType") contentType: String,
    @RequestParam("title") title: String,
    @RequestParam("description") description: String) = {
    // memberIdea
    securityService.requiresLogin()

    if(content == null && file == null){
      throw new RuntimeException("File or Content is requierd")
    }

    val memberIdeaRequest = new MemberIdeaRequest()
    memberIdeaRequest.id = id
    memberIdeaRequest.title = title
    memberIdeaRequest.description = description
    memberIdeaRequest.content = content
    memberIdeaRequest.contentType = contentType
    memberIdeaRequest.categoryId = categoryId
    memberIdeaRequest.file = file
    memberIdeaRequest.preview = preview
    memberIdeaService.saveMemberIdea(memberIdeaRequest)
  }

}
