package com.kiga.idea.member.web.responses

case class MemberCategoryResponse
(
  var id: Long,
  var title: String,
  var parentId: Any

)
