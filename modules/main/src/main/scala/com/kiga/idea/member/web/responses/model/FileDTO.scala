package com.kiga.idea.member.web.responses.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.kiga.upload.factory.AbstractFileUploader
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.springframework.web.multipart.MultipartFile

import scala.beans.BeanProperty

/**
  * Created by aynul on 3/1/16.
  */
class FileDTO () {

    @BeanProperty var id: Long = _
    @BeanProperty var name: String = _
    @BeanProperty var size: Long = _
    @BeanProperty var url: String = _
    @BeanProperty var thumbnailUrl: String = _
    @BeanProperty var contentType: String = _
    @BeanProperty var createdOn: String = _
    @BeanProperty var status: String = _
    @BeanProperty var message: String = _

}

//@JsonIgnore val dateTimeFormat = DateTimeFormat.forPattern("dd.MM.yyyy");

object FileDTO {

  def toFileDTO(status: String, message: String): FileDTO ={
    val returner = new FileDTO()
    returner.status = status
    returner.message = message
    returner
  }

  def toFileDTO(status: String, fileUploader: AbstractFileUploader, file: MultipartFile, contentType: String): FileDTO = {

    val returner = new FileDTO()
    returner.status = status
    returner.id = fileUploader.getSavedEntity.getId
    returner.name = fileUploader.getOriginalFile.getName
    returner.size = file.getSize
    returner.url = fileUploader.getFileUrl
    returner.thumbnailUrl = fileUploader.getFileUrl
    returner.contentType = contentType
    returner.createdOn = DateTimeFormat.forPattern("dd.MM.yyyy")
      .print(new DateTime(fileUploader.getSavedEntity.getCreated))

    returner
  }
}
