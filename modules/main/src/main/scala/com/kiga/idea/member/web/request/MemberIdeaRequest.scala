package com.kiga.idea.member.web.request

import com.kiga.types.JLong
import com.kiga.web.message.EndpointRequest
import org.springframework.web.multipart.MultipartFile

import scala.beans.BeanProperty


class MemberIdeaRequest extends EndpointRequest {
  @BeanProperty var title: String = _
  @BeanProperty var description: String = _
  @BeanProperty var content: String = _
  @BeanProperty var contentType: String = _
  @BeanProperty var file: MultipartFile = _
  @BeanProperty var preview: MultipartFile = _
  @BeanProperty var categoryId: JLong = _
  @BeanProperty var id: JLong = _
}
