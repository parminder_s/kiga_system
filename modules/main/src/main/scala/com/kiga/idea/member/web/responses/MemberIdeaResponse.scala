package com.kiga.idea.member.web.responses

import com.kiga.idea.member.web.responses.model.{FileDTO, MemberIdeaDTO}

case class MemberIdeaResponse(memberIdea: MemberIdeaDTO, file: FileDTO, preview: FileDTO)
