package com.kiga.idea.member.web

import javax.inject.Inject

import com.kiga.idea.member.provider.MemberIdeaCategoryService
import com.kiga.idea.member.web.responses.MemberCategoryResponse
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation._
import scala.collection.JavaConverters._

@Controller
class MemberIdeaCategoryController {

  @Inject var memberIdeaCategoryService: MemberIdeaCategoryService = _

  @RequestMapping(
    value = Array("/idea/member/category/list"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def list(): List[MemberCategoryResponse] = {
    val logger = LoggerFactory.getLogger(getClass)
    memberIdeaCategoryService
      .list().asScala.toList.map(category => {
        MemberCategoryResponse(category.getId, category.getTitle(), category.getParent().getId)
      })
  }
}
