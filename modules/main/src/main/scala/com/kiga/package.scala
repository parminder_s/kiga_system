package com

/**
 * Created by gonzaur on 4/21/15
 */
package object kiga {
  object types {

    type JList[T] = java.util.List[T]
    type JArrayList[T] = java.util.ArrayList[T]
    type JIterator[T] = java.util.Iterator[T]
    type JMap[K, V] = java.util.Map[K, V]
    type JHashMap[K, V] = java.util.HashMap[K, V]
    type JSet[T] = java.util.Set[T]

    type JBoolean = java.lang.Boolean
    type JLong = java.lang.Long
    type JInt = java.lang.Integer
    type JDouble = java.lang.Double
    type JFloat = java.lang.Float

    type JBigDecimal = java.math.BigDecimal
    type JBigInteger = java.math.BigInteger

    type JCalendar = java.util.Calendar
    type JDate = java.util.Date

    type JURL = java.net.URL

  }
}
