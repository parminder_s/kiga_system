package com.kiga.db.fixture

import com.kiga.InvalidConfigurationException
import com.kiga.db.DdlRule
import com.kiga.newsletter.backend.domain.zend.Recipient
import javax.inject.{Inject, Named}
import javax.persistence.{EntityManager, PersistenceContext}
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.transaction.annotation.Transactional

import scala.beans.BeanProperty

/**
  */
@Transactional
@Named
@ConfigurationProperties("datasource.zend")
class ZendFixtureLoader extends FixtureLoader with ApplicationListener[ContextRefreshedEvent] {
  @BeanProperty var loadFixtures: Boolean = _
  @BeanProperty var jdbcUrl: String = _

  @PersistenceContext(unitName = "zend") var em: EntityManager = _

  var persister: FixturePersister = _
  @Inject var parser: FixtureParser = _

  val logger = LoggerFactory.getLogger(classOf[FixtureLoader])

  def onApplicationEvent(e: ContextRefreshedEvent): Unit = {
    if (loadFixtures) {
      if (DdlRule.allowDdl(jdbcUrl)) {
        persister = new DefaultFixturePersister(em)
        logger.info("hsqldb, clearing fixtures!")
        clearFixtures()
        loadFixturesFromFiles()
      } else {
        throw new InvalidConfigurationException("Fixtures can only be loaded to hsqldb!")
      }
    }
  }

  def resetPersister(): Unit = {
    if (loadFixtures && DdlRule.allowDdl(jdbcUrl)) {
      persister = new DefaultFixturePersister(em)
    }
  }

  def clearFixtures() = {
    for (table <- List[String]()) {
      logger.info("Removed " + em.createQuery(s"Delete from $table").executeUpdate() + s" from $table")
    }
  }

  def loadFixturesFromFiles(): Unit = {
    if (loadFixtures) {

      /* Newsletter */
      parser.loadFixtureFromFile[Recipient](classOf[Recipient]).foreach(persist)

      logger.info("Fixtures (zend) have been added")
    }
  }

  def persist[T <: Entity](parserResult: ParserResult[T]) = {
    persister.persist(parserResult)
  }
}
