package com.kiga.db

import org.slf4j.LoggerFactory
import org.springframework.boot.orm.jpa.hibernate.SpringNamingStrategy

/**
 * Created by rainerh on 21.11.15.
 */
class KigaNamingStrategy extends SpringNamingStrategy {
  var logger = LoggerFactory.getLogger(classOf[KigaNamingStrategy])

  override def tableName(tableName: String) = tableName

  override def columnName(columnName: String) = columnName

  override def classToTableName(className: String) = {
    className.capitalize
  }

  override def foreignKeyColumnName(propertyName: String, propertyEntityName: String,
                                    propertyTableName: String, referencedColumnName: String) = {
    propertyName + "Id"
  }

  override def propertyToColumnName(columnName: String) = {
    if (columnName.toLowerCase.equals("id")) {
      "ID"
    }
    else {
      "Id$".r.replaceFirstIn(columnName, "ID").capitalize
    }
  }
}
