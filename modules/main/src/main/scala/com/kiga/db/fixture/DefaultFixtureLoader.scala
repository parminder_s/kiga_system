package com.kiga.db.fixture

import com.kiga.InvalidConfigurationException
import com.kiga.accounting.domain.{AcquirerTransaction, Invoice}
import com.kiga.accounting.dunning.domain.DunningLevel
import com.kiga.accounting.transactions.domain.PaymentTransaction
import com.kiga.content.domain.ss.KigaPageImage
import com.kiga.content.domain.ss.live._
import com.kiga.db.DdlRule
import com.kiga.favourites.domain.IdeaFavourite
import com.kiga.forum.domain.{Forum, ForumThread}
import com.kiga.homepage.domain.live.HomePageElementLive
import com.kiga.ideas.member.domain.MemberIdea
import com.kiga.kga.domain._
import com.kiga.kga.survey.domain.{Question, Section, Survey}
import com.kiga.payment.domain.Payment
import com.kiga.s3.domain.{KigaS3File, S3Image}
import com.kiga.security.domain.{Member, Role, RoleMember}
import com.kiga.shop.domain._
import com.kiga.shopcontent.domain.live.{ProductPageLive, PromotionLive, ShopCategoryLive, SliderFileLive}
import com.kiga.survey.domain.Questionnaire
import com.kiga.translation.domain._
import javax.inject.{Inject, Named}
import javax.persistence.{EntityManager, PersistenceContext}
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.transaction.annotation.Transactional

import scala.beans.BeanProperty


/**
  * Created by rainerh on 13.04.15.
  */
@Transactional
@Named
@ConfigurationProperties("datasource.ss")
class DefaultFixtureLoader extends FixtureLoader with ApplicationListener[ContextRefreshedEvent] {
  val logger = LoggerFactory.getLogger(classOf[FixtureLoader])
  @BeanProperty var loadFixtures: Boolean = _
  @BeanProperty var jdbcUrl: String = _
  @PersistenceContext(unitName = "ss") var em: EntityManager = _
  var persister: FixturePersister = _
  @Inject var parser: FixtureParser = _

  def onApplicationEvent(e: ContextRefreshedEvent): Unit = {
    if (loadFixtures) {
      if (DdlRule.allowDdl(jdbcUrl)) {
        persister = new DefaultFixturePersister(em)
        logger.info("hsqldb, clearing fixtures!")
        clearFixtures()
        loadFixturesFromFiles()
      } else {
        throw new InvalidConfigurationException("Fixtures can only be loaded to hsqldb!")
      }
    }
  }

  def clearFixtures() = {
    for (table <- List[String]("Member", "Role", "RoleMember", "CustomLanguageTranslation",
      "CustomLanguageTranslationHistory", "SiteTreeLive",
      "CustomLanguageTranslationComment", "IdeaGroupToIdeaLive", "KigaIdeaLive",
      "IdeaGroupLive", "IdeaGroupContainerLive", "IdeaCategoryLive",
      "LinkedKigaPageGroupLive", "LinkedKigaPageGroupToKigaPageLive", "Questionnaire",
      "Purchase", "PurchaseItem", "PromotionLive")) {
      logger.debug("Removed " + em.createQuery(s"Delete from $table").executeUpdate() + s" from $table")
    }
  }

  def loadFixturesFromFiles(): Unit = {
    if (loadFixtures) {
      logger.info("loading Fixtures ...")

      /* kga */
      parser.loadFixtureFromFile[Language](classOf[Language], filename = "Language").foreach(persist)
      parser.loadFixtureFromFile[KgaKindergarten](classOf[KgaKindergarten], "KgaKindergarten").foreach(persist)
      parser.loadFixtureFromFile[KgaYear](classOf[KgaYear], "KgaYear").foreach(persist)
      parser.loadFixtureFromFile[KgaGroup](classOf[KgaGroup], "KgaGroup").foreach(persist)
      parser.loadFixtureFromFile[KgaChild](classOf[KgaChild], "KgaChild").foreach(persist)
      parser.loadFixtureFromFile[KgaChild_Versions](classOf[KgaChild_Versions], "KgaChild_Versions").foreach(persist)
      parser.loadFixtureFromFile[KgaParent](classOf[KgaParent], "KgaParent").foreach(persist)
      parser.loadFixtureFromFile[KgaParentToChild](classOf[KgaParentToChild], "KgaParentToChild").foreach(persist)
      parser.loadFixtureFromFile[ChildInKgaGroup](classOf[ChildInKgaGroup], "ChildInKgaGroup").foreach(persist)

      /* kga survey */
      parser.loadFixtureFromFile[Survey](classOf[Survey], "Survey").foreach(persist)
      parser.loadFixtureFromFile[Section](classOf[Section], "Section").foreach(persist)
      parser.loadFixtureFromFile[Question](classOf[Question], "Question").foreach(persist)


      /* default content */
      parser.loadFixtureFromFile[SiteTreeLive](classOf[SiteTreeLive], "SiteTree").foreach(persist)
      parser.loadFixtureFromFile[IdeaCategoryLive](classOf[IdeaCategoryLive], "IdeaCategory").foreach(persist)
      parser.loadFixtureFromFile[KigaIdeaLive](classOf[KigaIdeaLive], "KigaIdea").foreach(persist)

      /* idea groups */
      parser.loadFixtureFromFile[IdeaGroupContainerLive](classOf[IdeaGroupContainerLive], "IdeaGroupContainer").foreach(persist)
      parser.loadFixtureFromFile[IdeaGroupLive](classOf[IdeaGroupLive], "IdeaGroup").foreach(persist)
      parser.loadFixtureFromFile[IdeaGroupToIdeaLive](classOf[IdeaGroupToIdeaLive], "IdeaGroupToIdea").foreach(persist)

      parser.loadFixtureFromFile[LinkedKigaPageGroupLive](classOf[LinkedKigaPageGroupLive], "LinkedKigaPageGroup").foreach(persist)
      parser.loadFixtureFromFile[LinkedKigaPageGroupToKigaPageLive](classOf[LinkedKigaPageGroupToKigaPageLive], "LinkedKigaPageGroupToKigaPage").foreach(persist)

      parser.loadFixtureFromFile[ShopCategoryLive](classOf[ShopCategoryLive], "ShopCategory").foreach(persist)
      parser.loadFixtureFromFile[ProductPageLive](classOf[ProductPageLive], "ProductPage").foreach(persist)
      parser.loadFixtureFromFile[SliderFileLive](classOf[SliderFileLive], "SliderFile").foreach(persist)
      parser.loadFixtureFromFile[Product](classOf[Product], "Product").foreach(persist)
      parser.loadFixtureFromFile[ProductDetail](classOf[ProductDetail], "ProductDetail").foreach(persist)
      parser.loadFixtureFromFile[CountryGroup](classOf[CountryGroup], "CountryGroup").foreach(persist)
      parser.loadFixtureFromFile[Country](classOf[Country], "Country").foreach(persist)
      parser.loadFixtureFromFile[CountryShop](classOf[CountryShop], "CountryShop").foreach(persist)
      parser.loadFixtureFromFile[CountryLocale](classOf[CountryLocale], "CountryLocale").foreach(persist)
      parser.loadFixtureFromFile[PromotionLive](classOf[PromotionLive], "Promotion").foreach(persist)

      parser.loadFixtureFromFile[DunningLevel](classOf[DunningLevel], "DunningLevel").foreach(persist)
      parser.loadFixtureFromFile[Invoice](classOf[Invoice], "Invoice").foreach(persist)
      parser.loadFixtureFromFile[PaymentTransaction](classOf[PaymentTransaction], "PaymentTransaction").foreach(persist)

      /* downloads */
      parser.loadFixtureFromFile[S3Image](classOf[S3Image]).foreach(persist)
      parser.loadFixtureFromFile[KigaS3File](classOf[KigaS3File]).foreach(persist)
      parser.loadFixtureFromFile[KigaPageImage](classOf[KigaPageImage]).foreach(persist)
      parser.loadFixtureFromFile[KigaPrintPreviewImageLive](classOf[KigaPrintPreviewImageLive], "KigaPrintPreviewImage").foreach(persist)

      /* HomePagePreview */
      parser.loadFixtureFromFile[HomePageElementLive](classOf[HomePageElementLive]).foreach(persist)


      /* Forum */
      parser.loadFixtureFromFile[Forum](classOf[Forum]).foreach(persist)
      parser.loadFixtureFromFile[ForumThread](classOf[ForumThread]).foreach(persist)
      //parser.loadFixtureFromFile[Post](classOf[Post]).foreach(persist);

      parser.loadFixtureFromFile[Member](classOf[Member]).foreach(persist)
      parser.loadFixtureFromFile[Role](classOf[Role]).foreach(persist)
      parser.loadFixtureFromFile[RoleMember](classOf[RoleMember]).foreach(persist)

      parser.loadFixtureFromFile[CustomLanguageTranslation](classOf[CustomLanguageTranslation]).foreach(persist)
      parser.loadFixtureFromFile[CustomLanguageTranslationComment](classOf[CustomLanguageTranslationComment]).foreach(persist)
      parser.loadFixtureFromFile[CustomLanguageTranslationHistory](classOf[CustomLanguageTranslationHistory]).foreach(persist)

      parser.loadFixtureFromFile[MemberIdea](classOf[MemberIdea]).foreach(persist)

      parser.loadFixtureFromFile[IdeaFavourite](classOf[IdeaFavourite], "IdeaFavourite").foreach(persist)

      parser.loadFixtureFromFile[Purchase](classOf[Purchase]).foreach(persist)
      //      parser.loadFixtureFromFile[PurchaseItem](classOf[PurchaseItem]).foreach(persist)
      parser.loadFixtureFromFile[Payment](classOf[Payment]).foreach(persist)
      parser.loadFixtureFromFile[AcquirerTransaction](classOf[AcquirerTransaction]).foreach(persist)
      parser.loadFixtureFromFile[Questionnaire](classOf[Questionnaire]).foreach(persist)
      persister.persistEntityJoinsByKey()

      logger.info("Fixtures have been added")
    }
  }

  def persist[T <: Entity](parserResult: ParserResult[T]) = {
    persister.persist(parserResult)
  }

  def resetPersister(): Unit = {
    if (loadFixtures && DdlRule.allowDdl(jdbcUrl)) {
      persister = new DefaultFixturePersister(em)
    }
  }
}
