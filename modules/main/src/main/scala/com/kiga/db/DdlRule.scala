package com.kiga.db
;

/**
 * Created by rainerh on 18.11.15.
 */

object DdlRule {
  def allowDdl(jdbc: String) = {
    isHsqlDb(jdbc)
  }

  def isHsqlDb(jdbc: String) = {
    "hsqldb".r.findFirstIn(jdbc).isDefined
  }

  def isMysqlDb(jdbc: String) = {
    "mysql".r.findFirstIn(jdbc).isDefined
  }
}
