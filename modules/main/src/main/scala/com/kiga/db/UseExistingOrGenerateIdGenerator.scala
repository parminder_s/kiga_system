package com.kiga.db

import org.hibernate.engine.spi.SessionImplementor
import org.hibernate.id.IdentityGenerator
import org.slf4j.LoggerFactory

/**
  * Created by rainerh on 06.12.15.
  *
  * For fixture loading we are working with preset id values which is
  * different to the default case where the primary keys are auto-generated
  * by the database.
  *
  * This Generator checks if the id is set and uses that value otherwise
  * it falls back to JPA's default.
  *
  * Currently Hibernate has a bug that prevent us from using that:
  * https://hibernate.atlassian.net/browse/HHH-7053
  *
  * Credit goes to http://stackoverflow.com/questions/3194721/
  */
class UseExistingOrGenerateIdGenerator extends IdentityGenerator {
  override def generate(session: SessionImplementor, obj: Object): java.io.Serializable = {
    val id = session.getEntityPersister(null, obj).getClassMetadata.getIdentifier(obj, session)
    var returner = id

    if (id == null) {
      returner = super.generate(session, obj)
    }
    LoggerFactory.getLogger(getClass).info("returning new id here: " + returner.toString)
    returner
  }
}
