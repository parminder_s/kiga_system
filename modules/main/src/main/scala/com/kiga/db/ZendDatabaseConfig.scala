package com.kiga.db

import javax.inject.Inject
import javax.sql.DataSource

import com.kiga.InvalidConfigurationException
import com.kiga.accounting.domain.zend.PaymentStatusExchange
import com.kiga.accounting.repository.zend.PaymentStatusExchangeRepository
import com.kiga.newsletter.backend.domain.zend.Recipient
import com.kiga.newsletter.backend.repository.{NewsletterRepository, RecipientRepository, RecipientSelectionRepository}
import com.zaxxer.hikari.HikariDataSource
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.{Autowired, Qualifier}
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.core.env.Environment
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.{JpaTransactionManager, LocalContainerEntityManagerFactoryBean}

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

/**
  * Created by rainerh on 27.01.15.
  */
@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories(
  basePackageClasses = Array(
    classOf[PaymentStatusExchangeRepository]
    , classOf[RecipientRepository]
    , classOf[NewsletterRepository]
    , classOf[RecipientSelectionRepository]
  ),
  entityManagerFactoryRef = "emfZend",
  transactionManagerRef = "zendTx")
@ConfigurationProperties("datasource.zend")
protected[db] class ZendDatabaseConfig {
  @BeanProperty var jdbcUrl: String = _
  @BeanProperty var username: String = _
  @BeanProperty var password: String = _
  val logger = LoggerFactory.getLogger(classOf[ZendDatabaseConfig])

  @BeanProperty var hbm2ddl: String = _

  @Inject var env: Environment = _

  @Bean(name = Array("dsZend"))
  def dataSource(): DataSource = {
    DataSourceBuilder
      .create()
      .`type`(classOf[HikariDataSource])
      .url(jdbcUrl)
      .username(username)
      .password(password)
      .build
  }

  @Bean(name = Array("emfZend"))
  def zendEmf(builder: EntityManagerFactoryBuilder): LocalContainerEntityManagerFactoryBean = {
    var actualHbm2Ddl = Option(hbm2ddl).getOrElse("validate")

    if (!DdlRule.allowDdl(jdbcUrl) && !List("validate", "none").contains(actualHbm2Ddl)) {
      throw new InvalidConfigurationException("Invalid configuration param: on non-hsqldb databases hbm2ddl property must be 'validate or none'. Current value is " + actualHbm2Ddl)
    }

    var dialect: String = ""
    if (DdlRule.isHsqlDb(jdbcUrl)) {
      dialect = "com.kiga.db.HsqlDialect"
    } else if (DdlRule.isMysqlDb(jdbcUrl)) {
      dialect = "com.kiga.db.MySqlDialect"
    } else {
      throw new InvalidConfigurationException("Invalid configuration param: No dialect found or unknown jdbc url: " + jdbcUrl)
    }

    builder
      .dataSource(dataSource())
      .properties(Map[String, String](
        "hibernate.hbm2ddl.auto" -> hbm2ddl,
        // "hibernate.ejb.naming_strategy" -> "com.kiga.db.KigaNamingStrategy",
        "hibernate.dialect" -> dialect
      ))
      .packages(
        classOf[PaymentStatusExchange]
        , classOf[Recipient]
      )
      .persistenceUnit("zend")
      .build()
  }

  @Bean(name = Array("zendTx"))
  def zendTex(@Autowired @Qualifier("emfZend") emf: LocalContainerEntityManagerFactoryBean) = {
    new JpaTransactionManager(emf.getObject)
  }
}
