package com.kiga.translation.web.controllers

import javax.inject.Inject

import com.kiga.translation.provider.CustomLanguageTranslationCommentService
import com.kiga.translation.web.controllers.request.TranslationCommentRequest
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestBody, RequestMapping, RequestMethod, ResponseBody}


/**
 *
 */


@Controller
class CommentController {
  @Inject var commentService: CustomLanguageTranslationCommentService = _

  @RequestMapping(
    value = Array("/api/translation/comment/get", "/api/translation/getComment"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def findByTranslationKey(@RequestBody commentRequest: TranslationCommentRequest) = {
    commentService.findByTranslationKey(commentRequest.translationKey)
  }

  //  @RequestMapping(
  //    value = Array("/api/comment/new", "/api/comment/save"),
  //    method = Array(RequestMethod.POST)
  //  )
  //  @ResponseBody
  //  def save(@RequestBody comment: Comment) = {
  //    commentService.save(comment)
  //  }
}
