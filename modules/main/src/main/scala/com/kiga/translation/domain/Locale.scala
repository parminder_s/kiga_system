package com.kiga.translation.domain

import scala.beans.BeanProperty

class Locale {

  @BeanProperty var title: String = _
  @BeanProperty var standard: String = _

  override def toString(): String = {
    title.toLowerCase() + "_" + standard.toUpperCase()
  }

}
