package com.kiga.translation.domain

import javax.persistence.Entity

import com.kiga.db.{KigaEntityModelWithoutId, KigaEntityModel}

import scala.beans.BeanProperty

@Entity class CustomLanguageTranslation extends KigaEntityModel {
  this.setClassName("CustomLanguageTranslation")

  @BeanProperty var entity: String = _

  @BeanProperty var locale: String = _

  @BeanProperty var translation: String = _

  @BeanProperty var priority: Int = _

  @BeanProperty var status: String = _
}
