package com.kiga.translation.web.controllers.request

import com.kiga.main.locale.Locale
import com.kiga.web.message.EndpointRequest

import scala.beans.BeanProperty

/**
 *
 */
class TranslationRequest {
  @BeanProperty var isTranslationEnabled: Boolean = false
  @BeanProperty var locale: Locale = _
}
