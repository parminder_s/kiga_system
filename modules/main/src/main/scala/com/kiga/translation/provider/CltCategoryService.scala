package com.kiga.translation.provider

import javax.inject.Named

@Named
class CltCategoryService {

  var allCategories = Map[String,List[String]]()

  def setKeyAndCategories(key:String, categories: List[String]) ={
    allCategories += (key -> categories)
  }

  def getCategoriesByKey(key:String) : List[String] ={
    allCategories.getOrElse(key, List[String]())
  }
}
