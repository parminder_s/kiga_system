package com.kiga.translation.provider.helper

import javax.inject.Named

import com.kiga.security.domain.Member
import com.kiga.translation.domain.CustomLanguageTranslationComment
import org.joda.time.DateTime

@Named
class CommentBuilder {

  def buildNew(body: String, key: String, user: Member, date: DateTime): CustomLanguageTranslationComment = {
    val comment = new CustomLanguageTranslationComment()
    comment.entity = key
    comment.user = user
    comment.body = body
    comment
  }

}
