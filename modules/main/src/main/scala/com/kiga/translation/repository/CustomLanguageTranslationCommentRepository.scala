package com.kiga.translation.repository

//import javax.persistence.Query

import com.kiga.translation.domain.CustomLanguageTranslationComment
import org.springframework.data.repository.CrudRepository

/**
  */
trait CustomLanguageTranslationCommentRepository extends CrudRepository[CustomLanguageTranslationComment, java.lang.Long] {
  def findAll(): java.util.List[CustomLanguageTranslationComment]

  def findByEntity(entity: String): java.util.List[CustomLanguageTranslationComment]
}
