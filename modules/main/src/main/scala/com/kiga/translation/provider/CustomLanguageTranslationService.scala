package com.kiga.translation.provider

import javax.inject.{Inject, Named}

import com.kiga.translation.domain.CustomLanguageTranslation
import com.kiga.translation.repository.CustomLanguageTranslationRepository

import scala.collection.JavaConverters._


/**
 *
 */
@Named class CustomLanguageTranslationService {
  @Inject var customTranslationRepository: CustomLanguageTranslationRepository = _
  @Inject var locales: LocaleService = _

  def getBaseTranslation(): List[CustomLanguageTranslation] = {
    customTranslationRepository.findAllByLocale(locales.getBaseLocale().toString()).asScala.toList
  }

  def getTranslation(localeStr: String): List[CustomLanguageTranslation] = {
    customTranslationRepository.findAllByLocale(localeStr).asScala.toList
  }

  def list(): List[CustomLanguageTranslation] = {
    customTranslationRepository.findAll().asScala.toList
  }

  def findByKeyAndLocale(key: String, locale: String): CustomLanguageTranslation = {
    customTranslationRepository.findByEntityAndLocale(key, locale)
  }

  def findByKeyAndNotLocale(key: String, locale: String): List[CustomLanguageTranslation] = {
    customTranslationRepository.findByEntityAndLocaleNot(key, locale).asScala.toList
  }

  def save(customTranslation: CustomLanguageTranslation) = {
    customTranslationRepository.save(customTranslation)
  }
}
