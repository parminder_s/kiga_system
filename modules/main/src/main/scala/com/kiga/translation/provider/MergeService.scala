package com.kiga.translation.provider

import javax.inject.{Inject, Named}

import com.kiga.translation.domain.CustomLanguageTranslation
import com.kiga.translation.provider.helper.{CltBuilder, TranslationHelper}

@Named class MergeService {

  def mergeKeys(jsonKeys:List[String], cltKeys: List[String]): List[String] = {
    (jsonKeys ::: cltKeys).distinct
  }
}
