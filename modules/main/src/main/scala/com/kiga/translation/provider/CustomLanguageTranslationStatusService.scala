package com.kiga.translation.provider

import javax.inject.Named

/**
 *
 */
@Named class CustomLanguageTranslationStatusService {

  val translated : String = "translated"
  val notTranslated : String = "not-translated"
  val changed: String = "changed"

  def getTranslatedStatus() : String = {
    translated
  }

  def getNotTranslatedStatus() : String = {
    notTranslated
  }

  def getChangedStatus() : String = {
    changed
  }

  def getPlainStatus() : List[String] = {
    List(translated , notTranslated, changed)
  }
}
