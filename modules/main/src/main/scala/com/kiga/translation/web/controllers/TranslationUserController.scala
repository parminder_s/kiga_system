package com.kiga.translation.web.controllers

import javax.inject.Inject

import com.kiga.security.repository.MemberRepository
import com.kiga.security.services.SecurityService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, ResponseBody}


/**
 *
 */

@Controller
class TranslationUserController {
  @Inject var memberRepo: MemberRepository = _

  @RequestMapping(
    value = Array("/api/user/list", "/api/user"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def list = {
    memberRepo.findCmsMember()
  }
}
