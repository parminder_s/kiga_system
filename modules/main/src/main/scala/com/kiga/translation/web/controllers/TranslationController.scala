package com.kiga.translation.web.controllers

import javax.inject.Inject

import com.kiga.translation.provider._
import com.kiga.translation.web.controllers.request.{TranslationCommentRequest, TranslationUpdateRequest}
import org.springframework.web.bind.annotation._

/**
 *
 */

@RestController
class TranslationController {
  @Inject var translationService: TranslationService = _
  @Inject var localeService : LocaleService = _
  @Inject var customTranslationService: CustomLanguageTranslationService = _
  @Inject var statusService: CustomLanguageTranslationStatusService = _
  @Inject var categoryService: CustomLanguageTranslationCategoryService = _
  @Inject var commentService: CustomLanguageTranslationCommentService = _


  @RequestMapping(
    value = Array("/api/translation/findWithMetaData"),
    method = Array(RequestMethod.GET)
  )
  @ResponseBody
  def findWithMetaData(@RequestParam() locale: String,
                       @RequestParam() isTranslationEnabled: Boolean) {
    if(isTranslationEnabled) {
      val returner = translationService.mergeAllTDMForAllLocales(locale)
      returner
    }
    else {
      translationService.getPlainJson(locale).toMap
    }
  }

  @RequestMapping(
    value = Array("/api/translation/listAll"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def list = customTranslationService.list()

  @RequestMapping(
    value = Array("/api/translation/update"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def update(@RequestBody translationRequest: TranslationUpdateRequest) = {
    translationService.save(translationRequest.word)
  }

  @RequestMapping(
    value = Array("/api/translation/checkRedundant"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def checkRedundant(@RequestBody translationRequest: TranslationUpdateRequest) = {
    translationService.checkRedundant(translationRequest.getLocale.toString, translationRequest.word)
  }

  @RequestMapping(
    value = Array("/api/translation/getWord"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def getWord(@RequestBody translationRequest: TranslationCommentRequest) = {
    translationService.getWord(translationRequest.translationKey, translationRequest.currentLocale)
  }

  @RequestMapping(
    value = Array("/api/translation/getTranslationStatus"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def getStatus = {
    statusService.getPlainStatus()
  }

  @RequestMapping(
    value = Array("/api/translation/getLocale"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def getLocale = localeService.getPlainLocales()

  @RequestMapping(
    value = Array("/api/translation/getSiblings"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def getCategories = categoryService.getCategories

}
