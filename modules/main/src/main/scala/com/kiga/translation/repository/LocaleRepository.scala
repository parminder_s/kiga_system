package com.kiga.translation.repository

import javax.inject.Named

import com.kiga.translation.domain.Locale
import com.kiga.types._
import org.springframework.boot.context.properties.ConfigurationProperties

import scala.beans.BeanProperty
import scala.collection.JavaConverters._


/**
  */
@Named
@ConfigurationProperties("translation")
class LocaleRepository {

  @BeanProperty var locales: JMap[String, String] = _
  @BeanProperty var baseLocale: String = _
  @BeanProperty var defaultLocale: String = _

  def getBase() = {
    findByTitle(baseLocale)
  }

  def getDefault() = {
    findByTitle(defaultLocale)
  }

  def buildLocales(title: String, standard: String) = {
    val locale = new Locale()
    locale.title = title
    locale.standard = standard
    locale
  }

  def findAll(): List[Locale] = {
    locales.asScala.map({
      case (key, value) => buildLocales(key, value)
    }).toList
  }

  def findByTitle(title: String) = {
    val allLocales = findAll()
    val matched = allLocales.find(locale => locale.title.equals(title))
    matched.get
  }
}
