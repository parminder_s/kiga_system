package com.kiga.translation.provider

import javax.inject.{Inject, Named}

import com.kiga.translation.provider.helper.CltBuilder
import net.liftweb.json._

import scala.collection.immutable.ListMap

/**
 */
@Named class JsonDataTransformService {
  implicit val formats = net.liftweb.json.DefaultFormats
  @Inject var cltData: CltBuilder = _
  @Inject var cltCategoryService: CltCategoryService = _

  def transformAsPlain(data : List[JField]) : Map[String,String] = {
    t_transform(data, false).asInstanceOf[Map[String,String]]
  }

  def transformAsTDM(data : List[JField]) : Map[String, TranslationDataModel] = {
    t_transform(data, true).asInstanceOf[Map[String,TranslationDataModel]]
  }

  private def t_transform(data: List[JField], isEnabled: Boolean): Map[String, Any] = {
    var translationsVar = Map[String, Any]()
    if(isEnabled) {
      translationsVar = parseFull(List[String](), data, Map[String, TranslationDataModel]())
    } else {
      translationsVar = parsePlain(List[String](), data, Map[String, String]())
    }
    val sortedMap = ListMap(translationsVar.toSeq.sortBy(_._1):_*)
//    for((k,d) <- sortedMap){ println(s" $k -> $d")}
    sortedMap
  }

  private def parsePlain(keys: List[String], data: List[JField],
              translations:Map[String, String]): Map[String, String] = {
    var translationsVar = translations

    for(i <- 0 until data.length){
      val aObj = data(i)
      var newKeys = keys
      newKeys ::= aObj.name
      if(aObj.value.isInstanceOf[JObject]){
        translationsVar =
          translationsVar ++ parsePlain(newKeys, aObj.value.children.asInstanceOf[List[JField]], translationsVar)
      } else {
        val fullkey = newKeys.toArray.reverse.mkString("_")
        translationsVar += (fullkey -> aObj.value.extract[String])
      }
    }
    translationsVar
  }

  private def parseFull(keys: List[String], data: List[JField],
              translations:Map[String, TranslationDataModel]): Map[String, TranslationDataModel] = {
    var translationsVar = translations

    for(i <- 0 until data.length){
      val aObj = data(i)
      var newKeys = keys
      newKeys ::= aObj.name
      if(aObj.value.isInstanceOf[JObject]){
        translationsVar =
          translationsVar ++ parseFull(newKeys, aObj.value.children.asInstanceOf[List[JField]], translationsVar)
      } else {
        val fullkey = newKeys.reverse.mkString("_")
        cltCategoryService.setKeyAndCategories(fullkey, keys.reverse)
        translationsVar += (fullkey -> cltData.buildCT(fullkey, aObj.value.extract[String], keys.reverse))
      }
    }
    translationsVar
  }

}
