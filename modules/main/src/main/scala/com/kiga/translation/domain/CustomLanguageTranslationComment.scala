package com.kiga.translation.domain

import javax.persistence._

import com.kiga.db.{KigaEntityModelWithoutId, KigaEntityModel}
import com.kiga.security.domain.Member

import scala.beans.BeanProperty

@Entity class CustomLanguageTranslationComment extends KigaEntityModel {
  this.setClassName("CustomLanguageTranslationComment")

  @BeanProperty var body: String = _

  @BeanProperty var entity: String = _

  @BeanProperty
  @OneToOne
  @JoinColumn(name = "memberID")
  var user: Member = _
}
