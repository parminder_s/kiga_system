package com.kiga.translation.repository

import com.kiga.translation.domain.CustomLanguageTranslation
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Component

/**
  */
@Component
trait CustomLanguageTranslationRepository extends CrudRepository[CustomLanguageTranslation, java.lang.Long] {
  def findAll(): java.util.List[CustomLanguageTranslation]

  def findAllByLocale(locale: String): java.util.List[CustomLanguageTranslation]

  def findByEntityAndLocale(entity: String, locale: String): CustomLanguageTranslation

  def findByEntityAndLocaleNot(entity: String, locale: String): java.util.List[CustomLanguageTranslation]

  /*
  * there is some Entity Manager or transactional boundary problem,
  * if this method is called other service method transactional calls breaks
  *
  * @Modifying(clearAutomatically = true)
  * @Query(nativeQuery = true,
  * value = "UPDATE CustomTranslation SET status=:status WHERE entity=:key and locale!=:locale")
  * def updateAllChangeStatus(@Param("key") key: String, @Param("locale") locale: String, @Param("status") status: String): Integer
  * */

}
