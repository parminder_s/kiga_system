package com.kiga.translation.web.controllers.request

import com.kiga.main.locale.Locale
import com.kiga.translation.provider.TranslationDataModel
import com.kiga.web.message.EndpointRequest

import scala.beans.BeanProperty

/**
 *
 */
class TranslationUpdateRequest {
  @BeanProperty var word: TranslationDataModel = _
  @BeanProperty var locale: Locale = _
}
