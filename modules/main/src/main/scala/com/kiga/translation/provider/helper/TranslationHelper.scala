package com.kiga.translation.provider.helper

import javax.inject.{Inject, Named}

import com.kiga.translation.domain.CustomLanguageTranslation
import com.kiga.translation.provider.{CltCategoryService, CustomLanguageTranslationStatusService, TranslationDataModel}

@Named
class TranslationHelper {
  val kiga = "Kiga."

  @Inject var customLanguageTranslationStatusService : CustomLanguageTranslationStatusService = _
  @Inject var cltCategoryService: CltCategoryService = _

  def addKiga(entityKey: String): String = {
    kiga.concat(removeKiga(entityKey))
  }

  def removeKiga(entityKey: String): String = {
    entityKey.replaceAll(kiga, "")
  }

  def getBlankTDM(key : String, status : String): TranslationDataModel ={
    val aTdm = new TranslationDataModel()
    aTdm.key = key
    aTdm.status = status
    aTdm.category = cltCategoryService.getCategoriesByKey(key)
    aTdm
  }


  def build_TDM_from_CT(clt: CustomLanguageTranslation) = {
    val aData = new TranslationDataModel()
    aData.locale = clt.locale
    aData.key = removeKiga(clt.entity)
    aData.translation = clt.translation
    aData.status = customLanguageTranslationStatusService.getTranslatedStatus()
    if(clt.status != null){
      aData.status = clt.status
    }
    aData.originalTranslation = clt.translation
    aData.created = clt.getCreated
    aData.lastEdited = clt.getLastEdited()
    aData.category = cltCategoryService.getCategoriesByKey(aData.key)
    aData
  }

  def mergeTdmByClt(clt: CustomLanguageTranslation,
                    matchingTdm : TranslationDataModel) = {
    matchingTdm.translation = clt.translation
    matchingTdm.locale = clt.locale
    if(clt.status != null){
      matchingTdm.status = clt.status
    }
    matchingTdm.originalTranslation = clt.translation
    matchingTdm.created = clt.getCreated
    matchingTdm.lastEdited = clt.getLastEdited
  }


  def buildMap(word: TranslationDataModel,
               comment: Any, history: Any) = {
    var map = Map[String, Any]()
    map += ("word" -> word)
    map += ("comment" -> comment)
    map += ("history" -> history)
    map
  }
}
