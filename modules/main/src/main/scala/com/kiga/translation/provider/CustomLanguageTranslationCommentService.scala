package com.kiga.translation.provider

import javax.inject.{Inject, Named}

import com.kiga.security.domain.Member
import com.kiga.translation.domain.CustomLanguageTranslationComment
import com.kiga.translation.provider.helper.{CommentBuilder, TranslationHelper}
import com.kiga.translation.repository.CustomLanguageTranslationCommentRepository
import org.joda.time.DateTime

import scala.collection.JavaConverters._

@Named class CustomLanguageTranslationCommentService {

  @Inject var commentRepository: CustomLanguageTranslationCommentRepository = _
  @Inject var translationHelper: TranslationHelper = _
  @Inject var commentBuilder: CommentBuilder = _

  def findByTranslationKey(key: String): List[CustomLanguageTranslationComment] = {
    val keyStr = translationHelper.addKiga(key)
    commentRepository.findByEntity(keyStr).asScala.toList
  }

  def save(body: String, key: String, user: Member, date: DateTime): CustomLanguageTranslationComment = {
    val comment: CustomLanguageTranslationComment = commentBuilder.buildNew(body, key, user, date)
    commentRepository.save(comment)
  }

  def save(comment: CustomLanguageTranslationComment): CustomLanguageTranslationComment = {
    commentRepository.save(comment)
  }

}
