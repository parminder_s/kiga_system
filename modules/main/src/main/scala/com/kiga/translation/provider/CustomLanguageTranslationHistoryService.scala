package com.kiga.translation.provider

import javax.inject.{Inject, Named}

import com.kiga.security.domain.Member
import com.kiga.translation.domain.{CustomLanguageTranslation, CustomLanguageTranslationHistory}
import com.kiga.translation.provider.helper.{HistoryBuilder, TranslationHelper}
import com.kiga.translation.repository.CustomLanguageTranslationHistoryRepository
import org.joda.time.DateTime

import scala.collection.JavaConverters._

/**
 *
 */
@Named
class CustomLanguageTranslationHistoryService {
  @Inject var historyRepository: CustomLanguageTranslationHistoryRepository = _
  @Inject var historyBuilder: HistoryBuilder = _
  @Inject var translationService: TranslationHelper = _

  def findByTranslationKey(key: String): List[CustomLanguageTranslationHistory] = {
    val keyStr = translationService.addKiga(key)
    historyRepository.findByEntity(keyStr).asScala.toList
  }

  def insert(customTranslation: CustomLanguageTranslation, currentUser: Member, dateTime: DateTime) = {
    val history: CustomLanguageTranslationHistory = historyBuilder.build(customTranslation, currentUser, dateTime)
    save(history)
  }

  def save(history: CustomLanguageTranslationHistory): CustomLanguageTranslationHistory = {
    historyRepository.save(history)
  }
}
