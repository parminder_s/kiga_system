package com.kiga.translation.domain


class CustomLanguageTranslationCategory {

  var title: String = _
  var parent: CustomLanguageTranslationCategory = _
  var categoryType: String = _

  override def toString(): String = {
    title
  }

}
