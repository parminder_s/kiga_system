package com.kiga.translation.provider.helper

import javax.inject.{Inject, Named}

import com.kiga.translation.domain.CustomLanguageTranslation
import com.kiga.translation.provider.{CustomLanguageTranslationStatusService, TranslationDataModel}
import org.joda.time.DateTime

// gradle test --tests *MyServicesTest.methodTest1
@Named class CltBuilder {
  @Inject var translationHelper: TranslationHelper = _
  @Inject var customLanguageTranslationStatusService: CustomLanguageTranslationStatusService = _

  def buildCLT(
               created:DateTime,    lastEdited:DateTime,
               locale:String,       entity:String,
               translation:String,  priority:Int
              ) = {
    val aData = new CustomLanguageTranslation()
    aData.setClassName("CustomLanguageTranslation")
    aData.setLastEdited(lastEdited.toDate)
    aData.locale = locale
    aData.entity = entity
    aData.translation = translation
    aData.priority = priority
    aData
  }

  def buildCT(entity: String, translation: String, category: List[String]) = {
    val aData = new TranslationDataModel()
    aData.key = entity
    aData.translation = translation
    aData.category = category
    aData
  }

  def buildCTFromTdm(translationDataModel: TranslationDataModel,
                     oldCustomTranslation: CustomLanguageTranslation,
                     dateTime: DateTime): CustomLanguageTranslation = {

    var aData: CustomLanguageTranslation = null
    if(oldCustomTranslation != null){
      aData = oldCustomTranslation
    } else {
      aData = new CustomLanguageTranslation()
    }
    aData.entity = translationHelper.addKiga(translationDataModel.key)
    aData.translation = translationDataModel.translation
    aData.locale = translationDataModel.locale
    aData.setLastEdited(dateTime.toDate)

    aData.priority = 50
    aData
  }

  def customizeCT(translationDataModel: TranslationDataModel,
              locale:String, key:String,
                  originalTranslation: CustomLanguageTranslation) = {

    translationDataModel.locale = locale
    translationDataModel.key = key
    setOriginalTranslation(translationDataModel, originalTranslation)
    translationDataModel
  }
  def setOtherProps(tdmExist: TranslationDataModel, tdmNew: TranslationDataModel) = {
    tdmExist.category = tdmNew.category
  }

  def build_TDM_from_CT(clt: CustomLanguageTranslation, originalTranslation: CustomLanguageTranslation) = {
    val aData = new TranslationDataModel()
    aData.locale = clt.locale
    aData.key = translationHelper.removeKiga(clt.entity)
    aData.translation = clt.translation
    aData.status = customLanguageTranslationStatusService.getTranslatedStatus()
    if(clt.status != null){
      aData.status = clt.status
    }
    aData.originalTranslation = clt.translation
    aData.created = clt.getCreated
    aData.lastEdited = clt.getCreated
    aData.category = List[String]()
    setOriginalTranslation(aData, originalTranslation)
    aData
  }

  def setOriginalTranslation(translationDataModel: TranslationDataModel, originalTranslation: CustomLanguageTranslation) = {
    if (originalTranslation != null) {
      translationDataModel.originalTranslation = originalTranslation.translation
    }
    translationDataModel.status = customLanguageTranslationStatusService.getTranslatedStatus()
    translationDataModel.html = s"""<span translation-client=\"\" status=\"${translationDataModel.status}\" key=\"${translationHelper.removeKiga(translationDataModel.key)}\" value=\"${translationDataModel.translation}\"></span>"""
  }
}
