package com.kiga.translation.provider

import javax.inject.{Inject, Named}

import com.kiga.security.services.SecurityService
import com.kiga.translation.domain.{CustomLanguageTranslation, CustomLanguageTranslationComment, CustomLanguageTranslationHistory, Locale}
import com.kiga.translation.provider.helper.{CltBuilder, TranslationHelper}
import org.joda.time.DateTime

import scala.util.parsing.json.JSON

/**
  */
@Named
class TranslationService {
  @Inject var localeService: LocaleService = _
  @Inject var dataService: JsonDataTransformService = _
  @Inject var fileService: JsonFileService = _
  @Inject var parseService: ParseService = _
  @Inject var cltMatcherService: CltMatcherService = _
  @Inject var customTranslationService: CustomLanguageTranslationService = _
  @Inject var customLanguageTranslationStatusService: CustomLanguageTranslationStatusService = _
  @Inject var commentService: CustomLanguageTranslationCommentService = _
  @Inject var historyService: CustomLanguageTranslationHistoryService = _
  @Inject var CLTData: CltBuilder = _
  @Inject var translationHelper: TranslationHelper = _
  @Inject var securityService: SecurityService = _

  implicit val formats = net.liftweb.json.DefaultFormats

  def getJsonFile(locale: String): String = {
    fileService.getTranslationFile(locale)
  }

  def getPlainJson(localeStr: String): Map[String, String] = {
    val fileData = fileService.getTranslationFile(localeStr)
    val rawJson = parseService.parse(fileData)
    dataService.transformAsPlain(rawJson)
  }

  def getTdmFromJson(localeStr: String): Map[String, TranslationDataModel] = {
    val fileData = fileService.getTranslationFile(localeStr)
    val rawJson = parseService.parse(fileData)
    dataService.transformAsTDM(rawJson)
  }

  def collectTDMForAllLocales(localeStr: String) : Map[String, TranslationDataModel] = {

    val allLocales = localeService.getPlainLocales()
    val currentLocale = localeService.get(localeStr).toString()
    var tdmMap = Map[String, TranslationDataModel]()
    for(i <- 0 until allLocales.length){
      val aLocale = allLocales(i)
      val plainJson = getTdmFromJson(aLocale)
      plainJson foreach { case (key, value) =>
        value.translation = null
        value.status = customLanguageTranslationStatusService.getNotTranslatedStatus()
        value.locale = currentLocale
        tdmMap += (key -> value)
      }
      val localeFullTitle = localeService.getQualifiedName(aLocale)
      val clts = customTranslationService.getTranslation(localeFullTitle)
      clts foreach { case aClt =>
        val key = translationHelper.removeKiga(aClt.getEntity())
        val aTdm = translationHelper.getBlankTDM(key, customLanguageTranslationStatusService.getTranslatedStatus())
        tdmMap += (key -> aTdm)
      }
    }
    val plainBaseLocaleJson: Map[String, String] = getBaseTranslation
    /* final customization */
    setOriginalTranslation(tdmMap, plainBaseLocaleJson)
    // original translation
    tdmMap
  }

  def getBaseTranslation: Map[String, String] = {
    val baseLocale = localeService.getBaseLocale()
    var plainBaseLocaleJson = getPlainJson(baseLocale.title)
    val baseLocaleClts = customTranslationService.getTranslation(baseLocale.toString())
    baseLocaleClts foreach { case aClt =>
      val key = translationHelper.removeKiga(aClt.getEntity())
      plainBaseLocaleJson += (key -> aClt.translation)
    }
    plainBaseLocaleJson
  }

  def setOriginalTranslation(tdmMap : Map[String, TranslationDataModel],
                             baseTranslation : Map[String, String]) = {
    tdmMap foreach { case (key, value) =>
      value.originalTranslation = baseTranslation.getOrElse(key, null)
      value.html = s"""<span translation-client=\"\" status=\"${value.status}\" key=\"${translationHelper.removeKiga(value.key)}\" value=\"${value.translation}\"></span>"""
    }
  }

  def mergeAllTDMForAllLocales(localeStr: String) : Map[String, TranslationDataModel] = {
    var allTDMFromLocales = collectTDMForAllLocales(localeStr)
    val currentLocalTDM = getTDM(localeStr: String)
    currentLocalTDM foreach { case (key, value) =>
      allTDMFromLocales += (key -> value)
    }
    allTDMFromLocales
  }

  def getTranslationTable(localeStr: String) : Map[String, String] = {
    val currentLocalTDM = getTDM(localeStr: String)
    var map = Map[String, String]()
    currentLocalTDM foreach { case (key, value) =>
      map += (key -> value.translation)
    }
    map
  }

  def getTDM(localeStr: String): Map[String, TranslationDataModel] = {
    val currentLocale = localeService.get(localeStr)
    var currentLocaleTdm = getTdmFromJson(currentLocale.title)

    /* Set Locale and status as there might have some Json entries*/
    currentLocaleTdm foreach { case (key, value) =>
      value.locale = currentLocale.toString()
      value.status = customLanguageTranslationStatusService.getTranslatedStatus()
    }

    val currentLocaleClts = customTranslationService.getTranslation(currentLocale.toString())
    currentLocaleClts foreach { case aClt =>
      val key = translationHelper.removeKiga(aClt.getEntity())
      val matchingTdm = currentLocaleTdm.getOrElse(key, null)
      if(matchingTdm != null){
        translationHelper.mergeTdmByClt(aClt, matchingTdm)
        currentLocaleTdm += (key -> matchingTdm)
      } else {
        currentLocaleTdm += (key -> translationHelper.build_TDM_from_CT(aClt))
      }
    }

    val plainBaseLocaleJson: Map[String, String] = getBaseTranslation
    /* final customization */
    setOriginalTranslation(currentLocaleTdm, plainBaseLocaleJson)
    currentLocaleTdm
  }

    def retrieveData() = {
    val json_string = fileService.getTranslationFile("de")
    JSON.parseFull(json_string).get.asInstanceOf[Map[String, Any]]
  }

  def checkRedundant(localeStr: String, word: TranslationDataModel): Boolean = {
    // check against only the DB entity.
    val allTDM = getTDM(localeStr)
    val filteredTDM = allTDM.filter {
      case (k, v) =>
        v.translation == word.translation && v.locale == word.locale
    }
    filteredTDM.nonEmpty
  }

  def checkAndSaveBaseTranslation(word: TranslationDataModel) = {
    val baseLocale = localeService.getBaseLocale()

    if (baseLocale.title == word.locale
      || baseLocale.toString() == word.locale) {

      val otherLocales = localeService.listAllExcluding(word.locale)
      otherLocales.foreach { case (locale) =>
        val aClt = customTranslationService.findByKeyAndLocale(translationHelper.addKiga(word.key), locale.toString())
        if(aClt != null){
          aClt.status = customLanguageTranslationStatusService.getChangedStatus()
          customTranslationService.save(aClt)
        } else {
          val tdmMap = getWord(word.key, locale.title)
          val newTdm = tdmMap("word").asInstanceOf[TranslationDataModel]
          saveImplicitly(newTdm, customLanguageTranslationStatusService.getChangedStatus(), locale)
        }
      }
    }
  }
  def save(word: TranslationDataModel) = {
    val currentDate = new DateTime()
    val kigaAddedKey: String = translationHelper.addKiga(word.key)
    val ct: CustomLanguageTranslation = customTranslationService
      .findByKeyAndLocale(kigaAddedKey, word.locale)
    val ctToSave = CLTData.buildCTFromTdm(word, ct, currentDate)
    customTranslationService.save(ctToSave)

    checkAndSaveBaseTranslation(word)

    word.created = ctToSave.getCreated
    word.lastEdited = ctToSave.getCreated

    var map = Map[String, Any]()
    map += ("word" -> word)

    var comment: CustomLanguageTranslationComment = null
    val currentMember = securityService.getSessionMember.get
    if (word.latestComment != null) {
      comment = commentService.save(
        word.latestComment,
        kigaAddedKey,
        currentMember,
        currentDate)
    }
    translationHelper.buildMap(word, comment, historyService.insert(ctToSave, currentMember, currentDate))
  }

  def saveImplicitly(word: TranslationDataModel, status: String, locale: Locale) = {
    val currentDate = new DateTime()
    val kigaAddedKey: String = translationHelper.addKiga(word.key)
    val ct: CustomLanguageTranslation = customTranslationService.findByKeyAndLocale(kigaAddedKey, word.locale)
    val ctToSave = CLTData.buildCTFromTdm(word, ct, currentDate)
    ctToSave.status = status
    ctToSave.locale = locale.toString()
    customTranslationService.save(ctToSave)
    historyService.insert(ctToSave, securityService.getSessionMember.get, currentDate)
  }

  def getWord(key: String, localeStr: String) : Map[String, Any] = {
    val keyWithoutKiga = translationHelper.removeKiga(key)
    val keyWithKiga = translationHelper.addKiga(key)
    val localeFullTitle = localeService.getQualifiedName(localeStr)

    val fileData = fileService.getTranslationFile(localeStr)
    val rawJson = parseService.parse(fileData)
    val json = dataService.transformAsTDM(rawJson)
    val filteredJson = json.filter {
      case (k, v) =>
        k == keyWithoutKiga
    }
    val original = customTranslationService.getBaseTranslation()
    val current = customTranslationService.findByKeyAndLocale(keyWithKiga, localeFullTitle)
    var currentList = List[CustomLanguageTranslation]()
    if(current == null && filteredJson.size == 0){
      val allTDMFromLocales = collectTDMForAllLocales(localeStr)
      val emptyTdm = allTDMFromLocales(keyWithoutKiga)
      return translationHelper.buildMap(emptyTdm,
        List[CustomLanguageTranslationComment](), List[CustomLanguageTranslationHistory]())
    }
    if (current != null) {
      currentList ::= current
    }
    val finalMap = cltMatcherService.migrateTo(original, currentList, filteredJson, localeFullTitle)
    translationHelper.buildMap(finalMap(keyWithoutKiga),
      commentService.findByTranslationKey(keyWithKiga),
      historyService.findByTranslationKey(keyWithKiga))
  }

}
