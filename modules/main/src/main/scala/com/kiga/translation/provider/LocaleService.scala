package com.kiga.translation.provider

import javax.inject.{Inject, Named}

import com.kiga.translation.domain.Locale
import com.kiga.translation.repository.LocaleRepository


// gradle test --tests *MyServicesTest.methodTest1
@Named class LocaleService {

  @Inject var localeRepository : LocaleRepository = _

  def getPlainLocales() : List[String] = {
    val allLocales = localeRepository.findAll()
    allLocales.map(locale => locale.title)
  }

  def getBaseLocale(): Locale = {
    localeRepository.getBase()
  }

  def get(locale:String) : Locale = {
    localeRepository.findByTitle(locale)
  }

  def listAllExcluding(locale:String) : List[Locale]  = {
    localeRepository.findAll().filter(_.toString() != locale)
  }

  def getQualifiedName(title:String) : String = {
    localeRepository.findByTitle(title).toString()
  }

}
