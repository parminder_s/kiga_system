package com.kiga.translation.web.controllers

import javax.inject.Inject

import com.kiga.translation.provider.CustomLanguageTranslationHistoryService
import com.kiga.translation.web.controllers.request.TranslationCommentRequest
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestBody, RequestMapping, RequestMethod, ResponseBody}

/**
 *
 */
@Controller
class HistoryController {
  @Inject var historyService: CustomLanguageTranslationHistoryService = _

  @RequestMapping(
    value = Array("/api/translation/history/get", "/api/translation/getHistory"),
    method = Array(RequestMethod.POST)
  )
  @ResponseBody
  def findByTranslationKey(@RequestBody commentRequest: TranslationCommentRequest) = {
    historyService.findByTranslationKey(commentRequest.translationKey)
  }

  //  @RequestMapping(
  //    value = Array("/api/history/new", "/api/history/save"),
  //    method = Array(RequestMethod.POST)
  //  )
  //  @ResponseBody
  //  def save(@RequestBody history: History) = {
  //    historyService.save(history)
  //  }
}
