package com.kiga.translation.provider

import javax.inject.Named

import net.liftweb.json.JsonAST.{JField, JObject}

/**
 *
 */
@Named
class ParseService {

  implicit val formats = net.liftweb.json.DefaultFormats

  def parse(data: String): List[JField] = {
    net.liftweb.json.parse(data).extract[JObject].children.asInstanceOf[List[JField]]
  }

}
