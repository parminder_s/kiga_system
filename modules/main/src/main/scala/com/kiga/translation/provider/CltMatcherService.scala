package com.kiga.translation.provider

import javax.inject.{Inject, Named}

import com.kiga.translation.domain.CustomLanguageTranslation
import com.kiga.translation.provider.helper.{CltBuilder, TranslationHelper}

@Named class CltMatcherService {
  @Inject var cltData: CltBuilder = _
  @Inject var translationHelper: TranslationHelper = _

  def mapFromCLTList(cltList: List[CustomLanguageTranslation],
                     baseCltList: List[CustomLanguageTranslation]): Map[String, TranslationDataModel] = {
      var aMap = Map[String, TranslationDataModel]()
      cltList.foreach { aClt =>
        val keyStr = translationHelper.removeKiga(aClt.entity)
        val keyWithKiga = translationHelper.addKiga(aClt.entity)
        val orgClt = pickOriginalTranslation(baseCltList, keyWithKiga)
        val mergedClt = cltData.build_TDM_from_CT(aClt, orgClt)
        aMap += (keyStr -> mergedClt)
      }
      aMap
    }

  def pickOriginalTranslation(baseCltList: List[CustomLanguageTranslation],
                              keyStr: String): CustomLanguageTranslation = {
    var originalTranslation: CustomLanguageTranslation = null
    val matched = baseCltList.find(data => data.entity == keyStr)
    if (!matched.isEmpty) {
      originalTranslation = matched.get
    }
    originalTranslation
  }

    def mapFromJsonList (aMap : Map[String, TranslationDataModel],
                         transformAsTDM: Map[String, TranslationDataModel],
                         baseCltList: List[CustomLanguageTranslation], localeStr: String): Map[String, TranslationDataModel] = {

      var newMap = aMap map {case (k,v) => (k -> v)}
//      TODO map.transform
      transformAsTDM foreach { case (key, value) =>
        val keyStr = translationHelper.removeKiga(key)
        val keyWithKiga = translationHelper.addKiga(key)
        val found = newMap.filter((anObj)=> anObj._2.key == keyStr && anObj._2.locale == localeStr)
        if(found.isEmpty){
          newMap += (keyStr -> cltData.customizeCT(value, localeStr, keyStr,
            pickOriginalTranslation(baseCltList, keyWithKiga)))
        } else {
          cltData.setOtherProps(found.head._2, value)
        }
      }
      newMap
    }

  def migrateTo(baseCltList: List[CustomLanguageTranslation],
                currentCltList: List[CustomLanguageTranslation],
                transformAsTDM: Map[String, TranslationDataModel],
                  currentLocal: String): Map[String, TranslationDataModel] = {

    val aMap = mapFromCLTList(currentCltList, baseCltList)
    val finalMap = mapFromJsonList(aMap, transformAsTDM, baseCltList, currentLocal)
    finalMap
  }
}
