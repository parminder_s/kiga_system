package com.kiga.translation.provider.helper

import javax.inject.{Inject, Named}

import com.kiga.security.domain.Member
import com.kiga.translation.domain.{CustomLanguageTranslation, CustomLanguageTranslationHistory}
import org.joda.time.DateTime

@Named
class HistoryBuilder {
  @Inject var translationHelper: TranslationHelper = _

  def build(customTranslation: CustomLanguageTranslation, currentUser: Member, dateTime: DateTime): CustomLanguageTranslationHistory = {
    val history = new CustomLanguageTranslationHistory()
    if (customTranslation.getCreated() == null) {
      history.body = "Created"
    } else if (customTranslation.getCreated() == customTranslation.getLastEdited()) {
      history.body = "Created"
    } else {
      history.body = "Updated"
    }
    history.entity = translationHelper.addKiga(customTranslation.entity)
    history.user = currentUser
    history
  }
}
