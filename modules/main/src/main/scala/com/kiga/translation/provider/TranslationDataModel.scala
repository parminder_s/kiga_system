package com.kiga.translation.provider

import java.util.Date

/**
 *
 */
class TranslationDataModel {
  var key: String = _
  var locale: String = _
  var originalTranslation: String = _
  var status: String = _
  var isEdited: Boolean = false
  var translation: String = _
  var latestComment: String = _
  var html: String = _
  var category: List[String]= _
  var created: Date = _
  var lastEdited: Date = _
  var commentCount: Integer = 0
  //  var comments: List[Comment]= _
  //  var history: List[History] = _
}

