package com.kiga.translation.repository

import com.kiga.translation.domain.CustomLanguageTranslationHistory
import org.springframework.data.repository.CrudRepository

/**
  */
trait CustomLanguageTranslationHistoryRepository extends CrudRepository[CustomLanguageTranslationHistory, java.lang.Long] {
  def findAll(): java.util.List[CustomLanguageTranslationHistory]

  def findByEntity(entity: String): java.util.List[CustomLanguageTranslationHistory]
}
