package com.kiga.translation.provider

import javax.inject.{Inject, Named}

import org.apache.commons.io.IOUtils

@Named class JsonFileService {

  @Inject var localeService: LocaleService = _

  def getTranslationFile(locale: String): String = {
    val path = "/data/translation/" + localeService.get(locale).title + ".json"
    getResourceFile(path)
  }

  def getBaseTranslationFile(): String = {
    val path = "/data/translation/" + localeService.getBaseLocale().title + ".json"
    getResourceFile(path)
  }

  def getResourceFile(path: String): String = {
    val rs = getClass.getResourceAsStream(path)
    val rsis = IOUtils.toString(rs)
    rsis
  }
}
