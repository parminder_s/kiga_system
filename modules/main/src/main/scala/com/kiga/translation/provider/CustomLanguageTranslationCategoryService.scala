package com.kiga.translation.provider

import javax.inject.{Inject, Named}

import com.kiga.translation.domain.CustomLanguageTranslationCategory
import net.liftweb.json.JsonAST.JField
import net.liftweb.json._

@Named class CustomLanguageTranslationCategoryService {

  @Inject var fileService: JsonFileService = _
  @Inject var parseService: ParseService = _
  implicit val formats = net.liftweb.json.DefaultFormats
  var categories = Map[String, CustomLanguageTranslationCategory]()

  def getCategories(): Map[String, CustomLanguageTranslationCategory] = {
    if (categories.size == 0) {
      setCategories
    }
    categories
  }

  def setCategories = {
    val baseTranslation = fileService.getBaseTranslationFile
    val parsedData = parseService.parse(baseTranslation)
    categories = parseCategory(categories, parsedData, null)
  }

  // TODO : Test
  def parseCategory(categories: Map[String, CustomLanguageTranslationCategory], data: List[JField], parent: CustomLanguageTranslationCategory): Map[String, CustomLanguageTranslationCategory] = {
    var categoriesVar = categories
    for (i <- 0 until data.length) {
      val aObj = data(i)
      if (aObj.value.isInstanceOf[JObject]) {
        val aCategory = new CustomLanguageTranslationCategory()
        aCategory.title = aObj.name
        aCategory.parent = parent
        if (parent != null)
          aCategory.categoryType = "Category"
        else aCategory.categoryType = "SubCategory"
        categoriesVar = parseCategory(categoriesVar, aObj.value.children.asInstanceOf[List[JField]], aCategory)
        categoriesVar += (aObj.name -> aCategory)
      }
    }
    categoriesVar
  }
}
