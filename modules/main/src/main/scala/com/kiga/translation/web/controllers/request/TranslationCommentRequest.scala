package com.kiga.translation.web.controllers.request

import com.kiga.web.message.EndpointRequest

import scala.beans.BeanProperty

/**
 *
 */
class TranslationCommentRequest extends EndpointRequest {
  @BeanProperty var translationKey: String = _
  @BeanProperty var currentLocale: String = _
}

