package com.kiga.print.validation.db.repository

import com.kiga.print.validation.domain.IdeaError
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

trait IdeaErrorRepository extends CrudRepository[IdeaError, java.lang.Long] {
  def countByIdeaValidationReportIdAndAutoCorrected(ideaValidationReportId: Long, autoCorrected: Boolean): Int

  def countByIdeaValidationReportIdAndAutoCorrectable(ideaValidationReportId: Long, autoCorrectable: Boolean): Int

  def countByIdeaValidationReportIdAndAutoCorrectableAndAutoCorrected(ideaValidationReportId: Long, autoCorrectable: Boolean, autoCorrected: Boolean): Int

  def countByIdeaValidationReportIdAndAutoCorrectableOrIdeaValidationReportIdAndAutoCorrected(ideaValidationReportId1: Long, autoCorrectable: Boolean, ideaValidationReportId2: Long, autoCorrected: Boolean): Int

  def countByIdeaValidationReportId(ideaValidationReportId: Long): Int

  def countByIdeaValidationReportIdAndArticleId(ideaValidationReportId: Long, articleId: Long): Int

  def countByIdeaValidationReportIdAndArticleIdAndAutoCorrected(ideaValidationReportId: Long, articleId: Long, autoCorrected: Boolean): Int

  def countByIdeaValidationReportIdAndArticleIdAndAutoCorrectableAndAutoCorrected(ideaValidationReportId1: Long, articleId1: Long, autoCorrectable: Boolean, autoCorrected: Boolean): Int

  def countByIdeaValidationReportIdAndArticleIdAndAutoCorrectableOrIdeaValidationReportIdAndArticleIdAndAutoCorrected(ideaValidationReportId1: Long, articleId1: Long, autoCorrectable: Boolean, ideaValidationReportId2: Long, articleId2: Long, autoCorrected: Boolean): Int

  def findAllByIdeaValidationReportIdAndArticleId(ideaValidationReportId: Long, articleId: Long): java.util.List[IdeaError]

  @Query(value = "SELECT a.errorGroup,count(DISTINCT a.articleId) as Articles, (a.autoCorrectable = 1 or a.autoCorrected = 1) as Correctable from IdeaError a where a.ideaValidationReportId = :ideaValidationReportId GROUP BY a.errorGroup", nativeQuery = true)
  def countAllErrorGroupsByIdeaValidationReportId(@Param("ideaValidationReportId") ideaValidationReportId: Long): java.util.List[Object]

  @Query(value = "SELECT a.errorGroup,count(DISTINCT a.articleId) as Articles, (a.autoCorrectable = 1 or a.autoCorrected = 1) as Correctable from IdeaError a where a.ideaValidationReportId = :ideaValidationReportId and a.locale = :locale GROUP BY a.errorGroup", nativeQuery = true)
  def countAllErrorGroupsByIdeaValidationReportIdAndLocale(@Param("ideaValidationReportId") ideaValidationReportId: Long, @Param("locale") locale: String): java.util.List[Object]

  //Object [ArticleId,Title,AutoCorrectable,manuell,AutoCorrected,All]
  @Query(value = "select ArticleId,(select Title from SiteTree where ID = ArticleId) as Title,sum(case when AutoCorrectable = 1 or AutoCorrected = 1 then 1 else 0 end) as AutoCorrectable,count(*) - sum(case when AutoCorrectable = 1 or AutoCorrected = 1 then 1 else 0 end) as manuell,sum(AutoCorrected) as AutoCorrected,count(*) as eAll From IdeaError where IdeaValidationReportId = :ideaValidationReportId group by ArticleId", nativeQuery = true)
  def countAllErrorsByIdeaValidationReportId(@Param("ideaValidationReportId") ideaValidationReportId: Long): java.util.List[Object]

  //Object [ArticleId,Title,AutoCorrectable,manuell,AutoCorrected,All]
  @Query(value = "select ArticleId,(select Title from SiteTree where ID = ArticleId) as Title,sum(case when AutoCorrectable = 1 or AutoCorrected = 1 then 1 else 0 end) as AutoCorrectable,count(*) - sum(case when AutoCorrectable = 1 or AutoCorrected = 1 then 1 else 0 end) as manuell,sum(AutoCorrected) as AutoCorrected,count(*) as eAll From IdeaError where IdeaValidationReportId = :ideaValidationReportId and ErrorGroup = :errorGroup group by ArticleId", nativeQuery = true)
  def countAllErrorsByIdeaValidationReportIdAndErrorGroup(@Param("ideaValidationReportId") ideaValidationReportId: Long,@Param("errorGroup") errorGroup: Int): java.util.List[Object]

  //Object [ArticleId,Title,AutoCorrectable,manuell,AutoCorrected,All]
  @Query(value = "select ArticleId,(select Title from SiteTree where ID = ArticleId) as Title,sum(case when AutoCorrectable = 1 or AutoCorrected = 1 then 1 else 0 end) as AutoCorrectable,count(*) - sum(case when AutoCorrectable = 1 or AutoCorrected = 1 then 1 else 0 end) as manuell,sum(AutoCorrected) as AutoCorrected,count(*) as eAll From IdeaError where IdeaValidationReportId = :ideaValidationReportId and Locale = :locale group by ArticleId", nativeQuery = true)
  def countAllErrorsByIdeaValidationReportIdAndLocale(@Param("ideaValidationReportId") ideaValidationReportId: Long,@Param("locale") locale: String): java.util.List[Object]

  //Object [ArticleId,Title,AutoCorrectable,manuell,AutoCorrected,All]
  @Query(value = "select ArticleId,(select Title from SiteTree where ID = ArticleId) as Title,sum(case when AutoCorrectable = 1 or AutoCorrected = 1 then 1 else 0 end) as AutoCorrectable,count(*) - sum(case when AutoCorrectable = 1 or AutoCorrected = 1 then 1 else 0 end) as manuell,sum(AutoCorrected) as AutoCorrected,count(*) as eAll From IdeaError where IdeaValidationReportId = :ideaValidationReportId and ErrorGroup = :errorGroup and Locale = :locale group by ArticleId", nativeQuery = true)
  def countAllErrorsByIdeaValidationReportIdAndErrorGroupAndLocale(@Param("ideaValidationReportId") ideaValidationReportId: Long,@Param("errorGroup") errorGroup: Int,@Param("locale") locale: String): java.util.List[Object]


}
