package com.kiga.print.validation.db.model

import java.util.Date
import javax.persistence.{Column, Entity}

import com.kiga.db.{KigaEntityModelWithoutId, KigaEntityModel}

import scala.beans.BeanProperty

/**
 * Created by peter on 08.09.15.
 */

@Entity
class IdeaValidationReport
  extends KigaEntityModel{
  @BeanProperty @Column(columnDefinition="DATETIME") var startDateTime: Date = _
  @BeanProperty @Column(columnDefinition="DATETIME",nullable = true) var endDateTime: Date = _
  @BeanProperty var withAutocorrect: Boolean = _
  @BeanProperty var withSaveToDb: Boolean = _
  @BeanProperty var manual:Int = _
  @BeanProperty var autocorrectable:Int = _
  @BeanProperty var autocorrected:Int = _
  @BeanProperty var allErrors:Int = _
  @BeanProperty var status:Int = _
  @BeanProperty var testedArticles:Int = _
  @BeanProperty var allArticles:Boolean = _
}
