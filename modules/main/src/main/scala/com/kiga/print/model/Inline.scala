package com.kiga.print.model

import scala.beans.BeanProperty
import scala.xml.Elem

/**
  * Created by peter on 23.02.16.
  */
class Inline extends Printable {
  @BeanProperty var fontFamily: String = null
  @BeanProperty var fontSize: Integer = null
  @BeanProperty var color: String = null
  override def getFop(): Elem = {
    var element = <inline font-family={attributeToString(Option(fontFamily))} font-size={attributeToString(Option(fontSize))} color={attributeToString(Option(color))}>{text}</inline>
    return element.copy(child = element.child ++ this.children.map(_.getFop()))
  }
}
