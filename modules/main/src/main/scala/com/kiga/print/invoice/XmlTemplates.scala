package com.kiga.print.invoice

import com.kiga.print.model.TableCellElement
import org.springframework.util.Base64Utils

/**
 * Created by peter on 29.09.15.
 */
object XmlTemplates {
    def invoice(emailSupport: String,
              kigaAddressLine1: String,
              kigaAddressLine2: String,
              content: xml.Elem): xml.Elem = {
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">

      <fo:layout-master-set>
        <fo:simple-page-master
        font-family="sans-serif"
        margin="1.5cm"
        margin-bottom="4cm"
        master-name="main"
        page-width="21cm"
        page-height="29.7cm">

          <fo:region-body margin-top="40mm" region-name="xsl-region-body"/>
          <fo:region-before region-name="xsl-region-before"/>
          <fo:region-after region-name="xsl-region-after"/>
        </fo:simple-page-master>
      </fo:layout-master-set>

      <fo:page-sequence master-reference="main" font-size="10">
        <fo:static-content flow-name="xsl-region-before">
          <fo:block margin-left="14cm">
            <fo:external-graphic
            border-color="#4b4b4b"  margin-bottom="15mm" content-width="108px" src={s"url('data:image/jped;base64,${Base64Images.kigaLogo()}')"}>
            </fo:external-graphic>
          </fo:block>
          <fo:block font-size="8" margin-top="-7mm">
            {emailSupport}
          </fo:block>
          <fo:table>
            <fo:table-body>
              <fo:table-row>
                <fo:table-cell>
                  <fo:block font-size="7" margin-top="1.3cm">
                    {kigaAddressLine1}
                  </fo:block>
                  <fo:block font-size="7">
                    {kigaAddressLine2}
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:block>
            <fo:leader leader-pattern="rule" leader-pattern-width="5pt" leader-length="6cm"/>
          </fo:block>
        </fo:static-content>

        <fo:static-content flow-name="xsl-region-after">
          <!-- special footer -->
          <fo:retrieve-marker retrieve-class-name="footer" retrieve-position="first-starting-within-page"/>
          <!-- common footer on every page -->
          <fo:block font-size="9" margin-top="0.5cm" margin-right="0cm">
            <fo:leader leader-pattern="rule" leader-pattern-width="5pt" leader-length="100%"/>
            <fo:block>HOHE Medien OG KiGaPortal Klosterwiesgasse 101b, A-8010 Graz</fo:block>
            <fo:block>E-Mail: kundenservice@kigaportal.com</fo:block>
            <fo:block>DVR:0051853 / UID:ATU64282626 / FN307106b / Handelsgericht Graz</fo:block>
            <fo:leader leader-pattern="rule" leader-pattern-width="5pt" leader-length="100%"/>
          </fo:block>
          <fo:block text-align="right">
            <fo:page-number/>
            /
            <fo:page-number-citation-last ref-id="last-page"/>
          </fo:block>

        </fo:static-content>{content.copy(
        child = content.child :+ <fo:block id="last-page"></fo:block>
      )}
      </fo:page-sequence>

    </fo:root>
  }


  def addressTable(printAddress1: List[String],
                   printAddress2: List[String]): xml.Elem = {

    var tableFirst = <fo:table-cell margin-top="-1cm" height="4.5cm" width="8cm"></fo:table-cell>
    tableFirst = tableFirst.copy(
      child = printAddress1.map(x => (block(x, "left", 12)))
    )
    var tableSecond = <fo:table-cell width="8.5cm"></fo:table-cell>
    tableSecond = tableSecond.copy(
      child = printAddress2.map(x => (block(x, "left", 12)))
    )
    if(tableSecond.child.size == 0){
      <fo:table>
        <fo:table-body>
          <fo:table-row>
            {tableFirst}<fo:table-cell width="1cm">
            <fo:block></fo:block>
          </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    }
    else {
      <fo:table>
        <fo:table-body>
          <fo:table-row>
            {tableFirst}<fo:table-cell width="1cm">
            <fo:block></fo:block>
          </fo:table-cell>{tableSecond}
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    }
  }

  def leader(text: String, width: String): String = {
    "<fo:leader leader-pattern=\"space\" width=\"" + width + "\" />" + text
  }

  def billPreInfo(date: String, billNr: String, customerNr: String,
                  shippingNr: String): xml.Elem = {
    billPreInfoWithTr("", "", "", date, billNr, customerNr, shippingNr)
  }

  def billPreInfoWithTr(taxNr: String, taxBureo: String, passportNr: String,
                        date: String,
                        billNr: String, customerNr: String,
                        shippingNr: String): xml.Elem = {
    <fo:table>
      <fo:table-body>
        <fo:table-row>
          <fo:table-cell width="13cm">
            <fo:block font-size="9">
              {taxNr}
            </fo:block>
          </fo:table-cell>
          <fo:table-cell width="2cm">
            <fo:block text-align="left">Datum:</fo:block>
          </fo:table-cell>
          <fo:table-cell width="3cm">
            <fo:block text-align="right">
              {date}
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell>
            <fo:block font-size="9">
              {taxBureo}
            </fo:block>
          </fo:table-cell>
          <fo:table-cell>
            <fo:block text-align="left">Rechungsnummer:</fo:block>
          </fo:table-cell>
          <fo:table-cell margin-left="0">
            <fo:block text-align="right">
              {billNr}
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell>
            <fo:block font-size="9">
              {passportNr}
            </fo:block>
          </fo:table-cell>
          <fo:table-cell>
            <fo:block text-align="left">Kundennummer:</fo:block>
          </fo:table-cell>
          <fo:table-cell margin-left="0">
            <fo:block text-align="right">
              {customerNr}
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row>
          <fo:table-cell>
            <fo:block text-align="left"></fo:block>
          </fo:table-cell>
          <fo:table-cell>
            <fo:block text-align="left">Lieferscheinnummer:</fo:block>
          </fo:table-cell>
          <fo:table-cell margin-left="0">
            <fo:block text-align="right">62185</fo:block>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  }

  def tablePreInfo(date: String, timeFromTo: String): xml.Elem = {
    <fo:table>
      <fo:table-body>
        <fo:table-row>
          <fo:table-cell>
            <fo:block text-align="left">Rechnungsdatum:
              {date}
            </fo:block>
          </fo:table-cell>
          <fo:table-cell>
            <fo:block>Abo-Zeitraum:
              {timeFromTo}
            </fo:block>
          </fo:table-cell>
          <fo:table-cell width="6cm">
            <fo:block></fo:block>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  }

  def Image(url: String, content_width: String): xml.Elem = {
    return <fo:external-graphic src={url} content-width={content_width} margin="0mm" border="0.15mm" border-style="solid" border-color="#4b4b4b"/>
  }

  def invoicePayData(message: String): xml.Elem = {
    var toReturn: xml.Elem = <fo:block font-size="9px" margin-top="1cm" border="dotted 0.3mm black" padding="0.2cm"></fo:block>
    toReturn.copy(
      child = toReturn.child :+ block(message, "left")
    )
  }

  def block(content: String, textAlign: String): xml.Elem = {
    block(content, textAlign, 9)
  }

  def block(content: String, textAlign: String, fontSize: Integer): xml.Elem = {
    block(content, textAlign, fontSize, false)
  }

  def block(content: String, textAlign: String, fontSize: Integer,
            bold: Boolean): xml.Elem = {
    var weight: String = if (bold) "bold" else "normal"
    var xmlString = "<fo:block " +
      "text-align=\"" + textAlign + "\" " +
      "font-size=\"" + fontSize + "\" " +
      "font-weight=\"" + weight + "\">" + "<![CDATA[" +
      content + "]]></fo:block>"
    xml.XML.loadString(xmlString)
  }

  def totalDeliver(total: Boolean): xml.Elem = {
    var whole = if(total) Base64Images.checkBoxChecked() else Base64Images.checkBoxUnChecked()
    var part = if(total) Base64Images.checkBoxUnChecked() else Base64Images.checkBoxChecked()

    var imgWholeString = s"url('data:image/jped;base64,$whole')"
    var imgPartString = s"url('data:image/jped;base64,$part')"

    <fo:table>
      <fo:table-body>
        <fo:table-row>
          <fo:table-cell width="2.5cm">
            <fo:block font-weight="normal" font-size="9" text-align="left">
              Gesamtlieferung:</fo:block>
          </fo:table-cell>
          <fo:table-cell>
            <fo:block>
              <fo:external-graphic
              border-color="#4b4b4b" border-style="solid" border="0.15mm" margin="0mm" content-width="9px" src={imgWholeString}>
              </fo:external-graphic>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell>
            <fo:block font-weight="normal" font-size="9" text-align="left">
              Teillieferung:</fo:block>
          </fo:table-cell>
          <fo:table-cell>
            <fo:block>
              <fo:external-graphic
              border-color="#4b4b4b" border-style="solid" border="0.15mm" margin="0mm" content-width="9px" src={imgPartString}>
              </fo:external-graphic>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  }

  def block(content: String): xml.Elem = {
    block(content, "right")
  }

  def table(header: List[TableCellElement],
            entrys: List[List[TableCellElement]],
            headerBold: Boolean = true): xml.Elem = {
    var toReturn = <fo:table></fo:table>
    if (header.size > 0) {
      toReturn = toReturn.copy(
        child = toReturn.child :+ <fo:table-header text-align="right">
          {tableRow(header, headerBold)}
        </fo:table-header>
      )
    }
    var tableBody = <fo:table-body></fo:table-body>
    for (row <- entrys) {
      tableBody = tableBody.copy(child = tableBody.child :+ tableRow(row))
    }
    toReturn.copy(child = toReturn.child :+ tableBody)
  }

  def tableRow(entrys: List[TableCellElement],
               bold: Boolean = false): xml.Elem = {
    var row = if (bold) <fo:table-row font-weight="bold"></fo:table-row> else <fo:table-row></fo:table-row>
    for (e <- entrys) {
      row = row.copy(
        child = row.child :+ tableCell(e.content, e.border, e.bold, e.textAlign,
          e.width))
    }
    row
  }

  def tableCell(content: String, withBorder: Boolean,
                isBold: Boolean, textAlign: String, width: String): xml.Elem = {
    val font = if (isBold) "bold" else "normal"
    val border = if (withBorder) "solid 0.2mm gray" else ""
    if (width != "") {
      <fo:table-cell border={border} padding="0.5mm" width={width}>
        <fo:block font-weight={font} text-align={textAlign}>
          {content}
        </fo:block>
      </fo:table-cell>
    }
    else {
      <fo:table-cell border={border} padding="0.5mm">
        <fo:block font-weight={font} text-align={textAlign}>
          {content}
        </fo:block>
      </fo:table-cell>
    }
  }

  def lieferschein(emailSupport: String,
                   kigaAddressLine1: String,
                   kigaAddressLine2: String,
                   content: xml.Elem): xml.Elem = {
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">

      <fo:layout-master-set>
        <fo:simple-page-master
        font-family="sans-serif"
        margin="1.5cm"
        margin-bottom="4cm"
        master-name="main"
        page-width="21cm"
        page-height="29.7cm">
          <fo:region-body margin-top="33mm" region-name="xsl-region-body"/>
          <fo:region-before region-name="xsl-region-before"/>
          <fo:region-after region-name="xsl-region-after"/>
        </fo:simple-page-master>
      </fo:layout-master-set>

      <fo:page-sequence master-reference="main" font-size="10">
        <fo:static-content flow-name="xsl-region-before">
          <fo:block margin-left="14cm" font-size="14">
            KiGaPortal
          </fo:block>
          <fo:block font-size="8" margin-top="-6mm">
            {emailSupport}
          </fo:block>
          <fo:table>
            <fo:table-body>
              <fo:table-row>
                <fo:table-cell>
                  <fo:block font-size="7" margin-top="1.3cm">
                    {kigaAddressLine1}
                  </fo:block>
                  <fo:block font-size="7">
                    {kigaAddressLine2}
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:block>
            <fo:leader leader-pattern="rule" leader-pattern-width="5pt" leader-length="6cm"/>
          </fo:block>


        </fo:static-content>

        <fo:static-content flow-name="xsl-region-after">
          <!-- special footer -->
          <fo:retrieve-marker retrieve-class-name="footer" retrieve-position="first-starting-within-page"/>
          <!-- common footer on every page -->{lieferscheinFooter()}<block text-align="center">
          <page-number/>
          /
          <page-number-citation-last ref-id="last-page"/>
        </block>
        </fo:static-content>{content}
      </fo:page-sequence>
    </fo:root>
  }

  def lieferscheinFooter(): xml.Elem = {
    <fo:block font-size="9">
      <fo:leader leader-pattern="rule" leader-pattern-width="5pt" leader-length="100%"/>
      <fo:block></fo:block>
      <fo:block margin-top="2mm">HOHE Medien OG KiGaPortal Klosterwiesgasse 101b, A-8010 Graz</fo:block>
      <fo:block>E-Mail: kundenservice@kigaportal.com</fo:block>
      <fo:leader leader-pattern="rule" leader-pattern-width="5pt" leader-length="100%"/>
    </fo:block>
  }

}
