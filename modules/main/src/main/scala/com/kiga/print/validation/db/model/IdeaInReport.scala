package com.kiga.print.validation.db.model

import javax.persistence.Entity

import com.kiga.db.{KigaEntityModelWithoutId, KigaEntityModel}

import scala.beans.BeanProperty

/**
 * Created by peter on 24.11.15.
 */
@Entity
class IdeaInReport extends KigaEntityModel {
  @BeanProperty var reportId: Long = _
  @BeanProperty var articleId: Long = _
  @BeanProperty var language: String = _
}
