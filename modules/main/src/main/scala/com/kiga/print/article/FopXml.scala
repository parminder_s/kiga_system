package com.kiga.print.article

import java.io._
import javax.xml.transform.TransformerFactory
import javax.xml.transform.sax.SAXResult
import javax.xml.transform.stream.StreamSource

import com.kiga.print.model.PrintObject
import org.apache.fop.apps.FopFactory
import org.apache.xmlgraphics.util.MimeConstants
import org.slf4j.LoggerFactory

/**
 * Created by peter on 29.06.15.
 */
object FopXml {

  val log = LoggerFactory.getLogger(getClass)

  def generateStaticA4(xml_content: xml.Node): xml.Elem = {
    val img_urls = xml_content \ "div" \ "div" \ "p" \ "img" \\ "@src"
    //img_urls.foreach(println)
    var content = XmlTemplates.emptyBlock();
    for (url <- img_urls) {
      content.child :+ XmlTemplates.a4Site(url.toString)
    }
    XmlTemplates.a4(content)
  }

  def generateStaticOld(idea: xml.Node): xml.Elem = {
    var xml_block = <block font-size="0">
      <block margin-top="1.99mm" font-size="0"/>
    </block>
    var divs = idea \ "div" \ "div"
    var count = 0
    var lastWasSpan2 = false
    //println(divs.length)
    for (div <- divs) {
      //println(div)
      val classes = processClasses((div \ "@class").toString)
      if (classes.span == 2) {
        if (lastWasSpan2 == false) {
          lastWasSpan2 = true;
        }
        else {
          //generating table with 2 images  (this and the last one)
          xml_block = xml_block.copy(child = xml_block.child :+ staticGenerateTable2Images((divs(count - 1) \\ "img" \ "@src").text, (div \\ "img" \ "@src").text))
          lastWasSpan2 = false
        }
      }
      else if (classes.span == 4 || classes.span == 3) {
        //generating table with 1 image
        var url = (div \\ "img" \ "@src").text
        if (url != "") {
          xml_block = xml_block.copy(child = xml_block.child :+ staticGenerateTable1Images(url))
        }
      }
      count = count + 1
    }
    //generate one row with one image on the left, if last div was a span 2
    if (lastWasSpan2) {
      xml_block = xml_block.copy(child = xml_block.child :+ staticGenerateTable2Images((divs(divs.length - 1) \\ "img" \ "@src").text, ""))
    }
    //println(XmlTemplates.staticOld(xml_block))
    return XmlTemplates.staticOld(xml_block)
  }

  def staticGenerateTable2Images(url_first: String, url_second: String): xml.Elem = {
    var xml_first = XmlTemplates.staticImageInTableWith2(url_first)
    var xml_second = XmlTemplates.staticImageInTableWith2(url_second)
    if (url_second == "") {
      xml_second = XmlTemplates.emptyBlock()
    }
    if (url_first == "") {
      xml_first = XmlTemplates.emptyBlock()
      println("Should not happen:GenerateTable2Images in Static Old, both urls = \"\"")
    }
    return XmlTemplates.staticTableWith2Images(xml_first, xml_second)
  }

  def staticGenerateTable1Images(url_first: String): xml.Elem = {
    return XmlTemplates.staticTableWith1Image(url_first)
  }

  def processClasses(classes: String): CssClasses = {
    var marginBottom = "0mm"
    var span = -1
    for (tmp <- classes.toString.split(' ')) {
      tmp match {
        case "zeilenabstand1" => marginBottom = "1mm"
        case "zeilenabstand2" => marginBottom = "2mm"
        case "zeilenabstand3" => marginBottom = "4mm"
        case "zeilenabstand4" => marginBottom = "5mm"
        case "zeilenabstand5" => marginBottom = "6mm"
        case "span-1" => span = 1
        case "span-2" => span = 2
        case "span-3" => span = 3
        case "span-4" => span = 4
        case default => println("Not supported CSS class:[" + tmp + "]")
      }
    }
    CssClasses(marginBottom, span)
  }

  def generateDynamic(idea: xml.Node, title: String, withImages: Boolean): xml.Elem = {
    var divs = idea \ "div" \ "div"
    var count = 0
    var xml_flow = <flow flow-name="xsl-region-body"></flow>
    var lastWasSpan1 = 0
    var lastWasSpan2 = false
    for (div <- divs) {
      // println(div)

      var classes = processClasses((div \ "@class").toString)
      if (lastWasSpan2) {
        xml_flow = xml_flow.copy(child = xml_flow.child :+ FopXml.dynamicGenerateSpan2(divs(count - 1), div, true))
        lastWasSpan2 = false;
      }
      else {
        classes.span match {
          case 1 => {
            lastWasSpan1 = lastWasSpan1 + 1
            if (lastWasSpan1 == 4) {
              xml_flow = xml_flow.copy(child = xml_flow.child :+ FopXml.dynamicGenerateSpan1(divs(count - 2), divs(count - 1), divs(count), true))
              lastWasSpan1 = 0
            }
          }
          case 2 => {
            if (lastWasSpan2) {
              xml_flow = xml_flow.copy(child = xml_flow.child :+ FopXml.dynamicGenerateSpan2(divs(count - 1), div, true))
              lastWasSpan2 = false
            }
            if (lastWasSpan1 == 2) {
              lastWasSpan1 = 0;
              xml_flow = xml_flow.copy(child = xml_flow.child :+ FopXml.dynamicGenerateSpan112(divs(count - 1), div))
            }
            else {
              lastWasSpan2 = true
              lastWasSpan1 = 0
            }
          }
          case 3 => {
            //contains image, layout changes! image is left, span3 is right of it
            if ((divs(count - 1) \\ "img" \ "@src").isEmpty == false) {
              xml_flow = xml_flow.copy(child = xml_flow.child :+ FopXml.dynamicGenerateSpan13Image(divs(count - 1), div, withImages))
            }
            //contains no images, normal layout, span1 is heading, span3 in content (under span1)
            else {
              xml_flow = xml_flow.copy(child = xml_flow.child :+ FopXml.dynamicGenerateSpan1(divs(count - 1), withImages))
              xml_flow = xml_flow.copy(child = xml_flow.child :+ FopXml.dynamicGenerateSpan3(div, withImages))
            }
            lastWasSpan1 = 0
          }
          case 4 => {
            xml_flow = xml_flow.copy(child = xml_flow.child :+ dynamicGenerateSpan4(div))
          }

        }
      }
      count = count + 1
    }
    println(XmlTemplates.dynamicFlow(xml_flow, title))
    return XmlTemplates.dynamicFlow(xml_flow, title)
  }

  def dynamicGenerateSpan1(xml_content: xml.Node, withImages: Boolean): xml.Elem = {
    XmlTemplates.dynamicTable(processChilds(xml_content, 1))
  }

  def dynamicGenerateSpan13Image(image: xml.Node, content: xml.Node, withImages: Boolean): xml.Elem = {
    XmlTemplates.dynamicSpan12(processChilds(image, 1), processChilds(content, 2))
  }

  def dynamicGenerateSpan1(first: xml.Node, second: xml.Node, third: xml.Node, withImages: Boolean): xml.Elem = {
    XmlTemplates.dynamicSpan1(
      processChilds(first, 1),
      processChilds(second, 1),
      processChilds(third, 1)
    )
  }

  def dynamicGenerateSpan112(first: xml.Node, second: xml.Node): xml.Elem = {
    XmlTemplates.dynamicSpan12(processChilds(first, 1), processChilds(second, 2))
  }

  def dynamicGenerateSpan2(left: xml.Node, right: xml.Node, withImages: Boolean): xml.Elem = {
    XmlTemplates.dynamicSpan2(processChilds(left, 2), processChilds(right, 2))
    /*var content_left = XmlTemplates.dynamicInliner(left.text)
    var content_right = XmlTemplates.dynamicInliner(right.text)
    if(withImages == true) {
      var img_url = left \\ "img" \ "@src"
      if (img_url.text != "") {
        content_left = XmlTemplates.dynamicImage(img_url.text,"57.5mm")
      }
      img_url = right \\ "img" \ "@src"
      if (img_url.text != "") {
        content_right = XmlTemplates.dynamicImage(img_url.text,"57.5mm")
      }
    }
    return XmlTemplates.dynamicSpan2(content_left,content_right)*/
  }

  def dynamicGenerateSpan3(xml_content: xml.Node, withImages: Boolean): xml.Elem = {
    return XmlTemplates.dynamicTable(processChilds(xml_content, 3))
  }

  def dynamicGenerateSpan4(xml_content: xml.Node): xml.Elem = {
    XmlTemplates.dynamicTable(processChilds(xml_content, 4))
  }

  def processChilds(xml_content: xml.Node, span: Int): xml.Elem = {
    //    if (xml_content.text.contains("Fotos")) {
    //      var x = xml_content.text
    //      var z = 3
    //    }
    var content = XmlTemplates.emptyBlock()
    for (child <- xml_content.child) {
      if (child.label != "#PCDATA") {
        var classes = processClasses((child \ "@class").toString)
        var tmpContent = child.label match {
          case "hr" => processHr()
          case "h1" => processH(child, 1)
          case "h2" => processH(child, 2)
          case "h3" => processH(child, 3)
          case "h4" => processH(child, 4)
          case "h5" => processH(child, 5)
          case "h6" => processH(child, 6)
          case "img" => processImg(child, span)
          case "p" => processP(child, classes, span)
          case "ul" => processUl(child, span)
          case default => XmlTemplates.emptyBlock()
        }
        content = content.copy(child = content.child :+ tmpContent)
      }
    }
    content
  }

  def processHr(): xml.Elem = {
    XmlTemplates.dynamicHr()
  }

  def processEm(xml_content: xml.Node): xml.Elem = {
    var elem = XmlTemplates.emptyInline()
    for (innerChild <- xml_content.child) {
      println(innerChild.toString())
      innerChild.label match {
        case "a" => elem = elem.copy(child = elem.child :+ XmlTemplates.dynamicInliner(innerChild.text))
        case "strong" => elem = elem.copy(child = elem.child :+ processStrong(innerChild))
        case "#PCDATA" => elem = elem.copy(child = elem.child :+ XmlTemplates.dynamicEm(innerChild.text))
        case "br" => println("todo") //todo
      }
    }
    elem
  }

  def processStrong(xml_content: xml.Node): xml.Elem = {
    var elem = XmlTemplates.emptyInline()
    for (innerChild <- xml_content.child) {
      println(innerChild.toString())
      innerChild.label match {
        case "a" => elem = elem.copy(child = elem.child :+ XmlTemplates.dynamicInliner(innerChild.text))
        case "em" => elem = elem.copy(child = elem.child :+ processEm(innerChild))
        case "#PCDATA" => elem = elem.copy(child = elem.child :+ XmlTemplates.dynamicPStrong(innerChild.text))
      }
    }
    elem
  }

  def processP(xml_content: xml.Node, classes: CssClasses, parantSpan: Int): xml.Elem = {
    println(xml_content)
    var styles = processCssStyles((xml_content \ "@style").toString)
    var x = xml_content.text
    var elem = XmlTemplates.emptyBlock(classes.marginBottom, styles)
    for (innerChild <- xml_content.child) {
      innerChild.label match {
        case "a" => {
          elem = elem.copy(child = elem.child :+ XmlTemplates.dynamicInliner(innerChild.text))
        }
        case "br" => {
          println("br in p,not implemented")
        }
        case "img" => {
          elem = elem.copy(child = elem.child :+ processImg(innerChild, parantSpan))
        }
        case "strong" => {
          elem = elem.copy(child = elem.child :+ processStrong(innerChild))
        }
        case "span" => {
          elem = elem.copy(child = elem.child :+ processSpanInP(innerChild))
        }
        case "em" => {
          elem = elem.copy(child = elem.child :+ processEm(innerChild))
        }
        case "#PCDATA" => elem = elem.copy(child = elem.child :+ XmlTemplates.dynamicInliner(innerChild.text))

      }
    }
    println(elem.toString)
    elem
  }

  def processSpanInP(xml_content: xml.Node): xml.Elem = {
    var elem = XmlTemplates.emptyInline()
    for (innerChild <- xml_content.child) {
      innerChild.label match {
        case "br" => {
          "not implemented br in span in p"
        }
        case "a" => {
          elem = elem.copy(child = elem.child :+ XmlTemplates.dynamicInliner(innerChild.text))
        }
        case "em" => {
          elem = elem.copy(child = elem.child :+ XmlTemplates.dynamicEm(innerChild.text))
        }
        case "strong" => {
          elem = elem.copy(child = elem.child :+ XmlTemplates.dynamicPStrong(innerChild.text))
        }
        case "#PCDATA" => {
          elem = elem.copy(child = elem.child :+ XmlTemplates.dynamicInliner(innerChild.text))
        }
      }
    }
    elem
  }

  def processH(xml_content: xml.Node, hType: Int): xml.Elem = {
    var elem = hType match {
      case 1 => XmlTemplates.dynamicH1(xml_content.text)
      case 2 => XmlTemplates.dynamicH2(xml_content.text)
      case 3 => XmlTemplates.dynamicH3(xml_content.text)
      case 4 => XmlTemplates.dynamicH3(xml_content.text)
      case 5 => XmlTemplates.dynamicH3(xml_content.text)
      case 6 => XmlTemplates.dynamicH3(xml_content.text)
    }
    elem
  }

  def processUl(xml_content: xml.Node, span: Int): xml.Elem = {
    var elem = XmlTemplates.dynamicListBlock()
    println(xml_content)
    for (innerChild <- xml_content.child) {
      //#PCDATA is a not li child which is coming up here!
      innerChild.label match {
        case "li" => {
          //println(innerChild)
          //println(innerChild.child)
          var classes = processClasses((innerChild \ "@class").toString)
          if (innerChild.child.size == 0) {
            elem = elem.copy(child = elem.child :+ XmlTemplates.dynamicListItem(innerChild.text, classes.marginBottom))
          }
          else {
            var innerElem = XmlTemplates.emptyBlock()
            for (inLi <- innerChild.child) {
              var inLiClasses = processClasses((inLi \ "@class").toString)
              var inListyles = processCssStyles((xml_content \ "@styles").toString)
              inLi.label match {
                case "strong" => innerElem = innerElem.copy(child = innerElem.child :+ XmlTemplates.dynamicPStrong(inLi.text))
                case "span" => innerElem = innerElem.copy(child = innerElem.child :+ XmlTemplates.dynamicInliner(inLi.text))
                case "p" => innerElem = innerElem.copy(child = innerElem.child :+ processP(inLi, CssClasses("0mm", classes.span), span)) //setting maring to 0mm, FOP-BUG https://issues.apache.org/jira/browse/FOP-2461
                case "a" => innerElem = innerElem.copy(child = innerElem.child :+ XmlTemplates.dynamicInliner(inLi.text))
                case "#PCDATA" => innerElem = innerElem.copy(child = innerElem.child :+ XmlTemplates.dynamicInliner(inLi.text))
              }
            }
            elem = elem.copy(child = elem.child :+ XmlTemplates.dynamicListItem(innerElem, classes.marginBottom))
          }
        }
        case "#PCDATA" => println("ignoring")
      }
    }
    elem
  }

  def processImg(xml_content: xml.Node, span: Int): xml.Elem = {
    span match {
      case 1 => XmlTemplates.dynamicImage((xml_content \ "@src").toString, XmlTemplates.CONTENT_WIDTH_SPAN_1)
      case 2 => XmlTemplates.dynamicImage((xml_content \ "@src").toString, XmlTemplates.CONTENT_WIDTH_SPAN_2)
      case 3 => XmlTemplates.dynamicImage((xml_content \ "@src").toString, XmlTemplates.CONTENT_WIDTH_SPAN_3)
      case 4 => XmlTemplates.dynamicImage((xml_content \ "@src").toString, XmlTemplates.CONTENT_WIDTH_SPAN_4)
    }
  }

  def processCssStyles(stylesString: String): CssStyles = {

    var textAlign = "left"
    if (stylesString != "") {
      for (styleEntry <- stylesString.split(";")) {
        val values = styleEntry.split(":")
        values(0) match {
          case "text-align" => textAlign = values(1)
        }
      }
    }
    CssStyles(textAlign)
  }

  def print(obj: PrintObject, fileName: String) = {
    createFile(getFopStream(obj.getFop().toString()), System.getProperty("java.io.tmpdir") + "/" + fileName)
  }

  def print(fopXml: String, fileName: String) = {
    createFile(getFopStream(fopXml), System.getProperty("java.io.tmpdir") + "/" + fileName)
  }

  def getFopStream(fopXml :String): ByteArrayOutputStream = {
    //Step 1

    //var fopFactory = FopFactory.newInstance(new File("/usr/share/doc/fop/fop.xconf"));

    var fopFactory = FopFactory.newInstance(getClass.getResource(
      "/print/fop.xconf").toURI)
    //Step 2
    var out = new ByteArrayOutputStream()

    //Step 3
    var fop = fopFactory.newFop(MimeConstants.MIME_PDF, out)

    //Step 4
    var factory = TransformerFactory.newInstance()
    var transformer = factory.newTransformer()

    //Step 5
    var src = new StreamSource(new StringReader(fopXml))
    var res = new SAXResult(fop.getDefaultHandler());

    //Step 6
    transformer.transform(src, res);
    out
  }

  def createFile(outStream: ByteArrayOutputStream,filePath: String) = {
    log.info(s"Creating pdf, file:$filePath")
    val fileOutputStream = new  FileOutputStream(filePath)
    outStream.writeTo(fileOutputStream)
    outStream.close()
    fileOutputStream.close()
    filePath
  }

  def createPDF(dst_file: String, fop_xml: xml.Elem) = {
    createFile(getFopStream(fop_xml.toString()),System.getProperty("java.io.tmpdir") + "/" + dst_file)
  }

}
