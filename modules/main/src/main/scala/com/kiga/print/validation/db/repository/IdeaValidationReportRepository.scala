package com.kiga.print.validation.db.repository

import java.util.Date

import com.kiga.print.validation.db.model.IdeaValidationReport
import org.springframework.data.repository.CrudRepository

/**
 * Created by peter on 08.09.15.
 */
trait IdeaValidationReportRepository extends CrudRepository[IdeaValidationReport, java.lang.Long] {
  def findAllByStartDateTimeGreaterThanAndEndDateTimeLessThan(startDateTime: Date, endDateTime: Date): java.util.List[IdeaValidationReport]

  def findAllByStartDateTimeGreaterThan(startDateTime: Date): java.util.List[IdeaValidationReport]

  def findAllByStatus(status: Int): java.util.List[IdeaValidationReport]
}
