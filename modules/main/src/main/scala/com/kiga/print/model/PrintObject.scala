package com.kiga.print.model


import scala.beans.BeanProperty
import scala.xml.Elem

/**
  * Created by peter on 23.02.16.
  */
class PrintObject( @BeanProperty var header: List[Printable],
                   @BeanProperty var body: List[Printable],
                   @BeanProperty var footer: List[Printable],
                   withPageNumbers: Boolean = false) {

  def getFop():xml.Elem = {
    return (
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
      <fo:layout-master-set>
        <fo:simple-page-master font-family="sans-serif" margin="1.5cm" margin-bottom="4cm" master-name="main" page-width="21cm" page-height="29.7cm">
          <fo:region-body margin-top="33mm" region-name="xsl-region-body"/>
          <fo:region-before region-name="xsl-region-before"/>
          <fo:region-after region-name="xsl-region-after"/>
        </fo:simple-page-master>
      </fo:layout-master-set>

      <fo:page-sequence master-reference="main" font-size="10">
        <fo:static-content flow-name="xsl-region-before">
          {header.map(_.getFop())}
        </fo:static-content>
        <fo:static-content flow-name="xsl-region-after">
          {footer.map(_.getFop())}

          {if(withPageNumbers){
          <fo:block text-align="right">
            <fo:page-number/>/<fo:page-number-citation-last ref-id="last-page"/>
          </fo:block>
          }
          }

        </fo:static-content>

        <fo:flow flow-name="xsl-region-body">
          {body.map(_.getFop())}
          {if(withPageNumbers){
          <fo:block id="last-page"></fo:block>
        }
          }
        </fo:flow>

      </fo:page-sequence>
    </fo:root>
      )
  }
}
