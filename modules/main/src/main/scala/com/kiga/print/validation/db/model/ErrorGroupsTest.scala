package com.kiga.print.validation.db.model

import javax.persistence.Entity

import com.kiga.db.{KigaEntityModelWithoutId, KigaEntityModel}
import com.kiga.types.{JBoolean, JLong}

import scala.beans.BeanProperty

/**
 * Created by peter on 08.09.15.
 */

@Entity
class ErrorGroupsTest extends KigaEntityModel {
  @BeanProperty var errorGroup: Integer = _
  @BeanProperty var articles: JLong = _
  @BeanProperty var correctable: JBoolean = _
}
