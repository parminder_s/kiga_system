package com.kiga.print.validation.web

import java.io.FileInputStream
import javax.inject.Inject
import javax.servlet.http.HttpServletResponse

import com.kiga.content.domain.ss.property.LayoutType
import com.kiga.content.repository.ss.draft._
import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive
import com.kiga.main.locale.Locale
import com.kiga.print.article.IdeaToPdf
import com.kiga.print.validation.ValidateXML
import com.kiga.print.validation.db.model.{ErrorGroup, IdeaValidationReport}
import com.kiga.print.validation.db.repository.{IdeaErrorRepository, IdeaInReportRepository, IdeaValidationReportRepository}
import com.kiga.print.validation.domain.IdeaError
import com.kiga.types._
import com.kiga.web.message.EndpointRequest
import org.apache.commons.io.IOUtils
import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.web.bind.annotation._

import scala.beans.BeanProperty
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

/**
 * Created by peter on 14.09.15.
 */
@ConfigurationProperties("translation")
@RestController
@RequestMapping(Array("/reports/validation"))
class ValidationController {

  @BeanProperty var locales: JMap[String, String] = _

  val log = LoggerFactory.getLogger(getClass)
  @Inject var validateXML: ValidateXML = _
  @Inject var ideaErrorRepository: IdeaErrorRepository = _
  @Inject var ideaValidationReportRepository: IdeaValidationReportRepository = _
  @Inject var ideaInReportRepository: IdeaInReportRepository = _
  @Inject var siteTreeTranslationGroupsRepository: SiteTreeTranslationGroupsRepository = _
  @Inject var ideaToPdf: IdeaToPdf = _
  @Inject var kigaIdearepositoryDaft: KigaIdeaRepositoryDraft = _

  @RequestMapping(value = Array("/reportList"), method = Array(RequestMethod.POST))
  @ResponseBody
  def reportList(@RequestBody reportsReq: ReportsReq) = {
    var startDate = DateTime.parse(reportsReq.start).toDate
    var endDate = DateTime.parse(reportsReq.end).toDate
    var reports = if (reportsReq.onlyFinished) {
      ideaValidationReportRepository.findAllByStartDateTimeGreaterThanAndEndDateTimeLessThan(startDate, endDate)
    } else {
      ideaValidationReportRepository.findAllByStartDateTimeGreaterThan(startDate)
    }
    reports
  }

  @RequestMapping(value = Array("/print/{ideaId}"), method = Array(RequestMethod.GET))
  @ResponseBody
  def getPrint(@PathVariable("ideaId") ideaId: Int, response: HttpServletResponse) = {
    ideaToPdf.print(new JLong(ideaId))
    val fileName = s"/tmp/$ideaId.pdf"
    var file = new FileInputStream(fileName)
    response.setContentType("application/pdf")
    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"")
    IOUtils.copy(file, response.getOutputStream())
    file.close()
    response.getOutputStream().flush()
  }

  @RequestMapping(value = Array("/printPreview"), method = Array(RequestMethod.POST))
  @ResponseBody
  def printPreview(@RequestBody printReq: PrintReq, response: HttpServletResponse) = {
    if (printReq.articleId != -1) {
      val article = kigaIdearepositoryDaft.findOne(new JLong(printReq.articleId))
      article.getLayoutType match {
        case LayoutType.STATIC_OLD => {
          ideaToPdf.printStaticOld(article)
          println("static Old")
        }
        case LayoutType.DYNAMIC => {
          ideaToPdf.printDynamic(article, printReq.withImages)
          println("Dynamic")
        }
        case LayoutType.A4_PDF => {
          ideaToPdf.printStaticA4(article)
          println("staticA4")
        }
      }
      val flieName = ("/tmp/" + printReq.articleId + ".pdf")
      ideaToPdf.printDynamic(kigaIdearepositoryDaft.findOne(new JLong(printReq.articleId)), true)
      var file = new FileInputStream(flieName);
      response.setContentType("application/pdf")
      response.setHeader("Content-Disposition", "attachment; filename=\""
        + flieName + "\"");
      IOUtils.copy(file, response.getOutputStream())
      file.close()
      response.getOutputStream().flush()
    }
  }

  @RequestMapping(value = Array("/startValidation"), method = Array(RequestMethod.POST))
  @ResponseBody
  def startValidation(@RequestBody startNewReportReq: StartNewReportReq) = {
    log.info("ValidationController startValidation")
    if (startNewReportReq.getAll() == true) {
      validateXML.validateAll(startNewReportReq.autoCorrect, startNewReportReq.autoCorrect)
    } else {
      validateXML.validateAll(startNewReportReq.autoCorrect,
        startNewReportReq.autoCorrect,
        startNewReportReq.getIds().split(",").map(new JLong(_)).toList)
    }
    log.info("ValidationController startValidation end")
    true
  }

  @RequestMapping(value = Array("languages"), method = Array(RequestMethod.POST))
  @ResponseBody
  def languages() = {
    locales
  }

  @RequestMapping(value = Array("/reportDetail"), method = Array(RequestMethod.POST))
  @ResponseBody
  def reportDetail(@RequestBody detailReq: DetailReq) = {
    log.debug("T1:" + detailReq.language + "/" + detailReq.reportId)
    val list = if (detailReq.language == "All") {
      ideaErrorRepository.countAllErrorGroupsByIdeaValidationReportId(detailReq.reportId)
    }
    else {
      ideaErrorRepository.countAllErrorGroupsByIdeaValidationReportIdAndLocale(detailReq.reportId, detailReq.language)
    }
    ValidationDetailResponse(list.asScala.toList, ideaValidationReportRepository.findOne(new JLong(detailReq.reportId)))
  }

  @RequestMapping(value = Array("/ideaList"), method = Array(RequestMethod.POST))
  @ResponseBody
  def ideaList(@RequestBody languageReq: LanguageReq) = {
    if (ErrorGroup(languageReq.error) == ErrorGroup.AllErrors) {
      if (languageReq.language == "All") {
        ideaErrorRepository.countAllErrorsByIdeaValidationReportId(languageReq.reportId)
      }
      else {
        ideaErrorRepository.countAllErrorsByIdeaValidationReportIdAndLocale(languageReq.reportId, languageReq.language)
      }
    }
    else {
      if (languageReq.language == "All") {
        ideaErrorRepository.countAllErrorsByIdeaValidationReportIdAndErrorGroup(languageReq.reportId, languageReq.error)
      }
      else {
        ideaErrorRepository.countAllErrorsByIdeaValidationReportIdAndErrorGroupAndLocale(languageReq.reportId, languageReq.error, languageReq.language)
      }
    }
  }

  @RequestMapping(value = Array("/ideaDetail"), method = Array(RequestMethod.POST))
  @ResponseBody
  def ideaDetail(@RequestBody articleReq: ArticleReq) = {
    val errors = ideaErrorRepository.findAllByIdeaValidationReportIdAndArticleId(articleReq.reportId, articleReq.articleId).toList
    val transgroup = siteTreeTranslationGroupsRepository.findByOriginalId(new JLong(articleReq.articleId))
    val title = kigaIdearepositoryDaft.findOne(new JLong(articleReq.articleId)).getTitle()
    val translationIds = siteTreeTranslationGroupsRepository.findAllByTranslationGroupId(transgroup.getTranslationGroupId).map(
      _.getOriginalId).toList
    var filteredTranslationIds = List[JLong]()
    for(id <- translationIds){
      val tmp = kigaIdearepositoryDaft.findOne(new JLong(id))
      if(tmp != null){
        filteredTranslationIds = filteredTranslationIds ++ List[JLong](id)
      }
    }
    val translationLanguage = for (id <- filteredTranslationIds) yield kigaIdearepositoryDaft.findOne(new JLong(id)).getLocale()
    //ValidationArticleResponse(errors.toList, translationIds, translationLanguage.toList)
    var languageDetail = List[ValidationLanguageResponseEntry]()
    for (id <- filteredTranslationIds) {
      log.debug("create entry for: " + id + " in articleDetail!")
      val report = new IdeaValidationReport()
      report.setId(id)
      report.setAutocorrected(ideaErrorRepository.countByIdeaValidationReportIdAndArticleIdAndAutoCorrected(articleReq.reportId, id, true))
      report.setAutocorrectable(ideaErrorRepository.countByIdeaValidationReportIdAndArticleIdAndAutoCorrectableOrIdeaValidationReportIdAndArticleIdAndAutoCorrected(
        articleReq.reportId,
        id,
        true,
        articleReq.reportId,
        id,
        true))
      report.setManual(ideaErrorRepository.countByIdeaValidationReportIdAndArticleIdAndAutoCorrectableAndAutoCorrected(articleReq.reportId,
        id,
        false,
        false))
      report.setAllErrors(ideaErrorRepository.countByIdeaValidationReportIdAndArticleId(articleReq.reportId, id))
      val tmp = ValidationLanguageResponseEntry(report, kigaIdearepositoryDaft.findOne(new JLong(id)).getLocale())
      languageDetail = languageDetail ::: List[ValidationLanguageResponseEntry](tmp)
    }
    ValidationArticleResponse(errors, languageDetail, title)
  }
}

class ArticleReq extends EndpointRequest {
  @BeanProperty var reportId: Int = -1
  @BeanProperty var articleId: Int = -1
}

class PrintReq extends EndpointRequest {
  @BeanProperty var articleId: Int = -1
  @BeanProperty var withImages: Boolean = true
}

class DetailReq extends EndpointRequest {
  @BeanProperty var reportId: Int = -1
  @BeanProperty var language: String = "All"
}

class LanguageReq extends EndpointRequest {
  @BeanProperty var reportId: Int = -1
  @BeanProperty var language: String = ""
  @BeanProperty var error: Int = -1
}

class ReportsReq extends EndpointRequest {
  @BeanProperty var start: String = ""
  @BeanProperty var end: String = ""
  @BeanProperty var onlyFinished: Boolean = false
}

class StartNewReportReq extends EndpointRequest {
  @BeanProperty var all: Boolean = false
  @BeanProperty var autoCorrect: Boolean = false
  @BeanProperty var ids: String = ""
}

case class ValidationLanguageResponseEntry
(
  report: IdeaValidationReport,
  language: Locale
)

case class ValidationDetailResponse(list: JList[Object],
                                    report: IdeaValidationReport)

case class ValidationArticleResponse(errors: List[IdeaError],
                                     translation: List[ValidationLanguageResponseEntry],
                                     title: String)
