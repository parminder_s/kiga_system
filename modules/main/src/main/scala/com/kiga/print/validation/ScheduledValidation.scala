package com.kiga.print.validation

import javax.inject.Inject

import org.slf4j.LoggerFactory
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.scheduling.support.CronTrigger

/**
 * Created by peter on 13.10.15.
 */
class ScheduledValidation(cronExpression: String) extends Runnable {

  @Inject var validateXML: ValidateXML = _
  val log = LoggerFactory.getLogger(getClass)
  var scheduler: ThreadPoolTaskScheduler = new ThreadPoolTaskScheduler()
  scheduler.afterPropertiesSet()
  scheduler.schedule(this, new CronTrigger(cronExpression))

  def run() = {
    log.debug("started validation at " + org.joda.time.DateTime.now.toString("dd/MM/yyyy HH:mm:ss"))
    validateXML.validateAll(true, true)
  }
}
