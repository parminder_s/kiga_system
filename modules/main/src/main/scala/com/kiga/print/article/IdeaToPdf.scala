package com.kiga.print.article

import javax.inject.{Inject, Named}

import com.kiga.content.ContentConfig
import com.kiga.content.domain.ss.KigaIdea
import com.kiga.content.domain.ss.live.KigaIdeaLive
import com.kiga.content.domain.ss.property.LayoutType
import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive
import com.kiga.print.autocorrect.AutoCorrect
import com.kiga.print.validation.ValidationHelper
import com.kiga.types.JLong
import org.apache.commons.lang3.StringEscapeUtils
import org.slf4j.LoggerFactory
import org.springframework.boot.test.SpringApplicationConfiguration
import org.xml.sax.SAXException

//import org.apache.fop.apps.MimeConstants

/**
 * Created by peter on 19.06.15.
 * Translate document content into Apache FOP xml elements.
 * TODO : maybe move to Printable-based classes (print.model.Printable)
 *        instead of this module (with many cases, ifs etc).
 */
@Named
@SpringApplicationConfiguration(classes = Array(classOf[ContentConfig]))
class IdeaToPdf {

  val log = LoggerFactory.getLogger(getClass)
  @Inject var kigaIdeaRepository: KigaIdeaRepositoryLive = _

  /*Layout types in ideas:
  * Statisch Alt:1
  * Dynamisch:2
  * A4 PDF:3
  * */
  def print(ideaId: JLong, withImages: Boolean = true) = {
    var idea = kigaIdeaRepository.findOne(ideaId)
    idea.getLayoutType match {
      case LayoutType.STATIC_OLD => printStaticOld(idea)
      case LayoutType.DYNAMIC => printDynamic(idea, withImages)
      case LayoutType.A4_PDF => printStaticA4(idea)
    }
  }

  def printStaticA4(idea: KigaIdea[_, _, _ , _]) = {
    FopXml.createPDF(idea.getId + ".pdf", FopXml.generateStaticA4(xml.XML.loadString(loadXML(ValidationHelper.prettyFormatXML(idea.getContent), idea.getId().toString(), idea.getTitle()))))
  }

  def printStaticOld(idea: KigaIdea[_, _, _ , _]) = {
    FopXml.createPDF(idea.getId + ".pdf", FopXml.generateStaticOld(xml.XML.loadString(loadXML(ValidationHelper.prettyFormatXML(idea.getContent), idea.getId().toString(), idea.getTitle()))))
  }

  def loadXML(xml_text: String, contId: String, contTitle: String): String = {
    var string = ""
    var xml = <a></a>
    string = StringEscapeUtils.unescapeHtml4(xml_text.replace("&nbsp;", ""))
    //unescapeHtml unescapes the &amp;, but it is also used in xml
    string = string.replace("&", "&amp;")
    //println(string)

    // inserting \n after <hr/>, so that line numbers are equalt to the line numbers in silverstripe
    string = string.replace("<hr/>", "<hr/>\n")
    //removing last < hr />
    string = string.reverse
    string = string.replaceFirst(">/rh<", ">/p<")
    string = string.reverse
    try {
      xml = scala.xml.XML.loadString(ValidationHelper.prettyFormatXML(string))
    }
    catch {
      case sax: SAXException => sax.printStackTrace()
      case e: Exception => e.printStackTrace()
    }
    var newXml = xml

    newXml.toString()
  }

  def printDynamic(idea: KigaIdea[_, _, _ , _], withImages: Boolean) = {
    FopXml.createPDF(idea.getId() + ".pdf", FopXml.generateDynamic(xml.XML.loadString(loadXML(AutoCorrect.validateSpaceAndSpecialCharacters(idea.getContent)._1, idea.getId().toString(), idea.getTitle())),
      idea.getTitle(),
      withImages)
    )
  }

  def printVoucher(fileName: String, from: String, to: String, message: String, code: String, hint: String, imgPath: String) = {
    FopXml.createPDF(fileName, XmlTemplates.voucher(from, to, message, code, hint, imgPath))
  }
}
