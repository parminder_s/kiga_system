package com.kiga.print.model

import scala.beans.BeanProperty

/**
  * Created by peter on 24.02.16.
  */
class Leader extends Printable{

  @BeanProperty var leaderPattern: String = null
  @BeanProperty var leaderPatternWidthPT: Integer = null
  @BeanProperty var leaderLenghtMM: Integer = null
  @BeanProperty var leaderLenghtPercent: Integer = null
  override def getFop(): xml.Elem = {
    if(leaderLenghtPercent == null) {
      <fo:leader leader-pattern={attributeToString(Option(leaderPattern))} leader-pattern-width={attributeToString(Option(leaderPatternWidthPT), "pt")} leader-length={attributeToString(Option(leaderLenghtMM), "mm")}></fo:leader>
    }
    else{
      <fo:leader leader-pattern={attributeToString(Option(leaderPattern))} leader-pattern-width={attributeToString(Option(leaderPatternWidthPT), "pt")} leader-length={attributeToString(Option(leaderLenghtPercent), "%")}></fo:leader>
    }
  }

}
