package com.kiga.print.validation

import java.io.{IOException, StringReader}
import javax.inject.{Inject, Named}
import javax.xml.XMLConstants
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory

import com.kiga.content.domain.ss.KigaPageImage
import com.kiga.content.domain.ss.draft.KigaIdeaDraft
import com.kiga.content.repository.ss.draft._
import com.kiga.print.autocorrect._
import com.kiga.print.validation.db.model.ErrorGroup.ErrorGroup
import com.kiga.print.validation.db.model.{ErrorGroup, IdeaValidationReport}
import com.kiga.print.validation.db.repository.{IdeaErrorRepository, IdeaValidationReportRepository}
import com.kiga.print.validation.domain.IdeaError
import com.kiga.s3.repository.ss.S3FileRepository
import com.kiga.types.{JList, JLong}
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.methods.HttpHead
import org.apache.http.impl.client.HttpClients
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.scheduling.annotation.Async
import org.springframework.transaction.annotation.Transactional
import org.xml.sax.{SAXException, SAXParseException}

import scala.beans.BeanProperty
import scala.collection.JavaConversions._
/**
  * Created by peter on 08.09.15.
  */
@Named
@ConfigurationProperties("print")
class DefaultValidateXML extends ValidateXML with ApplicationListener[ContextRefreshedEvent] {
  val schemaFactory = SchemaFactory.newInstance(
    XMLConstants.W3C_XML_SCHEMA_NS_URI)
  val schema = schemaFactory.newSchema(
    getClass.getResource("/article_schema.xsd"))
  val log = LoggerFactory.getLogger(getClass)

  @BeanProperty var canSaveToDb: Boolean = _

  @Inject var kigaIdeaRepository: KigaIdeaRepositoryDraft= _
  @Inject var siteTreeTranslationGroupRepo: SiteTreeTranslationGroupsRepository = _
  @Inject var siteTreeRepository: SiteTreeRepository = _
  @Inject var ideaErrorRepository: IdeaErrorRepository = _
  @Inject var ideaValidationReportRepository: IdeaValidationReportRepository = _
  @Inject var s3FileRepository: S3FileRepository = _
  //setting running validations to cancelld.
  def onApplicationEvent(e: ContextRefreshedEvent): Unit = {
    var test = ideaValidationReportRepository.findAllByStatus(0)
    for (report <- ideaValidationReportRepository.findAllByStatus(0)) {
      report.setStatus(2)
      ideaValidationReportRepository.save(report)
    }
  }

  @Async
  @Transactional
  override def validateAll(saveToDb: Boolean, autoCorrect: Boolean): Unit = {
    validateAll(saveToDb, autoCorrect, kigaIdeaRepository.findAll().map(_.getId).toList, true)
  }

  @Async
  @Transactional
  override def validateAll(saveToDb: Boolean, autoCorrect: Boolean, ids: List[JLong],
                           all: Boolean = false
  ): Unit = {
    log.info("Started Validation")
    var report = createReport(autoCorrect, saveToDb && canSaveToDb)
    val ideasToValidate = kigaIdeaRepository.findAll(ids)

    for (idea <- ideasToValidate ){
      if(true){//if (idea.getLayoutType() == 2) {
        log.info(s"Validating article with id: ${idea.getId} \t ${idea.getTitle}")
        val tmp = validate(idea, autoCorrect, -1L, report.getId())
        report.testedArticles = report.testedArticles + 1
        ideaValidationReportRepository.save(report)
        if (saveToDb == true && canSaveToDb == true) {
          try {
            if (idea.getParent != null) {
              var tmpContent = ValidationHelper.prettyFormatXML(tmp)
              idea.setContent(AutoCorrect.validateSpaceAndSpecialCharacters(tmpContent)._1)
              idea.setStatus("Saved (update)")
              kigaIdeaRepository.save(idea)
            }
          }
          catch {
            case ex:SAXParseException =>{
              log.info(s"Can't set content, ${idea.getId}//${idea.getTitle()}" +
                s" has not valid xml as content")
            }
          }
        }
      }
    }
    finishReport(report, all)
    log.info("Finished Validation")
  }

  override def validate(idea: KigaIdeaDraft, autoCorrect: Boolean, parentId: JLong,
                        reportId: JLong): String = {
    var saveAll = false
    var errorsAll = List[ValidationError]()
    var errorsFirst = List[ValidationError]()
    var errors = List[ValidationError]()
    var errorsLast = List[ValidationError]()
    var xmlContent: xml.Elem = null

    try {
      xmlContent = loadXmlString(idea.getContent())
    }
    catch {
      case ex: InvalidXmlException => {
        createErrorEntry(parentId, reportId, idea,
          new ValidationError("Invalid Xml", "", -1, -1, false), false,
          ErrorGroup.XmlInvalid)
        return idea.getContent()
      }
      case ex: NullPointerException => {
        if(idea == null){
          log.info("Idea is null");
        }
        else{
          log.info("Idea is not null, id: " + idea.getId);
        }
      }
    }
    var firstRun = true
    var allAreSame = true
    errorsAll = errorsFirst
    do {
      try {
        val validator = schema.newValidator()
        val errorHandler = new ValidationErrorHandler(
          ValidationHelper.prettyFormatXML(xmlContent.toString()),
          parentId,
          autoCorrect)
        validator.setErrorHandler(errorHandler)
        validator.validate(new StreamSource(new StringReader(
          ValidationHelper.prettyFormatXML(xmlContent.toString()))))
        if (firstRun) {
          firstRun = false;
          errorsFirst = errorsFirst ::: errorHandler.getErrorList()
          errors = errorHandler.getErrorList()
        } else {
          errorsLast = errors
          errors = errorHandler.getErrorList()
        }
        xmlContent = xmlIsValid(errorHandler.getNewXml(), xmlContent)
      } catch {
        case sax: SAXException => log.debug(sax.getMessage())
        case io: IOException => log.debug(io.getMessage())
      }

      allAreSame = errors.size == errorsLast.size
      if (errors.size == errorsLast.size) {
        for (i <- 0 to errors.size - 1) {
          if (errors(i).msg != errorsLast(i).msg ||
            errors(i).line != errorsLast(i).line ||
            errors(i).column != errorsLast(i).column) {
            allAreSame = false
          }
        }
      }
      if (!allAreSame) {
        //check for duplicates
        for(nError <- errors){
          var addIt = true
          for(oError <- errorsAll){
            if(
              nError.lineContent == oError.lineContent &&
                nError.msg == oError.msg &&
                (nError.column == oError.column || nError.line == oError.line)
            ){
              addIt = false
            }
          }
          if(addIt){
            errorsAll = errorsAll.::(nError)
          }
        }
        errorsLast = errors
      }
    } while ( autoCorrect && (errors.nonEmpty && !allAreSame))
    if(autoCorrect == false){
      errorsAll = errorsFirst
    }
    for (e <- errorsAll) {
      createErrorEntry(parentId, reportId, idea, e, !(errors.size > 0),
        ValidationHelper.getErrorGroup(e.msg))
    }
    val validatedContent = AutoCorrect.validateSpaceAndSpecialCharacters(
      xmlContent.toString())
    if (validatedContent._2 > 0) {
      createErrorEntry(parentId, reportId, idea, ValidationError(
        "There are " + validatedContent._2.toString + " missing quoates", "",
        -1, -1, false), false, ErrorGroup.QuoteMissing)
    }
    var pageImages = idea.getKigaPageImages().filter(_.getS3Image != null)
    for(missingS3 <- idea.getKigaPageImages().filter(_.getS3Image == null)){
      errorsFirst = errorsFirst ++ List( new ValidationError(
        "S3Image Image Missing", s"Image id: ${missingS3.getImageId}", -1, -1, false)
      )
    }
    for (url <- xmlContent \\ "img" \\ "@src") {
      if (checkIfImageExists(url.toString()) == false) {
       createErrorEntry(parentId,reportId,idea, new ValidationError("Invalid Img url:[" + url.toString + "]",
         url.toString, -1, -1, false), false, ErrorGroup.InvalidImageType)
      }
      var s3Url = getS3UrlFromImage(url.toString, pageImages)
      if(s3Url == "") {
        for (missingS3 <- idea.getKigaPageImages().filter(_.getS3Image == null)) {
          createErrorEntry(parentId,reportId,idea, new ValidationError(
            "S3Image Image Missing", s"html url: $url", -1, -1, false), false, ErrorGroup.S3ImageMissing)
        }
      }
      else if(checkImageType(s3Url) == false) {
        createErrorEntry(parentId,reportId,idea, new ValidationError(
          s"Wrong Image type,extension: ${s3Url.takeRight(4)}", s"html url: $url", -1, -1, false), false, ErrorGroup.InvalidImageType)

        errorsFirst = errorsFirst ++ List(

        )
      }
    }
    if (errors.size > 0) {
      idea.getContent()
    } else {
      xmlContent.toString()
    }
  }

  /*def checkIfDuplicate(all: List[ValidationError], toInsert: ValidationError ): Boolean = {
    var addIt = true
    for(err <- all){
      if(err.lineContent )) toInsert.lineContent)
    }
  }*/

  override def loadXmlString(content: String): xml.Elem = {
    try {
      scala.xml.XML.loadString(ValidationHelper.markSpansToDelete(content))
    }
    catch {
      case sax: SAXException => {
        throw new InvalidXmlException("Xml not valid")
      }
      case e: Exception => {
        throw new InvalidXmlException("Xml not valid")
      }
    }
  }

  override def xmlIsValid(newXml: xml.Elem, oldXml: xml.Elem): xml.Elem = {
    var contentValid = true
    if ((oldXml \\ "hr").size != (newXml \\ "hr").size) {
      log.debug("HR missing")
      contentValid = false
    }
    if ((oldXml \\ "img").size != (newXml \\ "img").size) {
      log.debug("IMG missing")
      contentValid = false
    }
    if (oldXml.text.replace(" ", "").replace("\n", "").replace("\t",
      "").replace("\r", "") != newXml.text.replace(" ",
      "").replace("\n", "").replace("\t", "").replace("\r", "")) {
      log.debug("TEXT missing")
      contentValid = false
    }
    if (contentValid) {
      xml.XML.loadString(ValidationHelper.markSpansToDelete(newXml.toString))
    }
    else {
      oldXml
    }
  }
  /*
    def saveArticleInReport(reportId: Long, idea: KigaIdea) = {
      var ideaInRepo = new IdeaInReport()
      ideaInRepo.setReportId(reportId)
      ideaInRepo.setClassName("IdeaInReport")
      ideaInRepo.setArticleId(idea.getId)
      ideaInRepo.setLanguage(idea.getLocale())
      ideaInReportRepository.save(ideaInRepo)
    }
  */

  override def checkImageType(url: String): Boolean = {
      //download and check chunks
      if(url.endsWith("jpg") || url.endsWith("png"))
        true
      else
        false
  }

  override def checkIfImageExists(url: String): Boolean = {
    try {
      val httpClient = HttpClients.createDefault()
      val head = new HttpHead(
        url.replace("https://", "http://")) //new HttpGet(url)
      head.addHeader("content-type", "application/x-www-form-urlencoded")
      val httpResponse = httpClient.execute(head)
      if (httpResponse.getStatusLine().getStatusCode() == 200 &&
        httpResponse.getHeaders("Content-Length")(0).getValue.toInt > 0) {
        return true
      }
      return false
    }

    catch {
      case cpe: ClientProtocolException => return false
      case io: IOException => log.debug(io.getMessage()); return false
    }
    return false
  }

  override def createErrorEntry(parentId: JLong, reportId: JLong, idea: KigaIdeaDraft,
                                e: ValidationError, autoCorrected: Boolean,
                                errorGroup: ErrorGroup) = {
    var errorEntry = new IdeaError()
    errorEntry.setArticleId(idea.getId())
    errorEntry.setCodeColumn(e.column)
    errorEntry.setCodeLine(e.line)
    errorEntry.setClassName("IdeaError")
    errorEntry.setErrorGroup(errorGroup.id)
    errorEntry.setIdeaValidationReportId(reportId)
    errorEntry.setAutoCorrectable(e.autocorrected)
    errorEntry.setAutoCorrected(autoCorrected)
    errorEntry.setLocale(idea.getLocale())
    errorEntry.setErrorDetail(getErrorDetail(ErrorGroup(errorEntry.getErrorGroup()), e.msg))
    errorEntry.setContent(e.lineContent)
    ideaErrorRepository.save(errorEntry)
  }

  override def getErrorDetail(errorGroup: ErrorGroup, msg: String): String = {
    errorGroup match {
      case ErrorGroup.InvalidElement => ("Wrong element: [" + msg.split("\'")(
        1) + "] Expected elements:[" + msg.replace(" ",
        "").split('{')(1).split('}')(0).split(',').mkString("|") + "]")
      case ErrorGroup.AttributeMissing => ("Attribute:[" + msg.split("'")(
        1) + "] must appear on [" + msg.split(
        "'")(3) + "]")
      case ErrorGroup.WrongAttribute => ("Attribute:[" + msg.split("'")(
        1) + "] not allowed in [" + msg.split("'")(3) + "]")
      case ErrorGroup.NoElementChildren => ("[" + msg.split("'")(
        1) + "] must have no children")
      case ErrorGroup.NoCharacterChildren => ("[" + msg.split("'")(
        1) + "] is not allowed to have characters as children")
      case ErrorGroup.WrongAttributeValue => ("[" + msg.split("'")(
        1) + "] is not a valid value for [" + msg.split(
        "'")(3) + "]")
      case ErrorGroup.XmlInvalid => (msg)
      case ErrorGroup.QuoteMissing => (msg)
      case ErrorGroup.InvalidImageUrl => (msg)
      case ErrorGroup.Unknown => (msg)
      case ErrorGroup.InvalidImageType => (msg)
      case _ => ("UNDEFINED ERROR GROUP" + msg)
    }
  }

  override def createReport(autoCorrect: Boolean, saveToDb: Boolean): IdeaValidationReport = {
    var report = new IdeaValidationReport()
    report.setStartDateTime(org.joda.time.DateTime.now().toDate())
    report.setEndDateTime(null)
    report.setClassName("IdeaValidationReport")
    report.setWithAutocorrect(autoCorrect)
    report.setWithSaveToDb(saveToDb)
    report.setManual(0)
    report.setAutocorrected(0)
    report.setAutocorrectable(0)
    report.setStatus(0)
    report.setAllArticles(false)
    ideaValidationReportRepository.save(report)
    report
  }

  override def finishReport(report: IdeaValidationReport, wasAll: Boolean) = {
    report.setEndDateTime(org.joda.time.DateTime.now().toDate())
    report.setAutocorrected(
      ideaErrorRepository.countByIdeaValidationReportIdAndAutoCorrected(report.getId, true))
    report.setAutocorrectable(
      ideaErrorRepository.countByIdeaValidationReportIdAndAutoCorrectableOrIdeaValidationReportIdAndAutoCorrected(
        report.getId,
        true,
        report.getId,
        true))
    report.setManual(
      ideaErrorRepository.countByIdeaValidationReportIdAndAutoCorrectableAndAutoCorrected(
        report.getId, false, false))
    report.setAllErrors(ideaErrorRepository.countByIdeaValidationReportId(report.getId))
    report.setEndDateTime(org.joda.time.DateTime.now().toDate())
    report.setStatus(1)
    report.setAllArticles(wasAll)
    ideaValidationReportRepository.save(report)
  }

  override def getS3UrlFromImage(url: String, kigaPageImages: JList[KigaPageImage]): String = {
    for(pageImage <- kigaPageImages){
      var s3Image = pageImage.getS3Image
      if(s3Image.getCachedData != null && (s3Image.getCachedData.contains(url.dropRight(4)) ||
        s3Image.getCachedData.contains(url.replace("http://","https://").dropRight(4)))){
        return s3Image.getUrl
      }
    }
    return  ""
  }
}
