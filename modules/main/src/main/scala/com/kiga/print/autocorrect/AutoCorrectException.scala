package com.kiga.print.autocorrect

import scala.beans.BeanProperty

/**
 * Created by peter on 08.09.15.
 */
class AutoCorrectException(msg: String) extends Exception(msg){
  @BeanProperty var content = ""
}

class LineEmptyException(msg:String) extends Exception(msg){

}

class InvalidXmlException(msg:String) extends Exception(msg){

}
