package com.kiga.print.model

import scala.beans.BeanProperty
import scala.xml.Elem
/**
  * Created by peter on 23.02.16.
  */
class TableRow extends Printable{
  @BeanProperty var marginTopMM: Integer = null
  @BeanProperty var marginBottomMM: Integer = null
  @BeanProperty var marginLeftMM: Integer = null
  @BeanProperty var marginRightMM: Integer = null
  @BeanProperty var border: String = null
  @BeanProperty var keepTogetherWithinColumn: String = null

  //margin="0mm"  border="0mm" keep-together.within-column="always"

  override def getFop(): Elem = {
    var element = <fo:table-row keep-together.within-column={attributeToString(Option(keepTogetherWithinColumn))} border={attributeToString(Option(border))} margin-top={attributeToString(Option(marginTopMM), "mm")} margin-bottom={attributeToString(Option(marginBottomMM), "mm")} margin-left={attributeToString(Option(marginLeftMM), "mm")}  margin-right={attributeToString(Option(marginRightMM), "mm")} ></fo:table-row>
    return element.copy(child = element.child ++ this.children.map(_.getFop()))
  }
}
