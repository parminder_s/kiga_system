package com.kiga.print.model

import scala.beans.BeanProperty
import scala.xml.Elem

/**
  * Created by peter on 24.02.16.
  */
class Image(
             @BeanProperty var src: String // because it is needed!
) extends Printable{

  @BeanProperty var marginMM: Integer = null
  @BeanProperty var contentWidthPx: Integer = null

  override def getFop(): Elem = {
    var element = <fo:external-graphic margin={attributeToString(Option(marginMM),"mm")} content-width={attributeToString(Option(contentWidthPx),"px")} src={src}></fo:external-graphic>
    return element
  }
}
