package com.kiga.print.model

import scala.beans.BeanProperty

/**
  * Created by peter on 23.02.16.
  */
abstract class Printable {

  def getFop(): xml.Elem

  @BeanProperty var  text:String = ""

  def attributeToString(input: Option[Any], unit:String = ""): String = {
    if(input.isDefined){
      input.get.toString + unit
    }
    else{
      null
    }
  }
  @BeanProperty var children: List[Printable] = List()

}
