package com.kiga.print.article

/**
 * Created by peter on 22.07.15.
 */
case class CssClasses (
                      marginBottom:String,
                      span:Int
                        )
