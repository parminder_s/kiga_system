package com.kiga.print.validation

import com.kiga.content.domain.ss.KigaPageImage
import com.kiga.content.domain.ss.draft.KigaIdeaDraft
import com.kiga.content.domain.ss.live.KigaIdeaLive
import com.kiga.print.autocorrect.ValidationError
import com.kiga.print.validation.db.model.ErrorGroup.ErrorGroup
import com.kiga.print.validation.db.model.IdeaValidationReport
import com.kiga.types.{JList, JLong}

import scala.xml.Elem

/**
  * Created by peter on 17.02.16.
  */
trait ValidateXML {

  def validateAll(saveToDb: Boolean, autoCorrect: Boolean): Unit

  def validateAll(saveToDb: Boolean, autoCorrect: Boolean, ids: List[JLong],
                  all: Boolean = false): Unit

  def validate(idea: KigaIdeaDraft, autoCorrect: Boolean, parentId: JLong,
               reportId: JLong): String

  def loadXmlString(content: String): Elem

  def xmlIsValid(newXml: Elem, oldXml: Elem): Elem

  def checkImageType(url: String): Boolean

  def checkIfImageExists(url: String): Boolean

  def getS3UrlFromImage(url: String, kigaPageImages:JList[KigaPageImage]): String

  def createErrorEntry(parentId: JLong, reportId: JLong, idea: KigaIdeaDraft,
                       e: ValidationError, autoCorrected: Boolean,
                       errorGroup: ErrorGroup)

  def getErrorDetail(errorGroup: ErrorGroup, msg: String): String

  def createReport(autoCorrect: Boolean, saveToDb: Boolean): IdeaValidationReport

  def finishReport(report: IdeaValidationReport, wasAll: Boolean)
}
