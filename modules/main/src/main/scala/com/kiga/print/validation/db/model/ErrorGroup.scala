package com.kiga.print.validation.db.model

import javax.persistence._

import com.kiga.db.{KigaEntityModel, LocaleToLongNotationConverter}
import com.kiga.main.locale.Locale
import com.kiga.print.validation.db.model.ErrorGroup.ErrorGroup

import scala.beans.BeanProperty

/**
 * Created by peter on 08.09.15.
 */


object ErrorGroup extends Enumeration {
  type ErrorGroup = Value
  val InvalidElement = Value(1)
  val AttributeMissing = Value(2)
  val WrongAttribute = Value(3)
  val NoElementChildren = Value(4)
  val NoCharacterChildren = Value(5)
  val WrongAttributeValue = Value(6)
  val InvalidImageUrl = Value(7)
  val Unknown = Value(8)
  val XmlInvalid = Value(9)
  val QuoteMissing = Value(10)
  val AllErrors = Value(11)
  val InvalidImageType = Value(12)
  val S3ImageMissing = Value(13)
}
