package com.kiga.print.autocorrect

import org.apache.commons.lang.StringEscapeUtils
import org.slf4j.LoggerFactory

/**
 * Created by peter on 20.08.15.
 */
object AutoCorrect {

  val log = LoggerFactory.getLogger(getClass)
  val tmp: Char = 160
  val tmp1: Char = 10
  val backN = tmp1.toString()
  val nbsp = tmp.toString()

  /*
  * Removed wrong items in wrong items if they have the same label
  * Fore example: "<span class="test">1<span class="test">2</span>3</span>" becomes "123"
  * */
  def checkIfLabelAndPcdata(node: xml.Node,
                            wrongChildLabel: String): Boolean = {
    var counts = node.child.map(_.label).groupBy(identity).mapValues(_.size)
    if (counts.contains("#PCDATA") && counts.contains(wrongChildLabel)) {
      if (counts("#PCDATA") == 2 && counts(wrongChildLabel) == 1) {
        true
      } else {
        false
      }
    } else {
      false
    }
  }

  def recursiveRemove(parent: xml.Node,
                      wrongChildLabel: String): Tuple2[Int, String] = {
    println("--")
    println(parent)
    parent.child.foreach(x => println(x.label))
    parent.child.size match {
      case 1 if (parent.child(0).label == wrongChildLabel) => {
        var tmp = recursiveRemove(parent.child(0), wrongChildLabel)
        Tuple2(
          tmp._1 + 5 + wrongChildLabel.length() * 2 + parent.attributes.toString.length,
          tmp._2)
      }
      case 2 if (parent.child.map(_.label).contains(
        wrongChildLabel) && parent.child.map(_.label).contains("#PCDATA")) => {
        var i = if (parent.child(0).label == wrongChildLabel) 0 else 1
        var tmp = recursiveRemove(parent.child(i), wrongChildLabel)
        Tuple2(
          tmp._1 + 5 + wrongChildLabel.length() * 2 + parent.attributes.toString.length,
          if (i == 1) parent.child(0).text + tmp._2
          else tmp._2 + parent.child(1).text)
      }
      case 3 if (checkIfLabelAndPcdata(parent, wrongChildLabel)) => {
        var tmp = recursiveRemove(parent.child(1), wrongChildLabel)
        Tuple2(
          tmp._1 + 5 + wrongChildLabel.length() * 2 + parent.attributes.toString.length,
          parent.child(0).text + tmp._2 + parent.child(2).text)
      }
      case _ => {
        if (parent.label == wrongChildLabel && parent.attributes.size == 0) {
          if (parent.child.size == 0) {
            Tuple2(
              5 + wrongChildLabel.length() * 2 + parent.attributes.toString.length,
              parent.text)
          } else if (parent.child.size == 1) {
            Tuple2(
              5 + wrongChildLabel.length() * 2 + parent.attributes.toString.length,
              parent.child(0).toString())
          } else {
            var newContent = ""
            var removedLength = 0
            for (c <- parent.child)
              c.label match {
                case "#PCDATA" => newContent = newContent + c.text
                case _ => newContent = newContent + c.toString()
              }

            Tuple2(
              5 + wrongChildLabel.length() * 2 + parent.attributes.toString.length,
              newContent)
          }
        } else if (parent.label == wrongChildLabel) {
          Tuple2(0, parent.text)
        } else {
          Tuple2(0, parent.toString())
        }
      }
    }
  }

  def removeInvalidElement(parent: xml.Node, wrongChildLabel: String,
                           allowedchilds: Array[String]): String = {
    if (wrongChildLabel == "div" && parent.attributes.toString().trim.split(
      "=").contains("class")) {
      log.debug("try to remove parts of div tree, not doing it !!")
      //println(parent.toString())
      throw new AutoCorrectException("Cant autocorrect, div, tree wrong")
    }
    if (wrongChildLabel == "hr" || wrongChildLabel == "img" || wrongChildLabel == "a") {
      log.debug("try to remove hr or img, not doing it!!")
      //println(parent.toString())
      throw new AutoCorrectException(
        "Cant autocorrect, do not delete hr or img")
    }
    var newContent = ""
    if (parent.label == wrongChildLabel) {
      // so that &quot; is unescaped to " in the string, otherwise the len difference is wrong!
      val pU = StringEscapeUtils.unescapeXml(parent.toString)
      var parentUnescapedLen = StringEscapeUtils.unescapeXml(
        parent.toString).length()
      var removedLength = if (wrongChildLabel == "br") {
        5
      } else {
        //if no text and no element, then it is a selc closing tag <p></p> becomes to <p/>
        if (parent.text == "" && parent.child.size == 0) {
          wrongChildLabel.length() + 3 + StringEscapeUtils.unescapeXml(
            parent.attributes.toString()).length()
        } else {
          2 * wrongChildLabel.length() + 5 + StringEscapeUtils.unescapeXml(
            parent.attributes.toString()).length()
        }
      }
      var newContent = ""
      if (parent.child.size > 0) {
        if (parent.child.size == 1 && parent.child(
          0).label == wrongChildLabel && parent.attributes.size == 0) {
          val tup = recursiveRemove(parent, wrongChildLabel)
          removedLength = tup._1
          newContent = tup._2
          log.debug("new content:" + newContent)
        } else if (parent.child.size == 1 && parent.child(
          0).label == wrongChildLabel) {
          removedLength = removedLength + 2 * wrongChildLabel.length() + 5 + parent.child(
            0).attributes.toString().length()
          for (c <- parent.child(0).child) {
            if (allowedchilds.contains(
              c.label) == false && allowedchilds.contains("All") == false) {
              c.label match {
                case "br" => removedLength += 5
                case "#PCDATA" => newContent = newContent + c.text
                case "img" => newContent = newContent + c.toString //img is alway taken, not only the "text" in the image tag
                case "hr" => newContent = newContent + c.toString //hr is alway taken, not only the "text" in the hr tag
                case _ => removedLength += 2 * c.label.toString().length() + c.attributes.toString().length() + 5
              }

              //because text is only ' '
              /*if (c.text.trim().length() == 0) {
                removedLength += c.text.length()
              }*/
            } else {
              newContent = newContent + c.toString
            }
          }
        } else {
          for (c <- parent.child) {
            //log.debug(c.toString())
            if (allowedchilds.contains(
              c.label) == false && allowedchilds.contains(
              "All") == false && c.label != "img" && c.label != "hr") {
              c.label match {
                case "br" => removedLength += 5
                case "#PCDATA" => newContent = newContent + c.text
                case _ => {
                  if (c.child.size == 1 && c.label == wrongChildLabel && c.child(
                    0).label == wrongChildLabel) {
                    removedLength += 2 * c.label.toString().length() + c.attributes.toString().length() + 5 // + 5 because <></>
                  }
                  newContent = newContent + c.text
                  removedLength += 2 * c.label.toString().length() + c.attributes.toString().length() + 5 // + 5 because <></>
                }
              }
            } else {
              newContent = newContent + c.toString
            }
          }
        }
      }
      if (newContent == "") {
        if (parent.child.size == 1) {
          parent.child(0).label match {
            case "img" | "hr" => newContent = parent.child(0).toString()
            case _ => newContent = parent.text
          }
        } else {
          newContent = parent.text
        }
      }
      var tmp = StringEscapeUtils.unescapeXml(newContent)
      // <hr/> will get deleted otherwise
      newContent = if (tmp == "") newContent else tmp
      var diff = parentUnescapedLen - newContent.length()

      var divIn = false
      for (s <- allowedchilds) {
        if (s == "div") {
          divIn = true
        }
      }

      if (allowedchilds.contains("div") && wrongChildLabel != "div") {
        log.debug("Can't autofix, div, tree wrong!:" + parent.toString())
        if (parent.text.trim.length == 0 && parent.label != "img" && parent.label != "hr") {
          log.debug(
            s"removing empty element [${parent.label}] which is outsite of divtree")
          return ""
        } else {
          throw new AutoCorrectException("Cant autocorrect, div, tree wrong")
        }
      } else if (removedLength != diff) {
        log.debug(parent.toString)
        log.debug(newContent)
        if (parent.attributes.size == 0) {
          return parent.toString.dropRight(3 + wrongChildLabel.length).drop(
            wrongChildLabel.length + 2)
        }
        log.debug("AllowedChild:[" + allowedchilds.mkString("][") + "]")
        log.debug(
          "Can't autofix, lenght not same\tparentUnescaped:" + parentUnescapedLen + "\tnewContent:" + newContent.length() + "\tdiff:" + diff + "\tremoved:" + removedLength)
        throw new AutoCorrectException("Cant autocorrect, lenght not same")
      } else {
        //println(parent.toString)
        //println(newContent)
        newContent
      }
    } else {
      throw new AutoCorrectException("Cant autocorrect, label not correct")
    }
  }

  def removeAttribute(line: xml.Elem, element: String,
                      attribute: String): xml.Elem = {
    if (line.label == element) {
      line.copy(attributes = line.attributes.remove(attribute))
    } else {
      throw new AutoCorrectException(
        "Cant autocorrect, element/attribute not correct!")
    }
  }

  def removeChilds(parent: xml.Node, parentLabel: String): String = {
    //happens if a hr is in a h1,h2,h3,h4,h5,h6
    var x = if (parent.child.size == 1) {
      //match for exceptions !
      parent.child(0).label match {
        case "hr" => "<hr/>"
        case "img" => parent.child(0).toString()
        case _ => "<" + parent.label + parent.attributes.toString() + ">" + parent.text + "</" + parent.label + ">"
      }
    } else {
      var pre = ""
      for (c <- parent.child) {
        if (c.label == "img" || c.label == "hr") {
          pre = c.toString()
        }
      }
      pre + "<" + parent.label + parent.attributes.toString() + ">" + parent.text + "</" + parent.label + ">"
    }
    x
  }

  def validateSpaceAndSpecialCharacters(input: String): Tuple2[String,Int] = {
    /*preparing input string
     -replaces &nbsp; with normal space
     -remove all whitespace before
     -remove all whitespace after quoates
     -remove all double spaces
    */
    var step1 = scala.xml.Utility.trim(xml.XML.loadString(input)).toString()
    step1 = StringEscapeUtils.unescapeXml(step1.toString)
    step1 = replaceAllWithoutSpaces(step1, " \"")
    step1 = replaceAllWithoutSpaces(step1, "\" ")
    //remove empty labels!
    //defining labels, where spaces should be moved out between them
    val labels = List[String]("span", "em", "p", "strong")
    for (l <- labels) {
      step1 = step1.replace(s"<$l/>", "");
    }

    while (step1.contains("  ")) step1 = step1.replace("  ", " ")
    while (step1.contains(nbsp + " ")) step1 = step1.replace(nbsp + " ", nbsp)
    while (step1.contains(" " + nbsp)) step1 = step1.replace(" " + nbsp, nbsp)
    var step2 = ""
    var firstQuote = true
    var inElement = false
    var missingQuotes = 0
    for (character <- step1) {
      var nextCharacter = ""
      character match {
        case '<' => inElement = true; nextCharacter = character.toString()
        case '>' => inElement = false; nextCharacter = character.toString()
        case 34 if inElement == false => nextCharacter = if (firstQuote) " \"" else "\" "; firstQuote = !firstQuote
        case 34 if inElement == true => nextCharacter = "\" "
        case 10 => nextCharacter = " " //changing ASCII 160/10(\n) space to ASCII 32 space!
        case 194 => nextCharacter = "" //removing 'Â' 194
        case _ => nextCharacter = character.toString()
      }
      step2 += nextCharacter
    }
    //soo that is all! "NOO
    step2 = step2.replace("=\" ", "=\"").replace("\" >", "\">")
    step2 = replaceAllWithoutSpaces(step2, "=\" ")
    step2 = replaceAllWithoutSpaces(step2, "\" >")
    step2 = replaceAllWithoutSpaces(step2, ">\" </")
    for (l <- labels) {
      step2 = step2.replace(s"</$l> <$l>", s" </$l><$l>")
      step2 = step2.replace(s"<$l> ", s"<$l>")
      step2 = step2.replace(s"</$l>", s" </$l>")
      step2 = step2.replace(s"<$l>", s" <$l>")
      step2 = step2.replace(s"</$l> <$l>", s"</$l><$l>")
      step2 = step2.replace(s"</$l>$nbsp<$l>", s"$nbsp</$l><$l>")
      step2 = step2.replace(s"> <$l", s"><$l")
      //that empty lines are not deleted!
      if (l != "p") {
        step2 = step2.replace(s"<$l>$nbsp", s"<$l>")
      }
    }
    //why - replacing whitespace if steps2 starts with it happens if idea
    // would start with <p> for example
    if (step2.startsWith(" ")) {
      step2 = step2.tail
    }
    var specialCharacters = List[String](":", ",", "!", "?", ".", "\'")
    //placing a space behind every special character
    for (s <- specialCharacters) {
      step2 = step2.replace(s, s + " ")
      step2 = replaceAllWithoutSpaces(step2, " " + s)
      //while(step2.contains(" " + s)) step2 = step2.replace(" "+ s,s)
    }
    step2 = replaceAllWithoutSpaces(step2, "\' ")
    step2 = replaceAllWithoutSpaces(step2, "- ")
    step2 = replaceAllWithoutSpaces(step2, " -")
    for (l <- labels) {
      for (s <- specialCharacters) {
        //step2 = step2.replace(s, s + " ") //</span><span>
        step2 = replaceAllWithoutSpaces(step2, " " + s)
        while (step2.contains(s" </$l><$l>" + s)) step2 = step2.replace(
          s" </$l><$l>" + s, s"</$l><$l>" + s)
      }
    }
    //reset helper valiables for next for loop!
    firstQuote = true
    var firstQuoteInElement = true
    var lastWasSpecial = false
    inElement = false
    var skipNext = false
    var lastWasSpace = false
    var lastWasStartOfElement = false
    var step3 = ""
    var step2Unescaped = StringEscapeUtils.unescapeXml(step2)
    step2Unescaped = replaceAllWithoutSpaces(step2Unescaped, (" \""))
    step2Unescaped = replaceAllWithoutSpaces(step2Unescaped, "\" ")
    var lastWasEndOfElement = false
    for (currentChar <- step2Unescaped) {

      if (skipNext && currentChar == 32) {
        skipNext = false
      } else {
        var tmp = if (lastWasSpace) " " + currentChar.toString() else currentChar.toString()
        //println(tmp)
        lastWasSpace = false
        if (lastWasStartOfElement == true && currentChar == 'p') {
          if (firstQuote == false) {
            firstQuote = true
            missingQuotes = missingQuotes + 1
          }
        }
        currentChar match {
          case '<' => inElement = true
          case '>' => {
            inElement = false
            if (tmp.startsWith(" ")) {
              tmp = tmp.tail
            }
          }
          case 46 | 58 => skipNext = inElement // 46 => ./: /if there is a dot/colon in an element, i skip the next char
          case 34 if inElement == true => {
            tmp = "\""
            lastWasSpace = !firstQuoteInElement
            if (lastWasSpace) {
              step3 = step3 + tmp
            }
            firstQuoteInElement = !firstQuoteInElement
          }
          case 34 if inElement == false => {
            tmp = if (firstQuote) "\"" else "\" "
            //  if (lastWasSpecial && firstQuote || !lastWasSpace && firstQuote) {
            if (lastWasSpecial && firstQuote || !lastWasSpace && firstQuote && !lastWasEndOfElement) {
              tmp = " " + tmp
            }
            //here is "<span>Text </span>", remove space before </span>
            if (firstQuote == false && lastWasEndOfElement || (tmp.startsWith(" ")) && step3.endsWith(" ")) {
              //if (firstQuote == false && lastWasEndOfElement) {
              step3 = step3.reverse.replaceFirst(" ", "").reverse
            }

            firstQuote = !firstQuote
          }
          case 32 => lastWasSpace = true
          case 160 => {
            tmp = tmp.replace(" ", "")
          }
          case _ => tmp = tmp
        }
        if (lastWasSpace == false) {
          step3 += tmp
        }
        lastWasSpecial = specialCharacters.contains(currentChar.toString)
        lastWasEndOfElement = currentChar == '>'
        lastWasStartOfElement = currentChar == '<'
      }
    }
    step3 = replaceAllWithoutSpaces(step3, "\" ,")
    while (step3.contains(nbsp + " ")) {
      step3 = step3.replace(nbsp + " ", nbsp)
    }
    while (step3.contains(nbsp + nbsp)) {
      step3 = step3.replace(nbsp + nbsp, nbsp)
    }
    for (l <- labels) {
      step3 = step3.replace(s" </$l> ", s" </$l>").replace(s" <$l> ", s" <$l>")
      for (s <- specialCharacters) {
        step3 = replaceAllWithoutSpaces(step3, s" </$l> $s")
        step3 = replaceAllWithoutSpaces(step3, s" </$l><$l>$s")
        step3 = replaceAllWithoutSpaces(step3, s"</$l> $s")
        step3 = replaceAllWithoutSpaces(step3, s" </$l>$s")
      }
    }
    step3 = replaceAllWithoutSpaces(step3, "\" .")
    new Tuple2(step3,missingQuotes)
  }

  def replaceAllWithoutSpaces(input: String, toReplace: String): String = {
    var newValue = input
    while (newValue.contains(toReplace)) newValue = newValue.replace(toReplace,
      toReplace.replace(" ", ""))
    var x = toReplace.replace(" ", nbsp)
    var y = if (toReplace(0) == 32 || toReplace(0) == 160 || toReplace(
      0) == 10) toReplace.tail
    else toReplace
    while (newValue.contains(x)) newValue = newValue.replace(x, y)
    x = toReplace.replace(" ", backN)
    y = if (toReplace(0) == 10 || toReplace(0) == 160 || toReplace(
      0) == 32) toReplace.tail
    else toReplace
    while (newValue.contains(x)) newValue = newValue.replace(x, y)
    newValue
  }
}
