package com.kiga.print.autocorrect

/**
 * Created by peter on 21.07.15.
 */

import com.kiga.print.validation.ValidationHelper
import com.kiga.print.validation.db.model.ErrorGroup
import com.kiga.types.JLong
import org.slf4j.LoggerFactory
import org.xml.sax._

import scala.collection.mutable.ArrayBuffer


case class ValidationError(
                            msg: String,
                            lineContent: String,
                            line: Int,
                            column: Int,
                            autocorrected: Boolean
                            )

class ValidationErrorHandler(content: String, parentId: JLong,
                             enableAutoCorrect: Boolean) extends ErrorHandler {

  val log = LoggerFactory.getLogger(getClass)
  var newXmlContent = content
  var errors = ArrayBuffer[ValidationError]()
  var linesCount = 0


  def getNewXml(): xml.Elem = {
    xml.XML.loadString(newXmlContent)
  }

  def fatalError(e: SAXParseException) = {
    warning(e)
  }

  def warning(e: SAXParseException) = {
    var line = ""
    val errorGroup = ValidationHelper.getErrorGroup(e.getMessage())
    try {
      line = getXmlElement(
        newXmlContent.split(System.getProperty("line.separator")),
        e.getLineNumber() - 1,
        errorGroup == ErrorGroup.NoElementChildren || errorGroup == ErrorGroup.NoCharacterChildren)


    }
    catch {
      case lempty: LineEmptyException => {
      }
      case ex: Exception => {
        line = "Can't get line"
      }
    }
    if (line != "") {
      try {
        if (enableAutoCorrect) {
          errorGroup match {
            case ErrorGroup.InvalidElement => {
              // if image, check if parent is <em> or <strong>, if so, remove <em> or <strong>
              val label = e.getMessage().split("\'")(1)
              if (label == "img" || label == "hr") {
                //get new line because i do not want to remove a img or hr
                var line = getXmlElement(
                  newXmlContent.split(System.getProperty("line.separator")),
                  e.getLineNumber() - 2, false)
                var xml = scala.xml.XML.loadString(line)
                newXmlContent = updateLines(
                  newXmlContent.split(System.getProperty("line.separator")),
                  e.getLineNumber() - 2,
                  AutoCorrect.removeInvalidElement(xml, line.split(">")(0).tail,
                    e.getMessage().replace(" ", "").split('{')(1).split('}')(
                      0).split(',')).toString, false)
              }
              else {
                log.info(line)
                var xml = scala.xml.XML.loadString(line)
                newXmlContent = updateLines(
                  newXmlContent.split(System.getProperty("line.separator")),
                  e.getLineNumber() - 1,
                  AutoCorrect.removeInvalidElement(xml, label,
                    e.getMessage().replace(" ", "").split('{')(1).split('}')(
                      0).split(',')).toString, false)
              }
              addError(e.getMessage(), line, e.getLineNumber(),
                e.getColumnNumber(), true)
            }
            case ErrorGroup.AttributeMissing => {
              addError(e.getMessage(), line, e.getLineNumber(),
                e.getColumnNumber(), false)
            }
            case ErrorGroup.WrongAttribute => {
              var xml = scala.xml.XML.loadString(line)
              // span with delete flag!
              val tmp = if (e.getMessage().contains("delete")) {
                AutoCorrect.removeInvalidElement(xml, "span",
                  Array[String]("All"))
              }
              else {
                AutoCorrect.removeAttribute(xml, e.getMessage().split("'")(3),
                  e.getMessage().split("'")(1))
              }
              newXmlContent = updateLines(
                newXmlContent.split(System.getProperty("line.separator")),
                e.getLineNumber() - 1, tmp.toString, false)
              addError(e.getMessage(), line, e.getLineNumber(),
                e.getColumnNumber(), true)
            }
            case ErrorGroup.NoElementChildren => {
              getXmlElement(
                newXmlContent.split(System.getProperty("line.separator")),
                e.getLineNumber() - 1, true)
              var xml = scala.xml.XML.loadString(line)
              val tmp = AutoCorrect.removeChilds(xml,
                e.getMessage().split("\'")(1))
              newXmlContent = updateLines(
                newXmlContent.split(System.getProperty("line.separator")),
                e.getLineNumber() - 1, tmp.toString, true)
              addError(e.getMessage(), line, e.getLineNumber(),
                e.getColumnNumber(), true)
            }
            case ErrorGroup.NoCharacterChildren => {
              addError(e.getMessage(), line, e.getLineNumber(),
                e.getColumnNumber(), false)
            }
            case ErrorGroup.WrongAttributeValue => {
              var value = e.getMessage().split("'")(1)
              var notValidFor = e.getMessage().split("'")(3)
              if (notValidFor == "integer") {
                var tmp = line.replace(value, value.split('.')(0))
                newXmlContent = updateLines(
                  newXmlContent.split(System.getProperty("line.separator")),
                  e.getLineNumber() - 1, tmp.toString, false)
                addError(e.getMessage(), line, e.getLineNumber(),
                  e.getColumnNumber(), true)
              }
              else {
                addError(e.getMessage(), line, e.getLineNumber(),
                  e.getColumnNumber(), false)
              }
            }
            //unkown Errors!
            case _ => {
              println("UNDEFINED ERROR:" + e.getMessage())
              addError(e.getMessage(), line, e.getLineNumber, e.getColumnNumber,
                false)
            }
          }
        }
        else {
          addError(e.getMessage(), line, e.getLineNumber(), e.getColumnNumber(),
            false)
        }
      }
      catch {
        //was not able to Autocorrect error, add a manual error!
        case aex: AutoCorrectException => {
          //var s = aex.getContent
          addError(e.getMessage(), line, e.getLineNumber(), e.getColumnNumber(),
            false)
        }
        //line in which xml error was had been removed, do nothing
        case lex: LineEmptyException => {
        }
        //other exception!
        case ex: Exception => {
          ex.printStackTrace()
          addError(e.getMessage(), "Normal Exception in ValidationErrorHandler!",
            e.getLineNumber(), e.getColumnNumber(), false)
        }
      }
    }
  }

  def getXmlElement(lines: Array[String], lineNumber: Int,
                    invert: Boolean): String = {
    //replace / with "" if <br/>
    var label = ""
    var NewLineNumber = lineNumber
    val startLine = lines(lineNumber)
    log.debug("GetXmlElementStartLine:" + startLine)
    //happens if delete marked span gets deteled, and then the next is
    // the span which is on the wrong place.... just keep the text the same!
    if (startLine.startsWith("<") == false || startLine == "") {
      // line is empty, or is normal sentence (look above)
      throw new LineEmptyException("Starting Line is Emptry")
    }
    try {
      label = xml.XML.loadString(startLine).label
      linesCount = 1
      return startLine
    }
    catch {
      case ex: Exception => {
        if (startLine.startsWith("<")) {
          label = startLine.split(">")(0).replace("<", "").split(" ")(0)
        }
        else {
          label = lines(lineNumber - 1).tail.dropRight(1)
          NewLineNumber = lineNumber - 1
        }
      }
    }
    if (invert) {
      label = label.tail
    }
    var conntent = ""
    var labels = 0
    val goal = if (invert) 0 else lines.length
    val step = if (invert) -1 else 1
    for (i <- NewLineNumber to goal by step) {
      var cLine = ""
      try {
        cLine = lines(i)
      } catch {
        case ex: Exception => {
          //println("Code1:" + startLine)
          if (startLine.length() > 50) {
            throw new AutoCorrectException("Start Line seems to long")
          }
        }
      }
      try {
        if ((lines(i).trim().startsWith("<" + label) &&
          lines(i).trim().endsWith("</" + label + ">") ||
          lines(i).trim().startsWith("<" + label + "/>")) &&
          labels == 0) {
          linesCount = i - NewLineNumber
          return lines(i)
        } else if (lines(i).trim().startsWith("<" + label)) {
          if (invert) {
            conntent = lines(i) + conntent
            labels = labels + step
            if (labels == 0)
              linesCount = NewLineNumber - i
            return conntent
          } else {
            if (lines(i).endsWith("</" + label + ">") == false) {
              //if a label is opened and closed in this line, i do not need
              // to increment labels
              labels = labels + step
            }
            conntent = conntent + lines(i)
          }
        } else if (lines(i).trim().startsWith("</" + label + ">")) {
          if (invert) {
            conntent = lines(i) + conntent
          } else {
            conntent = conntent + lines(i)
          }
          labels = labels - step
          if (labels == 0) {
            linesCount = i - NewLineNumber
            return conntent
          }
        } else {
          if (invert) {
            conntent = lines(i) + conntent

          } else {
            conntent = conntent + lines(i)
          }
        }
      }
      catch {
        //happens if deleteable span is deleted an inside was a element + PCDATA
        //<span delete="true"><strong>Bold</strong>Text</span> ->
        //<strong>Bold</strong>Text
        //<strong>Bold</strong>Text causes this error!
        case ai: ArrayIndexOutOfBoundsException => {
        }
        case ex: Exception => {
          log.debug("Error in get Xml element")
          ex.printStackTrace()
        }
      }
    }
    linesCount = lines.length - NewLineNumber
    conntent
  }

  def updateLines(lines: Array[String], startLineNumber: Int,
                  newContent: String, inverted: Boolean): String = {
    var newLines = lines.clone() //for debug
    newLines(startLineNumber) = newContent

    var goal = if (inverted) startLineNumber - linesCount else startLineNumber + linesCount
    var step = if (inverted) -1 else 1

    if (linesCount > 1) {
      for (i <- (startLineNumber + step) to goal by step) {
        newLines(i) = ""
      }
    }
    var newXml = newLines.mkString(System.getProperty("line.separator"))
    try {
      xml.XML.loadString(newXml)
    }
    catch {
      case e: Exception => {
        e.printStackTrace()
        log.debug("Error in updateLines:" + newXml)
        var ace = new AutoCorrectException("Xml after updateLines Invalid")
        ace.setContent(lines.mkString("\n"))
        throw ace
      }
      case ex: SAXParseException => {
        ex.printStackTrace()
        log.debug("Error in updateLines:" + newXml)
        var ace = new AutoCorrectException("Xml after updateLines Invalid")
        ace.setContent(lines.mkString("\n"))
        throw ace
      }
    }
    newXml
  }

  def addError(msg: String, lineContent: String, line: Int, column: Int,
               autocorrected: Boolean): Unit = {
    errors = errors :+ ValidationError(msg, lineContent, line, column,
      autocorrected)
  }

  def error(e: SAXParseException) = {
    warning(e)
  }

  def getErrorList(): List[ValidationError] = {
    errors.toList
  }

}
