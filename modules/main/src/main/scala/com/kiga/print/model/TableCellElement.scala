package com.kiga.print.model


import scala.xml.Elem
import scala.beans.BeanProperty

/**
 * Created by peter on 12.01.16.
 */
class TableCellElement(val content: String, val border: Boolean = true,
val bold: Boolean = false,
                       var textAlign: String = "right",
val width: String = "") extends Printable {

  @BeanProperty var widthMM: Integer = _
  @BeanProperty var marginTopMM: Integer = _

  super.setText(content)

  override def getFop(): Elem = {
    val font = if (bold) "bold" else "normal"
    val borderXml = if (border) "solid 0.2mm gray" else ""
    var element: xml.Elem = if(width == ""){
      <fo:table-cell margin-top={attributeToString(Option(marginTopMM),"mm")} font-weight={font} text-align={textAlign} border={borderXml} padding="0.5mm" width={width}>{if(text != null) <fo:block>{text}</fo:block>}</fo:table-cell>
    }
    else{
      <fo:table-cell margin-top={attributeToString(Option(marginTopMM),"mm")}  width={attributeToString(Option(widthMM),"mm")}  font-weight={font} text-align={textAlign} border={borderXml} padding="0.5mm">{if(text != null) <fo:block>{text}</fo:block>}</fo:table-cell>
    }
    return element.copy(child = element.child ++ this.children.map(_.getFop()))
  }


}

object TableCellElement {
  def Invisible(): TableCellElement = {
    new TableCellElement("", false, false)
  }
}
