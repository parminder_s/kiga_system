package com.kiga.print.validation.db.repository

import com.kiga.print.validation.db.model.IdeaInReport
import org.springframework.data.repository.CrudRepository

/**
 * Created by peter on 24.11.15.
 */
trait IdeaInReportRepository extends CrudRepository[IdeaInReport, java.lang.Long] {
  def countByReportId(reportId: Long): Int

  def findAllByReportIdAndLanguage(reportId: Long, language: String): java.util.List[IdeaInReport]

  def findAllByReportId(reportId: Long): java.util.List[IdeaInReport]

}
