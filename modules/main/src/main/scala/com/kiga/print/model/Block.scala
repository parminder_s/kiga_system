package com.kiga.print.model

import scala.beans.BeanProperty
import scala.xml.{UnprefixedAttribute, MetaData}


/**
  * Created by peter on 23.02.16.
  */
class Block(textVal: String = null) extends Printable {

  this.text = textVal

  @BeanProperty var fontSize: Integer = _
  @BeanProperty var marginMM: Integer = _
  @BeanProperty var paddingMM: Integer = _
  @BeanProperty var fontWeight: String = _
  @BeanProperty var marginTopMM: Integer = _
  @BeanProperty var marginBottomMM: Integer = _
  @BeanProperty var marginLeftMM: Integer = _
  @BeanProperty var marginRightMM: Integer = _
  @BeanProperty var color: String = _
  @BeanProperty var textAlign: String = _
  @BeanProperty var borderTopStyle: String = _
  @BeanProperty var border: String = _
  @BeanProperty var keepTogetherWithinPage: String = _
  override def getFop(): xml.Elem = {
    var element: xml.Elem = <fo:block keep-together.within-page={ attributeToString(Option(keepTogetherWithinPage))} color={attributeToString(Option(color))} font-weight={attributeToString(Option(fontWeight))} margin={attributeToString(Option(marginMM), "mm")} padding={attributeToString(Option(paddingMM), "mm")} font-size={attributeToString(Option(fontSize))} margin-top={attributeToString(Option(marginTopMM), "mm")} margin-bottom={attributeToString(Option(marginBottomMM), "mm")} margin-left={attributeToString(Option(marginLeftMM), "mm")}  margin-right={attributeToString(Option(marginRightMM), "mm")} text-align={attributeToString(Option(textAlign))} border-top-style={attributeToString(Option(borderTopStyle))} border={attributeToString(Option(border))}>{text}</fo:block>
    return element.copy(child = element.child ++ this.children.map(_.getFop()))
  }
}
