package com.kiga.print.validation

import com.kiga.print.validation.db.model.ErrorGroup
import com.kiga.print.validation.db.model.ErrorGroup.ErrorGroup
import org.slf4j.LoggerFactory

/**
 * Created by peter on 09.09.15.
 */
object ValidationHelper {

  val log = LoggerFactory.getLogger(getClass)

  def getErrorGroup(msg:String):ErrorGroup = {
    val retValue = if (msg.contains("Invalid content was found starting with element")) {
      return ErrorGroup.InvalidElement
    }
    if (msg.contains("must appear on element")) {
      return ErrorGroup.AttributeMissing
    }
    else if (msg.contains("is not allowed to appear in element")) {
      return ErrorGroup.WrongAttribute
    }
    else if (msg.contains("must have no element [children], and the value must be valid.")) {
      return ErrorGroup.NoElementChildren
    }
    else if (msg.contains("cannot have character [children], because the type's content type is element-only.")) {
      return ErrorGroup.NoCharacterChildren
    }
    else if (msg.contains("is not a valid value for") || msg.contains("is not valid with respect to its type") ){
      return ErrorGroup.WrongAttributeValue
    }
    else if (msg.contains("Invalid Img url")){
      return ErrorGroup.InvalidImageUrl
    }
    else if (msg.contains("Invalid Image Type")) {
      return ErrorGroup.InvalidImageType
    }
    else if (msg.contains("S3Image Image Missing")){
      return ErrorGroup.S3ImageMissing
    }
    else {
      return ErrorGroup.Unknown
    }
  }

  def prettyFormatXML(input:String):String = {
    //if <img ..../> becomes <img ....></img> line is too long (more than 300 characters!
    val p = new scala.xml.PrettyPrinter(300, 0)
    /*Replacing </span> <span>, because otherwise
    <p><span>also thanked</span> <span>the animals</span></p>
    would be converted to. So i lose the spaces.
    <p>
    <span>also thanked</span>
    <span>the animals</span>
    </p>

    * */
    var t = xml.XML.loadString(input.replace(" <span>","<span> "))
    p.format(t)
  }

  def markSpansToDelete(input:String):String = {
    log.debug("Marking spans without class or style attribute to be deleted!")
    input.replace("<span>","<span delete=\"true\">")
  }

}
