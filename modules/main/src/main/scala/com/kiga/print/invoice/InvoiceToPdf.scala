package com.kiga.print.invoice

import java.util.Date

import com.kiga.print.article.FopXml
import com.kiga.print.model.TableCellElement
import com.kiga.shop.document.model.{PrintInvoiceData, PrintOrderNoteData, PrintShippingNoteData}
import com.kiga.shop.web.requests.PrintOrderConfirmationRequest
import javax.inject.Named
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._

/**
  * Created by peter on 12.01.16.
  */
@Named
class InvoiceToPdf {

  val log = LoggerFactory.getLogger(getClass)

  def printCreditVoucher(invoice: PrintInvoiceData) = {
    //TODO: implement CreditVoucher
  }

  def orderConfirmation(order: PrintOrderConfirmationRequest) = {
    var flow = <fo:flow flow-name="xsl-region-body"></fo:flow>
    //Adresse

    //Wie Rechnung:Auftagsbestätigung - Rechts Datum und Bestellnummer Kundennummer

    //Tabelle mit einträgen welche Produkte es gibt

    //Vielen Dank für ihre Bestellung

  }

  def printShippingNote(delivery: PrintShippingNoteData) = {
    var deliveryFop = <fo:flow flow-name="xsl-region-body"></fo:flow>
    deliveryFop = deliveryFop.copy(
      child = deliveryFop.child :+ XmlTemplates.addressTable(
        delivery.shippingAddress,
        delivery.billAddress)
    )

    deliveryFop = deliveryFop.copy(
      child = deliveryFop.child :+ <fo:block margin-top="3.8cm"></fo:block> :+
        XmlTemplates.table(
          List(
            new TableCellElement("", false, false),
            new TableCellElement("", false, false),
            new TableCellElement("", false, false, "left", "0.5cm"),
            new TableCellElement("", false, false, "left", "2.6cm"))
          ,
          List(
            List(
              new TableCellElement("", false, false),
              new TableCellElement("Lieferscheindatum:", false, false, "right"),
              new TableCellElement("", false),
              new TableCellElement(new java.text.SimpleDateFormat(
                "dd.MM.yyyy").format(new Date()), false, false, "left")
            ), List(
              new TableCellElement("", false),
              new TableCellElement("Bestellnummer:", false, false, "right"),
              new TableCellElement("", false),
              new TableCellElement(delivery.orderNr, false, false, "left")
            ), List(
              new TableCellElement("", false),
              new TableCellElement("Versandort:", false, false, "right"),
              new TableCellElement("", false),
              new TableCellElement(delivery.deliveryPlace, false, false, "left")
            ), List(
              new TableCellElement("", false),
              new TableCellElement("Kundennummer:", false, false, "right"),
              new TableCellElement("", false),
              new TableCellElement(delivery.customerNr, false, false, "left")
            )
          )
        ))


    deliveryFop = deliveryFop.copy(
      child = deliveryFop.child :+ XmlTemplates.block(
        "Lieferschein: " + delivery.deliveryNr, "left", 12, true)
    )


    val header = List(
      new TableCellElement("Pos.", true, false, "center", "1cm"),
      new TableCellElement("Beschreibung", true, false, "center"),
      new TableCellElement("Artikelnr.", true, false, "center", "6cm"),
      new TableCellElement("Stk.", true, false, "center", "1.4cm"),
      new TableCellElement("Gew. (Kg)", true, false, "center", "1.7cm")
    )
    val products = delivery.tableCellItems.zip(Stream from 1).map(x => List(
      new TableCellElement(x._2.toString(), true, false, "left"),
      new TableCellElement(x._1.getTitle(), true, false, "left"),
      new TableCellElement(x._1.getCode(), true, false, "left"),
      new TableCellElement(x._1.getAmount(), true, false, "left"),
      new TableCellElement(x._1.getWeightGramTotal(), true, false, "right")
    )
    )
    deliveryFop = deliveryFop.copy(
      child = deliveryFop.child :+ XmlTemplates.block("", "left", 10).copy(
        child = XmlTemplates.table(header, products.toList ++ List(
          List(
            new TableCellElement("", false),
            new TableCellElement("", false),
            new TableCellElement("", false),
            new TableCellElement("Summe:", false),
            new TableCellElement(delivery.sumWeight)
          )
        )
        )
      )
    )

    deliveryFop = deliveryFop.copy(
      child = deliveryFop.child :+ XmlTemplates.totalDeliver(
        delivery.totalDelivery)
    )
    var complete = XmlTemplates.lieferschein("kundenservice@kigaportal.com",
      "HOHE Medien OG",
      "A-8010 Graz Klosterwiesgasse 101b", deliveryFop)
    FopXml.createPDF(delivery.deliveryNr.toString + ".pdf", complete)
  }

  def printInvoiceCc(invoice: PrintInvoiceData) = {
    var paymentText = invoice.paymentMethod.toString
    if (paymentText == "MP-CC") {
      paymentText = "Kreditkarte"
    }
    if (paymentText == "MP-PAYPAL") {
      paymentText = "PayPal"
    }
    if (paymentText == "SOFORT") {
      paymentText = "Sofort Überweisung"
    }

    var invoiceFop = <fo:flow flow-name="xsl-region-body"></fo:flow>
    invoiceFop = invoiceFop.copy(
      child = invoiceFop.child :+ XmlTemplates.addressTable(
        invoice.shippingAddress,
        invoice.billAddress))

    invoiceFop = invoiceFop.copy(
      child = invoiceFop.child :+ XmlTemplates.table(
        List(
          new TableCellElement("", false),
          new TableCellElement("", false),
          new TableCellElement("", false, false, "left", "0.5cm"),
          new TableCellElement("", false, false, "left", "2.6cm")
        ),
        List(
          List(
            new TableCellElement("", false),
            new TableCellElement("Rechnungsdatum:", false, false, "right"),
            new TableCellElement("", false),
            new TableCellElement(invoice.date, false, false, "left")
          ),
          List(
            new TableCellElement("", false),
            new TableCellElement("Bestellnummer:", false, false, "right"),
            new TableCellElement("", false),
            new TableCellElement("" + invoice.orderNumber, false, false, "left")
          )
          , List(
            new TableCellElement("", false),
            new TableCellElement("Kundennummer:", false, false, "right"),
            new TableCellElement("", false),
            new TableCellElement("" + invoice.customerId, false, false, "left")
          ))
      )
    )


    invoiceFop = invoiceFop.copy(
      child = invoiceFop.child :+ XmlTemplates.block(
        "Rechnung: " + invoice.invoiceNr, "left", 12, true)
    )

    var headers = List(
      new TableCellElement("Stk.", true, false, "center", "1cm"),
      new TableCellElement("Beschreibung", true, false, "center"),
      new TableCellElement("Artikelnr.", true, false, "center", "4.2cm"),
      new TableCellElement("Brutto Preis /Stk", true, false, "center", "2cm"),
      new TableCellElement("MWSt (%)", true, false, "center", "1.7cm"),
      new TableCellElement("Summe Brutto", true, false, "center", "1.5cm")
    )
    var products = invoice.products.toList.map(x => List[TableCellElement](
      new TableCellElement(x.getAmount(), true, false, "left"),
      new TableCellElement(x.getTitle(), true, false, "left"),
      new TableCellElement(x.getCode(), true, false, "left"),
      new TableCellElement(x.getPriceGrossSingle().toString(), true, false, "right"),
      new TableCellElement(x.getVatRateString(), true, false, "right"),
      new TableCellElement(x.getPriceGrossTotal().toString(), true, false, "right")
    ))
    invoiceFop = invoiceFop.copy(
      child = invoiceFop.child :+ XmlTemplates.block("", "left", 10).copy(
        child = XmlTemplates.table(headers, products)
      )
    )

    var tableCellSummery = List(
      List(
        new TableCellElement("", false, false, "right", "10cm"),
        new TableCellElement("Nettobetrag", true, false, "left"),
        new TableCellElement(invoice.priceNetTotal.toString, true, false)
      )) ++ invoice.vatSumPerRate.map(x => List(
      new TableCellElement("", false, false, "right", "10cm"),
      new TableCellElement("MwSt " + x.prettyRate + "%", true, false, "left"),
      new TableCellElement(x.total.toString)
    )).toList ++
      List(
        List(
          new TableCellElement("", false),
          new TableCellElement("Bruttobetrag", true, false, "left"),
          new TableCellElement(invoice.priceGrossTotal.toString, true, false)
        ),
        List(
          new TableCellElement("", false),
          new TableCellElement("Überweisungsbetrag", true, true, "left"),
          new TableCellElement(invoice.currency + " " + invoice.transferalSum.toString, true,
            true)
        )
      )

    invoiceFop = invoiceFop.copy(
      child = invoiceFop.child
        :+ xml.XML.loadString(
        "<fo:block keep-together.within-page=\"always\" margin-top=\"0.5cm\"></fo:block>").copy(
        child = XmlTemplates.table(List(), tableCellSummery)
          :+ xml.XML.loadString(
          "<fo:block margin-top=\"-2cm\" margin-bottom=\"3cm\"></fo:block>").copy(
          child =
            XmlTemplates.block("Vielen Dank,", "left")
              :+ XmlTemplates.block("Ihr KiGaPortal-Team", "left")
        )) :+ XmlTemplates.invoicePayData(
        s"Zahlung erfolgte durch: $paymentText")
    )
    var complete = XmlTemplates.invoice("kundenservice@kigaportal.com",
      "HOHE Medien OG",
      "A-8010 Graz Klosterwiesgasse 101b", invoiceFop)
    FopXml.createPDF(invoice.invoiceNr + ".pdf", complete)
  }


  def printOrder(delivery: PrintOrderNoteData) = {
    var deliveryFop = <fo:flow flow-name="xsl-region-body"></fo:flow>
    deliveryFop = deliveryFop.copy(
      child = deliveryFop.child :+ XmlTemplates.addressTable(
        delivery.shippingAddress,
        delivery.billAddress)
    )

    deliveryFop = deliveryFop.copy(
      child = deliveryFop.child :+ <fo:block margin-top="3.8cm"></fo:block> :+
        XmlTemplates.table(
          List(
            new TableCellElement("", false, false),
            new TableCellElement("", false, false),
            new TableCellElement("", false, false, "left", "0.5cm"),
            new TableCellElement("", false, false, "left", "2.6cm"))
          ,
          List(
            List(
              new TableCellElement("", false, false),
              new TableCellElement("Auftragsdatum:", false, false, "right"),
              new TableCellElement("", false),
              new TableCellElement(new java.text.SimpleDateFormat(
                "dd.MM.yyyy").format(new Date()), false, false, "left")
            ), List(
              new TableCellElement("", false),
              new TableCellElement("Bestellnummer:", false, false, "right"),
              new TableCellElement("", false),
              new TableCellElement(delivery.orderNr, false, false, "left")
            ), List(
              new TableCellElement("", false),
              new TableCellElement("Versandort:", false, false, "right"),
              new TableCellElement("", false),
              new TableCellElement(delivery.deliveryPlace, false, false, "left")
            ), List(
              new TableCellElement("", false),
              new TableCellElement("Kundennummer:", false, false, "right"),
              new TableCellElement("", false),
              new TableCellElement(delivery.customerNr, false, false, "left")
            )
          )
        ))


    deliveryFop = deliveryFop.copy(
      child = deliveryFop.child :+ XmlTemplates.block(
        "Auftragsschein: " + delivery.orderNr, "left", 12, true)
    )


    val header = List(
      new TableCellElement("Pos.", true, false, "center", "1cm"),
      new TableCellElement("Beschreibung", true, false, "center"),
      new TableCellElement("Artikelnr.", true, false, "center", "6cm"),
      new TableCellElement("Stk.", true, false, "center", "1.4cm"),
      new TableCellElement("Gew. (Kg)", true, false, "center", "1.7cm")
    )
    val products = delivery.tableCellItems.zip(Stream from 1).map(x => (
      List(
        new TableCellElement(x._2.toString, true, false, "left"),
        new TableCellElement(x._1.getTitle + "", true, false, "left"),
        new TableCellElement(x._1.getCode + "", true, false, "left"),
        new TableCellElement(x._1.getAmount(), true, false, "left"),
        new TableCellElement(x._1.getWeightGramTotal, true, false, "right")
      )
      )
    )
    deliveryFop = deliveryFop.copy(
      child = deliveryFop.child :+ XmlTemplates.block("", "left", 10).copy(
        child = XmlTemplates.table(header, products.toList ++ List(
          List(
            new TableCellElement("", false),
            new TableCellElement("", false),
            new TableCellElement("", false),
            new TableCellElement("Summe:", false),
            new TableCellElement(delivery.sumWeight)
          )
        )
        )
      )
    )

    deliveryFop = deliveryFop.copy(
      child = deliveryFop.child :+ XmlTemplates.totalDeliver(
        delivery.totalDelivery)
    )
    var complete = XmlTemplates.lieferschein("kundenservice@kigaportal.com",
      "HOHE Medien OG",
      "A-8010 Graz Klosterwiesgasse 101b", deliveryFop)
    FopXml.createPDF(delivery.orderNr.toString + ".pdf", complete)
  }
}
