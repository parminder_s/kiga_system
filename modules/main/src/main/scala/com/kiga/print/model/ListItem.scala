package com.kiga.print.model

import scala.beans.BeanProperty
import scala.xml.Elem

/**
  * Created by peter on 23.02.16.
  */
class ListItem(
                @BeanProperty var label: Printable,
                @BeanProperty var body: Printable) extends Printable{


  @BeanProperty var marginMM: Integer = null
  @BeanProperty var paddingMM: Integer = null

  @BeanProperty var marginTopMM: Integer = null
  @BeanProperty var marginBottomMM: Integer = null
  @BeanProperty var marginLeftMM: Integer = null
  @BeanProperty var marginRightMM: Integer = null

  override def getFop(): Elem = {
    var element = <list-item margin={attributeToString(Option(marginMM), "mm")} margin-top={attributeToString(Option(marginTopMM), "mm")} margin-bottom={attributeToString(Option(marginBottomMM), "mm")} margin-left={attributeToString(Option(marginLeftMM), "mm")}  margin-right={attributeToString(Option(marginRightMM), "mm")}><list-item-label end-indent="label-end()">{label.getFop()}</list-item-label><list-item-body start-indent="body-start()">{body.getFop()}</list-item-body></list-item>
    return element
  }
}
