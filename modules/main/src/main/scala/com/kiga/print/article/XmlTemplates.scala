package com.kiga.print.article

/**
 * Created by peter on 30.06.15.
 * FOP templates that are used to construct documents such as ideas, vouchers (? or not) , etc.
 */
object XmlTemplates {
  val fontFamily = "Helvetica"
  val CONTENT_WIDTH_SPAN_1 = "35mm"
  val CONTENT_WIDTH_SPAN_2 = "57.5mm"
  val CONTENT_WIDTH_SPAN_3 = "125mm"
  val CONTENT_WIDTH_SPAN_4 = "125mm"

  def staticImageInTableWith2(url:String):xml.Elem = {
      return <external-graphic src={url} content-width="74mm"  border-width="0.49mm" border-style="solid" border-color="black" font-size="0"/>
    }

  def voucher(from:String,to:String,message:String,code:String,hint:String,imgPath:String): xml.Elem ={
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
      <fo:layout-master-set>
        <fo:simple-page-master
        margin-right="0cm" margin-left="0cm"
        margin-bottom="0cm" margin-top="0cm"
        font-family="sans-serif" page-width="21cm"
        page-height="29.7cm" master-name="main">
          <fo:region-body margin-bottom="3cm" />
          <fo:region-before extent="0.5cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="main" font-size="14">
        <fo:flow flow-name="xsl-region-body">
          <fo:block-container z-index="1" position="absolute" overflow="hidden">
            <fo:block><fo:external-graphic src={imgPath} /></fo:block>
          </fo:block-container>
          <fo:block-container z-index="2" position="absolute" left="0cm" top="5cm" overflow="hidden">
            <fo:table margin-top="15mm">
              <fo:table-body>
                <fo:table-row >
                  <fo:table-cell >
                    <fo:block text-align="center" margin-right="4cm" margin-left="4cm" margin-top="5mm">{from}</fo:block>
                  </fo:table-cell>
                </fo:table-row>
                <fo:table-row >
                  <fo:table-cell>
                    <fo:block text-align="center" margin-right="4cm" margin-left="4cm" margin-top="5mm">{to}</fo:block>
                  </fo:table-cell>
                </fo:table-row>
                <fo:table-row >
                  <fo:table-cell>
                    <fo:block text-align="center" margin-right="4cm" margin-left="4cm" margin-top="11cm">{message}</fo:block>
                  </fo:table-cell>
                </fo:table-row>
                <fo:table-row >
                  <fo:table-cell>
                    <fo:block text-align="center" margin-right="4cm" margin-left="4cm" margin-top="1cm">{code}</fo:block>
                  </fo:table-cell>
                </fo:table-row>
                <fo:table-row >
                  <fo:table-cell >
                    <fo:block text-align="center" margin-right="4cm" margin-left="4cm" margin-top="0cm" font-size="10">{hint}</fo:block>
                  </fo:table-cell>
                </fo:table-row>
              </fo:table-body>
            </fo:table>
          </fo:block-container>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  }

  def staticTableWith2Images(xml_first:xml.Elem,xml_second:xml.Elem):xml.Elem = {
      <table table-layout="fixed" padding-bottom="0mm" margin-top="5mm">
        <table-column column-width="12.5mm"/>
        <table-column column-width="74mm"/>
        <table-column column-width="5mm"/>
        <table-column column-width="74mm"/>
        <table-column column-width="12.5mm"/>
        <table-body>
          <table-row>
            <table-cell>
              <block font-size="0"/>
            </table-cell>
            <table-cell>
              <block font-size="0" text-align="center">
                {xml_first}
              </block>
            </table-cell>
            <table-cell>
              <block font-size="0"/>
            </table-cell>
            <table-cell>
              <block font-size="0" text-align="center">
                {xml_second}
              </block>
            </table-cell>
            <table-cell>
              <block font-size="0"/>
            </table-cell>
          </table-row>
        </table-body>
      </table>
    }
  def staticTableWith1Image(url:String): xml.Elem = {
    return <table table-layout="fixed" padding-bottom="0mm">
      <table-column column-width="0mm"/>
      <table-column column-width="179mm"/>
      <table-column column-width="0mm"/>
      <table-body>
        <table-row>
          <table-cell>
            <block font-size="0"/>
          </table-cell>
          <table-cell>
            <block font-size="0" text-align="center">
              <external-graphic src={url} content-width="168.03147268409mm" border-width="0.49mm" border-style="solid" border-color="black" font-size="0"/>
            </block>
          </table-cell>
          <table-cell>
            <block font-size="0"/>
          </table-cell>
        </table-row>
      </table-body>
    </table>
  }

  def a4Site(url:String):xml.Elem = {
    <block break-after="page">
      <block-container width="210mm" height="297mm" top="0cm" left="0cm" position="absolute">
        <block>
          <external-graphic src={url}
                            content-height="297mm" content-width="210mm"/>
        </block>
      </block-container>
      <block-container width="210mm" height="297mm" top="0cm" left="0cm" position="absolute">
        <table table-layout="fixed" width="100%" margin-top="277mm">
          <table-column column-width="210mm"/>
          <table-body>
            <table-row border="0mm"

                       margin="0mm">
              <table-cell>
                <table table-layout="fixed"
                       width="210mm">
                  <table-column column-width="170mm"/>
                  <table-column column-width="40mm"/>
                  <table-body>
                    <table-row height="10mm">
                      <table-cell padding-left="20mm" display-align="center">
                        <block font-size="10pt" margin="0mm">© www.kigaportal.com</block>
                      </table-cell>
                      <table-cell padding-right="20mm" display-align="center">
                        <block text-align="right" overflow="auto">
                          <external-graphic src="http://kigastatic2.s3.amazonaws.com/images/logo_de.png" content-height="8mm"/>
                        </block>
                      </table-cell>
                    </table-row>
                  </table-body>
                </table>
              </table-cell>
            </table-row>
          </table-body>
        </table>
      </block-container>
    </block>
  }

  def a4(content:xml.Elem):xml.Elem = {
    <root xmlns="http://www.w3.org/1999/XSL/Format">
      <layout-master-set>
        <simple-page-master
        master-name="frame"
        page-height="297mm"
        page-width="210mm"
        margin-top="0mm"
        margin-bottom="0mm"
        margin-left="0mm"
        margin-right="0mm">
          <region-body margin="0mm" display-align="center"/>
          <region-before extent="0mm"/>
          <region-after extent="20mm"/>
          <region-start extent="0mm"/>
          <region-end extent="0mm"/>
        </simple-page-master>
      </layout-master-set>
      <page-sequence id="last-page" master-reference="frame">
        <flow flow-name="xsl-region-body" font-family={fontFamily}>
          {content}
        </flow>
      </page-sequence>
    </root>
  }

  def staticOld(xml_block:xml.Node): xml.Elem = {
    return <root xmlns="http://www.w3.org/1999/XSL/Format">
      <layout-master-set>
        <simple-page-master
        master-name="frame"
        page-height="297mm"
        page-width="210mm"
        margin-top="15mm"
        margin-bottom="20mm"
        margin-left="15mm"
        margin-right="15mm">
          <region-body margin-top="0mm" margin-bottom="0mm" display-align="center"/>
          <region-before/>
          <region-after/>
        </simple-page-master>
      </layout-master-set>
      <page-sequence id="last-page" master-reference="frame">
        <static-content flow-name="xsl-region-after" extent="15mm">
          <table table-layout="fixed" padding-bottom="0mm">
            <table-column column-width="150mm"/>
            <table-column column-width="30mm"/>
            <table-body font-family={fontFamily}>
              <table-row>
                <table-cell>
                  <block font-size="10pt" margin-top="0mm">© www.kigaportal.com</block>
                </table-cell>
                <table-cell>
                  <block text-align="right" font-size="10pt">
                    <block text-align="right">
                      <external-graphic src="url('http://kigastatic2.s3.amazonaws.com/images/logo_de.png')" content-height="10mm"/>
                    </block>
                  </block>
                </table-cell>
              </table-row>
            </table-body>
          </table>
        </static-content>
        <flow flow-name="xsl-region-body">
          {xml_block}
        </flow>
      </page-sequence>
    </root>
  }

  def dynamicSpan12(first:xml.Elem,second:xml.Elem):xml.Elem = {
    <block
    margin-left="55mm"
    font-size="9pt"
    color="#333333"
    font-family="Helvetica">
      <table table-layout="fixed" width="100%"  page-break-before="auto" >
        <table-column column-width={CONTENT_WIDTH_SPAN_1} />
        <table-column column-width="10mm" />
        <table-column column-width="80mm" />
        <table-body margin="0mm"  border="0mm">
          <table-row margin="0mm"  border="0mm" keep-together.within-column="always" >
            <table-cell margin="0mm"  border="0mm" keep-together.within-column="always">
              <block>
              </block>
              <block border="0mm"  margin="0mm" margin-bottom="0mm"  >
                {first}
                <inline></inline>
              </block>
            </table-cell>

            <table-cell margin="0mm"  border="0mm">
              <block>
              </block>
              <block>
              </block>
            </table-cell>

            <table-cell margin="0mm"  border="0mm">
              <block>
              </block>
              <block border="0mm"  margin="0mm" margin-bottom="0mm" text-align="left" >
                {second}
              </block>
            </table-cell>

          </table-row>
        </table-body>
      </table>
    </block>
  }

  def dynamicSpan1(first:xml.Elem,second:xml.Elem,third:xml.Elem):xml.Elem = {
    <block
    margin-left="55mm"
    font-size="9pt"
    color="#333333"
    font-family="Helvetica">
      <table table-layout="fixed" width="100%"  page-break-before="auto" >
        <table-column column-width={CONTENT_WIDTH_SPAN_1} />
        <table-column column-width="10mm" />
        <table-column column-width={CONTENT_WIDTH_SPAN_1} />
        <table-column column-width="10mm" />
        <table-column column-width={CONTENT_WIDTH_SPAN_1} />

        <table-body margin="0mm"  border="0mm">
          <table-row margin="0mm"  border="0mm" keep-together.within-column="always" >
            <table-cell margin="0mm"  border="0mm" keep-together.within-column="always">
              <block>
              </block>
              <block border="0mm"  margin="0mm" margin-bottom="0mm"  >
                {first}
                <inline></inline>
              </block>
            </table-cell>

            <table-cell margin="0mm"  border="0mm">
              <block>
              </block>
              <block>
              </block>
            </table-cell>

            <table-cell margin="0mm"  border="0mm">
              <block>
              </block>
              <block border="0mm"  margin="0mm" margin-bottom="0mm" >
                {second}
              </block>
            </table-cell>

            <table-cell margin="0mm"  border="0mm">
              <block>
              </block>
              <block>
              </block>
            </table-cell>

            <table-cell margin="0mm"  border="0mm">
              <block>
              </block>
              <block border="0mm"  margin="0mm" margin-bottom="0mm" >
                {third}
              </block>
            </table-cell>

          </table-row>
        </table-body>
      </table>
    </block>
  }

  def dynamicSpan2(content_left:xml.Elem,content_right:xml.Elem):xml.Elem = {
    return <block
    margin-left="55mm"
    font-size="9pt"
    color="#333333"
    font-family="Helvetica">
      <table table-layout="fixed" width="100%" page-break-before="auto" >
        <table-column column-width={CONTENT_WIDTH_SPAN_2} />
        <table-column column-width="10mm" />
        <table-column column-width={CONTENT_WIDTH_SPAN_2} />
        <table-body margin="0mm"  border="0mm">
          <table-row margin="0mm"  border="0mm" keep-together.within-column="always" >
            <table-cell margin="0mm"  border="0mm" keep-together.within-column="always">
              <block>
              </block>
              <block border="0mm"  margin="0mm" margin-bottom="0mm"  >
                {content_left}
                <inline></inline>
              </block>
            </table-cell>
            <table-cell margin="0mm"  border="0mm">
              <block>
              </block>
              <block>
              </block>
            </table-cell>
            <table-cell margin="0mm"  border="0mm">
              <block>
              </block>
              <block border="0mm"  margin="0mm" margin-bottom="0mm" >
                {content_right}
              </block>
            </table-cell>
          </table-row>
        </table-body>
      </table>
    </block>
  }

  def dynamicTable(content:xml.Elem):xml.Elem = {
  return  <block
  margin-left="55mm"
  font-size="9pt"
  color="#333333"
  font-family="Helvetica">
    <table table-layout="fixed" width="100%" page-break-before="auto" >
      <table-column column-width={CONTENT_WIDTH_SPAN_3} />
      <table-body margin="0mm"  border="0mm">
        <table-row margin="0mm"  border="0mm">
          <table-cell margin="0mm"  border="0mm">
            {content}
          </table-cell>
        </table-row>
      </table-body>
    </table>
  </block>
  }

  def dynamicListBlock(): xml.Elem = {
    return <list-block margin-top="4mm" border="0mm"  provisional-distance-between-starts="2mm" provisional-label-separation="1mm"></list-block>
  }
  def dynamicListItem(text:String,marginBottom:String):xml.Elem = {
    <list-item margin-bottom={marginBottom}  margin-top="3mm"   >
      <list-item-label end-indent="label-end()">
        <block>&#x2022;</block>
      </list-item-label>
      <list-item-body start-indent="body-start()">
          <block border="0mm"  margin="0mm">
            <inline>{text}</inline>
          </block>
      </list-item-body>
    </list-item>
  }

  def dynamicListItem(inline:xml.Elem,marginBottom:String):xml.Elem = {
    <list-item  margin-bottom={marginBottom} margin-top="3mm" >
      <list-item-label end-indent="label-end()">
        <block>&#x2022;</block>
      </list-item-label>
      <list-item-body start-indent="body-start()" >
        <block border="0mm"  margin="0mm"  >
            {inline}
        </block>
      </list-item-body>
    </list-item>
  }


  def dynamicH1(text:String):xml.Elem = {
    return <block font-size="12pt" font-weight="bold" margin-bottom="3mm" color="black">
      <block>{text}</block>
    </block>
  }

  def dynamicH2(text:String):xml.Elem = {
    return <block font-size="10pt" font-weight="bold" margin-bottom="3mm" color="black">
      <block>{text}</block>
    </block>
  }

  def dynamicH3(text:String):xml.Elem = {
    return <block font-size="9pt" font-weight="bold" margin-bottom="3mm" color="grey">
      <block>{text}</block>
    </block>
  }

  def dynamicEm(text:String): xml.Elem ={
    <inline font-family="Helvetica" font-style="italic" font-size="9pt" color="#3c3c3b">{text}</inline>
  }

  def dynamicPStrong(text:String):xml.Elem = {
    return <inline font-family="Helvetica" font-weight="bold" font-size="9pt" color="#3c3c3b">{text}</inline>
  }
  def dynamicP(text:String,marginBottom:String,cssStyles: CssStyles):xml.Elem = {
    <block text-align={cssStyles.textAlign} margin-bottom={marginBottom}>{text}</block>
  }
  def dynamicInliner(text:String):xml.Elem = {
    return <inline>{text}</inline>
  }
  def dynamicImage(url:String,content_width:String): xml.Elem = {
    return <external-graphic src={url} content-width={content_width} margin="0mm"  border="0.15mm" border-style="solid" border-color="#4b4b4b"/>
  }

  def dynamicFlow(): xml.Elem = {
    <flow flow-name="xsl-region-body" font-family={fontFamily}></flow>
  }

  def dynamicHr(): xml.Elem = {
    return  <block margin-top="4mm" margin-bottom="5mm" >
              <block border-top-style="solid" border-top-color="#333333" border-top-width="0.1mm">
              </block>
            </block>
  }
  def dynamicFlow(xml_flow :xml.Elem,ideaTitle:String):xml.Elem = {
    return <root xmlns="http://www.w3.org/1999/XSL/Format">
      <layout-master-set>
        <page-sequence-master master-name="kiga-dynamic-pdf">
          <repeatable-page-master-alternatives>
            <conditional-page-master-reference
            page-position="first"
            master-reference="kiga-dynamic-first-page"/>
            <conditional-page-master-reference
            master-reference="kiga-dynamic-other-page"/>
          </repeatable-page-master-alternatives>
        </page-sequence-master>

        <simple-page-master master-name="kiga-dynamic-first-page"
                            page-height="297mm"
                            page-width="210mm"
                            margin-top="15mm"
                            margin-bottom="15mm"
                            margin-left="15mm"
                            margin-right="15mm">

          <region-body margin-top="10mm" margin-bottom="5mm"/>
          <region-before region-name="kiga-dynamic-first-page-before"/>
          <region-after extent="0mm"/>
          <region-start extent="0mm"/>
          <region-end extent="0mm"/>
        </simple-page-master>

        <simple-page-master master-name="kiga-dynamic-other-page"
                            page-height="297mm"
                            page-width="210mm"
                            margin-top="15mm"
                            margin-bottom="15mm"
                            margin-left="15mm"
                            margin-right="15mm">


          <region-body margin-top="10mm" margin-bottom="5mm"/>
          <region-before region-name="kiga-dynamic-other-page-before" extent="0mm"/>
          <region-after extent="0mm"/>
          <region-start extent="0mm"/>
          <region-end extent="0mm"/>
        </simple-page-master>

      </layout-master-set>

      <page-sequence id="last-page" master-reference="kiga-dynamic-pdf">
        <static-content flow-name="kiga-dynamic-first-page-before">
          <table table-layout="fixed" width="100%">
            <table-column column-width="55mm"/>
            <table-column column-width={CONTENT_WIDTH_SPAN_3}/>
            <table-body>
              <table-row>
                <table-cell>
                  <block text-align="left">
                    <external-graphic src="http://kigastatic2.s3.amazonaws.com/images/logo_de.png" content-height="10mm"/>
                  </block>
                </table-cell>
                <table-cell>
                  <block text-align="left" font-size="16pt">
                    <block>
                      {ideaTitle}
                    </block>
                  </block>
                </table-cell>
              </table-row>
            </table-body>
          </table>
        </static-content>
        <static-content flow-name="kiga-dynamic-other-page-before">
          <table table-layout="fixed" width="100%">
            <table-column column-width="55mm"/>
            <table-column column-width={CONTENT_WIDTH_SPAN_3}/>
            <table-body>
              <table-row>
                <table-cell>
                  <block text-align="left" font-size="10pt" font-family="Helvetica">
                    <block text-align="left">
                      <external-graphic src="http://kigastatic2.s3.amazonaws.com/images/logo_de.png" content-height="10mm"/>
                    </block>
                  </block>
                </table-cell>
                <table-cell>
                  <block>
                  </block>
                </table-cell>
              </table-row>
            </table-body>
          </table>
        </static-content>
        <static-content flow-name="xsl-region-after">
          <table table-layout="fixed" width="100%">
            <table-column column-width="55mm"/>
            <table-column column-width="55mm"/>
            <table-column column-width="70mm"/>
            <table-body font-family="Helvetica">
              <table-row>

                <table-cell>
                  <block>
                  </block>
                </table-cell>
                <table-cell>
                  <block font-size="10pt" color="#333333" text-align="left"  margin-top="0mm">
                    © www.kigaportal.com</block>
                </table-cell>
                <table-cell>
                  <block font-size="10pt" color="#333333" text-align="right">
                    <block text-align="right">
                      <page-number/>/<page-number-citation-last ref-id="last-page"/>
                    </block>
                  </block>
                </table-cell>
              </table-row>
            </table-body>
          </table>
        </static-content>{xml_flow}
      </page-sequence>
    </root>
  }
  def emptyBlock(marginBottom:String):xml.Elem = {
      return <block margin-bottom={marginBottom}>
      </block>
  }
  def emptyBlock(marginBottom:String,styles: CssStyles):xml.Elem = {
    return <block margin-bottom={marginBottom} text-align={styles.textAlign}>
    </block>
  }

  def emptyBlock():xml.Elem = {
    return <block></block>
  }

  def emptyInline():xml.Elem = {
    return <inline>
    </inline>
  }

}
