package com.kiga.print.model

import scala.xml.Elem

import scala.beans.BeanProperty

/**
  * Created by peter on 23.02.16.
  */
class Table extends Printable{

  @BeanProperty var columWidthsMM: List[Int] = List()

  @BeanProperty var tableLayout: String = null
  @BeanProperty var widthMM: Integer = null
  @BeanProperty var pageBreakBefore: String = null

  //table-layout="fixed" width="100%"  page-break-before="auto"

  override def getFop(): Elem = {
    var element = <fo:table table-layout={attributeToString(Option(tableLayout))} width={attributeToString(Option(widthMM),"mm")} page-break-before={attributeToString(Option(pageBreakBefore))}>{columWidthsMM.map(x =>if(x >= 0) <fo:table-column column-width={x.toString + "mm"}></fo:table-column> else <fo:table-column/>)}</fo:table>
    return element.copy(child = element.child ++ <fo:table-body>{this.children.map(_.getFop())}</fo:table-body>)
  }
}
