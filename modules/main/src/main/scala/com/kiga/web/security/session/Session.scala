package com.kiga.web.security.session
import java.util.Optional
import javax.servlet.http.HttpSession

import com.kiga.types.JLong

/**
 * Created by rainerh on 12.11.15.
 */
trait Session {

  def get(key: Optional[String]): Optional[String]

  def set(key: String)

  def setAttribute(key: String, value: Any)

  def getAttribute(key: String): Any

  def serializeSession(httpSession: HttpSession): String

  def getMemberId(): Optional[JLong]

  def getCrmUserId(): Int

  def getCrmSessionToken(): String

  def checkParallelLog(key: Optional[String]): Unit

}
