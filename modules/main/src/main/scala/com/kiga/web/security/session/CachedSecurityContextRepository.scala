package com.kiga.web.security.session

import java.util.Optional

import com.kiga.security.exception.ParallelLoginException
import javax.inject.{Inject, Named}
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.{SecurityContext, SecurityContextImpl}
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.context.{HttpRequestResponseHolder, HttpSessionSecurityContextRepository}

import scala.collection.JavaConversions._

/**
  * Created by Evgeny Zhoga on 27.02.15.
  * Based on data in http://basrikahveci.com/configuring-spring-security-to-save-the-securitycontext-to-places-other-then-session-between-requests/
  */
@Named
class CachedSecurityContextRepository extends HttpSessionSecurityContextRepository {
  private val log = LoggerFactory.getLogger(getClass)

  private val ADMIN_CONTEXT: SecurityContext = {
    val authorities = List(new SimpleGrantedAuthority("ROLE_ADMIN"))
    new SecurityContextImpl() {
      setAuthentication(
        new UsernamePasswordAuthenticationToken(
          new User("admin", "admin", authorities),
          null, authorities
        )
      )
    }
  }

  @Inject
  private var cachedSession: Session = _

  @Value(value = "${web.security.cookieName}")
  private var COOKIE_NAME: String = _

  @Inject
  private var delegate: HttpSessionSecurityContextRepository = _

  override def loadContext(requestResponseHolder: HttpRequestResponseHolder): SecurityContext = {
    val getCookies = Option(requestResponseHolder.getRequest.getCookies).getOrElse(Array())
    val sessionCookie = getCookies.find(_.getName == COOKIE_NAME).map(_.getValue)
    val sessionCookieJava = Optional.ofNullable(sessionCookie.orNull)
    if (sessionCookieJava.isPresent) {
      var sessionValue: Optional[String] = Optional.empty()

      try {
        sessionValue = cachedSession.get(sessionCookieJava)
      } catch {
        case parallelLoginException: ParallelLoginException =>
        case e: Exception => throw e
      }

      if (sessionValue.isPresent) {
        //disabling any spring security roles fetching from cached data here and postponing
        //everything to the application layer, which will have obviously its own security layer :(
        delegate.loadContext(requestResponseHolder)
      }
      else {
        delegate.loadContext(requestResponseHolder)
      }
    } else {
      delegate.loadContext(requestResponseHolder)
    }
  }

  override def containsContext(request: HttpServletRequest): Boolean = {
    delegate.containsContext(request)
    true
  }

  override def saveContext(context: SecurityContext, request: HttpServletRequest, response: HttpServletResponse): Unit = {

    //    delegate.saveContext(context, request, response)
  }
}
