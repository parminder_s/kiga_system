package com.kiga.web.security

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.{Bean, Configuration}

/**
  * Created by rainerh on 25.04.15.
  */

@EnableAutoConfiguration
@Configuration
@ConfigurationProperties("web.security.cache")
class CachedSessionConfig {
  @Bean def executor = scala.concurrent.ExecutionContext.fromExecutor(null)
}

