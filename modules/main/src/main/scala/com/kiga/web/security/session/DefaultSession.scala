package com.kiga.web.security.session

import java.util.Optional
import javax.inject.Inject
import javax.servlet.http.HttpSession

import com.kiga.security.exception.ParallelLoginException
import com.kiga.types.JLong
import net.liftweb.json.Serialization.write
import net.liftweb.json._
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContextExecutor

/**
 * Created by Evgeny Zhoga on 14.03.15.
 */

class DefaultSession(var executor: ExecutionContextExecutor, var cachedSession: CachedSession) extends Session {
  val expirationMinutes = 20

  var attributes = Map[String, Any]()

  val log = LoggerFactory.getLogger(getClass)


  implicit val formats = DefaultFormats

  override def get(key: Optional[String]): Optional[String] = {
    attributes = Map[String, Any]()
    val sessionString = cachedSession.get("session_" + key.get)
    if (sessionString != null && !sessionString.equals("")) {
      log.debug("Session: " + sessionString)
      setAttribute("CompleteSessionID", key.get)
      val x = parse(sessionString).values.asInstanceOf[Map[String, Any]]
      for((k, v) <- x){
        v match {
          case null => setAttribute(k, Nil)
          case _ => setAttribute(k, v)
        }
      }
      Optional.of(sessionString)
    } else {
      log.debug("Session: [session_" + key.get + "] not defined")
      Optional.empty()
    }
  }

  override def getMemberId(): Optional[JLong] = {
    attributes.get("loggedInAs") match {
      case Some(memberId: BigInt) => Optional.ofNullable(memberId.longValue())
      case _ => Optional.empty();
    }
  }

  override def set(key: String): Unit = {

  }

  override def setAttribute(key: String, value: Any): Unit = {
    log.debug("Set SessionAttribute: key: [" + key + "],value: [" + value + "]")
    if(attributes.isDefinedAt(key)) {
      attributes = attributes.updated(key, value)
    }
    else{
      attributes += (key -> value)
    }
  }

  override def getAttribute(key: String): Any = {
    val x = attributes.get(key)
    if(x.isDefined){
      x.get
    }
    else {
      throw new Exception("Attribute: [" + key + "] not found!")
    }
  }

  override def serializeSession(httpSession: HttpSession): String = {
    var map = Map[String,Any]()
    for(attrName <- httpSession.getAttributeNames()){
      val value = httpSession.getAttribute(attrName)
      value match {
        case Nil => map = map + (attrName -> null)
        case _ => map = map + (attrName -> value)
      }
    }
    write(map)
  }

  override def getCrmUserId(): Int = {
    try {
      getAttribute("crmID").toString().toInt
    }
    catch {
      case e:Exception => -1
    }
  }

  override def getCrmSessionToken():String = {
    getAttribute("rpc_session_token").toString
  }

  override def checkParallelLog(key: Optional[String]): Unit = {
    val parallelLog = cachedSession.get("parallelLogMark_" + key.get())
    if (parallelLog != null && !parallelLog.isEmpty) {
      throw new ParallelLoginException(parallelLog)
    }
  }
}
