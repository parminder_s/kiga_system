package com.kiga.shop.document.model

/**
  * Created by faxxe on 2/23/16.
  */
class DhlCsvData(
                  var Sendungsreferenz: String = "",
                  var Empfaenger_Name_1: String,
                  var Empfaenger_Name_2_Postnummer: String,
                  var Empfaenger_Straße: String,
                  var Empfaenger_Hausnummer: String,
                  var Empfaenger_PLZ: String,
                  var Empfaenger_Ort: String,
                  var Empfaenger_Provinz: String,
                  var Empfaenger_Land: String,
                  var Empfaengerreferenz: String,
                  var Empfaenger_E_Mail_Adresse: String,
                  var Empfaenger_Telefonnummer: String,
                  var Gewicht: String,
                  var Laenge: String,
                  var Breite: String,
                  var Hoehe: String,
                  var Produkt__und_Servicedetails: String,
                  var Abrechnungsnummer: String,
                  var Sendungsdokumente___Beschreibung: String = "",
                  var Sendungsdokumente___Sendungsart: String = "",
                  var international0: String = "",
                  var international1: String = "",
                  var international2: String = "",
                  var international3: String = "",
                  var international4: String = "",
                  var international5: String = ""
                ) {




  val delimiter = ";"

  var Sendungsdatum: String = ""
  var Absender_Name_1: String = "Hohe Medien"
  var Absender_Name_2: String = ""
  var Absender_Name_3: String = ""
  var Absender_Straße: String = "Klosterwiesgasse"
  var Absender_Hausnummer: String = "101b"
  var Absender_PLZ: String = "8010"
  var Absender_Ort: String = "Graz"
  var Absender_Provinz: String = ""
  var Absender_Land: String = "AUT"
  var Absenderreferenz: String = ""

  var Absender_E_Mail_Adresse: String = "kundenservice@kigaportal.com"
  var Absender_Telefonnummer: String = ""
  var Empfaenger_Name_3: String = ""

  var Retourenempfaenger_Name_1: String = ""
  var Retourenempfaenger_Name_2: String = ""
  var Retourenempfaenger_Name_3: String = ""
  var Retourenempfaenger_Straße: String = ""
  var Retourenempfaenger_Hausnummer: String = ""
  var Retourenempfaenger_PLZ: String = ""
  var Retourenempfaenger_Ort: String = ""
  var Retourenempfaenger_Provinz: String = ""
  var Retourenempfaenger_Land: String = ""
  var Retourenrempfaenger_E_Mail_Adresse: String = ""
  var Retourenempfaenger_Telefonnummer: String = ""
  var Retouren_Abrechnungsnummer: String = ""
  var Service___Versandbestaetigung___E_Mail_Text_Vorlage: String = ""
  var Service___Versandbestaetigung___E_Mail_Adresse: String = ""
  var Service___Nachnahme___Kontoreferenz: String = ""
  var Service___Nachnahme___Betrag: String = ""
  var Service___Nachnahme___IBAN: String = ""
  var Service___Nachnahme___BIC: String = ""
  var Service___Nachnahme___Zahlungsempfaenger: String = ""
  var Service___Nachnahme___Bankname: String = ""
  var Service___Nachnahme___Verwendungszweck_1: String = ""
  var Service___Nachnahme___Verwendungszweck_2: String = ""
  var Service___Transportversicherung___Betrag: String = ""
  var Service___Weltpaket___Vorausverfügungstyp: String = ""
  var Service___DHL_Europaket___Frankaturtyp: String = ""
  var Sendungsdokumente___Ausfuhranmeldung: String = ""
  var Sendungsdokumente___Rechnungsnummer: String = ""
  var Sendungsdokumente___Genehmigungsnummer: String = ""
  var Sendungsdokumente___Bescheinigungsnummer: String = ""
  var Sendungsdokumente___Entgelte: String = ""
  var Sendungsdokumente___Gesamtnettogewicht: String = ""

  def getRow(): String = s"$Sendungsreferenz$delimiter$Sendungsdatum$delimiter$Absender_Name_1$delimiter" +
    s"$Absender_Name_2$delimiter$Absender_Name_3$delimiter$Absender_Straße$delimiter$Absender_Hausnummer" +
    s"$delimiter$Absender_PLZ$delimiter$Absender_Ort$delimiter$Absender_Provinz$delimiter$Absender_Land" +
    s"$delimiter$Absenderreferenz$delimiter$Absender_E_Mail_Adresse$delimiter$Absender_Telefonnummer$delimiter" +
    s"$Empfaenger_Name_1$delimiter$Empfaenger_Name_2_Postnummer$delimiter$Empfaenger_Name_3$delimiter$Empfaenger_Straße" +
    s"$delimiter$Empfaenger_Hausnummer$delimiter$Empfaenger_PLZ$delimiter$Empfaenger_Ort$delimiter$Empfaenger_Provinz" +
    s"$delimiter$Empfaenger_Land$delimiter$Empfaengerreferenz$delimiter$Empfaenger_E_Mail_Adresse$delimiter" +
    s"$Empfaenger_Telefonnummer$delimiter$Gewicht$delimiter$Laenge$delimiter$Breite$delimiter$Hoehe$delimiter" +
    s"$Produkt__und_Servicedetails$delimiter$Retourenempfaenger_Name_1$delimiter$Retourenempfaenger_Name_2" +
    s"$delimiter$Retourenempfaenger_Name_3$delimiter$Retourenempfaenger_Straße$delimiter$Retourenempfaenger_Hausnummer" +
    s"$delimiter$Retourenempfaenger_PLZ$delimiter$Retourenempfaenger_Ort$delimiter$Retourenempfaenger_Provinz$delimiter" +
    s"$Retourenempfaenger_Land$delimiter$Retourenrempfaenger_E_Mail_Adresse$delimiter$Retourenempfaenger_Telefonnummer" +
    s"$delimiter$Retouren_Abrechnungsnummer$delimiter$Abrechnungsnummer$delimiter" +
    s"$Service___Versandbestaetigung___E_Mail_Text_Vorlage$delimiter$Service___Versandbestaetigung___E_Mail_Adresse" +
    s"$delimiter$Service___Nachnahme___Kontoreferenz$delimiter$Service___Nachnahme___Betrag" +
    s"$delimiter$Service___Nachnahme___IBAN$delimiter$Service___Nachnahme___BIC$delimiter" +
    s"$Service___Nachnahme___Zahlungsempfaenger$delimiter$Service___Nachnahme___Bankname$delimiter" +
    s"$Service___Nachnahme___Verwendungszweck_1$delimiter$Service___Nachnahme___Verwendungszweck_2" +
    s"$delimiter$Service___Transportversicherung___Betrag$delimiter$Service___Weltpaket___Vorausverfügungstyp" +
    s"$delimiter$Service___DHL_Europaket___Frankaturtyp$delimiter$Sendungsdokumente___Ausfuhranmeldung$delimiter" +
    s"$Sendungsdokumente___Rechnungsnummer$delimiter$Sendungsdokumente___Genehmigungsnummer$delimiter" +
    s"$Sendungsdokumente___Bescheinigungsnummer$delimiter$Sendungsdokumente___Sendungsart$delimiter" +
    s"$Sendungsdokumente___Beschreibung$delimiter$Sendungsdokumente___Entgelte$delimiter" +
    s"$Sendungsdokumente___Gesamtnettogewicht$delimiter$international0$delimiter" +
    s"$international1$delimiter$international2$delimiter$international3$delimiter$international4$delimiter" +
    s"$international5" + "\r\n"

}
