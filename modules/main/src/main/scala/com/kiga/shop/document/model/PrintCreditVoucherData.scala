package com.kiga.shop.document.model

import com.kiga.shop.document.model.sub.InvoiceVatRow
import com.kiga.shop.document.service.InvoiceVatTableCalcer
import com.kiga.shop.domain.{CreditVoucherItem, Purchase}
import com.kiga.shop.service.AddressFormatter
import com.kiga.types._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.collection.JavaConversions._

/**
  * Created by peter on 15.03.16.
  */
class PrintCreditVoucherData(val purchase: Purchase, val _productDetails: JList[CreditVoucherItem],
                             val creditVoucherNr: String) {
  val dateTimeFormat = DateTimeFormat.forPattern("dd.MM.yyyy")
  val date = dateTimeFormat.print(new DateTime(purchase.getCreditVoucherDate.toString))
  val shippingAddress = AddressFormatter.getAddress(purchase)
  val billAddress = AddressFormatter.getBillAddress(purchase)
  var invoiceNr = purchase.getInvoiceNr
  var customerId = purchase.getCustomer.getCustomerId
  var purchaseNr = purchase.getId
  var priceNetTotal = _productDetails.toList.map(_.getPriceNetTotal).foldLeft(java.math.BigDecimal.ZERO)((a, b) => a.add(b).setScale(2, java.math.RoundingMode.HALF_UP))
  var priceGrossTotal = _productDetails.toList.map(_.getPriceGrossTotal).foldLeft(java.math.BigDecimal.ZERO)((a, b) => a.add(b).setScale(2, java.math.RoundingMode.HALF_UP))
  var transferalSum = priceGrossTotal
  var paymentMethod = purchase.getPaymentMethod
  var currency = purchase.getCurrency
  var products = _productDetails
  var vatSumPerRate: List[InvoiceVatRow] = new InvoiceVatTableCalcer().calcCreditVoucher(_productDetails.toList)
  var orderNumber: JLong = purchase.getOrderNr
}
