package com.kiga.shop.service

import java.io.{File, FileInputStream}
import java.time.Instant

import com.kiga.mail.Mailer
import com.kiga.print.article.FopXml
import com.kiga.print.invoice.InvoiceToPdf
import com.kiga.security.services.SecurityService
import com.kiga.shop.document.model.{PrintCreditVoucherData, PrintInvoiceData, PrintShippingNoteData}
import com.kiga.shop.document.{PdfCreditVoucher, PdfInvoice}
import com.kiga.shop.domain.{InvoiceVersion, Purchase}
import com.kiga.shop.numbers.NumberGenerator
import com.kiga.shop.repository._
import com.kiga.shop.web.response.PurchaseDetailResponse
import com.kiga.types.JLong
import com.kiga.util.TempFileCreator
import freemarker.template.Configuration
import javax.inject.{Inject, Named}
import javax.servlet.http.HttpServletRequest
import org.apache.commons.codec.digest.DigestUtils
import org.apache.pdfbox.util.PDFMergerUtility
import org.joda.time.DateTime
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils

import scala.beans.BeanProperty
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

/**
  * Created by faxxe on 3/1/16.
  */

@ConfigurationProperties("shop")
@Named
class DocumentService {

  val GrazLiteral = "GRAZ"
  val pdfExtension = "pdf"
  @BeanProperty var bucketName: String = _
  @Inject var securityService: SecurityService = _

  @Inject var mailer: Mailer = _
  @Inject var invoiceToPdf: InvoiceToPdf = _
  @Inject var purchaseRepository: PurchaseRepository = _
  @Inject var productHelper: ProductHelper = _

  @Inject var generateShopNumbers: NumberGenerator = _
  @Inject var shopS3Uploader: ShopS3Uploader = _
  @Inject var invoiceVersionRepository: InvoiceVersionRepository = _
  @Inject var creditVoucherItemRepository: CreditVoucherItemRepository = _
  @Inject var freeMarkerConfig: Configuration = _
  @Inject var tempFileCreator: TempFileCreator = _

  var pdfMergeUtility = new PDFMergerUtility()

  def concatFiles(files: List[File]): File = {
    val pdfMergeUtility = new PDFMergerUtility()
    val printFileName = s"/tmp/${System.currentTimeMillis()}.$pdfExtension"

    files.foreach(x => pdfMergeUtility.addSource(x))

    pdfMergeUtility.setDestinationFileName(printFileName)

    pdfMergeUtility.mergeDocuments()
    new File(printFileName)
  }

  def download(purchase: Purchase, request: HttpServletRequest): File = {
    new File(shopS3Uploader.downloadShippingNote(purchase))
  }

  def createPostLabel(purchase: Purchase): File = {
    var fileName = "label" + purchase.getOrderNr + "." + pdfExtension
    var model = Map("purchase" -> purchase).asJava
    new File(FopXml.print(FreeMarkerTemplateUtils.processTemplateIntoString(
      freeMarkerConfig.getTemplate("shop-shippinglabel.xml"), model), fileName))
  }

  def createShippingNote(purchase: Purchase): File = {

    val year: Integer = new DateTime(purchase.getCreated).getYear
    val shippingPoint = GrazLiteral // storage (origin) of shipping
    var shippingNoteNumber = getNewShippingNoteNumber()
    purchase.setShippingNoteNr(shippingNoteNumber)

    val productDetailsFromPurchase = productHelper.getProductDetailsFromPurchase(purchase)
    val printShippingNoteData = new PrintShippingNoteData(purchase, productDetailsFromPurchase)
    invoiceToPdf.printShippingNote(printShippingNoteData)

    val fileShippingNote = tempFileCreator.createFile(s"${shippingNoteNumber}.$pdfExtension")

    val shippingNoteUrl = s"shippingNote/$year/$shippingPoint/${fileShippingNote.getName}"

    purchase.setShippingNoteS3Url(shippingNoteUrl)

    shopS3Uploader.uploadShippingNote(fileShippingNote, shippingNoteUrl)

    purchaseRepository.save(purchase)

    fileShippingNote
  }

  def getNewShippingNoteNumber(): String = {
    val shippingPoint = ""
    val lastShipping = purchaseRepository.findTopByShippingNoteNrStartingWithOrderByShippingNoteNrDesc(shippingPoint)
    if (lastShipping != null) {
      generateShopNumbers.generateShippingNr(Option(lastShipping.getShippingNoteNr))
    } else {
      generateShopNumbers.generateShippingNr(None)
    }
  }

  def createOrDownloadInvoice(purchase: Purchase, request: HttpServletRequest): File = {
    new File(shopS3Uploader.downloadInvoice(purchase))
  }

  def downloadCreditVoucher(purchase: Purchase): String = {
    shopS3Uploader.downloadCreditVoucher(purchase)
  }

  def createCreditVoucher(purchaseDto: PurchaseDetailResponse): File = {
    val purchase = purchaseRepository.findOne(purchaseDto.getId)
    val country = purchase.getCountry.getCode.toUpperCase
    val year: Integer = new DateTime(purchase.getCreated).getYear

    val creditVoucherNumber = getNewCreditVoucherNumber()
    val creditVoucherDate = Instant.now()
    purchase.setCreditVoucherNr(creditVoucherNumber)
    purchase.setCreditVoucherDate(creditVoucherDate)

    purchaseDto.getPurchaseItems.map(_.creditVoucherTotal.setScale(2, java.math.RoundingMode.HALF_UP))

    val productDetails = productHelper.getProductDetailsCreditVoucher(purchaseDto.getPurchaseItems, purchase)
    productDetails.map(creditVoucherItemRepository.save(_))
    PdfCreditVoucher.CreditVoucher(new PrintCreditVoucherData(purchase, productDetails, creditVoucherNumber))

    val fileCreditVoucher = new File(s"/tmp/${creditVoucherNumber}.$pdfExtension")

    val creditVoucherUrl = s"voucher/$year/$country/${fileCreditVoucher.getName}"

    purchase.setCreditVoucherS3Url(creditVoucherUrl)
    purchase.setCreditVoucherS3Bucket(bucketName)
    purchaseRepository.save(purchase)

    shopS3Uploader.uploadToAmazon(fileCreditVoucher, creditVoucherUrl)

    fileCreditVoucher
  }

  def getNewCreditVoucherNumber(): String = {
    val purchase = purchaseRepository.findTopByOrderByCreditVoucherNrDesc()
    generateShopNumbers.generateCreditVoucherNr(if (purchase.getCreditVoucherNr == null) "" else purchase.getCreditVoucherNr)
  }

  def addInvoiceToPurchase(purchase: Purchase, url: String): Unit = {
    val productDetailsFromPurchase = productHelper.getProductDetailsFromPurchase(purchase)
    PdfInvoice.Invoice(new PrintInvoiceData(purchase, productDetailsFromPurchase))
    val fileInvoice = tempFileCreator.createFile(s"${purchase.getInvoiceNr}.$pdfExtension")
    val invoiceNoteUrl = url + fileInvoice.getName

    val fis = new FileInputStream(fileInvoice)
    val invoiceHash = DigestUtils.sha256Hex(fis)
    fis.close()

    purchase.setInvoiceHash(invoiceHash)
    purchase.setInvoiceS3Url(invoiceNoteUrl)
    shopS3Uploader.uploadToAmazon(fileInvoice, invoiceNoteUrl)
  }

  def getNewInvoiceNumber(country: String, year: Integer): String = {
    val lastOrder = purchaseRepository.findTopByInvoiceNrStartingWithOrderByInvoiceNrDesc(
      country.toUpperCase)
    if (lastOrder != null) {
      generateShopNumbers.generateInvoiceNr(lastOrder.getInvoiceNr, country, year)
    } else {
      generateShopNumbers.generateInvoiceNr("", country, year)
    }
  }

  def downloadShippingNote(purchase: Purchase): String = {
    shopS3Uploader.downloadShippingNote(purchase)
  }

  def createNewVersionedInvoice(purchase: Purchase): File = {

    val version = getNewVersionNumberForInvoice(purchase)
    val file = new File(downloadInvoice(purchase))
    val srcKey = purchase.getInvoiceS3Url
    val destKey = s"versions/${purchase.getInvoiceS3Url}".replace(".pdf", s"_$version.pdf")
    shopS3Uploader.copyFileOnS3(srcKey, destKey)

    val invoiceVersion = new InvoiceVersion;
    invoiceVersion.setInvoiceS3Bucket(bucketName)
    invoiceVersion.setInvoiceS3Url(destKey)
    invoiceVersion.setPurchase(purchase)
    invoiceVersion.setInvoiceVersion(version)

    invoiceVersionRepository.save(invoiceVersion)

    overrideInvoice(purchase)

    file
  }

  def downloadInvoice(purchase: Purchase): String = {
    shopS3Uploader.downloadInvoice(purchase)
  }

  def getNewVersionNumberForInvoice(purchase: Purchase): JLong = {
    val invoiceVersion = invoiceVersionRepository.findTopByPurchaseOrderByInvoiceVersionDesc(purchase)
    if (invoiceVersion == null) {
      0L
    } else {
      val version = invoiceVersion.getInvoiceVersion
      if (version == null) {
        return 0L;
      } else {
        version + 1L;
      }
    }
  }

  def overrideInvoice(purchase: Purchase): File = {
    val country = purchase.getCountry.getCode.toUpperCase
    val year: Integer = new DateTime().getYear

    val invoiceNumber = purchase.getInvoiceNr
    val invoiceDate = purchase.getInvoiceDate

    val invoiceNoteUrl = "invoice/" + year + "/" + country + "/"

    val productDetailsFromPurchase = productHelper.getProductDetailsFromPurchase(purchase)

    PdfInvoice.Invoice(new PrintInvoiceData(purchase, productDetailsFromPurchase))
    val invoiceFileName = s"${invoiceNumber}.$pdfExtension"
    val fileInvoice = tempFileCreator.createFile(invoiceFileName)

    val fis = new FileInputStream(fileInvoice)

    val invoiceUrl = invoiceNoteUrl + invoiceFileName
    purchase.setInvoiceS3Url(invoiceUrl)

    shopS3Uploader.uploadToAmazon(fileInvoice, invoiceUrl)

    //    workflowService.setNewStatus(purchase, PurchaseStatusEnum.InvoiceCreated, securityService.getSessionMember().get)

    fileInvoice
  }

  def getInvoiceVersion(id: JLong): String = {
    val iv = invoiceVersionRepository.findById(id)
    shopS3Uploader.downloadFile(iv.getInvoiceS3Url, iv.getInvoiceS3Bucket)
  }

  def overrideShippingNote(purchase: Purchase): File = {
    val year: Integer = new DateTime(purchase.getCreated).getYear
    val shippingPoint = GrazLiteral // storage (origin) of shipping
    var shippingNoteNumber = purchase.getShippingNoteNr

    val productDetailsFromPurchase = productHelper.getProductDetailsFromPurchase(purchase)
    invoiceToPdf.printShippingNote(new PrintShippingNoteData(purchase, productDetailsFromPurchase))

    val fileShippingNote = new File(s"/tmp/${shippingNoteNumber}.$pdfExtension")

    val s3url = purchase.getShippingNoteS3Url;

    shopS3Uploader.uploadShippingNote(fileShippingNote, s3url)

    fileShippingNote
  }

}
