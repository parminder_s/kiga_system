package com.kiga.shop.document

import com.kiga.main.paymentmethod.PaymentMethod
import com.kiga.print.article.FopXml
import com.kiga.print.model._
import com.kiga.shop.document.model.PrintCreditVoucherData
import com.kiga.shop.domain.CreditVoucherItem
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._

/**
  * Created by peter on 15.03.16.
  */
object PdfCreditVoucher {

  val log = LoggerFactory.getLogger(getClass)

  def CreditVoucher(printCreditVoucherData: PrintCreditVoucherData) = {
    var printObj = new PrintObject(
      InvoiceHeader(),
      InvoiceBody(printCreditVoucherData),
      InvoiceFooter(),
      true
    )
    FopXml.print(printObj, printCreditVoucherData.creditVoucherNr + ".pdf")

  }

  //Invoice methods

  def InvoiceHeader(): List[Printable] = {

    var logo = new Image(s"url('data:image/jped;base64,${Base64Images.kigaLogo()}')")
    logo.setMarginMM(0)
    logo.setContentWidthPx(108)
    var logoContainer = new Block()
    logoContainer.setMarginLeftMM(140)
    logoContainer.setChildren(List(logo))


    var emailSupport = new Block("kundenservice@kigaportal.com")
    emailSupport.setFontSize(8)
    emailSupport.setTextAlign("left")
    emailSupport.setMarginTopMM(-14)

    var addressLine1 = new Block("HOHE Medien OG")
    addressLine1.setFontSize(7)
    addressLine1.setTextAlign("left")
    addressLine1.setMarginTopMM(13)

    var addressLine2 = new Block("A-8010 Graz Klosterwiesgasse 101b")
    addressLine2.setTextAlign("left")
    addressLine1.setFontSize(7)

    var table = new Table()
    var row = new TableRow()
    var tableCell = new TableCellElement("", false)
    tableCell.setChildren(List(addressLine1, addressLine2))
    row.setChildren(List(tableCell))
    table.setChildren(List(row))

    var leader = new Leader()
    leader.setLeaderPattern("rule")
    leader.setLeaderPatternWidthPT(5)
    leader.setLeaderLenghtMM(60)
    var leaderContainer = new Block()
    leaderContainer.setChildren(List(leader))

    List(logoContainer, emailSupport, table, leaderContainer)
  }

  def InvoiceBody(printCreditVoucherData: PrintCreditVoucherData): List[Printable] = {
    //start of address
    var paymentText = printCreditVoucherData.paymentMethod match {
      case PaymentMethod.CREDITCARD => "Kreditkarte"
      case PaymentMethod.INVOICE => "Rechnung"
      case PaymentMethod.PAYPAL => "PayPal"
      case PaymentMethod.SOFORT => "Sofort Überweisung"
      case _ => printCreditVoucherData.paymentMethod.toString
    }

    var address = PdfHelper.Address(printCreditVoucherData.shippingAddress, printCreditVoucherData.billAddress)
    var rightData = PdfHelper.RightData(List(
      Tuple2("Gutschriftsdatum:", printCreditVoucherData.date),
      Tuple2("Bestellnummer:", printCreditVoucherData.orderNumber.toString),
      Tuple2("Kundennummer:", printCreditVoucherData.customerId),
      Tuple2("Rechnungsnummer:", printCreditVoucherData.invoiceNr)
    ))
    var billNr = new Block("Gutschrift: " + printCreditVoucherData.creditVoucherNr)
    billNr.setTextAlign("left")
    billNr.setFontSize(12)
    billNr.setFontWeight("bold")

    var productsTable = InvoiceProductsTable(
      List(
        Tuple2("Stk.", 10),
        Tuple2("Beschreibung.", -1),
        Tuple2("Artikelnr.", 42),
        Tuple2("Brutto Gutschrift /Stk", 20),
        Tuple2("MWSt (%)", 17),
        Tuple2("Summe Brutto", 15)),
      printCreditVoucherData.products.toList)

    var sumTable = InvoiceSumTable(
      List(
        Tuple2("Nettobetrag", printCreditVoucherData.priceNetTotal.toString)) ++
        printCreditVoucherData.vatSumPerRate.map(x => {
          Tuple2("MwSt " + x.prettyRate + "%", x.total.toString)
        }) ++
        List(
          Tuple2("Bruttobetrag", printCreditVoucherData.priceGrossTotal.toString),
          Tuple2("Überweisungsbetrag", s"${printCreditVoucherData.currency} ${printCreditVoucherData.transferalSum.toString}"))
    )

    var endContainer = new Block()
    endContainer.setMarginTopMM(-15)
    var thankYou = new Block("Vielen dank,")
    var team = new Block("Ihr KiGaPortal-Team")
    var payment = new Block(s"Ihnen werden demnächst ${printCreditVoucherData.transferalSum.toString}${printCreditVoucherData.currency} überwiesen!")
    payment.setFontSize(9)
    payment.setMarginTopMM(20)
    payment.setBorder("dotted 0.3mm black")
    payment.setPaddingMM(2)

    endContainer.setChildren(List(thankYou, team))


    List(address, rightData, billNr, productsTable, sumTable, endContainer, payment)
  }

  //heading Tuple: _1 = Content, _2 = WidthMM
  //width in parameter, because it could change in other language
  //produict Tuple: _1 ProductItemRow, _2 = textAlign
  def InvoiceProductsTable(heading: List[Tuple2[String, Int]], product: List[CreditVoucherItem]) = {
    var table = new Table()
    table.setColumWidthsMM(heading.map(_._2))
    var header = new TableRow()
    header.setChildren(heading.map(x => {
      var tableCell = new TableCellElement(x._1, true, false, "center")
      tableCell
    }))

    table.setChildren(List(header) ++ product.map(x => {
      var row = new TableRow()
      var cellAmount = new TableCellElement(x.getAmount.toString, true, false)
      cellAmount.textAlign = "left"
      var cellTitle = new TableCellElement(x.getTitle, true, false)
      cellTitle.textAlign = "left"
      var cellCode = new TableCellElement(x.getCode.toString, true, false)
      cellCode.textAlign = "left"
      var cellPriceGrossSingle = new TableCellElement(x.getPriceGrossTotal.toString, true, false)
      cellPriceGrossSingle.textAlign = "right"
      var cellVatRateString = new TableCellElement(x.getVatRate.toString, true, false)
      cellVatRateString.textAlign = "right"
      var cellPriceGrossTotal = new TableCellElement(x.getPriceGrossTotal.toString, true, false)
      cellPriceGrossTotal.textAlign = "right"

      row.setChildren(List(
        cellAmount, cellTitle, cellCode, cellPriceGrossSingle,
        cellVatRateString, cellPriceGrossTotal))
      row
    }))

    table
  }


  //Invoice helper

  //Tuple: _1 = Label, _2 = Content
  def InvoiceSumTable(elements: List[Tuple2[String, String]]) = {
    var table = new Table()
    table.setColumWidthsMM(List(100, -1, -1))
    table.setChildren(elements.map(x => {
      var row = new TableRow()
      row.setChildren(List(new TableCellElement("", false),
        new TableCellElement(x._1, true, false, "left"),
        new TableCellElement(x._2, true, false)))
      row
    }))
    var container = new Block()
    container.setKeepTogetherWithinPage("always")
    container.setMarginTopMM(5)
    container.setChildren(List(table))
    container
  }

  def InvoiceFooter(): List[Printable] = {

    var container = new Block()
    container.setFontSize(9)
    container.setMarginTopMM(5)
    container.setMarginRightMM(0)

    var leader = new Leader()
    leader.setLeaderPattern("rule")
    leader.setLeaderPatternWidthPT(5)
    leader.setLeaderLenghtPercent(100)

    container.setChildren(List(
      leader,
      new Block("HOHE Medien OG KiGaPortal Klosterwiesgasse 101b, A-8010 Graz"),
      new Block("E-Mail: kundenservice@kigaportal.com"),
      new Block("DVR:0051853 / UID:ATU64282626 / FN307106b / Handelsgericht Graz"),
      leader
    ))

    List(container)
  }
}
