package com.kiga.shop.service

import javax.inject.{Inject, Named}

import com.kiga.shop.domain.Purchase
import com.kiga.web.service.UrlGenerator

/**
  * Created by faxxe on 2/9/16.
  */
class ShopUrlGenerator(urlGenerator: UrlGenerator) {

  def generateInvoiceUrl(purchase: Purchase): String ={
    urlGenerator.getBaseUrl(s"shop/getInvoice/${purchase.getId}/${purchase.getInvoiceHash}")
  }

  def generateCreditVoucherUrl(purchase: Purchase): String = {
    urlGenerator.getBaseUrl(s"shop/getCreditVoucher/${purchase.getId}/${purchase.getInvoiceHash}")
  }

}
