package com.kiga.shop.web.requests

/**
  * Created by rainerh on 27.02.16.
  */

case class ProductInOrderConfirmation
(
  name: String,
  amount: Integer,
  maxDeliveryDays: Integer,
  minDeliveryDays: Integer
)
