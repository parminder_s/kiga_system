package com.kiga.shop.document.service

import java.io.{BufferedWriter, File, FileWriter}

import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import com.kiga.shop.document.model.DhlCsvData
import com.kiga.shop.domain.{Purchase, PurchaseItem}
import com.kiga.types.JList


/**
  * Created by faxxe on 2/23/16.
  */
class DhlCsvGenerator() {

  val dhlPaketAustria = "62901436698601"
  val dhlPaketConnect = "62901436698701"
  val dhlPaketInternational = "62901436698201"

  val abrechnungsMap = Map(
    "at" -> dhlPaketAustria,
    "be" -> dhlPaketConnect,
    "de" -> dhlPaketConnect,
    "lu" -> dhlPaketConnect,
    "nl" -> dhlPaketConnect,
    "pl" -> dhlPaketConnect,
    "sk" -> dhlPaketConnect,
    "cz" -> dhlPaketConnect
  )

  val serviceMap = Map(
    dhlPaketAustria -> "V86PARCEL",
    dhlPaketConnect -> "V87PARCEL",
    dhlPaketInternational -> "V82PARCEL"
  )

  val empty = ""

  val header =
    """Sendungsreferenz;Sendungsdatum;Absender Name 1;Absender Name 2;Absender Name 3;Absender Straße;Absender Hausnummer;
      |Absender PLZ;Absender Ort;Absender Provinz;Absender Land;Absenderreferenz;Absender E-Mail-Adresse;
      |Absender Telefonnummer;Empfänger Name 1;Empfänger Name 2 / Postnummer;Empfänger Name 3;Empfänger Straße;
      |Empfänger Hausnummer;Empfänger PLZ;Empfänger Ort;Empfänger Provinz;Empfänger Land;Empfängerreferenz;
      |Empfänger E-Mail-Adresse;Empfänger Telefonnummer;Gewicht;Länge;Breite;Höhe;Produkt- und Servicedetails;
      |Retourenempfänger Name 1;Retourenempfänger Name 2;Retourenempfänger Name 3;Retourenempfänger Straße;
      |Retourenempfänger Hausnummer;Retourenempfänger PLZ;Retourenempfänger Ort;Retourenempfänger Provinz;
      |Retourenempfänger Land;Retourenrempfänger E-Mail-Adresse;Retourenempfänger Telefonnummer;
      |Retouren-Abrechnungsnummer;Abrechnungsnummer;Service - Versandbestätigung - E-Mail Text-Vorlage;
      |Service - Versandbestätigung - E-Mail-Adresse;Service - Nachnahme - Kontoreferenz;Service - Nachnahme - Betrag;
      |Service - Nachnahme - IBAN;Service - Nachnahme - BIC;Service - Nachnahme - Zahlungsempfänger;
      |Service - Nachnahme - Bankname;Service - Nachnahme - Verwendungszweck 1;Service - Nachnahme - Verwendungszweck 2;
      |Service - Transportversicherung - Betrag;Service - Weltpaket - Vorausverfügungstyp;
      |Service - DHL Europaket - Frankaturtyp;Sendungsdokumente - Ausfuhranmeldung;
      |Sendungsdokumente - Rechnungsnummer;Sendungsdokumente - Genehmigungsnummer;
      |Sendungsdokumente - Bescheinigungsnummer;Sendungsdokumente - Sendungsart;Sendungsdokumente - Beschreibung;
      |Sendungsdokumente - Entgelte;Sendungsdokumente - Gesamtnettogewicht;Sendungsdokumente - Beschreibung (WP1);
      |Sendungsdokumente - Menge (WP1);Sendungsdokumente - Zollwert (WP1);Sendungsdokumente - Ursprungsland (WP1);
      |Sendungsdokumente - Zolltarifnummer (WP1);Sendungsdokumente - Gewicht (WP1);Sendungsdokumente - Beschreibung (WP2);
      |Sendungsdokumente - Menge (WP2);Sendungsdokumente - Zollwert (WP2);Sendungsdokumente - Ursprungsland (WP2);
      |Sendungsdokumente - Zolltarifnummer (WP2);Sendungsdokumente - Gewicht (WP2);Sendungsdokumente - Beschreibung (WP3);
      |Sendungsdokumente - Menge (WP3);Sendungsdokumente - Zollwert (WP3);Sendungsdokumente - Ursprungsland (WP3);
      |Sendungsdokumente - Zolltarifnummer (WP3);Sendungsdokumente - Gewicht (WP3);Sendungsdokumente - Beschreibung (WP4);
      |Sendungsdokumente - Menge (WP4);Sendungsdokumente - Zollwert (WP4);Sendungsdokumente - Ursprungsland (WP4);
      |Sendungsdokumente - Zolltarifnummer (WP4);Sendungsdokumente - Gewicht (WP4);Sendungsdokumente - Beschreibung (WP5);
      |Sendungsdokumente - Menge (WP5);Sendungsdokumente - Zollwert (WP5);Sendungsdokumente - Ursprungsland (WP5);
      |Sendungsdokumente - Zolltarifnummer (WP5);Sendungsdokumente - Gewicht (WP5);Sendungsdokumente - Beschreibung (WP6);
      |Sendungsdokumente - Menge (WP6);Sendungsdokumente - Zollwert (WP6);Sendungsdokumente - Ursprungsland (WP6);
      |Sendungsdokumente - Zolltarifnummer (WP6);Sendungsdokumente - Gewicht (WP6)""".stripMargin.replace("\n", "") + "\r\n"

  def getCsvData(purchases: JList[Purchase]): JList[String] = {
    val list = List[String](header) ++ purchases.map(x => createRow(x)).filter(x => x != "")

    val file = new File("/tmp/" + System.currentTimeMillis() + ".csv")
    val bw = new BufferedWriter(new FileWriter(file))
    list.foreach(bw.write)
    bw.close()
    list

  }

  def createRow(purchase: Purchase): String = {
    val code = purchase.getCountry.getCode
    new DhlCsvData(
      s"${purchase.getOrderNr}",
      getFirstLastName(purchase),
      getAddressName(purchase),
      Option(purchase.getStreet).getOrElse(empty),
      Option(purchase.getStreetNumber).getOrElse(empty),
      Option(purchase.getZip).getOrElse(empty),
      Option(purchase.getCity).getOrElse(empty),
      "", //Provinz
      Option(purchase.getCountry.getCode3.toUpperCase).getOrElse(empty),
      "", //Empfaengerreferenz
      Option(purchase.getEmail).getOrElse(empty),
      "", //Tel. Nr.
      getWeight(purchase),
      "", //Laenge
      "", //Breite
      "", //Hoehe
      getProductAndServiceDetails(purchase), //Produkt und servicedetails
      getAbrechnungsnummer(purchase),
      "LAPBAG",
      "OTHER",
      new InternationalCol(purchase, 0).getCols(),
      new InternationalCol(purchase, 1).getCols(),
      new InternationalCol(purchase, 2).getCols(),
      new InternationalCol(purchase, 3).getCols(),
      new InternationalCol(purchase, 4).getCols(),
      new InternationalCol(purchase, 5).getCols()
    ).getRow()
  }

  def getAddressName(purchase: Purchase): String = {
    val name1 = Option(purchase.getAddressName1).getOrElse(empty)
    val name2 = Option(purchase.getAddressName2).getOrElse(empty)
    s"$name1 $name2"
  }

  def getFirstLastName(purchase: Purchase): String = {
    val firstName = Option(purchase.getFirstname).getOrElse(empty)
    val lastName = Option(purchase.getLastname).getOrElse(empty)
    s"$firstName $lastName"
  }

  def getWeight(purchase: Purchase): String = {
    s"${purchase.getPurchaseItems.foldLeft(0)((r, c) =>
      (r + c.getWeightGramTotal)) / 1000f}".replace('.', ',') //DHL import not working with "." !!
  }

  def getAbrechnungsnummer(purchase: Purchase): String = {
    val code = purchase.getCountry.getCode
    abrechnungsMap.getOrElse(code, dhlPaketInternational)
  }

  def getProductAndServiceDetails(purchase: Purchase): String = {
    val code = purchase.getCountry.getCode
    val product = abrechnungsMap.getOrElse(code, dhlPaketInternational)
    serviceMap.getOrElse(product, "")
  }


  class InternationalCol(val purchase: Purchase, val index: Int) {

    val delimiter = ";"

    var purchaseItem: PurchaseItem = _
    var Sendungsdokumente___Beschreibung: String = ""
    var Sendungsdokumente___Menge: String = ""
    var Sendungsdokumente___Zollwert: String = ""
    var Sendungsdokumente___Ursprungsland: String = ""
    var Sendungsdokumente___Zolltarifnummer: String = ""
    var Sendungsdokumente___Gewicht: String = ""

    var pi = purchase.getPurchaseItems
    if (pi != null && index < pi.size()) {
      purchaseItem = pi.get(index)
    }

    if (purchaseItem != null) {
      Sendungsdokumente___Beschreibung = purchaseItem.getTitle
      Sendungsdokumente___Menge = s"${purchaseItem.getAmount}"
      Sendungsdokumente___Zollwert = ""
      Sendungsdokumente___Ursprungsland = ""
      Sendungsdokumente___Zolltarifnummer = ""
      Sendungsdokumente___Gewicht = s"${purchaseItem.getWeightGramSingle/1000f}".replace('.', ',')
    }

    def getCols(): String = {
      s"$Sendungsdokumente___Beschreibung$delimiter$Sendungsdokumente___Menge$delimiter$Sendungsdokumente___Zollwert$delimiter" +
        s"$Sendungsdokumente___Ursprungsland$delimiter$Sendungsdokumente___Zolltarifnummer$delimiter$Sendungsdokumente___Gewicht"
    }
  }


}
