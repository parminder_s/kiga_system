package com.kiga.shop.document.model

import com.kiga.shop.document.model.sub.InvoiceVatRow
import com.kiga.shop.document.service.InvoiceVatTableCalcer
import com.kiga.shop.documents.service.JPurchaseItemRowFactory
import com.kiga.shop.domain.{Country, ProductDetail, Purchase}
import com.kiga.shop.service.AddressFormatter
import com.kiga.types._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.collection.JavaConverters._

/**
  * Created by faxxe on 1/26/16.
  */
class PrintInvoiceData(val purchase: Purchase, val productDetails: Map[JLong, ProductDetail]) {
  val dateTimeFormat = DateTimeFormat.forPattern("dd.MM.yyyy")
  val shippingAddress = AddressFormatter.getAddress(purchase)
  val billAddress = AddressFormatter.getBillAddress(purchase)
  var invoiceNr = purchase.getInvoiceNr
  var customerId = purchase.getCustomer.getCustomerId
  var purchaseNr = purchase.getId
  var priceNetTotal = purchase.getPriceNetTotal
  var priceGrossTotal = purchase.getPriceGrossTotal
  var transferalSum = purchase.getPriceGrossTotal
  var paymentMethod = purchase.getPaymentMethod
  var date = dateTimeFormat.print(new DateTime(purchase.getInvoiceDate.toString))
  var currency = purchase.getCurrency
  var products = new JPurchaseItemRowFactory().createMultiple(purchase, productDetails.asJava, true)
  var vatSumPerRate: List[InvoiceVatRow] = new InvoiceVatTableCalcer().calc(purchase)
  var orderNumber: JLong = purchase.getOrderNr
  var bankName: String = purchase.getCountry.getCountryShop.getBankName
  var iban: String = purchase.getCountry.getCountryShop.getIban
  var bic: String = purchase.getCountry.getCountryShop.getBic
  var accountNr: String = purchase.getCountry.getCountryShop.getAccountNr
  var dayToPayInvoice: JInt = purchase.getCountry.getCountryShop.getDaysToPayInvoice
  var country: Country = purchase.getCountry
}
