package com.kiga.shop.document.model.sub

import com.kiga.types.JBigDecimal

/**
  * Created by faxxe on 1/28/16.
  */
case class InvoiceVatRow(prettyRate: String, total: JBigDecimal)
