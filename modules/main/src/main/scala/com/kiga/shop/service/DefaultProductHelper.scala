package com.kiga.shop.service

import java.math.{BigDecimal, MathContext, RoundingMode}

import com.kiga.shop.domain.{PurchaseItem, CreditVoucherItem, ProductDetail, Purchase}
import com.kiga.shop.repository.{ProductRepository, PurchaseItemRepository, ProductDetailRepository}
import com.kiga.shop.web.responses.PurchaseItemResponse
import com.kiga.types._

import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.mutable
import scala.collection.JavaConverters._

/**
  * Created by rainerh on 21.02.16.
  */
class DefaultProductHelper(productDetailRepo: ProductDetailRepository, purchaseItemRepository: PurchaseItemRepository) extends ProductHelper {
  def getProductDetailsFromPurchase(purchase: Purchase): Map[JLong, ProductDetail] = {
    purchase.getPurchaseItems.map(purchaseItem => {(
      purchaseItem.getProduct.getId,
      productDetailRepo.findProductDetailByProductIdAndCountryId
      (
        purchaseItem.getProduct.getId, purchase.getCountry.getId
      ))
    }).toMap
  }

  def getProductDetailsCreditVoucher(items: JList[PurchaseItemResponse], purchase: Purchase): JList[CreditVoucherItem] = {
    items.asScala.map(item => {
      var detail = purchaseItemRepository.findOne(item.id)
      var creditVoucherItem = new CreditVoucherItem
      creditVoucherItem.setAmount(item.amount.toLong)
      creditVoucherItem.setCode(detail.getCode)
      creditVoucherItem.setPriceGrossTotal(item.creditVoucherTotal)
      var priceNet = getNetPrice(item.creditVoucherTotal, purchaseItemRepository.findOne(item.id))
      creditVoucherItem.setPriceNetTotal(priceNet)
      creditVoucherItem.setVatTotal(item.creditVoucherTotal.subtract(priceNet))
      creditVoucherItem.setVatRate(detail.getVatRate)
      creditVoucherItem.setTitle(detail.getTitle)
      creditVoucherItem.setPurchase(purchase)
      creditVoucherItem.setProduct(detail.getProduct)
      creditVoucherItem
    }).asJava
  }

  def getNetPrice(creditVoucher: java.math.BigDecimal, purchaseItem: PurchaseItem): java.math.BigDecimal = {
    if(purchaseItem.getPriceGrossTotal.compareTo(creditVoucher) == 0)
     purchaseItem.getPriceNetSingle.multiply(new JBigDecimal(purchaseItem.getAmount)).setScale(2, RoundingMode.HALF_UP)
    else {
      val context = new MathContext(50, RoundingMode.HALF_UP)
      val divisor = java.math.BigDecimal.ONE.add(purchaseItem.getVatRate).setScale(2, RoundingMode.HALF_UP)
      val returner = creditVoucher.divide(divisor, context).setScale(2, RoundingMode.HALF_UP)
      returner
    }
  }

  def getProductDetailsFromPurchaseAsJava(purchase: Purchase): JMap[JLong, ProductDetail] = {
    getProductDetailsFromPurchase(purchase).asJava
  }
}
