package com.kiga.shop.document.model.sub

import scala.beans.BeanProperty

/**
  * Created by peter on 19.01.16.
  */
case class ArticleInOrder
(
  @BeanProperty var amount: Integer,
  @BeanProperty var description: String,
  @BeanProperty var productId: String,
  @BeanProperty var singlePrice: Double,
  @BeanProperty var taxRate: Double,
  @BeanProperty var totalPrice: Double
)
