package com.kiga.shop.document.model

import com.kiga.shop.documents.model.PurchaseItemRow
import com.kiga.shop.documents.service.JPurchaseItemRowFactory
import com.kiga.shop.domain.{ProductDetail, Purchase}
import com.kiga.shop.service.AddressFormatter
import com.kiga.types.JLong

import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

/**
  * Created by faxxe on 1/26/16.
  */
class PrintShippingNoteData(purchase: Purchase, productDetails: Map[JLong, ProductDetail]) {

  var shippingAddress: List[String] = AddressFormatter.getAddress(purchase)
  var billAddress: List[String] = AddressFormatter.getBillAddress(purchase)
  var orderNr: String = purchase.getOrderNr.toString
  var deliveryNr: String = purchase.getShippingNoteNr
  var deliveryPlace: String = "Graz"
  var customerNr: String = "" + purchase.getCustomer.getCustomerId
  var tableCellItems: List[PurchaseItemRow] = new JPurchaseItemRowFactory().createMultiple(purchase, productDetails.asJava, false).toList
  var totalDelivery: Boolean = true
  var sumWeight: String = s"${purchase.getPurchaseItems.foldLeft(0)((r, c) => (r + c.getWeightGramTotal)) / 1000.0}"

}
