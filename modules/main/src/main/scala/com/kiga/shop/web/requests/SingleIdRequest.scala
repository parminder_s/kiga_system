package com.kiga.shop.web.requests

import com.kiga.shop.document.model.sub.ArticleInOrder
import com.kiga.types.JLong

import scala.beans.BeanProperty

/**
 * Created by peter on 19.01.16.
 */
case class SingleIdRequest
(
  @BeanProperty var id: JLong,
  @BeanProperty var locale: String
)
