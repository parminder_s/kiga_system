package com.kiga.shop.document.service

import com.kiga.shop.document.model.sub.InvoiceVatRow
import com.kiga.shop.documents.service.VatRateFormatter
import com.kiga.shop.domain.{CreditVoucherItem, Purchase, PurchaseItem}
import com.kiga.types.JBigDecimal

import scala.collection.JavaConversions._

/**
  * Created by faxxe on 1/28/16.
  */
class InvoiceVatTableCalcer {
  def calc(purchase: Purchase): List[InvoiceVatRow] = {
    val purchaseItems: List[PurchaseItem] = purchase.getPurchaseItems.toList

    (mapProduct(purchaseItems) ++ mapShipping(purchase))
      .groupBy(_.prettyRate)
      .map({
        case (rate, invoiceVatRows) => {
          (rate, invoiceVatRows.foldLeft(new JBigDecimal(0))((r, c) => c.total.add(r)))
        }
      })
      .map({
        case (rate, sum) => {
          InvoiceVatRow(rate, sum)
        }
      })
      .toList
      .sortBy(_.prettyRate)
  }

  def mapProduct(purchaseItems: List[PurchaseItem]): List[InvoiceVatRow] = {
    purchaseItems.map(pi => new InvoiceVatRow(VatRateFormatter.format(pi.getVatRate), pi.getVatTotal))
  }

  def mapShipping(purchase: Purchase): Option[InvoiceVatRow] = {
    if (purchase.getVatShipping.compareTo(java.math.BigDecimal.ZERO) > 0 ||
      purchase.getVatRateShipping.compareTo(java.math.BigDecimal.ZERO) > 0
    ) {
      Option(new InvoiceVatRow(VatRateFormatter.format(purchase.getVatRateShipping), purchase.getVatShipping))
    } else {
      None
    }

  }

  def calcCreditVoucher(creditVoucherItem: List[CreditVoucherItem]): List[InvoiceVatRow] = {
    (mapProductCreditVoucher(creditVoucherItem))
      .groupBy(_.prettyRate)
      .map({
        case (rate, invoiceVatRows) => {
          (rate, invoiceVatRows.foldLeft(new JBigDecimal(0))((r, c) => c.total.add(r)))
        }
      })
      .map({
        case (rate, sum) => {
          InvoiceVatRow(rate, sum)
        }
      })
      .toList
      .sortBy(_.prettyRate)
  }

  def mapProductCreditVoucher(purchaseItems: List[CreditVoucherItem]): List[InvoiceVatRow] = {
    purchaseItems.map(pi => new InvoiceVatRow(VatRateFormatter.format(pi.getVatRate), pi.getVatTotal))
  }
}
