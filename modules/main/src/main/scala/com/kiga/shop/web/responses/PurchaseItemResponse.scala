package com.kiga.shop.web.responses

import com.kiga.shop.domain.PurchaseItem
import com.kiga.types._

import scala.beans.BeanProperty
/**
  * Created by faxxe on 3/8/16.
  */
class PurchaseItemResponse {

  @BeanProperty var id: JLong = _
  @BeanProperty var amount: Integer = _
  @BeanProperty var title: String = _
  @BeanProperty var priceGrossTotal: JBigDecimal = _
  @BeanProperty var creditVoucherTotal: JBigDecimal = _

}

object PurchaseItemResponse {

  def toPurchaseItemResponse(purchaseItem: PurchaseItem): PurchaseItemResponse = {
    val returner = new PurchaseItemResponse()
    returner.id = purchaseItem.getId
    returner.amount = purchaseItem.getAmount
    returner.title = purchaseItem.getTitle
    returner.priceGrossTotal = purchaseItem.getPriceGrossTotal
    returner.creditVoucherTotal = java.math.BigDecimal.ZERO
    returner
  }

}
