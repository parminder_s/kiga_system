package com.kiga.shop.web.responses

/**
  * Created by peter on 19.01.16.
  */
class InvoiceNumberResponse
(
  var invoiceNumber: String
)
