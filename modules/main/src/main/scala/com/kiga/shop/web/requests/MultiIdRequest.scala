package com.kiga.shop.web.requests

import com.kiga.types.{JList, JLong}

import scala.beans.BeanProperty

/**
  * Created by faxxe on 3/1/16.
  */
class MultiIdRequest {

  @BeanProperty var ids: JList[JLong] = _

}
