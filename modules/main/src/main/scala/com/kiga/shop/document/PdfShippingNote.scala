package com.kiga.shop.document

import java.util.Date

import com.kiga.print.article.FopXml
import com.kiga.print.model._
import com.kiga.shop.document.model.PrintShippingNoteData
import com.kiga.shop.documents.model.PurchaseItemRow

/**
  * Created by peter on 01.03.16.
  */
object PdfShippingNote {

  def ShippingNote(printShippingNoteData: PrintShippingNoteData) = {
    var printObj = new PrintObject(
      ShippingNoteHeader(),
      ShippingNoteBody(printShippingNoteData),
      ShippingNoteFooter(),
      true
    )
    FopXml.print(printObj, printShippingNoteData.deliveryNr + ".pdf")
  }

  def ShippingNoteHeader(): List[Printable] = {
    var logoContainer = new Block()
    logoContainer.setText("KiGaPortal")
    logoContainer.setMarginLeftMM(140)


    var emailSupport = new Block("kundenservice@kigaportal.com")
    emailSupport.setFontSize(8)
    emailSupport.setTextAlign("left")
    emailSupport.setMarginTopMM(-14)

    var addressLine1 = new Block("HOHE Medien OG")
    addressLine1.setFontSize(7)
    addressLine1.setTextAlign("left")
    addressLine1.setMarginTopMM(13)

    var addressLine2 = new Block("A-8010 Graz Klosterwiesgasse 101b")
    addressLine2.setTextAlign("left")
    addressLine1.setFontSize(7)

    var table = new Table()
    var row = new TableRow()
    var tableCell = new TableCellElement("", false)
    tableCell.setChildren(List(addressLine1, addressLine2))
    row.setChildren(List(tableCell))
    table.setChildren(List(row))

    var leader = new Leader()
    leader.setLeaderPattern("rule")
    leader.setLeaderPatternWidthPT(5)
    leader.setLeaderLenghtMM(60)
    var leaderContainer = new Block()
    leaderContainer.setChildren(List(leader))

    List(logoContainer, emailSupport, table, leaderContainer)
  }

  def ShippingNoteBody(printShippingNoteData: PrintShippingNoteData) = {
    var address = PdfHelper.Address(printShippingNoteData.shippingAddress, printShippingNoteData.billAddress)
    var rightData = PdfHelper.RightData(List(
      Tuple2("Lieferscheindatum:", new java.text.SimpleDateFormat("dd.MM.yyyy").format(new Date())),
      Tuple2("Bestellnummer:", printShippingNoteData.orderNr),
      Tuple2("Versandort:", printShippingNoteData.deliveryPlace),
      Tuple2("Kundennummer:", printShippingNoteData.customerNr)
    ))
    var shippingNoteNr = new Block("Lieferschein: " + printShippingNoteData.deliveryNr)
    shippingNoteNr.setTextAlign("left")
    shippingNoteNr.setFontSize(12)
    shippingNoteNr.setFontWeight("bold")

    var productsTable = ShippingNoteProductsTable(
      List(
        Tuple2("Pos.", 10),
        Tuple2("Beschreibung.", -1),
        Tuple2("Artikelnr.", 60),
        Tuple2("Stk.", 14),
        Tuple2("Gew. (Kg)", 17)),
      printShippingNoteData.tableCellItems,
      Tuple2("Summe:", printShippingNoteData.sumWeight)
    )

    var totalDelivery = ShippingNoteTotalDeliver("Gesamtlieferung:", "Teillieferung:",
      printShippingNoteData.totalDelivery)

    List(address, rightData, shippingNoteNr, productsTable, totalDelivery)
  }

  //heading Tuple: _1 = Content, _2 = WidthMM
  //width in parameter, because it could change in other language
  //produict Tuple: _1 ProductItemRow, _2 = textAlign
  //sum : _1 text like "Summe:", _2: actual value in KG
  def ShippingNoteProductsTable(heading: List[Tuple2[String, Int]], product: List[PurchaseItemRow], sum: Tuple2[String, String]) = {
    var table = new Table()
    table.setColumWidthsMM(heading.map(_._2))
    var header = new TableRow()
    header.setChildren(heading.map(x => {
      var tableCell = new TableCellElement(x._1, true, false, "center")
      tableCell
    }))

    table.setChildren(List(header) ++ product.zip(Stream from 1).map(x => {
      var row = new TableRow()
      var cellPos = new TableCellElement(x._2.toString, true, false)
      cellPos.textAlign = "left"
      var cellTitle = new TableCellElement(x._1.getTitle(), true, false)
      cellTitle.textAlign = "left"
      var cellCode = new TableCellElement(x._1.getCode(), true, false)
      cellCode.textAlign = "left"
      var cellAmount = new TableCellElement(x._1.getAmount().toString(), true, false)
      cellAmount.textAlign = "left"
      var cellWeight = new TableCellElement(x._1.getWeightGramTotal(), true, false)
      cellWeight.textAlign = "right"

      row.setChildren(List(
        cellPos, cellTitle, cellCode, cellAmount,
        cellWeight))
      row
    }))

    var sumRow = new TableRow()
    var cellText = new TableCellElement(sum._1, false)
    var cellValue = new TableCellElement(sum._2)
    sumRow.setChildren(List(
      TableCellElement.Invisible(),
      TableCellElement.Invisible(),
      TableCellElement.Invisible(),
      cellText,
      cellValue)
    )
    table.setChildren(table.getChildren ++ List(sumRow))
    table
  }


  //Shipping Note Helper

  def ShippingNoteTotalDeliver(totalDeliveryTxt: String, partDeliveryTxt: String, isTotal: Boolean): Printable = {
    var table = new Table()
    table.setColumWidthsMM(List(30, -1))
    var totalRow = new TableRow()
    var partRow = new TableRow()

    var totalCellTxt = new TableCellElement(totalDeliveryTxt, false)
    var totalImg = if (isTotal)
      new Image(s"url('data:image/jped;base64,${Base64Images.checkBoxChecked()}") else
      new Image(s"url('data:image/jped;base64,${Base64Images.checkBoxUnChecked()}")
    totalImg.setContentWidthPx(9)

    var partCellTxt = new TableCellElement(partDeliveryTxt, false)
    var partImg = if (!isTotal)
      new Image(s"url('data:image/jped;base64,${Base64Images.checkBoxChecked()}") else
      new Image(s"url('data:image/jped;base64,${Base64Images.checkBoxUnChecked()}")
    partImg.setContentWidthPx(9)

    var totalImgContainer = new Block()
    totalImgContainer.setTextAlign("left")
    totalImgContainer.setChildren(List(totalImg))

    var partImgContainer = new Block()
    partImgContainer.setTextAlign("left")
    partImgContainer.setChildren(List(partImg))

    var totalCellImg = TableCellElement.Invisible()
    var partCellImg = TableCellElement.Invisible()

    totalCellImg.setChildren(List(totalImgContainer))
    partCellImg.setChildren(List(partImgContainer))

    totalRow.setChildren(List(totalCellTxt, totalCellImg))
    partRow.setChildren(List(partCellTxt, partCellImg))

    table.setChildren(List(totalRow, partRow))
    table
  }

  def ShippingNoteFooter(): List[Printable] = {

    var container = new Block()
    container.setFontSize(9)
    container.setMarginTopMM(5)
    container.setMarginRightMM(0)

    var leader = new Leader()
    leader.setLeaderPattern("rule")
    leader.setLeaderPatternWidthPT(5)
    leader.setLeaderLenghtPercent(100)

    container.setChildren(List(
      leader,
      new Block("HOHE Medien OG KiGaPortal Klosterwiesgasse 101b, A-8010 Graz"),
      new Block("E-Mail: kundenservice@kigaportal.com"),
      leader
    ))

    List(container)
  }

}
