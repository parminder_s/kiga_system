package com.kiga.shop.web.requests

import com.kiga.shop.domain.ShippingPartner
import com.kiga.shop.web.responses.PurchaseItemResponse
import com.kiga.types.JLong

import scala.beans.BeanProperty

/**
  * Created by faxxe on 3/8/16.
  */
class PurchaseChangeRequest {

  @BeanProperty var id: JLong = _
  @BeanProperty var firstName: String = _
  @BeanProperty var lastName: String = _
  @BeanProperty var addressName1: String = _
  @BeanProperty var addressName2: String = _
  @BeanProperty var street: String = _
  @BeanProperty var streetNumber: String = _
  @BeanProperty var floorNumber: String = _
  @BeanProperty var doorNumber: String = _
  @BeanProperty var zip: String = _
  @BeanProperty var city: String = _
  @BeanProperty var country: String = _
  @BeanProperty var billAddressName1: String = _
  @BeanProperty var billAddressName2: String = _
  @BeanProperty var billStreet: String = _
  @BeanProperty var billStreetnumber: String = _
  @BeanProperty var billFloornumber: String = _
  @BeanProperty var billDoornumber: String = _
  @BeanProperty var billZip: String = _
  @BeanProperty var billCity: String = _
  @BeanProperty var billCountry: String = _
  @BeanProperty var memberEmail: String = _
  @BeanProperty var purchaseItems: List[PurchaseItemResponse] = _
  @BeanProperty var comment: String = _
  @BeanProperty var shippingPartner: ShippingPartner = _

}
