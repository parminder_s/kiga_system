package com.kiga.shop.web.requests

import com.kiga.crm.messages.Address

import scala.beans.BeanProperty

/**
  * Created by peter on 27.01.16.
  */
case class EmailOrderConfirmationRequest
(
  email: String,
  gender: String, //m or f
  orderNr: String,
  date: String,
  name: String,
  address: List[String],
  altAddress: List[String],
  paymentMethod: String,
  products: List[ProductInOrderConfirmation]
)
