package com.kiga.shop.service

import com.kiga.shop.domain.Purchase

/**
  * Created by faxxe on 2/2/16.
  */
object AddressFormatter {
  val empty = ""
  val space = " "

  def getAddress(purchase: Purchase): List[String] = {
    formatAddressHeader(purchase.getAddressName1, purchase.getAddressName2) ++
      formatAddressName(purchase.getFirstname, purchase.getLastname) ++
      formatAddressStreet(
        purchase.getStreet, purchase.getStreetNumber, purchase.getFloorNumber, purchase.getDoorNumber
      ) ++
      formatCity(purchase.getZip, purchase.getCity) ++
      List(Option(purchase.getCountryTitle).getOrElse(empty))
  }

  def formatAddressName(firstname: String, lastname: String): List[String] = {
    List(Option(firstname).getOrElse(empty) + space + Option(lastname).getOrElse(empty).trim)
  }

  def getBillAddress(purchase: Purchase): List[String] = {
    var billAddress = formatAddressHeader(purchase.getBillAddressName1, purchase.getBillAddressName2) ++
      formatAddressStreet(
        purchase.getBillStreet, purchase.getBillStreetNumber,
        purchase.getBillFloorNumber, purchase.getBillDoorNumber
      ) ++
      formatCity(purchase.getBillZip, purchase.getBillCity) ++
      List(Option(purchase.getCountryTitle).getOrElse(empty))
    billAddress = billAddress.filter(_ != "")
    if (billAddress.size < 3){
      List[String]()
    }else {
      billAddress
    }
  }

  def getShippingAddress(purchase: Purchase): List[String] = {
    formatAddressHeader(purchase.getShippingAddressName1, purchase.getShippingAddressName2) ++
      formatAddressStreet(
        purchase.getShippingStreet, purchase.getShippingStreetNumber,
        purchase.getShippingFloorNumber, purchase.getShippingDoorNumber
      ) ++
      formatCity(purchase.getShippingZip, purchase.getShippingCity) ++
      List(Option(purchase.getCountryTitle).getOrElse(empty))
  }

  def formatCity(zip: String, city: String): List[String] = {
    List((Option(zip).getOrElse(empty) + space + Option(city).getOrElse(empty)).trim)
  }

  def formatAddressHeader(addressName1: String, addressName2: String): List[String] = {
    List(addressName1, addressName2).filter(Option(_).isDefined)
  }

  def formatAddressStreet(street: String, streetNumber: String, floorNumber: String, doorNumber: String): List[String] = {
    List(
      Option(street).getOrElse(empty) + space +
      List(streetNumber, floorNumber, doorNumber).filter(Option(_).isDefined).mkString("/")
    )
  }


}
