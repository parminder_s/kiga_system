package com.kiga.shop.web

import javax.inject.Inject
import javax.servlet.http.HttpServletRequest

import com.kiga.payment.repository.PaymentRepository
import com.kiga.security.services.SecurityService
import com.kiga.shop.web.requests.PaymentReportFilterRequest
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.web.bind.annotation._

/**
  * Created by peter on 08.03.16.
  */
@RestController
@ConfigurationProperties("shop")
@RequestMapping(Array("/reports/shop"))
class ReportPaymentController {

  @Inject var securityService: SecurityService = _

  @Inject var paymentRepository: PaymentRepository = _

  @RequestMapping(value = Array("paymentlist/filtered"), method = Array(RequestMethod.POST))
  def paymentListFiltered(@RequestBody filterRequest: PaymentReportFilterRequest) = {
    securityService.requireShopAdmin()
    paymentRepository.findPayedBeetweenStartDateAndEndDateWithAcquirerTransaction(
      filterRequest.fromDate.replace("-","."), filterRequest.toDate.replace("-","."));
  }

  @RequestMapping(value = Array("paymentlist/byOrderNr/{orderNr}"), method = Array(RequestMethod.POST))
  def paymentListFiltered(@PathVariable("orderNr") orderNr: String, request: HttpServletRequest) = {
    securityService.requireShopAdmin()
    paymentRepository.findByOrderNr(orderNr)
  }
}
