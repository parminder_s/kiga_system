package com.kiga.shop

import com.kiga.main.paymentmethod.PaymentMethod
import com.kiga.shop.document.model.ConfirmationEmailData
import com.kiga.shop.documents.model.PurchaseItemRow

import scala.collection.JavaConversions._


/**
  * Created by peter on 27.01.16.
  */
object EmailTemplates {

  def confirmationEmail(salutation: String, data: ConfirmationEmailData, rightOfWithdrawalUrl: String): String = {
    var paymentText = data.paymentMethod match {
      case PaymentMethod.CREDITCARD => "Kreditkarte"
      case PaymentMethod.INVOICE => "Rechnung"
      case PaymentMethod.PAYPAL => "PayPal"
      case PaymentMethod.SOFORT => "Sofort Überweisung"
      case _ => data.paymentMethod.toString
    }
    val contentTable = contentTableTwoAddress(data.address, data.altAddress)
    s"""
            <div style="text-align: left; color: black;" >
            <h4>Guten Tag $salutation ${data.name},</h4>
            vielen Dank für Ihre Bestellung.<br>
            Wir werden Sie benachrichtigen, sobald Ihr(e) Artikel versandt wurde(n).
            <p></p>
            <p style="font-size: 14px;color:#000000;"><b>Bestellnummer: ${data.orderNr}</b><br><b>Datum: ${data.date}</b></p>
            <p></p>
            <p><b>Zahlungsübersicht:</b><br>Mit Versand der Ware erhalten Sie eine Rechnung per E-Mail</p>
            $contentTable
            <p><b>Zahlungsart:</b><br>$paymentText</p>
            <table style="font-size: 12px;text-align:left;width:500px;">
              <tr>
                <th width="100px">
                  <b>Produkt</b>
                </th>
                <th width="200px">
                  <b>Anzahl</b>
                </th>
              <th width="100px">
               <b>Preis</b>
              </th>
                <th width="100px">
                  <b>Lieferzeit</b>
                </th>
              </tr>
            </table>
              <hr align="left" width="500px">
          """ + printProducts(data.products.toList, data.currency) +
      s"""
            </table>
             <p>Haben Sie eine Frage ?<br>
               Wir stehen Ihnen jederzeit unter <span style="color:#f29400;">kundenservice@kigaportal.com</span> zur Verfügung.<br>
               Wir danken Ihnen für Ihr Vertrauen in unsere Produkte und freuen uns, Sie demnächst wieder in
               unserem KiGaPortal-Shop begrüßen zu dürfen.</p>
            <p>Ihr KiGaPortal Team</p>
            <p>Informationen zum Widerrufsrecht finden sie <span style="color:#f29400;"><a href="$rightOfWithdrawalUrl">hier</a></span></p>
           <img src="cid:idkigalogo">
           <p style="padding: 0px; margin: 0px;font-size: 8px;">HOHE Medien OG</p>
           <p style="padding: 0px; margin: 0px;font-size: 8px;">Klosterwiesgasse 101b</p>
           <p style="padding: 0px; margin: 0px;font-size: 8px;">8010 Graz</p>
           <p style="padding: 0px; margin: 0px;font-size: 8px;">UID: ATU64282626</p>
           <p style="padding: 0px; margin: 0px;font-size: 8px;">DVR: 0051853</p>
           <p style="padding: 0px; margin: 0px;font-size: 8px;">Firmenbuchnummer: FN 307106b</p>
           <p style="padding: 0px; margin: 0px;font-size: 8px;">Gericht: Handelsgericht Graz</p>
           <p style="padding: 0px; margin: 0px;font-size: 8px;">E-Mail: <span style="color:#f29400;">kundenservice@kigaportal.com</span></p>
           </div>
          """
  }

  def printProducts(products: List[PurchaseItemRow], currency: String): String = {
    def shippingTime(purchaseItemRow: PurchaseItemRow) = {
      s"Lieferzeit ca. ${purchaseItemRow.getMinDeliveryDays()}-${purchaseItemRow.getMaxDeliveryDays()} Werktage"
    }

    if (products.size > 0) {
      products
        .map(product =>
          s"""<table style="font-size: 12px;text-align:left;width:500px;"><tr>
             |<td width="100px">${product.getTitle()}</td>
             |<td width="200px">${product.getAmount()}</td>
             |<td width="100px">${product.getPriceGrossTotal()} $currency</td>
             |<td width="100px">${shippingTime(product)}</td>
             |</tr></table><hr align="left" width="500px">""".stripMargin)
        .mkString("")
    }
    else {
      ""
    }
  }

  def contentTableTwoAddress(address1: List[String], address2: List[String]): String = {
    if (address2.size > 0) {
      addressToTable("Anschrift:", address1) + addressToTable("Lieferadresse:", address2)
    }
    else {
      addressToTable("Anschrift", address1)
    }
  }

  def addressToTable(header: String, address: List[String]): String = {
    s"""<p><b>$header</b><br>""" + address.mkString("<br>") + "</p>"
  }

  def invoiceEmail(saltation: String, name: String, url: String): String = {
    s"""
      <div style="text-align: left; color: black;" >
      <h4>Guten Tag $saltation $name</h4>
      <p>vielen Dank für Ihren Einkauf im KiGaPortal-Shop!</p>
      <p>Hier ist der Link zu ihrer <a style="color:#f29400;" href="$url">Rechnung</a>.</p>
      <p><b>Wir wünschen Ihnen viel Freude mit unseren Produkten.</b></p>
      <hr align="left" width="500px">
      <p>Mit freundlichen Grüßen<br>Ihr KiGaPortal Team</p>
      <img src="cid:idkigalogo">
      <p style="padding: 0px; margin: 0px;font-size: 8px;">HOHE Medien OG</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">Klosterwiesgasse 101b</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">8010 Graz</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">UID: ATU64282626</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">DVR: 0051853</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">Firmenbuchnummer: FN 307106b</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">Gericht: Handelsgericht Graz</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">E-Mail: <span style="color:#f29400;">kundenservice@kigaportal.com</span></p>
      </div>
      """
  }

  def dunningEmail(): String = {
    //s"Sehr geehrter $salutation $lastname,<br> " +
    s"Sehr geehrte Kundin, Sehr geehrter Kunde<br>" +
      s"Nobody is perfect ... wir haben Verständnis, wenn Sie in der Alltagshektik die pünktliche Bezahlung userer Rechnung übersehen haben."
  }

  def creditVoucherEmail(salutation: String, name: String, url: String): String = {
    s"""
      <div style="text-align: left; color: black;" >
      <h4>Guten Tag $salutation $name</h4>
      <p>das hier ist die email zur Bestätigung ihrer Gutschrift!</p>
      <p>Hier ist der Link zu ihrer <a style="color:#f29400;" href="$url">Gutschrift</a>.</p>
      <hr align="left" width="500px">
      <p>Mit freundlichen Grüßen<br>Ihr KiGaPortal Team</p>
      <img src="cid:idkigalogo">
      <p style="padding: 0px; margin: 0px;font-size: 8px;">HOHE Medien OG</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">Klosterwiesgasse 101b</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">8010 Graz</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">UID: ATU64282626</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">DVR: 0051853</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">Firmenbuchnummer: FN 307106b</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">Gericht: Handelsgericht Graz</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">E-Mail: <span style="color:#f29400;">kundenservice@kigaportal.com</span></p>
      </div>
      """
  }

  def cancelEMail(salutation: String, name: String, orderNr: String, invoiceNr: String): String = {
    val body = if (invoiceNr.equals("")) {
      s"<p>Wir bestätigen hiermit die Stornierung Ihrer Bestellung $orderNr!</p>"
    } else {
      s"<p>Wir bestätigen hiermit die Stornierung Ihrer Bestellung $orderNr / Rechnung $invoiceNr!</p>"
    }
    s"""
      <div style="text-align: left; color: black;" >
      <h4>$salutation $name</h4>
      $body
      <hr align="left" width="500px">
      <p>Mit freundlichen Grüßen<br>Ihr KiGaPortal Team</p>
      <img src="cid:idkigalogo">
      <p style="padding: 0px; margin: 0px;font-size: 8px;">HOHE Medien OG</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">Klosterwiesgasse 101b</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">8010 Graz</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">UID: ATU64282626</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">DVR: 0051853</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">Firmenbuchnummer: FN 307106b</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">Gericht: Handelsgericht Graz</p>
      <p style="padding: 0px; margin: 0px;font-size: 8px;">E-Mail: <span style="color:#f29400;">kundenservice@kigaportal.com</span></p>
      </div>
      """
  }

}
