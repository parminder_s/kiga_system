package com.kiga.shop.document.model

import com.kiga.shop.documents.service.JPurchaseItemRowFactory
import com.kiga.shop.domain.{ProductDetail, Purchase}
import com.kiga.shop.service.AddressFormatter
import com.kiga.types.JLong
import org.joda.time.format.DateTimeFormat

import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

/**
  * Created by faxxe on 1/26/16.
  */
class PrintOrderNoteData(val purchase: Purchase, val productDetails: Map[JLong, ProductDetail]) {

  val dateTimeFormat = DateTimeFormat.forPattern("dd.MM.yyyy")

  var shippingAddress: List[String] = AddressFormatter.getAddress(purchase)
  var billAddress: List[String] = AddressFormatter.getBillAddress(purchase)
  var orderNr: String = purchase.getOrderNr.toString
  var deliveryNr: String = purchase.getShippingNoteNr
  var deliveryPlace: String = "Graz"
  var tableCellItems = new JPurchaseItemRowFactory().createMultiple(purchase, productDetails.asJava, false)
  var customerNr: String = "" + purchase.getCustomer.getCustomerId
  var totalDelivery: Boolean = true
  var sumWeight: String = s"${purchase.getPurchaseItems.foldLeft(0)((r, c) => (r + c.getWeightGramTotal)) / 1000.0}"

}
