package com.kiga.shop.web.requests

import com.kiga.shop.document.model.sub.ArticleInOrder

/**
  * Created by peter on 19.01.16.
  */
case class PrintOrderConfirmationRequest
(
  orderNr: String,
  products: List[ArticleInOrder]
)
