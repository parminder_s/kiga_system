package com.kiga.shop.web.requests

import scala.beans.BeanProperty
import org.elasticsearch.common.joda.time.DateTime

/**
  * Created by peter on 08.03.16.
  */
case class PaymentReportFilterRequest(
  @BeanProperty fromDate: String,
  @BeanProperty toDate: String
)
