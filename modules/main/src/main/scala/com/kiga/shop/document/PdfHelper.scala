package com.kiga.shop.document

import com.kiga.print.model.{Table, TableRow, Block, TableCellElement}

/**
  * Created by peter on 01.03.16.
  */
object PdfHelper {

  def Address(shippingAddress: List[String], billAddress: List[String])  = {
    var shippingTableCell = new TableCellElement("", false)
    shippingTableCell.setChildren(shippingAddress.map(x =>{
      var b = new Block(x)
      b.setFontSize(12)
      b.setTextAlign("left")
      b
    }))

    var billTableCell = new TableCellElement("", false)
    billTableCell.setChildren(billAddress.map(x =>{
      var b = new Block(x)
      b.setFontSize(12)
      b.setTextAlign("left")
      b
    }))

    var tableRow = new TableRow()
    if(billAddress.size == 0){
      tableRow.setChildren(List(shippingTableCell))
    }
    else{
      var bufferCell = new TableCellElement("", false)
      bufferCell.widthMM = 10
      tableRow.setChildren(List(shippingTableCell,billTableCell))
    }

    var table = new Table()
    table.setChildren(List(tableRow))

    table
  }

  //Tuple: _1 = Label, _2 = Content
  def RightData(elements: List[Tuple2[String,String]]) = {
    var table = new Table()
    table.setColumWidthsMM(List(-1,-1,5,26))
    table.setChildren(elements.map(x =>{
      var row = new TableRow()
      var label = new TableCellElement(x._1, false, false, "right")
      var content = new TableCellElement(x._2, false, false, "left")
      row.setChildren(List(new TableCellElement("", false), label, new TableCellElement("", false), content))
      row
    }))
    table
  }

  def InvoicePaymentCell(label:String, content:String) = {
    var returner = new TableRow()
    returner.setChildren(List(
      new TableCellElement(label, false, false),
      new TableCellElement(content, false, false, "left"))
    )
    returner
  }
}
