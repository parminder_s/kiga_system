package com.kiga.shop.document

import com.kiga.main.paymentmethod.PaymentMethod
import com.kiga.print.article.FopXml
import com.kiga.print.model._
import com.kiga.shop.document.model.PrintInvoiceData
import com.kiga.shop.documents.model.PurchaseItemRow

import scala.collection.JavaConversions._

/**
  * Created by peter on 24.02.16.
  */
object PdfInvoice {

  def Invoice(printInvoiceData: PrintInvoiceData) = {
    var printObj = new PrintObject(
      InvoiceHeader(),
      InvoiceBody(printInvoiceData),
      InvoiceFooter(printInvoiceData.country.getCode()),
      true)

    FopXml.print(printObj, printInvoiceData.invoiceNr + ".pdf")
  }

  //Invoice methods

  def InvoiceHeader(): List[Printable] = {

    var logo = new Image(s"url('data:image/jped;base64,${Base64Images.kigaLogo()}')")
    logo.setMarginMM(0)
    logo.setContentWidthPx(108)
    var logoContainer = new Block()
    logoContainer.setMarginLeftMM(140)
    logoContainer.setChildren(List(logo))


    var emailSupport = new Block("kundenservice@kigaportal.com")
    emailSupport.setFontSize(8)
    emailSupport.setTextAlign("left")
    emailSupport.setMarginTopMM(-14)

    var addressLine1 = new Block("HOHE Medien OG")
    addressLine1.setFontSize(7)
    addressLine1.setTextAlign("left")
    addressLine1.setMarginTopMM(13)

    var addressLine2 = new Block("A-8010 Graz Klosterwiesgasse 101b")
    addressLine2.setTextAlign("left")
    addressLine1.setFontSize(7)

    var table = new Table()
    var row = new TableRow()
    var tableCell = new TableCellElement("", false)
    tableCell.setChildren(List(addressLine1, addressLine2))
    row.setChildren(List(tableCell))
    table.setChildren(List(row))

    var leader = new Leader()
    leader.setLeaderPattern("rule")
    leader.setLeaderPatternWidthPT(5)
    leader.setLeaderLenghtMM(60)
    var leaderContainer = new Block()
    leaderContainer.setChildren(List(leader))

    List(logoContainer, emailSupport, table, leaderContainer)
  }

  def InvoiceBody(printInvoiceData: PrintInvoiceData): List[Printable] = {
    //start of address

    var address = PdfHelper.Address(printInvoiceData.shippingAddress, printInvoiceData.billAddress)
    var rightData = PdfHelper.RightData(List(
      Tuple2("Rechnungsdatum:", printInvoiceData.date),
      Tuple2("Bestellnummer:", printInvoiceData.orderNumber.toString),
      Tuple2("Rechnungsnummer:", printInvoiceData.invoiceNr),
      Tuple2("Kundennummer:", printInvoiceData.customerId)
    ))

    var billNr = new Block("Rechnung")
    billNr.setTextAlign("left")
    billNr.setFontSize(28)
    billNr.setFontWeight("bold")

    var productsTable = InvoiceProductsTable(
      List(
        Tuple2("Stk.", 10),
        Tuple2("Beschreibung", -1),
        Tuple2("Artikelnr.", 42),
        Tuple2("Brutto Preis /Stk", 20),
        Tuple2("MWSt (%)", 17),
        Tuple2("Summe Brutto", 15)),
      printInvoiceData.products.toList)

    var sumTable = InvoiceSumTable(
      List(
        Tuple2("Nettobetrag", printInvoiceData.priceNetTotal.toString)) ++
        printInvoiceData.vatSumPerRate.map(x => {
          Tuple2("MWSt " + x.prettyRate + "%", x.total.toString)
        }) ++
        List(
          Tuple2("Bruttobetrag", printInvoiceData.priceGrossTotal.toString),
          Tuple2("Überweisungsbetrag", s"${printInvoiceData.currency} ${printInvoiceData.transferalSum.toString}"))
    )

    var endContainer = new Block()
    endContainer.setMarginTopMM(-15)
    var thankYou = new Block("Vielen Dank,")
    var team = new Block("Ihr KiGaPortal Team")
    var payment = if (printInvoiceData.paymentMethod == PaymentMethod.INVOICE) PaymentRE(printInvoiceData) else PaymentCC(printInvoiceData)
    if (printInvoiceData.paymentMethod == PaymentMethod.INVOICE) {
      var daysDue1 = new Block(s"Zahlung innerhalb von ${printInvoiceData.dayToPayInvoice} Tagen")
      var daysDue2 = new Block("ohne Abzug.")
      daysDue2.setMarginBottomMM(10)
      endContainer.setChildren(List(daysDue1, daysDue2, thankYou, team))
    } else {
      endContainer.setChildren(List(thankYou, team))
    }

    List(address, rightData, billNr, productsTable, sumTable, endContainer, payment)
  }

  def PaymentCC(printInvoiceData: PrintInvoiceData) = {
    var paymentText = printInvoiceData.paymentMethod match {
      case PaymentMethod.CREDITCARD => "Kreditkarte"
      case PaymentMethod.INVOICE => "Rechnung"
      case PaymentMethod.PAYPAL => "PayPal"
      case PaymentMethod.SOFORT => "Sofort Überweisung"
      case _ => printInvoiceData.paymentMethod.toString
    }
    var payment = new Block(s"Zahlung erfolgte durch: $paymentText")
    payment.setFontSize(12)
    payment.setMarginTopMM(20)
    payment.setBorder("dotted 0.3mm black")
    payment.setPaddingMM(2)
    payment
  }

  def PaymentRE(printInvoiceData: PrintInvoiceData) = {
    var payment = new Block(s"Bitte führen Sie bei der Überweisung unbedingt Ihre Rechnungsnummer an," +
      s" da die Zahlung sonst NICHT zugeordnet werden kann!")

    var paymentTable = new Table()
    paymentTable.setColumWidthsMM(List(45, 90, -1))

    var paymentBankName = PdfHelper.InvoicePaymentCell("", printInvoiceData.bankName)
    var paymentIban = PdfHelper.InvoicePaymentCell("IBAN:", printInvoiceData.iban)
    var paymentBic = PdfHelper.InvoicePaymentCell("BIC:", printInvoiceData.bic)
    var paymentAccountNr = PdfHelper.InvoicePaymentCell("Kontonummer:", printInvoiceData.accountNr)
    var paymentInvoiceNumber = PdfHelper.InvoicePaymentCell("Rechnungsnummer:", printInvoiceData.invoiceNr)

    paymentTable.setChildren(List(paymentBankName))
    if (printInvoiceData.iban != null) {
      paymentTable.setChildren(paymentTable.getChildren ::: List(paymentIban))
    }
    if (printInvoiceData.bic != null) {
      paymentTable.setChildren(paymentTable.getChildren ::: List(paymentBic))
    }
    if (printInvoiceData.accountNr != null) {
      paymentTable.setChildren(paymentTable.getChildren ::: List(paymentAccountNr))
    }
    paymentTable.setChildren(paymentTable.getChildren ::: List(paymentInvoiceNumber))

    payment.setChildren(List(paymentTable))
    payment.setFontSize(14)
    payment.setMarginTopMM(20)
    payment.setFontWeight("bold")
    payment.setBorder("dotted 0.3mm black")
    payment.setPaddingMM(2)
    payment
  }

  //heading Tuple: _1 = Content, _2 = WidthMM
  //width in parameter, because it could change in other language
  //produict Tuple: _1 ProductItemRow, _2 = textAlign
  def InvoiceProductsTable(heading: List[Tuple2[String, Int]], product: List[PurchaseItemRow]) = {
    var table = new Table()
    table.setColumWidthsMM(heading.map(_._2))
    var header = new TableRow()
    header.setChildren(heading.map(x => {
      var tableCell = new TableCellElement(x._1, true, false, "center")
      tableCell
    }))

    table.setChildren(List(header) ++ product.map(x => {
      var row = new TableRow()
      var cellAmount = new TableCellElement(x.getAmount(), true, false)
      cellAmount.textAlign = "left"
      var cellTitle = new TableCellElement(x.getTitle(), true, false)
      cellTitle.textAlign = "left"
      var cellCode = new TableCellElement(x.getCode(), true, false)
      cellCode.textAlign = "left"
      var cellPriceGrossSingle = new TableCellElement(x.getPriceGrossSingle().toString(), true, false)
      cellPriceGrossSingle.textAlign = "right"
      var cellVatRateString = new TableCellElement(x.getVatRateString(), true, false)
      cellVatRateString.textAlign = "right"
      var cellPriceGrossTotal = new TableCellElement(x.getPriceGrossTotal().toString(), true, false)
      cellPriceGrossTotal.textAlign = "right"

      row.setChildren(List(
        cellAmount, cellTitle, cellCode, cellPriceGrossSingle,
        cellVatRateString, cellPriceGrossTotal))
      row
    }))

    table
  }

  //Invoice helper

  //Tuple: _1 = Label, _2 = Content
  def InvoiceSumTable(elements: List[Tuple2[String, String]]): Block = {
    var table = new Table()
    table.setColumWidthsMM(List(100, -1, -1))
    table.setChildren(elements.map(x => {
      var row = new TableRow()
      row.setChildren(List(new TableCellElement("", false),
        new TableCellElement(x._1, true, false, "left"),
        new TableCellElement(x._2, true, false)))
      row
    }))
    var container = new Block()
    container.setKeepTogetherWithinPage("always")
    container.setMarginTopMM(5)
    container.setChildren(List(table))
    container
  }

  def InvoiceFooter(countryCode: String): List[Printable] = {

    var container = new Block()
    container.setFontSize(9)
    container.setMarginTopMM(5)
    container.setMarginRightMM(0)

    var leader = new Leader()
    leader.setLeaderPattern("rule")
    leader.setLeaderPatternWidthPT(5)
    leader.setLeaderLenghtPercent(100)

    val taxText = if ("ch".equalsIgnoreCase(countryCode) || "li".equalsIgnoreCase(countryCode)) {
      "Steuer-Nr.:CHE-486.062.681"
    }
    else {
      "UID:ATU64282626"
    }

    container.setChildren(List(
      leader,
      new Block("HOHE Medien OG KiGaPortal Klosterwiesgasse 101b, A-8010 Graz"),
      new Block("E-Mail: kundenservice@kigaportal.com"),
      new Block(s"DVR:0051853 / $taxText / FN307106b / Handelsgericht Graz"),
      leader
    ))

    List(container)
  }
}
