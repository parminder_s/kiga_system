package com.kiga.shop.document.model

import com.kiga.shop.documents.service.JPurchaseItemRowFactory
import com.kiga.shop.domain.{ProductDetail, Purchase}
import com.kiga.shop.service.AddressFormatter
import com.kiga.types.JLong
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.collection.JavaConverters._

/**
  * Created by faxxe on 2/17/16.
  */
class ConfirmationEmailData(var purchase: Purchase, productDetails: Map[JLong, ProductDetail]) {
  val dateTimeFormat = DateTimeFormat.forPattern("dd.MM.yyyy")
  var email = purchase.getEmail
  var gender = purchase.getGender
  var orderNr = purchase.getOrderNr
  var date = dateTimeFormat.print(new DateTime(purchase.getLastEdited))
  var name = purchase.getLastname
  var address = AddressFormatter.getAddress(purchase)
  var altAddress = AddressFormatter.getBillAddress(purchase)
  var paymentMethod = purchase.getPaymentMethod
  var currency = purchase.getCurrency
  var products = new JPurchaseItemRowFactory().createMultiple(purchase, productDetails.asJava, true)
}
