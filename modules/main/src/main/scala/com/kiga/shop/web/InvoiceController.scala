package com.kiga.shop.web

import java.io.FileInputStream
import javax.inject.Inject
import javax.servlet.http.HttpServletResponse

import com.kiga.print.invoice.InvoiceToPdf
import com.kiga.shop.domain.Purchase
import com.kiga.shop.numbers.NumberGenerator
import com.kiga.shop.service.ShopS3Uploader
import com.kiga.types.JLong
import org.apache.commons.io.IOUtils
import org.joda.time.DateTime
import com.kiga.shop.repository.PurchaseRepository
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation.{PathVariable, RequestMapping, RequestMethod, RestController}

import scala.beans.BeanProperty

/**
 * Created by faxxe on 1/28/16.
 */
@RestController
@ConfigurationProperties("shop")
@RequestMapping(Array("/shop"))
class InvoiceController {

  @Inject var repo: PurchaseRepository = _
  @Inject var shopS3Uploader: ShopS3Uploader = _

  @BeanProperty var email: String = _

  @RequestMapping(value = Array("/getInvoice/{id}/{hash}"),
    method = Array(RequestMethod.GET))
  def getInvoice(@PathVariable("id") id: JLong, @PathVariable("hash") hash: String, response: HttpServletResponse): Unit ={
    var purchase: Purchase = repo.findById(id)
    if (purchase == null || purchase.getInvoiceHash != hash){
      response.sendError(HttpServletResponse.SC_NOT_FOUND)
    }

    val fileName = shopS3Uploader.downloadInvoice(purchase)
    val file = new FileInputStream(fileName)
    response.setContentType("application/pdf")
    response.setHeader("Content-Disposition",
      "attachment; filename=\"" + "KiGaRechnung.pdf" + "\"")
    IOUtils.copy(file, response.getOutputStream())
    file.close()
    response.getOutputStream().flush()
  }

  @RequestMapping(value = Array("/getCreditVoucher/{id}/{hash}"),
    method = Array(RequestMethod.GET))
  def getCreditVoucher(@PathVariable("id") id: JLong, @PathVariable("hash") hash: String, response: HttpServletResponse): Unit ={
    var purchase: Purchase = repo.findById(id)
    if (purchase == null || purchase.getInvoiceHash != hash){
      response.sendError(HttpServletResponse.SC_NOT_FOUND)
    }

    val fileName = shopS3Uploader.downloadCreditVoucher(purchase)
    val file = new FileInputStream(fileName)
    response.setContentType("application/pdf")
    response.setHeader("Content-Disposition",
      "attachment; filename=\"" + "Gutschrift.pdf" + "\"")
    IOUtils.copy(file, response.getOutputStream())
    file.close()
    response.getOutputStream().flush()
  }
}
