package com.kiga.shop.web.responses

import java.time.{Instant, LocalDate}

import com.kiga.types.{JBigDecimal, JDate}

/**
  * Created by rainerh on 09.02.16.
  */
case class InvoicesResponse
(
  var number: String,
  var invoiceDate: Instant,
  var url: String,
  var total: JBigDecimal,
  var currency: String,
  var isPaid: Boolean
)


