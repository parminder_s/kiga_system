package com.kiga.shop.service

import java.io.File
import javax.inject.Named

import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.{GetObjectRequest, PutObjectRequest}
import com.amazonaws.{AmazonClientException, AmazonServiceException}
import com.kiga.shop.domain.Purchase
import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties

import scala.beans.BeanProperty

/**
  * Created by faxxe on 1/24/16.
  */
@Named
@ConfigurationProperties("shop")
class ShopS3Uploader {

  val extension = "pdf"
  private val logger = LoggerFactory.getLogger(classOf[ShopS3Uploader])
  @BeanProperty var bucketName: String = _
  @BeanProperty var accessKey: String = _
  @BeanProperty var secretKey: String = _

  def uploadToAmazon(file: File, key: String): Boolean = {
    try {
      logger.debug(s"putting file to s3 $key")
      getAmazonS3Client.putObject(new PutObjectRequest(bucketName, key, file))
      true
    } catch {
      case e: AmazonClientException => {
        logger.error("The client encountered an internal error while trying to communicate with S3, such as not being " +
          "able to access the network.")
        throw e
      }
      case e: AmazonServiceException => {
        logger.error("The request made it to Amazon S3, but was rejected with an error response for some reason.")
        throw e
      }
    }
  }

  def uploadShippingNote(file: File, key: String): Unit = {
    uploadToAmazon(file, key)
  }

  def downloadCreditVoucher(purchase: Purchase): String = {
    downloadCreditVoucher(purchase.getCreditVoucherS3Url, purchase.getCreditVoucherNr)
  }


  def downloadCreditVoucher(key: String, creditVoucherNr: String): String = {
    val filename = s"/tmp/$creditVoucherNr.$extension"
    getAmazonS3Client.getObject(new GetObjectRequest(bucketName, key), new File(filename))
    filename
  }

  def downloadInvoice(purchase: Purchase): String = {
    downloadInvoice(purchase.getInvoiceS3Url, purchase.getInvoiceNr)
  }

  def downloadInvoice(key: String, invoiceNumber: String): String = {
    val filename = s"/tmp/$invoiceNumber.pdf"
    getAmazonS3Client.getObject(new GetObjectRequest(bucketName, key), new File(filename))
    filename
  }

  def downloadFile(key: String, bucket: String): String = {
    val nr = key.split("/").last
    val filename = s"/tmp/$nr.$extension"
    getAmazonS3Client.getObject(new GetObjectRequest(bucket, key), new File(filename))
    filename
  }

  def downloadAudio(key: String, bucket: String): String = {
    val nr = key.split("/").last
    val filename = s"/tmp/$nr"
    getAmazonS3Client.getObject(new GetObjectRequest(bucket, key), new File(filename))
    filename
  }

  def getAmazonS3Client: AmazonS3Client = {
    val awsCredentials: BasicAWSCredentials = new BasicAWSCredentials(accessKey, secretKey)
    val amazonS3Client = new AmazonS3Client(awsCredentials)
    amazonS3Client
  }

  def downloadShippingNote(purchase: Purchase): String = {
    val country = purchase.getCountry.getCode.toUpperCase
    downloadShippingNote(purchase.getShippingNoteS3Url, purchase.getShippingNoteNr)
  }

  def downloadShippingNote(key: String, shippingNoteNumber: String): String = {
    val filename = s"/tmp/$shippingNoteNumber.pdf"
    getAmazonS3Client.getObject(new GetObjectRequest(bucketName, key), new File(filename))
    filename
  }

  def copyFileOnS3(srcKey: String, destKey: String): Unit = {
    try {
      logger.debug(s"copying file on s3 from $srcKey to $destKey")
      getAmazonS3Client.copyObject(bucketName, srcKey, bucketName, destKey)
    } catch {
      case e: AmazonClientException => {
        logger.error("The client encountered an internal error while trying to communicate with S3, such as not being " +
          "able to access the network.")
        throw e
      }
      case e: AmazonServiceException => {
        logger.error("The request made it to Amazon S3, but was rejected with an error response for some reason.")
        throw e
      }
    }
  }

}
