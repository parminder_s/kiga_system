package com.kiga.shop.service

import com.kiga.shop.domain.{Product, CreditVoucherItem, ProductDetail, Purchase}
import com.kiga.shop.web.responses.PurchaseItemResponse
import com.kiga.types._

/**
  * Created by rainerh on 21.02.16.
  */
trait ProductHelper {
  def getProductDetailsFromPurchase(purchase: Purchase): Map[JLong, ProductDetail]
  def getProductDetailsFromPurchaseAsJava(purchase: Purchase): JMap[JLong, ProductDetail]
  def getProductDetailsCreditVoucher(items: JList[PurchaseItemResponse], purchase: Purchase): JList[CreditVoucherItem]
}
