package com.kiga.subscription.domain.zend;

/**
 * Created by mfit on 05.10.16.
 */
public enum SubscriptionStatus {
  REGISTERED,
  ORDERED,
  UNVERIFIED,
  CANCELED,
  ACTIVE,
  NEW,
  LOCKED,
  END
}
