package com.kiga.subscription.domain.zend;

import com.kiga.db.KigaLegacyDbEntityModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by mfit on 05.10.16.
 */
@Entity
@Table(name = "master_product")
public class SubscriptionType extends KigaLegacyDbEntityModel {

  @Id
  @Column(name = "product_id")
  @GeneratedValue
  private Long id;

  @Column(name = "prodtype_code")
  private String typeCode;

  @Column(name = "product_title")
  private String name;

  @Column(name = "product_period")
  private int durationPeriod;

  @Column(name = "product_period_days")
  private int durationPeriodDays;

  @Column(name = "product_period_type")
  private String durationType;

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }

  public String getTypeCode() {
    return typeCode;
  }

  public void setTypeCode(String typeCode) {
    this.typeCode = typeCode;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getDurationPeriod() {
    return durationPeriod;
  }

  public void setDurationPeriod(int durationPeriod) {
    this.durationPeriod = durationPeriod;
  }

  public int getDurationPeriodDays() {
    return durationPeriodDays;
  }

  public void setDurationPeriodDays(int durationPeriodDays) {
    this.durationPeriodDays = durationPeriodDays;
  }

  public String getDurationType() {
    return durationType;
  }

  public void setDurationType(String durationType) {
    this.durationType = durationType;
  }

}
