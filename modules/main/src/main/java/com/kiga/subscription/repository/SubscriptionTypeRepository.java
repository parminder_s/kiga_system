package com.kiga.subscription.repository;

import com.kiga.subscription.domain.zend.SubscriptionType;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by mfit on 05.10.16.
 */
public interface SubscriptionTypeRepository extends CrudRepository<SubscriptionType, Long> {
}
