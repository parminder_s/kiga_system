package com.kiga.subscription.repository;

import com.kiga.subscription.domain.zend.Subscription;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by mfit on 05.10.16.
 */
public interface SubscriptionRepository extends CrudRepository<Subscription, Long> {
}
