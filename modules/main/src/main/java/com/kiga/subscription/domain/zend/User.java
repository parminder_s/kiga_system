package com.kiga.subscription.domain.zend;

import com.kiga.db.KigaLegacyDbEntityModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by mfit on 05.10.16.
 */
@Entity
@Table(name = "customer_user")
public class User extends KigaLegacyDbEntityModel {

  @Id
  @Column(name = "user_id")
  @GeneratedValue
  private Long id;

  @ManyToOne
  @JoinColumn(name = "product_id", referencedColumnName = "id")
  Subscription subscription;

  @Column(name = "country_code")
  private String countryCode;

  @Column(name = "user_is_master")
  private Boolean isMainUser;

  @Column(name = "user_name")
  private String username;

  @Column(name = "user_email")
  private String email;

  @Column(name = "language_code")
  private String locale;

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }

  public Subscription getSubscription() {
    return subscription;
  }

  public void setSubscription(Subscription subscription) {
    this.subscription = subscription;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public Boolean getMainUser() {
    return isMainUser;
  }

  public void setMainUser(Boolean mainUser) {
    isMainUser = mainUser;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

}
