package com.kiga.subscription.domain.zend;

import com.kiga.db.KigaLegacyDbEntityModel;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Created by mfit on 05.10.16.
 */
@Entity
@Table(name = "product")
public class Subscription extends KigaLegacyDbEntityModel {

  @Id
  @Column(name = "product_id")
  @GeneratedValue
  private Long id;

  @ManyToOne
  @JoinColumn(name = "product_id", referencedColumnName = "product_id")
  SubscriptionType subscriptionType;

  @Column(name = "country_code")
  private String countryCode;

  @Column(name = "period_start")
  private Date periodStartDate;

  @Column(name = "period_end")
  private Date periodEndDate;

  @Column(name = "product_start")
  private Date productStartDate;

  @Column(name = "status")
  @Enumerated(EnumType.STRING)
  private SubscriptionStatus status;

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }

  public SubscriptionType getSubscriptionType() {
    return subscriptionType;
  }

  public void setSubscriptionType(SubscriptionType subscriptionType) {
    this.subscriptionType = subscriptionType;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public Date getPeriodStartDate() {
    return periodStartDate;
  }

  public void setPeriodStartDate(Date periodStartDate) {
    this.periodStartDate = periodStartDate;
  }

  public Date getPeriodEndDate() {
    return periodEndDate;
  }

  public void setPeriodEndDate(Date periodEndDate) {
    this.periodEndDate = periodEndDate;
  }

  public Date getProductStartDate() {
    return productStartDate;
  }

  public void setProductStartDate(Date productStartDate) {
    this.productStartDate = productStartDate;
  }

  public SubscriptionStatus getStatus() {
    return status;
  }

  public void setStatus(SubscriptionStatus status) {
    this.status = status;
  }

}
