package com.kiga.subscription.repository;

import com.kiga.subscription.domain.zend.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by mfit on 05.10.16.
 */
public interface UserRepository extends CrudRepository<User, Long> {
}
