package com.kiga;

/**
 * Created by rainerh on 30.04.16.
 */
public class InvalidConfigurationException extends Exception {
  public InvalidConfigurationException(String message) {
    super(message);
  }
}
