package com.kiga.metrics;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.metrics.tracing.KigaTraceFilter;
import com.kiga.metrics.tracing.KigaTraceRepository;
import com.kiga.metrics.tracing.NoopTraceRepository;
import com.kiga.security.services.SecurityService;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.actuate.metrics.writer.MetricWriter;
import org.springframework.boot.actuate.trace.TraceProperties;
import org.springframework.boot.actuate.trace.TraceRepository;
import org.springframework.boot.actuate.trace.WebRequestTraceFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.influxdb.InfluxDBTemplate;

@Configuration
@Log4j2
public class MetricsConfig {
  @Bean
  public MetricWriter kigaExporter(
      InfluxDBTemplate influxTemplate,
      MetricsProperties metricsProperties,
      HostnameGetter hostnameGetter) {

    if (metricsProperties.isEnabled()) {
      return new KigaExporter(
          influxTemplate, metricsProperties.getEnvironment(), hostnameGetter.getHostname());
    } else {
      return null;
    }
  }

  @Bean
  TraceRepository traceRepository(MetricsProperties metricsProperties) {
    if (metricsProperties.isTrace()) {
      ObjectMapper objectMapper = new ObjectMapper();
      try {
        FileOutputStream fos = new FileOutputStream(metricsProperties.getTraceLogPath());
        return new KigaTraceRepository(fos, objectMapper);
      } catch (FileNotFoundException fnfe) {
        log.error("FileNotFoundException in opening trace file");
        return new NoopTraceRepository();
      }
    } else {
      return new NoopTraceRepository();
    }
  }

  @Bean
  WebRequestTraceFilter webRequestTraceFilter(
      TraceRepository traceRepository,
      TraceProperties traceProperties,
      SecurityService securityService) {
    return new KigaTraceFilter(traceRepository, traceProperties, securityService);
  }
}
