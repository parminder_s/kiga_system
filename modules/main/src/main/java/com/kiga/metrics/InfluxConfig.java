package com.kiga.metrics;

/**
 * Created by faxxe on 2/1/17.
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.influxdb.DefaultInfluxDBTemplate;
import org.springframework.data.influxdb.InfluxDBConnectionFactory;

@Configuration
public class InfluxConfig {

  /**
   * connectionFactory.
   */
  @Bean
  public InfluxDBConnectionFactory connectionFactory(final MetricsProperties properties) {
    if (properties != null && properties.isEnabled()) {
      return new InfluxDBConnectionFactory(properties.influxProperties());
    } else {
      return null;
    }
  }

  /**
   * innflux template.
   */
  @Bean
  public DefaultInfluxDBTemplate defaultTemplate(
    final InfluxDBConnectionFactory connectionFactory
  ) {
    /*
     * If you are just dealing with Point objects from 'influxdb-java' you could
     * also use an instance of class DefaultInfluxDBTemplate.
     */
    if (connectionFactory != null) {
      return new DefaultInfluxDBTemplate(connectionFactory);
    } else {
      return null;
    }
  }
}

