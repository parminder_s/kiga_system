package com.kiga.metrics.tracing;

import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import com.kiga.web.web.ResettableInputStreamRequestWrapper;
import org.apache.commons.io.IOUtils;
import org.springframework.boot.actuate.trace.TraceProperties;
import org.springframework.boot.actuate.trace.TraceRepository;
import org.springframework.boot.actuate.trace.WebRequestTraceFilter;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by faxxe on 2/13/17.
 */
public class KigaTraceFilter extends WebRequestTraceFilter {

  private TraceRepository traceRepository;
  private TraceProperties traceProperties;
  private SecurityService securityService;

  /**
   * constructor.
   */
  public KigaTraceFilter(TraceRepository repository,
                         TraceProperties properties,
                         SecurityService securityService) {
    super(repository, properties);
    this.traceRepository = repository;
    this.traceProperties = properties;
    this.securityService = securityService;
  }


  @Override
  protected void doFilterInternal(HttpServletRequest request,
                                  HttpServletResponse response, FilterChain filterChain)
    throws ServletException, IOException {
    super.doFilterInternal(new ResettableInputStreamRequestWrapper(request), response, filterChain);
  }

  @Override
  protected Map<String, Object> getTrace(HttpServletRequest request) {
    Map<String, Object> trace = super.getTrace(request);
    trace.put("requestPayload", getPayload(request));
    trace.put("member", getMember());
    return trace;
  }

  private String getMember() {
    String member = "unknown";
    try {
      Optional memberOption = securityService.getSessionMember();
      if (memberOption.isPresent()) {
        Member member1 = (Member) memberOption.get();
        StringBuilder builder = new StringBuilder();
        builder.append(member1.getId());
        builder.append(" :");
        builder.append(member1.getFirstname());
        builder.append(" ");
        builder.append(member1.getLastname());
        member = builder.toString();
      }
      return member;
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return member;
  }

  private String getPayload(HttpServletRequest request) {

    StringWriter sw = new StringWriter();
    if (request != null) {
      try {
        InputStream inputStream = request.getInputStream();
        IOUtils.copy(inputStream, sw);
        return sw.toString();
      } catch (IOException ioe) {
        ioe.printStackTrace();
      }
    }
    return null;
  }
}
