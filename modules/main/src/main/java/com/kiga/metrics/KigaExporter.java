package com.kiga.metrics;

import org.influxdb.dto.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.autoconfigure.ExportMetricWriter;
import org.springframework.boot.actuate.metrics.Metric;
import org.springframework.boot.actuate.metrics.writer.Delta;
import org.springframework.boot.actuate.metrics.writer.MetricWriter;
import org.springframework.data.influxdb.InfluxDBTemplate;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by faxxe on 12/28/16.
 */

@ExportMetricWriter
public class KigaExporter implements MetricWriter {
  Logger logger = LoggerFactory.getLogger(KigaExporter.class);

  private String environment;
  private String hostname;

  private InfluxDBTemplate influxTemplate;

  private Map<String, Integer> values = new HashMap<>();
  private List<Metric<Number>> timings = new ArrayList<>();

  /**
   * constructor.
   */
  public KigaExporter(InfluxDBTemplate influxTemplate,
                      String environment,
                      String hostname) {
    this.influxTemplate = influxTemplate;
    this.environment = environment;
    this.hostname = hostname;
  }

  @Override
  public void increment(Delta delta) {
    if (delta == null) {
      return;
    }
    final Integer newValue = delta.getValue().intValue();
    final String name = delta.getName();

    values.computeIfAbsent(name, k -> newValue);
    values.computeIfPresent(name, (key, val) -> val + newValue);

  }

  /**
   * send the data.
   */
  public synchronized void send() {
    final Map<String, Integer> val = values;
    values = new HashMap<>();
    Long timestamp = System.currentTimeMillis();

    List<Point> points = val
      .entrySet()
      .stream()
      .map(x -> getCountPoint(timestamp, x.getKey(), x.getValue()))
      .collect(Collectors.toList());

    List<Metric<Number>> timingsToSend = timings;
    timings = new ArrayList<>();
    Map<String, Float> averageTimings = getAverage(timingsToSend);

    List<Point> averages = averageTimings
      .entrySet()
      .stream()
      .map(x -> getGaugePoint(timestamp, x.getKey(), x.getValue()))
      .collect(Collectors.toList());

    points.addAll(averages);

    try {
      if (influxTemplate != null) {
        influxTemplate.write(points);
      }
    } catch (Exception exception) {
      values.clear();
      timings.clear();
      logger.error("Failure writing metrics to influx: ", exception);
    }

  }


  /**
   * Calculate average over given list of metrics.
   */
  public Map<String, Float> getAverage(List<Metric<Number>> metrics) {

    Map<String, Float> sums = new HashMap<>();
    Map<String, Integer> counts = new HashMap<>();

    metrics.forEach(x -> {
      sums.computeIfPresent(x.getName(), (key, val) -> val + x.getValue().floatValue());
      counts.computeIfPresent(x.getName(), (key, val) -> val + 1);
      sums.computeIfAbsent(x.getName(), key -> x.getValue().floatValue());
      counts.computeIfAbsent(x.getName(), key -> 1);
    });

    return sums.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
      e -> sums.get(e.getKey()) / ((float)counts.get(e.getKey()))));
  }


  private Point getGaugePoint(Long timestamp, String name, Number value) {
    final Point.Builder point = Point.measurement(environment)
      .time(timestamp, TimeUnit.MILLISECONDS)
      .tag("url", name)
      .tag("hostname", hostname)
      .addField("duration", value);

    return point.build();
  }

  private Point getCountPoint(Long timestamp, String name, Number value) {

    final Point.Builder point = Point.measurement(environment)
      .time(timestamp, TimeUnit.MILLISECONDS)
      .tag("url", name)
      .tag("hostname", hostname)
      .addField("count", value);

    return point.build();
  }


  @Override
  public void reset(String metricName) {
  }

  @Override
  public void set(Metric<?> value) {
    if (value == null) {
      return;
    }
    timings.add((Metric<Number>) value);
  }

}
