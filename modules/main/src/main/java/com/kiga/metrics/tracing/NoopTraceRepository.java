package com.kiga.metrics.tracing;

import org.springframework.boot.actuate.trace.Trace;
import org.springframework.boot.actuate.trace.TraceRepository;


import java.util.List;
import java.util.Map;

/**
 * Created by faxxe on 2/8/17.
 */
public class NoopTraceRepository implements TraceRepository {

  @Override
  public List<Trace> findAll() {
    return null;
  }

  @Override
  public void add(Map<String, Object> traceInfo) {

  }
}
