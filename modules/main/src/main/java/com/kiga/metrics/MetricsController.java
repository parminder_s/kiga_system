package com.kiga.metrics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.MetricsEndpoint;
import org.springframework.boot.actuate.trace.Trace;
import org.springframework.boot.actuate.trace.TraceRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.Map;

/**
 * Created by faxxe on 1/3/17.
 */

@RestController
public class MetricsController {

  private MetricsEndpoint metricsEndpoint;
  private TraceRepository traceRepository;

  @Autowired
  public MetricsController(MetricsEndpoint metricsEndpoint, TraceRepository traceRepository) {
    this.metricsEndpoint = metricsEndpoint;
    this.traceRepository = traceRepository;
  }


  @RequestMapping(value = "/metrics")
  public Map<String, Object> getMetrics() {
    return this.metricsEndpoint.invoke();
  }

  @RequestMapping(value = "/trace", method = {RequestMethod.GET, RequestMethod.POST})
  public List<Trace> trace() {
    return traceRepository.findAll();
  }
}
