package com.kiga.metrics;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.influxdb.InfluxDBProperties;

/**
 * Created by faxxe on 2/1/17.
 */

@ConfigurationProperties("metrics")
@Configuration
@Getter
@Setter
public class MetricsProperties {

  private String url;
  private String username;
  private String password;
  private String database;
  private String retentionPolicy;
  private boolean enabled;
  private String environment;
  private boolean trace;
  private String traceLogPath;


  /**
   * get the corresponding influxproperties.
   */
  public InfluxDBProperties influxProperties() {
    InfluxDBProperties properties = new InfluxDBProperties();
    properties.setUrl(this.url);
    properties.setUsername(this.username);
    properties.setPassword(this.password);
    properties.setDatabase(this.database);
    properties.setRetentionPolicy(this.retentionPolicy);
    return properties;
  }

}
