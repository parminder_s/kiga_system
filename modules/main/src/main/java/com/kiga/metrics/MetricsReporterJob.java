package com.kiga.metrics;

import com.kiga.spec.Job;
import com.kiga.spec.Jobable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.writer.MetricWriter;
import org.springframework.stereotype.Service;

/**
 * Created by faxxe on 1/3/17.
 */

@Job(cronProperty = "metricsCron")
@Service
public class MetricsReporterJob implements Jobable {

  KigaExporter kigaExporter;

  @Autowired
  public MetricsReporterJob(MetricWriter kigaExporter) {
    this.kigaExporter = (KigaExporter) kigaExporter;
  }

  @Override
  public void runJob() {
    if (kigaExporter != null) {
      kigaExporter.send();
    }
  }
}
