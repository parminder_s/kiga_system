package com.kiga.metrics;

import lombok.Getter;
import org.springframework.stereotype.Service;


import java.net.InetAddress;

/**
 * Created by faxxe on 2/14/17.
 */
@Service
public class HostnameGetter {

  @Getter
  private String hostname;

  /**
   * constructor.
   */
  public HostnameGetter() {
    try {
      InetAddress addr = InetAddress.getLocalHost();
      hostname = addr.getHostName();
    } catch (Exception exeption) {
      hostname = "unknown";
      exeption.printStackTrace();
    }
  }

}
