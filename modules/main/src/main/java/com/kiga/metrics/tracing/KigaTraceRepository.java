package com.kiga.metrics.tracing;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.trace.Trace;
import org.springframework.boot.actuate.trace.TraceRepository;


import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * Created by faxxe on 2/8/17.
 */
public class KigaTraceRepository implements TraceRepository {
  private static final Logger logger = LoggerFactory.getLogger(KigaTraceRepository.class);
  private OutputStream outputStream;
  private ObjectMapper objectMapper;


  public KigaTraceRepository(OutputStream outputStream, ObjectMapper objectMapper) {
    this.outputStream = outputStream;
    this.objectMapper = objectMapper;
  }

  @Override
  public List<Trace> findAll() {
    return null;
  }

  @Override
  public void add(Map<String, Object> traceInfo) {
    try {
      outputStream.write(objectMapper.writeValueAsBytes(traceInfo));
      outputStream.write("\r\n".getBytes("UTF-8"));
      outputStream.flush();
    } catch (Exception exception) {
      logger.error("Exception when writing Trace: " + exception.toString());
    }
  }
}
