package com.kiga.idea.member.provider;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.draft.IdeaCategoryDraft;
import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.content.domain.ss.live.QIdeaCategoryLive;
import com.kiga.content.repository.ss.draft.IdeaCategoryRepositoryDraft;
import com.kiga.content.repository.ss.live.IdeaCategoryRepositoryLive;
import com.kiga.ideas.web.converter.IdeaCategoryHierarchyDropdownConverter;
import com.kiga.ideas.web.response.IdeaCategoryHierarchyViewModel;
import com.kiga.main.locale.Locale;
import com.mysema.query.types.expr.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberIdeaCategoryService {
  private IdeaCategoryRepositoryDraft ideaCategoryRepositoryDraft;
  private IdeaCategoryRepositoryLive ideaCategoryRepository;
  private IdeaCategoryHierarchyDropdownConverter ideaCategoryHierarchyDropdownConverter;

  /**
   * Dependencies.
   *
   * @param ideaCategoryRepositoryDraft            ideaCategoryRepositoryDraft
   * @param ideaCategoryRepository                 ideaCategoryRepository
   * @param ideaCategoryHierarchyDropdownConverter ideaCategoryHierarchyDropdownConverter
   */
  @Autowired
  public MemberIdeaCategoryService(
    IdeaCategoryRepositoryDraft ideaCategoryRepositoryDraft,
    IdeaCategoryRepositoryLive ideaCategoryRepository,
    IdeaCategoryHierarchyDropdownConverter ideaCategoryHierarchyDropdownConverter) {
    this.ideaCategoryRepositoryDraft = ideaCategoryRepositoryDraft;
    this.ideaCategoryRepository = ideaCategoryRepository;
    this.ideaCategoryHierarchyDropdownConverter = ideaCategoryHierarchyDropdownConverter;
  }

  /**
   * List subcategories.
   *
   * @param locale Locale
   * @return list of subcategories
   */
  public List<IdeaCategoryHierarchyViewModel> listCategoriesHierarchyForDropdown(Locale locale) {
    QIdeaCategoryLive ideaCategoryLive = QIdeaCategoryLive.ideaCategoryLive;
    BooleanExpression predicateForL1Category =
      ideaCategoryLive.parent.parent.className.eq("MainArticleCategory");
    BooleanExpression predicateForLocale = ideaCategoryLive.locale.eq(locale);

    Sort sort = new Sort(Sort.Direction.ASC, "parent.sort");
    Iterable<IdeaCategoryLive> allL1Categories = ideaCategoryRepository
      .findAll(BooleanExpression.allOf(predicateForL1Category, predicateForLocale), sort);
    return ideaCategoryHierarchyDropdownConverter.convertToResponse(allL1Categories);
  }

  public List<IdeaCategory> list() {
    return ideaCategoryRepository.findByClassName("ArticleCategory");
  }

  public IdeaCategoryDraft findOneDraft(Long id) {
    return ideaCategoryRepositoryDraft.findOne(id);
  }

  public IdeaCategoryLive findOne(Long id) {
    return ideaCategoryRepository.findOne(id);
  }
}
