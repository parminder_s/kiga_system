package com.kiga.security.spring;

import org.springframework.security.core.Authentication;

public interface KigaAuthentication extends Authentication {
  String getSessionId();

  void setUser(KigaUser user);

  boolean isAdmin();

  KigaUser getDetails();

  KigaUser getUser();
}
