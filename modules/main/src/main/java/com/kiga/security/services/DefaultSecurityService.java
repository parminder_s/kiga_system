package com.kiga.security.services;

import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import com.kiga.security.domain.Role;
import com.kiga.security.exception.LoginRequiredException;
import com.kiga.security.exception.RoleException;
import com.kiga.security.repository.MemberRepository;
import com.kiga.web.security.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;


import java.util.Arrays;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by faxxe on 3/22/17.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, scopeName = WebApplicationContext.SCOPE_REQUEST)
public class DefaultSecurityService implements SecurityService {

  private Session session;
  private MemberRepository memberRepository;
  private HttpServletRequest httpServletRequest;
  private static final String phpSessId = "PHPSESSID";


  /**
   * constructor.
   */
  @Autowired
  public DefaultSecurityService(
    Session session, MemberRepository memberRepository, HttpServletRequest httpServletRequest) {
    this.session = session;
    this.memberRepository = memberRepository;
    this.httpServletRequest = httpServletRequest;
  }

  @Override
  public Optional<Member> getSessionMember() {
    String sessionId = getSessionIdFromCookie();
    session.checkParallelLog(Optional.ofNullable(sessionId));
    Optional<Long> memberId = session.getMemberId();

    if (memberId.isPresent() && !sessionId.isEmpty()) {
      return Optional.ofNullable(memberRepository.findOne(memberId.get()));
    }
    return Optional.empty();
  }

  @Override
  public String getSessionIdFromCookie() {
    Cookie[] cookies = httpServletRequest.getCookies();
    if (cookies != null) {
      return Arrays.stream(cookies)
        .filter(x -> phpSessId.equals(x.getName()))
        .map(Cookie::getValue)
        .findFirst()
        .orElse("");
    }
    return "";
  }

  @Override
  public void requiresContentAdmin() {
    if (!hasRole("administrators")) {
      throw new RoleException("admin");
    }
  }

  @Override
  public void requiresPublisher(Locale locale) {
    if (hasRole("administrators")
      || hasRole("publishers") && hasRole("translator_" + locale.toString())) {
      return;
    } else {
      throw new RoleException("admin");
    }
  }

  @Override
  public void requiresCmsMember() {
    Optional<Member> member = getSessionMember();
    if (member.map(Member::getCrmId).isPresent()) {
      throw new RoleException("CmsMember");
    }
  }

  @Override
  public void requireShopAdmin() {
    if (!hasRole("administrators") && !hasRole("shop-administrators")) {
      throw new RoleException("admin");
    }
  }

  @Override
  public void requiresFullSubscription() {
    requiresLogin();
  }

  @Override
  public void requiresLogin() {
    if (!getSessionMember().isPresent()) {
      throw new LoginRequiredException("login required");
    }
  }

  @Override
  public String getFromSession(String key) {
    return session.getAttribute(key).toString();
  }

  @Override
  public Session getSession() {
    return session;
  }

  private boolean hasRole(String role) {
    return getSessionMember()
      .map(member ->  hasRole(role, member))
      .orElse(false);
  }

  /**
   * checks if Member has specified Role.
   */
  public boolean hasRole(String role, Member member) {
    return member
      .getRoles()
      .stream()
      .map(Role::getCode)
      .anyMatch(x -> x.equals(role));
  }
}
