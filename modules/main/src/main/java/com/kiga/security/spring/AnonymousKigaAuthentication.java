package com.kiga.security.spring;

import java.util.Collection;
import java.util.Collections;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class AnonymousKigaAuthentication implements KigaAuthentication {

  private final String sessionId;
  private final KigaUser kigaUser;

  private boolean authenticated;

  public AnonymousKigaAuthentication(String sessionId) {
    this.sessionId = sessionId;
    kigaUser = new KigaUser();
    this.authenticated = false;
  }

  @Override
  public String getSessionId() {
    return sessionId;
  }

  @Override
  public void setUser(KigaUser user) {}

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return Collections.singletonList(new SimpleGrantedAuthority("ROLE_anonymous"));
  }

  @Override
  public Object getCredentials() {
    return null;
  }

  @Override
  public KigaUser getDetails() {
    return kigaUser;
  }

  @Override
  public Object getPrincipal() {
    return null;
  }

  @Override
  public boolean isAuthenticated() {
    return authenticated;
  }

  @Override
  public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
    this.authenticated = isAuthenticated;
  }

  @Override
  public String getName() {
    return null;
  }

  @Override
  public boolean isAdmin() {
    return false;
  }

  @Override
  public KigaUser getUser() {
    return kigaUser;
  }
}
