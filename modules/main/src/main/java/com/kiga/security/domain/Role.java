package com.kiga.security.domain;

import com.kiga.db.KigaEntityModel;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by rainerh on 30.05.16.
 */
@Entity
@Table(name = "`Group`")
public class Role extends KigaEntityModel {
  private String code;

  public Role() {
    this.setClassName("Group");
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
