package com.kiga.security;

import com.kiga.security.spring.AnonymousKigaAuthentication;
import com.kiga.security.spring.KigaPermissionEvaluator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {

  @Autowired KigaPermissionEvaluator kigaPermissionEvaluator;

  @Override
  public MethodSecurityExpressionHandler createExpressionHandler() {
    DefaultMethodSecurityExpressionHandler methodSecurityExpressionHandler =
        new DefaultMethodSecurityExpressionHandler();
    methodSecurityExpressionHandler.setPermissionEvaluator(kigaPermissionEvaluator);
    return methodSecurityExpressionHandler;
  }

  @Bean
  public AuthenticationTrustResolver authenticationTrustResolver() {
    AuthenticationTrustResolverImpl returner = new AuthenticationTrustResolverImpl();
    returner.setAnonymousClass(AnonymousKigaAuthentication.class);
    return returner;
  }
}
