package com.kiga.security.services;

import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import com.kiga.web.security.session.Session;


import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by faxxe on 3/22/17.
 */
public interface SecurityService {

  Optional<Member> getSessionMember();

  String getSessionIdFromCookie();

  void requiresCmsMember();

  void requiresFullSubscription();

  void requiresContentAdmin();

  void requiresPublisher(Locale locale);

  void requireShopAdmin();

  void requiresLogin();

  String getFromSession(String key);

  Session getSession();

}
