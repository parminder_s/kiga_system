package com.kiga.security.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by faxxe on 12/14/16.
 */
public class LoginRequiredException extends RuntimeException {

  /**
   * constructor.
   */
  public LoginRequiredException(String message) {
    super(message);
    this.message = message;
  }

  @Getter
  @Setter
  private String message;

  @Getter
  @Setter
  private String code = "login_required";
}
