package com.kiga.security.spring;

import static java.util.Optional.ofNullable;

import java.util.Collection;
import java.util.Collections;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

public class AuthenticatedKigaAuthentication implements KigaAuthentication {
  @Getter private final String sessionId;

  @Getter private KigaUser user;
  private boolean authenticated;

  public AuthenticatedKigaAuthentication(String sessionId) {
    this.sessionId = sessionId;
    this.authenticated = false;
  }

  @Override
  public String getSessionId() {
    return sessionId;
  }

  @Override
  public void setUser(KigaUser user) {
    this.user = user;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    if (user == null) {
      return Collections.emptyList();
    }
    return user.getAuthorities();
  }

  @Override
  public Object getCredentials() {
    return sessionId;
  }

  @Override
  public KigaUser getDetails() {
    return user;
  }

  @Override
  public Object getPrincipal() {
    return user;
  }

  @Override
  public boolean isAuthenticated() {
    return this.authenticated;
  }

  @Override
  public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
    this.authenticated = isAuthenticated;
  }

  @Override
  public String getName() {
    return ofNullable(user).map(KigaUser::getUsername).orElse("");
  }

  /** indicats if the current logged in user is in the Admininistrator group. */
  @Override
  public boolean isAdmin() {
    return this.getAuthorities()
        .stream()
        .map(GrantedAuthority::getAuthority)
        .anyMatch(auth -> auth.equals("ROLE_administrators"));
  }
}
