package com.kiga.security.repository;

import com.kiga.security.domain.Member;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 31.05.16.
 */
public interface MemberRepository extends CrudRepository<Member, Long> {
  Member findByCrmId(String crmId);

  Member findByEmail(String email);

  @EntityGraph(attributePaths = {"roles"},
    type = EntityGraph.EntityGraphType.FETCH)
  Member findWithRoleByEmail(String email);

  @EntityGraph(attributePaths = {"roles"},
    type = EntityGraph.EntityGraphType.FETCH)
  Member findWithRoleById(Long id);

  @Query("select m from Member m where m.crmId is null")
  Member findCmsMember();
}
