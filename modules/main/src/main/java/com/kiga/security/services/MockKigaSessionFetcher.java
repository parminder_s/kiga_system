package com.kiga.security.services;

import com.kiga.security.domain.KigaSession;

import java.io.IOException;
import java.util.Optional;

public class MockKigaSessionFetcher implements KigaSessionFetcher {
  @Override
  public Optional<KigaSession> findBySessionId(String sessionId) throws IOException {
    if (sessionId.equals("lucy")) {
      return Optional.of(KigaSession.builder()
        .accessArticles(true)
        .accessArticlesPool(true)
        .accessForumWrite(true)
        .accessKs(true)
        .completeSessionId("lucy")
        .isCrm(false)
        .loggedInAs(7) //m_lucy?!?!
        .build());
    }
    return Optional.empty();
  }
}
