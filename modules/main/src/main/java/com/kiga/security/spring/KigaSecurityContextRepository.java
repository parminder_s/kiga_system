package com.kiga.security.spring;

import com.kiga.web.security.SecurityContextRepositoryWithoutSessionCreation;
import com.kiga.web.security.session.Session;
import java.util.Optional;
import java.util.stream.Stream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpRequestResponseHolder;

public class KigaSecurityContextRepository extends SecurityContextRepositoryWithoutSessionCreation {

  private final Session defaultSession;

  /** Constructor. */
  public KigaSecurityContextRepository(Session defaultSession) {
    this.defaultSession = defaultSession;
  }

  @Override
  public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
    Optional<String> sessionId =
        Stream.of(
                Optional.ofNullable(requestResponseHolder.getRequest().getCookies())
                    .orElse(new Cookie[] {}))
            .filter(cookie -> cookie.getName().equals("PHPSESSID"))
            .map(Cookie::getValue)
            .findFirst();
    if (sessionId.isPresent()) {
      SecurityContext returner = SecurityContextHolder.createEmptyContext();
      returner.setAuthentication(new AnonymousKigaAuthentication(sessionId.get()));
      defaultSession.get(sessionId);
      return returner;
    }
    return SecurityContextHolder.createEmptyContext();
  }

  /** As we don't write anything into the session in Spring yet, we don't do anything here. */
  @Override
  public void saveContext(
      SecurityContext context, HttpServletRequest request, HttpServletResponse response) {}
}
