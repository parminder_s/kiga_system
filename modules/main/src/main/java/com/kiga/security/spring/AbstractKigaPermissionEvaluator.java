package com.kiga.security.spring;

import java.io.Serializable;
import java.math.BigDecimal;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;

/** Wrapper class to avoid casting in custom permission evaluators. */
public abstract class AbstractKigaPermissionEvaluator implements PermissionEvaluator {
  @Override
  public boolean hasPermission(
      Authentication authentication, Object targetDomainObject, Object permission) {
    throw new IllegalArgumentException("Not Supported yet");
  }

  @Override
  public boolean hasPermission(
      Authentication authentication, Serializable targetId, String targetType, Object permission) {
    if (targetId == null) {
      return this.hasPermission(
          (KigaAuthentication) authentication, null, targetType, (String) permission);
    }
    Serializable id = targetId;
    if (targetId instanceof BigDecimal) {
      id = ((BigDecimal) targetId).intValue();
    }
    return this.hasPermission(
        (KigaAuthentication) authentication,
        ((Integer) id).longValue(),
        targetType,
        (String) permission);
  }

  public abstract boolean hasPermission(
      KigaAuthentication kigaUser, Long id, String targetType, String permission);
}
