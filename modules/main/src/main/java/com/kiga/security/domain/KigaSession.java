package com.kiga.security.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/*
 Once Spring boot is updated we can move this to a Redis Repository, see
 https://www.baeldung.com/spring-data-redis-tutorial
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class KigaSession {
  @JsonProperty("parallelLog_SessionID")
  private Object parallelLogSessionId;
  @JsonProperty("LandingPage")
  private String landingPage;
  @JsonProperty("CompleteSessionID")
  private String completeSessionId;
  @JsonProperty("Referer")
  private String referer;
  @JsonProperty("is_crm")
  private Boolean isCrm;
  @JsonProperty("access_articles")
  private Boolean accessArticles;
  @JsonProperty("access_articles_pool")
  private Boolean accessArticlesPool;
  @JsonProperty("access_forum_write")
  private Boolean accessForumWrite;
  @JsonProperty("access_ks")
  private Boolean accessKs;
  @JsonProperty("licence_user")
  private Boolean licenceUser;
  @JsonProperty("force_ks")
  private Boolean forceKs;
  @JsonProperty("auth_level")
  private Integer authLevel;
  @JsonProperty("msg")
  private List<String> msg = null;
  @JsonProperty("loggedInAs")
  private Integer loggedInAs;
  @JsonProperty("KigaPageReferer")
  private String kigaPageReferer;
}
