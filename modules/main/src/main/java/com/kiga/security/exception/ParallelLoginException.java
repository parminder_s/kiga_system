package com.kiga.security.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by rainerh on 14.03.17.
 */
public class ParallelLoginException extends RuntimeException {

  @Getter
  @Setter
  private String code = "parallel_login";

  @Getter
  @Setter
  private String entryId;

  public ParallelLoginException(String entryId) {
    this.entryId = entryId;
  }

}
