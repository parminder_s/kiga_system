package com.kiga.security.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.security.domain.KigaSession;
import org.springframework.data.redis.connection.jedis.JedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import java.io.IOException;
import java.util.Optional;

public class DefaultKigaSessionFetcher implements KigaSessionFetcher {
  private final JedisConnectionFactory jedisConnectionFactory;
  private final ObjectMapper objectMapper;

  public DefaultKigaSessionFetcher(JedisConnectionFactory jedisConnectionFactory,
                            ObjectMapper objectMapper) {
    this.jedisConnectionFactory = jedisConnectionFactory;
    this.objectMapper = objectMapper;
  }

  @Override
  public Optional<KigaSession> findBySessionId(String sessionId) throws IOException {
    JedisConnection connection = jedisConnectionFactory.getConnection();
    byte[] key = ("session_" + sessionId).getBytes();
    if (connection.exists(key)) {
      byte[] redisData = connection.get(key);
      connection.close();
      return Optional.of(objectMapper.readValue(new String(redisData), KigaSession.class));
    }
    connection.close();
    return Optional.empty();
  }
}
