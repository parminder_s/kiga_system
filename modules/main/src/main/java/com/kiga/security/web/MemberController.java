package com.kiga.security.web;

import static java.util.stream.Collectors.joining;

import com.kiga.security.domain.Role;
import com.kiga.security.spring.SecurityUtil;
import com.kiga.web.props.WebSecurityProperties;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberController {
  private final SecurityUtil securityUtil;
  private final WebSecurityProperties webSecurityProperties;
  private final HttpServletResponse response;

  @Autowired
  public MemberController(
      SecurityUtil securityUtil,
      WebSecurityProperties webSecurityProperties,
      HttpServletResponse response) {
    this.securityUtil = securityUtil;
    this.webSecurityProperties = webSecurityProperties;
    this.response = response;
  }

  @RequestMapping(value = "/test/memberInfo", method = RequestMethod.GET)
  public Object memberInfo() {
    return this.securityUtil
        .getOptMember()
        .map(
            member -> {
              Map<String, Object> returner = new HashMap<>();
              returner.put("id", member.getId());
              returner.put(
                  "roles", member.getRoles().stream().map(Role::getCode).collect(joining(",")));
              return (Object) returner;
            })
        .orElse("No user logged in");
  }

  @RequestMapping(value = "/test/loginUser/{user}", method = RequestMethod.POST)
  public boolean loginUser(@PathVariable String user) {
    Cookie cookie = new Cookie(webSecurityProperties.getCookieName(), user);
    cookie.setPath("/");
    response.addCookie(cookie);
    return true;
  }
}
