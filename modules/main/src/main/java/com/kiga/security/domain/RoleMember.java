package com.kiga.security.domain;

import com.kiga.db.KigaEntityModel;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by rainerh on 22.11.16.
 *
 * <p>Helper entity only required to create m:n fixtures.</p>
 */
@Entity
@Table(name = "Group_Members")
public class RoleMember extends KigaEntityModel {
  @ManyToOne
  @JoinColumn(name = "GroupID", referencedColumnName = "id")
  private Role role;

  @ManyToOne
  @JoinColumn(name = "MemberID", referencedColumnName = "id")
  private Member member;

  public RoleMember() {
    this.setClassName("Group_Members");
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }
}
