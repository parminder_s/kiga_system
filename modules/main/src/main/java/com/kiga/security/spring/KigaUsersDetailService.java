package com.kiga.security.spring;

import com.kiga.security.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * no Constructor because otherwise I get a Exception: java.lang.NoSuchMethodException:
 * com.kiga.security.spring.KigaUsersDetailService.<init>()
 */
@Service
public class KigaUsersDetailService implements UserDetailsService {
  private final MemberRepository memberRepository;

  @Autowired
  public KigaUsersDetailService(MemberRepository memberRepository) {
    this.memberRepository = memberRepository;
  }

  @Override
  public KigaUser loadUserByUsername(String username) throws UsernameNotFoundException {
    KigaUser kigaUser = new KigaUser();
    kigaUser.setMember(memberRepository.findWithRoleByEmail(username));
    return kigaUser;
  }

  public KigaUser loadUserById(Long memberId) throws UsernameNotFoundException {
    KigaUser kigaUser = new KigaUser();
    kigaUser.setMember(memberRepository.findWithRoleById(memberId));
    return kigaUser;
  }
}
