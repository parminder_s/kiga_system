package com.kiga.security.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.s3.domain.S3Image;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseWorkflow;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Created by rainerh on 30.05.16.
 */
@Entity
@Table(name = "Member")
public class Member extends KigaEntityModel {
  private String firstname;
  @Column(name = "Surname")
  private String lastname;
  private String email;
  private String rememberLoginToken;
  private String locale;
  private String country;
  private String crmId;
  private String password;
  private String customerId;

  @ManyToMany
  @JoinTable(
    name = "Group_Members",
    joinColumns = @JoinColumn(name = "MemberID"),
    inverseJoinColumns = @JoinColumn(name = "GroupID")
  )
  private List<Role> roles;

  @OneToMany(mappedBy = "customer")
  private List<Purchase> purchases;

  @OneToMany(mappedBy = "member")
  private List<PurchaseWorkflow> purchaseWorkflows;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "s3AvatarID", referencedColumnName = "id")
  private S3Image s3Avatar;

  public Member() {
    this.setClassName("Member");
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getRememberLoginToken() {
    return rememberLoginToken;
  }

  public void setRememberLoginToken(String rememberLoginToken) {
    this.rememberLoginToken = rememberLoginToken;
  }

  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCrmId() {
    return crmId;
  }

  public void setCrmId(String crmId) {
    this.crmId = crmId;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public List<Role> getRoles() {
    return roles;
  }

  public void setRoles(List<Role> roles) {
    this.roles = roles;
  }

  public List<Purchase> getPurchases() {
    return purchases;
  }

  public void setPurchases(List<Purchase> purchases) {
    this.purchases = purchases;
  }

  public List<PurchaseWorkflow> getPurchaseWorkflows() {
    return purchaseWorkflows;
  }

  public void setPurchaseWorkflows(List<PurchaseWorkflow> purchaseWorkflows) {
    this.purchaseWorkflows = purchaseWorkflows;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  private String nickname;

  public S3Image getS3Avatar() {
    return s3Avatar;
  }

  public void setS3Avatar(S3Image s3Avatar) {
    this.s3Avatar = s3Avatar;
  }
}
