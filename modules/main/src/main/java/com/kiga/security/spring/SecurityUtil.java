package com.kiga.security.spring;

import static java.util.Optional.ofNullable;

import com.kiga.security.domain.Member;
import java.util.Optional;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityUtil {
  /** returns an optional for a member that can also be anonymous. */
  public Optional<Member> getOptMember() {
    return ofNullable(getKigaAuthentication().getUser()).map(KigaUser::getMember);
  }

  /**
   * In this case the consumer forces a null pointer exception, when the user is anonymous. Makes
   * only sense if it is used in controllers where Spring Security already ensures a valid member
   * before the method is executed.
   */
  public Member getMember() {
    return getOptMember().get();
  }

  public String getSessionId() {
    return getKigaAuthentication().getSessionId();
  }

  public boolean hasRole(String role) {
    return getKigaAuthentication().getUser().hasRole(role);
  }

  private KigaAuthentication getKigaAuthentication() {
    return (KigaAuthentication) SecurityContextHolder.getContext().getAuthentication();
  }
}
