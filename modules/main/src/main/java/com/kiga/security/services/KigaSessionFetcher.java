package com.kiga.security.services;

import com.kiga.security.domain.KigaSession;
import java.io.IOException;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public interface KigaSessionFetcher {
  /** finds the kigaSession by the value of the session Token (cur: PHPSESSID). */
  Optional<KigaSession> findBySessionId(String sessionId) throws IOException;
}
