package com.kiga.security.spring;

import com.kiga.web.security.KigaRoles;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

@Service
public class KigaPermissionEvaluator implements PermissionEvaluator {
  private Map<String, PermissionEvaluator> evaluatorMap;

  public KigaPermissionEvaluator() {
    evaluatorMap = new HashMap<>();
  }

  public void addEvaluator(String targetType, PermissionEvaluator evaluator) {
    evaluatorMap.put(targetType, evaluator);
  }

  @Override
  public boolean hasPermission(
      Authentication authentication, Object targetDomainObject, Object permission) {
    if (authentication
        .getAuthorities()
        .stream()
        .map(GrantedAuthority::getAuthority)
        .anyMatch(KigaRoles.ADMIN::equals)) {
      return true;
    }

    // todo check how to do that with the defaultSecurityService
    return false;
  }

  @Override
  public boolean hasPermission(
      Authentication authentication, Serializable targetId, String targetType, Object permission) {
    if (targetId == null) {
      return true;
    }
    if (authentication
        .getAuthorities()
        .stream()
        .map(GrantedAuthority::getAuthority)
        .anyMatch(KigaRoles.ADMIN::equals)) {
      return false;
    }
    if (!evaluatorMap.containsKey(targetType)) {
      throw new IllegalArgumentException("Missing Permission Evaluator: " + targetType);
    }
    return evaluatorMap
        .get(targetType)
        .hasPermission(authentication, targetId, targetType, permission);
  }
}
