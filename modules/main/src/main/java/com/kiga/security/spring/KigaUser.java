package com.kiga.security.spring;

import static com.kiga.web.security.KigaRoles.availableRoles;

import com.kiga.security.domain.Member;
import com.kiga.security.domain.Role;
import java.util.Collection;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Getter
@Setter
public class KigaUser implements UserDetails {
  private Member member;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return member
        .getRoles()
        .stream()
        .map(Role::getCode)
        .map(code -> "ROLE_" + code)
        .map(SimpleGrantedAuthority::new)
        .collect(Collectors.toList());
  }

  @Override
  public String getPassword() {
    return "";
  }

  @Override
  public String getUsername() {
    return member.getEmail();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  public boolean hasRole(String role) {
    if (availableRoles.stream().noneMatch(role::equals)) {
      throw new RuntimeException("unavailable role: " + role);
    }

    return member.getRoles().stream().anyMatch(r -> r.getCode().equals(role));
  }
}
