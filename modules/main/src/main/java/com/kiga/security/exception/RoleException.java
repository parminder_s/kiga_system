package com.kiga.security.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by rainerh on 14.03.17.
 */
public class RoleException extends RuntimeException {
  @Getter
  @Setter
  private String role;

  public RoleException(String role) {
    this.role = role;
  }
}
