package com.kiga.security.spring;

import com.kiga.security.domain.KigaSession;
import com.kiga.security.services.KigaSessionFetcher;
import java.io.IOException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

@Service
public class KigaAuthenticationProvider implements AuthenticationProvider {

  private final KigaSessionFetcher kigaSessionFetcher;
  private final KigaUsersDetailService kigaUsersDetailService;

  @Autowired
  public KigaAuthenticationProvider(
      KigaSessionFetcher kigaSessionFetcher, KigaUsersDetailService kigaUsersDetailService) {
    this.kigaSessionFetcher = kigaSessionFetcher;
    this.kigaUsersDetailService = kigaUsersDetailService;
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    AnonymousKigaAuthentication kigaAuthentication = (AnonymousKigaAuthentication) authentication;
    try {
      Optional<KigaSession> session =
          kigaSessionFetcher.findBySessionId(kigaAuthentication.getSessionId());
      if (session.isPresent()) {
        AuthenticatedKigaAuthentication authenticated =
            new AuthenticatedKigaAuthentication(kigaAuthentication.getSessionId());
        KigaSession kigaSession = session.get();
        if (kigaSession.getLoggedInAs() != null) {
          KigaUser details =
              kigaUsersDetailService.loadUserById(kigaSession.getLoggedInAs().longValue());
          authenticated.setAuthenticated(true);
          authenticated.setUser(details);
          return authenticated;
        }
      }
      kigaAuthentication.setAuthenticated(true);
      return kigaAuthentication;
    } catch (IOException e) { // just for now!
      throw new RuntimeException("Error in authenticate!", e);
    }
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return KigaAuthentication.class.isAssignableFrom(authentication);
  }
}
