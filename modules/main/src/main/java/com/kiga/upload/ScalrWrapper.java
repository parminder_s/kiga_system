package com.kiga.upload;

import org.imgscalr.Scalr;
import org.springframework.stereotype.Service;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 * Created by rainerh on 27.08.16.
 */
@Service
public class ScalrWrapper {
  public BufferedImage resize(
    BufferedImage bufferedImage, Scalr.Method scalingMethod,
    Scalr.Mode resizeMode, int targetSize) {
    return Scalr.resize(bufferedImage, scalingMethod, resizeMode, targetSize);
  }

  public BufferedImage pad(BufferedImage bufferedImage, int padding, Color color) {
    return Scalr.pad(bufferedImage, padding, color);
  }

  public BufferedImage crop(
    BufferedImage bufferedImage, int coorX, int coorY, int width, int height) {
    return Scalr.crop(bufferedImage, coorX, coorY, width, height);
  }
}
