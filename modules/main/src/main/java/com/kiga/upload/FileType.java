package com.kiga.upload;

import com.kiga.upload.factory.AbstractFileUploader;
import com.kiga.upload.factory.AudioFileUploader;
import com.kiga.upload.factory.ExcelFileUploader;
import com.kiga.upload.factory.ImageAndPdfFileUploader;
import com.kiga.upload.factory.PowerPointFileUploader;
import com.kiga.upload.factory.VideoFileUploader;
import com.kiga.upload.factory.WordFileUploader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Created by bbs on 5/18/15.
 */
public enum FileType {
  AUDIO(AudioFileUploader.class, "audio/mp4", "audio/x-aac", "audio/aac", "audio/aacp",
    "audio/mpeg3", "audio/x-mpeg-3", "audio/ogg", "audio/wav", "audio/x-wav", "application/ogg"),
  EXCEL(ExcelFileUploader.class, "application/vnd.ms-excel",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "application/vnd.oasis.opendocument.spreadsheet"),
  SVG(ImageAndPdfFileUploader.class, "image/svg+xml"),
  GIF(ImageAndPdfFileUploader.class, "image/gif"),
  PNG(ImageAndPdfFileUploader.class, "image/png"),
  JPG(ImageAndPdfFileUploader.class, "image/jpeg", "image/pjpeg"),
  IMAGE(ImageAndPdfFileUploader.class, SVG, GIF, PNG, JPG),
  BITMAP_IMAGE(ImageAndPdfFileUploader.class, GIF, PNG, JPG),
  PDF(ImageAndPdfFileUploader.class, "application/pdf"),
  POWERPOINT(PowerPointFileUploader.class, "application/vnd.ms-powerpoint",
    "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    "application/vnd.oasis.opendocument.presentation"),
  VIDEO(VideoFileUploader.class, "application/x-troff-msvideo", "video/avi", "video/msvideo",
    "video/x-msvideo", "video/mp4", "video/mpeg", "video/x-mpeg"),
  WORD(WordFileUploader.class, "application/msword",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "application/vnd.oasis.opendocument.text", "application/x-tika-msoffice",
    "application/x-tika-ooxml");

  private Class<? extends AbstractFileUploader> uploaderClass;
  private List<String> contentTypes;
  private Function<Void, AbstractFileUploader> abstractFileUploaderConstructor;

  FileType(Class<? extends AbstractFileUploader> uploaderClass, String contentType) {
    this(uploaderClass, new String[]{contentType});
  }

  FileType(Class<? extends AbstractFileUploader> uploaderClass, String... contentTypes) {
    this.uploaderClass = uploaderClass;
    this.contentTypes = Arrays.asList(contentTypes);
  }

  FileType(Class<? extends AbstractFileUploader> uploaderClass, FileType... fileTypes) {
    List<String> contentTypes = new ArrayList<>();
    for (FileType fileType : fileTypes) {
      contentTypes.addAll(fileType.getContentTypes());
    }
    this.contentTypes = contentTypes;
    this.uploaderClass = uploaderClass;
  }

  public List<String> getContentTypes() {
    return contentTypes;
  }

  public AbstractFileUploader getNewUploaderInstance()
    throws IllegalAccessException, InstantiationException {
    return uploaderClass.newInstance();
  }

  public static boolean isBitmapImage(String contentType) {
    return BITMAP_IMAGE.getContentTypes().indexOf(contentType) != -1;
  }

  public static boolean isSvg(String contentType) {
    return SVG.getContentTypes().indexOf(contentType) != -1;
  }

  public static boolean isGif(String contentType) {
    return GIF.getContentTypes().indexOf(contentType) != -1;
  }

  public static boolean isPng(String contentType) {
    return PNG.getContentTypes().indexOf(contentType) != -1;
  }

  public static boolean isJpg(String contentType) {
    return JPG.getContentTypes().indexOf(contentType) != -1;
  }

  public static boolean isPdf(String contentType) {
    return PDF.getContentTypes().indexOf(contentType) != -1;
  }

  /**
   * Find proper fileType based on the content type.
   *
   * @param contentType content type which represents a particular FileType
   * @return file Type
   */
  public static FileType findFileTypeByContentType(String contentType) {
    List<FileType> fileTypes =
      Arrays.asList(AUDIO, EXCEL, SVG, GIF, PNG, JPG, PDF, POWERPOINT, VIDEO, WORD);

    for (FileType fileType : fileTypes) {
      if (fileType.getContentTypes().indexOf(contentType) != -1) {
        return fileType;
      }
    }

    throw new IllegalArgumentException("Wrong contentType provided: " + contentType);
  }
}
