package com.kiga.upload.exception;

/**
 * Created by robert on 28.05.17.
 */
public class FiltersApplicationException extends Exception {
  public FiltersApplicationException(Throwable cause) {
    super(cause);
  }
}
