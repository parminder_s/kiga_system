package com.kiga.upload.factory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.kiga.integration.amazon.s3.config.AmazonS3Parameters;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3File;
import com.kiga.s3.repository.ss.S3FileRepository;
import com.kiga.security.domain.Member;
import com.kiga.upload.ImageProcessor;
import com.kiga.upload.UploadParameters;
import com.kiga.upload.converter.MultipartFileUtil;
import com.kiga.upload.exception.FiltersApplicationException;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author robert.klodzinski@treevert.com
 * @since 5/26/15.
 */
public abstract class AbstractFileUploader {
  private static final Logger logger = LoggerFactory.getLogger(AbstractFileUploader.class);

  private UploadParameters uploadParameters;
  private MultipartFileUtil multipartFileUtil;
  private S3FileRepository s3FileRepository;
  private AmazonS3Parameters s3Config;
  private AmazonS3Client amazonS3Client;
  private ImageProcessor imageProcessor;
  private MultipartFile originalMultipartFile;
  private File originalFile;
  private S3File savedEntity;
  private String folder = "";
  private Member member;

  public UploadParameters getUploadParameters() {
    return uploadParameters;
  }

  public void setUploadParameters(UploadParameters uploadParameters) {
    this.uploadParameters = uploadParameters;
  }

  public S3FileRepository getS3FileRepository() {
    return s3FileRepository;
  }

  public void setS3FileRepository(S3FileRepository s3FileRepository) {
    this.s3FileRepository = s3FileRepository;
  }

  public AmazonS3Parameters getS3Config() {
    return s3Config;
  }

  public void setS3Config(AmazonS3Parameters s3Config) {
    this.s3Config = s3Config;
  }

  public ImageProcessor getImageProcessor() {
    return imageProcessor;
  }

  public void setImageProcessor(ImageProcessor imageProcessor) {
    this.imageProcessor = imageProcessor;
  }

  public MultipartFile getOriginalMultipartFile() {
    return originalMultipartFile;
  }

  public void setOriginalMultipartFile(MultipartFile originalMultipartFile) {
    this.originalMultipartFile = originalMultipartFile;
  }

  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public abstract String getEntityClassName();

  /**
   * Upload file.
   *
   * @return kiga s3 File
   * @throws Exception exception
   */
  public KigaS3File upload() throws Exception {
    String destinationPath =
        getFilePathGeneratedSanitized(originalMultipartFile.getOriginalFilename());
    originalFile = multipartFileUtil.upload(originalMultipartFile, destinationPath);
    applyFilters();
    return doUpload();
  }

  protected void applyFilters() throws FiltersApplicationException {}

  protected KigaS3File doUpload() throws IOException {
    uploadToAmazon(originalFile);
    KigaS3File kigaS3File = prepareEntity(null);
    storeInDatabase(kigaS3File);
    return kigaS3File;
  }

  protected final boolean uploadToAmazon(File file) {
    try {
      PutObjectRequest putObjectReqeust =
          new PutObjectRequest(
              s3Config.getBucketName(), getFilePathGeneratedSanitized(file.getName()), file);
      putObjectReqeust.setCannedAcl(uploadParameters.getCannedAccessControlList());
      getAmazonS3Client().putObject(putObjectReqeust);
      return true;
    } catch (AmazonServiceException exception) {
      logger.error(
          "The request made it to Amazon S3, but was rejected with an error response "
              + "for some reason.");
      throw exception;
    } catch (AmazonClientException exception) {
      logger.error(
          "The client encountered an internal error while trying to communicate with "
              + "S3, such as not being able to access the network.");
      throw exception;
    }
  }

  protected KigaS3File prepareEntity(KigaS3File s3File) throws IOException {
    Optional<KigaS3File> s3FileOption = Optional.ofNullable(s3File);
    KigaS3File kigaS3File = s3FileOption.orElseGet(KigaS3File::new);

    kigaS3File.setSize(originalMultipartFile.getBytes().length);
    kigaS3File.setBucket(s3Config.getBucketName());
    kigaS3File.setClassName(getEntityClassName());
    kigaS3File.setCreated(new Date());
    kigaS3File.setFolder(getFolderSanitized());
    kigaS3File.setName(getFileName());
    kigaS3File.setOwnerId(member.getId());
    kigaS3File.setUrl(getFileUrl());

    return kigaS3File;
  }

  protected final void storeInDatabase(S3File entity) {
    savedEntity = s3FileRepository.save(entity);
  }

  protected void cleanUp() {
    if (originalFile != null && originalFile.exists()) {
      originalFile.delete();
    }
    originalFile = null;
    originalMultipartFile = null;
  }

  protected final String getFilePathGeneratedSanitized(String fileName) {
    return StringUtils.strip(getFilePathGenerated(fileName), "/");
  }

  protected String getFileName() {
    return originalMultipartFile.getOriginalFilename();
  }

  private final String getFolderSanitized() {
    return StringUtils.strip(getFolder(), "/");
  }

  private String getFilePathGenerated(String fileName) {
    return getFolderSanitized() + "/" + fileName;
  }

  public AmazonS3Client getAmazonS3Client() {
    return amazonS3Client;
  }

  public void setAmazonS3Client(AmazonS3Client amazonS3Client) {
    this.amazonS3Client = amazonS3Client;
  }

  public String getFileUrl() {
    return s3Config.getBucketUrl() + "/" + getFilePathGeneratedSanitized(getFileName());
  }

  public String getFolder() {
    return folder;
  }

  public void setFolder(String folder) {
    this.folder = folder;
  }

  public MultipartFileUtil getMultipartFileUtil() {

    return multipartFileUtil;
  }

  public void setMultipartFileUtil(MultipartFileUtil multipartFileUtil) {
    this.multipartFileUtil = multipartFileUtil;
  }

  public S3File getSavedEntity() {
    return savedEntity;
  }

  public void setSavedEntity(S3File savedEntity) {
    this.savedEntity = savedEntity;
  }

  public File getOriginalFile() {

    return originalFile;
  }

  public void setOriginalFile(File originalFile) {
    this.originalFile = originalFile;
  }
}
