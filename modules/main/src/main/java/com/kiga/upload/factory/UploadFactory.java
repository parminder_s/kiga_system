package com.kiga.upload.factory;

import com.amazonaws.services.s3.AmazonS3Client;
import com.kiga.integration.amazon.s3.config.AmazonS3Parameters;
import com.kiga.s3.repository.ss.S3FileRepository;
import com.kiga.security.domain.Member;
import com.kiga.upload.FileType;
import com.kiga.upload.ImageProcessor;
import com.kiga.upload.UploadParameters;
import com.kiga.upload.converter.MultipartFileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author robert.klodzinski@treevert.com
 * @since 5/26/15.
 */
@Service
public class UploadFactory {
  private S3FileRepository s3FileRepository;
  private MultipartFileUtil multipartFileUtil;
  private AmazonS3Parameters s3Config;
  private UploadParameters uploadParameters;
  private ImageProcessor imageProcessor;
  private AmazonS3Client amazonS3Client;

  /**
   * Deps.
   *
   * @param s3FileRepository  s3 file repo
   * @param multipartFileUtil multipartfile util
   * @param s3Config          s3 config
   * @param uploadParameters  upload params
   * @param imageProcessor    img processor
   */
  @Autowired
  public UploadFactory(S3FileRepository s3FileRepository,
    MultipartFileUtil multipartFileUtil,
    AmazonS3Parameters s3Config, UploadParameters uploadParameters,
    ImageProcessor imageProcessor, AmazonS3Client amazonS3Client) {
    this.s3FileRepository = s3FileRepository;
    this.multipartFileUtil = multipartFileUtil;
    this.s3Config = s3Config;
    this.uploadParameters = uploadParameters;
    this.imageProcessor = imageProcessor;
    this.amazonS3Client = amazonS3Client;
  }

  /**
   * Get uploader instance.
   *
   * @param file   file
   * @param member member uploading the file
   * @return file Uploader instance
   * @throws Exception exception
   */
  public AbstractFileUploader getUploaderInstance(MultipartFile file, Member member)
    throws Exception {
    AbstractFileUploader fileUploader =
      FileType.findFileTypeByContentType(multipartFileUtil.getContentType(file))
        .getNewUploaderInstance();
    fileUploader.setMember(member);
    fileUploader.setOriginalMultipartFile(file);
    fileUploader.setS3Config(s3Config);
    fileUploader.setS3FileRepository(s3FileRepository);
    fileUploader.setUploadParameters(uploadParameters);
    fileUploader.setMultipartFileUtil(multipartFileUtil);
    fileUploader.setImageProcessor(imageProcessor);
    fileUploader.setAmazonS3Client(amazonS3Client);
    return fileUploader;
  }
}
