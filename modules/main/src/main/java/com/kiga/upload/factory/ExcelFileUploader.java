package com.kiga.upload.factory;

/**
 * @author robert.klodzinski@treevert.com
 * @since 5/31/15.
 */
public class ExcelFileUploader extends AbstractFileUploader {
  @Override
  public String getEntityClassName() {
    return "KigaS3File";
  }
}
