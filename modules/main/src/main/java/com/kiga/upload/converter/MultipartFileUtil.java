package com.kiga.upload.converter;

import com.kiga.util.TempFileCreator;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author robert.klodzinski@treevert.com
 * @since 5/26/15.
 */
@Service
public class MultipartFileUtil {
  public static final Logger log = LoggerFactory.getLogger(MultipartFileUtil.class);
  private final TempFileCreator tempFileCreator;

  @Autowired
  public MultipartFileUtil(TempFileCreator tempFileCreator) {
    this.tempFileCreator = tempFileCreator;
  }

  private String getProbingTmpFile() throws NoSuchAlgorithmException {
    return new BigInteger(
            1,
            MessageDigest.getInstance("MD5").digest(new RandomStringUtils().random(10).getBytes()))
        .toString();
  }

  public String getContentType(MultipartFile multipartFile) throws Exception {
    File file = upload(multipartFile, getProbingTmpFile());
    FileInputStream inputStream = new FileInputStream(file.getAbsolutePath());
    TikaConfig config = TikaConfig.getDefaultConfig();
    Detector detector = config.getDetector();
    TikaConfig.getDefaultConfig().getMimeRepository();

    TikaInputStream stream = TikaInputStream.get(inputStream);

    Metadata metadata = new Metadata();
    MediaType mediaType = detector.detect(stream, metadata);

    String contentType = mediaType.getType() + "/" + mediaType.getSubtype();
    stream.close();
    if (file.exists()) {
      file.delete();
    }
    return contentType;
  }

  public File upload(MultipartFile file, String path) throws Exception {
    byte[] bytes = file.getBytes();
    File downloadedFile = tempFileCreator.createFile(path);
    File parentDirectory = downloadedFile.getParentFile();

    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(downloadedFile));
    stream.write(bytes);
    stream.close();

    return downloadedFile;
  }
}
