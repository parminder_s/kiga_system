package com.kiga.upload;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author robert.klodzinski@treevert.com
 * @since 5/31/15.
 */
@Data
@Configuration
@ConfigurationProperties("uploadModule")
public class UploadParameters {
  private String temporaryDirectory;
  private String optipngExecutable;
  private String[] imageVariants;
  private String rasterizerExecutable;
  private String pdf2SvgExecutable;
  private CannedAccessControlList cannedAccessControlList;
}
