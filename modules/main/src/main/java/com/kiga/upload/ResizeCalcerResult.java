package com.kiga.upload;

import org.imgscalr.Scalr;

/**
 * Created by rainerh on 27.08.16.
 */
public class ResizeCalcerResult {
  private Scalr.Mode mode;

  private int targetSize;

  private int finalWidth;

  private int finalHeight;

  /**
   * constructor.
   */
  public ResizeCalcerResult(Scalr.Mode mode, int targetSize, int finalWidth, int finalHeight) {
    this.mode = mode;
    this.targetSize = targetSize;
    this.finalWidth = finalWidth;
    this.finalHeight = finalHeight;
  }

  public Scalr.Mode getMode() {
    return mode;
  }

  public int getTargetSize() {
    return targetSize;
  }

  public int getFinalWidth() {
    return finalWidth;
  }

  public int getFinalHeight() {
    return finalHeight;
  }
}
