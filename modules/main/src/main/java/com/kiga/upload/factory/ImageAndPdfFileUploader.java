package com.kiga.upload.factory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;
import com.kiga.upload.FileType;
import com.kiga.upload.exception.FiltersApplicationException;
import org.apache.commons.io.FileUtils;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;

/**
 * @author robert.klodzinski@treevert.com
 * @since 5/26/15.
 */
public class ImageAndPdfFileUploader extends AbstractFileUploader {
  private Map<String, File> imageVariantsFiles = new HashMap<>();
  private File svgFile;
  private File imageFile;

  public ImageAndPdfFileUploader() {
    cleanUp();
  }

  private BufferedImage getOriginalFileAsBufferedImage() throws IOException {
    return ImageIO.read(imageFile);
  }

  private boolean isPdf() throws IOException {
    Path path = Paths.get(getOriginalFile().getAbsolutePath());
    String contentType = Files.probeContentType(path);

    return FileType.isPdf(contentType);
  }

  private boolean isSvg() throws IOException {
    Path path = Paths.get(getOriginalFile().getAbsolutePath());
    String contentType = Files.probeContentType(path);
    return FileType.isSvg(contentType);
  }

  @Override
  protected void applyFilters() throws FiltersApplicationException {
    try {
      if (!isSvg() && !isPdf()) {
        imageFile = getOriginalFile();
      } else {
        if (isPdf()) {
          svgFile = getImageProcessor().pdfToSvg(getOriginalFile());
          imageFile = getImageProcessor().rasterizeSvg(svgFile);
        } else { // isSvg
          imageFile = getImageProcessor().rasterizeSvg(getOriginalFile());
        }
      }

      for (String imageVariantAsString : getUploadParameters().getImageVariants()) {
        String[] splitVariants = imageVariantAsString.split("x");
        Dimension imageVariant = new Dimension();

        imageVariant
          .setSize(Integer.parseInt(splitVariants[0]), Integer.parseInt(splitVariants[1]));

        String variantPath = getUploadParameters().getTemporaryDirectory() + File.separator
          + Double.valueOf(imageVariant.getWidth()).intValue() + "x"
          + Double.valueOf(imageVariant.getHeight()).intValue() + "_" + imageFile.getName();

        File fileVariant = new File(variantPath);
        FileUtils.copyFile(imageFile, fileVariant);

        getImageProcessor()
          .resize(ImageIO.read(fileVariant), Double.valueOf(imageVariant.getWidth()).intValue(),
            Double.valueOf(imageVariant.getHeight()).intValue());

        imageVariantsFiles.put(Double.valueOf(imageVariant.getWidth()).intValue() + "x"
          + Double.valueOf(imageVariant.getHeight()).intValue(), fileVariant);
      }
    } catch (Exception exception) {
      throw new FiltersApplicationException(exception);
    }
  }

  private Map<String, String> convertVariantFilesMapToJson() {
    Map<String, String> convertedVariants = new HashMap<>(imageVariantsFiles.size());
    imageVariantsFiles.entrySet().stream()
      .forEach(variant -> convertedVariants.put(variant.getKey(), variant.getValue().getName()));
    return convertedVariants;
  }

  @Override
  protected KigaS3File prepareEntity(KigaS3File s3File) throws IOException {
    BufferedImage bufferedImage = getOriginalFileAsBufferedImage();
    S3Image s3Image = (S3Image) super.prepareEntity(new S3Image());

    s3Image.setHeight(bufferedImage.getHeight());
    s3Image.setWidth(bufferedImage.getWidth());

    if (isPdf()) {
      s3Image.setUrl(
        getS3Config().getBucketUrl() + "/" + getFilePathGeneratedSanitized(imageFile.getName()));
      s3Image.setPdfUrl(getFileUrl());
    } else {
      s3Image.setPdfUrl(null);
    }

    s3Image.setCachedDataJson(convertVariantFilesMapToJson());

    return s3Image;
  }

  @Override
  protected KigaS3File doUpload() throws IOException {
    uploadToAmazon(getOriginalFile());
    if (svgFile != null) {
      uploadToAmazon(svgFile);
    }
    imageVariantsFiles.values().forEach(this::uploadToAmazon);
    KigaS3File kigaS3File = prepareEntity(null);
    storeInDatabase(kigaS3File);
    return kigaS3File;
  }

  @Override
  protected void cleanUp() {
    super.cleanUp();
    if (imageVariantsFiles != null) {
      imageVariantsFiles.values().stream().filter(File::exists).forEach(File::delete);
    }
    imageVariantsFiles = new HashMap<>();
    svgFile = null;
    imageFile = null;
  }

  @Override
  public String getEntityClassName() {
    return "KigaS3File";
  }

  @Override
  public String getFileName() {
    return getOriginalFile().getName();
  }
}
