package com.kiga.upload;

import com.kiga.s3.service.resize.exception.ResizeException;
import com.kiga.util.pdf2svg.Pdf2SvgWrapper;
import com.kiga.util.rasterizer.RasterizerWrapper;
import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by rainerh on 26.08.16.
 */
@Service
public class ImageProcessor {
  private ScalrWrapper scalrWrapper;
  private Pdf2SvgWrapper pdf2SvgWrapper;
  private RasterizerWrapper rasterizerWrapper;

  /**
   * Dependencies.
   *
   * @param scalrWrapper      scalrWrapper
   * @param pdf2SvgWrapper    pdf2svgWrapper
   * @param rasterizerWrapper rasterizerWrapper
   */
  @Autowired
  public ImageProcessor(ScalrWrapper scalrWrapper, Pdf2SvgWrapper pdf2SvgWrapper,
    RasterizerWrapper rasterizerWrapper) {
    this.scalrWrapper = scalrWrapper;
    this.pdf2SvgWrapper = pdf2SvgWrapper;
    this.rasterizerWrapper = rasterizerWrapper;
  }

  /**
   * Converts PDF to SVG.
   *
   * @param originalFile file to be converted
   * @return converted file
   * @throws Exception if errors occured
   */
  public File pdfToSvg(File originalFile) throws Exception {
    String folder = FilenameUtils.getFullPathNoEndSeparator(originalFile.getAbsolutePath());
    String baseFilename = FilenameUtils.getBaseName(originalFile.getAbsolutePath());
    String outFilePath = folder + "/" + baseFilename + ".svg";
    return pdf2SvgWrapper.convert(originalFile, outFilePath);
  }

  /**
   * Rasterizes SVG file.
   *
   * @param file file to be rasterized
   * @return rasterized file
   * @throws Exception if errors occured
   */
  public File rasterizeSvg(File file) throws Exception {
    String folder = FilenameUtils.getFullPathNoEndSeparator(file.getAbsolutePath());
    String baseFilename = FilenameUtils.getBaseName(file.getAbsolutePath());
    String outFilePath = folder + "/" + baseFilename + ".png";
    return rasterizerWrapper.rasterize(file, outFilePath, "image/png");
  }

  /**
   * resizes the image with best quality possible.
   */
  public BufferedImage resize(BufferedImage image, int width, int height) {
    if (image == null) {
      throw new IllegalArgumentException("Image cannot be null");
    } else if (width == 0 && height == 0) {
      throw new IllegalArgumentException("Width and height cannot be null");
    }

    ResizeCalcerResult result = ResizeCalcer.calc(image, width, height);
    int finalWidth = result.getFinalWidth();
    int finalHeight = result.getFinalHeight();

    BufferedImage returner = scalrWrapper.resize(
      image, Scalr.Method.ULTRA_QUALITY, result.getMode(), result.getTargetSize());

    int sidesWidthDifference = 0;
    if (returner.getWidth() < finalWidth) {
      sidesWidthDifference = finalWidth - returner.getWidth();
    } else if (returner.getHeight() < finalHeight) {
      sidesWidthDifference = finalHeight - returner.getHeight();
    }

    if (sidesWidthDifference > 0) {
      returner = scalrWrapper.pad(returner, sidesWidthDifference, Color.WHITE);
      returner = scalrWrapper.crop(
        returner, sidesWidthDifference, sidesWidthDifference / 2, finalWidth, finalHeight);
    }

    if (returner.getWidth() != width || returner.getHeight() != height) {
      throw new ResizeException();
    }

    return returner;
  }
}
