package com.kiga.upload;

import org.imgscalr.Scalr;

import java.awt.image.BufferedImage;

/**
 * Created by rainerh on 27.08.16.
 */
public class ResizeCalcer {
  /**
   * calculates the resizing mode and the final size by sticking to the original ratio.
   */
  public static ResizeCalcerResult calc(BufferedImage image, int width, int height) {
    double outRatio = (double)width / height;
    double inRatio = (double)image.getWidth() / image.getHeight();

    int finalWidth = width;
    int finalHeight = height;

    if (width == 0) {
      finalWidth = new Double(height * inRatio).intValue();
    }

    if (height == 0) {
      finalHeight = new Double(width / inRatio).intValue();
    }

    if (inRatio < outRatio) {
      return new ResizeCalcerResult(Scalr.Mode.FIT_TO_HEIGHT, finalHeight, finalWidth, finalHeight);
    } else {
      return new ResizeCalcerResult(Scalr.Mode.FIT_TO_WIDTH, finalWidth, finalWidth, finalHeight);
    }
  }
}
