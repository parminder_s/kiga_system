package com.kiga.db;

import org.hibernate.dialect.HSQLDialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StandardBasicTypes;

/**
 * @author bbs
 * @since 7/13/16.
 */
public class HsqlDialect extends HSQLDialect {
  /**
   * Create new hsqldb dialect.
   */
  public HsqlDialect() {
    super();
    registerFunction("in_group", new SQLFunctionTemplate(StandardBasicTypes.BOOLEAN,
      "(BITAND(?1, CAST((1 * POWER(2, ?2)) as INTEGER)) != 0)"));
  }
}
