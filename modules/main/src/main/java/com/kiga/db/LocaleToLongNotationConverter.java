package com.kiga.db;

import com.kiga.main.locale.Locale;
import com.kiga.main.locale.LocaleConverter;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.map.HashedMap;

import java.util.Map;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by rainerh on 08.11.16.
 */
@Converter
public class LocaleToLongNotationConverter implements AttributeConverter<Locale, String> {
  @Override
  public String convertToDatabaseColumn(Locale attribute) {
    return LocaleConverter.toLong(attribute);
  }

  @Override
  public Locale convertToEntityAttribute(String dbData) {
    return LocaleConverter.toLocale(dbData);
  }
}
