package com.kiga.db;

import com.kiga.content.domain.ss.property.LayoutType;

import javax.persistence.AttributeConverter;

/**
 * Created by peter on 22.02.17.
 */
public class LayoutTypeConverter implements AttributeConverter<LayoutType, Integer> {
  @Override
  public Integer convertToDatabaseColumn(LayoutType attribute) {
    switch (attribute) {
      case STATIC_OLD: return 1;
      case DYNAMIC: return 2;
      case A4_PDF: return 3;
      default: return null;
    }
  }

  @Override
  public LayoutType convertToEntityAttribute(Integer dbData) {
    switch (dbData) {
      case 1: return LayoutType.STATIC_OLD;
      case 2: return LayoutType.DYNAMIC;
      case 3: return LayoutType.A4_PDF;
      default: throw new IllegalArgumentException("unknowen layout type: " + dbData);
    }
  }
}
