package com.kiga.db.fixture;

import static com.kiga.util.NowService.KIGAZONE;

import com.kiga.main.locale.Locale;
import com.kiga.main.locale.LocaleConverter;
import java.time.Instant;
import java.time.LocalDate;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.NodeId;
import org.yaml.snakeyaml.nodes.ScalarNode;

/**
 * Created by rainerh on 08.11.16.
 *
 * <p>Deserializer for YAML format.
 */
public class CustomYamlConstructor extends Constructor {
  public CustomYamlConstructor() {
    yamlClassConstructors.put(NodeId.scalar, new CustomObjectsConstructor());
  }

  public class CustomObjectsConstructor extends Constructor.ConstructScalar {
    @Override
    public Object construct(Node nnode) {
      if (nnode.getType().equals(Locale.class)) {
        return LocaleConverter.toLocale(((ScalarNode) nnode).getValue());
      } else if (nnode.getType().equals(Instant.class)) {
        String value = ((ScalarNode) nnode).getValue();
        return LocalDate.parse(value).atStartOfDay(KIGAZONE).toInstant();
      }
      return super.construct(nnode);
    }
  }
}
