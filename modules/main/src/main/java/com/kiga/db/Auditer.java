package com.kiga.db;

import com.kiga.security.spring.KigaUser;
import java.util.Date;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

public abstract class Auditer<
    E extends KigaEntityModel,
    A extends KigaEntityModel & KigaAuditable<E>,
    R extends CrudRepository> {
  private Class<R> repoClass;

  public Auditer(Class<R> repo) {
    this.repoClass = repo;
  }

  protected abstract A map(E entity);

  protected void peristToArchive(E object) {
    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    A archive = this.map(object);
    if (SecurityContextHolder.getContext().getAuthentication() != null) {
      archive.setEditor(
          ((KigaUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
              .getMember());
    }
    archive.setId(null);
    archive.setLastEdited(new Date());
    archive.setOriginal(object);
    BeanHelper.getBean(repoClass).save(archive);
  }
}
