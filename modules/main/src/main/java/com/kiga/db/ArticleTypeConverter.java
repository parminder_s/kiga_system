package com.kiga.db;

import com.kiga.content.domain.ss.property.ArticleType;

import javax.persistence.AttributeConverter;

/**
 * Created by robert on 23.04.17.
 */
public class ArticleTypeConverter implements AttributeConverter<ArticleType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(ArticleType attribute) {
    if (attribute != null) {
      return attribute.getDatabaseValue();
    }
    return null;
  }

  @Override
  public ArticleType convertToEntityAttribute(Integer dbData) {
    return ArticleType.valueOf(dbData);
  }
}
