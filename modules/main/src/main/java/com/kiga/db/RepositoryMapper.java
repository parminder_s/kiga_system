package com.kiga.db;

import com.kiga.spec.Fetcher;
import com.kiga.spec.Persister;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 27.04.16.
 */
public class RepositoryMapper {
  public static <T> Fetcher<T> toFetcher(CrudRepository<T, Long> repository) {
    return new RepositoryFetcher<T>(repository);
  }

  public static <T> Persister toPersister(CrudRepository<T, Long> repository) {
    return new RepositoryPersister<>(repository);
  }
}
