package com.kiga.db;

import java.util.Date;

/**
 * Created by rainerh on 11.03.16.
 */
public interface KigaEntity {
  Long getId();

  void setId(Long id);

  String getClassName();

  void setClassName(String className);

  Date getCreated();

  void setCreated(Date created);

  Date getLastEdited();

  void setLastEdited(Date lastEdited);
}
