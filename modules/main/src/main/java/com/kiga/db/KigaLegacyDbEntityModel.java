package com.kiga.db;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Created by mfit on 04.10.16.
 */
public abstract class KigaLegacyDbEntityModel implements KigaEntity {

  @Override
  public String getClassName() {
    return this.getClass().getSimpleName();
  }

  @Override
  public void setClassName(String className) {

  }

  @Override
  public Date getCreated() {
    return null;
  }

  @Override
  public void setCreated(Date created) {

  }

  @Override
  public Date getLastEdited() {
    return null;
  }

  @Override
  public void setLastEdited(Date lastEdited) {

  }
}
