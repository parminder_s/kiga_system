package com.kiga.db;

import java.util.Date;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;

/** Created by rainerh on 09.03.16. */
@MappedSuperclass
public abstract class KigaEntityModelWithoutId implements KigaEntity {

  @NotNull private String className;
  private Date created = new Date();
  private Date lastEdited;

  @PrePersist
  private void changeDate() {
    this.lastEdited = new Date();
  }

  @Override
  public String getClassName() {
    return className;
  }

  @Override
  public void setClassName(String className) {
    this.className = className;
  }

  @Override
  public Date getCreated() {
    return created;
  }

  @Override
  public void setCreated(Date created) {
    this.created = created;
  }

  @Override
  public Date getLastEdited() {
    return lastEdited;
  }

  @Override
  public void setLastEdited(Date lastEdited) {
    this.lastEdited = lastEdited;
  }
}
