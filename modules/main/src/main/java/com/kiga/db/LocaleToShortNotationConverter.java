package com.kiga.db;

import com.kiga.main.locale.Locale;
import com.kiga.main.locale.LocaleConverter;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.map.HashedMap;

import java.util.Map;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by rainerh on 08.11.16.
 */
@Converter
public class LocaleToShortNotationConverter implements AttributeConverter<Locale, String> {
  @Override
  public String convertToDatabaseColumn(Locale attribute) {
    if (attribute == null) {
      return null;
    } else {
      return LocaleConverter.toShort(attribute);
    }
  }

  @Override
  public Locale convertToEntityAttribute(String dbData) {
    if (dbData == null) {
      return null;
    } else {
      return LocaleConverter.toLocale(dbData);
    }
  }
}
