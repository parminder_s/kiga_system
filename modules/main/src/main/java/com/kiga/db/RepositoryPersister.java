package com.kiga.db;

import com.kiga.spec.Persister;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

/**
 * Created by rainerh on 27.04.16.
 */
public class RepositoryPersister<T> implements Persister<T> {
  private CrudRepository<T, Long> repository;

  public RepositoryPersister(CrudRepository<T, Long> repository) {
    this.repository = repository;
  }

  @Override
  public T persist(T entity) {
    return this.repository.save(entity);
  }
}
