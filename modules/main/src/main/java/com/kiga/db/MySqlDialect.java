package com.kiga.db;

import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StandardBasicTypes;

/**
 * @author bbs
 * @since 7/13/16.
 */
public class MySqlDialect extends org.hibernate.dialect.MySQLDialect {
  /**
   * Create new mysql dialect.
   */
  public MySqlDialect() {
    super();
    registerFunction("in_group",
      new SQLFunctionTemplate(StandardBasicTypes.BOOLEAN, "(?1 & ?2 != 0)"));
  }
}
