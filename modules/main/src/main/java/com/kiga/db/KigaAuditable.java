package com.kiga.db;

import com.kiga.security.domain.Member;

public interface KigaAuditable<T extends KigaEntityModel> {
  Member getEditor();

  void setEditor(Member entry);

  T getOriginal();

  void setOriginal(T original);
}
