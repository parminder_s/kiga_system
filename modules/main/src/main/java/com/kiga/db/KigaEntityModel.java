package com.kiga.db;

import com.kiga.db.fixture.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** Created by peter on 16.11.16. */
@MappedSuperclass
public abstract class KigaEntityModel extends KigaEntityModelWithoutId implements Entity {
  @Id
  @Column(name = "ID")
  @GeneratedValue
  private Long id;

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }
}
