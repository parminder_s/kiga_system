package com.kiga.db;

import com.kiga.InvalidConfigurationException;
import com.kiga.accounting.domain.AcquirerTransaction;
import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.dunning.domain.DunningLevel;
import com.kiga.accounting.dunning.repository.DunningLevelRepository;
import com.kiga.accounting.repository.AcquirerTransactionRepository;
import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.accounting.transactions.domain.PaymentTransaction;
import com.kiga.accounting.transactions.repository.PaymentTransactionRepository;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.property.SortStrategyConverter;
import com.kiga.content.repository.ss.KigaPageImageRepository;
import com.kiga.db.fixture.CustomYamlConstructor;
import com.kiga.db.fixture.DefaultFixtureParser;
import com.kiga.db.fixture.FixtureParser;
import com.kiga.favourites.domain.IdeaFavourite;
import com.kiga.favourites.repository.FavouriteKigaIdeaRepository;
import com.kiga.favourites.repository.IdeaFavouritesRepository;
import com.kiga.forum.domain.Forum;
import com.kiga.forum.repository.ForumRepository;
import com.kiga.homepage.domain.HomePageElementConverter;
import com.kiga.homepage.repository.draft.HomePageElementRepositoryDraft;
import com.kiga.homepage.repository.live.HomePageElementRepositoryLive;
import com.kiga.ideas.downloadable.repository.DownloadableIdeasRepositoryLive;
import com.kiga.ideas.member.domain.MemberIdea;
import com.kiga.ideas.member.repository.MemberIdeaRepository;
import com.kiga.kga.domain.KgaChild_Versions;
import com.kiga.kga.repository.KgaChildArchiveRepository;
import com.kiga.kga.survey.domain.Survey;
import com.kiga.kga.survey.repository.SurveyRepository;
import com.kiga.logging.domain.LinkLog;
import com.kiga.logging.domain.PrintLog;
import com.kiga.logging.domain.SearchTerm;
import com.kiga.logging.repositories.LinkLogRepository;
import com.kiga.logging.repositories.PrintLogRepository;
import com.kiga.logging.repositories.SearchTermRepository;
import com.kiga.mail.domain.EMailLog;
import com.kiga.mail.repository.EMailLogRepository;
import com.kiga.payment.domain.Payment;
import com.kiga.payment.repository.PaymentRepository;
import com.kiga.plan.Plan;
import com.kiga.plan.PlanRepository;
import com.kiga.print.validation.db.model.IdeaValidationReport;
import com.kiga.print.validation.db.repository.IdeaErrorRepository;
import com.kiga.print.validation.domain.IdeaError;
import com.kiga.registration.domain.RegistrationData;
import com.kiga.registration.repository.RegistrationDataRepository;
import com.kiga.s3.domain.S3File;
import com.kiga.s3.repository.ss.S3FileRepository;
import com.kiga.security.domain.Member;
import com.kiga.security.repository.MemberRepository;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.shopcontent.domain.draft.ProductPageDraft;
import com.kiga.shopcontent.domain.live.ProductPageLive;
import com.kiga.shopcontent.repository.draft.ProductPageRepositoryDraft;
import com.kiga.shopcontent.repository.live.ProductPageRepositoryLive;
import com.kiga.survey.domain.Questionnaire;
import com.kiga.survey.repository.QuestionnaireRepository;
import com.kiga.translation.domain.CustomLanguageTranslation;
import com.kiga.translation.repository.CustomLanguageTranslationRepository;
import com.zaxxer.hikari.HikariDataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import javax.sql.DataSource;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

@Configuration
@EnableAutoConfiguration
@ConfigurationProperties("datasource.ss")
@EnableJpaRepositories(
    basePackageClasses = {
      AcquirerTransactionRepository.class,
      CustomLanguageTranslationRepository.class,
      ForumRepository.class,
      HomePageElementRepositoryLive.class,
      HomePageElementRepositoryDraft.class,
      IdeaErrorRepository.class,
      IdeaFavouritesRepository.class,
      IdeaValidationReport.class,
      KigaPageImageRepository.class,
      LinkLogRepository.class,
      MemberRepository.class,
      MemberIdeaRepository.class,
      PaymentRepository.class,
      PrintLogRepository.class,
      ProductPageRepositoryDraft.class,
      ProductPageRepositoryLive.class,
      PurchaseRepository.class,
      QuestionnaireRepository.class,
      RegistrationDataRepository.class,
      S3FileRepository.class,
      SearchTermRepository.class,
      PrintLogRepository.class,
      IdeaErrorRepository.class,
      HomePageElementRepositoryLive.class,
      LinkLogRepository.class,
      HomePageElementRepositoryDraft.class,
      FavouriteKigaIdeaRepository.class,
      IdeaFavouritesRepository.class,
      InvoiceRepository.class,
      DunningLevelRepository.class,
      PaymentTransactionRepository.class,
      DownloadableIdeasRepositoryLive.class,
      PlanRepository.class,
      KgaChildArchiveRepository.class,
      EMailLogRepository.class,
      SurveyRepository.class
    },
    entityManagerFactoryRef = "emfSs",
    transactionManagerRef = "txSs")
@Log4j2
@Data
public class SSDatabaseConfig {
  private String jdbcUrl;
  private String username;
  private String password;
  private String hbm2ddl;

  @Bean(name = "txSs")
  @Primary
  public JpaTransactionManager getTxSs(
      @Qualifier("emfSs") LocalContainerEntityManagerFactoryBean emf) {
    return new JpaTransactionManager(emf.getObject());
  }

  @Bean(name = "dsSs")
  @Primary
  public DataSource getDataSource() {
    return DataSourceBuilder.create()
        .type(HikariDataSource.class)
        .url(jdbcUrl)
        .username(username)
        .password(password)
        .build();
  }

  @Primary
  @Bean(name = "emfSs")
  public LocalContainerEntityManagerFactoryBean getEmf(EntityManagerFactoryBuilder builder)
      throws InvalidConfigurationException {
    String actualHbm2Ddl = Optional.ofNullable(hbm2ddl).orElse("validate");

    if (DdlRule.allowDdl(jdbcUrl)
        && Stream.of("validate", "none").anyMatch(actualHbm2Ddl::equals)) {
      throw new InvalidConfigurationException(
          "Invalid configuration param: on non-hsqldb databases hbm2ddl property must be 'validate or none'. Current value is "
              + actualHbm2Ddl);
    }

    String dialect = "";
    if (DdlRule.isHsqlDb(jdbcUrl)) {
      dialect = "com.kiga.db.HsqlDialect";
    } else if (DdlRule.isMysqlDb(jdbcUrl)) {
      dialect = "com.kiga.db.MySqlDialect";
    } else {
      throw new InvalidConfigurationException(
          "Invalid configuration param: No dialect found or unknown jdbc url: " + jdbcUrl);
    }

    Map<String, String> properties = new HashMap<>();
    properties.put("hibernate.hbm2ddl.auto", hbm2ddl);
    properties.put("hibernate.ejb.naming_strategy", "com.kiga.db.KigaNamingStrategy");
    properties.put("hibernate.dialect", dialect);

    return builder
        .dataSource(getDataSource())
        .properties(properties)
        .packages(
            AcquirerTransaction.class,
            CustomLanguageTranslation.class,
            EMailLog.class,
            Forum.class,
            HomePageElementConverter.class,
            IdeaError.class,
            IdeaValidationReport.class,
            IdeaFavourite.class,
            KigaPageImage.class,
            LinkLog.class,
            LocaleToLongNotationConverter.class,
            Member.class,
            MemberIdea.class,
            Payment.class,
            PrintLog.class,
            ProductPageDraft.class,
            ProductPageLive.class,
            Purchase.class,
            Questionnaire.class,
            RegistrationData.class,
            S3File.class,
            SearchTerm.class,
            SortStrategyConverter.class,
            Invoice.class,
            DunningLevel.class,
            PaymentTransaction.class,
            Plan.class,
            KgaChild_Versions.class,
            Survey.class)
        .persistenceUnit("ss")
        .build();
  }

  @Bean
  public FixtureParser getFixtureParser() {
    return new DefaultFixtureParser(new CustomYamlConstructor());
  }
}
