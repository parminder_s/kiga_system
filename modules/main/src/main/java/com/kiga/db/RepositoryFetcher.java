package com.kiga.db;

import com.kiga.spec.Fetcher;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 27.04.16.
 */
public class RepositoryFetcher<T> implements Fetcher<T> {
  private CrudRepository<T, Long> repository;

  public RepositoryFetcher(CrudRepository<T, Long> repository) {
    this.repository = repository;
  }

  @Override
  public T find(Long id) {
    return this.repository.findOne(id);
  }
}
