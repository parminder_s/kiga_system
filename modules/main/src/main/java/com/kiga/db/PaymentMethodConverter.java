package com.kiga.db;

import static com.kiga.main.paymentmethod.PaymentMethod.CREDITCARD;
import static com.kiga.main.paymentmethod.PaymentMethod.INVOICE;
import static com.kiga.main.paymentmethod.PaymentMethod.PAYPAL;
import static com.kiga.main.paymentmethod.PaymentMethod.SOFORT;

import com.kiga.main.paymentmethod.PaymentMethod;

import java.util.Objects;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by peter on 04.01.17.
 */
@Converter
public class PaymentMethodConverter implements AttributeConverter<PaymentMethod, String> {
  @Override
  public String convertToDatabaseColumn(PaymentMethod attribute) {
    if (attribute == null) {
      return null;
    } else {
      switch (attribute) {
        case INVOICE:
          return "INVOICE";
        case CREDITCARD:
          return "MP-CC";
        case PAYPAL:
          return "MP-PAYPAL";
        case SOFORT:
          return "SOFORT";
        default:
          return null;
      }
    }
  }

  @Override
  public PaymentMethod convertToEntityAttribute(String dbData) {
    if (Objects.equals(dbData, "INVOICE")) {
      return INVOICE;
    }
    if (Objects.equals(dbData, "MP-CC")) {
      return CREDITCARD;
    }
    if (Objects.equals(dbData, "MP-PAYPAL")) {
      return PAYPAL;
    }
    if (Objects.equals(dbData, "SOFORT")) {
      return SOFORT;
    }
    return null;
  }
}
