package com.kiga.db;

import com.kiga.spec.Remover;
import org.springframework.data.repository.CrudRepository;

public class RepositoryRemover<T> implements Remover<T> {
  private CrudRepository<T, Long> repository;

  public RepositoryRemover(CrudRepository<T, Long> repository) {
    this.repository = repository;
  }

  @Override
  public void remove(T entity) {
    this.repository.delete(entity);
  }
}
