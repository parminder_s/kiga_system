package com.kiga.spec;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by rainerh on 27.12.16.
 *
 * <p>Marks a class which implements Jobale as Job to be scheduled. The cron value is taken from
 * the configuration where the value of @Job indicates the configuration property.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Job {
  /** property name containing the schedule **/
  String cronProperty();
}
