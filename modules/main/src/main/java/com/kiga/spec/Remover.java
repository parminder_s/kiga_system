package com.kiga.spec;

/**
 * Like in {@link Persister} this is handy function that can be used in Services without the
 * requirement for dependency to a full Spring-Data Repository.
 */
@FunctionalInterface
public interface Remover<T> {
  void remove(T entity);
}
