package com.kiga.spec;

/**
 * Created by rainerh on 22.03.16.
 *
 * <p>The fetcher is primarily used to test methods without having a
 * dependency to Spring Data (repository) or any other retrieval service
 * from another external system.
 *
 * <p>Since in most cases we would require to lookup an entity by id, it just
 * has the single method find. In cases where more complex queries are
 * required one has to find other ways.
 */
public interface Fetcher<T> {
  T find(Long id);
}
