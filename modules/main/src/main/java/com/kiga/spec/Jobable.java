package com.kiga.spec;

/**
 * Created by rainerh on 18.10.16.
 */
public interface Jobable {
  void runJob();
}
