package com.kiga.shopcontent.viewmodel;

import lombok.Data;

/**
 * Created by asv on 30.11.16.
 */

@Data
public class PromotionResponse {
  private String ideaTeaser;
  private String ideaText;
  private String ideaImage;
  private String productPageUrl;
}
