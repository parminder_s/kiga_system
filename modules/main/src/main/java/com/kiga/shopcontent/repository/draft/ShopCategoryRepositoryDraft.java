package com.kiga.shopcontent.repository.draft;

import com.kiga.shopcontent.domain.draft.ShopCategoryDraft;
import com.kiga.shopcontent.repository.abstraction.ShopCategoryRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 9/6/16.
 */
public interface ShopCategoryRepositoryDraft extends ShopCategoryRepository,
  CrudRepository<ShopCategoryDraft, Long> {
}
