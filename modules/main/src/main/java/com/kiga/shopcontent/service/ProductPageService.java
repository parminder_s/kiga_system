package com.kiga.shopcontent.service;

import com.kiga.shop.ShopProperties;
import com.kiga.shopcontent.repository.ProductPageRepositoryProxy;
import com.kiga.shopcontent.viewmodel.ShopCategoryProductViewModel;
import com.kiga.shopcontent.viewmodel.ShopCategoryViewModel;
import com.kiga.shopcontent.viewmodel.converter.ShopCategoryProductToResponseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 9/6/16.
 */
@Service
public class ProductPageService {
  private ShopProperties shopProperties;
  private ProductPageRepositoryProxy productPageModel;
  private ShopCategoryProductToResponseConverter shopCategoryProductToResponseConverter;

  /**
   * Constructor for the service.
   *
   * @param shopProperties                         shopProperties
   * @param productPageModel                       productPageModel
   * @param shopCategoryProductToResponseConverter shopCategoryProductToResponseConverter
   */
  @Autowired
  public ProductPageService(ShopProperties shopProperties,
    ProductPageRepositoryProxy productPageModel,
    ShopCategoryProductToResponseConverter shopCategoryProductToResponseConverter) {
    this.shopProperties = shopProperties;
    this.productPageModel = productPageModel;
    this.shopCategoryProductToResponseConverter = shopCategoryProductToResponseConverter;
  }

  /**
   * returns the ShopCategoryViewModel.
   */
  public List<ShopCategoryProductViewModel> getProductsByCategory(ShopCategoryViewModel category,
    String countryCode) {
    return productPageModel
      .getProductsByCategoryId(category.getId(), countryCode,
        new PageRequest(0, shopProperties.getAmountPerCategory())).stream()
      .map(productPage ->
        shopCategoryProductToResponseConverter.convertToViewModel(productPage, countryCode))
      .collect(Collectors.toList());
  }
}
