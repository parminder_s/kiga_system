package com.kiga.shopcontent.repository;

import com.kiga.content.repository.ss.AbstractRepositoryProxy;
import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.shopcontent.domain.ShopCategory;
import com.kiga.shopcontent.repository.abstraction.ShopCategoryRepository;

import java.util.List;

/**
 * @author bbs
 * @since 9/6/16.
 */
public class ShopCategoryRepositoryProxy extends AbstractRepositoryProxy<ShopCategoryRepository>
  implements ShopCategoryRepository {

  /**
   * Constructor for the Model.
   *
   * @param liveRepository    live repository
   * @param draftRepository   draft repository
   * @param repositoryContext repository context holder
   */
  public ShopCategoryRepositoryProxy(ShopCategoryRepository liveRepository,
    ShopCategoryRepository draftRepository,
    RepositoryContext repositoryContext) {
    super(liveRepository, draftRepository, repositoryContext);
  }

  @Override
  public List<ShopCategory> getSubcategories(Long id) {
    return getRepository().getSubcategories(id);
  }

  @Override
  public ShopCategory findOne(Long id) {
    return getRepository().findOne(id);
  }
}
