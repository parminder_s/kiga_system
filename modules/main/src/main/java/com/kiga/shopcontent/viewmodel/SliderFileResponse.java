package com.kiga.shopcontent.viewmodel;

import lombok.Data;

/**
 * Created by asv on 14.11.16.
 */
@Data
public class SliderFileResponse {
  private long id;
  private String fileType;
  private String imageUrl;
  private String sliderImageUrl;
  private String videoUrl;
  private int sort;
}
