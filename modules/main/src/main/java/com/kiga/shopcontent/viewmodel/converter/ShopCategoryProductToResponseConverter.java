package com.kiga.shopcontent.viewmodel.converter;

import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.property.ImageSize;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.shop.ShopProperties;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shopcontent.domain.ProductPage;
import com.kiga.shopcontent.viewmodel.ShopCategoryProductViewModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

/**
 * @author bbs
 * @since 9/8/16.
 */
@Service
public class ShopCategoryProductToResponseConverter {
  private Logger logger = LoggerFactory.getLogger(getClass());
  private S3ImageResizeService s3ImageResizeService;
  private ShopProperties shopProperties;
  private SiteTreeUrlGenerator siteTreeUrlGenerator;

  /**
   * Constructor for converter.
   *
   * @param s3ImageResizeService s3ImageResizeService
   * @param shopProperties       shopProperties
   * @param siteTreeUrlGenerator siteTreeUrlGenerator
   */
  @Autowired
  public ShopCategoryProductToResponseConverter(
    S3ImageResizeService s3ImageResizeService, ShopProperties shopProperties,
    SiteTreeUrlGenerator siteTreeUrlGenerator) {
    this.s3ImageResizeService = s3ImageResizeService;
    this.shopProperties = shopProperties;
    this.siteTreeUrlGenerator = siteTreeUrlGenerator;
  }

  /**
   * converts to ViewShopCategoryProductViewModel.
   */
  public ShopCategoryProductViewModel convertToViewModel(
    ProductPage productPage, String countryCode) {
    ShopCategoryProductViewModel productPageResponse = new ShopCategoryProductViewModel();

    productPageResponse.setId(productPage.getId());
    productPageResponse.setClassName(productPage.getClassName());
    productPageResponse.setTitle(productPage.getTitle());
    productPageResponse.setUrlSegment(productPage.getUrlSegment());
    productPageResponse.setResolvedType(productPage.getClass().getSimpleName());
    productPageResponse.setDescription(productPage.getDescription());
    productPageResponse.setProductId(productPage.getProduct().getId());

    productPageResponse.setUrl(siteTreeUrlGenerator.getRelativeUrl(productPage));
    productPageResponse.setNgUrl(siteTreeUrlGenerator.getNgUrl(productPage));

    productPageResponse.setProductTitle(productPage.getProduct().getProductTitle());

    Optional<ProductDetail> optProductDetail =
      this.findProductDetail(productPage.getProduct(), countryCode);
    if (optProductDetail.isPresent()) {

      ProductDetail productDetail = optProductDetail.get();
      BigDecimal productGrossPrice = productDetail.getPriceNet()
        .multiply(BigDecimal.ONE.add(productDetail.getVatProduct()))
        .setScale(2, RoundingMode.HALF_UP);
      productPageResponse.setOldPriceGross(productDetail.getOldPriceGross());
      productPageResponse.setGrossPrice(productGrossPrice);
      productPageResponse.setCurrency(productDetail.getCountry().getCurrency());
      productPageResponse.setAvailableInCountry(true);
    } else {
      productPageResponse.setAvailableInCountry(false);
    }

    appendPreviewImages(productPageResponse, productPage);

    return productPageResponse;
  }

  private Optional<ProductDetail> findProductDetail(Product product, String countryCode) {
    return product.getProductDetails().stream()
      .filter(pd -> pd.getCountry().getCode().equalsIgnoreCase(countryCode))
      .findFirst();
  }

  private void appendPreviewImages(ShopCategoryProductViewModel productPageResponse,
                                   ProductPage productPage) {
    S3Image s3Image = productPage.getPreviewImage().getS3Image();
    try {
      ImageSize smallPreview = shopProperties.getPreviewImage().getSmall();
      ImageSize largePreview = shopProperties.getPreviewImage().getLarge();
      productPageResponse.setPreviewUrl(
        s3ImageResizeService.getResizedImageUrl(s3Image,
          new PaddedImageStrategy(smallPreview.getWidth(), smallPreview.getHeight())));
      productPageResponse.setLargePreviewUrl(
        s3ImageResizeService.getResizedImageUrl(s3Image,
          new PaddedImageStrategy(largePreview.getWidth(), largePreview.getHeight())));
    } catch (IOException ex) {
      logger.warn("Problem with cached (or caching) size variants of image: " + s3Image
        .getName(), ex);
    } catch (IllegalArgumentException ex) {
      logger.warn("Wrong arguments passed to the strategy class", ex);
    }
  }
}
