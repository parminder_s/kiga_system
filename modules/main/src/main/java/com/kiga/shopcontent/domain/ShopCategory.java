package com.kiga.shopcontent.domain;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.SiteTree;

/**
 * @author bbs
 * @since 9/4/16.
 */
public interface ShopCategory<T extends SiteTree> extends IdeaCategory<T> {
}
