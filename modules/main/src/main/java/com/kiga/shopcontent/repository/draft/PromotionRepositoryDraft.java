package com.kiga.shopcontent.repository.draft;

import com.kiga.shopcontent.domain.draft.PromotionDraft;
import com.kiga.shopcontent.repository.abstraction.PromotionRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by asv on 29.11.16.
 */
public interface PromotionRepositoryDraft extends PromotionRepository,
  CrudRepository<PromotionDraft, Long> {
}
