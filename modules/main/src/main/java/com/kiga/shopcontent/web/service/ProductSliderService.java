package com.kiga.shopcontent.web.service;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3File;
import com.kiga.s3.domain.S3Image;
import com.kiga.security.domain.Member;
import com.kiga.shopcontent.domain.ProductPage;
import com.kiga.shopcontent.domain.SliderFile;
import com.kiga.shopcontent.domain.live.ProductPageLive;
import com.kiga.shopcontent.domain.live.SliderFileLive;
import com.kiga.shopcontent.repository.live.ProductPageRepositoryLive;
import com.kiga.shopcontent.repository.live.SliderFileRepositoryLive;
import com.kiga.shopcontent.viewmodel.SliderAdminPageResponse;
import com.kiga.shopcontent.viewmodel.SliderFileResponse;
import com.kiga.upload.UploadParameters;
import com.kiga.upload.factory.AbstractFileUploader;
import com.kiga.upload.factory.UploadFactory;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asv on 26.12.16.
 */
public class ProductSliderService {
  private UploadFactory uploadFactory;
  private SliderFileRepositoryLive sliderFileRepositoryLive;
  private ProductPageRepositoryLive productPageRepositoryLive;
  private SiteTreeLiveRepository siteTreeRepository;
  private SiteTreeUrlGenerator urlGenerator;

  /**
   * Constructor.
   */
  public ProductSliderService(UploadFactory uploadFactory,
                              SliderFileRepositoryLive sliderFileRepositoryLive,
                              ProductPageRepositoryLive productPageRepositoryLive,
                              SiteTreeLiveRepository siteTreeRepository,
                              SiteTreeUrlGenerator urlGenerator) {
    this.uploadFactory = uploadFactory;
    this.sliderFileRepositoryLive = sliderFileRepositoryLive;
    this.productPageRepositoryLive = productPageRepositoryLive;
    this.siteTreeRepository = siteTreeRepository;
    this.urlGenerator = urlGenerator;
  }

  /**
   * uploads an s3Image.
   */
  public KigaS3File uploadS3Image(MultipartFile file, Member member) throws Exception {
    AbstractFileUploader fileUploader = uploadFactory.getUploaderInstance(file, member);
    UploadParameters uploadParameters = new UploadParameters();
    uploadParameters.setCannedAccessControlList(CannedAccessControlList.PublicRead);
    uploadParameters.setImageVariants(new String[0]);
    fileUploader.setUploadParameters(uploadParameters);
    KigaS3File kigaS3File = fileUploader.upload();
    if (kigaS3File != null) {
      if (kigaS3File.getClassName().equals("KigaS3File")) {
        return kigaS3File;
      }
      return (S3Image)kigaS3File;

    } else {
      return null;
    }
  }

  /**
   * Get the SliderFiles.
   */
  public List<SliderFileResponse> getSliderFiles(Long productId) {
    ProductPage product = productPageRepositoryLive.findOne(productId);
    ArrayList<SliderFileResponse> list = new ArrayList<>();
    List<SliderFile> sliderItems = product.getSliderFiles();
    for (SliderFile file: sliderItems) {
      SliderFileResponse response = new SliderFileResponse();
      response.setId(file.getId());
      response.setFileType(file.getFileType());
      response.setSort(file.getSort());
      if (file.getFileType().equals("video")) {
        if (file.getPreviewImage() != null) {
          response.setImageUrl(file.getPreviewImage().getUrl());
        }
      } else {
        response.setImageUrl(file.getKigaS3File().getUrl());
      }
      list.add(response);
    }
    return list;
  }

  /**
   * Get the ProductPage URL.
   */
  public SliderAdminPageResponse getProductPageUrl(Long productId) {
    SiteTreeLive siteTree = siteTreeRepository.findOne(productId);
    String relativeUrl;
    if (siteTree != null) {
      relativeUrl = urlGenerator.getRelativeNgUrl(siteTree);
    } else {
      relativeUrl = "";
    }
    SliderAdminPageResponse returner = new SliderAdminPageResponse();
    returner.setProductUrl(relativeUrl);
    return returner;
  }

  /**
   * Save the SliderFiles order.
   */
  public Boolean saveSliderOrder(List<SliderFileResponse> sliderItems, Long productId) {
    ProductPageLive product = productPageRepositoryLive.findOne(productId);
    List<SliderFileLive> newSliderItems = product.getSliderFiles();
    for (SliderFileResponse sliderFile: sliderItems) {
      for (SliderFile file: newSliderItems) {
        if (file.getId() == sliderFile.getId()) {
          file.setSort(sliderFile.getSort());
        }
      }
    }
    product.setSliderFiles(newSliderItems);
    productPageRepositoryLive.save(product);
    return true;
  }

  /**
   * Add new file to slider.
   */
  public Boolean addSliderFile(S3Image s3File, Long productId) {
    ProductPageLive product = productPageRepositoryLive.findOne(productId);
    SliderFileLive sliderFile = new SliderFileLive();
    sliderFile.setKigaS3File(s3File);
    sliderFile.setProductPage(product);
    List<SliderFileLive> newSliderItems = product.getSliderFiles();
    sliderFile.setSort(newSliderItems.size() + 1);
    sliderFile.setFileType("image");
    sliderFileRepositoryLive.save(sliderFile);
    return true;
  }

  /**
   * Add new video-file to slider.
   */
  public Boolean addVideoSliderFile(KigaS3File kigaS3File, S3Image previewImage, Long productId) {
    ProductPageLive product = productPageRepositoryLive.findOne(productId);
    SliderFileLive sliderFile = new SliderFileLive();
    sliderFile.setKigaS3File(kigaS3File);
    sliderFile.setPreviewImage(previewImage);
    sliderFile.setProductPage(product);
    List<SliderFileLive> newSliderItems = product.getSliderFiles();
    sliderFile.setSort(newSliderItems.size() + 1);
    sliderFile.setFileType("video");
    sliderFileRepositoryLive.save(sliderFile);
    return true;
  }

  /**
   * Add new file to slider.
   */
  public Boolean deleteSliderFile(Long sliderFileId) {
    sliderFileRepositoryLive.delete(sliderFileId);
    return true;
  }
}
