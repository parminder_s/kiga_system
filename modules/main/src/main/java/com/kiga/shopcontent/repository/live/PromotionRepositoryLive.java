package com.kiga.shopcontent.repository.live;

import com.kiga.shopcontent.domain.live.PromotionLive;
import com.kiga.shopcontent.repository.abstraction.PromotionRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by asv on 29.11.16.
 */
public interface PromotionRepositoryLive extends PromotionRepository,
  CrudRepository<PromotionLive, Long> {
}
