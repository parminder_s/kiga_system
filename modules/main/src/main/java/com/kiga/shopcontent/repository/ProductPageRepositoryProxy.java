package com.kiga.shopcontent.repository;

import static java.util.Comparator.comparing;

import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.repository.ss.AbstractRepositoryProxy;
import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.shopcontent.domain.ProductPage;
import com.kiga.shopcontent.domain.live.QProductPageLive;
import com.kiga.shopcontent.repository.abstraction.ProductPageRepository;
import com.kiga.shopcontent.repository.draft.ProductPageRepositoryDraft;
import com.kiga.shopcontent.repository.live.ProductPageRepositoryLive;
import com.kiga.shopcontent.repository.predicates.ProductPagePredicates;
import com.kiga.shopcontent.repository.querydsl.ProductPageQTypeResolver;
import com.mysema.query.types.expr.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author bbs
 * @since 9/6/16.
 */
@SuppressWarnings("unchecked")
@Service
public class ProductPageRepositoryProxy extends AbstractRepositoryProxy<ProductPageRepository>
  implements ProductPageRepository {
  private ProductPageQTypeResolver productPageQTypeResolver;

  /**
   * Constructor for the Model.
   *
   * @param liveRepository    live repository
   * @param draftRepository   draft repository
   * @param repositoryContext repository context holder
   */
  @Autowired
  public ProductPageRepositoryProxy(ProductPageRepositoryLive liveRepository,
    ProductPageRepositoryDraft draftRepository, RepositoryContext repositoryContext,
    ProductPageQTypeResolver productPageQTypeResolver) {
    super(liveRepository, draftRepository, repositoryContext);
    this.productPageQTypeResolver = productPageQTypeResolver;
  }

  /**
   * Get product pages by category id.
   *
   * @param categoryId  category id
   * @param countryCode country code
   * @param pageable    pageable\
   * @return list with product pages
   */
  public List<ProductPage> getProductsByCategoryId(Long categoryId, String countryCode,
    Pageable pageable) {
    QueryDslPredicateExecutor queryDslPredicateExecutor = (QueryDslPredicateExecutor)
      getRepository();
    ProductPagePredicates productPagePredicates = new ProductPagePredicates(
      productPageQTypeResolver.getEntityClass(),
      productPageQTypeResolver.getQueryDslVariableName());

    Iterable<ProductPage> allProductPages = queryDslPredicateExecutor.findAll(
      productPagePredicates.getAllMatchingProductsPredicate(categoryId));
    Iterable<ProductPage> productPagesWithCountry = queryDslPredicateExecutor.findAll(
      productPagePredicates.getMatchingProductsForCountryPredicate(categoryId, countryCode));

    return StreamSupport.stream(allProductPages.spliterator(), false)
      .map((productPage) -> this.matchProducts(productPage, productPagesWithCountry))
      .peek(SiteTree::getTitle)
      .sorted(comparing(SiteTree::getSort))
      .collect(Collectors.toList());
  }

  private ProductPage matchProducts(ProductPage matchingProductWithoutCountry,
    Iterable<ProductPage> matchingProductsForCountry) {
    for (ProductPage matchingProductForCountry : matchingProductsForCountry) {
      if (matchingProductForCountry.getId().equals(matchingProductWithoutCountry.getId())) {
        return matchingProductForCountry;
      }
    }
    return matchingProductWithoutCountry;
  }

  /**
   * find ProductPage by productId.
   */
  public ProductPage findByProductId(Long productId) {
    QProductPageLive productPageLive = QProductPageLive.productPageLive;
    BooleanExpression hasProductId = productPageLive.product.id.eq(productId);
    QueryDslPredicateExecutor queryDslPredicateExecutor = (QueryDslPredicateExecutor)
      getRepository();
    Iterable<ProductPage> pages = queryDslPredicateExecutor.findAll(hasProductId);
    return StreamSupport.stream(pages.spliterator(), false).collect(Collectors.toList()).get(0);
  }
}
