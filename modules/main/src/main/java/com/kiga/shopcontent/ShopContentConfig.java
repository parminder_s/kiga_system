package com.kiga.shopcontent;

import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.shop.backend.web.converter.ProductToViewModelConverter;
import com.kiga.shop.backend.web.converter.VoucherToViewModelConverter;
import com.kiga.shopcontent.repository.PromotionRepositoryProxy;
import com.kiga.shopcontent.repository.ShopCategoryRepositoryProxy;
import com.kiga.shopcontent.repository.draft.PromotionRepositoryDraft;
import com.kiga.shopcontent.repository.draft.ShopCategoryRepositoryDraft;
import com.kiga.shopcontent.repository.live.ProductPageRepositoryLive;
import com.kiga.shopcontent.repository.live.PromotionRepositoryLive;
import com.kiga.shopcontent.repository.live.ShopCategoryRepositoryLive;
import com.kiga.shopcontent.repository.live.SliderFileRepositoryLive;
import com.kiga.shopcontent.service.ProductPageService;
import com.kiga.shopcontent.service.ShopCategoryService;
import com.kiga.shopcontent.viewmodel.converter.ShopCategoryToResponseConverter;
import com.kiga.shopcontent.viewmodel.mapper.ShopCategoryViewModelMapper;
import com.kiga.shopcontent.web.service.ProductSliderService;
import com.kiga.upload.factory.UploadFactory;
import com.kiga.web.service.GeoIpService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShopContentConfig {
  @Bean
  public ShopCategoryRepositoryProxy createShopCategoryModel(
      ShopCategoryRepositoryLive shopCategoryRepositoryLive,
      ShopCategoryRepositoryDraft shopCategoryRepositoryDraft,
      RepositoryContext repositoryContext) {
    return new ShopCategoryRepositoryProxy(
        shopCategoryRepositoryLive, shopCategoryRepositoryDraft, repositoryContext);
  }

  @Bean
  public PromotionRepositoryProxy createPromotionModel(
      PromotionRepositoryLive promotionRepositoryLive,
      PromotionRepositoryDraft promotionRepositoryDraft,
      RepositoryContext repositoryContext) {
    return new PromotionRepositoryProxy(
        promotionRepositoryLive, promotionRepositoryDraft, repositoryContext);
  }

  @Bean
  public ShopCategoryService createShopCategoryService(
      ShopCategoryRepositoryProxy shopCategoryModel, ShopCategoryToResponseConverter converter) {
    return new ShopCategoryService(shopCategoryModel, converter);
  }

  @Bean
  public ProductSliderService createProductSliderService(
      UploadFactory uploadFactory,
      SliderFileRepositoryLive sliderFileRepositoryLive,
      ProductPageRepositoryLive productPageRepositoryLive,
      SiteTreeLiveRepository siteTreeRepository,
      SiteTreeUrlGenerator urlGenerator) {
    return new ProductSliderService(
        uploadFactory,
        sliderFileRepositoryLive,
        productPageRepositoryLive,
        siteTreeRepository,
        urlGenerator);
  }

  @Bean
  public ShopCategoryToResponseConverter createShopCategoryToResponseConverter() {
    return new ShopCategoryToResponseConverter();
  }

  @Bean
  public ProductToViewModelConverter createProductToViewModelConverter() {
    return new ProductToViewModelConverter();
  }

  @Bean
  public VoucherToViewModelConverter createVoucherToViewModelConverter() {
    return new VoucherToViewModelConverter();
  }

  /** Create dependency. */
  @Bean
  public ShopCategoryViewModelMapper createShopCategoryViewModelMapper(
      ShopCategoryService shopCategoryService,
      ProductPageService productPageService,
      GeoIpService geoIpService) {
    return new ShopCategoryViewModelMapper(shopCategoryService, productPageService, geoIpService);
  }
}
