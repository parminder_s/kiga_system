package com.kiga.shopcontent.web;

import com.kiga.security.services.SecurityService;
import com.kiga.shop.web.requests.SingleIdRequest;
import com.kiga.shopcontent.viewmodel.PromotionResponse;
import com.kiga.shopcontent.web.request.PromotionRequest;
import com.kiga.shopcontent.web.service.ShopContentPromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by asv on 29.11.16.
 */

@RestController
public class PromotionController {

  private ShopContentPromotionService promotionService;
  private SecurityService securityService;

  /**
   * Constructor to pass all dependencies.
   */
  @Autowired
  public PromotionController(ShopContentPromotionService promotionService,
                             SecurityService securityService) {
    this.promotionService = promotionService;
    this.securityService = securityService;
  }

  /**
   * Get the promotion.
   */
  @RequestMapping(value = "/promotion", method = RequestMethod.POST)
  public PromotionResponse getPromotion(
    @RequestBody(required = false) PromotionRequest request) throws IOException {
    Long pageId = request.getPageId();
    return promotionService.getPromotion(pageId);
  }

  @RequestMapping(value = "/setPromotionOnMainArticleCategory", method = RequestMethod.POST)
  public void setPromotionOnMainArticleCategory(@RequestBody SingleIdRequest singleIdRequest)
    throws IOException {
    this.securityService.requiresContentAdmin();
    promotionService.setPromotionOnMainArticleCategory(singleIdRequest.getId());
  }
}
