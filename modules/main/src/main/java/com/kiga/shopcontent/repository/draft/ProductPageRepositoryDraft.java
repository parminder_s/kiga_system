package com.kiga.shopcontent.repository.draft;

import com.kiga.shopcontent.domain.draft.ProductPageDraft;
import com.kiga.shopcontent.repository.abstraction.ProductPageRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author bbs
 * @since 9/6/16.
 */
public interface ProductPageRepositoryDraft extends ProductPageRepository,
  PagingAndSortingRepository<ProductPageDraft, Long>, QueryDslPredicateExecutor<ProductPageDraft> {
}
