package com.kiga.shopcontent.repository;

import com.kiga.content.repository.ss.AbstractRepositoryProxy;
import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.shopcontent.domain.Promotion;
import com.kiga.shopcontent.repository.abstraction.PromotionRepository;


/**
 * Created by asv on 29.11.16.
 */
public class PromotionRepositoryProxy extends AbstractRepositoryProxy<PromotionRepository>
  implements PromotionRepository {

  /**
   * Constructor for the Model.
   *
   * @param liveRepository    live repository
   * @param draftRepository   draft repository
   * @param repositoryContext repository context holder
   */
  public PromotionRepositoryProxy(PromotionRepository liveRepository,
                                  PromotionRepository draftRepository,
                                     RepositoryContext repositoryContext) {
    super(liveRepository, draftRepository, repositoryContext);
  }

  @Override
  public Promotion findByPageId(Long id) {
    return getRepository().findByPageId(id);
  }

}
