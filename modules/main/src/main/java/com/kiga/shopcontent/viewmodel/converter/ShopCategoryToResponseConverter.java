package com.kiga.shopcontent.viewmodel.converter;

import com.kiga.shopcontent.domain.ShopCategory;
import com.kiga.shopcontent.viewmodel.ShopCategoryViewModel;
import com.kiga.web.converter.ResponseConverter;
import org.apache.commons.collections4.CollectionUtils;

/**
 * @author bbs
 * @since 9/4/16.
 */
public class ShopCategoryToResponseConverter
  implements ResponseConverter<ShopCategory, ShopCategoryViewModel> {

  @Override
  public ShopCategoryViewModel convertToResponse(ShopCategory entity) {
    if (entity == null) {
      return null;
    }

    ShopCategoryViewModel returner = new ShopCategoryViewModel();

    returner.setClassName(entity.getClassName());
    returner.setResolvedType(entity.getClass().getSimpleName());
    returner.setId(entity.getId());
    returner.setTitle(entity.getTitle());
    returner.setUrlSegment(entity.getUrlSegment());
    returner.setLastCategoryLevel(CollectionUtils.isEmpty(entity.getChildren()));

    return returner;
  }
}
