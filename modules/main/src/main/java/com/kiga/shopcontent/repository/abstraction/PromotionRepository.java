package com.kiga.shopcontent.repository.abstraction;

import com.kiga.shopcontent.domain.Promotion;

/**
 * Created by asv on 29.11.16.
 */
public interface PromotionRepository {

  Promotion findByPageId(Long pageId);

}
