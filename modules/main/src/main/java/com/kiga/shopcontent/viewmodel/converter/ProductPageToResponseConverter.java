package com.kiga.shopcontent.viewmodel.converter;

import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.web.service.ProductMetaConverter;
import com.kiga.ideas.web.viewmodel.kigaidea.LinksReplacer;
import com.kiga.ideas.web.viewmodel.kigaidea.MediaLinksConverter;
import com.kiga.main.locale.Locale;
import com.kiga.main.locale.LocaleConverter;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import com.kiga.shop.ShopProperties;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.repository.CountryLocaleRepository;
import com.kiga.shop.repository.CountryRepository;
import com.kiga.shop.repository.CountryShopRepository;
import com.kiga.shopcontent.domain.ProductPage;
import com.kiga.shopcontent.domain.SliderFile;
import com.kiga.shopcontent.viewmodel.ProductPageViewModel;
import com.kiga.shopcontent.viewmodel.SliderFileResponse;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by asv on 10.11.16.
 */
@Service
public class ProductPageToResponseConverter {
  private ModelMapper modelMapper;
  private Logger logger = LoggerFactory.getLogger(getClass());
  private S3ImageResizeService s3ImageResizeService;
  private ShopProperties shopProperties;
  private SiteTreeUrlGenerator siteTreeUrlGenerator;
  private SecurityService securityService;
  private HttpServletRequest request;
  private CountryShopRepository countryShopRepository;
  private LinksReplacer linksReplacer;
  private CountryLocaleRepository countryLocaleRepository;
  private CountryRepository countryRepository;
  private LocaleConverter localeConverter;
  private int sliderImageHeight = 56;
  private int sliderImageWidth = 82;

  /**
   * Constructor for converter.
   */
  @Autowired
  public ProductPageToResponseConverter(
    S3ImageResizeService s3ImageResizeService, ShopProperties shopProperties,
    SiteTreeUrlGenerator siteTreeUrlGenerator,
    SecurityService securityService, ModelMapper modelMapper,
    CountryShopRepository countryShopRepository, LinksReplacer linksReplacer,
    CountryLocaleRepository countryLocaleRepository, CountryRepository countryRepository) {
    this.s3ImageResizeService = s3ImageResizeService;
    this.shopProperties = shopProperties;
    this.siteTreeUrlGenerator = siteTreeUrlGenerator;
    this.securityService = securityService;
    this.modelMapper = modelMapper;
    this.countryShopRepository = countryShopRepository;
    this.linksReplacer = linksReplacer;
    this.countryRepository = countryRepository;
    this.countryLocaleRepository = countryLocaleRepository;
  }

  /**
   * creates the ViewModel.
   */
  public ProductPageViewModel convertToResponse(ProductPage productPage, String countryCode) {
    ProductPageViewModel returner = modelMapper.map(productPage, ProductPageViewModel.class);
    returner.setName(productPage.getUrlSegment());
    returner.setResolvedType(productPage.getClass().getSimpleName());
    returner.setUrl(siteTreeUrlGenerator.getRelativeUrl(productPage));
    returner.setProductId(productPage.getProduct().getId());
    MediaLinksConverter mediaLinksConverter = new MediaLinksConverter();
    String ideaContent = mediaLinksConverter.convertMediaLinks(productPage.getContent());
    ProductMetaConverter productMetaConverter = new ProductMetaConverter();
    ideaContent = productMetaConverter.analyzeContent(ideaContent, productPage.getProduct());
    returner.setContent(linksReplacer.analyzeContentLinks(ideaContent));

    ArrayList<SliderFileResponse> list = new ArrayList<>();
    List<SliderFile> sliderItems = productPage.getSliderFiles();
    for (SliderFile file: sliderItems) {
      SliderFileResponse response = new SliderFileResponse();
      response.setFileType(file.getFileType());
      response.setSort(file.getSort());
      if (file.getFileType().equals("image")) {
        response.setImageUrl(file.getKigaS3File().getUrl());
        S3Image image = (S3Image)file.getKigaS3File();
        setPreviewImages(response, image);
      }
      if (file.getFileType().equals("video")) {
        response.setVideoUrl(file.getKigaS3File().getUrl());
        response.setImageUrl(file.getPreviewImage().getUrl());
        setPreviewImages(response, file.getPreviewImage());
      }
      list.add(response);
    }
    returner.setSliderItems(list);

    Optional<ProductDetail> optProductDetail =
      this.findProductDetail(productPage.getProduct(), countryCode);
    if (optProductDetail.isPresent()) {
      ProductDetail productDetail = optProductDetail.get();
      Long countryId = productDetail.getCountry().getId();
      BigDecimal productGrossPrice = productDetail.getPriceNet()
        .multiply(BigDecimal.ONE.add(productDetail.getVatProduct()));
      returner.setGrossPrice(productGrossPrice);
      returner.setCurrency(productDetail.getCountry().getCurrency());
      returner.setMinDeliveryDays(productDetail.getCountry().getCountryShop().getMinDeliveryDays());
      returner.setMaxDeliveryDays(productDetail.getCountry().getCountryShop().getMaxDeliveryDays());
      returner.setFreeShippingLimit(
        countryShopRepository.findByCountryId(countryId).getFreeShippingLimit());
      returner.setAvailableInCountry(true);
    } else {
      returner.setAvailableInCountry(false);
    }

    Optional<Member> member = securityService.getSessionMember();
    Country country = countryRepository.findByCode(countryCode);
    returner.setCountryName(countryLocaleRepository.findByCountryAndLocale(country,
      productPage.getLocale()).getTitle());
    if (member.isPresent()) {
      Locale locale = localeConverter.toLocale(member.get().getLocale());
      if (country != null && locale != null) {
        String countryName = countryLocaleRepository.findByCountryAndLocale(country, locale)
          .getTitle();
        if (countryName != null) {
          returner.setCountryName(countryName);
        }
      }
    }

    return returner;
  }

  private Optional<ProductDetail> findProductDetail(Product product, String countryCode) {
    return product.getProductDetails().stream()
      .filter(pd -> pd.getCountry().getCode().equalsIgnoreCase(countryCode))
      .findFirst();
  }

  private void setPreviewImages(SliderFileResponse response,
                                S3Image s3Image) {
    if (s3Image == null) {
      response.setImageUrl("");
    } else {
      try {
        ImageSize largePreview = shopProperties.getPreviewImage().getLarge();
        response.setSliderImageUrl(
          s3ImageResizeService.getResizedImageUrl(s3Image,
            new PaddedImageStrategy(sliderImageWidth, sliderImageHeight)));
        response.setImageUrl(
          s3ImageResizeService.getResizedImageUrl(s3Image,
            new PaddedImageStrategy(largePreview.getWidth(), largePreview.getHeight())));
      } catch (IOException ex) {
        logger.warn("Problem with cached (or caching) size variants of image: " + s3Image
          .getName(), ex);
      } catch (IllegalArgumentException ex) {
        logger.warn("Wrong arguments passed to the strategy class", ex);
      }
    }
  }

}
