package com.kiga.shopcontent.domain;

import com.kiga.content.domain.ss.KigaPage;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.LinkedKigaPageGroup;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.shop.domain.Product;

import java.util.List;

/**
 * @author bbs
 * @since 9/4/16.
 */
public interface ProductPage<
  T extends SiteTree, G extends LinkedKigaPageGroup, S extends SliderFile>
  extends KigaPage<T, G> {
  Product getProduct();

  void setProduct(Product product);

  List<S> getSliderFiles();

  void setSliderFiles(List<S> sliderFiles);

  String getIdeaTeaser();

  void setIdeaTeaser(String ideaTeaser);

  String getIdeaText();

  void setIdeaText(String ideaText);

  KigaPageImage getIdeaImage();

  void setIdeaImage(KigaPageImage ideaImage);

}
