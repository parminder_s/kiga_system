package com.kiga.shopcontent.web.service;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.repository.ss.KigaIdeaRepositoryProxy;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.shopcontent.domain.Promotion;
import com.kiga.shopcontent.repository.ProductPageRepositoryProxy;
import com.kiga.shopcontent.repository.PromotionRepositoryProxy;
import com.kiga.shopcontent.viewmodel.PromotionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by asv on 30.11.16.
 */
@Service
public class ShopContentPromotionService {
  private PromotionRepositoryProxy promotionRepositoryProxy;
  private SiteTreeUrlGenerator siteTreeUrlGenerator;
  private KigaIdeaRepositoryProxy kigaIdeaRepositoryProxy;
  private ProductPageRepositoryProxy productPageRepositoryProxy;


  /**
   * Constructor to pass all dependencies.
   */
  @Autowired
  public ShopContentPromotionService(PromotionRepositoryProxy promotionRepositoryProxy,
                                     SiteTreeUrlGenerator siteTreeUrlGenerator,
                                     KigaIdeaRepositoryProxy kigaIdeaRepositoryProxy,
                                     ProductPageRepositoryProxy productPageRepositoryProxy) {
    this.promotionRepositoryProxy = promotionRepositoryProxy;
    this.siteTreeUrlGenerator = siteTreeUrlGenerator;
    this.kigaIdeaRepositoryProxy = kigaIdeaRepositoryProxy;
    this.productPageRepositoryProxy = productPageRepositoryProxy;
  }

  /**
   * Get the promotion data.
   */
  public PromotionResponse getPromotion(Long pageId) {
    PromotionResponse returner = new PromotionResponse();
    Promotion promotion = promotionRepositoryProxy.findByPageId(pageId);
    if (promotion == null) {
      KigaIdea page = kigaIdeaRepositoryProxy.findOne(pageId);
      SiteTree ideaCategory = page.getParent();
      while (promotion == null && ideaCategory != null) {
        promotion = promotionRepositoryProxy.findByPageId(ideaCategory.getId());
        ideaCategory = ideaCategory.getParent();
      }
    }
    if (promotion != null) {
      returner.setIdeaTeaser(promotion.getProductPage().getIdeaTeaser());
      returner.setIdeaText(promotion.getProductPage().getIdeaText());
      returner.setProductPageUrl(siteTreeUrlGenerator.getRelativeNgUrl(promotion.getProductPage()));
      returner.setIdeaImage(promotion.getProductPage().getIdeaImage().getS3Image().getUrl());
    }
    return returner;
  }

  /**
   * Promotes the product with the given Id on the mainArticleCategory.
   * @param productId id of the product to promote
   */
  public void setPromotionOnMainArticleCategory(Long productId) {
    Promotion promotion = this.promotionRepositoryProxy.findByPageId(17L);
    promotion.setProductPage(productPageRepositoryProxy.findByProductId(productId));
    this.promotionRepositoryProxy.getCrudRepository().save(promotion);
  }
}
