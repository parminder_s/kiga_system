package com.kiga.shopcontent.repository.live;

import com.kiga.shopcontent.domain.live.ProductPageLive;
import com.kiga.shopcontent.repository.abstraction.ProductPageRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author bbs
 * @since 9/6/16.
 */
public interface ProductPageRepositoryLive extends ProductPageRepository,
  PagingAndSortingRepository<ProductPageLive, Long>, QueryDslPredicateExecutor<ProductPageLive> {
}
