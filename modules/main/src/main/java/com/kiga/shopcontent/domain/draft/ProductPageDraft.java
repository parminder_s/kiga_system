package com.kiga.shopcontent.domain.draft;

import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.draft.KigaPageDraft;
import com.kiga.content.domain.ss.draft.LinkedKigaPageGroupDraft;
import com.kiga.content.domain.ss.draft.SiteTreeDraft;
import com.kiga.shop.domain.Product;
import com.kiga.shopcontent.domain.ProductPage;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * @author bbs
 * @since 9/4/16.
 */
@Entity
@Table(name = "ShopArticlePage")
public class ProductPageDraft extends KigaPageDraft
  implements ProductPage<SiteTreeDraft, LinkedKigaPageGroupDraft, SliderFileDraft> {
  @ManyToOne
  @JoinColumn(name = "productID", referencedColumnName = "id")
  private Product product;

  @OneToMany(mappedBy = "productPage")
  @OrderBy("sort ASC")
  private List<SliderFileDraft> sliderFiles;

  private String ideaTeaser;
  private String ideaText;

  @ManyToOne
  @JoinColumn(name = "ideaImageID", referencedColumnName = "id")
  @NotFound(action = NotFoundAction.IGNORE)
  private KigaPageImage ideaImage;

  public ProductPageDraft() {
    this.setClassName("ShopArticlePage");
  }

  @Override
  public Product getProduct() {
    return product;
  }

  @Override
  public void setProduct(Product product) {
    this.product = product;
  }

  @Override
  public List<SliderFileDraft> getSliderFiles() {
    return sliderFiles;
  }

  @Override
  public void setSliderFiles(List<SliderFileDraft> sliderFiles) {
    this.sliderFiles = sliderFiles;
  }

  @Override
  public String getIdeaTeaser() {
    return ideaTeaser;
  }

  @Override
  public void setIdeaTeaser(String ideaTeaser) {
    this.ideaTeaser = ideaTeaser;
  }

  @Override
  public String getIdeaText() {
    return ideaText;
  }

  @Override
  public void setIdeaText(String ideaText) {
    this.ideaText = ideaText;
  }

  @Override
  public KigaPageImage getIdeaImage() {
    return ideaImage;
  }

  @Override
  public void setIdeaImage(KigaPageImage ideaImage) {
    this.ideaImage = ideaImage;
  }
}
