package com.kiga.shopcontent.initializer;

import com.kiga.cms.service.ViewModelMapperFactory;
import com.kiga.shopcontent.domain.live.ProductPageLive;
import com.kiga.shopcontent.domain.live.ShopCategoryLive;
import com.kiga.shopcontent.viewmodel.mapper.ProductPageViewModelMapper;
import com.kiga.shopcontent.viewmodel.mapper.ShopCategoryViewModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * @author bbs
 * @since 12/8/16.
 */
@Component
public class ShopViewModelMapperFactoryInitializer
  implements ApplicationListener<ContextRefreshedEvent> {
  private ViewModelMapperFactory viewModelMapperFactory;
  private ShopCategoryViewModelMapper shopCategoryViewModelMapper;
  private ProductPageViewModelMapper productPageViewModelMapper;

  /**
   * Setup dependencies.
   *
   * @param shopCategoryViewModelMapper shopCategoryViewModelMapper
   * @param productPageViewModelMapper  productPageViewModelMapper
   * @param viewModelMapperFactory      viewModelMapperFactory
   */
  @Autowired
  public ShopViewModelMapperFactoryInitializer(
    ShopCategoryViewModelMapper shopCategoryViewModelMapper,
    ProductPageViewModelMapper productPageViewModelMapper,
    ViewModelMapperFactory viewModelMapperFactory) {
    this.shopCategoryViewModelMapper = shopCategoryViewModelMapper;
    this.productPageViewModelMapper = productPageViewModelMapper;
    this.viewModelMapperFactory = viewModelMapperFactory;
  }

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    viewModelMapperFactory
      .register(shopCategoryViewModelMapper, ShopCategoryLive.class)
      .register(productPageViewModelMapper, ProductPageLive.class);
  }
}
