package com.kiga.shopcontent.web.request;

import com.kiga.s3.domain.S3Image;
import com.kiga.shopcontent.domain.SliderFile;
import com.kiga.shopcontent.viewmodel.SliderFileResponse;
import lombok.Data;

import java.util.List;

/**
 * Created by asv on 28.11.16.
 */
@Data
public class SaveProductSliderFilesRequest {
  private long productId;
  private List<Long> s3FileIds;
  private List<SliderFileResponse> sliderFiles;
}
