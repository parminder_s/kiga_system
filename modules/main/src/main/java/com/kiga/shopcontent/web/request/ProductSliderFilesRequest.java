package com.kiga.shopcontent.web.request;


import lombok.Data;

/**
 * Created by asv on 24.11.16.
 */
@Data
public class ProductSliderFilesRequest {
  private long productId;

  private long sliderFileId;
}
