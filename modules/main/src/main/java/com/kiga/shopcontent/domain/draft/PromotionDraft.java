package com.kiga.shopcontent.domain.draft;

import com.kiga.content.domain.ss.draft.PageDraft;
import com.kiga.shopcontent.domain.Promotion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by rainerh on 28.11.16.
 */
@Entity
@Table(name = "Promotion")
public class PromotionDraft extends Promotion<PageDraft, ProductPageDraft> {
  @Id
  @Column(name = "ID")
  @GeneratedValue
  private Long id;

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }
}
