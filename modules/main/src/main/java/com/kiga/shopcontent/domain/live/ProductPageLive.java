package com.kiga.shopcontent.domain.live;

import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.live.KigaPageLive;
import com.kiga.content.domain.ss.live.LinkedKigaPageGroupLive;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.shop.domain.Product;
import com.kiga.shopcontent.domain.ProductPage;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;


/**
 * @author bbs
 * @since 9/4/16.
 */
@Entity
@Table(name = "ShopArticlePage_Live")
public class ProductPageLive extends KigaPageLive
  implements ProductPage<SiteTreeLive, LinkedKigaPageGroupLive, SliderFileLive> {
  @ManyToOne
  @JoinColumn(name = "productID", referencedColumnName = "id")
  private Product product;

  @OneToMany(mappedBy = "productPage")
  @OrderBy("sort ASC")
  private List<SliderFileLive> sliderFiles;

  private String ideaTeaser;
  private String ideaText;

  @ManyToOne
  @JoinColumn(name = "ideaImageID", referencedColumnName = "id")
  @NotFound(action = NotFoundAction.IGNORE)
  private KigaPageImage ideaImage;

  public ProductPageLive() {
    this.setClassName("ShopArticlePage");
  }

  @Override
  public Product getProduct() {
    return product;
  }

  @Override
  public void setProduct(Product product) {
    this.product = product;
  }

  @Override
  public List<SliderFileLive> getSliderFiles() {
    return sliderFiles;
  }

  @Override
  public void setSliderFiles(List<SliderFileLive> sliderFiles) {
    this.sliderFiles = sliderFiles;
  }

  @Override
  public String getIdeaTeaser() {
    return ideaTeaser;
  }

  @Override
  public void setIdeaTeaser(String ideaTeaser) {
    this.ideaTeaser = ideaTeaser;
  }

  @Override
  public String getIdeaText() {
    return ideaText;
  }

  @Override
  public void setIdeaText(String ideaText) {
    this.ideaText = ideaText;
  }

  @Override
  public KigaPageImage getIdeaImage() {
    return ideaImage;
  }

  @Override
  public void setIdeaImage(KigaPageImage ideaImage) {
    this.ideaImage = ideaImage;
  }
}
