package com.kiga.shopcontent.viewmodel.mapper;

import com.kiga.cms.service.SimpleViewModelMapper;
import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.cms.web.response.Breadcrumb;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.shop.web.response.ShopViewModel;
import com.kiga.shopcontent.domain.ShopCategory;
import com.kiga.shopcontent.service.ProductPageService;
import com.kiga.shopcontent.service.ShopCategoryService;
import com.kiga.shopcontent.viewmodel.ShopCategoryProductViewModel;
import com.kiga.shopcontent.viewmodel.ShopCategoryViewModel;
import com.kiga.web.service.GeoIpService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 9/4/16.
 */

@Service
public class ShopCategoryViewModelMapper
  extends SimpleViewModelMapper<ShopCategory, ShopViewModel> {

  private ShopCategoryService shopCategoryService;
  private ProductPageService productPageService;
  private GeoIpService geoIpService;

  /**
   * Constructor.
   */
  @Autowired
  public ShopCategoryViewModelMapper(ShopCategoryService shopCategoryService,
    ProductPageService productPageService, GeoIpService geoIpService) {
    this.shopCategoryService = shopCategoryService;
    this.productPageService = productPageService;
    this.geoIpService = geoIpService;
  }

  @Override
  public ShopViewModel getViewModel(HttpServletRequest httpServletRequest,
    HttpServletResponse httpServletResponse, QueryRequestParameters<ShopCategory>
    queryRequestParameters) throws Exception {
    return super
      .getViewModel(httpServletRequest, httpServletResponse, queryRequestParameters);
  }

  private void addProductsToCategory(ShopCategoryViewModel category, String countryCode) {
    List<ShopCategoryProductViewModel> categoryProducts = productPageService
      .getProductsByCategory(category, countryCode);
    category.setProducts(categoryProducts);
  }

  private boolean isGroupNotEmpty(ShopCategoryViewModel category) {
    return CollectionUtils.isNotEmpty(category.getProducts());
  }

  @Override
  protected ShopViewModel buildViewModel(
    HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
    QueryRequestParameters<ShopCategory> queryRequestParameters) {
    SiteTree entity = queryRequestParameters.getEntity();
    List<Breadcrumb> breadcrumbs = new ArrayList<>();
    do {
      breadcrumbs.add(new Breadcrumb(entity.getTitle(), entity.getUrlSegment()));
      entity = entity.getParent();
    }
    while (entity != null);

    Collections.reverse(breadcrumbs);

    // removing homepage + module container from breadcrumbs
    if (breadcrumbs.size() > 2) {
      breadcrumbs = breadcrumbs.subList(2, breadcrumbs.size());
    } else {
      breadcrumbs = Collections.emptyList();
    }

    ShopViewModel returner = new ShopViewModel();
    returner.setBreadcrumbs(breadcrumbs);

    String countryCode = geoIpService.resolveCountryCode();
    returner.setKnownCountry(StringUtils.isNotBlank(countryCode));

    ShopCategoryViewModel shopCategoryResponse = shopCategoryService
      .getCategory(queryRequestParameters.getEntity().getId());
    returner.setActiveCategory(shopCategoryResponse);

    addProductsToCategory(shopCategoryResponse, countryCode);

    List<ShopCategoryViewModel> subcategories = shopCategoryService
      .getSubcategories(shopCategoryResponse);
    if (CollectionUtils.isNotEmpty(subcategories)) {
      subcategories = subcategories.stream()
        .peek(response -> addProductsToCategory(response, countryCode))
        .filter(this::isGroupNotEmpty).collect(
          Collectors.toList());
    }

    if (CollectionUtils.isEmpty(subcategories)) {
      shopCategoryResponse.setLastCategoryLevel(true);
      subcategories = Collections.singletonList(shopCategoryResponse);
    }

    returner.setSubcategories(subcategories);

    return returner;
  }
}
