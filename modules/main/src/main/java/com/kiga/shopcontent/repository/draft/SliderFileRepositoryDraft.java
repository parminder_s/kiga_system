package com.kiga.shopcontent.repository.draft;

import com.kiga.shopcontent.domain.draft.SliderFileDraft;
import com.kiga.shopcontent.repository.abstraction.SliderFileRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by asv on 29.11.16.
 */
public interface SliderFileRepositoryDraft extends SliderFileRepository,
  CrudRepository<SliderFileDraft, Long> {
}
