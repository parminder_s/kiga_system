package com.kiga.shopcontent.viewmodel;

import com.kiga.cms.web.response.ViewModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * @author bbs
 * @since 9/4/16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ShopCategoryProductViewModel extends ViewModel {
  private String url;
  private String ngUrl;
  private String previewUrl;
  private String largePreviewUrl;
  private String currency;
  private BigDecimal grossPrice;
  private String description;
  private long productId;
  private boolean availableInCountry;
  private BigDecimal oldPriceGross;
  private String productTitle;
}
