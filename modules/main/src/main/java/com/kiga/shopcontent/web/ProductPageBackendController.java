package com.kiga.shopcontent.web;

import com.kiga.s3.repository.ss.S3ImageRepository;
import com.kiga.security.services.SecurityService;
import com.kiga.shopcontent.viewmodel.SliderAdminPageResponse;
import com.kiga.shopcontent.viewmodel.SliderFileResponse;
import com.kiga.shopcontent.web.request.ProductSliderFilesRequest;
import com.kiga.shopcontent.web.request.SaveProductSliderFilesRequest;
import com.kiga.shopcontent.web.service.ProductSliderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by asv on 24.11.16.
 */
@RestController
@RequestMapping("/cms/backend/product/")
public class ProductPageBackendController {
  private SecurityService securityService;
  private ProductSliderService sliderService;
  private S3ImageRepository s3ImageRepository;

  /**
   * Constructor to pass all dependencies.
   */
  @Autowired
  public ProductPageBackendController(SecurityService securityService,
    ProductSliderService sliderService, S3ImageRepository s3ImageRepository) {
    this.securityService = securityService;
    this.sliderService = sliderService;
    this.s3ImageRepository = s3ImageRepository;
  }

  /**
   * Get the SliderFiles.
   */
  @RequestMapping("slider")
  public List<SliderFileResponse> slider(@RequestBody(required = false)
    ProductSliderFilesRequest request) {
    return sliderService.getSliderFiles(request.getProductId());
  }

  /**
   * Save the SliderFiles order.
   */
  @RequestMapping("save-slider")
  public Boolean saveSliderFiles(@RequestBody(required = false)
    SaveProductSliderFilesRequest request) throws Exception {
    securityService.requiresLogin();
    return sliderService.saveSliderOrder(request.getSliderFiles(), request.getProductId());
  }

  /**
   * Add new file to slider.
   */
  @RequestMapping("addFile")
  public Boolean addSliderFile(@RequestBody(required = false)
    SaveProductSliderFilesRequest request) {

    securityService.requireShopAdmin();
    request.getS3FileIds().forEach(id -> {
      if (false) {
        sliderService.addVideoSliderFile(null, null, request.getProductId());
      } else {
        sliderService.addSliderFile(s3ImageRepository.findOne(id), request.getProductId());
      }
    });

    return true;
  }

  /**
   * Get the ProductPage URL.
   */
  @RequestMapping("getProductUrl")
  public SliderAdminPageResponse getProductUrl(@RequestBody(required = false)
    ProductSliderFilesRequest request) {
    return sliderService.getProductPageUrl(request.getProductId());
  }

  /**
   * Delete SliderFile.
   */
  @RequestMapping("deleteSliderFile")
  public Boolean deleteSliderFile(@RequestBody(required = false)
    ProductSliderFilesRequest request) {
    return sliderService.deleteSliderFile(request.getSliderFileId());
  }

}

