package com.kiga.shopcontent.viewmodel.mapper;

import com.kiga.cms.service.SimpleViewModelMapper;
import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.shopcontent.domain.ProductPage;
import com.kiga.shopcontent.viewmodel.ProductPageViewModel;
import com.kiga.shopcontent.viewmodel.converter.ProductPageToResponseConverter;
import com.kiga.web.service.GeoIpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by asv on 10.11.16.
 */
@Service
public class ProductPageViewModelMapper
  extends SimpleViewModelMapper<ProductPage, ProductPageViewModel> {
  private GeoIpService geoIpService;
  private ProductPageToResponseConverter productPageToResponseConverter;

  @Autowired
  public ProductPageViewModelMapper(
    GeoIpService geoIpService,
    ProductPageToResponseConverter productPageToResponseConverter) {
    this.geoIpService = geoIpService;
    this.productPageToResponseConverter = productPageToResponseConverter;
  }

  /**
   * returns the data for the Product Page by using the cms module.
   */
  @Override
  protected ProductPageViewModel buildViewModel(HttpServletRequest httpServletRequest,
    HttpServletResponse httpServletResponse,
    QueryRequestParameters<ProductPage> queryRequestParameters) throws Exception {
    String countryCode = geoIpService.resolveCountryCode();
    ProductPageViewModel returner = productPageToResponseConverter
      .convertToResponse(queryRequestParameters.getEntity(), countryCode);

    return returner;
  }
}
