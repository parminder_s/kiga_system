package com.kiga.shopcontent.domain.draft;

import com.kiga.content.domain.ss.draft.IdeaCategoryDraft;
import com.kiga.content.domain.ss.draft.SiteTreeDraft;
import com.kiga.shopcontent.domain.ShopCategory;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author bbs
 * @since 9/4/16.
 */
@Entity
@Table(name = "ShopCategory")
public class ShopCategoryDraft extends IdeaCategoryDraft implements ShopCategory<SiteTreeDraft> {
  {
    this.setClassName("ShopCategory");
  }
}
