package com.kiga.shopcontent.domain.live;

import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.shopcontent.domain.ShopCategory;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author bbs
 * @since 9/4/16.
 */
@Entity
@Table(name = "ShopCategory_Live")
public class ShopCategoryLive extends IdeaCategoryLive implements ShopCategory<SiteTreeLive> {
  {
    this.setClassName("ShopCategory");
  }
}
