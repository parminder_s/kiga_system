package com.kiga.shopcontent.repository.querydsl;

import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.RepositoryContexts;
import com.kiga.shopcontent.domain.draft.ProductPageDraft;
import com.kiga.shopcontent.domain.live.ProductPageLive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author bbs
 * @since 3/23/17.
 */
@Service
public class ProductPageQTypeResolver {
  private RepositoryContext repositoryContext;

  @Autowired
  public ProductPageQTypeResolver(RepositoryContext repositoryContext) {
    this.repositoryContext = repositoryContext;
  }

  /**
   * Get entity class based on context.
   *
   * @return entity class based on context
   */
  public Class getEntityClass() {
    if (RepositoryContexts.LIVE.equals(repositoryContext.getContext())) {
      return ProductPageLive.class;
    }
    return ProductPageDraft.class;
  }

  /**
   * Get queryDsl variable name based on context.
   *
   * @return queryDsl variable name based on context
   */
  public String getQueryDslVariableName() {
    if (RepositoryContexts.LIVE.equals(repositoryContext.getContext())) {
      return "productPageLive";
    }
    return "productPageDraft";
  }
}
