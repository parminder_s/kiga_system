package com.kiga.shopcontent.repository.predicates;

import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.CollectionPath;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.StringPath;

/**
 * @author bbs
 * @since 2/4/17.
 */
public class ProductPagePredicates<T> {
  private Class<T> entityClass;
  private String qtypeVariable;

  public ProductPagePredicates(Class<T> entityClass, String qtypeVariable) {
    this.entityClass = entityClass;
    this.qtypeVariable = qtypeVariable;
  }

  /**
   * Get products predicate.
   *
   * @param id id
   * @return predicate
   */
  public Predicate getAllMatchingProductsPredicate(Long id) {
    PathBuilder<T> productPagePath = new PathBuilder<>(entityClass, qtypeVariable);
    PathBuilder product = productPagePath.get("product");
    PathBuilder<T> parentProductPagePath = productPagePath.get("parent", entityClass);
    PathBuilder<T> subParentProductPagePath = parentProductPagePath.get("parent", entityClass);
    PathBuilder<T> subSubParentProductPagePath = subParentProductPagePath
      .get("parent", entityClass);

    return Expressions.anyOf(
      parentProductPagePath.get("id", Long.class).eq(id),
      subParentProductPagePath.get("id", Long.class).eq(id),
      subSubParentProductPagePath.get("id", Long.class).eq(id)
    );
  }

  /**
   * Get products predicate.
   *
   * @param id          id
   * @param countryCode country code
   * @return predicate
   */
  public Predicate getMatchingProductsForCountryPredicate(Long id, String countryCode) {
    PathBuilder<T> productPagePath = new PathBuilder<>(entityClass, qtypeVariable);
    PathBuilder<Product> product = productPagePath.get("product", Product.class);

    CollectionPath productDetailsCollectionPath = product
      .getCollection("productDetails", ProductDetail.class);

    @SuppressWarnings("unchecked")
    PathBuilder<ProductDetail> productDetailPath =
      (PathBuilder<ProductDetail>) productDetailsCollectionPath.any();

    PathBuilder<Country> country = productDetailPath.get("country", Country.class);
    StringPath countryCodePath = country.getString("code");
    BooleanExpression countryCodeMatch = countryCodePath.upper().eq(countryCode);

    PathBuilder<T> parentProductPagePath = productPagePath.get("parent", entityClass);
    PathBuilder<T> subParentProductPagePath = parentProductPagePath.get("parent", entityClass);
    PathBuilder<T> subSubParentProductPagePath = subParentProductPagePath
      .get("parent", entityClass);
    BooleanExpression idMatchesWithAnyParent = Expressions.anyOf(
      parentProductPagePath.get("id", Long.class).eq(id),
      subParentProductPagePath.get("id", Long.class).eq(id),
      subSubParentProductPagePath.get("id", Long.class).eq(id)
    );

    return countryCodeMatch.and(idMatchesWithAnyParent);
  }
}
