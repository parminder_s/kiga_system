package com.kiga.shopcontent.domain;

import com.kiga.content.domain.ss.Page;
import com.kiga.db.KigaEntityModelWithoutId;

import javax.persistence.ConstraintMode;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

/**
 * Links a ProductPage to KigaPage including its children.
 */
@MappedSuperclass
public abstract class Promotion<T extends Page, U extends ProductPage>
  extends KigaEntityModelWithoutId {
  @ManyToOne
  @JoinColumn(name = "pageId",
    foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
  private T page;

  @ManyToOne
  @JoinColumn(name = "productPageId",
    foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
  private U productPage;

  public Promotion() {
    this.setClassName("Promotion");
  }

  public T getPage() {
    return page;
  }

  public void setPage(T page) {
    this.page = page;
  }

  public U getProductPage() {
    return productPage;
  }

  public void setProductPage(U productPage) {
    this.productPage = productPage;
  }
}
