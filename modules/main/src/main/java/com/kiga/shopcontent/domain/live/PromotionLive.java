package com.kiga.shopcontent.domain.live;

import com.kiga.content.domain.ss.live.PageLive;
import com.kiga.shopcontent.domain.Promotion;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/** Created by rainerh on 28.11.16. */
@Entity
@Table(name = "Promotion")
public class PromotionLive extends Promotion<PageLive, ProductPageLive>
    implements com.kiga.db.fixture.Entity {
  @Id
  @Column(name = "ID")
  private Long id;

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }
}
