package com.kiga.shopcontent.viewmodel;

import lombok.Data;

/**
 * Created by asv on 14.11.16.
 */
@Data
public class SliderAdminPageResponse {
  private String productUrl;
}
