package com.kiga.shopcontent.repository.live;

import com.kiga.shopcontent.domain.live.SliderFileLive;
import com.kiga.shopcontent.repository.abstraction.SliderFileRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by asv on 26.12.16.
 */
public interface SliderFileRepositoryLive extends SliderFileRepository,
  CrudRepository<SliderFileLive, Long> {
}
