package com.kiga.shopcontent.domain;

import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;

/**
 * Created by rainerh on 08.11.16.
 *
 * <p>Linked to a ProductPage and can be a video or image (S3Image).</p>
 */
public interface SliderFile<P extends ProductPage> {
  KigaS3File getKigaS3File();

  void setKigaS3File(KigaS3File kigaS3File);

  S3Image getPreviewImage();

  void setPreviewImage(S3Image previewImage);

  P getProductPage();

  void setProductPage(P productPage);

  int getSort();

  void setSort(int sort);

  String getFileType();

  void setFileType(String fileType);

  Long getId();

  void setId(Long id);
}
