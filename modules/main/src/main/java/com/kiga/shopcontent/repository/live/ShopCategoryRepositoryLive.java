package com.kiga.shopcontent.repository.live;

import com.kiga.shopcontent.domain.live.ShopCategoryLive;
import com.kiga.shopcontent.repository.abstraction.ShopCategoryRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 9/6/16.
 */
public interface ShopCategoryRepositoryLive extends ShopCategoryRepository,
  CrudRepository<ShopCategoryLive, Long> {
}
