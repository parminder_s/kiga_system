package com.kiga.shopcontent.web.request;

import lombok.Data;

/**
 * Created by asv on 29.11.16.
 */
@Data
public class PromotionRequest {
  private long pageId;
}
