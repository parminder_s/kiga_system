package com.kiga.shopcontent.domain.draft;

import com.kiga.db.KigaEntityModel;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;
import com.kiga.shopcontent.domain.SliderFile;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by rainerh on 08.11.16.
 */
@Entity
@Table(name = "SliderFile")
public class SliderFileDraft extends KigaEntityModel
  implements SliderFile<ProductPageDraft> {
  @ManyToOne
  @JoinColumn(name = "kigaS3FileId", referencedColumnName = "id")
  private KigaS3File kigaS3File;

  @ManyToOne
  @JoinColumn(name = "previewImageId", referencedColumnName = "id")
  private S3Image previewImage;

  @ManyToOne
  @JoinColumn(name = "productPageId", referencedColumnName = "id")
  private ProductPageDraft productPage;

  private int sort;

  private String fileType;

  public SliderFileDraft() {
    this.setClassName("SliderFile");
  }

  @Override
  public KigaS3File getKigaS3File() {
    return kigaS3File;
  }

  @Override
  public void setKigaS3File(KigaS3File kigaS3File) {
    this.kigaS3File = kigaS3File;
  }

  @Override
  public S3Image getPreviewImage() {
    return previewImage;
  }

  @Override
  public void setPreviewImage(S3Image previewImage) {
    this.previewImage = previewImage;
  }

  @Override
  public ProductPageDraft getProductPage() {
    return productPage;
  }

  @Override
  public void setProductPage(ProductPageDraft productPage) {
    this.productPage = productPage;
  }

  @Override
  public int getSort() {
    return sort;
  }

  @Override
  public void setSort(int sort) {
    this.sort = sort;
  }

  @Override
  public String getFileType() {
    return fileType;
  }

  @Override
  public void setFileType(String fileType) {
    this.fileType = fileType;
  }
}
