package com.kiga.shopcontent.viewmodel;

import com.kiga.cms.web.response.ViewModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * @author bbs
 * @since 9/4/16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ShopCategoryViewModel extends ViewModel {
  private String icon = "icon-cart";
  private List<ShopCategoryProductViewModel> products;
  private boolean lastCategoryLevel;
}
