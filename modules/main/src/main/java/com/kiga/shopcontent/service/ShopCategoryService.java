package com.kiga.shopcontent.service;

import com.kiga.shopcontent.domain.ShopCategory;
import com.kiga.shopcontent.repository.ShopCategoryRepositoryProxy;
import com.kiga.shopcontent.viewmodel.ShopCategoryViewModel;
import com.kiga.shopcontent.viewmodel.converter.ShopCategoryToResponseConverter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 9/6/16.
 */
public class ShopCategoryService {
  private ShopCategoryRepositoryProxy shopCategoryModel;
  private ShopCategoryToResponseConverter shopCategoryToResponseConverter;

  public ShopCategoryService(ShopCategoryRepositoryProxy shopCategoryModel,
                             ShopCategoryToResponseConverter shopCategoryToResponseConverter) {
    this.shopCategoryModel = shopCategoryModel;
    this.shopCategoryToResponseConverter = shopCategoryToResponseConverter;
  }

  public List<ShopCategoryViewModel> getSubcategories(ShopCategoryViewModel returner) {
    return shopCategoryModel.getSubcategories(returner.getId()).stream()
      .map(shopCategoryToResponseConverter::convertToResponse).collect(Collectors.toList());
  }

  public ShopCategoryViewModel getCategory(Long id) {
    ShopCategory categoryEntity = shopCategoryModel.findOne(id);
    return shopCategoryToResponseConverter.convertToResponse(categoryEntity);
  }
}
