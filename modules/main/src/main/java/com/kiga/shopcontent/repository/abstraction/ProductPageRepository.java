package com.kiga.shopcontent.repository.abstraction;

import com.kiga.shopcontent.domain.ProductPage;

/**
 * @author bbs
 * @since 9/6/16.
 */
public interface ProductPageRepository<T extends ProductPage> {
}
