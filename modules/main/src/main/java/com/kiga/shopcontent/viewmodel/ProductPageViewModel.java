package com.kiga.shopcontent.viewmodel;

import com.kiga.cms.web.response.ViewModel;
import com.kiga.ideas.web.response.IdeasResponseRating;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by asv on 10.11.16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ProductPageViewModel extends ViewModel {
  private String url;
  private String previewUrl;
  private String currency;
  private BigDecimal grossPrice;
  private BigDecimal freeShippingLimit;
  private Long minDeliveryDays;
  private Long maxDeliveryDays;
  private long productId;
  private boolean availableInCountry;

  private String name;
  private String description;
  private String largePreviewImageUrl;

  private String categoryName;
  private String content;
  private String ageRange;
  private Long categoryId;

  private List<SliderFileResponse> sliderItems;

  private String countryName;
}
