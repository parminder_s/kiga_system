package com.kiga.shopcontent.repository.abstraction;

import com.kiga.shopcontent.domain.ShopCategory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author bbs
 * @since 9/6/16.
 */
public interface ShopCategoryRepository {
  @Query("select distinct c from #{#entityName} c left join fetch c.children cp where c.parent.id"
    + " = :id")
  List<ShopCategory> getSubcategories(@Param("id") Long id);

  ShopCategory findOne(Long id);
}
