package com.kiga.cms.service.exception;

/**
 * @author bbs
 * @since 7/1/16.
 */
public class ViewModelMapperNotRegisteredException extends Exception {
  public ViewModelMapperNotRegisteredException(Class clazz) {
    super(buildMessage(clazz));
  }

  private static String buildMessage(Class clazz) {
    return "ViewModel mapper for type [" + clazz.getSimpleName() + "] not found";
  }
}
