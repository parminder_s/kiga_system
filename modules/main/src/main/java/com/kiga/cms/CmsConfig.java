package com.kiga.cms;

import com.kiga.cms.service.ViewModelMapperFactory;
import com.kiga.cms.url.resolver.UrlResolver;
import com.kiga.cms.url.resolver.UrlResolverBooleanExpressionCreator;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CmsConfig {
  @Bean
  public UrlResolver defaultUrlResolver(SiteTreeLiveRepository repository) {
    return new UrlResolver(repository, new UrlResolverBooleanExpressionCreator());
  }

  @Bean
  public ViewModelMapperFactory facadeServiceFactory() {
    return new ViewModelMapperFactory();
  }
}
