package com.kiga.cms.web.response;

import lombok.Data;

/**
 * Created by rainerh on 29.11.16.
 *
 * <p>If the viewModel cannot be shown since the user does not have the required permission,
 * we store here the required data for the client to process it properly.</p>
 */
@Data
public class Permission {
  private boolean denied = false;

  private String code = "";

  public Permission() {
  }

  public boolean isDenied() {
    return denied;
  }

  public void setDenied(boolean denied) {
    this.denied = denied;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
