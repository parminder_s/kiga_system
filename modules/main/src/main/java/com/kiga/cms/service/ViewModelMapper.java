package com.kiga.cms.service;

import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.SiteTree;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 7/1/16.
 */
public interface ViewModelMapper<E extends SiteTree, R extends ViewModel> {
  R getViewModel(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
      QueryRequestParameters<E> queryRequestParameters) throws Exception;
}
