package com.kiga.cms.url.resolver.exception;

/**
 * @author bbs
 * @since 6/15/16.
 */
public class InvalidMappingClassException extends Exception {
  public InvalidMappingClassException(String message) {
    super(message);
  }

  public InvalidMappingClassException(Exception exc) {
    super(exc);
  }
}
