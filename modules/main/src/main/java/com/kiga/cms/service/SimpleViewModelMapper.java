package com.kiga.cms.service;

import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.cms.web.response.Breadcrumb;
import com.kiga.cms.web.response.Metadata;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.SiteTree;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 11/27/16.
 */
public abstract class SimpleViewModelMapper<E extends SiteTree, R extends ViewModel>
    implements ViewModelMapper<E, R> {

  protected abstract R buildViewModel(HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse,
      QueryRequestParameters<E> queryRequestParameters) throws
      Exception;

  protected List<Breadcrumb> buildBreadcrumbs(E entity) {
    SiteTree parent = entity;
    List<Breadcrumb> breadcrumbs = new ArrayList<>();
    while (parent != null) {
      breadcrumbs.add(new Breadcrumb(parent.getTitle(), parent.getUrlSegment()));
      parent = parent.getParent();
    }
    if (CollectionUtils.isNotEmpty(breadcrumbs)) {
      if (breadcrumbs.size() > 2) {
        Collections.reverse(breadcrumbs);
        breadcrumbs = breadcrumbs.stream().skip(2).collect(Collectors.toList());
      } else {
        breadcrumbs = new ArrayList<>();
      }
    }
    return breadcrumbs;
  }

  protected Metadata buildMetadata(E entity) {
    Metadata metadata = new Metadata();

    metadata.setTitle(entity.getTitle());
    metadata.setDescription(entity.getMetaDescription());
    metadata.setKeywords(entity.getMetaKeywords());

    return metadata;
  }

  @Override
  public R getViewModel(HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse,
      QueryRequestParameters<E> queryRequestParameters) throws Exception {

    R viewModel = buildViewModel(httpServletRequest, httpServletResponse, queryRequestParameters);

    final E entity = queryRequestParameters.getEntity();
    List<Breadcrumb> breadcrumbs = buildBreadcrumbs(entity);
    viewModel.setBreadcrumbs(breadcrumbs);

    Metadata metadata = buildMetadata(entity);
    viewModel.setMetadata(metadata);
    viewModel.setResolvedType(entity.getClassName());
    viewModel.setId(entity.getId());

    return viewModel;
  }
}
