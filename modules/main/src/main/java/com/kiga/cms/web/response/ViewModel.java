package com.kiga.cms.web.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author bbs
 * @since 6/18/16.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ViewModel {
  private Long id;
  private String className;
  private String title;
  private String urlSegment;
  private String resolvedType;
  private boolean showBreadcrumbs = true;
  private List<Breadcrumb> breadcrumbs;
  private Metadata metadata;
  private Permission permission = new Permission();
}
