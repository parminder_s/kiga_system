package com.kiga.cms.web.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by rainerh on 21.03.17.
 * Artificial viewModel for redirects.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class RedirectViewModel extends ViewModel {
  private String resolvedType = "redirect";
  private String redirectTo;

}
