package com.kiga.cms.url.resolver;

import com.kiga.content.domain.ss.SiteTree;
import com.kiga.main.locale.Locale;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.PathBuilder;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by rainerh on 12.11.16.
 */
public class UrlResolverBooleanExpressionCreator<T extends SiteTree> {
  /**
   * Returns a list of BooleanExpression for QueryDsl to resolve the SiteTree, requested
   * by the urlSegments.
   */
  public List<BooleanExpression> getBooleanExpressions(
    LinkedList<String> urlSegments, Locale locale, Class<T> clazz, String entity) {
    ArrayList<BooleanExpression> returner = new ArrayList<>();
    returner.addAll(homepagePredicate(urlSegments.size(), locale, clazz, entity));
    returner.addAll(recurseUrlSegments(urlSegments, 0, clazz, entity));
    return returner;
  }

  private List<BooleanExpression> homepagePredicate(
    int level, Locale locale, Class<T> clazz, String entity) {
    String classNameProperty = StringUtils.repeat("parent", ".", level);
    return Arrays.asList(
      new PathBuilder<T>(clazz, entity)
        .getString(classNameProperty + ".className").like("HomePage"),
      new PathBuilder<T>(clazz, entity)
        .get(classNameProperty + ".locale").eq(locale));
  }

  private List<BooleanExpression> recurseUrlSegments(
    LinkedList<String> urlSegments, int level, Class<T> clazz, String entity) {
    List<BooleanExpression> returner;
    String urlSegment = urlSegments.pop();
    String urlSegmentProperty = "urlSegment";
    if (level > 0) {
      urlSegmentProperty = StringUtils.repeat("parent", ".", level) + "." + urlSegmentProperty;
    }

    BooleanExpression booleanExpression =
      new PathBuilder<>(clazz, entity).getString(urlSegmentProperty).like(urlSegment);

    if (urlSegments.size() == 0) {
      returner = new ArrayList<>();
      returner.add(booleanExpression);
    } else {
      returner = recurseUrlSegments(urlSegments, level + 1, clazz, entity);
      returner.add(booleanExpression);
    }
    return returner;
  }
}
