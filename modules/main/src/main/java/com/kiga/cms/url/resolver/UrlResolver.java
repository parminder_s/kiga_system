package com.kiga.cms.url.resolver;

import com.kiga.cms.url.resolver.exception.InvalidMappingClassException;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.main.locale.Locale;
import com.mysema.query.types.expr.BooleanExpression;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author bbs
 * @since 6/18/16.
 */
public class UrlResolver<T extends SiteTree>  {
  private QueryDslPredicateExecutor<T> repository;
  private UrlResolverBooleanExpressionCreator<T> expressionCreator;


  public UrlResolver(QueryDslPredicateExecutor<T> repository,
                     UrlResolverBooleanExpressionCreator<T> expressionCreator) {
    this.repository = repository;
    this.expressionCreator = expressionCreator;
  }

  /**
   * Resolve url returning specified entity.
   */
  public ResolvedUrlHierarchyItem<T> resolve(
    String[] urlSegments, Locale locale, Class<T> clazz) throws InvalidMappingClassException {
    ArrayUtils.reverse(urlSegments);

    int urlSegmentsLength = urlSegments.length;
    if (urlSegmentsLength == 0) {
      throw new InvalidMappingClassException("At least one url segment must be provided");
    }

    List<BooleanExpression> expressions =
      this.expressionCreator.getBooleanExpressions(
        new LinkedList<>(Arrays.asList(urlSegments)), locale, clazz, "siteTreeLive");
    T resolvedEntity = repository.findOne(concatExpressions(expressions));
    return new ResolvedUrlHierarchyItem<>(resolvedEntity, urlSegmentsLength);
  }

  private BooleanExpression concatExpressions(List<BooleanExpression> expressions) {
    BooleanExpression returner = expressions.get(0);
    for (int i = 1; i < expressions.size(); i++) {
      returner = returner.and(expressions.get(i));
    }
    return returner;
  }
}
