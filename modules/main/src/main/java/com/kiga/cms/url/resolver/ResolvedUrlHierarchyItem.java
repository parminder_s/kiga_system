package com.kiga.cms.url.resolver;

import com.kiga.content.domain.ss.SiteTree;

/**
 * @author bbs
 * @since 10/6/16.
 */
public class ResolvedUrlHierarchyItem<T extends SiteTree> {
  private T resolvedItem;
  private int hierarchyLevel;

  public ResolvedUrlHierarchyItem() {}

  public ResolvedUrlHierarchyItem(T resolvedItem, int hierarchyLevel) {
    this.resolvedItem = resolvedItem;
    this.hierarchyLevel = hierarchyLevel;
  }

  public T getResolvedItem() {
    return resolvedItem;
  }

  public void setResolvedItem(T resolvedItem) {
    this.resolvedItem = resolvedItem;
  }

  public int getHierarchyLevel() {
    return hierarchyLevel;
  }

  public void setHierarchyLevel(int hierarchyLevel) {
    this.hierarchyLevel = hierarchyLevel;
  }
}
