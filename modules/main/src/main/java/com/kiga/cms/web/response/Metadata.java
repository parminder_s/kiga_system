package com.kiga.cms.web.response;

/**
 * @author bbs
 * @since 11/28/16.
 */
public class Metadata {
  private String title;
  private String description;
  private String keywords;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getKeywords() {
    return keywords;
  }

  public void setKeywords(String keywords) {
    this.keywords = keywords;
  }
}
