package com.kiga.cms.service;

import com.kiga.cms.service.exception.ViewModelMapperNotRegisteredException;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bbs
 * @since 7/1/16.
 */
public class ViewModelMapperFactory {
  private Map<Class, ViewModelMapper> resolverMap = new HashMap<>();

  /**
   * Get mapper service based on the type.
   *
   * @param clazz type for which to return mapper
   * @return facade service
   * @throws ViewModelMapperNotRegisteredException if such a mapper has not been registered
   */
  @SuppressWarnings("unchecked")
  public <T> ViewModelMapper forType(Class<T> clazz) throws
    ViewModelMapperNotRegisteredException {
    if (clazz == null) {
      throw new IllegalArgumentException("Class cannot be null");
    }

    ViewModelMapper returner;
    if (!resolverMap.containsKey(clazz)) {
      returner = resolverMap.get(null);
    } else {
      returner = resolverMap.get(clazz);
    }

    if (returner == null) {
      throw new ViewModelMapperNotRegisteredException(clazz);
    }

    return returner;
  }

  /**
   * Register facade service to be used in the factory.
   *
   * @param viewModelMapper facade Service
   * @param type          type to be handled by the facade service
   * @return the facadeService factory itself
   */
  public <T> ViewModelMapperFactory register(ViewModelMapper viewModelMapper, Class<T> type) {
    resolverMap.put(type, viewModelMapper);
    return this;
  }
}
