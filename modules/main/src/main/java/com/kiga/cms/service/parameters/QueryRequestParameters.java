package com.kiga.cms.service.parameters;

import com.kiga.security.domain.Member;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Optional;

/**
 * @author bbs
 * @since 8/15/16.
 */
@Data
@Builder
@AllArgsConstructor
public class QueryRequestParameters<T> {
  private String[] pathSegments;
  private int ageGroup;
  private T entity;

  @Builder.Default
  private Optional<Member> member = Optional.empty();

  /**
   * no args constructor setting default values.
   */
  public QueryRequestParameters() {
    this.member = Optional.empty();
  }
}
