package com.kiga.cms.web.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bbs
 * @since 9/10/16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Breadcrumb {
  private String label;
  private String urlSegment;
}
