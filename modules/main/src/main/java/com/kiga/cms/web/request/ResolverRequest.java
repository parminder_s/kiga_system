package com.kiga.cms.web.request;

import static java.util.stream.Collectors.joining;

import com.kiga.main.locale.Locale;
import com.kiga.web.message.EndpointRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author bbs
 * @since 6/20/16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ResolverRequest extends EndpointRequest {
  private String [] pathSegments;
  private Integer ageGroup;

  public ResolverRequest() {
  }
}
