package com.kiga.cms.web;

import com.kiga.cms.service.ViewModelMapperFactory;
import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.cms.url.resolver.ResolvedUrlHierarchyItem;
import com.kiga.cms.url.resolver.UrlResolver;
import com.kiga.cms.url.resolver.exception.UrlResolverNotRegisteredException;
import com.kiga.cms.web.request.ResolverRequest;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 6/15/16.
 */
@RestController
@RequestMapping("/cms")
public class CmsController {
  private UrlResolver urlResolver;
  private ViewModelMapperFactory viewModelMapperFactory;

  /**
   * inject dependencies.
   */
  @Autowired
  public CmsController(
    UrlResolver urlResolver,
    ViewModelMapperFactory viewModelMapperFactory) throws
    UrlResolverNotRegisteredException {
    this.urlResolver = urlResolver;
    this.viewModelMapperFactory = viewModelMapperFactory;
  }

  /**
   * Endpoint resolving type by provided path.
   *
   * @return resolved response
   */
  @ResponseBody
  @RequestMapping("/resolve")
  @Cacheable(
    value = "cms",
    key = "#resolverRequest.toString()",
    unless = "!{'IdeaGroupMainContainer', 'IdeaGroupContainer', 'IdeaGroupCategory', "
      + "'IdeaGroup', 'redirect'}.contains(#result.resolvedType)")
  public ViewModel resolve(
    HttpServletRequest httpServletRequest,
    HttpServletResponse httpServletResponse,
    @RequestBody ResolverRequest resolverRequest) throws
    Exception {
    ResolvedUrlHierarchyItem<SiteTreeLive> resolvedItemHolder = urlResolver.resolve(
      resolverRequest.getPathSegments(), resolverRequest.getLocale(), SiteTreeLive.class);
    SiteTreeLive resolvedEntity = resolvedItemHolder.getResolvedItem();
    QueryRequestParameters queryRequestParameters =
      QueryRequestParameters.builder()
        .pathSegments(resolverRequest.getPathSegments())
        .ageGroup(Optional.ofNullable(resolverRequest.getAgeGroup()).orElse(0))
        .entity(resolvedEntity)
        .build();

    @SuppressWarnings("unchecked")
    ViewModel viewModel = viewModelMapperFactory.forType(resolvedEntity.getClass())
      .getViewModel(httpServletRequest, httpServletResponse, queryRequestParameters);
    return viewModel;
  }
}
