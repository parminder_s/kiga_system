package com.kiga.search.repositories;

import com.kiga.search.models.KigaIdeaSearchable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Created by faxxe on 8/10/16.
 */
public interface ElasticIdeaRepository extends ElasticsearchRepository<KigaIdeaSearchable, Long> {

}
