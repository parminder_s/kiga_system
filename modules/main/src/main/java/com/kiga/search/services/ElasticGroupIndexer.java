package com.kiga.search.services;

import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.content.repository.ss.live.IdeaCategoryRepositoryLive;
import com.kiga.search.models.ElasticGroupSearchable;
import com.kiga.search.repositories.ElasticGroupRepository;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by faxxe on 8/11/16.
 */
public class ElasticGroupIndexer {

  private IdeaCategoryRepositoryLive ideaCategoryRepositoryLive;
  private ElasticGroupRepository elasticGroupRepository;


  /**
   * constructor.
   */
  public ElasticGroupIndexer(IdeaCategoryRepositoryLive ideaCategoryRepositoryLive,
                             ElasticGroupRepository elasticGroupRepository) {
    this.ideaCategoryRepositoryLive = ideaCategoryRepositoryLive;
    this.elasticGroupRepository = elasticGroupRepository;
  }

  /**
   * index given or all IdeaCategories.
   */
  public void indexGroups(Iterable<Long> groupIds) {
    Collection<IdeaCategoryLive> ideaCategoryLives;

    if (groupIds == null) {
      ideaCategoryLives = (Collection<IdeaCategoryLive>) ideaCategoryRepositoryLive
        .findAll();
    } else {
      ideaCategoryLives = (Collection<IdeaCategoryLive>) ideaCategoryRepositoryLive
        .findAll(groupIds);
    }

    Collection<ElasticGroupSearchable> elasticGroupSearchables = ideaCategoryLives
      .stream()
      .map(this::toElasticGroupSearchable)
      .collect(Collectors.toList());

    elasticGroupRepository.save(elasticGroupSearchables);
  }

  private ElasticGroupSearchable toElasticGroupSearchable(IdeaCategoryLive ideaCategoryLive) {
    ElasticGroupSearchable returner = new ElasticGroupSearchable();
    returner.setId(ideaCategoryLive.getId());
    returner.setClassName(ideaCategoryLive.getClassName());
    returner.setIconCode(ideaCategoryLive.getIconCode());
    returner.setCreated(ideaCategoryLive.getCreated());
    returner.setSort(ideaCategoryLive.getSort());
    returner.setCode(ideaCategoryLive.getCode());
    returner.setTitle(ideaCategoryLive.getTitle());
    String shortLocale = ideaCategoryLive.getLocale().getValue();
    returner.setLanguageCode(shortLocale);
    return returner;
  }

}
