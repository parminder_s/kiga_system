package com.kiga.search.repositories;

import com.kiga.search.models.KigaGroupSearchable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Created by faxxe on 11/9/16.
 */
public interface KigaGroupSearchableRepository extends
  ElasticsearchRepository<KigaGroupSearchable, Long> {



}
