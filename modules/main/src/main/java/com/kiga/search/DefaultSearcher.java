package com.kiga.search;

import static org.elasticsearch.index.query.FilterBuilders.boolFilter;
import static org.elasticsearch.index.query.FilterBuilders.matchAllFilter;
import static org.elasticsearch.index.query.QueryBuilders.multiMatchQuery;


import com.kiga.search.filters.AgeGroupFilter;
import com.kiga.search.filters.FilterHelper;
import com.kiga.search.filters.LanguageFilter;
import com.kiga.search.filters.NewIdeasFilter;
import com.kiga.search.filters.ParentFilter;
import com.kiga.search.filters.SearchFilter;
import com.kiga.search.models.KigaGroupSearchable;
import com.kiga.search.models.KigaIdeaSearchable;
import com.kiga.search.repositories.KigaGroupSearchableRepository;
import org.elasticsearch.index.query.BoolFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by faxxe on 6/21/16.
 */
public class DefaultSearcher {

  private Logger logger = LoggerFactory.getLogger(getClass());
  private ElasticsearchTemplate elasticsearchTemplate;
  private KigaGroupSearchableRepository groupRepository;

  private String[] fields = {
    "keywords^10", "keywordsDe^10", "keywordsEn^10", "keywordsIt^10", "keywordsTr^10",
    "title^7", "titleDe^7", "titleEn^7", "titleIt^7", "titleTr^7",
    "description^5", "descriptionDe^5", "descriptionEn^5", "descriptionIt^5", "descriptionTr^5",
    "content^1", "contentDe^1", "contentEn^1", "contentIt^1", "contentTr^1",
    "category1TitleDe^3", "category1TitleEn^3", "category1TitleTr^3", "category1TitleIt^3",
    "category2TitleDe^3", "category2TitleEn^3", "category2TitleTr^3", "category2TitleIt^3",
    "category1Title^3", "category2Title^3", "groupNames"
  };

  /**
   * ....
   */
  public DefaultSearcher(ElasticsearchTemplate elasticsearchTemplate,
                         KigaGroupSearchableRepository groupRepository) {
    this.elasticsearchTemplate = elasticsearchTemplate;
    this.groupRepository = groupRepository;
  }

  /**
   * search very elastic.
   */
  public SearchQuery buildSearchQuery(String query, List<SearchFilter> searchFilters) {


    NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder()
      .withQuery(multiMatchQuery(query, fields )
        .operator(MatchQueryBuilder.Operator.AND)
        .type(MultiMatchQueryBuilder.Type.CROSS_FIELDS)
      )
      .withPageable(new PageRequest(0, 10000));

    BoolFilterBuilder boolFilter = boolFilter();

    searchFilters
      .stream()
      .map(this::toFilterBuilder)
      .forEach(boolFilter::must);

    searchQueryBuilder.withFilter(boolFilter);

    return searchQueryBuilder.build();
  }

  /**
   * search also very elastic.
   */
  public SearchResult searchElastic(SearchQuery searchQuery) {
    List<KigaIdeaSearchable> resultList = elasticsearchTemplate
      .queryForList(searchQuery, KigaIdeaSearchable.class);

    Set<Long> ids = resultList.stream()
      .flatMap(result -> result.getGroupIds().stream())
      .collect(Collectors.toSet());

    List<KigaGroupSearchable> groups = new ArrayList<>();

    if (ids.size() > 0 ) {
      groups = (List<KigaGroupSearchable>) groupRepository.findAll(ids);
    }

    SearchResult returner = new SearchResult();
    returner.setIdeas(resultList);
    returner.setTotalHits((long)resultList.size());
    returner.setGroups(groups);
    return returner;
  }

  private FilterBuilder toFilterBuilder(SearchFilter searchFilter) {
    if (searchFilter instanceof NewIdeasFilter) {
      return FilterHelper.getNewIdeasFilter();
    }
    if (searchFilter instanceof AgeGroupFilter) {
      return FilterHelper.getAgeGroupFilter(((AgeGroupFilter)searchFilter).getAgeGroup());
    }
    if (searchFilter instanceof ParentFilter) {
      return FilterHelper.getParentFilter(((ParentFilter)searchFilter).getParentId());
    }
    if (searchFilter instanceof LanguageFilter) {
      return FilterHelper.getLanguageFilter(((LanguageFilter)searchFilter).getLocale().toString());
    }
    return boolFilter().must(matchAllFilter());
  }

  public String explainSync(String query, Long id, String lang) {
    return null;
  }

}
