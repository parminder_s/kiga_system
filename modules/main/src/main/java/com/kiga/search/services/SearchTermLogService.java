package com.kiga.search.services;

import com.kiga.logging.domain.SearchTerm;
import com.kiga.logging.services.LogService;
import com.kiga.search.requests.SearchRequest;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by faxxe on 9/8/16.
 */
public class SearchTermLogService {
  private LogService logService;

  public SearchTermLogService(LogService logService) {
    this.logService = logService;
  }

  /**
   * log a searchquery.
   */
  public void logSearchTerm(SearchRequest searchRequest, HttpServletRequest httpServletRequest,
    Long resultCount) {

    SearchTerm searchTerm = new SearchTerm();
    searchTerm.setSearchTerm(StringUtils.join(searchRequest.getQuery(), " "));
    searchTerm.setLocale(searchRequest.getLocale());
    searchTerm.setAgeGroup(searchRequest.getAgeGroup());
    searchTerm.setParentId(searchRequest.getParentId());
    searchTerm.setResultCount(resultCount);

    logService.logSearchTerm(httpServletRequest, searchRequest.getLocale(), searchTerm);
  }
}
