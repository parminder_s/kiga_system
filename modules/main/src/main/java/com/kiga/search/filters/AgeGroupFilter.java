package com.kiga.search.filters;

/**
 * Created by faxxe on 11/9/16.
 */
public class AgeGroupFilter extends SearchFilter {

  private Long ageGroup;

  public AgeGroupFilter() {
    ageGroup = 0L;
  }

  public AgeGroupFilter(Long ageGroup) {
    this.ageGroup = ageGroup;
  }

  public Long getAgeGroup() {
    return ageGroup;
  }

  public void setAgeGroup(Long ageGroup) {
    this.ageGroup = ageGroup;
  }
}
