package com.kiga.search.filters;

import com.kiga.main.locale.Locale;

/**
 * Created by faxxe on 11/9/16.
 */
public class LanguageFilter extends SearchFilter {

  public LanguageFilter() {
    locale = Locale.de;
  }

  private Locale locale;

  public LanguageFilter(Locale locale) {
    this.locale = locale;
  }

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }
}
