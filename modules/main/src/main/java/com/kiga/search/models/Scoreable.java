package com.kiga.search.models;

/**
 * Created by faxxe on 12/6/16.
 */
public abstract class Scoreable {

  private float score;

  public float getScore() {
    return score;
  }

  public void setScore(float score) {
    this.score = score;
  }
}
