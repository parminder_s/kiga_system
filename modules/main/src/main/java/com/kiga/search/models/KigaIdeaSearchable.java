package com.kiga.search.models;

import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.ideas.web.response.PreviewPageResponse;
import com.kiga.s3.domain.S3File;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


import java.util.Collection;
import java.util.Date;

/**
 * Created by faxxe on 6/21/16.
 */

@Getter
@Setter
@Document(indexName = "kigaindex", type = "idea")
public class KigaIdeaSearchable extends Scoreable {

  @Field(type = FieldType.Long)
  private Long id;
  @Field(type = FieldType.Date)
  private Date created;
  @Field(type = FieldType.Boolean)
  private Boolean facebookShare;
  @Field(type = FieldType.Boolean)
  private Boolean inPool;
  @Field(type = FieldType.String)
  private String ageGroupRange;
  private Collection<Integer> ageGroupList;
  private Collection<Long> hierarchyIds;

  private Collection<String> groupNames;

  private Collection<Long> groupCategoryIds;
  private Collection<Long> groupIds;
  @Field(type = FieldType.Long)
  private Float rating;
  @Field(type = FieldType.Integer)
  private Integer ratingCount;
  @Field(type = FieldType.Integer)
  private Integer sortYear;
  @Field(type = FieldType.String)
  private String url;
  @Field(type = FieldType.String)
  private String ngUrl;
  @Field(type = FieldType.String)
  private String languageCode;
  @Field(type = FieldType.Nested)
  private Collection<PreviewPageResponse> previewPages;
  @Field(type = FieldType.String)
  private String previewUrl;
  @Field(type = FieldType.String)
  private String largePreviewUrl;
  @Field(type = FieldType.Long)
  private Long category1Id;
  @Field(type = FieldType.String)
  private String category1Title;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_english")
  private String category1TitleEn;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_german")
  private String category1TitleDe;
  @Field(type = FieldType.String)
  private String category1TitleEs;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_italian")
  private String category1TitleIt;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_turkish")
  private String category1TitleTr;
  @Field(type = FieldType.Long)
  private Long category2Id;
  @Field(type = FieldType.String)
  private String category2Title;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_german")
  private String category2TitleDe;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_english")
  private String category2TitleEn;
  @Field(type = FieldType.String)
  private String category2TitleEs;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_italian")
  private String category2TitleIt;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_turkish")
  private String category2TitleTr;
  @Field(type = FieldType.String)
  private String content;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_german", searchAnalyzer = "kiga_german")
  private String contentDe;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_english")
  private String contentEn;
  @Field(type = FieldType.String)
  private String contentEs;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_italian")
  private String contentIt;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_turkish")
  private String contentTr;
  @Field(type = FieldType.String)
  private String description;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_german")
  private String descriptionDe;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_english")
  private String descriptionEn;
  @Field(type = FieldType.String)
  private String descriptionEs;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_italian")
  private String descriptionIt;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_turkish")
  private String descriptionTr;
  @Field(type = FieldType.String)
  private String keywords;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_german")
  private String keywordsDe;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_english")
  private String keywordsEn;
  @Field(type = FieldType.String)
  private String keywordsEs;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_italian")
  private String keywordsIt;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_turkish")
  private String keywordsTr;
  @Field(type = FieldType.String)
  private String title;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_german")
  private String titleDe;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_english")
  private String titleEn;
  @Field(type = FieldType.String)
  private String titleEs;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_italian")
  private String titleIt;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_turkish")
  private String titleTr;

  @Field(type = FieldType.String)
  private String audioLink;
  @Field(type = FieldType.String)
  private String videoLink;
  @Field(type = FieldType.Long)
  private Integer articleType;
  @Field(type = FieldType.Object)
  private S3File uploadedFile;

}

