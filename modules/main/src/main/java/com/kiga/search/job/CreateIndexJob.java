package com.kiga.search.job;

import com.kiga.search.services.IndexerService;
import com.kiga.spec.Job;
import com.kiga.spec.Jobable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by faxxe on 10/25/16.
 */
@Job(cronProperty = "indexCreateCron")
public class CreateIndexJob implements Jobable {
  private Logger logger = LoggerFactory.getLogger(getClass());
  private IndexerService indexerService;

  public CreateIndexJob(IndexerService indexerService) {
    this.indexerService = indexerService;
  }

  @Override
  public void runJob() {
    logger.info("Create Index job started");
    indexerService.createNewIndex();
  }
}
