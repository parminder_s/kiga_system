package com.kiga.search.job;

import com.kiga.search.services.IndexerService;
import com.kiga.spec.Job;
import com.kiga.spec.Jobable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by faxxe on 10/25/16.
 */
@Job(cronProperty = "indexUpdateCron")
public class UpdateIndexJob implements Jobable {
  private Logger logger = LoggerFactory.getLogger(getClass());
  private IndexerService indexerService;

  public UpdateIndexJob(IndexerService indexerService) {
    this.indexerService = indexerService;
  }

  @Override
  public void runJob() {
    logger.info("Update Index job started");
    indexerService.updateIndex();
  }

}
