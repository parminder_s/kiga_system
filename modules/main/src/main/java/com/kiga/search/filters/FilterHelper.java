package com.kiga.search.filters;

import static org.elasticsearch.index.query.FilterBuilders.boolFilter;
import static org.elasticsearch.index.query.FilterBuilders.termFilter;


import org.elasticsearch.index.query.FilterBuilder;


/**
 * Created by faxxe on 11/2/16.
 */
public class FilterHelper {

  private FilterHelper() {}

  public static FilterBuilder getLanguageFilter(String language) {
    return boolFilter().must(termFilter("languageCode", language));
  }

  public static FilterBuilder getAgeGroupFilter(long ageGroup) {
    return boolFilter().must(termFilter("ageGroupList", ageGroup));
  }

  public static FilterBuilder getParentFilter(long parent) {
    return boolFilter().must(termFilter("hierarchyIds", parent));
  }

  public static FilterBuilder getNewIdeasFilter() {
    return boolFilter().must(termFilter("inPool", true));
  }

}
