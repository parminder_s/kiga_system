package com.kiga.search.services;

import com.kiga.content.domain.ss.IdeaGroup;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.domain.ss.live.IdeaGroupToIdeaLive;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.repository.ss.live.IdeaGroupToIdeaRepositoryLive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by faxxe on 11/9/16.
 */
public class SiteTreeTraversalService {

  private Logger logger = LoggerFactory.getLogger(DefaultIndexer.class);
  private IdeaGroupToIdeaRepositoryLive ideaGroupToIdeaRepositoryLive;

  /**
   * Helper for traversing the SiteTree.
   */
  public SiteTreeTraversalService(IdeaGroupToIdeaRepositoryLive ideaGroupToIdeaRepositoryLive) {
    this.ideaGroupToIdeaRepositoryLive = ideaGroupToIdeaRepositoryLive;
  }

  /**
   * Traverse SiteTree and get Hierarchy for Single Idea.
   */
  public IdeaHierarchy getIdeaHiearchy(KigaIdeaLive kigaIdeaLive) {
    logger.info("indexing idea: " + kigaIdeaLive.getId());

    Long category1Id = null;
    String category1Title = null;
    Long category2Id = null;
    String category2Title = null;

    SiteTree category1 = kigaIdeaLive.getParent();
    if (category1 != null) {
      category1Id = category1.getId();
      category1Title = category1.getTitle();
      SiteTree category2 = category1.getParent();
      if (category1.getParent() != null) {
        category2Id = category2.getId();
        category2Title = category2.getTitle();
      }
    }

    List<IdeaGroupToIdeaLive> ideaGroupToIdeaLives =
      ideaGroupToIdeaRepositoryLive.findByArticleId(kigaIdeaLive.getId());

    List<IdeaGroupLive> ideaGroups = ideaGroupToIdeaLives.stream()
      .map(IdeaGroupToIdeaLive::getIdeaGroup)
      .filter(Objects::nonNull)
      .collect(Collectors.toList());

    List<Long> groupIds = ideaGroups.stream()
      .map(IdeaGroupLive::getId)
      .collect(Collectors.toList());

    List<List<HierarchyEntry>> groupHierarchies = ideaGroups
      .stream()
      .map(this::getParentsForIdeaGroup)
      .collect(Collectors.toList());


    Set<Long> hierarchyIds = groupHierarchies
      .stream()
      .flatMap(Collection::stream)
      .map(HierarchyEntry::getId)
      .collect(Collectors
      .toSet()) ;

    Set<String> hierarchyNames = groupHierarchies
      .stream()
      .flatMap(Collection::stream)
      .map(HierarchyEntry::getName)
      .collect(Collectors
        .toSet()) ;

    IdeaHierarchy returner = new IdeaHierarchy();

    returner.setHierarchyIds(hierarchyIds);
    returner.setGroupIds(groupIds);
    returner.setGroupNames(hierarchyNames);
    returner.setIdeaGroups(ideaGroups);

    returner.setCategory1Title(category1Title);
    returner.setCategory1Id(category1Id);
    returner.setCategory2Title(category2Title);
    returner.setCategory2Id(category2Id);

    return returner;
  }


  private List<HierarchyEntry> getParentsForIdeaGroup(SiteTree ideaGroupLive) {
    SiteTree parent = ideaGroupLive.getParent();
    List<HierarchyEntry> hierarchy = null;
    if (parent != null) {
      hierarchy = getParentsForIdeaGroup(parent);
    }
    if (hierarchy == null) {
      hierarchy = new ArrayList<>();
    }
    hierarchy.add(new HierarchyEntry(ideaGroupLive.getId(), ideaGroupLive.getTitle()));
    return hierarchy;
  }

  /**
   * getting groupHierarchy.
   */
  public GroupHierarchy getGroupHierarchy(IdeaGroup ideaGroup) {
    return getHierarchyForIdeaGroup(ideaGroup);
  }

  private GroupHierarchy getHierarchyForIdeaGroup(SiteTree ideaGroupLive) {
    SiteTree parent = ideaGroupLive.getParent();
    GroupHierarchy hierarchy = null;
    if (parent != null) {
      hierarchy = getHierarchyForIdeaGroup(parent);
    }
    if (hierarchy == null) {
      hierarchy = new GroupHierarchy();
    }
    hierarchy.getPathIds().add(ideaGroupLive.getId());
    hierarchy.getPathNames().add(ideaGroupLive.getTitle());
    return hierarchy;
  }

}
