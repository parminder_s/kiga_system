package com.kiga.search.converter;

import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.ideas.web.response.IdeasResponseRating;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.search.models.KigaIdeaSearchable;
import com.kiga.web.converter.ResponseConverter;

/**
 * Created by faxxe on 11/16/16.
 */
public class KigaIdeaSearchableConverter implements
  ResponseConverter<KigaIdeaSearchable, IdeasViewModelIdea> {

  /**
   * Idea to ResponseIdea.
   */
  public IdeasViewModelIdea convertToResponse(KigaIdeaSearchable idea) {
    IdeasViewModelIdea ideasResponseIdea = new IdeasViewModelIdea();

    ideasResponseIdea.setId(idea.getId());
    ideasResponseIdea.setTitle(idea.getTitle());
    ideasResponseIdea.setName("" + idea.getId());
    ideasResponseIdea.setDescription(idea.getDescription());
    ideasResponseIdea.setPreviewUrl(idea.getPreviewUrl());
    ideasResponseIdea.setLargePreviewImageUrl(idea.getLargePreviewUrl());
    ideasResponseIdea.setPreviewPages(idea.getPreviewPages());
    ideasResponseIdea.setUrl(idea.getUrl());
    ideasResponseIdea.setNgUrl(idea.getNgUrl());
    ideasResponseIdea.setLevel1Title(idea.getCategory1Title());
    ideasResponseIdea.setLevel2Title(idea.getCategory2Title());
    ideasResponseIdea.setAgeRange(idea.getAgeGroupRange());
    ideasResponseIdea.setRating(new IdeasResponseRating(idea.getRating(), idea.getRatingCount()));
    ideasResponseIdea.setElasticScore(idea.getScore());
    ideasResponseIdea.setInPool(idea.getInPool());
    ideasResponseIdea.setFacebookShare(idea.getFacebookShare());
    ideasResponseIdea.setAudioLink(idea.getAudioLink());
    ideasResponseIdea.setVideoLink(idea.getVideoLink());
    if (ArticleType.DOWNLOAD.equals(ArticleType.valueOf(idea.getArticleType()))) {
      ideasResponseIdea.setDownload(true);
      ideasResponseIdea.setUploadedFileId(idea.getUploadedFile().getId());
      ideasResponseIdea.setUploadedFileName(idea.getUploadedFile().getName());
    }


    return ideasResponseIdea;
  }
}
