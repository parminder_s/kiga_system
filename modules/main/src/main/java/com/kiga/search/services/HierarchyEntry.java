package com.kiga.search.services;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by faxxe on 4/26/17.
 */
@Data
@AllArgsConstructor
public class HierarchyEntry {
  private long id;
  private String name;
}
