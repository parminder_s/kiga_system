package com.kiga.search.services;

import com.kiga.ideas.web.response.IdeaGroupViewModel;
import com.kiga.search.DefaultSearcher;
import com.kiga.search.SearchResult;
import com.kiga.search.converter.KigaIdeaSearchableConverter;
import com.kiga.search.converter.SearchResponseConverter;
import com.kiga.search.filters.AgeGroupFilter;
import com.kiga.search.filters.LanguageFilter;
import com.kiga.search.filters.ParentFilter;
import com.kiga.search.filters.SearchFilter;
import com.kiga.search.models.ElasticGroupSearchable;
import com.kiga.search.repositories.ElasticGroupRepository;
import com.kiga.search.requests.SearchRequest;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by faxxe on 6/19/16.
 */
public class DefaultSearchService implements SearchService {


  private DefaultSearcher searcher;
  private GroupService groupService;
  private ElasticGroupRepository elasticGroupRepository;
  private SearchTermLogService searchTermLogService;
  private Logger logger = LoggerFactory.getLogger(getClass());
  private String searchOrderGroupsBy;
  private SearchResponseConverter searchResponseConverter;
  KigaIdeaSearchableConverter kigaIdeaSearchableConverter;

  /**
   * Constructor.
   */
  public DefaultSearchService(
    DefaultSearcher searcher,
    GroupService groupService,
    ElasticGroupRepository elasticGroupRepository,
    SearchTermLogService searchTermLogService,
    SearchResponseConverter searchResponseConverter,
    KigaIdeaSearchableConverter kigaIdeaSearchableConverter
  ) {

    this.searcher = searcher;
    this.groupService = groupService;
    this.elasticGroupRepository = elasticGroupRepository;
    this.searchTermLogService = searchTermLogService;
    this.searchResponseConverter = searchResponseConverter;
    this.kigaIdeaSearchableConverter = kigaIdeaSearchableConverter;

  }

  private ElasticGroupSearchable getCorrectParent(Long parentId, String lang) {
    ElasticGroupSearchable elasticGroupSearchable = null;
    try {
      elasticGroupSearchable = elasticGroupRepository.findOne(parentId);
      if (elasticGroupSearchable != null
        && elasticGroupSearchable.getLanguageCode().equals(lang)
        && (elasticGroupSearchable.getClassName().equals("IdeaGroupMainContainer")
        || elasticGroupSearchable.getClassName().equals("IdeaGroupContainer")
        || elasticGroupSearchable.getClassName().equals("IdeaGroupCategory")
        || elasticGroupSearchable.getClassName().equals("IdeaGroup"))) {
        return elasticGroupSearchable;
      }
    } catch (EntityNotFoundException entityNotFoundException) {
      logger.warn("Entity with id: " + parentId + "not Found!");
    }
    return elasticGroupRepository
      .findTopByClassNameAndLanguageCode("IdeaGroupMainContainer", lang);
  }

  @Override
  public String explain(String query, Long id, String lang) {

    return searcher.explainSync(query, id, lang);
  }


  /**
   * Search Index kiga for ideas.
   */
  @Override
  public IdeaGroupViewModel search(SearchRequest searchRequest,
    HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

    SearchResult searchResult = doSearch(searchRequest);
    IdeaGroupViewModel ideasViewModel = searchResponseConverter.convertToResponse(searchResult);
    logSearchTerm(searchRequest, httpServletRequest, searchResult.getTotalHits());

    return ideasViewModel;
  }

  @Override
  public IdeaGroupViewModel searchGrid(SearchRequest searchRequest,
                                       HttpServletRequest httpServletRequest,
                                       HttpServletResponse httpServletResponse) {
    SearchResult searchResult = doSearch(searchRequest);
    IdeaGroupViewModel ideasViewModel = searchResponseConverter.convertToResponse(searchResult);
    logSearchTerm(searchRequest, httpServletRequest, searchResult.getTotalHits());
    ideasViewModel.getCategories()
      .forEach(ideasResponseCategory -> ideasResponseCategory.getIdeas().clear());

    int numberOfFoundIdeas = searchResult.getIdeas().size();
    if (numberOfFoundIdeas == 0) {
      return ideasViewModel;
    }
    int searchPageSize = 35; //todo decide where to place as config parameter!
    ideasViewModel.setNumberOfPages(numberOfFoundIdeas / searchPageSize + 1);
    int endIndex = (searchRequest.getPage() * searchPageSize);
    endIndex = (endIndex > numberOfFoundIdeas) ? numberOfFoundIdeas : endIndex;
    int startIndex = (searchRequest.getPage() - 1) * searchPageSize;
    ideasViewModel.getCategories().get(0).setIdeas(
      searchResult.getIdeas().subList(startIndex, endIndex).stream()
      .map(kigaIdeaSearchableConverter::convertToResponse)
      .collect(Collectors.toList()));
    return ideasViewModel;
  }

  /**
   * do search.
   */
  public SearchResult doSearch(SearchRequest searchRequest) {

    if (searchRequest.getQuery().length == 0) {
      return null;
    }

    boolean isRoot = false;

    ElasticGroupSearchable parent = getCorrectParent(searchRequest.getParentId(),
      searchRequest.getLocale().toString());

    Long parentId = parent.getId();
    if (parent.getClassName().compareTo("IdeaGroupMainContainer") == 0) {
      isRoot = true;
    }

    List<SearchFilter> searchFilters = getSearchFilters(searchRequest, parentId);

    String query = StringUtils.join(searchRequest.getQuery(), " ");

    SearchQuery searchQuery = searcher.buildSearchQuery(query.toLowerCase(), searchFilters);

    logger.info("executing query: " + searchQuery.getQuery());
    logger.info("with filters: " + searchQuery.getFilter());

    SearchResult result = searcher.searchElastic(searchQuery);

    logger.info("with " + result.getIdeas().size() + " hits");

    result.setParentId(parentId);
    result.setRoot(isRoot);

    return result;
  }

  private List<SearchFilter> getSearchFilters(SearchRequest searchRequest, Long parentId) {

    List<SearchFilter> searchFilters = new ArrayList<>();
    searchFilters.add(new ParentFilter(parentId));
    searchFilters.add(new LanguageFilter(searchRequest.getLocale()));
    //searchFilters.add(new NewIdeasFilter()); // uncomment this to find 'new ideas'

    if (searchRequest.getAgeGroup() != null && searchRequest.getAgeGroup() != 0) {
      searchFilters.add(new AgeGroupFilter(searchRequest.getAgeGroup()));
    }
    return searchFilters;

  }

  private void logSearchTerm(SearchRequest searchRequest, HttpServletRequest httpServletRequest,
    long resultCount) {
    try {
      searchTermLogService
        .logSearchTerm(searchRequest, httpServletRequest, resultCount);
    } catch (Exception excep) {
      logger.error("Failed logging SearchTerm!");
      logger.error(excep.getMessage());
    }
  }


}
