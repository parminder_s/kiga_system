package com.kiga.search;

import com.amazonaws.services.s3.AmazonS3Client;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.live.IdeaCategoryRepositoryLive;
import com.kiga.content.repository.ss.live.IdeaGroupToIdeaRepositoryLive;
import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.sitemap.SiteMapJob;
import com.kiga.content.sitemap.service.SiteMapGenerator;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.web.response.IdeasResponseCategory;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.logging.services.LogService;
import com.kiga.s3.S3Properties;
import com.kiga.search.comparators.IdeasResponseCategoryComparator;
import com.kiga.search.comparators.ResponseGroupComparator;
import com.kiga.search.converter.ElasticScoreMapper;
import com.kiga.search.converter.KigaGroupSearchableConverter;
import com.kiga.search.converter.KigaIdeaSearchableConverter;
import com.kiga.search.converter.SearchResponseConverter;
import com.kiga.search.job.CreateIndexJob;
import com.kiga.search.job.UpdateIndexJob;
import com.kiga.search.repositories.ElasticGroupRepository;
import com.kiga.search.repositories.ElasticIdeaRepository;
import com.kiga.search.repositories.KigaGroupSearchableRepository;
import com.kiga.search.services.DefaultIndexer;
import com.kiga.search.services.DefaultSearchService;
import com.kiga.search.services.ElasticGroupIndexer;
import com.kiga.search.services.GroupService;
import com.kiga.search.services.IndexerService;
import com.kiga.search.services.SearchService;
import com.kiga.search.services.SearchTermLogService;
import com.kiga.search.services.SiteTreeTraversalService;
import com.kiga.web.converter.ResponseConverter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.node.NodeBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.convert.MappingElasticsearchConverter;
import org.springframework.data.elasticsearch.core.mapping.SimpleElasticsearchMappingContext;

@Configuration
public class SearcherConfig {

  private List<String> groupsNotToBeShown = Arrays.asList("igc-topics");

  @Autowired @Setter @Getter private SearchProperties searchProperties;

  /** Building the SearchService. */
  @Bean
  public SearchService searchService(
      DefaultSearcher searcher,
      GroupService groupService,
      ElasticGroupRepository elasticGroupRepository,
      SearchTermLogService searchTermLogService,
      SearchResponseConverter searchResponseConverter) {
    return new DefaultSearchService(
        searcher,
        groupService,
        elasticGroupRepository,
        searchTermLogService,
        searchResponseConverter,
        new KigaIdeaSearchableConverter());
  }

  @Bean
  public SearchTermLogService serchTermLogService(LogService logService) {
    return new SearchTermLogService(logService);
  }

  /** Building the SearchService. */
  @Bean
  public ElasticGroupIndexer elasticGroupIndexer(
      IdeaCategoryRepositoryLive ideaCategoryRepositoryLive,
      ElasticGroupRepository elasticGroupRepository) {
    return new ElasticGroupIndexer(ideaCategoryRepositoryLive, elasticGroupRepository);
  }

  /** indexer. */
  @Bean
  public DefaultIndexer jdefaultIndexer(
      KigaIdeaRepositoryLive kigaIdeaRepositoryLive,
      ElasticIdeaRepository elasticIdeaRepository,
      SiteTreeTraversalService siteTreeTraversalService,
      ResponseConverter<KigaIdea, IdeasViewModelIdea> ideasResponseIdeaConverter,
      KigaGroupSearchableRepository jkigaGroupSearchableRepository,
      ElasticsearchTemplate elasticsearchTemplate,
      IdeaSearchableConverter ideaSearchableConverter) {
    return new DefaultIndexer(
        kigaIdeaRepositoryLive,
        elasticIdeaRepository,
        siteTreeTraversalService,
        ideasResponseIdeaConverter,
        jkigaGroupSearchableRepository,
        elasticsearchTemplate,
        ideaSearchableConverter,
        searchProperties.getIndexName());
  }

  /** create responseConverter. */
  @Bean
  public SearchResponseConverter searchResponseConverter(
      GroupService groupService,
      ElasticGroupRepository elasticGroupRepository,
      IdeasProperties ideasProperties) {
    Comparator<IdeasResponseCategory> comparator = new IdeasResponseCategoryComparator();
    if ("ELASTICSEARCH".equals(searchProperties.getSearchOrderGroupsBy())) {
      comparator = new ResponseGroupComparator();
    }

    KigaIdeaSearchableConverter jkisc = new KigaIdeaSearchableConverter();
    KigaGroupSearchableConverter jkgsc =
        new KigaGroupSearchableConverter(groupsNotToBeShown, elasticGroupRepository, jkisc);

    return new SearchResponseConverter(
        groupService, comparator, jkgsc, ideasProperties.getAmountPerCategory());
  }

  @Bean
  public IdeaSearchableConverter ideaSearchableConverter() {
    return new IdeaSearchableConverter();
  }

  @Bean
  public SiteTreeTraversalService ideaTraversalService(
      IdeaGroupToIdeaRepositoryLive ideaGroupToIdeaRepositoryLive) {
    return new SiteTreeTraversalService(ideaGroupToIdeaRepositoryLive);
  }

  @Bean
  public NodeBuilder nodeBuilder() {
    return new NodeBuilder();
  }

  /** Elasticsearch Client. */
  @Bean
  public Client client() {
    TransportClient client = new TransportClient();
    TransportAddress address =
        new InetSocketTransportAddress(searchProperties.getHost(), searchProperties.getPort());
    client.addTransportAddress(address);
    return client;
  }

  /** create ElasticsearchTemplate. */
  @Bean
  public ElasticsearchTemplate elasticsearchTemplate(Client client) {
    MappingElasticsearchConverter converter =
        new MappingElasticsearchConverter(new SimpleElasticsearchMappingContext());
    ElasticScoreMapper mapper = new ElasticScoreMapper(converter.getMappingContext());
    return new ElasticsearchTemplate(client, converter, mapper);
  }

  @Bean
  public ElasticsearchOperations elasticsearchOperations(Client client) {
    return new ElasticsearchTemplate(client);
  }

  /** Building the Searcher. */
  @Bean
  public DefaultSearcher searcher(
      ElasticsearchTemplate elasticsearchTemplate,
      KigaGroupSearchableRepository jkigaGroupSearchableRepository) {
    return new DefaultSearcher(elasticsearchTemplate, jkigaGroupSearchableRepository);
  }

  @Bean
  /** Building the GroupService. */
  public GroupService groupService() {
    return new GroupService();
  }

  @Bean
  public UpdateIndexJob updateIndexJob(IndexerService indexerService) {
    return new UpdateIndexJob(indexerService);
  }

  @Bean
  public CreateIndexJob createIndexJob(IndexerService indexerService) {
    return new CreateIndexJob(indexerService);
  }

  @Bean
  SiteMapJob createSiteMapJob(
      S3Properties s3Properties,
      SiteMapGenerator siteMapGenerator,
      SiteTreeLiveRepository siteTreeRepository,
      AmazonS3Client amazonS3Client) {
    return new SiteMapJob(s3Properties, siteMapGenerator, siteTreeRepository, amazonS3Client);
  }
}
