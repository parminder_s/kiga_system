package com.kiga.search.converter;

import com.kiga.ideas.web.response.IdeasResponseCategory;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.search.models.ElasticGroupSearchable;
import com.kiga.search.models.KigaGroupSearchable;
import com.kiga.search.repositories.ElasticGroupRepository;
import com.kiga.web.converter.ResponseConverter;
import java.util.List;
import java.util.stream.Collectors;

/** Created by faxxe on 11/16/16. */
public class KigaGroupSearchableConverter
    implements ResponseConverter<KigaGroupSearchable, IdeasResponseCategory> {

  private List<String> groupsNotToBeShown;
  private KigaIdeaSearchableConverter jkigaIdeaSearchableConverter;
  private ElasticGroupRepository elasticGroupRepository;

  /** constructor. */
  public KigaGroupSearchableConverter(
      List<String> groupsNotToBeShown,
      ElasticGroupRepository elasticGroupRepository,
      KigaIdeaSearchableConverter jkigaIdeaSearchableConverter) {

    this.groupsNotToBeShown = groupsNotToBeShown;
    this.jkigaIdeaSearchableConverter = jkigaIdeaSearchableConverter;
    this.elasticGroupRepository = elasticGroupRepository;
  }

  /** map. */
  @Override
  public IdeasResponseCategory convertToResponse(KigaGroupSearchable kigaGroupSearcher) {
    ElasticGroupSearchable elasticGroupSearchable =
        elasticGroupRepository.findOne(kigaGroupSearcher.getId());

    if (groupsNotToBeShown.contains(elasticGroupSearchable.getCode())) {
      return null;
    }

    IdeasResponseCategory ideasResponseCategory = new IdeasResponseCategory();

    ideasResponseCategory.setId(kigaGroupSearcher.getId());
    List<IdeasViewModelIdea> ideas =
        kigaGroupSearcher
            .getKigaIdeas()
            .stream()
            .map(jkigaIdeaSearchableConverter::convertToResponse)
            .collect(Collectors.toList());

    ideasResponseCategory.setIdeas(ideas);

    ideasResponseCategory.setName("" + kigaGroupSearcher.getId());
    ideasResponseCategory.setTitle(kigaGroupSearcher.getTitle());
    ideasResponseCategory.setLastCategoryLevel(kigaGroupSearcher.isLastLevel());

    String iconCode = elasticGroupSearchable.getIconCode();
    if (iconCode == null) {
      iconCode = "icon-idea";
    }
    ideasResponseCategory.setIcon(iconCode);
    ideasResponseCategory.setSort(elasticGroupSearchable.getSort());

    return ideasResponseCategory;
  }
}
