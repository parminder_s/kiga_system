package com.kiga.search.services;

import com.kiga.content.domain.ss.live.IdeaGroupLive;
import lombok.Getter;
import lombok.Setter;


import java.util.Collection;
import java.util.List;

/**
 * Created by faxxe on 11/9/16.
 */
@Getter
@Setter
public class IdeaHierarchy {

  private Collection<Long> groupIds;
  private Collection<String> groupNames;

  private Collection<Long> hierarchyIds;

  private String category1Title;
  private Long category1Id;
  private String category2Title;
  private Long category2Id;
  private List<IdeaGroupLive> ideaGroups;

}
