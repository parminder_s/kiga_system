package com.kiga.search.services;

import com.kiga.search.SearchProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by faxxe on 10/25/16.
 */
@Service
public class IndexerService {

  Logger logger = LoggerFactory.getLogger(IndexerService.class);

  private DefaultIndexer indexer;
  private ElasticGroupIndexer elasticGroupIndexer;
  private String indexName;

  /**
   * constructor.
   */
  @Autowired
  public IndexerService(DefaultIndexer indexer, ElasticGroupIndexer elasticGroupIndexer,
    SearchProperties searchProperties) {
    this.indexer = indexer;
    this.elasticGroupIndexer = elasticGroupIndexer;
    this.indexName = searchProperties.getIndexName();
  }

  /**
   * reset existing index and create a new one.
   */
  @Async
  @Transactional
  public void createNewIndex() {
    indexer.resetIndex(indexName);
    indexer.createIndex(indexName);
    elasticGroupIndexer.indexGroups(null);
    indexer.indexIdeas(null);
    logger.info("new index created!");
  }

  /**
   * update existing index.
   */
  @Async
  @Transactional
  public void updateIndex() {
    elasticGroupIndexer.indexGroups(null);
    indexer.indexIdeas(null);
    logger.info("index updated!");
  }

  /**
   * reset index completely.
   */
  @Async
  @Transactional
  public void resetIndex() {
    indexer.resetIndex(indexName);
    indexer.createIndex(indexName);
    elasticGroupIndexer.indexGroups(null);
    indexer.indexIdeas(null);
    logger.info("Index resetted!");
  }


  @Async
  @Transactional
  public void deleteIdeaFromIndex(Long id) {
    indexer.deleteIdeaFromIndex(id);
  }

  /**
   * index ideas.
   */
  @Async
  @Transactional
  public void indexIdeas(List<Long> ideas) {
    elasticGroupIndexer.indexGroups(null);
    indexer.indexIdeas(ideas);
    logger.info("ideas indexing finished!");
  }
}
