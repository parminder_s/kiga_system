package com.kiga.search.requests;

import com.kiga.web.message.EndpointRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by faxxe on 5/17/16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SearchRequest extends EndpointRequest {
  private Boolean prettyPrint = false;
  private String[] query;
  private String filters;
  private Boolean draft;
  private Integer pageSize;
  private Integer page;
  private Long ageGroup;
  private Long parentId;
}
