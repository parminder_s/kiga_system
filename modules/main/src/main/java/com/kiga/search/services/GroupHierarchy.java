package com.kiga.search.services;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by faxxe on 11/9/16.
 */
public class GroupHierarchy {

  private List<String> pathNames = new ArrayList<>();
  private List<Long> pathIds = new ArrayList<>();

  public List<String> getPathNames() {
    return pathNames;
  }

  public void setPathNames(List<String> pathNames) {
    this.pathNames = pathNames;
  }

  public List<Long> getPathIds() {
    return pathIds;
  }

  public void setPathIds(List<Long> pathIds) {
    this.pathIds = pathIds;
  }

}
