package com.kiga.search.repositories;

import com.kiga.search.models.ElasticGroupSearchable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Created by faxxe on 8/10/16.
 */
public interface ElasticGroupRepository extends
  ElasticsearchRepository<ElasticGroupSearchable, Long> {

  ElasticGroupSearchable findTopByClassNameAndLanguageCode(String className, String languageCode);
}
