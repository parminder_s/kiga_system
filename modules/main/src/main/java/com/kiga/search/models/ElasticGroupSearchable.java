package com.kiga.search.models;

import org.springframework.data.elasticsearch.annotations.Document;


import java.util.Date;

/**
 * Created by faxxe on 8/11/16.
 */
@Document(indexName = "kigaindex", indexStoreType = "basicGroups")
public class ElasticGroupSearchable {
  private Long id;
  private String title;
  private String languageCode;
  private Date created;

  private String iconCode;
  private String className;
  private Integer sort;
  private String code;


  public String getIconCode() {
    return iconCode;
  }

  public void setIconCode(String iconCode) {
    this.iconCode = iconCode;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getLanguageCode() {
    return languageCode;
  }

  public void setLanguageCode(String languageCode) {
    this.languageCode = languageCode;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public Integer getSort() {
    return sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
