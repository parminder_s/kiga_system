package com.kiga.search;

import org.elasticsearch.client.Client;


import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by faxxe on 6/21/16.
 */
public class KigaElasticClient {

  public Client client;

  /**
   * Constructor.
   */
  public KigaElasticClient(String host, int port) {
    this.client = null;

    //    try {
    //      this.client = TransportClient.builder().build().addTransportAddress(new
    //        InetSocketTransportAddress(InetAddress.getByName(host), 9300));
    //    } catch (UnknownHostException exception) {
    //      exception.printStackTrace();
    //    }
  }

  /**
   * Get indexname.
   */
  public String indexName(boolean draft) {
    return "kigaindex";
  }

  //TODO: ThreadLocal
  //val DATE_WITH_TIME_FORMAT = new ThreadLocal[DateFormat]() {
  //override protected def initialValue(): DateFormat = new SimpleDateFormat
  //("yyyy-MM-dd'T'HH:mm:ss")
  //}

  public static DateFormat DATE_WITH_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");


}
