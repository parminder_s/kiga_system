package com.kiga.search.converter;

import com.kiga.ideas.web.response.IdeaGroupViewModel;
import com.kiga.ideas.web.response.IdeasResponseCategory;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.search.SearchResult;
import com.kiga.search.models.KigaGroupSearchable;
import com.kiga.search.services.GroupService;
import com.kiga.web.converter.ResponseConverter;


import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by faxxe on 11/15/16.
 */
public class SearchResponseConverter
  implements ResponseConverter<SearchResult, IdeaGroupViewModel> {


  private KigaGroupSearchableConverter jkigaGroupSearchableConverter;
  private GroupService groupService;
  private Comparator<IdeasResponseCategory> comparator;
  private int maxIdeasPerRow = 30;


  /**
   * constructor.
   */
  public SearchResponseConverter(GroupService groupService,
                                 Comparator<IdeasResponseCategory> comparator,
                                 KigaGroupSearchableConverter jkigaGroupSearchableConverter,
                                 int maxIdeasPerRow
                                 ) {
    this.groupService = groupService;
    this.comparator = comparator;
    this.maxIdeasPerRow = maxIdeasPerRow;
    this.jkigaGroupSearchableConverter = jkigaGroupSearchableConverter;
  }

  @Override
  public IdeaGroupViewModel convertToResponse(SearchResult result) {
    List<KigaGroupSearchable> groups = groupService
      .groupGroups(result.getGroups(), result.getParentId());

    groupService.addIdeasToGroups(groups, result.getIdeas());

    List<IdeasResponseCategory> responseGroups = groups
      .stream()
      .map(jkigaGroupSearchableConverter::convertToResponse)
      .filter(Objects::nonNull)
      .collect(Collectors.toList());

    responseGroups.sort(comparator);

    IdeaGroupViewModel response = new IdeaGroupViewModel();
    if (groups.size() > 0) {
      KigaGroupSearchable kigaGroupSearcher = groups.get(0);
      int index = kigaGroupSearcher.getPathIds().indexOf(result.getParentId());

      response.setPath(kigaGroupSearcher.getPath().subList(2, index + 1));
      response.setPathIds(kigaGroupSearcher.getPathIds().subList(1, index + 1));

      if (!result.isRoot()) {
        response.setActiveCategory(jkigaGroupSearchableConverter
            .convertToResponse(kigaGroupSearcher));
      }

      boolean isLastNavigationLevel = kigaGroupSearcher.getPath().size() == index + 1;
      if (response.getActiveCategory() != null) {
        response.getActiveCategory().setLastCategoryLevel(isLastNavigationLevel);
      }

      if (!isLastNavigationLevel) {
        for (IdeasResponseCategory tmp : responseGroups) {
          List<IdeasViewModelIdea> ideas = (List<IdeasViewModelIdea>) tmp.getIdeas();
          if (ideas.size() > maxIdeasPerRow) {
            tmp.setIdeas(ideas.subList(0, maxIdeasPerRow));
          }
        }
      }
    }

    response.setCategories(responseGroups);
    return response;
  }



}
