package com.kiga.search.comparators;

import com.kiga.ideas.web.response.IdeasResponseCategory;
import com.kiga.ideas.web.response.IdeasViewModelIdea;


import java.util.Comparator;
import java.util.List;

/**
 * Created by faxxe on 8/16/16.
 */
public class ResponseGroupComparator implements Comparator<IdeasResponseCategory> {

  @Override
  public int compare(IdeasResponseCategory ideasResponseGroup, IdeasResponseCategory t1) {
    if (ideasResponseGroup.getIdeas() != null && ideasResponseGroup.getIdeas().size() > 0
      && t1.getIdeas() != null && t1.getIdeas().size() > 0) {
      List<IdeasViewModelIdea> ideaList = (List<IdeasViewModelIdea>)
        ideasResponseGroup.getIdeas();
      List<IdeasViewModelIdea> t1Ideas = (List<IdeasViewModelIdea>) t1.getIdeas();
      float score1 = ideaList.get(0).getElasticScore();
      float score2 = t1Ideas.get(0).getElasticScore();

      if (score1 > score2) {
        return -1;
      }
      if (score2 > score1) {
        return 1;
      }
    }

    return 0;
  }
}
