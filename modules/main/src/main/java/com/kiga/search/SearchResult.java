package com.kiga.search;

import com.kiga.search.models.KigaGroupSearchable;
import com.kiga.search.models.KigaIdeaSearchable;
import lombok.Getter;
import lombok.Setter;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by faxxe on 6/19/16.
 */
@Getter
@Setter
public class SearchResult {
  private Long totalHits;
  private int page;
  private Long parentId;
  private List<KigaIdeaSearchable> ideas = new ArrayList<>();
  private List<KigaGroupSearchable> groups = new ArrayList<>();
  private boolean isRoot = false;
}
