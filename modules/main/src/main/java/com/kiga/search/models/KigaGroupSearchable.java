package com.kiga.search.models;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Mapping;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by faxxe on 11/3/16.
 */
@Document(indexName = "kigaindex", type = "group")
@Mapping()
public class KigaGroupSearchable {


  @Field(type = FieldType.Long)
  private Long id;
  @Field(type = FieldType.String)
  String title;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_german")
  String titleDe;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_english")
  String titleEn;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_turkish")
  String titleTr;
  @Field(type = FieldType.String, indexAnalyzer = "kiga_italian")
  String titleIt;

  @Field(type = FieldType.String)
  String languageCode;

  List<String> path;

  List<Long> pathIds;

  List<KigaIdeaSearchable> kigaIdeas;

  private boolean isLastLevel = false;


  public KigaGroupSearchable() {

  }

  /**
   * Constructor.
   */
  public KigaGroupSearchable(Long id,
                             String title,
                             List<KigaIdeaSearchable> ideas,
                             List<String> path,
                             List<Long> pathIds) {
    this.id = id;
    this.title = title;
    this.kigaIdeas = ideas;
    this.path = path;
    this.pathIds = pathIds;

    if (pathIds.indexOf(id) == pathIds.size() - 1) {
      isLastLevel = true;
    }
  }



  /**
   * get Parent of KigaGroupSearcher.
   */
  public KigaGroupSearchable getParent(String id) {
    int idx = 0;
    if (id == null || id.compareTo("") == 0) {
      idx = 0;
    } else {
      idx = pathIds.indexOf(id);
      if (idx != -1L) {
        idx += 1L;
      }
    }
    if (idx == -1) {
      return null;
    }
    if (pathIds.size() > idx) {
      return new KigaGroupSearchable(pathIds.get(idx),
        path.get(idx), null, path, pathIds);
    }
    return null;
  }

  /**
   * get Parent of KigaGroupSearcher.
   */
  public KigaGroupSearchable getParent(Long id) {
    int idx = 0;
    if (id == null || id == 0L) {
      idx = 0;
    } else {
      idx = pathIds.indexOf(id);
      if (idx != -1L) {
        idx += 1L;
      }
    }
    if (idx == -1) {
      return null;
    }
    if (pathIds.size() > idx) {
      return new KigaGroupSearchable(pathIds.get(idx),
        path.get(idx), new ArrayList<KigaIdeaSearchable>(), path,
        pathIds);
    }
    if (pathIds.size() == idx) {
      return new KigaGroupSearchable(pathIds.get(idx - 1), path.get(idx - 1),
        new ArrayList<KigaIdeaSearchable>(), path, pathIds);
    }
    return null;
  }

  @Override
  public boolean equals(Object ooo) {
    if (this == ooo) {
      return true;
    }
    if (ooo == null || getClass() != ooo.getClass()) {
      return false;
    }

    KigaGroupSearchable kigaGroupSearcher = (KigaGroupSearchable) ooo;

    if (id != null ? !id.equals(kigaGroupSearcher.id) : kigaGroupSearcher.id != null) {
      return false;
    }
    return title != null ? title.equals(kigaGroupSearcher.title) : kigaGroupSearcher.title == null;

  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (title != null ? title.hashCode() : 0);
    return result;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getTitleDe() {
    return titleDe;
  }

  public void setTitleDe(String titleDe) {
    this.titleDe = titleDe;
  }

  public String getTitleEn() {
    return titleEn;
  }

  public void setTitleEn(String titleEn) {
    this.titleEn = titleEn;
  }

  public String getTitleTr() {
    return titleTr;
  }

  public void setTitleTr(String titleTr) {
    this.titleTr = titleTr;
  }

  public String getTitleIt() {
    return titleIt;
  }

  public void setTitleIt(String titleIt) {
    this.titleIt = titleIt;
  }

  public String getLanguageCode() {
    return languageCode;
  }

  public void setLanguageCode(String languageCode) {
    this.languageCode = languageCode;
  }

  public List<String> getPath() {
    return path;
  }

  public void setPath(List<String> path) {
    this.path = path;
  }

  public List<Long> getPathIds() {
    return pathIds;
  }

  public void setPathIds(List<Long> pathIds) {
    this.pathIds = pathIds;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public List<KigaIdeaSearchable> getKigaIdeas() {
    return kigaIdeas;
  }

  public void setKigaIdeas(List<KigaIdeaSearchable> kigaIdeas) {
    this.kigaIdeas = kigaIdeas;
  }

  public boolean isLastLevel() {
    return isLastLevel;
  }

  public void setLastLevel(boolean lastLevel) {
    isLastLevel = lastLevel;
  }
}
