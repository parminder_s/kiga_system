package com.kiga.search.services;

import com.kiga.search.models.KigaGroupSearchable;
import com.kiga.search.models.KigaIdeaSearchable;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by faxxe on 6/23/16.
 */
public class GroupService {

  public GroupService() {
  }

  /**
   * group groups by parent group.
   */
  public List<KigaGroupSearchable> groupGroups(List<KigaGroupSearchable> groups, Long parent) {
    Set<KigaGroupSearchable> returner = new HashSet<>();
    for (KigaGroupSearchable kigaGroupSearcher : groups) {
      KigaGroupSearchable tmp = kigaGroupSearcher.getParent(parent);
      if (tmp != null) {
        returner.add(tmp);
      }
    }
    return new ArrayList<>(returner);
  }

  /**
   * add ideas to groups.
   */
  public void addIdeasToGroups(List<KigaGroupSearchable> ideasResponseGroups,
                                List<KigaIdeaSearchable> kigaIdeas) {

    for (KigaIdeaSearchable idea : kigaIdeas) {
      for (KigaGroupSearchable kigaGroupSearcher : ideasResponseGroups) {
        if (idea.getHierarchyIds().contains(kigaGroupSearcher.getId())) {
          kigaGroupSearcher.getKigaIdeas().add(idea);
        }
      }
    }
  }

}
