package com.kiga.search.requests;

import com.kiga.web.message.EndpointRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by faxxe on 9/21/16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ExplainRequest extends EndpointRequest {
  private Long id;
  private String query;
}
