package com.kiga.search.converter;

import com.kiga.search.models.Scoreable;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.DefaultResultMapper;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.data.elasticsearch.core.mapping.ElasticsearchPersistentEntity;
import org.springframework.data.elasticsearch.core.mapping.ElasticsearchPersistentProperty;
import org.springframework.data.mapping.context.MappingContext;


import java.util.Iterator;

/**
 * Created by faxxe on 12/6/16.
 * Extend DefaultResultMappter to get the score from ElasticsearchTemplate.
 * http://stackoverflow.com/questions/35175319/spring-data-elasticsearch-metadata-score.
 */
public class ElasticScoreMapper extends DefaultResultMapper {
  /**
   * Results mapper with score support.
   */
  public ElasticScoreMapper(MappingContext<? extends ElasticsearchPersistentEntity<?>,
    ElasticsearchPersistentProperty> mappingContext) {
    super(mappingContext);
  }

  @Override
  public <T> FacetedPage<T> mapResults(SearchResponse response, Class<T> clazz, Pageable pageable) {
    FacetedPage<T> resultPage = super.mapResults(response, clazz, pageable);
    Iterator<T> it = resultPage.getContent().iterator();
    for (SearchHit hit : response.getHits()) {
      if (hit != null) {
        T next = it.next();
        if (next instanceof Scoreable) {
          ((Scoreable) next).setScore(hit.score());
        }
      }
    }
    return resultPage;
  }
}
