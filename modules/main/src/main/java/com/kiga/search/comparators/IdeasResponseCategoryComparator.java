package com.kiga.search.comparators;

import com.kiga.ideas.web.response.IdeasResponseCategory;


import java.util.Comparator;

/**
 * Created by faxxe on 8/22/16.
 */
public class IdeasResponseCategoryComparator implements Comparator<IdeasResponseCategory> {

  @Override
  public int compare(IdeasResponseCategory ideasResponseCategory, IdeasResponseCategory t1) {
    if (ideasResponseCategory != null
      && ideasResponseCategory.getSort() != null
      && t1 != null
      && t1.getSort() != null) {
      if (ideasResponseCategory.getSort() > t1.getSort()) {
        return 1;
      }
      if (t1.getSort() > ideasResponseCategory.getSort()) {
        return -1;
      }
    }
    return 0;
  }
}
