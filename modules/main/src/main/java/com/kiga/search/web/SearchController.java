package com.kiga.search.web;

import com.kiga.ideas.web.response.IdeaGroupViewModel;
import com.kiga.search.requests.ExplainRequest;
import com.kiga.search.requests.SearchRequest;
import com.kiga.search.services.ElasticGroupIndexer;
import com.kiga.search.services.IndexerService;
import com.kiga.search.services.SearchService;
import com.kiga.shop.web.requests.SingleIdRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by faxxe on 6/19/16.
 */
@RestController
@RequestMapping("/api/searcher")
public class SearchController {

  Logger logger = LoggerFactory.getLogger(getClass());

  private ElasticGroupIndexer elasticGroupIndexer;

  private SearchService searchService;

  private IndexerService indexerService;

  /**
   * Constructor with injections.
   */
  @Autowired
  public SearchController(ElasticGroupIndexer elasticGroupIndexer,
                          SearchService searchService,
                          IndexerService indexerService) {
    this.elasticGroupIndexer = elasticGroupIndexer;
    this.searchService = searchService;
    this.indexerService = indexerService;
  }

  @RequestMapping(value = "/reset", method = RequestMethod.GET)
  @ResponseBody
  public String resetIndex() {
    indexerService.resetIndex();
    return "created new, empty index!!";
  }

  /**
   * create a new index and index all ideas.
   */
  @RequestMapping(value = "/create", method = RequestMethod.GET)
  @ResponseBody
  public String createIndex() {
    indexerService.createNewIndex();
    return "Creating new index async!";
  }

  /**
   * update existing index and all ideas.
   */
  @RequestMapping(value = "/update", method = RequestMethod.POST)
  @ResponseBody
  public boolean updateIndex() {
    indexerService.updateIndex();
    return true;
  }


  /**
   * index a single idea.
   */
  @RequestMapping(value = "/unIndexIdea", method = RequestMethod.POST)
  @ResponseBody
  public String unIndexIdea(@RequestBody SingleIdRequest singleIdRequest) {
    indexerService.deleteIdeaFromIndex(singleIdRequest.getId());
    return "Deleteing single Idea async!";
  }

  /**
   * index a single idea.
   */
  @RequestMapping(value = "/indexIdea", method = RequestMethod.POST)
  @ResponseBody
  @Async
  @Transactional
  public String indexIdea(@RequestBody SingleIdRequest singleIdRequest) {
    indexerService.indexIdeas(Collections.singletonList(singleIdRequest.getId()));
    return "Updating single Idea async!";
  }

  /**
   * index a single group.
   */
  @RequestMapping(value = "/indexGroup", method = RequestMethod.POST)
  @ResponseBody
  @Async
  @Transactional
  public void indexSingleGroup(@RequestBody SingleIdRequest singleGroupRequest) {
    elasticGroupIndexer.indexGroups(Collections.singletonList(singleGroupRequest.getId()));
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  @ResponseBody
  public IdeaGroupViewModel search(@RequestBody SearchRequest searchRequest,
      HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
    return searchService.search(searchRequest, httpServletRequest, httpServletResponse);
  }

  @RequestMapping(value = "/searchGrid", method = RequestMethod.POST)
  @ResponseBody
  public IdeaGroupViewModel searchGrid(@RequestBody SearchRequest searchRequest,
                                   HttpServletRequest httpServletRequest,
                                       HttpServletResponse httpServletResponse) {
    return searchService.searchGrid(searchRequest, httpServletRequest, httpServletResponse);
  }

  /**
   * explain idea in query.
   */
  @RequestMapping(value = "/explainIdea", method = RequestMethod.POST)
  public String explainIdea(@RequestBody ExplainRequest explainRequest) {
    return searchService.explain(
      explainRequest.getQuery(),
      explainRequest.getId(),
      explainRequest.getLocale().toString());
  }
}
