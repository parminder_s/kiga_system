package com.kiga.search.services;

import com.kiga.ideas.web.response.IdeaGroupViewModel;
import com.kiga.search.requests.SearchRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by faxxe on 8/23/16.
 */
public interface SearchService {

  IdeaGroupViewModel search(SearchRequest searchRequest, HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse);

  String explain(String query, Long id, String lang);

  IdeaGroupViewModel searchGrid(SearchRequest searchRequest, HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse);
}
