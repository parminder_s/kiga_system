package com.kiga.search.filters;

/**
 * Created by faxxe on 11/9/16.
 */
public class ParentFilter extends SearchFilter {

  private Long parentId;

  public ParentFilter() {
    parentId = 0L;
  }

  public ParentFilter(Long parent) {
    this.parentId = parent;
  }

  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }
}
