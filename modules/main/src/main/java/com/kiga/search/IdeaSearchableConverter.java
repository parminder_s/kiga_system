package com.kiga.search;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.main.locale.Locale;
import com.kiga.main.locale.LocaleConverter;
import com.kiga.search.models.KigaIdeaSearchable;
import com.kiga.search.services.IdeaHierarchy;


import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by faxxe on 10/25/16.
 */
public class IdeaSearchableConverter {

  /**
   * create the Searchable.
   */
  public KigaIdeaSearchable createKigaIdeaSearchable(
    KigaIdea kigaIdea,
    IdeasViewModelIdea ideasViewModelIdea,
    IdeaHierarchy ideaHierarchy
  ) {

    Integer ageGroupNonfinal = kigaIdea.getAgeGroup();

    if (kigaIdea.getAgeGroup() == null) {
      ageGroupNonfinal = 0;
    }

    final Integer ageGroup = ageGroupNonfinal;

    KigaIdeaSearchable jkis = new KigaIdeaSearchable();

    jkis.setGroupIds(ideaHierarchy.getGroupIds());

    jkis.setId(kigaIdea.getId());
    jkis.setCreated(kigaIdea.getCreated());
    jkis.setFacebookShare(ideasViewModelIdea.getFacebookShare());
    jkis.setInPool(ideasViewModelIdea.getInPool());

    jkis.setHierarchyIds(ideaHierarchy.getHierarchyIds());
    jkis.setGroupNames(ideaHierarchy.getGroupNames());

    jkis.setAgeGroupRange(ideasViewModelIdea.getAgeRange());

    jkis.setUrl(ideasViewModelIdea.getUrl());
    jkis.setNgUrl(ideasViewModelIdea.getNgUrl());

    jkis.setAgeGroupList(Stream.of(1, 2, 4, 8, 16, 32, 64, 128)
      .map(x -> (x & ageGroup)).filter(x -> x != 0).collect(Collectors.toList()));

    jkis.setRating(ideasViewModelIdea.getRating().getRating());
    jkis.setRatingCount(ideasViewModelIdea.getRating().getCount());

    int sortYear = Optional.ofNullable(kigaIdea.getSortYearMini())
      .orElse( Optional.ofNullable(kigaIdea.getSortYear()).orElse(0));

    jkis.setSortYear(sortYear);

    Locale locale = LocaleConverter.toLocale(kigaIdea.getLocale().toString());

    jkis.setLanguageCode(locale.toString());

    jkis.setPreviewPages(ideasViewModelIdea.getPreviewPages());

    jkis.setPreviewUrl(ideasViewModelIdea.getPreviewUrl());
    jkis.setLargePreviewUrl(ideasViewModelIdea.getLargePreviewImageUrl());

    jkis.setVideoLink(ideasViewModelIdea.getVideoLink());
    jkis.setAudioLink(ideasViewModelIdea.getAudioLink());

    jkis.setTitle(kigaIdea.getTitle());
    jkis.setContent(kigaIdea.getContent());
    jkis.setDescription(kigaIdea.getDescription());
    jkis.setKeywords(kigaIdea.getMetaKeywords());

    jkis.setCategory1Id(ideaHierarchy.getCategory1Id());
    jkis.setCategory1Title(ideaHierarchy.getCategory1Title());
    jkis.setCategory2Id(ideaHierarchy.getCategory2Id());
    jkis.setCategory2Title(ideaHierarchy.getCategory2Title());

    jkis.setArticleType(kigaIdea.getArticleType().getDatabaseValue());
    jkis.setUploadedFile(kigaIdea.getUploadedFile());
    switch (kigaIdea.getLocale().getValue()) {
      default:
      case "de":
        jkis.setCategory1TitleDe(ideaHierarchy.getCategory1Title());
        jkis.setCategory2TitleDe(ideaHierarchy.getCategory2Title());
        jkis.setDescriptionDe(kigaIdea.getDescription());
        jkis.setContentDe(kigaIdea.getContent());
        jkis.setKeywordsDe(kigaIdea.getMetaKeywords());
        jkis.setTitleDe(kigaIdea.getTitle());
        break;
      case "en":
        jkis.setCategory1TitleEn(ideaHierarchy.getCategory1Title());
        jkis.setCategory2TitleEn(ideaHierarchy.getCategory2Title());
        jkis.setDescriptionEn(kigaIdea.getDescription());
        jkis.setContentEn(kigaIdea.getContent());
        jkis.setKeywordsEn(kigaIdea.getMetaKeywords());
        jkis.setTitleEn(kigaIdea.getTitle());
        break;
      case "it":
        jkis.setCategory1TitleIt(ideaHierarchy.getCategory1Title());
        jkis.setCategory2TitleIt(ideaHierarchy.getCategory2Title());
        jkis.setDescriptionIt(kigaIdea.getDescription());
        jkis.setContentIt(kigaIdea.getContent());
        jkis.setKeywordsIt(kigaIdea.getMetaKeywords());
        jkis.setTitleIt(kigaIdea.getTitle());
        break;
      case "tr":
        jkis.setCategory1TitleTr(ideaHierarchy.getCategory1Title());
        jkis.setCategory2TitleTr(ideaHierarchy.getCategory2Title());
        jkis.setDescriptionTr(kigaIdea.getDescription());
        jkis.setContentTr(kigaIdea.getContent());
        jkis.setKeywordsTr(kigaIdea.getMetaKeywords());
        jkis.setTitleTr(kigaIdea.getTitle());
        break;
    }
    return jkis;
  }

}
