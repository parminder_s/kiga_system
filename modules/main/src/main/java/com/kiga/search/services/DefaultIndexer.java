package com.kiga.search.services;

import com.kiga.content.domain.ss.IdeaGroup;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.ideas.web.service.IdeasResponseIdeaConverter;
import com.kiga.search.IdeaSearchableConverter;
import com.kiga.search.models.ElasticGroupSearchable;
import com.kiga.search.models.KigaGroupSearchable;
import com.kiga.search.models.KigaIdeaSearchable;
import com.kiga.search.repositories.ElasticIdeaRepository;
import com.kiga.search.repositories.KigaGroupSearchableRepository;
import com.kiga.web.converter.ResponseConverter;
import org.apache.commons.collections4.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by faxxe on 11/3/16.
 */
public class DefaultIndexer {

  private KigaIdeaRepositoryLive kigaIdeaRepositoryLive;

  private ElasticIdeaRepository elasticIdeaRepository;
  private KigaGroupSearchableRepository jkigaGroupSearchableRepository;
  private SiteTreeTraversalService siteTreeTraversalService;
  private IdeasResponseIdeaConverter responseIdeaConverter;
  private ElasticsearchTemplate elasticsearchTemplate;
  private IdeaSearchableConverter ideaSearchableConverter;
  private String indexName;

  private List<IdeaGroup> ideaGroups;
  private Logger logger = LoggerFactory.getLogger(DefaultIndexer.class);


  /**
   * Constructor.
   */
  public DefaultIndexer(KigaIdeaRepositoryLive kigaIdeaRepositoryLive,
                        ElasticIdeaRepository elasticIdeaRepository,
                        SiteTreeTraversalService siteTreeTraversalService,
                        ResponseConverter responseIdeaConverter,
                        KigaGroupSearchableRepository jkigaGroupSearchableRepository,
                        ElasticsearchTemplate elasticsearchTemplate,
                        IdeaSearchableConverter ideaSearchableConverter,
                        String indexName
                        ) {

    this.kigaIdeaRepositoryLive = kigaIdeaRepositoryLive;
    this.elasticIdeaRepository = elasticIdeaRepository;
    this.siteTreeTraversalService = siteTreeTraversalService;
    this.responseIdeaConverter = (IdeasResponseIdeaConverter) responseIdeaConverter;
    this.jkigaGroupSearchableRepository = jkigaGroupSearchableRepository;
    this.elasticsearchTemplate = elasticsearchTemplate;
    this.ideaSearchableConverter = ideaSearchableConverter;
    this.indexName = indexName;

  }

  private Object getSettings() {
    Map<String, String> settings = new HashedMap<>();
    settings.put("number_of_shards", "1");
    settings.put("number_of_replicas", "1");
    return settings;
  }

  private void putMappings() {
    elasticsearchTemplate.putMapping(KigaIdeaSearchable.class);
    elasticsearchTemplate.putMapping(KigaGroupSearchable.class);
    elasticsearchTemplate.putMapping(ElasticGroupSearchable.class);
  }

  /**
   * create Index.
   */
  public void createIndex(String indexName) {
    if (!elasticsearchTemplate.indexExists(indexName)) {
      String settings = ElasticsearchTemplate.readFileFromClasspath("/search/settings.json");
      logger.info("indexing with settings: " + settings);
      elasticsearchTemplate.createIndex(indexName, settings);
      putMappings();
    }
  }

  /**
   * reset Index completely.
   */
  public void resetIndex(String indexName) {
    elasticsearchTemplate.deleteIndex(indexName);
    createIndex(indexName);
  }


  /**
   * index ideas.
   */
  public void indexIdeas(List<Long> ids) {
    createIndex("kigaindex");
    ideaGroups = new ArrayList<>();

    List<KigaIdeaLive> kigaIdeaLives;
    if (ids == null || ids.size() == 0 ) {
      kigaIdeaLives = kigaIdeaRepositoryLive.findAll();
    } else {
      kigaIdeaLives = kigaIdeaRepositoryLive.findAll(ids);
    }
    kigaIdeaLives.forEach(this::indexIdea);
  }

  /**
   * index Idea.
   */
  public void indexIdea(KigaIdeaLive kigaIdeaLive) {
    IdeaHierarchy ideaHierarchy = siteTreeTraversalService.getIdeaHiearchy(kigaIdeaLive);
    IdeasViewModelIdea ideasViewModelIdea = responseIdeaConverter.convertToResponse(kigaIdeaLive);

    KigaIdeaSearchable searchable =  ideaSearchableConverter.createKigaIdeaSearchable(
      kigaIdeaLive,
      ideasViewModelIdea,
      ideaHierarchy
    );

    elasticIdeaRepository.save(searchable);

    ideaHierarchy.getIdeaGroups()
      .stream()
      .filter(x -> !ideaGroups.contains(x))
      .peek(x -> ideaGroups.add(x))
      .forEach(this::indexGroup);

  }

  /**
   * index a group.
   */
  public void indexGroup(IdeaGroup ideaGroup) {
    GroupHierarchy groupHierarchy =
      siteTreeTraversalService.getGroupHierarchy(ideaGroup);

    KigaGroupSearchable jkigaGroupSearchable = new KigaGroupSearchable();
    jkigaGroupSearchable.setId(ideaGroup.getId());
    jkigaGroupSearchable.setTitle(ideaGroup.getTitle());
    jkigaGroupSearchable.setPath(groupHierarchy.getPathNames());
    jkigaGroupSearchable.setPathIds(groupHierarchy.getPathIds());

    jkigaGroupSearchableRepository.save(jkigaGroupSearchable);
  }


  public void deleteIdeaFromIndex(Long id) {
    elasticIdeaRepository.delete(id);
  }
}
