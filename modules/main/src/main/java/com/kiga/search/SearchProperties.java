package com.kiga.search;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by faxxe on 11/22/16.
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties("search.elastic")
public class SearchProperties {
  private String index;
  private boolean enableElasticsearch;
  private String host;
  private int port;

  private String indexName;
  private String searchOrderGroupsBy = "SITETREE";
}
