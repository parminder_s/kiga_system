package com.kiga;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class Application {
  /** starts Spring with dev as default profile. */
  public static void main(String[] args) {

    String profile = System.getenv().getOrDefault("SPRING_PROFILES_ACTIVE", "dev");
    new SpringApplicationBuilder(Application.class)
        .bannerMode(Banner.Mode.OFF)
        .profiles(profile)
        .run();
  }
}
