package com.kiga.accounting.web.converter;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.dunning.web.converter.DunningLevelToViewModelConverter;
import com.kiga.accounting.dunning.web.response.DunningLevelViewModel;
import com.kiga.accounting.transactions.domain.PaymentTransaction;
import com.kiga.accounting.web.response.InvoiceViewModel;
import com.kiga.web.converter.DefaultViewModelConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

/**
 * @author bbs
 * @since 2/16/17.
 */
@Service
@Primary
public class InvoiceToViewModelConverter
  extends DefaultViewModelConverter<Invoice, InvoiceViewModel> {

  private DunningLevelToViewModelConverter dunningLevelToViewModelConverter;

  @Autowired
  public InvoiceToViewModelConverter(
    DunningLevelToViewModelConverter dunningLevelToViewModelConverter) {
    this.dunningLevelToViewModelConverter = dunningLevelToViewModelConverter;
  }

  @Override
  public InvoiceViewModel convertToResponse(Invoice entity) {
    if (entity == null) {
      return null;
    }

    InvoiceViewModel invoiceViewModel = new InvoiceViewModel();
    invoiceViewModel.setInvoiceId(entity.getId());
    invoiceViewModel.setInvoiceNumber(entity.getInvoiceNumber());

    invoiceViewModel.setCustomerEmail(entity.getCustomerEmail());
    invoiceViewModel.setCustomerName(entity.getCustomerName());
    invoiceViewModel.setOrderDate(entity.getOrderDate());
    invoiceViewModel.setOrderNumber(entity.getOrderNumber());
    invoiceViewModel.setInvoiceDate(entity.getInvoiceDate());
    invoiceViewModel.setDunningEmailSent(entity.isDunningEmailSent());

    invoiceViewModel.setDueDate(entity.getDueDate());
    invoiceViewModel.setAmount(entity.getAmount());
    invoiceViewModel.setCountryCode(entity.getCountryCode());
    BigDecimal dueAmount = entity.getAmount();
    invoiceViewModel.setSettled(entity.isSettled());

    Long daysBetween = ChronoUnit.DAYS
      .between(Instant.now(), entity.getDueDate().toInstant());
    invoiceViewModel.setDueDays(daysBetween.intValue());
    invoiceViewModel.setCurrency(entity.getCurrency());

    for (PaymentTransaction paymentTransaction : entity.getPaymentTransactions()) {
      dueAmount = dueAmount.subtract(paymentTransaction.getAmount());
    }

    if (dueAmount.compareTo(BigDecimal.ZERO) < 0) {
      dueAmount = BigDecimal.ZERO;
    }

    if (entity.getDunningLevel() != null) {
      DunningLevelViewModel dunningLevelViewModel = dunningLevelToViewModelConverter
        .convertToResponse(entity.getDunningLevel());
      invoiceViewModel.setDunningLevel(dunningLevelViewModel);
    }

    invoiceViewModel.setDueAmount(dueAmount);

    return invoiceViewModel;
  }
}
