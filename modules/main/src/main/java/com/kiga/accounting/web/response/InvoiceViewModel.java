package com.kiga.accounting.web.response;

import com.kiga.accounting.dunning.web.response.DunningLevelViewModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author bbs
 * @since 2/16/17.
 */
@Data
public class InvoiceViewModel {
  private long invoiceId;
  private String invoiceNumber;
  private Date invoiceDate;
  private Date dueDate;
  private int dueDays;
  private String countryCode;
  private BigDecimal amount;
  private BigDecimal dueAmount;
  private boolean settled;
  private boolean dunningEmailSent;
  private DunningLevelViewModel dunningLevel;
  private String customerName;
  private String customerEmail;
  private String orderNumber;
  private Date orderDate;
  private String currency;
}
