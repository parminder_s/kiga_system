package com.kiga.accounting.transactions.service;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.service.InvoiceService;
import com.kiga.accounting.transactions.web.request.FindPaymentTransactionRequest;
import com.kiga.accounting.transactions.web.response.BankViewModel;
import com.kiga.accounting.transactions.web.response.PaymentTransactionViewModel;
import com.kiga.shop.service.CountryShopService;
import com.kiga.shop.web.response.CountryShopViewModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 2/18/17.
 */
@Service
public class BankService {
  private InvoiceService invoiceService;
  private PaymentTransactionService paymentTransactionService;
  private CountryShopService countryShopService;

  /**
   * Constructor.
   *
   * @param invoiceService            service
   * @param paymentTransactionService service
   * @param countryShopService        service
   */
  @Autowired
  public BankService(InvoiceService invoiceService,
    PaymentTransactionService paymentTransactionService, CountryShopService countryShopService) {
    this.invoiceService = invoiceService;
    this.paymentTransactionService = paymentTransactionService;
    this.countryShopService = countryShopService;
  }

  /**
   * Retrieve all banks for dropdown.
   *
   * @return all banks
   */
  public List<BankViewModel> getAllBanks() {
    return countryShopService.getAll().stream()
      .map(this::convertToBankViewModel)
      .filter(bankViewModel -> StringUtils.isNotBlank(bankViewModel.getBankName()))
      .distinct()
      .collect(Collectors.toList());
  }

  /**
   * Get bank by invoice number.
   *
   * @param invoiceNumber invoice number
   * @return bank
   */
  public BankViewModel getByInvoiceNumber(String invoiceNumber) {
    Invoice invoice = invoiceService.getEntityByNumber(invoiceNumber);
    CountryShopViewModel countryShopViewModel = countryShopService
      .getByCode(invoice.getCountryCode());
    return convertToBankViewModel(countryShopViewModel);
  }

  private BankViewModel convertToBankViewModel(CountryShopViewModel countryShopViewModel) {
    BankViewModel bankViewModel = new BankViewModel();
    bankViewModel.setBankName(countryShopViewModel.getBankName());
    return bankViewModel;
  }

  /**
   * Get banks by transaction id.
   *
   * @param paymentTransactionId transaction id
   * @return bank
   */
  public BankViewModel getByPaymentTransactionId(long paymentTransactionId) {
    PaymentTransactionViewModel paymentTransactionViewModel = paymentTransactionService
      .getOne(new FindPaymentTransactionRequest(paymentTransactionId));
    String invoiceNumber = paymentTransactionViewModel.getPaidInvoiceNumber();
    return getByInvoiceNumber(invoiceNumber);
  }
}
