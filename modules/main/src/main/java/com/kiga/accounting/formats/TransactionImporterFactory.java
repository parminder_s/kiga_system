package com.kiga.accounting.formats;

import com.kiga.accounting.domain.AcquirerTransaction;
import com.kiga.accounting.formats.mapper.AlternativeOldPaypalTransactionMapper;
import com.kiga.accounting.formats.mapper.CardCompleteTransactionMapper;
import com.kiga.accounting.formats.mapper.HobexTransactionMapper;
import com.kiga.accounting.formats.mapper.PaypalTransactionMapper;
import com.kiga.accounting.formats.mapper.SofortTransactionMapper;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;

/**
 * Provides configured readers of various formats.
 * Created by mfit on 14.06.17.
 */
public class TransactionImporterFactory {

  /**
   * Factory method to obtain a configured reader for AcquirerTransactions.
   * @param readerType use one of the constants that denote the reader type
   * @param filename the name of the imported file.
   * @return FlatFileItemReader for AcquirerTransactions.
     */
  public static FlatFileItemReader<AcquirerTransaction> getReader(ImportFormat readerType,
                                                                  String filename) {
    switch (readerType) {
      case CC: return ccReaderFactory(filename);
      case SOFORT: return sofortReaderFactory(filename);
      case HOBEX: return hobexReaderFactory(filename);
      case OTHERPAYPAL: return alternativeOldPaypalReaderFactory(filename);
      case PAYPAL: return paypalReaderFactory(filename);
      default: return null;
    }
  }

  /**
   * Configure a reader for Sofort-Csv reports.
   * @param filename name of the file that is imported.
   * @return FlatFileItemReader configured to read from sofort csv report.
   */
  public static FlatFileItemReader<AcquirerTransaction> sofortReaderFactory(String filename) {

    String[] fieldNames = new String[]{"Transaction.user_id", "Transaction.project_id",
      "Transaction.transaction", "Transaction.sender_holder", "Transaction.sender_bank_holder",
      "Transaction.sender_account_number", "Transaction.sender_bank_code",
      "Transaction.sender_bank_name", "Transaction.sender_bank_bic", "Transaction.sender_iban",
      "Transaction.sender_country_id", "Transaction.recipient_holder",
      "Transaction.recipient_account_number", "Transaction.recipient_bank_code",
      "Transaction.recipient_bank_name", "Transaction.recipient_bank_bic",
      "Transaction.recipient_iban", "Transaction.recipient_country_id", "Transaction.amount",
      "Transaction.amount_refunded", "Transaction.currency_id", "Transaction.exchange_rate",
      "Transaction.reason_1", "Transaction.reason_2", "Transaction.test_transaction",
      "Transaction.notification_status", "Transaction.notification_attempts",
      "Transaction.notification_last_attempt", "Transaction.status", "Transaction.status_modified",
      "Transaction.user_costs", "Transaction.user_costs_currency_id",
      "Transaction.user_costs_guarantee", "Transaction.created",
      "Transaction.internationalization_fees"};

    DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
    tokenizer.setDelimiter(";");
    tokenizer.setStrict(false);  // don't throw on number-of-tokens mismatch
    tokenizer.setNames(fieldNames);

    DefaultLineMapper lineMapper = new AcquirerTransactionLineMapper();
    SofortTransactionMapper mapper = new SofortTransactionMapper();
    mapper.setFilename(filename);
    lineMapper.setFieldSetMapper(mapper);
    lineMapper.setLineTokenizer(tokenizer);

    FlatFileItemReader reader = new FlatFileItemReader<AcquirerTransaction>();
    reader.setLinesToSkip(1);  // one header line
    reader.setLineMapper(lineMapper);
    return reader;
  }

  /**
   * Configure a reader for card-complete transaction report in csv format.
   * @param filename name of file that is imported.
   * @return FlatFileItemReader configured to read AcquirerTransaction from card complete report.
   */
  public static FlatFileItemReader<AcquirerTransaction> ccReaderFactory(String filename) {

    String[] fieldNames = new String[]{"Chain", "Vertragsnummer", "Terminal-ID", "Umsatzdatum",
      "Einreichdatum", "Verarbeitungsdatum", "Abrechnungsdatum", "Einreichsumme", "Produkt",
      "Batchnummer", "Kartennummer", "Belegnummer", "Genehmigungsnr", "Kauf-/Umsatzbetrag",
      "Währung", "Originalbetrag", "Orig. Währung", "Akzeptanzpartner", "Auftragsnummer",
      "Stammkundennummer", "Interchange-Kategorie", "Interchange Fee*", "Scheme Fee*",
      "Acquiring Fee (%)*", "Acquiring Fee (EUR)*", "Special Fee (%)*", "Special Fee (EUR)*",
      "Interchange (%)", "Scheme Fee (%)", "Acquiring Fee (%)", "Acquiring Fee (EUR)",
      "Special Fee (%)", "Special Fee (EUR)"};

    DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
    tokenizer.setDelimiter(";");
    tokenizer.setStrict(false);  // don't throw on number-of-tokens mismatch
    tokenizer.setNames(fieldNames);

    DefaultLineMapper lineMapper = new AcquirerTransactionLineMapper();
    CardCompleteTransactionMapper mapper = new CardCompleteTransactionMapper();
    mapper.setFilename(filename);
    lineMapper.setFieldSetMapper(mapper);
    lineMapper.setLineTokenizer(tokenizer);

    FlatFileItemReader reader = new FlatFileItemReader<AcquirerTransaction>();
    reader.setLinesToSkip(1);  // one header line
    reader.setLineMapper(lineMapper);
    return reader;

  }

  /**
   * Configure a reader for Hobex transaction report in csv format.
   * @param filename name of file that is imported.
   * @return FlatFileItemReader configured to read AcquirerTransaction from Hobex report.
   */
  public static FlatFileItemReader<AcquirerTransaction> hobexReaderFactory(String filename) {

    String[] fieldNames = new String[]{"TID", "Paymuldate", "Type", "Clearingdate",
      "Turnoverdate", "Subtype", "Receipt", "Nativeamount", "Nativecurrency",
      "Nativedisagio", "Nativetxfee", "Nativetax", "Nativetransferamount", "Referencenumber",
      "Trans-Ref"};

    DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
    tokenizer.setDelimiter(";");
    tokenizer.setStrict(false);  // don't throw on number-of-tokens mismatch
    tokenizer.setNames(fieldNames);

    DefaultLineMapper lineMapper = new AcquirerTransactionLineMapper();
    HobexTransactionMapper mapper = new HobexTransactionMapper();
    mapper.setFilename(filename);
    lineMapper.setFieldSetMapper(mapper);
    lineMapper.setLineTokenizer(tokenizer);

    FlatFileItemReader reader = new FlatFileItemReader<AcquirerTransaction>();
    reader.setLinesToSkip(1);  // one header line
    reader.setLineMapper(lineMapper);
    return reader;
  }

  /**
   * Configure a reader for Paypal transaction report in csv format.
   * @param filename name of file that is imported.
   * @return FlatFileItemReader configured to read AcquirerTransaction from Paypal report.
   */
  public static FlatFileItemReader<AcquirerTransaction> alternativeOldPaypalReaderFactory(
    String filename) {

    String[] fieldNames = new String[]{"Transaktionscode",
      "Zugehöriger Transaktionscode",
      "Datum",
      "Typ",
      "Betreff",
      "Artikelnummer",
      "Artikelbezeichnung",
      "Rechnungsnummer",
      "Name",
      "E-Mail",
      "Versandname",
      "Versandadresse Zeile 1",
      "Versandadresse Zeile 2",
      "Lieferadresse Ort",
      "Versandadresse Staat/Provinz",
      "Versandadresse PLZ",
      "Lieferadresse Land",
      "Versandmethode",
      "Adress-Status",
      "Telefon",
      "Bruttobetrag",
      "Empfangsnummer",
      "Benutzerdefiniertes Feld",
      "Name Option 1",
      "Wert Option 1",
      "Name Option 2",
      "Wert Option 2",
      "Hinweis",
      "Auktions-Site",
      "eBay-Mitgliedsname",
      "Artikel-URL",
      "Auktionsende",
      "Versicherungsbetrag",
      "Währung",
      "Gebühren",
      "Nettobetrag",
      "Versandbetrag",
      "Umsatzsteuerbetrag",
      "An E-Mail",
      "Uhrzeit",
      "Zeitzone",
      "Rechnungsadresse Zeile 1",
      "Rechnungsadresse Zeile 2",
      "Rechnungsadresse Ort",
      "Rechnungsadresse Staat/Provinz",
      "Rechnungsadresse PLZ",
      "Rechnungsadresse Land",
      "Filiale",
      "Kasse",
    };

    DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
    tokenizer.setDelimiter(",");
    tokenizer.setStrict(false);  // don't throw on number-of-tokens mismatch
    tokenizer.setNames(fieldNames);

    DefaultLineMapper lineMapper = new AcquirerTransactionLineMapper();
    AlternativeOldPaypalTransactionMapper mapper = new AlternativeOldPaypalTransactionMapper();
    mapper.setFilename(filename);
    lineMapper.setFieldSetMapper(mapper);
    lineMapper.setLineTokenizer(tokenizer);

    FlatFileItemReader reader = new FlatFileItemReader<AcquirerTransaction>();
    reader.setLinesToSkip(1);  // one header line
    reader.setLineMapper(lineMapper);
    return reader;

  }

  /**
   * Configure a reader for Paypal transaction report in csv format.
   * @param filename name of file that is imported.
   * @return FlatFileItemReader configured to read AcquirerTransaction from Paypal report.
   */
  public static FlatFileItemReader<AcquirerTransaction> paypalReaderFactory(String filename) {

    String[] fieldNames = new String[]{
      "Datum", "Uhrzeit", "Zeitzone", "Beschreibung", "Währung", "Brutto", "Gebühr", "Netto",
      "Guthaben", "Transaktionscode", "Absender E-Mail-Adresse", "Name", "Name der Bank",
      "Bankkonto", "Versand- und Bearbeitungsgebühr", "Umsatzsteuer", "Rechnungsnummer",
      "Zugehöriger Transaktionscode"
    };

    DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
    tokenizer.setDelimiter(",");
    tokenizer.setStrict(false);  // don't throw on number-of-tokens mismatch
    tokenizer.setNames(fieldNames);

    DefaultLineMapper lineMapper = new AcquirerTransactionLineMapper();
    PaypalTransactionMapper mapper = new PaypalTransactionMapper();
    mapper.setFilename(filename);
    lineMapper.setFieldSetMapper(mapper);
    lineMapper.setLineTokenizer(tokenizer);

    FlatFileItemReader reader = new FlatFileItemReader<AcquirerTransaction>();
    reader.setLinesToSkip(1);  // one header line
    reader.setLineMapper(lineMapper);
    return reader;

  }
}
