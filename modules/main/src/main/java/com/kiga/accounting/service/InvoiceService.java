package com.kiga.accounting.service;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.domain.QInvoice;
import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.accounting.web.converter.InvoiceToViewModelConverter;
import com.kiga.accounting.web.response.InvoiceViewModel;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.BooleanPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 2/11/17.
 */
@Service
public class InvoiceService {
  private InvoiceRepository invoiceRepository;
  private InvoiceToViewModelConverter invoiceToViewModelConverter;

  @Autowired
  public InvoiceService(InvoiceRepository invoiceRepository,
    InvoiceToViewModelConverter invoiceToViewModelConverter) {
    this.invoiceRepository = invoiceRepository;
    this.invoiceToViewModelConverter = invoiceToViewModelConverter;
  }

  public Invoice getEntityByNumber(String number) {
    return invoiceRepository.findByInvoiceNumber(number);
  }

  /**
   * update invoice with information whether the email has been set.
   *
   * @param invoiceNumber invoice number
   */
  public void markDunningEmailSent(String invoiceNumber) {
    Invoice invoice = invoiceRepository.findByInvoiceNumber(invoiceNumber);
    invoice.setDunningEmailSent(true);
    invoiceRepository.save(invoice);
  }

  public InvoiceViewModel getOne(String invoiceNumber) {
    return invoiceToViewModelConverter.convertToResponse(getEntityByNumber(invoiceNumber));
  }

  public InvoiceViewModel getOne(long id) {
    return invoiceToViewModelConverter.convertToResponse(invoiceRepository.findById(id));
  }

  public void save(Invoice invoice) {
    invoiceRepository.save(invoice);
  }

  /**
   * Gets all overdue invoices.
   *
   * @return overdue invoices
   */
  public List<InvoiceViewModel> getAllOverdue() {
    BooleanPath settledPath = QInvoice.invoice.settled;
    BooleanExpression invoiceIsNotSettledExpr = BooleanExpression
      .anyOf(settledPath.isNull(), settledPath.isFalse());
    return invoiceToViewModelConverter
      .convertToResponse(invoiceRepository.findAll(invoiceIsNotSettledExpr)).stream()
      .filter(this::hasDueAmount).collect(Collectors.toList());
  }

  private boolean hasDueAmount(InvoiceViewModel invoiceViewModel) {
    return BigDecimal.ZERO.compareTo(invoiceViewModel.getDueAmount()) < 0;
  }

  public Invoice getEntity(Long invoiceId) {
    return invoiceRepository.findOne(invoiceId);
  }
}
