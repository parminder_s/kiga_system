package com.kiga.accounting.transactions.web.converter;

import com.kiga.accounting.transactions.domain.PaymentTransaction;
import com.kiga.accounting.transactions.web.response.PaymentTransactionViewModel;
import com.kiga.accounting.web.converter.InvoiceToViewModelConverter;
import com.kiga.accounting.web.response.InvoiceViewModel;
import com.kiga.web.converter.DefaultViewModelConverter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author bbs
 * @since 2/10/17.
 */
@Service
public class PaymentTransactionToViewModelConverter
  extends DefaultViewModelConverter<PaymentTransaction, PaymentTransactionViewModel> {

  private InvoiceToViewModelConverter invoiceToViewModelConverter;
  private ModelMapper modelMapper;

  @Autowired
  public PaymentTransactionToViewModelConverter(
    InvoiceToViewModelConverter invoiceToViewModelConverter, ModelMapper modelMapper) {
    this.invoiceToViewModelConverter = invoiceToViewModelConverter;
    this.modelMapper = modelMapper;
  }

  @Override
  public PaymentTransactionViewModel convertToResponse(PaymentTransaction entity) {
    PaymentTransactionViewModel returner = modelMapper
      .map(entity, PaymentTransactionViewModel.class);
    InvoiceViewModel invoiceViewModel = invoiceToViewModelConverter
      .convertToResponse(entity.getInvoice());
    returner.setInvoiceModel(invoiceViewModel);
    returner.setPaidInvoiceNumber(invoiceViewModel.getInvoiceNumber());

    return returner;
  }
}
