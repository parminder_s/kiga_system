package com.kiga.accounting.formats.export;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by mfit on 13.06.16.
 */
public class AccountingRow {
  private Date rcptDate;
  private Date rcptTransferDate;
  private String countryCode;
  private BigDecimal rcptAmount;
  private String rcptCurrencyCode;
  private String invoiceNr;
  private Date invoiceDate;
  private BigDecimal invoiceTotal;
  private BigDecimal invoiceTaxTotal;
  private String invoiceCurrencyCode;
  private String rcptAcquirerTid;

  public Date getRcptDate() {
    return rcptDate;
  }

  public void setRcptDate(Date rcptDate) {
    this.rcptDate = rcptDate;
  }

  public Date getRcptTransferDate() {
    return rcptTransferDate;
  }

  public void setRcptTransferDate(Date rcptTransferDate) {
    this.rcptTransferDate = rcptTransferDate;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public BigDecimal getRcptAmount() {
    return rcptAmount;
  }

  public void setRcptAmount(BigDecimal rcptAmount) {
    this.rcptAmount = rcptAmount;
  }

  public String getRcptCurrencyCode() {
    return rcptCurrencyCode;
  }

  public void setRcptCurrencyCode(String rcptCurrencyCode) {
    this.rcptCurrencyCode = rcptCurrencyCode;
  }

  public String getInvoiceNr() {
    return invoiceNr;
  }

  public void setInvoiceNr(String invoiceNr) {
    this.invoiceNr = invoiceNr;
  }

  public Date getInvoiceDate() {
    return invoiceDate;
  }

  public void setInvoiceDate(Date invoiceDate) {
    this.invoiceDate = invoiceDate;
  }

  public BigDecimal getInvoiceTotal() {
    return invoiceTotal;
  }

  public void setInvoiceTotal(BigDecimal invoiceTotal) {
    this.invoiceTotal = invoiceTotal;
  }

  public BigDecimal getInvoiceTaxTotal() {
    return invoiceTaxTotal;
  }

  public void setInvoiceTaxTotal(BigDecimal invoiceTaxTotal) {
    this.invoiceTaxTotal = invoiceTaxTotal;
  }

  public String getInvoiceCurrencyCode() {
    return invoiceCurrencyCode;
  }

  public void setInvoiceCurrencyCode(String invoiceCurrencyCode) {
    this.invoiceCurrencyCode = invoiceCurrencyCode;
  }

  public String getRcptAcquirerTid() {
    return rcptAcquirerTid;
  }

  public void setRcptAcquirerTid(String rcptAcquirerTid) {
    this.rcptAcquirerTid = rcptAcquirerTid;
  }
}

