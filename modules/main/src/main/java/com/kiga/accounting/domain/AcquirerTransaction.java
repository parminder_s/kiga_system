package com.kiga.accounting.domain;

import com.kiga.db.KigaEntityModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Transient;


/**
 * Created by mfit on 09.03.16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class AcquirerTransaction extends KigaEntityModel {

  private Date transactionDate;
  private Date transactionDateAcquirer;
  private Date processingDate;
  private Date settlementDate;

  private String cardType;
  private BigDecimal amountOrig;
  private String currencyOrig;
  private BigDecimal amount;
  private String currency;

  private String acquirerName;
  private String pspName;

  private String pspTransactionId;
  private String acquirerTransactionId;

  private BigDecimal batchSum;
  private String batchNr;

  private String destinationBankBic;

  private Integer parseStatus;
  private String sourceContent;
  private String sourceFileName;

  /**
   * Test same basic fields that we at least want to have if we persist to db.
   * @return whether transaction is valid for import.
   */
  public Boolean isValid() {
    return transactionDate != null && amountOrig != null;
  }

  @Transient
  private Boolean isDuplicate;

  @Transient
  private Boolean isImportable;

  /**
   * Distinguish transaction types - only transactions with the primary flag should
   * directly be related to payments. For now, only import primary transactions.
   */
  @Transient
  private Boolean isPrimaryTransaction = true;

  @PrePersist
  public void prePersist() {
    setClassName("AcquirerTransaction");
  }

}
