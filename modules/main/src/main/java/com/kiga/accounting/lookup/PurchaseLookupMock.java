package com.kiga.accounting.lookup;

import com.kiga.shop.domain.Purchase;

import java.util.HashMap;

/**
 * Created by mfit on 06.06.16.
 */
public class PurchaseLookupMock implements PurchaseLookup {
  public HashMap<Long, Purchase> items = new HashMap<>();

  public HashMap<Long, Purchase> getItems() {
    return this.items;
  }

  @Override
  public Purchase findByPurchaseId(Long pid) {
    return items.get(pid);
  }
}
