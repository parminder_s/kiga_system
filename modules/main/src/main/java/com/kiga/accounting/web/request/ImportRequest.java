package com.kiga.accounting.web.request;

import com.kiga.accounting.formats.ImportFormat;
import lombok.Data;

/**
 * Created by mfit on 31.05.16.
 * Request for importing data in text/csv format.
 */
@Data
public class ImportRequest {
  private String csv;
  private String filename;
  private ImportFormat formatCode;
  private Boolean doImport;
}

