package com.kiga.accounting.dunning.domain;

import com.kiga.accounting.domain.Invoice;
import com.kiga.db.KigaEntityModel;
import com.kiga.shop.domain.Country;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

/**
 * @author bbs
 * @since 2/19/17.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ToString
@Entity
public class DunningLevel extends KigaEntityModel {
  private String code;
  private int days;
  private BigDecimal feesAmount;
  private int dunningLevel;

  @ManyToOne
  @JoinColumn(name = "countryId", referencedColumnName = "id")
  private Country country;

  @OneToMany(mappedBy = "dunningLevel", orphanRemoval = false)
  private List<Invoice> invoices;

  @PrePersist
  public void prePersist() {
    setClassName("DunningLevel");
  }
}
