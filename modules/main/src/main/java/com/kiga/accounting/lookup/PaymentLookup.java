package com.kiga.accounting.lookup;

import com.kiga.payment.domain.Payment;

/**
 * Created by mfit on 06.06.16.
 */
public interface PaymentLookup {
  public Payment findByPaymentProviderId(String pid);
}
