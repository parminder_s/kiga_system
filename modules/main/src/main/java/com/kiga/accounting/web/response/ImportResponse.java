package com.kiga.accounting.web.response;

import com.kiga.accounting.domain.AcquirerTransaction;
import lombok.Data;

import java.util.List;

/**
 * Created by mfit on 31.05.16.
 * Result of an import operation.
 */
@Data
public class ImportResponse {
  private String status;
  private List<AcquirerTransaction> data;
  private int rowsParsed;
  private int rowsPersisted;
  private int rowsError;

  /**
   * Constructor that initialises all values.
   * @param status Numerical status.
   * @param data List of data-rows.
   * @param rowsParsed Number of rows parsed.
   * @param rowsPersisted Number of rows saved to database.
   * @param rowsError Number of rows with errors.
   */
  public ImportResponse(String status, List<AcquirerTransaction> data, int rowsParsed,
                        int rowsPersisted, int rowsError) {
    this.status = status;
    this.data = data;
    this.rowsParsed = rowsParsed;
    this.rowsPersisted = rowsPersisted;
    this.rowsError = rowsError;
  }

  public ImportResponse() {
  }
}
