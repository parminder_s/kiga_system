package com.kiga.accounting.lookup;

import com.kiga.payment.domain.Payment;

import java.util.HashMap;

/**
 * Created by mfit on 06.06.16.
 */
public class PaymentLookupMock implements PaymentLookup {
  public HashMap<String, Payment> items = new HashMap<>();

  public HashMap<String, Payment> getItems() {
    return this.items;
  }

  public Payment findByPaymentProviderId(String paymentId) {
    return this.items.get(paymentId);
  }
}
