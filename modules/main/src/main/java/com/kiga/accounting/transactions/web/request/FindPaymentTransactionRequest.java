package com.kiga.accounting.transactions.web.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bbs
 * @since 2/15/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FindPaymentTransactionRequest {
  private long id;
}
