package com.kiga.accounting.formats;

/**
 * Created by mfit on 25.07.17.
 */
public enum ImportFormat {
  CC,
  SOFORT,
  HOBEX,
  PAYPAL,
  OTHERPAYPAL;
}
