package com.kiga.accounting.dunning.web.request;

import lombok.Data;

import java.util.List;

/**
 * @author bbs
 * @since 2/24/17.
 */
@Data
public class RemoveDunningLevelRequest {
  private List<Long> ids;
}
