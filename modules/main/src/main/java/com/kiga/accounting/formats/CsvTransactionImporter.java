package com.kiga.accounting.formats;

import com.kiga.accounting.domain.AcquirerTransaction;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Collection of static methods to read in items from a transaction import.
 * Created by mfit on 30.05.16.
 */
public class CsvTransactionImporter {

  /**
   * Read transactions from a reader. In case of parse errors, an empty transaction with
   * zero parse-status is be inserted.
   * @param reader the reader to read from.
   * @return A list of transactions.
   */
  public static List<AcquirerTransaction> readItems(
    FlatFileItemReader<AcquirerTransaction> reader) {
    AcquirerTransaction item = null;
    List<AcquirerTransaction> result = new ArrayList<AcquirerTransaction>();
    reader.open(new ExecutionContext());
    do {
      try {
        item = reader.read();
        if (item != null) {
          item.setParseStatus(1);
          result.add(item);
        }
      } catch (Exception exc) {
        // Add an empty, non-parsed item
        item = new AcquirerTransaction();
        item.setParseStatus(0);
        result.add(item);
      }
    }
    while (item != null);
    return result;
  }


  /**
   * Read items from file.
   *
   * @param filename Name of the file.
   * @param resourceType Flag denoting the reader-type.
   * @return List of transactions.
   */
  public static List<AcquirerTransaction> readItemsFromFile(String filename,
                                                            ImportFormat resourceType) {
    FlatFileItemReader<AcquirerTransaction> reader =
      TransactionImporterFactory.getReader(resourceType, filename);
    reader.setResource(new FileSystemResource(filename));
    return readItems(reader);
  }

  /**
   * Read items from string.
   *
   * @param content File contents as string.
   * @param resourceType Flag denoting the reader-type.
   * @return List of transactions.
   */
  public static List<AcquirerTransaction> readItemsFromString(String content,
                                                              ImportFormat resourceType)
    throws UnsupportedEncodingException {
    FlatFileItemReader<AcquirerTransaction> reader =
      TransactionImporterFactory.getReader(resourceType, "");
    InputStreamResource resource = new InputStreamResource(
      new ByteArrayInputStream(content.getBytes("UTF-8")));
    reader.setResource(resource);
    return readItems(reader);
  }

  /**
   * Read items from a resource.
   *
   * @param resource Resource with content to import.
   * @param resourceType Flag denoting the reader-type.
   * @param resourceName Name to store with every row to determine the import batch.
   * @return List of transactions.
   */
  public static List<AcquirerTransaction> readItemsFromResource(Resource resource,
                                                                ImportFormat resourceType,
                                                                String resourceName) {
    FlatFileItemReader<AcquirerTransaction> reader =
      TransactionImporterFactory.getReader(resourceType, resourceName);
    reader.setResource(resource);
    return readItems(reader);
  }
}
