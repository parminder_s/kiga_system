package com.kiga.accounting.transactions.web.request;

import lombok.Data;

import java.util.List;

/**
 * @author bbs
 * @since 2/10/17.
 */
@Data
public class RemovePaymentTransactionRequest {
  private List<Long> ids;
}
