package com.kiga.accounting.repository;

import com.kiga.accounting.domain.AcquirerTransaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by mfit on 31.05.16.
 */
public interface AcquirerTransactionRepository
  extends CrudRepository<AcquirerTransaction, java.lang.Long> {


  public List<AcquirerTransaction> findByPspTransactionIdAndTransactionDate(
    String pspId, Date transactionDate);


  @Query("select t from AcquirerTransaction t where t.transactionDate >= :fromDate AND "
       + "t.transactionDate < :toDate")
  public List<AcquirerTransaction> findByDateRange(@Param("fromDate") Date fromDate,
                                                   @Param("toDate") Date toDate);
}


