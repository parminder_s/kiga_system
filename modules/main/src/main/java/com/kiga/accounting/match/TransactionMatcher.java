package com.kiga.accounting.match;

import com.kiga.accounting.domain.AcquirerTransaction;
import com.kiga.accounting.formats.export.AccountingFieldExtractor;
import com.kiga.accounting.formats.export.AccountingRow;
import com.kiga.accounting.helper.CsvWriterFactory;
import com.kiga.accounting.helper.StreamItemWriter;
import com.kiga.accounting.lookup.PaymentLookup;
import com.kiga.accounting.lookup.PurchaseLookup;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.FieldExtractor;
import org.springframework.batch.item.support.CompositeItemProcessor;

/**
 * Created by mfit on 13.06.16.
 * Batch processing from acquirer-transaction to accounting-export (via lookup of internal data
 * such as payment, invoice and purchase.
 */
public class TransactionMatcher {
  private PurchaseLookup purchaseLookup;
  private PaymentLookup paymentLookup;

  public PurchaseLookup getPurchaseLookup() {
    return purchaseLookup;
  }

  public void setPurchaseLookup(PurchaseLookup pl) {
    purchaseLookup = pl;
  }

  public PaymentLookup getPaymentLookup() {
    return paymentLookup;
  }

  public void setPaymentLookup(PaymentLookup pl) {
    paymentLookup = pl;
  }

  /**
   * Factory method that creates an item processor for matching transactions to purchase data.
   * @return the created processor.
     */
  public CompositeItemProcessor<AcquirerTransaction, AccountingRow> processorFactory() {
    LookupProcessor processor = new LookupProcessor();
    processor.setPaymentLookup(paymentLookup);
    processor.setPurchaseLookup(purchaseLookup);
    return processor;
  }

  public FieldExtractor<AccountingRow> extractorFactory() {
    return new AccountingFieldExtractor();
  }

  public FlatFileItemWriter<AccountingRow> getFlatFileItemWriter() {
    CsvWriterFactory csvWriterFactory = new CsvWriterFactory<AccountingRow>();
    return csvWriterFactory.getFlatFileItemWriter(extractorFactory());
  }

  public StreamItemWriter<AccountingRow> getStreamItemWriter() {
    CsvWriterFactory csvWriterFactory = new CsvWriterFactory<AccountingRow>();
    return csvWriterFactory.getStreamItemWriter(extractorFactory());
  }
}

