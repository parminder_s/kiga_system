package com.kiga.accounting.dunning.web.request;

import lombok.Data;

import java.util.List;

/**
 * @author bbs
 * @since 2/20/17.
 */
@Data
public class SendEmailRequest {
  private List<String> invoiceNumbers;
}
