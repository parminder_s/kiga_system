package com.kiga.accounting.helper;

import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.FieldExtractor;

/**
 * Created by mfit on 06.06.16.
 */
public class CsvWriterFactory<T> {
  public String lineDelimiter = "\n";
  public String fieldDelimiter = ";";

  /**
   * Get a stream writer.
   * @param extractor The extractor to use.
   * @return The writer.
     */
  public StreamItemWriter<T> getStreamItemWriter(FieldExtractor<T> extractor) {
    DelimitedLineAggregator lineAggregator = new DelimitedLineAggregator<T>();
    lineAggregator.setFieldExtractor(extractor);
    lineAggregator.setDelimiter(fieldDelimiter);
    return new StreamItemWriter<T>(lineAggregator, lineDelimiter);
  }

  /**
   * Get a writer to write directly to file.
   * @param extractor The extractor to use.
   * @return The writer.
   */
  public FlatFileItemWriter<T> getFlatFileItemWriter(FieldExtractor<T> extractor) {
    DelimitedLineAggregator lineAggregator = new DelimitedLineAggregator<T>();
    lineAggregator.setFieldExtractor(extractor);
    lineAggregator.setDelimiter(fieldDelimiter);
    FlatFileItemWriter writer = new FlatFileItemWriter<T>();
    writer.setLineAggregator(lineAggregator);
    writer.setLineSeparator(lineDelimiter);
    return writer;
  }
}
