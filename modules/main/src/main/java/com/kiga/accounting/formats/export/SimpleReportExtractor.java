package com.kiga.accounting.formats.export;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import org.springframework.batch.item.file.transform.FieldExtractor;

/** Created by mfit on 13.06.16. */
public class SimpleReportExtractor implements FieldExtractor<Object> {
  private SimpleDateFormat dtf = new SimpleDateFormat("dd.MM.yyyy");
  private NumberFormat nmf = NumberFormat.getInstance(Locale.GERMAN);

  /**
   * Get strings from an item, last step before writing to flat file such as csv.
   *
   * @param item the item to map.
   * @return The list of strings.
   */
  public Object[] extract(Object item) {

    Object[] result = {"test", "not implemented"};
    return result;
  }
}
