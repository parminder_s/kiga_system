package com.kiga.accounting.dunning.service;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneak;

import com.kiga.accounting.dunning.web.request.SendEmailRequest;
import com.kiga.accounting.service.InvoiceService;
import com.kiga.mail.Mailer;
import com.kiga.shop.EmailTemplates;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.function.Supplier;

/**
 * @author bbs
 * @since 2/20/17.
 */
@Service
public class DunningEmailService {
  static final String FILENAME_PDF = "Zahlungserinnerung.pdf";
  private static final Logger logger = LoggerFactory.getLogger(DunningEmailService.class);
  private static final String MAIL_FROM = "system@kigaportal.com";
  private static final String MAIL_SUBJECT = "Zahlungserinnerung KiGaPortal";
  private Mailer mailer;
  private InvoiceService invoiceService;
  private DunningDocumentService documentService;
  private Supplier<PDFMergerUtility> pdfMergerUtilitySupplier;

  /**
   * Deps.
   */
  @Autowired
  public DunningEmailService(
    Mailer mailer, InvoiceService invoiceService,
    DunningDocumentService documentService, Supplier<PDFMergerUtility> pdfMergerUtilitySupplier) {
    this.mailer = mailer;
    this.invoiceService = invoiceService;
    this.documentService = documentService;
    this.pdfMergerUtilitySupplier = pdfMergerUtilitySupplier;
  }

  /**
   * send emails.
   */
  public void sendEmails(SendEmailRequest sendEmailRequest) {
    sendEmailRequest.getInvoiceNumbers().stream().forEach(this::preparePdfAndSendEmail);
  }

  /**
   *  creates the pdf file containing all dunning documents.
   */
  public File dunningDocumentForInvoices(List<Long> invoiceIds) {
    PDFMergerUtility pdfMergerUtility = pdfMergerUtilitySupplier.get();

    invoiceIds.stream()
      .map(invoiceId -> documentService
        .createDunningDocument(invoiceService.getOne(invoiceId).getInvoiceNumber())
      )
      .forEach(pdfMergerUtility::addSource);
    String printFileName = "/tmp/mergedDunningDocuments" + System.currentTimeMillis() + ".pdf";
    pdfMergerUtility.setDestinationFileName(printFileName);
    sneak(() -> {
      pdfMergerUtility.mergeDocuments();
      return null;
    });
    return new File(printFileName);
  }

  private void preparePdfAndSendEmail(String invoiceNumber) {
    File file = documentService.createDunningDocument(invoiceNumber);
    sendEmailWithAttachement(file, invoiceNumber);
    invoiceService.markDunningEmailSent(invoiceNumber);
  }

  private void sendEmailWithAttachement(File dunningPdf, String invoiceNumber) {
    String to = invoiceService.getOne(invoiceNumber).getCustomerEmail();
    String body = EmailTemplates.dunningEmail();
    mailer.send("Dunning", "", invoiceNumber, email -> email
      .withTo(to)
      .withFrom(MAIL_FROM)
      .withSubject(MAIL_SUBJECT)
      .withBody(body)
      .withAttachment(FILENAME_PDF, dunningPdf)
    );
  }
}
