package com.kiga.accounting.transactions.web.request;

import lombok.Data;

/**
 * @author bbs
 * @since 2/18/17.
 */
@Data
public class BanksByTransactionRequest {
  private long paymentTransactionId;
}
