package com.kiga.accounting.dunning.web.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author bbs
 * @since 2/24/17.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UpdateDunningLevelRequest extends AddDunningLevelRequest {
  private long id;
}
