package com.kiga.accounting.dunning.web;

import com.kiga.accounting.dunning.service.DunningLevelService;
import com.kiga.accounting.dunning.web.request.AddDunningLevelRequest;
import com.kiga.accounting.dunning.web.request.GetDunningLevelRequest;
import com.kiga.accounting.dunning.web.request.RemoveDunningLevelRequest;
import com.kiga.accounting.dunning.web.request.UpdateDunningLevelRequest;
import com.kiga.accounting.dunning.web.response.DunningLevelViewModel;
import com.kiga.security.services.SecurityService;
import com.kiga.shop.service.CountryService;
import com.kiga.shop.web.response.CountryViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 2/19/17.
 */
@RestController
@RequestMapping("backend/dunning-management/levels")
public class DunningLevelsController {
  private DunningLevelService dunningLevelService;
  private CountryService countryService;
  private SecurityService securityService;

  /**
   * Autowire deps.
   *
   * @param dunningLevelService dunning level service
   * @param countryService      country service
   * @param securityService     security service
   */
  @Autowired
  public DunningLevelsController(DunningLevelService dunningLevelService,
    CountryService countryService, SecurityService securityService) {
    this.dunningLevelService = dunningLevelService;
    this.countryService = countryService;
    this.securityService = securityService;
  }

  @RequestMapping
  public List<DunningLevelViewModel> getLevels() {
    securityService.requireShopAdmin();
    return dunningLevelService.getAll();
  }

  @RequestMapping("get")
  public DunningLevelViewModel getOne(
    @RequestBody GetDunningLevelRequest singleDunningLevelRequest) {
    securityService.requireShopAdmin();
    return dunningLevelService.getOne(singleDunningLevelRequest.getId());
  }

  /**
   * Gets all available currencies.
   *
   * @return all available currencies
   */
  @RequestMapping("countries")
  public List<CountryViewModel> getCountries() {
    securityService.requireShopAdmin();
    return countryService.getAll().stream().distinct().collect(Collectors.toList());
  }

  @RequestMapping("add")
  public void add(@RequestBody AddDunningLevelRequest addDunningLevelRequest) {
    securityService.requireShopAdmin();
    dunningLevelService.add(addDunningLevelRequest);
  }

  @RequestMapping("update")
  public void update(@RequestBody UpdateDunningLevelRequest updateDunningLevelRequest) {
    securityService.requireShopAdmin();
    dunningLevelService.update(updateDunningLevelRequest);
  }

  @RequestMapping("remove")
  public void remove(@RequestBody RemoveDunningLevelRequest removeDunningLevelRequest) {
    securityService.requireShopAdmin();
    dunningLevelService.remove(removeDunningLevelRequest);
  }
}
