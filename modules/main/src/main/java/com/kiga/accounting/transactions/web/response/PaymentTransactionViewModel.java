package com.kiga.accounting.transactions.web.response;

import com.kiga.accounting.web.response.InvoiceViewModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author bbs
 * @since 2/10/17.
 */
@Data
public class PaymentTransactionViewModel {
  private Long id;
  private InvoiceViewModel invoiceModel;
  private String paidInvoiceNumber;
  private BigDecimal amount;
  private Date transferDate;
  private String bank;
  private String bankTransactionId;
}
