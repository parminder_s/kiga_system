package com.kiga.accounting.dunning.service;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.shop.documents.model.DunningDocumentModel;
import com.kiga.shop.documents.service.PrintingService;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.shop.service.ProductHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author bbs
 * @since 2/22/17.
 */
@Service
public class DunningDocumentService {
  private PrintingService printingService;
  private PurchaseRepository purchaseRepository;
  private ProductHelper productHelper;
  private InvoiceRepository invoiceRepository;

  /**
   * Inject dependencies.
   *
   * @param printingService printingService
   * @param purchaseRepository purchaseRepository
   * @param productHelper productHelper
   * @param invoiceRepository invoiceRepository
   */
  @Autowired
  public DunningDocumentService(PrintingService printingService,
    PurchaseRepository purchaseRepository, ProductHelper productHelper,
    InvoiceRepository invoiceRepository) {
    this.printingService = printingService;
    this.purchaseRepository = purchaseRepository;
    this.productHelper = productHelper;
    this.invoiceRepository = invoiceRepository;
  }

  File createDunningDocument(String invoiceNr) {
    Purchase purchase = purchaseRepository.findByInvoiceNr(invoiceNr);
    Invoice invoice = invoiceRepository.findByInvoiceNumber(invoiceNr);
    DunningDocumentModel dunningDocumentModel = new DunningDocumentModel(purchase,
      productHelper.getProductDetailsFromPurchaseAsJava(purchase),
      invoice);

    return createFile(dunningDocumentModel);
  }

  private File createFile(DunningDocumentModel dunningDocumentModel) {
    String templateFileName = "dunning-document-" + dunningDocumentModel.getDunningLevel() + ".xml";
    String outputFileName =
      "dunning_document-" + convertToSafeSystemPath(dunningDocumentModel.getInvoiceNr());

    Map<String, Object> modelMap = new HashMap<>();
    modelMap.put("model", dunningDocumentModel);

    return printingService.printDocument(templateFileName, modelMap, outputFileName);
  }

  private String convertToSafeSystemPath(String nonSafeName) {
    return nonSafeName.replace("/", "-");
  }
}
