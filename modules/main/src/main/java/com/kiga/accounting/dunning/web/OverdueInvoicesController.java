package com.kiga.accounting.dunning.web;

import com.kiga.accounting.service.InvoiceService;
import com.kiga.accounting.web.response.InvoiceViewModel;
import com.kiga.security.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 2/19/17.
 */
@RestController
@RequestMapping("backend/dunning-management/overdue")
public class OverdueInvoicesController {
  private InvoiceService invoiceService;
  private SecurityService securityService;

  @Autowired
  public OverdueInvoicesController(InvoiceService invoiceService, SecurityService securityService) {
    this.invoiceService = invoiceService;
    this.securityService = securityService;
  }

  @RequestMapping("list")
  public List<InvoiceViewModel> getAll() {
    securityService.requireShopAdmin();
    return invoiceService.getAllOverdue();
  }
}
