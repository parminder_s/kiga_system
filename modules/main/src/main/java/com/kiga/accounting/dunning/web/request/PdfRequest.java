package com.kiga.accounting.dunning.web.request;

import lombok.Data;

import java.util.List;

/**
 * Created by peter on 15.03.17.
 */
@Data
public class PdfRequest {
  private List<Long> invoiceIds;
}
