package com.kiga.accounting.domain;

import com.kiga.accounting.dunning.domain.DunningLevel;
import com.kiga.accounting.transactions.domain.PaymentTransaction;
import com.kiga.db.KigaEntityModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

/**
 * @author bbs
 * @since 2/11/17.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Invoice extends KigaEntityModel {
  private String invoiceNumber;
  private Date invoiceDate;
  private Date dueDate;
  private String customerName;
  private String customerEmail;
  private BigDecimal amount;
  private String currency;
  private boolean settled;
  private String countryCode;
  private String orderNumber;
  private Date orderDate;
  private boolean dunningEmailSent;

  @OneToMany(mappedBy = "invoice")
  private List<PaymentTransaction> paymentTransactions;

  @ManyToOne
  @JoinColumn(name = "dunningLevelId", referencedColumnName = "id")
  private DunningLevel dunningLevel;

  /**
   * prepersist.
   */
  @PrePersist
  public void prePersist() {
    setClassName("Invoice");
  }
}
