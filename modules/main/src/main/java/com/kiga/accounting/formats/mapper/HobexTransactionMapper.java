package com.kiga.accounting.formats.mapper;

import com.kiga.accounting.domain.AcquirerTransaction;
import org.apache.commons.io.FilenameUtils;
import org.springframework.batch.item.file.transform.FieldSet;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;


/**
 * Created by mfit on 30.05.16.
 * Map CardComplete transactions-report raw data to internal representation.
 */
public class HobexTransactionMapper implements TransactionImportMapper {
  public static String ACQUIRER_NAME = "hobex";
  public static String PSP_NAME = "mpay";
  protected String filename;

  protected NumberFormat format = NumberFormat.getInstance(Locale.GERMAN);

  protected BigDecimal parseDecimal(String text) {
    try {
      return new BigDecimal(this.format.parse(text).toString(), MathContext.DECIMAL64);
    } catch (ParseException exc) {
      return null;
    }
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  /**
   * Map incoming row to generic transaction.
   * @param fieldSet A FieldSet from down the importer pipeline.
   * @return AcquirerTransaction
     */
  public AcquirerTransaction mapFieldSet(FieldSet fieldSet) {
    AcquirerTransaction ta = new AcquirerTransaction();

    ta.setAmountOrig(parseDecimal(fieldSet.readString("Nativeamount")));
    ta.setCurrencyOrig(fieldSet.readString("Nativecurrency"));
    ta.setAmount(parseDecimal(fieldSet.readString("Nativeamount")));
    ta.setCurrency(fieldSet.readString("Nativecurrency"));

    ta.setTransactionDate(fieldSet.readDate("Turnoverdate", "dd.MM.yyyy"));
    ta.setTransactionDateAcquirer(fieldSet.readDate("Turnoverdate", "dd.MM.yyyy"));
    ta.setProcessingDate(fieldSet.readDate("Clearingdate", "dd.MM.yyyy"));
    ta.setSettlementDate(fieldSet.readDate("Paymuldate", "dd.MM.yyyy"));

    ta.setCardType(fieldSet.readString("Type"));

    ta.setPspTransactionId(fieldSet.readString("Referencenumber"));
    ta.setAcquirerTransactionId(fieldSet.readString("Receipt"));

    ta.setAcquirerName(ACQUIRER_NAME);
    ta.setPspName(PSP_NAME);
    ta.setSourceFileName(FilenameUtils.getName(this.filename));

    return ta;
  }
}

