package com.kiga.accounting.transactions.repository;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.transactions.domain.PaymentTransaction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author bbs
 * @since 2/8/17.
 */
public interface PaymentTransactionRepository extends CrudRepository<PaymentTransaction, Long> {
  List<PaymentTransaction> findAll();
}
