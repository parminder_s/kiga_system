package com.kiga.accounting.web;

import com.kiga.accounting.service.InvoiceService;
import com.kiga.accounting.web.request.SingleInvoiceRequest;
import com.kiga.accounting.web.response.InvoiceViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author bbs
 * @since 2/16/17.
 */
@RestController
@RequestMapping("backend/invoices")
public class AccountingInvoiceController {
  private InvoiceService invoiceService;

  @Autowired
  public AccountingInvoiceController(InvoiceService invoiceService) {
    this.invoiceService = invoiceService;
  }

  /**
   * get invoice by its number.
   *
   * @param singleInvoiceRequest request
   * @return invoice
   */
  @RequestMapping("get")
  public InvoiceViewModel getOne(@RequestBody SingleInvoiceRequest singleInvoiceRequest) {
    return invoiceService
      .getOne(singleInvoiceRequest.getInvoiceNumber());
  }
}
