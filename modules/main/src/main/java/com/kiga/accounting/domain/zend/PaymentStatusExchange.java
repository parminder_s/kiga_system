package com.kiga.accounting.domain.zend;

import com.kiga.db.KigaEntityModel;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Created by mfit on 21.06.16.
 */
@Entity
public class PaymentStatusExchange extends KigaEntityModel {
  private @Column(name = "rcpt_id") Long rcptId;

  private @Column(name = "request_status") String requestStatus;
  private @Column(name = "request_date") Date requestDate;
  private @Column(name = "request_update") Date requestUpdate;

  private @Column(name = "transaction_id") String transactionId;
  private @Column(name = "tid1") String tid1;
  private @Column(name = "tid2") String tid2;

  private @Column(name = "amount") Double amount;
  private @Column(name = "currency_code") String currencyCode;

  private @Column(name = "rcpt_date") Date rcptDate;
  private @Column(name = "payment_type") int paymentType;
  private @Column(name = "psp_name") String pspName;
  private @Column(name = "psp_tid") String pspTid;
  private @Column(name = "acquirer_name") String acquirerName;
  private @Column(name = "acquirer_tid") String acquirerTid;

  private @Column(name = "ta_date") Date transactionDate;
  private @Column(name = "transfer_date") Date transferDate;
  private @Column(name = "transfer_amount") Double transferAmount;

  private @Column(name = "rcpt_status") String rcptStatus;
  private @Column(name = "rcpt_status_long") String rcptStatusLong;
}
