package com.kiga.accounting.dunning.web.response;

import com.kiga.shop.web.response.CountryViewModel;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bbs
 * @since 2/19/17.
 */
@Data
public class DunningLevelViewModel {
  private long id;
  private String code;
  private int days;
  private BigDecimal feesAmount;
  private int dunningLevel;
  private CountryViewModel country;
}
