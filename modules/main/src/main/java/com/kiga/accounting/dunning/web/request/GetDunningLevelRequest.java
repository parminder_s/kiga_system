package com.kiga.accounting.dunning.web.request;

import lombok.Data;

/**
 * @author bbs
 * @since 2/26/17.
 */
@Data
public class GetDunningLevelRequest {
  private long id;
}
