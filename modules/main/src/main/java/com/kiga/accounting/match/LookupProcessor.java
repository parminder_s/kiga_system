package com.kiga.accounting.match;

import com.kiga.accounting.domain.AcquirerTransaction;
import com.kiga.accounting.formats.export.AccountingRow;
import com.kiga.accounting.lookup.PaymentLookup;
import com.kiga.accounting.lookup.PurchaseLookup;
import com.kiga.payment.domain.Payment;
import com.kiga.shop.domain.Purchase;
import org.springframework.batch.item.support.CompositeItemProcessor;

import java.util.Date;

/**
 * Created by mfit on 13.06.16.
 */
public class LookupProcessor extends CompositeItemProcessor<AcquirerTransaction, AccountingRow> {
  private PaymentLookup paymentLookup;
  private PurchaseLookup purchaseLookup;

  public PurchaseLookup getPurchaseLookup() {
    return purchaseLookup;
  }

  public void setPurchaseLookup(PurchaseLookup purchaseLookup) {
    this.purchaseLookup = purchaseLookup;
  }

  public PaymentLookup getPaymentLookup() {
    return paymentLookup;
  }

  public void setPaymentLookup(PaymentLookup paymentLookup) {
    this.paymentLookup = paymentLookup;
  }

  /**
   * A mapping function in the transactions pipeline that attempts to mach the transaction
   * to a Purchase object (referenceId of transaction is used in lookup for purchase).
   * @param importedTransaction the transaction to be matched/joined with purchase.
   * @return returns data from transaction and matching purchase (if any).
     */
  public AccountingRow process(AcquirerTransaction importedTransaction) {
    AccountingRow result = new AccountingRow();
    Payment payment = paymentLookup.findByPaymentProviderId(
      importedTransaction.getPspTransactionId());
    if (payment != null) {
      if (payment.getReferenceCode() == "shop") {
        try {
          Purchase purchase = purchaseLookup.findByPurchaseId(
            Long.valueOf(payment.getReferenceId()));
          if (purchase != null) {
            result.setInvoiceDate(Date.from(purchase.getInvoiceDate()));
            result.setInvoiceTotal(purchase.getPriceGrossTotal());
            result.setInvoiceTaxTotal(purchase.getPriceGrossTotal()
              .subtract(purchase.getPriceNetTotal()));
            result.setInvoiceCurrencyCode(purchase.getCurrency());
            result.setInvoiceNr(purchase.getInvoiceNr());
            result.setCountryCode(purchase.getCountryTitle());
          }
        } catch (NumberFormatException exc) {
          System.out.println("Purchase export failed : could not parse purchaseID");
        } catch (IllegalArgumentException exc) {
          System.out.println("Purchase export failed : could not calculate tax total");
        } catch (NullPointerException exc) {
          System.out.println("Purchase export failed : null pointer encountered");
        }
      }
    }

    result.setRcptDate(importedTransaction.getProcessingDate());
    result.setRcptTransferDate(importedTransaction.getSettlementDate());
    result.setRcptAmount(importedTransaction.getAmountOrig());
    result.setRcptCurrencyCode(importedTransaction.getCurrencyOrig());
    result.setRcptAcquirerTid(importedTransaction.getAcquirerTransactionId());
    return result;
  }

}
