package com.kiga.accounting.helper;

import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.transform.LineAggregator;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mfit on 06.06.16.
 */
public class StreamItemWriter<T> implements ItemWriter<T> {
  protected LineAggregator<T> lineAggregator;
  protected String lineSeparator;
  protected OutputStream outputStream;

  public StreamItemWriter(LineAggregator<T> lineAggregator, String lineSeparator) {
    this.lineAggregator = lineAggregator;
    this.lineSeparator = lineSeparator;
  }

  public void writeHeader(List<String> items) throws IOException {
    String headerString = items.stream().collect(Collectors.joining(";")) + this.lineSeparator;
    outputStream.write(headerString.getBytes(StandardCharsets.UTF_8));
  }

  public void setOutputStream(OutputStream outputStream) {
    this.outputStream = outputStream;
  }

  /**
   * Writes items to output resource-stream.
   * @param items list of instances of the item-type to write.
   * @throws Exception when writing produces error.
     */
  public void write(List<? extends T> items) throws Exception {
    StringBuilder lines = new StringBuilder();
    int lineCount = 0;
    for (T item: items) {
      lines.append(lineAggregator.aggregate(item) + lineSeparator);
      lineCount += 1;
    }
    outputStream.write(lines.toString().getBytes(StandardCharsets.UTF_8));
  }
}
