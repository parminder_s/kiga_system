package com.kiga.accounting.transactions.web.request;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author bbs
 * @since 2/9/17.
 */
@Data
public class AddPaymentTransactionRequest {
  private String paidInvoiceNumber;
  private BigDecimal amount;
  private Date transferDate;
  private String bank;
  private String bankTransactionId;
  private boolean settle;
}
