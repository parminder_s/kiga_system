package com.kiga.accounting.web.response;

import com.kiga.accounting.domain.AcquirerTransaction;
import lombok.Data;


import java.util.List;

/**
 * Created by mfit on 31.05.16.
 */
@Data
public class AcquirerTransactionsResponse {
  private List<AcquirerTransaction> data;

  public AcquirerTransactionsResponse() { }

  public AcquirerTransactionsResponse(List<AcquirerTransaction> data) {
    this.data = data;
  }
}

