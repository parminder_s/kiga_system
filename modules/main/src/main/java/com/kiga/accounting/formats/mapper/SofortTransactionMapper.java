package com.kiga.accounting.formats.mapper;

import com.kiga.accounting.domain.AcquirerTransaction;
import org.apache.commons.io.FilenameUtils;
import org.springframework.batch.item.file.transform.FieldSet;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/**
 * Created by mfit on 30.05.16.
 * Map "Sofort" transactions-report raw data to internal representation.
 */
public class SofortTransactionMapper implements TransactionImportMapper {
  public static String ACQUIRER_NAME = "sofort";
  public static String PSP_NAME = "sofort";
  protected String filename;
  protected NumberFormat format = NumberFormat.getInstance(Locale.GERMAN);

  protected BigDecimal parseDecimal(String text) {
    try {
      return new BigDecimal(this.format.parse(text).toString(), MathContext.DECIMAL64);
    } catch (ParseException exc) {
      return null;
    }
  }

  protected Date parseDate(FieldSet fieldSet, String fieldname) {
    Date result;
    for (String pattern: Arrays.asList("yyyy-MM-dd", "dd.MM.yyyy")) {
      try {
        result = fieldSet.readDate(fieldname, pattern);
        return result;
      } catch (IllegalArgumentException exception) {
        // Pass
      }
    }
    throw new IllegalArgumentException();
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  /**
   * Map incoming row to generic transaction.
   * @param fieldSet A FieldSet from down the importer pipeline.
   * @return AcquirerTransaction
     */
  public AcquirerTransaction mapFieldSet(FieldSet fieldSet) {
    AcquirerTransaction ta = new AcquirerTransaction();

    // This should be one, otherwise have to deal with amountOrig/amount
    if (parseDecimal(fieldSet.readString("Transaction.exchange_rate"))
        .compareTo(new BigDecimal("1.0")) != 0) {
      // throw new Exception("Exchange rate not equal to 1.0");
    }

    // This should be zero, otherwise have to determine the actual amount
    if (parseDecimal(fieldSet.readString("Transaction.amount_refunded"))
        .compareTo(new BigDecimal("0")) != 0) {
      // throw new Exception("Refunded amount encountered");
    }

    ta.setAmountOrig(parseDecimal(fieldSet.readString("Transaction.amount")));
    ta.setCurrencyOrig(fieldSet.readString("Transaction.currency_id"));
    ta.setAmount( parseDecimal(fieldSet.readString("Transaction.amount")));
    ta.setCurrency(fieldSet.readString("Transaction.currency_id"));

    ta.setTransactionDate(parseDate(fieldSet, "Transaction.created"));
    ta.setTransactionDateAcquirer(parseDate(fieldSet, "Transaction.created"));
    ta.setProcessingDate(parseDate(fieldSet, "Transaction.created"));
    ta.setSettlementDate(parseDate(fieldSet, "Transaction.created"));

    ta.setCardType("");

    // Format : 000064895362MPAY24 . Get just the number, without leading zeros.
    ta.setPspTransactionId(Integer.toString(Integer.parseInt(
      fieldSet.readString("Transaction.reason_1").split("MPAY24")[0])));
    ta.setAcquirerTransactionId(fieldSet.readString("Transaction.transaction"));

    ta.setAcquirerName(ACQUIRER_NAME);
    ta.setPspName(PSP_NAME);

    ta.setBatchSum(new BigDecimal(0));
    ta.setBatchNr("");
    ta.setDestinationBankBic(fieldSet.readString("Transaction.recipient_bank_bic"));

    ta.setSourceFileName(FilenameUtils.getName(this.filename));
    return ta;
  }
}
