package com.kiga.accounting.formats.mapper;

import com.kiga.accounting.domain.AcquirerTransaction;
import org.apache.commons.io.FilenameUtils;
import org.springframework.batch.item.file.transform.FieldSet;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Created by mfit on 30.05.16.
 * Map CardComplete transactions-report raw data to internal representation.
 */
public class CardCompleteTransactionMapper implements TransactionImportMapper {
  public static String ACQUIRER_NAME = "cardcomplete";
  public static String PSP_NAME = "mpay";
  protected String filename;

  protected NumberFormat format = NumberFormat.getInstance(Locale.GERMAN);

  protected BigDecimal parseDecimal(String text) {
    try {
      return new BigDecimal(this.format.parse(text).toString(), MathContext.DECIMAL64);
    } catch (ParseException exc) {
      return null;
    }
  }

  /**
   * To have more flexibility, a dedicated method is used to parse (multiple) date formats.
   * However, SimpleDateFormat("dd.MM.yy") seems to parse both "yy" AND "yyyy" for year anyways.
   * @param text the string input.
   * @return the parsed date.
   */
  protected Date parseDate(String text) {
    SimpleDateFormat formatter;
    formatter = new SimpleDateFormat("dd.MM.yy");
    try {
      Date date = formatter.parse(text);
      return date;
    } catch (ParseException parseException) {
      // pass
    }
    return null;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  /**
   * Map incoming row to generic transaction.
   * @param fieldSet A FieldSet from down the importer pipeline.
   * @return AcquirerTransaction
     */
  public AcquirerTransaction mapFieldSet(FieldSet fieldSet) {
    AcquirerTransaction ta = new AcquirerTransaction();

    ta.setAmountOrig(parseDecimal(fieldSet.readString("Originalbetrag")));
    ta.setCurrencyOrig(fieldSet.readString("Orig. Währung"));
    ta.setAmount(parseDecimal(fieldSet.readString("Kauf-/Umsatzbetrag")));
    ta.setCurrency(fieldSet.readString("Währung"));

    ta.setTransactionDate(parseDate(fieldSet.readString("Umsatzdatum")));
    ta.setTransactionDateAcquirer(parseDate(fieldSet.readString("Einreichdatum")));
    ta.setProcessingDate(parseDate(fieldSet.readString("Verarbeitungsdatum")));
    ta.setSettlementDate(parseDate(fieldSet.readString("Abrechnungsdatum")));

    ta.setCardType(fieldSet.readString("Produkt"));

    ta.setPspTransactionId(fieldSet.readString("Auftragsnummer"));
    ta.setAcquirerTransactionId(fieldSet.readString("Belegnummer"));

    ta.setBatchSum(parseDecimal(fieldSet.readString("Einreichsumme")));
    ta.setBatchNr(fieldSet.readString("Batchnummer"));
    ta.setAcquirerName(ACQUIRER_NAME);
    ta.setPspName(PSP_NAME);
    ta.setSourceFileName(FilenameUtils.getName(this.filename));

    return ta;
  }
}

