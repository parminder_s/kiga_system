package com.kiga.accounting.dunning.repository;

import com.kiga.accounting.dunning.domain.DunningLevel;
import com.kiga.shop.domain.Country;
import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 2/19/17.
 */
public interface DunningLevelRepository extends CrudRepository<DunningLevel, Long> {
  DunningLevel findByDunningLevelAndCountry(int dunningLevel, Country country);
}
