package com.kiga.accounting.transactions.web;

import com.kiga.accounting.transactions.service.PaymentTransactionService;
import com.kiga.accounting.transactions.web.request.AddPaymentTransactionRequest;
import com.kiga.accounting.transactions.web.request.FindPaymentTransactionRequest;
import com.kiga.accounting.transactions.web.request.RemovePaymentTransactionRequest;
import com.kiga.accounting.transactions.web.request.UpdatePaymentTransactionRequest;
import com.kiga.accounting.transactions.web.response.PaymentTransactionViewModel;
import com.kiga.accounting.web.request.SingleInvoiceRequest;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("backend/payment-transactions")
public class PaymentTransactionController {
  private PaymentTransactionService paymentTransactionService;

  @Autowired
  public PaymentTransactionController(PaymentTransactionService paymentTransactionService) {
    this.paymentTransactionService = paymentTransactionService;
  }

  @RequestMapping(value = "/{invoiceNumber}", method = RequestMethod.GET)
  public List<PaymentTransactionViewModel> list(
      @PathVariable("invoiceNumber") String invoiceNumber) {
    return paymentTransactionService.getAll(invoiceNumber);
  }

  /** Get single payment transaction. */
  @RequestMapping("get")
  public PaymentTransactionViewModel one(
      @RequestBody FindPaymentTransactionRequest findPaymentTransactionRequest) {
    return paymentTransactionService.getOne(findPaymentTransactionRequest);
  }

  /** get invoice by its number. */
  @RequestMapping("get-model")
  public PaymentTransactionViewModel getModelByInvoice(
      @RequestBody SingleInvoiceRequest singleInvoiceRequest) {
    return paymentTransactionService.getModelByInvoice(singleInvoiceRequest);
  }

  /** Add new Payment Transaction. */
  @RequestMapping("add")
  public void add(@RequestBody AddPaymentTransactionRequest addPaymentTransactionRequest) {
    paymentTransactionService.add(addPaymentTransactionRequest);
  }

  /** Update Payment Transaction. */
  @RequestMapping("update")
  public void update(@RequestBody UpdatePaymentTransactionRequest updatePaymentTransactionRequest) {
    paymentTransactionService.update(updatePaymentTransactionRequest);
  }

  /** Remove Payment Transaction. */
  @RequestMapping("remove")
  public void remove(@RequestBody RemovePaymentTransactionRequest removePaymentTransactionRequest) {
    paymentTransactionService.remove(removePaymentTransactionRequest.getIds());
  }
}
