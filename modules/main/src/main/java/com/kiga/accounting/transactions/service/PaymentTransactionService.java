package com.kiga.accounting.transactions.service;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.service.InvoiceService;
import com.kiga.accounting.transactions.domain.PaymentTransaction;
import com.kiga.accounting.transactions.repository.PaymentTransactionRepository;
import com.kiga.accounting.transactions.web.converter.PaymentTransactionToViewModelConverter;
import com.kiga.accounting.transactions.web.request.AddPaymentTransactionRequest;
import com.kiga.accounting.transactions.web.request.FindPaymentTransactionRequest;
import com.kiga.accounting.transactions.web.request.UpdatePaymentTransactionRequest;
import com.kiga.accounting.transactions.web.response.PaymentTransactionViewModel;
import com.kiga.accounting.web.request.SingleInvoiceRequest;
import com.kiga.accounting.web.response.InvoiceViewModel;
import com.kiga.main.util.NumberUtil;
import com.kiga.shop.service.CountryShopService;
import com.kiga.shop.web.response.CountryShopViewModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author bbs
 * @since 2/9/17.
 */
@Service
public class PaymentTransactionService {
  private InvoiceService invoiceService;
  private PaymentTransactionRepository paymentTransactionRepository;
  private PaymentTransactionToViewModelConverter paymentTransactionToViewModelConverter;
  private CountryShopService countryShopService;

  /**
   * Wire deps.
   *
   * @param invoiceService                         invoice service
   * @param paymentTransactionRepository           payment transaction repository
   * @param paymentTransactionToViewModelConverter payment transaction to viewmodel converter
   * @param countryShopService                     country shop service
   */
  @Autowired
  public PaymentTransactionService(
    InvoiceService invoiceService, PaymentTransactionRepository paymentTransactionRepository,
    PaymentTransactionToViewModelConverter paymentTransactionToViewModelConverter,
    CountryShopService countryShopService) {
    this.invoiceService = invoiceService;
    this.paymentTransactionRepository = paymentTransactionRepository;
    this.paymentTransactionToViewModelConverter = paymentTransactionToViewModelConverter;
    this.countryShopService = countryShopService;
  }


  /**
   * returns transactions per invoiceNumber or all.
   */
  public List<PaymentTransactionViewModel> getAll(String invoiceNumber) {
    List<PaymentTransaction> paymentTransactions;

    if ("all".equals(invoiceNumber)) {
      paymentTransactions = paymentTransactionRepository.findAll();
    } else {
      paymentTransactions =
        invoiceService.getEntityByNumber(invoiceNumber).getPaymentTransactions();
    }
    return paymentTransactionToViewModelConverter
      .convertToResponse(paymentTransactions);
  }


  public PaymentTransactionViewModel getOne(
    FindPaymentTransactionRequest findPaymentTransactionRequest) {
    return paymentTransactionToViewModelConverter.convertToResponse(
      paymentTransactionRepository.findOne(findPaymentTransactionRequest.getId()));
  }

  /**
   * Add payment transaction.
   *
   * @param addPaymentTransactionRequest add payment transaction request
   */
  public void add(AddPaymentTransactionRequest addPaymentTransactionRequest) {
    // link invoice number with invoice entity and update invoice's values, exception otherwise
    Invoice invoice = invoiceService
      .getEntityByNumber(addPaymentTransactionRequest.getPaidInvoiceNumber());
    PaymentTransaction paymentTransaction = new PaymentTransaction();
    copyValues(addPaymentTransactionRequest, paymentTransaction);
    paymentTransaction.setInvoice(invoice);
    paymentTransactionRepository.save(paymentTransaction);

    if (addPaymentTransactionRequest.isSettle()) {
      invoice.setSettled(true);
      invoiceService.save(invoice);
    }
  }

  /**
   * Remove Payment transaction.
   *
   * @param ids ids to be removed
   */
  public void remove(List<Long> ids) {
    List<Long> validIds = NumberUtil.getValidIds(ids);
    validIds.stream().forEach(id -> {
      PaymentTransaction paymentTransaction = paymentTransactionRepository.findOne(id);
      paymentTransactionRepository.delete(paymentTransaction);
    });
  }

  /**
   * Update Payment Transaction.
   *
   * @param updatePaymentTransactionRequest payment transaction request
   */
  public void update(UpdatePaymentTransactionRequest updatePaymentTransactionRequest)  {
    // link invoice number with invoice entity and update invoice's values, exception otherwise
    Invoice invoice = invoiceService
      .getEntityByNumber(updatePaymentTransactionRequest.getPaidInvoiceNumber());
    PaymentTransaction paymentTransaction = paymentTransactionRepository
      .findOne(updatePaymentTransactionRequest.getId());
    paymentTransaction.setInvoice(invoice);
    copyValues(updatePaymentTransactionRequest, paymentTransaction);
    paymentTransactionRepository.save(paymentTransaction);

    if (updatePaymentTransactionRequest.isSettle()) {
      invoice.setSettled(true);
      invoiceService.save(invoice);
    }
  }

  /**
   * create payment transaction model based on invoice.
   *
   * @param singleInvoiceRequest request
   * @return model
   */
  public PaymentTransactionViewModel getModelByInvoice(SingleInvoiceRequest singleInvoiceRequest) {
    InvoiceViewModel invoiceViewModel = invoiceService
      .getOne(singleInvoiceRequest.getInvoiceNumber());

    BigDecimal amountToBePaid = invoiceViewModel.getDueAmount();
    if (amountToBePaid == null) {
      amountToBePaid = invoiceViewModel.getAmount();
    }

    PaymentTransactionViewModel paymentTransactionViewModel = new PaymentTransactionViewModel();
    paymentTransactionViewModel.setAmount(amountToBePaid);
    paymentTransactionViewModel.setInvoiceModel(invoiceViewModel);
    paymentTransactionViewModel.setPaidInvoiceNumber(invoiceViewModel.getInvoiceNumber());
    paymentTransactionViewModel.setTransferDate(new Date());

    CountryShopViewModel countryShopViewModel = countryShopService
      .getByCode(invoiceViewModel.getCountryCode());
    if (countryShopViewModel != null) {
      paymentTransactionViewModel.setBank(countryShopViewModel.getBankName());
    }

    return paymentTransactionViewModel;
  }

  private void copyValues(AddPaymentTransactionRequest addPaymentTransactionRequest,
    PaymentTransaction paymentTransaction) {
    paymentTransaction.setAmount(addPaymentTransactionRequest.getAmount());
    paymentTransaction.setBank(addPaymentTransactionRequest.getBank());
    paymentTransaction.setBankTransactionId(addPaymentTransactionRequest.getBankTransactionId());
    paymentTransaction.setTransferDate(addPaymentTransactionRequest.getTransferDate());
  }
}
