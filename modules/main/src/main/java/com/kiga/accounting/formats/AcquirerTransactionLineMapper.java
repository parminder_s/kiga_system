package com.kiga.accounting.formats;

import com.kiga.accounting.domain.AcquirerTransaction;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;

/**
 * Created by mfit on 30.05.17.
 */
public class AcquirerTransactionLineMapper extends DefaultLineMapper<AcquirerTransaction> {

  @Override
  public AcquirerTransaction mapLine(String line, int lineNumber) throws Exception {
    AcquirerTransaction item = super.mapLine(line, lineNumber);
    item.setSourceContent(line);
    return item;
  }

}
