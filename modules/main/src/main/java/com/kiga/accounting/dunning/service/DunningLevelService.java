package com.kiga.accounting.dunning.service;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.dunning.domain.DunningLevel;
import com.kiga.accounting.dunning.repository.DunningLevelRepository;
import com.kiga.accounting.dunning.web.converter.DunningLevelToViewModelConverter;
import com.kiga.accounting.dunning.web.request.AddDunningLevelRequest;
import com.kiga.accounting.dunning.web.request.DecreaseLevelRequest;
import com.kiga.accounting.dunning.web.request.IncreaseLevelRequest;
import com.kiga.accounting.dunning.web.request.RemoveDunningLevelRequest;
import com.kiga.accounting.dunning.web.request.UpdateDunningLevelRequest;
import com.kiga.accounting.dunning.web.response.DunningLevelViewModel;
import com.kiga.accounting.service.InvoiceService;
import com.kiga.main.util.NumberUtil;
import com.kiga.shop.domain.Country;
import com.kiga.shop.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author bbs
 * @since 2/19/17.
 */
@Service
public class DunningLevelService {
  private InvoiceService invoiceService;
  private CountryService countryService;
  private DunningLevelRepository dunningLevelRepository;
  private DunningLevelToViewModelConverter dunningLevelToViewModelConverter;

  /**
   * deps.
   *
   * @param invoiceService                   service
   * @param countryService                   service
   * @param dunningLevelRepository           repository
   * @param dunningLevelToViewModelConverter converter
   */
  @Autowired
  public DunningLevelService(InvoiceService invoiceService,
    CountryService countryService, DunningLevelRepository dunningLevelRepository,
    DunningLevelToViewModelConverter dunningLevelToViewModelConverter) {
    this.invoiceService = invoiceService;
    this.countryService = countryService;
    this.dunningLevelRepository = dunningLevelRepository;
    this.dunningLevelToViewModelConverter = dunningLevelToViewModelConverter;
  }

  /**
   * get all dunning levels.
   *
   * @return all dunning levels
   */
  public List<DunningLevelViewModel> getAll() {
    Iterable<DunningLevel> dunningLevels = dunningLevelRepository.findAll();
    List<DunningLevelViewModel> dunningLevelViewModels = dunningLevelToViewModelConverter
      .convertToResponse(dunningLevels);
    return dunningLevelViewModels;
  }

  /**
   * decrease dunning level.
   *
   * @param decreaseLevelRequest request
   */
  public void decrease(DecreaseLevelRequest decreaseLevelRequest) {
    decreaseLevelRequest.getInvoiceIds().stream().forEach(this::decrease);
  }

  private void decrease(Long invoiceId) {
    Invoice invoice = invoiceService.getEntity(invoiceId);
    DunningLevel oldDunningLevel = invoice.getDunningLevel();
    DunningLevel newDunningLevel;
    if (oldDunningLevel != null) {
      newDunningLevel = dunningLevelRepository
        .findByDunningLevelAndCountry(oldDunningLevel.getDunningLevel() - 1,
          oldDunningLevel.getCountry());
    } else {
      Country country = countryService.getEntityByCode(invoice.getCountryCode());
      newDunningLevel = dunningLevelRepository.findByDunningLevelAndCountry(1, country);
    }

    if (newDunningLevel != null) {
      invoice.setDunningLevel(newDunningLevel);
      invoiceService.save(invoice);
    }
  }

  /**
   * increase dunning level.
   *
   * @param increaseLevelRequest request
   */
  public void increase(IncreaseLevelRequest increaseLevelRequest) {
    increaseLevelRequest.getInvoiceIds().stream().forEach(this::increase);
  }

  private void increase(Long invoiceId) {
    Invoice invoice = invoiceService.getEntity(invoiceId);
    DunningLevel oldDunningLevel = invoice.getDunningLevel();
    DunningLevel newDunningLevel;
    if (oldDunningLevel != null) {
      newDunningLevel = dunningLevelRepository
        .findByDunningLevelAndCountry(oldDunningLevel.getDunningLevel() + 1,
          oldDunningLevel.getCountry());
    } else {
      Country country = countryService.getEntityByCode(invoice.getCountryCode());
      newDunningLevel = dunningLevelRepository.findByDunningLevelAndCountry(1, country);
    }

    if (newDunningLevel != null) {
      invoice.setDunningLevel(newDunningLevel);
      invoiceService.save(invoice);
    }
  }

  public void add(AddDunningLevelRequest addDunningLevelRequest) {
    DunningLevel dunningLevel = new DunningLevel();
    save(addDunningLevelRequest, dunningLevel);
  }

  public void update(UpdateDunningLevelRequest updateDunningLevelRequest) {
    DunningLevel dunningLevel = dunningLevelRepository.findOne(updateDunningLevelRequest.getId());
    save(updateDunningLevelRequest, dunningLevel);
  }

  private void save(AddDunningLevelRequest dunningLevelRequest, DunningLevel dunningLevelEntity) {
    dunningLevelEntity.setCode(dunningLevelRequest.getCode());
    dunningLevelEntity.setDays(dunningLevelRequest.getDays());
    dunningLevelEntity.setDunningLevel(dunningLevelRequest.getDunningLevel());
    dunningLevelEntity.setFeesAmount(dunningLevelRequest.getFeesAmount());
    Country country = countryService.getEntityById(dunningLevelRequest.getCountryId());
    dunningLevelEntity.setCountry(country);
    dunningLevelRepository.save(dunningLevelEntity);
  }

  /**
   * Removes dunning leves by provided ids.
   *
   * @param removeDunningLevelRequest request
   */
  public void remove(RemoveDunningLevelRequest removeDunningLevelRequest) {
    List<Long> validIds = NumberUtil.getValidIds(removeDunningLevelRequest.getIds());
    for (Long validId : validIds) {
      DunningLevel dunningLevel = dunningLevelRepository.findOne(validId);
      unmatchInvoices(dunningLevel.getInvoices());
      dunningLevelRepository.delete(dunningLevel);
    }
  }

  private void unmatchInvoices(List<Invoice> invoices) {
    for (Invoice invoice : invoices) {
      invoice.setDunningLevel(null);
      invoiceService.save(invoice);
    }
  }

  public DunningLevelViewModel getOne(long id) {
    DunningLevel dunningLevel = dunningLevelRepository.findOne(id);
    return dunningLevelToViewModelConverter.convertToResponse(dunningLevel);
  }
}
