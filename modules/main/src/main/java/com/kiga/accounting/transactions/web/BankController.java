package com.kiga.accounting.transactions.web;

import com.kiga.accounting.transactions.service.BankService;
import com.kiga.accounting.transactions.web.request.BanksByInvoiceRequest;
import com.kiga.accounting.transactions.web.request.BanksByTransactionRequest;
import com.kiga.accounting.transactions.web.response.BankViewModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author bbs
 * @since 2/18/17.
 */
@RestController
@RequestMapping("backend/payment-transactions/banks")
public class BankController {
  private BankService bankService;

  @Autowired
  public BankController(BankService bankService) {
    this.bankService = bankService;
  }

  @RequestMapping
  public List<BankViewModel> getAll() {
    return bankService.getAllBanks();
  }

  @RequestMapping("by-invoice")
  public BankViewModel getByInvoiceNumber(
      @RequestBody BanksByInvoiceRequest banksByInvoiceRequest) {
    return bankService.getByInvoiceNumber(banksByInvoiceRequest.getInvoiceNumber());
  }

  /** Get Bank by paymentTransactionId. */
  @RequestMapping("by-transaction")
  public BankViewModel getByPaymentTransactionId(
      @RequestBody BanksByTransactionRequest banksByTransactionRequest) {
    return bankService.getByPaymentTransactionId(
        banksByTransactionRequest.getPaymentTransactionId());
  }
}
