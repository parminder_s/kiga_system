package com.kiga.accounting.dunning.web.request;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bbs
 * @since 2/24/17.
 */
@Data
public class AddDunningLevelRequest {
  private String code;
  private int days;
  private BigDecimal feesAmount;
  private int dunningLevel;
  private long countryId;
}
