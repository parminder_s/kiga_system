package com.kiga.accounting.transactions.web.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author bbs
 * @since 2/10/17.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UpdatePaymentTransactionRequest extends AddPaymentTransactionRequest {
  private Long id;
}
