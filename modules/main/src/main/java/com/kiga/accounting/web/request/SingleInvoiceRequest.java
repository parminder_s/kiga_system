package com.kiga.accounting.web.request;

import lombok.Data;

/**
 * @author bbs
 * @since 2/16/17.
 */
@Data
public class SingleInvoiceRequest {
  private String invoiceNumber;
}
