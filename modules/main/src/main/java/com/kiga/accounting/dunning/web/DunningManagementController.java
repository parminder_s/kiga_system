package com.kiga.accounting.dunning.web;

import com.kiga.accounting.dunning.service.DunningEmailService;
import com.kiga.accounting.dunning.service.DunningLevelService;
import com.kiga.accounting.dunning.web.request.DecreaseLevelRequest;
import com.kiga.accounting.dunning.web.request.IncreaseLevelRequest;
import com.kiga.accounting.dunning.web.request.PdfRequest;
import com.kiga.accounting.dunning.web.request.SendEmailRequest;
import com.kiga.security.services.SecurityService;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 2/20/17.
 */
@RestController
@RequestMapping("backend/dunning-management")
public class DunningManagementController {
  private DunningEmailService dunningEmailService;
  private DunningLevelService dunningLevelService;
  private SecurityService securityService;

  /**
   *
   * @param dunningEmailService dunningEmailService.
   * @param dunningLevelService dunningLevelService.
   * @param securityService securityService.
   */
  @Autowired
  public DunningManagementController(DunningEmailService dunningEmailService,
                                     DunningLevelService dunningLevelService,
                                     SecurityService securityService) {
    this.dunningEmailService = dunningEmailService;
    this.dunningLevelService = dunningLevelService;
    this.securityService = securityService;
  }

  @RequestMapping("send-email")
  public void sendEmail(@RequestBody SendEmailRequest sendEmailRequest) {
    securityService.requireShopAdmin();
    dunningEmailService.sendEmails(sendEmailRequest);
  }

  @RequestMapping("decrease-level")
  public void decreaseLevel(@RequestBody DecreaseLevelRequest decreaseLevelRequest) {
    securityService.requireShopAdmin();
    dunningLevelService.decrease(decreaseLevelRequest);
  }

  @RequestMapping("increase-level")
  public void increaseLevel(@RequestBody IncreaseLevelRequest increaseLevelRequest) {
    securityService.requireShopAdmin();
    dunningLevelService.increase(increaseLevelRequest);
  }

  /**
   *
   * @param invoiceIds invoicd ids.
   * @param response httpServletResponse.
   * @return one pdf file with all dunningDocuments.
   * @throws IOException .
   * @throws COSVisitorException .
   */
  @RequestMapping(value = "download-pdf/{invoiceIds}", method = RequestMethod.GET,
    produces = "application/pdf")
  public FileSystemResource downloadPdf(@PathVariable List<Long> invoiceIds,
                                        HttpServletResponse response)
    throws IOException, COSVisitorException {
    securityService.requireShopAdmin();
    response.setContentType("application/pdf");
    return new FileSystemResource(
      dunningEmailService.dunningDocumentForInvoices(invoiceIds));
  }
}
