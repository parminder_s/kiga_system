package com.kiga.accounting.transactions.web.response;

import lombok.Data;

/**
 * @author bbs
 * @since 2/18/17.
 */
@Data
public class BankViewModel {
  private String bankName;
}
