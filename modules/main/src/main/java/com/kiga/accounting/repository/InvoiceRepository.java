package com.kiga.accounting.repository;

import com.kiga.accounting.domain.Invoice;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 2/11/17.
 */
public interface InvoiceRepository extends CrudRepository<Invoice, Long>,
  QueryDslPredicateExecutor<Invoice> {
  Invoice findByInvoiceNumber(String number);

  Invoice findById(Long id);
}
