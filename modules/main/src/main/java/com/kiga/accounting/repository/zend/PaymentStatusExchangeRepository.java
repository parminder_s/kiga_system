package com.kiga.accounting.repository.zend;

import com.kiga.accounting.domain.zend.PaymentStatusExchange;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by mfit on 21.06.16.
 */
public interface PaymentStatusExchangeRepository
  extends CrudRepository<PaymentStatusExchange, Long> {
  public java.util.List<PaymentStatusExchange> findAll();
  // public java.util.List<PaymentStatusExchange> findByRequestStatus(String status);
}
