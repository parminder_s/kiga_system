package com.kiga.accounting.formats.mapper;

import com.kiga.accounting.domain.AcquirerTransaction;
import org.springframework.batch.item.file.mapping.FieldSetMapper;

/**
 * Created by mfit on 30.05.16.
 */
interface TransactionImportMapper extends FieldSetMapper<AcquirerTransaction> {
  public void setFilename(String filename);
}
