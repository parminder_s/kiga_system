package com.kiga.accounting.formats.export;

import org.springframework.batch.item.file.transform.FieldExtractor;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by mfit on 13.06.16.
 */
public class AccountingFieldExtractor implements FieldExtractor<AccountingRow> {
  private SimpleDateFormat dtf = new SimpleDateFormat("dd.MM.yyyy");
  private NumberFormat nmf = NumberFormat.getInstance(Locale.GERMAN);

  /**
   * Converts a transaction item into a list of strings, ready for writing to file/ CSV.
   * @param item the transaction item to be exported.
   * @return The array/string-converted representation of the item.
     */
  public Object[] extract(AccountingRow item) {
    Object[] result = {
      dtf.format(item.getRcptDate()),
      dtf.format(item.getRcptTransferDate()),
      item.getCountryCode(),
      nmf.format(item.getRcptAmount().setScale(2, BigDecimal.ROUND_HALF_UP)),
      item.getRcptCurrencyCode(),
      item.getInvoiceNr(),
      (item.getInvoiceDate() != null) ? dtf.format(item.getInvoiceDate()) : "",
      (item.getInvoiceTotal() != null) ? nmf.format(item.getInvoiceTotal()
        .setScale(2, BigDecimal.ROUND_HALF_UP)) : "",
      (item.getInvoiceTaxTotal() != null) ? nmf.format(item.getInvoiceTaxTotal()
        .setScale(2, BigDecimal.ROUND_HALF_UP)) : "",
      item.getInvoiceCurrencyCode(),
      item.getRcptAcquirerTid()
    };
    return result;
  }
}
