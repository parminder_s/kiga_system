package com.kiga.accounting.lookup;

import com.kiga.shop.domain.Purchase;

/**
 * Created by mfit on 06.06.16.
 */
public interface PurchaseLookup {
  Purchase findByPurchaseId(Long pid);
}
