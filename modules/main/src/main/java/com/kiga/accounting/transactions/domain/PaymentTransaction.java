package com.kiga.accounting.transactions.domain;

import com.kiga.accounting.domain.Invoice;
import com.kiga.db.KigaEntityModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author bbs
 * @since 2/8/17.
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class PaymentTransaction extends KigaEntityModel {
  private BigDecimal amount;
  private String bank;
  private String bankTransactionId;

  @ManyToOne
  @JoinColumn(name = "invoiceId", referencedColumnName = "id")
  private Invoice invoice;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(nullable = false)
  private Date transferDate;

  @PrePersist
  public void setupClassName() {
    setClassName("PaymentTransaction");
  }
}
