package com.kiga.accounting.web;

import com.kiga.accounting.domain.AcquirerTransaction;
import com.kiga.accounting.formats.CsvTransactionImporter;
import com.kiga.accounting.repository.AcquirerTransactionRepository;
import com.kiga.accounting.web.request.ImportRequest;
import com.kiga.accounting.web.response.AcquirerTransactionsResponse;
import com.kiga.accounting.web.response.ImportResponse;
import com.kiga.payment.repository.PaymentRepository;
import com.kiga.shop.repository.PurchaseRepository;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mfit on 15.03.16. Import and query of acquirer transactions. Allows to - import
 * transaction data from acquirers (such as card complete, sofort, paypal) in CSV format. - query
 * the transactions.
 */
@RestController
@RequestMapping("backend/accounting")
public class AcquirerTransactionController {

  private PurchaseRepository purchaseRepository;

  private PaymentRepository paymentRepository;

  private AcquirerTransactionRepository transactionRepository;

  @Autowired
  public AcquirerTransactionController(
      PurchaseRepository purchaseRepository,
      PaymentRepository paymentRepository,
      AcquirerTransactionRepository acquirerTransactionRepository) {
    this.purchaseRepository = purchaseRepository;
    this.paymentRepository = paymentRepository;
    this.transactionRepository = acquirerTransactionRepository;
  }

  /** Action to import (parse and eventually persist) accounting csv reports. */
  @RequestMapping(value = "import", method = RequestMethod.POST)
  public ImportResponse importCsv(@RequestBody ImportRequest request)
      throws UnsupportedEncodingException {

    int persisted = 0;
    int errors = 0;
    InputStreamResource inputStreamResource =
        new InputStreamResource(
            new ByteArrayInputStream(request.getCsv().getBytes(StandardCharsets.UTF_8)));

    List<AcquirerTransaction> transactions =
        CsvTransactionImporter.readItemsFromResource(
            inputStreamResource, request.getFormatCode(), request.getFilename());

    for (AcquirerTransaction transaction : transactions) {
      try {
        boolean isDuplicate =
            transactionRepository
                    .findByPspTransactionIdAndTransactionDate(
                        transaction.getPspTransactionId(), transaction.getTransactionDate())
                    .size()
                > 0;
        transaction.setIsDuplicate(isDuplicate);
        transaction.setIsImportable(transaction.isValid() && transaction.getIsPrimaryTransaction());

        if (request.getDoImport() && !isDuplicate && transaction.getIsImportable()) {
          transactionRepository.save(transaction);
          persisted += 1;
        }
      } catch (Exception exc) {
        errors += 1;
      }
    }

    return new ImportResponse("ok", transactions, transactions.size(), persisted, errors);
  }

  /** Gets AcquirerTransactions for list view / browsing. */
  @RequestMapping(value = "transactions", method = RequestMethod.GET)
  public AcquirerTransactionsResponse acquirerTransactionListFiltered(
      @RequestParam(value = "fromDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd")
          Date fromDate,
      @RequestParam(value = "toDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd")
          Date toDate) {

    Iterable<AcquirerTransaction> itemIterable;
    if (fromDate != null && toDate != null) {
      itemIterable = transactionRepository.findByDateRange(fromDate, toDate);
    } else {
      itemIterable = transactionRepository.findAll();
    }
    return new AcquirerTransactionsResponse(IteratorUtils.toList(itemIterable.iterator()));
  }
}
