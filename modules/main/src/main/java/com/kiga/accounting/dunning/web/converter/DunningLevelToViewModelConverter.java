package com.kiga.accounting.dunning.web.converter;

import com.kiga.accounting.dunning.domain.DunningLevel;
import com.kiga.accounting.dunning.web.response.DunningLevelViewModel;
import com.kiga.shop.web.converter.CountryToViewModelConverter;
import com.kiga.shop.web.response.CountryViewModel;
import com.kiga.web.converter.DefaultViewModelConverter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author bbs
 * @since 2/19/17.
 */
@Service
public class DunningLevelToViewModelConverter
  extends DefaultViewModelConverter<DunningLevel, DunningLevelViewModel> {

  private CountryToViewModelConverter countryToViewModelConverter;
  private ModelMapper modelMapper;

  @Autowired
  public DunningLevelToViewModelConverter(CountryToViewModelConverter countryToViewModelConverter,
    ModelMapper modelMapper) {
    this.countryToViewModelConverter = countryToViewModelConverter;
    this.modelMapper = modelMapper;
  }

  @Override
  public DunningLevelViewModel convertToResponse(DunningLevel entity) {
    DunningLevelViewModel viewModel = modelMapper.map(entity, DunningLevelViewModel.class);
    CountryViewModel countryViewModel = countryToViewModelConverter
      .convertToResponse(entity.getCountry());
    viewModel.setCountry(countryViewModel);

    return viewModel;
  }
}
