package com.kiga.accounting.formats.mapper;

import com.kiga.accounting.domain.AcquirerTransaction;
import org.apache.commons.io.FilenameUtils;
import org.springframework.batch.item.file.transform.FieldSet;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;


/**
 * Created by mfit on 30.05.16.
 * Map Paypal ("Report") transactions-report raw data to internal representation.
 */
public class PaypalTransactionMapper implements TransactionImportMapper {
  public static String ACQUIRER_NAME = "paypal";
  public static String PSP_NAME = "mpay";
  public static String INTERNAL_CURRENCY = "EUR";
  public static String TRANSACTION_TITLE = "PayPal Express-Zahlung";

  protected String filename;

  protected NumberFormat format = NumberFormat.getInstance(Locale.US);

  protected BigDecimal parseDecimal(String text) {
    try {
      return new BigDecimal(this.format.parse(text).toString(), MathContext.DECIMAL64);
    } catch (ParseException exc) {
      return null;
    }
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  /**
   * Map incoming row to generic transaction.
   * @param fieldSet A FieldSet from down the importer pipeline.
   * @return AcquirerTransaction
     */
  public AcquirerTransaction mapFieldSet(FieldSet fieldSet) {
    AcquirerTransaction ta = new AcquirerTransaction();

    // Amount in original (foreign) currency
    ta.setAmountOrig(parseDecimal(fieldSet.readString("Brutto")));
    ta.setCurrencyOrig(fieldSet.readString("Währung"));

    // Set the internal amount, but only if the currency matches the internal currency
    if (INTERNAL_CURRENCY.equals(fieldSet.readString("Währung"))) {
      ta.setAmount(parseDecimal(fieldSet.readString("Brutto")));
      ta.setCurrency(fieldSet.readString("Währung"));
    }

    ta.setTransactionDate(fieldSet.readDate("Datum", "MM/dd/yyyy"));
    ta.setTransactionDateAcquirer(fieldSet.readDate("Datum", "MM/dd/yyyy"));
    ta.setProcessingDate(fieldSet.readDate("Datum", "MM/dd/yyyy"));
    ta.setSettlementDate(fieldSet.readDate("Datum", "MM/dd/yyyy"));

    ta.setCardType("");

    String combinedNumber = fieldSet.readString("Rechnungsnummer");
    String[] parts = combinedNumber.split("/");
    ta.setPspTransactionId(parts[1].trim());
    ta.setAcquirerTransactionId(fieldSet.readString("Transaktionscode"));

    ta.setAcquirerName(ACQUIRER_NAME);
    ta.setPspName(PSP_NAME);
    ta.setSourceFileName(FilenameUtils.getName(this.filename));
    ta.setIsPrimaryTransaction(TRANSACTION_TITLE.equals(fieldSet.readString("Beschreibung")));

    return ta;
  }
}

