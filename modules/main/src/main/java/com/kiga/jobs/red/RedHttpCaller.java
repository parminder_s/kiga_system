package com.kiga.jobs.red;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneak;

import com.kiga.crm.service.HttpClientFactory;
import com.kiga.web.service.UrlGenerator;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RedHttpCaller {

  private HttpClientFactory httpClientFactory;
  private UrlGenerator urlGenerator;

  @Autowired
  public RedHttpCaller(HttpClientFactory httpClientFactory, UrlGenerator urlGenerator) {
    this.httpClientFactory = httpClientFactory;
    this.urlGenerator = urlGenerator;
  }

  void callRedEndpoint(String endpoint) {

    sneak(
        () -> {
          try (CloseableHttpClient closeableHttpClient = httpClientFactory.getDefaultClient()) {
            String url = urlGenerator.getUrl("https://red.kigaportal.com/" + endpoint);
            HttpUriRequest request = new HttpGet(url);
            closeableHttpClient.execute(request);
          }
          return null;
        });
  }
}
