package com.kiga.jobs;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
public class JobsConfig {
  @Bean
  public ScheduleInitializer getScheduleInitializer(
      ApplicationContext applicationContext,
      ThreadPoolTaskScheduler scheduler,
      Environment environment) {
    return new ScheduleInitializer(
        scheduler, environment, applicationContext, "com.kiga", new JobsRetriever());
  }

  @Bean
  public ThreadPoolTaskScheduler getScheduler() {
    return new ThreadPoolTaskScheduler();
  }
}
