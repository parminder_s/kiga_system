package com.kiga.jobs.red;

import com.kiga.spec.Job;
import com.kiga.spec.Jobable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Job(cronProperty = "red_recreateSortIndexCron")
public class RecreateSortIndexJob implements Jobable {
  private RedHttpCaller redHttpCaller;

  @Autowired
  public RecreateSortIndexJob(RedHttpCaller redHttpCaller) {
    this.redHttpCaller = redHttpCaller;
  }

  @Override
  public void runJob() {
    redHttpCaller.callRedEndpoint("misc/recreateSortIndex");
  }
}
