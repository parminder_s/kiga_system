package com.kiga.jobs.red;

import com.kiga.spec.Job;
import com.kiga.spec.Jobable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Job(cronProperty = "red_recreateRatingsCron")
public class RecreateRatingsJob implements Jobable {
  private RedHttpCaller redHttpCaller;

  @Autowired
  public RecreateRatingsJob(RedHttpCaller redHttpCaller) {
    this.redHttpCaller = redHttpCaller;
  }

  @Override
  public void runJob() {
    redHttpCaller.callRedEndpoint("misc/recreateRatings");
  }
}
