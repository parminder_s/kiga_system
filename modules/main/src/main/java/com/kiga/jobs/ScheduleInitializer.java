package com.kiga.jobs;

import com.kiga.spec.Jobable;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

/**
 * Created by rainerh on 18.10.16.
 */
@Log4j2
public class ScheduleInitializer implements ApplicationListener<ContextRefreshedEvent> {
  private ThreadPoolTaskScheduler scheduler;

  private Environment environment;
  private ApplicationContext applicationContext;
  private String classPath;
  private JobsRetriever jobsRetriever;

  /**
   * constructor.
   */
  public ScheduleInitializer(
    ThreadPoolTaskScheduler scheduler, Environment environment,
    ApplicationContext applicationContext, String classPath,
    JobsRetriever jobsRetriever) {
    this.scheduler = scheduler;
    this.environment = environment;
    this.applicationContext = applicationContext;
    this.jobsRetriever = jobsRetriever;
    this.classPath = classPath;
  }

  /**
   * schedule the configured jobs.
   */
  public void onApplicationEvent(ContextRefreshedEvent event) {
    jobsRetriever.getFromClassPath(classPath).stream().forEach(this::scheduleJob);
  }

  private void scheduleJob(JobWithCronProperty jobWithCronProperty) {
    String schedule = this.environment.getProperty("jobs." + jobWithCronProperty.getCronProperty());
    try {
      if (!schedule.isEmpty()) {
        Jobable jobable = this.applicationContext.getBean(jobWithCronProperty.getJobable());
        log.info(
          "scheduling job with " + jobWithCronProperty.getJobable().getName()
            + " with schedule " + schedule);
        scheduler.schedule(new RunnableJob(jobable), new CronTrigger(schedule));
      }
    } catch (Exception exception) {
      throw new RuntimeException(exception);
    }
  }
}
