package com.kiga.jobs;

import com.kiga.spec.Jobable;
import lombok.AllArgsConstructor;

/**
 * Created by rainerh on 27.12.16.
 */
@AllArgsConstructor
public class JobWithCronProperty {
  private Class<? extends Jobable> jobable;

  private String cronProperty;

  public Class<? extends Jobable> getJobable() {
    return jobable;
  }

  public String getCronProperty() {
    return cronProperty;
  }
}
