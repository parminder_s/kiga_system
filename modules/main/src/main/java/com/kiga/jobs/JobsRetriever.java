package com.kiga.jobs;

import com.kiga.spec.Job;
import com.kiga.spec.Jobable;
import org.reflections.Reflections;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by rainerh on 27.12.16.
 *
 */
public class JobsRetriever {
  /**
   * <p>Scans the set classpath and returns the List of class annotated by job along
   * the cronProperty as String in a Pair.</p>
   */
  public List<JobWithCronProperty> getFromClassPath(String classPath) {
    Reflections reflections = new Reflections(classPath);
    Set<Class<? extends Jobable>> jobClasses = reflections.getSubTypesOf(Jobable.class);

    return jobClasses.stream()
      .filter(jobClass -> jobClass.isAnnotationPresent(Job.class))
      .map(jobClass ->
        new JobWithCronProperty(jobClass, jobClass.getAnnotation(Job.class).cronProperty()))
      .collect(Collectors.toList());
  }
}
