package com.kiga.jobs;

import com.kiga.spec.Jobable;

/**
 * Created by rainerh on 18.10.16.
 *
 * <p>Wraps a Job interface into a Runnable so that it can be put into the scheduler.
 */
public class RunnableJob implements Runnable {
  private Jobable jobable;

  public RunnableJob(Jobable jobable) {
    this.jobable = jobable;
  }

  public void run() {
    this.jobable.runJob();
  }
}
