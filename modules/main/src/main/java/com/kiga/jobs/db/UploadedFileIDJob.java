package com.kiga.jobs.db;

import com.kiga.content.repository.ss.draft.KigaIdeaRepositoryDraft;
import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive;
import com.kiga.spec.Job;
import com.kiga.spec.Jobable;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Job(cronProperty = "uploadedFileIDCron")
@Service
public class UploadedFileIDJob implements Jobable {
  private KigaIdeaRepositoryDraft kigaIdeaRepositoryDraft;
  private KigaIdeaRepositoryLive kigaIdeaRepositoryLive;

  @Autowired
  public UploadedFileIDJob(
      KigaIdeaRepositoryDraft kigaIdeaRepositoryDraft,
      KigaIdeaRepositoryLive kigaIdeaRepositoryLive) {
    this.kigaIdeaRepositoryDraft = kigaIdeaRepositoryDraft;
    this.kigaIdeaRepositoryLive = kigaIdeaRepositoryLive;
  }

  @Transactional
  @Override
  public void runJob() {
    // set uploaded file id to null where it is 0 in Article_Live and Article
    kigaIdeaRepositoryLive.updateUploadedFileIdZeroToNull();
    kigaIdeaRepositoryDraft.updateUploadedFileIdZeroToNull();
  }
}
