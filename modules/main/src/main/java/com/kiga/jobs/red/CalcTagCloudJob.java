package com.kiga.jobs.red;

import com.kiga.spec.Job;
import com.kiga.spec.Jobable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Job(cronProperty = "red_calcTagCloudCron")
@Service
public class CalcTagCloudJob implements Jobable {
  private RedHttpCaller redHttpCaller;

  @Autowired
  public CalcTagCloudJob(RedHttpCaller redHttpCaller) {
    this.redHttpCaller = redHttpCaller;
  }

  @Override
  public void runJob() {
    redHttpCaller.callRedEndpoint("misc/calcTagCloud");
  }
}
