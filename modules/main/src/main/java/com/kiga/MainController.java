package com.kiga;

import com.kiga.content.sorttree.SortTreeManager;
import com.kiga.forum.service.ProfileService;
import com.kiga.main.bootstrap.BootstrapController;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import com.kiga.shop.service.CountryService;
import com.kiga.translation.provider.TranslationService;
import com.kiga.web.service.GeoIpService;
import com.kiga.web.service.UserIpService;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import scala.collection.JavaConversions;

/** Created by faxxe on 6/8/16. */
@RestController
public class MainController {
  private UserIpService userIpService;
  private GeoIpService geoIpService;
  private SecurityService securityService;
  private ProfileService profileService;
  private CountryService countryService;
  private TranslationService translationService;

  private BootstrapController bootstrapController;
  private SortTreeManager sortTreeManager;

  /** constructor. */
  @Autowired
  public MainController(
      UserIpService userIpService,
      GeoIpService geoIpService,
      SecurityService securityService,
      ProfileService profileService,
      CountryService countryService,
      TranslationService translationService,
      BootstrapController bootstrapController,
      SortTreeManager sortTreeManager) {
    this.userIpService = userIpService;
    this.geoIpService = geoIpService;
    this.securityService = securityService;
    this.profileService = profileService;
    this.countryService = countryService;
    this.translationService = translationService;
    this.bootstrapController = bootstrapController;
    this.sortTreeManager = sortTreeManager;
  }

  /** Config for frontend for externs without silverstripe. */
  @RequestMapping(value = "/api/main/bootstrap")
  public Map bootstrap(@RequestParam String locale) {
    Map<String, Object> returner = new HashMap<>();

    returner.put("links", bootstrapController.getFixedLinks());
    returner.put("config", bootstrapController.getFixedConfig());
    returner.put("countries", countryService.getAllForDropdown());
    returner.put("locale", locale);
    returner.put("locales", JavaConversions.mapAsJavaMap(translationService.getPlainJson(locale)));
    Map<String, Object> meta = new HashMap<>();
    meta.put("title", "KiGaPortal");
    meta.put("description", "Meta Description");
    meta.put("keywords", "Keywords");
    returner.put("meta", meta);

    return returner;
  }

  // ** Information for Config, Bootstrap, local and member. */
  //  @RequestMapping(value = "/api/main/bootstrap_information")
  //  public BootstrapInformationResponse getBootstrapInfo(@RequestParam String locale)
  //      throws IOException {
  //
  //    return bootstrapService.buildBootstrapInfoResponse(locale);
  //  }

  /** returns the data for the current logged in member. */
  @RequestMapping(value = "/api/main/member")
  public Map getMember() {
    Map<String, Object> returner = new HashMap<>();

    Optional<Member> memberOption = securityService.getSessionMember();
    Map<String, Object> kga = new HashMap<>();
    kga.put("isActive", false);

    if (!memberOption.isPresent()) {
      returner.put("isAnonymous", true);
      returner.put("isCms", false);
      returner.put("kga", kga);
      returner.put("subscription", new HashMap<>());
    } else {
      Member member = memberOption.get();
      returner.put("email", member.getEmail());
      returner.put("isAnonymous", false);
      returner.put("nickname", profileService.getNickname(member));
      returner.put("s3Avatar", profileService.getUrl(member));
      returner.put("isLicenceAdmin", false);
      returner.put("isCms", !securityService.getFromSession("is_crm").equals("1"));
      returner.put("productGroup", "standard");
      returner.put("hasFullLogin", true);
      returner.put("hasTestBlock", false);
      returner.put("kga", kga);
      Map<String, Object> subscription = new HashMap<>();
      subscription.put("group", "standard");
      subscription.put(
          "accessContent", Boolean.valueOf(securityService.getFromSession("access_content")));
      subscription.put(
          "accessPoolIdeas",
          Boolean.valueOf(securityService.getFromSession("access_articles_pool")));
      subscription.put(
          "accessNewIdeas", Boolean.valueOf(securityService.getFromSession("access_articles")));
      subscription.put(
          "isCancelled", Boolean.valueOf(securityService.getFromSession("is_cancelled")));
      subscription.put("isExpired", securityService.getFromSession("product_status").equals("end"));
      subscription.put(
          "isLocked", securityService.getFromSession("product_status").equals("locked"));
      subscription.put("hasFullLogin", securityService.getFromSession("auth_level").equals("2"));
      subscription.put(
          "hasTestBlock", Boolean.valueOf(securityService.getFromSession("has_testblock")));
      returner.put("subscription", subscription);
    }

    return returner;
  }

  /** locales. */
  @RequestMapping(value = "/api/main/locales")
  public Map getLocales(@RequestParam String locale) {
    return JavaConversions.mapAsJavaMap(translationService.getPlainJson(locale));
  }

  @RequestMapping(value = "/api/main/recalculatesorttree")
  public void recalculateSortTree() {
    sortTreeManager.recalculateSortTree();
  }

  @RequestMapping(value = "/main/getIp")
  public String getIp(HttpServletRequest request) {
    return userIpService.getUserIp(request);
  }

  @RequestMapping(value = "/main/getCountry")
  public String getCountry() {
    return geoIpService.resolveCountryCode();
  }

  @RequestMapping(value = "throwException", method = RequestMethod.GET)
  public void throwException() throws Exception {
    throw new Exception();
  }

  @RequestMapping(value = "healthCheck")
  public void health() {}

  public static class LinkEntity {
    private String label;
    private String code;
    private String url;
    private Boolean isLegacy;
    private Boolean inNavbar;

    /** Constructor. */
    public LinkEntity(String label, String code, String url, Boolean isLegacy, Boolean inNavbar) {
      this.label = label;
      this.code = code;
      this.url = url;
      this.isLegacy = isLegacy;
      this.inNavbar = inNavbar;
    }

    public String getLabel() {
      return label;
    }

    public void setLabel(String label) {
      this.label = label;
    }

    public String getCode() {
      return code;
    }

    public void setCode(String code) {
      this.code = code;
    }

    public String getUrl() {
      return url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public Boolean getLegacy() {
      return isLegacy;
    }

    public void setLegacy(Boolean legacy) {
      isLegacy = legacy;
    }

    public Boolean getInNavbar() {
      return inNavbar;
    }

    public void setInNavbar(Boolean inNavbar) {
      this.inNavbar = inNavbar;
    }
  }
}
