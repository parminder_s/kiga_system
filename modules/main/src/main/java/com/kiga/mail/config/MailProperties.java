package com.kiga.mail.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("mail")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MailProperties {
  private MailerType type;
  private boolean redirectMails;
  private String redirectAddress;
  private String defaultSender;
  private String passwordForTestSend;
  private MailPropertiesAws aws;
}
