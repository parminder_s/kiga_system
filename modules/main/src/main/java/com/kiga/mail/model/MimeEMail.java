package com.kiga.mail.model;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneak;

import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.MimeMessageHelper;

import java.io.File;
import java.io.OutputStream;
import java.util.Optional;
import javax.mail.internet.MimeMessage;

/**
 * This class is a wrapper to simplify the usage of EMails. The problem is, that you have tight
 * coupling to MimeMessage, MimeMessageHelper and JavaMailSender. This results in a very bad
 * testability and code readability.
 */
public class MimeEMail implements EMail {
  private MimeMessage mimeMessage;
  private MimeMessageHelper mimeMessageHelper;
  private String body;
  private String logCategory;
  private String logSubCategory;
  private String logReferenceId;

  protected MimeEMail(MimeMessage mimeMessage, MimeMessageHelper mimeMessageHelper) {
    this.mimeMessage = mimeMessage;
    this.mimeMessageHelper = mimeMessageHelper;
  }

  /** set the sender. */
  @Override
  public EMail withFrom(String from) {
    return sneak(
        () -> {
          mimeMessageHelper.setFrom(from);
          return this;
        });
  }

  /** get the sender. */
  @Override
  public Optional<String> getFrom() {
    return sneak(
        () -> {
          if (mimeMessage.getFrom().length == 0) {
            return Optional.empty();
          } else {
            return Optional.of(mimeMessage.getFrom()[0].toString());
          }
        });
  }

  /** set the recipient. */
  @Override
  public EMail withTo(String to) {
    return sneak(
        () -> {
          mimeMessageHelper.setTo(to);
          return this;
        });
  }

  @Override
  public EMail withReplyTo(String replyTo) {
    return sneak(
        () -> {
          mimeMessageHelper.setReplyTo(replyTo);
          return this;
        });
  }

  /** get the recipient. */
  @Override
  public Optional<String> getTo() {
    return sneak(
        () -> {
          if (mimeMessage.getAllRecipients().length == 0) {
            return Optional.empty();
          } else {
            return Optional.of(mimeMessage.getAllRecipients()[0].toString());
          }
        });
  }

  /** set the subject. */
  @Override
  public EMail withSubject(String subject) {
    return sneak(
        () -> {
          mimeMessageHelper.setSubject(subject);
          return this;
        });
  }

  /** get the subject. */
  @Override
  public String getSubject() {
    return sneak(mimeMessage::getSubject);
  }

  /** set the body. */
  @Override
  public EMail withBody(String body) {
    return sneak(
        () -> {
          this.body = body;
          mimeMessageHelper.setText(body, true);
          return this;
        });
  }

  @Override
  public String getBody() {
    return body;
  }

  /** add an attachment. */
  @Override
  public EMail withAttachment(String name, InputStreamSource inputStreamSource) {
    return sneak(
        () -> {
          mimeMessageHelper.addAttachment(name, inputStreamSource);
          return this;
        });
  }

  @Override
  public EMail withAttachment(String name, File file) {
    return sneak(
        () -> {
          mimeMessageHelper.addAttachment(name, file);
          return this;
        });
  }

  /** writes the content of MimeMessage directly to an output stream. */
  @Override
  public void writeTo(OutputStream outputStream) {
    sneak(
        () -> {
          mimeMessage.writeTo(outputStream);
          return null;
        });
  }

  @Override
  public EMail withInline(String contentId, Resource resource) {
    sneak(
        () -> {
          this.mimeMessageHelper.addInline(contentId, resource);
          return null;
        });
    return this;
  }

  public MimeMessage getMimeMessage() {
    return mimeMessage;
  }

  @Override
  public EMail withLogData(String category, String subCategory, String referenceId) {
    this.logCategory = category;
    this.logSubCategory = subCategory;
    this.logReferenceId = referenceId;
    return this;
  }

  @Override
  public String getLogCategory() {
    return this.logCategory;
  }

  @Override
  public String getLogSubCategory() {
    return this.logSubCategory;
  }

  @Override
  public String getLogReferenceId() {
    return this.logReferenceId;
  }
}
