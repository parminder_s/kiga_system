package com.kiga.mail.logic;

import com.kiga.mail.domain.EMailLog;
import com.kiga.mail.model.EMail;
import com.kiga.mail.repository.EMailLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EMailPersister {
  private EMailLogRepository repository;

  @Autowired
  public EMailPersister(EMailLogRepository repository) {
    this.repository = repository;
  }

  public void persist(EMail email) {
    repository.save(
        EMailLog.builder()
            .from(email.getFrom().orElse(""))
            .to(email.getTo().orElse(""))
            .subject(email.getSubject())
            .body(email.getBody())
            .logCategory(email.getLogCategory())
            .logSubCategory(email.getLogSubCategory())
            .logReferenceId(email.getLogReferenceId())
            .build());
  }
}
