This module should be used for sending emails. In the consuming moduls, just inject the Mailer interface as bean and use it like this.

```java
@Service
public class FooService {
  private final Mailer mailer;
  @Autowired
  public FooService(Mailer mailer) {
    this.mailer = mailer;
  }
  
  public void doFoo() {
    mailer.send(eMail -> eMail
      .withTo("somebody@kigaportal.com")
      .withFrom("office@kigaportal.com")
      .withSubject("An EMail")
      .withBody("some <h1>html</h1>"));
  }
}
```

The mail service exists in two implementations that can be set via the configuration. We have a LogMailer and an AmazonSesMailer. The LogMailer is the default one and just prints out the mail data to the log. The AmazonSesMailer uses AWS services to send mails and is configured for the live system.
