package com.kiga.mail;

import com.kiga.mail.model.EMail;

import java.util.function.UnaryOperator;

public interface Mailer {
  EMail createEMail();

  void send(String logCategory, String logSubCategory, String logReferenceId, EMail email);

  void send(
      String logCategory,
      String logSubCategory,
      String logReferenceId,
      UnaryOperator<EMail> unaryOperator);
}
