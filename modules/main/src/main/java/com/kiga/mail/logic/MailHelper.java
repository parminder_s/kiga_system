package com.kiga.mail.logic;

import com.kiga.mail.model.EMail;
import com.kiga.mail.model.EMailFactory;
import lombok.Getter;

import java.util.Optional;

@Getter
public class MailHelper {
  private String defaultSender;
  private Optional<String> redirectAddress;
  private EMailFactory emailFactory;

  /** inject deps. */
  public MailHelper(
      String defaultSender, EMailFactory emailFactory, Optional<String> redirectAddress) {
    this.defaultSender = defaultSender;
    this.emailFactory = emailFactory;
    this.redirectAddress = redirectAddress;
  }

  /**
   * should be used before a message is actually sent.
   *
   * <p>It sets the default sender if empty and redirects if configured.
   */
  public void preProcess(EMail email) {
    redirectAddress.ifPresent(
        redirectMail -> {
          email.withTo(redirectMail);
        });

    if (!email.getFrom().isPresent()) {
      email.withFrom(this.defaultSender);
    }
  }

  /** Creates a message by using the MimeMessageHelper. */
  public EMail createEMail() {
    return emailFactory.createEMail();
  }
}
