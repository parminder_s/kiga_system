package com.kiga.mail.domain;

import com.kiga.db.KigaEntityModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;

@Entity
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Builder
public class EMailLog extends KigaEntityModel {
  @Column(name = "fromAddress")
  private String from;

  @Column(name = "toAddress")
  private String to;

  @Column(name = "replyToAddress")
  private String replyTo;

  private String subject;
  private String body;
  private String logCategory;
  private String logSubCategory;
  private String logReferenceId;

  @PrePersist
  public void doClassName() {
    this.setClassName("EMailLog");
  }
}
