package com.kiga.mail.repository;

import com.kiga.mail.domain.EMailLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EMailLogRepository extends JpaRepository<EMailLog, Long> {}
