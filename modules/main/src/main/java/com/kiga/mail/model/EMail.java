package com.kiga.mail.model;

import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.OutputStream;
import java.util.Optional;

public interface EMail {
  EMail withFrom(String from);

  Optional<String> getFrom();

  EMail withTo(String to);

  EMail withReplyTo(String replyTo);

  Optional<String> getTo();

  EMail withSubject(String subject);

  String getSubject();

  EMail withBody(String body);

  String getBody();

  String getLogCategory();

  String getLogSubCategory();

  String getLogReferenceId();

  EMail withInline(String contentId, Resource resource);

  EMail withAttachment(String name, InputStreamSource inputStreamSource);

  EMail withAttachment(String name, File file);

  EMail withLogData(String category, String subCategory, String referenceId);

  void writeTo(OutputStream outputStream);
}
