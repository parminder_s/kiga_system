package com.kiga.mail.logic;

import com.kiga.mail.Mailer;
import com.kiga.mail.model.EMail;
import lombok.extern.log4j.Log4j2;

import java.util.function.UnaryOperator;

@Log4j2
public class LogMailer implements Mailer {
  private MailHelper mailHelper;

  public LogMailer(MailHelper mailHelper) {
    this.mailHelper = mailHelper;
  }

  @Override
  public EMail createEMail() {
    return mailHelper.createEMail();
  }

  @Override
  public void send(
      String logCategory,
      String logSubCategory,
      String logReferenceId,
      UnaryOperator<EMail> unaryOperator) {
    send(logCategory, logSubCategory, logReferenceId, unaryOperator.apply(createEMail()));
  }

  @Override
  public void send(String logCategory, String logSubCategory, String logReferenceId, EMail email) {
    mailHelper.preProcess(email);
    log.info("Mail " + email.getSubject() + " with content " + email.getBody());
  }
}
