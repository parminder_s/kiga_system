package com.kiga.mail;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.kiga.mail.config.MailProperties;
import com.kiga.mail.config.MailPropertiesAws;
import com.kiga.mail.logic.AmazonSesMailer;
import com.kiga.mail.logic.EMailPersister;
import com.kiga.mail.logic.LogMailer;
import com.kiga.mail.logic.MailHelper;
import com.kiga.mail.model.EMailFactory;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Optional;

@Log4j2
@Configuration
public class MailConfig {

  /** generates the default mailer bean. */
  @Bean
  public Mailer getMailer(
      MailProperties mailProperties,
      MailHelper mailHelper,
      AmazonSimpleEmailServiceClientBuilder builder,
      EMailPersister persister) {
    switch (mailProperties.getType()) {
      case AWS:
        return new AmazonSesMailer(mailHelper, builder.build(), persister);
      default:
        return new LogMailer(mailHelper);
    }
  }

  @Bean
  public EMailFactory getEMailFactory() {
    return new EMailFactory(new JavaMailSenderImpl());
  }

  /** Instantiate Mail Helper bean. */
  @Bean
  public MailHelper getMailHelper(MailProperties mailProperties, EMailFactory emailFactory) {
    String defaultSender = mailProperties.getDefaultSender();
    Optional<String> redirectAddress = fetchRedirectAddress(mailProperties);
    return new MailHelper(defaultSender, emailFactory, redirectAddress);
  }

  /** returns the builder required for creating an AWSMailer. */
  @Bean
  public AmazonSimpleEmailServiceClientBuilder awsClientBuilder(MailProperties mailProperties) {
    MailPropertiesAws mailPropertiesAws = mailProperties.getAws();
    AWSCredentials credentials =
        new BasicAWSCredentials(mailPropertiesAws.getKey(), mailPropertiesAws.getSecret());

    return AmazonSimpleEmailServiceClientBuilder.standard()
        .withCredentials(new AWSStaticCredentialsProvider(credentials))
        .withRegion(mailPropertiesAws.getRegion());
  }

  private Optional<String> fetchRedirectAddress(MailProperties mailProperties) {
    if (mailProperties.isRedirectMails()) {
      if (StringUtils.isEmpty(mailProperties.getRedirectAddress())) {
        return Optional.empty();
      } else {
        log.info("redirect all mails to " + mailProperties.getRedirectAddress());
        return Optional.of(mailProperties.getRedirectAddress());
      }
    } else {
      return Optional.empty();
    }
  }
}
