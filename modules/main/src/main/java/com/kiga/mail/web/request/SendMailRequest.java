package com.kiga.mail.web.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SendMailRequest {
  private String from = "technik@kigaportal.com";
  private String to = "technik@kigaportal.com";
  private String body = "This is a test mail";
  private String subject = "Testmail";
  private String password;
}
