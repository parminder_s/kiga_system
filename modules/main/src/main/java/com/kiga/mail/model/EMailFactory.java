package com.kiga.mail.model;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneak;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;

public class EMailFactory {
  private JavaMailSender javaMailSender;

  public EMailFactory(JavaMailSender javaMailSender) {
    this.javaMailSender = javaMailSender;
  }

  /**
   * factory method returning a new EMail instance.
   */
  public EMail createEMail() {
    MimeMessage mimeMessage = javaMailSender.createMimeMessage();
    MimeMessageHelper helper =
      sneak(() -> new MimeMessageHelper(mimeMessage, true, "UTF-8"));
    return new MimeEMail(mimeMessage, helper);
  }
}
