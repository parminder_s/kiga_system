package com.kiga.mail.config;

import com.amazonaws.regions.Regions;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MailPropertiesAws {
  private String key;
  private String secret;
  private Regions region;
}
