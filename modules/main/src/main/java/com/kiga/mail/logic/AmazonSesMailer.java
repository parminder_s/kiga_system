package com.kiga.mail.logic;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneak;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.kiga.mail.Mailer;
import com.kiga.mail.model.EMail;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Async;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.function.UnaryOperator;

@Log4j2
public class AmazonSesMailer implements Mailer {
  private MailHelper mailHelper;
  private AmazonSimpleEmailService client;
  private EMailPersister persister;

  public AmazonSesMailer(
      MailHelper mailHelper, AmazonSimpleEmailService service, EMailPersister persister) {
    this.mailHelper = mailHelper;
    this.client = service;
    this.persister = persister;
  }

  @Override
  @Async
  public void send(String logCategory, String logSubCategory, String referenceId, EMail email) {
    email.withLogData(logCategory, logSubCategory, referenceId);
    mailHelper.preProcess(email);

    try {
      persister.persist(email);
    } catch (Exception exception) {
      log.error("Couldn't persist EMail");
      exception.printStackTrace();
    }

    sneak(
        () -> {
          try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream(); ) {
            email.writeTo(outputStream);
            RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
            SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
            client.sendRawEmail(rawEmailRequest);
          }
          return null;
        });
  }

  @Override
  @Async
  public void send(
      String logCategory,
      String logSubCategory,
      String referenceId,
      UnaryOperator<EMail> unaryOperator) {
    send(logCategory, logSubCategory, referenceId, unaryOperator.apply(createEMail()));
  }

  @Override
  public EMail createEMail() {
    return mailHelper.createEMail();
  }
}
