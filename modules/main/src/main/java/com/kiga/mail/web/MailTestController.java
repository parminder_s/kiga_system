package com.kiga.mail.web;

import com.kiga.mail.Mailer;
import com.kiga.mail.config.MailProperties;
import com.kiga.mail.web.request.SendMailRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import javax.mail.MessagingException;

/** A controller to test various mail settings and to send testmails. Used for debugging only. */
@RestController
@RequestMapping("/test/mail")
public class MailTestController {
  private Mailer mailer;
  private MailProperties mailProperties;

  @Autowired
  public MailTestController(Mailer mailer, MailProperties mailProperties) {
    this.mailer = mailer;
    this.mailProperties = mailProperties;
  }

  /**
   * Outputs mailer configuration and sends a test mail.
   *
   * @return some output to check+verify mailer settings.
   */
  @RequestMapping(value = "/send", method = RequestMethod.POST)
  public String send(@RequestBody SendMailRequest sendMailRequest)
      throws MessagingException, UnsupportedEncodingException {
    if (!allowTestSend(sendMailRequest.getPassword())) {
      return "";
    }

    String response = "Mailer test\n";
    response += "Current Mail Config Type: " + mailProperties.getType() + "\n";
    response += "Mailer Type:" + this.mailer.getClass();

    this.mailer.send(
        "test",
        "controller",
        "/send",
        email ->
            email
                .withSubject(sendMailRequest.getSubject())
                .withBody(sendMailRequest.getBody())
                .withTo(sendMailRequest.getTo())
                .withFrom(sendMailRequest.getFrom()));

    response += "Test mails sent!\n";

    return response;
  }

  /**
   * sends an email containing special chars and attachment.
   *
   * <pre>
   * curl --request POST \
   *   --url http://localhost:8080/test/mail/send-kitchen-sink \
   *   --header 'content-type: application/json' \
   *   --data '{
   *     "password": "kiga123",
   *     "from": "system@kigaportal.com",
   *     "to": "rainer.hahnekamp@gmail.com"
   *   }'
   * </pre>
   */
  @RequestMapping(value = "/send-kitchen-sink", method = RequestMethod.POST)
  public String sendKitchenSink(@RequestBody SendMailRequest sendMailRequest) {
    if (allowTestSend(sendMailRequest.getPassword())) {
      this.mailer.send(
          "test",
          "controller",
          "/send-kitchen-sink",
          email ->
              email
                  .withBody(
                      String.join(
                          "\n",
                          "<h1>ありがとう, Konnichi wa</h1>",
                          "<img src=\"cid:idkigalogo\">",
                          "<h2>This is Japanese and means Hello or in Icelandic</h2>",
                          "<h2>Þakka þér kærlega</h2>"))
                  .withSubject("KiGaPortal KitchenSink (with Attachment) - 您好")
                  .withTo(sendMailRequest.getTo())
                  .withFrom(sendMailRequest.getFrom())
                  .withAttachment(
                      "Attachment.pdf", new ClassPathResource("/com/kiga/mail/Attachment.pdf"))
                  .withInline("idkigalogo", new ClassPathResource("/print/kigalogocolor.png")));
      return "Test mail kitchen sink sent!";
    } else {
      return "";
    }
  }

  private boolean allowTestSend(String password) {
    return StringUtils.isNotEmpty(password)
        && mailProperties.getPasswordForTestSend().equals(password);
  }
}
