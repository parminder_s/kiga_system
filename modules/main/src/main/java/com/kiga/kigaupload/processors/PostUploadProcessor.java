package com.kiga.kigaupload.processors;

import com.kiga.kigaupload.web.response.UploadFileStatus;
import com.kiga.security.domain.Member;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author bbs
 * @since 1/22/17.
 */
public interface PostUploadProcessor {
  void process(Member member, List<MultipartFile> files, List<UploadFileStatus> returner);
}
