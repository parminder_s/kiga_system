package com.kiga.kigaupload.annotation;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author bbs
 * @since 1/22/17.
 */
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(UploadProcessors.class)
public @interface UploadProcessor {
  /**
   * @return the context name
   */
  String context();
}
