package com.kiga.kigaupload.service;

import com.kiga.kigaupload.processors.PostUploadProcessor;
import com.kiga.kigaupload.processors.PreUploadProcessor;
import com.kiga.kigaupload.web.response.UploadFileStatus;
import com.kiga.security.domain.Member;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;

/**
 * @author bbs
 * @since 3/5/17.
 */
@Service
class KigaUploadProcessingService {
  private UploadProcessorLocator uploadProcessorLocator;

  @Autowired
  public KigaUploadProcessingService(UploadProcessorLocator uploadProcessorLocator) {
    this.uploadProcessorLocator = uploadProcessorLocator;
  }

  /**
   * Run preprocessing.
   *
   * @param files   files to preprocess
   * @param context context of preprocessing
   * @param member  member
   */
  void preProcess(List<MultipartFile> files, String context, Member member) {
    List<PreUploadProcessor> preUploadProcessors = Collections.emptyList();
    if (StringUtils.isNotBlank(context)) {
      preUploadProcessors = uploadProcessorLocator
        .lookupProcessors(context, PreUploadProcessor.class);
    }
    preUploadProcessors.forEach(processor -> processor.process(member, files));
  }

  /**
   * Run postprocessing.
   *
   * @param files   files to postprocess
   * @param context context of postprocessing
   * @param member  member
   */
  void postProcess(List<MultipartFile> files, List<UploadFileStatus> returner, String context,
    Member member) {
    List<PostUploadProcessor> postUploadProcessors = Collections.emptyList();

    if (StringUtils.isNotBlank(context)) {
      postUploadProcessors = uploadProcessorLocator
        .lookupProcessors(context, PostUploadProcessor.class);
    }
    postUploadProcessors.forEach(processor -> processor.process(member, files, returner));
  }
}
