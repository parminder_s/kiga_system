package com.kiga.kigaupload.web;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.kiga.kigaupload.exception.ContentTypeMismatchException;
import com.kiga.kigaupload.exception.EmptyMultipartFilesListException;
import com.kiga.kigaupload.service.KigaUploadService;
import com.kiga.kigaupload.web.response.UploadFileStatus;
import com.kiga.kigaupload.web.response.UploadResponse;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import com.kiga.upload.FileType;
import com.kiga.upload.converter.MultipartFileUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 1/15/17.
 */
@RestController
@RequestMapping("kiga-upload")
class KigaUploadController {
  private KigaUploadService kigaUploadService;
  private SecurityService securityService;
  private MultipartFileUtil multipartFileUtil;

  /**
   * Constructor for all dependencies required.
   *
   * @param kigaUploadService kiga upload service
   * @param securityService   security service
   * @param multipartFileUtil multipartFileUtil
   */
  @Autowired
  public KigaUploadController(KigaUploadService kigaUploadService, SecurityService securityService,
    MultipartFileUtil multipartFileUtil) {
    this.kigaUploadService = kigaUploadService;
    this.securityService = securityService;
    this.multipartFileUtil = multipartFileUtil;
  }

  /**
   * Save files pushed from the form.
   *
   * @param files files from the form
   * @return upload response
   */
  @RequestMapping("push")
  public UploadResponse push(
    @RequestParam(value = "acceptedTypes", required = false) String[] acceptedTypes,
    @RequestParam(value = "context", required = false) String context,
    @RequestParam("files") List<MultipartFile> files,
    @RequestParam(value = "cannedAccessControlList", required = false)
      String cannedAccessControlList) throws
    EmptyMultipartFilesListException, ContentTypeMismatchException {
    if (CollectionUtils.isEmpty(files)) {
      throw new EmptyMultipartFilesListException();
    }
    securityService.requiresFullSubscription();
    Member member = securityService.getSessionMember().get();

    if (StringUtils.isBlank(cannedAccessControlList)) {
      cannedAccessControlList = "Private";
    }

    if (ArrayUtils.isNotEmpty(acceptedTypes)
      && containsNotAcceptedContentTypes(acceptedTypes, files)) {
      throw new ContentTypeMismatchException();
    }

    List<UploadFileStatus> uploadFileStatuses = kigaUploadService
      .uploadAndProcess(files, context, CannedAccessControlList.valueOf(cannedAccessControlList),
        member);

    UploadResponse uploadResponse = new UploadResponse();
    uploadResponse.setUploadedFiles(uploadFileStatuses);

    return uploadResponse;
  }

  private boolean containsNotAcceptedContentTypes(String[] acceptedTypes,
    List<MultipartFile> files) {
    List<String> filteredContentTypes = Arrays.asList(acceptedTypes).stream()
      .filter(StringUtils::isNotBlank)
      .map(StringUtils::upperCase)
      .map(FileType::valueOf)
      .map(FileType::getContentTypes)
      .flatMap(List::stream)
      .collect(Collectors.toList());

    long correctContentTypeFilesCount = files.stream()
      .filter(file -> file != null)
      .map(file -> {
        try {
          return multipartFileUtil.getContentType(file);
        } catch (Exception exc) {
          return null;
        }
      })
      .filter(contentType -> StringUtils.isNotBlank(contentType)
        && filteredContentTypes.indexOf(contentType) > -1)
      .count();

    return correctContentTypeFilesCount != files.size();
  }
}
