package com.kiga.kigaupload.service;

import com.kiga.kigaupload.annotation.UploadProcessor;
import com.kiga.util.AnnotatedClassFinder;
import com.kiga.util.InstanceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 3/5/17.
 */
@Service
class UploadProcessorLocator {
  private AnnotatedClassFinder annotatedClassFinder;
  private InstanceProvider instanceProvider;

  @Autowired
  public UploadProcessorLocator(AnnotatedClassFinder annotatedClassFinder,
    InstanceProvider instanceProvider) {
    this.annotatedClassFinder = annotatedClassFinder;
    this.instanceProvider = instanceProvider;
  }

  /**
   * Find classes of a processorClass type, having a specific context.
   *
   * @param context context to be found
   * @param processorClass class to be found
   * @param <T> type of instantiated objects
   * @return list of instantiated objects
   */
  <T> List<T> lookupProcessors(String context, Class<T> processorClass) {
    return annotatedClassFinder.lookup(UploadProcessor.class, processorClass).stream()
      .filter(obj -> getAnnotationContext(obj).equalsIgnoreCase(context))
      .map(obj -> processorClass.cast(instanceProvider.getInstance(obj)))
      .collect(Collectors.toList());
  }

  private String getAnnotationContext(Class obj) {
    UploadProcessor uploadProcessorClass = (UploadProcessor) obj
      .getAnnotation(UploadProcessor.class);
    return uploadProcessorClass.context();
  }
}
