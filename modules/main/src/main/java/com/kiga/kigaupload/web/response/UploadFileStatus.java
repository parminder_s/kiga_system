package com.kiga.kigaupload.web.response;

import lombok.Data;

/**
 * @author bbs
 * @since 1/26/17.
 */
@Data
public class UploadFileStatus {
  private long id;
  private boolean failed;
  private String error;
  private String name;
  private String type;
  private String url;
}
