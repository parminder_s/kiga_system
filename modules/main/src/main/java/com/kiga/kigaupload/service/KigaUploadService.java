package com.kiga.kigaupload.service;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.kiga.kigaupload.exception.EmptyMultipartFileException;
import com.kiga.kigaupload.web.request.UniqueNameMultipartFileWrapper;
import com.kiga.kigaupload.web.response.UploadFileStatus;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.security.domain.Member;
import com.kiga.upload.UploadParameters;
import com.kiga.upload.factory.AbstractFileUploader;
import com.kiga.upload.factory.UploadFactory;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author bbs
 * @since 1/19/17.
 */
@Service
public class KigaUploadService {
  private static Logger LOGGER = LoggerFactory.getLogger(KigaUploadService.class);

  private ModelMapper mapper;
  private UploadFactory uploadFactory;
  private KigaUploadProcessingService kigaUploadProcessingService;

  /**
   * Dependencies.
   *
   * @param mapper mapper
   * @param uploadFactory upload factory
   * @param kigaUploadProcessingService kiga upload processing service
   */
  @Autowired
  public KigaUploadService(
      ModelMapper mapper,
      UploadFactory uploadFactory,
      KigaUploadProcessingService kigaUploadProcessingService) {
    this.mapper = mapper;
    this.uploadFactory = uploadFactory;
    this.kigaUploadProcessingService = kigaUploadProcessingService;
  }

  /**
   * Upload and perform processing.
   *
   * @param unwrappedFiles files to upload and process
   * @param context context of uploader
   * @param member member
   * @return list of upload file statuses
   */
  public List<UploadFileStatus> uploadAndProcess(
      List<MultipartFile> unwrappedFiles,
      String context,
      CannedAccessControlList cannedAccessControlList,
      Member member) {
    List<MultipartFile> files =
        unwrappedFiles
            .stream()
            .map(UniqueNameMultipartFileWrapper::new)
            .collect(Collectors.toList());

    kigaUploadProcessingService.preProcess(files, context, member);
    List<UploadFileStatus> returner =
        files
            .stream()
            .map(file -> uploadFile(member, file, cannedAccessControlList))
            .collect(Collectors.toList());
    kigaUploadProcessingService.postProcess(files, returner, context, member);
    return returner;
  }

  private UploadFileStatus uploadFile(
      Member member, MultipartFile file, CannedAccessControlList cannedAccessControlList) {
    UploadFileStatus returner = new UploadFileStatus();
    try {
      KigaS3File kigaS3File = doUpload(member, file, cannedAccessControlList);
      if (kigaS3File != null) {
        returner.setId(kigaS3File.getId());
        returner.setUrl(kigaS3File.getUrl());
        returner.setName(kigaS3File.getName());
      } else {
        returner.setFailed(true);
        returner.setError("Unexpected error - could not upload file.");
      }
      return returner;
    } catch (Exception exception) {
      LOGGER.error("An Exception occurred while trying to upload file.", exception);
      returner.setFailed(true);
      returner.setError(exception.getMessage());
      return returner;
    }
  }

  private KigaS3File doUpload(
      Member member, MultipartFile multipartFile, CannedAccessControlList cannedAccessControlList)
      throws Exception {
    if (multipartFile.isEmpty()) {
      throw new EmptyMultipartFileException();
    }
    AbstractFileUploader uploaderInstance =
        uploadFactory.getUploaderInstance(multipartFile, member);

    UploadParameters uploadParameters = uploaderInstance.getUploadParameters();
    UploadParameters modifiedUploadParameters =
        mapper.map(uploadParameters, UploadParameters.class);
    modifiedUploadParameters.setCannedAccessControlList(cannedAccessControlList);
    uploaderInstance.setUploadParameters(modifiedUploadParameters);
    return uploaderInstance.upload();
  }
}
