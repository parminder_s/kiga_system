package com.kiga.kigaupload.web.response;

import lombok.Data;

import java.util.List;

/**
 * @author bbs
 * @since 1/15/17.
 */
@Data
public class UploadResponse {
  private List<UploadFileStatus> uploadedFiles;
}
