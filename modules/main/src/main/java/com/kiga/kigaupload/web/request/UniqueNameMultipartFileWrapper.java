package com.kiga.kigaupload.web.request;

import com.kiga.util.UniqueFileNameUtil;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by robert on 27.05.17.
 */
public class UniqueNameMultipartFileWrapper implements MultipartFile {
  private MultipartFile originalMultipartFile;
  private String uniqueFileName;
  private UniqueFileNameUtil uniqueFileNameUtil = new UniqueFileNameUtil();

  public UniqueNameMultipartFileWrapper(MultipartFile multipartFile) {
    originalMultipartFile = multipartFile;
    uniqueFileName = uniqueFileNameUtil.getName(multipartFile.getOriginalFilename());
  }

  @Override
  public String getName() {
    return originalMultipartFile.getName();
  }

  @Override
  public String getOriginalFilename() {
    return uniqueFileName;
  }

  @Override
  public String getContentType() {
    return originalMultipartFile.getContentType();
  }

  @Override
  public boolean isEmpty() {
    return originalMultipartFile.isEmpty();
  }

  @Override
  public long getSize() {
    return originalMultipartFile.getSize();
  }

  @Override
  public byte[] getBytes() throws IOException {
    return originalMultipartFile.getBytes();
  }

  @Override
  public InputStream getInputStream() throws IOException {
    return originalMultipartFile.getInputStream();
  }

  @Override
  public void transferTo(File dest) throws IOException, IllegalStateException {
    originalMultipartFile.transferTo(dest);
  }

  public MultipartFile getOriginalMultipartFile() {
    return originalMultipartFile;
  }

  public void setOriginalMultipartFile(
    MultipartFile originalMultipartFile) {
    this.originalMultipartFile = originalMultipartFile;
  }
}
