package com.kiga.kigaupload.processors;

import com.kiga.kigaupload.annotation.UploadProcessor;
import com.kiga.kigaupload.web.response.UploadFileStatus;
import com.kiga.security.domain.Member;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author bbs
 * @since 1/22/17.
 */
@UploadProcessor(context = "dummy")
public class DummyPostUploadProcessor implements PostUploadProcessor {
  @Override
  public void process(Member member, List<MultipartFile> files, List<UploadFileStatus> returner) {
    System.out.println("DummyPostUploadProcessor");
  }
}
