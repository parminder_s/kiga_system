package com.kiga.kigaupload.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author bbs
 * @since 1/22/17.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface UploadProcessors {
  /**
   * @return the contexts
   */
  UploadProcessor[] value();
}
