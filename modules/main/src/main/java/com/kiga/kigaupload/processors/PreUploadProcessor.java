package com.kiga.kigaupload.processors;

import com.kiga.security.domain.Member;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author bbs
 * @since 1/22/17.
 */
public interface PreUploadProcessor {
  void process(Member member, List<MultipartFile> files);
}
