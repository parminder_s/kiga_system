package com.kiga;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("features")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FeaturesProperties {
  private boolean angularCmsEnabled;
  private boolean newIdeaHierarchyFavouritesEnabled;
  private boolean kgaEnabled;
  private boolean bootstrapFixturesEnabled;
}
