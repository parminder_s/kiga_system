package com.kiga;

import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;

/**
 * Created by rainerh on 30.04.16.
 */
public class AsyncExceptionHandler extends SimpleAsyncUncaughtExceptionHandler {
  @Override
  public void handleUncaughtException(Throwable ex, Method method, Object... params) {
    super.handleUncaughtException(ex, method, params);
    StringWriter sw = new StringWriter();
    ex.printStackTrace(new PrintWriter(sw));
    LoggerFactory.getLogger(getClass()).error(sw.toString());
  }
}
