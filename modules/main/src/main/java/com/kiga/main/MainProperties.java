package com.kiga.main;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by rainerh on 28.05.16.
 */
@Configuration
@ConfigurationProperties("main")
@Data
public class MainProperties {
  private PermissionMode permissionMode;
}
