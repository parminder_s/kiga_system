package com.kiga.main.paymentmethod;

/**
 * Created by peter on 04.01.17.
 */
public enum PaymentMethod {
  INVOICE,
  CREDITCARD,
  PAYPAL,
  ELV,
  SOFORT,
  GARANTI,
  UNKNOWN,
  MP_VISA;
}
