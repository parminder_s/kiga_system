package com.kiga.main.paymentmethod;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Created by peter on 18.01.17.
 */
public class PaymentMethodSerializer extends JsonSerializer<PaymentMethod> {
  @Override
  public void serialize(PaymentMethod value, JsonGenerator gen, SerializerProvider serializers)
    throws IOException {
    gen.writeString(String.valueOf(value).replace('_', '-'));
  }
}
