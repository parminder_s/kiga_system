package com.kiga.main.bootstrap;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneak;

import com.kiga.FeaturesProperties;
import com.kiga.bootstrap.api.controller.BootstrapApi;
import com.kiga.bootstrap.api.model.BootstrapLink;
import com.kiga.bootstrap.api.model.BootstrapResponse;
import com.kiga.bootstrap.api.model.Config;
import com.kiga.main.bootstrap.services.BootstrapConfigService;
import com.kiga.main.bootstrap.services.BootstrapLinkService;
import com.kiga.main.bootstrap.services.BootstrapMetaService;
import com.kiga.main.bootstrap.services.BootstrapTranslationService;
import com.kiga.shop.service.CountryService;
import com.kiga.translation.provider.TranslationService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import scala.collection.JavaConversions;

@RestController
public class BootstrapController implements BootstrapApi {
  private BootstrapConfigService bootstrapConfigService;
  private BootstrapLinkService bootstrapLinkService;
  private TranslationService translationService;
  private CountryService countryService;
  private FeaturesProperties featuresProperties;
  private BootstrapTranslationService bootstrapTranslationService;
  private BootstrapMetaService bootstrapMetaService;

  /** constructor. */
  @Autowired
  public BootstrapController(
      BootstrapConfigService bootstrapConfigService,
      BootstrapLinkService bootstrapLinkService,
      TranslationService translationService,
      CountryService countryService,
      FeaturesProperties featuresProperties,
      BootstrapTranslationService bootstrapTranslationService,
      BootstrapMetaService bootstrapMetaService) {
    this.bootstrapConfigService = bootstrapConfigService;
    this.bootstrapLinkService = bootstrapLinkService;
    this.translationService = translationService;
    this.countryService = countryService;
    this.featuresProperties = featuresProperties;
    this.bootstrapTranslationService = bootstrapTranslationService;
    this.bootstrapMetaService = bootstrapMetaService;
  }

  public List getFixedLinks() {
    return bootstrapLinkService.getOldLinks();
  }

  public Config getConfig() {
    return sneak(() -> bootstrapConfigService.requestConfig());
  }

  public Map getFixedConfig() {
    return bootstrapConfigService.requestFixedConfig();
  }

  private Config getFixedBootstrapConfig() {
    return bootstrapConfigService.requestFixedBootstrapConfig();
  }

  private ResponseEntity<BootstrapResponse> getFixedBootstrap(
      String locale, Map<String, String> translations) {
    Config config = getFixedBootstrapConfig();
    return new ResponseEntity<>(
        new BootstrapResponse()
            .config(config.angularCmsEnabled(featuresProperties.isAngularCmsEnabled()))
            .links(bootstrapLinkService.getOldFixedLinks())
            .locales(translations)
            .countries(countryService.getAllforBootstrap(locale))
            .locale(locale),
        HttpStatus.OK);
  }

  /** build bootstrap info response. */
  @Override
  public ResponseEntity<BootstrapResponse> initiateBootstrap(String locale) {
    Map<String, String> translations;

    if (featuresProperties.isBootstrapFixturesEnabled()) {
      translations = JavaConversions.mapAsJavaMap(translationService.getPlainJson(locale));
      return getFixedBootstrap(locale, translations);
    }

    // get from silverstripe!
    translations = getTranslations(locale);

    Config config = getConfig();

    return new ResponseEntity<>(
        new BootstrapResponse()
            .config(config.angularCmsEnabled(featuresProperties.isAngularCmsEnabled()))
            .links(getBootstrapInformationLinks(locale, config.getMemberAdmin(), translations))
            .locales(translations)
            .countries(countryService.getAllforBootstrap(locale))
            .locale(locale)
            .meta(bootstrapMetaService.getMetaData(locale)),
        HttpStatus.OK);
  }

  private Map<String, String> getTranslations(String locale) {
    return sneak(() -> bootstrapTranslationService.getTranslationFromSilverstripe(locale));
  }

  private List<BootstrapLink> getBootstrapInformationLinks(
      String locale, boolean isMemberAdmin, Map<String, String> translations) {
    return bootstrapLinkService.getLinks(locale, isMemberAdmin, translations);
  }
}
