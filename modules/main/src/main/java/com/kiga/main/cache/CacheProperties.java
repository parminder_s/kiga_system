package com.kiga.main.cache;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by faxxe on 8/24/16.
 */

@Configuration
@ConfigurationProperties("web.cache")
public class CacheProperties {

  private String url;
  private Integer port;
  private Boolean enabled;
  private Integer defaultTimeout;
  private Boolean useCache;

  public Boolean getUseCache() {
    return useCache;
  }

  public void setUseCache(Boolean useCache) {
    this.useCache = useCache;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public Integer getDefaultTimeout() {
    return defaultTimeout;
  }

  public void setDefaultTimeout(Integer defaultTimeout) {
    this.defaultTimeout = defaultTimeout;
  }
}
