package com.kiga.main;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.domain.ss.draft.SiteTreeDraft;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.main.locale.Locale;
import com.kiga.main.locale.LocaleDeserializer;
import com.kiga.main.locale.LocaleSerializer;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.main.paymentmethod.PaymentMethodDeserializer;
import com.kiga.main.paymentmethod.PaymentMethodSerializer;
import java.net.URISyntaxException;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;

@Configuration
public class MainConfig {
  @Bean
  public Module getJacksonLocaleModule() {
    SimpleModule returner = new SimpleModule();
    returner.addDeserializer(Locale.class, new LocaleDeserializer());
    returner.addSerializer(Locale.class, new LocaleSerializer());

    returner.addDeserializer(PaymentMethod.class, new PaymentMethodDeserializer());
    returner.addSerializer(PaymentMethod.class, new PaymentMethodSerializer());
    return returner;
  }

  @Bean
  @Primary
  public ModelMapper getModelMapper() {
    return new ModelMapper();
  }

  @Bean(name = "strictMatchingModelMapper")
  public ModelMapper getStrictMatchingModelMapper() {
    ModelMapper modelMapper = new ModelMapper();
    modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    PropertyMap<SiteTreeDraft, SiteTreeLive> siteTreeMap =
        new PropertyMap<SiteTreeDraft, SiteTreeLive>() {
          protected void configure() {
            map().setParent(null);
          }
        };
    PropertyMap<KigaIdeaDraft, KigaIdeaLive> kigaIdeaMap =
        new PropertyMap<KigaIdeaDraft, KigaIdeaLive>() {
          protected void configure() {
            map().setIdeaGroupToIdeaList(null);
          }
        };

    modelMapper.addMappings(siteTreeMap);
    modelMapper.addMappings(kigaIdeaMap);
    return modelMapper;
  }

  @Bean
  public FreeMarkerConfigurationFactoryBean freeMarkerConfiguration() throws URISyntaxException {
    FreeMarkerConfigurationFactoryBean configuration = new FreeMarkerConfigurationFactoryBean();
    configuration.setTemplateLoaderPath("classpath:/templates/");
    configuration.setPreferFileSystemAccess(false);
    return configuration;
  }
}
