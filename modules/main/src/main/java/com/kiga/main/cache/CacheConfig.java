package com.kiga.main.cache;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.support.CompositeCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import redis.clients.jedis.JedisShardInfo;

/**
 * Created by faxxe on 8/24/16.
 */

@Configuration
@EnableCaching
public class CacheConfig {
  @Autowired
  private CacheProperties cacheProperties;

  /**
   * comment.
   */
  @Bean
  public CacheManager cacheManager(RedisTemplate redisTemplate) {
    CompositeCacheManager compositeCacheManager;
    if (BooleanUtils.isTrue(cacheProperties.getEnabled())) {
      KigaCacheManager kigaCacheManager = new KigaCacheManager(
        redisTemplate, cacheProperties.getDefaultTimeout(),
        cacheProperties.getUseCache());
      kigaCacheManager.setDefaultExpiration(cacheProperties.getDefaultTimeout());
      compositeCacheManager = new CompositeCacheManager(kigaCacheManager);
      compositeCacheManager.setFallbackToNoOpCache(true);
    } else {
      compositeCacheManager = new CompositeCacheManager();
      compositeCacheManager.setFallbackToNoOpCache(true);
    }

    return compositeCacheManager;
  }

  @Bean
  public JedisConnectionFactory jedisConnectionFactory() {
    return new JedisConnectionFactory(new JedisShardInfo(cacheProperties.getUrl(),
      cacheProperties.getPort()));
  }

  /**
   * cache.
   */
  @Bean
  public RedisTemplate redisTemplate(RedisConnectionFactory redisConnectionFactory,
                                     RedisSerializer redisSerializer) {
    RedisTemplate redisTemplate = new RedisTemplate();
    redisTemplate.setConnectionFactory(redisConnectionFactory);
    redisTemplate.setDefaultSerializer(redisSerializer);
    return redisTemplate;
  }

  @Bean
  public RedisSerializer<Object> jackson2JsonRedisSerializer() {
    return new GenericJackson2JsonRedisSerializer();
  }


  @Bean
  public CachingConfigurer cachingConfigurer() {
    return new KigaCacheConfigurer(new KigaCacheErrorHandler());
  }


  public CacheProperties getCacheProperties() {
    return cacheProperties;
  }

  public void setCacheProperties(CacheProperties cacheProperties) {
    this.cacheProperties = cacheProperties;
  }

}
