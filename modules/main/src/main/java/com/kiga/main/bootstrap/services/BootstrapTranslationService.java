package com.kiga.main.bootstrap.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.crm.service.HttpClientFactory;
import com.kiga.web.service.UrlGenerator;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BootstrapTranslationService {

  private HttpClientFactory httpClientFactory;
  private Map<String, Map<String, String>> translationCache = new HashMap<>();
  private ObjectMapper objectMapper;
  private UrlGenerator urlGenerator;

  @Autowired
  public BootstrapTranslationService(
      HttpClientFactory httpClientFactory, ObjectMapper objectMapper, UrlGenerator urlGenerator) {

    this.httpClientFactory = httpClientFactory;
    this.objectMapper = objectMapper;
    this.urlGenerator = urlGenerator;
  }

  public Map<String, String> getTranslationFromSilverstripe(String locale) throws IOException {
    //    if (translationCache.containsKey(locale)) {
    //      return translationCache.get(locale);
    //    }

    try (CloseableHttpClient closeableHttpClient = httpClientFactory.getDefaultClient()) {
      String translationUrl = urlGenerator.getUrl("api/main/locales?locale=" + locale);
      HttpUriRequest request = new HttpGet(translationUrl);
      CloseableHttpResponse response = closeableHttpClient.execute(request);
      HttpEntity responseEntity = response.getEntity();
      if (responseEntity != null) {
        String entity = EntityUtils.toString(responseEntity);
        translationCache.put(locale, objectMapper.readValue(entity, Map.class));
      }
      return translationCache.get(locale);
    }
  }
}
