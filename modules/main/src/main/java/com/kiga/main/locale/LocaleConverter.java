package com.kiga.main.locale;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.map.HashedMap;

import java.util.Map;

/**
 * Created by rainerh on 09.11.16.
 *
 * <p>This converter is and should be used by any serializer format (json, jpa ,yaml).</p>
 */
public class LocaleConverter {
  /**
   * list of all existing locales in short notation.
   */
  private static Map<String, Locale> shortMap =
    MapUtils.putAll(new HashedMap<String, Locale>(), new Object[][] {
      {"de", Locale.de},
      {"en", Locale.en},
      {"it", Locale.it},
      {"tr", Locale.tr}
    });

  /**
   * list of all existing locales in long notation.
   */
  private static Map<String, Locale> longMap =
    MapUtils.putAll(new HashedMap<String, Locale>(), new Object[][] {
      {"de_DE", Locale.de},
      {"en_US", Locale.en},
      {"it_IT", Locale.it},
      {"tr_TR", Locale.tr}
    });

  /**
   * returns teh locale from a short or long notation.
   */
  public static Locale toLocale(String value) {
    Map<String, Locale> map = new HashedMap<>();
    map.putAll(shortMap);
    map.putAll(longMap);

    for (Map.Entry<String, Locale> entry: map.entrySet()) {
      if (entry.getKey().equals(value)) {
        return entry.getValue();
      }
    }

    if (value == null) {
      throw new IllegalArgumentException("null attribute");
    } else {
      throw new IllegalArgumentException("Unknown " + value);
    }
  }

  public static String toShort(Locale locale) {
    return fromLocale(locale, shortMap);
  }

  public static String toLong(Locale locale) {
    return fromLocale(locale, longMap);
  }

  private static String fromLocale(Locale locale, Map<String, Locale> localeMap) {
    if (locale == null) {
      throw new IllegalArgumentException("Locale is null");
    }

    for (Map.Entry<String, Locale> entry: localeMap.entrySet()) {
      if (entry.getValue().equals(locale)) {
        return entry.getKey();
      }
    }
    throw new IllegalArgumentException("Unknown locale");
  }
}
