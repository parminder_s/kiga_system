package com.kiga.main;

/**
 * Created by faxxe on 4/25/17.
 */
public enum PermissionMode {
  FullTest, NonFullTest
}
