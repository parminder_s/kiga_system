package com.kiga.main.locale;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Created by rainerh on 16.11.16.
 */
public class LocaleSerializer extends JsonSerializer<Locale> {
  @Override
  public void serialize(Locale value, JsonGenerator gen, SerializerProvider serializers)
    throws IOException, JsonProcessingException {
    gen.writeString(value.toString());
  }
}
