package com.kiga.main.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.interceptor.CacheErrorHandler;

/**
 * Created by faxxe on 8/24/16.
 */
public class KigaCacheErrorHandler implements CacheErrorHandler {
  private Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public void handleCacheGetError(RuntimeException exception, Cache cache, Object key) {
    logger.info("Cache get Error!" + exception.getMessage() + " key: " + key.toString(),
      exception);
  }

  @Override
  public void handleCachePutError(RuntimeException exception,
                                  Cache cache,
                                  Object key,
                                  Object value) {

  }

  @Override
  public void handleCacheEvictError(RuntimeException exception, Cache cache, Object key) {

  }

  @Override
  public void handleCacheClearError(RuntimeException exception, Cache cache) {

  }
}
