package com.kiga.main.bootstrap.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.bootstrap.api.model.Config;
import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.RepositoryContexts;
import com.kiga.crm.service.HttpClientFactory;
import com.kiga.main.MainProperties;
import com.kiga.web.service.GeoIpService;
import com.kiga.web.service.UrlGenerator;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.Setter;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Setter
public class BootstrapConfigService {

  private HttpClientFactory httpClientFactory;

  private Config config;
  private UrlGenerator urlGenerator;

  private GeoIpService geoIpService;
  private RepositoryContext repositoryContext;
  private MainProperties mainProperties;
  private ObjectMapper objectMapper;

  /** constructor. */
  @Autowired
  public BootstrapConfigService(
      UrlGenerator urlGenerator,
      GeoIpService geoIpService,
      RepositoryContext repositoryContext,
      MainProperties mainProperties,
      HttpClientFactory httpClientFactory,
      ObjectMapper objectMapper) {
    this.urlGenerator = urlGenerator;
    this.geoIpService = geoIpService;
    this.repositoryContext = repositoryContext;
    this.mainProperties = mainProperties;
    this.httpClientFactory = httpClientFactory;
    this.objectMapper = objectMapper;
  }

  /** request new config. */
  public Config requestConfig() throws java.io.IOException {
    if (config != null) {
      return config;
    }

    try (CloseableHttpClient closeableHttpClient = httpClientFactory.getDefaultClient()) {
      String configUrl = urlGenerator.getUrl("api/main/config");
      HttpUriRequest request = new HttpGet(configUrl);
      CloseableHttpResponse response = closeableHttpClient.execute(request);
      HttpEntity responseEntity = response.getEntity();
      if (responseEntity != null) {
        String entity = EntityUtils.toString(responseEntity);
        config = objectMapper.readValue(entity, Config.class);
      }
      return config;
    }
  }

  /** provide config map for externs without silverstripe. */
  public Map requestFixedConfig() {
    Map<String, Object> config = new HashMap<>();

    Object shopCountryCode = geoIpService.resolveCountryCode();

    config.put("currentCountry", shopCountryCode);
    config.put("allowCountrySelection", false);
    config.put("feedbackEnabled", false);
    config.put("jsErrorLogEnabled", false);
    config.put("licenceEnabled", false);
    config.put("locales", Arrays.asList("en", "de", "it", "tr"));
    config.put("currentRepositoryContext", repositoryContext.getContext().name());
    config.put(
        "repositoryContexts",
        Arrays.stream(RepositoryContexts.values()).map(Enum::name).collect(Collectors.toList()));
    config.put("regShowInfo", false);
    config.put("showVersion", false);
    config.put("showMemberIdeas", true);
    config.put("permissionMode", mainProperties.getPermissionMode().name());

    return config;
  }

  public Config requestFixedBootstrapConfig() {
    return (new Config())
        .currentCountry(geoIpService.resolveCountryCode())
        .allowCountrySelection(false)
        .feedbackEnabled(false)
        .jsErrorLogEnabled(false)
        .licenceEnabled(false)
        .locales(Arrays.asList("en", "de", "it", "tr"))
        .currentRepositoryContext(repositoryContext.getContext().name())
        .repositoryContexts(
            Arrays.stream(RepositoryContexts.values()).map(Enum::name).collect(Collectors.toList()))
        .regShowInfo(false)
        .showVersion(false)
        .showMemberIdeas(true)
        .permissionMode(mainProperties.getPermissionMode().name());
  }
}
