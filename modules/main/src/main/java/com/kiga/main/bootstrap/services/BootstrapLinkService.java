package com.kiga.main.bootstrap.services;

import com.kiga.MainController;
import com.kiga.bootstrap.api.model.BootstrapLink;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.main.locale.Locale;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BootstrapLinkService {

  private SiteTreeLiveRepository siteTreeLiveRepository;
  private SiteTreeUrlGenerator siteTreeUrlGenerator;
  private Map<String, List<BootstrapLink>> linkCache;
  private Map<String, Map<String, String>> legacyLinkUrlSegmentMap;

  /** constructor. */
  @Autowired
  public BootstrapLinkService(
      SiteTreeLiveRepository siteTreeLiveRepository, SiteTreeUrlGenerator siteTreeUrlGenerator) {
    this.siteTreeLiveRepository = siteTreeLiveRepository;
    this.siteTreeUrlGenerator = siteTreeUrlGenerator;
    linkCache = new HashMap<>();

    // temporary solution due to absent translations
    legacyLinkUrlSegmentMap = new HashMap<>();
    legacyLinkUrlSegmentMap.put("en", new HashMap<>());
    legacyLinkUrlSegmentMap.put("de", new HashMap<>());
    legacyLinkUrlSegmentMap.put("it", new HashMap<>());
    legacyLinkUrlSegmentMap.put("tr", new HashMap<>());

    legacyLinkUrlSegmentMap.get("en").put("new-ideas", "new-ideas");
    legacyLinkUrlSegmentMap.get("en").put("topic", "topic");
    legacyLinkUrlSegmentMap.get("en").put("monthly-ideas", "monthly-ideas");
    legacyLinkUrlSegmentMap.get("en").put("parents", "parents");

    legacyLinkUrlSegmentMap.get("de").put("new-ideas", "neue-ideen");
    legacyLinkUrlSegmentMap.get("de").put("topic", "thema");
    legacyLinkUrlSegmentMap.get("de").put("monthly-ideas", "monatstipp");
    legacyLinkUrlSegmentMap.get("de").put("parents", "eltern");

    legacyLinkUrlSegmentMap.get("it").put("new-ideas", "nuove-idee");
    legacyLinkUrlSegmentMap.get("it").put("topic", "tema");
    legacyLinkUrlSegmentMap.get("it").put("monthly-ideas", "scegli-il-mese");
    legacyLinkUrlSegmentMap.get("it").put("parents", "genitori");

    legacyLinkUrlSegmentMap.get("tr").put("new-ideas", "yeni-oneriler");
    legacyLinkUrlSegmentMap.get("tr").put("topic", "konular");
    legacyLinkUrlSegmentMap.get("tr").put("monthly-ideas", "ylik-oneriler");
    legacyLinkUrlSegmentMap.get("tr").put("parents", "aileler");
  }

  /** get old bootstrap links. */
  public List getOldLinks() {
    List<MainController.LinkEntity> links = new ArrayList<>();

    links.add(
        new MainController.LinkEntity("My Account", "memberAdmin", "root.member", false, false));
    links.add(
        new MainController.LinkEntity("Search", "mainArticleSearch", "root.search", false, false));
    links.add(new MainController.LinkEntity("Forum", "forum", "/forum", true, true));
    links.add(new MainController.LinkEntity("Shop", "shop", "/en/cms/shop", true, true));
    links.add(
        new MainController.LinkEntity(
            "ideaGroupMainContainer",
            "ideaGroupMainContainer",
            "/ng6/en/ideas/dummy-idea-group-container",
            true,
            true));
    links.add(
        new MainController.LinkEntity(
            "latestProductPage",
            "latestProductPage",
            "/#!/en/cms/shop/lapbags/kitchen-sink",
            true,
            true));

    return links;
  }

  public List<BootstrapLink> getOldFixedLinks() {
    List<BootstrapLink> links = new ArrayList<>();

    links.add(
        (new BootstrapLink())
            .label("My Account")
            .code("memberAdmin")
            .url("root.member")
            .isLegacy(false)
            .inNavbar(false));
    links.add(
        (new BootstrapLink())
            .label("Search")
            .code("mainArticleSearch")
            .url("root.search")
            .isLegacy(false)
            .inNavbar(false));
    links.add(
        (new BootstrapLink())
            .label("Forum")
            .code("forum")
            .url("/forum")
            .isLegacy(true)
            .inNavbar(true));
    links.add(
        (new BootstrapLink())
            .label("Shop")
            .code("shop")
            .url("/en/cms/shop")
            .isLegacy(true)
            .inNavbar(true));
    links.add(
        (new BootstrapLink())
            .label("ideaGroupMainContainer")
            .code("ideaGroupMainContainer")
            .url("/ng6/en/ideas/dummy-idea-group-container")
            .isLegacy(true)
            .inNavbar(true));
    links.add(
        (new BootstrapLink())
            .label("latestProductPage")
            .code("latestProductPage")
            .url("/#!/en/cms/shop/lapbags/kitchen-sink")
            .isLegacy(true)
            .inNavbar(true));
    return links;
  }

  /** get Links for new bootstrap info. */
  public List<BootstrapLink> getLinks(
      String locale, boolean isMemberAdmin, Map<String, String> translations) {
    Locale local = Locale.valueOf(locale);

    if (linkCache.get(locale) != null) {
      return linkCache.get(locale);
    }

    SiteTreeLive servicePage =
        siteTreeLiveRepository.findByClassNameAndLocale("MainServicePage", local).get(0);
    String servicePageLink =
        isMemberAdmin
            ? locale + "/member/account"
            : siteTreeUrlGenerator.getRelativeNgUrl(servicePage);
    SiteTreeLive planPage =
        siteTreeLiveRepository.findByClassNameAndLocale("PlanPage", local).get(0);
    SiteTreeLive mandalaCategory =
        siteTreeLiveRepository.findByClassNameAndLocale("MandalaCategory", local).get(0);
    String mandalaCategoryLink = siteTreeUrlGenerator.getRelativeUrl(mandalaCategory);
    SiteTreeLive forum =
        siteTreeLiveRepository.findByClassNameAndLocale("ForumHolder", local).get(0);
    String forumLink = siteTreeUrlGenerator.getRelativeUrl(forum);

    List<SiteTreeLive> shopMainContainerList =
        siteTreeLiveRepository.findByClassNameAndLocale("ShopMainContainer", local);

    String shopMainContainerLink =
        shopMainContainerList.size() <= 0
            ? "/"
            : siteTreeUrlGenerator.getRelativeNgUrl(shopMainContainerList.get(0));

    List<SiteTreeLive> shopArticlePageList =
        siteTreeLiveRepository.findByClassNameAndLocale("ShopArticlePage", local);
    String shopArticlePageLink =
        shopArticlePageList.size() <= 0
            ? ""
            : siteTreeUrlGenerator.getRelativeNgUrl(shopArticlePageList.get(0));

    SiteTreeLive ideaGroupMainContainer =
        siteTreeLiveRepository.findByClassNameAndLocale("IdeaGroupMainContainer", local).get(0);
    String ideaGroupMainContainerLink =
        siteTreeUrlGenerator.getRelativeNgUrl(ideaGroupMainContainer);
    String ideaGroupMainCategoryLink =
        ideaGroupMainContainerLink
            + "/"
            + ideaGroupMainContainer.getChildren().get(1).getUrlSegment();
    SiteTreeLive mainArticleCategory =
        siteTreeLiveRepository.findByClassNameAndLocale("MainArticleCategory", local).get(0);

    // legacy links
    String newIdeasLabel = translations.get("DIM_NEW_ARTICLES_TITLE");
    String newIdeasUrl = getLegacyUrl(local, "new-ideas");
    String topicLabel = translations.get("DIM_TOPIC_TITLE");
    String topicUrl = getLegacyUrl(local, "topic");
    String monthlyLabel = translations.get("DIM_MONTHLY_TITLE");
    String monthlyUrl = getLegacyUrl(local, "monthly-ideas");
    String parentsLabel = translations.get("DIM_PARENTS_TITLE");
    String parentsUrl = getLegacyUrl(local, "parents");

    SiteTreeLive registrationPage =
        siteTreeLiveRepository.findByClassNameAndLocale("RegistrationPage", local).get(0);
    String registrationPageLink = siteTreeUrlGenerator.getRelativeUrl(registrationPage);

    List<BootstrapLink> footerPages =
        siteTreeLiveRepository
            .findByClassNameAndLocale("FooterPage", local)
            .stream()
            .map(
                footerPage ->
                    new BootstrapLink()
                        .label(footerPage.getTitle())
                        .code("footer")
                        .url(siteTreeUrlGenerator.getRelativeUrl(footerPage))
                        .isLegacy(true)
                        .inNavbar(false))
            .collect(Collectors.toList());

    List<BootstrapLink> links =
        Arrays.asList(
            new BootstrapLink().label("HOME").code("home").url("/").isLegacy(true).inNavbar(false),
            new BootstrapLink()
                .label(servicePage.getTitle())
                .code("memberAdmin")
                .url(servicePageLink)
                .isLegacy(!(isMemberAdmin))
                .inNavbar(false),
            new BootstrapLink()
                .label(mainArticleCategory.getTitle())
                .code("mainArticleCategory")
                .url(mainArticleCategory.getUrlSegment())
                .isLegacy(true)
                .inNavbar(false),
            new BootstrapLink()
                .label(planPage.getTitle())
                .code("planner")
                .url("root.plan.planType.detail")
                .isLegacy(false)
                .inNavbar(true),
            new BootstrapLink()
                .label(topicLabel)
                .code("topic")
                .url(topicUrl)
                .isLegacy(true)
                .inNavbar(true),
            new BootstrapLink()
                .label(monthlyLabel)
                .code("monthly")
                .url(monthlyUrl)
                .isLegacy(true)
                .inNavbar(true),
            new BootstrapLink()
                .label(mandalaCategory.getTitle())
                .code("mandala")
                .url(mandalaCategoryLink)
                .isLegacy(true)
                .inNavbar(true),
            new BootstrapLink()
                .label(forum.getTitle())
                .code("forum")
                .url(forumLink)
                .isLegacy(true)
                .inNavbar(true),
            new BootstrapLink()
                .label(parentsLabel)
                .code("parents")
                .url(parentsUrl)
                .isLegacy(true)
                .inNavbar(true),
            new BootstrapLink()
                .label(translations.get("SHOP_TITLE"))
                .code("shop")
                .url(shopMainContainerLink)
                .isLegacy(true)
                .inNavbar(false),
            new BootstrapLink()
                .label("latestProductPage")
                .code("latestProductPage")
                .url(shopArticlePageLink)
                .isLegacy(false)
                .inNavbar(false),
            new BootstrapLink()
                .label(newIdeasLabel)
                .code("newIdeas")
                .url(newIdeasUrl)
                .isLegacy(true)
                .inNavbar(true),
            new BootstrapLink()
                .label("ideaGroups")
                .code("ideaGroupMainContainer")
                .url(ideaGroupMainCategoryLink)
                .isLegacy(false)
                .inNavbar(false),
            new BootstrapLink()
                .label(translations.get("REGISTRATION"))
                .code("registration")
                .url(registrationPageLink)
                .isLegacy(false)
                .inNavbar(false),
            new BootstrapLink()
                .label(translations.get("SEARCH"))
                .code("mainArticleSearch")
                .url("root.search")
                .isLegacy(false)
                .inNavbar(false),
            new BootstrapLink()
                .label("dataPrivacy")
                .code("dataPrivacy")
                .url("/" + locale + "/footerpages/privacy/")
                .isLegacy(false)
                .inNavbar(false));

    @SuppressWarnings("unchecked")
    List<BootstrapLink> returnedLinks = ListUtils.union(footerPages, links);
    linkCache.put(locale, returnedLinks);
    return returnedLinks;
  }

  private String getLegacyUrl(Locale local, String urlSegment) {
    SiteTreeLive mainArticleCategory =
        siteTreeLiveRepository.findByClassNameAndLocale("MainArticleCategory", local).get(0);
    return "/"
        + local.getValue()
        + "/"
        + mainArticleCategory.getUrlSegment()
        + "/"
        + legacyLinkUrlSegmentMap.get(local.getValue()).get(urlSegment);
  }
}
