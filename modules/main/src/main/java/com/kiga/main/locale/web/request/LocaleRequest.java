package com.kiga.main.locale.web.request;

import com.kiga.main.locale.Locale;
import lombok.Data;

/**
 * Created by robert on 25.04.17.
 */
@Data
public class LocaleRequest {
  private Locale locale;
}
