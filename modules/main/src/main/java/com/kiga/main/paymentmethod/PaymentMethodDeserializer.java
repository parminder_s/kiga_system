package com.kiga.main.paymentmethod;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

/**
 * Created by peter on 18.01.17.
 */
public class PaymentMethodDeserializer extends JsonDeserializer<PaymentMethod> {
  @Override
  public PaymentMethod deserialize(JsonParser parser, DeserializationContext ctxt)
    throws IOException {
    if ("PAYPAL".equals(parser.getValueAsString())) {
      return PaymentMethod.PAYPAL;
    } else if ("MP-CC".equals(parser.getValueAsString())) {
      return PaymentMethod.CREDITCARD;
    } else if ("MP-ELV".equals(parser.getValueAsString())) {
      return PaymentMethod.ELV;
    } else if ("SOFORT".equals(parser.getValueAsString())) {
      return PaymentMethod.SOFORT;
    } else if ("RE".equals(parser.getValueAsString())) {
      return PaymentMethod.INVOICE;
    }
    return PaymentMethod.UNKNOWN;
  }
}
