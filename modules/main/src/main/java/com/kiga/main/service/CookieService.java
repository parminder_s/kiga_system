package com.kiga.main.service;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;


import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 2/7/17.
 */
@Service
@Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CookieService {
  private HttpServletRequest httpServletRequest;
  private HttpServletResponse httpServletResponse;

  @Autowired
  public CookieService(HttpServletRequest httpServletRequest,
    HttpServletResponse httpServletResponse) {
    this.httpServletRequest = httpServletRequest;
    this.httpServletResponse = httpServletResponse;
  }

  public String get(String name) {
    return get(name, null);
  }

  public String get(String name, String orElseValue) {
    return getOptional(name).orElse(orElseValue);
  }

  /**
   * Get optional cookie value.
   *
   * @param name name of the cookie
   * @return optional value of the cookie
   */
  public Optional<String> getOptional(String name) {
    Cookie[] cookies = httpServletRequest.getCookies();
    if (ArrayUtils.isNotEmpty(cookies)) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().equalsIgnoreCase(name)) {
          return Optional.of(cookie.getValue());
        }
      }
    }
    return Optional.empty();
  }

  /**
   * Add new cookie to the response.
   *
   * @param name a <code>String</code> specifying the name of the cookie
   * @param value a <code>String</code> specifying the value of the cookie
   */
  public void add(String name, String value) {
    Cookie cookie = new Cookie(name, value);
    cookie.setPath("/");
    httpServletResponse.addCookie(cookie);
  }

  /**
   * removes a cookie.
   */
  public void remove(String name) {
    Cookie cookie = new Cookie(name, null);
    cookie.setMaxAge(0);
    cookie.setPath("/");
    httpServletResponse.addCookie(cookie);
  }
}
