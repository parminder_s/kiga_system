package com.kiga.main.bootstrap.services;

import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.main.locale.LocaleConverter;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BootstrapMetaService {
  private SiteTreeLiveRepository siteTreeLiveRepository;

  @Autowired
  public BootstrapMetaService(SiteTreeLiveRepository siteTreeLiveRepository) {
    this.siteTreeLiveRepository = siteTreeLiveRepository;
  }

  public Map<String, String> getMetaData(String locale) {
    SiteTreeLive homePage =
        siteTreeLiveRepository
            .findByClassNameAndLocale("HomePage", LocaleConverter.toLocale(locale))
            .get(0);
    Map<String, String> metaMap = new HashMap<>();
    metaMap.put("description", homePage.getMetaDescription());
    metaMap.put("title", homePage.getMetaTitle());
    metaMap.put("keywords", homePage.getMetaKeywords());
    return metaMap;
  }
}
