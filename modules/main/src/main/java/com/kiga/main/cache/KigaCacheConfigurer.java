package com.kiga.main.cache;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.KeyGenerator;

/**
 * Created by faxxe on 8/24/16.
 */
public class KigaCacheConfigurer extends CachingConfigurerSupport {
  private CacheManager cacheManager;
  private CacheResolver cacheResolver;
  private KeyGenerator keyGenerator;
  private CacheErrorHandler cacheErrorHandler;

  /**
   * constructor.
   */
  public KigaCacheConfigurer(CacheErrorHandler cacheErrorHandler) {
    this.cacheErrorHandler = cacheErrorHandler;
  }

  @Override
  public CacheErrorHandler errorHandler() {
    return cacheErrorHandler;
  }
}
