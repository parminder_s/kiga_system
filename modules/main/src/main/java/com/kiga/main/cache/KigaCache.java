package com.kiga.main.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.data.redis.core.RedisOperations;

/**
 * Created by peter on 29.09.16.
 */
public class KigaCache extends RedisCache {
  Logger logger;
  boolean useCache;

  /**
   * Constructs a new <code>RedisCache</code> instance.
   */
  public KigaCache(String name, byte[] prefix,
                   RedisOperations<? extends Object, ? extends Object> redisOperations,
                   long expiration, boolean useCache) {
    super(name, prefix, redisOperations, expiration);
    this.useCache = useCache;
    logger = LoggerFactory.getLogger(getClass());
  }

  public boolean isUseCache() {
    return useCache;
  }

  public void setUseCache(boolean useCache) {
    this.useCache = useCache;
  }

  @Override
  public ValueWrapper get(Object key) {
    if (useCache) {
      return super.get(key);
    }
    return null;
  }
}
