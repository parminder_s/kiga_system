package com.kiga.main.locale;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

/**
 * Created by rainerh on 29.10.16.
 *
 * <p>Since it is too complicated in the current version of Spring to add a customized
 * deserializer to the default ObjectMapper without creating an own one, we register a
 * module that contains the Deserializer.</p>
 *
 * <p>Please not that this should change with v2 of Spring Boot.</p>
  **/
public class LocaleDeserializer extends JsonDeserializer<Locale> {
  /**
   * throws an error if locale is not available.
   */
  public Locale deserialize(
    JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
    return LocaleConverter.toLocale(jsonParser.getValueAsString());
  }
}
