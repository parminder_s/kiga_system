package com.kiga.main.util;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bbs
 * @since 2/10/17.
 */
public class NumberUtil {
  public static boolean isValidId(Long id) {
    return id != null && id.compareTo(0L) > 0;
  }

  public static boolean isNotValidId(Long id) {
    return !isValidId(id);
  }

  /**
   * Returns valid ids list from provided list of ids.
   *
   * @param ids ids to be validated
   * @return valid id list
   */
  public static List<Long> getValidIds(List<Long> ids) {
    List<Long> returner = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(ids)) {
      for (Long id : ids) {
        if (isValidId(id)) {
          returner.add(id);
        }
      }
    }
    return returner;
  }
}
