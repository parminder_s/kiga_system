package com.kiga.main.cache;

import org.springframework.data.redis.cache.RedisCache;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.core.RedisOperations;
import scala.Array;

import java.util.Collection;

/**
 * Created by peter on 29.09.16.
 */
public class KigaCacheManager extends RedisCacheManager {
  long expiration;
  boolean useCache;
  RedisOperations redisOperations;

  /**
   * constructor.
   */
  public KigaCacheManager(RedisOperations redisOperations, long expiration, boolean useCache) {
    super(redisOperations);
    this.expiration = expiration;
    this.useCache = useCache;
    this.redisOperations = redisOperations;
  }

  public KigaCacheManager(RedisOperations redisOperations, Collection<String> cacheNames) {
    super(redisOperations, cacheNames);
  }

  @Override
  protected RedisCache createCache(String cacheName) {
    return new KigaCache(cacheName, Array.emptyByteArray(), redisOperations, expiration, useCache);
  }

  /**
   * use this if you want to set a customized expiration time.
   */
  public void addNewKigaCache(String name, int expiration) {
    super.addCache(new KigaCache(
      name, Array.emptyByteArray(), redisOperations, expiration, useCache));
  }
}
