package com.kiga.main.locale;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rainerh on 26.10.16.
 *
 * <p>Common Locale type for the two possible locale notations (short and long).</p>
 *
 * <p>Since the notation is required on database/entity level, each entity that is using the
 * Locale has to select one of the two possible converters.</p>
 *
 * <p>Locale should be seen as immutable (read-only) type. To emphasize the right usage of it
 * it cannot be instantiated. Only the defined static instances exist.</p>
 */
public class Locale {
  public static final Locale de = new Locale("de");
  public static final Locale en = new Locale("en");
  public static final Locale it = new Locale("it");
  public static final Locale tr = new Locale("tr");

  private String value;

  private Locale(String value) {
    this.value = value;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }

    if (object == null || getClass() != object.getClass()) {
      return false;
    }

    return value.equals(((Locale) object).value);
  }

  @Override
  public int hashCode() {
    return value.hashCode();
  }

  @Override
  public String toString() {
    return this.value;
  }

  public Locale() {
  }

  public String getValue() {
    return value;
  }

  /**
   * Returns Locale based on locale string.
   *
   * @param localeString Locale string
   * @return matched Locale
   */
  public static Locale valueOf(String localeString) {
    List<Locale> matchingLocales = Arrays.asList(de, en, it, tr).stream()
      .filter(locale -> locale.getValue().equals(localeString))
      .collect(Collectors.toList());

    if (matchingLocales.size() > 0) {
      return matchingLocales.get(0);
    }

    return null;
  }
}
