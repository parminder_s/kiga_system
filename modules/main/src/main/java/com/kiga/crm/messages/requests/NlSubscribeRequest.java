package com.kiga.crm.messages.requests;

import lombok.Data;

/**
 * Created by mfit on 01.02.17.
 */

@Data
public class NlSubscribeRequest {
  private String email;
  private String locale;
}
