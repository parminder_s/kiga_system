package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RequestChangeSubscriptionSubscription {
  @JsonProperty("product_id")
  private Integer productId;
}
