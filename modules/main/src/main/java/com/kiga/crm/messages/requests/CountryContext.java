package com.kiga.crm.messages.requests;

import lombok.Data;

/**
 * Created by rainerh on 29.05.16.
 */

@Data
public class CountryContext {
  private String taxNr;
  private String passportNr;
}
