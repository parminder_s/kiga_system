package com.kiga.crm.service;

/**
 * Created by peter on 14.12.16.
 */
public class AuthenticationException extends Exception {
  public AuthenticationException(String message) {
    super(message);
  }
}
