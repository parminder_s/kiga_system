package com.kiga.crm.messages.response;

import com.kiga.memberadmin.domain.InvoiceOption;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by peter on 04.10.16.
 */

@Data
public class InvoiceOptionsResponse {
  List<InvoiceOption> invoiceOptions;
}
