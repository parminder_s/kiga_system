package com.kiga.crm.messages;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by rainerh on 24.05.16.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AboParameter {
  @JsonProperty("product_id")
  private int productId = -1;

  private String country = "";

  @JsonProperty("n_groups")
  private int maxNumberSubUsers = -1;

  @JsonProperty("licence_type")
  private int licenceType = -1;
}
