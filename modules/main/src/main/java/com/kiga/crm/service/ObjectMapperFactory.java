package com.kiga.crm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.kiga.crm.messages.response.InvoiceOptionsResponse;
import com.kiga.crm.service.converter.InvoiceOptionsResponseDeserializer;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.main.paymentmethod.PaymentMethodDeserializer;
import com.kiga.main.paymentmethod.PaymentMethodSerializer;

/**
 * Create an ObjectMapper instance that is configured to work mapper for
 * CrmConverter.
 */
public class ObjectMapperFactory {

  /**
   * Create and configure mapper.
   */
  public static ObjectMapper getObjectMapper() {
    SimpleModule simpleModule = new SimpleModule();
    simpleModule.addDeserializer(PaymentMethod.class, new PaymentMethodDeserializer());
    simpleModule.addSerializer(PaymentMethod.class, new PaymentMethodSerializer());

    simpleModule.addDeserializer(InvoiceOptionsResponse.class,
      new InvoiceOptionsResponseDeserializer());

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.registerModule(simpleModule);
    objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
    objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true);

    return objectMapper;
  }
}
