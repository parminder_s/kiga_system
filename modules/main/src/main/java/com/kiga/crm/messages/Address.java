package com.kiga.crm.messages;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by rainerh on 24.05.16.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
  private String firstname;
  private String lastname;
  private String title;
  private String sex;
  private String street;
  private String housenr;
  private String floor;
  private String doornr;
  private String zip;
  private String city;
  private String state;
  private String country;
  private String organisation = "";
  private String organisation2 = "";
  private String email;
  private String tel;

  @JsonProperty("date_of_birth")
  private String dateOfBirth;

}
