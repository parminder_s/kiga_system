package com.kiga.crm.web.request;

import lombok.Data;

/**
 * Created by peter on 25.10.16.
 */
@Data
public class UpdatePasswordRequest {
  private String newPassword;
  private String oldPassword;
}
