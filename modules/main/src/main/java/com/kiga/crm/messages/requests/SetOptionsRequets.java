package com.kiga.crm.messages.requests;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 19.10.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SetOptionsRequets extends CrmRequest {
  private Options options;
}
