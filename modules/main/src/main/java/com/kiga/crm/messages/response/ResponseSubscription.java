package com.kiga.crm.messages.response;

import lombok.Data;

/**
 * Created by peter on 27.09.16.
 */

@Data
public class ResponseSubscription {
  String customerNr;
  String productId;
  String period;
  String periodStart;
  String periodEnd;
  String country;
  String ngroups;
  String nlSubscribed;
  String hasCancellation;
  String hasTransition;
  String atu;
  String taxNr;
  String taxOffice;
  String renew;
  String isIncomplete;
  String status;
}
