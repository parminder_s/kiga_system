package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

/**
 * Created by peter on 09.11.16.
 */

@Data
public class RequestChangeSubscription {
  private RequestChangeSubscriptionSubscription subscription;
  private Integer now;

  /**
   * Optional.
   * Used to transmit parameters for checkout.
   */
  @JsonProperty("payment_data")
  private Map<String, String> paymentData;


  public RequestChangeSubscription() {
    this.subscription = new RequestChangeSubscriptionSubscription();
  }
}

