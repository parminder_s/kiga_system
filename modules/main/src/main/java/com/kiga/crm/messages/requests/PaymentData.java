package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by mfit on 02.12.16.
 * This structure is used optionally in some requests that trigger a checkout.
 * In addition, it is currently only used with paypal-checkouts.
 */

@Data
public class PaymentData {
  @JsonProperty("checkout_success_url")
  String checkoutSuccessUrl;

  @JsonProperty("checkout_fail_url")
  String checkoutFailUrl;
}
