package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Created by peter on 05.10.16.
 */

@Data
public class Checkout {
  @JsonProperty("payment_option")
  Integer paymentOption;

  @JsonProperty("payment_option_code")
  String paymentOptionCode;

  @JsonProperty("payment_data")
  Map<String, String> paymentData;

  @JsonProperty("payment_acquirer")
  String paymentAcquirer;

  @JsonProperty("payment_psp")
  String paymentPsp;

  Cart cart;
}
