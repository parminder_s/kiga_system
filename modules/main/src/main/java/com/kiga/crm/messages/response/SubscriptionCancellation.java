package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * Created by peter on 04.10.16.
 */

@Data
public class SubscriptionCancellation {
  @JsonProperty("date_ordered")
  Date ordered;
  @JsonProperty("date_cancel")
  Date cancel;
}
