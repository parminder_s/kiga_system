package com.kiga.crm.messages.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.crm.messages.response.PaymentOption;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mfit on 11.10.17.
 * Due to a not-ideal data structure of the CRM-response,
 * map a string=>PaymentOption map into a simple list of PaymentOption.
 */
public class PaymentOptionsDeserializer extends JsonDeserializer<List<PaymentOption>> {
  TypeReference<Map<String, PaymentOption>> typeRef =
    new TypeReference<Map<String, PaymentOption>>() { };

  @Override
  public List<PaymentOption> deserialize(JsonParser parser, DeserializationContext ctxt)
    throws IOException {
    Map<String, PaymentOption> paymentOptionMap = parser.readValueAs(typeRef);
    return new ArrayList<PaymentOption>(paymentOptionMap.values());
  }
}
