package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * Created by peter on 19.10.16.
 */

@Data
public class ResponseCancelSubscription {
  Integer logout;
  Integer status;

  @JsonProperty("date_cancel")
  Date dateCancel;

}
