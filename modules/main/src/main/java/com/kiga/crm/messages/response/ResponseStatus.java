package com.kiga.crm.messages.response;

import lombok.Data;

import java.util.Date;

/**
 * Created by peter on 27.09.16.
 */

@Data
public class ResponseStatus {
  int invoicePaid;
  double invoveOpen;
  Date invoiceDate;
  String invoiceCurrency;
  String invoiceNr;
  double productBalance;
}
