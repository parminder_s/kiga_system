package com.kiga.crm.web;

import com.kiga.crm.messages.response.ResponseGetCustomerData;
import com.kiga.crm.messages.response.ResponseGetProfile;
import com.kiga.crm.service.CrmConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by peter on 27.09.16.
 */
@RestController
@RequestMapping("/test/crm")
public class CrmTestController {

  CrmConverter crmConverter;

  @Autowired
  public CrmTestController(CrmConverter crmConverter) {
    this.crmConverter = crmConverter;
  }

  /**
   *
   * @return testResult.
   * @throws Exception exception.
   */
  @RequestMapping(value = "/jackson")
  public ResponseGetProfile test() throws Exception {
    ResponseGetProfile test = crmConverter.userGetProfile();
    ResponseGetCustomerData test1 = crmConverter.customerGetData(0);
    return test;
  }


}
