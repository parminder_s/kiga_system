package com.kiga.crm.messages.requests;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 02.11.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SetLoginRequest extends CrmRequest {
  private String login;
}
