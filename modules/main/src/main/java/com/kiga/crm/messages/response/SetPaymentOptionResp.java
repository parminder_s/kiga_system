package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

public class SetPaymentOptionResp extends GenericTransactionalResponse {
  @JsonProperty("transaction_result")
  JsonNode transactionResult;
}
