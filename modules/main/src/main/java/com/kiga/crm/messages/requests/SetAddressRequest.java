package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kiga.crm.messages.Address;
import com.kiga.memberadmin.domain.AddressType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 02.11.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SetAddressRequest extends CrmRequest {

  @JsonProperty("type")
  private AddressType adressType;

  private Address address;
}
