package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * This requests changes the number of extra "licenses" (aka "groups" aka sub-users) that
 * this subscription should have.
 * If the new number is lower, a list of usernames that get marked for deletion can be
 * provided.
 * Created by mfit on 02.12.16.
 */
@Data
public class SetGroupCountRequest {
  /**
   * The absolute number of users/groups that this subscription should have in the future.
   */
  private int count;

  /**
   * Optional: list of usernames that will be marked for deletion, in case the number of
   * users is to be reduced.
   */
  private List<String> usernames;

  /**
   * Optional payment-checkout data (applies only for checkouts for paypal).
   */
  @JsonProperty("payment_data")
  private PaymentData paymentData;
}
