package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.Date;

/**
 * Created by peter on 04.10.16.
 */
public class Subscription {
  String customerNr;
  Integer productId;
  Integer period;
  String periodType;
  Date start;
  Date end;
  String country;
  Integer maxSubUsers;
  Integer usedSubUsers;
  Integer nlSubscribed;
  Integer hasCancellation;
  Integer hasTransition;
  String atu;
  String taxNr;
  String taxOffice;
  String renew;
  Integer isIncomplete;
  String status;

  public Subscription() {
  }

  /**
   *
   * @param customerNr .
   * @param productId .
   * @param period .
   * @param start .
   * @param end .
   * @param country .
   * @param maxSubUsers .
   * @param usedSubUsers .
   * @param nlSubscribed .
   * @param hasCancellation .
   * @param hasTransition .
   * @param atu .
   * @param taxNr .
   * @param taxOffice .
   * @param renew .
   * @param isIncomplete .
   * @param status .
   */
  public Subscription(String customerNr, Integer productId, Integer period, Date start, Date end,
                      String country, Integer maxSubUsers, Integer usedSubUsers,
                      Integer nlSubscribed, Integer hasCancellation, Integer hasTransition,
                      String atu, String taxNr, String taxOffice, String renew,
                      Integer isIncomplete, String status, String periodType) {
    this.customerNr = customerNr;
    this.productId = productId;
    this.period = period;
    this.start = start;
    this.end = end;
    this.country = country;
    this.maxSubUsers = maxSubUsers;
    this.usedSubUsers = usedSubUsers;
    this.nlSubscribed = nlSubscribed;
    this.hasCancellation = hasCancellation;
    this.hasTransition = hasTransition;
    this.atu = atu;
    this.taxNr = taxNr;
    this.taxOffice = taxOffice;
    this.renew = renew;
    this.isIncomplete = isIncomplete;
    this.status = status;
    this.periodType = periodType;
  }

  @JsonGetter("customerNr")
  public String getCustomerNr() {
    return customerNr;
  }

  @JsonSetter("customer_nr")
  public void setCustomerNr(String customerNr) {
    this.customerNr = customerNr;
  }

  @JsonGetter("productId")
  public Integer getProductId() {
    return productId;
  }

  @JsonSetter("product_id")
  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  @JsonGetter("usedSubUsers")
  public Integer getUsedSubUsers() {
    return usedSubUsers;
  }

  @JsonSetter("n_groups_used")
  public void setUsedSubUsers(Integer usedSubUsers) {
    this.usedSubUsers = usedSubUsers;
  }

  public Integer getPeriod() {
    return period;
  }

  public void setPeriod(Integer period) {
    this.period = period;
  }

  @JsonGetter("start")
  public Date getStart() {
    return start;
  }

  @JsonSetter("period_start")
  public void setStart(Date start) {
    this.start = start;
  }

  @JsonGetter("end")
  public Date getEnd() {
    return end;
  }

  @JsonSetter("period_end")
  public void setEnd(Date end) {
    this.end = end;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  @JsonGetter("maxSubUsers")
  public Integer getMaxSubUsers() {
    return maxSubUsers;
  }

  @JsonSetter("n_groups")
  public void setMaxSubUsers(Integer maxSubUsers) {
    this.maxSubUsers = maxSubUsers;
  }

  @JsonGetter("nlSubscribed")
  public Integer getNlSubscribed() {
    return nlSubscribed;
  }

  @JsonSetter("nl_subscribed")
  public void setNlSubscribed(Integer nlSubscribed) {
    this.nlSubscribed = nlSubscribed;
  }

  @JsonGetter("hasCancellation")
  public Integer getHasCancellation() {
    return hasCancellation;
  }

  @JsonSetter("has_cancellation")
  public void setHasCancellation(Integer hasCancellation) {
    this.hasCancellation = hasCancellation;
  }

  @JsonGetter("hasTransition")
  public Integer getHasTransition() {
    return hasTransition;
  }

  @JsonSetter("has_transition")
  public void setHasTransition(Integer hasTransition) {
    this.hasTransition = hasTransition;
  }

  public String getAtu() {
    return atu;
  }

  public void setAtu(String atu) {
    this.atu = atu;
  }

  @JsonGetter("taxNr")
  public String getTaxNr() {
    return taxNr;
  }

  @JsonSetter("tax_nr")
  public void setTaxNr(String taxNr) {
    this.taxNr = taxNr;
  }

  @JsonGetter("taxOffice")
  public String getTaxOffice() {
    return taxOffice;
  }

  @JsonSetter("tax_office")
  public void setTaxOffice(String taxOffice) {
    this.taxOffice = taxOffice;
  }

  public String getRenew() {
    return renew;
  }

  public void setRenew(String renew) {
    this.renew = renew;
  }

  @JsonGetter("isIncomplete")
  public Integer getIsIncomplete() {
    return isIncomplete;
  }

  @JsonSetter("is_incomplete")
  public void setIsIncomplete(Integer isIncomplete) {
    this.isIncomplete = isIncomplete;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @JsonGetter("periodType")
  public String getPeriodType() {
    return periodType;
  }

  @JsonSetter("period_type")
  public void setPeriodType(String periodType) {
    this.periodType = periodType;
  }

}
