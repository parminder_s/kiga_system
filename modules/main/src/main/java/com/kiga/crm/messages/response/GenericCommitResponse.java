package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

import java.util.Map;

@Data
public class GenericCommitResponse {
  @JsonProperty("transaction_id")
  String transactionId;

  @JsonProperty("transaction_status")
  String transactionStatus;

  @JsonProperty("transaction_result")
  JsonNode transactionResult;

  @JsonProperty("checkout_result")
  Map<String, String> checkoutResult;
}
