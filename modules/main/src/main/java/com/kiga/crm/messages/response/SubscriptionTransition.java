package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by peter on 04.10.16.
 */

@Data
public class SubscriptionTransition {
  @JsonProperty("subscr_id")
  String id;

  @JsonProperty("subscr_mode")
  String mode;
  String period;

  @JsonProperty("date_ordered")
  Date ordered;

  @JsonProperty("date_change")
  Date change;

  @JsonProperty("n_groups")
  Integer maxSubUsers;

  @JsonProperty("users_remove")
  HashMap<Integer, String> removedUsers;

  @JsonProperty("licence_type")
  String licenceType;
}
