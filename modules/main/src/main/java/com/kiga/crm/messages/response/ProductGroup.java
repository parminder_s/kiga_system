package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by peter on 04.10.16.
 */

@Data
public class ProductGroup {
  String id;

  @JsonProperty("subscr_mode")
  String subscriptionMode;

  public ProductGroup() {
  }

  public ProductGroup(String id, String subscriptionMode) {
    this.id = id;
    this.subscriptionMode = subscriptionMode;
  }
}
