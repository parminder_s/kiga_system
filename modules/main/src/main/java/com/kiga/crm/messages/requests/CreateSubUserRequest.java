package com.kiga.crm.messages.requests;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 02.12.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CreateSubUserRequest extends CrmRequest {
  String email;
  String password;
}
