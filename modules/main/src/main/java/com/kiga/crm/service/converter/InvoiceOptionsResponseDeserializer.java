package com.kiga.crm.service.converter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.kiga.crm.messages.response.InvoiceOptionsResponse;
import com.kiga.memberadmin.domain.InvoiceOption;
import org.modelmapper.internal.util.Lists;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by peter on 08.02.17.
 */
public class InvoiceOptionsResponseDeserializer extends JsonDeserializer<InvoiceOptionsResponse> {

  @Override
  public InvoiceOptionsResponse deserialize(JsonParser parser, DeserializationContext ctxt)
    throws IOException, JsonProcessingException {
    InvoiceOptionsResponse returner = new InvoiceOptionsResponse();
    List<InvoiceOption> invoiceOptionList = new ArrayList<>();

    JsonNode node = parser.getCodec().readTree(parser);
    node.forEach(jsonNode ->
      invoiceOptionList.add(InvoiceOption.valueOf(jsonNode.get("option_id").textValue()))
    );
    returner.setInvoiceOptions(invoiceOptionList);
    return returner;
  }
}
