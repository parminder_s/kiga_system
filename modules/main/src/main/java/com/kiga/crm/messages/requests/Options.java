package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by peter on 19.10.16.
 */

@Data
public class Options {
  String job;

  @JsonProperty("orgType")
  String organization;

  String jobName;
}
