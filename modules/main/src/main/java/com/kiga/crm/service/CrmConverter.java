package com.kiga.crm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.crm.messages.Address;
import com.kiga.crm.messages.DataContainer;
import com.kiga.crm.messages.requests.ChangeSubUserRequest;
import com.kiga.crm.messages.requests.ChangeSubscriptionContainerRequest;
import com.kiga.crm.messages.requests.CountryRequest;
import com.kiga.crm.messages.requests.CreateSubUserRequest;
import com.kiga.crm.messages.requests.CrmRequest;
import com.kiga.crm.messages.requests.GenericDataContainerRequest;
import com.kiga.crm.messages.requests.GetAvailableSubscriptionsRequest;
import com.kiga.crm.messages.requests.GetDataRequest;
import com.kiga.crm.messages.requests.GetInvoiceRequest;
import com.kiga.crm.messages.requests.NlSubscribeRequest;
import com.kiga.crm.messages.requests.Options;
import com.kiga.crm.messages.requests.PasswordRequest;
import com.kiga.crm.messages.requests.RemoveSubUserRequest;
import com.kiga.crm.messages.requests.RequestNewSubscription;
import com.kiga.crm.messages.requests.RequestUpdateCustomerSettingsContainer;
import com.kiga.crm.messages.requests.SetAddressRequest;
import com.kiga.crm.messages.requests.SetGroupCountRequest;
import com.kiga.crm.messages.requests.SetLocaleRequets;
import com.kiga.crm.messages.requests.SetLoginRequest;
import com.kiga.crm.messages.requests.SetOptionsRequets;
import com.kiga.crm.messages.requests.SetPaymentOptionReq;
import com.kiga.crm.messages.requests.UpdatePasswordRequest;
import com.kiga.crm.messages.requests.UserDeleteStatusRequest;
import com.kiga.crm.messages.requests.Username;
import com.kiga.crm.messages.response.ChangeSubscriptionResp;
import com.kiga.crm.messages.response.CrmError;
import com.kiga.crm.messages.response.GenericCommitResponse;
import com.kiga.crm.messages.response.Invoice;
import com.kiga.crm.messages.response.ResponseCancelSubscription;
import com.kiga.crm.messages.response.ResponseGetAvailableSubscriptions;
import com.kiga.crm.messages.response.ResponseGetCustomerData;
import com.kiga.crm.messages.response.ResponseGetProfile;
import com.kiga.crm.messages.response.ResponseGetSubscriptions;
import com.kiga.crm.messages.response.ResponseNewSubscription;
import com.kiga.crm.messages.response.ResponseUpdateCustomerSettings;
import com.kiga.crm.messages.response.SetGroupCountResponseContainer;
import com.kiga.crm.messages.response.SetPaymentOptionResp;
import com.kiga.crm.messages.response.SubUser;
import com.kiga.crm.messages.response.TransactionalCommitCheckout;
import com.kiga.crm.messages.response.TransactionalCommitRequest;
import com.kiga.crm.messages.response.UserDeleteStatusResponse;
import com.kiga.memberadmin.domain.AddressType;
import com.kiga.payment.PaymentType;
import com.kiga.web.security.session.Session;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by peter on 27.09.16.
 */

/**
 * CrmConverter, class to Communicate with CRM.
 */
public class CrmConverter {
  private String crmUrl;
  private ObjectMapper objectMapper;
  private Session session;
  private HttpClientFactory httpClientFactory;

  private <T> T sendRequest(String resourceName, Object param, Class<T> type) throws Exception {

    if (param instanceof CrmRequest) {
      CrmRequest crmRequest = (CrmRequest) param;
      crmRequest.setSessionToken(session.getCrmSessionToken());
      crmRequest.setUserId(session.getCrmUserId());
    }

    HttpResponse httpResponse;
    String result;
    StatusLine statusLine;
    try (CloseableHttpClient httpClient = this.httpClientFactory.getDefaultClient()) {
      HttpPost httpPost = new HttpPost(crmUrl + resourceName);
      String requestBody = objectMapper.writeValueAsString(param);
      httpPost.setEntity(
        new StringEntity(requestBody, ContentType.APPLICATION_JSON));

      httpResponse = httpClient.execute(httpPost);
      statusLine = httpResponse.getStatusLine();
      HttpEntity httpEntity = httpResponse.getEntity();

      // Possibly not really required. (will it ever be null? if it is,
      // do we care to catch/rethrow or just let bubble up? )
      if (httpEntity == null) {
        throw new Exception("Error in sendRequest! CRM returned null");
      }
      result = IOUtils.toString(httpEntity.getContent());

    } catch (HttpHostConnectException exception) {
      throw new Exception("No connection to CRM", exception);
    } catch (IOException exception) {
      throw new Exception("Error getting CRM response body", exception);
    }

    try {
      if (statusLine.getStatusCode() == 200) {

        // Parse response
        return objectMapper.readValue(result, type);

      } else {

        // Parse response into error object
        CrmError crmError;
        crmError = objectMapper.readValue(result, CrmError.class);
        if (crmError.getCode() == 901) {
          // Missing data
          throw new Exception("Missing Data 901");
        }
        if (crmError.getCode() == 902) {
          // Session
          throw new AuthenticationException("Session Expired 902");
        }
        if (crmError.getCode() == 931) {
          // Auth level (requires higher auth level) - re-login
          throw new AuthenticationException("Auth Level 931");
        }
        if (crmError.getCode() == 951) {
          //cant change to this product!
          throw new Exception("Crm Error Code: 951");
        }
        if (crmError.getCode() == 955) {
          //some CRM Exception, 1: updateDuration with not allowed option!
          throw new Exception("Crm Error Code: 955");
        }
        throw new Exception("Unkown CRM ERROR: " + crmError.getCode());
      }

    } catch (IOException errorParseException) {
      // exception.printStackTrace();
      throw new Exception("Crm response parse error (" + resourceName + ")",  errorParseException);
    }
  }

  /**
   * @param crmUrl url to the crm service.
   * @param session kiga security session.
   * @param objectMapper custom objectMapper, crm uses custom objectMapper!
   *                     otherwise errors like: com.fasterxml.jackson.databind.JsonMappingException:
   *                     Can not instantiate value of type [simple type,
   *                     class com.kiga.crm.messages.Address] from String value ('');
   *                     no single-String constructor/factory method
   * @param httpClientFactory for obtaining the http request.
   */
  public CrmConverter(String crmUrl, Session session,  ObjectMapper objectMapper,
                      HttpClientFactory httpClientFactory) {
    this.crmUrl = crmUrl;
    this.session = session;
    this.objectMapper = objectMapper;
    this.httpClientFactory = httpClientFactory;
  }

  public ResponseGetProfile userGetProfile() throws Exception {
    return this.sendRequest("user.getProfile", new CrmRequest(),
      ResponseGetProfile.class);
  }

  //  public ResponseGetProfile userSetProfile() throws Exception {
  //    return this.sendRequest("user.setProfile", new CrmRequest(),
  //      ResponseGetProfile.class);
  //  }

  /**
   *
   * @param locale new Local of the User.
   * @return Todo.
   * @throws Exception in casting CRM Response.
   */
  public Integer userSetLocale(String locale) throws Exception {
    SetLocaleRequets setLocaleRequets = new SetLocaleRequets();
    setLocaleRequets.setLocale(locale);
    return this.sendRequest("user.setLocale", setLocaleRequets, Integer.class);
  }

  /**
   *
   * @param country country from which to get the Subscriptions.
   * @return Information about the possible Subscriptions in the country.
   * @throws Exception in casting CRM Response.
   */
  public ResponseGetSubscriptions masterGetSubscriptions(String country) throws Exception {
    CountryRequest countryRequest = new CountryRequest();
    countryRequest.setCountry(country);
    return this.sendRequest("master.getSubscriptions", countryRequest,
      ResponseGetSubscriptions.class);
  }

  /**
   * Retrieve the subscriptions that are available for a specific
   * customer (the subscriptions the current product could be changed to
   * via a transaction - either "now" or "at product period end").
   * @return structure with information about available changed.
   * @throws Exception in case of CRM error.
   */
  public ResponseGetAvailableSubscriptions customerGetAvailableSubscriptions() throws Exception {
    return this.sendRequest("customer.getAvailableSubscriptions",
      new GetAvailableSubscriptionsRequest(),
      ResponseGetAvailableSubscriptions.class);
  }

  /**
   *
   * @param fetchBitmask Todo.
   * @return Data about the user, depends on the param fetchBitmask.
   * @throws Exception in casting CRM Response.
   */
  public ResponseGetCustomerData customerGetData(Integer fetchBitmask) throws Exception {
    GetDataRequest getDataRequest = new GetDataRequest();
    getDataRequest.setFetchBitmask(fetchBitmask);
    return this.sendRequest("customer.getData", getDataRequest,
      ResponseGetCustomerData.class);
  }

  /**
   *
   * @param requestNewSubscription creates a new Abo.
   * @return ResponseNewSubscription.
   * @throws Exception in casting CRM Response.
   */
  public ResponseNewSubscription customerNewSubscription(
    RequestNewSubscription requestNewSubscription) throws Exception {
    DataContainer dataContainer = new DataContainer();
    dataContainer.setData(requestNewSubscription);
    return this.sendRequest("transactional.newSubscription", dataContainer,
      ResponseNewSubscription.class);
  }

  /**
   *
   * @return list of user Invoices.
   * @throws Exception in casting CRM Response.
   */
  public List<Invoice> customerGetInvoice() throws Exception {
    GetInvoiceRequest invoiceRequest = new GetInvoiceRequest();
    invoiceRequest.setLimitFrom(0);
    invoiceRequest.setLimitN(10000);
    Invoice[] returner = this.sendRequest("customer.getInvoices", invoiceRequest, Invoice[].class);
    if (returner == null) {
      return Collections.emptyList();
    } else {
      return Arrays.asList(returner);
    }
  }

  /**
   *
   * @param job name of the job type.
   * @param organization name of the organization type.
   * @return true if success, false if error.
   * @throws Exception in casting CRM Response.
   */
  public  boolean customerSetOptions(String job, String organization,
                                     String jobName) throws Exception {
    Options options = new Options();
    options.setOrganization(organization);
    options.setJob(job);
    options.setJobName(jobName);
    SetOptionsRequets setOptionsRequets = new SetOptionsRequets();
    setOptionsRequets.setOptions(options);
    return this.sendRequest("customer.setOptions", setOptionsRequets, Integer.class) == 1;
  }

  /**
   *
   * @return ResponseCancelSubscription.
   * @throws Exception in casting CRM Response.
   */
  public  ResponseCancelSubscription customerCancelSubscription() throws Exception {
    return this.sendRequest("customer.cancelSubscription", new CrmRequest(),
      ResponseCancelSubscription.class);
  }

  /**
   *
   * @param request RequestUpdateCustomerSettingsContainer.
   * @return ResponseUpdateCustomerSettings.
   * @throws Exception in casting CRM Response.
   */
  public ResponseUpdateCustomerSettings customerUpdateCustomerSettings(
    RequestUpdateCustomerSettingsContainer request) throws Exception {
    return this.sendRequest("transactional.updateCustomerSettings", request,
      ResponseUpdateCustomerSettings.class);
  }

  /**
   *
   * @param email check if this email is available to create a new abo.
   * @return true if available, false if not.
   * @throws Exception in casting CRM Response.
   */
  public boolean userTestUserName(String email) throws Exception {
    Username username = new Username();
    username.setUsername(email);
    return this.sendRequest("user.testUsername", username, Integer.class) == 1;
  }

  /**
   *
   * @param newPassword the new password of the user.
   * @return true if success, false if error.
   * @throws Exception in casting CRM Response.
   */
  public boolean userSetPassword(String newPassword) throws Exception {
    UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest();
    updatePasswordRequest.setNewPassword(newPassword);
    return this.sendRequest("user.setPassword", updatePasswordRequest, Integer.class) == 1;
  }

  /**
   *
   * @param password Todo.
   * @return true if success, false if error.
   * @throws Exception in casting CRM Response.
   */
  public boolean userTestPassword(String password) throws Exception {
    PasswordRequest passwordRequest = new PasswordRequest();
    passwordRequest.setPassword(password);
    return this.sendRequest("user.testPassword", passwordRequest, Integer.class) == 1;
  }

  /**
   * Subscribe to newsletter.
   * @param email address.
   * @param locale language / locale string.
   * @param country country string.
   * @return status of request
     * @throws Exception in casting CRM Response.
     */
  public boolean userNewsletterSubscribeRequest(String email, String locale, String country)
    throws Exception {
    NlSubscribeRequest nlSubscribeRequest = new NlSubscribeRequest();
    nlSubscribeRequest.setEmail(email);
    nlSubscribeRequest.setLocale(locale);
    return this.sendRequest("user.nlSubscribeRequest", nlSubscribeRequest, Integer.class) == 1;
  }

  /**
   *
   * @param addressType Todo.
   * @param address new Address.
   * @param birthday birthday of the user (ignored if null).
   * @return true if success, false if error.
   * @throws Exception in casting CRM Response.
   */
  public boolean customerSetAddress(AddressType addressType, Address address, Date birthday)
    throws Exception {
    SetAddressRequest setAddressRequest = new SetAddressRequest();
    setAddressRequest.setAddress(address);
    setAddressRequest.setAdressType(addressType);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    if (birthday != null) {
      address.setDateOfBirth(simpleDateFormat.format(birthday));
    }
    return this.sendRequest("customer.setAddress", setAddressRequest, Integer.class) == 1;
  }

  public boolean customerRemoveBillingAddress() throws Exception {
    return this.sendRequest("customer.removeBillingAddress", new CrmRequest(), Integer.class) == 1;
  }

  /**
   *
   * @param login new login of the user.
   * @return true if success, false if error.
   * @throws Exception in casting CRM Response.
   */
  public boolean userSetLogin(String login) throws Exception {
    SetLoginRequest setLoginRequest = new SetLoginRequest();
    setLoginRequest.setLogin(login);
    return this.sendRequest("user.setLogin", setLoginRequest, Integer.class) == 1;
  }

  /**
   * Change subscription (duration).
   * Change the period.
   */
  public ChangeSubscriptionResp transactionalChangeSubscription(
    ChangeSubscriptionContainerRequest request) throws Exception {

    return this.sendRequest("transactional.changeSubscription", request,
      ChangeSubscriptionResp.class);
  }

  public void customerCancelSubscriptionUndo() throws Exception {
    this.sendRequest("customer.cancelSubscriptionUndo", new CrmRequest(), Object.class);
  }

  public List<SubUser> multiuserListSubUsers() throws Exception {
    return Arrays.asList(this.sendRequest("multiuser.listSubUsers",
      new CrmRequest(), SubUser[].class));
  }

  /**
   *
   * @param email email of the new subuser.
   * @param password password of the new subuser.
   * @return true if success, false if error.
   * @throws Exception in casting CRM Response.
   */
  public Boolean multiuserCreateSubUser(String email, String password) throws Exception {
    CreateSubUserRequest createSubUserRequest = new CreateSubUserRequest();
    createSubUserRequest.setEmail(email);
    createSubUserRequest.setPassword(password);
    return this.sendRequest("multiuser.createSubUser", createSubUserRequest, Integer.class) == 1;
  }

  /**
   * @param subUserId id of the subuser to be deleted.
   * @return true if success, false if error.
   * @throws Exception in casting CRM Response.
   */
  public Boolean multiuserRemoveSubUser(Integer subUserId) throws Exception {
    RemoveSubUserRequest removeSubUserRequest = new RemoveSubUserRequest();
    removeSubUserRequest.setSubuserId(subUserId);
    return this.sendRequest("multiuser.removeSubUser", removeSubUserRequest, Integer.class) == 1;
  }

  /**
   *
   * @param subUserId id of the subuser to be updated.
   * @param email new email of the subuser.
   * @param password new password of the subuser.
   * @return true if success, false if error.
   * @throws Exception in casting CRM Response.
   */
  public Boolean multiuserChangeSubUser(Integer subUserId, String email, String password)
    throws Exception {
    ChangeSubUserRequest changeSubUserRequest = new ChangeSubUserRequest();
    changeSubUserRequest.setSubuserId(subUserId);
    changeSubUserRequest.setEmail(email);
    changeSubUserRequest.setPassword(password);
    return this.sendRequest("multiuser.changeSubUser", changeSubUserRequest, Integer.class) == 1;
  }

  /**
   *
   * @param newNumberLicences new number of licences (set after abo period end)
   * @param usersToDelete if number of users is reduces, logins of the users which will be deleted.
   * @return SetGroupCountResponse.
   * @throws Exception in casting CRM Response.
   */
  public SetGroupCountResponseContainer transactionalSetGroupCount(Integer newNumberLicences,
                                                          List<String> usersToDelete)
    throws Exception {
    GenericDataContainerRequest genericDataContainerRequest = new GenericDataContainerRequest();
    SetGroupCountRequest request = new SetGroupCountRequest();
    request.setCount(newNumberLicences);
    request.setUsernames(usersToDelete);
    genericDataContainerRequest.setData(request);


    // TODO: checkout pre/post handling


    return this.sendRequest("transactional.setGroupCount", genericDataContainerRequest,
      SetGroupCountResponseContainer.class);
  }

  /**
   * Set payment option - and and initiates a checkout, if required.
   */
  public SetPaymentOptionResp transactionalSetPaymentOption(PaymentType option)
    throws Exception {
    SetPaymentOptionReq optionRequest = new SetPaymentOptionReq();
    optionRequest.setPaymentOption(option);
    return this.sendRequest("transactional.setPaymentOption",
      optionRequest,
      SetPaymentOptionResp.class);
  }

  /**
   *
   * @param crmTransactionId crmTransaction id.
   * @param cid cid.
   * @param tid tid.
   * @return transactionResult.
   * @throws Exception crm Exception.
   */
  public GenericCommitResponse transactionalCommit(String crmTransactionId, String cid, String tid)
    throws Exception {
    TransactionalCommitRequest transactionalCommitRequest = new TransactionalCommitRequest();
    transactionalCommitRequest.setCheckout(new TransactionalCommitCheckout(cid, tid));
    transactionalCommitRequest.setTransactionId(crmTransactionId);
    return this.sendRequest("transactional.commitTransaction",
      transactionalCommitRequest,
      GenericCommitResponse.class);
  }

  /**
   *
   * @return obje.
   * @throws Exception crmException.
   */
  public UserDeleteStatusResponse multiuserUserDeleteStatus(UserDeleteStatusRequest request)
    throws Exception {
    return this.sendRequest("multiuser.userDeleteStatus", request, UserDeleteStatusResponse.class);
  }


}
