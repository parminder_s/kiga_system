package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 05.10.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GetInvoiceRequest extends CrmRequest {
  @JsonProperty("limit_from")
  private Integer limitFrom;

  @JsonProperty("limit_n")
  private Integer limitN;
}
