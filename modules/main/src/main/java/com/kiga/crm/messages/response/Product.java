package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.kiga.crm.messages.deserializers.AvailablePaymentOptionsDeserializer;
import com.kiga.payment.PaymentType;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by peter on 04.10.16.
 * Subscription type data - describes one flavour of a subscription, e.g. 'parents, 12-month'.
 */
@AllArgsConstructor
@NoArgsConstructor
public class Product {

  Integer id;
  String productGroupId;
  String customerType;
  String duration;
  Double price;
  Double origPrice;
  String licencePrice;
  Integer maxGroups;
  List<PaymentType> paymentOptions;

  @JsonGetter("origPrice")
  public Double getOrigPrice() {
    return origPrice;
  }

  @JsonSetter("orig_price")
  public void setOrigPrice(Double origPrice) {
    this.origPrice = origPrice;
  }

  @JsonGetter("id")
  public Integer getId() {
    return id;
  }

  @JsonSetter("product_id")
  public void setId(Integer id) {
    this.id = id;
  }

  @JsonGetter("productGroupId")
  public String getProductGroupId() {
    return productGroupId;
  }

  @JsonSetter("product_group_id")
  public void setProductGroupId(String productGroupId) {
    this.productGroupId = productGroupId;
  }

  @JsonSetter("customerType")
  public String getCustomerType() {
    return customerType;
  }

  @JsonSetter("customer_type")
  public void setCustomerType(String customerType) {
    this.customerType = customerType;
  }

  public String getDuration() {
    return duration;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  @JsonGetter("licencePrice")
  public String getLicencePrice() {
    return licencePrice;
  }

  @JsonSetter("licence_price")
  public void setLicencePrice(String licencePrice) {
    this.licencePrice = licencePrice;
  }

  @JsonGetter("max_groups")
  public Integer getMaxGroups() {
    return maxGroups;
  }

  @JsonSetter("max_groups")
  public void setMaxGroups(Integer maxGroups) {
    this.maxGroups = maxGroups;
  }

  @JsonGetter("payment_options")
  public List<PaymentType> getPaymentOptions() {
    return paymentOptions;
  }

  @JsonSetter("payment_options")
  @JsonDeserialize(using = AvailablePaymentOptionsDeserializer.class)
  public void setPaymentOptions(List<PaymentType> paymentOptions) {
    this.paymentOptions = paymentOptions;
  }

  public String toString() {
    return this.getClass() + ":" + this.getCustomerType() + "-" + this.getDuration();
  }
}
