package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.kiga.crm.messages.Address;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.memberadmin.domain.InvoiceOption;

import java.util.Date;
import java.util.List;

/**
 * Created by peter on 27.09.16.
 */
@JsonIgnoreProperties({"payment_option"})
public class ResponseGetCustomerData {
  Address shippingAddress;
  Address billingAddress;

  @JsonProperty("subscription")
  Subscription subscription;

  PaymentMethod paymentOption;
  InvoiceOption invoiceOption;
  String job;
  String orgType;

  String jobName;

  Date dateOfBirth;
  SubscriptionType subscriptionType;
  SubscriptionTransition subscriptionTransition;
  SubscriptionCancellation subscriptionCancellation;
  Status status;
  List<String> countryContext;

  @JsonGetter("shippingAddress")
  public Address getShippingAddress() {
    return shippingAddress;
  }

  @JsonSetter("shippingaddress")
  public void setShippingAddress(Address shippingAddress) {
    this.shippingAddress = shippingAddress;
  }

  @JsonGetter("billingAddress")
  public Address getBillingAddress() {
    return billingAddress;
  }

  @JsonSetter("billingaddress")
  public void setBillingAddress(Address billingAddress) {
    this.billingAddress = billingAddress;
  }

  //paymentOption and payment_option_code since paymentOption is created
  //via payment_option_code
  @JsonGetter("paymentOption")
  public PaymentMethod getPaymentOption() {
    return paymentOption;
  }

  @JsonSetter("payment_option_code")
  public void setPaymentOption(PaymentMethod paymentOption) {
    this.paymentOption = paymentOption;
  }

  @JsonGetter("invoiceOption")
  public InvoiceOption getInvoiceOption() {
    return invoiceOption;
  }

  @JsonSetter("invoice_option")
  public void setInvoiceOption(InvoiceOption invoiceOption) {
    this.invoiceOption = invoiceOption;
  }

  public String getJob() {
    return job;
  }

  public void setJob(String job) {
    this.job = job;
  }

  public String getOrgType() {
    return orgType;
  }

  public void setOrgType(String orgType) {
    this.orgType = orgType;
  }

  public String getJobName() {
    return jobName;
  }

  public void setJobName(String jobName) {
    this.jobName = jobName;
  }

  @JsonGetter("dateOfBirth")
  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  @JsonSetter("date_of_birth")
  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  @JsonGetter("subscriptionType")
  public SubscriptionType getSubscriptionType() {
    return subscriptionType;
  }

  @JsonSetter("subscr_type")
  public void setSubscriptionType(SubscriptionType subscriptionType) {
    this.subscriptionType = subscriptionType;
  }

  @JsonGetter("subscriptionTransition")
  public SubscriptionTransition getSubscriptionTransition() {
    return subscriptionTransition;
  }

  @JsonSetter("subscr_transition")
  public void setSubscriptionTransition(SubscriptionTransition subscriptionTransition) {
    this.subscriptionTransition = subscriptionTransition;
  }

  @JsonGetter("subscriptionCancellation")
  public SubscriptionCancellation getSubscriptionCancellation() {
    return subscriptionCancellation;
  }

  @JsonSetter("subscr_cancellation")
  public void setSubscriptionCancellation(SubscriptionCancellation subscriptionCancellation) {
    this.subscriptionCancellation = subscriptionCancellation;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  @JsonGetter("countryContext")
  public List<String> getCountryContext() {
    return countryContext;
  }

  @JsonSetter("country_context")
  public void setCountryContext(List<String> countryContext) {
    this.countryContext = countryContext;
  }

  public Subscription getSubscription() {
    return subscription;
  }

  public void setSubscription(Subscription subscription) {
    this.subscription = subscription;
  }
}
