package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by peter on 14.12.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class SetGroupCountResponseContainer
  extends GenericTransactionalResponse {

  @JsonProperty("transaction_result")
  SetGroupCountResponse data;

}
