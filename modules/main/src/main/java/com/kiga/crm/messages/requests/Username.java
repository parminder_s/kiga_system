package com.kiga.crm.messages.requests;

import lombok.Data;

/**
 * Created by peter on 25.10.16.
 */

@Data
public class Username {
  private String username;
}
