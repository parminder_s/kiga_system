package com.kiga.crm;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by peter on 10/20/16.
 */
@Configuration
@ConfigurationProperties("crm")
public class CrmProperties {
  Integer defaultCrm;
  Integer testCrmApi;
  String url;

  public Integer getDefaultCrm() {
    return defaultCrm;
  }

  public void setDefaultCrm(Integer defaultCrm) {
    this.defaultCrm = defaultCrm;
  }

  public Integer getTestCrmApi() {
    return testCrmApi;
  }

  public void setTestCrmApi(Integer testCrmApi) {
    this.testCrmApi = testCrmApi;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
