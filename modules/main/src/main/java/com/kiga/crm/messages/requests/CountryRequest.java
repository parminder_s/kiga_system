package com.kiga.crm.messages.requests;

import lombok.Data;

/**
 * Created by peter on 04.10.16.
 */

@Data
public class CountryRequest {
  private String country;
}
