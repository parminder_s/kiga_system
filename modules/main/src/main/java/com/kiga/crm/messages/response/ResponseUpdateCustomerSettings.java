package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by peter on 05.10.16.
 */

@Data
public class ResponseUpdateCustomerSettings {
  @JsonProperty("transaction_id")
  String transactionId;

  Checkout checkout;

  @JsonProperty("transaction_result")
  TransactionResult transactionResult;
  @JsonProperty("transaction_status")
  String transactionStatus;
  @JsonProperty("needs_checkout")
  Integer needsCheckout;
}
