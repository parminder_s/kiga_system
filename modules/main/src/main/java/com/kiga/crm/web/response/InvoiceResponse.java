package com.kiga.crm.web.response;

import com.kiga.crm.messages.response.Invoice;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.memberadmin.domain.InvoiceOption;
import com.kiga.memberadmin.web.response.InvoiceDetailResponse;
import com.kiga.payment.PaymentType;
import lombok.Data;

import java.util.List;

/**
 * Created by peter on 04.10.16.
 */
@Data
public class InvoiceResponse {
  List<InvoiceDetailResponse> invoiceList;
  List<PaymentType> paymentMethodList;
  PaymentMethod paymentMethod;
  List<InvoiceOption> invoiceOptionList;
  InvoiceOption invoiceOption;
}
