package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by peter on 27.09.16.
 */

@Data
public class ResponseGetProfile {
  @JsonProperty("nickname")
  String nickname;

  @JsonProperty("avatar")
  String avatar;
}
