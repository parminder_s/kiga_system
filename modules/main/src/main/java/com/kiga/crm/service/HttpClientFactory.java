package com.kiga.crm.service;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;

@Service
public class HttpClientFactory {
  public CloseableHttpClient getDefaultClient() {
    return HttpClients.createDefault();
  }
}
