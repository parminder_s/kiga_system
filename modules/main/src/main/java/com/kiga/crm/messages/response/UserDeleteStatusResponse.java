package com.kiga.crm.messages.response;

import lombok.Data;

import java.util.List;

/**
 * Created by peter on 22.03.17.
 */
@Data
public class UserDeleteStatusResponse {
  private Integer status;
  List<List<String>> users;
}
