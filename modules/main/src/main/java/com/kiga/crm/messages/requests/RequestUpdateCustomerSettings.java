package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kiga.crm.messages.Address;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 25.10.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RequestUpdateCustomerSettings extends CrmRequest {
  @JsonProperty("payment_option")
  private String paymentOption;
  @JsonProperty("invoice_option")
  private String invoiceOption;
  @JsonProperty("shippingaddress")
  private Address shippingAddress;
  @JsonProperty("billingaddress")
  private Address billingAddress;
  @JsonProperty("payment_data")
  private String paymentData;
}


