package com.kiga.crm.messages;

import lombok.Data;

/**
 * Created by peter on 12.10.16.
 */

@Data
public class DataContainer {
  Object data;
}
