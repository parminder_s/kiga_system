package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by peter on 04.10.16.
 */

@Data
public class LicenceType {
  String country;

  @JsonProperty("licence_type")
  Integer licenceType;

  @JsonProperty("product_id")
  Integer productId;
  Double price;
  Integer duration;

  @JsonProperty("users_min")
  Integer usersMin;

  @JsonProperty("users_max")
  Integer usersMax;

  public LicenceType() {
  }

  /**
   *
   * @param country country.
   * @param licenceType licenceType.
   * @param productId productId.
   * @param price price.
   * @param duration duration.
   * @param usersMin usersMin.
   * @param usersMax userMax.
   */
  public LicenceType(String country, Integer licenceType, Integer productId, Double price,
                     Integer duration, Integer usersMin, Integer usersMax) {
    this.country = country;
    this.licenceType = licenceType;
    this.productId = productId;
    this.price = price;
    this.duration = duration;
    this.usersMin = usersMin;
    this.usersMax = usersMax;
  }
}
