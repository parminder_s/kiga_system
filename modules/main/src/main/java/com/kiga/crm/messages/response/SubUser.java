package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Created by peter on 02.12.16.
 */
public class SubUser {
  Integer userId;
  String login;
  String email;
  Boolean canceled;

  public SubUser() {
  }

  /**
   *
   * @param userId userId of the SubUser.
   * @param email email of the SubUser.
   * @param canceled if the SubUser is canceled in the future.
   */
  public SubUser(Integer userId, String email, Boolean canceled) {
    this.userId = userId;
    this.login = email;
    this.email = email;
    this.canceled = canceled;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Boolean getCanceled() {
    return canceled;
  }

  public void setCanceled(Boolean canceled) {
    this.canceled = canceled;
  }

  @JsonGetter("userId")
  public Integer getUserId() {

    return userId;
  }

  @JsonSetter("user_id")
  public void setUserId(Integer userId) {
    this.userId = userId;
  }
}
