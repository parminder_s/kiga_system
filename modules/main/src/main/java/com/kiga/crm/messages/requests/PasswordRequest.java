package com.kiga.crm.messages.requests;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 25.10.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PasswordRequest extends CrmRequest {
  private String password;
}
