package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by peter on 13.02.17.
 */
@Data
public class TransactionalCommitRequest {
  @JsonProperty("transaction_id")
  String transactionId;
  TransactionalCommitCheckout checkout;
}
