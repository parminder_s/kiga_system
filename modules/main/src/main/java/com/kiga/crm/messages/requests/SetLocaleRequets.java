package com.kiga.crm.messages.requests;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 12.10.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SetLocaleRequets extends CrmRequest {
  private String locale;
}
