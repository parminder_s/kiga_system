package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.kiga.crm.messages.deserializers.AvailableSubscriptionsDeserializer;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * For the member, contains info regarding possible changes in subscription
 * (transition/product-change) and modalities of such a change.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseGetAvailableSubscriptions {

  @JsonProperty("remain_duration")
  Integer remainingDuration;

  @JsonProperty
  Double discount;

  @JsonProperty("invoice_willcancel")
  Integer invoiceWillCancel;

  @JsonProperty("invoice_ispaid")
  Integer invoiceIsPaid;

  @JsonProperty("start_now")
  Date startDateForTransitionNow;

  @JsonProperty("start_end")
  Date startDateForTransitionEnd;

  @JsonProperty("products")
  @JsonDeserialize(using = AvailableSubscriptionsDeserializer.class)
  List<AvailableSubscription> products;
}

