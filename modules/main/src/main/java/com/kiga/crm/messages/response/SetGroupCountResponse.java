package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Response as a result when setting the group (=subuser)-count of a subscription
 * that allows multiple sub-users ("OG" aka group aka "small"-license product).
 * Created by mfit on 02.12.16.
 */
@Data
public class SetGroupCountResponse {
  int status;

  /**
   * Groups at the moment.
   */
  @JsonProperty("n_groups")
  int groupCountCurrent;

  /**
   * Group count as it will be after this period.
   * (Subtractions take effect only next period).
   */
  @JsonProperty("n_groups_nextperiod")
  int groupCountNextPeriod;
}
