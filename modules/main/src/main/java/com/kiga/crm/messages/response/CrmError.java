package com.kiga.crm.messages.response;

import lombok.Data;

/**
 * Created by peter on 04.10.16.
 * Class for errors caused by communication with CRM.
 * Code 901:
 */

@Data
public class CrmError {
  String error;
  int code;
}
