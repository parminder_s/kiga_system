package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kiga.payment.PaymentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SetPaymentOptionReq extends CrmRequest {

  @JsonProperty(value = "payment_option")
  PaymentType paymentOption;

  @JsonGetter("payment_option")
  public String getSerializedPaymentOption() {
    return String.valueOf(paymentOption).replace("_", "-");
  }

  /**
   * Optional payment-checkout data (applies only for checkouts for paypal).
   */
  @JsonProperty("payment_data")
  private PaymentData paymentData;
}
