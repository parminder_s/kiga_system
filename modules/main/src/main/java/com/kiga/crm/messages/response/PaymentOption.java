package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by peter on 04.10.16.
 * A method of payment (e.g. credit card).
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentOption {
  @JsonProperty("payment_id")
  Integer paymentId;

  @JsonProperty("payment_code")
  String paymentCode;

  @JsonProperty("payment_title")
  String paymentTitle;

  @JsonProperty("uses_checkout")
  Integer usesCheckout;

  @JsonProperty("payment_psp")
  String paymentPsp;

  @JsonProperty("payment_acquirer")
  String paymentAcquirer;

}
