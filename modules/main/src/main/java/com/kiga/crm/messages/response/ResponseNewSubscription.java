package com.kiga.crm.messages.response;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by peter on 12.10.16.
 */

@Data
public class ResponseNewSubscription {
  @JsonProperty("transaction_id")
  String transactionId;

  @JsonProperty("checkout")
  Checkout checkout;

  @JsonProperty("needs_checkout")
  Integer needsCheckout;

  @JsonProperty("transaction_status")
  String transactionStatus;
}
