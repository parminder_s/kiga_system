package com.kiga.crm.messages.response;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by peter on 05.10.16.
 */

@Data
public class Cart {
  String description;
  String currencyCode;
  Double currentAmount;
  Double currentTax;
  String currentDescription;
  String recurringStartDate;
  String recurringCycle;
  Integer recurringFrequency;
  Double recurringAmount;
  Double recurringTax;
  Integer recurringRepeats;
  String recurringDescription;
  Integer hasTrialPeriod;
  String trialCycle;
  Integer trialFrequency;
  Integer trialAmount;
  Integer trialTax;
  Integer trialRepeats;
  BigDecimal checkoutAmount;
  BigDecimal checkoutTax;
  BigDecimal checkoutAmountGross;
  Boolean newProduct;
}
