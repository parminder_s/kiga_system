package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Common subset of fields all transactional services calls use.
 * These fields are added once a command call is wrapped in a "transaction",
 * because a checkout can occur. The contents of the specific response
 * is transmitted in the "transaction_result" field, which is defined
 * differently for each case and is of course found only in the subclasses
 * that extend this class.
 */
@Data
public class GenericTransactionalResponse {

  /**
   * Handle for the transaction, e.g. used for committing it.
   */
  @JsonProperty("transaction_id")
  String transactionId;

  /**
   * Status of the transaction, "start" or "done".
   * Start mostly indicates a checkout, done indicates transaction
   * is already committed and no further action is required.
   */
  @JsonProperty("transaction_status")
  String transactionStatus;

  /**
   * 1 or 0 (boolean-as-int).
   */
  @JsonProperty("needs_checkout")
  Integer needsCheckout;

  /**
   * Contains checkout-cart and specifics about the checkout process.
   */
  Checkout checkout;
}
