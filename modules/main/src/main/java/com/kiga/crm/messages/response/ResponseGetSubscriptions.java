package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.kiga.crm.messages.deserializers.PaymentOptionsDeserializer;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by peter on 04.10.16.
 * Basically all static data per country in one request.
 * Holds lists of subscriptions
 * (with payment options per subscription type), invoice options,
 * license types, payment options.
 * Also holds currency, vat and age limit per country.
 */
@Data
public class ResponseGetSubscriptions {
  @JsonProperty("product_groups")
  List<ProductGroup> productGroups;

  List<Product> products;

  @JsonProperty("licence_types")
  List<LicenceType> licenceTypes;

  @JsonProperty("payment_options")
  @JsonDeserialize(using = PaymentOptionsDeserializer.class)
  List<PaymentOption> paymentOptions;

  @JsonProperty("invoice_options")
  InvoiceOptionsResponse invoiceOptions;
  String currency;
  Double vat;

  @JsonProperty("vat_subtractable")
  Double vatSubtractable;

  @JsonProperty("age_limit")
  Integer ageLimit;
}
