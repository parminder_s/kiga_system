package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * Created by peter on 04.10.16.
 */

@Data
public class Status {
  @JsonProperty("invoice_paid")
  Integer paid;

  @JsonProperty("invoice_status")
  String status;

  @JsonProperty("invoice_open")
  Double open;

  @JsonProperty("invoice_date")
  Date date;

  @JsonProperty("invoice_currency")
  String currency;

  @JsonProperty("invoice_nr")
  String number;

  @JsonProperty("product_balance")
  Double productBalance;
}
