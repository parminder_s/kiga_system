package com.kiga.crm.messages.requests;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ChangeSubscriptionContainerRequest extends CrmRequest {
  private RequestChangeSubscription data;

  public ChangeSubscriptionContainerRequest() {
    this.data = new RequestChangeSubscription();
  }
}
