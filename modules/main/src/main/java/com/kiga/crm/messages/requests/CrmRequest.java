package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by peter on 27.09.16.
 */

@Data
public class CrmRequest {
  @JsonProperty("user_id")
  private int userId;

  @JsonProperty("session_token")
  private String sessionToken;
}
