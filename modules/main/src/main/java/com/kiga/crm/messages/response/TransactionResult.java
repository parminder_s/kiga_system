package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * Created by peter on 25.10.16.
 */
@Data
public class TransactionResult {
  @JsonProperty("user_id")
  Integer userId;

  String login;
  String country;
  Integer status;
  @JsonProperty("product_status")
  String productStatus;
  @JsonProperty("product_id")
  Integer productId;

  Integer testblock;
  @JsonProperty("access_content_archive")
  Integer accessContentArchive;
  @JsonProperty("access_content")
  Integer accessContent;
  @JsonProperty("access_forum_write")
  Integer accessForumWrite;
  @JsonProperty("is_master")
  Integer isMaster;
  @JsonProperty("restrict_ks")
  Integer restrictKs;
  @JsonProperty("is_gift")
  Integer isGift;
  @JsonProperty("is_forum")
  Integer isForum;
  @JsonProperty("force_ks")
  Integer forceKs;
  @JsonProperty("has_issue")
  Integer hasIssue;
  @JsonProperty("user_lang")
  String userLang;
  @JsonProperty("add_data")
  LoginAddData addData;
  @JsonProperty("customer_nr")
  String customerNr;
  @JsonProperty("auth_level")
  Integer authLevel;
  @JsonProperty("needs_checkout")
  Integer needsCheckout;
  @JsonProperty("date_change")
  Date dateChange;
  @JsonProperty("required_activation")
  Integer requiredActivation;
  @JsonProperty("transaction_id")
  String transactionId;
  @JsonProperty("transaction_status")
  String transactionStatus;
}
