package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kiga.crm.messages.AboParameter;
import com.kiga.crm.messages.Address;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by rainerh on 24.05.16.
 */

@Data
public class RequestNewSubscription {
  private String password;

  @JsonProperty("billingaddress")
  private Address billingAddress;

  @JsonProperty("shippingaddress")
  private Address shippingAddress;

  private AboParameter subscription;

  private CountryContext countryContext;

  @JsonProperty("payment_option")
  private String paymentOption;

  @JsonProperty("invoice_option")
  private String invoiceOption;

  private String job = "";
  private String orgType = "";

  @JsonProperty("atu")
  private String vatNumber = "";

  @JsonProperty("user_lang")
  private String userLang;

  private int newsletter;
  private int create;

  @JsonProperty("date_of_birth")
  private String dateOfBirth;

}
