package com.kiga.crm.messages.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.kiga.payment.PaymentType;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by mfit on 11.10.17.
 * Deserializes a map (stemming from a PHP associative array on the other side) to a
 * simple flat list.
 */
public class AvailablePaymentOptionsDeserializer extends JsonDeserializer<List<PaymentType>> {

  TypeReference<Map<String, String>> typeRef =
    new TypeReference<Map<String, String>>() { };

  @Override
  public List<PaymentType> deserialize(JsonParser parser, DeserializationContext ctxt)
    throws IOException {
    return asListWithoutUnknown(parser.readValueAs(typeRef));
  }

  private List<PaymentType> asListWithoutUnknown(Map<String, String> paymentCodeMap) {
    return paymentCodeMap.values().stream()
      .map((stringVal) -> {
        PaymentType paymentType;
        try {
          paymentType = PaymentType.valueOf(stringVal.replace("-", "_"));
        } catch (IllegalArgumentException exc) {
          paymentType = null;
        }
        return paymentType;
      }).filter(option -> option != null).collect(Collectors.toList());
  }
}
