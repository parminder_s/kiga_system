package com.kiga.crm.web.request;

import com.kiga.crm.messages.Address;
import lombok.Data;

/**
 * Created by peter on 05.10.16.
 */
@Data
public class AddressSaveRequest {
  private Address shippingAddress;
  private Address billingAddress;
}
