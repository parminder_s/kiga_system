package com.kiga.crm.messages.response;

import lombok.Data;

/**
 * Created by peter on 13.02.17.
 */
@Data
public class TransactionalCommitCheckout {
  String cid;
  String tid;

  public TransactionalCommitCheckout(String cid, String tid) {
    this.cid = cid;
    this.tid = tid;
  }
}
