package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Describes a subscription option with regards to a product transition (change).
 * Next to normal subscription options stores what is the discounted price,
 * when can transition take place etc.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AvailableSubscription {
  @JsonProperty("product_id")
  Integer productId;

  @JsonProperty("product_group_id")
  String productGroupId;

  @JsonProperty("customer_type")
  String customerType;

  @JsonProperty("duration")
  Integer duration;

  @JsonProperty("price")
  Float price;

  @JsonProperty("licence_price")
  Float licencePrice;

  @JsonProperty("now")
  Integer canChooseNow;

  @JsonProperty("pre_order")
  Integer canPreOrder;

  @JsonProperty("discounted_price")
  Float discountedPrice;

}
