package com.kiga.crm;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.crm.service.CrmConverter;
import com.kiga.crm.service.HttpClientFactory;
import com.kiga.crm.service.ObjectMapperFactory;
import com.kiga.web.security.session.Session;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CrmConfig {

  @Bean
  CrmConverter crmConverter(CrmProperties crmProperties, Session session) {
    ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper();
    HttpClientFactory httpClientFactory = new HttpClientFactory();
    return new CrmConverter(crmProperties.getUrl(), session, objectMapper, httpClientFactory);
  }
}
