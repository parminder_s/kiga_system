package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by peter on 20.12.16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChangeSubscriptionResp extends GenericTransactionalResponse {

  @JsonProperty("transaction_result")
  TransactionResult transaction;
}
