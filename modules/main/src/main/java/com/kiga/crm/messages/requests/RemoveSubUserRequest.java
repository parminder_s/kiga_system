package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 02.12.16.
 */


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RemoveSubUserRequest extends CrmRequest {
  @JsonProperty("subuser_id")
  private Integer subuserId;
}
