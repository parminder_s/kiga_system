package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Data;

/**
 * Created by peter on 04.10.16.
 */

@Data
public class SubscriptionType {
  @JsonProperty("subscr_id")
  Integer subscriptionId;

  @JsonProperty("subscr_mode")
  String subscriptionmode;
  String country;

  @JsonProperty("mode_type")
  Integer modeType;

  @JsonProperty("for_org")
  Integer forOrganisation;

  @JsonProperty("for_student")
  Integer forStudent;

  @JsonProperty("with_archive")
  Integer withArchive;

  @JsonProperty("uses_groups")
  Integer usesGroups;

  Integer period;
  Double cost;

  @JsonProperty("cost_archive")
  Double costArchive;

  String title;
  String description;

  @JsonProperty("name")
  String subscriptionTypeTranslateKey;

  @JsonProperty("licence_type")
  Integer licenceType;

  @JsonProperty("cost_licence")
  Integer costLicence;

  @JsonProperty("cost_per_group")
  Double costperGroup;

  @JsonProperty("period_type")
  String periodType;
}
