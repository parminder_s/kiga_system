package com.kiga.crm.messages.response;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by peter on 04.10.16.
 */
@AllArgsConstructor
@Builder
public class Invoice {
  String status;
  Date date;
  Double amount;
  String currency;
  Integer ok;
  Integer charges;
  String invoiceNr;
  Double amountOpen;
  Date dateDue;
  Integer dunningLevel;

  public Invoice() {
  }

  @JsonProperty("document")
  String url;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @JsonGetter("invoiceNr")
  public String getInvoiceNr() {
    return invoiceNr;
  }

  @JsonSetter("nr")
  public void setInvoiceNr(String invoiceNr) {
    this.invoiceNr = invoiceNr;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  @JsonGetter("date")
  public Date getDate() {
    return date;
  }

  @JsonSetter("date")
  public void setDate(String date) throws ParseException {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    this.date = simpleDateFormat.parse(date);
  }

  public void setDate(Date date) {
    this.date = date;
  }


  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Integer getOk() {
    return ok;
  }

  public void setOk(Integer ok) {
    this.ok = ok;
  }

  @JsonGetter("amountOpen")
  public Double getAmountOpen() {
    return amountOpen;
  }

  @JsonSetter("amount_open")
  public void setAmountOpen(Double amountOpen) {
    this.amountOpen = amountOpen;
  }

  @JsonProperty("dateDue")
  public Date getDateDue() {
    return dateDue;
  }

  @JsonProperty("date_due")
  public void setDateDue(Date dateDue) {
    this.dateDue = dateDue;
  }


  @JsonGetter("dunningLevel")
  public Integer getDunningLevel() {
    return dunningLevel;
  }


  @JsonSetter("dunninglevel")
  public void setDunningLevel(Integer dunningLevel) {
    this.dunningLevel = dunningLevel;
  }

  public Integer getCharges() {
    return charges;
  }

  public void setCharges(Integer charges) {
    this.charges = charges;
  }
}
