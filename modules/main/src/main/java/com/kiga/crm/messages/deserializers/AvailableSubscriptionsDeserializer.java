package com.kiga.crm.messages.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.kiga.crm.messages.response.AvailableSubscription;
import com.kiga.crm.messages.response.PaymentOption;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mfit on 11.10.17.
 * Due to a not-ideal data structure of the CRM-response,
 * deserialize a map with discardable keys to a list.
 */
public class AvailableSubscriptionsDeserializer
  extends JsonDeserializer<List<AvailableSubscription>> {
  TypeReference<Map<String, AvailableSubscription>> typeRef =
    new TypeReference<Map<String, AvailableSubscription>>() { };

  @Override
  public List<AvailableSubscription> deserialize(JsonParser parser,
                                                 DeserializationContext ctxt)
    throws IOException {
    Map<String, AvailableSubscription> map = parser.readValueAs(typeRef);
    return new ArrayList<AvailableSubscription>(map.values());
  }
}
