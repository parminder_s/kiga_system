package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by peter on 22.03.17.
 */

/*
session_id / user_id wie gewohnt vom Abo (authentifizierung)
subuser_id ODER  email (vom subuser, geht beides)
delete ... 0 / 1 ist ein flag zum hinzufügen oder wegnehmen des DELETE-status
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UserDeleteStatusRequest extends CrmRequest {
  @JsonProperty("subuser_id")
  private Integer subuserId;
  private String email;

  @JsonIgnore
  private Boolean delete;

  @JsonGetter("delete")
  private Integer jsonValue() {
    return (delete) ? 1 : 0;
  }
}
