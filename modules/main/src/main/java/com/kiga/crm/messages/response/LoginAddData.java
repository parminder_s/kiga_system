package com.kiga.crm.messages.response;

import lombok.Data;

/**
 * Created by peter on 25.10.16.
 */

@Data
public class LoginAddData {
  String firstname;
  String lastname;
  String sex;
}
