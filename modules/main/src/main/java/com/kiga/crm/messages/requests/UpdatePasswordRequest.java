package com.kiga.crm.messages.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 25.10.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UpdatePasswordRequest extends CrmRequest {
  @JsonProperty("password_new")
  private String newPassword;
}
