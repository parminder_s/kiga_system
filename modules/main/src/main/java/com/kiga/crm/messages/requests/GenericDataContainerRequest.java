package com.kiga.crm.messages.requests;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Map;

/**
 * Created by mfit on 02.12.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GenericDataContainerRequest<T> extends CrmRequest {
  private T data;

  @JsonProperty("payment_data")
  private Map<String, String> paymentData;
}
