package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaEmployee;
import com.kiga.security.domain.Member;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface KgaEmployeeRepository extends CrudRepository<KgaEmployee, Long> {
  List<KgaEmployee> findByEmail(String email);

  List<KgaEmployee> findByKindergarten_Id(Long id);

  KgaEmployee findByMember(Member member);
}
