package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaChild_Versions;
import org.springframework.data.repository.CrudRepository;

public interface KgaChildArchiveRepository extends CrudRepository<KgaChild_Versions, Long> {}
