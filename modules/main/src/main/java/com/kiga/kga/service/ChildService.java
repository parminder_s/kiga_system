package com.kiga.kga.service;

import com.kiga.db.KigaEntityModel;
import com.kiga.db.RepositoryFetcher;
import com.kiga.db.RepositoryPersister;
import com.kiga.kga.domain.*;
import com.kiga.kga.repository.*;
import com.kiga.spec.Fetcher;
import com.kiga.spec.Persister;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Builder
@AllArgsConstructor
public class ChildService {
  private final KgaChildRepository kgaChildRepository;
  private final KgaChildArchiveRepository kgaChildArchiveRepository;
  private final KgaParentRepository parentRepository;
  private final KgaParentToChildRepository kgaParentToChildRepository;
  private final ChildInKgaGroupRepository childInKgaGroupRepository;
  private final Persister<KgaChild> childPersister;
  private final Persister<KgaParentToChild> kgaParentToChildPersister;

  private final Fetcher<KgaYear> yearFetcher;

  private final VaccinationRepository vaccinationRepository;
  private final DiseasesRepository diseasesRepository;
  private final AllergyRepository allergyRepository;

  /** Constructor. */
  @Autowired
  public ChildService(
      KgaChildRepository kgaChildRepository,
      KgaChildArchiveRepository kgaChildArchiveRepository,
      KgaParentRepository parentRepository,
      KgaParentToChildRepository kgaParentToChildRepository,
      ChildInKgaGroupRepository childInKgaGroupRepository,
      KgaYearRepository kgaYearRepository,
      VaccinationRepository vaccinationRepository,
      DiseasesRepository diseasesRepository,
      AllergyRepository allergyRepository) {
    this.kgaChildRepository = kgaChildRepository;
    this.kgaChildArchiveRepository = kgaChildArchiveRepository;
    this.parentRepository = parentRepository;
    this.kgaParentToChildRepository = kgaParentToChildRepository;
    this.childInKgaGroupRepository = childInKgaGroupRepository;
    this.childPersister = kgaChildRepository::save;
    this.kgaParentToChildPersister = new RepositoryPersister<>(kgaParentToChildRepository);
    this.yearFetcher = new RepositoryFetcher<>(kgaYearRepository);
    this.vaccinationRepository = vaccinationRepository;
    this.diseasesRepository = diseasesRepository;
    this.allergyRepository = allergyRepository;
  }

  /** saves a kgachild data, as member of the current group. */
  @Transactional
  public KgaChild save(KgaChild toSave, KgaKindergarten kindergarten) {
    toSave.setKindergarten(kindergarten);
    return childPersister.persist(toSave);
  }

  public KgaChild findOne(long id) {
    return kgaChildRepository.findOne(id);
  }

  /** returns a list of all childs that are in a group in a given year. */
  public List<KgaChild> findByGroup(Integer groupId) {
    return childInKgaGroupRepository
        .findWithChildByGroup_Id(groupId.longValue())
        .stream()
        .map(ChildInKgaGroup::getChild)
        .collect(Collectors.toList());
  }

  /** returns a list of all childs that are in a group in a given year. */
  public List<KgaChild> findByKindergarten(long kindergartenId) {
    return new ArrayList<>(kgaChildRepository.findByKindergarten_Id(kindergartenId));
  }

  /** saves the role of a parent which is already connected to a child. */
  public void saveParentRole(KgaParentToChild parentViewModel) {
    KgaParentToChild kgaParentToChild = kgaParentToChildRepository.findOne(parentViewModel.getId());
    kgaParentToChild.setRole(parentViewModel.getRole());
    kgaParentToChildPersister.persist(kgaParentToChild);
  }

  /** adds a parent to a child. */
  public void addParentToChild(Long childId, Long parentId, ParentRole role) {
    KgaParentToChild kgaParentToChild =
        kgaParentToChildRepository.findByParent_IdAndChild_Id(parentId, childId);
    if (kgaParentToChild == null) {
      kgaParentToChild = new KgaParentToChild();
    }
    kgaParentToChild.setRole(role);
    kgaParentToChild.setChild(kgaChildRepository.findOne(childId));
    kgaParentToChild.setParent(parentRepository.findOne(parentId));
    kgaParentToChildPersister.persist(kgaParentToChild);
  }

  public void removeParentFromChild(Long kgaParentToChildId) {
    kgaParentToChildRepository.delete(kgaParentToChildId);
  }

  /** returns a child with its parents already loaded. */
  public KgaChild findWithParentsById(long id) {
    return kgaChildRepository.findWithParentsById(id);
  }

  public List<KgaChildData> findByYear(long yearId) {
    KgaYear year = yearFetcher.find(yearId);
    List<KgaChild> children = this.findByKindergarten(year.getKindergarten().getId());
    if (year.isArchived()) {
      List<KgaChild_Versions> data =
          children
              .stream()
              .map(
                  child ->
                      child
                          .getArchives()
                          .stream()
                          .filter(
                              x ->
                                  x.getLastEdited()
                                      .toInstant()
                                      .isBefore(year.getArchivationDate().toInstant()))
                          .max(
                              Comparator.comparing(KigaEntityModel::getLastEdited, Date::compareTo))
                          .get()) // save since every child has at least on versions entry.
              .filter(child -> !child.isDeleted())
              .collect(Collectors.toList());
      return data.stream()
          .peek(
              child ->
                  child.setChildInKgaGroups(
                      child
                          .getChildInKgaGroups()
                          .stream()
                          .filter(x -> x.getGroup().getYear().getId().equals(yearId))
                          .collect(Collectors.toList())))
          .collect(Collectors.toList());
    } else {
      return children
          .stream()
          .peek(
              child ->
                  child.setChildInKgaGroups(
                      child
                          .getChildInKgaGroups()
                          .stream()
                          .filter(x -> x.getGroup().getYear().getId().equals(yearId))
                          .collect(Collectors.toList())))
          .filter(child -> !child.isDeleted())
          .collect(Collectors.toList());
    }
  }

  public void deleteOne(Integer id) {
    KgaChild child = this.findOne(id);
    child.setDeleted(true);
    this.save(child, child.getKindergarten());
  }
}
