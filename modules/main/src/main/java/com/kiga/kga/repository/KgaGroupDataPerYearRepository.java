package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaGroup;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface KgaGroupDataPerYearRepository extends CrudRepository<KgaGroup, Long> {
  List<KgaGroup> findByYear_Id(long id);

  List<KgaGroup> findByIdIn(List<Long> ids);
}
