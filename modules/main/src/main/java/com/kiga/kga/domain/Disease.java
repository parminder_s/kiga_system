package com.kiga.kga.domain;

import javax.persistence.Entity;
import lombok.*;

@Entity
@Data
@Builder
public class Disease extends DiseaseData<KgaChild> {
  public Disease() {
    this.setClassName("Disease");
  }

  public Disease(DiseaseEnum diseaseEnum) {
    this();
    this.setDisease(diseaseEnum);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Disease) {
      return ((Disease) o).getDisease().equals(this.getDisease());
    }
    return false;
  }

  @Override
  public int hashCode() {
    return super.getDisease().hashCode();
  }
}
