package com.kiga.kga.domain.converter;

import com.kiga.kga.domain.AllergyEnum;
import javax.persistence.AttributeConverter;

public class AllergyConverter implements AttributeConverter<AllergyEnum, String> {

  @Override
  public String convertToDatabaseColumn(AllergyEnum attribute) {
    switch (attribute) {
      case PEANUT:
        return "peanut";
      case EGG:
        return "egg";
      case FISH:
        return "fish";
      case GLUTEN:
        return "gluten";
      case SHELLFISH:
        return "shellfish";
      case LUPINEN:
        return "lupinen";
      case MILK:
        return "milk";
      case NUTS:
        return "nuts";
      case BEE:
        return "bee";
      case POLLEN:
        return "pollen";
      case GRASS:
        return "grass";
      case LATEX:
        return "latex";
      case OTHER:
        return "other";
    }

    throw new IllegalArgumentException("Not mapped: " + attribute);
  }

  @Override
  public AllergyEnum convertToEntityAttribute(String dbData) {
    switch (dbData) {
      case "peanut":
        return AllergyEnum.PEANUT;
      case "egg":
        return AllergyEnum.EGG;
      case "fish":
        return AllergyEnum.FISH;
      case "gluten":
        return AllergyEnum.GLUTEN;
      case "shellfish":
        return AllergyEnum.SHELLFISH;
      case "lupinen":
        return AllergyEnum.LUPINEN;
      case "milk":
        return AllergyEnum.MILK;
      case "nuts":
        return AllergyEnum.NUTS;
      case "bee":
        return AllergyEnum.BEE;
      case "pollen":
        return AllergyEnum.POLLEN;
      case "grass":
        return AllergyEnum.GRASS;
      case "latex":
        return AllergyEnum.LATEX;
      case "other":
        return AllergyEnum.OTHER;
    }

    throw new IllegalArgumentException("Not mapped: " + dbData);
  }
}
