package com.kiga.kga.domain.converter;

import com.kiga.kga.domain.ParentRole;

import javax.persistence.AttributeConverter;

public class ParentRoleConverter implements AttributeConverter<ParentRole, String> {
  @Override
  public String convertToDatabaseColumn(ParentRole attribute) {
    switch (attribute) {
      case MOTHER:
        return "mother";
      case FATHER:
        return "father";
      case GRAND_FATHER:
        return "grand_father";
      case GRAND_MOTHER:
        return "grand_mother";
      case SIGNIFICANT_OTHER:
        return "significant_other";
      case OTHER:
        return "other";
      default:
        throw new IllegalArgumentException("Invalid parentRole: " + attribute);
    }
  }

  @Override
  public ParentRole convertToEntityAttribute(String dbData) {
    switch (dbData) {
      case "mother":
        return ParentRole.MOTHER;
      case "father":
        return ParentRole.FATHER;
      case "grand_father":
        return ParentRole.GRAND_FATHER;
      case "grand_mother":
        return ParentRole.GRAND_MOTHER;
      case "significant_other":
        return ParentRole.SIGNIFICANT_OTHER;
      case "other":
        return ParentRole.OTHER;
      default:
        throw new IllegalArgumentException("Invalid parentRole: " + dbData);
    }

  }
}
