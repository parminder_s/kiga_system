package com.kiga.kga.security;

import com.kiga.kga.domain.KgaKindergartenPermission;
import com.kiga.kga.repository.KgaKindergartenPermissionRepository;
import com.kiga.security.spring.AbstractKigaPermissionEvaluator;
import com.kiga.security.spring.KigaAuthentication;
import com.kiga.security.spring.KigaPermissionEvaluator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KgaKindergartenPermissionEvaluator extends AbstractKigaPermissionEvaluator {

  private final KgaKindergartenPermissionRepository kgaKindergartenPermissionRepository;

  @Autowired
  public KgaKindergartenPermissionEvaluator(
      KgaKindergartenPermissionRepository kgaKindergartenPermissionRepository,
      KigaPermissionEvaluator kigaPermissionEvaluator) {
    this.kgaKindergartenPermissionRepository = kgaKindergartenPermissionRepository;
    kigaPermissionEvaluator.addEvaluator("KgaKindergarten", this);
  }

  @Override
  public boolean hasPermission(
      KigaAuthentication authentication, Long id, String targetType, String permission) {
    if (authentication.isAdmin()) {
      return true;
    }
    KgaKindergartenPermission kindergartenPermission =
        kgaKindergartenPermissionRepository.findByEmployee_MemberAndKindergarten_Id(
            authentication.getDetails().getMember(), id);
    if (kindergartenPermission == null) {
      return false;
    } else {
      switch (KgaPermission.valueOf(permission)) {
        case READ:
          return kindergartenPermission.isAllowRead();
        case WRITE:
          return kindergartenPermission.isAllowWrite();
        default:
          return false;
      }
    }
  }
}
