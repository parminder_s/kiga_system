package com.kiga.kga.controller;

import com.kiga.kga.api.controller.GroupApi;
import com.kiga.kga.api.model.IdRequest;
import com.kiga.kga.api.model.ImportFromOtherYearRequest;
import com.kiga.kga.api.model.KgaGroupModel;
import com.kiga.kga.api.model.SetAdminRequest;
import com.kiga.kga.api.model.UpdateChildInGroup;
import com.kiga.kga.mapper.KgaGroupMapper;
import com.kiga.kga.service.KgaGroupService;
import com.kiga.kga.service.KgaPermissionService;
import com.kiga.kga.service.YearService;
import com.kiga.security.spring.KigaUser;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GroupController implements GroupApi {

  private final KgaGroupService kgaGroupService;
  private final KgaPermissionService kgaPermissionService;
  private final YearService yearService;

  @Autowired
  public GroupController(
      KgaGroupService kgaGroupService,
      KgaPermissionService kgaPermissionService,
      YearService yearService) {
    this.kgaGroupService = kgaGroupService;
    this.kgaPermissionService = kgaPermissionService;
    this.yearService = yearService;
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaGroup', 'READ')")
  public ResponseEntity<KgaGroupModel> findOneGroup(@RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        KgaGroupMapper.INSTANCE.kgaGroupToKgaGroupModel(
            kgaGroupService.findOne(idRequest.getId().longValue())));
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaYear', 'READ')")
  public ResponseEntity<List<KgaGroupModel>> listKgaGroupByYear(@RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        kgaGroupService
            .findByYearId(idRequest.getId().longValue())
            .stream()
            .map(KgaGroupMapper.INSTANCE::kgaGroupToKgaGroupModel)
            .collect(Collectors.toList()));
  }

  @Override
  @PreAuthorize("hasPermission(#kgaGroupModel.id, 'KgaGroup', 'WRITE')")
  public ResponseEntity<BigDecimal> saveKgaGroup(@RequestBody KgaGroupModel kgaGroupModel) {
    return ResponseEntity.ok(
        new BigDecimal(
            kgaGroupService
                .save(
                    KgaGroupMapper.INSTANCE.kgaGroupModelToKgaGroup(kgaGroupModel),
                    kgaGroupModel.getYearId())
                .toString()));
  }

  @Override
  @PreAuthorize("hasPermission(#setAdminRequest.id, 'KgaKindergarten', 'WRITE')")
  public ResponseEntity<String> setAdmin(@RequestBody SetAdminRequest setAdminRequest) {
    kgaGroupService.setAdmin(setAdminRequest.getId().longValue(), setAdminRequest.getEmail());
    return ResponseEntity.ok("Set Admin successfully");
  }

  @Override
  public ResponseEntity<String> acceptAdmin() {
    kgaPermissionService.finishRegistration(
        ((KigaUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
            .getMember());
    return null;
  }

  @Override
  @PreAuthorize("hasPermission(#setAdminRequest.id, 'KgaKindergarten', 'WRITE')")
  public ResponseEntity<String> setSecondAdmin(@RequestBody SetAdminRequest setAdminRequest) {
    kgaGroupService.setSecondAdmin(setAdminRequest.getId().longValue(), setAdminRequest.getEmail());
    return ResponseEntity.ok("Set Second Admin successfully");
  }

  @Override
  @PreAuthorize("hasPermission(#importRequest.yearId, 'KgaYear', 'WRITE')")
  public ResponseEntity<String> importFromOtherYear(
      @RequestBody ImportFromOtherYearRequest importRequest) {
    yearService.importFromOtherYear(
        importRequest.getYearId().longValue(),
        importRequest.getGroupIds().stream().map(Integer::longValue).collect(Collectors.toList()));
    return ResponseEntity.ok("imported data year");
  }

  @Override
  @PreAuthorize("hasPermission(#updateChildInGroup.yearId, 'KgaYear', 'WRITE')")
  public ResponseEntity<String> updateGroups(@RequestBody UpdateChildInGroup updateChildInGroup) {
    kgaGroupService.updateGroups(updateChildInGroup);
    return ResponseEntity.ok("Ok");
  }
}
