package com.kiga.kga.domain;

import javax.persistence.Entity;

@Entity
public class Allergy_Versions extends AllergyData<KgaChild_Versions> {
  public Allergy_Versions() {
    setClassName("Allergy_Versions");
  }

  public Allergy_Versions(AllergyEnum allergyEnum) {
    this();
    this.setAllergy(allergyEnum);
  }
}
