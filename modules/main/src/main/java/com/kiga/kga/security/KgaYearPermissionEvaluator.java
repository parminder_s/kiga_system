package com.kiga.kga.security;

import com.kiga.kga.domain.KgaPermissions;
import com.kiga.kga.repository.KgaYearPermissionRepository;
import com.kiga.kga.repository.KgaYearRepository;
import com.kiga.security.spring.AbstractKigaPermissionEvaluator;
import com.kiga.security.spring.KigaAuthentication;
import com.kiga.security.spring.KigaPermissionEvaluator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KgaYearPermissionEvaluator extends AbstractKigaPermissionEvaluator {
  private final KgaYearPermissionRepository kgaYearPermissionRepository;
  private final KgaKindergartenPermissionEvaluator kgaKindergartenPermissionEvaluator;
  private final KgaYearRepository kgaYearRepository;

  @Autowired
  public KgaYearPermissionEvaluator(
      KgaYearPermissionRepository kgaYearPermissionRepository,
      KigaPermissionEvaluator kigaPermissionEvaluator,
      KgaKindergartenPermissionEvaluator kgaKindergartenPermissionEvaluator,
      KgaYearRepository kgaYearRepository) {
    this.kgaYearPermissionRepository = kgaYearPermissionRepository;
    this.kgaKindergartenPermissionEvaluator = kgaKindergartenPermissionEvaluator;
    this.kgaYearRepository = kgaYearRepository;
    kigaPermissionEvaluator.addEvaluator("KgaYear", this);
  }

  @Override
  public boolean hasPermission(
      KigaAuthentication authentication, Long id, String targetType, String permission) {

    KgaPermissions kgaPermission =
        kgaYearPermissionRepository.findByEmployee_MemberAndYear_Id(
            authentication.getDetails().getMember(), id);
    if (kgaPermission == null) {
      return kgaKindergartenPermissionEvaluator.hasPermission(
          authentication,
          kgaYearRepository.findOne(id).getKindergarten().getId(),
          targetType,
          permission);
    } else {
      switch (KgaPermission.valueOf(permission)) {
        case READ:
          return kgaPermission.isAllowRead();
        case WRITE:
          return kgaPermission.isAllowWrite();
        default:
          return false;
      }
    }
  }
}
