package com.kiga.kga.domain;

public enum AllergyEnum {
  PEANUT,
  EGG,
  FISH,
  GLUTEN,
  SHELLFISH,
  LUPINEN,
  MILK,
  NUTS,
  BEE,
  POLLEN,
  GRASS,
  LATEX,
  OTHER
}
