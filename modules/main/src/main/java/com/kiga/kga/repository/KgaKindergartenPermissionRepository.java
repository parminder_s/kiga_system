package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaEmployee;
import com.kiga.kga.domain.KgaKindergartenPermission;
import com.kiga.security.domain.Member;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface KgaKindergartenPermissionRepository
    extends CrudRepository<KgaKindergartenPermission, Long> {

  List<KgaKindergartenPermission> findByEmployee(KgaEmployee kgaEmployee);

  KgaKindergartenPermission findByEmployee_MemberAndKindergarten_Id(Member member, Long id);
}
