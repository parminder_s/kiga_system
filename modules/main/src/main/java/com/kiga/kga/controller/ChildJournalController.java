package com.kiga.kga.controller;

import com.kiga.kga.api.controller.ChildJournalApi;
import com.kiga.kga.api.model.ChildJournalEntry;
import com.kiga.kga.api.model.IdRequest;
import com.kiga.kga.mapper.ChildJournalEntryMapper;
import com.kiga.kga.service.ChildJournalService;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChildJournalController implements ChildJournalApi {

  private final ChildJournalService childJournalService;

  @Autowired
  public ChildJournalController(ChildJournalService childJournalService) {
    this.childJournalService = childJournalService;
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaChild', 'READ')")
  public ResponseEntity<List<ChildJournalEntry>> findForChild(@RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        childJournalService
            .findByChildId(idRequest.getId().longValue())
            .stream()
            .map(ChildJournalEntryMapper.INSTANCE::toModel)
            .collect(Collectors.toList()));
  }

  @Override
  @PreAuthorize("hasPermission(#childJournalEntry.childId, 'KgaChild', 'WRITE')")
  public ResponseEntity<BigDecimal> saveChildJournalEntry(
      @RequestBody ChildJournalEntry childJournalEntry) {
    return ResponseEntity.ok(
        new BigDecimal(
            childJournalService
                .save(
                    ChildJournalEntryMapper.INSTANCE.toEntity(childJournalEntry),
                    childJournalEntry.getChildId())
                .getId()
                .toString()));
  }

  @RequestMapping(
      value = "/kga/childJournal/file/{childId}/{fileId}",
      produces = {"application/json"},
      method = RequestMethod.GET)
  @PreAuthorize("hasPermission(#childId, 'KgaChild', 'READ')")
  public void fetchFile(
      @PathVariable Integer childId, @PathVariable Integer fileId, HttpServletResponse response)
      throws IOException {
    childJournalService.fetchFile(childId, fileId, response);
  }
}
