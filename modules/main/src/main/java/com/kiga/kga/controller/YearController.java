package com.kiga.kga.controller;

import com.kiga.kga.api.controller.YearApi;
import com.kiga.kga.api.model.IdRequest;
import com.kiga.kga.api.model.YearModel;
import com.kiga.kga.mapper.YearMapper;
import com.kiga.kga.service.YearService;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class YearController implements YearApi {

  private final YearService yearService;

  @Autowired
  public YearController(YearService yearService) {
    this.yearService = yearService;
  }

  @Override
  public ResponseEntity<String> archiveYear(@RequestBody IdRequest idRequest) {
    yearService.archive(idRequest.getId().longValue());
    return ResponseEntity.ok("archived year");
  }

  @Override
  public ResponseEntity<YearModel> findOneYear(@RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        YearMapper.INSTANCE.yearToYearModel(yearService.findOne(idRequest.getId().longValue())));
  }

  @Override
  public ResponseEntity<List<YearModel>> listYearByKindergarten(@RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        yearService
            .findByKindergarten(idRequest.getId().longValue())
            .stream()
            .map(YearMapper.INSTANCE::yearToYearModel)
            .collect(Collectors.toList()));
  }

  @Override
  public ResponseEntity<BigDecimal> saveYear(@RequestBody YearModel yearModel) {
    return ResponseEntity.ok(
        new BigDecimal(
            yearService
                .save(
                    YearMapper.INSTANCE.yearModelToYear(yearModel),
                    yearModel.getKindergartenId().longValue())
                .toString()));
  }
}
