package com.kiga.kga.security;

import com.kiga.kga.domain.KgaGroupPermission;
import com.kiga.kga.repository.KgaChildPermissionRepository;
import com.kiga.kga.repository.KgaChildRepository;
import com.kiga.security.spring.AbstractKigaPermissionEvaluator;
import com.kiga.security.spring.KigaAuthentication;
import com.kiga.security.spring.KigaPermissionEvaluator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KgaChildPermissionEvaluator extends AbstractKigaPermissionEvaluator {

  private final KgaChildPermissionRepository kgaChildPermissionRepository;
  private final KgaKindergartenPermissionEvaluator kindergartenPermissionEvaluator;
  private final KgaChildRepository kgaChildRepository;

  @Autowired
  public KgaChildPermissionEvaluator(
      KgaChildPermissionRepository kgaChildPermissionRepository,
      KigaPermissionEvaluator kigaPermissionEvaluator,
      KgaKindergartenPermissionEvaluator kindergartenPermissionEvaluator,
      KgaChildRepository kgaChildRepository) {
    this.kgaChildPermissionRepository = kgaChildPermissionRepository;
    this.kindergartenPermissionEvaluator = kindergartenPermissionEvaluator;
    this.kgaChildRepository = kgaChildRepository;
    kigaPermissionEvaluator.addEvaluator("KgaChild", this);
  }

  @Override
  public boolean hasPermission(
      KigaAuthentication authentication, Long id, String targetType, String permission) {
    KgaGroupPermission kgaChildPermission =
        kgaChildPermissionRepository.findByEmployee_MemberAndChild_Id(
            authentication.getDetails().getMember(), id);
    if (id == null) {
      return false;
    }
    if (kgaChildPermission == null) {
      return kindergartenPermissionEvaluator.hasPermission(
          authentication,
          kgaChildRepository.findOne(id).getKindergarten().getId(),
          targetType,
          permission);
    } else {
      switch (KgaPermission.valueOf(permission)) {
        case READ:
          return kgaChildPermission.isAllowRead();
        case WRITE:
          return kgaChildPermission.isAllowWrite();
        default:
          return false;
      }
    }
  }
}
