package com.kiga.kga.domain;

public interface KgaPermissions {
  boolean isAllowRead();

  boolean isAllowWrite();
}
