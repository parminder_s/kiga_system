package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.s3.domain.S3File;
import java.util.Date;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Builder
public class ChildJournalEntry extends KigaEntityModel {
  @ManyToOne KgaChild child;
  String title;
  String content;
  Date date;

  @ManyToMany
  @JoinTable(
      name = "ChildJournal_has_File",
      joinColumns = @JoinColumn(name = "journalId"),
      inverseJoinColumns = @JoinColumn(name = "s3FileId"))
  private Set<S3File> files;

  public ChildJournalEntry() {
    this.setClassName("ChildJournalEntry");
  }
}
