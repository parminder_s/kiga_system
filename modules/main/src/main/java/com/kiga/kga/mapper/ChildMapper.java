package com.kiga.kga.mapper;

import com.kiga.kga.api.model.AllergyEnum;
import com.kiga.kga.api.model.ChildModel;
import com.kiga.kga.api.model.DiseasesEnum;
import com.kiga.kga.api.model.KgaParentModel;
import com.kiga.kga.domain.*;
import com.kiga.util.mapper.DateMapper;
import com.kiga.util.mapper.MapStructConfig;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(
    config = MapStructConfig.class,
    uses = {GenderMapper.class, DateMapper.class, KgaParentToChildMapper.class})

// check https://github.com/mapstruct/mapstruct/issues/583
// https://github.com/mapstruct/mapstruct/issues/653
public abstract class ChildMapper {
  public static ChildMapper INSTANCE = Mappers.getMapper(ChildMapper.class);

  @Mapping(target = "className", constant = "KgaChild")
  @Mapping(source = "nativeLanguageId", target = "nativeLanguage.id")
  @Mapping(source = "citizenshipCountryId", target = "citizenship.id")
  @Mapping(target = "created", ignore = true)
  @Mapping(target = "lastEdited", ignore = true)
  @Mapping(target = "parents", ignore = true)
  @Mapping(target = "childInKgaGroups", ignore = true)
  @Mapping(target = "archives", ignore = true)
  @Mapping(target = "kindergarten", ignore = true)
  @Mapping(target = "deleted", ignore = true)
  public abstract KgaChild childModelToChild(ChildModel request);

  @Mapping(source = "nativeLanguage.id", target = "nativeLanguageId")
  @Mapping(source = "citizenship.id", target = "citizenshipCountryId")
  @Mapping(source = "kindergarten.id", target = "kindergartenId")
  @Mapping(target = "groupId", ignore = true)
  public abstract ChildModel helper(KgaChildData req);

  @Mapping(source = "childInKgaGroups", target = "groupId")
  @Mapping(target = "diseases", qualifiedByName = "test1234")
  public ChildModel childToChildModel(KgaChildData request) {
    ChildModel returner = helper(request);
    returner.setGroupId(getYear(request.getChildInKgaGroups()));

    returner.setParents(Collections.emptyList());
    if (request.getParents() != null) {
      returner.setParents(
          (List<KgaParentModel>)
              (request.getParents())
                  .stream()
                  .map(x -> KgaParentToChildMapper.INSTANCE.entityToModel((KgaParentToChildData) x))
                  .collect(Collectors.toList()));
    }

    returner.setDiseases(Collections.emptyList());
    if (request.getDiseases() != null) {
      returner.setDiseases(
          (List<DiseasesEnum>)
              (request.getDiseases())
                  .stream()
                  .map(x -> asSwagger((DiseaseData) x))
                  .collect(Collectors.toList()));
    }

    returner.setAllergies(Collections.emptyList());
    if (request.getAllergies() != null) {
      returner.setAllergies(
          (List<AllergyEnum>)
              (request.getAllergies())
                  .stream()
                  .map(x -> asSwagger((AllergyData) x))
                  .collect(Collectors.toList()));
    }

    returner.setVaccinations(Collections.emptyList());
    if (request.getVaccinations() != null) {
      returner.setVaccinations(
          (List<com.kiga.kga.api.model.VaccinationEnum>)
              (request.getVaccinations())
                  .stream()
                  .map(x -> asSwagger((VaccinationData) x))
                  .collect(Collectors.toList()));
    }

    return returner;
  }

  @Mapping(target = "child", ignore = true)
  @Mapping(target = "editor", ignore = true)
  @Mapping(target = "original", ignore = true)
  @Mapping(target = "childInKgaGroups", ignore = true)
  @Mapping(target = "parents", ignore = true)
  @Mapping(target = "className", constant = "KgaChild_Versions")
  public abstract KgaChild_Versions childToChildArchive(KgaChild kgaChild);

  /** Custom mapping for languageId to Language. */
  Language asLannguage(Integer id) {
    if (id == null) {
      return null;
    }
    Language returner = new Language();
    returner.setId(id.longValue());
    return returner;
  }

  BigDecimal languageToSwagger(Language language) {
    return new BigDecimal(language.getId().toString());
  }

  Allergy allergy(com.kiga.kga.api.model.AllergyEnum value) {
    Allergy returner = new Allergy();
    returner.setAllergy(com.kiga.kga.domain.AllergyEnum.valueOf(value.toString()));
    return returner;
  }

  com.kiga.kga.api.model.AllergyEnum asSwagger(AllergyData allergy) {
    return AllergyEnum.valueOf(allergy.getAllergy().toString());
  }

  Vaccination vacinationAsDb(com.kiga.kga.api.model.VaccinationEnum vacinationEnum) {
    Vaccination returner = new Vaccination();
    returner.setVaccination(com.kiga.kga.domain.VaccinationEnum.valueOf(vacinationEnum.toString()));
    return returner;
  }

  com.kiga.kga.api.model.VaccinationEnum vacinationAsSwagger(Vaccination vacination) {
    return com.kiga.kga.api.model.VaccinationEnum.valueOf(vacination.getVaccination().toString());
  }

  DiseasesEnum asSwagger(DiseaseData disease) {
    return DiseasesEnum.valueOf(disease.getDisease().toString());
  }

  com.kiga.kga.api.model.VaccinationEnum asSwagger(VaccinationData disease) {
    return com.kiga.kga.api.model.VaccinationEnum.valueOf(disease.getVaccination().toString());
  }

  Disease asDb(DiseasesEnum diseasesEnum) {
    Disease returner = new Disease();
    returner.setDisease(DiseaseEnum.valueOf(diseasesEnum.toString()));
    return returner;
  }

  /** returns the current year. */
  private Integer getYear(List<ChildInKgaGroupData> list) {
    if (list == null || list.isEmpty()) {
      return null;
    }
    return list.get(0).getGroup().getId().intValue();
  }

  Vaccination_Versions toVersions(Vaccination data) {
    Vaccination_Versions returner = new Vaccination_Versions();
    returner.setVaccination(data.getVaccination());
    return returner;
  }

  Vaccination toActual(Vaccination_Versions data) {
    Vaccination returner = new Vaccination();
    returner.setVaccination(data.getVaccination());
    return returner;
  }

  Allergy_Versions toVersions(Allergy data) {
    Allergy_Versions returner = new Allergy_Versions();
    returner.setAllergy(data.getAllergy());
    return returner;
  }

  Allergy toActual(Allergy_Versions data) {
    Allergy returner = new Allergy();
    returner.setAllergy(data.getAllergy());
    return returner;
  }

  Disease_Versions toVersions(Disease data) {
    Disease_Versions returner = new Disease_Versions();
    returner.setDisease(data.getDisease());
    return returner;
  }

  Disease toActual(Disease_Versions data) {
    Disease returner = new Disease();
    returner.setDisease(data.getDisease());
    return returner;
  }

  ChildInKgaGroup_Versions toVersions(ChildInKgaGroup data) {
    ChildInKgaGroup_Versions returner = new ChildInKgaGroup_Versions();
    returner.setChild(null);
    returner.setGroup(data.getGroup());
    return returner;
  }

  ChildInKgaGroup toActual(ChildInKgaGroup_Versions data) {
    ChildInKgaGroup returner = new ChildInKgaGroup();
    returner.setChild(null);
    returner.setGroup(data.getGroup());
    return returner;
  }

  Integer toInt(List list) {
    if (list == null || list.isEmpty()) {
      return null;
    }
    return 1;
    // return list.get(0).getGroup().getId().intValue();
  }
}
