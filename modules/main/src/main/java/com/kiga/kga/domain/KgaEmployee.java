package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.kga.domain.converter.EmploymentTypeConverter;
import com.kiga.kga.domain.converter.GenderConverter;
import com.kiga.security.domain.Member;
import com.kiga.shop.domain.Country;
import java.util.Date;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
public class KgaEmployee extends KigaEntityModel {
  public KgaEmployee() {
    this.setClassName("KgaEmployee");
  }

  @ManyToOne private KgaKindergarten kindergarten;

  @ManyToOne private Member member;

  private String email;
  private String firstname;
  private String lastname;

  @Convert(converter = EmploymentTypeConverter.class)
  private EmploymentType employmentType;

  private boolean parentalLeave;

  @Convert(converter = GenderConverter.class)
  private Gender gender;

  private String socialSecurityNumber;
  private String phoneNumber;
  private Date birthDate;

  private String street;
  private String houseNumber;
  private String doorNumber;
  private String floor;
  private String zip;
  private String city;
  @ManyToOne private Country country;

  private EmploymentContract employmentContract;
  private String employmentContractNote;
}
