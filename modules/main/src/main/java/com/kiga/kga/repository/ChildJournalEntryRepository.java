package com.kiga.kga.repository;

import com.kiga.kga.domain.ChildJournalEntry;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface ChildJournalEntryRepository extends CrudRepository<ChildJournalEntry, Long> {
  List<ChildJournalEntry> findByChild_Id(Long id);
}
