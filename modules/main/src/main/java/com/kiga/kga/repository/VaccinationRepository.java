package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaChild;
import com.kiga.kga.domain.Vaccination;
import org.springframework.data.repository.CrudRepository;

public interface VaccinationRepository extends CrudRepository<Vaccination, Long> {
  void deleteByChild(KgaChild child);
}
