package com.kiga.kga.survey.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.security.domain.Member;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder()
@AllArgsConstructor
public class Survey extends KigaEntityModel {
  private String title;
  private String description;
  @ManyToOne private Member author;

  @OneToMany(mappedBy = "survey")
  @OrderBy("position")
  private List<Section> sections;

  public Survey() {
    this.setClassName("Survey");
  }
}
