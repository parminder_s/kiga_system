package com.kiga.kga.domain;

public enum Gender {
  MALE,
  FEMALE,
  OTHER;
}
