package com.kiga.kga.controller;

import com.kiga.kga.api.controller.EmployeeApi;
import com.kiga.kga.api.model.EmployeeModel;
import com.kiga.kga.api.model.IdRequest;
import com.kiga.kga.mapper.KgaEmployeeMapper;
import com.kiga.kga.service.EmployeeService;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController implements EmployeeApi {
  private final EmployeeService employeeService;

  @Autowired
  public EmployeeController(EmployeeService employeeService) {
    this.employeeService = employeeService;
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaEmployee', 'READ')")
  public ResponseEntity<EmployeeModel> findOneEmployee(@RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        KgaEmployeeMapper.INSTANCE.entityToModel(
            employeeService.findOne(idRequest.getId().longValue())));
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaKindergarten', 'READ')")
  public ResponseEntity<List<EmployeeModel>> listEmployeeByKindergarten(
      @RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        employeeService
            .listByKindergarten(idRequest.getId().longValue())
            .stream()
            .map(KgaEmployeeMapper.INSTANCE::entityToModel)
            .collect(Collectors.toList()));
  }

  @Override
  @PreAuthorize("hasPermission(#employeeModel.id, 'KgaEmployee', 'WRITE')")
  public ResponseEntity<BigDecimal> saveEmployee(@RequestBody EmployeeModel employeeModel) {
    return ResponseEntity.ok(
        new BigDecimal(
            employeeService
                .save(
                    KgaEmployeeMapper.INSTANCE.modelToEntiyt(employeeModel),
                    employeeModel.getKindergartenId().longValue())
                .getId()
                .toString()));
  }
}
