package com.kiga.kga.mapper;

import com.kiga.kga.api.model.FileModel;
import com.kiga.kga.domain.ChildJournalEntry;
import com.kiga.s3.domain.S3File;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ChildJournalEntryMapper {
  ChildJournalEntryMapper INSTANCE = Mappers.getMapper(ChildJournalEntryMapper.class);

  @Mapping(target = "created", ignore = true)
  @Mapping(target = "lastEdited", ignore = true)
  @Mapping(target = "child", ignore = true)
  @Mapping(target = "className", constant = "ChildJournalEntry")
  ChildJournalEntry toEntity(com.kiga.kga.api.model.ChildJournalEntry model);

  @Mapping(source = "child.id", target = "childId")
  com.kiga.kga.api.model.ChildJournalEntry toModel(ChildJournalEntry model);

  default List<FileModel> toFileModel(Set<S3File> files) {
    return files
        .stream()
        .map(file -> new FileModel().id(file.getId().intValue()).name(file.getName()))
        .collect(Collectors.toList());
  }

  default Set<S3File> toFileModel(List<FileModel> files) {
    return files
        .stream()
        .map(
            file -> {
              S3File s3File = new S3File();
              s3File.setId(file.getId().longValue());
              return s3File;
            })
        .collect(Collectors.toSet());
  }
}
