package com.kiga.kga.domain;

import javax.persistence.Entity;

@Entity
public class ChildInKgaGroup_Versions extends ChildInKgaGroupData<KgaChild_Versions> {
  public ChildInKgaGroup_Versions() {
    this.setClassName("ChildInKgaGroup_Versions");
  }
}
