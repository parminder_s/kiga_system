package com.kiga.kga.domain;

import javax.persistence.Entity;

@Entity
public class Disease_Versions extends DiseaseData<KgaChild_Versions> {
  public Disease_Versions(DiseaseEnum diseaseEnum) {
    this();
    this.setDisease(diseaseEnum);
  }

  public Disease_Versions() {
    setClassName("Disease_Versions");
  }
}
