package com.kiga.kga.props;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("kga")
@Data
public class KgaProperties {
  @Autowired private KgaS3Properties kgaS3Properties;
}
