package com.kiga.kga.survey.domain;

import com.kiga.db.KigaEntityModel;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder(toBuilder = true)
@AllArgsConstructor
public class Question extends KigaEntityModel {
  private int position;
  private String questionText;
  private String description;
  private int minAnswer;
  private int maxAnswer;

  @ManyToOne private Section section;

  public Question() {
    this.setClassName("Question");
  }
}
