package com.kiga.kga.mapper;

import com.kiga.kga.api.model.KindergartenModel;
import com.kiga.kga.domain.KgaKindergarten;
import com.kiga.kga.domain.KgaYear;
import com.kiga.util.mapper.MapStructConfig;
import java.util.Comparator;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(
    config = MapStructConfig.class,
    uses = {CountryMapper.class})
public interface KindergartenMapper {
  KindergartenMapper INSTANCE = Mappers.getMapper(KindergartenMapper.class);

  @Mapping(target = "address.street", source = "street")
  @Mapping(target = "address.houseNumber", source = "houseNumber")
  @Mapping(target = "address.floor", source = "floor")
  @Mapping(target = "address.doorNumber", source = "doorNumber")
  @Mapping(target = "address.zipCode", source = "zip")
  @Mapping(target = "address.city", source = "city")
  @Mapping(target = "address.countryId", source = "country.id")
  @Mapping(target = "currentYearId", source = "years")
  @Mapping(target = "adminCustomerNumber", constant = "#123123123") // todo
  KindergartenModel kindergartenToKindergartenModel(KgaKindergarten kindergarten);

  @Mapping(target = "className", constant = "KgaKindergarten")
  @Mapping(target = "created", ignore = true)
  @Mapping(target = "lastEdited", ignore = true)
  @Mapping(target = "years", ignore = true)
  @Mapping(target = "children", ignore = true)
  @Mapping(target = "street", source = "address.street")
  @Mapping(target = "houseNumber", source = "address.houseNumber")
  @Mapping(target = "floor", source = "address.floor")
  @Mapping(target = "doorNumber", source = "address.doorNumber")
  @Mapping(target = "zip", source = "address.zipCode")
  @Mapping(target = "city", source = "address.city")
  @Mapping(target = "country", source = "address.countryId")
  KgaKindergarten kindergartenModelToKindergarten(KindergartenModel kindergartenModel);

  /** maps the id of the year with the latest end date. */
  default Integer yearsToCurrentYearId(List<KgaYear> years) {
    if (years.isEmpty()) {
      return null;
    }
    years.sort(Comparator.comparing(KgaYear::getEndDate));
    return years.get(years.size() - 1).getId().intValue();
  }
}
