package com.kiga.kga.repository;

import com.kiga.kga.domain.Allergy;
import com.kiga.kga.domain.KgaChild;
import org.springframework.data.repository.CrudRepository;

public interface AllergyRepository extends CrudRepository<Allergy, Long> {
  void deleteByChild(KgaChild child);
}
