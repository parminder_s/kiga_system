package com.kiga.kga.mapper;

import com.kiga.kga.api.model.KgaParentModel;
import com.kiga.kga.domain.KgaParent;
import com.kiga.util.mapper.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapStructConfig.class, uses = GenderMapper.class)
public interface KgaParentMapper {
  KgaParentMapper INSTANCE = Mappers.getMapper(KgaParentMapper.class);

  @Mapping(target = "className", constant = "KgaParent")
  @Mapping(target = "created", ignore = true)
  @Mapping(target = "lastEdited", ignore = true)
  @Mapping(target = "kindergarten", ignore = true) // todo check if we should load it
  @Mapping(target = "title", ignore = true)
  KgaParent kgaParentModelToKgaParent(KgaParentModel kgaParentModel);

  @Mapping(target = "role", ignore = true)
  KgaParentModel kgaParentToKgaParentModel(KgaParent parent);
}
