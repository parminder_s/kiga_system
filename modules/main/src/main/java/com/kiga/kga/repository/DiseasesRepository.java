package com.kiga.kga.repository;

import com.kiga.kga.domain.Disease;
import com.kiga.kga.domain.KgaChild;
import org.springframework.data.repository.CrudRepository;

public interface DiseasesRepository extends CrudRepository<Disease, Long> {
  void deleteByChild(KgaChild child);
}
