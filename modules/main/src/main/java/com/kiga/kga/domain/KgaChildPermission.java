package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class KgaChildPermission extends KigaEntityModel implements KgaPermissions {
  private boolean allowRead;
  private boolean allowWrite;

  @ManyToOne private KgaEmployee employee;

  @ManyToOne private KgaChild child;

  public KgaChildPermission() {
    this.setClassName("KgaChildPermission");
  }
}
