package com.kiga.kga.domain.converter;

import com.kiga.kga.domain.VaccinationEnum;
import javax.persistence.AttributeConverter;

public class VaccinationConverter implements AttributeConverter<VaccinationEnum, String> {
  @Override
  public String convertToDatabaseColumn(VaccinationEnum attribute) {
    switch (attribute) {
      case ACWY:
        return "acwy";
      case HUMANE_PAPILLONMAVIREN:
        return "humane_papillonmaviren";
      case MENINGOKOKKEN_B:
        return "meningokokken_b";
      case MENINGOKOKKEN_C:
        return "meningokokken_c";
      case FSME:
        return "fsme";
      case VARIZELLEN:
        return "varizellen";
      case HEPATITIS_A:
        return "hepatitis_a";
      case INFLUENZA:
        return "influenza";
      case HERPES_ZOSTA:
        return "herpes_zosta";
      case ROTAVIRUS:
        return "rotavirus";
      case DIPHTHERIE:
        return "diphtherie";
      case TETANUS:
        return "tetanus";
      case PERTUSSIS:
        return "pertussis";
      case POLIOMYELITIS:
        return "poliomyelitis";
      case HAEMOHILUS_INFLUENZA_B:
        return "haemohilus_influenza_b";
      case HEPATITIS_B:
        return "hepatitis_b";
      case PNEUMOKOKKEN:
        return "pneumokokken";
      case MASERN_MUMPS_ROTELN:
        return "masern_mumps_roteln";
      case OTHER:
        return "other";
    }
    throw new IllegalArgumentException("Not mapped: " + attribute);
  }

  @Override
  public VaccinationEnum convertToEntityAttribute(String dbData) {
    switch (dbData) {
      case "acwy":
        return VaccinationEnum.ACWY;
      case "humane_papillonmaviren":
        return VaccinationEnum.HUMANE_PAPILLONMAVIREN;
      case "meningokokken_b":
        return VaccinationEnum.MENINGOKOKKEN_B;
      case "meningokokken_c":
        return VaccinationEnum.MENINGOKOKKEN_C;
      case "fsme":
        return VaccinationEnum.FSME;
      case "varizellen":
        return VaccinationEnum.VARIZELLEN;
      case "hepatitis_a":
        return VaccinationEnum.HEPATITIS_A;
      case "influenza":
        return VaccinationEnum.INFLUENZA;
      case "herpes_zosta":
        return VaccinationEnum.HEPATITIS_A;
      case "masern_mumps_roteln":
        return VaccinationEnum.MASERN_MUMPS_ROTELN;
      case "rotavirus":
        return VaccinationEnum.ROTAVIRUS;
      case "diphtherie":
        return VaccinationEnum.DIPHTHERIE;
      case "tetanus":
        return VaccinationEnum.TETANUS;
      case "pertussis":
        return VaccinationEnum.PERTUSSIS;
      case "poliomyelitis":
        return VaccinationEnum.POLIOMYELITIS;
      case "haemohilus_influenza_b":
        return VaccinationEnum.HAEMOHILUS_INFLUENZA_B;
      case "hepatitis_b":
        return VaccinationEnum.HEPATITIS_B;
      case "pneumokokken":
        return VaccinationEnum.PNEUMOKOKKEN;
      case "other":
        return VaccinationEnum.OTHER;
    }
    throw new IllegalArgumentException("Not mapped: " + dbData);
  }
}
