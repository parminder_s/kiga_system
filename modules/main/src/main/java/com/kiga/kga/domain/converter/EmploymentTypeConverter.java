package com.kiga.kga.domain.converter;

import com.kiga.kga.domain.EmploymentType;
import javax.persistence.AttributeConverter;

public class EmploymentTypeConverter implements AttributeConverter<EmploymentType, String> {
  @Override
  public String convertToDatabaseColumn(EmploymentType attribute) {
    if (attribute == null) {
      return null;
    }
    switch (attribute) {
      case EDUCATOR:
        return "educator";
      case CLEANER:
        return "cleaner";
      case CHILD_CARE_WORKER:
        return "child_care_worker";
      case DAY_NANNY:
        return "day_nanny";
      case DAY_DADDY:
        return "day_daddy";
      case COMMUNITY_SERVICE:
        return "community_service";
      case OTHER:
        return "other";
    }
    throw new IllegalArgumentException("Unmapped EmploymentType: " + attribute);
  }

  @Override
  public EmploymentType convertToEntityAttribute(String dbData) {
    if (dbData == null) {
      return null;
    }
    switch (dbData) {
      case "educator":
        return EmploymentType.EDUCATOR;
      case "child_care_worker":
        return EmploymentType.CHILD_CARE_WORKER;
      case "other":
        return EmploymentType.OTHER;
      case "cleaner":
        return EmploymentType.CLEANER;
      case "day_nanny":
        return EmploymentType.DAY_NANNY;
      case "day_daddy":
        return EmploymentType.DAY_DADDY;
      case "community_service":
        return EmploymentType.COMMUNITY_SERVICE;
    }
    throw new IllegalArgumentException("Unmapped EmploymentType: " + dbData);
  }
}
