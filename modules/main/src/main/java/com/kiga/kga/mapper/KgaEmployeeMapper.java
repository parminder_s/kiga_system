package com.kiga.kga.mapper;

import com.kiga.kga.api.model.EmployeeModel;
import com.kiga.kga.domain.KgaEmployee;
import com.kiga.util.mapper.DateMapper;
import com.kiga.util.mapper.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(
    config = MapStructConfig.class,
    uses = {GenderMapper.class, DateMapper.class, CountryMapper.class})
public interface KgaEmployeeMapper {
  KgaEmployeeMapper INSTANCE = Mappers.getMapper(KgaEmployeeMapper.class);

  @Mapping(target = "className", constant = "KgaEmployee")
  @Mapping(target = "member", ignore = true)
  @Mapping(target = "created", ignore = true)
  @Mapping(target = "lastEdited", ignore = true)
  @Mapping(target = "kindergarten", ignore = true)
  @Mapping(target = "street", source = "address.street")
  @Mapping(target = "houseNumber", source = "address.houseNumber")
  @Mapping(target = "floor", source = "address.floor")
  @Mapping(target = "doorNumber", source = "address.doorNumber")
  @Mapping(target = "zip", source = "address.zipCode")
  @Mapping(target = "city", source = "address.city")
  @Mapping(target = "country", source = "address.countryId")
  KgaEmployee modelToEntiyt(EmployeeModel employeeModel);

  @Mapping(target = "kindergartenId", ignore = true)
  @Mapping(target = "address.street", source = "street")
  @Mapping(target = "address.houseNumber", source = "houseNumber")
  @Mapping(target = "address.floor", source = "floor")
  @Mapping(target = "address.doorNumber", source = "doorNumber")
  @Mapping(target = "address.zipCode", source = "zip")
  @Mapping(target = "address.city", source = "city")
  @Mapping(target = "address.countryId", source = "country.id")
  EmployeeModel entityToModel(KgaEmployee entity);
}
