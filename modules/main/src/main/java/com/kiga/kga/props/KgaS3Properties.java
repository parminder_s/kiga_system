package com.kiga.kga.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("kga.s3")
public class KgaS3Properties {
  private String bucketName;
  private String bucketUrl;
  private String accessKey;
  private String secretKey;
}
