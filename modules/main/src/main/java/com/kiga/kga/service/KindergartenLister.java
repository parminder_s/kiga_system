package com.kiga.kga.service;

import com.kiga.kga.domain.KgaKindergarten;
import com.kiga.kga.repository.KindergartenRepository;
import com.kiga.security.spring.SecurityUtil;
import com.kiga.web.security.KigaRoles;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * returns all kindergartens for the current user.
 *
 * <p>Admin have access to all, normal users according to the KgaKindergartenPermission.
 */
@Service
public class KindergartenLister {
  private final SecurityUtil securityUtil;
  private final KindergartenForEmployeeLister kindergartenForEmployeeLister;
  private final KindergartenRepository kindergartenRepository;

  @Autowired
  public KindergartenLister(
      SecurityUtil securityUtil,
      KindergartenForEmployeeLister kindergartenForEmployeeLister,
      KindergartenRepository kindergartenRepository) {
    this.securityUtil = securityUtil;
    this.kindergartenForEmployeeLister = kindergartenForEmployeeLister;
    this.kindergartenRepository = kindergartenRepository;
  }

  public List<KgaKindergarten> list() {
    return securityUtil
        .getOptMember()
        .map(member -> getForCurrentRole().collect(Collectors.toList()))
        .orElseGet(ArrayList::new);
  }

  private Stream<KgaKindergarten> getForCurrentRole() {
    if (securityUtil.hasRole(KigaRoles.ADMIN)) {
      return StreamSupport.stream(kindergartenRepository.findAll().spliterator(), false);
    } else {
      return kindergartenForEmployeeLister.list().stream();
    }
  }
}
