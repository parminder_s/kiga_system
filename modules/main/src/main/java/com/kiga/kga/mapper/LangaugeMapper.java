package com.kiga.kga.mapper;

import com.kiga.kga.api.model.LanguageViewModel;
import com.kiga.kga.domain.Language;
import com.kiga.util.mapper.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapStructConfig.class)
public interface LangaugeMapper {
  LangaugeMapper INSTANCE = Mappers.getMapper(LangaugeMapper.class);

  @Mapping(source = "code", target = "code")
  LanguageViewModel toViewModel(Language language);
}
