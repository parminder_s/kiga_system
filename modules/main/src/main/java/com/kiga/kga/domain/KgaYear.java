package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
public class KgaYear extends KigaEntityModel {

  private boolean archived;
  private Date startDate;
  private Date endDate;
  private Date archivationDate;

  @ManyToOne private KgaKindergarten kindergarten;

  @OneToMany(mappedBy = "year")
  private List<KgaGroup> groups;

  public KgaYear() {
    this.setClassName("Year");
  }
}
