package com.kiga.kga.domain;

import javax.persistence.Entity;
import lombok.Builder;
import lombok.Data;

@Entity
@Data
@Builder
public class Vaccination extends VaccinationData<KgaChild> {
  public Vaccination() {
    setClassName("Vaccination");
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Vaccination) {
      return ((Vaccination) o).getVaccination().equals(this.getVaccination());
    }
    return false;
  }

  @Override
  public int hashCode() {
    return super.getVaccination().hashCode();
  }
}
