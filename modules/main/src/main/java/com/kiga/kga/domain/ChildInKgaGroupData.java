package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@MappedSuperclass
@Data
@EqualsAndHashCode(
    callSuper = true,
    exclude = {"child", "group"})
@Builder(toBuilder = true)
@AllArgsConstructor
public class ChildInKgaGroupData<C extends KgaChildData> extends KigaEntityModel {

  @ManyToOne private C child;

  @ManyToOne private KgaGroup group;

  public ChildInKgaGroupData() {
    this.setClassName("ChildInKgaGroupData");
  }
}
