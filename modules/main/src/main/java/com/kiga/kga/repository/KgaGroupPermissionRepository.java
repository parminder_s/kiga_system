package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaGroupPermission;
import com.kiga.security.domain.Member;
import org.springframework.data.repository.CrudRepository;

public interface KgaGroupPermissionRepository
  extends CrudRepository<KgaGroupPermission, Long> {
  KgaGroupPermission findByEmployee_MemberAndGroup_Id(Member member, Long id);

}
