package com.kiga.kga.service;

import com.kiga.db.KigaEntityModel;
import com.kiga.db.RepositoryFetcher;
import com.kiga.kga.domain.ChildJournalEntry;
import com.kiga.kga.domain.ChildJournalHasFile;
import com.kiga.kga.domain.ChildJournalHasFileEntry;
import com.kiga.kga.repository.ChildJournalEntryRepository;
import com.kiga.kga.repository.ChildJournalHasFileRepository;
import com.kiga.s3.domain.S3File;
import com.kiga.s3.download.DownloadedS3Image;
import com.kiga.s3.repository.ss.S3FileRepository;
import com.kiga.s3.service.download.DownloadService;
import com.kiga.spec.Fetcher;
import com.kiga.util.BrowserPusherService;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChildJournalService {

  private final ChildJournalEntryRepository childJournalEntryRepository;
  private final ChildService childService;
  private final Fetcher<S3File> s3FileFetcher;
  private final ChildJournalHasFileRepository journalHasFileRepository;

  private final DownloadService downloadUsingRequestService;
  private final BrowserPusherService browserPusherService;

  @Autowired
  public ChildJournalService(
      ChildJournalEntryRepository childJournalEntryRepository,
      ChildService childService,
      S3FileRepository s3FileRepository,
      ChildJournalHasFileRepository journalHasFileRepository,
      DownloadService downloadUsingRequestService,
      BrowserPusherService browserPusherService) {
    this.childJournalEntryRepository = childJournalEntryRepository;
    this.childService = childService;
    this.journalHasFileRepository = journalHasFileRepository;
    this.downloadUsingRequestService = downloadUsingRequestService;
    this.browserPusherService = browserPusherService;
    this.s3FileFetcher = new RepositoryFetcher<>(s3FileRepository);
  }

  public List<ChildJournalEntry> findByChildId(Long childId) {
    return childJournalEntryRepository.findByChild_Id(childId);
  }

  public ChildJournalEntry save(ChildJournalEntry toSave, long childId) {
    // delete entries
    if (toSave.getId() != null) {
      ChildJournalEntry old = childJournalEntryRepository.findOne(toSave.getId());
      journalHasFileRepository.deleteByEntry_Journal_IdAndEntry_S3File_IdIn(
          old.getId(),
          Optional.ofNullable(old.getFiles())
              .orElse(Collections.emptySet())
              .stream()
              .map(KigaEntityModel::getId)
              .collect(Collectors.toSet()));
    }

    toSave.setChild(childService.findOne(childId));
    if (toSave.getId() == null) {
      toSave.setDate(new Date());
    }
    ChildJournalEntry saved = childJournalEntryRepository.save(toSave);

    Set<Long> files =
        Optional.ofNullable(toSave.getFiles())
            .orElse(Collections.emptySet())
            .stream()
            .map(KigaEntityModel::getId)
            .collect(Collectors.toSet());
    // TODO best way to check that only files from user can be added.
    files.forEach(
        fileId -> {
          ChildJournalHasFile entry = new ChildJournalHasFile();
          entry.setEntry(
              new ChildJournalHasFileEntry(
                  childJournalEntryRepository.findOne(saved.getId()), s3FileFetcher.find(fileId)));
          journalHasFileRepository.save(entry);
        });

    return saved;
  }

  public void fetchFile(Integer childId, Integer fileId, HttpServletResponse response)
      throws IOException {
    S3File s3Image =
        journalHasFileRepository
            .findByEntry_Journal_Child_IdAndEntry_S3File_Id(childId.longValue(), fileId.longValue())
            .getEntry()
            .getS3File();
    DownloadedS3Image image = this.downloadUsingRequestService.download(s3Image);
    browserPusherService.pushFile(
        response, image.getStreamedImage(), s3Image.getName(), image.getContentType());
  }
}
