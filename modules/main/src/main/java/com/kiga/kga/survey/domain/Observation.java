package com.kiga.kga.survey.domain;

import com.kiga.db.KigaEntityModel;
import java.time.Instant;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder()
@AllArgsConstructor
public class Observation extends KigaEntityModel {
  @ManyToOne private ObservationSeries observationSeries;
  private Instant executedDateTime;
  private String comment;

  public Observation() {
    this.setClassName("Observation");
  }
}
