package com.kiga.kga.service;

import com.kiga.db.RepositoryPersister;
import com.kiga.kga.api.model.CountryViewModel;
import com.kiga.kga.api.model.DashboardViewModel;
import com.kiga.kga.api.model.LanguageViewModel;
import com.kiga.kga.api.model.YearViewModel;
import com.kiga.kga.domain.*;
import com.kiga.kga.mapper.CountryMapper;
import com.kiga.kga.mapper.LangaugeMapper;
import com.kiga.kga.repository.KgaEmployeeRepository;
import com.kiga.kga.repository.KgaKindergartenPermissionRepository;
import com.kiga.kga.repository.KindergartenRepository;
import com.kiga.kga.repository.LanguageRepository;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import com.kiga.security.spring.KigaAuthentication;
import com.kiga.shop.repository.CountryLocaleRepository;
import com.kiga.shop.repository.CountryRepository;
import com.kiga.spec.Persister;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.collections4.IterableUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Builder
@AllArgsConstructor
@Getter
public class KindergartenService {
  private final KindergartenRepository kindergartenRepository;
  private final CountryRepository countryRepository;
  private final CountryLocaleRepository countryLocaleRepository;
  private final LanguageRepository languageRepository;
  private final ChildService childService;
  private final Persister<KgaKindergarten> kindergartenPersister;
  private final Persister<KgaKindergartenPermission> permissionPersister;
  private final YearService yearService;
  private final KgaEmployeeRepository kgaEmployeeRepository;

  /** Constructor. */
  @Autowired
  public KindergartenService(
      KindergartenRepository kindergartenRepository,
      CountryRepository countryRepository,
      CountryLocaleRepository countryLocaleRepository,
      LanguageRepository languageRepository,
      ChildService childService,
      YearService yearService,
      KgaEmployeeRepository kgaEmployeeRepository,
      KgaKindergartenPermissionRepository permissionRepository) {
    this.kindergartenRepository = kindergartenRepository;
    this.countryRepository = countryRepository;
    this.countryLocaleRepository = countryLocaleRepository;
    this.languageRepository = languageRepository;
    this.childService = childService;
    this.kindergartenPersister = new RepositoryPersister<>(kindergartenRepository);
    this.permissionPersister = new RepositoryPersister<>(permissionRepository);
    this.yearService = yearService;
    this.kgaEmployeeRepository = kgaEmployeeRepository;
  }

  /** saves a Kindergarten. */
  @Transactional
  public KgaKindergarten save(KgaKindergarten kindergarten, Long countryId) {
    boolean creation = kindergarten.getId() == null;
    kindergarten.setCountry(countryRepository.findOne(countryId));
    KgaKindergarten saved = kindergartenPersister.persist(kindergarten);
    if (creation) {
      Member member =
          ((KigaAuthentication) SecurityContextHolder.getContext().getAuthentication())
              .getUser()
              .getMember();
      KgaEmployee kgaEmployee = new KgaEmployee();
      kgaEmployee.setFirstname(member.getFirstname());
      kgaEmployee.setLastname(member.getLastname());
      kgaEmployee.setEmail(member.getEmail());
      kgaEmployee.setMember(member);
      kgaEmployee.setKindergarten(kindergarten);
      kgaEmployee.setGender(Gender.OTHER);
      kgaEmployeeRepository.save(kgaEmployee);
      KgaKindergartenPermission permission = new KgaKindergartenPermission();
      permission.setKindergarten(kindergarten);
      permission.setEmployee(kgaEmployee);
      permission.setAllowRead(true);
      permission.setAllowWrite(true);
      permissionPersister.persist(permission);
    }
    return saved;
  }

  public KgaKindergarten findOne(long id) {
    return kindergartenRepository.findOne(id);
  }

  public List<KgaKindergarten> findAll() {
    return IterableUtils.toList(kindergartenRepository.findAll());
  }

  /**
   * list countries for address selection of the kindergarten.
   *
   * @param locale
   */
  public List<CountryViewModel> listCountries(String locale) {
    return countryLocaleRepository
        .findByLocaleOrderByCountry_SequenceAsc(Locale.valueOf(locale))
        .stream()
        .map(CountryMapper.INSTANCE::toViewModel)
        .collect(Collectors.toList());
  }

  /** list languages for language selection of a child. */
  public List<LanguageViewModel> listLanguages() {
    return languageRepository
        .findAll()
        .stream()
        .map(LangaugeMapper.INSTANCE::toViewModel)
        .collect(Collectors.toList());
  }

  public KgaChild saveChild(KgaChild kgaChild, Integer kindergartenId) {
    KgaKindergarten kindergarten = kindergartenRepository.findOne(kindergartenId.longValue());
    return childService.save(kgaChild, kindergarten);
  }

  public DashboardViewModel createDashboardViewModel(int kindergartenId) {
    KgaKindergarten kindergarten = findOne(kindergartenId);
    return new DashboardViewModel()
        .kindergartenId(kindergartenId)
        .kindergartenDescription(kindergarten.getName())
        .years(
            yearService
                .findByKindergarten(kindergartenId)
                .stream()
                .map(
                    year ->
                        new YearViewModel()
                            .id(year.getId().intValue())
                            .name(
                                new DateTime(year.getStartDate()).getYear()
                                    + "/"
                                    + new DateTime(year.getEndDate()).getYear()))
                .collect(Collectors.toList()));
  }
}
