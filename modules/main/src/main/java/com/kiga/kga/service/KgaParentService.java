package com.kiga.kga.service;

import com.kiga.db.RepositoryPersister;
import com.kiga.kga.api.model.ParentRoleEnum;
import com.kiga.kga.domain.KgaChild;
import com.kiga.kga.domain.KgaParent;
import com.kiga.kga.domain.KgaParentToChild;
import com.kiga.kga.domain.ParentRole;
import com.kiga.kga.repository.KgaParentRepository;
import com.kiga.spec.Persister;
import java.util.Collections;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Builder
public class KgaParentService {
  private final KgaParentRepository parentRepository;
  private final KgaGroupService kgaGroupService;
  private final ChildService childService;
  private final Persister<KgaParent> parentPerister;

  /** Constructor. */
  @Autowired
  public KgaParentService(
      KgaParentRepository parentRepository,
      KgaGroupService kgaGroupService,
      ChildService childService) {
    this.kgaGroupService = kgaGroupService;
    this.childService = childService;
    this.parentRepository = parentRepository;
    this.parentPerister = new RepositoryPersister<>(parentRepository);
  }

  public KgaParent saveOrCreate(KgaParent parent, Long childId, ParentRoleEnum role) {
    KgaParent returner = null;
    parent.setKindergarten(childService.findOne(childId).getKindergarten());
    returner = save(parent);
    childService.addParentToChild(childId, returner.getId(), ParentRole.valueOf(role.toString()));
    return returner;
  }

  public KgaParent save(KgaParent parent) {
    return parentPerister.persist(parent);
  }

  public KgaParent findOne(Long id) {
    return parentRepository.findOne(id);
  }

  public List<KgaParent> findByKindergarten(long id) {
    return parentRepository.findByKindergarten_Id(id);
  }

  /**
   * returns a list of all possible new parents (parents already connected to the child get
   * removed).
   */
  public List<KgaParent> findForChild(long childId) {
    KgaChild child = childService.findWithParentsById(childId);
    List<KgaParent> returner =
        parentRepository.findByKindergarten_Id(child.getKindergarten().getId());
    child.getParents().stream().map(KgaParentToChild::getParent).forEach(returner::remove);
    return returner;
  }

  public List<KgaParent> findForGroup(long groupId) {
    return Collections.emptyList();
  }
}
