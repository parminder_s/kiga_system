package com.kiga.kga.survey.domain;

import com.kiga.db.KigaEntityModel;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder()
@AllArgsConstructor
public class ObservationAnswer extends KigaEntityModel {
  @ManyToOne Observation observation;
  @ManyToOne Question question;
  private int answer;
  private String comment;

  public ObservationAnswer() {
    this.setClassName("ObservationAnswer");
  }
}
