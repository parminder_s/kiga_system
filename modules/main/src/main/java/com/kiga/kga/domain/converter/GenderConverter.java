package com.kiga.kga.domain.converter;

import com.kiga.kga.domain.Gender;

import javax.persistence.AttributeConverter;

public class GenderConverter implements AttributeConverter<Gender, Integer> {
  @Override
  public Integer convertToDatabaseColumn(Gender attribute) {
    switch (attribute) {
      case MALE:
        return 0;
      case FEMALE:
        return 1;
      case OTHER:
        return 2;
      default:
        throw new IllegalArgumentException("Not supported Gender: " + attribute);
    }
  }

  @Override
  public Gender convertToEntityAttribute(Integer dbData) {
    switch (dbData) {
      case 0:
        return Gender.MALE;
      case 1:
        return Gender.FEMALE;
      case 2:
        return Gender.OTHER;
      default:
        throw new IllegalArgumentException("Not supported Gender: " + dbData);
    }
  }
}
