package com.kiga.kga;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Region;
import com.kiga.kga.mapper.KindergartenMapper;
import com.kiga.kga.props.KgaS3Properties;
import com.kiga.kga.service.KgaS3Client;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KgaBaseConfig {
  @Bean
  public KindergartenMapper getKindergartenMapper() {
    return KindergartenMapper.INSTANCE;
  }

  @Bean
  public KgaS3Client getKgaS3Client(KgaS3Properties kgaS3Properties) {
    return new KgaS3Client(
        AmazonS3ClientBuilder.standard()
            .withCredentials(
                new AWSStaticCredentialsProvider(
                    new BasicAWSCredentials(
                        kgaS3Properties.getAccessKey(), kgaS3Properties.getSecretKey())))
            .withRegion(String.valueOf(Region.EU_Frankfurt))
            .build(),
        kgaS3Properties.getBucketName());
  }
}
