package com.kiga.kga.domain;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "ChildJournal_has_File")
public class ChildJournalHasFile {
  @EmbeddedId private ChildJournalHasFileEntry entry;
}
