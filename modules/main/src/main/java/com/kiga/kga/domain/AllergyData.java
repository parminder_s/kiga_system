package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.kga.domain.converter.AllergyConverter;
import javax.persistence.Convert;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = "child")
public class AllergyData<T extends KgaChildData> extends KigaEntityModel {
  @ManyToOne private T child;

  @Convert(converter = AllergyConverter.class)
  private AllergyEnum allergy;
}
