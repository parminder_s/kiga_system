package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.kga.domain.converter.VaccinationConverter;
import javax.persistence.Convert;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = "child")
public class VaccinationData<T extends KgaChildData> extends KigaEntityModel {
  @ManyToOne() private T child;

  @Convert(converter = VaccinationConverter.class)
  private VaccinationEnum vaccination;
}
