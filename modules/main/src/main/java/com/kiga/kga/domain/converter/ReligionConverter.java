package com.kiga.kga.domain.converter;

import com.kiga.kga.domain.Religion;
import javax.persistence.AttributeConverter;

public class ReligionConverter implements AttributeConverter<Religion, String> {
  @Override
  public String convertToDatabaseColumn(Religion attribute) {
    switch (attribute) {
      case ATHEIST:
        return "atheist";
      case CATHOLIC:
        return "catholic";
      case PROTESTANT:
        return "protestant";
      case ISLAM:
        return "islam";
      case OTHER:
        return "other";
      default:
        throw new IllegalArgumentException("Invalid religion: " + attribute);
    }
  }

  @Override
  public Religion convertToEntityAttribute(String dbData) {
    switch (dbData) {
      case "atheist":
        return Religion.ATHEIST;
      case "catholic":
        return Religion.CATHOLIC;
      case "protestant":
        return Religion.PROTESTANT;
      case "islam":
        return Religion.ISLAM;
      case "other":
        return Religion.OTHER;
      default:
        throw new IllegalArgumentException("Invalid religion: " + dbData);
    }
  }
}
