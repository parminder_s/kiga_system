package com.kiga.kga.domain;

import javax.persistence.Entity;

@Entity
public class ChildInKgaGroup extends ChildInKgaGroupData<KgaChild> {
  public ChildInKgaGroup() {
    this.setClassName("ChildInKgaGroup");
  }
}
