package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder(toBuilder = true)
@AllArgsConstructor
public class KgaGroup extends KigaEntityModel {
  public KgaGroup() {
    this.setClassName("Group");
  }

  String name;
  String email;
  String room;
  String phoneNumber;
  String notes;

  @ManyToOne KgaYear year;

  @OneToMany(mappedBy = "group")
  List<KgaGroupPermission> groupPermissions;

  @OneToMany(mappedBy = "group")
  List<ChildInKgaGroup> childInKgaGroup;
}
