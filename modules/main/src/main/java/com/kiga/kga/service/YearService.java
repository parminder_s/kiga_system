package com.kiga.kga.service;

import com.kiga.db.RepositoryPersister;
import com.kiga.kga.domain.KgaYear;
import com.kiga.kga.repository.KgaYearRepository;
import com.kiga.kga.repository.KindergartenRepository;
import com.kiga.spec.Persister;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Builder
@AllArgsConstructor
public class YearService {
  private final KgaYearRepository kgaYearRepository;
  private final KindergartenRepository kindergartenRepository;
  private final KgaGroupService kgaGroupService;
  private final ChildService childService;
  private final Persister<KgaYear> yearPersister;

  /** Constructor. */
  @Autowired
  public YearService(
      KgaYearRepository kgaYearRepository,
      KindergartenRepository kindergartenRepository,
      KgaGroupService kgaGroupService,
      ChildService childService) {
    this.kgaYearRepository = kgaYearRepository;
    this.kindergartenRepository = kindergartenRepository;
    this.kgaGroupService = kgaGroupService;
    this.childService = childService;
    this.yearPersister = new RepositoryPersister<>(kgaYearRepository);
  }

  public Long save(KgaYear year, Long kindergartenId) {
    year.setKindergarten(kindergartenRepository.findOne(kindergartenId));
    return yearPersister.persist(year).getId();
  }

  public KgaYear findOne(long id) {
    return kgaYearRepository.findOne(id);
  }

  public List<KgaYear> findByKindergarten(long id) {
    return kgaYearRepository.findByKindergarten_Id(id);
  }

  /** archives a year (all groups, and ChildInGroupTable. */
  public void archive(long yearId) {
    KgaYear year = kgaYearRepository.findWithGroupsById(yearId);
    if (year.isArchived()) {
      throw new IllegalArgumentException("Can't archive an already archived year!");
    }
    year.setArchived(true);
    year.setArchivationDate(new Date());
    kgaYearRepository.save(year);
  }

  public void importFromOtherYear(long yearId, List<Long> groupIds) {
    KgaYear year = this.findOne(yearId);
    kgaGroupService.importToYear(year, groupIds);
  }
}
