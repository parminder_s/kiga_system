package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaParentToChild;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface KgaParentToChildRepository extends CrudRepository<KgaParentToChild, Long> {
  KgaParentToChild findByParent_IdAndChild_Id(Long parentId, Long childId);

  List<KgaParentToChild> findByParent_Id(Long parentId);
}
