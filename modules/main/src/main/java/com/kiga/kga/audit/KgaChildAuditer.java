package com.kiga.kga.audit;

import com.kiga.db.Auditer;
import com.kiga.kga.domain.KgaChild;
import com.kiga.kga.domain.KgaChild_Versions;
import com.kiga.kga.mapper.ChildMapper;
import com.kiga.kga.repository.KgaChildArchiveRepository;
import javax.persistence.PreUpdate;

public class KgaChildAuditer
    extends Auditer<KgaChild, KgaChild_Versions, KgaChildArchiveRepository> {
  public KgaChildAuditer() {
    super(KgaChildArchiveRepository.class);
  }

  @Override
  protected KgaChild_Versions map(KgaChild entity) {
    return ChildMapper.INSTANCE.childToChildArchive(entity);
  }

  @PreUpdate
  public void postUpdate(KgaChild o) {
    super.peristToArchive(o);
  }
}
