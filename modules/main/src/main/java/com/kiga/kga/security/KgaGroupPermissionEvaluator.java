package com.kiga.kga.security;

import com.kiga.kga.domain.KgaGroupPermission;
import com.kiga.kga.repository.KgaGroupDataPerYearRepository;
import com.kiga.kga.repository.KgaGroupPermissionRepository;
import com.kiga.security.spring.AbstractKigaPermissionEvaluator;
import com.kiga.security.spring.KigaAuthentication;
import com.kiga.security.spring.KigaPermissionEvaluator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KgaGroupPermissionEvaluator extends AbstractKigaPermissionEvaluator {

  private final KgaGroupPermissionRepository kgaGroupPermissionRepository;
  private final KgaYearPermissionEvaluator yearPermissionEvaluator;
  private final KgaGroupDataPerYearRepository kgaGroupDataPerYearRepository;

  @Autowired
  public KgaGroupPermissionEvaluator(
      KgaGroupPermissionRepository kgaGroupPermissionRepository,
      KigaPermissionEvaluator kigaPermissionEvaluator,
      KgaYearPermissionEvaluator yearPermissionEvaluator,
      KgaGroupDataPerYearRepository kgaGroupDataPerYearRepository) {
    this.kgaGroupPermissionRepository = kgaGroupPermissionRepository;
    this.yearPermissionEvaluator = yearPermissionEvaluator;
    this.kgaGroupDataPerYearRepository = kgaGroupDataPerYearRepository;
    kigaPermissionEvaluator.addEvaluator("KgaGroup", this);
  }

  @Override
  public boolean hasPermission(
      KigaAuthentication authentication, Long id, String targetType, String permission) {
    KgaGroupPermission kgaGroupPermission =
        kgaGroupPermissionRepository.findByEmployee_MemberAndGroup_Id(
            authentication.getDetails().getMember(), id);
    if (kgaGroupPermission == null) {
      return yearPermissionEvaluator.hasPermission(
          authentication,
          kgaGroupDataPerYearRepository.findOne(id).getYear().getId(),
          targetType,
          permission);
    } else {
      switch (KgaPermission.valueOf(permission)) {
        case READ:
          return kgaGroupPermission.isAllowRead();
        case WRITE:
          return kgaGroupPermission.isAllowWrite();
        default:
          return false;
      }
    }
  }
}
