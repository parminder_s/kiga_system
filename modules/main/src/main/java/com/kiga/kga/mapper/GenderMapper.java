package com.kiga.kga.mapper;

import com.kiga.kga.api.model.GenderEnum;
import com.kiga.kga.domain.Gender;

public class GenderMapper {

  /**
   * to java gender.
   */
  public Gender asGender(GenderEnum genderEnum) {
    switch (genderEnum) {
      case M:
        return Gender.MALE;
      case F:
        return Gender.FEMALE;
      case O:
        return Gender.OTHER;
      default: throw new IllegalArgumentException("Unmapped genderEnum: " + genderEnum);
    }
  }

  /**
   * to swagger gender.
   */
  public GenderEnum asGenderEnum(Gender gender) {
    switch (gender) {
      case MALE:
        return GenderEnum.M;
      case FEMALE:
        return GenderEnum.F;
      case OTHER:
        return GenderEnum.O;
      default: throw new IllegalArgumentException("Unmapped genderEnum: " + gender);
    }
  }
}
