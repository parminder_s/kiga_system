package com.kiga.kga.mapper;

import com.kiga.kga.api.model.CountryViewModel;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryLocale;
import com.kiga.util.mapper.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapStructConfig.class)
public interface CountryMapper {
  CountryMapper INSTANCE = Mappers.getMapper(CountryMapper.class);

  @Mapping(target = "code", source = "country.code")
  @Mapping(target = "id", source = "country.id")
  CountryViewModel toViewModel(CountryLocale country);

  /** maps a id to an empty country object. */
  default Country countryToId(Integer id) {
    if (id == null) {
      return null;
    }
    Country returner = new Country();
    returner.setId(id.longValue());
    return returner;
  }
}
