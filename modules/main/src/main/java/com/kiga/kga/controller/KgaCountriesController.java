package com.kiga.kga.controller;

import com.kiga.kga.api.controller.CountryApi;
import com.kiga.kga.api.model.CountryViewModel;
import com.kiga.kga.service.KindergartenService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KgaCountriesController implements CountryApi {

  private final KindergartenService kindergartenService;

  @Autowired
  public KgaCountriesController(KindergartenService kindergartenService) {
    this.kindergartenService = kindergartenService;
  }

  @Override
  public ResponseEntity<List<CountryViewModel>> listCountries(@PathVariable String locale) {
    return ResponseEntity.ok(kindergartenService.listCountries(locale));
  }
}
