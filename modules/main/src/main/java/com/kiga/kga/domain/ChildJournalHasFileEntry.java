package com.kiga.kga.domain;

import com.kiga.s3.domain.S3File;
import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ChildJournalHasFileEntry implements Serializable {
  @ManyToOne
  @JoinColumn(name = "journalId")
  private ChildJournalEntry journal;

  @ManyToOne
  @JoinColumn(name = "s3FileId")
  private S3File s3File;
}
