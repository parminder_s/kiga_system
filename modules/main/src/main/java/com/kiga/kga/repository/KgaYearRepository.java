package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaYear;
import java.util.List;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

public interface KgaYearRepository extends CrudRepository<KgaYear, Long> {
  List<KgaYear> findByKindergarten_Id(long id);

  @EntityGraph(
      attributePaths = {"groups"},
      type = EntityGraph.EntityGraphType.FETCH)
  KgaYear findWithGroupsById(long id);
}
