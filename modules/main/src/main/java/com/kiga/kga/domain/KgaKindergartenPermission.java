package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.security.domain.Member;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class KgaKindergartenPermission extends KigaEntityModel {
  private boolean allowRead;
  private boolean allowWrite;

  @ManyToOne
  private KgaEmployee employee;

  @ManyToOne
  private KgaKindergarten kindergarten;

  public KgaKindergartenPermission() {
    this.setClassName("KgaKindergartenPermission");
  }
}
