package com.kiga.kga.security;

public enum KgaPermission {
  READ,
  WRITE
}
