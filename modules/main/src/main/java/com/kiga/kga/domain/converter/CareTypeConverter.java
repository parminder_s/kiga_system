package com.kiga.kga.domain.converter;

import com.kiga.kga.domain.CareType;
import javax.persistence.AttributeConverter;

public class CareTypeConverter implements AttributeConverter<CareType, Long> {
  @Override
  public Long convertToDatabaseColumn(CareType attribute) {
    switch (attribute) {
      case MORNING:
        return 1L;
      case AFTERNOON:
        return 2L;
      case ALLDAY:
        return 3L;
      case FLEXIBLE:
        return 4L;
      case OTHER:
        return 5L;
      default:
        throw new IllegalArgumentException("Wrong caretype: " + attribute);
    }
  }

  @Override
  public CareType convertToEntityAttribute(Long dbData) {
    switch (dbData.intValue()) {
      case 1:
        return CareType.MORNING;
      case 2:
        return CareType.AFTERNOON;
      case 3:
        return CareType.ALLDAY;
      case 4:
        return CareType.FLEXIBLE;
      case 5:
        return CareType.OTHER;
      default:
        throw new IllegalArgumentException("Wrong caretype: " + dbData);
    }
  }
}
