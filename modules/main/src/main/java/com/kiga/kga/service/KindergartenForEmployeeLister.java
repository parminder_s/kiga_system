package com.kiga.kga.service;

import com.kiga.kga.domain.KgaKindergarten;
import com.kiga.kga.domain.KgaKindergartenPermission;
import com.kiga.kga.repository.KgaKindergartenPermissionRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KindergartenForEmployeeLister {
  private final KgaKindergartenPermissionRepository repository;
  private final KgaPermissionService kgaPermissionService;

  @Autowired
  public KindergartenForEmployeeLister(
      KgaKindergartenPermissionRepository repository, KgaPermissionService kgaPermissionService) {
    this.repository = repository;
    this.kgaPermissionService = kgaPermissionService;
  }

  public List<KgaKindergarten> list() {
    return repository
        .findByEmployee(kgaPermissionService.getCurrentEmployee())
        .stream()
        .map(KgaKindergartenPermission::getKindergarten)
        .collect(Collectors.toList());
  }
}
