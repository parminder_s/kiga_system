package com.kiga.kga.survey.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.kga.domain.KgaChild;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder()
@AllArgsConstructor
public class ObservationSeries extends KigaEntityModel {
  @ManyToOne Survey survey;

  @ManyToOne KgaChild kgaChild;

  public ObservationSeries() {
    this.setClassName("ObservationSerie");
  }
}
