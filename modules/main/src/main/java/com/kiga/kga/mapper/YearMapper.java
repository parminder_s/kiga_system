package com.kiga.kga.mapper;

import com.kiga.kga.api.model.YearModel;
import com.kiga.kga.domain.KgaYear;
import com.kiga.util.mapper.DateMapper;
import com.kiga.util.mapper.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapStructConfig.class, uses = DateMapper.class)
public interface YearMapper {
  YearMapper INSTANCE = Mappers.getMapper(YearMapper.class);

  @Mapping(target = "className", constant = "KgaYear")
  @Mapping(target = "created", ignore = true)
  @Mapping(target = "lastEdited", ignore = true)
  @Mapping(target = "groups", ignore = true) // todo check if we should load it
  @Mapping(target = "kindergarten", ignore = true) // todo check if we should load it
  @Mapping(target = "archivationDate", ignore = true) // todo check if we should load it
  KgaYear yearModelToYear(YearModel yearModel);

  @Mapping(source = "kindergarten.id", target = "kindergartenId")
  @Mapping(source = "archived", target = "archived")
  YearModel yearToYearModel(KgaYear year);
}
