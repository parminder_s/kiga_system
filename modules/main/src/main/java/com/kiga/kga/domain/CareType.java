package com.kiga.kga.domain;

public enum CareType {
  MORNING,
  AFTERNOON,
  ALLDAY,
  FLEXIBLE,
  OTHER
}
