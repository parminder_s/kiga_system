package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaChild;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface KgaChildRepository extends CrudRepository<KgaChild, Long> {
  @EntityGraph(attributePaths = {"parents"},
    type = EntityGraph.EntityGraphType.FETCH)
  KgaChild findWithParentsById(long id);

  List<KgaChild> findByKindergarten_Id(long kindergartenId);
}
