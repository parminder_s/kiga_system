package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.kga.domain.converter.ParentRoleConverter;
import javax.persistence.Convert;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@MappedSuperclass
@Data
@EqualsAndHashCode(callSuper = true)
@Builder(toBuilder = true)
@AllArgsConstructor
public class KgaParentToChildData<C extends KgaChildData> extends KigaEntityModel {
  @ManyToOne private KgaParent parent;

  @ManyToOne private C child;

  @Convert(converter = ParentRoleConverter.class)
  private ParentRole role;

  public KgaParentToChildData() {
    this.setClassName("KgaParentToChildData");
  }
}
