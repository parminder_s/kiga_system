package com.kiga.kga.mapper;

import com.kiga.kga.api.model.KgaGroupViewModel;
import com.kiga.kga.domain.KgaGroup;
import com.kiga.util.mapper.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapStructConfig.class)
public interface KgaGroupViewModelMapper {
  KgaGroupViewModelMapper INSTANCE = Mappers.getMapper(KgaGroupViewModelMapper.class);

  KgaGroupViewModel kgaGroupToViewModel(KgaGroup kgaGroupDataPerYear);
}
