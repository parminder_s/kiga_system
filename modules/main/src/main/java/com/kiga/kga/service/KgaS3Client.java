package com.kiga.kga.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import java.io.File;

public class KgaS3Client {
  private final AmazonS3 s3;
  private final String bucketName;

  public KgaS3Client(AmazonS3 s3, String bucketName) {
    this.s3 = s3;
    this.bucketName = bucketName;
  }

  public void upload(File file, String key) {
    this.s3.putObject(new PutObjectRequest(this.bucketName, key, file));
  }

  public S3Object download(String key) {
    return this.s3.getObject(this.bucketName, key);
  }
}
