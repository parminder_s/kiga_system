package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaKindergarten;
import org.springframework.data.repository.CrudRepository;

public interface KindergartenRepository extends CrudRepository<KgaKindergarten, Long> {
}
