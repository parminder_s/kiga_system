package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class KgaGroupPermission extends KigaEntityModel implements KgaPermissions {
  private boolean allowRead;
  private boolean allowWrite;

  @ManyToOne private KgaEmployee employee;

  @ManyToOne private KgaGroup group;

  public KgaGroupPermission() {
    this.setClassName("KgaGroupPermission");
  }
}
