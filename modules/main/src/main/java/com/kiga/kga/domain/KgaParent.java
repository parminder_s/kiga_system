package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
public class KgaParent extends KigaEntityModel {

  private String firstname;
  private String lastname;
  private String email;
  private String phoneNumber;
  private String title;

  private boolean legalGuardian;
  private boolean pickupAuthorized;
  private boolean emergencyAuthorized;

  @ManyToOne private KgaKindergarten kindergarten;
  private EmploymentContract employment;

  public KgaParent() {
    this.setClassName("KgaParent");
  }
}
