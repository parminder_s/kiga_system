package com.kiga.kga.security;

import com.kiga.kga.service.EmployeeService;
import com.kiga.security.spring.AbstractKigaPermissionEvaluator;
import com.kiga.security.spring.KigaAuthentication;
import com.kiga.security.spring.KigaPermissionEvaluator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KgaEmployeePermissionEvaluator extends AbstractKigaPermissionEvaluator {
  private final EmployeeService employeeService;
  private final KgaKindergartenPermissionEvaluator kgaKindergartenPermissionEvaluator;

  @Autowired
  public KgaEmployeePermissionEvaluator(
      EmployeeService employeeService,
      KigaPermissionEvaluator kigaPermissionEvaluator,
      KgaKindergartenPermissionEvaluator kgaKindergartenPermissionEvaluator) {
    this.employeeService = employeeService;
    this.kgaKindergartenPermissionEvaluator = kgaKindergartenPermissionEvaluator;
    kigaPermissionEvaluator.addEvaluator("KgaEmployee", this);
  }

  @Override
  public boolean hasPermission(
      KigaAuthentication authentication, Long id, String targetType, String permission) {
    return kgaKindergartenPermissionEvaluator.hasPermission(
        authentication,
        employeeService.findOne(id).getKindergarten().getId(),
        targetType,
        permission);
  }
}
