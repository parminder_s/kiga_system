package com.kiga.kga.controller;

import com.kiga.kga.api.controller.ChildApi;
import com.kiga.kga.api.model.AddChildToGroup;
import com.kiga.kga.api.model.AddParentToChild;
import com.kiga.kga.api.model.ChildModel;
import com.kiga.kga.api.model.IdRequest;
import com.kiga.kga.api.model.KgaParentModel;
import com.kiga.kga.domain.ParentRole;
import com.kiga.kga.mapper.ChildMapper;
import com.kiga.kga.mapper.KgaParentToChildMapper;
import com.kiga.kga.service.ChildService;
import com.kiga.kga.service.KgaGroupService;
import com.kiga.kga.service.KindergartenService;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChildController implements ChildApi {
  private final ChildService childService;
  private final KindergartenService kindergartenService;
  private final KgaGroupService groupService;

  @Autowired
  public ChildController(
      ChildService childService,
      KindergartenService kindergartenService,
      KgaGroupService groupService) {
    this.childService = childService;
    this.kindergartenService = kindergartenService;
    this.groupService = groupService;
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaYear', 'READ')")
  public ResponseEntity<List<ChildModel>> listChildByKgaYear(@RequestBody IdRequest idRequest) {
    List<ChildModel> returner =
        childService
            .findByYear(idRequest.getId())
            .stream()
            .map(ChildMapper.INSTANCE::childToChildModel)
            .collect(Collectors.toList());
    return ResponseEntity.ok(returner);
  }

  @Override
  @PreAuthorize("hasPermission(#addChildToGroup.childId, 'KgaChild', 'WRITE')")
  public ResponseEntity<String> addChildToGroup(@RequestBody AddChildToGroup addChildToGroup) {
    groupService.putChildInGroup(
        childService.findOne(addChildToGroup.getChildId()), addChildToGroup.getGroupId());
    return ResponseEntity.ok("Added child to group");
  }

  @Override
  public ResponseEntity<String> deleteOneChild(@RequestBody IdRequest idRequest) {
    childService.deleteOne(idRequest.getId());
    return ResponseEntity.ok("Delted a child");
  }

  @Override
  @PreAuthorize("hasPermission(#addChildToGroupRequest.childId, 'KgaChild', 'WRITE')")
  public ResponseEntity<String> addParent(@RequestBody AddParentToChild addChildToGroupRequest) {
    childService.addParentToChild(
        addChildToGroupRequest.getChildId().longValue(),
        addChildToGroupRequest.getParentId().longValue(),
        ParentRole.valueOf(addChildToGroupRequest.getRole().toString()));
    return ResponseEntity.ok("Added parent to child");
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaChild', 'READ')")
  public ResponseEntity<ChildModel> findOneChild(@RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        ChildMapper.INSTANCE.childToChildModel(
            childService.findOne(idRequest.getId().longValue())));
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaGroup', 'READ')")
  public ResponseEntity<List<ChildModel>> listChildByKgaGroup(@RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        childService
            .findByGroup(idRequest.getId())
            .stream()
            .map(ChildMapper.INSTANCE::childToChildModel)
            .collect(Collectors.toList()));
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaKindergarten', 'READ')")
  public ResponseEntity<List<ChildModel>> listChildByKindergarten(
      @RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        childService
            .findByKindergarten(idRequest.getId())
            .stream()
            .map(ChildMapper.INSTANCE::childToChildModel)
            .collect(Collectors.toList()));
  }

  @Override // todo think about permission!
  public ResponseEntity<String> removeParent(@RequestBody IdRequest idRequest) {
    childService.removeParentFromChild(idRequest.getId().longValue());
    return ResponseEntity.ok("Removed parent from child");
  }

  @Override
  @PreAuthorize(
      "hasPermission(#childModel.kindergartenId, 'KgaKindergarten', 'WRITE') or hasPermission(#childModel.id, 'KgaChild', 'WRITE')")
  public ResponseEntity<BigDecimal> saveChild(@RequestBody ChildModel childModel) {
    return ResponseEntity.ok(
        new BigDecimal(
            kindergartenService
                .saveChild(
                    ChildMapper.INSTANCE.childModelToChild(childModel),
                    childModel.getKindergartenId())
                .getId()
                .toString()));
  }

  @Override
  @PreAuthorize("hasPermission(#childModel.id, 'KgaChild', 'WRITE')")
  public ResponseEntity<String> saveParentToChild(@RequestBody KgaParentModel childModel) {
    childService.saveParentRole(KgaParentToChildMapper.INSTANCE.modelToEntity(childModel));
    return ResponseEntity.ok("Saved parent to child");
  }
}
