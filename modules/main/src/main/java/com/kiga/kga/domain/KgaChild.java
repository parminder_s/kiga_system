package com.kiga.kga.domain;

import com.kiga.kga.audit.KgaChildAuditer;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.OneToMany;
import javax.persistence.PreUpdate;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@EntityListeners(KgaChildAuditer.class)
public class KgaChild
    extends KgaChildData<Vaccination, Disease, Allergy, ChildInKgaGroup, KgaParentToChild> {

  @OneToMany(mappedBy = "child")
  private List<KgaChild_Versions> archives;

  public KgaChild() {
    this.setClassName("KgaChild");
  }

  @PreUpdate
  public void prePersist() {
    super.prePersist();
  }
}
