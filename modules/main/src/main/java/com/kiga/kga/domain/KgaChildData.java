package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.kga.domain.converter.CareTypeConverter;
import com.kiga.kga.domain.converter.GenderConverter;
import com.kiga.kga.domain.converter.ReligionConverter;
import com.kiga.shop.domain.Country;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class KgaChildData<
        V extends VaccinationData,
        D extends DiseaseData,
        A extends AllergyData,
        CIG extends ChildInKgaGroupData,
        P extends KgaParentToChildData>
    extends KigaEntityModel {

  private String notes;
  private String firstname;
  private String lastname;
  private String socialSecurityNumber;

  private Long numberSiblings;

  private Boolean lactoseIntolerant;
  private Boolean withMeal;
  private boolean deleted;

  private Date birthDate;
  private Date entryDate;
  private Date seperationDate;

  @Convert(converter = ReligionConverter.class)
  private Religion religion;

  @ManyToOne private KgaKindergarten kindergarten;

  @OneToMany(mappedBy = "child")
  private List<CIG> childInKgaGroups;

  @OneToMany(mappedBy = "child")
  private List<P> parents;

  @OneToMany(mappedBy = "child", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<V> vaccinations;

  @OneToMany(mappedBy = "child", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<D> diseases;

  @OneToMany(mappedBy = "child", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<A> allergies;

  @ManyToOne private Language nativeLanguage;

  @ManyToMany
  @JoinTable(
      name = "Child_has_Language",
      joinColumns = @JoinColumn(name = "ChildId"),
      inverseJoinColumns = @JoinColumn(name = "LanguageId"))
  private Set<Language> secondLanguage;

  @Convert(converter = GenderConverter.class)
  private Gender gender;

  @ManyToOne private Country citizenship;

  @Convert(converter = CareTypeConverter.class)
  CareType careType;

  private String careTypeNote;
  private String religionNote;
  private String nativeLanguageNote;
  private String allergiesNote;
  private String diseasesNote;
  private String vaccinationsNote;
  private String secondLanguageNote;

  public void prePersist() {
    Optional.ofNullable(this.getVaccinations())
        .ifPresent(vac -> vac.forEach(vaccination -> vaccination.setChild(this)));
    Optional.ofNullable(this.getDiseases())
        .ifPresent(vac -> vac.forEach(disease -> disease.setChild(this)));
    Optional.ofNullable(this.getAllergies())
        .ifPresent(vac -> vac.forEach(allergies -> allergies.setChild(this)));
  }
}
