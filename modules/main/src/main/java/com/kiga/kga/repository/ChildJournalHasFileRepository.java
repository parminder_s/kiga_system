package com.kiga.kga.repository;

import com.kiga.kga.domain.ChildJournalHasFile;
import com.kiga.kga.domain.ChildJournalHasFileEntry;
import java.util.Set;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface ChildJournalHasFileRepository
    extends CrudRepository<ChildJournalHasFile, ChildJournalHasFileEntry> {
  @Transactional
  void deleteByEntry_Journal_IdAndEntry_S3File_IdIn(Long journalId, Set<Long> s3FileId);

  ChildJournalHasFile findByEntry_Journal_Child_IdAndEntry_S3File_Id(Long childId, Long s3FileId);
}
