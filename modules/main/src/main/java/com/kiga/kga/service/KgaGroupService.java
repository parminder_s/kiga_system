package com.kiga.kga.service;

import com.kiga.kga.api.model.UpdateChildInGroup;
import com.kiga.kga.api.model.UpdateChildInGroupEntry;
import com.kiga.kga.domain.ChildInKgaGroup;
import com.kiga.kga.domain.KgaChild;
import com.kiga.kga.domain.KgaGroup;
import com.kiga.kga.domain.KgaYear;
import com.kiga.kga.repository.ChildInKgaGroupRepository;
import com.kiga.kga.repository.KgaGroupDataPerYearRepository;
import com.kiga.kga.repository.KgaYearRepository;
import com.kiga.mail.Mailer;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KgaGroupService {
  private final KgaGroupDataPerYearRepository kgaGroupDataPerYearRepository;
  private final ChildInKgaGroupRepository childInKgaGroupRepository;
  private final KgaYearRepository kgaYearRepository;
  private final Mailer mailer;
  private final ChildService childService;

  /** Constructor. */
  @Autowired
  public KgaGroupService(
      KgaGroupDataPerYearRepository kgaGroupDataPerYearRepository,
      ChildInKgaGroupRepository childInKgaGroupRepository,
      KgaYearRepository kgaYearRepository,
      Mailer mailer,
      ChildService childService) {
    this.kgaGroupDataPerYearRepository = kgaGroupDataPerYearRepository;
    this.childInKgaGroupRepository = childInKgaGroupRepository;
    this.kgaYearRepository = kgaYearRepository;
    this.mailer = mailer;
    this.childService = childService;
  }

  /** Saves a GroupData. If a new Group is created, also a KgaGroup entity gets created. */
  public Long save(KgaGroup kgaGroup, Integer yearId) {
    kgaGroup.setYear(kgaYearRepository.findOne(yearId.longValue()));
    return kgaGroupDataPerYearRepository.save(kgaGroup).getId();
  }

  public KgaGroup findOne(Long id) {
    return kgaGroupDataPerYearRepository.findOne(id);
  }

  public List<KgaGroup> findByYearId(long id) {
    return kgaGroupDataPerYearRepository.findByYear_Id(id);
  }

  /** sends email to set admin. */
  public void setAdmin(long groupId, String email) {
    mailer.send(
        "kga",
        "admin",
        Long.toString(groupId),
        mail ->
            mail.withFrom("kga@kigaportal.com")
                .withBody("you are selected as groupAdmin")
                .withSubject("kiga admin")
                .withReplyTo("peter_kohout@gmx.at")
                .withTo(email));
  }

  /** sends email to set second admin. */
  public void setSecondAdmin(long groupId, String email) {
    mailer.send(
        "kga",
        "secondAdmin",
        Long.toString(groupId),
        mail ->
            mail.withFrom("kga@kigaportal.com")
                .withBody("you are selected as second groupAdmin")
                .withSubject("kiga second admin")
                .withReplyTo("peter_kohout@gmx.at")
                .withTo(email));
  }

  /** imports groups and it's children to a new year. */
  public void importToYear(KgaYear year, List<Long> groupIds) {
    List<KgaGroup> groups =
        kgaGroupDataPerYearRepository
            .findByIdIn(groupIds)
            .stream()
            .map(x -> copyToNewKgaGroupDataPerYear(x, year))
            .collect(Collectors.toList());
    kgaGroupDataPerYearRepository.save(groups);
    /*
    groups.forEach(x -> x.getChildInKgaGroup()
      .forEach(a -> childService.save(a.getChild(), x.getId())));
    */
  }

  /** Copies a KgaGroup. */
  public KgaGroup copyToNewKgaGroupDataPerYear(KgaGroup old, KgaYear year) {
    KgaGroup returner =
        old.toBuilder()
            .year(year)
            .childInKgaGroup(copyToNewChildInKgaGroup(old.getChildInKgaGroup()))
            .build();
    returner.setClassName("KgaGroup");
    returner.setId(null);
    return returner;
  }

  /** Copies a ChildInKgaGroup. */
  public List<ChildInKgaGroup> copyToNewChildInKgaGroup(List<ChildInKgaGroup> old) {
    throw new IllegalArgumentException("Not Implemented");
    /*
    return old.stream()
        .map(x -> x.toBuilder().build())
        .peek(x -> x.setId(null))
        .peek(x -> x.setClassName("ChildInKgaGroup"))
        .collect(Collectors.toList());*/
  }

  /**
   * puts a child in a group. If the child is in a group in the target year, the onld entry gets
   * updated, else a new entry is created. This ensures that a child is in one year only in one
   * group.
   */
  public void putChildInGroup(KgaChild kgaChild, Integer groupId) {
    KgaGroup group = this.findOne(groupId.longValue());
    ChildInKgaGroup childInKgaGroup =
        Optional.ofNullable(
                childInKgaGroupRepository.findByChildAndGroup_Year(kgaChild, group.getYear()))
            .orElse(new ChildInKgaGroup());
    childInKgaGroup.setGroup(group);
    childInKgaGroup.setChild(kgaChild);
    childInKgaGroupRepository.save(childInKgaGroup);
  }

  public void updateGroups(UpdateChildInGroup updateChildInGroup) {
    KgaYear year = this.kgaYearRepository.findOne(updateChildInGroup.getYearId().longValue());
    for (UpdateChildInGroupEntry childInGroup : updateChildInGroup.getData()) {
      KgaChild child = this.childService.findOne(childInGroup.getChildId());
      KgaGroup group = this.findOne(childInGroup.getGroupId().longValue());
      ChildInKgaGroup childInKgaGroup =
          childInKgaGroupRepository.findByChildAndGroup_Year(child, year);
      if (childInKgaGroup == null) {
        childInKgaGroup = new ChildInKgaGroup();
        childInKgaGroup.setChild(child);
      }
      childInKgaGroup.setGroup(group);
      childInKgaGroupRepository.save(childInKgaGroup);
    }
  }
}
