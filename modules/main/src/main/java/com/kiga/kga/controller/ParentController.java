package com.kiga.kga.controller;

import com.kiga.kga.api.controller.ParentApi;
import com.kiga.kga.api.model.IdRequest;
import com.kiga.kga.api.model.KgaParentModel;
import com.kiga.kga.api.model.SaveKgaParentRequest;
import com.kiga.kga.mapper.KgaParentMapper;
import com.kiga.kga.service.KgaParentService;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ParentController implements ParentApi {

  private final KgaParentService kgaParentService;

  @Autowired
  public ParentController(KgaParentService kgaParentService) {
    this.kgaParentService = kgaParentService;
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaParent', 'READ')")
  public ResponseEntity<KgaParentModel> findOneKgaParent(@RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        KgaParentMapper.INSTANCE.kgaParentToKgaParentModel(
            kgaParentService.findOne(idRequest.getId().longValue())));
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaChild', 'READ')")
  public ResponseEntity<List<KgaParentModel>> listForChild(@RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        kgaParentService
            .findForChild(idRequest.getId().longValue())
            .stream()
            .map(KgaParentMapper.INSTANCE::kgaParentToKgaParentModel)
            .collect(Collectors.toList()));
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaKindergarten', 'READ')")
  public ResponseEntity<List<KgaParentModel>> listKgaParentByKindergarten(
      @RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        kgaParentService
            .findByKindergarten(idRequest.getId().longValue())
            .stream()
            .map(KgaParentMapper.INSTANCE::kgaParentToKgaParentModel)
            .collect(Collectors.toList()));
  }

  @Override
  @PreAuthorize("hasPermission(#parentModel.childId, 'KgaChild', 'WRITE')")
  public ResponseEntity<BigDecimal> saveKgaParent(@RequestBody SaveKgaParentRequest parentModel) {
    return ResponseEntity.ok(
        new BigDecimal(
            kgaParentService
                .saveOrCreate(
                    KgaParentMapper.INSTANCE.kgaParentModelToKgaParent(parentModel.getParent()),
                    parentModel.getChildId().longValue(),
                    parentModel.getParent().getRole())
                .getId()
                .toString()));
  }
}
