package com.kiga.kga.domain;

public enum ParentRole {
  MOTHER,
  FATHER,
  GRAND_FATHER,
  GRAND_MOTHER,
  SIGNIFICANT_OTHER,
  OTHER
}
