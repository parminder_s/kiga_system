package com.kiga.kga.controller;

import com.kiga.kga.api.controller.KindergartenApi;
import com.kiga.kga.api.model.DashboardViewModel;
import com.kiga.kga.api.model.IdRequest;
import com.kiga.kga.api.model.KindergartenModel;
import com.kiga.kga.api.model.SetAdminRequest;
import com.kiga.kga.mapper.KindergartenMapper;
import com.kiga.kga.service.KindergartenLister;
import com.kiga.kga.service.KindergartenService;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KindergartenController implements KindergartenApi {

  private final KindergartenService kindergartenService;
  private final KindergartenLister kindergartenLister;
  private final KindergartenMapper mapper;

  @Autowired
  public KindergartenController(
      KindergartenService kindergartenService,
      KindergartenLister kindergartenLister,
      KindergartenMapper mapper) {
    this.kindergartenService = kindergartenService;
    this.kindergartenLister = kindergartenLister;
    this.mapper = mapper;
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaKindergarten', 'READ')")
  public ResponseEntity<KindergartenModel> findOneKindergarten(@RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(
        mapper.kindergartenToKindergartenModel(
            kindergartenService.findOne(idRequest.getId().longValue())));
  }

  @Override
  public ResponseEntity<List<KindergartenModel>> listKindergarten() {
    return ResponseEntity.ok(
        kindergartenLister
            .list()
            .stream()
            .map(mapper::kindergartenToKindergartenModel)
            .collect(Collectors.toList()));
  }

  @Override
  @PreAuthorize("hasPermission(#kindergartenModel.id, 'KgaKindergarten', 'WRITE')")
  public ResponseEntity<BigDecimal> saveKindergarten(
      @RequestBody KindergartenModel kindergartenModel) {
    return ResponseEntity.ok(
        new BigDecimal(
            kindergartenService
                .save(
                    KindergartenMapper.INSTANCE.kindergartenModelToKindergarten(kindergartenModel),
                    kindergartenModel.getAddress().getCountryId().longValue())
                .getId()
                .toString()));
  }

  @Override
  @PreAuthorize("hasPermission(#kindergartenModel.id, 'KgaKindergarten', 'WRITE')")
  public ResponseEntity<BigDecimal> setKindergartenAdmin(
      @RequestBody SetAdminRequest kindergartenModel) {
    return ResponseEntity.ok(null);
  }

  @Override
  @PreAuthorize("hasPermission(#idRequest.id, 'KgaKindergarten', 'READ')")
  public ResponseEntity<DashboardViewModel> fetchDashboard(@RequestBody IdRequest idRequest) {
    return ResponseEntity.ok(kindergartenService.createDashboardViewModel(idRequest.getId()));
  }
}
