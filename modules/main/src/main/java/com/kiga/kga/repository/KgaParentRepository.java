package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaParent;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface KgaParentRepository extends CrudRepository<KgaParent, Long> {
  List<KgaParent> findByKindergarten_Id(Long kindergartenId);
}
