package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.shop.domain.Country;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
public class KgaKindergarten extends KigaEntityModel {
  public KgaKindergarten() {
    this.setClassName("kindergarten");
  }

  private String name;
  private String email;
  private String phoneNumber;
  private String website;
  private String street;
  private String houseNumber;
  private String doorNumber;
  private String floor;
  private String zip;
  private String city;

  @ManyToOne private Country country;

  @OneToMany(mappedBy = "kindergarten")
  private List<KgaYear> years;

  @OneToMany(mappedBy = "kindergarten")
  private List<KgaChild> children;
}
