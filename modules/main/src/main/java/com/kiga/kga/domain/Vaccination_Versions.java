package com.kiga.kga.domain;

import javax.persistence.Entity;

@Entity
public class Vaccination_Versions extends VaccinationData<KgaChild_Versions> {
  public Vaccination_Versions() {
    setClassName("Vaccination_Versions");
  }
}
