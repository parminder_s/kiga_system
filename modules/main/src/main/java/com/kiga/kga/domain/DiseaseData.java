package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.kga.domain.converter.DiseaseConverter;
import javax.persistence.Convert;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = "child")
public class DiseaseData<T extends KgaChildData> extends KigaEntityModel {
  @ManyToOne private T child;

  @Convert(converter = DiseaseConverter.class)
  private DiseaseEnum disease;
}
