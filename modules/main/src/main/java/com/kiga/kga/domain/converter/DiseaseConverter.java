package com.kiga.kga.domain.converter;

import com.kiga.kga.domain.DiseaseEnum;
import javax.persistence.AttributeConverter;

public class DiseaseConverter implements AttributeConverter<DiseaseEnum, String> {

  @Override
  public String convertToDatabaseColumn(DiseaseEnum attribute) {
    switch (attribute) {
      case DIABETES:
        return "diabetis";
      case OTHER:
        return "other";
    }

    throw new IllegalArgumentException("Not mapped:" + attribute);
  }

  @Override
  public DiseaseEnum convertToEntityAttribute(String dbData) {
    switch (dbData) {
      case "diabetis":
        return DiseaseEnum.DIABETES;
      case "other":
        return DiseaseEnum.OTHER;
    }

    throw new IllegalArgumentException("Not mapped:" + dbData);
  }
}
