package com.kiga.kga.repository;

import com.kiga.kga.domain.ChildInKgaGroup;
import com.kiga.kga.domain.KgaChild;
import com.kiga.kga.domain.KgaGroup;
import com.kiga.kga.domain.KgaYear;
import java.util.List;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

public interface ChildInKgaGroupRepository extends CrudRepository<ChildInKgaGroup, Long> {

  ChildInKgaGroup findByChildAndGroup_Year(KgaChild child, KgaYear group);

  @EntityGraph(
      attributePaths = {"child"},
      type = EntityGraph.EntityGraphType.FETCH)
  List<ChildInKgaGroup> findWithChildByGroup_Id(long groupId);

  List<ChildInKgaGroup> findByGroup(KgaGroup group);

  List<ChildInKgaGroup> findByChild(KgaChild child);
}
