package com.kiga.kga.survey.domain;

import com.kiga.db.KigaEntityModel;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder(toBuilder = true)
@AllArgsConstructor
public class Section extends KigaEntityModel {
  private int position;
  private String title;
  private String description;

  @ManyToOne private Survey survey;

  @OneToMany(mappedBy = "section")
  @OrderBy("position")
  private List<Question> questions;

  public Section() {
    this.setClassName("Section");
  }
}
