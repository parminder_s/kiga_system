package com.kiga.kga.domain;

import javax.persistence.Entity;
import lombok.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
public class Allergy extends AllergyData<KgaChild> {
  public Allergy() {
    this.setClassName("Allergy");
  }

  public Allergy(AllergyEnum allergyEnum) {
    this();
    this.setAllergy(allergyEnum);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Allergy) {
      return ((Allergy) o).getAllergy().equals(this.getAllergy());
    }
    return false;
  }

  @Override
  public int hashCode() {
    return super.getAllergy().hashCode();
  }
}
