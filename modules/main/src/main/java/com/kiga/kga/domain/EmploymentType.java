package com.kiga.kga.domain;

public enum EmploymentType {
  EDUCATOR,
  CLEANER,
  CHILD_CARE_WORKER,
  DAY_NANNY,
  DAY_DADDY,
  COMMUNITY_SERVICE,
  OTHER
}
