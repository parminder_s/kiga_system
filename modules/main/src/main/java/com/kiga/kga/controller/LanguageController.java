package com.kiga.kga.controller;

import com.kiga.kga.api.controller.LanguageApi;
import com.kiga.kga.api.model.LanguageViewModel;
import com.kiga.kga.service.KindergartenService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LanguageController implements LanguageApi {

  private final KindergartenService kindergartenService;

  @Autowired
  public LanguageController(KindergartenService kindergartenService) {
    this.kindergartenService = kindergartenService;
  }

  @Override
  public ResponseEntity<List<LanguageViewModel>> listLanguage() {
    return ResponseEntity.ok(kindergartenService.listLanguages());
  }
}
