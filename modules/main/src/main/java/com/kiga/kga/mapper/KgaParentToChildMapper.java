package com.kiga.kga.mapper;

import com.kiga.kga.api.model.KgaParentModel;
import com.kiga.kga.domain.KgaParentToChild;
import com.kiga.kga.domain.KgaParentToChildData;
import com.kiga.util.mapper.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapStructConfig.class)
public interface KgaParentToChildMapper {
  KgaParentToChildMapper INSTANCE = Mappers.getMapper(KgaParentToChildMapper.class);

  @Mapping(target = "className", constant = "KgaParentToChild")
  @Mapping(target = "created", ignore = true)
  @Mapping(target = "lastEdited", ignore = true)
  @Mapping(target = "parent", ignore = true)
  @Mapping(target = "child", ignore = true)
  KgaParentToChild modelToEntity(KgaParentModel kgaParentViewModel);

  @Mapping(target = "id", source = "parent.id")
  @Mapping(target = "firstname", source = "parent.firstname")
  @Mapping(target = "lastname", source = "parent.lastname")
  @Mapping(target = "phoneNumber", source = "parent.phoneNumber")
  @Mapping(target = "email", source = "parent.email")
  @Mapping(target = "legalGuardian", source = "parent.legalGuardian")
  @Mapping(target = "pickupAuthorized", source = "parent.pickupAuthorized")
  @Mapping(target = "emergencyAuthorized", source = "parent.emergencyAuthorized")
  @Mapping(target = "employment", source = "parent.employment")
  KgaParentModel entityToModel(KgaParentToChildData data);
}
