package com.kiga.kga.mapper;

import com.kiga.kga.api.model.KgaGroupModel;
import com.kiga.kga.domain.ChildInKgaGroup;
import com.kiga.kga.domain.KgaGroup;
import com.kiga.util.mapper.MapStructConfig;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapStructConfig.class)
public interface KgaGroupMapper {
  KgaGroupMapper INSTANCE = Mappers.getMapper(KgaGroupMapper.class);

  @Mapping(target = "className", constant = "KgaGroup")
  @Mapping(target = "created", ignore = true)
  @Mapping(target = "lastEdited", ignore = true)
  @Mapping(target = "year", ignore = true)
  @Mapping(target = "groupPermissions", ignore = true)
  @Mapping(target = "childInKgaGroup", ignore = true)
  KgaGroup kgaGroupModelToKgaGroup(KgaGroupModel request);

  @Mapping(source = "year.id", target = "yearId")
  @Mapping(target = "adminEmail", ignore = true)
  @Mapping(target = "secondAdminEmail", ignore = true)
  @Mapping(target = "children", ignore = true)
  @Mapping(target = "countChildren", source = "childInKgaGroup")
  KgaGroupModel kgaGroupToKgaGroupModel(KgaGroup request);

  default Integer convert(List<ChildInKgaGroup> childInKgaGroupList) {
    return childInKgaGroupList.size();
  }
}
