package com.kiga.kga.security;

import com.kiga.kga.domain.KgaParentToChild;
import com.kiga.kga.repository.KgaParentToChildRepository;
import com.kiga.security.spring.AbstractKigaPermissionEvaluator;
import com.kiga.security.spring.KigaAuthentication;
import com.kiga.security.spring.KigaPermissionEvaluator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KgaParentPermissionEvaluator extends AbstractKigaPermissionEvaluator {

  private final KgaChildPermissionEvaluator childPermissionEvaluator;
  private final KgaParentToChildRepository parentToChildRepository;

  @Autowired
  public KgaParentPermissionEvaluator(
      KgaChildPermissionEvaluator childPermissionEvaluator,
      KgaParentToChildRepository parentToChildRepository,
      KigaPermissionEvaluator kigaPermissionEvaluator) {
    this.childPermissionEvaluator = childPermissionEvaluator;
    this.parentToChildRepository = parentToChildRepository;
    kigaPermissionEvaluator.addEvaluator("KgaParent", this);
  }

  @Override
  public boolean hasPermission(
      KigaAuthentication authentication, Long id, String targetType, String permission) {
    if (id == null) {
      return true;
    }

    for (KgaParentToChild parentToChild : parentToChildRepository.findByParent_Id(id)) {
      if (childPermissionEvaluator.hasPermission(
          authentication, parentToChild.getChild().getId(), "KgaChild", permission)) {
        return true;
      }
    }

    return false;
  }
}
