package com.kiga.kga.domain;

import com.kiga.db.KigaEntityModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
public class Language extends KigaEntityModel {
  public Language() {
    this.setClassName("Language");
  }

  @Column(name = "iso6391")
  private String code;
}
