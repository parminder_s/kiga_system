package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaGroupPermission;
import com.kiga.kga.domain.KgaYearPermission;
import com.kiga.security.domain.Member;
import org.springframework.data.repository.CrudRepository;

public interface KgaYearPermissionRepository extends CrudRepository<KgaYearPermission, Long> {
  KgaGroupPermission findByEmployee_MemberAndYear_Id(Member member, Long id);
}
