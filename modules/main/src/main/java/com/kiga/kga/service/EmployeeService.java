package com.kiga.kga.service;

import com.kiga.kga.domain.KgaEmployee;
import com.kiga.kga.repository.KgaEmployeeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
  private final KgaEmployeeRepository employeeRepository;
  private final KindergartenService kindergartenService;

  @Autowired
  public EmployeeService(
      KgaEmployeeRepository repository, KindergartenService kindergartenService) {
    this.employeeRepository = repository;
    this.kindergartenService = kindergartenService;
  }

  public List<KgaEmployee> listByKindergarten(Long kindergartenId) {
    return employeeRepository.findByKindergarten_Id(kindergartenId);
  }

  public KgaEmployee save(KgaEmployee toSave, Long kindergartenId) {
    if (toSave.getId() != null) {
      KgaEmployee oldDb = employeeRepository.findOne(toSave.getId());
      toSave.setMember(oldDb.getMember());
      toSave.setKindergarten(oldDb.getKindergarten());
    } else {
      toSave.setKindergarten(kindergartenService.findOne(kindergartenId));
    }
    return employeeRepository.save(toSave);
  }

  public KgaEmployee findOne(Long id) {
    return employeeRepository.findOne(id);
  }
}
