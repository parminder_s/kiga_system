package com.kiga.kga.repository;

import com.kiga.kga.domain.KgaChildPermission;
import com.kiga.kga.domain.KgaGroupPermission;
import com.kiga.security.domain.Member;
import org.springframework.data.repository.CrudRepository;

public interface KgaChildPermissionRepository extends CrudRepository<KgaChildPermission, Long> {
  KgaGroupPermission findByEmployee_MemberAndChild_Id(Member member, Long id);
}
