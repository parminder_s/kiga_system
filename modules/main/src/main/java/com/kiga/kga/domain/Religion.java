package com.kiga.kga.domain;

public enum Religion {
  CATHOLIC,
  PROTESTANT,
  ISLAM,
  ATHEIST,
  OTHER
}
