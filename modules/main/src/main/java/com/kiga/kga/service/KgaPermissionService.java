package com.kiga.kga.service;

import com.kiga.db.RepositoryPersister;
import com.kiga.kga.domain.KgaEmployee;
import com.kiga.kga.repository.KgaEmployeeRepository;
import com.kiga.security.domain.Member;
import com.kiga.security.spring.SecurityUtil;
import com.kiga.spec.Persister;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Builder
public class KgaPermissionService {

  private final KgaEmployeeRepository kgaEmployeeRepository;
  private final Persister<KgaEmployee> kgaEmployeePersister;
  private final SecurityUtil securityUtil;

  /** Constructor. */
  @Autowired
  public KgaPermissionService(
      KgaEmployeeRepository kgaEmployeeRepository, SecurityUtil securityUtil) {
    this.kgaEmployeeRepository = kgaEmployeeRepository;
    this.kgaEmployeePersister = new RepositoryPersister<>(kgaEmployeeRepository);
    this.securityUtil = securityUtil;
  }

  /** set the member object after a user registers. */
  public void finishRegistration(Member member) {
    List<KgaEmployee> kgaEmployees = kgaEmployeeRepository.findByEmail(member.getEmail());
    kgaEmployees.forEach(kgaEmployee -> kgaEmployee.setMember(member));
    kgaEmployees.forEach(kgaEmployeePersister::persist);
  }

  /**
   * returns the current employee, which stands at the moment for the kgaUser by mapping the
   * security member to the employee.
   */
  public KgaEmployee getCurrentEmployee() {
    return kgaEmployeeRepository.findByMember(securityUtil.getMember());
  }
}
