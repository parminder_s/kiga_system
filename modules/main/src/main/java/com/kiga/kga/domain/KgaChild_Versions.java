package com.kiga.kga.domain;

import com.kiga.db.KigaAuditable;
import com.kiga.security.domain.Member;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Builder
public class KgaChild_Versions
    extends KgaChildData<
        Vaccination_Versions,
        Disease_Versions,
        Allergy_Versions,
        ChildInKgaGroup_Versions,
        KgaParentToChild_Versions>
    implements KigaAuditable<KgaChild> {

  public KgaChild_Versions() {
    this.setClassName("KgaChild_Versions");
  }

  @ManyToOne private KgaChild child;

  @ManyToOne private Member editor;

  @Override
  public KgaChild getOriginal() {
    return child;
  }

  @Override
  public void setOriginal(KgaChild original) {
    this.child = original;
  }

  @PrePersist
  public void prePersist() {
    super.prePersist();
  }
}
