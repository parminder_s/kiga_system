package com.kiga.translation.service;

import com.kiga.translation.domain.CustomLanguageTranslation;
import com.kiga.translation.model.TranslationModel;
import net.liftweb.json.JsonAST;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by aynul on 4/18/16.
 */
public class MergerService {
  /**
   * merges json and CLT data by giving CTL priority.
   */
  public Map<String, TranslationModel> merge(
    List<JsonAST.JField> jsonObjList, List<CustomLanguageTranslation> cltList
  ) {

    TranslationModelFactory modelFactory = new DefaultTranslationModelFactory();
    Map<String, TranslationModel> mergedTdm = new HashMap();

    for (JsonAST.JField entity : jsonObjList) {

      TranslationModel tdm = modelFactory.transformTo(entity);
      mergedTdm.put(tdm.getKey(), tdm);
    }

    for (CustomLanguageTranslation clt : cltList) {
      TranslationModel tdm = modelFactory.transformTo(clt);
      mergedTdm.put(tdm.getKey(),tdm);
    }

    return mergedTdm;

  }
}
