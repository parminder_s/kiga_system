package com.kiga.translation;

import com.kiga.translation.service.DefaultTranslationModelFactory;
import com.kiga.translation.service.TranslationModelFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TranslationConfiguration {
  @Bean
  protected TranslationModelFactory translationModelFactory() {
    return new DefaultTranslationModelFactory();
  }
}
