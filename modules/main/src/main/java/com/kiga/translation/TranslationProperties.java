package com.kiga.translation;

import com.kiga.main.locale.Locale;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * Created by rainerh on 02.04.16.
 */
@Configuration
@ConfigurationProperties("translation")
public class TranslationProperties {
  private String baseLocale;
  private String defaultLocale;
  private Map<Locale, String> locales;

  public String getBaseLocale() {
    return baseLocale;
  }

  public void setBaseLocale(String baseLocale) {
    this.baseLocale = baseLocale;
  }

  public String getDefaultLocale() {
    return defaultLocale;
  }

  public void setDefaultLocale(String defaultLocale) {
    this.defaultLocale = defaultLocale;
  }

  public Map<Locale, String> getLocales() {
    return locales;
  }

  public void setLocales(Map<Locale, String> locales) {
    this.locales = locales;
  }
}
