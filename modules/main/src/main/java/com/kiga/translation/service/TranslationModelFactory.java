package com.kiga.translation.service;

import com.kiga.translation.domain.CustomLanguageTranslation;
import com.kiga.translation.model.TranslationModel;
import net.liftweb.json.JsonAST.JField;

/**
 * Created by aynul on 4/18/16.
 */
public interface TranslationModelFactory {
  TranslationModel transformTo(JField myJson);

  TranslationModel transformTo(CustomLanguageTranslation clt);
}
