package com.kiga.translation.service;

import com.kiga.translation.domain.CustomLanguageTranslation;
import com.kiga.translation.model.TranslationModel;
import net.liftweb.json.JsonAST.JField;

/**
 * Created by aynul on 4/18/16.
 */
public class DefaultTranslationModelFactory implements TranslationModelFactory {
  @Override
  public TranslationModel transformTo(JField myJson) {
    TranslationModel model = new TranslationModel(myJson);
    return model;
  }

  @Override
  public TranslationModel transformTo(CustomLanguageTranslation clt) {
    TranslationModel model = new TranslationModel(clt);
    return model;
  }

}
