package com.kiga.translation.model;

import com.kiga.translation.domain.CustomLanguageTranslation;
import net.liftweb.json.JsonAST.JField;

import java.util.Date;
import java.util.List;

/**
 * Created by aynul on 4/18/16.
 */
public class TranslationModel {

  private String key;
  private String locale;
  private String translation;
  private String originalTranslation;
  private String status;
  private String html;
  private List<String> category;
  private Date createdAt;
  private Date lastEditedAt;
  private Boolean isEdited = false;

  public TranslationModel(JField myJson) {
    this.key = myJson.name();
    this.translation = myJson.value().toString();
  }

  /**
   * inject clt.
   * TODO: clear why CLT is required when we actually have a factory
   */
  public TranslationModel(CustomLanguageTranslation clt) {
    this.key = clt.getEntity();
    this.locale = clt.getLocale();
    this.translation = clt.getTranslation();
    this.createdAt = clt.getCreated();
    this.lastEditedAt = clt.getLastEdited();
    this.status = clt.getStatus();
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public String getTranslation() {
    return translation;
  }

  public void setTranslation(String translation) {
    this.translation = translation;
  }

  public String getOriginalTranslation() {
    return originalTranslation;
  }

  public void setOriginalTranslation(String originalTranslation) {
    this.originalTranslation = originalTranslation;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getHtml() {
    return html;
  }

  public void setHtml(String html) {
    this.html = html;
  }

  public List<String> getCategory() {
    return category;
  }

  public void setCategory(List<String> category) {
    this.category = category;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getLastEditedAt() {
    return lastEditedAt;
  }

  public void setLastEditedAt(Date lastEditedAt) {
    this.lastEditedAt = lastEditedAt;
  }

  public Boolean getEdited() {
    return isEdited;
  }

  public void setEdited(Boolean edited) {
    isEdited = edited;
  }
}
