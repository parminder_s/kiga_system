package com.kiga.logging.services;

import com.kiga.logging.domain.LinkLog;
import com.kiga.logging.domain.LogModel;
import com.kiga.logging.domain.PrintLog;
import com.kiga.logging.domain.SearchTerm;
import com.kiga.logging.domain.Transition;
import com.kiga.logging.domain.TransitionLog;
import com.kiga.logging.repositories.LinkLogRepository;
import com.kiga.logging.repositories.PrintLogRepository;
import com.kiga.logging.repositories.SearchTermRepository;
import com.kiga.logging.repositories.TransitionLogRepository;
import com.kiga.logging.requests.LinkLogRequest;
import com.kiga.logging.requests.PrintLogRequest;
import com.kiga.logging.requests.PrintLogSsRequest;
import com.kiga.logging.requests.TransitionRequest;
import com.kiga.main.locale.Locale;
import com.kiga.security.services.SecurityService;
import com.kiga.web.service.GeoIpService;
import com.kiga.web.service.LocateByIpResolver;
import com.kiga.web.service.UserIpService;
import org.modelmapper.ModelMapper;

import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by faxxe on 9/28/16.
 */
@Named
public class LogService {

  private ModelMapper modelMapper;

  private SecurityService securityService;

  private LinkLogRepository linkLogRepository;

  private SearchTermRepository searchTermRepository;

  private PrintLogRepository printLogRepository;

  private UserIpService userIpService;

  private GeoIpService geoIpService;

  private LocateByIpResolver locateByIpResolver;

  private TransitionLogRepository transitionLogRepository;

  /**
   * constructor with all arguments.
   */
  public LogService(ModelMapper modelMapper,
    SecurityService securityService,
    LinkLogRepository linkLogRepository,
    SearchTermRepository searchTermRepository,
    PrintLogRepository printLogRepository,
    TransitionLogRepository transitionLogRepository,
    UserIpService userIpService,
    GeoIpService geoIpService,
    LocateByIpResolver locateByIpResolver) {
    this.modelMapper = modelMapper;
    this.securityService = securityService;
    this.linkLogRepository = linkLogRepository;
    this.searchTermRepository = searchTermRepository;
    this.printLogRepository = printLogRepository;
    this.transitionLogRepository = transitionLogRepository;
    this.userIpService = userIpService;
    this.geoIpService = geoIpService;
    this.locateByIpResolver = locateByIpResolver;
  }

  /**
   * log an url-click.
   */
  public void logLink(HttpServletRequest httpServletRequest, LinkLogRequest logRequest) {
    LinkLog linkLog = new LinkLog();
    linkLog.setUrl(logRequest.getUrl());
    linkLog.setTag(logRequest.getTag());

    setLocaleIpAndSessionId(linkLog, logRequest.getLocale(), httpServletRequest);
    linkLogRepository.save(linkLog);
  }

  /**
   * log an articlePrint.
   */
  public void logPrint(HttpServletRequest httpServletRequest, PrintLogRequest printLogRequest) {

    PrintLog printLog = new PrintLog();

    setLocaleIpAndSessionId(printLog, printLogRequest.getLocale(), httpServletRequest);

    printLog.setLocale(printLogRequest.getLocale());
    printLog.setTitle(printLogRequest.getTitle());

    printLog.setArticleId(printLogRequest.getArticleId());
    printLog.setOrigin(printLogRequest.getOrigin());
    printLog.setUrl(printLogRequest.getUrl());

    printLogRepository.save(printLog);
  }

  /**
   * log an articlePrint.
   */
  public void logPrintSs(PrintLogSsRequest printLogSsRequest) {

    PrintLog printLog = new PrintLog();

    printLog.setArticleId(printLogSsRequest.getArticleId());
    printLog.setRemoteIp(printLogSsRequest.getIp());
    printLog.setSessionId(printLogSsRequest.getSessionId());
    printLog.setLocale(printLogSsRequest.getLocale());
    printLog.setTitle(printLogSsRequest.getTitle());
    printLog.setUrl(printLogSsRequest.getUrl());
    printLog.setOrigin(printLogSsRequest.getOrigin());
    printLog.setCountryCode(locateByIpResolver.getCountryIsoCode(printLogSsRequest.getIp()));
    printLogRepository.save(printLog);
  }

  /**
   * log a searchquery in elasticsearch.
   */
  public void logSearchTerm(HttpServletRequest httpServletRequest, Locale locale,
    SearchTerm searchTerm) {

    setLocaleIpAndSessionId(searchTerm, locale, httpServletRequest);
    searchTermRepository.save(searchTerm);
  }


  /**
   * log a page transition.
   */
  public void logTransition(TransitionRequest transitionRequest,
    HttpServletRequest httpServletRequest) {
    Locale locale = transitionRequest.getLocale();

    for (Transition tmp : transitionRequest.getTransitions()) {
      TransitionLog transitionLog = new TransitionLog();
      setLocaleIpAndSessionId(transitionLog, locale, httpServletRequest);
      transitionLog.setToUrl(tmp.getToUrl());
      transitionLog.setFromUrl(tmp.getFromUrl());
      transitionLogRepository.save(transitionLog);
    }
  }

  private void setLocaleIpAndSessionId(LogModel logModel, Locale locale,
    HttpServletRequest httpServletRequest) {

    logModel.setRemoteIp(userIpService.getUserIp(httpServletRequest));
    logModel
      .setCountryCode(geoIpService.resolveCountryCode());

    logModel.setLocale(locale);
    logModel.setSessionId(securityService.getSessionIdFromCookie());

  }

}
