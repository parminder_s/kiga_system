package com.kiga.logging.requests;

import com.kiga.web.message.EndpointRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by faxxe on 9/28/16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)

public class LinkLogRequest extends EndpointRequest {
  private String url;
  private String tag;
}
