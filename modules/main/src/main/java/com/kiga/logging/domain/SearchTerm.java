package com.kiga.logging.domain;

import javax.persistence.Entity;

/**
 * Created by faxxe on 9/8/16.
 */
@Entity
public class SearchTerm extends LogModel {

  private String searchTerm;
  private Long parentId;
  private Long ageGroup;
  private Long resultCount;

  public SearchTerm() {
    this.setClassName("SearchTerm");
  }

  public String getSearchTerm() {
    return searchTerm;
  }

  public void setSearchTerm(String searchTerm) {
    this.searchTerm = searchTerm;
  }

  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  public Long getAgeGroup() {
    return ageGroup;
  }

  public void setAgeGroup(Long ageGroup) {
    this.ageGroup = ageGroup;
  }

  public Long getResultCount() {
    return resultCount;
  }

  public void setResultCount(Long resultCount) {
    this.resultCount = resultCount;
  }

}
