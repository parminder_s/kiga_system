package com.kiga.logging.repositories;

import com.kiga.logging.domain.LinkLog;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by faxxe on 9/28/16.
 */
public interface LinkLogRepository extends CrudRepository<LinkLog, Long> {
}
