package com.kiga.logging.repositories;

import com.kiga.logging.domain.PrintLog;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by faxxe on 9/28/16.
 */
public interface PrintLogRepository extends CrudRepository<PrintLog, Long> {

}
