package com.kiga.logging.domain;

import javax.persistence.Entity;

/**
 * Created by faxxe on 10/5/16.
 */

@Entity
public class PrintLog extends LogModel {

  private Long articleId;
  private String title;
  private String origin;
  private String url;

  public PrintLog() {
    this.setClassName("PrintLog");
  }

  public Long getArticleId() {
    return articleId;
  }

  public void setArticleId(Long articleId) {
    this.articleId = articleId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
