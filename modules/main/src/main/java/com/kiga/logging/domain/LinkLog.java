package com.kiga.logging.domain;

import javax.persistence.Entity;

/**
 * Created by faxxe on 9/28/16.
 */
@Entity
public class LinkLog extends LogModel {


  private String url;
  private String tag;

  public LinkLog() {
    this.setClassName("LinkLog");
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }
}
