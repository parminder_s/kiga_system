package com.kiga.logging.repositories;

import com.kiga.logging.domain.SearchTerm;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by faxxe on 8/10/16.
 */
public interface SearchTermRepository extends CrudRepository<SearchTerm, java.lang.Long> {

  SearchTerm findBySearchTerm(String searchTerm);

}
