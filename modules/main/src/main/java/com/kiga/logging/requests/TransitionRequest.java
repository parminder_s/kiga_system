package com.kiga.logging.requests;

import com.kiga.logging.domain.Transition;
import com.kiga.web.message.EndpointRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by faxxe on 11/8/16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class TransitionRequest extends EndpointRequest {
  List<Transition> transitions = new ArrayList<>();
}
