package com.kiga.logging;

import com.kiga.logging.repositories.LinkLogRepository;
import com.kiga.logging.repositories.PrintLogRepository;
import com.kiga.logging.repositories.SearchTermRepository;
import com.kiga.logging.repositories.TransitionLogRepository;
import com.kiga.logging.services.LogService;
import com.kiga.security.services.SecurityService;
import com.kiga.web.service.GeoIpService;
import com.kiga.web.service.LocateByIpResolver;
import com.kiga.web.service.UserIpService;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LogConfig {

  /** Construct the LogService - Bean. */
  @Bean
  public LogService logService(
      ModelMapper modelMapper,
      SecurityService securityService,
      LinkLogRepository linkLogRepository,
      SearchTermRepository searchTermRepository,
      PrintLogRepository printLogRepository,
      TransitionLogRepository transitionLogRepository,
      UserIpService userIpService,
      GeoIpService geoIpService,
      LocateByIpResolver locateByIpResolver) {
    return new LogService(
        modelMapper,
        securityService,
        linkLogRepository,
        searchTermRepository,
        printLogRepository,
        transitionLogRepository,
        userIpService,
        geoIpService,
        locateByIpResolver);
  }
}
