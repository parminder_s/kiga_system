package com.kiga.logging.domain;

import javax.persistence.Entity;

/**
 * Created by faxxe on 9/28/16.
 */
@Entity
public class TransitionLog extends LogModel {


  private String fromUrl;
  private String toUrl;

  public TransitionLog() {
    this.setClassName("TransitionLog");
  }

  public String getFromUrl() {
    return fromUrl;
  }

  public void setFromUrl(String fromUrl) {
    this.fromUrl = fromUrl;
  }

  public String getToUrl() {
    return toUrl;
  }

  public void setToUrl(String toUrl) {
    this.toUrl = toUrl;
  }
}
