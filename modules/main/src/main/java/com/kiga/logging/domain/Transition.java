package com.kiga.logging.domain;

/**
 * Created by faxxe on 12/1/16.
 */
public class Transition {
  private String fromUrl;
  private String toUrl;

  public Transition() {
  }

  public String getFromUrl() {
    return fromUrl;
  }

  public void setFromUrl(String fromUrl) {
    this.fromUrl = fromUrl;
  }

  public String getToUrl() {
    return toUrl;
  }

  public void setToUrl(String toUrl) {
    this.toUrl = toUrl;
  }
}
