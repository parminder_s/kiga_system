package com.kiga.logging.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.db.LocaleToShortNotationConverter;
import com.kiga.main.locale.Locale;


import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;

/**
 * Created by faxxe on 9/28/16.
 */

@MappedSuperclass
public abstract class LogModel extends KigaEntityModel {

  @Convert(converter = LocaleToShortNotationConverter.class)
  private Locale locale;
  private String sessionId;
  private String remoteIp;
  private String countryCode;

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getRemoteIp() {
    return remoteIp;
  }

  public void setRemoteIp(String remoteIp) {
    this.remoteIp = remoteIp;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }
}
