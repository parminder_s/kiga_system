package com.kiga.logging;

import com.kiga.logging.requests.LinkLogRequest;
import com.kiga.logging.requests.PrintLogRequest;
import com.kiga.logging.requests.PrintLogSsRequest;
import com.kiga.logging.requests.TransitionRequest;
import com.kiga.logging.services.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

/**
 * Created by faxxe on 9/28/16.
 */
@RestController
@RequestMapping("log/")
public class LogController {

  LogService logService;

  @Autowired
  public LogController(LogService logService) {
    this.logService = logService;
  }

  /**
   * Log a link.
   */
  @ResponseBody
  @Transactional
  @RequestMapping(value = "/logLink", method = RequestMethod.POST)
  public void logLink(HttpServletRequest httpServletRequest,
    @RequestBody LinkLogRequest logRequest) {
    logService.logLink(httpServletRequest, logRequest);
  }


  /**
   * Log a print.
   */
  @ResponseBody
  @Transactional
  @RequestMapping(value = "/logPrint", method = RequestMethod.POST)
  public void logPrint(HttpServletRequest httpServletRequest,
    @RequestBody PrintLogRequest printLogRequest) {
    logService.logPrint(httpServletRequest, printLogRequest);
  }

  /**
   * Log a print from SS.
   */
  @ResponseBody
  @Transactional
  @RequestMapping(value = "/logPrintSs", method = RequestMethod.POST)
  public void logPrintSs(@RequestBody PrintLogSsRequest printLogSsRequest) {
    logService.logPrintSs(printLogSsRequest);
  }

  /**
   * Log a transition.
   */
  @ResponseBody
  @Transactional
  @RequestMapping(value = "/logTransition", method = RequestMethod.POST)
  public void logTransition(HttpServletRequest httpServletRequest,
    @RequestBody TransitionRequest transitionRequest) {
    logService.logTransition(transitionRequest, httpServletRequest);
  }
}

