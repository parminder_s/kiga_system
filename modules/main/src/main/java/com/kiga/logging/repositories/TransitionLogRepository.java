package com.kiga.logging.repositories;

import com.kiga.logging.domain.TransitionLog;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by faxxe on 9/28/16.
 */
public interface TransitionLogRepository extends CrudRepository<TransitionLog, Long> {
}
