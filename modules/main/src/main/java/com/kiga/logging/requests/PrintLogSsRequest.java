package com.kiga.logging.requests;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by faxxe on 10/5/16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PrintLogSsRequest extends PrintLogRequest {
  private String ip;
  private String sessionId;
}
