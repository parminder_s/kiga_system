package com.kiga.logging.requests;

import com.kiga.web.message.EndpointRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by faxxe on 10/5/16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PrintLogRequest extends EndpointRequest {
  private long articleId;
  private String title;
  private String origin;
  private String url;
}
