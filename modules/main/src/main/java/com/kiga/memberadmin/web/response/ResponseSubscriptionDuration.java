package com.kiga.memberadmin.web.response;

import com.kiga.crm.messages.response.Product;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Created by peter on 09.11.16.
 */
@Data
public class ResponseSubscriptionDuration {
  private int productId;
  private List<Product> productList;
  private Date subscriptionEnd;
  private String periodChange;
}
