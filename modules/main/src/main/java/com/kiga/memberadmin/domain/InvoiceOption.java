package com.kiga.memberadmin.domain;

/**
 * Created by peter on 08.02.17.
 */
public enum InvoiceOption {
  email_customer,
  post_customer,
  email_bill,
  post_bill
}
