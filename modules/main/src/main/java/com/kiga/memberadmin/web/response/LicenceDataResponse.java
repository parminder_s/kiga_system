package com.kiga.memberadmin.web.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by peter on 01.12.16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LicenceDataResponse {
  List<UserDataLicenceResponse> userData;
  int maxUsers;
}
