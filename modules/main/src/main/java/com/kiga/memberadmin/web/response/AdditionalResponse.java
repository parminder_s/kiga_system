package com.kiga.memberadmin.web.response;

import lombok.Data;

import java.util.List;

/**
 * Created by peter on 19.10.16.
 */
@Data
public class AdditionalResponse {
  String job;
  String organization;
}
