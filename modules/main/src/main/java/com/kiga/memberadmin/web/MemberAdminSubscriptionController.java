package com.kiga.memberadmin.web;

import com.fasterxml.jackson.databind.JsonNode;
import com.kiga.crm.messages.response.ChangeSubscriptionResp;
import com.kiga.crm.messages.response.GenericCommitResponse;
import com.kiga.crm.messages.response.ResponseCancelSubscription;
import com.kiga.crm.messages.response.ResponseGetAvailableSubscriptions;
import com.kiga.crm.messages.response.SetGroupCountResponseContainer;
import com.kiga.crm.messages.response.SetPaymentOptionResp;
import com.kiga.crm.messages.response.TransactionalCommitRequest;
import com.kiga.memberadmin.service.LicenceService;
import com.kiga.memberadmin.service.LicencesDiffState;
import com.kiga.memberadmin.service.SubscriptionService;
import com.kiga.memberadmin.web.request.RequestSubscriptionPayingSave;
import com.kiga.memberadmin.web.request.RequestUpdateSubUsers;
import com.kiga.memberadmin.web.request.SetPaymentRequest;
import com.kiga.memberadmin.web.request.SubscriptionDurationSaveRequest;
import com.kiga.memberadmin.web.response.AboPeriodEndResponse;
import com.kiga.memberadmin.web.response.LicenceResponse;
import com.kiga.memberadmin.web.response.ResponseSubscriptionDuration;
import com.kiga.memberadmin.web.response.ResponseSubscriptionPayment;
import com.kiga.security.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by peter on 19.10.16.
 */
@RestController
@RequestMapping("member")
public class MemberAdminSubscriptionController {

  SubscriptionService subscriptionService;
  LicenceService licenseService;
  SecurityService securityService;

  /**
   *
   * @param subscriptionService subscriptionService.
   * @param licenseService  licenceService.
   * @param securityService securityService.
   */
  @Autowired
  public MemberAdminSubscriptionController(SubscriptionService subscriptionService,
                                           LicenceService licenseService,
                                           SecurityService securityService) {
    this.subscriptionService = subscriptionService;
    this.licenseService = licenseService;
    this.securityService = securityService;
  }

  @RequestMapping(value = "/subscriptionDuration")
  ResponseSubscriptionDuration subscriptionDuration()
    throws Exception {
    securityService.requiresLogin();
    return subscriptionService.duration();
  }

  @RequestMapping(value = "/subscriptionsAvailable")
  ResponseGetAvailableSubscriptions subscriptionsAvailable() throws Exception {
    securityService.requiresLogin();
    return subscriptionService.getAvailableSubscriptions();
  }

  /**
   * Change subscription (duration) endpoint.
   */
  @RequestMapping(value = "/subscriptionDurationSave")
  ChangeSubscriptionResp subscriptionDurationSave(
    @RequestBody SubscriptionDurationSaveRequest request)
    throws Exception {
    securityService.requiresLogin();
    return subscriptionService.updateSubscriptionDuration(
      request.getProductId(), request.getChangeTime());
  }

  @RequestMapping(value = "/subscriptionPaying")
  ResponseSubscriptionPayment subscriptionPaying()
    throws Exception {
    securityService.requiresLogin();
    return subscriptionService.getPaymentOptions();
  }

  @RequestMapping(value = "/subscriptionPayingSave")
  void subscriptionPayingSave(
    @RequestBody RequestSubscriptionPayingSave request) throws Exception {
    securityService.requiresLogin();
    subscriptionService.updatePaymentSetting(request.getPaymentMethod(),
      request.getInvoiceOption());
  }


  @RequestMapping(value = "/subscriptionCancellation")
  AboPeriodEndResponse subscriptionCancellation()
    throws Exception {
    securityService.requiresLogin();
    return subscriptionService.subscriptionCancellation();
  }

  @RequestMapping(value = "/subscriptionCancellationExecute")
  ResponseCancelSubscription subscriptionCancellationExecute()
    throws Exception {
    securityService.requiresLogin();
    return subscriptionService.subscriptionCancellationExecute();
  }

  @RequestMapping(value = "/subscriptionCancellationUndo")
  void subscriptionCancellationUndo() throws Exception {
    securityService.requiresLogin();
    subscriptionService.subscriptionCancellationUndo();
  }

  @RequestMapping(value = "/subscriptionLicences")
  LicenceResponse subscriptionLicences() throws Exception {
    securityService.requiresLogin();
    return licenseService.getLicenceData();
  }

  @RequestMapping(value = "/subscriptionDelete")
  void subscriptionDelete() throws Exception {
    securityService.requiresLogin();
    subscriptionService.subscriptionDelete(securityService.getSessionMember().get());
  }

  @RequestMapping(value = "/subscriptionLicencesSave")
  SetGroupCountResponseContainer subscriptionLicencesSave(
    @RequestBody RequestUpdateSubUsers requestUpdateSubUsers) throws Exception {
    securityService.requiresLogin();

    SetGroupCountResponseContainer result;

    LicenceResponse currentState = licenseService.getLicenceData();
    LicencesDiffState differ = new LicencesDiffState(currentState,
      requestUpdateSubUsers);

    if (differ.shouldCreateTransition() || differ.shouldResetTransition()) {
      // Update in group count - this includes the setting of delete-status
      result = licenseService.updateNumberOfLicences(
        requestUpdateSubUsers.getMaxUsers(),
        differ.getAllUsersMarkedForDeletion());
    } else if (differ.getAdditionalLicensesNum() > 0) {
      // Directly order groups (increase)
      result = licenseService.updateNumberOfLicences(
        requestUpdateSubUsers.getMaxUsers(),
        differ.getAllUsersMarkedForDeletion());
    } else {
      result = new SetGroupCountResponseContainer();
    }

    licenseService.removeSubusers(differ.getUsersToRemove());
    licenseService.createSubusers(differ.getUsersToCreate());

    if (!differ.shouldCreateTransition() && !differ.shouldResetTransition()) {
      licenseService.updateMarkForDeletionStatus(
        differ.getUsersToChangeDeltionMark());
    }

    return result;
  }

  @RequestMapping(value = "/aboPeriod")
  Integer getAboPeriod() throws Exception {
    securityService.requiresLogin();
    return subscriptionService.aboPeriod();
  }

  /**
   * Commit a transaction (coming back from external checkout, telling crm
   * checkout was success and changes can indeed be saved).
   */
  @RequestMapping(value = "/transactionCommit")
  public JsonNode commit(
    @RequestBody TransactionalCommitRequest commit) throws Exception {
    // securityService.requiresLogin();

    // TODO: !! cid !!

    String dummyCid = "123456";

    GenericCommitResponse response = subscriptionService.transactionCommit(
      commit.getTransactionId(), dummyCid, dummyCid);

    return response.getTransactionResult();
  }

  @RequestMapping(value = "/setPaymentToCheckout", method = RequestMethod.POST)
  public SetPaymentOptionResp setPayment(@RequestBody SetPaymentRequest request)
    throws Exception {
    return subscriptionService.setPaymentToCheckout(request.getPaymentMethod());
  }

}
