package com.kiga.memberadmin.web.request;

import lombok.Data;

/**
 * Created by peter on 27.09.16.
 */
@Data
public class ProfileAdditionalRequest {
  private String organization;
  private String job;
  private String jobName;
}
