package com.kiga.memberadmin.web;

import com.kiga.crm.web.request.UpdatePasswordRequest;
import com.kiga.memberadmin.service.ProfileService;
import com.kiga.memberadmin.web.request.CheckEmailRequest;
import com.kiga.memberadmin.web.request.Email;
import com.kiga.memberadmin.web.request.ProfileAdditionalRequest;
import com.kiga.memberadmin.web.request.ProfileRequest;
import com.kiga.memberadmin.web.request.UpdateLanguageRequest;
import com.kiga.memberadmin.web.response.LanguageResponse;
import com.kiga.memberadmin.web.response.ProfileAdditionalResponse;
import com.kiga.memberadmin.web.response.ProfileResponse;
import com.kiga.s3.domain.S3Image;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import com.kiga.translation.TranslationProperties;
import com.kiga.web.message.EndpointRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by peter on 27.09.16.
 */
@ConfigurationProperties("translation")
@RestController
@MultipartConfig(maxFileSize = 2097152L)
@RequestMapping("member")
public class MemberAdminProfileController {
  ProfileService profileService;
  SecurityService securityService;
  TranslationProperties translationProperties;

  /**
   *
   * @param profileService profile service.
   * @param securityService security service.
   * @param translationProperties translationProperties.
   */
  @Autowired
  public MemberAdminProfileController(ProfileService profileService,
                                      SecurityService securityService,
                                      TranslationProperties translationProperties) {
    this.profileService = profileService;
    this.securityService = securityService;
    this.translationProperties = translationProperties;
  }

  @RequestMapping(value = "/profileUser", method = RequestMethod.POST)
  public ProfileResponse getProfile(HttpServletRequest httpServletRequest) throws Exception {
    securityService.requiresLogin();
    return profileService.getProfile(securityService.getSessionMember().get());
  }

  /**
   * saves a new profile.
   */
  @RequestMapping(value = "/profileUserSave", method = RequestMethod.POST)
  public Boolean saveProfile(@RequestBody ProfileRequest request) throws Exception {
    securityService.requiresLogin();
    return profileService.updateProfile(securityService.getSessionMember().get(),
      request.getAddress(), request.getBirthday(), request.getNickname(), request.getAvatar());
  }

  /**
   * return the defaut language.
   */
  @RequestMapping(value = "/profileLanguage", method = RequestMethod.POST)
  public LanguageResponse getLangauge() {
    securityService.requiresLogin();
    return profileService.languageResponse(
      securityService.getSessionMember().get());
  }

  /**
   * saves a new default language.
   */
  @RequestMapping(value = "/profileLanguageSave", method = RequestMethod.POST)
  public Integer saveLangauge(@RequestBody UpdateLanguageRequest updateLanguageRequest)
    throws Exception {
    securityService.requiresLogin();
    return profileService.updateLocale(securityService.getSessionMember().get(),
      updateLanguageRequest.getLanguage());
  }

  @RequestMapping(value = "/profileAdditional", method = RequestMethod.POST)
  public ProfileAdditionalResponse getProfileAdditional(
    @RequestBody EndpointRequest endpointRequest) throws Exception {
    securityService.requiresLogin();
    return profileService.profileAdditional();
  }

  @RequestMapping(value = "/profileAdditionalSave", method = RequestMethod.POST)
  public boolean getProfileAdditionalSave(@RequestBody ProfileAdditionalRequest request)
    throws Exception {
    securityService.requiresLogin();
    return profileService.profileAdditionalSave(request);
  }

  @RequestMapping(value = "/profilePasswordSave", method = RequestMethod.POST)
  public boolean profilePasswordSave(@RequestBody  UpdatePasswordRequest request) throws Exception {
    return profileService.updatePassword(request.getNewPassword(), request.getOldPassword());
  }

  @RequestMapping(value = "/profileEmail", method = RequestMethod.POST)
  public Email profileEmail() {
    securityService.requiresLogin();
    return profileService.findEmail(securityService.getSessionMember().get());
  }


  /**
   * email check.
   */
  @RequestMapping(value = "/profileEmailOk", method = RequestMethod.POST)
  public boolean profileEmailOk(@RequestBody CheckEmailRequest request) throws Exception {
    securityService.requiresLogin();
    return profileService.checkEmail(request.getValue());
  }

  /**
   * saves a new email.
   */
  @RequestMapping(value = "/profileEmailSave", method = RequestMethod.POST)
  public boolean profileEmailSave(@RequestBody Email email) throws Exception {
    securityService.requiresLogin();
    return profileService.updateEmail(securityService.getSessionMember().get(),
      email.getEmail());
  }

  /**
   * uploads a new profile image.
   */
  @Transactional
  @RequestMapping(
    value = "/uploadImage",
    method = RequestMethod.POST
  )
  public S3Image uploadImage(@RequestParam MultipartFile file) throws Exception {
    securityService.requiresLogin();
    Optional<Member> member = securityService.getSessionMember();
    if (member.isPresent()) {
      return profileService.uploadS3Image(file, member.get());
    } else {
      return null;
    }
  }

}
