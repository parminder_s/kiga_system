package com.kiga.memberadmin.service;

import com.kiga.crm.messages.response.Checkout;
import com.kiga.crm.messages.response.GenericTransactionalResponse;
import com.kiga.main.locale.Locale;
import com.kiga.money.Money;
import com.kiga.payment.PaymentInitiator;
import com.kiga.payment.PaymentInitiatorConfig;
import com.kiga.payment.PaymentType;
import com.kiga.web.service.UrlGenerator;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * CheckoutResponseHelper
 *
 * <p>Inspect the CRM response. Depending on the response (`requires_checkout`) , a checkout is
 * initiated, and the return-url is added to the response!
 */
public class CheckoutResponseHelper {
  PaymentInitiator paymentInitiator;
  UrlGenerator urlGenerator;

  static final String FRONTEND_URL = "/member/account/commit";

  /** Set up the checkout helper. */
  @Autowired
  public CheckoutResponseHelper(UrlGenerator urlGenerator, PaymentInitiator paymentInitiator) {
    //    this.paymentInitiator = new PaymentInitiator(null, null,
    //      null, null);
    this.paymentInitiator = paymentInitiator;
    this.urlGenerator = urlGenerator;
  }

  /** Initate payment/checkout. */
  public String initiatePayment(GenericTransactionalResponse response) {
    Checkout checkout = response.getCheckout();

    // String redirectUrl;

    if (hasInternalCheckout(response)) {

      // TODO: get this from somewhere
      String countryCode = "at";

      // TODO: get this from somewhere, too!
      String languageCode = "de";
      Locale locale = Locale.valueOf("de");

      PaymentType paymentType = stringToPaymentType(checkout.getPaymentOptionCode());

      Money checkoutPrice =
          new Money(
              checkout.getCart().getCheckoutAmount(),
              response.getCheckout().getCart().getCurrencyCode());

      String goBackUrl = getFrontendFailUrl(languageCode, response.getTransactionId());
      String commitUrl = getFrontendSuccessUrl(languageCode, response.getTransactionId());

      PaymentInitiatorConfig paymentConfig =
          new PaymentInitiatorConfig(
              paymentType, commitUrl, goBackUrl, checkoutPrice, countryCode, locale);

      return this.paymentInitiator.initiate(paymentConfig);
    }

    return "";
  }

  /**
   * Inspect the crm response for a required checkout and react to it.
   *
   * <p>If required, a paymnet/checkout is initiated. Any redirection and checkout data necessary
   * for the frontend will be added to the response (for passing on to frontend).
   *
   * @param response response from CRM
   */
  public void checkForCheckoutAndInitiate(GenericTransactionalResponse response) {
    String redirectUrl = "";
    if (hasExternalPaymentParameters(response)) {
      // Redirect from CRM Response
      redirectUrl = getRedirectUrl(response);
    }
    if (hasInternalCheckout(response)) {
      // Redirect from internal checkout service
      redirectUrl = initiatePayment(response);
    }
    if (!redirectUrl.equals("")) {
      if (response.getCheckout().getPaymentData() == null) {
        response.getCheckout().setPaymentData(new HashMap<String, String>());
      }
      response.getCheckout().getPaymentData().put("checkout_url", redirectUrl);
    }
  }

  /** Url to redirect back to from payment provider once checkout succeeded. */
  public String getFrontendSuccessUrl(String languageCode, String transactionId) {
    return urlGenerator.getNgUrl(languageCode + FRONTEND_URL) + "?tid=" + transactionId;
  }

  /** Url to redirect back to from payment provider in case checkout fails. */
  public String getFrontendFailUrl(String languageCode, String transactionId) {
    return urlGenerator.getNgUrl(languageCode + FRONTEND_URL) + "?tid=" + transactionId;
  }

  /** Get Parameters to include in the request to CRM. */
  public Map<String, String> getPaymentInitParameters() {
    Map<String, String> map = new HashMap<>();

    // TODO : we're missing some unique id here appended to the urls ...
    String transactionId = "";

    // TODO: get this from somewhere, too!
    String languageCode = "de";

    map.put("checkout_success_url", getFrontendSuccessUrl(languageCode, transactionId));
    map.put("checkout_fail_url", getFrontendFailUrl(languageCode, transactionId));
    return map;
  }

  /** Get the url to redirect to. For now only for external checkout (initiated by crm). */
  public String getRedirectUrl(GenericTransactionalResponse response) {
    Map<String, String> paymentData = response.getCheckout().getPaymentData();
    if (paymentData.containsKey("checkout_url")) {
      return response.getCheckout().getPaymentData().get("checkout_url");
    }
    return "";
  }

  /** Test if external params to crm request re to be added. */
  public boolean hasExternalPaymentParameters(GenericTransactionalResponse response) {
    return response.getNeedsCheckout() == 1 && hasExternalCheckout(response);
  }

  /** Test if external params to crm request re to be added (depending on payment type). */
  public boolean hasExternalPaymentParameters(PaymentType paymentType) {
    return hasExternalCheckout(paymentType);
  }

  /**
   * Test if internal checkout is required. Depends on if any checkout at all, and what payment
   * system is in place.
   */
  public boolean hasInternalCheckout(GenericTransactionalResponse response) {
    return response.getNeedsCheckout() == 1 && !hasExternalCheckout(response);
  }

  private boolean hasExternalCheckout(GenericTransactionalResponse response) {
    if (response.getCheckout() != null) {
      return response.getCheckout().getPaymentOptionCode() == "PAYPAL";
    } else {
      return false;
    }
  }

  private boolean hasExternalCheckout(PaymentType paymentType) {
    return paymentType == PaymentType.PAYPAL;
  }

  private PaymentType stringToPaymentType(String str) {
    return PaymentType.valueOf(str.replace("-", "_"));
  }
}
