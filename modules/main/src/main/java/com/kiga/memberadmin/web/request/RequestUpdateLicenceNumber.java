package com.kiga.memberadmin.web.request;

import lombok.Data;

/**
 * Created by peter on 02.12.16.
 */
@Data
public class RequestUpdateLicenceNumber {
  int numberLicences;
}

