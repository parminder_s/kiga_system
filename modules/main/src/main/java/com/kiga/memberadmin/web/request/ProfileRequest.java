package com.kiga.memberadmin.web.request;

import com.kiga.crm.messages.Address;
import com.kiga.s3.domain.S3Image;
import lombok.Data;

import java.util.Date;

/**
 * Created by peter on 03.01.17.
 */
@Data
public class ProfileRequest {
  private Address address;
  private Date birthday;
  private int minimumAge;
  private String nickname;
  private S3Image avatar;
}

