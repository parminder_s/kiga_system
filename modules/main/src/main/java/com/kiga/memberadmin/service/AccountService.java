package com.kiga.memberadmin.service;

import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.crm.messages.response.Invoice;
import com.kiga.crm.messages.response.ResponseGetCustomerData;
import com.kiga.crm.messages.response.ResponseGetSubscriptions;
import com.kiga.crm.messages.response.Status;
import com.kiga.crm.service.CrmConverter;
import com.kiga.crm.web.response.InvoiceResponse;
import com.kiga.memberadmin.domain.AddressType;
import com.kiga.memberadmin.web.request.UpdateAddressRequest;
import com.kiga.memberadmin.web.response.InvoiceDetailResponse;
import com.kiga.memberadmin.web.response.MemberAdminOverviewResponse;
import com.kiga.payment.PaymentType;
import com.kiga.security.domain.Member;
import com.kiga.shop.service.ShopInvoiceService;
import com.kiga.shop.web.responses.InvoicesResponse;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by peter on 04.10.16.
 */
public class AccountService {

  CrmConverter crmConverter;

  ShopInvoiceService shopInvoiceService;

  InvoiceRepository invoiceRepository;

  /**
   * Constructor.
   */
  public AccountService(CrmConverter crmConverter, ShopInvoiceService shopInvoiceService,
                        InvoiceRepository invoiceRepository) {
    this.crmConverter = crmConverter;
    this.shopInvoiceService = shopInvoiceService;
    this.invoiceRepository = invoiceRepository;
  }

  /**
   *
   * @param member current CMS member.
   * @return returns MemberAdminOverviewReponse.
   * @throws Exception CrmException.
   */
  public MemberAdminOverviewResponse account(Member member) throws Exception {
    ResponseGetCustomerData responseGetCustomerData = crmConverter.customerGetData(0);
    MemberAdminOverviewResponse returner = new MemberAdminOverviewResponse();
    returner.setCustomerNr(responseGetCustomerData.getSubscription().getCustomerNr());

    // TODO : for test products, the getStatus returns nothing (no info about a subscription,
    // invoice etc. is available in this case
    if (responseGetCustomerData.getStatus() != null) {
      returner.setSubscriptionBillOk(responseGetCustomerData.getStatus().getPaid() == 1);
    }

    List<InvoicesResponse> openShopInvoices = shopInvoiceService.getInvoicesForCustomer(
      member.getId()).stream()
      .filter(invoice -> !invoice.isPaid())
      .collect(Collectors.toList());
    returner.setShopBillOk(openShopInvoices.isEmpty());
    if (!returner.isShopBillOk()) {
      returner.setCurrentShopInvouceNr(openShopInvoices.get(0).number());
    }

    // TODO: seems to get current invoice (nr) twice ? (see below)
    Status status = responseGetCustomerData.getStatus();
    String currentInvoiceNr = status != null ? status.getNumber() : "";
    returner.setCurrentInvoiceNr(currentInvoiceNr);

    List<Invoice> invoices = crmConverter.customerGetInvoice()
      .stream()
      .filter(invoice -> invoice.getInvoiceNr()
                            .equals(responseGetCustomerData.getStatus().getNumber()))
      .collect(Collectors.toList());

    if (invoices.size() > 0) {
      Invoice currentInvoice = invoices.get(0);
      returner.setAboBillUrl(currentInvoice.getUrl());
    }

    returner.setDuration(
      responseGetCustomerData.getSubscription().getPeriod());
    returner.setDurationType(
      responseGetCustomerData.getSubscription().getPeriodType());
    returner.setAboType(responseGetCustomerData.getSubscriptionType().getDescription());
    returner.setNameTranslationKey(responseGetCustomerData.getSubscriptionType()
      .getSubscriptionTypeTranslateKey());
    return returner;
    // return crmConverter.customerGetData(0);
  }

  /*
  public InvoiceResponse invoicesSubscription() throws Exception {
    ResponseGetCustomerData responseGetData = crmConverter.customerGetData(0);
    InvoiceResponse invoiceResponse = new InvoiceResponse();
    invoiceResponse.setInvoiceOption(responseGetData.getInvoiceOption());
    invoiceResponse.setPaymentMethod(responseGetData.getPaymentOption());
    ResponseGetSubscriptions responseGetSubscriptions = crmConverter.masterGetSubscriptions(
      responseGetData.getShippingAddress().getCountry());
    ProductPaymentOption productPaymentOption = responseGetSubscriptions.getProducts().stream()
      .filter(
        product -> product.getId().equals(responseGetData.getSubscription().getProductId()))
      .collect(Collectors.toList()).get(0).getPaymentOptions();
    invoiceResponse.setPaymentMethodList(productPaymentOption.asList());


    invoiceResponse.setInvoiceOptionList(responseGetSubscriptions.getInvoiceOptions()
      .getInvoiceOptions().stream()
      .filter(invoiceOption ->
        invoiceOption != InvoiceOption.email_bill && invoiceOption != InvoiceOption.post_bill)
      .collect(Collectors.toList()));
    invoiceResponse.setInvoiceOption(responseGetData.getInvoiceOption());
    invoiceResponse.setPaymentMethod(responseGetData.getPaymentOption());

    invoiceResponse.setInvoiceList(crmConverter.customerGetInvoice().stream()
      .map(invoice -> new InvoiceDetailResponse(invoice.getStatus(), invoice.getAmount(),
        invoice.getInvoiceNr(), "abo", invoice.getUrl(), invoice.getCurrency(), invoice.getDate()))
      .collect(Collectors.toList()));
    return invoiceResponse;
  }

   */

  /**
   * Get subscription invoices.
   * TODO: responseGetData.getSubscription().getCountry() is a better country code field
   * @return list of invoices for subscription (abo) for the user.
   */
  public InvoiceResponse invoicesSubscription() throws Exception {
    ResponseGetCustomerData responseGetCustomerData = crmConverter.customerGetData(0);
    InvoiceResponse invoiceResponse = new InvoiceResponse();
    ResponseGetSubscriptions responseGetSubscriptions = crmConverter.masterGetSubscriptions(
      responseGetCustomerData.getShippingAddress().getCountry());

    // TODO : make sure this is correctly changed according to refactoring of #1336
    List<PaymentType> paymentOptions = responseGetSubscriptions.getProducts().stream()
      .filter(product -> product.getId()
                          .equals(responseGetCustomerData.getSubscription().getProductId()))
      .collect(Collectors.toList()).get(0).getPaymentOptions();
    invoiceResponse.setPaymentMethodList(paymentOptions.stream()
      .filter(method -> method != PaymentType.RE)
      .collect(Collectors.toList()));

    invoiceResponse.setInvoiceList(crmConverter.customerGetInvoice().stream()
      .map(invoice -> new InvoiceDetailResponse(invoice.getStatus(), invoice.getAmount(),
        invoice.getInvoiceNr(), "abo", "", invoice.getCurrency(), invoice.getDate()))
      .collect(Collectors.toList()));
    return invoiceResponse;
  }

  /**
   * Get specific invoice url by invoiceNr
   * @param invoiceNr invoice to get the url for.
   * @return crm invoice pdf url.
   * @throws Exception for unspecified reasons.
   */
  public URL getInvoiceUrl(String invoiceNr) throws MalformedURLException, Exception {
    List<Invoice> invoiceList = crmConverter.customerGetInvoice();
    List<String> invoiceUrlList = invoiceList.stream()
      .filter(invoice -> invoice.getInvoiceNr().equals(invoiceNr))
      .map(invoice -> invoice.getUrl())
      .collect(Collectors.toList());
    return invoiceUrlList.size() > 0 ? new URL(invoiceUrlList.get(0)) : null;
  }

  /**
   *
   * @param member current member.
   * @return list of shop invoices for that user.
   */
  public InvoiceResponse invoicesShop(Member member)  {
    InvoiceResponse invoiceResponse = new InvoiceResponse();
    invoiceResponse.setInvoiceList(shopInvoiceService.getInvoicesForCustomer(member.getId())
      .stream().map(invoice -> new InvoiceDetailResponse(
        invoice.isPaid() ? "paid" : "open", invoice.total().doubleValue(), invoice.number(),
        "shop", invoice.url(), invoice.currency(), Date.from(invoice.invoiceDate())))
      .collect(Collectors.toList()));
    return invoiceResponse;
  }

  /**
   * TODO: finish up and verify that this method works.
   * Update the (snailmail) addresses of a customer account.
   * @param updateAddressRequest request with both addresses in it
   * @return status of operation
   * @throws Exception because.
   */
  public boolean updateAddress(UpdateAddressRequest updateAddressRequest) throws Exception {
    Boolean shippingUpdateStatus;
    Boolean billingUpdateStatus;
    shippingUpdateStatus = crmConverter.customerSetAddress(AddressType.SHIPPING,
      updateAddressRequest.getShippingAddress(), null);
    if (updateAddressRequest.getBillingAddress() != null) {
      billingUpdateStatus = crmConverter.customerSetAddress(AddressType.BILLING,
        updateAddressRequest.getBillingAddress(), null);
    } else {
      billingUpdateStatus = crmConverter.customerRemoveBillingAddress();
    }
    return shippingUpdateStatus && billingUpdateStatus;
  }

  public ResponseGetCustomerData getAddress() throws Exception {
    return crmConverter.customerGetData(0);
  }

}
