package com.kiga.memberadmin.web.request;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by peter on 14.12.16.
 */
@Data
@AllArgsConstructor
public class Subuser {
  private boolean removed;
  private Integer userId;
  private String email;
  private String password;
}
