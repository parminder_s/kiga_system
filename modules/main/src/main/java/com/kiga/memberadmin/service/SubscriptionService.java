package com.kiga.memberadmin.service;

import com.kiga.content.repository.ss.RatingRepository;
import com.kiga.crm.messages.requests.ChangeSubscriptionContainerRequest;
import com.kiga.crm.messages.requests.RequestUpdateCustomerSettingsContainer;
import com.kiga.crm.messages.response.ChangeSubscriptionResp;
import com.kiga.crm.messages.response.GenericCommitResponse;
import com.kiga.crm.messages.response.Invoice;
import com.kiga.crm.messages.response.ResponseCancelSubscription;
import com.kiga.crm.messages.response.ResponseGetAvailableSubscriptions;
import com.kiga.crm.messages.response.ResponseGetCustomerData;
import com.kiga.crm.messages.response.ResponseGetSubscriptions;
import com.kiga.crm.messages.response.SetPaymentOptionResp;
import com.kiga.crm.service.CrmConverter;
import com.kiga.favourites.repository.IdeaFavouritesRepository;
import com.kiga.forum.repository.PostRepository;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.memberadmin.domain.InvoiceOption;
import com.kiga.memberadmin.web.response.AboPeriodEndResponse;
import com.kiga.memberadmin.web.response.ResponseSubscriptionDuration;
import com.kiga.memberadmin.web.response.ResponseSubscriptionPayment;
import com.kiga.memberadmin.web.response.SubscriptionDurationResponse;
import com.kiga.payment.PaymentType;
import com.kiga.plan.PlanRepository;
import com.kiga.security.domain.Member;
import com.kiga.shop.repository.PurchaseRepository;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Created by peter on 19.10.16. */
@Service
@Builder
public class SubscriptionService {
  CrmConverter crmConverter;
  IdeaFavouritesRepository ideaFavouritesRepository;
  PlanRepository planRepository;
  RatingRepository ratingRepository;
  PurchaseRepository purchaseRepository;
  PostRepository postRepository;
  CheckoutResponseHelper checkoutResponseHelper;

  /** Consructor. */
  @Autowired
  public SubscriptionService(
      CrmConverter crmConverter,
      IdeaFavouritesRepository ideaFavouritesRepository,
      PlanRepository planRepository,
      RatingRepository ratingRepository,
      PurchaseRepository purchaseRepository,
      PostRepository postRepository,
      CheckoutResponseHelper checkoutResponseHelper) {
    this.crmConverter = crmConverter;
    this.ideaFavouritesRepository = ideaFavouritesRepository;
    this.planRepository = planRepository;
    this.ratingRepository = ratingRepository;
    this.purchaseRepository = purchaseRepository;
    this.postRepository = postRepository;
    this.checkoutResponseHelper = checkoutResponseHelper;
  }

  /** @throws Exception crm Converter exception. */
  public SubscriptionDurationResponse getSubscriptionDuration() throws Exception {
    ResponseGetCustomerData responseGetCustomerData = crmConverter.customerGetData(0);
    ResponseGetSubscriptions responseSubscription =
        crmConverter.masterGetSubscriptions(responseGetCustomerData.getSubscription().getCountry());

    SubscriptionDurationResponse subscriptionDurationResponse = new SubscriptionDurationResponse();
    subscriptionDurationResponse.setAboPeriodEnd(
        responseGetCustomerData.getSubscription().getEnd());
    subscriptionDurationResponse.setChangeTime(responseGetCustomerData.getSubscription().getEnd());
    subscriptionDurationResponse.setChangeTimeList(Arrays.asList("onend", "immediate"));
    subscriptionDurationResponse.setProductId(
        responseGetCustomerData.getSubscription().getProductId());
    String productGroupId =
        responseSubscription
            .getProducts()
            .stream()
            .filter(
                product ->
                    product
                        .getId()
                        .equals(responseGetCustomerData.getSubscription().getProductId()))
            .collect(Collectors.toList())
            .get(0)
            .getProductGroupId();
    subscriptionDurationResponse.setProducts(
        responseSubscription
            .getProducts()
            .stream()
            .filter(product -> product.getProductGroupId().equals(productGroupId))
            .collect(Collectors.toList()));
    return subscriptionDurationResponse;
  }

  public ResponseGetAvailableSubscriptions getAvailableSubscriptions() throws Exception {
    return crmConverter.customerGetAvailableSubscriptions();
  }

  /**
   * @return AboPeriodEndResponse.
   * @throws Exception occures in crmConverter.
   */
  public AboPeriodEndResponse subscriptionCancellation() throws Exception {
    AboPeriodEndResponse aboPeriodEndResponse = new AboPeriodEndResponse();
    ResponseGetCustomerData responseGetCustomerData = crmConverter.customerGetData(0);
    aboPeriodEndResponse.setAboPeriodEnd(responseGetCustomerData.getSubscription().getEnd());
    if (responseGetCustomerData.getSubscriptionCancellation() != null) {
      aboPeriodEndResponse.setAboPeriodEnd(
          responseGetCustomerData.getSubscriptionCancellation().getCancel());
      aboPeriodEndResponse.setDateOrdered(
          responseGetCustomerData.getSubscriptionCancellation().getOrdered());
    }
    return aboPeriodEndResponse;
  }

  public ResponseCancelSubscription subscriptionCancellationExecute() throws Exception {
    return crmConverter.customerCancelSubscription();
  }

  public void subscriptionCancellationUndo() throws Exception {
    crmConverter.customerCancelSubscriptionUndo();
  }

  /**
   * Delete all (?) data related to a member. Intended for delete-requests from a customer ("remove
   * all my data").
   *
   * @param member of which to delete all data.
   */
  public void subscriptionDelete(Member member) {
    ideaFavouritesRepository.delete(ideaFavouritesRepository.findByMember(member));
    planRepository.delete(planRepository.findByAuthor(member));
    ratingRepository.delete(ratingRepository.findByMember(member));
    purchaseRepository
        .findByCustomer(member)
        .forEach(
            purchase -> {
              purchase.setCustomer(null);
              purchaseRepository.save(purchase);
            });
    postRepository
        .findByAuthor(member)
        .forEach(
            post -> {
              post.setAuthor(null);
              postRepository.save(post);
            });
  }

  /**
   * @return responseSubscriptionDuration.
   * @throws Exception crm Exception.
   */
  public ResponseSubscriptionDuration duration() throws Exception {
    ResponseSubscriptionDuration response = new ResponseSubscriptionDuration();
    ResponseGetCustomerData data = crmConverter.customerGetData(0);
    response.setSubscriptionEnd(data.getSubscription().getEnd());
    response.setProductId(data.getSubscription().getProductId());
    ResponseGetSubscriptions subscriptions =
        crmConverter.masterGetSubscriptions(data.getShippingAddress().getCountry());
    String productGroupId =
        subscriptions
            .getProducts()
            .stream()
            .filter(
                product -> Objects.equals(product.getId(), data.getSubscription().getProductId()))
            .collect(Collectors.toList())
            .get(0)
            .getProductGroupId();
    response.setProductList(
        subscriptions
            .getProducts()
            .stream()
            .filter(product -> product.getProductGroupId().equals(productGroupId))
            .collect(Collectors.toList()));
    // if i have RE as paymentCode remove products where it is not available!
    if (data.getPaymentOption() == PaymentMethod.INVOICE) {
      response
          .getProductList()
          .removeIf(product -> !product.getPaymentOptions().contains(PaymentMethod.INVOICE));
    }
    if (data.getSubscriptionTransition() != null) {
      response.setPeriodChange(data.getSubscriptionTransition().getPeriod());
    }
    return response;
  }

  /**
   * Get available payment options (and invoice options).
   *
   * @return ResponseSubscriptionPayment
   * @throws Exception crm Exception.
   */
  public ResponseSubscriptionPayment getPaymentOptions() throws Exception {
    List<Invoice> invoiceList = crmConverter.customerGetInvoice();
    ResponseGetCustomerData data = crmConverter.customerGetData(0);
    ResponseGetSubscriptions subscriptions =
        crmConverter.masterGetSubscriptions(data.getShippingAddress().getCountry());
    ResponseSubscriptionPayment response = new ResponseSubscriptionPayment();
    response.setInvoiceOption(data.getInvoiceOption());
    response.setPaymentMethod(data.getPaymentOption());

    response.setPaymentMethodList(
        subscriptions
            .getProducts()
            .stream()
            .filter(
                product -> Objects.equals(product.getId(), data.getSubscription().getProductId()))
            .collect(Collectors.toList())
            .get(0)
            .getPaymentOptions());

    response.setInvoiceOptionList(
        subscriptions
            .getInvoiceOptions()
            .getInvoiceOptions()
            .stream()
            .filter(
                invoiceOption ->
                    invoiceOption != InvoiceOption.post_bill
                        && invoiceOption != InvoiceOption.post_customer)
            .collect(Collectors.toList()));
    return response;
  }

  /**
   * Set payment and invoice option. No checkout initiated.
   *
   * @param paymentMethod paymentMethod.
   * @param invoiceOption invoiceOption.
   * @throws Exception crm Exception.
   */
  public void updatePaymentSetting(String paymentMethod, String invoiceOption) throws Exception {
    RequestUpdateCustomerSettingsContainer request = new RequestUpdateCustomerSettingsContainer();
    request.getData().setPaymentOption(paymentMethod);
    request.getData().setInvoiceOption(invoiceOption);
    crmConverter.customerUpdateCustomerSettings(request);
  }

  /**
   * @param productId id of product to change.
   * @param changeTime "immediate" or "onend".
   */
  public ChangeSubscriptionResp updateSubscriptionDuration(Integer productId, String changeTime)
      throws Exception {

    ChangeSubscriptionContainerRequest request = new ChangeSubscriptionContainerRequest();
    request.getData().getSubscription().setProductId(productId);
    request.getData().setNow(changeTime.equalsIgnoreCase("immediate") ? 1 : 0);

    // Always transmit the return urls
    // TODO : we're missing an id to provide - the payment is to be created
    // if (checkoutResponseHelper.hasExternalPaymentParameters())

    if (true) {
      request.getData().setPaymentData(this.checkoutResponseHelper.getPaymentInitParameters());
    }

    // Make call
    ChangeSubscriptionResp response = crmConverter.transactionalChangeSubscription(request);

    // Determine if a checkout should be initiated and where to redirect
    checkoutResponseHelper.checkForCheckoutAndInitiate(response);

    return response;
  }

  public Integer aboPeriod() throws Exception {
    return crmConverter.customerGetData(0).getSubscription().getPeriod();
  }

  /** Finalize commit. */
  public GenericCommitResponse transactionCommit(String tid, String cid, String tid2)
      throws Exception {
    return crmConverter.transactionalCommit(tid, cid, tid2);
  }

  /** Set payment (and initiate checkout, if required). */
  public SetPaymentOptionResp setPaymentToCheckout(String paymentType) throws Exception {
    SetPaymentOptionResp response =
        crmConverter.transactionalSetPaymentOption(PaymentType.valueOf(paymentType));

    // TODO: do pre transaction initate

    checkoutResponseHelper.checkForCheckoutAndInitiate(response);

    return response;
  }
}
