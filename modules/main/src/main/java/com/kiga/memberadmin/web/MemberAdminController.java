package com.kiga.memberadmin.web;

import com.kiga.crm.messages.response.ResponseGetCustomerData;
import com.kiga.crm.web.response.InvoiceResponse;
import com.kiga.memberadmin.service.AccountService;
import com.kiga.memberadmin.web.request.UpdateAddressRequest;
import com.kiga.memberadmin.web.response.MemberAdminOverviewResponse;
import com.kiga.security.services.SecurityService;
import com.kiga.web.NotFoundException;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;
import java.net.URL;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by peter on 04.10.16.
 */
@RestController
@RequestMapping("member")
public class MemberAdminController {

  AccountService accountService;
  SecurityService securityService;

  @Autowired
  public MemberAdminController(AccountService accountService,
                               SecurityService securityService) {
    this.accountService = accountService;
    this.securityService = securityService;
  }

  @RequestMapping(value = "/account")
  public MemberAdminOverviewResponse account() throws Exception {
    securityService.requiresLogin();
    return accountService.account(securityService.getSessionMember().get());
  }

  /**
   * return all invoices from the shop.
   */
  @RequestMapping(value = "/invoicesShop")
  public InvoiceResponse invoicesShop() throws Exception {
    securityService.requiresLogin();
    return accountService.invoicesShop(
      securityService.getSessionMember().get());
  }

  @RequestMapping(value = "/invoicesSubscription")
  public InvoiceResponse invoicesSubscription(HttpServletRequest httpServletRequest)
    throws Exception {
    securityService.requiresLogin();
    return accountService.invoicesSubscription();
  }

  /**
   * A proxy to the crm invoice PDF.
   */
  @RequestMapping(value = "/invoicesSubscription/download", produces = "application/pdf")
  public void fetchInvoice(HttpServletRequest httpServletRequest,
                                           HttpServletResponse response) throws Exception {
    securityService.requiresLogin();

    String invoiceNr = httpServletRequest.getParameter("invoiceNr");
    URL invoiceUrl = this.accountService.getInvoiceUrl(invoiceNr);
    if (invoiceUrl  == null) {
      throw new NotFoundException();
    }

    response.setContentType("application/pdf");
    response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
      "attachment; filename=\"" + invoiceNr + ".pdf\"");

    InputStream inputStream = invoiceUrl.openStream();
    IOUtils.copy(inputStream, response.getOutputStream());
    inputStream.close();
    response.flushBuffer();

  }


  @RequestMapping(value = "/billPayingPrepare")
  public String billPayingPrepare() throws Exception {
    return "";
  }

  @RequestMapping(value = "/updateAddress")
  public boolean updateAddress(@RequestBody UpdateAddressRequest updateAddressRequest)
    throws Exception {
    securityService.requiresLogin();
    return accountService.updateAddress(updateAddressRequest);
  }

  @RequestMapping(value = "/updateCustomerSettings")
  public InvoiceResponse updateCustomerSettings() throws Exception {
    throw new Exception("MemberAdminController.udpateCustomerSettings");
    //return null; //to implement!!
  }

  @RequestMapping(value = "/address", method = RequestMethod.POST)
  public ResponseGetCustomerData getAddress() throws Exception {
    return this.accountService.getAddress();
  }
}
