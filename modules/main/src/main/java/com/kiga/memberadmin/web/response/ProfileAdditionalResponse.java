package com.kiga.memberadmin.web.response;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Created by peter on 27.09.16.
 */
@Data
public class ProfileAdditionalResponse {
  private String organization;
  private String job;
  private String jobName;
}
