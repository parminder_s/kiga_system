package com.kiga.memberadmin.web.request;

import lombok.Data;

/**
 * Created by peter on 12.10.16.
 */
@Data
public class UpdateLanguageRequest {
  private String language;
}
