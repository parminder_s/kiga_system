package com.kiga.memberadmin.web.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by peter on 29.05.17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberAdminOverviewResponse {
  private boolean shopBillOk;
  private boolean subscriptionBillOk;
  private String currentInvoiceNr;
  private String currentShopInvouceNr;
  private String customerNr;
  private String aboType;
  private String aboBillUrl;
  private String nameTranslationKey;
  private Integer duration;
  private String durationType;
}
