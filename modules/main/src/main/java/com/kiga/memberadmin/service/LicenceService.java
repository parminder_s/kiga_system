package com.kiga.memberadmin.service;

import com.kiga.crm.messages.requests.UserDeleteStatusRequest;
import com.kiga.crm.messages.response.ResponseGetCustomerData;
import com.kiga.crm.messages.response.SetGroupCountResponseContainer;
import com.kiga.crm.messages.response.Status;
import com.kiga.crm.messages.response.SubUser;
import com.kiga.crm.service.CrmConverter;
import com.kiga.memberadmin.web.request.Subuser;
import com.kiga.memberadmin.web.response.LicenceDataResponse;
import com.kiga.memberadmin.web.response.LicenceResponse;
import com.kiga.memberadmin.web.response.UserDataLicenceResponse;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Backend calls regarding group count (number of licenses) and subusers (sub-licenses) - user
 * accounts that are tied together to a subscription. Created by peter on 01.12.16.
 */
public class LicenceService {
  CrmConverter crmConverter;
  CheckoutResponseHelper checkoutResponseHelper;

  public LicenceService(CrmConverter crmConverter, CheckoutResponseHelper checkoutResponseHelper) {
    this.crmConverter = crmConverter;
    this.checkoutResponseHelper = checkoutResponseHelper;
  }

  /**
   * Retrieve current state of license settings.
   *
   * @return licenceResponse.
   * @throws Exception crmException.
   */
  public LicenceResponse getLicenceData() throws Exception {
    ResponseGetCustomerData responseGetCustomerData = crmConverter.customerGetData(0);

    LicenceResponse licenceResponse = new LicenceResponse();
    licenceResponse.setAboPeriodDat(responseGetCustomerData.getSubscription().getEnd());
    if (responseGetCustomerData.getSubscriptionTransition() != null) {
      licenceResponse.setMaxPossibleSubUsersAfterTransition(
          responseGetCustomerData.getSubscriptionTransition().getMaxSubUsers());
      licenceResponse.setTransitionDate(
          responseGetCustomerData.getSubscriptionTransition().getChange());
    }
    LicenceDataResponse licenceDataResponse = new LicenceDataResponse();
    licenceDataResponse.setMaxUsers(responseGetCustomerData.getSubscription().getMaxSubUsers());

    List<SubUser> subUserList = crmConverter.multiuserListSubUsers();
    licenceDataResponse.setUserData(
        subUserList
            .stream()
            .map(
                subUser ->
                    new UserDataLicenceResponse(
                        subUser.getUserId(),
                        isUserRemoved(responseGetCustomerData, subUser),
                        subUser.getEmail()))
            .collect(Collectors.toList()));

    // TODO : find better way to get currency from (e.g. country), in case
    //    when there is no current invoice status
    Status invoiceStatus = responseGetCustomerData.getStatus();
    String currency = invoiceStatus != null ? invoiceStatus.getCurrency() : "";
    licenceResponse.setCurrency(currency);

    licenceResponse.setMaxLicences(
        getMaxSubUser(
            responseGetCustomerData.getSubscription().getCountry(),
            responseGetCustomerData.getSubscription().getProductId()));
    licenceResponse.setPrice(responseGetCustomerData.getSubscriptionType().getCostperGroup());
    licenceResponse.setLicenceData(licenceDataResponse);
    licenceResponse.setLicences(licenceDataResponse.getMaxUsers());
    return licenceResponse;
  }

  private Integer getMaxSubUser(String country, Integer productId) throws Exception {
    return crmConverter
        .masterGetSubscriptions(country)
        .getProducts()
        .stream()
        .filter(product -> product.getId() == productId)
        .collect(Collectors.toList())
        .get(0)
        .getMaxGroups();
  }

  private boolean isUserRemoved(ResponseGetCustomerData responseGetCustomerData, SubUser subUser) {
    if (responseGetCustomerData.getSubscriptionTransition() == null) {
      return false;
    }

    if (responseGetCustomerData.getSubscriptionTransition().getRemovedUsers() == null) {
      return false;
    }
    return responseGetCustomerData
        .getSubscriptionTransition()
        .getRemovedUsers()
        .containsKey(subUser.getUserId());
  }

  /**
   * Set number of licenses (group count).
   *
   * @param licences new number of licences for the subscription.
   * @return SetGroupCountResponse.
   * @throws Exception crmExeption.
   */
  public SetGroupCountResponseContainer updateNumberOfLicences(
      Integer licences, List<String> usersToDelete) throws Exception {

    SetGroupCountResponseContainer setGroupCountResponseContainer =
        crmConverter.transactionalSetGroupCount(licences, usersToDelete);

    checkoutResponseHelper.checkForCheckoutAndInitiate(setGroupCountResponseContainer);
    return setGroupCountResponseContainer;

    //    SubscriptionLicencesSaveResponse returner = new SubscriptionLicencesSaveResponse();
    //
    //    if (setGroupCountResponseContainer.getNeedsCheckout() == 1) {
    //      returner.setRedirectToCheckout(true);
    //      returner.setPaymentMethod(crmConverter.customerGetData(0).getPaymentOption());
    //    }
    //    return returner;
  }

  /**
   * Create users.
   *
   * @param subusers list of users to create
   * @return some status value
   * @throws Exception in case of error.
   */
  public Boolean createSubusers(List<Subuser> subusers) throws Exception {
    for (Subuser user : subusers) {
      crmConverter.multiuserCreateSubUser(user.getEmail(), user.getPassword());
    }
    return true;
  }

  /**
   * Changes the marked-for-deletion status.
   *
   * @param changes map of user->status that will be saved.
   * @throws Exception in case of error.
   */
  public void updateMarkForDeletionStatus(Map<Subuser, Boolean> changes) throws Exception {
    for (Map.Entry<Subuser, Boolean> entry : changes.entrySet()) {
      crmConverter.multiuserUserDeleteStatus(
          new UserDeleteStatusRequest(
              entry.getKey().getUserId(), entry.getKey().getEmail(), entry.getValue()));
    }
  }

  /**
   * Remove subusers.
   *
   * @param subuserList list of Subusers to be deleted.
   * @return true if ok, false if error.
   * @throws Exception crmException.
   */
  public Boolean removeSubusers(List<Subuser> subuserList) throws Exception {
    for (Subuser user : subuserList) {
      if (crmConverter.multiuserRemoveSubUser(user.getUserId()) == false) {
        return false;
      }
    }
    return true;
  }
}
