package com.kiga.memberadmin.web.request;

import lombok.Data;

/**
 * Created by peter on 27.09.16.
 */
@Data
public class PasswordUpdateRequest {
  private String newPassword;
  private String oldPassword;
}
