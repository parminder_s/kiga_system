package com.kiga.memberadmin.web.request;

import com.kiga.crm.messages.Address;
import lombok.Data;

/**
 * Created by peter on 02.11.16.
 */
@Data
public class UpdateAddressRequest {
  private Address billingAddress;
  private Address shippingAddress;
}
