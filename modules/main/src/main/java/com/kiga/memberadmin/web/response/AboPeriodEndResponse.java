package com.kiga.memberadmin.web.response;

import lombok.Data;

import java.util.Date;

/**
 * Created by peter on 19.10.16.
 */
@Data
public class AboPeriodEndResponse {
  Date aboPeriodEnd;
  Date dateOrdered;
}
