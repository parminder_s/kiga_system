package com.kiga.memberadmin.web.response;

import lombok.Data;

import java.util.Date;

/**
 * Created by peter on 01.12.16.
 * Holds a subscription's state regarding sub-users (licenses).
 * Contains the list of current sub users and the current number of
 * licenses as well as the future count of licenses (if a transition is
 * in place to reduce the licence count at the begin of next period).
 */
@Data
public class LicenceResponse {
  private Date aboPeriodDat;
  private LicenceDataResponse licenceData;  // the list of subusers
  private String currency;
  private Double price;
  private Integer maxLicences;
  private Integer licences;
  private Date transitionDate;
  private Integer maxPossibleSubUsersAfterTransition;
}
