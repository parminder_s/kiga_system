package com.kiga.memberadmin.web.response;

import lombok.Data;

import java.util.List;

/**
 * Created by peter on 27.09.16.
 */
@Data
public class LanguageResponse {
  String value;
  List<String> code;
}
