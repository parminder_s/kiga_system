package com.kiga.memberadmin.web.response;

import lombok.Data;

import java.util.Date;

/**
 * Created by peter on 16.11.16.
 */
@Data
public class InvoiceDetailResponse {
  private String status;
  private Double amount;
  private String invoiceNr;
  private String type;
  private String pdfUrl;
  private String currency;
  private Date date;

  /**
   * @param status    status (paid, unpaid).
   * @param amount    total gross amount.
   * @param invoiceNr invoice nr.
   * @param type      type of invoice (shop, abo).
   * @param pdfUrl    url to the pdf.
   * @param currency  currency.
   * @param date      invoice date.
   */
  public InvoiceDetailResponse(String status, Double amount, String invoiceNr,
                               String type, String pdfUrl, String currency, Date date) {
    this.status = status;
    this.amount = amount;
    this.invoiceNr = invoiceNr;
    this.type = type;
    this.pdfUrl = pdfUrl;
    this.currency = currency;
    this.date = date;
  }

  public InvoiceDetailResponse() {}
}
