package com.kiga.memberadmin.service;

import com.kiga.memberadmin.web.request.RequestUpdateSubUsers;
import com.kiga.memberadmin.web.request.Subuser;
import com.kiga.memberadmin.web.response.LicenceResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This object detects the change in the state of licences and group count
 * of a subscription.
 * It provides the lists of neccessary changes that have to be carried out on
 * the subscription on its subusers.
 */
public class LicencesDiffState {
  LicenceResponse currentState;
  RequestUpdateSubUsers newState;

  /**
   * Consturctor of licenses diff service.
   * @param currentState the current state of the subscription.
   * @param newState the new desired state of the subscription.
   */
  public LicencesDiffState(LicenceResponse currentState,
                           RequestUpdateSubUsers newState) {
    this.currentState = currentState;
    this.newState = newState;
  }

  /**
   * Whether transition is not required anymore (if any exists at all).
   * @return does the new change cancel out a currently scheduled change ?
   */
  public Boolean shouldResetTransition() {
    Integer currentNow = Optional.ofNullable(currentState.getLicences())
      .orElse(0);
    Integer currentLater = Optional.ofNullable(
      currentState.getMaxPossibleSubUsersAfterTransition()).orElse(currentNow);

    return (currentNow != currentLater) && currentNow == newState.getMaxUsers();
  }

  /**
   * Whether a transition regarding group count reduction is due.
   * @return is a transition necessary ?
   */
  public Boolean shouldCreateTransition() {
    Integer currentNow = Optional.ofNullable(currentState.getLicences())
      .orElse(0);
    Integer currentLater = Optional.ofNullable(
      currentState.getMaxPossibleSubUsersAfterTransition()).orElse(currentNow);

    return currentNow > newState.getMaxUsers()
      && (currentLater != newState.getMaxUsers());
  }

  /**
   * Increase in group count - how many new licenses to order (if any).
   * @return number of new license to order.
   */
  public int getAdditionalLicensesNum() {
    Integer currentNow = Optional.of(currentState.getLicences()).orElse(0);
    return Math.max(newState.getMaxUsers() - currentNow, 0);
  }

  /**
   * Get the list of users that actually need to be created.
   * @return List of users to be created minus the ones that already exist.
   */
  public List<Subuser> getUsersToCreate() {
    List<String> existingEmails = currentState.getLicenceData().getUserData()
      .stream()
      .map(v -> v.getEmail().toLowerCase())
      .collect(Collectors.toList());
    return Optional.of(newState.getUsers()).orElse(new ArrayList<Subuser>())
      .stream()
      .filter(v -> !existingEmails.contains(v.getEmail().toLowerCase()))
      .collect(Collectors.toList());
  }

  /**
   * List of users that actually need to be removed.
   * @return users to remove minus the ones that don't exist anyways.
   */
  public List<Subuser> getUsersToRemove() {
    List<String> existingEmails = currentState.getLicenceData().getUserData()
      .stream()
      .map(v -> v.getEmail().toLowerCase())
      .collect(Collectors.toList());
    return
      Optional.of(newState.getRemovedUsers()).orElse(new ArrayList<Subuser>())
        .stream()
        .filter(v -> existingEmails.contains(v.getEmail().toLowerCase()))
        .collect(Collectors.toList());
  }

  /**
   * Determine for which users the 'marked-for-deletion' status needs
   * to be changed.
   * @return a map Subuser->Boolean of changes in deletion-status.
   */
  public Map<Subuser, Boolean> getUsersToChangeDeltionMark() {
    Map<String, Boolean> currentDeletionsMap =
      currentState.getLicenceData().getUserData()
      .stream()
      .collect(Collectors.toMap(
        v -> v.getEmail().toLowerCase(),
        v -> v.isRemoved()));

    return
      Optional.of(newState.getUsers()).orElse(new ArrayList<Subuser>())
      .stream()
      .filter(v ->
        (!currentDeletionsMap.containsKey(v.getEmail().toLowerCase())
          && v.isRemoved())
        || (currentDeletionsMap.containsKey(v.getEmail().toLowerCase())
            && currentDeletionsMap.get(v.getEmail().toLowerCase())
          != v.isRemoved()))
      .collect(Collectors.toMap(v -> v, v -> v.isRemoved()));
  }

  /**
   * Get email of users marked for deletion.
   * @return list of user's emails marked for deletion.
   */
  public List<String> getAllUsersMarkedForDeletion() {
    return newState.getUsers()
      .stream()
      .filter(v -> v.isRemoved())
      .map(v -> v.getEmail())
      .collect(Collectors.toList());

  }
}
