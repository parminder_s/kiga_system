package com.kiga.memberadmin.web.request;

import lombok.Data;

@Data
public class SetPaymentRequest {
  String paymentMethod;
  String invoiceOption;
}
