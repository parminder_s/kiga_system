package com.kiga.memberadmin.web.response;

import com.kiga.crm.messages.Address;
import com.kiga.s3.domain.S3Image;
import lombok.Data;

import java.util.Date;

/**
 * Created by peter on 27.09.16.
 */
@Data
public class ProfileResponse {
  private Address address;
  private Date birthday;
  private int minimumAge;
  private String nickname;
  private S3Image avatar;
}
