package com.kiga.memberadmin.web.response;

import lombok.Data;

/**
 * Created by peter on 27.09.16.
 */
@Data
public class AccountResponse {
  private String customerId;
  private boolean org;
  private String currentBillState;
  private String currentBillUrl;
  private String currentBillId;
}
