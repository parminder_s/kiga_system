package com.kiga.memberadmin.web.response;

import lombok.Data;

/**
 * Created by peter on 01.12.16.
 */
@Data
public class UserDataLicenceResponse {
  private int userId;
  private boolean removed;
  private String email;

  public UserDataLicenceResponse() {}

  /**
   *
   * @param userId id of the subUser.
   * @param removed if the subUser should be removed.
   * @param email email of the subUser.
   */
  public UserDataLicenceResponse(int userId, boolean removed, String email) {
    this.userId = userId;
    this.removed = removed;
    this.email = email;
  }
}
