package com.kiga.memberadmin.web.request;

import lombok.Data;

/**
 * Created by peter on 09.11.16.
 */
@Data
public class SubscriptionDurationSaveRequest {
  private String changeTime;
  private int productId;
}

