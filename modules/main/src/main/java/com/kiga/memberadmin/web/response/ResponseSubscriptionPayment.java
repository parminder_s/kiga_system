package com.kiga.memberadmin.web.response;

import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.memberadmin.domain.InvoiceOption;
import com.kiga.payment.PaymentType;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by peter on 09.11.16.
 */
@Data
public class ResponseSubscriptionPayment {
  private PaymentMethod paymentMethod;
  private List<PaymentType> paymentMethodList;
  private InvoiceOption invoiceOption;
  private List<InvoiceOption> invoiceOptionList;

  public ResponseSubscriptionPayment() {
    this.paymentMethodList = new ArrayList<>();
    this.invoiceOptionList = new ArrayList<>();
  }
}
