package com.kiga.memberadmin;

import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.crm.service.CrmConverter;
import com.kiga.memberadmin.service.AccountService;
import com.kiga.memberadmin.service.CheckoutResponseHelper;
import com.kiga.memberadmin.service.LicenceService;
import com.kiga.memberadmin.service.ProfileService;
import com.kiga.payment.PaymentInitiator;
import com.kiga.security.repository.MemberRepository;
import com.kiga.shop.service.ShopInvoiceService;
import com.kiga.translation.TranslationProperties;
import com.kiga.upload.factory.UploadFactory;
import com.kiga.web.service.UrlGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MemberAdminConfig {
  @Bean
  AccountService mySubscriptionService(
      CrmConverter crmConverter,
      ShopInvoiceService shopInvoiceService,
      InvoiceRepository invoiceRepository) {
    return new AccountService(crmConverter, shopInvoiceService, invoiceRepository);
  }

  @Bean
  ProfileService profileService(
      CrmConverter crmConverter,
      TranslationProperties translationProperties,
      MemberRepository memberRepository,
      SiteTreeLiveRepository siteTreeLiveRepository,
      UploadFactory uploadFactory,
      com.kiga.forum.service.ProfileService profileService) {
    return new ProfileService(
        crmConverter,
        translationProperties,
        memberRepository,
        siteTreeLiveRepository,
        uploadFactory,
        profileService);
  }

  @Bean
  LicenceService licenceService(
      CrmConverter crmConverter, CheckoutResponseHelper checkoutResponseHelper) {
    return new LicenceService(crmConverter, checkoutResponseHelper);
  }

  @Bean
  CheckoutResponseHelper checkoutResponseHelper(
      UrlGenerator urlGenerator, PaymentInitiator paymentInitiator) {
    return new CheckoutResponseHelper(urlGenerator, paymentInitiator);
  }
}
