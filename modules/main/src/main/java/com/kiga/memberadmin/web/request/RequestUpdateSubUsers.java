package com.kiga.memberadmin.web.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by peter on 02.12.16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestUpdateSubUsers {
  private List<Subuser> users; //new Subusers
  private List<Subuser> removedUsers;
  private int maxUsers;
}
