package com.kiga.memberadmin.web.request;

import com.kiga.web.message.EndpointRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 02.11.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CheckEmailRequest extends EndpointRequest {
  private String type;
  private String value;
}
