package com.kiga.memberadmin.web.request;

import lombok.Data;

/**
 * Created by peter on 25.10.16.
 */
@Data
public class Email {
  private String email;
}
