package com.kiga.memberadmin.web.response;

import com.kiga.main.paymentmethod.PaymentMethod;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by peter on 04.09.17.
 */
@Data
@NoArgsConstructor
public class SubscriptionLicencesSaveResponse {
  Boolean redirectToCheckout;
  PaymentMethod paymentMethod;
}
