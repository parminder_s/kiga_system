package com.kiga.memberadmin.web.response;

import com.kiga.crm.messages.response.Product;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Created by peter on 19.10.16.
 */
@Data
public class SubscriptionDurationResponse {
  private Integer productId;
  private List<String> changeTimeList;
  private Date changeTime;
  private List<Product> products;
  private Date aboPeriodEnd;
}
