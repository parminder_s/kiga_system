package com.kiga.memberadmin.service;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.crm.messages.Address;
import com.kiga.crm.messages.response.ResponseGetCustomerData;
import com.kiga.crm.messages.response.ResponseGetSubscriptions;
import com.kiga.crm.service.CrmConverter;
import com.kiga.memberadmin.domain.AddressType;
import com.kiga.memberadmin.web.request.Email;
import com.kiga.memberadmin.web.request.ProfileAdditionalRequest;
import com.kiga.memberadmin.web.response.LanguageResponse;
import com.kiga.memberadmin.web.response.ProfileAdditionalResponse;
import com.kiga.memberadmin.web.response.ProfileResponse;
import com.kiga.s3.domain.S3Image;
import com.kiga.security.domain.Member;
import com.kiga.security.repository.MemberRepository;
import com.kiga.translation.TranslationProperties;
import com.kiga.upload.UploadParameters;
import com.kiga.upload.factory.AbstractFileUploader;
import com.kiga.upload.factory.UploadFactory;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by peter on 27.09.16.
 */
public class ProfileService {
  CrmConverter crmConverter;

  TranslationProperties translationProperties;

  MemberRepository memberRepository;

  SiteTreeLiveRepository siteTreeLiveRepository;

  UploadFactory uploadFactory;

  private com.kiga.forum.service.ProfileService profileService;
  /**
   *
   * @param crmConverter crmConverter.
   * @param translationProperties translationProperties.
   * @param memberRepository memberRepository.
   * @param siteTreeLiveRepository siteTreeLiveRepository.
   */

  public ProfileService(CrmConverter crmConverter,
                        TranslationProperties translationProperties,
                        MemberRepository memberRepository,
                        SiteTreeLiveRepository siteTreeLiveRepository,
                        UploadFactory uploadFactory,
                        com.kiga.forum.service.ProfileService profileService) {
    this.crmConverter = crmConverter;
    this.translationProperties = translationProperties;
    this.memberRepository = memberRepository;
    this.siteTreeLiveRepository = siteTreeLiveRepository;
    this.uploadFactory = uploadFactory;
    this.profileService = profileService;
  }

  /**
   *
   * @return profileResponse.
   * @throws Exception crmException.
   */
  public ProfileResponse getProfile(Member member) throws Exception {
    ResponseGetCustomerData responseGetCustomerData = crmConverter.customerGetData(0);
    ProfileResponse profileResponse = new ProfileResponse();
    profileResponse.setAddress(responseGetCustomerData.getShippingAddress());
    profileResponse.setBirthday(responseGetCustomerData.getDateOfBirth());
    profileResponse.setAvatar(new S3Image());
    profileResponse.getAvatar().setUrl((profileService.getUrl(member)));
    profileResponse.setNickname(member.getNickname());
    if (Objects.equals(profileResponse.getAvatar().getUrl(), "")
      || Objects.equals(profileResponse.getAvatar().getUrl(), null)) {
      profileResponse.setAvatar(null);
    }
    ResponseGetSubscriptions asdf = crmConverter.masterGetSubscriptions(member.getCountry());
    profileResponse.setMinimumAge(crmConverter.masterGetSubscriptions(member.getCountry())
      .getAgeLimit());
    return profileResponse;
  }

  /**
   *
   * @param member current member.
   * @param locale new local.
   * @return 1 if ok, 0 if error.
   * @throws Exception crm Exception.
   */
  public Integer updateLocale(Member member, String locale) throws Exception {
    member.setLocale(locale);
    memberRepository.save(member);
    return crmConverter.userSetLocale(locale);
  }

  /**
   *
   * @param member current member.
   * @param address new Shipping address.
   * @param birhday new birthday.
   * @param nickname new nickname.
   * @param avatar new avatar image.
   * @return true if ok, false if error.
   * @throws Exception crm Exception.
   */
  public Boolean updateProfile(Member member, Address address, Date birhday, String nickname,
                               S3Image avatar)
    throws Exception {
    Member mem = memberRepository.findOne(member.getId());
    mem.setNickname(nickname);
    mem.setS3Avatar(avatar);
    memberRepository.save(mem);

    // TODO: could be passed on to crm via `user.setProfile` as well

    return true;
  }

  /**
   *
   * @param file image file.
   * @param member member who uploads.
   * @return uploaded S3Image.
   */
  public S3Image uploadS3Image(MultipartFile file, Member member) throws Exception {
    AbstractFileUploader fileUploader = uploadFactory.getUploaderInstance(file, member);
    UploadParameters uploadParameters = new UploadParameters();
    uploadParameters.setCannedAccessControlList(CannedAccessControlList.PublicRead);
    uploadParameters.setImageVariants(new String[0]);
    fileUploader.setUploadParameters(uploadParameters);
    return (S3Image)fileUploader.upload();
  }

  /**
   *
   * @param member current member.
   * @return current language from user.
   */
  public LanguageResponse languageResponse(Member member) {
    LanguageResponse languageResponse = new LanguageResponse();
    languageResponse.setCode(translationProperties.getLocales().entrySet().stream()
      .map(e -> e.getKey().toString().toUpperCase()).collect(Collectors.toList()));
    languageResponse.setValue(member.getLocale().toUpperCase());
    return languageResponse;
  }

  /**
   *
   * @return ProfileAdditionalResponse
   * @throws Exception crm Exception.
   */
  public ProfileAdditionalResponse profileAdditional() throws Exception {
    ProfileAdditionalResponse additionalResponse = new ProfileAdditionalResponse();
    ResponseGetCustomerData responseGetCustomerData = crmConverter.customerGetData(0);
    additionalResponse.setJob(responseGetCustomerData.getJob());
    additionalResponse.setOrganization(responseGetCustomerData.getOrgType());
    additionalResponse.setJobName(responseGetCustomerData.getJobName());
    return additionalResponse;
  }


  /**
   * Save additional profile information (which job).
   * @param request the request data.
   * @return the result.
   * @throws Exception in case of crm exception.
   */
  public Boolean profileAdditionalSave(ProfileAdditionalRequest request)
    throws Exception {
    crmConverter.customerSetOptions(request.getJob(), request.getOrganization(),
      request.getJobName());
    return true;
  }

  /**
   *
   * @param member current member.
   * @return email of the member;
   */
  public Email findEmail(Member member) {
    Email email = new Email();
    email.setEmail(memberRepository.findOne(member.getId()).getEmail());
    return email;
  }


  public Boolean checkEmail(String email) throws Exception {
    return crmConverter.userTestUserName(email);
  }

  /**
   *
   * @param member current member.
   * @param email new email.
   * @return true if ok, false if error.
   * @throws Exception crm Exception.
   */
  public Boolean updateEmail(Member member, String email) throws Exception {
    Member mem = memberRepository.findOne(member.getId());
    mem.setEmail(email);
    memberRepository.save(mem);
    return crmConverter.userSetLogin(email);
  }

  /**
   *
   * @param newPassword new password.
   * @param oldPassword old password.
   * @return true if ok, false if error.
   * @throws Exception crm Exception.
   */
  public Boolean updatePassword(String newPassword, String oldPassword) throws Exception {
    if (!crmConverter.userTestPassword(oldPassword)) {
      throw new Exception("Old Password Wrong");
    }
    return crmConverter.userSetPassword(newPassword);
  }
}
