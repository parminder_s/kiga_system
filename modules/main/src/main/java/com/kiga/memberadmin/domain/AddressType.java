package com.kiga.memberadmin.domain;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Flag to describe subscription address as billing or shipping.
 * Created by mfit on 07.12.17.
 */
public enum AddressType {
  SHIPPING(0), BILLING(1);

  private int code;

  private AddressType(int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }

  @JsonValue
  public int toValue() {
    return ordinal();
  }
}
