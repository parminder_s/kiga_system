package com.kiga.web.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rainerh on 23.11.16.
 *
 * <p>Controller that is required in dev mode for endpoints that are answered by the legacy
 * system in the production system.</p>
 */
@RestController
public class MockController {
  /**
   * used in the permission screens for showing the cheapest price along duration.
   */
  @RequestMapping("/api/nregistration/cheapestPrice")
  public Map<String, Object> getCheapestPrice() {
    Map<String, Object> returner = new HashMap<String, Object>();
    returner.put("currency", "EUR");
    returner.put("price", 7.9);
    returner.put("duration", 1);

    return returner;
  }
}
