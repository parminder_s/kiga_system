package com.kiga.web.request;

import lombok.Data;

import java.util.List;

/**
 * @author bbs
 * @since 4/8/17.
 */
@Data
public class IdListRequest {
  private List<Long> ids;
}
