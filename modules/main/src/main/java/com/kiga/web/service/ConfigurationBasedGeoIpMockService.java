package com.kiga.web.service;

import org.apache.commons.lang3.NotImplementedException;

/**
 * @author bbs
 * @since 9/29/16.
 */
public class ConfigurationBasedGeoIpMockService implements GeoIpService {
  private String mockedCountry;

  /**
   * Just a constructor with deps.
   *
   * @param mockedCountry mocked country
   */
  public ConfigurationBasedGeoIpMockService(String mockedCountry) {
    this.mockedCountry = mockedCountry;
  }

  @Override
  public String resolveCountryCode() {
    return mockedCountry;
  }

  @Override
  public void changeCountryCode(String newCountryCode) {
    this.mockedCountry = newCountryCode;
  }

  @Override
  public void removeCountryCode() {
    throw new NotImplementedException("not available for this class");
  }
}
