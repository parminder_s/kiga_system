package com.kiga.web.converter;

import java.util.List;

/**
 * @author bbs
 * @since 10/16/16.
 */
public interface ViewModelConverter<E, R> extends ResponseConverter<E, R> {
  List<R> convertToResponse(List<? extends E> entity);

  List<R> convertToResponse(Iterable<? extends E> entity);
}
