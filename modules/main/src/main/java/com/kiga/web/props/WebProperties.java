package com.kiga.web.props;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** Created by rainerh on 04.10.16. */
@Configuration
@ConfigurationProperties("web")
@Data
public class WebProperties {
  private String baseUrl;
  private String url;
  private String ngUrl;
  private String cacheUrl;
  private boolean mockMemcached;
  private String cacheType;
  private String systemName;
  private String ipHttpHeaderName;
  private String geoIp2CountryDatabase;
  private String mockedCountry;
  private String userIpResolverMock;
  @Autowired private WebSecurityProperties webSecurityProperties;
}
