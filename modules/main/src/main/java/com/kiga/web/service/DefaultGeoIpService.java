package com.kiga.web.service;

import org.apache.commons.lang3.NotImplementedException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 9/29/16.
 */
public class DefaultGeoIpService implements GeoIpService {
  private final LocateByIpResolver locateByIpResolver;
  private final UserIpService userIpService;
  private HttpServletRequest httpServletRequest;

  /**
   * Just a constructor with deps.
   *
   * @param locateByIpResolver LocateByIpResolver
   * @param httpServletRequest httpServletRequest
   */
  public DefaultGeoIpService(LocateByIpResolver locateByIpResolver, UserIpService userIpService,
    HttpServletRequest httpServletRequest) {
    this.userIpService = userIpService;
    this.locateByIpResolver = locateByIpResolver;
    this.httpServletRequest = httpServletRequest;
  }

  @Override
  public String resolveCountryCode() {
    String ip = userIpService.getUserIp(httpServletRequest);
    return locateByIpResolver.getCountryIsoCode(ip);
  }

  @Override
  public void changeCountryCode(String newCountryCode) {
    throw new NotImplementedException("");
  }

  @Override
  public void removeCountryCode() {
    throw new NotImplementedException("");
  }
}
