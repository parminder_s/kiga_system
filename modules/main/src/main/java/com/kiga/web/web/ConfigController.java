package com.kiga.web.web;

import com.kiga.FeaturesProperties;
import com.kiga.web.web.response.ConfigResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigController {
  private FeaturesProperties featuresProperties;

  @Autowired
  public ConfigController(FeaturesProperties featuresProperties) {
    this.featuresProperties = featuresProperties;
  }

  /** returns the configuration properties required for the frontend. */
  @RequestMapping(value = "/config", method = RequestMethod.GET)
  public ConfigResponse getConfig() {
    return ConfigResponse.builder()
        .angularCmsEnabled(featuresProperties.isAngularCmsEnabled())
        .kgaEnabled(featuresProperties.isKgaEnabled())
        .build();
  }
}
