package com.kiga.web.service;

/**
 * Created by rainerh on 10.04.16.
 */
public interface UrlGenerator {
  String getBaseUrl(String urlPart);

  String getNgUrl(String urlPart);

  String getUrl(String urlPart);
}
