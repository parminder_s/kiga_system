package com.kiga.web.service;

/**
 * Created by rainerh on 10.04.16.
 *
 * <p>Helper class that generates Url for three use cases:</p>
 * <ul>
 *   <li>baseUrl: Url of the website, e.g. www.kigaportal.com</li>
 *   <li>url: Url of the Spring application. In production mode the load balancer</li>
 *   <li>hides the Spring application behind the /endpoint path, which is not the
 *   case in development mode.</li>
 *   <li>ngUrl: Direct entrypoint to the AngularJs application.</li>
 * </ul>
 */
public class DefaultUrlGenerator implements UrlGenerator {
  private String baseUrl;
  private String url;
  private String ngUrl;
  private String slash = "/";

  /**
   * injection of the configuration parameters.
   */
  public DefaultUrlGenerator(String baseUrl, String url, String ngUrl) {
    this.baseUrl = baseUrl;
    this.url = url;
    this.ngUrl = ngUrl;
  }

  @Override
  public String getBaseUrl(String urlPart) {
    return create(baseUrl, urlPart);
  }

  @Override
  public String getNgUrl(String urlPart) {
    return create(ngUrl, urlPart);
  }

  @Override
  public String getUrl(String urlPart) {
    return create(url, urlPart);
  }

  private String create(String base, String urlPart) {
    if (base.endsWith(slash) && urlPart.startsWith(slash)) {
      return base + urlPart.substring(1);
    } else if (!base.endsWith(slash) && !urlPart.startsWith(slash)) {
      return base + slash + urlPart;
    } else {
      return base + urlPart;
    }
  }
}
