package com.kiga.web.web.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfigResponse {
  private boolean angularCmsEnabled;
  private boolean kgaEnabled;
}
