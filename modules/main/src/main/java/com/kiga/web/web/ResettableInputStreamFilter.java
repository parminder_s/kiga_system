package com.kiga.web.web;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by rainerh on 09.08.16.
 *
 * <p>This filter is required for scenarios where one requires the input
 * stream even after it is closed.
 *
 * <p>This can be in @WebExceptionHandler that catches all exceptions and
 * logs the original http request along the sent data. If the original
 * controller method is consuming the InputStream by using @RequestBody
 * it would not be possible for the controlleradvice to access that stream.
 *
 * <p>This filter uses the @ResettableInputStreamReuestWrapper that stores
 * the inputstream as stream and therefore allows to use it multiple times.
 */
public class ResettableInputStreamFilter implements Filter {
  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
    throws IOException, ServletException {
    chain.doFilter(new ResettableInputStreamRequestWrapper((HttpServletRequest)request), response);
  }

  @Override
  public void destroy() {

  }
}
