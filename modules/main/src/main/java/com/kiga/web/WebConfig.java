package com.kiga.web;

import com.fasterxml.jackson.module.scala.DefaultScalaModule;
import com.kiga.main.service.CookieService;
import com.kiga.util.ResourceLoader;
import com.kiga.web.props.WebProperties;
import com.kiga.web.security.session.CachedSession;
import com.kiga.web.security.session.DefaultSession;
import com.kiga.web.security.session.Session;
import com.kiga.web.service.ConfigurationBasedGeoIpMockService;
import com.kiga.web.service.CookieBasedGeoIpMockService;
import com.kiga.web.service.DefaultGeoIpService;
import com.kiga.web.service.DefaultUrlGenerator;
import com.kiga.web.service.GeoIp2DatabaseResolver;
import com.kiga.web.service.GeoIpService;
import com.kiga.web.service.LocateByIpResolver;
import com.kiga.web.service.UrlGenerator;
import com.kiga.web.service.UserIpService;
import com.maxmind.geoip2.DatabaseReader;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;
import scala.concurrent.ExecutionContextExecutor;

import java.io.IOException;
import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;

@Configuration
public class WebConfig {

  @Bean
  public DispatcherServlet getDispatcherServlet() {
    return new DispatcherServlet();
  }

  @Bean
  public DefaultScalaModule getScalaJackson() {
    return new DefaultScalaModule();
  }

  @Bean
  public UrlGenerator getUrlGenerator(WebProperties webProperties) {
    return new DefaultUrlGenerator(
        webProperties.getBaseUrl(), webProperties.getUrl(), webProperties.getNgUrl());
  }

  @Bean
  public Session getSession(ExecutionContextExecutor executor, CachedSession cachedSession) {
    return new DefaultSession(executor, cachedSession);
  }

  @Profile("!test")
  @Bean
  public DelegatingFilterProxy getDelegatingProxyFilter() {
    return new DelegatingFilterProxy("springSecurityFilterChain");
  }

  /** registers Filters and sets name of SessionID. */
  @Bean
  public ServletContextInitializer getServletContextInitializer(WebProperties webProperties) {
    return new KigaServletContextInitializer(webProperties);
  }

  @Bean
  public ConfigurationBasedGeoIpMockService createConfigurationBasedGeoIpMockService(
      WebProperties webProperties) {
    return new ConfigurationBasedGeoIpMockService(webProperties.getMockedCountry());
  }

  /**
   * Create instance of GeoIpService.
   *
   * @param locateByIpResolver resolver
   * @param userIpService ip service
   * @param webProperties properties
   * @return geoipservice instance
   */
  @Bean
  @Primary
  @Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, scopeName = WebApplicationContext.SCOPE_REQUEST)
  public GeoIpService getGeoIpService(
      ConfigurationBasedGeoIpMockService configurationBasedGeoIpMockService,
      LocateByIpResolver locateByIpResolver,
      UserIpService userIpService,
      WebProperties webProperties,
      HttpServletRequest httpServletRequest,
      CookieService cookieService) {
    if ("configuration".equalsIgnoreCase(webProperties.getUserIpResolverMock())) {
      return configurationBasedGeoIpMockService;
    }

    DefaultGeoIpService defaultGeoIpService =
        new DefaultGeoIpService(locateByIpResolver, userIpService, httpServletRequest);

    if ("cookie".equalsIgnoreCase(webProperties.getUserIpResolverMock())) {
      return new CookieBasedGeoIpMockService(defaultGeoIpService, cookieService);
    }

    return defaultGeoIpService;
  }

  /** GeoIp with inputstream to database. */
  @Bean
  public GeoIp2DatabaseResolver getGeoIp2DatabaseResolver(WebProperties webProperties)
      throws IOException {
    InputStream databaseStream =
        ResourceLoader.getResource(webProperties.getGeoIp2CountryDatabase());
    DatabaseReader reader = new DatabaseReader.Builder(databaseStream).build();
    return new GeoIp2DatabaseResolver(reader);
  }

  /** conditions IpService. */
  @Bean
  public UserIpService getUserIpService(WebProperties webProperties) {
    return new UserIpService(webProperties.getIpHttpHeaderName());
  }
}
