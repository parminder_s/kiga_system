package com.kiga.web.security;

import static com.kiga.web.security.KigaRoles.ADMIN;
import static com.kiga.web.security.KigaRoles.SHOP_ADMIN;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.security.services.DefaultKigaSessionFetcher;
import com.kiga.security.services.KigaSessionFetcher;
import com.kiga.security.services.MockKigaSessionFetcher;
import com.kiga.security.spring.KigaSecurityContextRepository;
import com.kiga.web.props.WebProperties;
import com.kiga.web.props.WebSecurityCacheProperties;
import com.kiga.web.security.session.CachedSecurityContextRepository;
import com.kiga.web.security.session.CachedSession;
import com.kiga.web.security.session.MockedSession;
import com.kiga.web.security.session.RedisSession;
import com.kiga.web.security.session.Session;
import com.lambdaworks.redis.RedisClient;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import redis.clients.jedis.JedisShardInfo;

/** Created by rainerh on 01.11.16. */
@Configuration
@EnableWebSecurity
@ConfigurationProperties("security")
@Getter
@Setter
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired private WebProperties webProperties;
  @Autowired private Session session;
  @Autowired private AuthenticationProvider kigaAuthenticationProvider;
  @Autowired private CachedSecurityContextRepository cachedSecurityContextRepository;
  @Autowired private UserDetailsService kigaUsersDetailService;

  /** create the sessionFetcher. */
  @Bean
  public KigaSessionFetcher kigaSessionFetcher(ObjectMapper objectMapper) {
    WebSecurityCacheProperties cacheProperties =
        webProperties.getWebSecurityProperties().getCacheProperties();
    if (cacheProperties.getType().equals("mocked")) {
      return new MockKigaSessionFetcher();
    } else {
      JedisConnectionFactory connectionFactory =
          new JedisConnectionFactory(
              new JedisShardInfo(cacheProperties.getUrl(), cacheProperties.getPort()));
      return new DefaultKigaSessionFetcher(connectionFactory, objectMapper);
    }
  }

  @Bean
  public CachedSession getCachedSession(WebSecurityCacheProperties properties) {
    if (properties.getType().equals("mocked")) {
      return new MockedSession();
    } else {
      String url = "redis://" + properties.getUrl() + ":" + properties.getPort();
      return new RedisSession(RedisClient.create(url).connect());
    }
  }

  @Bean
  public HttpSessionSecurityContextRepository getSecurityContextRepository() {
    return new SecurityContextRepositoryWithoutSessionCreation();
  }

  @Override
  public void configure(HttpSecurity httpSecurity) throws Exception {
    httpSecurity
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.NEVER)
        .and()
        .csrf()
        .disable()
        .authorizeRequests()
        .antMatchers("/backend/accounting/**")
        .hasAnyRole(SHOP_ADMIN, ADMIN)
        .antMatchers("/api/shop/check-payments")
        .hasAnyRole(SHOP_ADMIN, ADMIN)
        .antMatchers("/backend/invoices/**")
        .hasAnyRole(SHOP_ADMIN, ADMIN)
        .antMatchers("/backend/payment-transactions/**")
        .hasAnyRole(SHOP_ADMIN, ADMIN)
        .antMatchers("/shop/backend/**")
        .hasAnyRole(SHOP_ADMIN, ADMIN)
        .antMatchers("/**")
        .permitAll()
        .and()
        .securityContext()
        .securityContextRepository(new KigaSecurityContextRepository(session));
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(kigaUsersDetailService)
        .and()
        .authenticationProvider(kigaAuthenticationProvider);
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
  }
}
