package com.kiga.web.message;

import lombok.Builder;
import lombok.Data;

/**
 * Created by rainerh on 12.04.16.
 */
@Data
public class RedirectResponse {
  private String redirect;

  public RedirectResponse() { }

  public RedirectResponse(String redirect) {
    this.redirect = redirect;
  }
}
