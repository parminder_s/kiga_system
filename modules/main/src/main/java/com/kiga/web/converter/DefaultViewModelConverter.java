package com.kiga.web.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 10/16/16.
 */
public abstract class DefaultViewModelConverter<E, R> implements ViewModelConverter<E, R> {
  @Override
  public List<R> convertToResponse(List<? extends E> entities) {
    if (entities == null) {
      return null;
    }
    return entities.stream().map(this::convertToResponse).collect(Collectors.toList());
  }

  @Override
  public List<R> convertToResponse(Iterable<? extends E> iterable) {
    List<E> listFromIterable = new ArrayList<>();
    for (E entity : iterable) {
      listFromIterable.add(entity);
    }
    return convertToResponse(listFromIterable);
  }
}
