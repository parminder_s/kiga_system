package com.kiga.web.web.request;

import lombok.Data;

/**
 * @author bbs
 * @since 12/28/16.
 */
@Data
public class SwitchCountryRequest {
  private String newCountryCode;
}
