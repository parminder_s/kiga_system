package com.kiga.web.security.session;

import com.lambdaworks.redis.api.StatefulRedisConnection;
import org.apache.commons.lang3.NotImplementedException;

/**
 * Created by rainerh on 03.05.16.
 */
public class RedisSession implements CachedSession {
  private StatefulRedisConnection<String, String> statefulRedisConnection;

  public RedisSession(StatefulRedisConnection statefulRedisConnection) {
    this.statefulRedisConnection = statefulRedisConnection;
  }

  @Override
  public String get(String key) {
    return statefulRedisConnection.sync().get(key);
  }
}
