package com.kiga.web.service;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CountryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;

/**
 * @author bbs
 * @since 9/29/16.
 */
public class GeoIp2DatabaseResolver implements LocateByIpResolver {
  private final Logger logger = LoggerFactory.getLogger(getClass());
  private DatabaseReader reader;

  /**
   * Constructor initiates GeoipResolver.
   */
  public GeoIp2DatabaseResolver(DatabaseReader reader) {
    this.reader = reader;
  }


  @Override
  public String getCountryIsoCode(String ip) {
    try {
      InetAddress ipAddress = InetAddress.getByName(ip);
      CountryResponse response = reader.country(ipAddress);
      return response.getCountry().getIsoCode();
    } catch (IOException | NullPointerException | GeoIp2Exception exception) {
      logger.error("Couldn't resolve country by ip.", exception);
      return null;
    }
  }
}
