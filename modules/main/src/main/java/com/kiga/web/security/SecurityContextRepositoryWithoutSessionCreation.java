package com.kiga.web.security;

import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

/**
 * Created by rainerh on 02.11.16.
 */
public class SecurityContextRepositoryWithoutSessionCreation
  extends HttpSessionSecurityContextRepository {
  public SecurityContextRepositoryWithoutSessionCreation() {
    this.setAllowSessionCreation(false);
  }
}
