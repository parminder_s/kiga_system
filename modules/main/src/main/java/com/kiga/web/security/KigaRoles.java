package com.kiga.web.security;

import static java.util.Arrays.asList;

import java.util.List;

public class KigaRoles {
  public static final String ADMIN = "administrators";
  public static final String SHOP_ADMIN = "shop-administrators";

  public static final List<String> availableRoles = asList(ADMIN, SHOP_ADMIN);
}
