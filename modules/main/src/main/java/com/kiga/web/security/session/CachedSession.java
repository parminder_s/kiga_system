package com.kiga.web.security.session;

/**
 * Created by rainerh on 03.05.16.
 */
public interface CachedSession {
  String get(String key);
}
