package com.kiga.web.web.response;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class BootstrapInformationResponse {
  private ConfigResponse configResponse;
  private Map<String, Object> bootstrap; // in klassen aufteilen!
  private Map locales;
}
