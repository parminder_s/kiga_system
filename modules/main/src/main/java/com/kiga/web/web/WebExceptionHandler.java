package com.kiga.web.web;

import com.kiga.crm.service.AuthenticationException;
import com.kiga.security.exception.LoginRequiredException;
import com.kiga.security.exception.ParallelLoginException;
import com.kiga.security.exception.RoleException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by rainerh on 08.08.16.
 */
@ControllerAdvice
@ConfigurationProperties("web")
@Configuration
public class WebExceptionHandler {
  /**
   * Global catcher for all exceptions that are thrown within the application.
   * A proper email is sent out and the user  gets an error message.
   */
  @ExceptionHandler(Exception.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public Map<String, Object> handle(Exception exception, HttpServletRequest request) {
    logException(exception, request);

    HashMap<String, Object> returner = new HashMap<>();
    returner.put("status", false);
    return returner;
  }

  /**
   * handler for RoleException.
   */
  @ExceptionHandler(RoleException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public Map<String, Object> handleAdminRoleException(Exception exception,
                                                      HttpServletRequest request) {
    logException(exception, request);

    HashMap<String, Object> returner = new HashMap<>();
    returner.put("status", false);
    returner.put("errorCode", "permission_server_" + ((RoleException) exception).getRole());
    return returner;
  }



  /**
   * handler for securityexception.
   */
  @ExceptionHandler(LoginRequiredException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public Map<String, Object> handleLoginRequiredException(Exception exception, HttpServletRequest
    request) {
    logException(exception, request);

    HashMap<String, Object> returner = new HashMap<>();
    returner.put("status", false);
    returner.put("errorCode", ((LoginRequiredException)exception).getCode());
    return returner;
  }

  /**
   * A handler for authentication exceptions happening at the backend.
   * We want to communicate a specific error code (forbidden).
   * @param exception the exception.
   * @param request the request.
   * @return the response object.
   */
  @ExceptionHandler(AuthenticationException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.FORBIDDEN)
  public Map<String, Object> handleCrmAuthException(Exception exception, HttpServletRequest
    request) {
    // logException(exception, request);
    HashMap<String, Object> returner = new HashMap<>();
    returner.put("status", false);
    returner.put("errorCode", "login_required");
    return returner;
  }

  /**
   * handler for securityexception.
   */
  @ExceptionHandler(ParallelLoginException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public Map<String, Object> handleMultipleLoginException(Exception exception,
                                                          HttpServletRequest request) {
    HashMap<String, Object> returner = new HashMap<>();
    returner.put("status", false);
    returner.put("errorCode", ((ParallelLoginException)exception).getCode());
    returner.put("entryId", ((ParallelLoginException)exception).getEntryId());
    return returner;
  }

  private void logException(Exception exception, HttpServletRequest request) {
    StringWriter sw = new StringWriter();

    if (request != null) {
      sw.append("URL: " + request.getRequestURL() + "\n");
      sw.append("HTML Body: ");

      try {
        IOUtils.copy(request.getInputStream(), sw);
      } catch (IOException ioe) {
        ioe.printStackTrace();
      }
      sw.append("\n");
    }

    sw.append("Cookies: \n");
    Cookie[] cookies = request.getCookies();

    if (cookies != null) {
      for (Cookie tmp: cookies) {
        sw.append(tmp.getName());
        sw.append((": "));
        sw.append(tmp.getValue());
        sw.append("\n");
      }
    }

    exception.printStackTrace(new PrintWriter(sw));
    Logger logger = LoggerFactory.getLogger(getClass());
    logger.error(sw.toString(), exception);
  }

}
