package com.kiga.web.service;

/**
 * @author bbs
 * @since 9/29/16.
 */
public interface GeoIpService {
  String resolveCountryCode();

  void changeCountryCode(String newCountryCode);

  void removeCountryCode();
}
