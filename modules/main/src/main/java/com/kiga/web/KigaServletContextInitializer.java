package com.kiga.web;

import com.kiga.web.props.WebProperties;
import com.kiga.web.web.ResettableInputStreamFilter;
import java.util.EnumSet;
import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.springframework.boot.context.embedded.ServletContextInitializer;

/** Created by rainerh on 13.10.16. */
public class KigaServletContextInitializer implements ServletContextInitializer {
  WebProperties webProperties;

  public KigaServletContextInitializer(WebProperties webProperties) {
    this.webProperties = webProperties;
  }

  @Override
  public void onStartup(ServletContext servletContext) throws ServletException {
    EnumSet disps =
        EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.ASYNC);
    servletContext
        .addFilter("resettableInputStreamFilter", new ResettableInputStreamFilter())
        .addMappingForUrlPatterns(disps, true, "/*");
    servletContext
        .getSessionCookieConfig()
        .setName(webProperties.getWebSecurityProperties().getCookieName());
  }
}
