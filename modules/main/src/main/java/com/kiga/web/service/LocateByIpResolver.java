package com.kiga.web.service;

/**
 * @author bbs
 * @since 9/29/16.
 */
public interface LocateByIpResolver {
  String getCountryIsoCode(String ip);
}
