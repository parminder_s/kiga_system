package com.kiga.web.service;

import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 10/1/16.
 */
public class UserIpService {
  private final String ipHttpHeaderName;

  public UserIpService(String ipHttpHeaderName) {
    this.ipHttpHeaderName = ipHttpHeaderName;
  }

  /**
   * Get user ip from a hreader.
   *
   * @param request current request
   * @return user ip
   */
  public String getUserIp(HttpServletRequest request) {
    if (ipHttpHeaderName.equals("default")) {
      return request.getRemoteAddr();
    } else {
      return request.getHeader(ipHttpHeaderName);
    }
  }
}
