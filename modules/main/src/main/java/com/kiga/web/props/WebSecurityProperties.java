package com.kiga.web.props;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@ConfigurationProperties("web.security")
@Configuration
public class WebSecurityProperties {
  private String cookieName;
  @Autowired private WebSecurityCacheProperties cacheProperties;
}
