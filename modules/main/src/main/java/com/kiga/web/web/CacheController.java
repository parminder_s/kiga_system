package com.kiga.web.web;

import com.kiga.security.services.SecurityService;
import com.kiga.web.message.EndpointRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by peter on 22.09.16.
 */
@RestController
@RequestMapping("/cache")
public class CacheController {
  SecurityService securityService;

  @Autowired
  public CacheController(SecurityService securityService) {
    this.securityService = securityService;
  }

  @RequestMapping(value = "/invalidate/homepage", method = RequestMethod.POST)
  @Caching(
    evict = {
      @CacheEvict(value = "default", key = "'homepage-de'"),
      @CacheEvict(value = "default", key = "'homepage-en'"),
      @CacheEvict(value = "default", key = "'homepage-it'"),
      @CacheEvict(value = "default", key = "'homepage-tr'")
    })
  public void ideasAll(@RequestBody EndpointRequest endpointRequest) {
    securityService.requiresContentAdmin();
  }

  @RequestMapping(value = "/invalidate/homepage-local", method = RequestMethod.POST)
  @CacheEvict(value = "default", key = "'homepage-' + #endpointRequest.getLocale()")
  public void homepageLocale(@RequestBody EndpointRequest endpointRequest) {
    securityService.requiresContentAdmin();
  }
}
