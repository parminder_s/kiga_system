package com.kiga.web.converter;

/**
 * @author bbs
 * @since 4/28/16.
 */
public interface ResponseConverter<E, R> {
  R convertToResponse(E entity);
}
