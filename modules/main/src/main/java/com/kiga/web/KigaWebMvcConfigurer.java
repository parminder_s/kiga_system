package com.kiga.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by rainerh on 30.10.16.
 */
@Configuration
public class KigaWebMvcConfigurer extends WebMvcConfigurerAdapter {
  @Override
  public void addCorsMappings(CorsRegistry corsRegistry) {
    corsRegistry.addMapping("/**");
  }


  /**
   * Since we want to return simple Strings as Json we have to put all StringConverters
   * at the end of the list. Otherwise StringConverter will jump in before JsonConverter.
   */
  @Override
  public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {

    Map<String, List<HttpMessageConverter<?>>> groups = converters.stream()
      .collect(Collectors.groupingBy(
        c -> c instanceof StringHttpMessageConverter ? "strings" : "non-strings",
        Collectors.toList()));
    converters.clear();
    converters.addAll(groups.get("non-strings"));
    converters.addAll(groups.get("strings"));
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry
        .addResourceHandler("swagger-ui-html")
        .addResourceLocations("classpath:/META-INF/resources/");

    registry
        .addResourceHandler("/webjars/**")
        .addResourceLocations("classpath:/META-INF/resources/webjars/");
  }
}
