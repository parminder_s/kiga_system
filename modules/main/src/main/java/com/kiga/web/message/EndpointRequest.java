package com.kiga.web.message;

import com.kiga.main.locale.Locale;

/**
 * Created by rainerh on 21.10.16.
 */
public class EndpointRequest {
  private Locale locale;

  public EndpointRequest() {}

  public EndpointRequest(Locale locale) {
    this.locale = locale;
  }

  public Locale getLocale() {
    return this.locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  /**
   * equals operation.
   */
  public boolean equals(Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof EndpointRequest)) {
      return false;
    }
    final EndpointRequest other = (EndpointRequest) object;
    if (!other.canEqual((Object) this)) {
      return false;
    }
    final Object this$locale = this.getLocale();
    final Object other$locale = other.getLocale();
    if (this$locale == null ? other$locale != null : !this$locale.equals(other$locale)) {
      return false;
    }
    return true;
  }

  /**
   * hashCode.
   */
  public int hashCode() {
    final int prime = 59;
    int result = 1;
    final Object $locale = this.getLocale();
    result = result * prime + ($locale == null ? 43 : $locale.hashCode());
    return result;
  }

  protected boolean canEqual(Object other) {
    return other instanceof EndpointRequest;
  }

  public String toString() {
    return "com.kiga.web.message.EndpointRequest(locale=" + this.getLocale() + ")";
  }
}
