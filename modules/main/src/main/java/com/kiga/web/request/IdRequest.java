package com.kiga.web.request;

import com.kiga.main.locale.Locale;
import lombok.Data;

/**
 * Created by robert on 10.06.17.
 */
@Data
public class IdRequest {
  Long id;
  Locale locale;
}
