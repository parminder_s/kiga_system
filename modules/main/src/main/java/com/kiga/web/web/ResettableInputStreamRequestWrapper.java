package com.kiga.web.web;

/**
 * Created by rainerh on 09.08.16.
 */

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * Wrapper used to be able to consume multiple times the InputStream provided by
 * HttpServletRequest.
 *
 * <p>see @ResettableInputStreamFilter.
 */
public class ResettableInputStreamRequestWrapper extends HttpServletRequestWrapper {
  private final byte[] body;

  /**
   * reads the input stream and stores it as string in body.
   */
  public ResettableInputStreamRequestWrapper(HttpServletRequest request) throws IOException {
    super(request);
    if (request.getContentType() != null
      && request.getContentType().contains(MediaType.APPLICATION_JSON.toString())) {
      InputStream inputStream = request.getInputStream();
      if (inputStream != null) {
        body = IOUtils.toByteArray(inputStream);
        return;
      }
    }
    body = null;
  }

  @Override
  public ServletInputStream getInputStream() throws IOException {
    if (body == null) {
      return super.getInputStream();
    }

    Logger logger = LoggerFactory.getLogger(getClass());

    final ByteArrayInputStream stream = new ByteArrayInputStream(body);
    ServletInputStream inputStream = new ServletInputStream() {
      public boolean isFinished() {
        logger.warn("isFinished called in customized ServletInputStream, "
          + "please check if correct return value is set");
        return true;
      }

      @Override
      public boolean isReady() {
        logger.warn("isReady called in customized ServletInputStream, "
          + "please check if correct return value is set");
        return true;
      }

      @Override
      public void setReadListener(ReadListener listener) {

      }

      @Override
      public int read() throws IOException {
        return stream.read();
      }
    };

    return inputStream;
  }

  @Override
  public BufferedReader getReader() throws IOException {
    if (body == null) {
      return super.getReader();
    }

    return new BufferedReader(
      new InputStreamReader( new ByteArrayInputStream(body), StandardCharsets.UTF_8));
  }
}
