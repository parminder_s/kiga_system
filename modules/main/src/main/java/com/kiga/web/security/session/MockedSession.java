package com.kiga.web.security.session;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by rainerh on 13.11.16.
 *
 * <p>The mocked session is used in dev mode and contains a limited set of available sessions
 * associated with the member data of the fixtures.</p>
 */
public class MockedSession implements CachedSession {
  private ObjectMapper mapper;

  public MockedSession() {
    this.mapper = new ObjectMapper();
  }

  /**
   * static session data.
   */
  private static Map<String, String> defaultSession =
    MapUtils.putAll(new HashedMap<String, String>(), new Object[][] {});

  private static Map<String, String> johnSession =
    MapUtils.putAll(new HashedMap<String, String>(), new Object[][] {
      {"access_content", "true"},
      {"access_articles_pool", "true"},
      {"access_articles", "true"},
      {"auth_level", 2},
      {"has_testblock", 0},
      {"is_crm", 1},
      {"loggedInAs", 1},
      {"is_cancelled", "true"},
      {"product_status", ""},
    });

  private static Map<String, String> homerSession =
    MapUtils.putAll(new HashedMap<String, String>(), new Object[][] {
      {"access_content", "false"},
      {"access_articles_pool", "false"},
      {"access_articles", "false"},
      {"auth_level", 2},
      {"has_testblock", 0},
      {"is_crm", 1},
      {"loggedInAs", 5},
      {"is_cancelled", "true"},
      {"product_status", "end"},
    });

  private static Map<String, String> lucySession =
    MapUtils.putAll(new HashedMap<String, String>(), new Object[][] {
      {"access_content", "false"},
      {"access_articles_pool", "false"},
      {"access_articles", "false"},
      {"auth_level", 2},
      {"has_testblock", 0},
      {"is_crm", 0},
      {"loggedInAs", 7},
      {"is_cancelled", "true"},
      {"product_status", ""},
    });

  private static Map<String, Map<String, String>> sessionMap =
    MapUtils.putAll(new HashedMap<String, Map<String, String>>(), new Object[][] {
      {"session_john", johnSession},
      {"session_homer", homerSession},
      {"session_lucy", lucySession}
    });


  @Override
  public String get(String key) {
    if (sessionMap.containsKey(key)) {
      try {
        return this.mapper.writeValueAsString(sessionMap.get(key));
      } catch (JsonProcessingException jpe) {
        LoggerFactory.getLogger(getClass()).error("json parser exception");
        return "";
      }
    } else {
      return "";
    }
  }
}
