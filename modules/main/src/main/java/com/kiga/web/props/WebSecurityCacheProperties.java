package com.kiga.web.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@ConfigurationProperties("web.security.cache")
@Configuration
public class WebSecurityCacheProperties {
  private String url;
  private int port;
  private String type;
}
