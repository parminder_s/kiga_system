package com.kiga.web.service;

import com.kiga.main.service.CookieService;

/**
 * @author bbs
 * @since 9/29/16.
 */
public class CookieBasedGeoIpMockService implements GeoIpService {
  private static final String COUNTRY_COOKIE_KEY = "countryCode";

  private DefaultGeoIpService defaultGeoIpService;
  private CookieService cookieService;

  /**
   * Dependency wiring.
   *
   * @param defaultGeoIpService defaultGeoIpService
   * @param cookieService       cookie service
   */
  public CookieBasedGeoIpMockService(DefaultGeoIpService defaultGeoIpService,
    CookieService cookieService) {
    this.defaultGeoIpService = defaultGeoIpService;
    this.cookieService = cookieService;
  }

  @Override
  public String resolveCountryCode() {
    String resolvedCountry = cookieService.get(COUNTRY_COOKIE_KEY);
    if (resolvedCountry == null) {
      // fallback to default resolving without cookies
      resolvedCountry = defaultGeoIpService.resolveCountryCode();
      changeCountryCode(resolvedCountry);
    }
    return resolvedCountry;
  }

  @Override
  public void changeCountryCode(String newCountryCode) {
    cookieService.add(COUNTRY_COOKIE_KEY, newCountryCode);
  }

  @Override
  public void removeCountryCode() {
    cookieService.remove(COUNTRY_COOKIE_KEY);
  }
}
