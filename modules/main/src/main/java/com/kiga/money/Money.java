package com.kiga.money;

import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Pattern;

@Data
public class Money {
  private BigDecimal amount;
  private String currency;

  public Money() {}

  /**
   * instantiates a new Money object.
   */
  public Money(String amount, String currency) {
    this.amount = new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP);
    this.currency = currency;
  }

  /**
   * instantiates a new Money object - directly from a BigDecimal.
   */
  public Money(BigDecimal amount, String currency) {
    this.amount = amount.setScale(2, RoundingMode.HALF_UP);
    this.currency = currency;
  }
}
