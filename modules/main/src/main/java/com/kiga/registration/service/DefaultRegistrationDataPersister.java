package com.kiga.registration.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.registration.domain.RegistrationData;
import com.kiga.registration.web.request.RegistrationRequest;
import com.kiga.spec.Fetcher;
import com.kiga.spec.Persister;

/**
 * Created by rainerh on 23.04.16.
 */
public class DefaultRegistrationDataPersister implements RegistrationDataPersister {
  private Persister<RegistrationData> persister;
  private Fetcher<RegistrationData> fetcher;
  private ObjectMapper objectMapper;

  /**
   * inject dependencies.
   */
  public DefaultRegistrationDataPersister(
    Persister<RegistrationData> persister,
    Fetcher<RegistrationData> fetcher, ObjectMapper objectMapper
  ) {
    this.persister = persister;
    this.fetcher = fetcher;
    this.objectMapper = objectMapper;
  }

  /**
   * stores the registration data as serialized json.
   */
  @Override
  public void persist(RegistrationRequest registrationRequest) throws JsonProcessingException {
    RegistrationData registrationData = fetcher.find(registrationRequest.getRegistrationId());
    objectMapper.writeValueAsString(registrationRequest);
    registrationData.setData(objectMapper.writeValueAsString(registrationRequest));
    this.persister.persist(registrationData);
  }
}
