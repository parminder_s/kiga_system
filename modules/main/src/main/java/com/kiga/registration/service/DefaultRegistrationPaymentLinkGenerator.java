package com.kiga.registration.service;

import com.kiga.main.locale.Locale;
import com.kiga.web.service.UrlGenerator;
import org.apache.commons.lang3.StringUtils;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.net.URLEncoder;

/**
 * Created by rainerh on 10.04.16.
 *
 * <p>Generates the link to an intermediate page that creates a form
 * with the fields it gets from get parameter and autosubmits to garanti.
 */
public class DefaultRegistrationPaymentLinkGenerator implements RegistrationPaymentLinkGenerator {
  private UrlGenerator urlGenerator;

  public DefaultRegistrationPaymentLinkGenerator(UrlGenerator urlGenerator) {
    this.urlGenerator = urlGenerator;
  }

  /**
   * returns the link containing the required form fields.
   */
  @Override
  public String getLink(Object pojo, Locale locale) throws Exception {
    StringBuilder builder = new StringBuilder(
      urlGenerator.getNgUrl(locale + "/payment/checkout?")
    );
    PropertyDescriptor[] pds = Introspector.getBeanInfo(pojo.getClass()).getPropertyDescriptors();

    for (PropertyDescriptor pd: pds) {
      if (pd.getReadMethod() != null && !pd.getName().equals("class")) {
        builder.append(pd.getName() + "=");
        builder.append(URLEncoder.encode(pd.getReadMethod().invoke(pojo).toString(), "UTF-8"));
        builder.append("&");
      }
    }

    return StringUtils.chop(builder.toString());
  }
}
