package com.kiga.registration.web.request;

import com.kiga.main.locale.Locale;
import lombok.Data;

/**
 * Created by rainerh on 15.03.16.
 */
@Data
public class RegistrationRequest {
  private Long registrationId;
  private String productGroup;
  private Integer productId;
  private Integer licences;
  private String country;
  private String gender;
  private String firstname;
  private String lastname;
  private String birthday;
  private String email;
  private String password;
  private String phone;
  private String street;
  private String streetNumber;
  private String floorNumber;
  private String doorNumber;
  private String zip;
  private String city;
  private Boolean billOn;
  private String billAddressName1;
  private String billAddressName2;
  private String billStreet;
  private String billStreetNumber;
  private String billFloorNumber;
  private String billDoorNumber;
  private String billZip;
  private String billCity;
  private Boolean newsletter;
  private Boolean agb;
  private Boolean security;
  private String paymentMethod;
  private String invoiceOption;
  private Locale locale;

  private String giftCode;
  private String taxnumber;
  private String passportnumber;
}
