package com.kiga.registration.web;

import com.kiga.registration.web.request.InitiateResponse;
import com.kiga.web.message.EndpointRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by rainerh on 12.10.16.
 */
@RestController
public class RegistrationMockController {
  /**
   * returns a mocked initiate response.
   */
  @RequestMapping(
    value = "/api/nregistration/initiate",
    method = RequestMethod.POST
  )
  public InitiateResponse initiate(EndpointRequest endpointRequest) {
    InitiateResponse returner = new InitiateResponse();
    returner.setId(1);
    returner.setStepName("preSelectionFields");
    return returner;
  }
}
