package com.kiga.registration.mapper;

import org.modelmapper.AbstractConverter;

/**
 * Created by rainerh on 25.05.16.
 */
public class GenderSexConverter extends AbstractConverter<String, String> {
  /**
   * converts gender string from registration to sex string for crm.
   */
  public String convert(String source) {
    if (source == null) {
      return null;
    } else {
      return source.equals("female") ? "f" : "m";
    }
  }
}

