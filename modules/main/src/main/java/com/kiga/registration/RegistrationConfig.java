package com.kiga.registration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.db.RepositoryMapper;
import com.kiga.registration.mapper.RequestNewSubscriptionMapper;
import com.kiga.registration.repository.RegistrationDataRepository;
import com.kiga.registration.service.DefaultRegistrationDataPersister;
import com.kiga.registration.service.DefaultRegistrationPaymentLinkGenerator;
import com.kiga.registration.service.RegistrationDataPersister;
import com.kiga.registration.service.RegistrationPaymentLinkGenerator;
import com.kiga.web.service.UrlGenerator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

/** Created by rainerh on 25.03.16. */
@Configuration
public class RegistrationConfig implements ApplicationListener<ContextRefreshedEvent> {
  @Autowired ModelMapper modelMapper;
  /** RegistrationPaymentLinkGenerator bean. */
  @Bean
  public RegistrationPaymentLinkGenerator getRegistrationPaymentLinkGenerator(
      UrlGenerator urlGenerator) {
    return new DefaultRegistrationPaymentLinkGenerator(urlGenerator);
  }

  /** RegistrationDataPersister bean. */
  @Bean
  public RegistrationDataPersister getRegistrationDataPersister(
      ObjectMapper objectMapper, RegistrationDataRepository registrationDataRepository) {
    return new DefaultRegistrationDataPersister(
        RepositoryMapper.toPersister(registrationDataRepository),
        RepositoryMapper.toFetcher(registrationDataRepository),
        objectMapper);
  }

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    modelMapper.addMappings(new RequestNewSubscriptionMapper());
  }

  public void setModelMapper(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }
}
