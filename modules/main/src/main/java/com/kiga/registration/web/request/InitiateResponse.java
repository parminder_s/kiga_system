package com.kiga.registration.web.request;

import com.kiga.registration.web.field.RegistrationField;
import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * Created by rainerh on 12.10.16.
 */
@Data
public class InitiateResponse {
  private int id;
  private String stepName;
  private List<RegistrationField> fields;
}
