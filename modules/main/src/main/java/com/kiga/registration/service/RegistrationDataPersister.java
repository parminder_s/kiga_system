package com.kiga.registration.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kiga.registration.web.request.RegistrationRequest;

/**
 * Created by rainerh on 27.04.16.
 */
public interface RegistrationDataPersister {
  void persist(RegistrationRequest registrationRequest) throws JsonProcessingException;
}
