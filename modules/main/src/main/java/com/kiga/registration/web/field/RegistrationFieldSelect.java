package com.kiga.registration.web.field;

import java.util.List;

/**
 * Created by rainerh on 12.10.16.
 */
public class RegistrationFieldSelect extends RegistrationField {
  String renderType = "select";
  List<SelectOption> values;

  public RegistrationFieldSelect() {

  }

  public String getRenderType() {
    return renderType;
  }

  public void setRenderType(String renderType) {
    this.renderType = renderType;
  }

  public List<SelectOption> getValues() {
    return values;
  }

  public void setValues(List<SelectOption> values) {
    this.values = values;
  }
}
