package com.kiga.registration.web.field;

/**
 * Created by rainerh on 12.10.16.
 */
public class SelectOption {
  private String key;
  private String value;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
