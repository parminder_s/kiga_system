package com.kiga.registration.domain;

import com.kiga.db.KigaEntityModel;

import javax.persistence.Entity;

/**
 * Created by rainerh on 23.04.16.
 */
@Entity
public class RegistrationData extends KigaEntityModel {
  private String data;

  public RegistrationData() {
    this.setClassName("RegistrationData");
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }
}
