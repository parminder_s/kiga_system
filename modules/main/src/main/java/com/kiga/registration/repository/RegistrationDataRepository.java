package com.kiga.registration.repository;

import com.kiga.registration.domain.RegistrationData;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by rainerh on 23.04.16.
 */
public interface RegistrationDataRepository extends JpaRepository<RegistrationData, Long> {
}
