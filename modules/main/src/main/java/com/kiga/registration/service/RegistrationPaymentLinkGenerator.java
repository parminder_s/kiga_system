package com.kiga.registration.service;

import com.kiga.main.locale.Locale;

/**
 * Created by rainerh on 11.04.16.
 */
public interface RegistrationPaymentLinkGenerator {
  String getLink(Object pojo, Locale shortLocale) throws Exception;
}
