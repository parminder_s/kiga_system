package com.kiga.registration.mapper;

import com.kiga.crm.messages.requests.RequestNewSubscription;
import com.kiga.registration.web.request.RegistrationRequest;
import org.modelmapper.Condition;
import org.modelmapper.Conditions;
import org.modelmapper.PropertyMap;
import org.modelmapper.spi.MappingContext;

/**
 * Created by rainerh on 25.05.16.
 */
public class RequestNewSubscriptionMapper extends
  PropertyMap<RegistrationRequest, RequestNewSubscription> {
  private Condition billAddressCondition =
    new Condition<RegistrationRequest, RequestNewSubscription>() {
    @Override
    public boolean applies(MappingContext<RegistrationRequest, RequestNewSubscription> context) {
      RegistrationRequest registrationRequest =
        (RegistrationRequest)context.getParent().getSource();
      if (registrationRequest.getBillOn() == null) {
        return false;
      } else {
        return registrationRequest.getBillOn();
      }
    }
  };

  /**
   * ModelMapper from RegistrationRequest to RequestNewSubscription.
   */
  protected void configure() {
    map(source.getFirstname(), destination.getShippingAddress().getFirstname());
    map(source.getLastname(), destination.getShippingAddress().getLastname());
    using(new GenderSexConverter())
      .map(source.getGender(), destination.getShippingAddress().getSex());
    map(source.getStreet(), destination.getShippingAddress().getStreet());
    map(source.getStreetNumber(), destination.getShippingAddress().getHousenr());
    map(source.getFloorNumber(), destination.getShippingAddress().getFloor());
    map(source.getDoorNumber(), destination.getShippingAddress().getDoornr());
    map(source.getZip(), destination.getShippingAddress().getZip());
    map(source.getCity(), destination.getShippingAddress().getCity());
    map(source.getCountry(), destination.getShippingAddress().getCountry());
    map(source.getEmail(), destination.getShippingAddress().getEmail());
    map(source.getPhone(), destination.getShippingAddress().getTel());

    map(source.getTaxnumber(), destination.getCountryContext().getTaxNr());
    map(source.getPassportnumber(), destination.getCountryContext().getPassportNr());

    when(billAddressCondition)
      .map(source.getBillAddressName1(), destination.getBillingAddress().getOrganisation());
    when(billAddressCondition)
      .map(source.getBillAddressName2(), destination.getBillingAddress().getOrganisation2());
    when(billAddressCondition)
      .map(source.getBillStreet(), destination.getBillingAddress().getStreet());
    when(billAddressCondition)
      .map(source.getBillStreetNumber(), destination.getBillingAddress().getHousenr());
    when(billAddressCondition)
      .map(source.getBillFloorNumber(), destination.getBillingAddress().getFloor());
    when(billAddressCondition)
      .map(source.getBillDoorNumber(), destination.getBillingAddress().getDoornr());
    when(billAddressCondition)
      .map(source.getBillZip(), destination.getBillingAddress().getZip());
    when(billAddressCondition)
      .map(source.getBillCity(), destination.getBillingAddress().getCity());

    map(source.getCountry(), destination.getSubscription().getCountry());
    map(source.getProductId(), destination.getSubscription().getProductId());
    when(Conditions.isNotNull())
      .map(source.getLicences(), destination.getSubscription().getMaxNumberSubUsers());

    map(source.getPaymentMethod(), destination.getPaymentOption());
    map(source.getLocale(), destination.getUserLang());
    map(source.getBirthday(), destination.getDateOfBirth());
  }
}
