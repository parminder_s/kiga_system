package com.kiga.forum.repository;

import com.kiga.forum.domain.Forum;
import com.kiga.forum.domain.ForumThread;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by peter on 08.06.16.
 */
public interface ForumThreadRepository extends CrudRepository<ForumThread, Long> {
  ForumThread findByKigaPageId(Long kigaPageId);

  ForumThread findTopByForumOrderByRealLastEditedDesc(Forum forum);
}
