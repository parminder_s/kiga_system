package com.kiga.forum.domain;

/**
 * Created by peter on 21.06.16.
 */
public enum ForumType {
  IDEA("article"),
  ACTIVITY("idea"),
  KIGA("kiga"),
  JOB("job");

  private final String stringValue;
  private ForumType(final String stringValue) {
    this.stringValue = stringValue;
  }

  public String toString() {
    return stringValue;
  }

}
