package com.kiga.forum.web.responses;

import lombok.Data;

import java.util.Date;

/**
 * Created by peter on 14.06.16.
 */
@Data
public class PostResponse {
  String authorName;
  String authorPictureUrl;
  String content;
  Date dateWritten;
}
