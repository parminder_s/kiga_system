package com.kiga.forum.web.responses;

import lombok.Data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by peter on 21.06.16.
 */
@Data
public class NewestForumResponse {
  private String title;
  private String content;
  private String url;

  public NewestForumResponse() {

  }

  /**
   *
   * @param title title of the thread.
   * @param content content of the thread.
   * @param url url to the thread.
   */
  public NewestForumResponse(String title, String content, String url) {
    this.title = title;
    this.content = content;

    // very simple regex to remove bbcode - tags
    if (content != null) {
      Pattern pattern = Pattern.compile(Pattern.quote("[") + ".*?" + Pattern.quote("]"));
      Matcher matcher = pattern.matcher(content);
      this.content = matcher.replaceAll("");
    }

    this.url = url;
  }
}
