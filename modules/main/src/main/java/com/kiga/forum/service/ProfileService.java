package com.kiga.forum.service;

import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.security.domain.Member;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;

/**
 * returns the nickname and url of a member.
 */
public class ProfileService {
  private S3ImageResizeService s3ImageResizeService;

  public ProfileService(S3ImageResizeService s3ImageResizeService) {
    this.s3ImageResizeService = s3ImageResizeService;
  }

  /**
   * returns nickname or anonymous if empty or null.
   */
  public String getNickname(Member member) {
    return Optional.ofNullable(member)
      .map(Member::getNickname)
      .filter((nickname) -> !nickname.isEmpty())
      .orElse("anonymous");
  }


  /**
   * returns the url to the resized Avatar image or an empty string.
   */
  public String getUrl(Member member) {
    return Optional.ofNullable(member).map(member1 -> {
      try {
        S3Image s3Avatar = member.getS3Avatar();
        return s3ImageResizeService.getResizedImageUrl(s3Avatar, new PaddedImageStrategy(75, 75));
      } catch (EntityNotFoundException | IOException exception) {
        return "";
      }
    }).orElse("");
  }
}
