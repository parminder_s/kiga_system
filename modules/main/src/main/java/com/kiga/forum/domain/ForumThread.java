package com.kiga.forum.domain;

import com.kiga.content.domain.ss.live.KigaPageLive;
import com.kiga.db.KigaEntityModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

/**
 * Created by peter on 08.06.16.
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class ForumThread extends KigaEntityModel {
  private String title;
  private long numViews;
  private boolean isSticky = false;
  private boolean isReadOnly = false;
  private boolean isGlobalSticky = false;

  public ForumThread() {
    this.setClassName("ForumThread");
  }

  @ManyToOne
  @JoinColumn(name = "forumID", referencedColumnName = "id")
  @NotFound(action = NotFoundAction.IGNORE)
  private Forum forum;

  private Date realLastEdited;

  @ManyToOne
  @JoinColumn(name = "kigaPageID", referencedColumnName = "id")
  @NotFound(action = NotFoundAction.IGNORE)
  private KigaPageLive kigaPage;


  @OneToMany(mappedBy = "thread")
  @OrderBy("created")
  private List<Post> posts;
}
