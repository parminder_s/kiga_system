package com.kiga.forum.web.requests;

import com.kiga.web.message.EndpointRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 08.06.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AddPostRequest extends EndpointRequest {
  String content;

  long kigaIdeaId;
}
