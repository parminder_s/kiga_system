package com.kiga.forum.web.responses;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by peter on 14.06.16.
 */
@Data
public class ForumThreadResponse {
  long id;
  String title;
  long requests;
  List<PostResponse> posts = new ArrayList<>();
}
