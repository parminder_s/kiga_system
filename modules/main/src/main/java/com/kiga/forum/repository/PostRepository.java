package com.kiga.forum.repository;

import com.kiga.forum.domain.Post;
import com.kiga.security.domain.Member;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by peter on 08.06.16.
 */
public interface PostRepository extends CrudRepository<Post, Long> {
  List<Post> findByAuthor(Member author);
}
