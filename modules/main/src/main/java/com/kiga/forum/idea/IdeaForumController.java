package com.kiga.forum.idea;

import com.kiga.forum.web.requests.AddPostRequest;
import com.kiga.forum.web.responses.ForumThreadResponse;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by peter on 08.06.16.
 */
@RestController
@RequestMapping("/forum/idea")
public class IdeaForumController {

  private SecurityService securityService;
  private IdeaForumService ideaForumService;

  @Autowired
  public IdeaForumController(IdeaForumService ideaForumService, SecurityService securityService) {
    this.ideaForumService = ideaForumService;
    this.securityService = securityService;
  }

  @RequestMapping(
    value = "/get/{ideaId}",
    method = RequestMethod.GET
  )
  public ForumThreadResponse getThread(@PathVariable("ideaId") Long ideaId) {
    return ideaForumService.getThread(ideaId);
  }

  /**
   * Add new post to the Idea Thread.
   */
  @RequestMapping(
    value = "/addPost/{ideaId}",
    method = RequestMethod.POST
  )
  public void addPost(@RequestBody AddPostRequest addPostRequest, HttpServletRequest request) {
    securityService.requiresLogin();
    Member member = securityService.getSessionMember().get();
    ideaForumService.addPost(addPostRequest.getKigaIdeaId(), member,
      addPostRequest.getContent(), addPostRequest.getLocale());
  }
}
