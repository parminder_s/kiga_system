package com.kiga.forum.service;

import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.forum.domain.Forum;
import com.kiga.forum.domain.ForumThread;
import com.kiga.forum.repository.ForumRepository;
import com.kiga.forum.repository.ForumThreadRepository;
import com.kiga.forum.repository.PostRepository;
import com.kiga.main.locale.Locale;
import com.kiga.security.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by peter on 7/16/16.
 */
@Service
public class ForumService {
  private ForumThreadRepository forumThreadRepository;
  private ForumRepository forumRepository;
  private SiteTreeUrlGenerator siteTreeUrlGenerator;

  /**
   * inject beans.
  */
  @Autowired
  public ForumService(ForumThreadRepository forumThreadRepository, ForumRepository forumRepository,
                      SiteTreeUrlGenerator siteTreeUrlGenerator) {
    this.forumThreadRepository = forumThreadRepository;
    this.forumRepository = forumRepository;
    this.siteTreeUrlGenerator = siteTreeUrlGenerator;
  }

  public ForumThread getNewest(String type, Locale locale) {
    Forum forum = forumRepository.findByForumCodeAndLocale(type, locale);
    return forumThreadRepository.findTopByForumOrderByRealLastEditedDesc(forum);
  }

  public String getUrl(ForumThread forumThread) {
    return siteTreeUrlGenerator.getRelativeUrl(forumThread.getForum()) + "/show/"
      + forumThread.getId();
  }
}
