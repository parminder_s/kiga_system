package com.kiga.forum.service;

import com.kiga.forum.domain.ForumThread;
import com.kiga.forum.domain.Post;
import com.kiga.forum.web.responses.NewestForumResponse;
import com.kiga.main.locale.Locale;

/**
 * Created by peter on 15.09.16.
 */
public class NewestForumService {
  ForumService forumService;

  public NewestForumService(ForumService forumService) {
    this.forumService = forumService;
  }

  /**
   * returns the newest forum post.
   */
  public NewestForumResponse getNewestForum(String type, Locale locale) {
    ForumThread forumThread = forumService.getNewest(type, locale);
    if (forumThread == null) {
      return new NewestForumResponse("", "", "");
    }
    String content = forumThread.getTitle();
    forumThread.getPosts().sort((Post post, Post t1) ->
      post.getLastEdited().compareTo(t1.getLastEdited()));
    if (forumThread.getPosts().size() > 0) {
      content = forumThread.getPosts().get(forumThread.getPosts().size() - 1).getContent();
    }
    return new NewestForumResponse(forumThread.getTitle(), content,
      forumService.getUrl(forumThread));
  }
}
