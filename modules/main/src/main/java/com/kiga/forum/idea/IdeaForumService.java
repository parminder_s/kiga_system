package com.kiga.forum.idea;

import com.kiga.content.domain.ss.live.KigaPageLive;
import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive;
import com.kiga.forum.domain.Forum;
import com.kiga.forum.domain.ForumThread;
import com.kiga.forum.domain.Post;
import com.kiga.forum.repository.ForumRepository;
import com.kiga.forum.repository.ForumThreadRepository;
import com.kiga.forum.repository.PostRepository;
import com.kiga.forum.service.ProfileService;
import com.kiga.forum.web.responses.ForumThreadResponse;
import com.kiga.forum.web.responses.PostResponse;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import com.kiga.security.repository.MemberRepository;
import org.kefirsf.bb.BBProcessorFactory;
import org.kefirsf.bb.TextProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by peter on 08.06.16.
 */
public class IdeaForumService {
  private ForumRepository forumRepository;
  private ForumThreadRepository forumThreadRepository;
  private KigaIdeaRepositoryLive kigaIdeaRepositoryLive;
  private PostRepository postRepository;
  private ProfileService profileService;
  private MemberRepository memberRepository;

  /**
   * inject dependencies.
   */
  public IdeaForumService(
    ForumRepository forumRepository, ForumThreadRepository forumThreadReposiotry,
    KigaIdeaRepositoryLive kigaIdeaRepositoryLive, PostRepository postRepository,
    ProfileService profileService, MemberRepository memberRepository) {
    this.forumRepository = forumRepository;
    this.forumThreadRepository = forumThreadReposiotry;
    this.kigaIdeaRepositoryLive = kigaIdeaRepositoryLive;
    this.postRepository = postRepository;
    this.profileService = profileService;
    this.memberRepository = memberRepository;
  }

  /**
   * returns the thread for an KigaIdea.
   */
  public ForumThreadResponse getThread(long ideaId) {
    ForumThreadResponse returner = new ForumThreadResponse();
    ForumThread forumThread = forumThreadRepository.findByKigaPageId(ideaId);
    Logger logger = LoggerFactory.getLogger(getClass());

    if (forumThread != null) {
      Forum forum = forumThread.getForum();
      int skipPosts = forum.getForumCode().equals("article") ? 1 : 0;

      returner.setTitle(forumThread.getTitle());
      returner.setRequests(forumThread.getNumViews());
      returner.setId(forumThread.getId());
      returner.setPosts(
        forumThread.getPosts().stream()
          .skip(skipPosts)
          .map(post -> {
            PostResponse postResponse = new PostResponse();
            Member author = post.getAuthor();
            postResponse.setContent(convertBbCodeToHtml(post.getContent()));
            postResponse.setAuthorName(profileService.getNickname(author));
            postResponse.setAuthorPictureUrl(profileService.getUrl(author));
            postResponse.setDateWritten(post.getCreated());
            return postResponse;
          })
          .collect(Collectors.toList())
      );
    }

    return returner;
  }

  /**
   * adds a posting to an existing thread and creates the thread if it does not exist yet.
   */
  public void addPost(long ideaId, Member author, String content, Locale locale) {
    Post firstPost = new Post();
    firstPost.setAnonNickname("KigaPortal");

    Post post = new Post();
    post.setContent(content);
    post.setAuthor(author);
    if (author.getNickname() == null) {
      post.setAnonNickname("anonymous");
    } else {
      post.setAnonNickname(author.getNickname());
    }
    ForumThread forumThread = forumThreadRepository.findByKigaPageId(ideaId);
    if (forumThread != null) {
      post.setThread(forumThreadRepository.findByKigaPageId(ideaId));
      forumThread.setRealLastEdited(new Date());
      forumThread.setLastEdited(new Date());
    } else {
      KigaPageLive kigaPage = kigaIdeaRepositoryLive.findOne(ideaId);
      ForumThread newForumThread = new ForumThread();
      newForumThread.setKigaPage(kigaPage);
      newForumThread.setTitle(kigaPage.getTitle());
      newForumThread.setRealLastEdited(new Date());
      newForumThread.setForum(
        forumRepository.findByForumCodeAndLocale("article", locale));
      forumThreadRepository.save(newForumThread);
      firstPost.setThread(newForumThread);
      Member forumMember = memberRepository.findOne(1L);
      firstPost.setAuthor(forumMember);
      postRepository.save(firstPost);

      post.setThread(newForumThread);
    }

    postRepository.save(post);
  }

  private String convertBbCodeToHtml(String content) {
    TextProcessor processor = BBProcessorFactory.getInstance().create();
    return processor.process(Optional.ofNullable(content).orElse(""));
  }

}
