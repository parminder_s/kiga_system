package com.kiga.forum;

import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive;
import com.kiga.forum.idea.IdeaForumService;
import com.kiga.forum.repository.ForumRepository;
import com.kiga.forum.repository.ForumThreadRepository;
import com.kiga.forum.repository.PostRepository;
import com.kiga.forum.service.ForumService;
import com.kiga.forum.service.NewestForumService;
import com.kiga.forum.service.ProfileService;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.security.repository.MemberRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ForumConfig {
  @Bean
  IdeaForumService ideaForumService(
      ForumRepository forumRepository,
      ForumThreadRepository forumThreadReposiotry,
      KigaIdeaRepositoryLive kigaIdeaRepositoryLive,
      PostRepository postRepository,
      ProfileService profileService,
      MemberRepository memberRepository) {
    return new IdeaForumService(
        forumRepository,
        forumThreadReposiotry,
        kigaIdeaRepositoryLive,
        postRepository,
        profileService,
        memberRepository);
  }

  @Bean
  public NewestForumService newestForumService(ForumService forumService) {
    return new NewestForumService(forumService);
  }

  @Bean
  public ProfileService getProfileService(S3ImageResizeService s3ImageResizeService) {
    return new ProfileService(s3ImageResizeService);
  }
}
