package com.kiga.forum.domain;

import com.kiga.content.domain.ss.live.PageLive;

import javax.persistence.Column;
import javax.persistence.Entity;


/**
 * Created by peter on 08.06.16.
 */

@Entity
public class Forum extends PageLive {
  //@Enumerated(EnumType.STRING)
  @Column(name = "Code")
  String forumCode;

  public String getForumCode() {
    return forumCode;
  }

  public void setForumCode(String forumCode) {
    this.forumCode = forumCode;
  }
}
