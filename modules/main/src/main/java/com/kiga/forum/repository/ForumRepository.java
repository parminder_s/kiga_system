package com.kiga.forum.repository;

import com.kiga.forum.domain.Forum;
import com.kiga.main.locale.Locale;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by peter on 08.06.16.
 */
public interface ForumRepository extends CrudRepository<Forum, Long> {
  public Forum findByForumCodeAndLocale(String forumCode, Locale locale);
}
