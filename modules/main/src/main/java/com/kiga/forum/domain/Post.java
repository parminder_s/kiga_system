package com.kiga.forum.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.security.domain.Member;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by peter on 08.06.16.
 */
@Entity
public class Post extends KigaEntityModel {
  private String content;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "authorID", referencedColumnName = "id")
  @NotFound(action = NotFoundAction.IGNORE)
  private Member author;

  @ManyToOne
  @JoinColumn(name = "threadID", referencedColumnName = "id")
  private ForumThread thread;

  private String anonNickname;

  public Post() {
    this.setClassName("Post");
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  /*
  PostStatus status;

  public PostStatus getStatus() {
    return status;
  }

  public void setStatus(PostStatus status) {
    this.status = status;
  }

*/
  public Member getAuthor() {
    return author;
  }

  public void setAuthor(Member author) {
    this.author = author;
  }

  public ForumThread getThread() {
    return thread;
  }

  public void setThread(ForumThread thread) {
    this.thread = thread;
  }

  public String getAnonNickname() {
    return anonNickname;
  }

  public void setAnonNickname(String anonNickname) {
    this.anonNickname = anonNickname;
  }
}
