package com.kiga.survey.web.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Map;

/**
 * Created by rainerh on 11.05.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SaveRequest extends FindQuestionnaireRequest {
  private Map<String, String> questionnaireData;
  private boolean finish;
}
