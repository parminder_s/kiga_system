package com.kiga.survey.web.service;

import java.io.IOException;

/**
 * Created by rainerh on 11.05.16.
 */
public interface EmailNotifierRunner {
  void execute() throws IOException;
}
