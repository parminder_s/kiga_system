package com.kiga.survey.web.response;

import lombok.Data;

/**
 * Created by rainerh on 11.05.16.
 */
@Data
public class FindQuestionnaireResponse {
  private String surveyData;
  private String status;

  public FindQuestionnaireResponse() {
  }

  public FindQuestionnaireResponse(String surveyData, String status) {
    this.surveyData = surveyData;
    this.status = status;
  }
}
