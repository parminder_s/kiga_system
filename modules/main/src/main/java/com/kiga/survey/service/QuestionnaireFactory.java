package com.kiga.survey.service;

import com.kiga.security.domain.Member;
import com.kiga.survey.domain.Questionnaire;
import com.kiga.survey.domain.Survey;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.security.crypto.keygen.StringKeyGenerator;

/**
 * Created by rainerh on 11.05.16.
 */
public class QuestionnaireFactory {
  /**
   * creates a questionnaire which is only accessible via salt and id.
   */
  public Questionnaire create(String referenceCode, String referenceId,
                              Survey survey, Member member) {
    Questionnaire returner = new Questionnaire();
    returner.setIsCompleted(false);
    returner.setIsNotifiedByEMail(false);
    returner.setIsNotifiedByMessage(false);
    returner.setReferenceCode(referenceCode);
    returner.setReferenceId(referenceId);
    returner.setSurvey(survey);
    returner.setMember(member);
    returner.setSalt(KeyGenerators.string().generateKey());

    return returner;
  }
}
