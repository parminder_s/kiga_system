package com.kiga.survey.repository;

import com.kiga.survey.domain.Questionnaire;
import org.aspectj.weaver.patterns.TypePatternQuestions;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by rainerh on 10.05.16.
 */
public interface QuestionnaireRepository extends CrudRepository<Questionnaire, Long> {
  @Query("select q from Questionnaire q where "
    + "q.id = :id and q.salt = :salt and q.isCompleted = :isCompleted")
  Questionnaire findOpened(
    @Param("id") Long id, @Param("salt") String salt, @Param("isCompleted") Boolean isCompleted);

  List<Questionnaire> findFirst10ByIsNotifiedByEMail(Boolean isNotifiedByEMail);
}
