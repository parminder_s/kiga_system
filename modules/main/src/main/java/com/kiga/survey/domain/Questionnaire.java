package com.kiga.survey.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.s3.domain.converters.MapToJsonConverter;
import com.kiga.security.domain.Member;

import java.util.Map;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by rainerh on 10.05.16.
 *
 * <p>Stores the data of a filled in survey as map.
 */
@Entity
public class Questionnaire extends KigaEntityModel {
  @Convert(converter = MapToJsonConverter.class)
  private Map<String, String> questionnaireData;

  private String referenceCode;
  private String referenceId;
  private String salt;
  private Boolean isCompleted;

  private Boolean isNotifiedByEMail;
  private Boolean isNotifiedByMessage;

  @ManyToOne
  @JoinColumn(name = "memberId", referencedColumnName = "id")
  private Member member;

  @ManyToOne
  @JoinColumn(name = "surveyId", referencedColumnName = "id")
  private Survey survey;

  public Questionnaire() {
    this.setClassName("Questionnaire");
  }

  public Map<String, String> getQuestionnaireData() {
    return questionnaireData;
  }

  public void setQuestionnaireData(Map<String, String> questionnaireData) {
    this.questionnaireData = questionnaireData;
  }

  public void setReferenceCode(String referenceCode) {
    this.referenceCode = referenceCode;
  }

  public void setReferenceId(String referenceId) {
    this.referenceId = referenceId;
  }

  public Boolean getIsCompleted() {
    return isCompleted;
  }

  public void setIsCompleted(Boolean completed) {
    isCompleted = completed;
  }

  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public Survey getSurvey() {
    return survey;
  }

  public void setSurvey(Survey survey) {
    this.survey = survey;
  }

  public String getSalt() {
    return salt;
  }

  public void setSalt(String salt) {
    this.salt = salt;
  }

  public Boolean getIsNotifiedByEMail() {
    return isNotifiedByEMail;
  }

  public void setIsNotifiedByEMail(Boolean notifiedByEMail) {
    isNotifiedByEMail = notifiedByEMail;
  }

  public Boolean getIsNotifiedByMessage() {
    return isNotifiedByMessage;
  }

  public void setIsNotifiedByMessage(Boolean notifiedByMessage) {
    isNotifiedByMessage = notifiedByMessage;
  }
}
