package com.kiga.survey.web;

import com.kiga.survey.domain.Questionnaire;
import com.kiga.survey.repository.QuestionnaireRepository;
import com.kiga.survey.web.request.FindQuestionnaireRequest;
import com.kiga.survey.web.request.SaveRequest;
import com.kiga.survey.web.response.FindQuestionnaireResponse;
import com.kiga.survey.web.service.EmailNotifierRunner;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by rainerh on 10.05.16.
 *
 * <p>Returns an stores the answers for surveys.
 */
@RestController
@RequestMapping("survey")
public class SurveyController {
  private QuestionnaireRepository questionnaireRepository;

  private EmailNotifierRunner emailNotifierRunner;

  @Autowired
  public SurveyController(
    QuestionnaireRepository questionnaireRepository, EmailNotifierRunner emailNotifierRunner) {
    this.questionnaireRepository = questionnaireRepository;
    this.emailNotifierRunner = emailNotifierRunner;
  }

  /**
   * saves a new questionnaire.
   */
  @RequestMapping("save")
  public Boolean save(@RequestBody SaveRequest saveRequest) {
    Questionnaire questionnaire = questionnaireRepository.findOpened(
      saveRequest.getId(), saveRequest.getSalt(), false);
    if (questionnaire != null) {
      questionnaire.setQuestionnaireData(saveRequest.getQuestionnaireData());
      if (saveRequest.isFinish()) {
        questionnaire.setIsCompleted(true);
      }
      questionnaireRepository.save(questionnaire);
      return true;
    } else {
      return false;
    }
  }

  /**
   * returns survey data for an uncompleted questionnaire.
   */
  @RequestMapping("findQuestionnaire")
  public FindQuestionnaireResponse findQuestionnaire(
    @RequestBody FindQuestionnaireRequest findQuestionnaireRequest) throws IOException {
    Questionnaire questionnaire = questionnaireRepository.findOpened(
      findQuestionnaireRequest.getId(), findQuestionnaireRequest.getSalt(), false);
    if (questionnaire == null) {
      return new FindQuestionnaireResponse("", "completed");
    } else {
      return new FindQuestionnaireResponse(
        IOUtils.toString(getClass().getResourceAsStream("/com.kiga.survey/shop1.csv")),
        "opened"
      );
    }
  }

  /**
   * executes the email notification for open questionnaires.
   */
  @RequestMapping("runEMailNotifications")
  public void runEMailNotifications() throws IOException {
    emailNotifierRunner.execute();
  }
}
