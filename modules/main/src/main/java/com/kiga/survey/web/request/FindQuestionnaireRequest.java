package com.kiga.survey.web.request;

import lombok.Data;

/**
 * Created by rainerh on 10.05.16.
 */
@Data
public class FindQuestionnaireRequest {
  private String salt;
  private Long id;
}
