package com.kiga.survey;

import com.kiga.mail.Mailer;
import com.kiga.shop.ShopProperties;
import com.kiga.survey.repository.QuestionnaireRepository;
import com.kiga.survey.service.DefaultEmailNotifier;
import com.kiga.survey.service.EmailNotifier;
import com.kiga.survey.web.service.DefaultEmailNotifierRunner;
import com.kiga.survey.web.service.EmailNotifierRunner;
import com.kiga.web.service.UrlGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SurveyConfig {
  @Bean
  public EmailNotifier getEmailNotifier(
      Mailer mailer, UrlGenerator urlGenerator, ShopProperties shopProperties) {
    return new DefaultEmailNotifier(mailer, urlGenerator, shopProperties.getShopEmail());
  }

  @Bean
  public EmailNotifierRunner getEmailNotifierRunner(
      EmailNotifier emailNotifier, QuestionnaireRepository questionnaireRepository) {
    return new DefaultEmailNotifierRunner(emailNotifier, questionnaireRepository);
  }
}
