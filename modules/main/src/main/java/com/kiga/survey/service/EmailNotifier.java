package com.kiga.survey.service;

import org.springframework.stereotype.Service;

/**
 * Created by rainerh on 11.05.16.
 */
@Service
public interface EmailNotifier {
  void notify(Long id, String salt, String email, String content);
}
