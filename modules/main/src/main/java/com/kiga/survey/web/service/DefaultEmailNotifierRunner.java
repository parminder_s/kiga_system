package com.kiga.survey.web.service;

import com.kiga.survey.domain.Questionnaire;
import com.kiga.survey.repository.QuestionnaireRepository;
import com.kiga.survey.service.EmailNotifier;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.List;

/**
 * Created by rainerh on 11.05.16.
 *
 * <p>Requests all non-notified questionnaires from database and invokes
 * the EmailNotifier on each entry. Stops at 10 questionnaires.
 */
public class DefaultEmailNotifierRunner implements EmailNotifierRunner {
  private EmailNotifier emailNotifier;
  private QuestionnaireRepository questionnaireRepository;

  /**
   * inject dependencies.
   */
  public DefaultEmailNotifierRunner(
    EmailNotifier emailNotifier, QuestionnaireRepository questionnaireRepository) {
    this.emailNotifier = emailNotifier;
    this.questionnaireRepository = questionnaireRepository;
  }

  /**
   * runs the execution task.
   */
  @Override
  public void execute() throws IOException {
    String content = IOUtils
      .toString(getClass().getResourceAsStream("/com.kiga.survey/shop-mail1.html"));
    List<Questionnaire> questionnaires = questionnaireRepository
      .findFirst10ByIsNotifiedByEMail(false);
    questionnaires
      .stream()
      .forEach(questionnaire -> {
        emailNotifier.notify(
          questionnaire.getId(), questionnaire.getSalt(),
          questionnaire.getMember().getEmail(), content
        );
        questionnaire.setIsNotifiedByEMail(true);
        questionnaireRepository.save(questionnaire);
      });
  }
}
