package com.kiga.survey.service;

import com.kiga.mail.Mailer;
import com.kiga.web.service.UrlGenerator;

/**
 * Created by rainerh on 11.05.16.
 *
 * <p>Sends a notification for a questionnaire to a specific user. The body of the email must be
 * passed as string (html content) where the link is replaced by the expresssion $basePath.
 */
public class DefaultEmailNotifier implements EmailNotifier {
  private Mailer mailer;
  private UrlGenerator urlGenerator;
  private String sender;

  /** inject dependencies. */
  public DefaultEmailNotifier(Mailer mailer, UrlGenerator urlGenerator, String sender) {
    this.mailer = mailer;
    this.urlGenerator = urlGenerator;
    this.sender = sender;
  }

  /** Sends a notification to a user with a link to a questionnaire. */
  @Override
  public void notify(Long id, String salt, String email, String content) {
    String url = urlGenerator.getNgUrl("de/survey/start/" + id.toString() + "/" + salt);
    String contentWithUrl = content.replaceAll("\\$basePath", url);

    mailer.send(
        "survey",
        "lapbag",
        Long.toString(id),
        e ->
            e.withSubject("Ihre Meinung ist gefragt - LAPBAG Wünsche")
                .withBody(contentWithUrl)
                .withTo(email)
                .withFrom(sender));
  }
}
