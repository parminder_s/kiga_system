package com.kiga.survey.domain;

import com.kiga.db.KigaEntityModel;
import javax.persistence.Entity;

/** Created by rainerh on 10.05.16. */
@Entity(name = "QuestionnaireSurvey")
public class Survey extends KigaEntityModel {
  private String urlSegment;

  public Survey() {
    this.setClassName("Survey");
  }

  public String getUrlSegment() {
    return urlSegment;
  }

  public void setUrlSegment(String urlSegment) {
    this.urlSegment = urlSegment;
  }
}
