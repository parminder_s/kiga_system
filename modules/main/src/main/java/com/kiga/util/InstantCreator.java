package com.kiga.util;

import static com.kiga.util.NowService.KIGAZONE;

import java.time.Instant;
import java.time.ZonedDateTime;

public class InstantCreator {
  public static Instant create(int year, int month, int day) {
    return ZonedDateTime.of(year, month, day, 0, 0, 0, 0, KIGAZONE).toInstant();
  }
}
