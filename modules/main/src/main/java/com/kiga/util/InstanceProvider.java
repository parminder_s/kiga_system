package com.kiga.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * @author bbs
 * @since 3/5/17.
 */
@Service
public class InstanceProvider {
  private ApplicationContext appContext;

  @Autowired
  public InstanceProvider(ApplicationContext appContext) {
    this.appContext = appContext;
  }

  /**
   * Get instance of a class. Try to locate bean first. If none found, try to create new instance.
   *
   * @param clazz class
   * @param <T> generic type
   * @return instance
   */
  public <T> T getInstance(Class<T> clazz) {
    T instance = getBean(clazz);

    if (instance == null) {
      instance = createNewObject(clazz);
    }

    return instance;
  }

  private <T> T getBean(Class<T> clazz) {
    try {
      return appContext.getBean(clazz);
    } catch (BeansException beansException) {
      return null;
    }
  }

  private <T> T createNewObject(Class<T> clazz) {
    try {
      return clazz.newInstance();
    } catch (InstantiationException | IllegalAccessException exception) {
      return null;
    }
  }
}
