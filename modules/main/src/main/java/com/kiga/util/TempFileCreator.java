package com.kiga.util;

import java.io.File;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

/**
 * This class creates is a wrapper for getting a temporary File object. and also ensure
 * platform-specific paths.
 */
@Service
public class TempFileCreator {
  public File createFile(String filename) {
    String tempDirectory = System.getProperty("java.io.tmpdir");
    return new File(tempDirectory + File.separator + filename);
  }

  public File createFile() {
    return createFile(RandomStringUtils.randomAlphabetic(12));
  }
}
