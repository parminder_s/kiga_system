package com.kiga.util.mapper;

import static com.kiga.util.NowService.KIGAZONE;

import java.time.Instant;
import java.time.LocalDate;

public class InstantLocalDateMapper {
  public LocalDate asLocalDate(Instant instant) {
    return instant.atZone(KIGAZONE).toLocalDate();
  }

  public Instant asLocalDate(LocalDate localDate) {
    return localDate.atStartOfDay(KIGAZONE).toInstant();
  }
}
