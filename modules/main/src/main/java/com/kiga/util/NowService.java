package com.kiga.util;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import org.springframework.stereotype.Service;

/**
 * This service should be used everywhere, where we require the current date.
 *
 * <p>We don't use simply Instant.now(), because results in hard-to-test code.
 */
@Service
public class NowService {
  public static ZoneId KIGAZONE = ZoneId.of("Europe/Vienna");

  public Instant getInstant() {
    return Instant.now();
  }

  public ZonedDateTime getZoned() {
    return ZonedDateTime.now(KIGAZONE);
  }

  @Deprecated
  public Date getDate() {
    return new Date();
  }
}
