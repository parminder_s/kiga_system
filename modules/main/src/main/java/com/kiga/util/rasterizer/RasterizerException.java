package com.kiga.util.rasterizer;

import java.io.IOException;

/**
 * Created by robert on 30.07.17.
 */
public class RasterizerException extends IOException {
  public RasterizerException(Throwable cause) {
    super(cause);
  }

  public RasterizerException(String message) {
    super(message);
  }
}
