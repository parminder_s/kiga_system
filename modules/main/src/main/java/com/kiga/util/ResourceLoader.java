package com.kiga.util;

import java.io.InputStream;

/**
 * @author bbs
 * @since 10/25/16.
 */
public final class ResourceLoader {
  public static InputStream getResource(String streamSource) {
    return ResourceLoader.class.getClassLoader().getResourceAsStream(streamSource);
  }
}
