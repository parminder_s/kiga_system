package com.kiga.util.pdf2svg;

import java.io.IOException;

/**
 * Created by robert on 30.07.17.
 */
public class Pdf2SvgConversionException extends IOException {
  public Pdf2SvgConversionException(Throwable cause) {
    super(cause);
  }

  public Pdf2SvgConversionException(String message) {
    super(message);
  }
}
