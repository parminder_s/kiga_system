package com.kiga.util.mapper;

import com.kiga.main.locale.Locale;

/**
 * mapper for MapStruct to map a String to a @Locale.
 */
public class LocaleStringMapper {
  public Locale asLocale(String string) {
    return Locale.valueOf(string);
  }

  public String asString(Locale locale) {
    return locale.toString();
  }

}
