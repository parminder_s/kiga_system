package com.kiga.util.mapper;

import org.mapstruct.MapperConfig;
import org.mapstruct.ReportingPolicy;

@MapperConfig(
  unmappedTargetPolicy = ReportingPolicy.ERROR
)
public class MapStructConfig {
}
