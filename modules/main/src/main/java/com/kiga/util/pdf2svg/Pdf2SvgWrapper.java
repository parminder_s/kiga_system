package com.kiga.util.pdf2svg;

import com.kiga.upload.UploadParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

/**
 * Created by robert on 30.07.17.
 */
@Service
public class Pdf2SvgWrapper {
  private UploadParameters uploadParameters;

  @Autowired
  public Pdf2SvgWrapper(UploadParameters uploadParameters) {
    this.uploadParameters = uploadParameters;
  }

  /**
   * Method converts file from pdf to svg.
   *
   * @param srcFile     PDF file to be converted to svg
   * @param outFilePath path of the converted svg file
   * @return converted file
   * @throws Pdf2SvgConversionException if convertion has failed
   */
  public File convert(File srcFile, String outFilePath) throws Pdf2SvgConversionException {
    try {
      String srcFilePath = srcFile.getAbsolutePath();

      new ProcessBuilder(uploadParameters.getPdf2SvgExecutable(), srcFilePath, outFilePath).start()
        .waitFor();

      File outFile = new File(outFilePath);

      if (outFile.exists()) {
        return outFile;
      } else {
        throw new Pdf2SvgConversionException(
          "Conversion has failed for file: `" + outFilePath + "`");
      }
    } catch (IOException | InterruptedException ioe) {
      throw new Pdf2SvgConversionException(ioe);
    }
  }
}
