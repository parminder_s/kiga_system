package com.kiga.util;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 4/10/17.
 */
@Service
public class BrowserPusherService {
  private static final Logger logger = LoggerFactory.getLogger(BrowserPusherService.class);

  /**
   * Push file to browser.
   *
   * @param response    response
   * @param inputStream inputStream
   * @param fileName    fileName
   * @param contentType contentType
   * @throws IOException if some error occurs
   */
  public void pushFile(HttpServletResponse response, InputStream inputStream,
    String fileName, String contentType) throws IOException {
    response.setContentType(contentType);
    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

    try {
      IOUtils.copy(inputStream, response.getOutputStream());
      response.flushBuffer();
    } catch (IOException exception) {
      logger.info("Error writing file to output stream. Filename was '{}'", fileName, exception);
      throw exception;
    }
  }
}
