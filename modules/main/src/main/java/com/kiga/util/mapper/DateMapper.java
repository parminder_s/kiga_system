package com.kiga.util.mapper;

import org.joda.time.DateTime;

import java.util.Date;

public class DateMapper {
  public String asString(Date date) {
    return new DateTime(date).toString("yyyy-MM-dd");
  }

  /**
   *
   * @param dateString date as string format: yyy-MM-dd.
   * @return if dateString is null, null, otherwise the string gets parsed to a date.
   */
  public Date asDate(String dateString) {
    if (dateString == null) {
      return null;
    }
    return DateTime.parse(dateString).toDate();
  }
}
