package com.kiga.util.mapper;

import com.kiga.kga.api.model.KgaParentViewModel;
import com.kiga.kga.api.model.ParentRoleEnum;
import com.kiga.kga.domain.KgaParentToChild;

import java.util.List;
import java.util.stream.Collectors;

public class KgaParentToChildMapper {

  List<KgaParentViewModel> asParentViewModel(List<KgaParentToChild> kgaParentToChild) {
    return kgaParentToChild.stream()
      .map(parent -> new KgaParentViewModel()
        .role(ParentRoleEnum.valueOf(parent.getRole().toString())))
      .collect(Collectors.toList());
  }

  List<KgaParentToChild> asKgaParentToChild(List<KgaParentViewModel> kgaParentViewModels) {
    throw new IllegalArgumentException("Not implemented");
  }
}
