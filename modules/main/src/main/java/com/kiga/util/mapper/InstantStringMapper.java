package com.kiga.util.mapper;

import java.time.Instant;

public class InstantStringMapper {
  public Instant asInstant(String string) {
    return Instant.parse(string);
  }

  public String asString(Instant instant) {
    return instant.toString();
  }
}
