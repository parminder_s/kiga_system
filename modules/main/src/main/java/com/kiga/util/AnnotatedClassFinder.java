package com.kiga.util;

import org.reflections.Reflections;
import org.springframework.stereotype.Service;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 1/22/17.
 */
@Service
public class AnnotatedClassFinder {
  private static final String DEFAULT_CLASSPATH_PREFIX = "com.kiga";

  /**
   * Lookup for list of classes annotated with provided annotation.
   *
   * @param annotation annotation
   * @param superType  super type
   * @param <T>        type to be returned
   * @return List of annotated classes
   */
  @SuppressWarnings("unchecked")
  public <T> List<Class<?>> lookup(Class<? extends Annotation> annotation,
    Class<T> superType) {
    Reflections reflections = new Reflections(DEFAULT_CLASSPATH_PREFIX);
    Set<Class<?>> lookedUpClasses = reflections.getTypesAnnotatedWith(annotation);
    return lookedUpClasses.stream().filter(superType::isAssignableFrom)
      .collect(Collectors.toList());
  }
}
