package com.kiga.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * Created by robert on 27.05.17.
 */
public class UniqueFileNameUtil {
  /**
   * Convert provided fileName to fileName with unique part to avoid names collisions.
   *
   * @param path path to be converted
   * @return converted path
   */
  public String getName(String path) {
    if (StringUtils.isBlank(path)) {
      return path;
    }

    String [] pathItems = path.split("\\.");
    pathItems[pathItems.length - 2] += RandomStringUtils.randomAlphabetic(5);

    return StringUtils.join(pathItems, ".");
  }
}
