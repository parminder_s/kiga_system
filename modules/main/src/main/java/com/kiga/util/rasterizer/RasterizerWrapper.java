package com.kiga.util.rasterizer;

import com.kiga.upload.UploadParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

/**
 * Created by robert on 31.07.17.
 */
@Service
public class RasterizerWrapper {
  private UploadParameters uploadParameters;

  @Autowired
  public RasterizerWrapper(UploadParameters uploadParameters) {
    this.uploadParameters = uploadParameters;
  }

  /**
   * Rasterize file.
   *
   * @param srcFile     src file (svg) to be rasterized
   * @param outFilePath output file path (Rasterized)
   * @param outMimeType mime type of output file
   * @return rasterized file
   * @throws RasterizerException if rasterization failed
   */
  public File rasterize(File srcFile, String outFilePath, String outMimeType)
    throws RasterizerException {
    try {
      String srcFilePath = srcFile.getAbsolutePath();

      new ProcessBuilder(uploadParameters.getRasterizerExecutable(),
        srcFilePath,
        "-m", outMimeType,
        "-d", outFilePath).start().waitFor();

      File outFile = new File(outFilePath);

      if (outFile.exists()) {
        return outFile;
      } else {
        throw new RasterizerException(
          "Rasterization has failed for file: `" + outFilePath + "` with mime type: `"
            + outMimeType + "`");
      }
    } catch (IOException | InterruptedException ioe) {
      throw new RasterizerException(ioe);
    }
  }
}
