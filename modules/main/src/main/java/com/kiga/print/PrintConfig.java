package com.kiga.print;

import com.kiga.print.validation.ScheduledValidation;
import java.util.function.Supplier;
import lombok.Data;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/** Created by rainerh on 22.03.17. */
@Configuration
@ConfigurationProperties("print")
@Data
public class PrintConfig {
  private boolean enableValidation;
  private String cronExpression;

  @Bean
  public Supplier<PDFMergerUtility> pdfMergerUtilityProducer() {
    return () -> new PDFMergerUtility();
  }

  @Bean
  public ScheduledValidation scheduleValidation() {
    return isEnableValidation() ? new ScheduledValidation(cronExpression) : null;
  }
}
