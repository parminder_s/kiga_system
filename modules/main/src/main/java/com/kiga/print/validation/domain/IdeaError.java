package com.kiga.print.validation.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.db.LocaleToLongNotationConverter;
import com.kiga.main.locale.Locale;

import javax.persistence.Convert;
import javax.persistence.Entity;

/**
 * Created by rainerh on 03.01.17.
 */
@Entity
public class IdeaError extends KigaEntityModel {
  private long ideaValidationReportId;

  @Convert(converter = LocaleToLongNotationConverter.class)
  private Locale locale;

  private int errorGroup;

  private String errorDetail;
  private String content;
  private int codeLine;
  private int codeColumn;
  private boolean autoCorrected;
  private boolean autoCorrectable;
  private long articleId;

  public IdeaError() {
  }

  public long getIdeaValidationReportId() {
    return ideaValidationReportId;
  }

  public void setIdeaValidationReportId(long ideaValidationReportId) {
    this.ideaValidationReportId = ideaValidationReportId;
  }

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  public int getErrorGroup() {
    return errorGroup;
  }

  public void setErrorGroup(int errorGroup) {
    this.errorGroup = errorGroup;
  }

  public String getErrorDetail() {
    return errorDetail;
  }

  public void setErrorDetail(String errorDetail) {
    this.errorDetail = errorDetail;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public int getCodeLine() {
    return codeLine;
  }

  public void setCodeLine(int codeLine) {
    this.codeLine = codeLine;
  }

  public int getCodeColumn() {
    return codeColumn;
  }

  public void setCodeColumn(int codeColumn) {
    this.codeColumn = codeColumn;
  }

  public long getArticleId() {
    return articleId;
  }

  public void setArticleId(long articleId) {
    this.articleId = articleId;
  }

  public boolean isAutoCorrected() {
    return autoCorrected;
  }

  public void setAutoCorrected(boolean autoCorrected) {
    this.autoCorrected = autoCorrected;
  }

  public boolean isAutoCorrectable() {
    return autoCorrectable;
  }

  public void setAutoCorrectable(boolean autoCorrectable) {
    this.autoCorrectable = autoCorrectable;
  }
}
