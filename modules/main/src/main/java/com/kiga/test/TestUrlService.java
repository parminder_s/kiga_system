package com.kiga.test;

import com.kiga.test.subservices.IdeaTestUrlService;
import com.kiga.test.subservices.VoucherTestUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("testurl")
@Profile({"dev", "stage"})
public class TestUrlService {

  private IdeaTestUrlService ideaTestUrlService;
  private VoucherTestUrlService voucherTestUrlService;

  @Autowired
  public TestUrlService(
    VoucherTestUrlService voucherTestUrlService,
    IdeaTestUrlService ideaTestUrlService) {
    this.voucherTestUrlService = voucherTestUrlService;
    this.ideaTestUrlService = ideaTestUrlService;
  }

  /**
   * modify first new idea to have requested agegroup and return its name.
   */
  @RequestMapping(value = "ideas/agegroup")
  public String getIdeaWithAgeGroup(
    @RequestParam("agegroup") String agegroup) {

    return "New Idea with agegroup " + agegroup + ": "
      + ideaTestUrlService.getTestIdeaWithAgeGroup(agegroup);
  }

  /**
   * Create a new voucher with the given properties.
   */
  @RequestMapping(value = "shop/voucher")
  public String getVoucherWithProperties(
    @RequestParam("code") String code, @RequestParam("discount") Float discount,
    @RequestParam("productcode") String productCode) {

    String testVoucherInfo = voucherTestUrlService.getTestVoucherWithProperties(
      code, discount, productCode
    );

    return testVoucherInfo;
  }
}
