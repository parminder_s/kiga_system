package com.kiga.test.subservices;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.KigaIdeaRepositoryProxy;
import com.kiga.content.repository.ss.NewIdeasRepositoryProxy;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper.IdeaEngineHelper;
import com.kiga.main.locale.Locale;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile({"dev", "stage"})
@Service
public class IdeaTestUrlService {

  private NewIdeasRepositoryProxy newIdeasRepositoryProxy;
  private KigaIdeaRepositoryProxy kigaIdeaRepositoryProxy;
  private IdeaEngineHelper ideaEngineHelper;
  private CacheManager cacheManager;

  /** constructor. */
  @Autowired
  public IdeaTestUrlService(
      NewIdeasRepositoryProxy newIdeasRepositoryProxy,
      KigaIdeaRepositoryProxy kigaIdeaRepositoryProxy,
      IdeaEngineHelper ideaEngineHelper,
      CacheManager cacheManager) {

    this.newIdeasRepositoryProxy = newIdeasRepositoryProxy;
    this.kigaIdeaRepositoryProxy = kigaIdeaRepositoryProxy;
    this.ideaEngineHelper = ideaEngineHelper;
    this.cacheManager = cacheManager;
  }

  /** return Idea within given ageGroup. */
  public String getTestIdeaWithAgeGroup(String agegroup) {

    KigaIdea kigaIdea =
        newIdeasRepositoryProxy
            .getAllIdeas(Locale.en, 0)
            .stream()
            .filter(kigaIdeafilter -> ideaEngineHelper.hasValidParent(kigaIdeafilter))
            .collect(Collectors.toList())
            .get(0);

    int ageGroupFilter = convertAgeGroup(agegroup);
    // Modify first found kigaIdea to match requested properties
    kigaIdea.setAgeGroup(ageGroupFilter);

    kigaIdeaRepositoryProxy.save(kigaIdea);
    flushNewIdeasCache();
    return kigaIdea.getTitle();
  }

  private int convertAgeGroup(String agegroup) {

    int ageGroupFilter;

    switch (agegroup) {
      case "1to3":
        ageGroupFilter = 1;
        break;
      case "3to5":
        ageGroupFilter = 2;
        break;
      case "5to6":
        ageGroupFilter = 4;
        break;
      case "6to7":
        ageGroupFilter = 8;
        break;
      default:
        ageGroupFilter = 0;
    }

    return ageGroupFilter;
  }

  private void flushNewIdeasCache() {
    Cache kigaCache = cacheManager.getCache("KigaCache");
    List<Integer> ageGroupList = Arrays.asList(0, 1, 2, 4, 8);
    ageGroupList.forEach(
        ag ->
            kigaCache.evict(
                "ResolverRequest(super=com.kiga.web.message.EndpointRequest(locale=en), "
                    + "pathSegments=[ideas, new-ideas], ageGroup="
                    + ag
                    + ")"));
  }
}
