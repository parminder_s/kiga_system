package com.kiga.test.payment;

import com.kiga.main.locale.Locale;
import com.kiga.money.Money;
import com.kiga.payment.PaymentInitiator;
import com.kiga.payment.PaymentInitiatorConfig;
import com.kiga.payment.PaymentType;
import com.kiga.payment.api.controller.TestApi;
import com.kiga.payment.api.model.PaymentInitConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile({"dev", "stage"})
public class TestPaymentController implements TestApi {
  private PaymentInitiator paymentInitiator;

  @Autowired
  public TestPaymentController(PaymentInitiator paymentInitiator) {
    this.paymentInitiator = paymentInitiator;
  }

  /**
   * initiates a new payment and returns the url to redirect.
   */
  @Override
  public ResponseEntity<String> initiatePayment(PaymentInitConfig paymentInitConfig) {
    PaymentInitiatorConfig config = new PaymentInitiatorConfig(
      PaymentType.MP_VISA, "success", "failure", new Money("50", "EUR"), "at",
      Locale.de);
    return ResponseEntity.ok(paymentInitiator.initiate(config));
  }
}
