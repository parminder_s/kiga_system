package com.kiga.test.payment;


import com.kiga.main.locale.Locale;
import com.kiga.money.Money;
import com.kiga.payment.PaymentInitiatorConfig;
import com.kiga.payment.PaymentType;
import com.kiga.payment.api.model.PaymentInitConfig;

import java.math.BigDecimal;

public class PaymentInitMapper {
  /**
   * maps a paymentInitConfig from an endpoint to the logic one.
   */
  public PaymentInitiatorConfig purchaseToPurchaseResponse(
    PaymentInitConfig paymentInitConfig) {
    Money price = new Money(
      BigDecimal.valueOf(paymentInitConfig.getPrice().getAmount()),
      paymentInitConfig.getPrice().getCurrency()
    );

    return new PaymentInitiatorConfig(
      PaymentType.valueOf(paymentInitConfig.getPaymentType().toString()),
      paymentInitConfig.getSuccessUrl(), paymentInitConfig.getFailureUrl(),
      price, paymentInitConfig.getCountryCode(),
      Locale.valueOf(paymentInitConfig.getLocale()));
  }
}
