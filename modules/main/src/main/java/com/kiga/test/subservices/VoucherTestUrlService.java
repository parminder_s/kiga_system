package com.kiga.test.subservices;

import com.google.common.collect.Lists;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Voucher;
import com.kiga.shop.repository.ProductRepository;
import com.kiga.shop.repository.VoucherRepository;
import java.math.BigDecimal;
import java.util.Date;
import org.elasticsearch.common.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile({"dev", "stage"})
@Service
public class VoucherTestUrlService {

  private VoucherRepository voucherRepository;
  private ProductRepository productRepository;

  /** constructor. */
  @Autowired
  public VoucherTestUrlService(
      VoucherRepository voucherRepository, ProductRepository productRepository) {

    this.voucherRepository = voucherRepository;
    this.productRepository = productRepository;
  }

  /** return name of a voucher with given properties. */
  public String getTestVoucherWithProperties(String code, float discount, String productCode) {

    Voucher voucher = getVoucher(code);
    BigDecimal bigDiscount_unscaled = new BigDecimal(Float.toString(discount));
    BigDecimal bigDiscount = bigDiscount_unscaled.setScale(2);

    if (voucher != null) {
      voucher.setActivated(true);
      if (!voucher.getDiscountRate().equals(bigDiscount)) {
        voucher.setDiscountRate(bigDiscount);
      }

      Date today = new Date();
      DateTime dtOrg = new DateTime(today);
      DateTime tmr = dtOrg.plusDays(1);
      voucher.setValidToDate(tmr.toDate().toInstant());

      if (!voucher.getProduct().getCode().equals(productCode)) {
        voucher.setProduct(getProduct(productCode));
      }

    } else {
      voucher =
          Voucher.builder()
              .code(code)
              .discountRate(bigDiscount)
              .product(getProduct(productCode))
              .validFromDate((new Date()).toInstant())
              .validToDate((new Date().toInstant()))
              .activated(true)
              .build();
      voucher.setClassName("Voucher");
      voucher.setLastEdited(new Date());
      voucher.setCreated(new Date());
    }

    voucherRepository.save(voucher);

    return "VoucherCode: " + voucher.getCode() + "\nProductCode: " + voucher.getProduct().getCode();
  }

  private Voucher getVoucher(String voucherCode) {
    return voucherRepository.findByCode(voucherCode);
  }

  /** find product by code, if not found return first product in repository. */
  private Product getProduct(String productCode) {
    Product product = productRepository.findByCode(productCode);

    if (product == null) {
      product = Lists.newArrayList(productRepository.findAll()).get(0);
    }

    return product;
  }
}
