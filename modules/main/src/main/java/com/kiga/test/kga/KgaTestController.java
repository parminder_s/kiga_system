package com.kiga.test.kga;

import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile({"dev", "stage"})
public class KgaTestController {}
