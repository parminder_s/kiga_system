## Test Url Service

#### Entry Point

[TestUrlService](TestUrlService.java)

#### Usage

Test URLs are requested via following url pattern

kiga:8080/testurl/**module**/**service**?**parameter**=**value**

The 'testurl' RequestMapping points to the Entry Point.
From there the module and parameter name will point to a specific method
forwarding the request to a subservice.

#### Subservices

Each Module has a subservice that executes the request and returns a result.

#### Supported

* module
  * service
    * parameter
      * (values)

---

* ideas
  * agegroup
    * agegroup (String)
      * 1to3
      * 3to5
      * 5to6
      * 6to7

Submodule: [IdeaTestUrlService](subservices/IdeaTestUrlService.java)

Example Link:

kiga:8080/testurl/ideas/agegroup?agegroup=1to3

---

* shop
  * voucher
    * code (String)
      * the code for the voucher (e.g: friday, huhn, laterne)
      * if it already exists it will be modified else created
    * discount (float)
      * the discount for the voucher as float (e.g: 0.2 for 20%)
    * productcode
      * the code for the product (e.g: cd-1)
      * if the product does not exist the first existing product in
      * the repository will be returned

Submodule: [VoucherTestUrlService](subservices/VoucherTestUrlService.java)

Example Links:

kiga:8080/testurl/shop/voucher?code=lapbag&discount=0.50&productcode=lap-3-1
stage.kigaportal.com/endpoint/testurl/shop/voucher?code=lapbag&discount=0.50&productcode=lap-3-1

---
