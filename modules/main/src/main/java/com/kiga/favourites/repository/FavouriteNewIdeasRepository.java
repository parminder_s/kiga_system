package com.kiga.favourites.repository;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

/**
 * @author bbs
 * @since 7/25/16.
 */
public interface FavouriteNewIdeasRepository extends JpaRepository<KigaIdeaLive, Long> {
  @EntityGraph(value = "fetchForIdeaGroupsLive", type = EntityGraph.EntityGraphType.LOAD)
  @Query("select idea from #{#entityName} idea join idea.favourites favs WHERE favs.member = "
    + ":member and idea.inPool = 0 and idea.ageGroup is not null and idea.ageGroup > 0 and "
    + "(:ageGroupFilter = 0 or in_group(idea.ageGroup, :ageGroupFilter) = true) and idea.locale ="
    + " :locale order by idea.sortIndexMini desc, idea.title")
  List<KigaIdea> getIdeasForAllAgeGroups(@Param("locale") Locale locale,
                                         @Param("ageGroupFilter") int ageGroupFilter,
                                         @Param("member") Member member, Pageable pageable);

  @EntityGraph(value = "fetchForIdeaGroupsLive", type = EntityGraph.EntityGraphType.LOAD)
  @Query("select distinct idea from #{#entityName} idea join idea.favourites favs "
    + "WHERE favs.member = "
    + ":member and idea.inPool = 0 and idea.ageGroup is not null and idea.ageGroup > 0 and "
    + "(:ageGroupFilter = 0 or in_group(idea.ageGroup, :ageGroupFilter) = true) and idea.locale ="
    + " :locale order by idea.sortIndexMini desc, idea.title")
  List<KigaIdea> getAllIdeas(@Param("locale") Locale locale,
                             @Param("ageGroupFilter") int ageGroupFilter,
                             @Param("member") Member member);

  @EntityGraph(value = "fetchForIdeaGroupsLive", type = EntityGraph.EntityGraphType.LOAD)
  @Query("select idea from #{#entityName} idea join idea.favourites favs WHERE favs.member = "
    + ":member and idea.inPool = 0 and in_group(idea.ageGroup, :ageGroup) = true and "
    + "(:ageGroupFilter = 0 or in_group(idea.ageGroup, :ageGroupFilter) = true) and idea.locale ="
    + " :locale order by idea.sortIndexMini desc, idea.title")
  Set<KigaIdea> getIdeasForAgeGroupByBitPosition(@Param("ageGroup") int ageGroupCode,
                                                 @Param("locale") Locale locale,
                                                 @Param("ageGroupFilter") int ageGroupFilter,
                                                 @Param("member") Member member);
}
