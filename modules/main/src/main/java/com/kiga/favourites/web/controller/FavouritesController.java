package com.kiga.favourites.web.controller;

import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.cms.url.resolver.ResolvedUrlHierarchyItem;
import com.kiga.cms.url.resolver.UrlResolver;
import com.kiga.cms.web.request.ResolverRequest;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepositoryProxy;
import com.kiga.favourites.service.FavouritesService;
import com.kiga.favourites.web.requests.ToggleFavouriteRequest;
import com.kiga.favourites.web.response.FavouritesResponse;
import com.kiga.ideas.web.viewmodel.ideahierarchy.IdeaHierarchyManager;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author bbs
 * @since 11/1/16.
 */
@RestController
@RequestMapping("/favourites")
public class FavouritesController {
  private UrlResolver<IdeaGroupLive> urlResolver;
  private FavouritesService favouritesService;
  private SecurityService securityService;
  private IdeaHierarchyManager ideaHierarchyManager;
  private IdeaGroupContainerRepositoryProxy ideaGroupContainerRepository;

  /** Constructor with dependencies. */
  @Autowired
  public FavouritesController(
      UrlResolver<IdeaGroupLive> urlResolver,
      FavouritesService favouritesService,
      SecurityService securityService,
      IdeaHierarchyManager ideaHierarchyManager,
      IdeaGroupContainerRepositoryProxy ideaGroupContainerRepository) {
    this.urlResolver = urlResolver;
    this.favouritesService = favouritesService;
    this.securityService = securityService;
    this.ideaHierarchyManager = ideaHierarchyManager;
    this.ideaGroupContainerRepository = ideaGroupContainerRepository;
  }

  /**
   * Get favourites.
   *
   * @return response
   */
  @RequestMapping
  public FavouritesResponse favourites() {
    if (!securityService.getSessionMember().isPresent()) {
      return new FavouritesResponse();
    }
    securityService.requiresFullSubscription();
    FavouritesResponse returner = new FavouritesResponse();
    Member member = securityService.getSessionMember().get();
    returner.setFavourites(favouritesService.getMemberFavourites(member));
    return returner;
  }

  /**
   * Toggle favourite.
   *
   * @param toggleFavouriteRequest request
   */
  @RequestMapping("toggle")
  public void toggle(@RequestBody ToggleFavouriteRequest toggleFavouriteRequest) {
    securityService.requiresFullSubscription();
    Member member = securityService.getSessionMember().get();
    long ideaId = toggleFavouriteRequest.getIdeaId();
    favouritesService.toggle(ideaId, member);
  }

  /** checks if a given idea is a favourite of the current member. */
  @RequestMapping("isFavourite/{ideaId}")
  public boolean isFavourite(@PathVariable long ideaId) {
    Optional<Member> member = securityService.getSessionMember();
    if (member.isPresent()) {
      return this.favouritesService.getForIdea(member.get(), ideaId) != null;
    } else {
      return false;
    }
  }

  /**
   * Endpoint resolving type by provided path.
   *
   * @return resolved response
   */
  @ResponseBody
  @RequestMapping("/resolve")
  public ViewModel resolve(
      HttpServletRequest request,
      HttpServletResponse response,
      @RequestBody ResolverRequest resolverRequest)
      throws Exception {
    return resolveOld(request, response, resolverRequest);
  }

  private ViewModel resolveOld(
      HttpServletRequest request,
      HttpServletResponse response,
      @RequestBody ResolverRequest resolverRequest)
      throws Exception {

    securityService.requiresFullSubscription();

    String[] urlSegments =
        this.getUrlSegments(resolverRequest.getPathSegments(), resolverRequest.getLocale());
    ResolvedUrlHierarchyItem<IdeaGroupLive> resolvedItemHolder =
        urlResolver.resolve(urlSegments, resolverRequest.getLocale(), IdeaGroupLive.class);

    Member member = securityService.getSessionMember().get();

    IdeaCategory resolvedEntity = resolvedItemHolder.getResolvedItem();

    QueryRequestParameters queryResponseParameters =
        QueryRequestParameters.builder()
            .member(Optional.of(member))
            .pathSegments(resolverRequest.getPathSegments())
            .ageGroup(resolverRequest.getAgeGroup() == null ? 0 : resolverRequest.getAgeGroup())
            .entity(resolvedEntity)
            .build();

    ViewModel viewModel =
        ideaHierarchyManager.getViewModel(request, response, queryResponseParameters);

    viewModel.setResolvedType(resolvedEntity.getClassName());
    return viewModel;
  }

  private String[] getUrlSegments(String[] requestedUrlSegments, Locale locale) {
    String[] urlSegment = {
      ideaGroupContainerRepository.findIdeaGroupMainContainer(locale).getUrlSegment()
    };

    return ArrayUtils.addAll(urlSegment, requestedUrlSegments);
  }
}
