package com.kiga.favourites.properties;

import com.kiga.ideas.properties.IdeasRetrieveProperties;
import com.kiga.security.domain.Member;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author bbs
 * @since 11/12/16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FavouriteIdeasRetrieveProperties extends IdeasRetrieveProperties {
  private Member member;
}
