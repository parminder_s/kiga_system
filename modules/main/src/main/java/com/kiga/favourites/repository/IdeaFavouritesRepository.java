package com.kiga.favourites.repository;

import com.kiga.favourites.domain.IdeaFavourite;
import com.kiga.security.domain.Member;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author bbs
 * @since 11/3/16.
 */
public interface IdeaFavouritesRepository extends CrudRepository<IdeaFavourite, Long>,
    QueryDslPredicateExecutor<IdeaFavourite> {
  List<IdeaFavourite> findByMember(Member member);

  IdeaFavourite findByMemberAndKigaIdeaLiveId(Member member, long kigaIdeaLiveId);
}
