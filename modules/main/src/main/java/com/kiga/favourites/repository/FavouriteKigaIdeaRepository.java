package com.kiga.favourites.repository;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;

/**
 * @author bbs
 * @since 11/9/16.
 */
public interface FavouriteKigaIdeaRepository extends JpaRepository<KigaIdeaLive, Long> {
  /**
   * List of all ideas together with ideaGroupToIdeas relations. We orders by property of
   * ideaGroupToIdea and hsqldb requires all columns from order by clause to be also provided on
   * select clause. Therefore we need to return Object[] where Object[0] is KigaIdea and Object[1]
   * is IdeaGroupToIdea
   *
   * @param id             id of the group
   * @param ageGroupFilter ageGroupFilter
   * @return Object[] please check description for more info
   */
  @EntityGraph(value = "fetchForIdeaGroupsLive", type = EntityGraph.EntityGraphType.LOAD)
  @Query("select distinct f from KigaIdeaLive f join f.favourites favs join f"
    + ".ideaGroupToIdeaList igti "
    + "join igti.ideaGroup g "
    + "WHERE favs.member = :member and g.id = :id AND f.inPool = 1 "
    + "and (:ageGroupFilter = 0 or in_group(f.ageGroup, :ageGroupFilter) = true) "
  )
  List<KigaIdea> findAllForGroup(@Param("id") Long id, @Param("ageGroupFilter") int ageGroupFilter,
    @Param("member") Member member);

  @EntityGraph(value = "fetchForIdeaGroupsLive", type = EntityGraph.EntityGraphType.LOAD)
  @Query("select distinct f from KigaIdeaLive f join f.favourites favs join f"
    + ".ideaGroupToIdeaList igti "
    + "join igti.ideaGroup g left join g.parent p1 left join p1.parent p2 left "
    + "join p2.parent p3 left join p3.parent p4 "
    + "WHERE favs.member = :member "
    + "and f.locale= :locale "
    + "and f.inPool = 1 and (:ageGroupFilter = 0 or in_group(f.ageGroup, :ageGroupFilter) = true) "
    + "and ((:level = 0 and :id in (p1.id, p2.id, p3.id, p4.id)) "
    + "or (:level = 1 and :id in (g.id, p1.id, p2.id, p3.id)) "
    + "or (:level = 2 and :id in (g.id, p1.id, p2.id)) "
    + "or (:level = 3 and :id in (g.id, p1.id)) "
    + "or (:level = 4 and :id = g.id)) ")
  List<KigaIdea> findPagedForGroup(
    @Param("locale") Locale locale, @Param("id") Long id, @Param("level") Integer level,
    @Param("ageGroupFilter") int ageGroupFilter, @Param("member") Member member);

  @EntityGraph(value = "fetchForIdeaGroupsLive", type = EntityGraph.EntityGraphType.LOAD)
  @Query("select distinct f from KigaIdeaLive f join f.favourites favs join f"
    + ".ideaGroupToIdeaList igti "
    + "WHERE favs.member = :member "
    + "and igti.sortTree LIKE CONCAT(:sortTree, '%') "
    + "and f.locale= :locale "
    + "and f.inPool = 1 and (:ageGroupFilter = 0 or in_group(f.ageGroup, :ageGroupFilter) = true) ")
  List<KigaIdea> findPagedForGroupSortTree(
    @Param("locale") Locale locale,
    @Param("sortTree") String sortTree,
    @Param("ageGroupFilter") int ageGroupFilter,
    @Param("member") Member member,
    Pageable pageable
  );

  @Query("select count(f) from KigaIdeaLive f join f.favourites favs join f"
    + ".ideaGroupToIdeaList igti "
    + "WHERE favs.member = :member "
    + "and igti.sortTree LIKE CONCAT(:sortTree, '%') "
    + "and f.locale= :locale "
    + "and f.inPool = 1 and (:ageGroupFilter = 0 or in_group(f.ageGroup, :ageGroupFilter) = true) ")
  int getIdeaCountForGroup(
    @Param("locale") Locale locale,
    @Param("sortTree") String sortTree,
    @Param("ageGroupFilter") int ageGroupFilter,
    @Param("member") Member member);

}
