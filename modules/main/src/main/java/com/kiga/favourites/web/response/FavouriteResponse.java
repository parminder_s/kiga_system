package com.kiga.favourites.web.response;

import lombok.Data;

/**
 * @author bbs
 * @since 11/4/16.
 */
@Data
public class FavouriteResponse {
  private Long id;
  private Long ideaId;
}
