package com.kiga.favourites.web.response;

import lombok.Data;

import java.util.List;

/**
 * @author bbs
 * @since 11/1/16.
 */
@Data
public class FavouritesResponse {
  private List<FavouriteResponse> favourites;
}
