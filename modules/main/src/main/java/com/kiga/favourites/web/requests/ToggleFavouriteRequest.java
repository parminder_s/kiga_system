package com.kiga.favourites.web.requests;

import lombok.Data;

/**
 * @author bbs
 * @since 11/4/16.
 */
@Data
public class ToggleFavouriteRequest {
  private long ideaId;
}
