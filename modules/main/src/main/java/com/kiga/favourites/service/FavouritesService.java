package com.kiga.favourites.service;

import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive;
import com.kiga.favourites.domain.IdeaFavourite;
import com.kiga.favourites.repository.IdeaFavouritesCustomQueriesRepository;
import com.kiga.favourites.repository.IdeaFavouritesRepository;
import com.kiga.favourites.web.converter.IdeaFavouriteToViewModelConverter;
import com.kiga.favourites.web.response.FavouriteResponse;
import com.kiga.security.domain.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 11/4/16.
 */
@Service
public class FavouritesService {
  private KigaIdeaRepositoryLive kigaIdeaRepositoryLive;
  private IdeaFavouritesRepository ideaFavouritesRepository;
  private IdeaFavouriteToViewModelConverter ideaFavouriteToViewModelConverter;
  private IdeaFavouritesCustomQueriesRepository ideaFavouritesCustomQueriesRepository;

  /**
   * Constructor with dependencies.
   *
   * @param kigaIdeaRepositoryLive                kiga idea repository
   * @param ideaFavouritesRepository              favs repo
   * @param ideaFavouriteToViewModelConverter     favs converter
   * @param ideaFavouritesCustomQueriesRepository ideaFavouritesCustomQueriesRepository
   */
  @Autowired
  public FavouritesService(KigaIdeaRepositoryLive kigaIdeaRepositoryLive,
    IdeaFavouritesRepository ideaFavouritesRepository,
    IdeaFavouriteToViewModelConverter ideaFavouriteToViewModelConverter,
    IdeaFavouritesCustomQueriesRepository ideaFavouritesCustomQueriesRepository) {
    this.kigaIdeaRepositoryLive = kigaIdeaRepositoryLive;
    this.ideaFavouritesRepository = ideaFavouritesRepository;
    this.ideaFavouriteToViewModelConverter = ideaFavouriteToViewModelConverter;
    this.ideaFavouritesCustomQueriesRepository = ideaFavouritesCustomQueriesRepository;
  }

  /**
   * Toggle favourite.
   *
   * @param ideaId idea id
   * @param member member
   */
  public void toggle(long ideaId, Member member) {
    IdeaFavourite ideaFavourite = ideaFavouritesCustomQueriesRepository
      .findFavouriteWithKigaIdea(member, ideaId);
    if (ideaFavourite == null) {
      ideaFavourite = new IdeaFavourite();

      KigaIdeaLive kigaIdeaLive = kigaIdeaRepositoryLive.findOne(ideaId);
      ideaFavourite.setKigaIdeaLive(kigaIdeaLive);
      ideaFavourite.setMember(member);

      ideaFavouritesRepository.save(ideaFavourite);
    } else {
      ideaFavouritesRepository.delete(ideaFavourite);
    }
  }

  public IdeaFavourite getForIdea(Member member, long ideaId) {
    return this.ideaFavouritesRepository.findByMemberAndKigaIdeaLiveId(member, ideaId);
  }

  /**
   * Retrieves list of favourites for given member.
   *
   * @param member member
   * @return list of favourites responses
   */
  public List<FavouriteResponse> getMemberFavourites(Member member) {
    List<IdeaFavourite> ideaFavourites = ideaFavouritesRepository.findByMember(member);
    return ideaFavouriteToViewModelConverter.convertToResponse(ideaFavourites).stream()
      .filter(this::isNotNull).collect(Collectors.toList());
  }

  private boolean isNotNull(FavouriteResponse favouriteResponse) {
    return favouriteResponse != null;
  }
}
