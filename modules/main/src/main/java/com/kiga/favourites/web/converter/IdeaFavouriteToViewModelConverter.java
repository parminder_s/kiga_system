package com.kiga.favourites.web.converter;

import com.kiga.favourites.domain.IdeaFavourite;
import com.kiga.favourites.web.response.FavouriteResponse;
import com.kiga.web.converter.DefaultViewModelConverter;
import org.springframework.stereotype.Service;

@Service
public class IdeaFavouriteToViewModelConverter
    extends DefaultViewModelConverter<IdeaFavourite, FavouriteResponse> {
  @Override
  public FavouriteResponse convertToResponse(IdeaFavourite entity) {
    if (entity.getKigaIdeaLive() == null) {
      return null;
    }
    FavouriteResponse favouriteResponse = new FavouriteResponse();
    favouriteResponse.setId(entity.getId());
    favouriteResponse.setIdeaId(entity.getKigaIdeaLive().getId());
    return favouriteResponse;
  }
}
