package com.kiga.favourites.domain;

import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.db.KigaEntityModel;
import com.kiga.security.domain.Member;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author bbs
 * @since 11/1/16.
 */
@Entity
public class IdeaFavourite extends KigaEntityModel {
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "kigaIdeaId", referencedColumnName = "id")
  @NotFound(action = NotFoundAction.IGNORE)
  private KigaIdeaLive kigaIdeaLive;

  @ManyToOne
  @JoinColumn(name = "memberId", referencedColumnName = "id")
  private Member member;

  public IdeaFavourite() {
    this.setClassName("IdeaFavourite");
  }

  public KigaIdeaLive getKigaIdeaLive() {
    return kigaIdeaLive;
  }

  public void setKigaIdeaLive(KigaIdeaLive kigaIdeaLive) {
    this.kigaIdeaLive = kigaIdeaLive;
  }

  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }
}
