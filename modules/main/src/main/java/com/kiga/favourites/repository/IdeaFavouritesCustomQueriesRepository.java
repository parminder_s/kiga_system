package com.kiga.favourites.repository;

import com.kiga.content.domain.ss.live.QKigaIdeaLive;
import com.kiga.favourites.domain.IdeaFavourite;
import com.kiga.favourites.domain.QIdeaFavourite;
import com.kiga.security.domain.Member;
import com.mysema.query.types.expr.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author bbs
 * @since 12/10/16.
 */
@Service
public class IdeaFavouritesCustomQueriesRepository {
  private IdeaFavouritesRepository ideaFavouritesRepository;

  @Autowired
  public IdeaFavouritesCustomQueriesRepository(IdeaFavouritesRepository ideaFavouritesRepository) {
    this.ideaFavouritesRepository = ideaFavouritesRepository;
  }

  /**
   * Find favourite with KigaIdea.
   *
   * @param member member
   * @param ideaId idea id
   * @return favourite idea
   */
  public IdeaFavourite findFavouriteWithKigaIdea(Member member, long ideaId) {
    QIdeaFavourite ideaFavourite = QIdeaFavourite.ideaFavourite;
    QKigaIdeaLive kigaIdeaLive = ideaFavourite.kigaIdeaLive;

    BooleanExpression memberMatchesExpression = ideaFavourite.member.eq(member);
    BooleanExpression ideaIdMatchesExpression = kigaIdeaLive.id.eq(ideaId);

    return ideaFavouritesRepository.findOne(memberMatchesExpression.and(ideaIdMatchesExpression));
  }
}
