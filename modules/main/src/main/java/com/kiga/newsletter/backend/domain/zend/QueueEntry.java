package com.kiga.newsletter.backend.domain.zend;

import com.kiga.db.KigaLegacyDbEntityModel;
import com.kiga.newsletter.backend.domain.QueueEntryStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


/**
 * A queue entry represents one single mailing (of a newsletter to one recipient).
 * Created by mfit on 05.10.16.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "nl_queue",
  uniqueConstraints = {@UniqueConstraint(columnNames = {"nlq_email", "nlq_newsletter"})})
public class QueueEntry extends KigaLegacyDbEntityModel {
  @Id
  @Column(name = "nlq_id")
  @GeneratedValue
  private Long id;

  @Column(name = "nlq_email")
  private String email;

  @Column(name = "nlq_status")
  @Enumerated(EnumType.STRING)
  private QueueEntryStatus status;

  @Column(name = "nlq_newsletter")
  private String newsletter;

  private Date sendDate;
  private Long sendTaskId;

  /**
   * Set up a new, pending queue entry.
   * @param email recipient's email address.
   * @param newsletter name of the newsletter
   * @return the queue entry.
   */
  public static QueueEntry createPendingEntry(String email, String newsletter) {
    QueueEntry queueEntry = new QueueEntry();
    queueEntry.setNewsletter(newsletter);
    queueEntry.setEmail(email);
    queueEntry.setStatus(QueueEntryStatus.pending);
    return queueEntry;
  }

  /**
   * Set up a new, pending queue entry.
   * @param email recipient's email address.
   * @param sendTask the sendTask.
   * @return the queue entry.
   */
  public static QueueEntry createPendingEntry(String email, SendTask sendTask) {
    QueueEntry queueEntry = new QueueEntry();
    queueEntry.setNewsletter(sendTask.getNewsletter().getName());
    queueEntry.setSendTaskId(sendTask.getId());
    queueEntry.setSendDate(sendTask.getSendDate());
    queueEntry.setEmail(email);
    queueEntry.setStatus(QueueEntryStatus.pending);
    return queueEntry;
  }
}
