package com.kiga.newsletter.backend.web.response;

import com.kiga.newsletter.backend.domain.SelectionStats;
import com.kiga.newsletter.backend.domain.SubscriptionTypeGroup;
import com.kiga.newsletter.backend.domain.zend.RecipientSelection;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mfit on 07.12.16.
 */
@Data
@NoArgsConstructor
public class SelectionResponse {
  private Long id;
  private String locale;
  private String country;
  private String parentCountry;
  private SubscriptionTypeGroup subscriptionType;
  private Integer subscriptionDuration;
  private Boolean shopFlag;
  private Boolean subscriptionFlag;
  private String subscriptionStatus;
  private SelectionStats stats;

  /**
   * Create transferable object from domain model.
   * @param selection the source object.
   */
  public SelectionResponse(RecipientSelection selection) {
    this.id = selection.getId();
    this.locale = selection.getLocale();
    this.country = selection.getCountry();
    this.parentCountry = selection.getParentCountry();
    this.subscriptionType = selection.getSubscriptionType();
    this.subscriptionStatus = selection.getSubscriptionStatus();
    this.subscriptionDuration = selection.getSubscriptionDuration();
    this.shopFlag = selection.getShopFlag();
    this.subscriptionFlag = selection.getSubscriptionFlag();
  }
}
