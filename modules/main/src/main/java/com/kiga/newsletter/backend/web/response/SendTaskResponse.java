package com.kiga.newsletter.backend.web.response;

import com.kiga.newsletter.backend.domain.SelectionStats;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by mfit on 08.03.17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendTaskResponse {
  Long taskId;
  Date created;
  Date finished;
  Boolean isFinished = false;
  SelectionStats stats;
}
