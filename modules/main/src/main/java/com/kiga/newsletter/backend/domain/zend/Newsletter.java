package com.kiga.newsletter.backend.domain.zend;

import lombok.Data;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * A Newsletter has content and meta data.
 * Created by mfit on 03.10.16.
 */
@Data
@Entity
@Table(name = "nl_newsletter")
public class Newsletter {

  @Id
  @Column(name = "id")
  @GeneratedValue
  Long id;

  @Column
  String name;

  @Column
  String url;

  @Column
  String locale;

  @Column
  String subject;

  @Column(name = "publishing_date")
  Date publishingDate;

  @Column(name = "body_html")
  String body;

  @OneToMany(mappedBy = "newsletter")
  private List<RecipientSelection> selections;

}
