package com.kiga.newsletter.web;

import com.kiga.crm.service.CrmConverter;
import com.kiga.newsletter.web.request.SubscribeRequest;
import com.kiga.newsletter.web.response.SubscribeResponse;
import com.kiga.web.service.GeoIpService;
import org.apache.fop.fo.pagination.bookmarks.Bookmark;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Subscribe for newsletter endpoint.
 * Created by mfit on 01.02.17.
 */
@RestController
@RequestMapping("newsletter")
public class SubscriptionController {
  CrmConverter crmConverter;
  private GeoIpService geoIpService;

  @Autowired
  public SubscriptionController(CrmConverter crmConverter, GeoIpService geoIpService) {
    this.crmConverter = crmConverter;
    this.geoIpService = geoIpService;
  }

  /**
   * Subscribe an email to the newsletter.
   * @param subscribeRequest request containing email, locale and country.
   * @return status of the action.
     */
  @RequestMapping( value = "subscription", method = RequestMethod.POST)
  public SubscribeResponse postSubscriptionRequest(@RequestBody SubscribeRequest subscribeRequest) {
    SubscribeResponse response = new SubscribeResponse();
    boolean result = false;
    try {
      result = this.crmConverter.userNewsletterSubscribeRequest(
        subscribeRequest.getEmail(),
        subscribeRequest.getLocale().toString(),
        geoIpService.resolveCountryCode());
      response.setActionOk(result);
    } catch (Exception exception) {
      // pass
      response.setActionOk(false);
    }
    return response;
  }
}
