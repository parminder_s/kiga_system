package com.kiga.newsletter.backend.service;

import com.kiga.newsletter.backend.domain.QueueEntryStatus;
import com.kiga.newsletter.backend.domain.QueueStatus;
import com.kiga.newsletter.backend.domain.SelectionStats;
import com.kiga.newsletter.backend.domain.zend.Newsletter;
import com.kiga.newsletter.backend.domain.zend.QueueEntry;
import com.kiga.newsletter.backend.domain.zend.SendTask;
import com.kiga.newsletter.backend.repository.QueueEntryRepository;
import com.kiga.newsletter.backend.repository.RecipientRepository;
import com.kiga.newsletter.backend.repository.SendTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by mfit on 14.03.17.
 */
@Service
public class SendService {
  QueueEntryRepository queueEntryRepository;
  RecipientRepository recipientRepository;
  SendTaskRepository sendTaskRepository;

  /**
   * Constructor to set up selection service with all dependencies.
   * @param recipientRepository repository for recipients.
   * @param queueEntryRepository repository for queue entries.
   */
  @Autowired
  public SendService(RecipientRepository recipientRepository,
                          QueueEntryRepository queueEntryRepository,
                          SendTaskRepository sendTaskRepository) {
    this.recipientRepository = recipientRepository;
    this.queueEntryRepository = queueEntryRepository;
    this.sendTaskRepository = sendTaskRepository;
  }

  /**
   * Creates a send-task for a newsletter.
   * With a send-task, preProcess() can be called, the sendTask will recieve updates on the status
   * of putting recipients into the queue (=processing the sendTask).
   * @param newsletter the newsletter to create the send-task from.
   * @return the created send-task.
   */
  public SendTask createSendTask(Newsletter newsletter) {
    SendTask sendTask = new SendTask(newsletter);
    this.sendTaskRepository.save(sendTask);
    return sendTask;
  }

  /**
   * Get counts per status for a newsletter. Queries the QueueEntryRepository and
   * maps from a result set of queue entries, counted and grouped by QueueEntryStatus to
   * a SelectionStats object.
   * @param newsletter the newsletter to be sent.
   */
  public SelectionStats getQueueStatus(Newsletter newsletter) {

    List<QueueStatus> queueStatusList = this.queueEntryRepository
      .getStatusForNewsletterName(newsletter.getName());
    HashMap<QueueEntryStatus, Long> result = new HashMap<>();
    queueStatusList.forEach(v -> result.put(v.getStatus(), v.getEntries()));

    SelectionStats selectionStats = new SelectionStats();
    selectionStats.setDone(result.containsKey(QueueEntryStatus.done)
      ? result.get(QueueEntryStatus.done) : 0L);
    selectionStats.setPending(result.containsKey(QueueEntryStatus.pending)
      ? result.get(QueueEntryStatus.pending) : 0L);
    selectionStats.setError(result.containsKey(QueueEntryStatus.error)
      ? result.get(QueueEntryStatus.error) : 0L);
    selectionStats.setTotal(selectionStats.getDone()
      + selectionStats.getPending()
      + selectionStats.getError());

    return selectionStats;
  }

  /**
   * Processes a send-task (=sends the newsletter to the queue, according to
   * the selections that are defined for it).
   * TODO: provide meaningful stats regarding error / skipped etc.
   * @param sendTask the sendTask to be processed to be sent.
   * @param recipients stream of recipient email addresses
   */
  @Async
  public void send(SendTask sendTask, Stream<String> recipients) {
    Newsletter newsletter = sendTask.getNewsletter();
    final QueueEntryRepository queueEntryRepository = this.queueEntryRepository;
    final String newsletterName = newsletter.getName();

    Map<Boolean, List<QueueEntry>> queueEntries = recipients
      .map(adr -> QueueEntry.createPendingEntry(adr, sendTask))
      .collect(Collectors.partitioningBy((QueueEntry queueEntry) ->
        queueEntryRepository.findByEmailAndNewsletter(queueEntry.getEmail(),
          queueEntry.getNewsletter()).size() == 0));

    sendTask.setRecipients(new Long(queueEntries.get(true).size()));
    sendTask.setSkipped(new Long(queueEntries.get(false).size()));
    sendTask.setCreated(new Date());
    sendTask.setIsFinished(false);
    this.sendTaskRepository.save(sendTask);

    Long okCount = 0L;
    Long errorCount = 0L;
    Long processCounter = 0L;

    for (QueueEntry queueEntry : queueEntries.get(true)) {
      try {
        this.queueEntryRepository.save(queueEntry);
        okCount += 1;
      } catch (Exception exception) {
        // catch (MySQLIntegrityConstraintViolationException) {
        errorCount += 1;
      }

      // Intermediate save of send task every 5000 submissions
      if (processCounter > 5000) {
        processCounter = 0L;
        sendTask.setDone(okCount);
        sendTask.setError(errorCount);
        this.sendTaskRepository.save(sendTask);
      }
      processCounter += 1;
    }

    // Task is done
    sendTask.setIsFinished(true);
    sendTask.setFinished(new Date());
    sendTask.setDone(okCount);
    sendTask.setError(errorCount);
    this.sendTaskRepository.save(sendTask);
  }

  /**
   * Remove single queue entry (newsletter -> email address) from the queue.
   * @param newsletter the newsletter.
   * @param recipientAddress the recipient address.
   */
  public void cleanQueueEntry(Newsletter newsletter, String recipientAddress) {
    List<QueueEntry> queueEntries = this.queueEntryRepository.findByEmailAndNewsletter(
      recipientAddress, newsletter.getName());
    if (queueEntries.size() > 0) {
      this.queueEntryRepository.delete(queueEntries.get(0));
    }
  }

}
