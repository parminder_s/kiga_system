package com.kiga.newsletter.backend.domain;

import lombok.Data;

/**
 * Contains result of aggregate operation (counts per status for a newsletter in the queue tab).
 * Created by mfit on 11.01.17.
 */
@Data
public class QueueStatus {
  private QueueEntryStatus status;
  private String newsletter;
  private Long entries;

  /**
   * Constructor with all fields.
   * @param status The status of the aggregation.
   * @param newsletter The newsletter of the aggregation.
   * @param entries the number of entries (rowcount) of the aggregation.
     */
  public QueueStatus(QueueEntryStatus status, String newsletter, Long entries) {
    this.status = status;
    this.newsletter = newsletter;
    this.entries = entries;
  }
}
