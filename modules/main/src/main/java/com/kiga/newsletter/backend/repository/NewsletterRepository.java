package com.kiga.newsletter.backend.repository;

import com.kiga.newsletter.backend.domain.zend.Newsletter;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by mfit on 03.10.16.
 */
public interface NewsletterRepository extends CrudRepository<Newsletter, Long> {
  public List<Newsletter> findAll();

  public List<Newsletter> findTop10ByOrderByPublishingDateDesc();


}
