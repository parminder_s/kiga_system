package com.kiga.newsletter.backend.repository;

import com.kiga.newsletter.backend.domain.zend.RecipientSelection;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by mfit on 07.12.16.
 */
public interface RecipientSelectionRepository extends CrudRepository<RecipientSelection, Long> {
  public List<RecipientSelection> findAll();

  public List<RecipientSelection> findByNewsletterId(Long id);

}
