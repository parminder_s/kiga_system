package com.kiga.newsletter.backend.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mfit on 07.12.16.
 */
@Data
@NoArgsConstructor
public class SelectionStats {
  long total = 0L;
  long pending = 0L;
  long error = 0L;
  long done = 0L;
  long skipped = 0L;

  /**
   * Create a SelectionStats object by providing all counts.
   * @param pending number of pending entries.
   * @param done number of done entries.
   * @param skipped number of skipped entries.
   * @param error number of entries for which an error has occured.
   */
  public SelectionStats(long pending, long done, long skipped, long error) {
    this.pending = pending;
    this.done = done;
    this.skipped = skipped;
    this.error = error;
  }
}
