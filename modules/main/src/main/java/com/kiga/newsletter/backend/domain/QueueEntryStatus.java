package com.kiga.newsletter.backend.domain;

/**
 * The status that a queue entry can be
 * Created by mfit on 05.10.16.
 */
public enum QueueEntryStatus {
  pending,
  done,
  error,
  canceled;
}
