package com.kiga.newsletter.backend.repository;

import com.kiga.newsletter.backend.domain.zend.Recipient;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by mfit on 03.10.16.
 */
public interface RecipientRepository
  extends CrudRepository<Recipient, Long>, JpaSpecificationExecutor<Recipient> {

  List<Recipient> findAll();

  List<Recipient> findByEmail(String email);

}

