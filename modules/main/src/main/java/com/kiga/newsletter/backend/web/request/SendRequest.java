package com.kiga.newsletter.backend.web.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by mfit on 02.05.17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendRequest {
  Date sendDate;
}
