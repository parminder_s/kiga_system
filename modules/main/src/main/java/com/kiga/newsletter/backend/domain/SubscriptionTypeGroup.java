package com.kiga.newsletter.backend.domain;

/**
 * Created by mfit on 25.01.17.
 */
public enum SubscriptionTypeGroup {
  parent,
  paed,
  test,
  gift
}
