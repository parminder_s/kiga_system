package com.kiga.newsletter.backend.repository;

import com.kiga.newsletter.backend.domain.zend.SendTask;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by mfit on 08.03.17.
 */
public interface SendTaskRepository extends CrudRepository<SendTask, Long> {
}
