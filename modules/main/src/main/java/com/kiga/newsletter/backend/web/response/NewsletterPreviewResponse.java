package com.kiga.newsletter.backend.web.response;

import lombok.Data;

/**
 * Created by mfit on 06.12.16.
 */
@Data
public class NewsletterPreviewResponse {
  private String content;
}
