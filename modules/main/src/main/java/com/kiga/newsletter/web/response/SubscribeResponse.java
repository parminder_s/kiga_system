package com.kiga.newsletter.web.response;

import lombok.Data;

/**
 * Created by mfit on 01.02.17.
 */
@Data
public class SubscribeResponse {
  private boolean actionOk;
  private String errorIndication;
}
