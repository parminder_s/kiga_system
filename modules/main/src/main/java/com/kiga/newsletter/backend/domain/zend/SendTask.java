package com.kiga.newsletter.backend.domain.zend;

import com.kiga.newsletter.backend.domain.SelectionStats;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * Created by mfit on 08.03.17.
 * For a newsletter, and selections, keep track of the process of putting all the selected
 * recipients in the queue.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "nl_sendtask")
public class SendTask {

  @Id
  @Column(name = "id")
  @GeneratedValue
  Long id;

  @ManyToOne
  @JoinColumn(name = "newsletterId", referencedColumnName = "id")
  Newsletter newsletter;

  Long recipients = 0L;
  Long done = 0L;
  Long skipped = 0L;
  Long error = 0L;
  Date created;
  Date finished;
  Boolean isFinished = false;
  Date sendDate;

  @Transient
  SelectionStats stats;

  public SendTask(Newsletter newsletter) {
    this.newsletter = newsletter;
  }

}
