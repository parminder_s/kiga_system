package com.kiga.newsletter.backend.web;

import com.kiga.newsletter.backend.domain.SelectionStats;
import com.kiga.newsletter.backend.domain.zend.Newsletter;
import com.kiga.newsletter.backend.domain.zend.SendTask;
import com.kiga.newsletter.backend.repository.NewsletterRepository;
import com.kiga.newsletter.backend.repository.RecipientRepository;
import com.kiga.newsletter.backend.repository.SendTaskRepository;
import com.kiga.newsletter.backend.service.SelectionService;
import com.kiga.newsletter.backend.service.SendService;
import com.kiga.newsletter.backend.web.request.SendRequest;
import com.kiga.newsletter.backend.web.request.SendTestRequest;
import com.kiga.newsletter.backend.web.response.NewsletterListResponse;
import com.kiga.newsletter.backend.web.response.NewsletterPreviewResponse;
import com.kiga.newsletter.backend.web.response.NewsletterResponse;
import com.kiga.newsletter.backend.web.response.SelectionResponse;
import com.kiga.newsletter.backend.web.response.SendTaskResponse;
import com.kiga.security.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Created by mfit on 03.10.16.
 */
@RestController
@RequestMapping("backend/newsletter")
public class NewsletterController {

  SecurityService securityService;
  RecipientRepository recipientRepository;
  NewsletterRepository newsletterRepository;
  SendTaskRepository sendTaskRepository;
  SelectionService selectionService;
  SendService sendService;

  /**
   * NewsletterController constructor.
   * @param securityService to check permission of the user / session
   * @param recipientRepository access to recipients that might recieve a mailing
   * @param newsletterRepository access to newsletters available for sending
   * @param selectionService service for adding and applying selections
   * @param sendService service for adding entries to the send queue
   */
  @Autowired
  public NewsletterController(SecurityService securityService,
                              RecipientRepository recipientRepository,
                              NewsletterRepository newsletterRepository,
                              SendTaskRepository sendTaskRepository,
                              SelectionService selectionService,
                              SendService sendService) {
    this.recipientRepository = recipientRepository;
    this.newsletterRepository = newsletterRepository;
    this.sendTaskRepository = sendTaskRepository;
    this.securityService = securityService;
    this.selectionService = selectionService;
    this.sendService = sendService;
  }

  /**
   * Fetch the available newsletters.
   * @return the response with the list of newsletters
   */
  @RequestMapping( value = "newsletters", method = RequestMethod.GET)
  public NewsletterListResponse getNewsletters() {
    securityService.requireShopAdmin();
    NewsletterListResponse returner = new NewsletterListResponse();
    List<NewsletterResponse> newsletter = this.newsletterRepository
          .findTop10ByOrderByPublishingDateDesc()
          .stream()
          .map(nl -> {
            NewsletterResponse newsletterResponse = new NewsletterResponse(nl);
            newsletterResponse.setStats(sendService.getQueueStatus(nl));
            return newsletterResponse;
          })
          .collect(Collectors.toList());
    returner.setNewsletters(newsletter);
    return returner;
  }

  /**
   * Fetch detailed data about one newsletter.
   * @param newsletterId the id of the newsletter.
   * @return the newsletter response, containing newsletter and related data.
   */
  @RequestMapping( value = "newsletter/{newsletterId}", method = RequestMethod.GET )
  public NewsletterResponse getNewsletter(@PathVariable Long newsletterId) {
    securityService.requireShopAdmin();
    Newsletter newsletter = this.newsletterRepository.findOne(newsletterId);
    NewsletterResponse returner = new NewsletterResponse(newsletter);

    List<SelectionResponse> selectionResponses =
      this.selectionService.getSendingStats(newsletter)
        .entrySet().stream()
        .map(entry -> {
          SelectionResponse selectionResponse = new SelectionResponse(entry.getKey());
          selectionResponse.setStats(entry.getValue());
          return selectionResponse;
        })
        .sorted((firstItem, secondItem) -> (int) (firstItem.getId() - secondItem.getId()))
        .collect(Collectors.toList());

    returner.setSelections(selectionResponses);
    return returner;
  }

  /**
   * Return the content of a newsletter (pre-fetched HTML that is kept with the nl entity.
   * @TODO when/howto actually fetch the document (and in particular, re-fetch + update the content)
   * @param newsletterId Id of the nesletter.
   * @return the response that contains the content as HTML.
   */
  @RequestMapping( value = "newsletter/{newsletterId}/preview", method = RequestMethod.GET)
  public NewsletterPreviewResponse getPreview(@PathVariable long newsletterId) {
    securityService.requireShopAdmin();
    Newsletter newsletter = this.newsletterRepository.findOne(newsletterId);
    NewsletterPreviewResponse returner = new NewsletterPreviewResponse();
    returner.setContent(newsletter.getBody());
    return returner;
  }

  /**
   * Get HTML content of newsletter, directly and not wrapped in JSON.
   * @param newsletterId id of newsletter.
   * @param response response, for setting X-Frame-Options on.
   * @return response of html content.
   */
  @RequestMapping(value = "newsletter/{newsletterId}/previewRaw",
                  produces = MediaType.TEXT_HTML_VALUE)
  public @ResponseBody String getPreviewHtml(@PathVariable long newsletterId,
                                             HttpServletResponse response) {
    securityService.requireShopAdmin();
    Newsletter newsletter = this.newsletterRepository.findOne(newsletterId);
    response.setHeader("X-Frame-Options", "SAMEORIGIN");
    // response.setHeader("X-Frame-Options", "ALLOW-FROM *");

    return newsletter.getBody();
  }

  /**
   * Sends to single recipient(s).
   * @param newsletterId the newsletter to be sent.
   * @param sendTestRequest contains email to send to.
   * @return send status.
   */
  @RequestMapping( value = "newsletter/{newsletterId}/testsend", method = RequestMethod.POST )
  public SendTaskResponse sendTestNewsletter(@PathVariable long newsletterId,
                                             @RequestBody SendTestRequest sendTestRequest) {
    securityService.requireShopAdmin();
    Newsletter newsletter = this.newsletterRepository.findOne(newsletterId);
    this.sendService.cleanQueueEntry(newsletter, sendTestRequest.getEmail());
    SendTask sendTask = this.sendService.createSendTask(newsletter);
    this.sendService.send(sendTask, Stream.of(sendTestRequest.getEmail()));
    return new SendTaskResponse(0L, new Date(), new Date(), true,
      new SelectionStats(0L, 1, 1, 0));
  }


  /**
   * Send a newsletter. Create a queue entry for the newsletter for every recipient that is
   * affected by the selections.
   * @param newsletterId the id of the newsletter to send.
   * @return the newsletter that has been sent.
   */
  @RequestMapping( value = "newsletter/{newsletterId}/send", method = RequestMethod.POST )
  public SendTaskResponse sendNewsletter(@PathVariable long newsletterId,
                                         @RequestBody SendRequest sendRequest) {
    securityService.requireShopAdmin();
    Newsletter newsletter = this.newsletterRepository.findOne(newsletterId);
    SendTask sendTask = this.sendService.createSendTask(newsletter);
    sendTask.setSendDate(sendRequest.getSendDate());
    Stream<String> recipientAddresses = this.selectionService.getSelectedRecipients(
      this.selectionService.getSelections(newsletter)).map(r -> r.getEmail());

    this.sendService.send(sendTask, recipientAddresses);

    return new SendTaskResponse(sendTask.getId(), sendTask.getCreated(), sendTask.getFinished(),
      sendTask.getIsFinished(),
      new SelectionStats(0L, sendTask.getDone(), sendTask.getSkipped(),
                         sendTask.getError()));
  }

  /**
   * Get status of a send-task.
   * @param sendTaskId the id of the send task.
   * @return task status.
   */
  @RequestMapping( value = "sendtask/{sendTaskId}", method = RequestMethod.GET )
  public SendTaskResponse getSendTaskStatus(@PathVariable long sendTaskId) {
    securityService.requireShopAdmin();
    SendTask sendTask = this.sendTaskRepository.findOne(sendTaskId);
    return new SendTaskResponse(sendTask.getId(), sendTask.getCreated(), sendTask.getFinished(),
      sendTask.getIsFinished(),
      new SelectionStats(0L, sendTask.getDone(), sendTask.getSkipped(),
        sendTask.getError()));
  }
}
