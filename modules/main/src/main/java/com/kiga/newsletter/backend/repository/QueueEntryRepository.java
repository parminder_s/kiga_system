package com.kiga.newsletter.backend.repository;

import com.kiga.newsletter.backend.domain.QueueStatus;
import com.kiga.newsletter.backend.domain.zend.QueueEntry;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by mfit on 03.10.16.
 */
public interface QueueEntryRepository extends CrudRepository<QueueEntry, Long> {

  @Query("select new com.kiga.newsletter.backend.domain.QueueStatus(qe.status,"
    + "qe.newsletter, count(qe))"
    + " from QueueEntry qe"
    + " where newsletter = :newsletterName"
    + " group by status")
  List<QueueStatus> getStatusForNewsletterName(@Param("newsletterName") String newsletterName);

  List<QueueEntry> findByEmailAndNewsletter(String email, String newsletter);

}

