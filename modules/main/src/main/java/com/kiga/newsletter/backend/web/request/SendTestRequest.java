package com.kiga.newsletter.backend.web.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mfit on 29.03.17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendTestRequest {
  String email;
}
