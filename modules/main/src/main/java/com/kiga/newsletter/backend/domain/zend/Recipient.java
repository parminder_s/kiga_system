package com.kiga.newsletter.backend.domain.zend;

import com.kiga.db.KigaLegacyDbEntityModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/** An address along with meta data such as language and country. Created by mfit on 03.10.16. */
@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Table(name = "nl_recepients")
public class Recipient extends KigaLegacyDbEntityModel implements com.kiga.db.fixture.Entity {

  @Id
  @Column(name = "rcp_id")
  @GeneratedValue
  private Long id;

  @Column(name = "rcp_email", unique = true)
  private String email;

  @Column(name = "rcp_language")
  private String locale;

  private String countryCode;
  private String parentCountryCode;
  private String subscriptionType;
  private String subscriptionStatus;
  private Integer subscriptionDuration;
  private Boolean isShop;

  @Column(name = "isSubscription")
  private Boolean isActiveSubscription;

  public Recipient(String email, String locale) {
    this.email = email;
    this.locale = locale;
  }
}
