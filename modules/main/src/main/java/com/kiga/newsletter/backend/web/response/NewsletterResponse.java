package com.kiga.newsletter.backend.web.response;

import com.kiga.newsletter.backend.domain.SelectionStats;
import com.kiga.newsletter.backend.domain.zend.Newsletter;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * A single newsletter as a response object for transfer over the web.
 * Created by mfit on 03.10.16.
 */
@Data
public class NewsletterResponse {
  private Long id;
  private String name;
  private String url;
  private String locale;
  private String subject;
  private Date publishingDate;
  private String html;
  private String text;

  private SelectionStats stats;

  private List<SelectionResponse> selections;

  public NewsletterResponse() {}

  /**
   * From a newsletter domain model, create an object for json transfer.
   * @param newsletter the newsletter source.
   */
  public NewsletterResponse(Newsletter newsletter) {
    this.id = newsletter.getId();
    this.name = newsletter.getName();
    this.url = newsletter.getUrl();
    this.locale = newsletter.getLocale();
    this.subject = newsletter.getSubject();
    this.publishingDate = newsletter.getPublishingDate();
    this.html = "";
    this.text = "";
  }

}
