package com.kiga.newsletter.backend.domain.zend;

import com.kiga.db.KigaLegacyDbEntityModel;
import com.kiga.newsletter.backend.domain.SubscriptionTypeGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * A filter settings. set of meta-data fields that represents an ad-hoc list
 * (segmentation) of recipients.
 * Created by mfit on 07.12.16.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "nl_recipient_selection")
public class RecipientSelection extends KigaLegacyDbEntityModel {

  @Id
  @Column
  @GeneratedValue
  private Long id;

  // A 2-letter value
  String locale;
  String country;
  String parentCountry;
  String countryGroup;

  @Enumerated(EnumType.STRING)
  SubscriptionTypeGroup subscriptionType;

  Boolean shopFlag;
  Boolean subscriptionFlag;
  String subscriptionStatus;
  Integer subscriptionDuration;

  @ManyToOne
  @JoinColumn(name = "newsletterId", referencedColumnName = "id")
  Newsletter newsletter;

  /**
   * Get a mapping between a product type group settings, that are stored in
   * the RecipientSelection, and a list of detailed product types, that are kept with
   * the recipients.
   * @return a map from the product group to the list of single product-type codes.
   */
  private Map<SubscriptionTypeGroup, List<String>> getSubscriptionSettingMapping() {
    Map<SubscriptionTypeGroup, List<String>> mapping = new HashMap<>();
    mapping.put(SubscriptionTypeGroup.parent, Collections.singletonList("PAR"));
    mapping.put(SubscriptionTypeGroup.paed, Arrays.asList("NA", "NN", "SA", "SN", "OG"));
    mapping.put(SubscriptionTypeGroup.gift, Collections.singletonList("GIFT"));
    mapping.put(SubscriptionTypeGroup.test, Collections.singletonList("TS"));
    return mapping;
  }

  /**
   * Test whether a pqrent country setting is in place.
   * @return has a parent country setting or not.
     */
  public Boolean hasParentCountryCode() {
    return (this.parentCountry != null);
  }

  /**
   * Test whether a country setting is in place.
   * @return has a country setting or not.
   */
  public Boolean hasCountryCode() {
    return (this.country != null) && (!"".equals(this.country));
  }


  /**
   * Test whether a locale setting is in place.
   * @return has a locale setting or not.
   */
  public Boolean hasLocale() {
    return (this.locale != null) && (!"".equals(this.locale));
  }

  /**
   * Test whether a shop setting is in place.
   * @return has a shop setting or not.
   */
  public Boolean hasShopFlag() {
    return this.shopFlag != null;
  }

  /**
   * Test whether setting for an active subscription is in place.
   * @return has a setting for active subscription or not.
   */
  public Boolean hasSubscriptionFlag() {
    return this.subscriptionFlag != null;
  }

  /**
   * Test whether a subscription type setting is in place.
   * @return has a subscription type setting or not.
   */
  public Boolean hasSubscriptionType() {
    return this.subscriptionType != null;
  }

  /**
   * Test whether a subscription duration setting is in place.
   * @return has a subscription duration setting or not.
   */
  public Boolean hasSubscriptionDuration() {
    return this.subscriptionDuration != null;
  }

  /**
   * Test whether a subscription status setting is in place.
   * @return has a subscription status setting or not.
   */
  public Boolean hasSubscriptionStatus() {
    return (this.subscriptionStatus != null) && (!"".equals(this.subscriptionStatus));
  }

  /**
   * Get a list of product types that should be selected by this setting.
   * @return list of product type codes.
     */
  public List<String> getExpandedSubscriptionTypes() {
    List<String> types = getSubscriptionSettingMapping().getOrDefault(
      this.subscriptionType,
      new ArrayList<>());
    return types;
  }
}
