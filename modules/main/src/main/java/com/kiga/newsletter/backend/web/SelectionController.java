package com.kiga.newsletter.backend.web;

import com.kiga.newsletter.backend.domain.zend.Newsletter;
import com.kiga.newsletter.backend.domain.zend.RecipientSelection;
import com.kiga.newsletter.backend.repository.NewsletterRepository;
import com.kiga.newsletter.backend.repository.RecipientSelectionRepository;
import com.kiga.newsletter.backend.service.SelectionService;
import com.kiga.newsletter.backend.web.response.SelectionResponse;
import com.kiga.security.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mfit on 08.03.17.
 */
@RestController
@RequestMapping("backend/newsletter")
public class SelectionController {
  SecurityService securityService;
  NewsletterRepository newsletterRepository;
  RecipientSelectionRepository recipientSelectionRepository;
  SelectionService selectionService;

  /**
   * SelectionController constructor.
   * @param securityService to check permission of the user / session
   * @param newsletterRepository access to newsletters available for sending
   * @param recipientSelectionRepository access to selections per newsletter
   */
  @Autowired
  public SelectionController(SecurityService securityService,
                              NewsletterRepository newsletterRepository,
                              RecipientSelectionRepository recipientSelectionRepository,
                              SelectionService selectionService) {
    this.securityService = securityService;
    this.newsletterRepository = newsletterRepository;
    this.recipientSelectionRepository = recipientSelectionRepository;
    this.selectionService = selectionService;
  }

  /**
   * Add a new selection for a newsletter.
   * @param nlid the newsletter id.
   * @return the list of selections.
   */
  @RequestMapping( value = "newsletter/{nlid}/selections", method = RequestMethod.POST )
  public SelectionResponse addSelection(@PathVariable long nlid) {
    securityService.requireShopAdmin();
    Newsletter newsletter = this.newsletterRepository.findOne(nlid);
    RecipientSelection selection = this.selectionService.buildDefaultSelection(newsletter);
    this.recipientSelectionRepository.save(selection);
    return new SelectionResponse(selection);
  }

  /**
   * Update a selection.
   * @param selid the id of the selection to update.
   * @param selectionUpdate contents of the update.
   * @return the selection that was removed.
   */
  @RequestMapping( value = "selection/{selid}/update", method = RequestMethod.POST )
  public SelectionResponse updateSelection(
    @PathVariable long selid, @RequestBody SelectionResponse selectionUpdate) throws Exception {
    securityService.requireShopAdmin();
    RecipientSelection selection = this.recipientSelectionRepository.findOne(selid);
    if (selection == null) {
      throw new Exception("Selection setting not found");
    }
    selection.setCountry(selectionUpdate.getCountry());
    selection.setParentCountry(selectionUpdate.getParentCountry());
    selection.setLocale(selectionUpdate.getLocale());
    selection.setShopFlag(selectionUpdate.getShopFlag());
    selection.setSubscriptionFlag(selectionUpdate.getSubscriptionFlag());
    selection.setSubscriptionType(selectionUpdate.getSubscriptionType());
    selection.setSubscriptionStatus(selectionUpdate.getSubscriptionStatus());
    selection.setSubscriptionDuration(selectionUpdate.getSubscriptionDuration());
    selection.setSubscriptionDuration(selectionUpdate.getSubscriptionDuration());
    this.recipientSelectionRepository.save(selection);

    SelectionResponse returner = new  SelectionResponse(selection);
    returner.setStats(this.selectionService.getSendingStats(selection));
    return returner;

  }

  /**
   * Remove a selection.
   * @param selid the id of the selection to remove.
   * @return the selection that was removed.
   */
  @RequestMapping( value = "selection/{selid}/delete", method = RequestMethod.POST )
  public SelectionResponse removeSelection(@PathVariable long selid) throws Exception {
    securityService.requireShopAdmin();
    RecipientSelection selection = this.recipientSelectionRepository.findOne(selid);
    if (selection == null) {
      throw new Exception("Not found");
    }
    this.recipientSelectionRepository.delete(selection);
    return new SelectionResponse(selection);
  }


}
