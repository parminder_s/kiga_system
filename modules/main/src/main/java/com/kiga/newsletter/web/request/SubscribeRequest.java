package com.kiga.newsletter.web.request;

import com.kiga.web.message.EndpointRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by mfit on 01.02.17.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SubscribeRequest extends EndpointRequest {
  private String email;
}
