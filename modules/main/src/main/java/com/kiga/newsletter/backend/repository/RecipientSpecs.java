package com.kiga.newsletter.backend.repository;

import com.kiga.newsletter.backend.domain.zend.Recipient;
import com.kiga.newsletter.backend.domain.zend.RecipientSelection;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.criteria.Predicate;

/**
 * Created by mfit on 17.01.17.
 */
public class RecipientSpecs {

  /**
   * Create (a lambda that produces) the predicate for restricting recipients
   * from a RecipientSelection.
   * @param selection The selection settings that will be translated into a jpa-predicate.
   * @return The predicate (where-clause).
     */
  public static Specification<Recipient> segmentList(RecipientSelection selection) {
    return (root, query, builder) -> {

      List<Predicate> predicateList = new ArrayList<Predicate>();
      if (selection.hasLocale()) {
        predicateList.add(builder.equal(
          root.<String>get("locale"), selection.getLocale()));
      }

      if (selection.hasCountryCode()) {
        predicateList.add(builder.equal(
          root.<String>get("countryCode"), selection.getCountry()));
      }

      if (selection.hasParentCountryCode()) {
        predicateList.add(builder.equal(
          root.<String>get("parentCountryCode"), selection.getParentCountry()));
      }

      if (selection.hasShopFlag()) {
        predicateList.add(builder.equal(
          root.<String>get("isShop"), selection.getShopFlag()));
      }

      if (selection.hasSubscriptionFlag()) {
        predicateList.add(builder.equal(
          root.<String>get("isActiveSubscription"), selection.getSubscriptionFlag()));
      }

      if (selection.hasSubscriptionType()) {
        predicateList.add(
          root.<String>get("subscriptionType").in(selection.getExpandedSubscriptionTypes()));
      }

      if (selection.hasSubscriptionDuration()) {
        predicateList.add(
          root.<String>get("subscriptionDuration").in(selection.getSubscriptionDuration()));
      }

      if (selection.hasSubscriptionStatus()) {
        predicateList.add(builder.equal(
          root.<String>get("subscriptionStatus"), selection.getSubscriptionStatus()));
      }

      Predicate[] predicateArray = new Predicate[predicateList.size()];
      predicateList.toArray(predicateArray);
      Predicate selectionPredicate = builder.and(predicateArray);

      return selectionPredicate;
    };
  }

  /**
   * Create (a lambda that produces) the predicate for restricting recipients
   * from a list of RecipientSelections.
   * @param selectionList The list of selection settings, will combined with an OR,
   *                      that will be translated into a jpa-predicate.
   * @return The predicate (where-clause).
   */
  public static Specification<Recipient> combinedSegments(List<RecipientSelection> selectionList) {
    return (root, query, builder) -> {

      List<Predicate> predicateList = selectionList
        .stream()
        .map((sel) -> RecipientSpecs.segmentList(sel))
        .map((spec) -> spec.toPredicate(root, query, builder))
        .collect(Collectors.toList());

      Predicate[] specificationArray = new Predicate[predicateList.size()];
      predicateList.toArray(specificationArray);
      Predicate selectionPredicate = builder.or(specificationArray);

      return selectionPredicate;
    };
  }

}
