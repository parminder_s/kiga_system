package com.kiga.newsletter.backend.service;

import com.kiga.newsletter.backend.domain.SelectionStats;
import com.kiga.newsletter.backend.domain.zend.Newsletter;
import com.kiga.newsletter.backend.domain.zend.Recipient;
import com.kiga.newsletter.backend.domain.zend.RecipientSelection;
import com.kiga.newsletter.backend.repository.RecipientRepository;
import com.kiga.newsletter.backend.repository.RecipientSelectionRepository;
import com.kiga.newsletter.backend.repository.RecipientSpecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Operate with and create models involved with selecting recipients that will recieve a
 * mailing / newsletter e.g. manage selections of recipients and create default selection of
 * recipients from a newsletter's meta-data.
 * Created by mfit on 07.12.16.
 */
@Service
public class SelectionService {
  RecipientRepository recipientRepository;
  RecipientSelectionRepository recipientSelectionRepository;

  /**
   * Constructor to set up selection service with all dependencies.
   * @param recipientRepository repository for recipients.
     */
  @Autowired
  public SelectionService(RecipientRepository recipientRepository,
                          RecipientSelectionRepository recipientSelectionRepository) {
    this.recipientRepository = recipientRepository;
    this.recipientSelectionRepository = recipientSelectionRepository;
  }

  /**
   * From a newsletter, create the default recipient selection that matches the newsletter.
   *
   * @param newsletter Newsletter for which the default selection is to be created.
   * @return The filter selection that matches the newsletter meta data.
   */
  public RecipientSelection buildDefaultSelection(Newsletter newsletter) {
    RecipientSelection recipientSelection = new RecipientSelection();
    recipientSelection.setNewsletter(newsletter);
    recipientSelection.setLocale(newsletter.getLocale().substring(0, 2));
    return recipientSelection;
  }

  /**
   * For a selection, return the "sending stats", or more precise the
   * number of potentially affected recipients.
   *
   * @param selection The selection to create the stats for.
   * @return Return the status and numbers regarding the specified selection.
   */
  public SelectionStats getSendingStats(RecipientSelection selection) {

    Long count = this.recipientRepository
      .count(RecipientSpecs.segmentList(selection));

    SelectionStats selectionStats = new SelectionStats();
    selectionStats.setDone(0L);
    selectionStats.setPending(count);
    selectionStats.setError(0L);
    return selectionStats;
  }

  /**
   * For a newsletter, return the "sending stats", or more precise the
   * number of potentially affected recipients (of all its selections)
   *
   * @param newsletter The selection to create the stats for.
   * @return Return the status and numbers regarding the specified newsletter's selections.
   */
  public Map<RecipientSelection, SelectionStats> getSendingStats(Newsletter newsletter) {
    return this.recipientSelectionRepository
      .findByNewsletterId(newsletter.getId()).stream()
      .collect(Collectors.toMap(
        nlSel -> nlSel, nlSel -> getSendingStats(nlSel)
      ));
  }

  /**
   * Get receipients for a list of selections as stream.
   * @param selections the selections.
   * @return stream of recipients affected (selected) by the selections.
   */
  public Stream<Recipient> getSelectedRecipients(List<RecipientSelection> selections) {
    return this.recipientRepository
      .findAll(RecipientSpecs.combinedSegments(selections))
      .stream();
  }

  public List<RecipientSelection> getSelections(Newsletter newsletter) {
    // TODO : should be able to get this by traversal from newsletter
    return this.recipientSelectionRepository.findByNewsletterId(newsletter.getId());
  }
}
