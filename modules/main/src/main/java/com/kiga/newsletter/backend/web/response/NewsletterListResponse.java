package com.kiga.newsletter.backend.web.response;

import lombok.Data;

import java.util.List;

/**
 * Response object that contains the list of newsletters.
 * Created by mfit on 23.11.16.
 */
@Data
public class NewsletterListResponse {
  private List<NewsletterResponse> newsletters;
}
