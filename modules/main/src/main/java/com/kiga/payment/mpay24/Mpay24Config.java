package com.kiga.payment.mpay24;

import com.kiga.mail.Mailer;
import com.kiga.payment.PaymentCheckerService;
import com.kiga.payment.mpay24.properties.Mpay24AuthProperties;
import com.kiga.payment.mpay24.properties.Mpay24HttpProperties;
import com.kiga.payment.mpay24.properties.Mpay24Properties;
import com.kiga.payment.repository.PaymentRepository;
import com.mpay24.etp.ETP;
import com.mpay24.etp.ETP_Service;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.codec.Base64;

/** Created by faxxe on 4/5/16. */
@Configuration
public class Mpay24Config {

  @Autowired private Mpay24Properties mpay24Properties;
  @Autowired private PaymentRepository paymentRepository;
  @Autowired private Mailer mailer;

  @Bean
  protected ETP aboEtp() {
    ETP_Service etpService = new ETP_Service();
    ETP etp = etpService.getETP();
    extendEtp(etp, mpay24Properties.getAbo(), mpay24Properties.getHttp());
    return etp;
  }

  @Bean
  protected ETP shopEtp() {
    ETP_Service etpService = new ETP_Service();
    ETP etp = etpService.getETP();
    extendEtp(etp, mpay24Properties.getShop(), mpay24Properties.getHttp());
    return etp;
  }

  private void extendEtp(
      ETP etp,
      Mpay24AuthProperties mpay24AuthProperties,
      Mpay24HttpProperties mpay24HttpProperties) {
    Map<String, Object> reqctx = ((BindingProvider) etp).getRequestContext();
    Map<String, List<String>> headers = new HashMap<String, List<String>>();

    reqctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    reqctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, mpay24HttpProperties.getUrl());

    String userpassword =
        mpay24AuthProperties.getUsername() + ":" + mpay24AuthProperties.getPassword();
    String encodedAuthorization = new String(Base64.encode(userpassword.getBytes()));
    headers.put("Authorization", Collections.singletonList("Basic " + encodedAuthorization));
  }

  @Bean
  public PaymentCheckerService paymentCheckerService() {
    return new PaymentCheckerService(paymentRepository, mpay24Properties, shopEtp(), mailer);
  }
}
