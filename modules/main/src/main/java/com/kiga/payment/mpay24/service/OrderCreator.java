package com.kiga.payment.mpay24.service;

import static com.kiga.payment.mpay24.properties.Mpay24Brand.HOBEX_AT;
import static com.kiga.payment.mpay24.properties.Mpay24Brand.HOBEX_DE;
import static com.kiga.payment.mpay24.properties.Mpay24Brand.MASTERCARD;
import static com.kiga.payment.mpay24.properties.Mpay24Brand.VISA;
import static com.kiga.payment.mpay24.properties.Mpay24Type.CC;
import static com.kiga.payment.mpay24.properties.Mpay24Type.ELV;

import com.kiga.payment.PaymentInitiatorConfig;
import com.kiga.payment.mpay24.model.Order;
import com.kiga.payment.mpay24.model.sub.Customer;
import com.kiga.payment.mpay24.model.sub.Payment;
import com.kiga.payment.mpay24.model.sub.PaymentTypes;
import com.kiga.payment.mpay24.model.sub.ShoppingCart;
import com.kiga.payment.mpay24.model.sub.TemplateSet;
import com.kiga.payment.mpay24.model.sub.Url;
import com.kiga.payment.mpay24.properties.Mpay24Brand;
import org.springframework.stereotype.Service;

@Service
public class OrderCreator {
  /**
   * create an Order object which is required to initiate a payment in MPay24.
   */
  public Order create(PaymentInitiatorConfig config, String tid) {
    return Order.builder()
      .tid(tid)
      .templateSet(TemplateSet.builder()
        .cssName("WEB")
        .language(config.getLocale().getValue().toUpperCase())
        .build())
      .paymentTypes(PaymentTypes.builder()
        .enable(1)
        .payment(getPayment(config))
        .build())
      .shoppingCart(ShoppingCart.builder()
        .description(config.getDescription())
        .build())
      .price(config.getPrice().getAmount().toString())
      .currency(config.getPrice().getCurrency())
      .customer(Customer.builder().id(tid).useProfile(true).build())
      .url(Url.builder()
        .success(config.getSuccessUrl())
        .cancel(config.getFailureUrl())
        .error(config.getFailureUrl())
        .build())
      .build();
  }

  private Payment getPayment(PaymentInitiatorConfig config) {
    Payment.PaymentBuilder builder = Payment.builder();
    switch (config.getPaymentType()) {
      case MP_VISA:
        builder.type(CC).brand(VISA);
        break;
      case MP_MC:
        builder.type(CC).brand(MASTERCARD);
        break;
      case MP_ELV:
        builder.type(ELV).brand(getHobexType(config.getCountryCode()));
        break;
      default:
        throw new RuntimeException("invalid payment type for MPay24");
    }

    return builder.build();
  }

  private Mpay24Brand getHobexType(String countryCode) {
    if ("at".equalsIgnoreCase(countryCode)) {
      return HOBEX_AT;
    } else if ("de".equalsIgnoreCase(countryCode)) {
      return HOBEX_DE;
    } else {
      throw new RuntimeException("No Hobex payment for country " + countryCode);
    }
  }
}
