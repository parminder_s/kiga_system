package com.kiga.payment.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.db.LocaleToShortNotationConverter;
import com.kiga.main.locale.Locale;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * Created by rainerh on 26.03.16.
 * TODO: As soon as mPay24 Payment is added following properties need to be added:
 * commitUrl, initRequestData, initResponseData
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Payment extends KigaEntityModel {
  private String currency;
  private String cid;
  private String transactionId;
  private String remoteTransactionId;
  private String crmTransactionId;
  private String referenceCode;
  private String referenceId;

  private BigDecimal price;
  private String paymentMethod;

  @Convert(converter = LocaleToShortNotationConverter.class)
  private Locale locale;

  private Boolean isPostCommitted;

  @Column(name = "initData") private String initRequestData;
  @Column(name = "responseData") private String initResponseData;
  private String pspReturnData;
  private String sessionId;

  @NotNull
  private Boolean isAutomatic;
  @NotNull private Boolean isCrmCommitted;
  private String status;

  private String acquirer;
  private String psp;

  private String successUrl;
  private String failureUrl;

  public Payment() {
    this.setClassName("Payment");
  }
}
