package com.kiga.payment.mpay24.model.sub;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Url {
  @JacksonXmlProperty(localName = "Success")
  private String success;

  @JacksonXmlProperty(localName = "Error")
  private String error;

  @JacksonXmlProperty(localName = "Cancel")
  private String cancel;
}
