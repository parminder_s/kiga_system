package com.kiga.payment.service;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneak;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class MdxiConverter {
  private XmlMapper xmlMapper;

  /**
   * inject and configure dependencies.
   */
  public MdxiConverter(XmlMapper xmlMapper) {
    this.xmlMapper = xmlMapper;
    xmlMapper.getFactory().getXMLOutputFactory()
      .setProperty("javax.xml.stream.isRepairingNamespaces", false);
  }

  public String convertToMdxi(Object object) {
    return sneak(() -> this.xmlMapper.writeValueAsString(object))
      .replace(" xmlns=\"\"", "");
  }
}
