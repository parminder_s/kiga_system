package com.kiga.payment.mpay24.model.sub;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ShoppingCart {
  @JacksonXmlProperty(localName = "Description")
  private String description;
}
