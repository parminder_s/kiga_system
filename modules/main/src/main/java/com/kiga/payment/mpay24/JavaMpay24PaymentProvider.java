package com.kiga.payment.mpay24;


import com.kiga.payment.PaymentProvider;

/**
 * Created by faxxe on 4/5/16.
 */
public class JavaMpay24PaymentProvider extends PaymentProvider {

  @Override
  public String name() {
    return "MPAY24";
  }

}
