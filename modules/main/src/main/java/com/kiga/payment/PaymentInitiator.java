package com.kiga.payment;

import com.kiga.payment.mpay24.model.Order;
import com.kiga.payment.mpay24.properties.Mpay24AuthProperties;
import com.kiga.payment.mpay24.properties.Mpay24Properties;
import com.kiga.payment.mpay24.service.OrderCreator;
import com.kiga.payment.service.MdxiConverter;
import com.mpay24.etp.ETP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentInitiator {
  private ETP shopEtp;
  private Mpay24Properties mpay24Properties;
  private OrderCreator orderCreator;
  private MdxiConverter mdxiConverter;

  /**
   * inject dependencies.
   */
  @Autowired
  public PaymentInitiator(ETP shopEtp, Mpay24Properties mpay24Properties,
                          OrderCreator orderCreator, MdxiConverter mdxiConverter) {
    this.shopEtp = shopEtp;
    this.mpay24Properties = mpay24Properties;
    this.orderCreator = orderCreator;

    this.mdxiConverter = mdxiConverter;
  }

  /**
   * initiates a new payment and returns a link where the consumer has
   * to redirect the user.
   */
  public String initiate(PaymentInitiatorConfig paymentInitiatorConfig) {

    Order order = orderCreator.create(paymentInitiatorConfig, "");
    String mdxi = mdxiConverter.convertToMdxi(order);

    this.shopEtp.selectPayment(
      mpay24Properties.getAbo().getMerchantId(),
      mdxi, null, null, null, null, null);

    return paymentInitiatorConfig.getSuccessUrl();
  }
}
