package com.kiga.payment.mpay24.properties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by faxxe on 4/6/16.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Mpay24AuthProperties {

  private Long merchantId;
  private String password;
  private String username;
}
