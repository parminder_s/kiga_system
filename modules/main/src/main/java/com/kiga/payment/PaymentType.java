package com.kiga.payment;

public enum PaymentType {
  GARANTI,
  MP_AMEX,
  MP_CC,
  MP_ELV,
  MP_MC,
  MP_PAYPAL,
  MP_VISA,
  PAYPAL,
  RE,
  REZS,
  SOFORT
}
