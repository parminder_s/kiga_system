package com.kiga.payment.repository;

import com.kiga.payment.domain.Payment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by rainerh on 16.06.16.
 */
public interface PaymentRepository extends CrudRepository<Payment, Long> {
  List<Payment> findByTransactionId(String transactionId);

  List<Payment> findByTransactionIdOrderByCreatedDesc(String transactionId);

  @Query(value = "SELECT Purchase.ordernr, "
    + "PurchaseWorkflow.created as OrderDate, "
    + "Payment.remotetransactionid as MpayId, "
    + "Country.code, "
    + "P+ urchase.pricegrosstotal, "
    + "Purchase.pricenettotal, "
    + "Purchase.InvoiceNr, "
    + "Purchase.pricegrosstotal - Purchase.pricenettotal as Vat, "
    + "Payment.paymentmethod, "
    + "Purchase.Currency "
    + "FROM   Payment "
    + "INNER JOIN Purchase "
    + "ON Purchase.id = Payment.referenceid "
    + "INNER JOIN Country "
    + "ON Purchase.countryid = Country.id "
    + "INNER JOIN PurchaseWorkflow "
    + "ON PurchaseWorkflow.purchaseid = Purchase.id "
    + "WHERE  Payment.referencecode = 'shop' "
    + "AND Payment.status = 'success' "
    + "AND PurchaseWorkflow.status = 'ordered' "
    + "AND PurchaseWorkflow.created between :start and :end "
    + "ORDER  BY ordernr", nativeQuery = true)
  List<Object> findPayedBeetweenStartDateAndEndDate(
    @Param("start") String start, @Param("end") String end);

  @Query(value = "SELECT Purchase.ordernr, "
    + "PurchaseWorkflow.created as OrderDate, "
    + "Payment.remotetransactionid as MpayId, "
    + "Country.code, "
    + "Purchase.pricegrosstotal, "
    + "Purchase.pricenettotal, "
    + "Purchase.InvoiceNr, "
    + "Purchase.pricegrosstotal - Purchase.pricenettotal as Vat, "
    + "Payment.paymentmethod, "
    + "Purchase.Currency "
    + "FROM   Payment "
    + "INNER JOIN Purchase "
    + "ON Purchase.id = Payment.referenceid "
    + "INNER JOIN Country "
    + "ON Purchase.countryid = Country.id "
    + "INNER JOIN PurchaseWorkflow "
    + "ON PurchaseWorkflow.purchaseid = Purchase.id "
    + "WHERE  Payment.referencecode = 'shop' "
    + "AND Payment.status = 'success' "
    + "AND PurchaseWorkflow.status = 'ordered' "
    + "AND Purchase.OrderNr = :orderNr "
    + "ORDER  BY ordernr", nativeQuery = true)
  List<Object> findByOrderNr(@Param("orderNr") String orderNr);

  List<Payment> findByReferenceCodeAndReferenceId(String referenceCode, String referenceId);

  @Query(value = "SELECT Purchase.ordernr, "
    + "PurchaseWorkflow.created as OrderDate, "
    + "Payment.remotetransactionid as MpayId, "
    + "Country.code, "
    + "Purchase.pricegrosstotal, "
    + "Purchase.pricenettotal, "
    + "Purchase.InvoiceNr, "
    + "Purchase.pricegrosstotal - Purchase.pricenettotal as Vat, "
    + "Payment.paymentmethod, "
    + "Purchase.Currency, "
    + "AcquirerTransaction.AcquirerTransactionId "
    + "FROM   Payment "
    + "INNER JOIN Purchase "
    + "ON Purchase.id = Payment.referenceid "
    + "INNER JOIN Country "
    + "ON Purchase.countryid = Country.id "
    + "INNER JOIN PurchaseWorkflow "
    + "ON PurchaseWorkflow.purchaseid = Purchase.id "
    + "LEFT OUTER JOIN AcquirerTransaction "
    + "ON AcquirerTransaction.PspTransactionId = Payment.remoteTransactionId "
    + "WHERE  Payment.referencecode = 'shop' "
    + "AND Payment.status = 'success' "
    + "AND PurchaseWorkflow.status = 'ordered' "
    + "AND PurchaseWorkflow.created between :start and :end "
    + "ORDER  BY ordernr", nativeQuery = true)
  List<Object> findPayedBeetweenStartDateAndEndDateWithAcquirerTransaction(
    @Param("start") String start, @Param("end") String end);
}
