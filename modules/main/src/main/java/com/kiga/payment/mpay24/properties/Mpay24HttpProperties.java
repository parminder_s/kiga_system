package com.kiga.payment.mpay24.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by faxxe on 4/5/16.
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("mpay24.http")
public class Mpay24HttpProperties {

  String url;
  String protocol;
  String host;
  String port;
  String basePath;

  public String getUrl() {
    return url;
  }

  public String getProtocol() {
    return protocol;
  }

  public String getHost() {
    return host;
  }

  public String getPort() {
    return port;
  }

  public String getBasePath() {

    return basePath;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public void setProtocol(String protocol) {
    this.protocol = protocol;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public void setPort(String port) {
    this.port = port;
  }

  public void setBasePath(String basePath) {
    this.basePath = basePath;
  }
}
