package com.kiga.payment.mpay24;

/**
 * Created by faxxe on 5/16/16.
 */
public enum PaymentStatusEnum {
  OK,
  FAILED,
  SUSPENDED,
  ERROR,
  NOT_FOUND,
}

