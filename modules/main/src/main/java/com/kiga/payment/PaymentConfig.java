package com.kiga.payment;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.kiga.db.RepositoryMapper;
import com.kiga.payment.domain.Payment;
import com.kiga.payment.repository.PaymentRepository;
import com.kiga.payment.service.MdxiConverter;
import com.kiga.spec.Fetcher;
import com.kiga.spec.Persister;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PaymentConfig {

  @Bean(name = "PaymentPersister")
  public Persister<Payment> getPersister(PaymentRepository paymentRepository) {
    return RepositoryMapper.toPersister(paymentRepository);
  }

  @Bean(name = "PaymentFetcher")
  public Fetcher<Payment> getFetcher(PaymentRepository paymentRepository) {
    return RepositoryMapper.toFetcher(paymentRepository);
  }

  @Bean
  public MdxiConverter getMdxiConverter() {
    return new MdxiConverter(new XmlMapper());
  }
}
