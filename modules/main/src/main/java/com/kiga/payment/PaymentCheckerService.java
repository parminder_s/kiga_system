package com.kiga.payment;

import com.kiga.mail.Mailer;
import com.kiga.payment.domain.Payment;
import com.kiga.payment.mpay24.Mpay24Response;
import com.kiga.payment.mpay24.PaymentCheckerResponse;
import com.kiga.payment.mpay24.PaymentStatusEnum;
import com.kiga.payment.mpay24.properties.MPay24ParameterEnum;
import com.kiga.payment.mpay24.properties.Mpay24Properties;
import com.kiga.payment.repository.PaymentRepository;
import com.mpay24.etp.ETP;
import com.mpay24.etp.Parameter;
import com.mpay24.etp.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.ws.Holder;

/** Created by faxxe on 3/30/16. */
public class PaymentCheckerService {

  private Logger logger = LoggerFactory.getLogger(getClass());

  private PaymentRepository paymentRepository;
  private Mpay24Properties mpay24Properties;
  private ETP shopEtp;
  private Mailer mailer;

  /** Constructor... */
  public PaymentCheckerService(
      PaymentRepository paymentRepository,
      Mpay24Properties mpay24Properties,
      ETP shopEtp,
      Mailer mailer) {
    this.paymentRepository = paymentRepository;
    this.mpay24Properties = mpay24Properties;
    this.shopEtp = shopEtp;
    this.mailer = mailer;
  }

  /** make checks on payments done for the shop. */
  public PaymentCheckerResponse checkShopPayments(
      Long transactionId, boolean persistPayments, Date purchaseLastEdited) {

    PaymentCheckerResponse returner = new PaymentCheckerResponse("", PaymentStatusEnum.ERROR);

    List<Payment> paymentsToCheck =
        paymentRepository.findByReferenceCodeAndReferenceId("shop", transactionId.toString());
    for (Payment tmp : paymentsToCheck) {
      PaymentCheckerResponse pcr = checkShopPayment(tmp, persistPayments);
      if (pcr.getPaymentStatus() == PaymentStatusEnum.OK
          && returner.getPaymentStatus() == PaymentStatusEnum.OK) {
        notifyDoublePayment(tmp.getId(), transactionId);
        returner = pcr;
      }
      if (pcr.getPaymentStatus() == PaymentStatusEnum.OK
          && returner.getPaymentStatus() != PaymentStatusEnum.OK) {
        returner = pcr;
      }
      if (pcr.getPaymentStatus() == PaymentStatusEnum.SUSPENDED
          && returner.getPaymentStatus() != PaymentStatusEnum.OK) {
        returner = pcr;
      }
      if (pcr.getPaymentStatus() == PaymentStatusEnum.NOT_FOUND
          && returner.getPaymentStatus() != PaymentStatusEnum.SUSPENDED
          && returner.getPaymentStatus() != PaymentStatusEnum.OK) {
        returner = pcr;
      }
      if (pcr.getPaymentStatus() == PaymentStatusEnum.FAILED
          && returner.getPaymentStatus() != PaymentStatusEnum.NOT_FOUND
          && returner.getPaymentStatus() != PaymentStatusEnum.SUSPENDED
          && returner.getPaymentStatus() != PaymentStatusEnum.OK) {
        returner = pcr;
      }
      if (pcr.getPaymentStatus() == PaymentStatusEnum.ERROR
          && returner.getPaymentStatus() != PaymentStatusEnum.FAILED
          && returner.getPaymentStatus() != PaymentStatusEnum.NOT_FOUND
          && returner.getPaymentStatus() != PaymentStatusEnum.SUSPENDED
          && returner.getPaymentStatus() != PaymentStatusEnum.OK) {
        returner = pcr;
      }
    }

    return returner;
  }

  /** makes the actual request to mpay24. */
  public Mpay24Response getMpay24TransactionStatus(Long merchantId, String paymentId) {

    Holder<Status> haStatus = new Holder<>();
    Holder<String> returnCode = new Holder<>();
    Holder<List<Parameter>> hlParameter = new Holder<>();

    shopEtp.transactionStatus(merchantId, null, paymentId, haStatus, returnCode, hlParameter);

    return new Mpay24Response(haStatus.value, returnCode.value, hlParameter.value);
  }

  /** checkShopPayments. */
  public PaymentCheckerResponse checkShopPayment(Payment payment, boolean persistPayment) {

    Long merchantId = mpay24Properties.getShop().getMerchantId();
    Mpay24Response mpay24Response =
        getMpay24TransactionStatus(merchantId, payment.getId().toString());
    Map<MPay24ParameterEnum, String> parameters = parameterToMap(mpay24Response.getParameters());
    PaymentCheckerResponse returner =
        checkMpayResponse(mpay24Response.getStatus(), mpay24Response.getReturnCode(), parameters);

    Calendar cal = Calendar.getInstance();
    cal.setTime(payment.getCreated());
    long difference = Calendar.getInstance().getTimeInMillis() - cal.getTimeInMillis();

    if ((returner.getPaymentStatus() != PaymentStatusEnum.OK
            && returner.getPaymentStatus() != PaymentStatusEnum.FAILED)
        && difference < 30 * 60 * 1000) {
      returner.setPaymentStatus(PaymentStatusEnum.SUSPENDED);
      return returner;
    }

    if (returner.getPaymentStatus() == PaymentStatusEnum.SUSPENDED
        && difference > 25 * 1000 * 60 * 60) {
      returner.setPaymentStatus(PaymentStatusEnum.FAILED);
    }

    if (persistPayment) {
      updatePaymentStatus(payment, returner, parameters);
    }

    return returner;
  }

  private void updatePaymentStatus(
      Payment payment,
      PaymentCheckerResponse paymentCheckerResponse,
      Map<MPay24ParameterEnum, String> parameters) {

    switch (paymentCheckerResponse.getPaymentStatus()) {
      case OK:
        payment.setStatus("success");
        payment.setIsPostCommitted(true);
        payment.setRemoteTransactionId(parameters.getOrDefault(MPay24ParameterEnum.MPAYTID, ""));
        paymentRepository.save(payment);
        break;
      case NOT_FOUND:
        payment.setStatus("initFailed");
        payment.setIsPostCommitted(true);
        paymentRepository.save(payment);
        break;
      case FAILED:
        payment.setStatus("failed");
        payment.setIsPostCommitted(true);
        payment.setRemoteTransactionId(parameters.getOrDefault(MPay24ParameterEnum.MPAYTID, ""));
        paymentRepository.save(payment);
        break;
      default:
      case ERROR:
    }
  }

  private Map<MPay24ParameterEnum, String> parameterToMap(List<Parameter> parameters) {
    Map<MPay24ParameterEnum, String> map = new HashMap<>();
    for (Parameter parameter : parameters) {
      map.put(MPay24ParameterEnum.valueOf(parameter.getName()), parameter.getValue());
    }
    return map;
  }

  /** nit so wunderbar. */
  public PaymentCheckerResponse checkMpayResponse(
      Status status, String returnCode, Map<MPay24ParameterEnum, String> parameters) {

    PaymentStatusEnum reStatus = checkStatus(status);
    PaymentStatusEnum reReturnCode = checkReturnCode(returnCode);
    PaymentStatusEnum reParameters = checkParameterMap(parameters);

    if (reStatus == PaymentStatusEnum.OK) {
      if (reReturnCode == PaymentStatusEnum.OK) {
        return new PaymentCheckerResponse(
            parameters.getOrDefault(MPay24ParameterEnum.MPAYTID, ""), reParameters);
      } else {
        return new PaymentCheckerResponse(
            parameters.getOrDefault(MPay24ParameterEnum.MPAYTID, ""), PaymentStatusEnum.ERROR);
      }
    } else {
      if (reReturnCode == PaymentStatusEnum.NOT_FOUND) {
        return new PaymentCheckerResponse(
            parameters.getOrDefault(MPay24ParameterEnum.MPAYTID, ""), PaymentStatusEnum.NOT_FOUND);
      } else {
        return new PaymentCheckerResponse(
            parameters.getOrDefault(MPay24ParameterEnum.MPAYTID, ""), PaymentStatusEnum.ERROR);
      }
    }
  }

  private PaymentStatusEnum checkStatus(Status status) {
    switch (status) {
      case OK:
        return PaymentStatusEnum.OK;
      case ERROR:
      default:
        return PaymentStatusEnum.ERROR;
    }
  }

  private PaymentStatusEnum checkReturnCode(String returnCode) {
    switch (returnCode) {
      case "OK":
        return PaymentStatusEnum.OK;
      case "NOT_FOUND": // The transaction was not found.
        return PaymentStatusEnum.NOT_FOUND;
      case "DECLINED": // The transaction was declined by the external payment
        // interface
      case "BLOCKED": // The transaction was blocked.
      case "ACCESS_DENIED": // The merchant’s IP address is not white listed.
      case "MERCHANT_LOCKED": // The merchant id is locked.
      case "PAYMENT_METHOD_NOT_ACTIVE": // The desired payment system is not active.
      case "<parameter>_NOT_ENTERED": // A mandatory parameter is missing.
      case "<parameter>_NOT_CORRECT": // The parameter supplied is not valid.
      case "<parameter>_NOT_SUPPORTED": // The parameter is not supported.
      case "PTYPE_MISMATCH": // The payment method mismatches.
      case "ALREADY_PROCESSED": // 12 CACHE_DATA_EXPIRED The transaction has been
        // processed and can not be processed again. The temporary cache data are
        // invalid due to expiration.
      case "INVALID_MDXI": // The MDXI XML stream could not be validated.
      case "INVALID_PRICE":
      case "INVALID_AMOUNT":
      case "INVALID_CREDITCARD_NUMBER":
      case "INVALID_MAESTRO_NUMBER": // The Maestro card number is not plausible.
      case "INVALID_IBAN": // The provided IBAN is not plausible.
      case "PROFILE_NOT_FOUND": // mPAY24 payment profile could not be found.
      case "PROFILE_NOT_SUPPORTED": // mPAY24 Profile is not activated.
      case "PROFILE_FLEX_NOT_SUPPORTED":
      default:
        return PaymentStatusEnum.ERROR;
    }
  }

  private PaymentStatusEnum checkParameterMap(Map<MPay24ParameterEnum, String> parameters) {

    switch (parameters.getOrDefault(MPay24ParameterEnum.STATUS, "ERROR")) {
      case "BILLED":
        return PaymentStatusEnum.OK;
      case "SUSPENDED":
        return PaymentStatusEnum.SUSPENDED;
      case "FAILED":
      case "ERROR":
      default:
        return PaymentStatusEnum.FAILED;
    }
  }

  private void notifyModifiedPurchase(long paymentId, long transactionId) {
    mailer.send(
        "payment",
        "modifiedPurchase",
        Long.toString(paymentId),
        email ->
            email
                .withTo("technik@kigaportal.com")
                .withFrom("kundenservice@kigaportal.com")
                .withSubject("[shop] ATTENTION: Modified Purchase after Payment!")
                .withBody(
                    "There has been a modified Purchase. See: purchase: "
                        + transactionId
                        + " and "
                        + "payment: "
                        + paymentId)
                .withInline("idkigalogo", new ClassPathResource("/print/kigalogocolor.png")));
  }

  private void notifyDoublePayment(long paymentId, long transactionId) {
    mailer.send(
        "payment",
        "doublePayment",
        Long.toString(paymentId),
        email ->
            email
                .withTo("technik@kigaportal.com")
                .withFrom("kundenservice@kigaportal.com")
                .withSubject("[shop] ATTENTION: Double Payment!")
                .withBody(
                    "There has been a double payment for a "
                        + "purchase! See \r\npaymentid: "
                        + paymentId
                        + " and \r\n"
                        + "purchaseid: "
                        + transactionId)
                .withInline("idkigalogo", new ClassPathResource("/print/kigalogocolor.png")));
  }
}
