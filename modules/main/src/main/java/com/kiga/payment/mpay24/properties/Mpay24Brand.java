package com.kiga.payment.mpay24.properties;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Mpay24Brand {
  VISA("VISA"), MASTERCARD("MASTERCARD"),
  HOBEX_AT("HOBEX-AT"), HOBEX_DE("HOBEX-DE");

  private String name;

  Mpay24Brand(String name) {
    this.name = name;
  }

  @Override
  @JsonValue
  public String toString() {
    return this.name;
  }
}
