package com.kiga.payment.mpay24.model.sub;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TemplateSet {
  @JacksonXmlProperty(isAttribute = true, namespace = "", localName = "Language")
  private String language;

  @JacksonXmlText
  private String cssName;
}
