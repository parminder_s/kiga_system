package com.kiga.payment.mpay24.properties;

public enum Mpay24Type {
  CC("CC"), ELV("ELV");

  private String name;

  Mpay24Type(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return this.name;
  }
}
