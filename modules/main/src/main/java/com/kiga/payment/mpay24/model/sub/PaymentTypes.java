package com.kiga.payment.mpay24.model.sub;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PaymentTypes {
  @JacksonXmlProperty(isAttribute = true, localName = "Enable")
  private int enable;

  @JacksonXmlProperty(localName = "Payment")
  private Payment payment;
}
