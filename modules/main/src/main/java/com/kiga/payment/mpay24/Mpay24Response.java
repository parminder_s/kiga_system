package com.kiga.payment.mpay24;

import com.mpay24.etp.Parameter;
import com.mpay24.etp.Status;
import lombok.Data;

import java.util.List;

/**
 * Created by faxxe on 4/14/16.
 */
@Data
public class Mpay24Response {
  Status status;
  String returnCode;
  List<Parameter> parameters;

  /**
   * Constructor....
   */
  public Mpay24Response(Status status, String returnCode, List<Parameter> parameters) {
    this.status = status;
    this.parameters = parameters;
    this.returnCode = returnCode;
  }

  public Mpay24Response() {
  }
}
