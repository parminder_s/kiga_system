package com.kiga.payment.mpay24.model.sub;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Customer {
  @JacksonXmlProperty(localName = "Id", isAttribute = true)
  private String id;

  @JacksonXmlProperty(localName = "UseProfile", isAttribute = true)
  private Boolean useProfile;
}
