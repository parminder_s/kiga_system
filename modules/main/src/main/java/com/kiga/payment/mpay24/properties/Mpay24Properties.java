package com.kiga.payment.mpay24.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

/**
 * Created by faxxe on 4/5/16.
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("mpay24")
public class Mpay24Properties {

  @NestedConfigurationProperty
  private Mpay24AuthProperties shop;
  @NestedConfigurationProperty
  private Mpay24AuthProperties abo;
  @NestedConfigurationProperty
  private Mpay24HttpProperties http;

  public Mpay24AuthProperties getShop() {
    return shop;
  }

  public void setShop(Mpay24AuthProperties shop) {
    this.shop = shop;
  }

  public Mpay24AuthProperties getAbo() {
    return abo;
  }

  public void setAbo(Mpay24AuthProperties abo) {
    this.abo = abo;
  }

  public Mpay24HttpProperties getHttp() {
    return http;
  }

  public void setHttp(Mpay24HttpProperties http) {
    this.http = http;
  }
}
