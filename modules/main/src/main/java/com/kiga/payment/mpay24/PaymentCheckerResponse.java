package com.kiga.payment.mpay24;

import lombok.Data;

/**
 * Created by faxxe on 4/13/16.
 */
@Data
public class PaymentCheckerResponse {
  private String pspId;
  private PaymentStatusEnum paymentStatus;

  public PaymentCheckerResponse(String pspId, PaymentStatusEnum paymentStatus) {
    this.pspId = pspId;
    this.paymentStatus = paymentStatus;
  }

  public PaymentCheckerResponse() {}
}
