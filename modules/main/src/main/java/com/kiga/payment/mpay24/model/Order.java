package com.kiga.payment.mpay24.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kiga.payment.mpay24.model.sub.Customer;
import com.kiga.payment.mpay24.model.sub.PaymentTypes;
import com.kiga.payment.mpay24.model.sub.ShoppingCart;
import com.kiga.payment.mpay24.model.sub.TemplateSet;
import com.kiga.payment.mpay24.model.sub.Url;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Order {
  @JacksonXmlProperty(localName = "Tid")
  private String tid;

  @JacksonXmlProperty(localName = "TemplateSet")
  private TemplateSet templateSet;

  @JacksonXmlProperty(localName = "PaymentTypes")
  private PaymentTypes paymentTypes;

  @JacksonXmlProperty(localName = "ShoppingCart")
  private ShoppingCart shoppingCart;

  @JacksonXmlProperty(localName = "Price")
  private String price;

  @JacksonXmlProperty(localName = "Currency")
  private String currency;

  @JacksonXmlProperty(localName = "Customer")
  private Customer customer;

  @JacksonXmlProperty(localName = "URL")
  private Url url;
}
