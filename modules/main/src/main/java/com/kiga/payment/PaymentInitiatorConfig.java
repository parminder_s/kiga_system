package com.kiga.payment;

import com.kiga.main.locale.Locale;
import com.kiga.money.Money;
import lombok.Data;

/**
 * This class acts as Pojo containing all relevant information for the
 * Payment module to initiate a new payment.
 *
 * <p>The mandatory parameters are part of the constructor. The optional ones
 * can be set with fluent or setter functions.
 */

@Data
public class PaymentInitiatorConfig {
  private PaymentType paymentType;
  private String successUrl;
  private String failureUrl;
  private Money price;

  private String countryCode;
  private Locale locale;
  private String referenceId;
  private String referenceCode;
  private String description;

  /**
   * constructor containing the minimal parameters.
   */
  public PaymentInitiatorConfig(
    PaymentType paymentType, String successUrl, String failureUrl, Money price,
    String countryCode, Locale locale) {
    this.paymentType = paymentType;
    this.successUrl = successUrl;
    this.failureUrl = failureUrl;
    this.price = price;
    this.countryCode = countryCode;
    this.locale = locale;
  }

  public PaymentInitiatorConfig referenceId(String referenceId) {
    this.referenceId = referenceId;
    return this;
  }

  public PaymentInitiatorConfig referenceCode(String referenceCode) {
    this.referenceCode = referenceCode;
    return this;
  }
}
