package com.kiga.payment.mpay24.properties;

/**
 * Created by faxxe on 4/13/16.
 */
public enum MPay24ParameterEnum {
  APPR_CODE,
  BRAND,
  CURRENCY,
  CUSTOMER,
  CUSTOMER_EMAIL,
  CUSTOMER_ID,
  FILTER_STATUS,
  LANGUAGE,
  MPAYTID,
  OPERATION,
  ORDERDESC,
  PRICE,
  PROFILE_ID,
  PROFILE_STATUS,
  P_TYPE,
  STATUS,
  TID,
  USER_FIELD
}
