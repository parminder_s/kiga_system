package com.kiga.payment.mpay24.model.sub;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kiga.payment.mpay24.properties.Mpay24Brand;
import com.kiga.payment.mpay24.properties.Mpay24Type;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Payment {
  @JacksonXmlProperty(isAttribute = true, localName = "Type")
  private Mpay24Type type;

  @JacksonXmlProperty(isAttribute = true, localName = "Brand")
  private Mpay24Brand brand;
}
