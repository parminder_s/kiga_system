package com.kiga.s3;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by peter on 10.05.16.
 */
@Configuration
@ConfigurationProperties("amazon.s3")
public class S3Properties {
  private String bucketName;
  private String bucketUrl;
  private String accessKey;
  private String secretKey;

  public String getBucketName() {
    return bucketName;
  }

  public void setBucketName(String bucketName) {
    this.bucketName = bucketName;
  }

  public String getBucketUrl() {
    return bucketUrl;
  }

  public void setBucketUrl(String bucketUrl) {
    this.bucketUrl = bucketUrl;
  }

  public String getAccessKey() {
    return accessKey;
  }

  public void setAccessKey(String accessKey) {
    this.accessKey = accessKey;
  }

  public String getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }
}
