package com.kiga.s3.url;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 5/27/16.
 */
public abstract class UrlBuilder {
  private UrlNamingStrategy urlNamingStrategy;
  private String baseUrl;
  private String bucketName;
  private String folder;
  private String fileNamePart;
  private String fileExtensionPart;
  private String fileSuffixPart = "";

  protected UrlBuilder() {}

  /**
   * Append suffix to the filename.
   *
   * @param suffix expected suffix
   * @return UrlBuilder
     */
  public UrlBuilder appendFilenameSuffix(String suffix) {
    fileSuffixPart = suffix;
    return this;
  }

  /**
   * Change bucket name.
   *
   * @param bucketName expected bucket name
   * @return UrlBuilder
   */
  public UrlBuilder useBucket(String bucketName) {
    this.bucketName = StringUtils.strip(bucketName, "/");
    return this;
  }

  /**
   * Change folder of the file.
   *
   * @param folderName expected folder
   * @return UrlBuilder
   */
  public UrlBuilder useFolder(String folderName) {
    this.folder = StringUtils.strip(folderName, "/");
    return this;
  }

  /**
   * Build full url.
   *
   * @return full url
   */
  public String getUrlPath() {
    StringBuilder urlStringBuilder = new StringBuilder();
    if (StringUtils.isNotBlank(folder)) {
      urlStringBuilder.append(folder).append("/");
    }
    urlStringBuilder.append(getFileName());
    return urlStringBuilder.toString();
  }

  public abstract String getUrl();

  public String getBaseUrl() {
    return baseUrl;
  }

  public String getBucketName() {
    return bucketName;
  }

  public String getFolder() {
    return folder;
  }

  public String getFileNamePart() {
    return fileNamePart;
  }

  public String getFileExtensionPart() {
    return fileExtensionPart;
  }

  public String getFileSuffixPart() {
    return fileSuffixPart;
  }

  public UrlNamingStrategy getUrlNamingStrategy() {
    return urlNamingStrategy;
  }

  public void setUrlNamingStrategy(UrlNamingStrategy urlNamingStrategy) {
    this.urlNamingStrategy = urlNamingStrategy;
  }

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  public void setBucketName(String bucketName) {
    this.bucketName = bucketName;
  }

  public void setFolder(String folder) {
    this.folder = folder;
  }

  public void setFileNamePart(String fileNamePart) {
    this.fileNamePart = fileNamePart;
  }

  public void setFileExtensionPart(String fileExtensionPart) {
    this.fileExtensionPart = fileExtensionPart;
  }

  public void setFileSuffixPart(String fileSuffixPart) {
    this.fileSuffixPart = fileSuffixPart;
  }

  /**
   * Build file name only.
   *
   * @return file name.
   */
  public String getFileName() {
    StringBuilder fileNameStringBuilder = new StringBuilder(fileNamePart);

    if (StringUtils.isNotBlank(fileSuffixPart)) {
      fileNameStringBuilder.append("_");
      fileNameStringBuilder.append(fileSuffixPart);
    }

    fileNameStringBuilder.append(".");
    fileNameStringBuilder.append(fileExtensionPart);

    return fileNameStringBuilder.toString();
  }

  /**
   * build file name with folder only.
   * @return file name with folder
   */
  public String getFileNameWithFolder() {
    String folder = "";
    if (StringUtils.isNotBlank(getFolder())) {
      folder = getFolder() + "/";
    }
    return folder + getFileName();
  }

  public String toString() {
    return getUrl();
  }
}
