package com.kiga.s3;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.kiga.db.RepositoryPersister;
import com.kiga.integration.amazon.s3.config.AmazonS3Parameters;
import com.kiga.s3.repository.ss.S3ImageRepository;
import com.kiga.s3.service.KigaAmazonS3Client;
import com.kiga.s3.service.download.DownloadService;
import com.kiga.s3.service.download.DownloadUsingRequestService;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.s3.wrapper.DefaultImageIoWrapper;
import com.kiga.s3.wrapper.ImageIoWrapper;
import com.kiga.upload.ImageProcessor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class S3Config {
  @Bean
  public S3ImageResizeService getS3ImageResizeService(
      AmazonS3Parameters amazonS3Parameters,
      ImageProcessor imageProcessor,
      S3ImageRepository s3ImageRepository,
      @Qualifier("kigAmazonS3ClientIdeas") KigaAmazonS3Client kigaAmazonS3Client,
      DownloadService downloadService,
      ImageIoWrapper imageIoWrapper) {
    return new S3ImageResizeService(
        amazonS3Parameters,
        imageProcessor,
        new RepositoryPersister<>(s3ImageRepository),
        kigaAmazonS3Client,
        downloadService,
        imageIoWrapper);
  }

  /**
   * Create amazon S3 Client bean.
   *
   * @return AmazonS3Client
   */
  @Bean
  public AmazonS3Client getAmazonS3Client(AmazonS3Parameters amazonS3Parameters) {
    BasicAWSCredentials awsCredentials =
        new BasicAWSCredentials(
            amazonS3Parameters.getAccessKey(), amazonS3Parameters.getSecretKey());
    return new AmazonS3Client(awsCredentials);
  }

  @Bean
  public KigaAmazonS3Client getUploaderService(
      AmazonS3Client amazonS3Client, AmazonS3Parameters amazonS3Parameters) {
    return new KigaAmazonS3Client(amazonS3Client, amazonS3Parameters);
  }

  @Bean
  public DownloadService createDownloadService() {
    return new DownloadUsingRequestService();
  }

  @Bean
  public ImageIoWrapper createImageIoWrapper() {
    return new DefaultImageIoWrapper();
  }
}
