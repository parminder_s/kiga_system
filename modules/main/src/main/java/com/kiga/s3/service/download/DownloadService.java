package com.kiga.s3.service.download;

import com.kiga.s3.domain.S3File;
import com.kiga.s3.download.DownloadedS3Image;
import java.io.IOException;

/**
 * @author bbs
 * @since 9/25/16.
 */
public interface DownloadService {
  DownloadedS3Image download(S3File image) throws IOException;
}
