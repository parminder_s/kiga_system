package com.kiga.s3;

import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.Permission;

import java.util.concurrent.Callable;

/**
 * @author bbs
 * @since 5/1/16.
 */
public enum EAmazonS3Acl {
  READ_ALL((Callable<AccessControlList>) () -> {
    AccessControlList accessControlList = new AccessControlList();
    accessControlList.grantPermission(GroupGrantee.AllUsers, Permission.Read);
    return accessControlList;
  });

  private AccessControlList acl;

  EAmazonS3Acl(Callable<AccessControlList> func) {
    try {
      acl = func.call();
    } catch (Exception ex) {
      acl = null;
    }
  }

  public AccessControlList getAcl() {
    return acl;
  }
}
