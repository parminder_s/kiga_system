package com.kiga.s3.web.converter;

import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.web.response.KigaS3FileViewModel;
import com.kiga.web.converter.DefaultViewModelConverter;
import org.springframework.stereotype.Service;

/**
 * @author bbs
 * @since 4/17/17.
 */
@Service
public class KigaS3FileViewModelConverter
  extends DefaultViewModelConverter<KigaS3File, KigaS3FileViewModel> {

  @Override
  public KigaS3FileViewModel convertToResponse(KigaS3File entity) {
    KigaS3FileViewModel returner = new KigaS3FileViewModel();
    returner.setFileName(entity.getName());
    return returner;
  }
}
