package com.kiga.s3.wrapper;

import org.apache.commons.io.output.ByteArrayOutputStream;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

/**
 * @author bbs
 * @since 9/25/16.
 */
public class DefaultImageIoWrapper implements ImageIoWrapper {
  public BufferedImage read(InputStream streamedImage) throws IOException {
    return ImageIO.read(streamedImage);
  }

  public boolean write(BufferedImage downloadedImage, String fileExtensionPart,
    ByteArrayOutputStream os) throws IOException {
    return ImageIO.write(downloadedImage, fileExtensionPart, os);
  }
}
