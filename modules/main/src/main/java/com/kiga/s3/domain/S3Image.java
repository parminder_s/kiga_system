package com.kiga.s3.domain;

import com.kiga.s3.domain.converters.MapToJsonConverter;

import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * @author bbs
 * @since 4/23/16.
 */

@Entity
@PrimaryKeyJoinColumn(name = "ID")
public class S3Image extends KigaS3File {
  @Column(name = "CachedData")
  private String cachedData;

  @Convert(converter = MapToJsonConverter.class)
  @Column(name = "CachedDataJson", columnDefinition = "LONGTEXT")
  private Map<String, String> cachedDataJson;

  @Column(name = "Width")
  private Integer width;

  @Column(name = "Height")
  private Integer height;

  @Column(name = "PDFURL")
  private String pdfUrl;

  public Integer getWidth() {
    return width;
  }

  public void setWidth(Integer width) {
    this.width = width;
  }

  public Integer getHeight() {
    return height;
  }

  public void setHeight(Integer height) {
    this.height = height;
  }

  public String getPdfUrl() {
    return pdfUrl;
  }

  public void setPdfUrl(String pdfUrl) {
    this.pdfUrl = pdfUrl;
  }

  public Map<String, String> getCachedDataJson() {
    return cachedDataJson;
  }

  public void setCachedDataJson(Map<String, String> cachedDataJson) {
    this.cachedDataJson = cachedDataJson;
  }

  public String getCachedData() {
    return cachedData;
  }

  public void setCachedData(String cachedData) {
    this.cachedData = cachedData;
  }
}
