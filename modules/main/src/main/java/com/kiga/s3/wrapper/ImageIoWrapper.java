package com.kiga.s3.wrapper;

import org.apache.commons.io.output.ByteArrayOutputStream;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author bbs
 * @since 9/25/16.
 */
public interface ImageIoWrapper {
  BufferedImage read(InputStream streamedImage) throws IOException;

  boolean write(BufferedImage downloadedImage, String fileExtensionPart,
    ByteArrayOutputStream os) throws IOException;
}
