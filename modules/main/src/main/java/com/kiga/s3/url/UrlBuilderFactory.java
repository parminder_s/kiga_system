package com.kiga.s3.url;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by rainerh on 29.05.16.
 */
public class UrlBuilderFactory {
  /**
   * Creates a UrlBuilder instance depending on its naming strategy which
   * is resolved by the passed s3Url.
   */
  private static SecureRandom random = new SecureRandom();

  /**
   *
   * @param s3Url url of the file on S3.
   * @return a unique url to the new file.
   * @throws MalformedURLException if url not ok.
   */
  public static UrlBuilder create(String s3Url) throws MalformedURLException {
    URL url = new URL(s3Url);
    if (url.getHost().split("\\.").length == 3) {
      return createBucketInPath(url);
    } else {
      return createBucketInHost(url);
    }
  }

  private static UrlBuilderBucketInPath createBucketInPath(URL url) {
    UrlBuilderBucketInPath returner = new UrlBuilderBucketInPath();
    returner.setUrlNamingStrategy(UrlNamingStrategy.BucketInPath);
    returner.setBaseUrl(url.getProtocol() + "://" + url.getHost());

    String[] urlPaths = url.getPath().substring(1).split("\\/");
    returner.setBucketName(urlPaths[0]);

    String[] folderPaths = Arrays.copyOfRange(urlPaths, 1, urlPaths.length - 1);
    returner.setFolder(StringUtils.join(folderPaths, "/"));

    String[] fileNameParts = urlPaths[urlPaths.length - 1].split("\\.");
    returner.setFileNamePart(StringUtils.join(Arrays.stream(fileNameParts)
      .limit(fileNameParts.length - 1)
      .collect(Collectors.toList())
      .toArray(new String[fileNameParts.length - 1]), ".") + RandomStringUtils.random(5,
      true, true));

    returner.setFileExtensionPart(fileNameParts[fileNameParts.length - 1]);
    return returner;
  }

  private static UrlBuilderBucketInHost createBucketInHost(URL url) {
    UrlBuilderBucketInHost returner = new UrlBuilderBucketInHost();
    returner.setUrlNamingStrategy(UrlNamingStrategy.BucketInHost);
    String[] hostParts = url.getHost().split("\\.");
    returner.setHost(StringUtils.join(Arrays.copyOfRange(hostParts, 1, hostParts.length), "."));
    returner.setProtocol(url.getProtocol());
    returner.setBucketName(hostParts[0]);
    returner.setBaseUrl(url.getProtocol() + "://" + returner.getHost());
    String[] urlPaths = url.getPath().substring(1).split("\\/");
    String[] folderPaths = Arrays.copyOfRange(urlPaths, 0, urlPaths.length - 1);
    returner.setFolder(StringUtils.join(folderPaths, "/"));

    String[] fileNameParts = urlPaths[urlPaths.length - 1].split("\\.");
    returner.setFileNamePart(StringUtils.join(Arrays.stream(fileNameParts)
      .limit(fileNameParts.length - 1)
      .collect(Collectors.toList())
      .toArray(new String[fileNameParts.length - 1]), ".") + RandomStringUtils.random(5,
      true, true));
    returner.setFileExtensionPart(fileNameParts[fileNameParts.length - 1]);

    return returner;
  }
}
