package com.kiga.s3.service.resize;

import com.kiga.s3.domain.S3Image;

/**
 * @author bbs
 * @since 5/8/16.
 */
public class ImageWidthStrategy implements ResizeStrategy {
  private double outputWidth;
  private double outputHeight;

  /**
   * Create strategy instance for resizing image by width.
   *
   * @param image s3image record to be resized
   * @param width resulting width of the image
   */
  public ImageWidthStrategy(S3Image image, int width) {
    if (image == null) {
      throw new IllegalArgumentException("image cannot be null");
    }

    if (image.getWidth() == null || image.getWidth() <= 0) {
      throw new IllegalArgumentException("Image's width must be bigger than 0");
    }

    if (image.getHeight() == null || image.getHeight() <= 0) {
      throw new IllegalArgumentException("Image's height must be bigger than 0");
    }

    if (width <= 0) {
      throw new IllegalArgumentException("Output width must be bigger than 0");
    }

    double originalHeight = image.getHeight();
    double originalWidth = image.getWidth();
    double originalRatio = originalWidth / originalHeight;
    outputWidth = width;
    outputHeight = (outputWidth / originalRatio);
  }

  @Override
  public int getWidth() {
    return (int) outputWidth;
  }

  @Override
  public int getHeight() {
    return (int) outputHeight;
  }

  @Override
  public String getCachedDataKey() {
    return "w" + getWidth();
  }
}
