package com.kiga.s3.url;

/**
 * Created by rainerh on 29.05.16.
 */
public class UrlBuilderBucketInHost extends UrlBuilder {
  private String host;
  private String protocol;

  /**
   * returns the full url.
   */
  public String getUrl() {
    return new StringBuilder().append(protocol).append("://")
      .append(getBucketName()).append(".").append(host)
      .append("/").append(getUrlPath())
      .toString();
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getProtocol() {
    return protocol;
  }

  public void setProtocol(String protocol) {
    this.protocol = protocol;
  }
}
