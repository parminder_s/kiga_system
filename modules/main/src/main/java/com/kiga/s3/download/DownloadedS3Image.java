package com.kiga.s3.download;

import java.io.InputStream;

/**
 * @author bbs
 * @since 9/25/16.
 */
public class DownloadedS3Image {
  private InputStream streamedImage;
  private String contentType;

  public DownloadedS3Image() {
  }

  public DownloadedS3Image(InputStream streamedImage, String contentType) {
    this.streamedImage = streamedImage;
    this.contentType = contentType;
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public InputStream getStreamedImage() {
    return streamedImage;
  }

  public void setStreamedImage(InputStream streamedImage) {
    this.streamedImage = streamedImage;
  }
}
