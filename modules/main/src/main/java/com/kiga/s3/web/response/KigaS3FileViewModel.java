package com.kiga.s3.web.response;

import lombok.Data;

/**
 * @author bbs
 * @since 4/17/17.
 */
@Data
public class KigaS3FileViewModel {
  private String fileName;
}
