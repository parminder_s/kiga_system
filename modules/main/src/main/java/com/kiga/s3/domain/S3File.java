package com.kiga.s3.domain;

import com.kiga.db.KigaEntityModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * @author bbs
 * @since 5/21/15.
 */

@Entity
@PrimaryKeyJoinColumn(name = "ID")
@Inheritance(strategy = InheritanceType.JOINED)
public class S3File extends KigaEntityModel {
  @Column(name = "Name")
  private String name;
  @Column(name = "Bucket")
  private String bucket;
  @Column(name = "URL")
  private String url;
  @Column(name = "OwnerID")
  private Long ownerId;
  @Column(name = "Folder")
  private String folder;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBucket() {
    return bucket;
  }

  public void setBucket(String bucket) {
    this.bucket = bucket;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Long getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(Long ownerId) {
    this.ownerId = ownerId;
  }

  public String getFolder() {
    return folder;
  }

  public void setFolder(String folder) {
    this.folder = folder;
  }
}
