package com.kiga.s3.service.resize;

/**
 * @author bbs
 * @since 5/8/16.
 */
public class PaddedImageStrategy implements ResizeStrategy {
  private int outputWidth;
  private int outputHeight;

  /**
   * Padded image strategy. Set output width and height.
   *
   * @param width designated width
   * @param height designated height
     */
  public PaddedImageStrategy(int width, int height) {
    if (width <= 0) {
      throw new IllegalArgumentException("Output width must be bigger than 0");
    }

    if (height <= 0) {
      throw new IllegalArgumentException("Output height must be bigger than 0");
    }

    this.outputWidth = width;
    this.outputHeight = height;
  }

  @Override
  public int getWidth() {
    return outputWidth;
  }

  @Override
  public int getHeight() {
    return outputHeight;
  }

  @Override
  public String getCachedDataKey() {
    return "p" + outputWidth + outputHeight;
  }
}
