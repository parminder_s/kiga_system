package com.kiga.s3.service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.kiga.integration.amazon.s3.config.AmazonS3Parameters;
import com.kiga.s3.EAmazonS3Acl;
import com.kiga.s3.domain.S3File;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StreamUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import javax.validation.constraints.NotNull;

/**
 * @author bbs
 * @since 4/30/16.
 */
public class KigaAmazonS3Client {
  private Logger logger = LoggerFactory.getLogger(getClass());

  private AmazonS3Client amazonS3Client;
  private AmazonS3Parameters amazonS3Parameters;

  public KigaAmazonS3Client(AmazonS3Client amazonS3Client, AmazonS3Parameters amazonS3Parameters) {
    this.amazonS3Client = amazonS3Client;
    this.amazonS3Parameters = amazonS3Parameters;
  }

  public boolean upload(File file) throws IOException {
    return upload(file, EAmazonS3Acl.READ_ALL.getAcl());
  }

  public boolean upload(File file, AccessControlList acl) throws IOException {
    return upload(file, file.getName(), acl);
  }

  public boolean upload(File file, String fileName) throws IOException {
    return upload(file, fileName, EAmazonS3Acl.READ_ALL.getAcl());
  }

  /**
   * Upload file to the amazon s3.
   *
   * @param file     file to be uploaded
   * @param fileName name of the file (if different than uploaded one)
   * @param acl      access control list
   * @return true if upload was successful
   */
  public boolean upload(File file, String fileName, AccessControlList acl) throws IOException {
    if (fileName == null) {
      fileName = file.getName();
    }

    try {
      String bucketName = amazonS3Parameters.getBucketName();
      String key = StringUtils.strip(fileName, "/");
      logger.info("uploading " + key + " to bucket " + bucketName);

      ObjectMetadata objectMetadata = new ObjectMetadata();
      objectMetadata.setContentType(Files.probeContentType(Paths.get(file.getAbsolutePath())));
      objectMetadata.setExpirationTime(Date.from(Instant.now().plus(360, ChronoUnit.DAYS)));
      objectMetadata.setCacheControl("max-ages=" + (60 * 60 * 24 * 360));

      PutObjectRequest putRequest = new PutObjectRequest(bucketName, key, file)
        .withAccessControlList(acl)
        .withMetadata(objectMetadata);
      amazonS3Client.putObject(putRequest);
      return true;
    } catch (AmazonServiceException ex) {
      logger
        .error("The request made it to Amazon S3, but was rejected with an error response for "
          + "some reason.");
      throw ex;
    } catch (AmazonClientException ex) {
      logger
        .error("The client encountered an internal error while trying to communicate with S3, "
          + "such as not being able to access the network.");
      throw ex;
    }
  }

  /**
   * Upload stream to the s3.
   *
   * @param outputStream outputStream to be uploaded
   * @param fileName     name of the file to be uploaded
   * @param acl          acccess control list for the file
   * @return true if uploaded properly
   */
  public boolean upload(@NotNull ByteArrayOutputStream outputStream, @NotNull String fileName,
    @NotNull String contentType, AccessControlList acl) {
    if (fileName == null) {
      throw new IllegalArgumentException("FileName cannot be null");
    }

    try {
      String bucketName = amazonS3Parameters.getBucketName();
      String key = StringUtils.strip(fileName, "/");
      logger.info("uploading " + key + " to bucket " + bucketName);
      ObjectMetadata objectMetadata = new ObjectMetadata();
      objectMetadata.setContentLength(outputStream.size());

      objectMetadata.setContentType(contentType);
      objectMetadata.setHttpExpiresDate(Date.from(Instant.now().plus(360, ChronoUnit.DAYS)));
      objectMetadata.setCacheControl("max-ages=" + (60 * 60 * 24 * 360));

      InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
      PutObjectRequest putRequest =
        new PutObjectRequest(bucketName, key, inputStream, objectMetadata)
          .withAccessControlList(acl);
      amazonS3Client.putObject(putRequest);
      return true;
    } catch (AmazonServiceException ex) {
      logger
        .error("The request made it to Amazon S3, but was rejected with an error response for "
          + "some reason.");
      throw ex;
    } catch (AmazonClientException ex) {
      logger
        .error("The client encountered an internal error while trying to communicate with S3, "
          + "such as not being able to access the network.");
      throw ex;
    }
  }

  /**
   * Download file from s3.
   *
   * @param s3File file entity of the file to be downloaded
   * @return downloaded file
   * @throws IOException IOException
   */
  public File download(S3File s3File) throws IOException {
    return download(s3File.getName());
  }

  /**
   * Download file from s3.
   *
   * @param bucket   bucket on s3
   * @param fileName file name of the file to be downloaded
   * @return downloaded file
   * @throws IOException ioexception
   */
  public File download(String bucket, String fileName) throws IOException {
    File returner = File.createTempFile("kiga", null, new File("/tmp/"));

    int bytesCopied = StreamUtils
      .copy(stream(bucket, fileName).getObjectContent(), new FileOutputStream(returner));

    if (bytesCopied == 0) {
      returner = null;
    }

    return returner;
  }

  /**
   * Returns the file from the configured bucket.
   *
   * @param fileName s3 filename
   * @return downloaded File
   */
  public File download(String fileName) throws IOException {
    return download(amazonS3Parameters.getBucketName(), fileName);
  }

  /**
   * Get stream of the file from s3.
   *
   * @param s3File file to be downloaded as a database record
   * @return input stream from the file
   */
  public S3Object stream(S3File s3File) {
    return stream(s3File.getName());
  }

  public S3Object stream(String fileName) {
    return stream(amazonS3Parameters.getBucketName(), fileName);
  }

  /**
   * Stream file from s3.
   *
   * @param bucket   bucket name
   * @param fileName filename
   * @return input stream of the downloaded file
   */
  public S3Object stream(String bucket, String fileName) {
    return amazonS3Client.getObject(new GetObjectRequest(bucket, fileName));
  }
}
