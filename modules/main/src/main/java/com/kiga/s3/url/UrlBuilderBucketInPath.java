package com.kiga.s3.url;

/**
 * Created by rainerh on 29.05.16.
 */
public class UrlBuilderBucketInPath extends UrlBuilder {

  /**
   * returns the full url.
   */
  public String getUrl() {
    return new StringBuilder()
      .append(getBaseUrl()).append("/")
      .append(getBucketName()).append("/")
      .append(getUrlPath())
      .toString();
  }
}
