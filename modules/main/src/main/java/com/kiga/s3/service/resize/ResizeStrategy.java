package com.kiga.s3.service.resize;

/**
 * @author bbs
 * @since 5/8/16.
 */
public interface ResizeStrategy {
  int getWidth();

  int getHeight();

  String getCachedDataKey();
}
