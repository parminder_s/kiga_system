package com.kiga.s3.service.download;

import com.kiga.s3.domain.S3File;
import com.kiga.s3.download.DownloadedS3Image;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.client.fluent.Request;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;

/**
 * @author bbs
 * @since 9/25/16.
 */
public class DownloadUsingRequestService implements DownloadService {
  @Override
  public DownloadedS3Image download(S3File image) throws IOException {
    InputStream streamedImage = getStreamedImage(image);

    TikaConfig config = TikaConfig.getDefaultConfig();
    Detector detector = config.getDetector();

    TikaInputStream stream = TikaInputStream.get(streamedImage);

    Metadata metadata = new Metadata();
    metadata.add(Metadata.RESOURCE_NAME_KEY, image.getName());
    MediaType mediaType = detector.detect(stream, metadata);

    String contentType = mediaType.getType() + "/" + mediaType.getSubtype();
    stream.close();

    return new DownloadedS3Image(streamedImage, contentType);
  }

  protected InputStream getStreamedImage(S3File image) throws IOException {
    return Request.Get(image.getUrl()).execute().returnContent().asStream();
  }

  public InputStream getStreamedFile(String url) throws IOException {
    return Request.Get(url).execute().returnContent().asStream();
  }
}
