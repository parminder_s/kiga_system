package com.kiga.s3.web;

import com.kiga.s3.domain.S3File;
import com.kiga.s3.repository.ss.S3FileRepository;
import com.kiga.security.services.SecurityService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by peter on 18.07.17.
 */

@Controller
@RequestMapping("/s3")
public class S3Controller {
  SecurityService securityService;
  S3FileRepository s3FileRepository;

  @Autowired
  public S3Controller(SecurityService securityService, S3FileRepository s3FileRepository) {
    this.securityService = securityService;
    this.s3FileRepository = s3FileRepository;
  }

  /**
   * returns an s3File.
   */
  @RequestMapping(value = "download/{id}", method = RequestMethod.GET)
  public void downloadS3File(@PathVariable Long id, HttpServletResponse response)
    throws IOException {
    securityService.requiresLogin();
    S3File s3File = s3FileRepository.findOne(id);

    InputStream fis = new URL(s3File.getUrl()).openStream();
    OutputStream outputStream = response.getOutputStream();
    response.setContentType(Files.probeContentType(Paths.get(s3File.getUrl())));
    response.setHeader(org.springframework.http.HttpHeaders.CONTENT_DISPOSITION,
      "attachment; filename=\"" + s3File.getName() + "\"");
    IOUtils.copy(fis, outputStream);
    fis.close();
    outputStream.close();
  }

}
