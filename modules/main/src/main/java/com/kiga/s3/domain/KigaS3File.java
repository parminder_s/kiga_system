package com.kiga.s3.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * @author bbs
 * @since 5/21/15.
 */

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@PrimaryKeyJoinColumn(name = "ID")
public class KigaS3File extends S3File {
  private Integer size;
}
