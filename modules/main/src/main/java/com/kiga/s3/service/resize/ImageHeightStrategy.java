package com.kiga.s3.service.resize;

import com.kiga.s3.domain.S3Image;

/**
 * @author bbs
 * @since 5/8/16.
 */
public class ImageHeightStrategy implements ResizeStrategy {
  private double outputWidth;
  private double outputHeight;

  /**
   * Create strategy instance for resizing image by height.
   *
   * @param image s3image record to be resized
   * @param height resulting height of the image
   */
  public ImageHeightStrategy(S3Image image, int height) {
    if (image == null) {
      throw new IllegalArgumentException("image cannot be null");
    }

    if (image.getWidth() == null || image.getWidth() <= 0) {
      throw new IllegalArgumentException("Image's width must be bigger than 0");
    }

    if (image.getHeight() == null || image.getHeight() <= 0) {
      throw new IllegalArgumentException("Image's height must be bigger than 0");
    }

    if (height <= 0) {
      throw new IllegalArgumentException("Output height must be bigger than 0");
    }

    double originalHeight = image.getHeight();
    double originalWidth = image.getWidth();
    double originalRatio = originalWidth / originalHeight;
    outputHeight = height;
    outputWidth = (outputHeight / originalRatio);
  }

  @Override
  public int getWidth() {
    return (int) outputWidth;
  }

  @Override
  public int getHeight() {
    return (int) outputHeight;
  }

  @Override
  public String getCachedDataKey() {
    return "h" + getHeight();
  }
}
