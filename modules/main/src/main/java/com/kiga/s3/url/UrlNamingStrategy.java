package com.kiga.s3.url;

/**
 * Created by rainerh on 29.05.16.
 */
public enum UrlNamingStrategy {
  BucketInPath,
  BucketInHost
}
