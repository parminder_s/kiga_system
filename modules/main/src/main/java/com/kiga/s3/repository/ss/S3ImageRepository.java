package com.kiga.s3.repository.ss;

import com.kiga.s3.domain.S3Image;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 14.02.16.
 */
public interface S3ImageRepository extends CrudRepository<S3Image, Long> {
}

