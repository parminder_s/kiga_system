package com.kiga.s3.repository.ss;

import com.kiga.s3.domain.S3File;
import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 5/21/15.
 */
public interface S3FileRepository extends CrudRepository<S3File, Long> {
}
