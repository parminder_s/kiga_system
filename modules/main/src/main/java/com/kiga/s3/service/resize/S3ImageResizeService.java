package com.kiga.s3.service.resize;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.kiga.integration.amazon.s3.config.AmazonS3Parameters;
import com.kiga.s3.EAmazonS3Acl;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.download.DownloadedS3Image;
import com.kiga.s3.service.KigaAmazonS3Client;
import com.kiga.s3.service.download.DownloadService;
import com.kiga.s3.service.resize.exception.ResizeException;
import com.kiga.s3.url.UrlBuilder;
import com.kiga.s3.url.UrlBuilderFactory;
import com.kiga.s3.wrapper.ImageIoWrapper;
import com.kiga.spec.Persister;
import com.kiga.upload.ImageProcessor;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * This service is required for generating resized images for a given S3Image.
 * By giving the required dimensions that service checks the internal cache
 * and if the image is not yet available downloads it from S3, resizes and uploads
 * the new thumbnail to S3 along storing into the cache (property of S3Image entity).
 *
 * @author bbs
 * @since 4/24/16.
 */
public class S3ImageResizeService {
  private Logger logger = LoggerFactory.getLogger(getClass());

  private ImageIoWrapper imageIoWrapper;
  private DownloadService downloadService;
  private AmazonS3Parameters amazonS3Parameters;
  private ImageProcessor imageProcessor;
  private Persister<S3Image> persister;
  private KigaAmazonS3Client kigaAmazonS3Client;

  /**
   * Controller for dependency injection.
   */
  public S3ImageResizeService(AmazonS3Parameters amazonS3Parameters, ImageProcessor
    imageProcessor, Persister<S3Image> persister, KigaAmazonS3Client kigaAmazonS3Client,
    DownloadService downloadService, ImageIoWrapper imageIoWrapper) {
    this.imageIoWrapper = imageIoWrapper;
    this.amazonS3Parameters = amazonS3Parameters;
    this.imageProcessor = imageProcessor;
    this.persister = persister;
    this.kigaAmazonS3Client = kigaAmazonS3Client;
    this.downloadService = downloadService;
  }

  /**
   * Resize image, upload to the amazon s3, save in CachedData and return url to the modified image.
   *
   * @param image          S3Image entity
   * @param resizeStrategy Resizing strategy
   * @return Url to the new image
   * @throws IOException if the image does not exists in s3 bucket
   */
  public String getResizedImageUrl(S3Image image, ResizeStrategy resizeStrategy)
    throws IOException {
    if (image == null || StringUtils.isEmpty(image.getUrl())) {
      logger.error("image is empty or has empty url property");
      return null;
    }

    Map<String, String> cachedDataJson = new HashMap<>();
    if (MapUtils.isNotEmpty(image.getCachedDataJson())) {
      cachedDataJson.putAll(image.getCachedDataJson());
    }

    String cachedDataKey = resizeStrategy.getCachedDataKey();

    if (cachedDataJson.containsKey(cachedDataKey)) {
      return cachedDataJson.get(cachedDataKey);
    } else {
      InputStream streamedImage = null;
      ByteArrayOutputStream os = null;
      try {
        DownloadedS3Image downloadedS3Image = downloadService.download(image);
        streamedImage = downloadedS3Image.getStreamedImage();

        if (streamedImage == null || streamedImage.available() == 0) {
          logger.error("Could not stream image from s3; returning empty url");
          return "";
        }

        String contentType = downloadedS3Image.getContentType();

        // resize image
        BufferedImage downloadedImage = imageIoWrapper.read(streamedImage);
        if (downloadedImage == null) {
          logger.error("Could not read " + image.getUrl() + " with content type " + contentType);
          return "";
        }

        downloadedImage = imageProcessor
          .resize(downloadedImage, resizeStrategy.getWidth(), resizeStrategy.getHeight());

        // upload to amazon s3
        UrlBuilder urlBuilder = UrlBuilderFactory.create(image.getUrl())
          .useBucket(amazonS3Parameters.getBucketName())
          .appendFilenameSuffix(cachedDataKey);

        os = new ByteArrayOutputStream();
        imageIoWrapper.write(downloadedImage, urlBuilder.getFileExtensionPart(), os);

        kigaAmazonS3Client.upload(
          os, urlBuilder.getFileNameWithFolder(), contentType, EAmazonS3Acl.READ_ALL.getAcl());

        // save entity with cached data
        String returner = urlBuilder.toString();
        cachedDataJson.put(cachedDataKey, returner);
        image.setCachedDataJson(cachedDataJson);
        persister.persist(image);
        return returner;
      } catch (AmazonS3Exception ase) {
        logger.error(ase.getErrorResponseXml(), ase);
        logger.error("failed to download " + image.getUrl());
        return "";
      } catch (ResizeException resizeException) {
        logger.error("resize exception on S3Image " + image.getId(), resizeException);
        return "";
      } finally {
        if (streamedImage != null) {
          streamedImage.close();
        }
        if (os != null) {
          os.close();
        }
      }
    }
  }
}
