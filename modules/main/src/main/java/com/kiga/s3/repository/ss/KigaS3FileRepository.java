package com.kiga.s3.repository.ss;

import com.kiga.s3.domain.KigaS3File;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 14.02.16.
 */
public interface KigaS3FileRepository extends CrudRepository<KigaS3File, Long> {
}
