package com.kiga.s3.domain.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import javax.persistence.AttributeConverter;

/**
 * @author bbs
 * @since 4/23/16.
 */
public class MapToJsonConverter implements AttributeConverter<Map<String, String>, String> {
  private static final ObjectMapper objectMapper = new ObjectMapper();
  private Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public String convertToDatabaseColumn(Map<String, String> attributes) {
    try {
      return objectMapper.writeValueAsString(attributes);
    } catch (JsonProcessingException ex) {
      logger.error("Problem with processing JSON from database column");
      return null;
    }
  }

  @Override
  public Map<String, String> convertToEntityAttribute(String json) {
    if (json == null) {
      return null;
    } else {
      try {
        @SuppressWarnings("unchecked")
        Map<String, String> returner = objectMapper.readValue(json, Map.class);
        return returner;
      } catch (IOException ex) {
        logger.error("Unexpected IOEx decoding json from database: " + json);
        return null;
      }
    }
  }
}
