package com.kiga.plan;

import com.kiga.content.domain.ss.live.KigaPageLive;
import com.kiga.db.KigaEntityModel;
import com.kiga.db.LocaleToLongNotationConverter;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * Created by peter on 05.09.17.
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Plan extends KigaEntityModel {
  @OneToOne
  @JoinColumn(name = "AuthorID")
  Member author;

  @Convert(converter = LocaleToLongNotationConverter.class)
  Locale locale;

  @OneToOne
  @JoinColumn(name = "PreviewItemID")
  KigaPageLive previewPage;

  @OneToOne
  @JoinColumn(name = "CopiedFromPlanID")
  Plan copiedFrom;

  @OneToOne
  @JoinColumn(name = "ReferencePlanID")
  Plan reference;

  String title;
  String color;
  String description;
  String icon;
  String planType;

  Date startDate;
  Date endDate;

  Boolean isArchived;
  Boolean isPublished;
  Boolean syncEnabled;
  Boolean atHomePage;
}
