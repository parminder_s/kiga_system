package com.kiga.plan;

import com.kiga.security.domain.Member;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by peter on 05.09.17.
 */
public interface PlanRepository extends CrudRepository<Plan, Long> {
  List<Plan> findByAuthor(Member member);
}
