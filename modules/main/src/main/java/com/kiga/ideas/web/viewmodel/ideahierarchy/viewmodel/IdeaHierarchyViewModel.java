package com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel;

import com.kiga.cms.web.response.ViewModel;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Florian Schneider
 * @since 09.01.17
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class IdeaHierarchyViewModel extends ViewModel {
  private Category activeCategory; // actual
  private List<Category> categories;
  private List<ParentCategoryViewModel> parentCategories; // siblings
  private String firstLevelCategoryName;
}
