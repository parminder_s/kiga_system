package com.kiga.ideas;

import com.amazonaws.services.s3.AmazonS3Client;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.KigaIdeaRepositoryProxy;
import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepositoryProxy;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.service.DefaultIdeaCategorySortToDbSort;
import com.kiga.ideas.service.IdeaGroupToIdeaService;
import com.kiga.ideas.service.IdeaPermissionService;
import com.kiga.ideas.service.NewIdeasGroupService;
import com.kiga.ideas.task.cache.IdeasCacheActualizer;
import com.kiga.ideas.task.cache.IdeasCacheExecutor;
import com.kiga.ideas.task.cache.IdeasCacheJob;
import com.kiga.ideas.task.cache.IdeasCachePutter;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.ideas.web.viewmodel.kigaidea.LinkedIdeasSetter;
import com.kiga.ideas.web.viewmodel.kigaidea.LinksReplacer;
import com.kiga.ideas.web.viewmodel.kigaidea.PreviewImagesSetter;
import com.kiga.s3.service.KigaAmazonS3Client;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.security.services.SecurityService;
import com.kiga.web.converter.ResponseConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class IdeasConfig {
  @Bean
  @Primary
  IdeaGroupToIdeaService getIdeaGroupToIdeaService(
      KigaIdeaRepositoryProxy kigaIdeaRepositorySwitcher,
      ResponseConverter<KigaIdea, IdeasViewModelIdea> ideasResponseFactory,
      IdeasProperties ideasProperties,
      IdeaCategoryRepositoryProxy ideaCategoryModel) {
    return new IdeaGroupToIdeaService(
        kigaIdeaRepositorySwitcher,
        ideasResponseFactory,
        ideasProperties,
        new DefaultIdeaCategorySortToDbSort(),
        ideaCategoryModel);
  }

  @Bean
  @Primary
  public NewIdeasGroupService getAgeGroupIdeasService(
      NewIdeasRepository newIdeasRepository,
      ResponseConverter<KigaIdea, IdeasViewModelIdea> ideasResponseFactory,
      IdeasProperties ideasProperties,
      IdeaCategoryRepositoryProxy ideaCategoryModel,
      SiteTreeUrlGenerator siteTreeUrlGenerator) {
    return new NewIdeasGroupService(
        ideasResponseFactory,
        newIdeasRepository,
        ideasProperties,
        ideaCategoryModel,
        siteTreeUrlGenerator);
  }

  @Bean(name = "kigAmazonS3ClientIdeas")
  public KigaAmazonS3Client getUploaderService(
      AmazonS3Client amazonS3Client,
      @Qualifier("ideasAmazonS3Parameters") IdeasAmazonS3Parameters ideasAmazonS3Parameters) {
    return new KigaAmazonS3Client(amazonS3Client, ideasAmazonS3Parameters);
  }

  @Bean
  public IdeasCacheActualizer getIdeasCacheActualizer(IdeasCachePutter ideasCachePutter) {
    return new IdeasCacheActualizer(ideasCachePutter);
  }

  @Bean
  public IdeasCacheJob getIdeasCacheJob(
      IdeasCacheActualizer actualizer, IdeaGroupContainerRepositoryProxy repositoryProxy) {
    return new IdeasCacheJob(actualizer, repositoryProxy);
  }

  @Bean
  public IdeasCacheExecutor getIdeasCacheExecutor(
      IdeasCacheActualizer actualizer, IdeaCategoryRepositoryProxy repositoryProxy) {
    return new IdeasCacheExecutor(repositoryProxy, actualizer);
  }

  @Bean
  public IdeaPermissionService getIdeaPermissionService(SecurityService securityService) {
    return new IdeaPermissionService(securityService);
  }

  @Bean
  public LinkedIdeasSetter createLinkedIdeasSetter(
      IdeasProperties ideasProperties,
      S3ImageResizeService s3ImageResizeService,
      SiteTreeUrlGenerator urlGenerator) {
    return new LinkedIdeasSetter(ideasProperties, s3ImageResizeService, urlGenerator);
  }

  @Bean
  public PreviewImagesSetter createPreviewImagesSetter(
      IdeasProperties ideasProperties, S3ImageResizeService s3ImageResizeService) {
    return new PreviewImagesSetter(ideasProperties, s3ImageResizeService);
  }

  @Bean
  public LinksReplacer createLinksReplacer(
      SiteTreeLiveRepository siteTreeRepository, SiteTreeUrlGenerator urlGenerator) {
    return new LinksReplacer(siteTreeRepository, urlGenerator);
  }
}
