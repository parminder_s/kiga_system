package com.kiga.ideas.web.viewmodel.ideahierarchy.submanagers;

import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.IdeaGroupViewModelMapper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.IdeaGroupQueryEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaGroupQueryResult;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.IdeaGroupViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 24.01.18
 */
@Service
public class IdeaGroupManager {

  private IdeaGroupViewModelMapper ideaGroupViewModelMapper;
  private IdeaGroupQueryEngine ideaGroupQueryEngine;

  @Autowired
  public IdeaGroupManager(
      IdeaGroupViewModelMapper ideaGroupViewModelMapper,
      IdeaGroupQueryEngine ideaGroupQueryEngine) {
    this.ideaGroupViewModelMapper = ideaGroupViewModelMapper;
    this.ideaGroupQueryEngine = ideaGroupQueryEngine;
  }

  /** get IdeaGroupViewModel. */
  public IdeaGroupViewModel getViewModel(IdeaQueryParameter ideaQueryParameter) {
    IdeaGroupQueryResult ideaGroupQueryResult = ideaGroupQueryEngine.query(ideaQueryParameter);

    return ideaGroupViewModelMapper.getViewModel(
        ideaQueryParameter.getIdeaCategory(),
        ideaGroupQueryResult.getIdeas(),
        ideaGroupQueryResult.getSiblings());
  }
}
