package com.kiga.ideas.web.viewmodel.ideahierarchy.submanagers;

import com.kiga.cms.web.response.ViewModel;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.IdeaGroupCategoryViewModelMapper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.IdeaGroupCategoryQueryEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaGroupCategoryQueryResult;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 24.01.18
 */
@Service
public class IdeaGroupCategoryManager {

  private IdeaGroupCategoryQueryEngine ideaGroupCategoryQueryEngine;
  private IdeaGroupCategoryViewModelMapper ideaGroupCategoryViewModelMapper;

  @Autowired
  public IdeaGroupCategoryManager(
      IdeaGroupCategoryQueryEngine ideaGroupCategoryQueryEngine,
      IdeaGroupCategoryViewModelMapper ideaGroupCategoryViewModelMapper) {
    this.ideaGroupCategoryQueryEngine = ideaGroupCategoryQueryEngine;
    this.ideaGroupCategoryViewModelMapper = ideaGroupCategoryViewModelMapper;
  }

  /** get IdeaGroupCategoryViewModel or RedirectViewModel. */
  public ViewModel getViewModel(IdeaQueryParameter ideaQueryParameter) {
    IdeaGroupCategoryQueryResult ideaGroupCategoryQueryResult =
        ideaGroupCategoryQueryEngine.query(ideaQueryParameter);

    return ideaGroupCategoryViewModelMapper.getViewModel(
        ideaQueryParameter.getMember().isPresent(),
        ideaQueryParameter.getAgeGroupFilter(),
        ideaQueryParameter.getIdeaCategory(),
        ideaGroupCategoryQueryResult.getIdeaSets(),
        ideaGroupCategoryQueryResult.getSiblings());
  }
}
