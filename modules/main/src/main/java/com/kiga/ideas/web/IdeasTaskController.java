package com.kiga.ideas.web;

import com.kiga.ideas.task.cache.IdeasCacheExecutor;
import com.kiga.ideas.task.cache.IdeasCacheJob;
import com.kiga.ideas.web.request.IdeasCacheRequest;
import com.kiga.security.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by rainerh on 30.05.16.
 */
@RestController
@RequestMapping("ideasTask")
public class IdeasTaskController {
  private SecurityService securityService;
  private IdeasCacheExecutor ideasCacheExecutor;
  private IdeasCacheJob ideasCacheJob;

  /**
   * inject dependencies.
   */
  @Autowired
  public IdeasTaskController(
    SecurityService securityService,
    IdeasCacheExecutor ideasCacheExecutor, IdeasCacheJob ideasCacheJob) {
    this.securityService = securityService;
    this.ideasCacheExecutor = ideasCacheExecutor;
    this.ideasCacheJob = ideasCacheJob;
  }

  /**
   * runs the ideasCacheActualizer.
   */
  @RequestMapping("actualizeIdeasCache")
  public String actualizeIdeasCache(IdeasCacheRequest ideasCacheRequest) {
    securityService.requiresContentAdmin();
    ideasCacheExecutor
      .run(ideasCacheRequest.getSiteTreeId(), ideasCacheRequest.getAgeGroupFilters(), true);
    return "job started asynchronously";
  }

  /**
   * run job task.
   */
  @RequestMapping("runJob")
  public String runJob() throws Exception {
    securityService.requiresContentAdmin();
    ideasCacheJob.runJob();
    return "job started asynchronously";
  }
}
