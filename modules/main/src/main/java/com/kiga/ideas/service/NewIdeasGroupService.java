package com.kiga.ideas.service;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.properties.IdeasRetrieveProperties;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.main.locale.Locale;
import com.kiga.web.converter.ResponseConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;

/**
 * @author bbs
 * @since 7/12/16.
 */
public class NewIdeasGroupService {
  protected ResponseConverter<KigaIdea, IdeasViewModelIdea> ideasResponseFactory;
  private IdeasProperties ideasProperties;
  private NewIdeasRepository newIdeasRepository;
  private IdeaCategoryRepositoryProxy ideaCategoryModel;
  private SiteTreeUrlGenerator siteTreeUrlGenerator;
  private Logger logger = LoggerFactory.getLogger(getClass());

  /**
   * Create service with deps.
   *
   * @param ideasResponseFactory ideasResponseFactory
   * @param newIdeasRepository        AgeGroupIdeasModel
   * @param ideasProperties      ideasProperties
   */
  public NewIdeasGroupService(
    ResponseConverter<KigaIdea, IdeasViewModelIdea> ideasResponseFactory,
    NewIdeasRepository newIdeasRepository, IdeasProperties ideasProperties,
    IdeaCategoryRepositoryProxy ideaCategoryModel, SiteTreeUrlGenerator siteTreeUrlGenerator) {
    this.ideasResponseFactory = ideasResponseFactory;
    this.ideasProperties = ideasProperties;
    this.newIdeasRepository = newIdeasRepository;
    this.ideaCategoryModel = ideaCategoryModel;
    this.siteTreeUrlGenerator = siteTreeUrlGenerator;
  }

  /**
   * get ideas for all age groups based on provided properties.
   *
   * @param ideasRetrieveProperties properties
   * @return list of ideas view models
   */
  public List<IdeasViewModelIdea> getIdeasForAllAgeGroups(
    IdeasRetrieveProperties ideasRetrieveProperties) {
    Pageable pageable = new PageRequest(0, ideasProperties.getAmountPerCategory());
    return getIdeasForAllAgeGroupsDelegate(ideasRetrieveProperties, pageable)
      .stream()
      .filter(this::hasValidParent)
      .map(ideasResponseFactory::convertToResponse)
      .collect(Collectors.toList());
  }

  /**
   * get all new ideas.
   *
   * @return list of all new ideas view models
   */
  public List<IdeasViewModelIdea> getNewIdeas(IdeasRetrieveProperties ideasRetrieveProperties) {
    List<KigaIdea> ideas = newIdeasRepository.getAllIdeas(ideasRetrieveProperties.getLocale(),
      ideasRetrieveProperties.getAgeGroupFilter());

    return ideas
      .stream()
      .filter(this::hasValidParent)
      .map(ideasResponseFactory::convertToResponse)
      .collect(Collectors.toList());
  }


  protected List<KigaIdea> getIdeasForAllAgeGroupsDelegate(
    IdeasRetrieveProperties ideasRetrieveProperties, Pageable pageable) {
    Locale locale = ideasRetrieveProperties.getLocale();
    int ageGroupFilter = ideasRetrieveProperties.getAgeGroupFilter();
    return newIdeasRepository.getIdeasForAllAgeGroups(locale, ageGroupFilter, pageable);
  }

  protected Set<KigaIdea> getIdeasForAgeGroupByBitPositionDelegate(
    IdeasRetrieveProperties ideasRetrieveProperties) {
    final int ageGroupCode = ideasRetrieveProperties.getGroup().calculateAgeGroupValue();
    final Locale locale = ideasRetrieveProperties.getLocale();
    final int ageGroupFilter = ideasRetrieveProperties.getAgeGroupFilter();
    return newIdeasRepository
      .getIdeasForAgeGroupByBitPosition(ageGroupCode, locale, ageGroupFilter);
  }

  /**
   * Get all ideas by age filter value and based on provided properties.
   *
   * @param ideasRetrieveProperties properties
   * @return ideas
   */
  public List<IdeasViewModelIdea> getAllIdeasByValue(
    IdeasRetrieveProperties ideasRetrieveProperties) {
    return getIdeasForAgeGroupByBitPositionDelegate(ideasRetrieveProperties)
      .stream()
      .filter(this::hasValidParent)
      .map(ideasResponseFactory::convertToResponse)
      .collect(Collectors.toList());
  }

  /**
   * returns the url for new ideas.
   */
  public String getNewIdeasUrl(Locale locale) {
    IdeaCategory ideaCategory = ideaCategoryModel.findByCodeAndLocale("new-ideas", locale);
    return this.siteTreeUrlGenerator.getRelativeNgUrl(ideaCategory);
  }

  protected boolean hasValidParent(KigaIdea kigaIdea) {
    try {
      kigaIdea.getParent().getTitle();
      return true;
    } catch (EntityNotFoundException enfe) {
      logger.error("SiteTree with ID " + kigaIdea.getId() + " has no parent");
      return false;
    } catch (NullPointerException npe) {
      logger.error("SiteTree with ID " + kigaIdea.getId() + " has a null parent");
      return false;
    }
  }
}
