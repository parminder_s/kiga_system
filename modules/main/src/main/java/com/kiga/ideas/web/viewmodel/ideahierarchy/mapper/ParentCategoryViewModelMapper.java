package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.ParentCategoryViewModel;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 23.01.18
 */
@Service
public class ParentCategoryViewModelMapper {

  /**
   * Convert a List of IdeaCategories to a list of ParentCategoryViewModels reserved for siblings.
   */
  public List<ParentCategoryViewModel> convertSiblings(List<IdeaCategory> siblings) {

    return siblings.stream().map(this::convert).collect(Collectors.toList());
  }

  private ParentCategoryViewModel convert(IdeaCategory ideaCategory) {

    ParentCategoryViewModel returner =
        ParentCategoryViewModel.builder()
            .name(ideaCategory.getUrlSegment())
            .title(ideaCategory.getTitle())
            .icon(ideaCategory.getIconCode() != null ? ideaCategory.getIconCode() : "icon-ideas")
            .disableForFavourites(ideaCategory.isDisableForFavourites())
            .build();
    returner.setCode(ideaCategory.getCode());
    return returner;
  }
}
