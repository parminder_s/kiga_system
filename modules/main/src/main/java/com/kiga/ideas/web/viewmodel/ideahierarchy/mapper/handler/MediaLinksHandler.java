package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.handler;

import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.Idea;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class MediaLinksHandler {

  /** Set MediaLinks. */
  public void setMediaLinks(Idea returner, String content) {
    if (content != null) {
      Document doc = Jsoup.parse(content);
      Element audioLink = doc.select("a.sm2_link").first();
      if (audioLink != null) {
        String linkHref = audioLink.attr("href");
        returner.setAudioLink(linkHref);
      }
      Element videoLink = doc.select("a.videoplayer").first();
      if (videoLink != null) {
        String linkHref = videoLink.attr("href");
        returner.setVideoLink(linkHref);
      }
    }
  }
}
