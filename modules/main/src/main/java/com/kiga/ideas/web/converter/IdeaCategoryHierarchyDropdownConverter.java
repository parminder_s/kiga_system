package com.kiga.ideas.web.converter;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.ideas.web.response.IdeaCategoryHierarchyViewModel;
import com.kiga.web.converter.DefaultViewModelConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author bbs
 * @since 5/13/16.
 */
@Service
public class IdeaCategoryHierarchyDropdownConverter
  extends DefaultViewModelConverter<IdeaCategory, IdeaCategoryHierarchyViewModel> {
  private static Logger logger =
    LoggerFactory.getLogger(IdeaCategoryHierarchyDropdownConverter.class);

  @Override
  public IdeaCategoryHierarchyViewModel convertToResponse(IdeaCategory entity) {
    IdeaCategoryHierarchyViewModel returner = new IdeaCategoryHierarchyViewModel();
    returner.setCategoryId(entity.getParent().getId());
    returner.setCategoryName(entity.getParent().getUrlSegment());
    returner.setCategoryTitle(entity.getParent().getTitle());
    returner.setCategorySort(entity.getParent().getSort());

    returner.setSubCategoryId(entity.getId());
    returner.setSubCategoryName(entity.getUrlSegment());
    returner.setSubCategoryTitle(entity.getTitle());
    returner.setSubCategorySort(entity.getSort());

    return returner;
  }
}
