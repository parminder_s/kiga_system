package com.kiga.ideas.web;

import com.kiga.ideas.service.RatingCalculatorService;
import com.kiga.ideas.web.response.IdeasResponseRating;
import com.kiga.ideas.web.service.IdeaDetailsService;
import com.kiga.s3.service.download.DownloadUsingRequestService;
import com.kiga.util.BrowserPusherService;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * @author asv
 * @since 23/06/16.
 */

@RestController
@RequestMapping({"/idea"})
public class IdeaDetailsController {
  private IdeaDetailsService ideaDetailsService;
  private RatingCalculatorService ratingCalculatorService;
  private BrowserPusherService browserPusherService;


  /**
   * Constructor to pass all dependencies.
   *
   * @param ideaDetailsService   ideaDetailsService
   * @param browserPusherService browser pusher service
   */

  @Autowired
  public IdeaDetailsController(
    IdeaDetailsService ideaDetailsService,
    RatingCalculatorService ratingCalculatorService, BrowserPusherService browserPusherService) {
    this.ideaDetailsService = ideaDetailsService;
    this.ratingCalculatorService = ratingCalculatorService;
    this.browserPusherService = browserPusherService;
  }

  /**
   * Get idea rating.
   */
  @ResponseBody
  @RequestMapping(value = "/rating/{id}", method = RequestMethod.GET)
  public IdeasResponseRating getRating(@PathVariable("id") long id) {
    return ratingCalculatorService
      .getIdeaResponseRating(ideaDetailsService.getKigaPage(id));
  }

  /**
   * Download file.
   */
  @RequestMapping(value = "/download", method = RequestMethod.GET, params = {"url"})
  public void downloadFile(@RequestParam(value = "url") String url,
    HttpServletResponse response) {
    Logger logger = LoggerFactory.getLogger(getClass());
    DownloadUsingRequestService downloadService = new DownloadUsingRequestService();
    try {
      InputStream file = downloadService.getStreamedFile(url);
      String fileName = FilenameUtils.getName(url);
      browserPusherService.pushFile(response, file, fileName, "application/octet-stream");
      file.close();
    } catch (FileNotFoundException ex) {
      logger.warn("File not found", ex);
    } catch (IOException ex) {
      logger.warn("IOException", ex);
    }
  }


  /**
   * returns the previewUrl of a given idea. A preview url is defined
   */
  @ResponseBody
  @RequestMapping("/getPreviewUrl/{id}")
  public String getPreviewUrl(@PathVariable long id) {
    return ideaDetailsService.getIdeaDetails(id).getPreviewSiteUrl();
  }
}
