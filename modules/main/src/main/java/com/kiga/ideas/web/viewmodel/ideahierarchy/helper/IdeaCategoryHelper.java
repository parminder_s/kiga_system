package com.kiga.ideas.web.viewmodel.ideahierarchy.helper;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.ideas.IdeasProperties;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 24.01.18
 */
@Service
@Log4j2
public class IdeaCategoryHelper {
  private IdeasProperties ideasProperties;

  @Autowired
  public IdeaCategoryHelper(IdeasProperties ideasProperties) {
    this.ideasProperties = ideasProperties;
  }

  /** get Icon Code of given IdeaCategory. */
  public String getIconCode(IdeaCategory ideaCategory) {
    try {
      while (!ideaCategory.getClassName().equals("IdeaGroupMainContainer")) {
        if (!StringUtils.isEmpty(ideaCategory.getIconCode())) {
          return ideaCategory.getIconCode();
        }
        ideaCategory = (IdeaCategory) ideaCategory.getParent();
      }
    } catch (Exception exception) {
      log.error(exception.getMessage());
    }
    return "icon-ideas";
  }

  /** inherit the value from the first child below IdeaGroupMainContainer. */
  public boolean getDisableForFavourites(IdeaCategory ideaCategory) {
    boolean returner = false;
    while (!ideaCategory.getClassName().equals("IdeaGroupMainContainer")
        && !ideaCategory.getClassName().equals("MainArticleCategory")) {
      if (ideaCategory.getParent().getClassName().equals("IdeaGroupMainContainer")
          || ideaCategory.getParent().getClassName().equals("MainArticleCategory")) {
        returner = ideaCategory.isDisableForFavourites();
        break;
      }
      ideaCategory = (IdeaCategory) ideaCategory.getParent();
    }

    return returner;
  }

  /** Check if redirect is necessary. */
  public boolean isRedirect(SiteTree siteTree) {
    return "IdeaGroupMainContainer".equalsIgnoreCase(siteTree.getClassName())
        && ideasProperties.isSiblingsAsMenu();
  }

  public boolean isNewIdeaGroupCategory(IdeaCategory ideaCategory) {
    return StringUtils.isNotBlank(ideaCategory.getCode())
        && ideaCategory.getCode().equalsIgnoreCase("new-ideas");
  }

  public boolean isNewIdeaGroup(IdeaCategory ideaCategory) {
    return StringUtils.isNotBlank(ideaCategory.getCode())
        && ideaCategory.getCode().matches("^new-ideas-ig-[0-9]{1,2}$");
  }

  public boolean isIdeaGroup(IdeaCategory ideaCategory) {
    return "IdeaGroup".equals(ideaCategory.getClassName());
  }

  public boolean isLastCategoryLevel(IdeaCategory ideaCategory) {
    return "IdeaGroup".equals(ideaCategory.getClassName());
  }
}
