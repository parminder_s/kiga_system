package com.kiga.ideas.web.response;

import com.kiga.main.locale.Locale;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * @author bbs
 * @since 09/06/17.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class IdeasResponseGroupWithParents extends IdeasResponseGroup {
  private List<IdeasResponseGroup> parents;
}
