package com.kiga.ideas.web.response;

import com.kiga.cms.web.response.ViewModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @author bbs
 * @since 3/25/16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
public class IdeaHierarchyViewModel extends ViewModel {
  private List<String> path;
  private List<Long> pathIds;
  private IdeasResponseCategory activeCategory;
  private List<IdeasResponseCategory> categories;
  private List<IdeasResponseParentCategory> parentCategories;
  private String firstLevelCategoryName;

  public List<Long> getPathIds() {
    return pathIds;
  }

  public void setPathIds(List<Long> pathIds) {
    this.pathIds = pathIds;
  }

  public List<String> getPath() {
    return path;
  }

  public void setPath(List<String> path) {
    this.path = path;
  }

  public List<IdeasResponseCategory> getCategories() {
    return categories;
  }

  public void setCategories(List<IdeasResponseCategory> categories) {
    this.categories = categories;
  }

  public IdeasResponseCategory getActiveCategory() {
    return activeCategory;
  }

  public void setActiveCategory(IdeasResponseCategory activeCategory) {
    this.activeCategory = activeCategory;
  }

  public List<IdeasResponseParentCategory> getParentCategories() {
    return parentCategories;
  }

  public void setParentCategories(List<IdeasResponseParentCategory> parentCategories) {
    this.parentCategories = parentCategories;
  }

  public String getFirstLevelCategoryName() {
    return firstLevelCategoryName;
  }

  public void setFirstLevelCategoryName(String firstLevelCategoryName) {
    this.firstLevelCategoryName = firstLevelCategoryName;
  }
}
