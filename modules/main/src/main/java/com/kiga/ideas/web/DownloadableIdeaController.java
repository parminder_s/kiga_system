package com.kiga.ideas.web;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.kiga.content.sorttree.SortTreeManager;
import com.kiga.ideas.web.request.ModifyDownloadableIdeaRequest;
import com.kiga.ideas.web.request.UrlSegmentValidationRequest;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.ideas.web.response.UrlSegmentValidationResponse;
import com.kiga.ideas.web.service.DownloadableIdeasService;
import com.kiga.integration.amazon.s3.config.AmazonS3Parameters;
import com.kiga.main.locale.Locale;
import com.kiga.search.services.IndexerService;
import com.kiga.util.BrowserPusherService;
import com.kiga.web.request.IdListRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 4/5/17.
 */
@RestController
@RequestMapping("idea/downloadable")
public class DownloadableIdeaController {
  private BrowserPusherService browserPusherService;
  private DownloadableIdeasService downloadableIdeasService;
  private AmazonS3Client amazonS3Client;
  private AmazonS3Parameters amazonS3Parameters;
  private IndexerService indexer;
  private SortTreeManager sortTreeManager;

  /**
   * Add dependencies.
   */
  @Autowired
  public DownloadableIdeaController(
    BrowserPusherService browserPusherService,
    DownloadableIdeasService downloadableIdeasService,
    AmazonS3Client amazonS3Client,
    AmazonS3Parameters amazonS3Parameters, IndexerService indexer,
    SortTreeManager sortTreeManager) {
    this.browserPusherService = browserPusherService;
    this.downloadableIdeasService = downloadableIdeasService;
    this.amazonS3Client = amazonS3Client;
    this.amazonS3Parameters = amazonS3Parameters;
    this.indexer = indexer;
    this.sortTreeManager = sortTreeManager;
  }

  @RequestMapping("add")
  public IdeasViewModelIdea add(
    @RequestBody ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest) {
    return downloadableIdeasService.add(modifyDownloadableIdeaRequest);
  }

  @RequestMapping("update")
  public IdeasViewModelIdea update(
    @RequestBody ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest) {
    return downloadableIdeasService.update(modifyDownloadableIdeaRequest);
  }

  @RequestMapping("remove")
  public void remove(@RequestBody IdListRequest idsToRemove) {
    downloadableIdeasService.remove(idsToRemove.getIds());
  }

  /**
   * Get downloadable kigaidea.
   *
   * @param ideaId ideaId
   * @return kigaidea
   */
  @RequestMapping("get/{ideaId:[0-9]+}")
  public IdeasViewModelIdea get(@PathVariable long ideaId) {
    return downloadableIdeasService.findById(ideaId);
  }

  /**
   * Download downloadable kigaidea.
   *
   * @param ideaId ideaId
   */
  @RequestMapping("download/{ideaId:[0-9]+}")
  public void download(@PathVariable long ideaId, HttpServletResponse response) throws IOException {
    String fileName = downloadableIdeasService.findUploadedFileByIdeaId(ideaId).getFileName();
    S3Object fileDownloaded = amazonS3Client
      .getObject(new GetObjectRequest(amazonS3Parameters.getBucketName(), fileName));
    String contentType = fileDownloaded.getObjectMetadata().getContentType();
    InputStream is = fileDownloaded.getObjectContent();
    browserPusherService.pushFile(response, is, fileName, contentType);
  }

  /**
   * List all downloadable kigaidea.
   *
   * @return kigaidea
   */
  @RequestMapping("list/{locale}")
  public List<IdeasViewModelIdea> list(@PathVariable("locale") String localeString) {
    Locale locale = Locale.valueOf(localeString);
    return downloadableIdeasService.getAll(locale);
  }

  /**
   * Publish ideas.
   *
   * @param idsToPublish ids of ideas to publish
   */
  @RequestMapping("publish")
  public void publish(@RequestBody IdListRequest idsToPublish) {
    downloadableIdeasService.publish(idsToPublish);
    indexer.indexIdeas(idsToPublish.getIds());
    sortTreeManager.recalculateSortTree();
  }

  /**
   * Unpublish ideas.
   *
   * @param idsToUnpublish ids of ideas to unpublish
   */
  @RequestMapping("unpublish")
  public void unpublish(@RequestBody IdListRequest idsToUnpublish) {
    downloadableIdeasService.unpublish(idsToUnpublish);
    idsToUnpublish.getIds().forEach(indexer::deleteIdeaFromIndex);
    sortTreeManager.recalculateSortTree();
  }

  /**
   * Validate url segment.
   *
   * @param urlSegmentValidationRequest properties to validate
   * @return validation response
   */
  @RequestMapping("validate")
  public UrlSegmentValidationResponse validate(
    @RequestBody UrlSegmentValidationRequest urlSegmentValidationRequest) {
    return downloadableIdeasService.validate(urlSegmentValidationRequest);
  }
}
