package com.kiga.ideas.service;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;


import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by rainerh on 01.12.16.
 */
public class IdeaPermissionService {
  private SecurityService securityService;

  public IdeaPermissionService(SecurityService securityService) {
    this.securityService = securityService;
  }

  /**
   * checks if the current user has access to an KigaIdea..
   */
  public boolean permissionGranted(KigaIdea kigaIdea) {
    if (kigaIdea.getFacebookShare() == 1) {
      return true;
    } else {
      Optional<Member> memberOptional = this.securityService.getSessionMember();
      boolean hasMember = memberOptional.isPresent();

      if (hasMember && securityService.getFromSession("is_crm").equals("0")) {
        return true;
      } else if (!hasMember) {
        return false;
      } else if (kigaIdea.getInPool() == 1
        && Boolean.valueOf(securityService.getFromSession("access_articles_pool")) == false) {
        return false;
      } else if (kigaIdea.getInPool() != 1
        && Boolean.valueOf(securityService.getFromSession("access_articles")) == false) {
        return false;
      } else {
        return true;
      }
    }
  }
}
