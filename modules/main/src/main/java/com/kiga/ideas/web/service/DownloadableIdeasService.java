package com.kiga.ideas.web.service;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.draft.IdeaCategoryDraft;
import com.kiga.content.domain.ss.draft.IdeaGroupDraft;
import com.kiga.content.domain.ss.draft.IdeaGroupToIdeaDraft;
import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.content.domain.ss.property.LayoutType;
import com.kiga.content.repository.ss.KigaPageImageRepository;
import com.kiga.content.repository.ss.draft.IdeaGroupRepository;
import com.kiga.content.repository.ss.draft.IdeaGroupToIdeaRepository;
import com.kiga.content.repository.ss.draft.SiteTreeRepository;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.service.SiteTreeTranslationGroupService;
import com.kiga.content.sorttree.SortTreeManager;
import com.kiga.idea.member.provider.MemberIdeaCategoryService;
import com.kiga.ideas.IdeaState;
import com.kiga.ideas.downloadable.repository.DownloadableIdeasRepositoryDraft;
import com.kiga.ideas.downloadable.repository.DownloadableIdeasRepositoryLive;
import com.kiga.ideas.web.request.ModifyDownloadableIdeaRequest;
import com.kiga.ideas.web.request.UrlSegmentValidationRequest;
import com.kiga.ideas.web.response.IdeasResponseGroup;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.ideas.web.response.UrlSegmentValidationResponse;
import com.kiga.main.locale.Locale;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.repository.ss.KigaS3FileRepository;
import com.kiga.s3.repository.ss.S3ImageRepository;
import com.kiga.s3.web.converter.KigaS3FileViewModelConverter;
import com.kiga.s3.web.response.KigaS3FileViewModel;
import com.kiga.search.services.IndexerService;
import com.kiga.web.request.IdListRequest;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author bbs
 * @since 4/7/17.
 */
@Service
public class DownloadableIdeasService {
  private SiteTreeRepository siteTreeRepository;
  private SiteTreeLiveRepository siteTreeRepositoryLive;
  private KigaS3FileViewModelConverter kigaS3FileViewModelConverter;
  private MemberIdeaCategoryService memberIdeaCategoryService;
  private DownloadableIdeasRepositoryLive downloadableIdeasRepositoryLive;
  private DownloadableIdeasRepositoryDraft downloadableIdeasRepositoryDraft;
  private IdeasResponseIdeaConverter ideasResponseIdeaConverter;
  private S3ImageRepository s3ImageRepository;
  private KigaS3FileRepository kigaS3FileRepository;
  private KigaPageImageRepository kigaPageImageRepository;
  private ModelMapper strictMatchingModelMapper;
  private IdeaGroupRepository ideaGroupRepositoryDraft;
  private IdeaGroupToIdeaRepository ideaGroupToIdeaRepository;
  private SiteTreeTranslationGroupService siteTreeTranslationGroupService;

  private IndexerService indexer;
  private SortTreeManager sortTreeManager;

  /** Constructor. */
  @Autowired
  public DownloadableIdeasService(
      SiteTreeRepository siteTreeRepository,
      SiteTreeLiveRepository siteTreeRepositoryLive,
      KigaS3FileViewModelConverter kigaS3FileViewModelConverter,
      MemberIdeaCategoryService memberIdeaCategoryService,
      DownloadableIdeasRepositoryLive downloadableIdeasRepositoryLive,
      DownloadableIdeasRepositoryDraft downloadableIdeasRepositoryDraft,
      IdeasResponseIdeaConverter ideasResponseIdeaConverter,
      S3ImageRepository s3ImageRepository,
      KigaS3FileRepository kigaS3FileRepository,
      KigaPageImageRepository kigaPageImageRepository,
      @Qualifier("strictMatchingModelMapper") ModelMapper strictMatchingModelMapper,
      IdeaGroupRepository ideaGroupRepositoryDraft,
      IdeaGroupToIdeaRepository ideaGroupToIdeaRepository,
      SiteTreeTranslationGroupService siteTreeTranslationGroupService,
      IndexerService indexer,
      SortTreeManager sortTreeManager) {
    this.siteTreeRepository = siteTreeRepository;
    this.siteTreeRepositoryLive = siteTreeRepositoryLive;
    this.kigaS3FileViewModelConverter = kigaS3FileViewModelConverter;
    this.memberIdeaCategoryService = memberIdeaCategoryService;
    this.downloadableIdeasRepositoryLive = downloadableIdeasRepositoryLive;
    this.downloadableIdeasRepositoryDraft = downloadableIdeasRepositoryDraft;
    this.ideasResponseIdeaConverter = ideasResponseIdeaConverter;
    this.s3ImageRepository = s3ImageRepository;
    this.kigaS3FileRepository = kigaS3FileRepository;
    this.kigaPageImageRepository = kigaPageImageRepository;
    this.strictMatchingModelMapper = strictMatchingModelMapper;
    this.ideaGroupRepositoryDraft = ideaGroupRepositoryDraft;
    this.ideaGroupToIdeaRepository = ideaGroupToIdeaRepository;
    this.siteTreeTranslationGroupService = siteTreeTranslationGroupService;
    this.indexer = indexer;
    this.sortTreeManager = sortTreeManager;
  }

  /** Get all ideas. */
  public List<IdeasViewModelIdea> getAll(Locale locale) {
    Map<Long, IdeasViewModelIdea> ideasViewModelIdeasLive =
        ideasResponseIdeaConverter
            .convertToResponse(downloadableIdeasRepositoryLive.findAllDownloadableIdeas(locale))
            .stream()
            .map(liveElement -> new AbstractMap.SimpleEntry<>(liveElement.getId(), liveElement))
            .collect(
                Collectors.toMap(
                    AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));

    List<IdeasViewModelIdea> ideasViewModelIdeas =
        ideasResponseIdeaConverter.convertToResponse(
            downloadableIdeasRepositoryDraft.findAllDownloadableIdeas(locale));

    ideasViewModelIdeas =
        ideasViewModelIdeas
            .stream()
            .map(
                draftElement -> {
                  IdeasViewModelIdea liveElement =
                      ideasViewModelIdeasLive.get(draftElement.getId());
                  if (liveElement != null
                      && liveElement.getLastEdited().compareTo(draftElement.getLastEdited()) >= 0) {
                    liveElement.setState(IdeaState.PUBLISHED);
                    ideasViewModelIdeasLive.remove(draftElement.getId());
                    return liveElement;
                  } else if (liveElement != null) {
                    liveElement.setState(IdeaState.UPDATED);
                    ideasViewModelIdeasLive.remove(draftElement.getId());
                    return liveElement;
                  }
                  draftElement.setState(IdeaState.UNPUBLISHED);
                  return draftElement;
                })
            .collect(Collectors.toList());

    final List<IdeasViewModelIdea> finalIdeasViewModelIdeas = ideasViewModelIdeas;
    ideasViewModelIdeasLive.forEach(
        (key, idea) -> {
          idea.setState(IdeaState.NOT_IN_DRAFT);
          finalIdeasViewModelIdeas.add(idea);
        });

    return ideasViewModelIdeas;
  }

  /**
   * Find by id.
   *
   * @param id id
   * @return idea
   */
  public IdeasViewModelIdea findById(long id) {
    KigaIdea downloadable = downloadableIdeasRepositoryLive.getOneDownloadable(id);
    if (downloadable == null) {
      downloadable = downloadableIdeasRepositoryDraft.getOneDownloadable(id);
    }
    return ideasResponseIdeaConverter.convertToResponse(downloadable);
  }

  public KigaS3FileViewModel findUploadedFileByIdeaId(long id) {
    KigaIdea oneDownloadable = downloadableIdeasRepositoryLive.getOneDownloadable(id);
    return kigaS3FileViewModelConverter.convertToResponse(oneDownloadable.getUploadedFile());
  }

  /**
   * Add downloadable idea.
   *
   * @param modifyDownloadableIdeaRequest modifyDownloadableIdeaRequest
   */
  public IdeasViewModelIdea add(ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest) {
    S3Image s3Image = s3ImageRepository.findOne(modifyDownloadableIdeaRequest.getPreviewFileId());
    KigaPageImage kigaPageImage = new KigaPageImage();
    kigaPageImage.setClassName("KigaPageImage");
    kigaPageImage.setS3Image(s3Image);
    kigaPageImage.setLocale(modifyDownloadableIdeaRequest.getLocale().getValue());
    kigaPageImageRepository.save(kigaPageImage);

    KigaIdeaDraft kigaIdea = new KigaIdeaDraft();
    kigaIdea.setPreviewImage(kigaPageImage);

    kigaIdea.setParent(siteTreeRepository.findOne(modifyDownloadableIdeaRequest.getCategoryId()));

    KigaS3File kigaS3File =
        kigaS3FileRepository.findOne(modifyDownloadableIdeaRequest.getUploadedFileId());
    kigaIdea.setUploadedFile(kigaS3File);

    strictMatchingModelMapper.map(modifyDownloadableIdeaRequest, kigaIdea);

    kigaIdea.setSort(0);
    kigaIdea.setVersion(1);
    kigaIdea.setLayoutType(LayoutType.DYNAMIC);
    kigaIdea.setArticleType(ArticleType.DOWNLOAD);
    kigaIdea.setClassName("Article");
    kigaIdea.setRatingComplete(1.0f);
    kigaIdea.setInPool(modifyDownloadableIdeaRequest.isInPool() ? 1 : 0);
    kigaIdea.setFacebookShare(modifyDownloadableIdeaRequest.isFacebookShare() ? 1 : 0);
    kigaIdea.setForParents(modifyDownloadableIdeaRequest.isForParents() ? 1 : 0);

    KigaIdeaDraft savedIdea = downloadableIdeasRepositoryDraft.save(kigaIdea);
    assignIdeaToGroups(modifyDownloadableIdeaRequest, savedIdea);
    assignIdeaToTranslationGroup(modifyDownloadableIdeaRequest, savedIdea);
    return ideasResponseIdeaConverter.convertToResponse(savedIdea);
  }

  /**
   * Update downloadable idea.
   *
   * @param modifyDownloadableIdeaRequest modifyDownloadableIdeaRequest
   */
  public IdeasViewModelIdea update(ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest) {
    KigaIdeaDraft kigaIdea =
        downloadableIdeasRepositoryDraft.findOne(modifyDownloadableIdeaRequest.getId());

    if (!modifyDownloadableIdeaRequest
        .getPreviewFileId()
        .equals(kigaIdea.getPreviewImage().getS3Image().getId())) {
      S3Image s3Image = s3ImageRepository.findOne(modifyDownloadableIdeaRequest.getPreviewFileId());
      KigaPageImage kigaPageImage = new KigaPageImage();
      kigaPageImage.setClassName("KigaPageImage");
      kigaPageImage.setS3Image(s3Image);
      kigaPageImage.setLocale(modifyDownloadableIdeaRequest.getLocale().getValue());
      kigaPageImageRepository.save(kigaPageImage);
      kigaIdea.setPreviewImage(kigaPageImage);
    }

    IdeaCategoryDraft ideaCategory =
        memberIdeaCategoryService.findOneDraft(modifyDownloadableIdeaRequest.getCategoryId());
    kigaIdea.setParent(ideaCategory);

    if (!modifyDownloadableIdeaRequest
        .getUploadedFileId()
        .equals(kigaIdea.getUploadedFile().getId())) {
      KigaS3File kigaS3File =
          kigaS3FileRepository.findOne(modifyDownloadableIdeaRequest.getUploadedFileId());
      kigaIdea.setUploadedFile(kigaS3File);
    }

    ideaGroupToIdeaRepository.delete(kigaIdea.getIdeaGroupToIdeaList());
    assignIdeaToGroups(modifyDownloadableIdeaRequest, kigaIdea);

    strictMatchingModelMapper.map(modifyDownloadableIdeaRequest, kigaIdea);

    kigaIdea.setInPool(modifyDownloadableIdeaRequest.isInPool() ? 1 : 0);
    kigaIdea.setFacebookShare(modifyDownloadableIdeaRequest.isFacebookShare() ? 1 : 0);
    kigaIdea.setForParents(modifyDownloadableIdeaRequest.isForParents() ? 1 : 0);

    KigaIdeaDraft savedIdea = downloadableIdeasRepositoryDraft.save(kigaIdea);
    return ideasResponseIdeaConverter.convertToResponse(savedIdea);
  }

  private void assignIdeaToTranslationGroup(
      ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest, KigaIdeaDraft kigaIdea) {
    if (modifyDownloadableIdeaRequest.getTranslationGroupId() == null) {
      // create new translation group
      siteTreeTranslationGroupService.createTranslationGroup(kigaIdea);
    } else {
      // add to translation group
      siteTreeTranslationGroupService.addToTranslationGroup(
          modifyDownloadableIdeaRequest.getTranslationGroupId(), kigaIdea);
    }
  }

  private void assignIdeaToGroups(
      ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest, KigaIdeaDraft kigaIdea) {
    List<IdeasResponseGroup> ideaGroups = modifyDownloadableIdeaRequest.getIdeaGroups();
    if (CollectionUtils.isNotEmpty(ideaGroups)) {
      Set<IdeaGroupToIdeaDraft> ideaGroupToIdeas = new HashSet<>();
      for (int i = 0; i < ideaGroups.size(); ++i) {
        IdeasResponseGroup ideasResponseGroup = ideaGroups.get(i);
        IdeaGroupDraft ideaGroup = ideaGroupRepositoryDraft.findOne(ideasResponseGroup.getId());
        IdeaGroupToIdeaDraft ideaGroupToIdea = new IdeaGroupToIdeaDraft();
        ideaGroupToIdea.setSort(i);
        ideaGroupToIdea.setArticle(kigaIdea);
        ideaGroupToIdea.setIdeaGroup(ideaGroup);
        ideaGroup.getIdeaGroupToIdeaList().add(ideaGroupToIdea);
        // todo ideaGroupToIdea.setSortOrder(???);
        // todo ideaGroupToIdea.setSortTree(???);
        ideaGroupToIdeas.add(ideaGroupToIdea);
      }
      kigaIdea.setIdeaGroupToIdeaList(ideaGroupToIdeas);
      ideaGroupToIdeaRepository.save(ideaGroupToIdeas);
    }
  }

  /**
   * Publishes this ideas.
   *
   * @param idsToPublish id of DownloadableIds to publish.
   */
  public void publish(IdListRequest idsToPublish) {
    idsToPublish.getIds().stream().forEach(this::publish);
    indexer.indexIdeas(idsToPublish.getIds());
    sortTreeManager.recalculateSortTree();
  }

  private void publish(Long idToPublish) {
    KigaIdeaDraft kigaIdeaDraft = downloadableIdeasRepositoryDraft.findOne(idToPublish);
    KigaIdeaLive kigaIdeaLive = strictMatchingModelMapper.map(kigaIdeaDraft, KigaIdeaLive.class);
    kigaIdeaLive.setParent(siteTreeRepositoryLive.findOne(kigaIdeaDraft.getParent().getId()));
    downloadableIdeasRepositoryLive.save(kigaIdeaLive);
  }

  public void unpublish(IdListRequest idsToUnpublish) {
    idsToUnpublish.getIds().stream().forEach(downloadableIdeasRepositoryLive::delete);
  }

  public void remove(List<Long> idsToRemove) {
    idsToRemove.stream().forEach(downloadableIdeasRepositoryDraft::delete);
  }

  /**
   * Validate url segment.
   *
   * @param urlSegmentValidationRequest urlSegmentValidationRequest
   * @return urlSegmentValidationResponse
   */
  public UrlSegmentValidationResponse validate(
      UrlSegmentValidationRequest urlSegmentValidationRequest) {
    UrlSegmentValidationResponse response = new UrlSegmentValidationResponse();
    response.setValid(true);

    String urlSegment = urlSegmentValidationRequest.getUrlSegment();

    if (urlSegment == null) {
      return response;
    }

    if (urlSegmentValidationRequest.getCategoryId() == 0) {
      response.setMessage("IDEAS_DOWNLOAD_IDEA_VALIDATION_URL_SEGMENT_MISSING_CATEGORY");
      response.setValid(false);
    } else if (!Pattern.matches("^[A-Za-z0-9\\-]+$", urlSegment)) {
      response.setMessage("IDEAS_DOWNLOAD_IDEA_VALIDATION_URL_SEGMENT_NOT_VALID");
      response.setValid(false);
    } else {
      boolean isUniquePerCategory =
          siteTreeRepository.findByLocaleAndUrlSegmentAndParentIdAndIdNot(
                  urlSegmentValidationRequest.getLocale(),
                  urlSegment,
                  urlSegmentValidationRequest.getCategoryId(),
                  urlSegmentValidationRequest.getId())
              == null;

      if (!isUniquePerCategory) {
        response.setMessage("IDEAS_DOWNLOAD_IDEA_VALIDATION_URL_SEGMENT_NOT_UNIQUE");
        response.setValid(false);
      }
    }

    return response;
  }
}
