package com.kiga.ideas.service;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.KigaIdeaRepositoryProxy;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.properties.IdeasRetrieveProperties;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.main.locale.Locale;
import com.kiga.web.converter.ResponseConverter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;


import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 5/13/16.
 */
public class IdeaGroupToIdeaService {
  private KigaIdeaRepositoryProxy kigaIdeaRepositoryProxy;
  private ResponseConverter<KigaIdea, IdeasViewModelIdea> ideasResponseIdeaConverter;
  private IdeasProperties ideasProperties;
  private IdeaCategorySortToDbSort ideaCategorySortToDbSort;
  private IdeaCategoryRepositoryProxy ideaCategoryModel;

  /**
   * inject dependencies.
   */
  public IdeaGroupToIdeaService(
    KigaIdeaRepositoryProxy kigaIdeaRepositoryProxy,
    ResponseConverter<KigaIdea, IdeasViewModelIdea> ideasResponseIdeaConverter,
    IdeasProperties ideasProperties, IdeaCategorySortToDbSort ideaCategorySortToDbSort,
    IdeaCategoryRepositoryProxy ideaCategoryModel) {
    this.kigaIdeaRepositoryProxy = kigaIdeaRepositoryProxy;
    this.ideasResponseIdeaConverter = ideasResponseIdeaConverter;
    this.ideasProperties = ideasProperties;
    this.ideaCategorySortToDbSort = ideaCategorySortToDbSort;
    this.ideaCategoryModel = ideaCategoryModel;
  }

  /**
   * Get all ideas based on provided properties.
   *
   * @param ideasRetrieveProperties properties
   * @return ideas
   */
  public List<IdeasViewModelIdea> getAllIdeas(IdeasRetrieveProperties ideasRetrieveProperties) {
    final Long id = ideasRetrieveProperties.getGroup().getId();
    IdeaCategory ideaCategory = ideaCategoryModel.findById(id).get(0);
    final Sort sort = ideaCategorySortToDbSort.mapSort(ideaCategory);
    return processIdeas(getAllForGroup(ideasRetrieveProperties, sort));
  }

  protected List<KigaIdea> getAllForGroup(IdeasRetrieveProperties ideasRetrieveProperties,
    Sort sort) {
    final Long id = ideasRetrieveProperties.getGroup().getId();
    final int ageGroupFilter = ideasRetrieveProperties.getAgeGroupFilter();
    return kigaIdeaRepositoryProxy.findAllForGroup(id, ageGroupFilter, sort);
  }

  /**
   * Get paged ideas based on provided properties.
   *
   * @param ideasRetrieveProperties properties
   * @return ideas
   */
  public List<IdeasViewModelIdea> getPagedIdeas(IdeasRetrieveProperties ideasRetrieveProperties) {
    final Long id = ideasRetrieveProperties.getGroup().getId();
    IdeaCategory ideaCategory = ideaCategoryModel.findById(id).get(0);
    Pageable pageable = new PageRequest(0, ideasProperties.getAmountPerCategory(),
      ideaCategorySortToDbSort.mapSort(ideaCategory));
    List<KigaIdea> entitiesList = getPagedForGroup(ideasRetrieveProperties, pageable);

    return entitiesList.stream().map(ideasResponseIdeaConverter::convertToResponse)
      .collect(Collectors.toList());
  }

  protected List<KigaIdea> getPagedForGroup(IdeasRetrieveProperties ideasRetrieveProperties,
    Pageable pageable) {
    final Locale locale = ideasRetrieveProperties.getLocale();
    final Long id = ideasRetrieveProperties.getGroup().getId();
    final Integer level = ideasRetrieveProperties.getLevel();
    final int ageGroupFilter = ideasRetrieveProperties.getAgeGroupFilter();
    final String sortTree = ideasRetrieveProperties.getSortTree();

    if (ideasProperties.isUseSortTree()) {
      return kigaIdeaRepositoryProxy
        .findPagedForGroupSortTree(locale, sortTree, ageGroupFilter, pageable);
    } else {
      return kigaIdeaRepositoryProxy
        .findPagedForGroup(locale, id, level, ageGroupFilter, pageable);
    }

  }

  private List<IdeasViewModelIdea> processIdeas(List<KigaIdea> kigaIdeas) {
    return kigaIdeas.stream()
      .map(ideasResponseIdeaConverter::convertToResponse)
      .collect(Collectors.toList());
  }

  /**
   * check if group is not empty.
   */
  public boolean isGroupNonEmpty(IdeasRetrieveProperties ideasRetrieveProperties) {
    final Locale locale = ideasRetrieveProperties.getLocale();
    final Long id = ideasRetrieveProperties.getGroup().getId();
    final Integer level = ideasRetrieveProperties.getLevel();
    final int ageGroupFilter = ideasRetrieveProperties.getAgeGroupFilter();
    final String sortTree = ideasRetrieveProperties.getSortTree();
    final int exists = kigaIdeaRepositoryProxy.getGroupCount(locale, sortTree,
      ageGroupFilter);
    return exists > 0;
  }

}
