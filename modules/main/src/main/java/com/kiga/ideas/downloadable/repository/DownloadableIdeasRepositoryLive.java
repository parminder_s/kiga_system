package com.kiga.ideas.downloadable.repository;

import com.kiga.content.domain.ss.live.KigaIdeaLive;
import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 4/7/17.
 */
public interface DownloadableIdeasRepositoryLive
  extends CrudRepository<KigaIdeaLive, Long>, DownloadableIdeasRepository {
}
