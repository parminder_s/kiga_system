package com.kiga.ideas.web.viewmodel.kigaidea;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.LinkedKigaPageGroup;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.web.response.IdeasResponseLinkedIdea;
import com.kiga.ideas.web.response.KigaIdeaViewModel;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asv on 06.01.17.
 */
public class LinkedIdeasSetter {
  private SiteTreeUrlGenerator urlGenerator;
  private IdeasProperties ideasProperties;
  private Logger logger = LoggerFactory.getLogger(getClass());
  private S3ImageResizeService s3ImageResizeService;

  /**
   * Constructor.
   */
  public LinkedIdeasSetter(IdeasProperties ideasProperties,
                           S3ImageResizeService s3ImageResizeService,
                           SiteTreeUrlGenerator urlGenerator) {
    this.ideasProperties = ideasProperties;
    this.s3ImageResizeService = s3ImageResizeService;
    this.urlGenerator = urlGenerator;
  }

  /**
   * Set Linked ideas.
   */
  public void setLinkedIdeas(KigaIdeaViewModel kigaIdeaViewModel,
                              List<LinkedKigaPageGroup> linkedKigaPageGroups) {
    ArrayList<IdeasResponseLinkedIdea> list = new ArrayList<>();
    for (LinkedKigaPageGroup linkedKigaPageGroup : linkedKigaPageGroups) {
      List<KigaIdea> kigaPages = linkedKigaPageGroup.getKigaPages();
      for (KigaIdea linkedIdea : kigaPages) {
        if (!kigaIdeaViewModel.getId().equals(linkedIdea.getId())) {
          IdeasResponseLinkedIdea idea = new IdeasResponseLinkedIdea();
          idea.setId(linkedIdea.getId());
          setLinkedIdeaPreviewImage(idea, linkedIdea.getPreviewImage());
          idea.setTitle(linkedIdea.getTitle());
          idea.setDescription(linkedIdea.getDescription());
          idea.setUrl(urlGenerator.getRelativeNgUrl(linkedIdea));
          list.add(idea);
        }
      }
    }
    kigaIdeaViewModel.setLinkedIdeas(list);
  }

  private void setLinkedIdeaPreviewImage(IdeasResponseLinkedIdea ideasResponseLinkedIdea,
                                         KigaPageImage previewImage) {
    if (previewImage == null) {
      ideasResponseLinkedIdea.setLargePreviewImageUrl("");
    } else {
      S3Image s3Image = previewImage.getS3Image();
      try {
        ImageSize smallPreview = ideasProperties.getPreviewImage().getSmall();
        ideasResponseLinkedIdea.setPreviewUrl(
          s3ImageResizeService.getResizedImageUrl(s3Image,
            new PaddedImageStrategy(smallPreview.getWidth(), smallPreview.getHeight())));
      } catch (IOException ex) {
        logger.warn("Problem with cached (or caching) size variants of image: " + s3Image
          .getName(), ex);
      } catch (IllegalArgumentException ex) {
        logger.warn("Wrong arguments passed to the strategy class", ex);
      }
    }
  }

}

