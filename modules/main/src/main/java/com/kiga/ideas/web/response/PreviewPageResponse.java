package com.kiga.ideas.web.response;

import lombok.Data;

/**
 * @author bbs
 * @since 5/31/16.
 */
@Data
public class PreviewPageResponse {
  private String small;
  private String large;
  private int pageNumber;
}
