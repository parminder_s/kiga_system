package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import com.kiga.cms.web.response.Breadcrumb;
import com.kiga.cms.web.response.Metadata;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.Page;
import com.kiga.content.domain.ss.SiteTree;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 23.01.18
 */
@Service
public class ViewModelMetaDataProvider {

  /** fill given viewModel with Metadata and Breadcrumbs from entity, also set resolvedType. */
  public void fillWithMetaDataAndBreadcrumbs(ViewModel viewModel, Page entity) {
    List<Breadcrumb> breadcrumbs = buildBreadcrumbs(entity);

    Metadata metadata = buildMetadata(entity);
    viewModel.setBreadcrumbs(breadcrumbs);
    viewModel.setMetadata(metadata);
    viewModel.setResolvedType(entity.getClassName());
    viewModel.setId(entity.getId());
  }

  private List<Breadcrumb> buildBreadcrumbs(Page entity) {
    SiteTree parent = entity;
    List<Breadcrumb> breadcrumbs = new ArrayList<>();
    while (parent != null) {
      breadcrumbs.add(new Breadcrumb(parent.getTitle(), parent.getUrlSegment()));
      parent = parent.getParent();
    }
    if (CollectionUtils.isNotEmpty(breadcrumbs)) {
      if (breadcrumbs.size() > 2) {
        Collections.reverse(breadcrumbs);
        breadcrumbs = breadcrumbs.stream().skip(2).collect(Collectors.toList());
      } else {
        breadcrumbs = new ArrayList<>();
      }
    }
    return breadcrumbs;
  }

  private Metadata buildMetadata(Page entity) {
    Metadata metadata = new Metadata();

    metadata.setTitle(entity.getTitle());
    metadata.setDescription(entity.getMetaDescription());
    metadata.setKeywords(entity.getMetaKeywords());

    return metadata;
  }
}
