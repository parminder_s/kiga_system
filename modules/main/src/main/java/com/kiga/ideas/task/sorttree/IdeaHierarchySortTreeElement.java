package com.kiga.ideas.task.sorttree;

import com.kiga.content.sorttree.spec.SortTree;
import com.kiga.content.sorttree.spec.SortTreeElement;
import lombok.Data;


import java.util.Collections;
import java.util.List;

/**
 * Created by rainerh on 29.03.17.
 */

@Data
public class IdeaHierarchySortTreeElement implements SortTreeElement {

  private List<SortTreeElement> children;
  private SortTree sortTree;

  IdeaHierarchySortTreeElement() {
    this.children = Collections.emptyList();
  }

}
