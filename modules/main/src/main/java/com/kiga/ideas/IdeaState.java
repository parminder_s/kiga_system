package com.kiga.ideas;

/**
 * Created by robert on 13.08.17.
 */
public enum IdeaState {
  PUBLISHED,
  UNPUBLISHED,
  UPDATED,
  NOT_IN_DRAFT
}
