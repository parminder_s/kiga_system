package com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Florian Schneider
 * @since 10.01.17
 */
@Data
@Builder
@AllArgsConstructor
public class ParentCategoryViewModel { // IdeasResponseParentCategory
  private String name;
  private String title;
  private boolean disableForFavourites;

  @Builder.Default private String icon = "icon-ideas";

  private boolean ageGroup;
  private boolean ageGroupContainer;

  @JsonIgnore private String code;

  /** no args constructor setting default values. */
  public ParentCategoryViewModel() {
    this.icon = "icon-ideas";
  }

  /**
   * setter for code, additionally sets indicator of ageGroup (and agrGroupContainer) if it's one of
   * these types.
   */
  public void setCode(String code) {
    this.code = code;
    ageGroupContainer = false;
    ageGroup = false;

    if (StringUtils.isNotBlank(code)) {
      ageGroupContainer = code.equalsIgnoreCase("new-ideas");
      ageGroup = code.matches("^new-ideas-ig-[0-9]{1,2}$");
    }
  }
}
