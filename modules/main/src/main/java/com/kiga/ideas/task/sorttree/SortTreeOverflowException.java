package com.kiga.ideas.task.sorttree;

/**
 * Created by faxxe on 6/13/17.
 */
public class SortTreeOverflowException extends RuntimeException {
  public SortTreeOverflowException() {
    super("Overflow @ calculating the sorttree");
  }
}
