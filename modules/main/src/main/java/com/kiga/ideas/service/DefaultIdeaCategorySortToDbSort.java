package com.kiga.ideas.service;

import static com.kiga.ideas.service.DbSortValues.sortDate;
import static com.kiga.ideas.service.DbSortValues.sortSequence;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.property.SortStrategy;
import com.kiga.spec.Fetcher;
import org.springframework.data.domain.Sort;

/**
 * Created by rainerh on 09.09.16.
 */
public class DefaultIdeaCategorySortToDbSort implements IdeaCategorySortToDbSort {
  @Override
  public Sort mapSort(IdeaCategory ideaCategory) {
    return mapSort(getCategoryWithoutInherited(ideaCategory));
  }

  private Sort mapSort(SortStrategy sortStrategy) {
    if (sortStrategy.equals(SortStrategy.SEQUENCE)) {
      return sortSequence;
    } else {
      return sortDate;
    }
  }

  private SortStrategy getCategoryWithoutInherited(IdeaCategory ideaCategory) {
    while (ideaCategory.getSortStrategy().equals(SortStrategy.INHERITED)) {
      ideaCategory = (IdeaGroupContainer)ideaCategory.getParent();
    }

    return ideaCategory.getSortStrategy();
  }
}
