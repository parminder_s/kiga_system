package com.kiga.ideas;

import com.kiga.integration.amazon.s3.config.AmazonS3Parameters;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author bbs
 * @since 5/8/16.
 */
@Component
@ConfigurationProperties("ideas.amazon.s3")
public class IdeasAmazonS3Parameters extends AmazonS3Parameters {
}
