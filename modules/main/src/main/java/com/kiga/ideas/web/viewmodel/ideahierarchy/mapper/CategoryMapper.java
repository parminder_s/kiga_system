package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.ideas.web.viewmodel.ideahierarchy.helper.IdeaCategoryHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.base.GroupViewModelMapper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 09.01.2018.
 */
@Service
public class CategoryMapper {
  private IdeaCategoryHelper ideaCategoryHelper;

  @Autowired
  public CategoryMapper(IdeaCategoryHelper ideaCategoryHelper) {
    this.ideaCategoryHelper = ideaCategoryHelper;
  }

  /** converts IdeaCategory to Category. */
  public Category convertIdeaCategory(IdeaCategory ideaCategory) {

    Category groupViewModel = null;

    if (ideaCategory != null) {
      groupViewModel = GroupViewModelMapper.INSTANCE.ideaCategoryToGroupViewModel(ideaCategory);
      groupViewModel.setLastCategoryLevel(ideaCategoryHelper.isLastCategoryLevel(ideaCategory));
      groupViewModel.setIcon(ideaCategoryHelper.getIconCode(ideaCategory));
    }

    return groupViewModel;
  }
}
