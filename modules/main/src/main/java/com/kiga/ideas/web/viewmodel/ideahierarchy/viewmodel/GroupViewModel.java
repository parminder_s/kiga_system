package com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel;

import com.kiga.main.locale.Locale;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Florian Schneider
 * @since 10.01.17
 */
@Data
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class GroupViewModel extends CategoryViewModel { // IdeasResponseGroup
  private boolean ideaGroup;
  private Locale locale;
  private String urlSegment;
  private List<String> urlSegmentsPath;
  private String code;
  private boolean ageGroup;
  private boolean ageGroupContainer;
  private boolean disableForFavourites;

  /**
   * setter for code, additionally sets indicator of ageGroup (and agrGroupContainer) if it's one of
   * these types.
   */
  public void setCode(String code) {
    this.code = code;
    ageGroupContainer = false;
    ageGroup = false;

    if (StringUtils.isNotBlank(code)) {
      ageGroupContainer = code.equalsIgnoreCase("new-ideas");
      ageGroup = code.matches("^new-ideas-ig-[0-9]{1,2}$");
    }
  }

  /** Getter for age group value - it's calculated from the code if this group is an ageGroup. */
  public Integer calculateAgeGroupValue() {
    if (ageGroup) {
      String[] codeParts = code.split("-");
      return Integer.valueOf(codeParts[codeParts.length - 1]);
    }

    return null;
  }
}
