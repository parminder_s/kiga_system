package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.handler;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.content.domain.ss.property.LayoutType;
import com.kiga.ideas.web.response.KigaIdeaViewModel;
import com.kiga.ideas.web.viewmodel.kigaidea.FirstRowRemover;
import com.kiga.ideas.web.viewmodel.kigaidea.LinksReplacer;
import com.kiga.ideas.web.viewmodel.kigaidea.MediaLinksConverter;
import com.kiga.ideas.web.viewmodel.kigaidea.PinterestTagAdder;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KigaIdeaViewModelArticleTypeHandler {

  private LinksReplacer linksReplacer;

  @Autowired
  public KigaIdeaViewModelArticleTypeHandler(LinksReplacer linksReplacer) {
    this.linksReplacer = linksReplacer;
  }

  /** fills fields for kigaIdeaViewModel depending on kigaIdea's ArticleType. */
  public void handleArticleType(KigaIdeaViewModel kigaIdeaViewModel, KigaIdea kigaIdea) {
    if (ArticleType.DOWNLOAD.equals(kigaIdea.getArticleType())) {
      kigaIdeaViewModel.setUploadedFileName(kigaIdea.getUploadedFile().getName());
      kigaIdeaViewModel.setUploadedFileId(kigaIdea.getUploadedFile().getId());
      kigaIdeaViewModel.setContent(
          "<div class=\"container\">\n"
              + "<div class=\"rowDiv\">\n"
              + "<div class=\"span-4 last activeCell\" style=\"text-align:center;\">"
              + "<button  t=\"BUTTON_DOWNLOAD\" type=\"button\""
              + " ng-click=\"ctrl.downloadIdea()\">"
              + "</button>"
              + "</div></div></div>");
    } else {
      String ideaContent = linksReplacer.analyzeContentLinks(kigaIdea.getContent());
      MediaLinksConverter mediaLinksConverter = new MediaLinksConverter();
      ideaContent = mediaLinksConverter.convertMediaLinks(ideaContent);
      PinterestTagAdder pinterestTagAdder = new PinterestTagAdder();
      ideaContent =
          pinterestTagAdder.analyzeContentImages(
              ideaContent,
              ((List<KigaPageImage>) kigaIdea.getKigaPageImages())
                  .stream()
                  .map(KigaPageImage::getS3Image)
                  .filter(Objects::nonNull)
                  .collect(Collectors.toList()));
      if (kigaIdea.getLayoutType() == LayoutType.DYNAMIC) {
        kigaIdeaViewModel.setContent((new FirstRowRemover()).analyzeContent(ideaContent));
      } else {
        kigaIdeaViewModel.setContent(ideaContent);
      }
    }
  }
}
