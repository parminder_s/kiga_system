package com.kiga.ideas.properties;

import com.kiga.ideas.web.response.IdeasResponseGroup;
import com.kiga.main.locale.Locale;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bbs
 * @since 11/7/16.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IdeasRetrieveProperties {
  private Locale locale;
  private IdeasResponseGroup group;
  private Integer level;
  private Integer ageGroupFilter;
  private boolean showAll;
  private String sortTree;
}
