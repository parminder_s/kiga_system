package com.kiga.ideas.web.request;

import com.kiga.web.message.EndpointRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by robert klodzinski on 01.04.16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class IdeasGroupListRequest extends EndpointRequest {
  private String urlSegment;
  private Integer level;
  private int ageGroupFilter;
}
