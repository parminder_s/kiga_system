package com.kiga.ideas.web.viewmodel.ideahierarchy.query.model;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import java.util.List;
import lombok.Builder;
import lombok.Data;

/**
 * @author Florian Schneider
 * @since 10.01.2018
 */
@Data
@Builder
public class IdeaSet {
  private IdeaCategory ideaCategory;
  private List<KigaIdea> ideas;
}
