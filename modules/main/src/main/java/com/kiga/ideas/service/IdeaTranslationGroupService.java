package com.kiga.ideas.service;

import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.domain.ss.draft.SiteTreeDraft;
import com.kiga.content.domain.ss.draft.SiteTreeTranslationGroups;
import com.kiga.content.repository.ss.draft.KigaIdeaRepositoryDraft;
import com.kiga.content.repository.ss.draft.SiteTreeTranslationGroupsRepository;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.web.response.IdeaTranslationGroup;
import com.kiga.ideas.web.response.IdeasResponseGroup;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.ideas.web.service.IdeasResponseIdeaConverter;
import com.kiga.ideas.web.service.IdeasSeparateAgeGroupsConverter;
import com.kiga.main.locale.Locale;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by robert on 29.08.17.
 */
@Service
public class IdeaTranslationGroupService {
  private SiteTreeTranslationGroupsRepository siteTreeTranslationGroupsRepository;
  private KigaIdeaRepositoryDraft kigaIdeaRepositoryDraft;
  private IdeasResponseIdeaConverter ideasResponseIdeaConverter;
  private CategoryTranslationService categoryTranslationService;
  private S3ImageResizeService s3ImageResizeService;
  private IdeasProperties ideasProperties;

  /**
   * Dependencies.
   *
   * @param siteTreeTranslationGroupsRepository siteTreeTranslationGroupsRepository
   * @param kigaIdeaRepositoryDraft             kigaIdeaRepositoryDraft
   * @param ideasResponseIdeaConverter          ideasResponseIdeaConverter
   * @param categoryTranslationService          categoryTranslationService
   * @param s3ImageResizeService                s3ImageResizeService
   * @param ideasProperties                     ideasProperties
   */
  @Autowired
  public IdeaTranslationGroupService(
    SiteTreeTranslationGroupsRepository siteTreeTranslationGroupsRepository,
    KigaIdeaRepositoryDraft kigaIdeaRepositoryDraft,
    IdeasResponseIdeaConverter ideasResponseIdeaConverter,
    CategoryTranslationService categoryTranslationService,
    S3ImageResizeService s3ImageResizeService, IdeasProperties ideasProperties) {
    this.siteTreeTranslationGroupsRepository = siteTreeTranslationGroupsRepository;
    this.kigaIdeaRepositoryDraft = kigaIdeaRepositoryDraft;
    this.ideasResponseIdeaConverter = ideasResponseIdeaConverter;
    this.categoryTranslationService = categoryTranslationService;
    this.s3ImageResizeService = s3ImageResizeService;
    this.ideasProperties = ideasProperties;
  }

  /**
   * Gets translation group with all ideas from a translation groups to which an idea of given id
   * belongs.
   *
   * @param id id of the idea
   * @return list of ideas belonging to one translation group
   */
  public IdeaTranslationGroup getByIdeaId(long id) {
    SiteTreeTranslationGroups translationGroup =
      siteTreeTranslationGroupsRepository.findByOriginalId(id);

    return getByTranslationGroupId(translationGroup.getTranslationGroupId());
  }

  /**
   * Gets translation group with all its ideas.
   *
   * @param translationGroupId id of the translation group
   * @return list of ideas belonging to the translation group
   */
  public IdeaTranslationGroup getByTranslationGroupId(long translationGroupId) {
    List<Long> idsList = siteTreeTranslationGroupsRepository
      .findAllByTranslationGroupId(translationGroupId)
      .stream()
      .map(SiteTreeTranslationGroups::getOriginalId)
      .collect(Collectors.toList());

    List<IdeasViewModelIdea> translationGroupIdeas =
      ideasResponseIdeaConverter.convertToResponse(kigaIdeaRepositoryDraft.findAll(idsList));

    IdeaTranslationGroup returner = new IdeaTranslationGroup();
    returner.setIdeas(translationGroupIdeas);
    returner.setTranslationGroupId(translationGroupId);

    return returner;
  }

  /**
   * Takes template idea (existing translation of an idea) and builds new idea to be translated to
   * another language. Since we are going to have translated idea, we expect to have some properties
   * cloned into the new object and therefore some of them are simply copied, and some of them
   * needs to be translated.
   *
   * @param id     id of idea to be translated
   * @param locale locale to which the idea will be translated
   * @return prepared idea for translation
   */
  public IdeasViewModelIdea prepareIdeaForTranslation(long id, Locale locale) throws IOException {
    KigaIdeaDraft templateIdea = kigaIdeaRepositoryDraft.findOne(id);
    IdeasViewModelIdea returner = new IdeasViewModelIdea();

    returner.setSortMonth(templateIdea.getSortMonth());
    returner.setSortYear(templateIdea.getSortYear());

    returner.setSortDayMini(templateIdea.getSortDayMini());
    returner.setSortMonthMini(templateIdea.getSortMonthMini());
    returner.setSortYearMini(templateIdea.getSortYearMini());

    S3Image s3Image = templateIdea.getPreviewImage().getS3Image();
    returner.setPreviewFileId(s3Image.getId());

    ImageSize largePreview = ideasProperties.getPreviewImage().getLarge();
    String largePreviewImageUrl = s3ImageResizeService.getResizedImageUrl(s3Image,
      new PaddedImageStrategy(largePreview.getWidth(), largePreview.getHeight()));

    returner.setLargePreviewImageUrl(largePreviewImageUrl);
    returner.setPreviewName(s3Image.getName());
    returner.setPreviewSize(s3Image.getSize());

    KigaS3File uploadedFile = templateIdea.getUploadedFile();
    returner.setUploadedFileId(uploadedFile.getId());
    returner.setFileName(uploadedFile.getName());
    returner.setFileSize(uploadedFile.getSize());

    IdeasSeparateAgeGroupsConverter ideasSeparateAgeGroupsConverter =
      new IdeasSeparateAgeGroupsConverter();
    List<Integer> separatedAgeGroups =
      ideasSeparateAgeGroupsConverter.convert(templateIdea.getAgeGroup());

    returner.setFacebookShare(BooleanUtils.toBoolean(templateIdea.getFacebookShare()));
    returner.setSeparatedAgeGroups(separatedAgeGroups);
    returner.setForParents(BooleanUtils.toBoolean(templateIdea.getForParents()));
    returner.setInPool(BooleanUtils.toBoolean(templateIdea.getInPool()));

    SiteTreeDraft parent = templateIdea.getParent();
    if (parent != null) {
      IdeasResponseGroup translatedCategory =
        categoryTranslationService.translate(parent.getId(), locale);
      returner.setCategoryId(translatedCategory.getId());
    }

    return returner;
  }
}
