package com.kiga.ideas.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by rainerh on 12.06.16.
 */
@Configuration
@ConfigurationProperties("ideas.printPreviewImage")
public class PrintPreviewImage extends PreviewImage {
}
