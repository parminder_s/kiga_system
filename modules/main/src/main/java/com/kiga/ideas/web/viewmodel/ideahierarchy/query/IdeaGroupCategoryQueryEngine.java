package com.kiga.ideas.web.viewmodel.ideahierarchy.query;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.ideas.web.viewmodel.ideahierarchy.helper.IdeaCategoryHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper.IdeaSiblingHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaGroupCategoryQueryResult;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaSet;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.IdeaQueryEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.NewIdeaBlockViewEngine;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 09.01.17.
 */
@Service
@Log4j2
public class IdeaGroupCategoryQueryEngine {
  private IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxy;
  private IdeaQueryEngine ideaQueryEngine;
  private NewIdeaBlockViewEngine newIdeaBlockViewEngine;
  private IdeaSiblingHelper ideaSiblingHelper;
  private IdeaCategoryHelper ideaCategoryHelper;

  /** Constructor. */
  @Autowired
  public IdeaGroupCategoryQueryEngine(
      NewIdeaBlockViewEngine newIdeaBlockViewEngine,
      IdeaSiblingHelper ideaSiblingHelper,
      IdeaCategoryHelper ideaCategoryHelper,
      IdeaQueryEngine ideaQueryEngine,
      IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxy) {
    this.newIdeaBlockViewEngine = newIdeaBlockViewEngine;
    this.ideaSiblingHelper = ideaSiblingHelper;
    this.ideaCategoryHelper = ideaCategoryHelper;
    this.ideaQueryEngine = ideaQueryEngine;
    this.ideaCategoryRepositoryProxy = ideaCategoryRepositoryProxy;
  }

  /** query Ideas / IdeaSets for IdeaCategories. */
  public IdeaGroupCategoryQueryResult query(IdeaQueryParameter ideaQueryParameter) {

    return IdeaGroupCategoryQueryResult.builder()
        .ideaSets(getIdeaSets(ideaQueryParameter))
        .siblings(ideaSiblingHelper.getSiblings(ideaQueryParameter))
        .build();
  }

  private List<IdeaSet> getIdeaSets(IdeaQueryParameter ideaQueryParameter) {
    List<IdeaCategory> categories = getCategories(ideaQueryParameter.getIdeaCategory());

    // when blockview is needed then return all new ideas in one category
    if (ideaCategoryHelper.isNewIdeaGroupCategory(ideaQueryParameter.getIdeaCategory())) {
      IdeaQueryParameter categoryIdeaQueryParameter =
          IdeaQueryParameter.builder()
              .locale(ideaQueryParameter.getLocale())
              .ageGroupFilter(ideaQueryParameter.getAgeGroupFilter())
              .member(ideaQueryParameter.getMember())
              .build();

      return Collections.singletonList(
          IdeaSet.builder()
              .ideaCategory(ideaQueryParameter.getIdeaCategory())
              .ideas(newIdeaBlockViewEngine.getIdeas(categoryIdeaQueryParameter))
              .build());

    } else {
      return categories
          .stream()
          .map(
              category -> {
                IdeaQueryParameter categoryIdeaQueryParameter =
                    IdeaQueryParameter.builder()
                        .locale(category.getLocale())
                        .member(ideaQueryParameter.getMember())
                        .ageGroupFilter(ideaQueryParameter.getAgeGroupFilter())
                        .ideaCategory(category)
                        .build();

                return IdeaSet.builder()
                    .ideaCategory(category)
                    .ideas(ideaQueryEngine.getIdeas(categoryIdeaQueryParameter))
                    .build();
              })
          .filter(this::filterEmptyIdeaSets)
          .collect(Collectors.toList());
    }
  }

  /** Get categories for parent. */
  private List<IdeaCategory> getCategories(IdeaCategory parentGroup) {
    return ideaCategoryRepositoryProxy.findBySortTreeStartsWithAndLocaleLikeOrderBySortAsc(
        parentGroup.getSortTree(),
        parentGroup.getLocale()); // possibly needs filter for class name?
  }

  private boolean filterEmptyIdeaSets(IdeaSet ideaSet) {
    return ideaSet.getIdeas() != null && ideaSet.getIdeas().size() > 0;
  }
}
