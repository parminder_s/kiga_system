package com.kiga.ideas.task.cache;

import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepositoryProxy;
import com.kiga.main.locale.Locale;
import com.kiga.spec.Job;
import com.kiga.spec.Jobable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * Created by rainerh on 15.10.16.
 */
@Job(cronProperty = "ideasCacheCron")
public class IdeasCacheJob implements Jobable {
  IdeasCacheActualizer ideasCacheActualizer;
  IdeaGroupContainerRepositoryProxy repository;
  List<Locale> locales =
    Arrays.asList(Locale.de, Locale.en, Locale.it, Locale.tr);
  int[] ageGroupFilters = new int[] {0, 1, 2, 4, 8};
  Logger logger = LoggerFactory.getLogger(getClass());

  public IdeasCacheJob(
    IdeasCacheActualizer ideasCacheActualizer, IdeaGroupContainerRepositoryProxy repository) {
    this.ideasCacheActualizer = ideasCacheActualizer;
    this.repository = repository;
  }

  /**
   * runs actualizer with set locales and ageGroupFilters.
   */
  @Transactional
  @Async
  public void runJob() {
    logger.info("IdeasCache Job started");
    for (int ageGroupFilter: ageGroupFilters) {
      logger.info("IdeasCache Job iterating ageGroupFilter: " + ageGroupFilter);
      for (Locale locale: locales) {
        logger.info("IdeasCache Job iterating locale: " + locale);
        ideasCacheActualizer.run(
          repository.findIdeaGroupMainContainer(locale), ageGroupFilter, true);
      }
    }
    logger.info("IdeasCache Job ended");
  }
}
