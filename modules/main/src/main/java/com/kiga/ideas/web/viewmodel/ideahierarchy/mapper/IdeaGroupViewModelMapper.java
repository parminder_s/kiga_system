package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.IdeaGroupViewModel;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 10.01.17
 */
@Service
public class IdeaGroupViewModelMapper {
  private CategoryMapper categoryMapper;
  private IdeaMapper ideaMapper;
  private ParentCategoryViewModelMapper parentCategoryViewModelMapper;
  private ViewModelMetaDataProvider viewModelMetaDataProvider;

  /** Constructor. */
  @Autowired
  public IdeaGroupViewModelMapper(
      ViewModelMetaDataProvider viewModelMetaDataProvider,
      ParentCategoryViewModelMapper parentCategoryViewModelMapper,
      IdeaMapper ideaMapper,
      CategoryMapper categoryMapper) {
    this.viewModelMetaDataProvider = viewModelMetaDataProvider;
    this.parentCategoryViewModelMapper = parentCategoryViewModelMapper;
    this.categoryMapper = categoryMapper;
    this.ideaMapper = ideaMapper;
  }

  /** Return the ViewModel of the described IdeaGroupEntity. */
  public IdeaGroupViewModel getViewModel(
      IdeaCategory activeGroup, List<KigaIdea> ideas, List<IdeaCategory> siblings) {
    IdeaGroupViewModel ideaGroupViewModel = new IdeaGroupViewModel();

    ideaGroupViewModel.setShowBreadcrumbs(true);

    // map active Group
    ideaGroupViewModel.setActiveCategory(categoryMapper.convertIdeaCategory(activeGroup));

    // fill categories field to display when clicked on
    ideaGroupViewModel.setCategories(
        Collections.singletonList(ideaGroupViewModel.getActiveCategory()));

    // map ideas
    ideaGroupViewModel
        .getActiveCategory()
        .setIdeas(ideas.stream().map(ideaMapper::convert).collect(Collectors.toList()));

    // map siblings
    ideaGroupViewModel.setParentCategories(parentCategoryViewModelMapper.convertSiblings(siblings));

    ideaGroupViewModel.setFirstLevelCategoryName(activeGroup.getParent().getTitle());

    viewModelMetaDataProvider.fillWithMetaDataAndBreadcrumbs(ideaGroupViewModel, activeGroup);

    return ideaGroupViewModel;
  }
}
