package com.kiga.ideas.service;

import com.kiga.content.domain.ss.draft.IdeaCategoryDraft;
import com.kiga.content.domain.ss.draft.SiteTreeTranslationGroups;
import com.kiga.content.repository.ss.draft.IdeaCategoryRepository;
import com.kiga.content.repository.ss.draft.SiteTreeTranslationGroupsRepository;
import com.kiga.ideas.web.converter.IdeaCategoryToIdeasResponseGroupConverter;
import com.kiga.ideas.web.converter.IdeaCategoryToIdeasResponseGroupWithParentsConverter;
import com.kiga.ideas.web.response.IdeasResponseGroup;
import com.kiga.ideas.web.response.IdeasResponseGroupWithParents;
import com.kiga.main.locale.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by robert on 09.09.17.
 */
@Service
public class CategoryTranslationService {
  private IdeaCategoryRepository ideaCategoryRepository;
  private IdeaCategoryToIdeasResponseGroupConverter ideaCategoryToIdeasResponseGroupConverter;
  private IdeaCategoryToIdeasResponseGroupWithParentsConverter
    ideaCategoryToIdeasResponseGroupWithParentsConverter;
  private SiteTreeTranslationGroupsRepository translationGroupsRepository;

  /**
   * Inject dependencies.
   */
  @Autowired
  public CategoryTranslationService(IdeaCategoryRepository ideaCategoryRepository,
    IdeaCategoryToIdeasResponseGroupConverter ideaCategoryToIdeasResponseGroupConverter,
    IdeaCategoryToIdeasResponseGroupWithParentsConverter
      ideaCategoryToIdeasResponseGroupWithParentsConverter,
    SiteTreeTranslationGroupsRepository translationGroupsRepository) {
    this.ideaCategoryRepository = ideaCategoryRepository;
    this.ideaCategoryToIdeasResponseGroupConverter = ideaCategoryToIdeasResponseGroupConverter;
    this.ideaCategoryToIdeasResponseGroupWithParentsConverter =
      ideaCategoryToIdeasResponseGroupWithParentsConverter;
    this.translationGroupsRepository = translationGroupsRepository;
  }

  /**
   * This method tries to find translated category using translationgroups.
   *
   * @param categoryId id of category to be translated
   * @param locale     locale to be translated to
   * @return translated group
   */
  public IdeasResponseGroup translate(long categoryId, Locale locale) {
    return ideaCategoryToIdeasResponseGroupConverter
      .convertToResponse(translateToEntity(categoryId, locale));
  }

  /**
   * This method tries to find translated categories using translationgroups.
   *
   * @param categoryIds list of category ids to be translated
   * @param locale      locale to be translated to
   * @return list of translated groups
   */
  public List<IdeasResponseGroup> translate(List<Long> categoryIds, Locale locale) {
    Assert.notNull(categoryIds);
    Assert.notNull(locale);

    return categoryIds
      .stream()
      .map(id -> this.translate(id, locale))
      .collect(Collectors.toList());
  }

  /**
   * This method tries to find translated categories using translationgroups.
   *
   * @param categoryIds list of category ids to be translated
   * @param locale      locale to be translated to
   * @return list of translated groups
   */
  public List<IdeasResponseGroupWithParents> translateWithParents(List<Long> categoryIds,
    Locale locale) {
    Assert.notNull(categoryIds);
    Assert.notNull(locale);

    return categoryIds
      .stream()
      .map(id -> this.translateWithParents(id, locale))
      .filter(i -> i != null)
      .collect(Collectors.toList());
  }

  /**
   * This method tries to find translated category using translationgroups.
   *
   * @param categoryId id of category to be translated
   * @param locale     locale to be translated to
   * @return translated group
   */
  public IdeasResponseGroupWithParents translateWithParents(long categoryId, Locale locale) {
    IdeaCategoryDraft entity = translateToEntity(categoryId, locale);
    if (entity == null) {
      return null;
    }
    return ideaCategoryToIdeasResponseGroupWithParentsConverter.convertToResponse(entity);
  }

  private IdeaCategoryDraft translateToEntity(long categoryId, Locale locale) {
    Assert.notNull(locale);

    SiteTreeTranslationGroups translationGroup =
      translationGroupsRepository.findByOriginalId(categoryId);

    if (translationGroup == null) {
      return null;
    }

    List<Long> categoryIdsInTranslationGroup = translationGroupsRepository
      .findAllByTranslationGroupId(translationGroup.getTranslationGroupId())
      .stream()
      .map(SiteTreeTranslationGroups::getOriginalId)
      .collect(Collectors.toList());

    return ideaCategoryRepository.findByLocaleAndIdIn(locale, categoryIdsInTranslationGroup);
  }
}
