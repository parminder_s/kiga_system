package com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.favourites.repository.FavouriteNewIdeasRepository;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper.IdeaEngineHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 24.01.18
 */
@Service
@Log4j2
public class NewIdeaBlockViewEngine {
  private FavouriteNewIdeasRepository favouriteNewIdeasRepository;
  private NewIdeasRepository newIdeasRepository;
  private IdeaEngineHelper ideaEngineHelper;

  /** Constructor. */
  @Autowired
  public NewIdeaBlockViewEngine(
      IdeaEngineHelper ideaEngineHelper,
      FavouriteNewIdeasRepository favouriteNewIdeasRepository,
      NewIdeasRepository newIdeasRepository) {
    this.ideaEngineHelper = ideaEngineHelper;
    this.favouriteNewIdeasRepository = favouriteNewIdeasRepository;
    this.newIdeasRepository = newIdeasRepository;
  }

  /** get List of KigaIdeas depending on IdeaQueryParameters. */
  public List<KigaIdea> getIdeas(IdeaQueryParameter ideaQueryParameter) {
    return ideaQueryParameter
        .getMember()
        .map(
            member ->
                favouriteNewIdeasRepository.getAllIdeas(
                    ideaQueryParameter.getLocale(), ideaQueryParameter.getAgeGroupFilter(), member))
        .orElseGet(
            () -> {
              List<KigaIdea> returner =
                  newIdeasRepository.getAllIdeas(
                      ideaQueryParameter.getLocale(), ideaQueryParameter.getAgeGroupFilter());
              return returner;
            })
        .stream()
        .filter(ideaEngineHelper::hasValidParent)
        .collect(Collectors.toList());
  }
}
