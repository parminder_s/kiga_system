package com.kiga.ideas.downloadable.repository;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.AbstractRepositoryProxy;
import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.main.locale.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author bbs
 * @since 4/7/17.
 */
@Service
public class DownloadableIdeasRepositoryProxy
  extends AbstractRepositoryProxy<DownloadableIdeasRepository>
  implements DownloadableIdeasRepository {

  /**
   * Constructor for the Model.
   *
   * @param liveRepository    live repository
   * @param draftRepository   draft repository
   * @param repositoryContext repository context holder
   */
  @Autowired
  public DownloadableIdeasRepositoryProxy(
    DownloadableIdeasRepositoryLive liveRepository,
    DownloadableIdeasRepositoryDraft draftRepository,
    RepositoryContext repositoryContext) {
    super(liveRepository, draftRepository, repositoryContext);
  }

  @Override
  public List<KigaIdea> findAllDownloadableIdeas(Locale locale) {
    return getRepository().findAllDownloadableIdeas(locale);
  }

  @Override
  public KigaIdea getOneDownloadable(Long id) {
    return getRepository().getOneDownloadable(id);
  }
}
