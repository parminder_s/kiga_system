package com.kiga.ideas.downloadable.repository;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.main.locale.Locale;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author bbs
 * @since 4/7/17.
 */
public interface DownloadableIdeasRepository {
  @Query("from #{#entityName} e where e.articleType = 7 and e.locale = :locale")
  List<KigaIdea> findAllDownloadableIdeas(@Param("locale") Locale locale);

  @Query("from #{#entityName} e where e.articleType = 7 and e.id = :id")
  KigaIdea getOneDownloadable(@Param("id") Long id);
}
