package com.kiga.ideas.web.response;

import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.main.locale.Locale;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

/**
 * @author asv
 * @since 23/06/16.
 */
@Data
public class IdeasResponseIdeaDetails {
  private Long id;
  private String title;
  private String name;
  private String description;
  private String previewUrl;
  private String previewSiteUrl;
  private String largePreviewImageUrl;
  private Date createDate;
  private IdeasResponseRating ideasResponseRating;
  private long userRating;
  private ArticleType articleType;

  private String level1Title;
  private String level2Title;
  private String categoryName;
  private String parentCategoryName;
  private String url;
  private String content;
  private String ageRange;

  private Long categoryId;

  private Long parentCategoryId;

  private Locale locale;

  private Set<PreviewPageResponse> previewPages;
  private ArrayList<IdeasResponseLinkedIdea> linkedIdeas;
}
