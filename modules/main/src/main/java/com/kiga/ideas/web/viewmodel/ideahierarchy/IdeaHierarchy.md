# Idea Hierarchy Management

### IdeaHierarchyManager

* Entry Point
* Top Level Manager
* Implements the interface **ViewModelMapper**
* registered in **IdeaViewModelMapperFactoryInitializer** to retrieve ViewModels for
  * **IdeaGroupLive**
  * **IdeaGroupContainerLive**
  * **KigaIdeaLive**
* FeatureToggle: **isNewIdeaHierarchyEnabled**
* Uses Submanagers to retrieve ViewModels after deciding which ViewModel is needed
  * Communicates Information by building **IdeaQueryParameter**
    using the **IdeaQueryParameterBuilder**

#### Submanagers

use the **query** package to retrieve entities and forward them to the corresponding
**ViewModelConverter**

**KigaIdeaManager**

* gets ViewModel for **KigaIdea** from **KigaIdeaViewModelConverter**

**IdeaGroupManager**

* retrieves an **IdeaGroupQueryResult** from the **IdeaGroupQueryEngine**
* forwards information from the result to **IdeaGroupViewModelConverter**
  to get an **IdeaGroupViewModel**

**IdeaGroupCategoryManager**

* retrieves an **IdeaGroupCategoryQueryResult** from the **IdeaGroupCategoryQueryEngine**
* forwards information from the result to **IdeaGroupCategoryViewModelConverter**
  to either get a **GroupViewModel** or a **RedirectViewModel**

#### Query Package

* Entry Points
  * **IdeaGroupCategoryQueryEngine**
  * **ideaGroupQueryEngine**

- Distinguishes requested entities based on extracted information from given
  **IdeaQueryParameter**
- Uses Subengines like **IdeaEngine**, **IdeaQueryEngine** or
  **NewIdeaBlockViewEngine** to retrieve different requested entities.
- Builds an **IdeaGroupQueryResult** or **IdeaGroupCategoryQueryResult**
  to return a compact form of entities and information

#### Converter

* Convert entities to their corresponding ViewModel
* Inject basic mapper classes \* Usage of **MapStruct** to better list and use basic
  mappings

#### Response and Viewmodels Packages

* Viewmodels
  * Main ViewModels
  * **IdeaGroupCategoryViewModel**
  * **IdeaGroupViewModel**
  * implement **IdeaHierarchyViewModel**
* Response
  * "Sub"-ViewModels that are used within the Main ViewModels
  * Siblings
  * Parents
  * Ideas
