package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.base;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface GroupViewModelMapper {
  GroupViewModelMapper INSTANCE = Mappers.getMapper(GroupViewModelMapper.class);

  @Mappings({
    @Mapping(target = "id", source = "id"),
    @Mapping(target = "name", source = "urlSegment"),
    @Mapping(target = "title", source = "title"),
    // @Mapping(target = "locale", source = "locale"),
    @Mapping(target = "code", source = "code")
  })
  Category ideaCategoryToGroupViewModel(IdeaCategory ideaCategory);
}
