package com.kiga.ideas.web.viewmodel.ideahierarchy.builder;

import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 24.01.18
 */
@Service
public class IdeaQueryParameterBuilder {

  /** build queryParameters used by idea managers. */
  public IdeaQueryParameter buildQuery(QueryRequestParameters queryRequestParameters) {

    if (queryRequestParameters.getEntity() instanceof IdeaCategory) {
      IdeaCategory activeGroup = (IdeaCategory) queryRequestParameters.getEntity();
      IdeaQueryParameter ideaQueryParameter =
          IdeaQueryParameter.builder()
              .locale(activeGroup.getLocale())
              .ageGroupFilter(queryRequestParameters.getAgeGroup())
              .member(queryRequestParameters.getMember())
              .ideaCategory(activeGroup)
              .showAll(activeGroup instanceof IdeaGroupLive)
              .build();
      return ideaQueryParameter;
    } else if (queryRequestParameters.getEntity() instanceof KigaIdea) {

      KigaIdea activeIdea = (KigaIdea) queryRequestParameters.getEntity();

      IdeaQueryParameter ideaQueryParameter =
          IdeaQueryParameter.builder()
              .locale(activeIdea.getLocale())
              .ageGroupFilter(queryRequestParameters.getAgeGroup())
              .member(queryRequestParameters.getMember())
              .idea(activeIdea)
              .build();
      return ideaQueryParameter;
    }
    return null;
  }
}
