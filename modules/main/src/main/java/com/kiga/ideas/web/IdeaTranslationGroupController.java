package com.kiga.ideas.web;

import com.kiga.ideas.service.IdeaTranslationGroupService;
import com.kiga.ideas.web.response.IdeaTranslationGroup;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.web.request.IdRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by robert on 29.08.17.
 */
@RestController
@RequestMapping("idea/translation-group")
public class IdeaTranslationGroupController {
  private IdeaTranslationGroupService ideaTranslationGroupService;

  @Autowired
  public IdeaTranslationGroupController(IdeaTranslationGroupService ideaTranslationGroupService) {
    this.ideaTranslationGroupService = ideaTranslationGroupService;
  }

  @RequestMapping("by/idea")
  public IdeaTranslationGroup getByIdea(@RequestBody IdRequest idRequest) {
    return ideaTranslationGroupService.getByIdeaId(idRequest.getId());
  }

  @RequestMapping("by/translation-group")
  public IdeaTranslationGroup getByTranslationGroup(@RequestBody IdRequest idRequest) {
    return ideaTranslationGroupService.getByTranslationGroupId(idRequest.getId());
  }

  @RequestMapping("prepare-idea")
  public IdeasViewModelIdea prepareIdeaForTranslation(@RequestBody IdRequest idRequest)
    throws IOException {
    return ideaTranslationGroupService
      .prepareIdeaForTranslation(idRequest.getId(), idRequest.getLocale());
  }
}
