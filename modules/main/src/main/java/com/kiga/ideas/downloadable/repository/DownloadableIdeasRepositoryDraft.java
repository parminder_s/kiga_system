package com.kiga.ideas.downloadable.repository;

import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 4/7/17.
 */
public interface DownloadableIdeasRepositoryDraft extends DownloadableIdeasRepository,
  CrudRepository<KigaIdeaDraft, Long> {
}
