package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.base;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.Idea;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MinimizedIdeaViewModelMapper {
  MinimizedIdeaViewModelMapper INSTANCE = Mappers.getMapper(MinimizedIdeaViewModelMapper.class);

  @Mapping(target = "uploadedFileId", source = "uploadedFile.id")
  @Mapping(target = "uploadedFileName", source = "uploadedFile.name")
  @Mapping(target = "rating", ignore = true)
  @Mapping(target = "previewUrl", ignore = true)
  @Mapping(target = "largePreviewImageUrl", ignore = true)
  @Mapping(target = "ngUrl", ignore = true)
  @Mapping(target = "previewPages", ignore = true)
  @Mapping(target = "elasticScore", ignore = true)
  @Mapping(target = "audioLink", ignore = true)
  @Mapping(target = "videoLink", ignore = true)
  @Mapping(target = "download", source = "articleType")
  @Mapping(
      target = "facebookShare",
      source = "facebookShare",
      qualifiedByName = "facebookShareToFacebookShare")
  Idea kigaIdeaToIdeaViewModel(KigaIdea kigaIdea);

  default Boolean inPoolToInPool(long inPool) {
    return inPool == 1;
  }

  default Boolean articleTypeToDownload(ArticleType articleType) {
    return ArticleType.DOWNLOAD.equals(articleType);
  }

  @Named("facebookShareToFacebookShare")
  default Boolean facebookShareToFacebookShare(Integer facebookShare) {
    return facebookShare != null && facebookShare == 1;
  }
}
