package com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel;

import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.property.LayoutType;
import com.kiga.ideas.IdeaState;
import com.kiga.ideas.web.response.IdeasResponseRating;
import com.kiga.ideas.web.response.PreviewPageResponse;
import com.kiga.main.locale.Locale;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Florian Schneider
 * @since 10.01.17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class IdeaViewModel extends ViewModel { // IdeaViewModelIdea
  private boolean download;
  private String name;
  private String description;
  private Long previewFileId;
  private String previewUrl = "";
  private String previewName = "";
  private Integer previewSize;
  private String largePreviewImageUrl = "";
  private Date createDate;
  private Date lastEdited;
  private IdeasResponseRating rating;
  private Boolean facebookShare = false;
  private Boolean inPool = false;
  private Boolean forParents = false;
  private String level1Title;
  private String level2Title;
  private String categoryName;
  private String parentCategoryName;
  private String url;
  private Long uploadedFileId;
  private String uploadedFileName;
  private String fileProxyUrl = "";
  private String fileUrl = "";
  private String fileName = "";
  private Integer fileSize;
  private String filePreviewUrl;
  private String ngUrl;
  private LayoutType layoutType;
  private Long categoryId;
  private Long parentCategoryId;
  private Collection<PreviewPageResponse> previewPages;
  private String ageRange = "";
  private List<Integer> separatedAgeGroups;
  private Float elasticScore;
  private String audioLink;
  private String videoLink;
  private Boolean published;
  private Integer sortMonth;
  private Integer sortYear;
  private Integer sortDayMini;
  private Integer sortMonthMini;
  private Integer sortYearMini;
  private IdeaState state;
  private Locale locale;
}
