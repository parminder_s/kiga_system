package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.base;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.ideas.web.response.KigaIdeaViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface KigaIdeaViewModelBaseMapper {
  KigaIdeaViewModelBaseMapper INSTANCE = Mappers.getMapper(KigaIdeaViewModelBaseMapper.class);

  @Mapping(target = "id", source = "id")
  @Mapping(target = "title", source = "title")
  @Mapping(target = "name", source = "urlSegment")
  @Mapping(target = "description", source = "description")
  @Mapping(target = "createDate", source = "created")
  @Mapping(target = "layoutType", source = "layoutType")
  @Mapping(target = "articleType", source = "articleType")
  @Mapping(target = "uploadedFileName", source = "uploadedFile.name")
  @Mapping(target = "uploadedFileId", source = "uploadedFile.id")
  @Mapping(target = "resolvedType", ignore = true)
  @Mapping(target = "showBreadcrumbs", ignore = true)
  @Mapping(target = "breadcrumbs", ignore = true)
  @Mapping(target = "metadata", ignore = true)
  @Mapping(target = "permission", ignore = true)
  @Mapping(target = "previewIdeaUrl", ignore = true)
  @Mapping(target = "previewUrl", ignore = true)
  @Mapping(target = "largePreviewImageUrl", ignore = true)
  @Mapping(target = "level1Title", ignore = true)
  @Mapping(target = "level2Title", ignore = true)
  @Mapping(target = "categoryName", ignore = true)
  @Mapping(target = "parentCategoryName", ignore = true)
  @Mapping(target = "url", ignore = true)
  @Mapping(target = "ageRange", ignore = true)
  @Mapping(target = "categoryId", ignore = true)
  @Mapping(target = "parentCategoryId", ignore = true)
  @Mapping(target = "previewPages", ignore = true)
  @Mapping(target = "linkedIdeas", ignore = true)
  KigaIdeaViewModel kigaIdeaToKigaIdeaViewModel(KigaIdea kigaIdea);
}
