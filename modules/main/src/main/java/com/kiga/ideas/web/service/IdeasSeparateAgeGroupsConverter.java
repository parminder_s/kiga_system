package com.kiga.ideas.web.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 4/12/17.
 */
@Service
public class IdeasSeparateAgeGroupsConverter {
  /**
   * Convert ageGroup sum into separate age group values.
   *
   * @param ageGroup age group sum
   * @return separate list
   */
  public List<Integer> convert(int ageGroup) {
    List<Integer> returner = new ArrayList<>();
    returner.add(ageGroup & 1);
    returner.add(ageGroup & 2);
    returner.add(ageGroup & 4);
    returner.add(ageGroup & 8);
    returner.add(ageGroup & 16);
    return returner.stream().filter(element -> element > 0).collect(Collectors.toList());
  }
}
