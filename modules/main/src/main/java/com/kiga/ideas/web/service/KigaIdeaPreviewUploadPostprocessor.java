package com.kiga.ideas.web.service;

import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.property.ImageSize;
import com.kiga.kigaupload.annotation.UploadProcessor;
import com.kiga.kigaupload.processors.PostUploadProcessor;
import com.kiga.kigaupload.web.response.UploadFileStatus;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.repository.ss.S3ImageRepository;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.security.domain.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Created by robert on 07.05.17.
 */
@Service
@UploadProcessor(context = "kigaidea-download-type-preview")
public class KigaIdeaPreviewUploadPostprocessor implements PostUploadProcessor {
  private S3ImageResizeService s3ImageResizeService;
  private S3ImageRepository s3ImageRepository;
  private IdeasProperties ideasProperties;

  /**
   * Dependencies.
   *
   * @param s3ImageResizeService s3ImageResizeService
   * @param s3ImageRepository s3ImageRepository
   * @param ideasProperties ideasProperties
   */
  @Autowired
  public KigaIdeaPreviewUploadPostprocessor(
    S3ImageResizeService s3ImageResizeService,
    S3ImageRepository s3ImageRepository, IdeasProperties ideasProperties) {
    this.s3ImageResizeService = s3ImageResizeService;
    this.s3ImageRepository = s3ImageRepository;
    this.ideasProperties = ideasProperties;
  }

  @Override
  public void process(Member member, List<MultipartFile> files, List<UploadFileStatus> returner) {
    returner.forEach(uploadFileStatus -> {
      ImageSize previewSize = ideasProperties.getPreviewImage().getSmall();
      S3Image s3Image = s3ImageRepository.findOne(uploadFileStatus.getId());
      try {
        String resizedImageUrl = s3ImageResizeService.getResizedImageUrl(s3Image,
          new PaddedImageStrategy(previewSize.getWidth(), previewSize.getHeight()));
        uploadFileStatus.setUrl(resizedImageUrl);
      } catch (IOException exception) {
        // todo logger
      }
    });
  }
}
