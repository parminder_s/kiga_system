package com.kiga.ideas.web.viewmodel.ideahierarchy.query.model;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author Florian Schneider
 * @since 19/12/17.
 */
@Data
@Builder
@AllArgsConstructor
public class IdeaQueryParameter {
  private Locale locale;
  private int ageGroupFilter;
  private boolean showAll;
  private IdeaCategory ideaCategory;
  private KigaIdea idea;

  @Builder.Default private Optional<Member> member = Optional.empty();

  /** no args constructor setting default values. */
  public IdeaQueryParameter() {
    this.member = Optional.empty();
  }
}
