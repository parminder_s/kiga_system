package com.kiga.ideas.web.response;

import lombok.Getter;
import lombok.Setter;


import java.util.Collection;

/**
 * @author bbs
 * @since 3/3/16.
 */
@Getter
@Setter
public class IdeasResponseCategory {
  private Long id;
  private String name;
  private String title;
  private String icon = "icon-ideas";
  private boolean lastCategoryLevel;
  private int level;
  private Collection<IdeasViewModelIdea> ideas;
  private Integer sort;
  private String sortTree;
}
