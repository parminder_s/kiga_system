package com.kiga.ideas.task.cache;

import com.kiga.content.domain.ss.SiteTree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by rainerh on 15.10.16.
 */
public class IdeaGroupCollector {
  List<String> allowedClassNames = Arrays
    .asList("IdeaGroupMainContainer", "IdeaGroupContainer", "IdeaGroupCategory", "IdeaGroup");


  public List<SiteTree> collect(SiteTree siteTree, boolean recursively) {
    return recursively ? iterateChildren(siteTree) : Collections.singletonList(siteTree);
  }

  private List<SiteTree<? extends SiteTree>> iterateChildren(
    SiteTree<? extends SiteTree> siteTree) {
    List<SiteTree<? extends SiteTree>> returner = new ArrayList<>();

    if (isAllowedClass(siteTree)) {
      for (SiteTree child: siteTree.getChildren()) {
        returner.addAll(this.iterateChildren(child));
      }
      returner.add(siteTree);
    }

    return returner;
  }

  private boolean isAllowedClass(SiteTree siteTree) {
    return allowedClassNames.contains(siteTree.getClassName());
  }
}
