package com.kiga.ideas.task.cache;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepositoryProxy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

/**
 * This is the pendant to IdeasCacheJob and is triggered manually
 * by the controller, whereas the Job is triggered by a cron schedule.
 */
public class IdeasCacheExecutor {
  IdeaCategoryRepositoryProxy repository;
  IdeasCacheActualizer ideasCacheActualizer;

  public IdeasCacheExecutor(
    IdeaCategoryRepositoryProxy repository, IdeasCacheActualizer ideasCacheActualizer) {
    this.repository = repository;
    this.ideasCacheActualizer = ideasCacheActualizer;
  }

  /**
   * runs the actualizer with given parameters.
   */
  @Async
  @Transactional
  public void run(long siteTreeId, int[] ageGroupFilters, boolean recursively) {
    IdeaCategory ideaCategory = repository.findOne(siteTreeId);
    Arrays.stream(ageGroupFilters)
      .forEach(ageGroupFilter -> ideasCacheActualizer
        .run(ideaCategory, ageGroupFilter, recursively));
  }
}
