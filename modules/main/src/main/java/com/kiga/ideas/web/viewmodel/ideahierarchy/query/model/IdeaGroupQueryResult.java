package com.kiga.ideas.web.viewmodel.ideahierarchy.query.model;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import java.util.List;
import lombok.Builder;
import lombok.Data;

/**
 * @author Florian Schneider
 * @since 09.01.17
 */
@Data
@Builder
public class IdeaGroupQueryResult {
  private List<KigaIdea> ideas;
  private List<IdeaCategory> siblings;
}
