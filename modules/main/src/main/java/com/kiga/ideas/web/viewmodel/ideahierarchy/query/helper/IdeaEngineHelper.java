package com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper;

import com.kiga.content.domain.ss.KigaIdea;
import javax.persistence.EntityNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 24.01.18
 */
@Service
@Log4j2
public class IdeaEngineHelper {

  /** Check for valid parent of KigaIdea entity. */
  public boolean hasValidParent(KigaIdea kigaIdea) {
    try {
      kigaIdea.getParent().getTitle();
      return true;
    } catch (EntityNotFoundException enfe) {
      log.error("SiteTree with ID " + kigaIdea.getId() + " has no parent");
      return false;
    } catch (NullPointerException npe) {
      log.error("SiteTree with ID " + kigaIdea.getId() + " has a null parent");
      return false;
    }
  }
}
