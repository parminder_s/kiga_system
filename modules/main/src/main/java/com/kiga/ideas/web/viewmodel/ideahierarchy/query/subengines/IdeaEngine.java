package com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.KigaIdeaRepositoryProxy;
import com.kiga.ideas.service.DefaultIdeaCategorySortToDbSort;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 24.01.18
 */
@Service
public class IdeaEngine {

  private KigaIdeaRepositoryProxy kigaIdeaRepositoryProxy;

  /** Constructor. */
  @Autowired
  public IdeaEngine(KigaIdeaRepositoryProxy kigaIdeaRepositoryProxy) {

    this.kigaIdeaRepositoryProxy = kigaIdeaRepositoryProxy;
  }

  /** get List of KigaIdeas depending on IdeaQueryParameters. */
  public List<KigaIdea> getIdeas(IdeaQueryParameter ideaQueryParameter) {
    DefaultIdeaCategorySortToDbSort defaultIdeaCategorySortToDbSort =
        new DefaultIdeaCategorySortToDbSort();
    return kigaIdeaRepositoryProxy.findAllForGroup(
        ideaQueryParameter.getIdeaCategory().getId(),
        ideaQueryParameter.getAgeGroupFilter(),
        defaultIdeaCategorySortToDbSort.mapSort(ideaQueryParameter.getIdeaCategory()));
  }
}
