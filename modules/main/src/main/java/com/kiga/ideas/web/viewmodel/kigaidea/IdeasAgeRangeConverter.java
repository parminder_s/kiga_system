package com.kiga.ideas.web.viewmodel.kigaidea;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by asv on 20.09.16.
 */
public class IdeasAgeRangeConverter {

  /**
   * get agegroup-string from the ored value.
   */
  public String getAgeRange(int value) {
    Map<Integer, String> map = new HashMap<>();

    map.put(1, "1 - 3");
    map.put(1 | 2, "1 - 5");
    map.put(1 | 2 | 4, "1 - 6");
    map.put(1 | 2 | 4 | 8, "1 - 7");
    map.put(1 | 2 | 4 | 8 | 16, "1 - 8");

    map.put(2, "3 - 5");
    map.put(2 | 4, "3 - 6");
    map.put(2 | 4 | 8, "3 - 7");
    map.put(2 | 4 | 8 | 16, "3 - 8");

    map.put(4, "5 - 6");
    map.put(4 | 8, "5 - 7");
    map.put(4 | 8 | 16, "5 - 8");

    map.put(8, "6 - 7");
    map.put(8 | 16, "6 - 8");

    map.put(16, "7 - 8");

    return map.getOrDefault(value, "1 - 8");
  }
}
