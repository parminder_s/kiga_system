package com.kiga.ideas.web.response;

import lombok.Data;

/**
 * @author bbs
 * @since 5/13/16.
 */
@Data
public class IdeaCategoryHierarchyViewModel {
  private Long categoryId;
  private String categoryName;
  private String categoryTitle;
  private Integer categorySort;

  private Long subCategoryId;
  private String subCategoryName;
  private String subCategoryTitle;
  private Integer subCategorySort;
}
