package com.kiga.ideas.web.response;

import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.content.domain.ss.property.LayoutType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

/**
 * @author asv
 * @since 09/11/16.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class KigaIdeaViewModel extends ViewModel {
  private Long id;
  private String title;
  private String name;
  private String description;
  private String previewIdeaUrl;
  private String previewUrl;
  private String largePreviewImageUrl;
  private Date createDate;
  private LayoutType layoutType;
  private ArticleType articleType;

  private String level1Title;
  private String level2Title;
  private String categoryName;
  private String parentCategoryName;
  private String url;
  private String content;
  private String ageRange;

  private Long categoryId;

  private Long parentCategoryId;

  private Set<PreviewPageResponse> previewPages;
  private ArrayList<IdeasResponseLinkedIdea> linkedIdeas;

  private String uploadedFileName;
  private Long uploadedFileId;
}
