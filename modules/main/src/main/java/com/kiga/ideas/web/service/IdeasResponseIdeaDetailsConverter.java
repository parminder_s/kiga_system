package com.kiga.ideas.web.service;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.LinkedKigaPageGroup;
import com.kiga.content.domain.ss.Rating;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.content.repository.ss.RatingRepository;
import com.kiga.content.service.KigaIdeaUrlGenerator;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.service.RatingCalculatorService;
import com.kiga.ideas.web.response.IdeasResponseIdeaDetails;
import com.kiga.ideas.web.response.IdeasResponseLinkedIdea;
import com.kiga.ideas.web.viewmodel.kigaidea.IdeasAgeRangeConverter;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import com.kiga.web.converter.ResponseConverter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * Created by rainerh on 17.09.16.
 */
@Service
public class IdeasResponseIdeaDetailsConverter
  implements ResponseConverter<KigaIdea, IdeasResponseIdeaDetails> {
  private Logger logger = LoggerFactory.getLogger(getClass());
  private S3ImageResizeService s3ImageResizeService;
  private IdeasProperties ideasProperties;
  private SiteTreeUrlGenerator urlGenerator;
  private RatingCalculatorService ratingCalculatorService;
  private RatingRepository ratingRepository;
  private SecurityService securityService;
  private KigaIdeaUrlGenerator kigaIdeaUrlGenerator;

  /**
   * inject dependencies.
   */
  @Autowired
  public IdeasResponseIdeaDetailsConverter(
    S3ImageResizeService s3ImageResizeService,
    IdeasProperties ideasProperties,
    SiteTreeUrlGenerator urlGenerator,
    RatingCalculatorService ratingCalculatorService,
    RatingRepository ratingRepository,
    SecurityService securityService,
    KigaIdeaUrlGenerator kigaIdeaUrlGenerator) {
    this.s3ImageResizeService = s3ImageResizeService;
    this.ideasProperties = ideasProperties;
    this.urlGenerator = urlGenerator;
    this.ratingCalculatorService = ratingCalculatorService;
    this.ratingRepository = ratingRepository;
    this.securityService = securityService;
    this.kigaIdeaUrlGenerator = kigaIdeaUrlGenerator;
  }

  /**
   * Do the conversion.
   */
  public IdeasResponseIdeaDetails convertToResponse(KigaIdea idea) {
    if (idea == null) {
      return null;
    }

    IdeasResponseIdeaDetails returner = new IdeasResponseIdeaDetails();
    IdeasAgeRangeConverter converter = new IdeasAgeRangeConverter();

    returner.setId(idea.getId());
    returner.setTitle(idea.getTitle());
    returner.setName(idea.getUrlSegment());
    returner.setDescription(idea.getDescription());
    returner.setCreateDate(idea.getCreated());
    returner.setLocale(idea.getLocale());

    if (idea.getArticleType() != null && ArticleType.DOWNLOAD.equals(idea.getArticleType())) {
      returner.setArticleType(idea.getArticleType());
      analyzeContent(returner, idea.getContent());
    } else {
      returner.setContent(idea.getContent());
    }

    if (idea.getParent() != null) {
      returner.setCategoryName(idea.getParent().getUrlSegment());
      returner.setCategoryId(idea.getParent().getId());
      returner.setParentCategoryName(idea.getParent().getParent().getUrlSegment());
      returner.setParentCategoryId(idea.getParent().getParent().getId());
    }
    setDetailPreviewImages(returner, idea.getPreviewImage());
    setLinkedIdeas(returner, idea.getLinkedKigaPageGroups());
    returner.setIdeasResponseRating(
      ratingCalculatorService.getIdeaResponseRating(idea));

    if (idea.getAgeGroup() != null) {
      returner.setAgeRange(converter.getAgeRange(idea.getAgeGroup()));
    }

    SiteTree<? extends SiteTree> parent = idea.getParent();
    returner.setLevel2Title(parent.getTitle());
    returner.setLevel1Title(parent.getParent().getTitle());

    returner.setPreviewSiteUrl(kigaIdeaUrlGenerator.getRelativePreviewUrl(idea));
    returner.setUrl(urlGenerator.getRelativeUrl(idea));

    Optional<Member> member = securityService.getSessionMember();
    if (member.isPresent()) {
      long userId = member.get().getId();
      Rating userRating = ratingRepository.findByMemberIdAndKigaPageId(userId, idea.getId());
      if (userRating != null) {
        returner.setUserRating(userRating.getRatingValue());
      }
    }

    return returner;
  }

  private int cellCount(Document doc) {
    Element firstRowDiv = doc.select(".rowDiv").first();
    if (firstRowDiv == null) {
      return 0;
    }
    return firstRowDiv.children().size();
  }

  private void analyzeContent(IdeasResponseIdeaDetails ideasResponseIdeaDetails,
    String content) {
    Document doc = Jsoup.parse(content);
    int cellCount = cellCount(doc);
    if (cellCount == 2) {
      Elements row = doc.select(".rowDiv").first().children();
      Element firstCell = row.first();
      Element secondCell = row.first().nextElementSibling();
      if (firstCell.select("img").size() == 1 && secondCell.select("img").size() == 0) {
        if ((firstCell.hasClass("span-1") && secondCell.hasClass("span-3"))
          || (firstCell.hasClass("span-2") && secondCell.hasClass("span-2"))
          || (firstCell.hasClass("span-3") && secondCell.hasClass("span-1"))) {
          doc.select(".rowDiv").first().remove();
          String newContent = doc.toString();
          ideasResponseIdeaDetails.setContent(newContent);
        } else {
          ideasResponseIdeaDetails.setContent(content);
        }
      } else {
        ideasResponseIdeaDetails.setContent(content);
      }
    } else {
      ideasResponseIdeaDetails.setContent(content);
    }
  }

  private void setDetailPreviewImages(IdeasResponseIdeaDetails ideasResponseIdeaDetails,
    KigaPageImage previewImage) {
    if (previewImage == null) {
      ideasResponseIdeaDetails.setLargePreviewImageUrl("");
    } else {
      S3Image s3Image = previewImage.getS3Image();
      try {
        ImageSize smallPreview = ideasProperties.getPreviewImage().getSmall();
        ImageSize largePreview = ideasProperties.getPreviewImage().getLarge();
        ideasResponseIdeaDetails.setPreviewUrl(
          s3ImageResizeService.getResizedImageUrl(s3Image,
            new PaddedImageStrategy(smallPreview.getWidth(), smallPreview.getHeight())));
        ideasResponseIdeaDetails.setLargePreviewImageUrl(
          s3ImageResizeService.getResizedImageUrl(s3Image,
            new PaddedImageStrategy(largePreview.getWidth(), largePreview.getHeight())));
      } catch (IOException ex) {
        logger.warn("Problem with cached (or caching) size variants of image: " + s3Image
          .getName(), ex);
      } catch (IllegalArgumentException ex) {
        logger.warn("Wrong arguments passed to the strategy class", ex);
      }
    }
  }

  private void setLinkedIdeas(IdeasResponseIdeaDetails ideasResponseIdeaDetails,
    List<LinkedKigaPageGroup> linkedKigaPageGroups) {
    ArrayList<IdeasResponseLinkedIdea> list = new ArrayList<>();
    for (LinkedKigaPageGroup linkedKigaPageGroup : linkedKigaPageGroups) {
      List<KigaIdea> kigaPages = linkedKigaPageGroup.getKigaPages();
      for (KigaIdea linkedIdea : kigaPages) {
        if (!ideasResponseIdeaDetails.getId().equals(linkedIdea.getId())) {
          IdeasResponseLinkedIdea idea = new IdeasResponseLinkedIdea();
          idea.setId(linkedIdea.getId());
          setLinkedIdeaPreviewImage(idea, linkedIdea.getPreviewImage());
          idea.setTitle(linkedIdea.getTitle());
          idea.setDescription(linkedIdea.getDescription());
          idea.setUrl(urlGenerator.getRelativeUrl(linkedIdea));
          list.add(idea);
        }
      }
    }
    ideasResponseIdeaDetails.setLinkedIdeas(list);
  }

  private void setLinkedIdeaPreviewImage(IdeasResponseLinkedIdea ideasResponseLinkedIdea,
    KigaPageImage previewImage) {
    if (previewImage == null) {
      ideasResponseLinkedIdea.setLargePreviewImageUrl("");
    } else {
      S3Image s3Image = previewImage.getS3Image();
      try {
        ImageSize smallPreview = ideasProperties.getPreviewImage().getSmall();
        ideasResponseLinkedIdea.setPreviewUrl(
          s3ImageResizeService.getResizedImageUrl(s3Image,
            new PaddedImageStrategy(smallPreview.getWidth(), smallPreview.getHeight())));
      } catch (IOException ex) {
        logger.warn("Problem with cached (or caching) size variants of image: " + s3Image
          .getName(), ex);
      } catch (IllegalArgumentException ex) {
        logger.warn("Wrong arguments passed to the strategy class", ex);
      }
    }
  }
}
