package com.kiga.ideas.service;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.draft.QIdeaGroupDraft;
import com.kiga.content.domain.ss.draft.QIdeaGroupToIdeaDraft;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.draft.IdeaGroupRepository;
import com.kiga.ideas.web.converter.IdeaCategoryToIdeasResponseGroupConverter;
import com.kiga.ideas.web.converter.IdeaCategoryToIdeasResponseGroupWithParentsConverter;
import com.kiga.ideas.web.response.IdeasResponseGroup;
import com.kiga.ideas.web.response.IdeasResponseGroupWithParents;
import com.kiga.ideas.web.response.IdeasResponseIdeaDetails;
import com.kiga.ideas.web.service.IdeaDetailsService;
import com.kiga.main.locale.Locale;
import com.kiga.main.util.NumberUtil;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.query.ListSubQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 5/13/16.
 */
@Service
public class IdeaGroupService {
  private IdeaCategoryToIdeasResponseGroupConverter ideaCategoryToIdeasResponseGroupConverter;
  private IdeaCategoryToIdeasResponseGroupWithParentsConverter
    ideaCategoryToIdeasResponseGroupWithParentsConverter;
  private IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxy;
  private IdeaGroupRepository ideaGroupRepository;
  private CategoryTranslationService categoryTranslationService;
  private IdeaDetailsService ideaDetailsService;

  /**
   * Constructor to add dependencies by its arguments.
   *
   * @param ideaCategoryToIdeasResponseGroupConverter            converter
   * @param ideaCategoryToIdeasResponseGroupWithParentsConverter converter
   * @param ideaGroupRepository                                  ideaGroupRepository
   * @param categoryTranslationService                           categoryTranslationService
   * @param ideaDetailsService                                   ideaDetailsService
   */
  @Autowired
  public IdeaGroupService(
    IdeaCategoryToIdeasResponseGroupConverter ideaCategoryToIdeasResponseGroupConverter,
    IdeaCategoryToIdeasResponseGroupWithParentsConverter
      ideaCategoryToIdeasResponseGroupWithParentsConverter,
    IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxy,
    IdeaGroupRepository ideaGroupRepository, CategoryTranslationService categoryTranslationService,
    IdeaDetailsService ideaDetailsService) {
    this.ideaCategoryToIdeasResponseGroupConverter = ideaCategoryToIdeasResponseGroupConverter;
    this.ideaCategoryToIdeasResponseGroupWithParentsConverter =
      ideaCategoryToIdeasResponseGroupWithParentsConverter;
    this.ideaCategoryRepositoryProxy = ideaCategoryRepositoryProxy;
    this.ideaGroupRepository = ideaGroupRepository;
    this.categoryTranslationService = categoryTranslationService;
    this.ideaDetailsService = ideaDetailsService;
  }

  /**
   * Get groups for parent group.
   *
   * @param parentGroup parent
   * @return ideas groups
   */
  public List<IdeasResponseGroup> getGroupsSortTree(IdeasResponseGroup parentGroup) {
    List<IdeaCategory> categories = ideaCategoryRepositoryProxy
      .findBySortTreeStartsWithAndLocaleLikeOrderBySortAsc(parentGroup.getSortTree(),
        parentGroup.getLocale());
    return processGroups(categories, parentGroup.getLevel() + 1);
  }

  /**
   * Get groups for parent group.
   */
  public List<IdeasResponseGroup> getParentGroups(Long parentGroupId, Integer level) {
    return processGroups(
      ideaCategoryRepositoryProxy.findByParentIdOrderBySortAsc(parentGroupId),
      level);
  }

  private List<IdeasResponseGroup> processGroups(List<IdeaCategory> ideaGroups, int level) {
    return ideaGroups.stream()
      .map(ideaCategoryToIdeasResponseGroupConverter::convertToResponse)
      .filter(group -> level < 4 || group.isIdeaGroup())
      .peek(group -> group.setLevel(level))
      .collect(Collectors.toList());
  }

  /**
   * Get idea groups not assigned to a particular idea.
   *
   * @param id id of the idea
   * @return list of unassigned idea groups
   */
  public List<IdeasResponseGroupWithParents> getUnassigned(Long id, Locale locale) {
    if (NumberUtil.isValidId(id)) {
      QIdeaGroupDraft ideaGroupDraft = QIdeaGroupDraft.ideaGroupDraft;
      QIdeaGroupToIdeaDraft ideaGroupToIdeaDraft = QIdeaGroupToIdeaDraft.ideaGroupToIdeaDraft;

      ListSubQuery<Long> list = new JPASubQuery().distinct().from(ideaGroupToIdeaDraft)
        .where(ideaGroupToIdeaDraft.article.id.eq(id)).list(ideaGroupToIdeaDraft.ideaGroup.id);

      List<IdeaCategory> ideaCategoryList = new ArrayList<>();

      BooleanExpression allIdeaGroupsByIdsAndLocale =
        BooleanExpression.allOf(ideaGroupDraft.id.notIn(list), ideaGroupDraft.locale.eq(locale));

      ideaGroupRepository.findAll(allIdeaGroupsByIdsAndLocale).forEach(ideaCategoryList::add);

      return ideaCategoryToIdeasResponseGroupWithParentsConverter
        .convertToResponse(ideaCategoryList);
    } else {
      List<IdeaCategory> ideaCategoryList = new ArrayList<>();

      QIdeaGroupDraft ideaGroupDraft = QIdeaGroupDraft.ideaGroupDraft;
      BooleanExpression allIdeaGroupsByLocale = BooleanExpression
        .allOf(ideaGroupDraft.className.eq("IdeaGroup"), ideaGroupDraft.locale.eq(locale));

      ideaGroupRepository.findAll(allIdeaGroupsByLocale).forEach(ideaCategoryList::add);

      ideaCategoryList = ideaCategoryList.stream().filter(this::hasIdeaGroupMainContainer)
        .collect(Collectors.toList());

      return ideaCategoryToIdeasResponseGroupWithParentsConverter
        .convertToResponse(ideaCategoryList);
    }
  }

  private boolean hasIdeaGroupMainContainer(SiteTree ideaCategory) {
    if (ideaCategory == null) {
      return false;
    }

    SiteTree parent = ideaCategory.getParent();
    if (parent == null) {
      return false;
    }

    if ("IdeaGroupMainContainer".equals(parent.getClassName())) {
      return true;
    }

    return hasIdeaGroupMainContainer(parent);
  }

  /**
   * Get idea groups assigned to a particular idea.
   *
   * @param id id of the idea
   * @return list of assigned idea groups
   */
  public List<IdeasResponseGroupWithParents> getAssigned(Long id, Locale locale) {
    QIdeaGroupDraft ideaGroupDraft = QIdeaGroupDraft.ideaGroupDraft;
    QIdeaGroupToIdeaDraft ideaGroupToIdeaDraft = QIdeaGroupToIdeaDraft.ideaGroupToIdeaDraft;

    ListSubQuery<Long> list = new JPASubQuery().distinct().from(ideaGroupToIdeaDraft)
      .where(ideaGroupToIdeaDraft.article.id.eq(id)).list(ideaGroupToIdeaDraft.ideaGroup.id);

    List<IdeaCategory> ideaCategoryList = new ArrayList<>();

    BooleanExpression ideaGroupsDraftByIdsAndLocale =
      BooleanExpression.allOf(ideaGroupDraft.id.in(list), ideaGroupDraft.locale.eq(locale));

    ideaGroupRepository.findAll(ideaGroupsDraftByIdsAndLocale).forEach(ideaCategoryList::add);

    return ideaCategoryToIdeasResponseGroupWithParentsConverter.convertToResponse(ideaCategoryList);
  }

  /**
   * Get idea groups assigned to a particular idea, then return their translations.
   *
   * @param clonedIdeaId id of the idea to be cloned
   * @return list of assigned idea groups
   */
  public List<IdeasResponseGroupWithParents> getTranslatedAssigned(Long clonedIdeaId,
    Locale toLocale) {
    IdeasResponseIdeaDetails ideaDetails = ideaDetailsService.getIdeaDetails(clonedIdeaId);
    Locale fromLocale = ideaDetails.getLocale();

    List<IdeasResponseGroupWithParents> assignedToClonedIdea =
      getAssigned(clonedIdeaId, fromLocale);

    if (assignedToClonedIdea == null) {
      return new ArrayList<>();
    }

    List<Long> groupIds = assignedToClonedIdea
      .stream()
      .map(IdeasResponseGroupWithParents::getId)
      .collect(Collectors.toList());

    return categoryTranslationService.translateWithParents(groupIds, toLocale);
  }
}
