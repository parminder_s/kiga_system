package com.kiga.ideas.initializer;

import com.kiga.cms.service.ViewModelMapperFactory;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.ideas.web.viewmodel.ideahierarchy.IdeaHierarchyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * @author bbs
 * @since 12/8/16.
 */
@Component
public class IdeaViewModelMapperFactoryInitializer
    implements ApplicationListener<ContextRefreshedEvent> {
  private IdeaHierarchyManager ideaHierarchyManager;
  private ViewModelMapperFactory viewModelMapperFactory;

  /** Setup dependencies. */
  @Autowired
  public IdeaViewModelMapperFactoryInitializer(
      IdeaHierarchyManager ideaHierarchyManager, ViewModelMapperFactory viewModelMapperFactory) {
    this.ideaHierarchyManager = ideaHierarchyManager;
    this.viewModelMapperFactory = viewModelMapperFactory;
  }

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    viewModelMapperFactory
        .register(ideaHierarchyManager, IdeaGroupLive.class)
        .register(ideaHierarchyManager, IdeaGroupContainerLive.class)
        .register(ideaHierarchyManager, KigaIdeaLive.class);
  }
}
