package com.kiga.ideas.service;

import com.kiga.content.domain.ss.IdeaCategory;
import org.springframework.data.domain.Sort;

/**
 * Created by rainerh on 10.09.16.
 */
public interface IdeaCategorySortToDbSort {
  Sort mapSort(IdeaCategory ideaCategory);
}
