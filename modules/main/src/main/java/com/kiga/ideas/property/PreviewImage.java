package com.kiga.ideas.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by rainerh on 30.05.16.
 */
@Configuration
@ConfigurationProperties("ideas.previewImage")
public class PreviewImage {
  private ImageSize small;

  private ImageSize large;

  public ImageSize getSmall() {
    return small;
  }

  public void setSmall(ImageSize small) {
    this.small = small;
  }

  public ImageSize getLarge() {
    return large;
  }

  public void setLarge(ImageSize large) {
    this.large = large;
  }
}
