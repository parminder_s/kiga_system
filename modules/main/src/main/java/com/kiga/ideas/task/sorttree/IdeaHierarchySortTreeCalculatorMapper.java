package com.kiga.ideas.task.sorttree;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.IdeaGroup;
import com.kiga.content.domain.ss.IdeaGroupToIdea;
import com.kiga.content.sorttree.spec.SortTreeElement;


import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rainerh on 29.03.17.
 */
public class IdeaHierarchySortTreeCalculatorMapper {
  /**
   * maps a siteTree hierarchy to SortTreeElement.
   */
  public SortTreeElement map(IdeaCategory ideaCategory) {
    IdeaHierarchySortTreeElement returner = new IdeaHierarchySortTreeElement();

    if ("IdeaGroup".equals(ideaCategory.getClassName())) {
      IdeaGroup ideaGroup = (IdeaGroup) ideaCategory;
      List<IdeaGroupToIdea> ideaGroupToIdeas = ideaGroup.getIdeaGroupToIdeaList();
      returner.setChildren(ideaGroupToIdeas.stream().map((this::mapIdeaGroup))
        .collect(Collectors.toList()));
    } else {
      List<IdeaCategory> children = ideaCategory.getChildren();
      if (children.size() > 0) {
        returner.setChildren(children
          .stream()
          .map(this::map)
          .collect(Collectors
            .toList()));
      }
    }
    returner.setSortTree(ideaCategory);

    return returner;
  }

  private SortTreeElement mapIdeaGroup(
    IdeaGroupToIdea ideaGroupToIdea) {
    IdeaHierarchySortTreeElement returner = new IdeaHierarchySortTreeElement();
    returner.setSortTree(ideaGroupToIdea);
    return returner;
  }
}
