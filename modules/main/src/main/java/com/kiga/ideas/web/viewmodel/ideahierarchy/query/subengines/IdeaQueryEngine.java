package com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines;

import com.kiga.FeaturesProperties;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.ideas.web.viewmodel.ideahierarchy.helper.IdeaCategoryHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 16.01.2018
 */
@Service
public class IdeaQueryEngine {

  private FeaturesProperties featuresProperties;
  private NewIdeaBlockViewEngine newIdeaBlockViewEngine;
  private IdeaEngine ideaEngine;
  private NewIdeaGroupEngine newIdeaGroupEngine;
  private NewIdeaEngine newIdeaEngine;
  private IdeaCategoryHelper ideaCategoryHelper;
  private PagedIdeaEngine pagedIdeaEngine;

  /** autowired constructor. */
  @Autowired
  public IdeaQueryEngine(
      IdeaCategoryHelper ideaCategoryHelper,
      PagedIdeaEngine pagedIdeaEngine,
      IdeaEngine ideaEngine,
      NewIdeaGroupEngine newIdeaGroupEngine,
      NewIdeaEngine newIdeaEngine,
      NewIdeaBlockViewEngine newIdeaBlockViewEngine) {
    this.ideaCategoryHelper = ideaCategoryHelper;
    this.pagedIdeaEngine = pagedIdeaEngine;
    this.ideaEngine = ideaEngine;
    this.newIdeaGroupEngine = newIdeaGroupEngine;
    this.newIdeaEngine = newIdeaEngine;
    this.newIdeaBlockViewEngine = newIdeaBlockViewEngine;
  }

  /** return get Entity list depending on ideaQueryParameter. */
  public List<KigaIdea> getIdeas(IdeaQueryParameter ideaQueryParameter) {

    if (ideaCategoryHelper.isNewIdeaGroupCategory(ideaQueryParameter.getIdeaCategory())) {
      return newIdeaBlockViewEngine.getIdeas(ideaQueryParameter);
    } else if (ideaCategoryHelper.isNewIdeaGroupCategory(ideaQueryParameter.getIdeaCategory())) {
      return newIdeaEngine.getIdeas(ideaQueryParameter);
    } else if (ideaCategoryHelper.isNewIdeaGroup(ideaQueryParameter.getIdeaCategory())) {
      return newIdeaGroupEngine.getIdeas(ideaQueryParameter);
    } else if (ideaQueryParameter.isShowAll()) {
      return ideaEngine.getIdeas(ideaQueryParameter);
    } else {
      return pagedIdeaEngine.getIdeas(ideaQueryParameter);
    }
  }
}
