package com.kiga.ideas.web.converter;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.ideas.web.response.IdeasResponseGroupWithParents;
import com.kiga.web.converter.DefaultViewModelConverter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @author bbs
 * @since 5/13/16.
 */
@Service
public class IdeaCategoryToIdeasResponseGroupWithParentsConverter
  extends DefaultViewModelConverter<IdeaCategory, IdeasResponseGroupWithParents> {
  Logger logger = LoggerFactory.getLogger(getClass());

  private IdeaCategoryToIdeasResponseGroupConverter ideaCategoryToIdeasResponseGroupConverter;
  private ModelMapper modelMapper;

  @Autowired
  public IdeaCategoryToIdeasResponseGroupWithParentsConverter(
    IdeaCategoryToIdeasResponseGroupConverter ideaCategoryToIdeasResponseGroupConverter,
    ModelMapper modelMapper) {
    this.ideaCategoryToIdeasResponseGroupConverter = ideaCategoryToIdeasResponseGroupConverter;
    this.modelMapper = modelMapper;
  }

  /**
   * Convert entity and its parents as well.
   *
   * @param entity entity to be converted
   * @return viewmodel with parents
   */
  public IdeasResponseGroupWithParents convertToResponse(IdeaCategory entity) {
    IdeasResponseGroupWithParents returner =
      modelMapper.map(ideaCategoryToIdeasResponseGroupConverter.convertToResponse(entity),
        IdeasResponseGroupWithParents.class);

    returner.setParents(new ArrayList<>());

    SiteTree parentIdeaCategory = entity.getParent();
    while (parentIdeaCategory != null
      && !parentIdeaCategory.getClassName().equalsIgnoreCase("HomePage")) {
      returner.getParents().add(ideaCategoryToIdeasResponseGroupConverter
        .convertToResponse((IdeaCategory) parentIdeaCategory));
      parentIdeaCategory = parentIdeaCategory.getParent();
    }

    return returner;
  }
}
