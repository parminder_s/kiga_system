package com.kiga.ideas.web.request;

import lombok.Data;

/**
 * Created by rainerh on 16.10.16.
 */
@Data
public class IdeasCacheRequest {
  private int[] ageGroupFilters;
  private long siteTreeId;
  private boolean recursively = true;
}
