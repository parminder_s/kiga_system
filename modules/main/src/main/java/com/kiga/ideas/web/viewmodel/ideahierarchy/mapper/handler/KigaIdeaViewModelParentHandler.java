package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.handler;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.ideas.web.response.KigaIdeaViewModel;
import org.springframework.stereotype.Service;

@Service
public class KigaIdeaViewModelParentHandler {

  /** handles the fields depending on the entity's parent. */
  public void handleParent(KigaIdeaViewModel kigaIdeaViewModel, KigaIdea kigaIdea) {
    if (kigaIdea.getParent() != null) {
      kigaIdeaViewModel.setCategoryName(kigaIdea.getParent().getUrlSegment());
      kigaIdeaViewModel.setCategoryId(kigaIdea.getParent().getId());
      kigaIdeaViewModel.setParentCategoryName(kigaIdea.getParent().getParent().getUrlSegment());
      kigaIdeaViewModel.setParentCategoryId(kigaIdea.getParent().getParent().getId());
      kigaIdeaViewModel.setLevel2Title(kigaIdea.getParent().getTitle());
      kigaIdeaViewModel.setLevel1Title(kigaIdea.getParent().getParent().getTitle());
    }
  }
}
