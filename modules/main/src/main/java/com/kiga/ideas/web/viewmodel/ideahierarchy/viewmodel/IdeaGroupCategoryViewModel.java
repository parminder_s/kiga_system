package com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Florian Schneider
 * @since 09.01.17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class IdeaGroupCategoryViewModel extends IdeaHierarchyViewModel {
  private List<Category> categories;
}
