package com.kiga.ideas.service;

import com.kiga.content.domain.ss.KigaPage;
import com.kiga.ideas.web.response.IdeasResponseRating;
import org.springframework.stereotype.Service;

/**
 * Created by faxxe on 8/16/16.
 */
@Service
public class RatingCalculatorService {
  /**
   * calculates the ratings.
   */
  public IdeasResponseRating getIdeaResponseRating(KigaPage kigaPage) {
    if (kigaPage == null) {
      return new IdeasResponseRating(0f, 0);
    }

    int rating1 = kigaPage.getRating1();
    int rating2 = kigaPage.getRating2();
    int rating3 = kigaPage.getRating3();
    int rating4 = kigaPage.getRating4();
    int rating5 = kigaPage.getRating5();

    int ratingCount = rating1 + rating2 + rating3 + rating4 + rating5;
    float ratingValue = 0f;

    if (ratingCount > 0) {
      ratingValue = (rating1 + rating2 * 2 + rating3 * 3 + rating4 * 4 + rating5 * 5) / ratingCount;
    }
    if (ratingCount > 0) {
      ratingValue = Math.round(((rating1 + rating2 * 2 + rating3 * 3 + rating4 * 4 + rating5 * 5)
        / ((float)ratingCount)) * 2f) / 2f;
    }

    return new IdeasResponseRating(ratingValue, ratingCount);
  }
}
