package com.kiga.ideas.web.viewmodel.ideahierarchy.submanagers;

import com.kiga.ideas.web.response.KigaIdeaViewModel;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.KigaIdeaViewModelMapper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KigaIdeaManager {

  private KigaIdeaViewModelMapper kigaIdeaViewModelMapper;

  @Autowired
  public KigaIdeaManager(KigaIdeaViewModelMapper kigaIdeaViewModelMapper) {
    this.kigaIdeaViewModelMapper = kigaIdeaViewModelMapper;
  }

  /** get IdeaGroupViewModel. */
  public KigaIdeaViewModel getViewModel(IdeaQueryParameter ideaQueryParameter) throws Exception {

    return kigaIdeaViewModelMapper.convertToViewModel(ideaQueryParameter.getIdea());
  }
}
