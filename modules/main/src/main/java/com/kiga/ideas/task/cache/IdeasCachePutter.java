package com.kiga.ideas.task.cache;

import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.cms.web.request.ResolverRequest;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.ideas.web.viewmodel.ideahierarchy.IdeaHierarchyManager;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

/**
 * Created by rainerh on 16.10.16.
 *
 * <p>Simple class that has a method annotated with @CachePut to make sure it cannot be called by
 * another method within it's own class, e.g. access is always via the bean that has the required
 * cache functionalities setup.
 */
@Service
public class IdeasCachePutter {
  private IdeaHierarchyManager viewModelMapper;

  @Autowired
  public IdeasCachePutter(IdeaHierarchyManager viewModelMapper) {
    this.viewModelMapper = viewModelMapper;
  }

  /** actualize the cache for rendering by the CMS module. */
  @CachePut(value = "cms", key = "#resolverRequest.toString()")
  public ViewModel resolve(ResolverRequest resolverRequest, IdeaCategory ideaCategory)
      throws Exception {
    QueryRequestParameters queryRequestParameters =
        QueryRequestParameters.builder()
            .pathSegments(resolverRequest.getPathSegments())
            .ageGroup(Optional.ofNullable(resolverRequest.getAgeGroup()).orElse(0))
            .entity(ideaCategory)
            .build();

    return viewModelMapper.getViewModel(null, null, queryRequestParameters);
  }
}
