package com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.favourites.repository.FavouriteNewIdeasRepository;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.main.locale.Locale;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 24.01.18
 */
@Service
public class NewIdeaGroupEngine {

  private NewIdeasRepository newIdeasRepository;
  private FavouriteNewIdeasRepository favouriteNewIdeasRepository;

  /** Constructor. */
  @Autowired
  public NewIdeaGroupEngine(
      NewIdeasRepository newIdeasRepository,
      FavouriteNewIdeasRepository favouriteNewIdeasRepository) {
    this.newIdeasRepository = newIdeasRepository;
    this.favouriteNewIdeasRepository = favouriteNewIdeasRepository;
  }

  /** get List of KigaIdeas depending on the IdeaQueryParameters. */
  public List<KigaIdea> getIdeas(IdeaQueryParameter ideaQueryParameter) {

    final int ageGroupCode = calculateAgeGroupValue(ideaQueryParameter);
    final Locale locale = ideaQueryParameter.getLocale();
    final int ageGroupFilter = ideaQueryParameter.getAgeGroupFilter();

    return ideaQueryParameter
        .getMember()
        .map(
            member ->
                new ArrayList<>(
                    favouriteNewIdeasRepository.getIdeasForAgeGroupByBitPosition(
                        ageGroupCode, locale, ageGroupFilter, member)))
        .orElseGet(
            () ->
                new ArrayList<>(
                    newIdeasRepository.getIdeasForAgeGroupByBitPosition(
                        ageGroupCode, locale, ageGroupFilter)));
  }

  /** Getter for age group value - it's calculated from the code if this group is an ageGroup. */
  private Integer calculateAgeGroupValue(IdeaQueryParameter ideaQueryParameter) {
    String[] codeParts = ideaQueryParameter.getIdeaCategory().getCode().split("-");
    return Integer.valueOf(codeParts[codeParts.length - 1]);
  }
}
