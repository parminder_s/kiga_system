package com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kiga.content.domain.ss.property.LayoutType;
import com.kiga.ideas.web.response.IdeasResponseRating;
import com.kiga.ideas.web.response.PreviewPageResponse;
import java.util.Collection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
public class Idea {
  private Long id;
  private Boolean inPool = false;
  private String previewUrl = "";
  private String title;
  private String description;
  private boolean download;
  private String ngUrl;
  private String name;
  private String largePreviewImageUrl = "";
  private Collection<PreviewPageResponse> previewPages;

  private String level1Title;
  private String level2Title;
  private String ageRange = "";

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String audioLink;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String videoLink;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Long uploadedFileId;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String uploadedFileName;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Float elasticScore;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private IdeasResponseRating rating;

  // only ideaLinkResolver!
  private String url;
  private Boolean facebookShare = false;
  private LayoutType layoutType;
}
