package com.kiga.ideas.web.service;

import com.kiga.shop.domain.Product;
import com.kiga.shop.repository.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by peter on 16.08.17.
 */
public class ProductMetaConverter {

  /**
   * Adds metadata to a ShopProduct.
   */
  public String analyzeContent(String content, Product product) {
    Document doc = Jsoup.parse(content);
    doc.select(".container").first().children().first()
      .before("<meta name=\"productTitle\" content=\"" + product.getProductTitle() + "\"></meta>");
    return doc.select("body").html();
  }
}
