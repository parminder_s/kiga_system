package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.base;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.IdeaViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper
public interface IdeaViewModelMapper {
  IdeaViewModelMapper INSTANCE = Mappers.getMapper(IdeaViewModelMapper.class);

  @Mapping(target = "name", source = "urlSegment")
  @Mapping(target = "createDate", source = "created")
  @Mapping(target = "categoryName", source = "parent.urlSegment")
  @Mapping(target = "categoryId", source = "parent.id")
  @Mapping(target = "parentCategoryName", source = "parent.parent.urlSegment")
  @Mapping(target = "parentCategoryId", source = "parent.parent.id")
  @Mapping(target = "uploadedFileId", source = "uploadedFile.id")
  @Mapping(target = "uploadedFileName", source = "uploadedFile.name")
  @Mapping(target = "fileUrl", source = "uploadedFile.url")
  @Mapping(target = "fileName", source = "uploadedFile.name")
  @Mapping(target = "fileSize", source = "uploadedFile.size")
  @Mapping(target = "previewFileId", source = "previewImage.s3Image.id")
  @Mapping(target = "previewName", source = "previewImage.s3Image.name")
  @Mapping(target = "previewSize", source = "previewImage.s3Image.size")
  @Mapping(target = "published", ignore = true)
  @Mapping(target = "filePreviewUrl", ignore = true)
  @Mapping(target = "rating", ignore = true)
  @Mapping(target = "ageRange", ignore = true)
  @Mapping(target = "previewUrl", ignore = true)
  @Mapping(target = "largePreviewImageUrl", ignore = true)
  @Mapping(target = "url", ignore = true)
  @Mapping(target = "ngUrl", ignore = true)
  @Mapping(target = "fileProxyUrl", ignore = true)
  @Mapping(target = "resolvedType", ignore = true)
  @Mapping(target = "showBreadcrumbs", ignore = true)
  @Mapping(target = "breadcrumbs", ignore = true)
  @Mapping(target = "metadata", ignore = true)
  @Mapping(target = "permission", ignore = true)
  @Mapping(target = "level1Title", ignore = true)
  @Mapping(target = "level2Title", ignore = true)
  @Mapping(target = "previewPages", ignore = true)
  @Mapping(target = "separatedAgeGroups", ignore = true)
  @Mapping(target = "elasticScore", ignore = true)
  @Mapping(target = "audioLink", ignore = true)
  @Mapping(target = "videoLink", ignore = true)
  @Mapping(target = "state", ignore = true)
  @Mapping(target = "download", source = "articleType")
  @Mapping(target = "forParents", source = "forParents", qualifiedByName = "forParentsToForParents")
  @Mapping(
      target = "facebookShare",
      source = "facebookShare",
      qualifiedByName = "facebookShareToFacebookShare")
  IdeaViewModel kigaIdeaToIdeaViewModel(KigaIdea kigaIdea);

  @Named("facebookShareToFacebookShare")
  default Boolean facebookShareToFacebookShare(Integer facebookShare) {
    return facebookShare != null && facebookShare == 1;
  }

  @Named("forParentsToForParents")
  default Boolean forParentsToForParents(Integer forParents) {
    return forParents != null && forParents == 1;
  }

  default Boolean inPoolToInPool(long inPool) {
    return inPool == 1;
  }

  default Boolean articleTypeToDownload(ArticleType articleType) {
    return ArticleType.DOWNLOAD.equals(articleType);
  }
}
