package com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel;

import java.util.Collection;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Florian Schneider
 * @since 10.01.18
 */
@Data
@ToString
@NoArgsConstructor
public class CategoryViewModel { // IdeasResponseCategory
  private Long id;
  private String name;
  private String title;
  private String icon = "icon-ideas";
  private boolean lastCategoryLevel;
  private int level;
  private Collection<IdeaViewModel> ideas;
  private Integer sort;
  private String sortTree;
}
