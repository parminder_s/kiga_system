package com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.KigaIdeaRepositoryProxy;
import com.kiga.favourites.repository.FavouriteKigaIdeaRepository;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.service.DefaultIdeaCategorySortToDbSort;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 24.01.18
 */
@Service
public class PagedIdeaEngine {

  private IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxy;
  private IdeasProperties ideasProperties;
  private KigaIdeaRepositoryProxy kigaIdeaRepositoryProxy;
  private FavouriteKigaIdeaRepository favouriteKigaIdeaRepository;

  /** Constructor. */
  @Autowired
  public PagedIdeaEngine(
      IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxy,
      IdeasProperties ideasProperties,
      KigaIdeaRepositoryProxy kigaIdeaRepositoryProxy,
      FavouriteKigaIdeaRepository favouriteKigaIdeaRepository) {

    this.ideaCategoryRepositoryProxy = ideaCategoryRepositoryProxy;
    this.ideasProperties = ideasProperties;
    this.kigaIdeaRepositoryProxy = kigaIdeaRepositoryProxy;
    this.favouriteKigaIdeaRepository = favouriteKigaIdeaRepository;
  }

  /** get List of KigaIdeas depending on IdeaQueryParameters. */
  public List<KigaIdea> getIdeas(IdeaQueryParameter ideaQueryParameter) {
    final Long id = ideaQueryParameter.getIdeaCategory().getId();

    IdeaCategory ideaCategory = ideaQueryParameter.getIdeaCategory();

    DefaultIdeaCategorySortToDbSort defaultIdeaCategorySortToDbSort =
        new DefaultIdeaCategorySortToDbSort();
    Pageable pageable =
        new PageRequest(
            0,
            ideasProperties.getAmountPerCategory(),
            defaultIdeaCategorySortToDbSort.mapSort(ideaCategory));

    final Locale locale = ideaQueryParameter.getLocale();
    final int ageGroupFilter = ideaQueryParameter.getAgeGroupFilter();
    final String sortTree = ideaQueryParameter.getIdeaCategory().getSortTree();
    final Optional<Member> member = ideaQueryParameter.getMember();

    // check if we retrieve a favourite idea
    if (member.isPresent()) {
      if (ideasProperties.isUseSortTree()) {
        return favouriteKigaIdeaRepository.findPagedForGroupSortTree(
            locale, sortTree, ageGroupFilter, member.get(), pageable);
      } else {
        return favouriteKigaIdeaRepository.findPagedForGroup(
            locale, id, 0, ageGroupFilter, member.get());
      }
    }
    if (ideasProperties.isUseSortTree()) {
      return kigaIdeaRepositoryProxy.findPagedForGroupSortTree(
          locale, sortTree, ageGroupFilter, pageable);
    } else {
      return kigaIdeaRepositoryProxy.findPagedForGroup(locale, id, 0, ageGroupFilter, pageable);
    }
  }
}
