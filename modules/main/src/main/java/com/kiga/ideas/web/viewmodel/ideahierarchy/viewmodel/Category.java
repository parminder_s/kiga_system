package com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Collection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
public class Category {
  @JsonIgnore Long id;

  private String name;
  private String title;
  private String icon = "icon-ideas";
  private boolean lastCategoryLevel;
  private Collection<Idea> ideas;
  private boolean ageGroup;
  private boolean ageGroupContainer;
  private boolean disableForFavourites;

  @JsonIgnore // todo send this instead of ageGroup
  private String code;

  /**
   * setter for code, additionally sets indicator of ageGroup (and agrGroupContainer) if it's one of
   * these types.
   */
  public void setCode(String code) {
    this.code = code;
    ageGroupContainer = false;
    ageGroup = false;

    if (StringUtils.isNotBlank(code)) {
      ageGroupContainer = code.equalsIgnoreCase("new-ideas");
      ageGroup = code.matches("^new-ideas-ig-[0-9]{1,2}$");
    }
  }
}
