package com.kiga.ideas.web.response;


/**
 * Created by faxxe on 8/10/16.
 */
public class IdeasResponseRating {
  private float rating;
  private int count;
  private int userRating;

  public IdeasResponseRating(float rating, int count) {
    this.rating = rating;
    this.count = count;
  }

  public IdeasResponseRating() {
  }

  public float getRating() {
    return rating;
  }

  public void setRating(float rating) {
    this.rating = rating;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public int getUserRating() {
    return userRating;
  }

  public void setUserRating(int userRating) {
    this.userRating = userRating;
  }
}
