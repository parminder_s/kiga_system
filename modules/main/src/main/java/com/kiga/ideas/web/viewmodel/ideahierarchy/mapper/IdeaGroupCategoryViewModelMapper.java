package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import com.kiga.cms.web.response.RedirectViewModel;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.web.viewmodel.ideahierarchy.helper.IdeaCategoryHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaSet;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.Category;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.IdeaGroupCategoryViewModel;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 09.01.17
 */
@Service
public class IdeaGroupCategoryViewModelMapper {
  private IdeaCategoryHelper ideaCategoryHelper;
  private CategoryMapper categoryMapper;
  private IdeaMapper ideaMapper;
  private ParentCategoryViewModelMapper parentCategoryViewModelMapper;
  private IdeaCategoryRepositoryProxy ideaCategoryRepository;
  private SiteTreeUrlGenerator siteTreeUrlGenerator;
  private ViewModelMetaDataProvider viewModelMetaDataProvider;

  /** Constructor. */
  @Autowired
  public IdeaGroupCategoryViewModelMapper(
      IdeaCategoryHelper ideaCategoryHelper,
      ViewModelMetaDataProvider viewModelMetaDataProvider,
      SiteTreeUrlGenerator siteTreeUrlGenerator,
      IdeaCategoryRepositoryProxy ideaCategoryRepository,
      ParentCategoryViewModelMapper parentCategoryViewModelMapper,
      IdeaMapper ideaMapper,
      CategoryMapper categoryMapper) {
    this.ideaCategoryHelper = ideaCategoryHelper;
    this.viewModelMetaDataProvider = viewModelMetaDataProvider;
    this.siteTreeUrlGenerator = siteTreeUrlGenerator;
    this.ideaCategoryRepository = ideaCategoryRepository;
    this.parentCategoryViewModelMapper = parentCategoryViewModelMapper;
    this.ideaMapper = ideaMapper;
    this.categoryMapper = categoryMapper;
  }

  /** Return the ViewModel of the described IdeaGroupEntity. */
  public ViewModel getViewModel(
      boolean favourites,
      int ageGroupFilter,
      IdeaCategory activeGroup,
      List<IdeaSet> ideaSets,
      List<IdeaCategory> siblings) {
    IdeaGroupCategoryViewModel ideaGroupCategoryViewModel = new IdeaGroupCategoryViewModel();

    // map active Group
    Category groupViewModel = categoryMapper.convertIdeaCategory(activeGroup);
    ideaGroupCategoryViewModel.setActiveCategory(groupViewModel);

    // map ideaSets
    ideaGroupCategoryViewModel.setCategories(
        ideaSets
            .stream()
            .map(
                ideaSet -> {
                  Category viewModel =
                      categoryMapper.convertIdeaCategory(ideaSet.getIdeaCategory());
                  viewModel.setIdeas(
                      ideaSet
                          .getIdeas()
                          .stream()
                          .map(ideaMapper::convert)
                          .collect(Collectors.toList()));
                  return viewModel;
                })
            .collect(Collectors.toList()));

    // map siblings
    if (siblings != null) {
      ideaGroupCategoryViewModel.setParentCategories(
          parentCategoryViewModelMapper.convertSiblings(siblings));

      // set first level category name for back button
      if (!activeGroup.getClassName().equals("IdeaGroupMainContainer")
          && !"IdeaGroupMainContainer".equals(activeGroup.getParent().getClassName())) {
        ideaGroupCategoryViewModel.setFirstLevelCategoryName(activeGroup.getTitle());
        SiteTree parent = activeGroup.getParent();

        while (parent != null && !"IdeaGroupMainContainer".equals(parent.getClassName())) {
          ideaGroupCategoryViewModel.setFirstLevelCategoryName(parent.getTitle());
          parent = parent.getParent();
        }
      }
    }

    if (ideaCategoryHelper.isRedirect(activeGroup) && !favourites) {
      RedirectViewModel returner = new RedirectViewModel();
      String redirectTo =
          ideaGroupCategoryViewModel
              .getCategories()
              .stream()
              .skip(1)
              .findFirst()
              .map(Category::getId)
              .map(ideaCategoryRepository::findOne)
              .map(siteTreeUrlGenerator::getRelativeNgUrl)
              .get();
      if (ageGroupFilter > 0) {
        redirectTo += "?ageGroupFilter=" + ageGroupFilter;
      }

      returner.setRedirectTo(redirectTo);
      return returner;
    }

    viewModelMetaDataProvider.fillWithMetaDataAndBreadcrumbs(
        ideaGroupCategoryViewModel, activeGroup);
    return ideaGroupCategoryViewModel;
  }
}
