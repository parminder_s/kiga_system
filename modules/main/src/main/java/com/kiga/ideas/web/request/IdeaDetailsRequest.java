package com.kiga.ideas.web.request;

import com.kiga.web.message.EndpointRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author asv
 * @since 23/06/16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class IdeaDetailsRequest extends EndpointRequest {
  private String urlSegment;
  private Long id;
}
