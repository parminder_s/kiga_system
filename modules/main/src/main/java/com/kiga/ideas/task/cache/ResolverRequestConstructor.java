package com.kiga.ideas.task.cache;

import com.kiga.cms.web.request.ResolverRequest;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.main.locale.LocaleConverter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by rainerh on 09.12.16.
 */
public class ResolverRequestConstructor {
  /**
   * generates the ResolverRequest constructor for a given SiteTree element and ageGroup.
   */
  public ResolverRequest fromIdeasGroupsListRequest(SiteTree ideaCategory, int ageGroup) {
    ArrayList<String> urlSegments = new ArrayList<>();
    SiteTree parent = ideaCategory;
    while (!parent.getClassName().equals("IdeaGroupMainContainer")) {
      urlSegments.add(parent.getUrlSegment());
      parent = parent.getParent();
    }
    urlSegments.add(parent.getUrlSegment());
    Collections.reverse(urlSegments);

    ResolverRequest returner = new ResolverRequest();
    returner.setPathSegments(urlSegments.toArray(new String[0]));
    returner.setAgeGroup(ageGroup);
    returner.setLocale(LocaleConverter.toLocale(ideaCategory.getLocale().toString()));

    return returner;
  }
}
