package com.kiga.ideas;

import com.kiga.ideas.property.PreviewImage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by rainerh on 09.05.16.
 */
@Configuration
@ConfigurationProperties("ideas")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdeasProperties {
  private int amountPerCategory;

  private PreviewImage previewImage;

  private PreviewImage printPreviewImage;

  private boolean siblingsAsMenu;

  private boolean useSortTree;
}
