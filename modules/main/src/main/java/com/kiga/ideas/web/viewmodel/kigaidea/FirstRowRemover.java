package com.kiga.ideas.web.viewmodel.kigaidea;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by asv on 05.01.17.
 */
public class FirstRowRemover {

  /**
   * Remove row.
   */
  public String analyzeContent(String content) {
    Document doc = Jsoup.parse(content);
    if (doc.select(".rowDiv").first() != null) {
      int cellCount = doc.select(".rowDiv").first().nextElementSibling().children().size();
      if (cellCount == 2) {
        Elements row = doc.select(".rowDiv").first().nextElementSibling().children();
        Element firstCell = row.first();
        Element secondCell = row.first().nextElementSibling();
        if (firstCell.select("img").size() == 1 && secondCell.select("img").size() == 0) {
          if ((firstCell.hasClass("span-1") && secondCell.hasClass("span-3"))
            || (firstCell.hasClass("span-2") && secondCell.hasClass("span-2"))
            || (firstCell.hasClass("span-3") && secondCell.hasClass("span-1"))) {
            doc.select(".rowDiv").first().nextElementSibling().remove();
            doc.select(".rowDiv").first().remove();
            content = doc.select("body").html();
          }
        }
      }
    }
    return content;
  }
}
