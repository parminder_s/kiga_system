package com.kiga.ideas.web.response;


/**
 * @author asv
 * @since 21/10/16.
 */
public class IdeasResponseLinkedIdea {
  private Long id;
  private String title;
  private String name;
  private String description;
  private String previewUrl;
  private String largePreviewImageUrl;
  private IdeasResponseRating ideasResponseRating;
  private String url;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getPreviewUrl() {
    return previewUrl;
  }

  public void setPreviewUrl(String previewUrl) {
    this.previewUrl = previewUrl;
  }

  public String getLargePreviewImageUrl() {
    return largePreviewImageUrl;
  }

  public void setLargePreviewImageUrl(String largePreviewImageUrl) {
    this.largePreviewImageUrl = largePreviewImageUrl;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public IdeasResponseRating getIdeasResponseRating() {
    return ideasResponseRating;
  }

  public void setIdeasResponseRating(IdeasResponseRating ideasResponseRating) {
    this.ideasResponseRating = ideasResponseRating;
  }

}
