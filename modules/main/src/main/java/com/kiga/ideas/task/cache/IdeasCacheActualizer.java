package com.kiga.ideas.task.cache;

import com.kiga.cms.web.request.ResolverRequest;
import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.ideas.web.request.IdeasGroupListRequest;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by rainerh on 14.10.16.
 */
public class IdeasCacheActualizer {
  IdeasCachePutter ideasCachePutter;
  IdeaGroupCollector ideaGroupCollector;
  ResolverRequestConstructor resolverRequestConstructor;
  Logger logger;

  /**
   * inject beans.
   */
  public IdeasCacheActualizer(IdeasCachePutter ideasCachePutter) {
    this.ideasCachePutter = ideasCachePutter;
    this.ideaGroupCollector = new IdeaGroupCollector();
    this.resolverRequestConstructor = new ResolverRequestConstructor();
    this.logger = LoggerFactory.getLogger(getClass());
  }

  /**
   * Actualize beginning from SiteTree element downwards all children
   * until an IdeaGroup is reached.
   */
  public void run(IdeaCategory ideaCategory, int ageGroupFilter, boolean recursively) {
    ideaGroupCollector
      .collect(ideaCategory, recursively)
      .parallelStream()
      .forEach(st -> {
        try {
          ResolverRequest request = resolverRequestConstructor
            .fromIdeasGroupsListRequest(st, ageGroupFilter);
          this.ideasCachePutter.resolve(request, (IdeaCategory)st);
        } catch (Exception exception) {
          logger.error("exception when actualizing ideas cache for " + st.getUrlSegment()
            + "/" + st.getId());
        }
      });
  }
}
