package com.kiga.ideas.service;

import org.springframework.scheduling.annotation.Async;

/**
 * Created by rainerh on 18.06.16.
 */
public interface PrintPreviewImageActualizer {
  @Async
  void actualizePrintPreviews();
}
