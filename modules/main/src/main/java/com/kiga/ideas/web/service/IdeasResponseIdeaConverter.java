package com.kiga.ideas.web.service;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.KigaPrintPreviewImage;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.service.RatingCalculatorService;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.ideas.web.response.PreviewPageResponse;
import com.kiga.ideas.web.viewmodel.kigaidea.IdeasAgeRangeConverter;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.web.converter.DefaultViewModelConverter;
import com.kiga.web.service.DefaultUrlGenerator;
import com.kiga.web.service.UrlGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 4/28/16.
 */
@Service
public class IdeasResponseIdeaConverter extends
  DefaultViewModelConverter<KigaIdea, IdeasViewModelIdea> {
  private static final String PROXY_DOWNLOAD_PATH_PREFIX = "/idea/downloadable/download/";

  private Logger logger = LoggerFactory.getLogger(getClass());
  private S3ImageResizeService s3ImageResizeService;
  private IdeasProperties ideasProperties;
  private SiteTreeUrlGenerator siteTreeUrlGenerator;
  private UrlGenerator urlGenerator;
  private RatingCalculatorService ratingCalculatorService;
  private IdeasSeparateAgeGroupsConverter ideasSeparateAgeGroupsConverter;

  /**
   * inject dependencies.
   */
  @Autowired
  public IdeasResponseIdeaConverter(S3ImageResizeService s3ImageResizeService,
    IdeasProperties ideasProperties,
    SiteTreeUrlGenerator siteTreeUrlGenerator,
    DefaultUrlGenerator urlGenerator, RatingCalculatorService ratingCalculatorService,
    IdeasSeparateAgeGroupsConverter ideasSeparateAgeGroupsConverter) {
    this.s3ImageResizeService = s3ImageResizeService;
    this.ideasProperties = ideasProperties;
    this.siteTreeUrlGenerator = siteTreeUrlGenerator;
    this.urlGenerator = urlGenerator;
    this.ratingCalculatorService = ratingCalculatorService;
    this.ideasSeparateAgeGroupsConverter = ideasSeparateAgeGroupsConverter;
  }

  @Override
  public IdeasViewModelIdea convertToResponse(KigaIdea idea) {
    IdeasViewModelIdea returner = new IdeasViewModelIdea();

    if (idea instanceof KigaIdeaLive) {
      returner.setPublished(true);
    } else if (idea instanceof KigaIdeaDraft) {
      returner.setPublished(false);
    }

    IdeasAgeRangeConverter converter = new IdeasAgeRangeConverter();
    MediaLinksSetter mediaLinksSetter = new MediaLinksSetter();

    mediaLinksSetter.setMediaLinks(returner, idea.getContent());
    returner.setDownload(ArticleType.DOWNLOAD.equals(idea.getArticleType()));
    returner.setId(idea.getId());
    returner.setTitle(idea.getTitle());
    returner.setName(idea.getUrlSegment());
    returner.setUrlSegment(idea.getUrlSegment());
    returner.setDescription(idea.getDescription());
    returner.setCreateDate(idea.getCreated());
    returner.setLastEdited(idea.getLastEdited());
    returner.setLocale(idea.getLocale());

    if (idea.getFacebookShare() != null) {
      returner.setFacebookShare(idea.getFacebookShare() == 1);
    }

    if (idea.getInPool() != null) {
      returner.setInPool(idea.getInPool() == 1);
    }

    if (idea.getForParents() != null) {
      returner.setForParents(idea.getForParents() == 1);
    }

    if (idea.getParent() != null) {
      returner.setCategoryName(idea.getParent().getUrlSegment());
      returner.setCategoryId(idea.getParent().getId());
      returner.setParentCategoryName(idea.getParent().getParent().getUrlSegment());
      returner.setParentCategoryId(idea.getParent().getParent().getId());
    }

    KigaS3File file = idea.getUploadedFile();
    if (file != null) {
      returner.setUploadedFileId(file.getId());
      returner.setUploadedFileName(file.getName());
      String proxyDownloadUrl = PROXY_DOWNLOAD_PATH_PREFIX + idea.getId();
      returner.setFileProxyUrl(urlGenerator.getBaseUrl(proxyDownloadUrl));
      returner.setFileUrl(file.getUrl());
      returner.setFileName(file.getName());
      returner.setFileSize(file.getSize());

      if (file instanceof S3Image) {
        try {
          ImageSize smallPreview = ideasProperties.getPreviewImage().getSmall();
          returner.setFilePreviewUrl(createPreview((S3Image) file, smallPreview));
        } catch (IOException exception) {
          logger.error("Problem with cached (or caching) size variants of image: " + file.getName(),
            exception);
        }
      }
    }

    setPreviewImages(returner, idea.getPreviewImage());
    setKigaPrintPreviewImages(returner, idea.getKigaPrintPreviewImages());

    returner.setRating(ratingCalculatorService.getIdeaResponseRating(idea));

    if (idea.getAgeGroup() != null) {
      returner.setAgeRange(converter.getAgeRange(idea.getAgeGroup()));
      returner.setSeparatedAgeGroups(ideasSeparateAgeGroupsConverter.convert(idea.getAgeGroup()));
    }

    SiteTree<? extends SiteTree> parent = idea.getParent();
    if (parent != null) {
      returner.setLevel2Title(parent.getTitle());
      returner.setLevel1Title(parent.getParent().getTitle());
    }

    returner.setUrl(siteTreeUrlGenerator.getRelativeUrl(idea));
    returner.setNgUrl(siteTreeUrlGenerator.getRelativeNgUrl(idea));

    returner.setLayoutType(idea.getLayoutType());

    returner.setSortMonth(idea.getSortMonth());
    returner.setSortYear(idea.getSortYear());
    returner.setSortDayMini(idea.getSortDayMini());
    returner.setSortMonthMini(idea.getSortMonthMini());
    returner.setSortYearMini(idea.getSortYearMini());

    return returner;
  }

  private void setPreviewImages(IdeasViewModelIdea ideasResponseIdea, KigaPageImage previewImage) {
    if (previewImage != null) {
      S3Image s3Image = previewImage.getS3Image();
      ideasResponseIdea.setPreviewFileId(s3Image.getId());
      ideasResponseIdea.setPreviewName(s3Image.getName());
      ideasResponseIdea.setPreviewSize(s3Image.getSize());
      try {
        ImageSize smallPreview = ideasProperties.getPreviewImage().getSmall();
        ImageSize largePreview = ideasProperties.getPreviewImage().getLarge();
        ideasResponseIdea.setPreviewUrl(createPreview(s3Image, smallPreview));
        ideasResponseIdea.setLargePreviewImageUrl(createPreview(s3Image, largePreview));
      } catch (IOException ex) {
        logger.error("Problem with cached (or caching) size variants of image: " + s3Image
          .getName(), ex);
      } catch (IllegalArgumentException ex) {
        logger.error("Wrong arguments passed to the strategy class", ex);
      }
    }
  }

  private String createPreview(S3Image s3Image, ImageSize previewSize) throws IOException {
    return s3ImageResizeService.getResizedImageUrl(s3Image,
      new PaddedImageStrategy(previewSize.getWidth(), previewSize.getHeight()));
  }

  private void setKigaPrintPreviewImages(
    IdeasViewModelIdea ideasResponseIdea, List<KigaPrintPreviewImage> kigaPrintPreviewImages) {
    if (kigaPrintPreviewImages == null) {
      return;
    }

    ideasResponseIdea.setPreviewPages(kigaPrintPreviewImages.stream()
      .map(image -> {
        PreviewPageResponse previewPageResponse = new PreviewPageResponse();
        try {
          ImageSize smallPreview = ideasProperties.getPrintPreviewImage().getSmall();
          ImageSize largePreview = ideasProperties.getPrintPreviewImage().getLarge();
          previewPageResponse.setSmall(createPreview(image.getS3Image(), smallPreview));
          previewPageResponse.setLarge(createPreview(image.getS3Image(), largePreview));
          previewPageResponse.setPageNumber(image.getPageNumber());
          return previewPageResponse;
        } catch (IOException ioe) {
          ioe.printStackTrace();
        }
        return null;
      })
      .filter(page -> page != null)
      .collect(Collectors.toList()));
  }
}
