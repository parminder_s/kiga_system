package com.kiga.ideas.web.viewmodel.kigaidea;

import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.service.SiteTreeUrlGenerator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by asv on 06.01.17.
 */
public class LinksReplacer {
  private SiteTreeLiveRepository siteTreeRepository;
  private SiteTreeUrlGenerator urlGenerator;

  public LinksReplacer(SiteTreeLiveRepository siteTreeRepository,
                       SiteTreeUrlGenerator urlGenerator) {
    this.siteTreeRepository = siteTreeRepository;
    this.urlGenerator = urlGenerator;
  }

  /**
   * Replace links.
   */
  public String analyzeContentLinks(String content) {
    Document doc = Jsoup.parse(content);
    Elements links = doc.select("a[href~=\\[sitetree_link id=\\d+\\]]");
    for (Element link : links) {
      String linkText = link.toString();
      Pattern pattern = Pattern.compile("\\[sitetree_link id=(\\d+)\\]");
      Matcher matcher = pattern.matcher(linkText);
      if (matcher.find()) {
        Long id = Long.parseLong(matcher.group(1));
        SiteTreeLive siteTree = siteTreeRepository.findOne(id);
        if (siteTree != null) {
          String relativeUrl = urlGenerator.getRelativeNgUrl(siteTree);
          link.attr("href", relativeUrl);
        } else {
          link.attr("href", "");
        }
      }
    }
    return doc.select("body").html();
  }
}

