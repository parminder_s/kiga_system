package com.kiga.ideas.web.response;

import lombok.Data;

/**
 * @author bbs
 * @since 4/9/17.
 */
@Data
public class UrlSegmentValidationResponse {
  private boolean valid;
  private String message;
}
