package com.kiga.ideas.web.viewmodel.kigaidea;

import com.kiga.s3.domain.S3Image;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by peter on 18.04.17.
 */
public class PinterestTagAdder {
  public PinterestTagAdder() {
  }

  /**
   * Replace links.
   */
  public String analyzeContentImages(String content, List<S3Image> images) {
    Document doc = Jsoup.parse(content);
    Elements links = doc.select("img");
    links.stream().forEach(link -> {
      S3Image image = getCorrectS3Image(images, link.attr("src"));
      if (image != null && image.getCachedDataJson().containsKey("wm2_w650")) {
        link.attr("data-pin-media", image.getCachedDataJson().get("wm2_w650"));
      } else if (image != null && image.getCachedDataJson().containsKey("w650")) {
        link.attr("nopin", "nopin");
      }
    });
    return doc.select("body").html();
  }

  private S3Image getCorrectS3Image(List<S3Image> images, String url) {
    List<S3Image> filtered = images.stream()
      .filter(image -> image.getCachedDataJson() != null
        && image.getCachedDataJson().containsValue(url))
      .collect(Collectors.toList());
    return filtered.size() == 0 ? null : filtered.get(0);
  }
}
