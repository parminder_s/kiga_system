package com.kiga.ideas.web.response;

import com.kiga.main.locale.Locale;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * @author bbs
 * @since 5/13/16.
 */
public class IdeasResponseGroup extends IdeasResponseCategory {
  private boolean ideaGroup;
  private Locale locale;
  private String urlSegment;
  private List<String> urlSegmentsPath;
  private String code;
  private boolean ageGroup;
  private boolean ageGroupContainer;
  private boolean disableForFavourites;

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  public String getUrlSegment() {
    return urlSegment;
  }

  public void setUrlSegment(String urlSegment) {
    this.urlSegment = urlSegment;
  }

  public boolean isIdeaGroup() {
    return ideaGroup;
  }

  public void setIdeaGroup(boolean ideaGroup) {
    this.ideaGroup = ideaGroup;
  }

  public String getCode() {
    return code;
  }

  /**
   * setter for code, additionally sets indicator of ageGroup (and agrGroupContainer) if it's one
   * of these types.
   *
   * @param code code
   */
  public void setCode(String code) {
    this.code = code;
    ageGroupContainer = false;
    ageGroup = false;

    if (StringUtils.isNotBlank(code)) {
      ageGroupContainer = code.equalsIgnoreCase("new-ideas");
      ageGroup = code.matches("^new-ideas-ig-[0-9]{1,2}$");
    }
  }

  public boolean isAgeGroup() {
    return ageGroup;
  }

  public boolean isAgeGroupContainer() {
    return ageGroupContainer;
  }

  /**
   * Getter for age group value - it's calculated from the code if this group is an ageGroup.
   *
   * @return age group value (1, 2, 4, 8 or 16)
   */
  public Integer calculateAgeGroupValue() {
    if (ageGroup) {
      String[] codeParts = code.split("-");
      return Integer.valueOf(codeParts[codeParts.length - 1]);
    }

    return null;
  }

  public void setAgeGroup(boolean ageGroup) {
    this.ageGroup = ageGroup;
  }

  public void setAgeGroupContainer(boolean ageGroupContainer) {
    this.ageGroupContainer = ageGroupContainer;
  }

  public boolean isDisableForFavourites() {
    return disableForFavourites;
  }

  public void setDisableForFavourites(boolean disableForFavourites) {
    this.disableForFavourites = disableForFavourites;
  }
}
