package com.kiga.ideas.web.viewmodel.ideahierarchy;

import com.kiga.cms.service.ViewModelMapper;
import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.ideas.web.viewmodel.ideahierarchy.builder.IdeaQueryParameterBuilder;
import com.kiga.ideas.web.viewmodel.ideahierarchy.helper.IdeaCategoryHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.ideas.web.viewmodel.ideahierarchy.submanagers.IdeaGroupCategoryManager;
import com.kiga.ideas.web.viewmodel.ideahierarchy.submanagers.IdeaGroupManager;
import com.kiga.ideas.web.viewmodel.ideahierarchy.submanagers.KigaIdeaManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 09.01.17
 */
@Service
public class IdeaHierarchyManager implements ViewModelMapper<IdeaCategory, ViewModel> {
  private KigaIdeaManager kigaIdeaManager;
  private IdeaQueryParameterBuilder ideaQueryParameterBuilder;
  private IdeaGroupCategoryManager ideaGroupCategoryManager;
  private IdeaGroupManager ideaGroupManager;
  private IdeaCategoryHelper ideaCategoryHelper;

  /** Constructor. */
  @Autowired
  public IdeaHierarchyManager(
      KigaIdeaManager kigaIdeaManager,
      IdeaQueryParameterBuilder ideaQueryParameterBuilder,
      IdeaGroupCategoryManager ideaGroupCategoryManager,
      IdeaGroupManager ideaGroupManager,
      IdeaCategoryHelper ideaCategoryHelper) {
    this.kigaIdeaManager = kigaIdeaManager;
    this.ideaQueryParameterBuilder = ideaQueryParameterBuilder;
    this.ideaGroupCategoryManager = ideaGroupCategoryManager;
    this.ideaGroupManager = ideaGroupManager;
    this.ideaCategoryHelper = ideaCategoryHelper;
  }

  /**
   * Build the corresponding View Model of the queried Entity that is either IdeaGroup or
   * IdeaGroupCategory return value is either KigaIdeaViewModel, IdeaGroupViewModel,
   * IdeaGroupCategoryViewModel or RedirectViewModel.
   */
  @Override
  public ViewModel getViewModel(
      HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse,
      QueryRequestParameters<IdeaCategory> queryRequestParameters)
      throws Exception {

    IdeaQueryParameter ideaQueryParameter =
        ideaQueryParameterBuilder.buildQuery(queryRequestParameters);

    // If KigaIdea
    if (ideaQueryParameter.getIdea() != null) {
      return kigaIdeaManager.getViewModel(ideaQueryParameter);
      // If IdeaGroup
    } else if (ideaCategoryHelper.isLastCategoryLevel(ideaQueryParameter.getIdeaCategory())) {
      return ideaGroupManager.getViewModel(ideaQueryParameter);
    }
    // IdeaGroupCategory
    return ideaGroupCategoryManager.getViewModel(ideaQueryParameter);
  }
}
