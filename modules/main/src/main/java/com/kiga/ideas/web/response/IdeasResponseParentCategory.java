package com.kiga.ideas.web.response;


/**
 * Created by asv on 14/03/17.
 *
 */
public class IdeasResponseParentCategory {
  private String name;
  private String title;
  private String icon = "icon-ideas";
  private boolean ageGroup;
  private boolean ageGroupContainer;
  private boolean disableForFavourites;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public boolean isDisableForFavourites() {
    return disableForFavourites;
  }

  public void setDisableForFavourites(boolean disableForFavourites) {
    this.disableForFavourites = disableForFavourites;
  }

  public boolean isAgeGroup() {
    return ageGroup;
  }

  public void setAgeGroup(boolean ageGroup) {
    this.ageGroup = ageGroup;
  }

  public boolean isAgeGroupContainer() {
    return ageGroupContainer;
  }

  public void setAgeGroupContainer(boolean ageGroupContainer) {
    this.ageGroupContainer = ageGroupContainer;
  }

}
