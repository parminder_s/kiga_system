package com.kiga.ideas.web.service;

import com.kiga.content.domain.ss.live.KigaPageLive;
import com.kiga.content.repository.ss.draft.KigaIdeaRepositoryDraft;
import com.kiga.content.repository.ss.live.KigaPageRepositoryLive;
import com.kiga.ideas.web.response.IdeasResponseIdeaDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author bbs
 * @since 7/25/16.
 */
@Service
public class IdeaDetailsService {
  IdeasResponseIdeaDetailsConverter converter;
  private KigaIdeaRepositoryDraft kigaIdeaRepository;
  private KigaPageRepositoryLive kigaPageRepositoryLive;

  /**
   * @author asv
   * @since 9/26/16.
   */
  @Autowired
  public IdeaDetailsService(
    IdeasResponseIdeaDetailsConverter converter,
    KigaIdeaRepositoryDraft kigaIdeaRepository, KigaPageRepositoryLive kigaPageRepositoryLive) {
    this.converter = converter;
    this.kigaIdeaRepository = kigaIdeaRepository;
    this.kigaPageRepositoryLive = kigaPageRepositoryLive;
  }

  public IdeasResponseIdeaDetails getIdeaDetails(Long id) {
    return converter.convertToResponse(kigaIdeaRepository.findOne(id));
  }

  /**
   * Find the KigaPage entity.
   */
  public KigaPageLive getKigaPage(Long id) {
    return kigaPageRepositoryLive.findOne(id);
  }

}
