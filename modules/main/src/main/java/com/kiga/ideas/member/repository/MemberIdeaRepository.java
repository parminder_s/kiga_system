package com.kiga.ideas.member.repository;

import com.kiga.ideas.member.domain.MemberIdea;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 22.08.16.
 */
public interface MemberIdeaRepository extends CrudRepository<MemberIdea, Long> {
}
