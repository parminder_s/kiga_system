package com.kiga.ideas.property;

/**
 * Created by rainerh on 30.05.16.
 */
public class ImageSize {
  private int width;
  private int height;

  public ImageSize() {}

  public ImageSize(int width, int height) {
    this.width = width;
    this.height = height;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }
}
