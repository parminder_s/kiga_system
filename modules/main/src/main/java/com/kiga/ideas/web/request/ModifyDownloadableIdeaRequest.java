package com.kiga.ideas.web.request;

import com.kiga.ideas.web.response.IdeasResponseGroup;
import com.kiga.main.locale.Locale;
import lombok.Data;

import java.util.List;

/**
 * @author bbs
 * @since 4/5/17.
 */
@Data
public class ModifyDownloadableIdeaRequest {
  private Long id;
  private Locale locale;
  private String title;
  private String description;
  private Integer ageGroup;
  private Long categoryId;
  private String urlSegment;
  private boolean inPool;
  private boolean forParents;
  private Integer sortMonth;
  private Integer sortYear;
  private Integer sortDayMini;
  private Integer sortMonthMini;
  private Integer sortYearMini;
  private boolean facebookShare;
  private Long previewFileId;
  private Long uploadedFileId;
  private List<IdeasResponseGroup> ideaGroups;
  private Long translationGroupId;
}
