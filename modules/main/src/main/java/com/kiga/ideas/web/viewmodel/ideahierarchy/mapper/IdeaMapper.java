package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.KigaPrintPreviewImage;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.service.RatingCalculatorService;
import com.kiga.ideas.web.response.PreviewPageResponse;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.base.MinimizedIdeaViewModelMapper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.handler.MediaLinksHandler;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.Idea;
import com.kiga.ideas.web.viewmodel.kigaidea.IdeasAgeRangeConverter;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 16.01.2018.
 */
@Service
@Log4j2
public class IdeaMapper {

  private S3ImageResizeService s3ImageResizeService;
  private IdeasProperties ideasProperties;
  private SiteTreeUrlGenerator siteTreeUrlGenerator;
  private RatingCalculatorService ratingCalculatorService;
  private MediaLinksHandler mediaLinksHandler;

  @Autowired
  public IdeaMapper(
      S3ImageResizeService s3ImageResizeService,
      IdeasProperties ideasProperties,
      SiteTreeUrlGenerator siteTreeUrlGenerator,
      RatingCalculatorService ratingCalculatorService) {
    this.s3ImageResizeService = s3ImageResizeService;
    this.ideasProperties = ideasProperties;
    this.siteTreeUrlGenerator = siteTreeUrlGenerator;
    this.ratingCalculatorService = ratingCalculatorService;
    this.mediaLinksHandler = new MediaLinksHandler();
  }

  public Idea convert(KigaIdea kigaIdea) {
    Idea ideaViewModel = MinimizedIdeaViewModelMapper.INSTANCE.kigaIdeaToIdeaViewModel(kigaIdea);

    IdeasAgeRangeConverter ideasAgeRangeConverter = new IdeasAgeRangeConverter();
    mediaLinksHandler.setMediaLinks(ideaViewModel, kigaIdea.getContent());
    setPreviewImages(ideaViewModel, kigaIdea.getPreviewImage());
    setKigaPrintPreviewImages(ideaViewModel, kigaIdea.getKigaPrintPreviewImages());

    ideaViewModel.setRating(ratingCalculatorService.getIdeaResponseRating(kigaIdea));

    if (kigaIdea.getAgeGroup() != null) {
      ideaViewModel.setAgeRange(ideasAgeRangeConverter.getAgeRange(kigaIdea.getAgeGroup()));
    }

    SiteTree<? extends SiteTree> parent = kigaIdea.getParent();
    if (parent != null) {
      ideaViewModel.setLevel2Title(parent.getTitle());
      ideaViewModel.setLevel1Title(parent.getParent().getTitle());
    }
    ideaViewModel.setLayoutType(kigaIdea.getLayoutType());
    ideaViewModel.setNgUrl(siteTreeUrlGenerator.getRelativeNgUrl(kigaIdea));
    ideaViewModel.setUrl(siteTreeUrlGenerator.getRelativeUrl(kigaIdea));
    ideaViewModel.setName(kigaIdea.getUrlSegment());
    return ideaViewModel;
  }

  private void setPreviewImages(Idea ideaViewModel, KigaPageImage previewImage) {
    if (previewImage != null) {
      S3Image s3Image = previewImage.getS3Image();
      try {
        ImageSize smallPreview = ideasProperties.getPreviewImage().getSmall();
        ImageSize largePreview = ideasProperties.getPreviewImage().getLarge();
        ideaViewModel.setPreviewUrl(createPreview(s3Image, smallPreview));
        ideaViewModel.setLargePreviewImageUrl(createPreview(s3Image, largePreview));
      } catch (IOException ex) {
        log.error(
            "Problem with cached (or caching) size variants of image: " + s3Image.getName(), ex);
      } catch (IllegalArgumentException ex) {
        log.error("Wrong arguments passed to the strategy class", ex);
      }
    }
  }

  private String createPreview(S3Image s3Image, ImageSize previewSize) throws IOException {
    return s3ImageResizeService.getResizedImageUrl(
        s3Image, new PaddedImageStrategy(previewSize.getWidth(), previewSize.getHeight()));
  }

  private void setKigaPrintPreviewImages(
      Idea ideaViewModel, List<KigaPrintPreviewImage> kigaPrintPreviewImages) {
    if (kigaPrintPreviewImages == null) {
      return;
    }

    ideaViewModel.setPreviewPages(
        kigaPrintPreviewImages
            .stream()
            .map(
                image -> {
                  PreviewPageResponse previewPageResponse = new PreviewPageResponse();
                  try {
                    ImageSize smallPreview = ideasProperties.getPrintPreviewImage().getSmall();
                    ImageSize largePreview = ideasProperties.getPrintPreviewImage().getLarge();
                    previewPageResponse.setSmall(createPreview(image.getS3Image(), smallPreview));
                    previewPageResponse.setLarge(createPreview(image.getS3Image(), largePreview));
                    previewPageResponse.setPageNumber(image.getPageNumber());
                    return previewPageResponse;
                  } catch (IOException ioe) {
                    ioe.printStackTrace();
                  }
                  return null;
                })
            .filter(page -> page != null)
            .collect(Collectors.toList()));
  }
}
