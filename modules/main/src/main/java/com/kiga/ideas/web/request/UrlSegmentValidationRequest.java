package com.kiga.ideas.web.request;

import com.kiga.main.locale.Locale;
import lombok.Data;

/**
 * @author bbs
 * @since 4/17/17.
 */
@Data
public class UrlSegmentValidationRequest {
  private long id;
  private long categoryId;
  private String urlSegment;
  private Locale locale;
}
