package com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper;

import com.kiga.FeaturesProperties;
import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.ideas.web.viewmodel.ideahierarchy.helper.IdeaCategoryHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaSet;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.IdeaQueryEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.NewIdeaBlockViewEngine;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 24.01.18
 */
@Service
public class IdeaSiblingHelper {

  private IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxy;
  private IdeaQueryEngine ideaQueryEngine;
  private NewIdeaBlockViewEngine newIdeaBlockViewEngine;
  private FeaturesProperties featuresProperties;
  private IdeaCategoryHelper ideaCategoryHelper;

  /** Constructor. */
  @Autowired
  public IdeaSiblingHelper(
      IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxy,
      IdeaQueryEngine ideaQueryEngine,
      NewIdeaBlockViewEngine newIdeaBlockViewEngine,
      FeaturesProperties featuresProperties,
      IdeaCategoryHelper ideaCategoryHelper) {

    this.ideaCategoryRepositoryProxy = ideaCategoryRepositoryProxy;
    this.ideaQueryEngine = ideaQueryEngine;
    this.newIdeaBlockViewEngine = newIdeaBlockViewEngine;
    this.featuresProperties = featuresProperties;
    this.ideaCategoryHelper = ideaCategoryHelper;
  }

  /** return siblings of given group. */
  public List<IdeaCategory> getSiblings(IdeaQueryParameter ideaQueryParameter) {
    if (querySiblings(ideaQueryParameter.getIdeaCategory())) {
      return ideaCategoryRepositoryProxy
          .findByParentIdOrderBySortAsc(ideaQueryParameter.getIdeaCategory().getParent().getId())
          .stream()
          .filter(
              sibling -> {
                IdeaQueryParameter siblingQueryParameter =
                    IdeaQueryParameter.builder()
                        .member(ideaQueryParameter.getMember())
                        .ageGroupFilter(ideaQueryParameter.getAgeGroupFilter())
                        .idea(ideaQueryParameter.getIdea())
                        .locale(ideaQueryParameter.getLocale())
                        .showAll(ideaQueryParameter.isShowAll())
                        .ideaCategory(sibling)
                        .build();
                List<IdeaSet> ideaSets =
                    getIdeaSets(siblingQueryParameter)
                        .stream()
                        .filter(ideaSet -> !ideaSet.getIdeas().isEmpty())
                        .collect(Collectors.toList());
                return !ideaSets.isEmpty();
              })
          .collect(Collectors.toList());
    } else {
      return null;
    }
  }

  private boolean querySiblings(SiteTree siteTree) {
    return !"IdeaGroupMainContainer".equalsIgnoreCase(siteTree.getClassName());
  }

  private List<IdeaSet> getIdeaSets(IdeaQueryParameter ideaQueryParameter) {
    List<IdeaCategory> categories = getCategories(ideaQueryParameter.getIdeaCategory());

    // when blockview is needed then return all new ideas in one category
    if (categories.size() > 0
        && ideaCategoryHelper.isNewIdeaGroupCategory(ideaQueryParameter.getIdeaCategory())) {
      IdeaQueryParameter categoryIdeaQueryParameter =
          IdeaQueryParameter.builder()
              .locale(categories.get(0).getLocale())
              .ageGroupFilter(ideaQueryParameter.getAgeGroupFilter())
              .member(ideaQueryParameter.getMember())
              .ideaCategory(categories.get(0))
              .build();

      return Collections.singletonList(
          IdeaSet.builder()
              .ideaCategory(categories.get(0))
              .ideas(newIdeaBlockViewEngine.getIdeas(categoryIdeaQueryParameter))
              .build());

    } else {
      return categories
          .stream()
          .map(
              category -> {
                IdeaQueryParameter categoryIdeaQueryParameter =
                    IdeaQueryParameter.builder()
                        .locale(category.getLocale())
                        .member(ideaQueryParameter.getMember())
                        .ageGroupFilter(ideaQueryParameter.getAgeGroupFilter())
                        .ideaCategory(category)
                        .build();

                return IdeaSet.builder()
                    .ideaCategory(category)
                    .ideas(ideaQueryEngine.getIdeas(categoryIdeaQueryParameter))
                    .build();
              })
          .filter(this::filterEmptyIdeaSets)
          .collect(Collectors.toList());
    }
  }

  /** Get categories for parent. */
  private List<IdeaCategory> getCategories(IdeaCategory parentGroup) {
    return ideaCategoryRepositoryProxy.findBySortTreeStartsWithAndLocaleLikeOrderBySortAsc(
        parentGroup.getSortTree(),
        parentGroup.getLocale()); // possibly needs filter for class name?
  }

  private boolean filterEmptyIdeaSets(IdeaSet ideaSet) {
    return ideaSet.getIdeas() != null && ideaSet.getIdeas().size() > 0;
  }
}
