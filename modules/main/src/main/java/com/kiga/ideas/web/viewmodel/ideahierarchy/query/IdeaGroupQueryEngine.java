package com.kiga.ideas.web.viewmodel.ideahierarchy.query;

import com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper.IdeaSiblingHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaGroupQueryResult;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.IdeaQueryEngine;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 19/12/17.
 */
@Service
@Log4j2
public class IdeaGroupQueryEngine {
  private IdeaSiblingHelper ideaSiblingHelper;
  private IdeaQueryEngine ideaQueryEngine;

  /** Constructor. */
  @Autowired
  public IdeaGroupQueryEngine(
      IdeaSiblingHelper ideaSiblingHelper, IdeaQueryEngine ideaQueryEngine) {
    this.ideaSiblingHelper = ideaSiblingHelper;
    this.ideaQueryEngine = ideaQueryEngine;
  }

  /** get Information about the IdeaGroup Entity described by the ideaQueryParameter. */
  public IdeaGroupQueryResult query(IdeaQueryParameter ideaQueryParameter) {

    return IdeaGroupQueryResult.builder()
        .ideas(ideaQueryEngine.getIdeas(ideaQueryParameter))
        .siblings(ideaSiblingHelper.getSiblings(ideaQueryParameter))
        .build();
  }
}
