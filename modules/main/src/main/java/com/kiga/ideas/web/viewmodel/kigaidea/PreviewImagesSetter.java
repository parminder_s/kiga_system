package com.kiga.ideas.web.viewmodel.kigaidea;

import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.web.response.KigaIdeaViewModel;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by asv on 06.01.17.
 *
 */
public class PreviewImagesSetter {
  private IdeasProperties ideasProperties;
  private Logger logger = LoggerFactory.getLogger(getClass());
  private S3ImageResizeService s3ImageResizeService;

  /**
   * Constructor.
   */
  public PreviewImagesSetter(IdeasProperties ideasProperties,
                           S3ImageResizeService s3ImageResizeService) {
    this.ideasProperties = ideasProperties;
    this.s3ImageResizeService = s3ImageResizeService;
  }

  /**
   * Set Images.
   */
  public void setPreviewImages(KigaIdeaViewModel kigaIdeaViewModel,
                                      KigaPageImage previewImage) {
    if (previewImage == null) {
      kigaIdeaViewModel.setLargePreviewImageUrl("");
    } else {
      S3Image s3Image = previewImage.getS3Image();
      try {
        ImageSize smallPreview = ideasProperties.getPreviewImage().getSmall();
        ImageSize largePreview = ideasProperties.getPreviewImage().getLarge();
        kigaIdeaViewModel.setPreviewUrl(
          s3ImageResizeService.getResizedImageUrl(s3Image,
            new PaddedImageStrategy(smallPreview.getWidth(), smallPreview.getHeight())));
        kigaIdeaViewModel.setLargePreviewImageUrl(
          s3ImageResizeService.getResizedImageUrl(s3Image,
            new PaddedImageStrategy(largePreview.getWidth(), largePreview.getHeight())));
      } catch (IOException ex) {
        logger.warn("Problem with cached (or caching) size variants of image: " + s3Image
          .getName(), ex);
      } catch (IllegalArgumentException ex) {
        logger.warn("Wrong arguments passed to the strategy class", ex);
      }
    }
  }

}

