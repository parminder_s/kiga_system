package com.kiga.ideas.web;

import com.kiga.ideas.service.IdeaGroupService;
import com.kiga.ideas.web.response.IdeasResponseGroupWithParents;
import com.kiga.web.request.IdRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by robert on 08.06.17.
 */
@RestController
@RequestMapping("idea/groups")
public class IdeaGroupsController {
  private IdeaGroupService ideaGroupService;

  /**
   * Dependencies.
   *
   * @param ideaGroupService ideaGroupService
   */
  @Autowired
  public IdeaGroupsController(IdeaGroupService ideaGroupService) {
    this.ideaGroupService = ideaGroupService;
  }

  @RequestMapping("assigned")
  public List<IdeasResponseGroupWithParents> getAssigned(@RequestBody IdRequest idRequest) {
    return ideaGroupService.getAssigned(idRequest.getId(), idRequest.getLocale());
  }

  @RequestMapping("assigned/translated")
  public List<IdeasResponseGroupWithParents> getTranslatedAssigned(
    @RequestBody IdRequest idRequest) {
    Assert.notNull(idRequest.getId());
    return ideaGroupService.getTranslatedAssigned(idRequest.getId(), idRequest.getLocale());
  }

  @RequestMapping("unassigned")
  public List<IdeasResponseGroupWithParents> getUnassigned(@RequestBody IdRequest idRequest) {
    return ideaGroupService.getUnassigned(idRequest.getId(), idRequest.getLocale());
  }
}
