package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import com.kiga.cms.web.response.Permission;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.service.KigaIdeaUrlGenerator;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.service.IdeaPermissionService;
import com.kiga.ideas.web.response.KigaIdeaViewModel;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.base.KigaIdeaViewModelBaseMapper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.handler.KigaIdeaViewModelArticleTypeHandler;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.handler.KigaIdeaViewModelParentHandler;
import com.kiga.ideas.web.viewmodel.kigaidea.IdeasAgeRangeConverter;
import com.kiga.ideas.web.viewmodel.kigaidea.LinkedIdeasSetter;
import com.kiga.ideas.web.viewmodel.kigaidea.PreviewImagesSetter;
import com.kiga.main.MainProperties;
import com.kiga.main.PermissionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KigaIdeaViewModelMapper {

  private ViewModelMetaDataProvider viewModelMetaDataProvider;
  private IdeaPermissionService ideaPermissionService;
  private SiteTreeUrlGenerator urlGenerator;
  private KigaIdeaUrlGenerator kigaIdeaUrlGenerator;
  private LinkedIdeasSetter linkedIdeasSetter;
  private PreviewImagesSetter previewImagesSetter;
  private PermissionMode permissionMode;
  private KigaIdeaViewModelParentHandler kigaIdeaViewModelParentHandler;
  private KigaIdeaViewModelArticleTypeHandler kigaIdeaViewModelArticleTypeHandler;

  /** Constructor. */
  @Autowired
  public KigaIdeaViewModelMapper(
      KigaIdeaViewModelParentHandler kigaIdeaViewModelParentHandler,
      KigaIdeaViewModelArticleTypeHandler kigaIdeaViewModelArticleTypeHandler,
      ViewModelMetaDataProvider viewModelMetaDataProvider,
      IdeaPermissionService ideaPermissionService,
      SiteTreeUrlGenerator urlGenerator,
      KigaIdeaUrlGenerator kigaIdeaUrlGenerator,
      LinkedIdeasSetter linkedIdeasSetter,
      PreviewImagesSetter previewImagesSetter,
      MainProperties mainProperties) {
    this.kigaIdeaViewModelParentHandler = kigaIdeaViewModelParentHandler;
    this.kigaIdeaViewModelArticleTypeHandler = kigaIdeaViewModelArticleTypeHandler;
    this.viewModelMetaDataProvider = viewModelMetaDataProvider;
    this.ideaPermissionService = ideaPermissionService;
    this.urlGenerator = urlGenerator;
    this.kigaIdeaUrlGenerator = kigaIdeaUrlGenerator;
    this.linkedIdeasSetter = linkedIdeasSetter;
    this.previewImagesSetter = previewImagesSetter;
    this.permissionMode = mainProperties.getPermissionMode();
  }

  /** converts a KigaIdea Entity to a ViewModel. */
  public KigaIdeaViewModel convertToViewModel(KigaIdea kigaIdea) throws Exception {

    if (ideaPermissionService.permissionGranted(kigaIdea)) {
      return permissionGranted(kigaIdea);
    } else {
      return permissionDenied(kigaIdea);
    }
  }

  private KigaIdeaViewModel permissionGranted(KigaIdea kigaIdea) {
    if (kigaIdea == null) {
      return null;
    }

    IdeasAgeRangeConverter converter = new IdeasAgeRangeConverter();

    KigaIdeaViewModel kigaIdeaViewModel =
        KigaIdeaViewModelBaseMapper.INSTANCE.kigaIdeaToKigaIdeaViewModel(kigaIdea);
    viewModelMetaDataProvider.fillWithMetaDataAndBreadcrumbs(kigaIdeaViewModel, kigaIdea);
    kigaIdeaViewModel.setShowBreadcrumbs(false);

    kigaIdeaViewModel.setUrl(urlGenerator.getRelativeUrl(kigaIdea));
    kigaIdeaViewModel.setPreviewIdeaUrl(kigaIdeaUrlGenerator.getRelativePreviewUrl(kigaIdea));

    kigaIdeaViewModelArticleTypeHandler.handleArticleType(kigaIdeaViewModel, kigaIdea);
    kigaIdeaViewModelParentHandler.handleParent(kigaIdeaViewModel, kigaIdea);
    previewImagesSetter.setPreviewImages(kigaIdeaViewModel, kigaIdea.getPreviewImage());
    linkedIdeasSetter.setLinkedIdeas(kigaIdeaViewModel, kigaIdea.getLinkedKigaPageGroups());

    if (kigaIdea.getAgeGroup() != null) {
      kigaIdeaViewModel.setAgeRange(converter.getAgeRange(kigaIdea.getAgeGroup()));
    }

    return kigaIdeaViewModel;
  }

  private KigaIdeaViewModel permissionDenied(KigaIdea kigaIdea) {

    KigaIdeaViewModel kigaIdeaViewModel = new KigaIdeaViewModel();
    Permission permission = new Permission();
    permission.setDenied(true);
    kigaIdeaViewModel.setUrl(urlGenerator.getRelativeNgUrl(kigaIdea));
    if (permissionMode == PermissionMode.FullTest) {
      permission.setCode("subscription");
    } else {
      if (kigaIdea.getInPool() == 1) {
        permission.setCode("fullSubscription");
      } else {
        permission.setCode("testSubscription");
      }
    }

    kigaIdeaViewModel.setPermission(permission);
    return kigaIdeaViewModel;
  }
}
