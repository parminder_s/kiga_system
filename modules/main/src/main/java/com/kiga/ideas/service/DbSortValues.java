package com.kiga.ideas.service;

import org.springframework.data.domain.Sort;

/**
 * Created by rainerh on 10.09.16.
 */
public class DbSortValues {
  public static Sort sortSequence = new Sort(
    new Sort.Order(Sort.Direction.ASC, "igti.sortOrder"),
    new Sort.Order(Sort.Direction.DESC, "sortIndex"),
    new Sort.Order(Sort.Direction.DESC, "ratingComplete"),
    new Sort.Order(Sort.Direction.ASC, "title")
  );

  public static Sort sortDate = new Sort(
    new Sort.Order(Sort.Direction.DESC, "sortIndex"),
    new Sort.Order(Sort.Direction.DESC, "ratingComplete"),
    new Sort.Order(Sort.Direction.ASC, "title")
  );
}
