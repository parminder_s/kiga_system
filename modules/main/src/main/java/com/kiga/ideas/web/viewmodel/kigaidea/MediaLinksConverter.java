package com.kiga.ideas.web.viewmodel.kigaidea;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


/**
 * Created by asv on 04.02.17.
 */
public class MediaLinksConverter {
  public MediaLinksConverter() {
  }

  /**
   * Set MediaLinks.
   */
  public String convertMediaLinks(String content) {
    Document doc = Jsoup.parse(content);
    Elements audioLinks = doc.select("a.sm2_link");
    for (Element audioLink : audioLinks) {
      String link = audioLink.attr("href");
      String label = audioLink.text();
      audioLink.before("<audio-player label=\"" + label + "\" href=\"" + link + "\"></audio-player>");
      audioLink.remove();
    }
    Elements videoLinks = doc.select("a.videoplayer");
    for (Element videoLink : videoLinks) {
      String link = videoLink.attr("href");
      Element img = videoLink.select("img").first();
      String previewLink = img.attr("src");
      videoLink.before("<video-player video-link=\"" + link + "\"preview-link=\""
        + "" + previewLink + "\"></video-player>");
      videoLink.remove();
    }
    return doc.select("body").html();
  }
}

