package com.kiga.ideas.web.response;

import lombok.Data;

import java.util.List;

/**
 * Created by robert on 29.08.17.
 */
@Data
public class IdeaTranslationGroup {
  private List<IdeasViewModelIdea> ideas;
  private long translationGroupId;
}
