package com.kiga.ideas.member.domain;

import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.db.KigaEntityModel;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by rainerh on 22.08.16.
 */
@Entity
public class MemberIdea extends KigaEntityModel {
  private String title;
  private String description;
  private Long fileId;
  private Long previewId;
  private String content;
  private String contentType;

  @ManyToOne
  @JoinColumn(name = "ideaCategoryId", referencedColumnName = "id")
  private IdeaCategoryLive ideaCategory;

  public MemberIdea() {
    this.setClassName("MemberIdea");
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getFileId() {
    return fileId;
  }

  public void setFileId(Long fileId) {
    this.fileId = fileId;
  }

  public Long getPreviewId() {
    return previewId;
  }

  public void setPreviewId(Long previewId) {
    this.previewId = previewId;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public IdeaCategoryLive getIdeaCategory() {
    return ideaCategory;
  }

  public void setIdeaCategory(IdeaCategoryLive ideaCategory) {
    this.ideaCategory = ideaCategory;
  }
}
