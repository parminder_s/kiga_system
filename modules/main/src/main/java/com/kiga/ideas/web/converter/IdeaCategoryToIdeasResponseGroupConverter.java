package com.kiga.ideas.web.converter;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.ideas.web.response.IdeasResponseGroup;
import com.kiga.web.converter.DefaultViewModelConverter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author bbs
 * @since 5/13/16.
 */
@Service
public class IdeaCategoryToIdeasResponseGroupConverter
  extends DefaultViewModelConverter<IdeaCategory, IdeasResponseGroup> {
  Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public IdeasResponseGroup convertToResponse(IdeaCategory entity) {

    IdeasResponseGroup returner = null;

    if (entity != null) {
      returner = new IdeasResponseGroup();
      returner.setId(entity.getId());
      returner.setName(entity.getUrlSegment());
      returner.setTitle(entity.getTitle());
      returner.setLocale(entity.getLocale());
      returner.setUrlSegment(entity.getUrlSegment());
      returner.setIdeaGroup("IdeaGroup".equals(entity.getClassName()));
      returner.setLastCategoryLevel("IdeaGroup".equals(entity.getClassName()));
      returner.setCode(entity.getCode());
      returner.setIcon(this.getIconCode(entity));
      returner.setDisableForFavourites(this.getDisableForFavourites(entity));
      returner.setSortTree(Optional.ofNullable(entity.getSortTree()).orElse("A"));

    }

    return returner;
  }

  /**
   * inherit the value from the first child below IdeaGroupMainContainer.
   */
  private boolean getDisableForFavourites(IdeaCategory ideaCategory) {
    boolean returner = false;
    while (!ideaCategory.getClassName().equals("IdeaGroupMainContainer") && !ideaCategory
      .getClassName().equals("MainArticleCategory")) {
      if (ideaCategory.getParent().getClassName().equals("IdeaGroupMainContainer") || ideaCategory
        .getParent().getClassName().equals("MainArticleCategory")) {
        returner = ideaCategory.isDisableForFavourites();
        break;
      }
      ideaCategory = (IdeaCategory) ideaCategory.getParent();
    }

    return returner;
  }

  private String getIconCode(IdeaCategory ideaCategory) {
    try {
      while (!ideaCategory.getClassName().equals("IdeaGroupMainContainer")) {
        if (!StringUtils.isEmpty(ideaCategory.getIconCode())) {
          return ideaCategory.getIconCode();
        }
        ideaCategory = (IdeaCategory) ideaCategory.getParent();
      }
    } catch (Exception exception) {
      logger.error(exception.getMessage());
    }
    return "icon-ideas";
  }
}
