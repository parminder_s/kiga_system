package com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.favourites.repository.FavouriteNewIdeasRepository;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper.IdeaEngineHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.main.locale.Locale;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author Florian Schneider
 * @since 24.01.18
 */
@Service
public class NewIdeaEngine {

  private IdeasProperties ideasProperties;
  private FavouriteNewIdeasRepository favouriteNewIdeasRepository;
  private NewIdeasRepository newIdeasRepository;
  private IdeaEngineHelper ideaEngineHelper;

  /** Constructor. */
  @Autowired
  public NewIdeaEngine(
      IdeasProperties ideasProperties,
      FavouriteNewIdeasRepository favouriteNewIdeasRepository,
      NewIdeasRepository newIdeasRepository,
      IdeaEngineHelper ideaEngineHelper) {
    this.ideasProperties = ideasProperties;
    this.favouriteNewIdeasRepository = favouriteNewIdeasRepository;
    this.newIdeasRepository = newIdeasRepository;
    this.ideaEngineHelper = ideaEngineHelper;
  }

  /** get List of KigaIdeas depending on IdeaQueryParameters. */
  public List<KigaIdea> getIdeas(IdeaQueryParameter ideaQueryParameter) {
    Pageable pageable = new PageRequest(0, ideasProperties.getAmountPerCategory());
    Locale locale = ideaQueryParameter.getLocale();
    int ageGroupFilter = ideaQueryParameter.getAgeGroupFilter();

    return ideaQueryParameter
        .getMember()
        .map(
            member ->
                favouriteNewIdeasRepository.getIdeasForAllAgeGroups(
                    locale, ageGroupFilter, member, pageable))
        .orElseGet(
            () -> newIdeasRepository.getIdeasForAllAgeGroups(locale, ageGroupFilter, pageable))
        .stream()
        .filter(ideaEngineHelper::hasValidParent)
        .collect(Collectors.toList());
  }
}
