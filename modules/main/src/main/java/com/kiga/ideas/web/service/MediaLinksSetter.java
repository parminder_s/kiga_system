package com.kiga.ideas.web.service;

import com.kiga.ideas.web.response.IdeasViewModelIdea;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Created by asv on 04.02.17.
 */
public class MediaLinksSetter {
  public MediaLinksSetter() {
  }

  /**
   * Set MediaLinks.
   */
  public void setMediaLinks(IdeasViewModelIdea returner, String content) {
    if (content != null) {
      Document doc = Jsoup.parse(content);
      Element audioLink = doc.select("a.sm2_link").first();
      if (audioLink != null) {
        String linkHref = audioLink.attr("href");
        returner.setAudioLink(linkHref);
      }
      Element videoLink = doc.select("a.videoplayer").first();
      if (videoLink != null) {
        String linkHref = videoLink.attr("href");
        returner.setVideoLink(linkHref);
      }
    }
  }
}

