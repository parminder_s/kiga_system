package com.kiga.shop.repository;

import com.kiga.shop.domain.CountryShop;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author bbs
 * @since 10/16/16.
 */
public interface CountryShopRepository extends JpaRepository<CountryShop, Long> {
  CountryShop findByCountryId(long countryId);
}
