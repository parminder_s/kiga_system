package com.kiga.shop.service;

import java.math.BigDecimal;
import org.springframework.stereotype.Service;

/**
 * This class calculates the gross price from a given net price and vice versa. This is done to have
 * a central calculation unit since BigDecimal based calculations can produce different results,
 * depending on the their parameterization.
 *
 * <p>Please note, that net price has a scale of 3, since db stores the net price and product price
 * is set with the gross price. With scale of 3 we can eliminate all rounding issues when
 * calculating back to gross.
 *
 * <p>Assertions are in place to ensure that the inputs have the allowed scales.
 */
@Service
public class VatRateCalculator {
  /** Calculate price from Gross to Net. */
  public BigDecimal calculateNetPrice(BigDecimal grossPrice, BigDecimal vatRate) {
    assertInputs(grossPrice, vatRate, 2);
    return grossPrice.divide(BigDecimal.ONE.add(vatRate), 3, BigDecimal.ROUND_FLOOR);
  }

  /** Calculate price from Net to Gross. */
  public BigDecimal calculateGrossPrice(BigDecimal netPrice, BigDecimal vatRate) {
    assertInputs(netPrice, vatRate, 3);
    return netPrice.multiply(BigDecimal.ONE.add(vatRate)).setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  private void assertInputs(BigDecimal price, BigDecimal vatRate, int maxPriceScale) {
    int priceScale = price.scale();
    int vatRateScale = vatRate.scale();

    if (priceScale != maxPriceScale) {
      throw new RuntimeException(
          String.format(
              "passed price %s has a scale of %d. It must have exactly 2.",
              price.toString(), maxPriceScale));
    }

    if (vatRateScale != 3) {
      throw new RuntimeException(
          String.format(
              "passed vatRate %s has a scale of %d. It must have exactly 3.",
              vatRate.toString(), vatRateScale));
    }
  }
}
