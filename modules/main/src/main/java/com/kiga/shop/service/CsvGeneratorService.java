package com.kiga.shop.service;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneak;
import static com.rainerhahnekamp.sneakythrow.Sneaky.sneaked;

import com.kiga.shop.document.service.DhlCsvGenerator;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.ShippingPartner;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

/** Created by faxxe on 1/10/17. */
@Service
public class CsvGeneratorService {

  /** generate csv file for dhl. */
  public File getDhlCsvData(List<Purchase> purchases) {
    File file = new File("/tmp/dhlCsv" + System.currentTimeMillis() + ".csv");
    return sneak(
        () -> {
          try (OutputStream fos = new FileOutputStream(file);
              OutputStreamWriter osw = new OutputStreamWriter(fos, Charset.forName("ISO8859-1"))) {

            new DhlCsvGenerator()
                .getCsvData(
                    purchases
                        .stream()
                        .filter(x -> ShippingPartner.DHL.equals(x.getShippingPartner()))
                        .collect(Collectors.toList()))
                .forEach(line -> sneaked(() -> osw.write(line)).run());
            return file;
          }
        });
  }
}
