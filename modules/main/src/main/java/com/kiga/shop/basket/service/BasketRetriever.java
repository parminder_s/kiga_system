package com.kiga.shop.basket.service;

import com.kiga.security.domain.Member;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.repository.CountryRepository;
import com.kiga.shop.repository.PurchaseRepositoryExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Stream;

/**
 We are looking up all purchases that match for customerId, sessionId or cookieId. From these
 three possible ones we take the one that has been the youngest lastEdited property.
 */
@Service
public class BasketRetriever {
  private PurchaseRepositoryExt purchaseRepositoryExt;
  private CountryRepository countryRepository;

  /**
   * inject dependencies.
   */
  @Autowired
  public BasketRetriever(
    PurchaseRepositoryExt purchaseRepositoryExt,
    CountryRepository countryRepository) {
    this.purchaseRepositoryExt = purchaseRepositoryExt;
    this.countryRepository = countryRepository;
  }

  /**
   * main routing of the class.
   */
  public Optional<Purchase> find(Optional<Member> optMember, String sessionId,
                                 Optional<String> cookieId) {
    Optional<Purchase> purchaseByCustomer = optMember
      .filter(member -> member.getCrmId() != null)
      .map(member -> purchaseRepositoryExt.findNewByCustomer(member.getId()))
      .orElseGet(Optional::empty);
    Optional<Purchase> purchaseBySession = purchaseRepositoryExt.findBySession(sessionId);

    Optional<Purchase> purchaseByCookie = cookieId
      .map(cookie -> purchaseRepositoryExt.findBySession(cookie))
      .orElseGet(Optional::empty);

    Optional<Purchase> returner = Stream.of(purchaseByCustomer, purchaseByCookie, purchaseBySession)
      .filter(Optional::isPresent)
      .map(Optional::get)
      .sorted(Comparator.comparing(Purchase::getLastEdited).reversed())
      .findFirst();

    optMember.ifPresent(
      member -> returner.ifPresent(purchase -> this.addMemberIdAndCountry(purchase, member)));

    return returner;
  }

  private void addMemberIdAndCountry(Purchase purchase, Member member) {
    purchase.setLastEdited(new Date());
    purchase.setCountry(countryRepository.findByCode(member.getCountry()));
  }
}
