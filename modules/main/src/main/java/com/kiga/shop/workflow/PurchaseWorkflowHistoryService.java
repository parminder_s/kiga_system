package com.kiga.shop.workflow;

import com.kiga.security.domain.Member;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseStatus;
import com.kiga.shop.domain.PurchaseWorkflow;
import com.kiga.shop.repository.PurchaseWorkflowRepository;
import java.time.Instant;
import java.util.List;
import javax.transaction.Transactional;

/** Created by faxxe on 1/11/17. */
public class PurchaseWorkflowHistoryService {

  private PurchaseWorkflowRepository purchaseWorkflowRepository;
  private Workflow<Purchase, PurchaseStatus> workflow;

  public PurchaseWorkflowHistoryService(
      PurchaseWorkflowRepository purchaseWorkflowRepository,
      Workflow<Purchase, PurchaseStatus> workflow) {
    this.purchaseWorkflowRepository = purchaseWorkflowRepository;
    this.workflow = workflow;
  }

  /** proceed to state. */
  public void proceedToState(List<Purchase> purchases, PurchaseStatus targetState, Member member) {

    purchases.forEach(
        purchase -> {
          PurchaseStatus previousState = (PurchaseStatus) purchase.getStatus();
          workflow.proceedToState(purchase, targetState);
          recordPurchaseStatusChange(purchase, previousState, member);
        });
  }

  /** record a purchase change status because of reasons. */
  @Transactional
  private void recordPurchaseStatusChange(
      Purchase purchase, PurchaseStatus previousState, Member member) {

    PurchaseWorkflow purchaseWorkflow = new PurchaseWorkflow();

    purchaseWorkflow.setChangeDate(Instant.now());
    purchaseWorkflow.setMember(member);
    purchaseWorkflow.setPurchase(purchase);
    purchaseWorkflow.setPrevStatus(previousState);
    purchaseWorkflow.setStatus((PurchaseStatus) purchase.getStatus());

    purchaseWorkflowRepository.save(purchaseWorkflow);
  }
}
