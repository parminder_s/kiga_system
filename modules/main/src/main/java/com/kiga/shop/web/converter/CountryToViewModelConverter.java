package com.kiga.shop.web.converter;

import com.kiga.main.locale.Locale;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryLocale;
import com.kiga.shop.repository.CountryLocaleRepository;
import com.kiga.shop.web.response.CountryViewModel;
import com.kiga.web.converter.DefaultViewModelConverter;
import org.modelmapper.ModelMapper;

import java.util.Optional;

/**
 * @author bbs
 * @since 10/16/16.
 */
public class CountryToViewModelConverter
  extends DefaultViewModelConverter<Country, CountryViewModel> {
  private CountryLocaleRepository repository;
  private ModelMapper modelMapper;

  public CountryToViewModelConverter(CountryLocaleRepository repository, ModelMapper modelMapper) {
    this.repository = repository;
    this.modelMapper = modelMapper;
  }

  @Override
  public CountryViewModel convertToResponse(Country entity) {
    if (entity == null) {
      return null;
    }

    CountryViewModel returner = modelMapper.map(entity.getCountryShop(), CountryViewModel.class);
    modelMapper.map(entity, returner);
    // returner.setPriceShippingNet(entity.getCountryShop().getPriceShippingNet());
    CountryLocale countryLocale = Optional
      .ofNullable(repository.findByCountryAndLocale(entity, Locale.en))
      .orElse(new CountryLocale());
    returner.setTitle(countryLocale.getTitle());

    return returner;
  }
}
