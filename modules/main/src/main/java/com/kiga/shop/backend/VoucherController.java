package com.kiga.shop.backend;

import com.kiga.security.services.SecurityService;
import com.kiga.shop.backend.exception.IllegalPaginationArgumentException;
import com.kiga.shop.backend.exception.NonexistentEntityUpdateException;
import com.kiga.shop.backend.web.request.DeactivateVoucherRequest;
import com.kiga.shop.backend.web.request.VoucherFormRequest;
import com.kiga.shop.backend.web.request.VouchersListRequest;
import com.kiga.shop.backend.web.response.VoucherViewModel;
import com.kiga.shop.domain.Product;
import com.kiga.shop.repository.ProductRepository;
import com.kiga.shop.repository.VoucherRepository;
import com.kiga.shop.service.DefaultVoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Florian Schneider
 * @since 20/11/17.
 */
@RestController
@RequestMapping("shop/backend/vouchers")
class VoucherController {
  private static final int DEFAULT_PAGE_SIZE = 50;
  private SecurityService securityService;
  private VoucherRepository voucherRepository;
  private DefaultVoucherService defaultVoucherService;
  private ProductRepository productRepository;

  @Autowired
  VoucherController(SecurityService securityService, VoucherRepository voucherRepository,
                    DefaultVoucherService defaultVoucherService,
                    ProductRepository productRepository) {
    this.securityService = securityService;
    this.voucherRepository = voucherRepository;
    this.defaultVoucherService = defaultVoucherService;
    this.productRepository = productRepository;
  }


  @RequestMapping("list")
  List<VoucherViewModel> getList(
          @RequestBody(required = false) VouchersListRequest vouchersListRequest) throws
          IllegalPaginationArgumentException {
    this.securityService.requireShopAdmin();

    if (vouchersListRequest == null) {
      vouchersListRequest = new VouchersListRequest();
    }

    if (vouchersListRequest.getPageSize() <= 0) {
      vouchersListRequest.setPageSize(DEFAULT_PAGE_SIZE);
    }

    if (vouchersListRequest.getPage() < 0) {
      vouchersListRequest.setPage(0);
    }

    List<VoucherViewModel> voucherViewModels = defaultVoucherService
            .getVouchers(vouchersListRequest.getPage(), vouchersListRequest.getPageSize());

    if (voucherViewModels == null) {
      voucherViewModels = new ArrayList<>();
    }

    return voucherViewModels;
  }

  @RequestMapping("get")
  public VoucherViewModel getVoucher(
          @RequestBody VoucherFormRequest voucherFormRequest) {
    this.securityService.requireShopAdmin();
    return defaultVoucherService.getVoucher(voucherFormRequest.getVoucherId());
  }

  @RequestMapping("products")
  public Map<Long, String> getProductDisplayStrings() {
    return StreamSupport.stream(productRepository.findAll().spliterator(), false)
            .collect(Collectors.toMap(Product::getId, Product::getProductTitle));
  }

  @RequestMapping("save")
  public void save(
          @RequestBody VoucherViewModel voucherViewModel) throws NonexistentEntityUpdateException {
    this.securityService.requireShopAdmin();
    defaultVoucherService.save(voucherViewModel);
  }

  @RequestMapping("deactivate")
  public void deactivate(@RequestBody DeactivateVoucherRequest deactivateVoucherRequest)
          throws NonexistentEntityUpdateException {
    this.securityService.requireShopAdmin();
    defaultVoucherService.deactivate(deactivateVoucherRequest.getId());
  }
}
