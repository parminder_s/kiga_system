package com.kiga.shop.web;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneaked;

import com.kiga.security.domain.Member;
import com.kiga.security.spring.SecurityUtil;
import com.kiga.shop.ShopBackendService;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseStatus;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.shop.service.CreditVoucherService;
import com.kiga.shop.service.CsvGeneratorService;
import com.kiga.shop.service.DocumentService;
import com.kiga.shop.service.ProductHelper;
import com.kiga.shop.service.ShopMailer;
import com.kiga.shop.web.converter.PurchaseDetailResponseConverter;
import com.kiga.shop.web.requests.MultiIdRequest;
import com.kiga.shop.web.requests.PurchaseChangeRequest;
import com.kiga.shop.web.requests.SingleIdRequest;
import com.kiga.shop.web.response.PurchaseDetailResponse;
import com.kiga.shop.workflow.PurchaseWorkflowHistoryService;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/** Created by faxxe on 1/3/17. */
@RestController
@ConfigurationProperties("shop")
@RequestMapping("shop/backend")
public class ShopBackendController {

  private final SecurityUtil securityUtil;
  private PurchaseRepository purchaseRepository;
  private ProductHelper productHelper;
  private ShopBackendService shopBackendService;
  private DocumentService documentService;
  private ShopMailer shopMailer;
  private CreditVoucherService creditVoucherService;
  private CsvGeneratorService csvGeneratorService;
  private PurchaseWorkflowHistoryService workflow;
  private PurchaseDetailResponseConverter purchaseDetailResponseConverter;

  /** constructor. */
  @Autowired
  public ShopBackendController(
      SecurityUtil securityUtil,
      PurchaseRepository purchaseRepository,
      ProductHelper productHelper,
      ShopBackendService shopBackendService,
      PurchaseWorkflowHistoryService workflow,
      CsvGeneratorService csvGeneratorService,
      DocumentService documentService,
      ShopMailer shopMailer,
      CreditVoucherService creditVoucherService,
      PurchaseDetailResponseConverter purchaseDetailResponseConverter) {
    this.securityUtil = securityUtil;
    this.purchaseRepository = purchaseRepository;
    this.productHelper = productHelper;
    this.shopBackendService = shopBackendService;
    this.workflow = workflow;
    this.csvGeneratorService = csvGeneratorService;
    this.documentService = documentService;
    this.shopMailer = shopMailer;
    this.creditVoucherService = creditVoucherService;
    this.purchaseDetailResponseConverter = purchaseDetailResponseConverter;
  }

  /** generate and print shippingnotes and related documents. */
  @Transactional
  @RequestMapping(value = "printSelectedShippingNotes/{ids}", method = RequestMethod.GET)
  public void printSelectedOrders(@PathVariable List<Long> ids, HttpServletResponse response) {
    Member member = securityUtil.getMember();
    List<Purchase> purchases = (List<Purchase>) purchaseRepository.findAll(ids);
    workflow.proceedToState(purchases, PurchaseStatus.ShippingInformationCreated, member);
    File file = shopBackendService.printSelectedOrders(purchases);
    copyFileToResponse(file, response, "ShippingInformation.pdf");
  }

  /** mark selected purchases as shipped. */
  @RequestMapping(value = "setSelectedStatusShipped", method = RequestMethod.POST)
  public void setStatusShipped(@RequestBody MultiIdRequest request) {
    List<Purchase> purchases = (List<Purchase>) purchaseRepository.findAll(request.getIds());
    workflow.proceedToState(purchases, PurchaseStatus.Shipped, securityUtil.getMember());
    purchaseRepository.save(purchases);
  }

  /** get some specific invoiceversion. */
  @RequestMapping(value = "getInvoiceVersion/{id}", method = RequestMethod.GET)
  public void getInvoiceVersion(@PathVariable Long id, HttpServletResponse response) {
    String fileName = documentService.getInvoiceVersion(id);

    copyFileToResponse(
        new File(fileName),
        response,
        (Arrays.stream(fileName.split("/")).reduce((first, second) -> second).orElse("filename")));
  }

  /** change a purchase. */
  @RequestMapping(value = "savePurchase", method = RequestMethod.POST)
  public void savePurchase(@RequestBody PurchaseChangeRequest purchaseChangeRequest) {
    Purchase purchase = purchaseRepository.findById(purchaseChangeRequest.getId());

    purchase.setFirstname(purchaseChangeRequest.getFirstName());
    purchase.setLastname(purchaseChangeRequest.getLastName());
    purchase.setAddressName1(purchaseChangeRequest.getAddressName1());
    purchase.setAddressName2(purchaseChangeRequest.getAddressName2());
    purchase.setStreet(purchaseChangeRequest.getStreet());
    purchase.setStreetNumber(purchaseChangeRequest.getStreetNumber());
    purchase.setFloorNumber(purchaseChangeRequest.getFloorNumber());
    purchase.setDoorNumber(purchaseChangeRequest.getDoorNumber());
    purchase.setZip(purchaseChangeRequest.getZip());
    purchase.setCity(purchaseChangeRequest.getCity());
    purchase.setCountryTitle(purchaseChangeRequest.getCountry());
    purchase.setBillAddressName1(purchaseChangeRequest.getBillAddressName1());
    purchase.setBillAddressName2(purchaseChangeRequest.getBillAddressName2());
    purchase.setBillStreet(purchaseChangeRequest.getBillStreet());
    purchase.setBillStreetNumber(purchaseChangeRequest.getBillStreetnumber());
    purchase.setBillFloorNumber(purchaseChangeRequest.getBillFloornumber());
    purchase.setBillDoorNumber(purchaseChangeRequest.getBillDoornumber());
    purchase.setBillZip(purchaseChangeRequest.getBillZip());
    purchase.setBillCity(purchaseChangeRequest.getBillCity());
    purchase.setEmail(purchaseChangeRequest.getMemberEmail());
    purchase.setComment(purchaseChangeRequest.getComment());
    purchase.setShippingPartner(purchaseChangeRequest.getShippingPartner());

    purchaseRepository.save(purchase);
  }

  /** find details by invoiceNr. */
  @RequestMapping(value = "findByInvoiceNr/{invoiceNr}", method = RequestMethod.POST)
  public PurchaseDetailResponse findByInvoiceNr(@PathVariable String invoiceNr) {
    return purchaseDetailResponseConverter.convert(purchaseRepository.findByInvoiceNr(invoiceNr));
  }

  /** get details for a specific purchase. */
  @RequestMapping(value = "find", method = RequestMethod.POST)
  public PurchaseDetailResponse findById(@RequestBody SingleIdRequest request) {
    return this.purchaseDetailResponseConverter.convert(
        purchaseRepository.findById(request.getId()));
  }

  /** get the invoice for a specific purchase. */
  @RequestMapping(value = "downloadInvoice/{id}", method = RequestMethod.GET)
  public void downloadInvoice(@PathVariable Long id, HttpServletResponse response) {
    Purchase purchase = purchaseRepository.findById(id);
    String fileName = documentService.downloadInvoice(purchase);
    File file = new File(fileName);

    copyFileToResponse(
        file,
        response,
        Arrays.stream(fileName.split("/")).reduce((first, second) -> second).orElse("filename"));
  }

  private void copyFileToResponse(File file, HttpServletResponse response, String fileName) {
    sneaked(
            () -> {
              try (FileInputStream fis = new FileInputStream(file);
                  OutputStream outputStream = response.getOutputStream()) {
                response.setContentType("application/pdf");
                response.setHeader(
                    org.springframework.http.HttpHeaders.CONTENT_DISPOSITION,
                    "attachment; filename=\"" + fileName + "\"");
                IOUtils.copy(fis, outputStream);
              }
            })
        .run();
  }

  /** get shippingNote for a specific purchase. */
  @RequestMapping(value = "downloadShippingNote/{id}", method = RequestMethod.GET)
  public void downloadShippingNote(@PathVariable Long id, HttpServletResponse response) {
    Purchase purchase = purchaseRepository.findById(id);

    String fileName = documentService.downloadShippingNote(purchase);
    File file = new File(fileName);

    copyFileToResponse(
        file,
        response,
        Arrays.stream(fileName.split("/")).reduce((first, second) -> second).orElse("filename"));
  }

  /** mark purchases as ready for shipping. */
  @RequestMapping(value = "setSelectedReadyForShipping", method = RequestMethod.POST)
  public void setSelectedReadyForShipping(@RequestBody MultiIdRequest request) {
    List<Purchase> purchases = (List<Purchase>) purchaseRepository.findAll(request.getIds());
    workflow.proceedToState(purchases, PurchaseStatus.ReadyForShipping, securityUtil.getMember());
    purchaseRepository.save(purchases);
  }

  /** resend invoice manually. */
  @RequestMapping(value = "resendInvoice", method = RequestMethod.POST)
  public void resendInvoice(@RequestBody SingleIdRequest request) {
    final Purchase purchase = purchaseRepository.findById(request.getId());
    shopMailer.emailInvoice(purchase);
  }

  /** recreates documents (invoice, shippingNote) for a purchase. */
  @RequestMapping(value = "recreateDocuments", method = RequestMethod.POST)
  public void reCreateInvoice(@RequestBody SingleIdRequest request) {
    final Purchase purchase = purchaseRepository.findById(request.getId());

    documentService.createNewVersionedInvoice(purchase);
    documentService.overrideShippingNote(purchase);

    purchaseRepository.save(purchase);
  }

  /** some controllermethod. */
  @RequestMapping(value = "createCreditVoucher", method = RequestMethod.POST)
  public void creditVoucherCreate(@RequestBody PurchaseDetailResponse purchaseDetail) {
    Purchase purchase = purchaseRepository.findOne(purchaseDetail.getId());

    workflow.proceedToState(
        Collections.singletonList(purchase),
        PurchaseStatus.CreditVoucherPending,
        securityUtil.getMember());
    creditVoucherService.createCreditVoucher(purchaseDetail);
    creditVoucherService.emailCreditVoucher(purchase);
    purchaseRepository.save(purchase);
  }

  /** some controllermethod. */
  @RequestMapping(value = "finishCreditVoucher", method = RequestMethod.POST)
  public void creditVoucherFinish(@RequestBody SingleIdRequest request) {
    Purchase purchase = purchaseRepository.findOne(request.getId());

    creditVoucherService.finalizeCreditVoucher(purchase);
    workflow.proceedToState(
        Collections.singletonList(purchase),
        PurchaseStatus.CreditVoucherFinalized,
        securityUtil.getMember());

    purchaseRepository.save(purchase);
  }

  /** some controllermethod. */
  @RequestMapping(value = "storno", method = RequestMethod.POST)
  public void storno(@RequestBody SingleIdRequest request, HttpServletRequest httpServletRequest) {}

  /** get csv export. */
  @RequestMapping(value = "getSelectedCsv/{ids}", method = RequestMethod.GET)
  public void getSelectedCsv(@PathVariable List<Long> ids, HttpServletResponse response) {
    List<Purchase> purchases = (List<Purchase>) purchaseRepository.findAll(ids);

    workflow.proceedToState(purchases, PurchaseStatus.CsvExported, securityUtil.getMember());
    purchaseRepository.save(purchases);

    File file = csvGeneratorService.getDhlCsvData(purchases);

    copyFileToResponse(file, response, "dhl_" + new Date() + ".csv");
  }

  /** some controllermethod. */
  @RequestMapping(value = "setSelectedCsvFin", method = RequestMethod.POST)
  public void setSelectedCsvFin(@RequestBody MultiIdRequest request) {
    List<Purchase> purchases = (List<Purchase>) purchaseRepository.findAll(request.getIds());
    workflow.proceedToState(purchases, PurchaseStatus.CsvFinished, securityUtil.getMember());
    purchaseRepository.save(purchases);
  }

  /** download shippinglabel. */
  @RequestMapping(value = "downloadShippingLabel/{id}", method = RequestMethod.GET)
  public void downloadPostLabel(@PathVariable Long id, HttpServletResponse response) {
    Purchase purchase = purchaseRepository.findById(id);

    File file = documentService.createPostLabel(purchase);
    copyFileToResponse(file, response, "Label" + "_" + purchase.getOrderNr().toString() + ".pdf");
  }
}
