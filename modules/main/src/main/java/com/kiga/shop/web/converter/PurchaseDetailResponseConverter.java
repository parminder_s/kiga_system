package com.kiga.shop.web.converter;

import static com.kiga.util.NowService.KIGAZONE;
import static java.util.stream.Collectors.toList;

import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.InvoiceVersion;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseWorkflow;
import com.kiga.shop.repository.InvoiceVersionRepository;
import com.kiga.shop.repository.PurchaseWorkflowRepository;
import com.kiga.shop.service.ShopUrlGenerator;
import com.kiga.shop.web.response.InvoiceVersionResponse;
import com.kiga.shop.web.response.PurchaseDetailResponse;
import com.kiga.shop.web.response.PurchaseWorkflowResponse;
import com.kiga.shop.web.responses.PurchaseItemResponse;
import java.util.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchaseDetailResponseConverter {
  private ShopUrlGenerator shopUrlGenerator;
  private InvoiceVersionRepository invoiceVersionRepository;
  private PurchaseWorkflowRepository purchaseWorkflowRepository;

  /** inject deps. */
  @Autowired
  public PurchaseDetailResponseConverter(
      ShopUrlGenerator shopUrlGenerator,
      InvoiceVersionRepository invoiceVersionRepository,
      PurchaseWorkflowRepository purchaseWorkflowRepository) {
    this.shopUrlGenerator = shopUrlGenerator;
    this.invoiceVersionRepository = invoiceVersionRepository;
    this.purchaseWorkflowRepository = purchaseWorkflowRepository;
  }

  /** converts a PurchaseDetail into a PurchaseDetailResponse. */
  public PurchaseDetailResponse convert(Purchase purchase) {
    PurchaseDetailResponseMapper mapper = PurchaseDetailResponseMapper.INSTANCE;

    return PurchaseDetailResponse.builder()
        .id(purchase.getId())
        .firstName(purchase.getFirstname())
        .lastName(purchase.getLastname())
        .addressName1(purchase.getAddressName1())
        .addressName2(purchase.getAddressName2())
        .priceNetTotal(purchase.getPriceNetTotal().toString())
        .priceGrossTotal(purchase.getPriceGrossTotal().toString())
        .currency(purchase.getCurrency())
        .status(purchase.getStatus().toString())
        .shippingNoteNumber(purchase.getShippingNoteNr())
        .shippingPartner(purchase.getShippingPartner())
        .invoiceNumber(purchase.getInvoiceNr())
        .invoiceUrl(shopUrlGenerator.generateInvoiceUrl(purchase))
        .invoiceDate(purchase.getInvoiceDate())
        .orderNr(purchase.getOrderNr().toString())
        .orderDate(purchase.getOrderDate().atZone(KIGAZONE).toLocalDateTime().toString())
        .paymentMethod(purchase.getPaymentMethod())
        .memberId(purchase.getCustomer().getCustomerId())
        .memberEmail(purchase.getEmail())
        .phoneNr(purchase.getPhoneNr())
        .street(purchase.getStreet())
        .streetNumber(purchase.getStreetNumber())
        .floorNumber(purchase.getFloorNumber())
        .doorNumber(purchase.getDoorNumber())
        .zip(purchase.getZip())
        .city(purchase.getCity())
        .country(purchase.getCountryTitle())
        .billAddresssName1(purchase.getBillAddressName1())
        .billAddresssName2(purchase.getBillAddressName2())
        .billStreet(purchase.getBillStreet())
        .billStreetnumber(purchase.getBillStreetNumber())
        .billFloornumber(purchase.getBillFloorNumber())
        .billDoornumber(purchase.getBillDoorNumber())
        .billZip(purchase.getBillZip())
        .billCity(purchase.getBillCity())
        .billCountry(
            Optional.ofNullable(purchase.getBillCountry()).map(Country::getCode).orElse(""))
        .comment(purchase.getComment())
        .purchaseItems(
            purchase
                .getPurchaseItems()
                .stream()
                .map(PurchaseItemResponse::toPurchaseItemResponse)
                .collect(toList()))
        .workflow(
            purchaseWorkflowRepository
                .findAllByPurchase(purchase)
                .stream()
                .map(this::mapPurchaseWorkflowResponse)
                .collect(toList()))
        .invoices(
            invoiceVersionRepository
                .findAllByPurchase(purchase)
                .stream()
                .map(this::mapInvoiceVersionResponse)
                .collect(toList()))
        .voucherUrl(shopUrlGenerator.generateCreditVoucherUrl(purchase))
        .voucherNr(purchase.getCreditVoucherNr())
        .build();
  }

  protected PurchaseWorkflowResponse mapPurchaseWorkflowResponse(
      PurchaseWorkflow purchaseWorkflow) {
    return PurchaseWorkflowResponse.builder()
        .name(
            Optional.ofNullable(purchaseWorkflow.getMember())
                .map(member -> member.getFirstname() + " " + member.getLastname())
                .orElse("server"))
        .changeDate(Date.from(purchaseWorkflow.getChangeDate()))
        .prevStatus(purchaseWorkflow.getPrevStatus())
        .status(purchaseWorkflow.getStatus())
        .build();
  }

  protected InvoiceVersionResponse mapInvoiceVersionResponse(InvoiceVersion invoiceVersion) {
    return InvoiceVersionResponse.builder()
        .id(invoiceVersion.getId())
        .version(invoiceVersion.getInvoiceVersion())
        .changeDate(invoiceVersion.getCreated())
        .build();
  }
}
