package com.kiga.shop.backend.web.converter;

import com.kiga.shop.backend.web.response.VoucherViewModel;
import com.kiga.shop.domain.Voucher;
import com.kiga.web.converter.ResponseConverter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Florian Schneider
 * @since 20/11/17.
 */
public final class VoucherToViewModelConverter
    implements ResponseConverter<Voucher, VoucherViewModel> {
  /**
   * Convert list of vouchers entities to view models.
   *
   * @param vouchers entities list to map
   * @return vouchers view models list
   */
  public List<VoucherViewModel> convertToResponse(List<Voucher> vouchers) {
    if (vouchers == null) {
      return null;
    }
    return vouchers.stream().map(this::convertToResponse).collect(Collectors.toList());
  }

  /**
   * Converts Voucher entity to ProductViewModel.
   *
   * @param voucher entity
   * @return view model
   */
  @Override
  public VoucherViewModel convertToResponse(Voucher voucher) {
    if (voucher == null) {
      return null;
    }

    VoucherViewModel voucherViewModel = new VoucherViewModel();
    voucherViewModel.setDiscountRate(voucher.getDiscountRate());
    voucherViewModel.setCode(voucher.getCode());
    voucherViewModel.setId(voucher.getId());
    if (voucher.getProduct() == null) {
      voucherViewModel.setProductId(null);
      voucherViewModel.setProductDisplayString("PRODUCT_NOT_FOUND (code_not_found)");
    } else {
      voucherViewModel.setProductId(voucher.getProduct().getId());
      voucherViewModel.setProductDisplayString(
          voucher.getProduct().getProductTitle() + " (" + voucher.getProduct().getCode() + ")");
    }

    if (voucher.getValidFromDate() != null) {
      voucherViewModel.setValidFrom(Date.from(voucher.getValidFromDate()));
    }

    if (voucher.getValidToDate() != null) {
      voucherViewModel.setValidTo(Date.from(voucher.getValidToDate()));
    }

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("Y-M-dd HH:mm:ss");
    if (voucher.getCreated() != null) {
      voucherViewModel.setCreatedOn(simpleDateFormat.format(voucher.getCreated()));
    }
    if (voucher.getLastEdited() != null) {
      voucherViewModel.setLastEditedOn(simpleDateFormat.format(voucher.getLastEdited()));
    }

    return voucherViewModel;
  }
}
