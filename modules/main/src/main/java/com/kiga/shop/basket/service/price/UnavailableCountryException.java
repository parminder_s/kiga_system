package com.kiga.shop.basket.service.price;

/**
 * This exception is thrown if we have a PurchaseItem for the given user
 * that isn't available in their country.
 */
public class UnavailableCountryException extends RuntimeException {

}
