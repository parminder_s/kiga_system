package com.kiga.shop.backend.service.delivery;

import com.kiga.shop.domain.PurchaseDelivery;
import com.kiga.shop.domain.PurchaseDeliveryPurchaseProduct;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class DeliverySplitterChecker {
  private PurchaseDelivery delivery;
  private List<PurchaseProductIdAmount> productAmounts;

  /** checks if a planned delivery split can be done or not. */
  public boolean isValid(PurchaseDelivery delivery, List<PurchaseProductIdAmount> productAmounts) {
    this.delivery = delivery;
    this.productAmounts = productAmounts;
    if (containsCompleteDelivery(delivery, productAmounts)) {
      return false;
    } else {
      return this.productAmounts.stream().allMatch(this::validatePurchaseItem);
    }
  }

  private boolean containsCompleteDelivery(
      PurchaseDelivery delivery, List<PurchaseProductIdAmount> productAmounts) {
    return delivery
        .getPurchaseProducts()
        .stream()
        .allMatch(
            purchaseProduct ->
                productAmounts
                    .stream()
                    .filter(
                        productAmount ->
                            productAmount.getPurchaseProductId()
                                == purchaseProduct.getPurchaseItem().getId())
                    .allMatch(
                        productAmount -> productAmount.getAmount() == purchaseProduct.getAmount()));
  }

  private boolean validatePurchaseItem(PurchaseProductIdAmount productAmount) {
    Optional<PurchaseDeliveryPurchaseProduct> optPid =
        findPurchaseDeliveryProduct(productAmount.getPurchaseProductId());

    return optPid
        .map(
            pid -> {
              if (pid.getAmount() < productAmount.getAmount()) {
                return false;
              }
              return productAmount.getAmount() >= 1;
            })
        .orElse(false);
  }

  private Optional<PurchaseDeliveryPurchaseProduct> findPurchaseDeliveryProduct(
      int purchaseProductId) {
    return delivery
        .getPurchaseProducts()
        .stream()
        .filter(purchaseProduct -> purchaseProductId == purchaseProduct.getPurchaseItem().getId())
        .findFirst();
  }
}
