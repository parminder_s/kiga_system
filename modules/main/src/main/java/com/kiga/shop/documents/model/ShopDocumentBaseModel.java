package com.kiga.shop.documents.model;

import static com.kiga.util.NowService.KIGAZONE;

import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.shop.documents.service.InvoiceVatTableCalcer;
import com.kiga.shop.documents.service.JAddressFormatter;
import com.kiga.shop.documents.service.JPurchaseItemRowFactory;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.domain.Purchase;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;

/** Created by peter on 13.02.17. */
@Data
@AllArgsConstructor
abstract class ShopDocumentBaseModel {
  protected List<String> shippingAddress;
  protected List<String> billingAddress;
  protected List<PurchaseItemRow> products;
  protected List<InvoiceVatRow> taxTable;
  protected Integer daysToPay;
  protected String invoiceNr;
  protected String customerId;
  protected BigDecimal priceNetTotal;
  protected BigDecimal priceGrossTotal;
  protected BigDecimal transferalSum;
  protected String invoiceDate;
  protected String currency;
  protected Long orderNr;
  protected PaymentMethod paymentMethod;
  protected String salutation;
  protected String lastname;
  DateTimeFormatter formatter =
      DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
          .withLocale(Locale.GERMAN)
          .withZone(KIGAZONE);

  public ShopDocumentBaseModel(
      Purchase purchase, Map<Long, ProductDetail> productDetails, boolean includeShipping) {
    shippingAddress = new JAddressFormatter(purchase).getAddress();
    billingAddress = new JAddressFormatter(purchase).getBillAddress();
    products =
        new JPurchaseItemRowFactory().createMultiple(purchase, productDetails, includeShipping);
    taxTable = new InvoiceVatTableCalcer().calc(purchase);
    daysToPay = purchase.getCountry().getCountryShop().getDaysToPayInvoice();
    invoiceNr = purchase.getInvoiceNr();
    customerId = purchase.getCustomer().getCustomerId();
    priceNetTotal = purchase.getPriceNetTotal();
    priceGrossTotal = purchase.getPriceGrossTotal();
    transferalSum = purchase.getPriceGrossTotal();
    invoiceDate = formatter.format(purchase.getInvoiceDate());
    currency = purchase.getCurrency();
    orderNr = purchase.getOrderNr();
    paymentMethod = purchase.getPaymentMethod();
    salutation = purchase.getGender().compareToIgnoreCase("male") == 0 ? "Herr" : "Frau";
    lastname = purchase.getLastname();
  }
}
