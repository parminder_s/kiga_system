package com.kiga.shop.basket.web.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplyVoucherResponse {
  private boolean successful;
  private BasketResponse basket;
}
