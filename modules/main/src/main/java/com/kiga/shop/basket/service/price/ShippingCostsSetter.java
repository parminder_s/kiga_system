package com.kiga.shop.basket.service.price;

import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.service.VatRateCalculator;
import java.math.BigDecimal;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShippingCostsSetter {
  private VatRateCalculator vatRateCalculator;
  private PriceGrossWithoutShippingCalcer priceGrossWithoutShippingCalcer;

  @Autowired
  public ShippingCostsSetter(
      VatRateCalculator vatRateCalculator,
      PriceGrossWithoutShippingCalcer priceGrossWithoutShippingCalcer) {
    this.vatRateCalculator = vatRateCalculator;
    this.priceGrossWithoutShippingCalcer = priceGrossWithoutShippingCalcer;
  }

  /** sets the shipping costs along vat for a Purchase. */
  public void setCosts(Purchase purchase) {
    CountryShop countryShop = purchase.getCountry().getCountryShop();
    purchase.setVatRateShipping(countryShop.getVatRateShipping().setScale(3));

    if (freeShippingApplies(purchase)) {
      purchase.setPriceShippingNet(BigDecimal.ZERO);
      purchase.setVatShipping(BigDecimal.ZERO);
    } else {
      purchase.setPriceShippingNet(countryShop.getPriceShippingNet().setScale(2));
      BigDecimal total =
          vatRateCalculator.calculateGrossPrice(
              purchase.getPriceShippingNet().setScale(3), purchase.getVatRateShipping());
      purchase.setVatShipping(total.subtract(purchase.getPriceShippingNet()));
    }
  }

  private boolean freeShippingApplies(Purchase purchase) {
    BigDecimal freeShippingLimit =
        Optional.ofNullable(purchase.getCountry().getCountryShop().getFreeShippingLimit())
            .orElse(BigDecimal.ZERO);

    if (freeShippingLimit.equals(BigDecimal.ZERO)) {
      return false;
    }

    BigDecimal priceGrossTotalWithoutShipping = priceGrossWithoutShippingCalcer.calc(purchase);
    return priceGrossTotalWithoutShipping.compareTo(freeShippingLimit) > 0;
  }
}
