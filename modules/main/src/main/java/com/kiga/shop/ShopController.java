package com.kiga.shop;

import com.kiga.homepage.domain.HomePageElementContext;
import com.kiga.homepage.service.HomePageElementService;
import com.kiga.homepage.web.responses.SliderResponse;
import com.kiga.security.spring.SecurityUtil;
import com.kiga.shop.backend.web.request.AddProductToBasketRequest;
import com.kiga.shop.service.FinishOrderService;
import com.kiga.shop.service.ShopInvoiceService;
import com.kiga.shop.service.ShopPaymentCheckerService;
import com.kiga.shop.web.requests.SingleIdRequest;
import com.kiga.shop.web.responses.InvoicesResponse;
import com.kiga.web.message.EndpointRequest;
import java.util.List;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/shop")
public class ShopController {

  private final FinishOrderService finishOrderService;
  private final ShopInvoiceService shopInvoiceService;
  private final SecurityUtil securityUtil;
  private final ShopPaymentCheckerService shopPaymentCheckerService;
  private final HomePageElementService homePageElementService;

  @Autowired
  public ShopController(
      FinishOrderService finishOrderService,
      ShopInvoiceService shopInvoiceService,
      SecurityUtil securityUtil,
      ShopPaymentCheckerService shopPaymentCheckerService,
      HomePageElementService homePageElementService) {
    this.finishOrderService = finishOrderService;
    this.shopInvoiceService = shopInvoiceService;
    this.securityUtil = securityUtil;
    this.shopPaymentCheckerService = shopPaymentCheckerService;
    this.homePageElementService = homePageElementService;
  }

  /** Finishes an Order/Purchase which comes in state orderedPending. */
  @RequestMapping(value = "finishOrder", method = RequestMethod.POST)
  public void finishOrder(@RequestBody SingleIdRequest request) {
    finishOrderService.finishPurchase(request.getId());
  }

  /** Get invoices for a specific customer. */
  @RequestMapping(value = "invoices", method = RequestMethod.POST)
  public List<InvoicesResponse> invoices() {
    Long customerId = securityUtil.getMember().getId();
    return shopInvoiceService.getInvoicesForCustomer(customerId);
  }

  /** check suspended payments and output json for manual review. */
  @RequestMapping(value = "check-payments", method = RequestMethod.GET)
  public List<String> checkPayments(@RequestParam(required = false) Boolean persist) {
    if (BooleanUtils.isTrue(persist)) {
      return shopPaymentCheckerService.checkAndFinishedSuspendedPayments();
    } else {
      return shopPaymentCheckerService.checkSuspendedPayments();
    }
  }

  /** Mocked Purchase endpoint. Get ProductId and Amount. */
  @RequestMapping(value = "addToBasket", method = RequestMethod.POST)
  public Boolean addToBasket(
      @RequestBody(required = false) AddProductToBasketRequest addProductRequest) {

    return (addProductRequest.getAmount() > 0 && addProductRequest.getProductId() > 0);
  }

  /** Mocked Purchase endpoint. Get ProductId and Amount. */
  @RequestMapping(value = "slider", method = RequestMethod.POST)
  @Cacheable(value = "default", key = "'shop-slider-' + #endpointRequest.getLocale()")
  public List<SliderResponse> slider(
      @RequestBody(required = false) EndpointRequest endpointRequest) {
    return homePageElementService.getSlider(
        endpointRequest.getLocale(), HomePageElementContext.shop);
  }
}
