package com.kiga.shop.web.response;

import com.kiga.shop.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author bbs
 * @since 3/19/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductInfo {
  // Product:
  private long id;
  private String productTitle;
  private String productCode;
  private BigDecimal volumeLitre;
  private int weightGram;

  // ProductDetail:
  private BigDecimal priceNet;
  private BigDecimal priceGross;
  private BigDecimal vatRate;
  private Long maxDeliveryDays;
  private Long minDeliveryDays;

  // CountryShop:
  private BigDecimal freeShippingLimit;
  private BigDecimal priceShippingNet;
  private BigDecimal vatRateShipping;
  private Integer daysToPayInvoice;

  // Country:
  private String currency;

  // calculated
  private BigDecimal vatProduct;

  private Product product;
}
