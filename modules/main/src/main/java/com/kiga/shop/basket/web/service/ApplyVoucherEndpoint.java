package com.kiga.shop.basket.web.service;

import com.kiga.shop.basket.voucher.ApplyVoucher;
import com.kiga.shop.basket.web.request.ApplyVoucherRequest;
import com.kiga.shop.basket.web.response.ApplyVoucherResponse;
import com.kiga.shop.domain.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplyVoucherEndpoint {
  private BasketHttpRetriever basketHttpRetriever;
  private ApplyVoucher applyVoucher;
  private BasketMapper basketMapper;

  /** inject dependencies. */
  @Autowired
  public ApplyVoucherEndpoint(
      BasketHttpRetriever basketHttpRetriever,
      ApplyVoucher applyVoucher,
      BasketMapper basketMapper) {
    this.basketHttpRetriever = basketHttpRetriever;
    this.applyVoucher = applyVoucher;
    this.basketMapper = basketMapper;
  }

  /** gets the basket, applies discount and returns result. */
  public ApplyVoucherResponse apply(ApplyVoucherRequest applyVoucherRequest) {
    Purchase purchase = basketHttpRetriever.getCurrentBasket().get();
    boolean successful = this.applyVoucher.apply(applyVoucherRequest.getCode(), purchase);

    return ApplyVoucherResponse.builder()
        .basket(basketMapper.map(purchase))
        .successful(successful)
        .build();
  }
}
