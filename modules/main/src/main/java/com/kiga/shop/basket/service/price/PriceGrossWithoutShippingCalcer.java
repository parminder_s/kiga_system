package com.kiga.shop.basket.service.price;

import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PriceGrossWithoutShippingCalcer {
  /**
   * helper function for summing the total gross prices over all purchase items.
   */
  public BigDecimal calc(Purchase purchase) {
    return purchase.getPurchaseItems().stream()
      .map(PurchaseItem::getPriceGrossTotal)
      .reduce(BigDecimal::add)
      .orElse(BigDecimal.ZERO);
  }
}
