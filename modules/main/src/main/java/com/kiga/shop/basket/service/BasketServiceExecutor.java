package com.kiga.shop.basket.service;

import com.kiga.main.locale.Locale;
import com.kiga.shop.basket.web.service.BasketFactory;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.function.BiConsumer;
import javax.servlet.http.HttpServletRequest;

/**
 * This class is used by the basket service and provides the purchase and purchaseItem
 * which has to be modified. If Purchase or PuchaseItem does not exist, they are created.
 */
@Service
public class BasketServiceExecutor {
  private BasketFactory basketFactory;
  private FindOrMakePurchaseItem findOrMakePurchaseItem;
  private PricesSetter pricesSetter;

  /**
   * inject beans.
   */
  @Autowired
  public BasketServiceExecutor(
    BasketFactory basketFactory, FindOrMakePurchaseItem findOrMakePurchaseItem,
    PricesSetter pricesSetter) {
    this.basketFactory = basketFactory;
    this.findOrMakePurchaseItem = findOrMakePurchaseItem;
    this.pricesSetter = pricesSetter;
  }

  /**
   * executes a consumer function which is passed the purchase and the item.
   */
  public Purchase execute(
    Optional<Purchase> optPurchase, long productId, Locale locale,
    BiConsumer<Purchase, PurchaseItem> biConsumer) {
    Purchase returner = optPurchase.orElseGet(() -> basketFactory.create(locale));
    biConsumer.accept(returner, findOrMakePurchaseItem.get(returner, productId));
    pricesSetter.setPrices(returner);
    return returner;
  }

}
