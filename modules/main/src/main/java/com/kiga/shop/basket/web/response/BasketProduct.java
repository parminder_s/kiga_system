package com.kiga.shop.basket.web.response;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class BasketProduct {
  private long id;
  private long productId;
  private String title;
  private int amount;
  private BigDecimal price;
  private BigDecimal originalPrice;
  private BigDecimal sum;
  private BigDecimal originalSum;
  private long minDeliveryDays;
  private long maxDeliveryDays;
  private boolean hasDiscount;
}
