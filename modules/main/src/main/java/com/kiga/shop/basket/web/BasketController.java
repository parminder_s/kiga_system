package com.kiga.shop.basket.web;

import com.kiga.shop.basket.service.BasketService;
import com.kiga.shop.basket.service.CountryDeliveryCheck;
import com.kiga.shop.basket.web.request.ApplyVoucherRequest;
import com.kiga.shop.basket.web.response.ApplyVoucherResponse;
import com.kiga.shop.basket.web.response.BasketResponse;
import com.kiga.shop.basket.web.service.ApplyVoucherEndpoint;
import com.kiga.shop.basket.web.service.BasketHttpRetriever;
import com.kiga.shop.basket.web.service.BasketMapper;
import com.kiga.shop.basket.web.service.ModifyProductAmountRequest;
import com.kiga.shop.domain.Purchase;
import com.kiga.web.message.EndpointRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/shop/basket")
public class BasketController {
  private CountryDeliveryCheck countryDeliveryCheck;
  private BasketHttpRetriever basketHttpRetriever;
  private BasketMapper basketMapper;
  private BasketService basketService;
  private ApplyVoucherEndpoint applyVoucherEndpoint;

  /** inject beans. */
  @Autowired
  public BasketController(
      CountryDeliveryCheck countryDeliveryCheck,
      BasketHttpRetriever basketHttpRetriever,
      BasketMapper basketMapper,
      BasketService basketService,
      ApplyVoucherEndpoint applyVoucherEndpoint) {
    this.countryDeliveryCheck = countryDeliveryCheck;
    this.basketHttpRetriever = basketHttpRetriever;
    this.basketMapper = basketMapper;
    this.basketService = basketService;
    this.applyVoucherEndpoint = applyVoucherEndpoint;
  }

  @RequestMapping(value = "/canDeliver", method = RequestMethod.GET)
  public boolean canDeliver() {
    return this.countryDeliveryCheck.canDeliver(basketHttpRetriever.getCurrentBasket());
  }

  /** returns the current basket or null if it does not exist. */
  @RequestMapping(value = "/find", method = RequestMethod.GET)
  @ResponseBody
  public BasketResponse find() {
    return this.basketHttpRetriever.getCurrentBasket().map(basketMapper::map).orElse(null);
  }

  /** adds a product to a basket. */
  @RequestMapping(value = "add-product", method = RequestMethod.POST)
  @ResponseBody
  public void addProduct(@RequestBody ModifyProductAmountRequest modifyProductAmountRequest) {
    basketService.addProduct(
        basketHttpRetriever.getCurrentBasket(), modifyProductAmountRequest.getProductId(),
        modifyProductAmountRequest.getAmount(), modifyProductAmountRequest.getLocale());
  }

  /** sets the amount of a product in a basket. */
  @RequestMapping(value = "set-amount", method = RequestMethod.POST)
  @ResponseBody
  public BasketResponse setAmount(
      @RequestBody ModifyProductAmountRequest modifyProductAmountRequest) {
    Purchase purchase =
        basketService.setAmount(
            basketHttpRetriever.getCurrentBasket(), modifyProductAmountRequest.getProductId(),
            modifyProductAmountRequest.getAmount(), modifyProductAmountRequest.getLocale());
    return basketMapper.map(purchase);
  }

  /** removes a product from a basket. */
  @RequestMapping(value = "remove-product/{productId}", method = RequestMethod.POST)
  @ResponseBody
  public BasketResponse removeProduct(
      @PathVariable long productId, EndpointRequest endpointRequest) {
    Purchase purchase =
        basketService.removeProduct(
            basketHttpRetriever.getCurrentBasket(), productId, endpointRequest.getLocale());
    return basketMapper.map(purchase);
  }

  /** applies a voucher if it exists. */
  @RequestMapping(value = "apply-voucher", method = RequestMethod.POST)
  @ResponseBody
  public ApplyVoucherResponse applyVoucher(@RequestBody ApplyVoucherRequest applyVoucherRequest) {
    return applyVoucherEndpoint.apply(applyVoucherRequest);
  }
}
