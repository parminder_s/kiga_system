package com.kiga.shop.spec;

/**
 * Created by faxxe on 1/10/17.
 */
public class WorkflowException extends RuntimeException {

  public WorkflowException(String msg) {
    super(msg);
  }

}
