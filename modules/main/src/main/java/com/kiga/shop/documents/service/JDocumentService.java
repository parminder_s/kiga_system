package com.kiga.shop.documents.service;

import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.print.article.FopXml;
import com.kiga.shop.documents.model.PaymentReminderModel;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.shop.service.ProductHelper;
import freemarker.core.TemplateNumberFormatFactory;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by peter on 2/15/17.
 */

public class JDocumentService {

  Configuration freeMarkerConfig;
  PurchaseRepository purchaseRepository;
  ProductHelper productHelper;
  InvoiceRepository invoiceRepository;

  /**
   *
   * @param freeMarkerConfig freeMarkerConfig.
   * @param purchaseRepository purchaseRepository.
   * @param productHelper ShopProductHelper.
   */
  public JDocumentService(Configuration freeMarkerConfig, PurchaseRepository purchaseRepository,
                          ProductHelper productHelper, InvoiceRepository invoiceRepository) {
    this.freeMarkerConfig = freeMarkerConfig;
    Map<String, TemplateNumberFormatFactory> formatter = new HashMap<>();
    formatter.put("kigacurrency", new KigaCurrencyFormatterFactory());
    freeMarkerConfig.setCustomNumberFormats(formatter);
    this.purchaseRepository = purchaseRepository;
    this.productHelper = productHelper;
    this.invoiceRepository = invoiceRepository;
  }

  /**
   * Create payment reminder.
   *
   * @param purchase purchase object
   * @return file
   * @throws IOException       exception
   * @throws TemplateException exception
   */
  public File createPaymentReminder(Purchase purchase) throws IOException, TemplateException {
    PaymentReminderModel paymentReminderModel = new PaymentReminderModel(purchase, null);
    return new File(FopXml.print(
      FreeMarkerTemplateUtils.processTemplateIntoString(freeMarkerConfig.getTemplate(
        "shop-payment-reminder.xml"), paymentReminderModel),
      "paymentreminder" + convertToSafeSystemPath(purchase.getInvoiceNr())));
  }

  private String convertToSafeSystemPath(String nonSafeName) {
    return nonSafeName.replace("/", "-");
  }
}
