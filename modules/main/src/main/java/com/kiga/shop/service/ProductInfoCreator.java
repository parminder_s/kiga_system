package com.kiga.shop.service;

import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.domain.QProductDetail;
import com.kiga.shop.repository.ProductDetailRepository;
import com.kiga.shop.web.response.ProductInfo;
import com.kiga.web.service.GeoIpService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * <p>This is the creator for the ProductInfo object, which contains all useful information
 * for a given product in the context of a country.</p>
 *
 * <p>The creator makes heavy usage of the ModelMapper. Since we are mapping here multiple
 * classes to a single destination the order of the mapping calls are important.
 * Sadly I could not manage to make one major deep map.
 *
 */
@Service
public class ProductInfoCreator {
  private VatRateCalculator vatRateCalculator;
  private GeoIpService geoIpService;
  private ProductDetailRepository detailRepository;

  /**
   * inject beans.
   */
  @Autowired
  public ProductInfoCreator(
    VatRateCalculator vatRateCalculator, GeoIpService geoIpService,
    ProductDetailRepository detailRepository) {
    this.vatRateCalculator = vatRateCalculator;
    this.geoIpService = geoIpService;
    this.detailRepository = detailRepository;
  }


  public Optional<ProductInfo> create(long id) {
    return this.create(id, this.geoIpService.resolveCountryCode());
  }

  /**
   * get the product from a given id and country code.
   */
  public Optional<ProductInfo> create(long id, String countryCode) {
    QProductDetail queryProductDetail = QProductDetail.productDetail;
    ProductDetail productDetail = detailRepository.findOne(
      queryProductDetail.country.code.equalsIgnoreCase(countryCode)
        .and(queryProductDetail.product.id.eq(id)));

    if (productDetail == null) {
      return Optional.empty();
    }

    ProductInfo productInfo = new ProductInfo();
    CountryShop countryShop = productDetail.getCountry().getCountryShop();

    ModelMapper modelMapper = new ModelMapper();
    modelMapper.getConfiguration().setSkipNullEnabled(true);
    modelMapper.map(countryShop, productInfo);
    modelMapper.map(productDetail, productInfo);
    modelMapper.map(productDetail.getProduct(), productInfo);

    productInfo.setVatRate(productDetail.getVatProduct());
    productInfo.setPriceGross(vatRateCalculator
      .calculateGrossPrice(productInfo.getPriceNet(), productInfo.getVatRate()));
    productInfo.setVatProduct(productInfo.getPriceGross().subtract(productInfo.getPriceNet()));

    productInfo.setCurrency(productDetail.getCountry().getCurrency());

    return Optional.of(productInfo);
  }
}
