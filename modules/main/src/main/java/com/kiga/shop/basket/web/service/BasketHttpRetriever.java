package com.kiga.shop.basket.web.service;

import com.kiga.main.service.CookieService;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import com.kiga.shop.basket.service.BasketRetriever;
import com.kiga.shop.domain.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;

/**
 * This class connects the http data along the helper services with the acutal
 * BasketFinder to return the active basket.
 */
@Service
public class BasketHttpRetriever {
  private SecurityService securityService;
  private CookieService cookieService;
  private BasketRetriever basketRetriever;

  /**
   * inject beans.
   */
  @Autowired
  public BasketHttpRetriever(
    SecurityService securityService, CookieService cookieService,
    BasketRetriever basketRetriever) {
    this.securityService = securityService;
    this.cookieService = cookieService;
    this.basketRetriever = basketRetriever;
  }

  /**
   * main entry point.
   */
  public Optional<Purchase> getCurrentBasket() {
    Optional<Member> member = securityService.getSessionMember();
    String sessionId = securityService.getSessionIdFromCookie();
    Optional<String> cookieId = cookieService.getOptional(BasketFactory.COOKIE_NAME);

    return basketRetriever.find(member, sessionId, cookieId);
  }
}
