package com.kiga.shop.repository;

import com.kiga.shop.domain.QVoucher;
import com.kiga.shop.domain.Voucher;
import com.mysema.query.types.Predicate;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class VoucherRepositoryExt {
  private VoucherRepository voucherRepository;

  @Autowired
  public VoucherRepositoryExt(VoucherRepository voucherRepository) {
    this.voucherRepository = voucherRepository;
  }

  /**
   * returns for a given String the list of possible vouchers.
   */
  public Optional<Voucher> findValidVoucher(String code, Instant now) {
    QVoucher qvoucher = QVoucher.voucher;
    Predicate predicate = qvoucher.code.toLowerCase().like(code.toLowerCase())
      .and(qvoucher.validFromDate.loe(now))
      .and(qvoucher.validToDate.after(now));
    return Optional.ofNullable(voucherRepository.findOne(predicate));
  }
}
