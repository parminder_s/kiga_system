package com.kiga.shop.backend.web.request;

import lombok.Data;

/**
 * @author bbs
 * @since 10/14/16.
 */
@Data
public final class ToggleActivateProductDetailRequest {
  private long id;
}
