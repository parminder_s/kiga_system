package com.kiga.shop.repository;

import com.kiga.shop.domain.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by rainerh on 29.04.16.
 */
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
  Product findByCode(String code);

  List<Product> findByActivatedIsTrue(Pageable pageable);

  List<Product> findByActivatedIsTrue();

  Product findByActivatedTrueAndId(long id);
}
