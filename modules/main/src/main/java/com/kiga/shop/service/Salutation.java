package com.kiga.shop.service;

public class Salutation {
  public static String getFromGender(String gender) {
    if (gender.equalsIgnoreCase("male")) {
      return "Herr";
    }
    return "Frau";
  }
}
