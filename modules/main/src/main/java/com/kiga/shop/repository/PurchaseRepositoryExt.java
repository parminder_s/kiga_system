package com.kiga.shop.repository;

import static com.kiga.shop.domain.PurchaseStatus.Checkout;
import static com.kiga.shop.domain.PurchaseStatus.InPayment;
import static com.kiga.shop.domain.PurchaseStatus.New;
import static com.kiga.shop.domain.PurchaseStatus.Resumed;

import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.QPurchase;
import com.mysema.query.types.Predicate;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchaseRepositoryExt {
  private PurchaseRepository purchaseRepository;

  @Autowired
  public PurchaseRepositoryExt(PurchaseRepository purchaseRepository) {
    this.purchaseRepository = purchaseRepository;
  }

  /**
   * finds the purchase of a given customer that hasn't been checked out yet. There should be only
   * one or none.
   */
  public Optional<Purchase> findNewByCustomer(long customerId) {
    QPurchase purchase = QPurchase.purchase;
    Predicate predicate =
        purchase
            .status
            .in(Arrays.asList(New, Checkout, Resumed, InPayment))
            .and(purchase.customer.id.eq(customerId));

    Iterator<Purchase> purchases =
        purchaseRepository.findAll(predicate, purchase.lastEdited.desc()).iterator();

    if (purchases.hasNext()) {
      return Optional.of(purchases.next());
    } else {
      return Optional.empty();
    }
  }

  /**
   * Looks up a purchase for an anonymous customer by a sessionId or former sessionId stored in a
   * cookie.
   */
  public Optional<Purchase> findBySession(String sessionId) {
    QPurchase purchase = QPurchase.purchase;
    Predicate predicate =
        purchase
            .sessionId
            .eq(sessionId)
            .and(purchase.customer.isNull())
            .and(purchase.status.eq(New));
    return Optional.ofNullable(this.purchaseRepository.findOne(predicate));
  }
}
