package com.kiga.shop.web.converter;

import com.kiga.shop.domain.CountryGroup;
import com.kiga.shop.web.response.CountryGroupViewModel;
import com.kiga.web.converter.DefaultViewModelConverter;

/**
 * @author bbs
 * @since 10/16/16.
 */
public class CountryGroupToViewModelConverter
  extends DefaultViewModelConverter<CountryGroup, CountryGroupViewModel> {
  @Override
  public CountryGroupViewModel convertToResponse(CountryGroup entity) {
    CountryGroupViewModel returner = new CountryGroupViewModel();
    returner.setTitle(entity.getTitle());
    returner.setId(entity.getId());
    return returner;
  }
}
