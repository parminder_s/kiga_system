package com.kiga.shop.spec;

/**
 * Created by faxxe on 1/10/17.
 */
public interface Workflowable<T extends Enum> {

  void setStatus(T status);

  T getStatus();

}
