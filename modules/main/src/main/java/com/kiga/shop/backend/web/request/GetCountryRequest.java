package com.kiga.shop.backend.web.request;

import lombok.Data;

/**
 * @author bbs
 * @since 3/15/17.
 */
@Data
public class GetCountryRequest {
  private long id;
}
