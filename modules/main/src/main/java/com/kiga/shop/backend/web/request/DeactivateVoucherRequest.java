package com.kiga.shop.backend.web.request;

import lombok.Data;

/**
 * @author Florian Schneider
 * @since 28/11/17.
 */
@Data
public class DeactivateVoucherRequest {
  private long id;
}
