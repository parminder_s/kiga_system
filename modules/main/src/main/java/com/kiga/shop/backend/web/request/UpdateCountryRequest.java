package com.kiga.shop.backend.web.request;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bbs
 * @since 3/15/17.
 */
@Data
public class UpdateCountryRequest {
  private long id;
  private long countryGroupId;
  private BigDecimal vat;
  private String currency;
  private String code3;
  private String code;

  // from countryshop
  private BigDecimal maxPurchasePrice;
  private BigDecimal priceShippingNet;
  private BigDecimal vatRateShipping;
  private BigDecimal freeShippingLimit;
  private String iban;
  private String bic;
  private String bankName;
  private Integer daysToPayInvoice;
  private String accountNr;

  private Long minDeliveryDays;
  private Long maxDeliveryDays;
}
