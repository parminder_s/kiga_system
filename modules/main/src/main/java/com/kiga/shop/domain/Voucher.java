package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.Instant;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Builder
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = "purchaseItems")
public class Voucher extends KigaEntityModel {
  @NotNull
  private String code;

  @NotNull
  private BigDecimal discountRate;

  @ManyToOne
  private Product product;

  @NotNull
  private Instant validFromDate;

  @Builder.Default
  private Boolean activated = true;

  @NotNull
  private Instant validToDate;

  @OneToMany(mappedBy = "voucher")
  private List<PurchaseItem> purchaseItems;

  /**
   * no args constructor setting default values.
   */
  public Voucher() {
    this.activated = true;
  }
}
