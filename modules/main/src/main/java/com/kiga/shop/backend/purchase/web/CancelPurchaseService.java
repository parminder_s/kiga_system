package com.kiga.shop.backend.purchase.web;

import static com.kiga.shop.domain.PurchaseStatus.Cancelled;
import static java.util.Collections.singletonList;

import com.kiga.security.domain.Member;
import com.kiga.security.spring.SecurityUtil;
import com.kiga.shop.backend.api.model.PurchaseResponse;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.workflow.PurchaseWorkflowHistoryService;
import com.kiga.spec.Fetcher;

public class CancelPurchaseService {
  private Fetcher<Purchase> fetcher;
  private PurchaseWorkflowHistoryService workflow;
  private SecurityUtil securityUtil;
  private PurchaseResponseConverter converter;
  private Member member;

  public CancelPurchaseService(
      Fetcher<Purchase> fetcher,
      PurchaseWorkflowHistoryService workflow,
      SecurityUtil securityUtil,
      PurchaseResponseConverter converter) {
    this.fetcher = fetcher;
    this.workflow = workflow;
    this.securityUtil = securityUtil;
    this.converter = converter;
  }

  public PurchaseResponse cancel(long id) {
    Purchase purchase = fetcher.find(id);
    workflow.proceedToState(singletonList(purchase), Cancelled, securityUtil.getMember());
    return converter.convertToResponse(purchase);
  }
}
