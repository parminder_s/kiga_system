package com.kiga.shop.basket.web.service;

import com.kiga.main.locale.Locale;
import com.kiga.main.service.CookieService;
import com.kiga.security.domain.Member;
import com.kiga.security.spring.SecurityUtil;
import com.kiga.shop.basket.service.BasketFactoryExecutor;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.repository.CountryRepository;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.web.service.GeoIpService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasketFactory {
  public static String COOKIE_NAME = "PurchaseID";
  private final SecurityUtil securityUtil;

  private BasketFactoryExecutor basketFactoryExecutor;
  private GeoIpService geoIpService;
  private CountryRepository countryRepository;
  private PurchaseRepository purchaseRepository;
  private CookieService cookieService;

  /** inject dependencies. */
  @Autowired
  public BasketFactory(
      BasketFactoryExecutor basketFactoryExecutor,
      GeoIpService geoIpService,
      CountryRepository countryRepository,
      PurchaseRepository purchaseRepository,
      CookieService cookieService,
      SecurityUtil securityUtil) {
    this.basketFactoryExecutor = basketFactoryExecutor;
    this.geoIpService = geoIpService;
    this.countryRepository = countryRepository;
    this.purchaseRepository = purchaseRepository;
    this.cookieService = cookieService;
    this.securityUtil = securityUtil;
  }

  /**
   * wrapper method that gets all relevant data for creating a new basket from an HttpRequest and
   * stores it into the database.
   */
  public Purchase create(Locale locale) {
    Country country = countryRepository.findByCode(geoIpService.resolveCountryCode());
    Optional<Member> optMember = securityUtil.getOptMember();
    String sessionId = securityUtil.getSessionId();

    Purchase purchase = basketFactoryExecutor.create(country, optMember, locale, sessionId);
    if (!optMember.isPresent()) {
      cookieService.add(COOKIE_NAME, sessionId);
    }
    return purchaseRepository.save(purchase);
  }
}
