package com.kiga.shop.basket.service;

import static com.kiga.shop.domain.PurchaseStatus.New;

import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Purchase;
import java.util.ArrayList;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * creates an empty basket and runs the price calculator over it. If the user is anonymous we store
 * the session id in a cookie.
 */
@Service
public class BasketFactoryExecutor {
  private PricesSetter pricesSetter;

  @Autowired
  public BasketFactoryExecutor(PricesSetter pricesSetter) {
    this.pricesSetter = pricesSetter;
  }

  /** main entry point. */
  public Purchase create(
      Country country, Optional<Member> optMember, Locale locale, String sessionId) {
    Purchase returner =
        new Purchase()
            .builder()
            .locale(locale)
            .currency(country.getCurrency())
            .purchaseItems(new ArrayList<>())
            .status(New)
            .country(country)
            .sessionId(sessionId)
            .currentStep("new")
            .build();
    returner.setClassName("Purchase");
    this.pricesSetter.setPrices(returner);

    optMember.ifPresent(returner::setCustomer);

    return returner;
  }
}
