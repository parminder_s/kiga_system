package com.kiga.shop.backend.service;

import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.numbers.NumberGenerator;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.shop.service.DocumentService;
import com.kiga.util.NowService;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This class creates the is responsible for creating the invoice as document and in collaboration
 * with the accounting module.
 */
@Service
public class InvoiceCreator {
  private final NowService nowService;
  private DocumentService documentService;
  private InvoiceRepository invoiceRepository;
  private PurchaseRepository purchaseRepository;
  private NumberGenerator numberGenerator;
  private PurchaseInvoiceMapper purchaseInvoiceMapper;

  /** constructor. */
  @Autowired
  public InvoiceCreator(
      DocumentService documentService,
      InvoiceRepository invoiceRepository,
      PurchaseRepository purchaseRepository,
      NumberGenerator numberGenerator,
      PurchaseInvoiceMapper purchaseInvoiceMapper,
      NowService nowService) {
    this.documentService = documentService;
    this.invoiceRepository = invoiceRepository;
    this.purchaseRepository = purchaseRepository;
    this.numberGenerator = numberGenerator;
    this.purchaseInvoiceMapper = purchaseInvoiceMapper;
    this.nowService = nowService;
  }

  /** creates the invoice as described above. */
  public void createInvoice(Purchase purchase) {
    String country = purchase.getCountry().getCode().toUpperCase();
    ZonedDateTime now = nowService.getZoned();
    int year = now.getYear();

    String invoiceNumber = getNewInvoiceNumber(country, year);
    Instant invoiceDate = now.toInstant();
    purchase.setInvoiceNr(invoiceNumber);
    purchase.setInvoiceDate(invoiceDate);

    String invoiceNoteUrl = "invoice/" + year + "/" + country + "/";
    this.documentService.addInvoiceToPurchase(purchase, invoiceNoteUrl);

    if (purchase.getPaymentMethod().equals(PaymentMethod.INVOICE)) {
      invoiceRepository.save(purchaseInvoiceMapper.map(purchase));
    }

    purchaseRepository.save(purchase);
  }

  private String getNewInvoiceNumber(String country, int year) {
    Purchase lastOrder =
        purchaseRepository.findTopByInvoiceNrStartingWithOrderByInvoiceNrDesc(
            country.toUpperCase() + year % 10);

    String invoiceNr = Optional.ofNullable(lastOrder).map(Purchase::getInvoiceNr).orElse("");
    return this.numberGenerator.generateInvoiceNr(invoiceNr, country, year);
  }
}
