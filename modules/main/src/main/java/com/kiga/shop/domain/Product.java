package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

/**
 * Created by rainerh on 06.04.16.
 */
@Entity
@Builder
@Data
@EqualsAndHashCode(callSuper = true, exclude = "productDetails")
@ToString(exclude = "productDetails")
@AllArgsConstructor
public class Product extends KigaEntityModel {
  @NotNull
  private String productTitle;

  @NotNull
  private String code;

  @Builder.Default
  private BigDecimal volumeLitre = BigDecimal.ZERO;

  @Builder.Default
  private int weightGram = 0;

  @Builder.Default
  private Boolean activated = true;

  @OneToMany(mappedBy = "product")
  private List<ProductDetail> productDetails;

  /**
   * no args constructor setting default values.
   */
  public Product() {
    this.setClassName("Product");
    this.volumeLitre = BigDecimal.ZERO;
    this.weightGram = 0;
    this.activated = true;
  }
}
