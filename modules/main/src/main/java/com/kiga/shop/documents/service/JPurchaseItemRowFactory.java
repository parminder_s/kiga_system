package com.kiga.shop.documents.service;

import com.kiga.shop.documents.model.PurchaseItemRow;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/** Created by peter on 13.02.17. */
public class JPurchaseItemRowFactory {
  public List<PurchaseItemRow> createMultiple(
      Purchase purchase, Map<Long, ProductDetail> productDetails, Boolean includeShipping) {
    List<PurchaseItemRow> returner =
        purchase
            .getPurchaseItems()
            .stream()
            .map(
                purchaseItem ->
                    this.create(
                        purchaseItem, productDetails.get(purchaseItem.getProduct().getId())))
            .collect(Collectors.toList());

    if (includeShipping) {
      returner.add(createForShipping(purchase));
    }
    return returner;
  }

  public PurchaseItemRow create(PurchaseItem purchaseItem, ProductDetail productDetail) {
    return new PurchaseItemRow(
        purchaseItem.getAmount().toString(),
        purchaseItem.getTitle(),
        purchaseItem.getCode(),
        calcGrossSingle(purchaseItem.getPriceNetSingle(), purchaseItem.getVatRate()),
        purchaseItem.getPriceGrossTotal(),
        VatRateFormatter.format(purchaseItem.getVatRate()) + "%",
        NumberFormat.getInstance(Locale.GERMAN).format(purchaseItem.getWeightGramTotal() / 1000.0),
        productDetail.getCountry().getCountryShop().getMaxDeliveryDays().toString(),
        productDetail.getCountry().getCountryShop().getMinDeliveryDays().toString(),
        purchaseItem.getPriceGrossTotal());
  }

  public PurchaseItemRow createForShipping(Purchase purchase) {
    BigDecimal priceGrossTotal = purchase.getVatShipping().add(purchase.getPriceShippingNet());

    if (purchase.getPriceShippingNet().compareTo(BigDecimal.ZERO) == 0) {
      return new PurchaseItemRow(
          "",
          "Versandkosten",
          "",
          priceGrossTotal.setScale(2),
          priceGrossTotal.setScale(2),
          "",
          "",
          "",
          "",
          BigDecimal.ZERO);
    } else {
      return new PurchaseItemRow(
          "",
          "Versandkosten",
          "",
          priceGrossTotal.setScale(2),
          priceGrossTotal.setScale(2),
          VatRateFormatter.format(purchase.getVatRateShipping()) + "%",
          "",
          "",
          "",
          priceGrossTotal);
    }
  }

  private BigDecimal calcGrossSingle(BigDecimal priceNetSingle, BigDecimal vatRate) {
    return priceNetSingle.multiply(BigDecimal.ONE.add(vatRate)).setScale(2, RoundingMode.HALF_UP);
  }
}
