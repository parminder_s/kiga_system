package com.kiga.shop.web.converter;

import com.kiga.main.locale.Locale;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryLocale;
import com.kiga.shop.repository.CountryLocaleRepository;
import com.kiga.shop.web.response.CountryDropdownViewModel;
import com.kiga.web.converter.DefaultViewModelConverter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author bbs
 * @since 10/16/16.
 */
@Service
public class CountryToDropdownViewModelConverter
  extends DefaultViewModelConverter<Country, CountryDropdownViewModel> {
  private CountryLocaleRepository repository;
  private ModelMapper modelMapper;

  @Autowired
  public CountryToDropdownViewModelConverter(CountryLocaleRepository repository,
    ModelMapper modelMapper) {
    this.repository = repository;
    this.modelMapper = modelMapper;
  }

  @Override
  public CountryDropdownViewModel convertToResponse(Country entity) {
    if (entity == null) {
      return null;
    }

    CountryDropdownViewModel returner = modelMapper.map(entity, CountryDropdownViewModel.class);
    CountryLocale countryLocale = Optional
      .ofNullable(repository.findByCountryAndLocale(entity, Locale.en))
      .orElse(new CountryLocale());
    returner.setTitle(countryLocale.getTitle());

    return returner;
  }
}
