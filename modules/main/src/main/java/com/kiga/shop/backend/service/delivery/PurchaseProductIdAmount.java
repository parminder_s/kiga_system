package com.kiga.shop.backend.service.delivery;

import lombok.Data;

@Data
public class PurchaseProductIdAmount {
  private final int purchaseProductId;
  private final int amount;

  public PurchaseProductIdAmount(int purchaseProductId, int amount) {
    this.purchaseProductId = purchaseProductId;
    this.amount = amount;
  }
}
