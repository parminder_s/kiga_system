package com.kiga.shop.backend.exception;

/**
 * @author bbs
 * @since 10/8/16.
 */
public class IllegalPaginationArgumentException extends Exception {
  public IllegalPaginationArgumentException(String message) {
    super(message);
  }
}
