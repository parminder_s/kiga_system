package com.kiga.shop.domain;

/** Created by faxxe on 1/10/17. */
public enum PurchaseStatus {
  New,
  Resumed,
  FailedCheckout,
  Checkout,
  InPayment,

  OrderedPending,
  Ordered,

  ShippingInformationCreated,
  CsvExported,
  CsvFinished,
  ReadyForShipping,
  Shipped,

  CreditVoucherPending,
  CreditVoucherFinalized,

  Error,
  Cancelled
}
