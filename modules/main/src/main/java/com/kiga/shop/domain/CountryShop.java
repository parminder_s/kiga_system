package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * Created by faxxe on 9/8/16.
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Builder
@Inheritance(strategy = InheritanceType.JOINED)
public class CountryShop extends KigaEntityModel {
  private BigDecimal maxPurchasePrice;
  private BigDecimal priceShippingNet;
  private BigDecimal vatRateShipping;
  private BigDecimal freeShippingLimit;
  private String iban;
  private String bic;
  private String bankName;
  private Integer daysToPayInvoice;
  private String accountNr;
  private Long maxDeliveryDays;
  private Long minDeliveryDays;

  @OneToOne
  private Country country;

  public CountryShop() {
    this.setClassName("CountryShop");
  }

  /**
   * getters are required for Scala compatibility.
   */
  public BigDecimal getMaxPurchasePrice() {
    return maxPurchasePrice;
  }

  public BigDecimal getPriceShippingNet() {
    return priceShippingNet;
  }

  public BigDecimal getVatRateShipping() {
    return vatRateShipping;
  }

  public BigDecimal getFreeShippingLimit() {
    return freeShippingLimit;
  }

  public String getIban() {
    return iban;
  }

  public String getBic() {
    return bic;
  }

  public String getBankName() {
    return bankName;
  }

  public Integer getDaysToPayInvoice() {
    return daysToPayInvoice;
  }

  public String getAccountNr() {
    return accountNr;
  }

  public Long getMaxDeliveryDays() {
    return maxDeliveryDays;
  }

  public Long getMinDeliveryDays() {
    return minDeliveryDays;
  }

  public Country getCountry() {
    return country;
  }
}
