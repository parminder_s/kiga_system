package com.kiga.shop.backend.web.converter;

import com.kiga.shop.backend.web.response.ProductViewModel;
import com.kiga.shop.domain.Product;
import com.kiga.web.converter.ResponseConverter;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 10/7/16.
 */
public final class ProductToViewModelConverter
    implements ResponseConverter<Product, ProductViewModel> {
  /**
   * Convert list of products entities to view models.
   *
   * @param products entities list to map
   * @return products view models list
   */
  public List<ProductViewModel> convertToResponse(List<Product> products) {
    if (products == null) {
      return null;
    }
    return products.stream().map(this::convertToResponse).collect(Collectors.toList());
  }

  /**
   * Converts Product entity to ProductViewModel.
   *
   * @param product entity
   * @return view model
   */
  @Override
  public ProductViewModel convertToResponse(Product product) {
    if (product == null) {
      return null;
    }

    ProductViewModel productViewModel = new ProductViewModel();
    productViewModel.setProductTitle(product.getProductTitle());
    productViewModel.setCode(product.getCode());
    productViewModel.setId(product.getId());
    productViewModel.setWeightGram(product.getWeightGram());
    productViewModel.setVolumeLitre(product.getVolumeLitre());

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("Y-M-dd HH:mm:ss");
    if (product.getCreated() != null) {
      productViewModel.setCreatedOn(simpleDateFormat.format(product.getCreated()));
    }
    if (product.getLastEdited() != null) {
      productViewModel.setLastEditedOn(simpleDateFormat.format(product.getLastEdited()));
    }

    return productViewModel;
  }
}
