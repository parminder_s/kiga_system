package com.kiga.shop.documents.service;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneak;

import com.kiga.print.article.FopXml;
import freemarker.core.TemplateNumberFormatFactory;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by robert on 16.09.17.
 */
@Service
public class PrintingService {
  private Configuration freemarkerConfig;

  /**
   * Inject dependencies.
   *
   * @param freemarkerConfig   freemarkerConfig
   */
  @Autowired
  public PrintingService(Configuration freemarkerConfig) {
    this.freemarkerConfig = freemarkerConfig;

    Map<String, TemplateNumberFormatFactory> formatter = new HashMap<>();
    formatter.put("kigacurrency", new KigaCurrencyFormatterFactory());
    freemarkerConfig.setCustomNumberFormats(formatter);
  }

  /**
   * Prints document.
   */
  public File printDocument(String templateName, Map<String, Object> model, String filename) {
    return sneak(() -> {
      Template template = freemarkerConfig.getTemplate(templateName);
      String templateXml = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
      return new File(FopXml.print(templateXml, filename));
    });
  }
}
