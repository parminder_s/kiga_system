package com.kiga.shop.repository;

import com.kiga.security.domain.Member;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseStatus;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/** Created by rainerh on 29.04.16. */
public interface PurchaseRepository
    extends CrudRepository<Purchase, Long>, QueryDslPredicateExecutor<Purchase> {
  List<Purchase> findAll();

  Purchase findById(Long id);

  Purchase findByInvoiceNr(String invoiceNr);

  Purchase findByOrderNr(Long orderNr);

  List<Purchase> findByCustomer(Member member);

  @Query("select p from Purchase p where p.customer.id = :customerId and p.invoiceNr is not null")
  List<Purchase> findByCustomerId(@Param("customerId") Long customerId);

  Purchase findTopByInvoiceNrStartingWithOrderByInvoiceNrDesc(String invoiceNr);

  Purchase findTopByShippingNoteNrStartingWithOrderByShippingNoteNrDesc(String shippingNoteNr);

  Purchase findTopByOrderByCreditVoucherNrDesc();

  @Query("select MAX(p.orderNr) from Purchase p")
  Object findMaxOrderNr();

  List<Purchase> findByStatus(PurchaseStatus status);
}
