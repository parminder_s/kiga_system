package com.kiga.shop.backend.web.request;

import lombok.Data;

/**
 * @author bbs
 * @since 10/7/16.
 */
@Data
public final class ProductsListRequest {
  private int page;
  private int pageSize;
}
