package com.kiga.shop.backend;

import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;

import java.util.Collections;
import java.util.stream.Collectors;

/**
 * @author Florian Schneider
 * @since 14/11/17.
 */
public class ProductCloner {

  /**
   * Clone product.
   */
  public Product cloneProduct(Product product) {

    Product productClone = Product.builder()
            .productTitle(product.getProductTitle() + "_C")
            .code(product.getCode() + "_C")
            .productDetails(product.getProductDetails()
                    .stream()
                    .map(productDetail ->
                            ProductDetail.builder()
                                    .priceNet(productDetail.getPriceNet())
                                    .shippingCosts(productDetail.getShippingCosts())
                                    .activated(productDetail.getActivated())
                                    .vatProduct(productDetail.getVatProduct())
                                    .vatShipping(productDetail.getVatShipping())
                                    .freeShippingAmount(productDetail.getFreeShippingAmount())
                                    .shippingDiscountType(productDetail.getShippingDiscountType())
                                    .oldPriceGross(productDetail.getOldPriceGross())
                                    .countryGroup(productDetail.getCountryGroup())
                                    .country(productDetail.getCountry())
                                    .shippingDiscounts(Collections.emptyList())
                                    .build())
                    .collect(Collectors.toList()))
            .volumeLitre(product.getVolumeLitre())
            .weightGram(product.getWeightGram())
            .activated(product.getActivated())
            .build();
    productClone.setClassName("Product");
    productClone.setId(null);
    productClone.getProductDetails().stream()
            .forEach(productDetail -> {
              productDetail.setProduct(productClone);
              productDetail.setClassName("ProductDetail");
              productDetail.setId(null);
            });

    return productClone;
  }
}
