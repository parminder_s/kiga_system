package com.kiga.shop.service;

import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.crm.web.response.InvoiceResponse;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.shop.web.responses.InvoicesResponse;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by faxxe on 4/13/16.
 */
public class DefaultShopInvoiceService implements ShopInvoiceService {

  private PurchaseRepository purchaseRepository;
  private ShopUrlGenerator shopUrlGenerator;
  private InvoiceRepository invoiceRepository;

  /**
   * Constuctor.
   */
  public DefaultShopInvoiceService(PurchaseRepository purchaseRepository,
                                   ShopUrlGenerator shopUrlGenerator,
                                   InvoiceRepository invoiceRepository) {
    this.purchaseRepository = purchaseRepository;
    this.shopUrlGenerator = shopUrlGenerator;
    this.invoiceRepository = invoiceRepository;
  }

  @Override
  public List<InvoicesResponse> getInvoicesForCustomer(Long customerId) {
    List<Purchase> purchases = purchaseRepository.findByCustomerId(customerId);
    return purchases.stream().map(purchase ->
      new InvoicesResponse(
        purchase.getInvoiceNr(),
        purchase.getInvoiceDate(),
        shopUrlGenerator.generateInvoiceUrl(purchase),
        purchase.getPriceGrossTotal(),
        purchase.getCurrency(),
        isPaid(purchase)
      )).collect(Collectors.toList());
  }

  private boolean isPaid(Purchase purchase) {
    if (PaymentMethod.INVOICE.equals(purchase.getPaymentMethod())) {
      return invoiceRepository.findByInvoiceNumber(purchase.getInvoiceNr()).isSettled();
    } else {
      return true;
    }
  }
}
