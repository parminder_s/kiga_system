package com.kiga.shop;

import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.db.RepositoryFetcher;
import com.kiga.db.RepositoryPersister;
import com.kiga.db.RepositoryRemover;
import com.kiga.mail.Mailer;
import com.kiga.payment.PaymentCheckerService;
import com.kiga.security.services.SecurityService;
import com.kiga.security.spring.SecurityUtil;
import com.kiga.shop.backend.purchase.PurchaseCanceller;
import com.kiga.shop.backend.purchase.web.CancelPurchaseService;
import com.kiga.shop.backend.purchase.web.PurchaseResponseConverter;
import com.kiga.shop.backend.service.delivery.DeliveryCreator;
import com.kiga.shop.backend.service.delivery.DeliveryPersister;
import com.kiga.shop.backend.service.delivery.DeliverySplitter;
import com.kiga.shop.backend.service.delivery.DeliverySplitterChecker;
import com.kiga.shop.backend.service.delivery.DeliverySplitterExecutor;
import com.kiga.shop.backend.service.delivery.PurchaseDeliveryMapper;
import com.kiga.shop.documents.service.JDocumentService;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseStatus;
import com.kiga.shop.numbers.NumberGenerator;
import com.kiga.shop.repository.CountryGroupRepository;
import com.kiga.shop.repository.CountryLocaleRepository;
import com.kiga.shop.repository.CreditVoucherItemRepository;
import com.kiga.shop.repository.ProductDetailRepository;
import com.kiga.shop.repository.PurchaseDeliveryPurchaseProductRepository;
import com.kiga.shop.repository.PurchaseDeliveryRepository;
import com.kiga.shop.repository.PurchaseItemRepository;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.shop.repository.PurchaseWorkflowRepository;
import com.kiga.shop.service.CountryGroupService;
import com.kiga.shop.service.CreditVoucherService;
import com.kiga.shop.service.DefaultProductHelper;
import com.kiga.shop.service.DefaultShopInvoiceService;
import com.kiga.shop.service.FinishOrderService;
import com.kiga.shop.service.ProductHelper;
import com.kiga.shop.service.ShopInvoiceService;
import com.kiga.shop.service.ShopPaymentCheckerService;
import com.kiga.shop.service.ShopS3Uploader;
import com.kiga.shop.service.ShopUrlGenerator;
import com.kiga.shop.web.converter.CountryGroupToViewModelConverter;
import com.kiga.shop.web.converter.CountryToViewModelConverter;
import com.kiga.shop.workflow.PurchaseWorkflowHistoryService;
import com.kiga.shop.workflow.PurchaseWorkflowLogic;
import com.kiga.shop.workflow.Workflow;
import com.kiga.web.service.UrlGenerator;
import java.util.Collections;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/** Created by rainerh on 03.04.16. */
@Configuration
@ConfigurationProperties("shop")
public class ShopConfig {

  @Autowired
  @Value("${shop.shopEmail}")
  private String shopMail;

  @Autowired
  @Value("${shop.bucketName}")
  private String bucketName;

  @Autowired private SecurityService securityService;
  @Autowired private PurchaseRepository purchaseRepository;
  @Autowired private CreditVoucherItemRepository creditVoucherItemRepository;
  @Autowired private ShopUrlGenerator shopUrlGenerator;
  @Autowired private UrlGenerator urlGenerator;
  @Autowired private Mailer mailer;

  @Autowired private PaymentCheckerService paymentCheckerService;

  @Bean
  protected ProductHelper productHelper(
      ProductDetailRepository productDetailRepository,
      PurchaseItemRepository purchaseItemRepository) {
    return new DefaultProductHelper(productDetailRepository, purchaseItemRepository);
  }

  @Bean
  protected ShopUrlGenerator shopUrlGenerator(UrlGenerator urlGenerator) {
    return new ShopUrlGenerator(urlGenerator);
  }

  @Bean
  protected ShopInvoiceService shopInvoiceService(InvoiceRepository invoiceRepository) {
    return new DefaultShopInvoiceService(purchaseRepository, shopUrlGenerator, invoiceRepository);
  }

  @Bean
  protected ShopPaymentCheckerService shopPaymentCheckerService(
      PaymentCheckerService paymentCheckerService,
      PurchaseRepository purchaseRepository,
      FinishOrderService finishOrderService) {
    ShopPaymentCheckerService shopPaymentCheckerService =
        new ShopPaymentCheckerService(
            paymentCheckerService, purchaseRepository, finishOrderService);

    return shopPaymentCheckerService;
  }

  @Bean
  protected CreditVoucherService creditVoucherService(
      ProductHelper productHelper,
      ShopS3Uploader shopS3Uploader,
      InvoiceRepository invoiceRepository) {
    return new CreditVoucherService(
        mailer,
        purchaseRepository,
        new NumberGenerator(),
        shopUrlGenerator,
        shopMail,
        bucketName,
        productHelper,
        creditVoucherItemRepository,
        shopS3Uploader,
        invoiceRepository);
  }

  @Bean
  public CountryGroupService createCountryGroupService(
      CountryGroupRepository countryGroupRepository,
      CountryGroupToViewModelConverter countryGroupToViewModelConverter) {
    return new CountryGroupService(countryGroupRepository, countryGroupToViewModelConverter);
  }

  @Bean
  public CountryToViewModelConverter createCountryToViewModelConverter(
      CountryLocaleRepository repository, ModelMapper modelMapper) {
    return new CountryToViewModelConverter(repository, modelMapper);
  }

  @Bean
  public CountryGroupToViewModelConverter createCountryGroupToViewModelConverter() {
    return new CountryGroupToViewModelConverter();
  }

  @Bean
  public Workflow<Purchase, PurchaseStatus> workflow(PurchaseWorkflowLogic purchaseWorkflowLogic) {
    return new Workflow<>(Collections.singletonList(purchaseWorkflowLogic));
  }

  @Bean
  public PurchaseWorkflowHistoryService purchaseWorkflowHistoryService(
      PurchaseWorkflowRepository purchaseWorkflowRepository,
      Workflow<Purchase, PurchaseStatus> workflow) {
    return new PurchaseWorkflowHistoryService(purchaseWorkflowRepository, workflow);
  }

  @Bean
  public JDocumentService javaDocumentService(
      freemarker.template.Configuration freemarkerConfig,
      PurchaseRepository purchaseRepository,
      ProductHelper productHelper,
      InvoiceRepository invoiceRepository) {
    return new JDocumentService(
        freemarkerConfig, purchaseRepository, productHelper, invoiceRepository);
  }

  @Bean
  public NumberGenerator numberGenerator() {
    return new NumberGenerator();
  }

  @Bean
  public DeliveryPersister deliveryPersister(
      PurchaseDeliveryRepository purchaseDeliveryRepository,
      PurchaseDeliveryPurchaseProductRepository purchaseDeliveryPurchaseProductRepository) {
    return new DeliveryPersister(
        new RepositoryPersister<>(purchaseDeliveryRepository),
        new RepositoryPersister<>(purchaseDeliveryPurchaseProductRepository),
        new RepositoryRemover<>(purchaseDeliveryPurchaseProductRepository));
  }

  @Bean
  public DeliverySplitter deliverySplitter(
      DeliverySplitterChecker checker,
      DeliverySplitterExecutor executor,
      PurchaseDeliveryRepository repository,
      DeliveryPersister persister) {
    return new DeliverySplitter(
        PurchaseDeliveryMapper.INSTANCE,
        checker,
        executor,
        new RepositoryFetcher<>(repository),
        persister);
  }

  @Bean
  public DeliveryCreator deliveryCreator(
      PurchaseDeliveryRepository purchaseDeliveryRepository,
      PurchaseDeliveryPurchaseProductRepository purchaseDeliveryPurchaseProductRepository) {
    return new DeliveryCreator(
        new RepositoryPersister<>(purchaseDeliveryRepository),
        new RepositoryPersister<>(purchaseDeliveryPurchaseProductRepository));
  }

  @Bean
  public PurchaseCanceller getPurchaseCancellor(Mailer mailer) {
    return new PurchaseCanceller(mailer, shopMail);
  }

  @Bean
  public CancelPurchaseService getCancelPurchaseServce(
      PurchaseRepository repository,
      PurchaseWorkflowHistoryService workflowHistoryService,
      SecurityUtil securityUtil,
      PurchaseResponseConverter purchaseResponseConverter) {
    return new CancelPurchaseService(
        new RepositoryFetcher<>(repository),
        workflowHistoryService,
        securityUtil,
        purchaseResponseConverter);
  }
}
