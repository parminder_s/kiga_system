package com.kiga.shop.service;

import com.kiga.spec.Job;
import com.kiga.spec.Jobable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by faxxe on 2/22/17.
 */
@Job(cronProperty = "shopPaymentCheckerCron")
@Service
public class ShopPaymentCheckerJob implements Jobable {

  Logger logger = LoggerFactory.getLogger(getClass());

  private ShopPaymentCheckerService shopPaymentCheckerService;

  @Autowired
  public ShopPaymentCheckerJob(ShopPaymentCheckerService shopPaymentCheckerService) {
    this.shopPaymentCheckerService = shopPaymentCheckerService;
  }

  @Override
  public void runJob() {
    if (shopPaymentCheckerService != null) {
      logger.info("running shopPaymentChecker");
      shopPaymentCheckerService.checkAndFinishedSuspendedPayments();
    }
  }
}
