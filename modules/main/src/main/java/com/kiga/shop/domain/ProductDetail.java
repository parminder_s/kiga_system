package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Created by rainerh on 06.04.16.
 */
@Entity
@Builder
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductDetail extends KigaEntityModel {
  private BigDecimal priceNet;

  @Builder.Default
  private BigDecimal shippingCosts = BigDecimal.ZERO;

  @Builder.Default
  private Boolean activated = true;

  private BigDecimal vatProduct;
  private BigDecimal vatShipping;
  private Integer freeShippingAmount;
  private String shippingDiscountType;
  private BigDecimal oldPriceGross;

  @ManyToOne
  @JoinColumn(name = "countryGroupID", referencedColumnName = "id")
  private CountryGroup countryGroup;

  @ManyToOne
  @JoinColumn(name = "countryID", referencedColumnName = "id")
  private Country country;

  @ManyToOne
  @JoinColumn(name = "productId", referencedColumnName = "id")
  private Product product;

  @OneToMany(mappedBy = "productDetail")
  private List<ShippingDiscount> shippingDiscounts;

  /**
   * no args constructor setting default values.
   */
  public ProductDetail() {
    this.setClassName("ProductDetail");
    this.shippingCosts = BigDecimal.ZERO;
    this.activated = true;
  }

  public Country getCountry() {
    return country;
  }
}
