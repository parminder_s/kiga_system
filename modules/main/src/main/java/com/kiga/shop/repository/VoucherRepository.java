package com.kiga.shop.repository;

import com.kiga.shop.domain.Voucher;
import com.mysema.query.types.Predicate;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.List;

public interface VoucherRepository
  extends JpaRepository<Voucher, Long>, QueryDslPredicateExecutor<Voucher> {
  List<Voucher> findAll(Predicate predicate);

  List<Voucher> findByActivatedIsTrue(Pageable pageable);

  Voucher findByActivatedTrueAndId(long id);

  Voucher findByCode(String code);
}
