package com.kiga.shop.repository;

import com.kiga.main.locale.Locale;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryLocale;
import java.util.List;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

/** Created by rainerh on 19.10.16. */
public interface CountryLocaleRepository extends JpaRepository<CountryLocale, Long> {
  CountryLocale findByCountryAndLocale(Country country, Locale locale);

  @EntityGraph(
      attributePaths = {"country"},
      type = EntityGraph.EntityGraphType.FETCH)
  List<CountryLocale> findByLocaleOrderByCountry_SequenceAsc(Locale locale);
}
