package com.kiga.shop.web.response;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bbs
 * @since 10/16/16.
 */
@Data
public class CountryViewModel {
  private long id;
  private long countryGroupId;
  private BigDecimal vat;
  private String currency;
  private String code3;
  private String code;
  private String title;

  private BigDecimal priceShippingNet;
  private BigDecimal maxPurchasePrice;
  private BigDecimal vatRateShipping;
  private BigDecimal freeShippingLimit;
  private String iban;
  private String bic;
  private String bankName;
  private Integer daysToPayInvoice;
  private String accountNr;

  private Long maxDeliveryDays;
  private Long minDeliveryDays;
}
