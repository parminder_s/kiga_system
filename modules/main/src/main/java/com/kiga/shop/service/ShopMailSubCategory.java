package com.kiga.shop.service;

public class ShopMailSubCategory {
  public static String INVOICE = "invoice";
  public static String ORDER_CONFIRMATION = "orderConfirmation";
  public static String CREDIT_VOUCHER = "creditVoucher";
  public static String CANCELLED = "cancelled";
}
