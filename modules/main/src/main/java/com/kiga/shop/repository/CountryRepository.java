package com.kiga.shop.repository;

import com.kiga.shop.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * @author bbs
 * @since 10/16/16.
 */
public interface CountryRepository
    extends JpaRepository<Country, Long>, QueryDslPredicateExecutor<Country> {
  Country findByCode(String code);
}
