package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;

import java.time.Instant;
import javax.persistence.Entity;

/**
 * Created by rainerh on 22.04.16.
 */
@Entity
public class CrmOrderNote extends KigaEntityModel {
  private String crmId;
  private Instant invoiceDate;
  private String invoiceNr;

  public CrmOrderNote() {
    this.setClassName("CrmOrderNote");
  }

  public String getCrmId() {
    return crmId;
  }

  public void setCrmId(String crmId) {
    this.crmId = crmId;
  }

  public Instant getInvoiceDate() {
    return invoiceDate;
  }

  public void setInvoiceDate(Instant invoiceDate) {
    this.invoiceDate = invoiceDate;
  }

  public String getInvoiceNr() {
    return invoiceNr;
  }

  public void setInvoiceNr(String invoiceNr) {
    this.invoiceNr = invoiceNr;
  }
}
