package com.kiga.shop.basket.service;

import com.kiga.main.service.CookieService;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.service.ProductInfoCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;

/**
 * This classes iterates over all products of the existing basket and checks if all
 * products are deliverable from the customer's country.
 */

@Service
public class CountryDeliveryCheck {
  private ProductInfoCreator productInfoCreator;

  @Autowired
  public CountryDeliveryCheck(ProductInfoCreator productInfoCreator) {
    this.productInfoCreator = productInfoCreator;
  }

  /**
   * main entry point.
   */
  public boolean canDeliver(Optional<Purchase> optPurchase) {
    return optPurchase.map(
      purchase -> purchase.getPurchaseItems().stream()
        .map(PurchaseItem::getProduct)
        .map(product ->
          productInfoCreator.create(product.getId(), purchase.getCountry().getCode()))
        .allMatch(Optional::isPresent)
    ).orElse(true);
  }
}
