package com.kiga.shop.repository;

import com.kiga.shop.domain.CountryGroup;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author bbs
 * @since 10/16/16.
 */
public interface CountryGroupRepository extends JpaRepository<CountryGroup, Long> {
}
