package com.kiga.shop.backend.web.converter;

import com.kiga.shop.backend.web.response.ProductDetailViewModel;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.service.VatRateCalculator;
import com.kiga.web.converter.ResponseConverter;
import lombok.Builder;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 10/8/16.
 */
@Service
public class ProductDetailsToViewModelConverter
  implements ResponseConverter<ProductDetail, ProductDetailViewModel> {
  private VatRateCalculator vatRateCalculator;

  @Autowired
  public ProductDetailsToViewModelConverter(VatRateCalculator vatRateCalculator) {
    this.vatRateCalculator = vatRateCalculator;
  }

  @Override
  public ProductDetailViewModel convertToResponse(ProductDetail entity) {
    Country country = entity.getCountry();
    return ProductDetailViewModel.builder()
      .id(entity.getId()).productId(entity.getProduct().getId())
      .countryId(country.getId())
      .currencyCode(country.getCurrency())
      .country(country.getCode())
      .shippingDiscountType(entity.getShippingDiscountType())
      .vatProduct(entity.getVatProduct())
      .priceGross(
        vatRateCalculator.calculateGrossPrice(entity.getPriceNet(), entity.getVatProduct()))
      .activated(entity.getActivated())
      .build();
  }
}
