package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.security.domain.Member;
import com.kiga.shop.converter.PurchaseStatusEnumConverter;
import java.time.Instant;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/** Created by rainerh on 18.04.16. */
@Entity
public class PurchaseWorkflow extends KigaEntityModel {

  @ManyToOne
  @JoinColumn(name = "PurchaseID", referencedColumnName = "id")
  private Purchase purchase;

  @ManyToOne
  @JoinColumn(name = "MemberID", referencedColumnName = "id")
  private Member member;

  private Instant changeDate;

  @Convert(converter = PurchaseStatusEnumConverter.class)
  private PurchaseStatus prevStatus;

  @Convert(converter = PurchaseStatusEnumConverter.class)
  private PurchaseStatus status;

  public PurchaseWorkflow() {
    this.setClassName("PurchaseWorkflow");
  }

  public Purchase getPurchase() {
    return purchase;
  }

  public void setPurchase(Purchase purchase) {
    this.purchase = purchase;
  }

  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public Instant getChangeDate() {
    return changeDate;
  }

  public void setChangeDate(Instant changeDate) {
    this.changeDate = changeDate;
  }

  public PurchaseStatus getStatus() {
    return status;
  }

  public void setStatus(PurchaseStatus status) {
    this.status = status;
  }

  public PurchaseStatus getPrevStatus() {
    return prevStatus;
  }

  public void setPrevStatus(PurchaseStatus prevStatus) {
    this.prevStatus = prevStatus;
  }
}
