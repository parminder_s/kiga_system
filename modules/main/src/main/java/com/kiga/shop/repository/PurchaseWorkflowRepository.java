package com.kiga.shop.repository;

import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseWorkflow;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by rainerh on 29.04.16.
 */
public interface PurchaseWorkflowRepository extends CrudRepository<PurchaseWorkflow, Long> {
  List<PurchaseWorkflow> findByPurchaseId(Long purchaseId);

  @Query(
    "select p from PurchaseWorkflow p where p.purchase = :purchase AND p.status LIKE 'ordered'")
  List<PurchaseWorkflow> findOrderedPurchaseWorkflow(@Param("purchase") Purchase purchase);

  @Query(
    "select p from PurchaseWorkflow p where p.purchase = :purchase AND "
      + "p.status LIKE 'ordered' AND p.prevStatus LIKE 'ordered'")
  List<PurchaseWorkflow> findOldOrderedPurchaseWorkflow(@Param("purchase") Purchase purchase);

  @Query(
    "select p from PurchaseWorkflow p where p.purchase = :purchase AND p.status LIKE 'shipped'")
  PurchaseWorkflow findInvoicePurchaseWorkflow(@Param("purchase") Purchase purchase);

  List<PurchaseWorkflow> findAllByPurchase(Purchase purchase);

}
