package com.kiga.shop.repository;

import com.kiga.shop.domain.PurchaseDelivery;
import org.springframework.data.repository.CrudRepository;

public interface PurchaseDeliveryRepository extends CrudRepository<PurchaseDelivery, Long> {}
