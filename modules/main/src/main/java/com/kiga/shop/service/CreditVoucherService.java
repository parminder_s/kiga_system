package com.kiga.shop.service;

import static com.kiga.shop.service.ShopMailSubCategory.CREDIT_VOUCHER;
import static com.kiga.shop.service.ShopMailer.SHOP_MAIL_CATEGORY;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.mail.Mailer;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.shop.EmailTemplates;
import com.kiga.shop.document.PdfCreditVoucher;
import com.kiga.shop.document.model.PrintCreditVoucherData;
import com.kiga.shop.domain.CreditVoucherItem;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.numbers.NumberGenerator;
import com.kiga.shop.repository.CreditVoucherItemRepository;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.shop.web.response.PurchaseDetailResponse;
import java.io.File;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.Calendar;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.core.io.ClassPathResource;

// import javax.mail.internet.MimeMessage;

/** Created by peter on 25.05.16. */
@Builder
@AllArgsConstructor
public class CreditVoucherService {

  Mailer mailer;
  PurchaseRepository purchaseRepository;
  NumberGenerator generateShopNumbers;
  ShopUrlGenerator shopUrlGenerator;
  String shopEmail;
  String buckerName;
  ProductHelper productHelper;
  CreditVoucherItemRepository creditVoucherItemRepository;
  ShopS3Uploader shopS3Uploader;
  InvoiceRepository invoiceRepository;

  public void emailCreditVoucher(Purchase purchase) {
    String creditVoucherUrl = shopUrlGenerator.generateCreditVoucherUrl(purchase);
    String body =
        EmailTemplates.creditVoucherEmail(
            Salutation.getFromGender(purchase.getGender()),
            purchase.getLastname(),
            creditVoucherUrl);
    String to = purchase.getEmail();
    mailer.send(
        SHOP_MAIL_CATEGORY,
        CREDIT_VOUCHER,
        purchase.getId().toString(),
        email ->
            email
                .withTo(to)
                .withFrom(shopEmail)
                .withSubject("Ihre KiGaPortal Gutschrift")
                .withBody(body)
                .withInline("idkigalogo", new ClassPathResource("/print/kigalogocolor.png")));
  }

  public File createCreditVoucher(PurchaseDetailResponse purchaseDto) {
    Purchase purchase = purchaseRepository.findById(purchaseDto.getId());

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(purchase.getCreated());

    String creditVoucherNumber = getNewCreditVoucherNumber();
    Instant creditVoucherDate = Instant.now();
    purchase.setCreditVoucherNr(creditVoucherNumber);
    purchase.setCreditVoucherDate(creditVoucherDate);

    purchaseDto
        .getPurchaseItems()
        .forEach(
            (purchaseItemResponse) ->
                purchaseItemResponse.creditVoucherTotal().setScale(2, RoundingMode.HALF_UP));

    List<CreditVoucherItem> productDetails =
        productHelper.getProductDetailsCreditVoucher(purchaseDto.getPurchaseItems(), purchase);

    productDetails.forEach(
        (detail) -> {
          creditVoucherItemRepository.save(detail);
        });

    PdfCreditVoucher.CreditVoucher(
        new PrintCreditVoucherData(purchase, productDetails, creditVoucherNumber));
    String country = purchase.getCountry().getCode().toUpperCase();
    Integer year = calendar.get(Calendar.YEAR);
    File file = new File("/tmp/" + creditVoucherNumber + ".pdf");
    String creditVoucherUrl = "voucher/" + year + "/" + country + "/" + "/" + file.getName();

    purchase.setCreditVoucherS3Url(creditVoucherUrl);
    purchase.setCreditVoucherS3Bucket(buckerName);

    purchaseRepository.save(purchase);

    shopS3Uploader.uploadToAmazon(file, creditVoucherUrl);

    return file;
  }

  /** @param purchase to settle invoice on (if existing). */
  public void finalizeCreditVoucher(Purchase purchase) {
    if (purchase.getPaymentMethod().equals(PaymentMethod.INVOICE)) {
      Invoice invoice = invoiceRepository.findByInvoiceNumber(purchase.getInvoiceNr());
      invoice.setSettled(true);
      invoiceRepository.save(invoice);
    }
  }

  /** @return a new creditVoucherNumber. */
  public String getNewCreditVoucherNumber() {
    Purchase purchase = purchaseRepository.findTopByOrderByCreditVoucherNrDesc();
    if (purchase.getCreditVoucherNr() == null) {
      return generateShopNumbers.generateCreditVoucherNr("");
    } else {
      return generateShopNumbers.generateCreditVoucherNr(purchase.getCreditVoucherNr());
    }
  }
}
