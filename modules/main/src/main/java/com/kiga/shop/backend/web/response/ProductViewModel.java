package com.kiga.shop.backend.web.response;

import lombok.Data;


import java.math.BigDecimal;

/**
 * Please not that we are mixing here primitive data types with classes. This is due to
 * the planned extension to inheritance. So basically ProductView's properties could
 * overwrite default values from ProductGroup or Country.
 */
@Data
public final class ProductViewModel {
  private Long id;
  private String productTitle;
  private String code;
  private boolean activated;
  private BigDecimal volumeLitre;
  private Integer weightGram;
  private String createdOn;
  private String lastEditedOn;
  private BigDecimal oldPriceGross;
}
