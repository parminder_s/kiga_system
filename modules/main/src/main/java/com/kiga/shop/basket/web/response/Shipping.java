package com.kiga.shop.basket.web.response;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class Shipping {
  private BigDecimal cost;
  private String to;
}
