package com.kiga.shop.converter;

import static com.kiga.shop.domain.PurchaseStatus.Cancelled;
import static com.kiga.shop.domain.PurchaseStatus.Checkout;
import static com.kiga.shop.domain.PurchaseStatus.CreditVoucherFinalized;
import static com.kiga.shop.domain.PurchaseStatus.CreditVoucherPending;
import static com.kiga.shop.domain.PurchaseStatus.CsvExported;
import static com.kiga.shop.domain.PurchaseStatus.CsvFinished;
import static com.kiga.shop.domain.PurchaseStatus.FailedCheckout;
import static com.kiga.shop.domain.PurchaseStatus.InPayment;
import static com.kiga.shop.domain.PurchaseStatus.New;
import static com.kiga.shop.domain.PurchaseStatus.Ordered;
import static com.kiga.shop.domain.PurchaseStatus.OrderedPending;
import static com.kiga.shop.domain.PurchaseStatus.ReadyForShipping;
import static com.kiga.shop.domain.PurchaseStatus.Resumed;
import static com.kiga.shop.domain.PurchaseStatus.Shipped;
import static com.kiga.shop.domain.PurchaseStatus.ShippingInformationCreated;

import com.kiga.shop.domain.PurchaseStatus;
import com.kiga.shop.spec.WorkflowException;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/** Created by faxxe on 1/11/17. */
@Converter
public class PurchaseStatusEnumConverter implements AttributeConverter<PurchaseStatus, String> {

  @Override
  public String convertToDatabaseColumn(PurchaseStatus attribute) {
    if (attribute == null) {
      return null;
    }
    switch (attribute) {
      case Cancelled:
        return "cancelled";
      case Checkout:
        return "checkout";
      case CreditVoucherFinalized:
        return "creditVoucherFinalized";
      case CreditVoucherPending:
        return "creditVoucherPending";
      case CsvExported:
        return "csvExported";
      case CsvFinished:
        return "csvFinished";
      case FailedCheckout:
        return "failedCheckout";
      case InPayment:
        return "inPayment";
      case Ordered:
        return "ordered";
      case OrderedPending:
        return "orderedPending";
      case ShippingInformationCreated:
        return "shippingInformationCreated";
      case ReadyForShipping:
        return "readyForShipping";
      case Resumed:
        return "resumed";
      case Shipped:
        return "shipped";
      case New:
        return "new";
      default:
        throw new WorkflowException("string: " + attribute);
    }
  }

  @Override
  public PurchaseStatus convertToEntityAttribute(String dbData) {
    if (dbData == null) {
      return null;
    }
    switch (dbData.toLowerCase()) {
      case "cancelled":
        return Cancelled;
      case "checkout":
        return Checkout;
      case "creditvoucherfinalized":
        return CreditVoucherFinalized;
      case "creditvoucherpending":
        return CreditVoucherPending;
      case "csv_export":
      case "csvexported":
        return CsvExported;
      case "csvfinished":
        return CsvFinished;
      case "failedcheckout":
        return FailedCheckout;
      case "inpayment":
        return InPayment;
      case "new":
        return New;
      case "ordered":
        return Ordered;
      case "orderedpending":
        return OrderedPending;
      case "order_printed":
      case "shippingnote_created":
      case "shippinginformationcreated":
        return ShippingInformationCreated;
      case "ready_to_ship":
      case "readyforshipping":
        return ReadyForShipping;
      case "resumed":
        return Resumed;
      case "shipped":
        return Shipped;
      default:
        throw new WorkflowException("string: " + dbData);
    }
  }
}
