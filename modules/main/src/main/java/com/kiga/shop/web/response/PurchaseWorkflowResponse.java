package com.kiga.shop.web.response;

import com.kiga.shop.domain.PurchaseStatus;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseWorkflowResponse {
  private String name;
  private Date changeDate;
  private PurchaseStatus prevStatus;
  private PurchaseStatus status;
}
