package com.kiga.shop.repository;

import com.kiga.shop.domain.ProductDetail;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by rainerh on 29.04.16.
 */
public interface ProductDetailRepository extends PagingAndSortingRepository<ProductDetail, Long>,
  QueryDslPredicateExecutor<ProductDetail> {
  ProductDetail findProductDetailByProductIdAndCountryId(Long productId, Long countryId);

  List<ProductDetail> findByProductId(Long productId, Pageable pageable);
}
