package com.kiga.shop.backend.service.delivery;

import com.kiga.shop.domain.PurchaseDelivery;
import com.kiga.shop.domain.PurchaseDeliveryPurchaseProduct;
import com.kiga.spec.Persister;
import com.kiga.spec.Remover;

public class DeliveryPersister {
  private final Persister<PurchaseDelivery> deliveryPersister;
  private final Persister<PurchaseDeliveryPurchaseProduct> productPersister;
  private final Remover<PurchaseDeliveryPurchaseProduct> remover;

  /** inject beans. */
  public DeliveryPersister(
      Persister<PurchaseDelivery> deliveryPersister,
      Persister<PurchaseDeliveryPurchaseProduct> productPersister,
      Remover<PurchaseDeliveryPurchaseProduct> remover) {
    this.deliveryPersister = deliveryPersister;
    this.productPersister = productPersister;
    this.remover = remover;
  }

  /** persist newDelivery and remove potential empty products from the existingDelivery. */
  public void persist(PurchaseDelivery existingDelivery, PurchaseDelivery newDelivery) {
    existingDelivery
        .getPurchaseProducts()
        .stream()
        .filter(pd -> pd.getAmount() == 0)
        .forEach(remover::remove);

    deliveryPersister.persist(newDelivery);
    newDelivery.getPurchaseProducts().stream().forEach(productPersister::persist);
  }
}
