package com.kiga.shop.web.response;

import lombok.Data;

/**
 * @author bbs
 * @since 10/16/16.
 */
@Data
public class CountryDropdownViewModel {
  private String title;
  private String code;
  private String currency;
}
