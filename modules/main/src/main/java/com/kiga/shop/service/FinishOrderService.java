package com.kiga.shop.service;

import static com.kiga.shop.service.ShopMailSubCategory.ORDER_CONFIRMATION;
import static com.kiga.shop.service.ShopMailer.SHOP_MAIL_CATEGORY;

import com.kiga.mail.Mailer;
import com.kiga.shop.EmailTemplates;
import com.kiga.shop.ShopProperties;
import com.kiga.shop.backend.service.delivery.DeliveryCreator;
import com.kiga.shop.document.model.ConfirmationEmailData;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseStatus;
import com.kiga.shop.domain.ShippingPartner;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.shop.workflow.PurchaseWorkflowHistoryService;
import com.kiga.web.service.UrlGenerator;
import java.time.Instant;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

@Service
public class FinishOrderService {
  private PurchaseWorkflowHistoryService purchaseWorkflowHistoryService;
  private PurchaseRepository purchaseRepository;
  private ProductHelper productHelper;
  private UrlGenerator urlGenerator;
  private Mailer mailer;
  private ShopProperties shopProperties;
  private DeliveryCreator deliveryCreator;

  @Autowired
  public FinishOrderService(
      PurchaseRepository purchaseRepository,
      ProductHelper productHelper,
      UrlGenerator urlGenerator,
      Mailer mailer,
      PurchaseWorkflowHistoryService purchaseWorkflowHistoryService,
      ShopProperties shopProperties,
      DeliveryCreator deliveryCreator) {
    this.purchaseRepository = purchaseRepository;
    this.productHelper = productHelper;
    this.urlGenerator = urlGenerator;
    this.mailer = mailer;
    this.purchaseWorkflowHistoryService = purchaseWorkflowHistoryService;
    this.shopProperties = shopProperties;
    this.deliveryCreator = deliveryCreator;
  }

  /**
   * Requires order to be in Status 'OrderedPending'. Generates a OrderNumber and sends
   * ConfirmationEmail.
   *
   * @param purchaseId Id of purchase to be finished.
   */
  public void finishPurchase(long purchaseId) {
    Purchase purchaseToFinish = purchaseRepository.findById(purchaseId);
    finish(purchaseToFinish);
  }

  public void finishPurchaseFromSuspendedPayment(long purchaseId, String pspId) {
    Purchase purchase = purchaseRepository.findById(purchaseId);
    purchaseWorkflowHistoryService.proceedToState(
        Collections.singletonList(purchase), PurchaseStatus.OrderedPending, null);
    purchase.setPaymentPspId(pspId);
    purchaseRepository.save(purchase);
    finish(purchase);
  }

  private void finish(Purchase purchaseToFinish) {
    purchaseToFinish.setOrderDate(Instant.now());
    updatePurchaseWorkflow(purchaseToFinish);
    updateShippingPartner(purchaseToFinish);
    setOrderNr(purchaseToFinish);
    deliveryCreator.create(purchaseToFinish);
    sendOrderConfirmation(purchaseToFinish);
  }

  private void updatePurchaseWorkflow(Purchase purchase) {
    purchaseWorkflowHistoryService.proceedToState(
        Collections.singletonList(purchase), PurchaseStatus.Ordered, null);
  }

  private void updateShippingPartner(Purchase purchase) {
    if (purchase.getPurchaseItems().size() == 1
        && purchase.getPurchaseItems().get(0).getCode().startsWith("mlap")
        && purchase.getPurchaseItems().get(0).getCode().split("-").length == 2
        && purchase.getPurchaseItems().get(0).getAmount() <= 2) {
      purchase.setShippingPartner(ShippingPartner.POST);
    } else {
      purchase.setShippingPartner(ShippingPartner.DHL);
    }
  }

  private void setOrderNr(Purchase purchase) {
    Long orderNr = (Long) purchaseRepository.findMaxOrderNr();
    if (orderNr == null) {
      orderNr = 0L;
    } else {
      orderNr += 1L;
    }
    purchase.setOrderNr(orderNr);
    purchaseRepository.save(purchase);
  }

  private void sendOrderConfirmation(Purchase purchase) {
    ConfirmationEmailData confirmationEmailData =
        new ConfirmationEmailData(purchase, productHelper.getProductDetailsFromPurchase(purchase));
    boolean gender =
        confirmationEmailData.gender().compareTo("male") == 0
            | confirmationEmailData.gender().compareTo("m") == 0;
    String salutation = gender ? "Herr" : "Frau";
    String body =
        EmailTemplates.confirmationEmail(
            salutation,
            confirmationEmailData,
            urlGenerator.getUrl("/de/footerpages/widerrufsrecht/"));

    this.mailer.send(
        SHOP_MAIL_CATEGORY,
        ORDER_CONFIRMATION,
        purchase.getId().toString(),
        email ->
            email
                .withFrom(shopProperties.getShopEmail())
                .withTo(confirmationEmailData.email())
                .withReplyTo("kundenservice@kigaportal.com")
                .withSubject("Bestätigung Ihrer Bestellung bei KiGaPortal")
                .withBody(body)
                .withInline("idkigalogo", new ClassPathResource("/print/kigalogocolor.png")));
  }
}
