package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by rainerh on 22.04.16.
 */
@Entity
public class ShippingDiscount extends KigaEntityModel {
  public ShippingDiscount() {
    this.setClassName("ShippingDiscount");
  }

  private Integer threshold;
  private BigDecimal discountValue;

  @ManyToOne
  @JoinColumn(name = "productDetailID", referencedColumnName = "id")
  private ProductDetail productDetail;

  public Integer getThreshold() {
    return threshold;
  }

  public void setThreshold(Integer threshold) {
    this.threshold = threshold;
  }

  public BigDecimal getDiscountValue() {
    return discountValue;
  }

  public void setDiscountValue(BigDecimal discountValue) {
    this.discountValue = discountValue;
  }

  public ProductDetail getProductDetail() {
    return productDetail;
  }

  public void setProductDetail(ProductDetail productDetail) {
    this.productDetail = productDetail;
  }
}
