package com.kiga.shop.numbers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import scala.Option;

/**
 * Creates strings used as "numbers" for invoice, vouchers etc. according to certain rules regarding
 * how to draw the next "numbers" from the sequence. Created by mfit on 14.11.17.
 */
public class NumberGenerator {
  Pattern invoicePattern = Pattern.compile("([a-zA-Z]{2})(\\d)(\\d*)");
  Pattern voucherPattern = Pattern.compile("([a-zA-Z]{1})(\\d*)");

  /**
   * Determines the succeeding invoice number code, starting from the current number, a country code
   * and a year.
   */
  public String generateInvoiceNr(String lastInvoice, String country, Integer year) {
    if (lastInvoice.endsWith("999999")) {
      // Text for exception , would like to use that:
      // "InvoiceNr Overflow for " + lastInvoice + "," + country + "," + year.toString()
      throw new InvoiceNrOverflowException();
    }

    // the invoice number year will always be the given year or the invoiceNr is empty
    // because of the search query one layer above
    if (lastInvoice != "") {
      Matcher matcher = invoicePattern.matcher(lastInvoice);
      matcher.matches(); // TODO: might test here for true / false
      Integer currentNr = Integer.parseInt(matcher.group(3));

      return country + Integer.toString(year % 10) + String.format("%06d", currentNr + 1);
    }

    return country + Integer.toString(year % 10) + String.format("%06d", 1);
  }

  /**
   * Determines next shipping number.
   *
   * @param lastShipping current shipping number
   * @return the next shipping number.
   */
  public String generateShippingNr(Option<String> lastShipping) {
    if (lastShipping.isEmpty() || lastShipping.get().equals("")) {
      return String.format("%06d", 1);
    }
    Integer currentNr = Integer.parseInt(lastShipping.get());
    if (currentNr >= 999999) {
      throw new ShippingNoteOverflowException();
    } else {
      return String.format("%06d", currentNr + 1);
    }
  }

  /**
   * Determines next credit voucher number (the successor to a credit voucher number).
   *
   * @param lastCreditVoucherNr the current voucher number.
   * @return the successor (next number).
   */
  public String generateCreditVoucherNr(String lastCreditVoucherNr) {
    if (lastCreditVoucherNr.endsWith("9999999")) {
      throw new CreditVoucherOverflowException();
    } else if (lastCreditVoucherNr != "") {
      Matcher matcher = voucherPattern.matcher(lastCreditVoucherNr);
      matcher.matches(); // TODO: might test here for true / false
      Integer currentNr = Integer.parseInt(matcher.group(2));
      return "G" + String.format("%07d", currentNr + 1);
    } else {
      return "G" + String.format("%07d", 1);
    }
  }
}
