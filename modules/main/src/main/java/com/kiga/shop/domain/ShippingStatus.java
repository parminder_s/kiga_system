package com.kiga.shop.domain;

public enum ShippingStatus {
  NONE,
  ShippingInformationCreated,
  CsvExported,
  CsvFinished,
  ReadyForShipping,
  Shipped,
}
