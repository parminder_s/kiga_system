package com.kiga.shop;

import com.kiga.ideas.property.PreviewImage;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * Created by rainerh on 02.04.16.
 */
@Configuration
@ConfigurationProperties("shop")
@Data
public class ShopProperties {
  private String shopEmail;
  private String bucketName;
  private String successKey;
  private String secretKey;
  private Integer amountPerCategory;
  private PreviewImage previewImage;
}
