package com.kiga.shop.backend.web;

import com.kiga.main.exception.InvalidIdException;
import com.kiga.main.util.NumberUtil;
import com.kiga.security.services.SecurityService;
import com.kiga.shop.backend.web.request.GetCountryRequest;
import com.kiga.shop.backend.web.request.UpdateCountryRequest;
import com.kiga.shop.service.CountryService;
import com.kiga.shop.web.response.CountryViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author bbs
 * @since 3/14/17.
 */
@RestController
@RequestMapping("shop/backend/countries")
public class CountryController {
  private CountryService countryService;
  private SecurityService securityService;

  @Autowired
  public CountryController(CountryService countryService, SecurityService securityService) {
    this.countryService = countryService;
    this.securityService = securityService;
  }

  private void convertVatRate(CountryViewModel countryViewModel) {
    BigDecimal vatRate = countryViewModel.getVat();
    if (vatRate != null) {
      countryViewModel.setVat(vatRate.multiply(BigDecimal.valueOf(100)));
    }
  }

  /**
   * Get list of countries.
   *
   * @return list of countries
   */
  @RequestMapping
  public List<CountryViewModel> list() {
    securityService.requireShopAdmin();
    List<CountryViewModel> countryViewModels = countryService.getAll();
    countryViewModels.stream().forEach(this::convertVatRate);
    return countryViewModels;
  }

  /**
   * Get list of important countries.
   *
   * @return list of countries
   */
  @RequestMapping("important")
  public List<CountryViewModel> listImportant() {
    securityService.requireShopAdmin();
    List<CountryViewModel> countryViewModels = countryService.getAllImportant();
    countryViewModels.stream().forEach(this::convertVatRate);
    return countryViewModels;
  }

  /**
   * Get list of other (not importnat) countries.
   *
   * @return list of countries
   */
  @RequestMapping("other")
  public List<CountryViewModel> listOther() {
    securityService.requireShopAdmin();
    List<CountryViewModel> countryViewModels = countryService.getAllOther();
    countryViewModels.stream().forEach(this::convertVatRate);
    return countryViewModels;
  }

  /**
   * Get single country.
   *
   * @param getCountryRequest request
   * @return single country
   * @throws InvalidIdException if id is invalid
   */
  @RequestMapping("get")
  public CountryViewModel get(@RequestBody GetCountryRequest getCountryRequest)
    throws InvalidIdException {
    securityService.requireShopAdmin();
    long id = getCountryRequest.getId();
    if (NumberUtil.isNotValidId(id)) {
      throw new InvalidIdException();
    }
    return countryService.getById(id);
  }

  /**
   * Update country.
   *
   * @param updateCountryRequest request
   * @throws InvalidIdException if id is invalid
   */
  @RequestMapping("update")
  public void update(@RequestBody UpdateCountryRequest updateCountryRequest)
    throws InvalidIdException {
    securityService.requireShopAdmin();
    if (NumberUtil.isNotValidId(updateCountryRequest.getId())) {
      throw new InvalidIdException();
    }
    countryService.update(updateCountryRequest);
  }
}
