package com.kiga.shop;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.springframework.stereotype.Service;


import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by faxxe on 1/3/17.
 */

@Service
public class DocumentHelper {

  private PDFMergerUtility pdfMergerUtility;


  /**
   * Helper for merging documents.
   */
  public void mergeFiles(List<File> fileList, String targetFileName) {

    pdfMergerUtility = new PDFMergerUtility();
    pdfMergerUtility.setDestinationFileName(targetFileName);

    for (File tmp: fileList) {
      pdfMergerUtility.addSource(tmp);
    }

    try {
      pdfMergerUtility.mergeDocuments();
    } catch (IOException | COSVisitorException exception) {
      exception.printStackTrace();
    }

  }

}
