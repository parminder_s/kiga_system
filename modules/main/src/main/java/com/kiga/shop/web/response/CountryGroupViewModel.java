package com.kiga.shop.web.response;

import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * @author bbs
 * @since 10/16/16.
 */
@Data
@ToString
public class CountryGroupViewModel {
  private Long id;
  private String title;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public boolean equals(Object comparedObject) {
    if (this == comparedObject) {
      return true;
    }

    if (comparedObject == null || getClass() != comparedObject.getClass()) {
      return false;
    }

    CountryGroupViewModel that = (CountryGroupViewModel) comparedObject;

    return new EqualsBuilder()
      .append(id, that.id)
      .append(title, that.title)
      .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
      .append(id)
      .append(title)
      .toHashCode();
  }
}
