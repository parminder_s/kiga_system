package com.kiga.shop.backend.service.delivery;

import static com.kiga.shop.domain.ShippingStatus.ShippingInformationCreated;

import com.kiga.shop.domain.PurchaseDelivery;
import com.kiga.shop.domain.PurchaseDeliveryPurchaseProduct;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class DeliverySplitterExecutor {
  /** executes the actually splitting of two deliveries. */
  public void split(
      PurchaseDelivery existingDelivery,
      PurchaseDelivery newDelivery,
      List<PurchaseProductIdAmount> purchaseProductIdAmounts) {
    newDelivery.setStatus(ShippingInformationCreated);
    newDelivery.setPurchaseProducts(
        purchaseProductIdAmounts
            .stream()
            .map(
                pa -> {
                  PurchaseDeliveryPurchaseProduct deliveryProduct =
                      existingDelivery
                          .getPurchaseProducts()
                          .stream()
                          .filter(pp -> pp.getPurchaseItem().getId() == pa.getPurchaseProductId())
                          .findFirst()
                          .get();
                  deliveryProduct.setAmount(deliveryProduct.getAmount() - pa.getAmount());
                  return new PurchaseDeliveryPurchaseProduct(
                      pa.getAmount(),
                      deliveryProduct.getPurchaseDelivery(),
                      deliveryProduct.getPurchaseItem());
                })
            .collect(Collectors.toList()));
  }
}
