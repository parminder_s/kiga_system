package com.kiga.shop.web.response;

import static java.util.stream.Collectors.toList;

import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.InvoiceVersion;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseWorkflow;
import com.kiga.shop.domain.ShippingPartner;
import com.kiga.shop.web.responses.PurchaseItemResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseDetailResponse {
  private Long id;
  private String firstName;
  private String lastName;
  private String addressName1;
  private String addressName2;
  private String priceNetTotal;
  private String priceGrossTotal;
  private String currency;
  private String status;
  private Integer amount;
  private String shippingNoteNumber;
  private String invoiceNumber;
  private Instant invoiceDate;
  private String memberId;
  private String memberEmail;
  private String orderNr;
  private ShippingPartner shippingPartner;

  private String street;
  private String city;
  private String zip;
  private String streetNumber;
  private String doorNumber;
  private String floorNumber;
  private String country;
  private String countryCode;
  private String phoneNr;

  private String billCity;
  private String billZip;
  private String billCountry;
  private String billAddresssName1;
  private String billAddresssName2;
  private String billStreet;
  private String billStreetnumber;
  private String billDoornumber;
  private String billFloornumber;

  private List<PurchaseItemResponse> purchaseItems;
  private String orderDate;
  private PaymentMethod paymentMethod;
  private String invoiceUrl;
  private String voucherNr;
  private String voucherUrl;
  private String comment;

  private List<PurchaseWorkflowResponse> workflow;
  private List<InvoiceVersionResponse> invoices;

  // For Scala.
  public Long getId() {
    return id;
  }

  public List<PurchaseItemResponse> getPurchaseItems() {
    return purchaseItems;
  }

}
