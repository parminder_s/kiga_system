package com.kiga.shop.repository;

import com.kiga.shop.domain.PurchaseItem;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 29.04.16.
 */
public interface PurchaseItemRepository extends CrudRepository<PurchaseItem, Long> {
}
