package com.kiga.shop.basket.service.price;

import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.service.VatRateCalculator;
import java.math.BigDecimal;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DiscountSetter {
  private final VatRateCalculator vatRateCalculator;

  @Autowired
  public DiscountSetter(VatRateCalculator vatRateCalculator) {
    this.vatRateCalculator = vatRateCalculator;
  }

  /** passes all purchaseItems with existing non-applied vouchers to the applying function. */
  public void applyDiscounts(List<PurchaseItem> purchaseItems) {
    purchaseItems
        .stream()
        .filter(purchaseItem -> purchaseItem.getVoucher() != null)
        .forEach(this::applyDiscount);
  }

  private void applyDiscount(PurchaseItem purchaseItem) {
    BigDecimal amount = new BigDecimal(purchaseItem.getAmount());
    BigDecimal discountRate;
    if (purchaseItem.getAppliedDiscountRate().compareTo(BigDecimal.ZERO) == 0) {
      discountRate = purchaseItem.getVoucher().getDiscountRate();
    } else {
      discountRate = purchaseItem.getAppliedDiscountRate();
    }
    BigDecimal priceGrossSingle =
        purchaseItem.getPriceGrossTotal().divide(amount, 2, BigDecimal.ROUND_HALF_UP);
    BigDecimal discountedPriceGrossSingle =
        priceGrossSingle.divide(BigDecimal.ONE.add(discountRate), 2, BigDecimal.ROUND_HALF_UP);
    BigDecimal discountGrossSingle = priceGrossSingle.subtract(discountedPriceGrossSingle);
    BigDecimal vatSingle =
        discountedPriceGrossSingle
            .subtract(
                vatRateCalculator.calculateNetPrice(
                    discountedPriceGrossSingle, purchaseItem.getVatRate()))
            .setScale(2, BigDecimal.ROUND_HALF_UP);

    purchaseItem.setAppliedDiscountRate(discountRate);
    purchaseItem.setPriceGrossTotal(discountedPriceGrossSingle.multiply(amount));
    purchaseItem.setVatTotal(vatSingle.multiply(amount));
    purchaseItem.setPriceNetSingle(discountedPriceGrossSingle.subtract(vatSingle).setScale(3));
    purchaseItem.setAppliedDiscountGrossTotal(discountGrossSingle.multiply(amount));
  }
}
