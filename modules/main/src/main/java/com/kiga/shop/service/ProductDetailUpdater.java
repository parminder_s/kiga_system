package com.kiga.shop.service;

import com.kiga.shop.backend.web.response.ProductDetailViewModel;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.repository.CountryRepository;
import com.kiga.shop.repository.ProductDetailRepository;
import com.kiga.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ProductDetailUpdater {
  private ProductRepository productRepository;
  private CountryRepository countryRepository;
  private VatRateCalculator vatRateCalculator;
  private ProductDetailRepository productDetailRepository;


  /**
   * inject dependencies.
   */
  @Autowired
  public ProductDetailUpdater(
    ProductRepository productRepository, CountryRepository countryRepository,
    VatRateCalculator vatRateCalculator, ProductDetailRepository productDetailRepository) {
    this.productRepository = productRepository;
    this.countryRepository = countryRepository;
    this.vatRateCalculator = vatRateCalculator;
    this.productDetailRepository = productDetailRepository;
  }

  /**
   * update or add a new ProductDetail.
   */
  public void update(ProductDetailViewModel productDetailViewModel, ProductDetail productDetail) {
    productDetail.setProduct(
      productRepository.findByActivatedTrueAndId(productDetailViewModel.getProductId()));
    productDetail.setCountry(
      countryRepository.findOne(productDetailViewModel.getCountryId()));
    productDetail.setActivated(productDetailViewModel.isActivated());
    productDetail.setPriceNet(this.vatRateCalculator.calculateNetPrice(
      productDetailViewModel.getPriceGross(), productDetailViewModel.getVatProduct()));
    productDetail.setVatProduct(productDetailViewModel.getVatProduct());
    productDetail.setShippingDiscountType(productDetailViewModel.getShippingDiscountType());
    productDetail.setShippingCosts(BigDecimal.ZERO);

    productDetailRepository.save(productDetail);
  }
}
