package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.db.LocaleToLongNotationConverter;
import com.kiga.main.locale.Locale;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;

/** Created by rainerh on 19.10.16. */
@Entity
@Table(name = "Country_Locale")
@AllArgsConstructor
@Builder
public class CountryLocale extends KigaEntityModel {
  @Convert(converter = LocaleToLongNotationConverter.class)
  private Locale locale;

  private String title;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "countryID", referencedColumnName = "id")
  private Country country;

  public CountryLocale() {
    this.setClassName("Country_Locale");
  }

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  public Country getCountry() {
    return country;
  }

  public void setCountry(Country country) {
    this.country = country;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
