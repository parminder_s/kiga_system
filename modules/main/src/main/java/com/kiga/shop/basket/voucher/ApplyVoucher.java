package com.kiga.shop.basket.voucher;

import com.kiga.shop.basket.service.PricesSetter;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.repository.VoucherRepositoryExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import javax.transaction.Transactional;

@Service
public class ApplyVoucher {
  private PricesSetter pricesSetter;
  private VoucherRepositoryExt voucherRepositoryExt;

  @Autowired
  public ApplyVoucher(
    PricesSetter pricesSetter, VoucherRepositoryExt voucherRepositoryExt) {
    this.pricesSetter = pricesSetter;
    this.voucherRepositoryExt = voucherRepositoryExt;
  }

  /**
   * we find if the product even exists in the current basket and if so, we set the
   * voucher and voucher code. Everything is done by the priceSetter.
   */
  @Transactional
  public boolean apply(String code, Purchase purchase) {
    return voucherRepositoryExt.findValidVoucher(code, Instant.now())
      .map(voucher ->
        purchase.getPurchaseItems().stream()
          .filter(purchaseItem -> purchaseItem.getVoucher() == null)
          .filter(purchaseItem -> purchaseItem.getProduct().equals(voucher.getProduct()))
          .findFirst()
          .map(purchaseItem -> {
            purchaseItem.setVoucher(voucher);
            purchaseItem.setVoucherCode(code);
            pricesSetter.setPrices(purchase);
            return true;
          }).orElse(false)
      ).orElse(false);
  }
}
