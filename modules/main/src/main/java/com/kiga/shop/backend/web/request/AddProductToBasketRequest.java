package com.kiga.shop.backend.web.request;

import lombok.Data;

/**
 * Created by asv on 17.11.16.
 */
@Data
public class AddProductToBasketRequest {
  private int amount;
  private long productId;
}
