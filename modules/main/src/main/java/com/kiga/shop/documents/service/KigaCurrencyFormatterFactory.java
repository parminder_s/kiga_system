package com.kiga.shop.documents.service;

import freemarker.core.Environment;
import freemarker.core.TemplateNumberFormat;
import freemarker.core.TemplateNumberFormatFactory;
import freemarker.core.TemplateValueFormatException;

import java.util.Locale;

/**
 * Created by peter on 3/1/17.
 */
public class KigaCurrencyFormatterFactory extends TemplateNumberFormatFactory {
  @Override
  public TemplateNumberFormat get(String params, Locale locale, Environment env)
    throws TemplateValueFormatException {
    return new KigaCurrencyFormatter(params);
  }
}
