package com.kiga.shop.backend.purchase;

import static com.kiga.shop.service.ShopMailSubCategory.CANCELLED;
import static com.kiga.shop.service.ShopMailer.SHOP_MAIL_CATEGORY;
import static java.util.Optional.ofNullable;

import com.kiga.mail.Mailer;
import com.kiga.shop.EmailTemplates;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.service.Salutation;
import org.springframework.core.io.ClassPathResource;

public class PurchaseCanceller {
  private final String shopEmail;
  private Mailer mailer;

  public PurchaseCanceller(Mailer mailer, String shopEmail) {
    this.mailer = mailer;
    this.shopEmail = shopEmail;
  }

  public void cancel(Purchase purchase) {
    String body =
        EmailTemplates.cancelEMail(
            Salutation.getFromGender(purchase.getGender()),
            purchase.getLastname(),
            purchase.getOrderNr().toString(),
            ofNullable(purchase.getInvoiceNr()).orElse(""));
    mailer.send(
        SHOP_MAIL_CATEGORY,
        CANCELLED,
        purchase.getId().toString(),
        eMail ->
            eMail
                .withTo(purchase.getEmail())
                .withFrom(shopEmail)
                .withBody(body)
                .withSubject("Ihr KiGaPortal Storno")
                .withInline("idkigalogo", new ClassPathResource("/print/kigalogocolor.png")));
  }
}
