package com.kiga.shop.documents.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by peter on 14.02.17.
 */
@Data
@AllArgsConstructor
public class InvoiceVatRow {
  BigDecimal rate;
  BigDecimal total;
}
