package com.kiga.shop.basket.service.price;

import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.service.ProductInfoCreator;
import com.kiga.shop.web.response.ProductInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class PurchaseItemSetter {
  private ProductInfoCreator productInfoCreator;

  @Autowired
  public PurchaseItemSetter(ProductInfoCreator productInfoCreator) {
    this.productInfoCreator = productInfoCreator;
  }

  public void setPurchaseItems(Purchase purchase) {
    purchase.getPurchaseItems().stream()
      .forEach(purchaseItem -> setPurchaseItem(purchase, purchaseItem));
  }

  private void setPurchaseItem(Purchase purchase, PurchaseItem purchaseItem) {
    Optional<ProductInfo> optProductInfo = productInfoCreator
      .create(purchaseItem.getProduct().getId(), purchase.getCountry().getCode());

    if (!optProductInfo.isPresent()) {
      throw new UnavailableCountryException();
    }

    ProductInfo productInfo = optProductInfo.get();
    BigDecimal amount = new BigDecimal(purchaseItem.getAmount());

    purchaseItem.setPriceNetSingle(productInfo.getPriceNet());
    purchaseItem.setVatRate(productInfo.getVatRate());
    purchaseItem.setVatTotal(productInfo.getVatProduct().multiply(amount));
    purchaseItem.setPriceGrossTotal(
      productInfo.getPriceGross().multiply(amount));

    purchaseItem.setVolumeLitreSingle(productInfo.getVolumeLitre());
    purchaseItem.setVolumeLitreTotal(productInfo.getVolumeLitre().multiply(amount));
    purchaseItem.setWeightGramSingle(productInfo.getWeightGram());
    purchaseItem.setWeightGramTotal(productInfo.getWeightGram() * purchaseItem.getAmount());
  }
}
