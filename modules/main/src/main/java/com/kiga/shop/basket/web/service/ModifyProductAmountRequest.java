package com.kiga.shop.basket.web.service;

import com.kiga.web.message.EndpointRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ModifyProductAmountRequest extends EndpointRequest {
  private long productId;
  private int amount;
}
