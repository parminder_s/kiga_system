package com.kiga.shop.web.response;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bbs
 * @since 2/18/17.
 */
@Data
public class CountryShopViewModel {
  private long id;
  private BigDecimal maxPurchasePrice;
  private BigDecimal priceShippingNet;
  private BigDecimal vatRateShipping;
  private BigDecimal freeShippingLimit;
  private String iban;
  private String bic;
  private String bankName;
  private Integer daysToPayInvoice;
  private String accountNr;
}
