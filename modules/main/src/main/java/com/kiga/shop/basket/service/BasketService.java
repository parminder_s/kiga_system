package com.kiga.shop.basket.service;

import com.kiga.main.locale.Locale;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.repository.PurchaseItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

/**
 * This class is responsible for all actions related to purchaseItems.
 * It has a central executeOnPurchase function, that accepts a consumer
 * doing the actual work and provides the Purchase and the PurchaseItem.
 */
@Service
public class BasketService {
  private BasketServiceExecutor basketServiceExecutor;
  private PurchaseItemRepository purchaseItemRepository;

  /**
   * inject beans.
   */
  @Autowired
  public BasketService(BasketServiceExecutor basketServiceExecutor,
                       PurchaseItemRepository purchaseItemRepository) {
    this.basketServiceExecutor = basketServiceExecutor;
    this.purchaseItemRepository = purchaseItemRepository;
  }

  /**
   * We add a given amount of a product to the basket.
   * A newly created PurchaseItem has always amount 0.
   */
  @Transactional
  public Purchase addProduct(
    Optional<Purchase> optPurchase, long productId, int amount,
    Locale locale) {
    return basketServiceExecutor.execute(optPurchase, productId, locale,
      (purchase, purchaseItem) -> purchaseItem.setAmount(purchaseItem.getAmount() + amount)
    );
  }

  /**
   * set a fixed amount for a product.
   */
  @Transactional
  public Purchase setAmount(
    Optional<Purchase> optPurchase, long productId, int amount, Locale locale) {
    return basketServiceExecutor.execute(optPurchase, productId, locale,
      (purchase, purchaseItem) -> purchaseItem.setAmount(amount)
    );
  }

  /**
   * removes a product.
   */
  @Transactional
  public Purchase removeProduct(
    Optional<Purchase> optPurchase, long productId, Locale locale) {
    return basketServiceExecutor
      .execute(optPurchase, productId, locale,
        (purchase, pi) -> {
          purchase.getPurchaseItems().stream()
            .filter(purchaseItem -> purchaseItem.getProduct().getId().equals(productId))
            .findFirst()
            .ifPresent(purchaseItem -> {
              purchaseItemRepository.delete(purchaseItem);
              purchase.getPurchaseItems().remove(purchaseItem);
            });
        });
  }
}
