package com.kiga.shop.backend.purchase.web;

import com.kiga.shop.backend.api.model.PurchaseResponse;
import com.kiga.shop.backend.purchase.PurchasesFinder;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FindPurchasesService {
  private final PurchasesFinder finder;
  private final PurchaseResponseConverter converter;

  @Autowired
  public FindPurchasesService(PurchasesFinder finder, PurchaseResponseConverter converter) {
    this.finder = finder;
    this.converter = converter;
  }

  public List<PurchaseResponse> findPurchases(boolean hideFinished) {
    return finder
        .find(hideFinished)
        .stream()
        .map(converter::convertToResponse)
        .collect(Collectors.toList());
  }
}
