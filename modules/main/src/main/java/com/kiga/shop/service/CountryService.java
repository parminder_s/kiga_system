package com.kiga.shop.service;

import com.kiga.main.locale.Locale;
import com.kiga.shop.backend.web.request.UpdateCountryRequest;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryLocale;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.domain.QCountry;
import com.kiga.shop.domain.QProductDetail;
import com.kiga.shop.repository.CountryLocaleRepository;
import com.kiga.shop.repository.CountryRepository;
import com.kiga.shop.web.converter.CountryToDropdownViewModelConverter;
import com.kiga.shop.web.converter.CountryToViewModelConverter;
import com.kiga.shop.web.response.CountryDropdownViewModel;
import com.kiga.shop.web.response.CountryViewModel;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.query.ListSubQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 10/16/16.
 */
@Service
public class CountryService {
  private CountryRepository countryRepository;
  private CountryToViewModelConverter countryToViewModelConverter;
  private CountryToDropdownViewModelConverter countryToDropdownViewModelConverter;
  private CountryLocaleRepository countryLocaleRepository;
  private Map<String, Map<String, String>> bootstrapCountryCache;

  /**
   * Dependencies.
   *
   * @param countryRepository                   countryRepository
   * @param countryToViewModelConverter         countryToViewModelConverter
   * @param countryToDropdownViewModelConverter countryToDropdownViewModelConverter
   */
  @Autowired
  public CountryService(CountryRepository countryRepository,
                        CountryToViewModelConverter countryToViewModelConverter,
                        CountryToDropdownViewModelConverter countryToDropdownViewModelConverter,
                        CountryLocaleRepository countryLocaleRepository) {
    this.countryRepository = countryRepository;
    this.countryToViewModelConverter = countryToViewModelConverter;
    this.countryToDropdownViewModelConverter = countryToDropdownViewModelConverter;
    this.countryLocaleRepository = countryLocaleRepository;
    bootstrapCountryCache = new HashMap<>();
  }

  private List<CountryViewModel> getAllForPredicate(Predicate predicate) {
    Sort sort = new Sort(Arrays.asList(
      new Sort.Order(Sort.Direction.DESC, "vat"),
      new Sort.Order(Sort.Direction.DESC, "code")
    ));

    List<Country> countries = new ArrayList<>();
    if (predicate == null) {
      countries = countryRepository.findAll(sort);
    } else {
      Iterable<Country> all = countryRepository.findAll(predicate, sort);
      Optional.ofNullable(all).orElseGet(ArrayList::new).forEach(countries::add);
    }

    List<CountryViewModel> returner = new ArrayList<>();
    if (countries != null) {
      returner = countries.stream().map(countryToViewModelConverter::convertToResponse)
        .collect(Collectors.toList());
    }

    return returner;
  }

  /**
   * Get list of all Countries.
   *
   * @return Country View Models List
   */
  public List<CountryViewModel> getAll() {
    return getAllForPredicate(null);
  }

  /**
   * Get list of all important Countries.
   *
   * @return Country View Models List
   */
  public List<CountryViewModel> getAllImportant() {
    QProductDetail productDetail = QProductDetail.productDetail;

    ListSubQuery<Long> countriesWithProductDetails
      = new JPASubQuery().distinct().from(productDetail).list(productDetail.country.id);

    QCountry country = QCountry.country;
    BooleanExpression allImportantCountriesExpr = country.id.in(countriesWithProductDetails);

    return getAllForPredicate(allImportantCountriesExpr);
  }

  /**
   * Get list of all other (not important) Countries.
   *
   * @return Country View Models List
   */
  public List<CountryViewModel> getAllOther() {
    QProductDetail productDetail = QProductDetail.productDetail;

    ListSubQuery<Long> countriesWithProductDetails
      = new JPASubQuery().distinct().from(productDetail).list(productDetail.country.id);

    QCountry country = QCountry.country;
    BooleanExpression allImportantCountriesExpr = country.id.notIn(countriesWithProductDetails);

    return getAllForPredicate(allImportantCountriesExpr);
  }

  /**
   * Get list of all Countries.
   *
   * @return Country View Models List
   */
  public List<CountryDropdownViewModel> getAllForDropdown() {
    List<Country> countries = countryRepository.findAll();
    List<CountryDropdownViewModel> returner = new ArrayList<>();

    if (countries != null) {
      returner = countries.stream().map(countryToDropdownViewModelConverter::convertToResponse)
        .collect(Collectors.toList());
    }

    return returner;
  }

  /**
   * get countrys for bootstrap info endpoint.
   */
  public Map<String, String> getAllforBootstrap(String locale) {
    if (bootstrapCountryCache.get(locale) != null) {
      return bootstrapCountryCache.get(locale);
    }
    Locale local = Locale.valueOf(locale);
    Map<String, String> countries = countryRepository.findAll().stream()
      .collect(Collectors.toMap(
        Country::getCode,
        country -> {
          CountryLocale countryLocale =
            countryLocaleRepository.findByCountryAndLocale(country, local);
          return countryLocale == null ? country.getCode() : countryLocale.getTitle();
        }
      ));
    bootstrapCountryCache.put(locale, countries);
    return countries;
  }

  public Country getEntityByCode(String countryCode) {
    return countryRepository.findByCode(countryCode);
  }

  public Country getEntityById(long countryId) {
    return countryRepository.findOne(countryId);
  }

  public CountryViewModel getById(long id) {
    Country country = countryRepository.findOne(id);
    return countryToViewModelConverter.convertToResponse(country);
  }

  /**
   * Update country.
   *
   * @param updateCountryRequest request
   */
  public void update(UpdateCountryRequest updateCountryRequest) {
    Country country = countryRepository.findOne(updateCountryRequest.getId());

    country.setCode(updateCountryRequest.getCode());
    country.setCode3(updateCountryRequest.getCode3());
    country.setCurrency(updateCountryRequest.getCurrency());

    CountryShop countryShop = country.getCountryShop();
    countryShop.setAccountNr(updateCountryRequest.getAccountNr());
    countryShop.setBankName(updateCountryRequest.getBankName());
    countryShop.setBic(updateCountryRequest.getBic());
    countryShop.setIban(updateCountryRequest.getIban());
    countryShop.setDaysToPayInvoice(updateCountryRequest.getDaysToPayInvoice());
    countryShop.setMaxPurchasePrice(updateCountryRequest.getMaxPurchasePrice());
    countryShop.setFreeShippingLimit(updateCountryRequest.getFreeShippingLimit());
    countryShop.setVatRateShipping(updateCountryRequest.getVatRateShipping());
    countryShop.setPriceShippingNet(updateCountryRequest.getPriceShippingNet());

    countryShop.setMinDeliveryDays(updateCountryRequest.getMinDeliveryDays());
    countryShop.setMaxDeliveryDays(updateCountryRequest.getMaxDeliveryDays());

    BigDecimal vatRate = updateCountryRequest.getVat();
    if (vatRate != null) {
      vatRate = vatRate.multiply(BigDecimal.valueOf(100));
      country.setVat(vatRate);
    }

    countryRepository.save(country);
  }
}
