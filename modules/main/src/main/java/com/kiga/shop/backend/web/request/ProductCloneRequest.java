package com.kiga.shop.backend.web.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Florian Schneider
 * @since 14/11/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCloneRequest {
  private long productId;
}