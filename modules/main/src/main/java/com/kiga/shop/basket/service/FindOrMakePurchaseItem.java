package com.kiga.shop.basket.service;

import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.repository.PurchaseItemRepository;
import com.kiga.shop.service.ProductInfoCreator;
import com.kiga.shop.web.response.ProductInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FindOrMakePurchaseItem {
  private ProductInfoCreator productInfoCreator;
  private PurchaseItemRepository purchaseItemRepository;

  @Autowired
  public FindOrMakePurchaseItem(
    ProductInfoCreator productInfoCreator,
    PurchaseItemRepository purchaseItemRepository) {
    this.productInfoCreator = productInfoCreator;
    this.purchaseItemRepository = purchaseItemRepository;
  }

  /**
   * we create or return the existing PurchaseItem from a given Purchase.
   */
  public PurchaseItem get(Purchase purchase, long productId) {
    Optional<PurchaseItem> optExisting = findExisting(productId, purchase);

    return optExisting.orElseGet(() -> {
      ProductInfo productInfo =
        productInfoCreator.create(productId, purchase.getCountry().getCode()).get();
      PurchaseItem purchaseItem = PurchaseItem.builder()
        .title(productInfo.getProductTitle()).product(productInfo.getProduct())
        .amount(0).code(productInfo.getProductCode()).purchase(purchase)
        .build();
      purchaseItem.setClassName("PurchaseItem");
      purchaseItem = purchaseItemRepository.save(purchaseItem);
      purchase.getPurchaseItems().add(purchaseItem);

      return purchaseItem;
    });
  }

  private Optional<PurchaseItem> findExisting(long productId, Purchase purchase) {
    return purchase.getPurchaseItems().stream()
      .filter(purchaseItem -> purchaseItem.getProduct().getId().equals(productId))
      .findFirst();
  }
}
