package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class PurchaseDeliveryPurchaseProduct extends KigaEntityModel {
  private int amount;

  @ManyToOne private PurchaseDelivery purchaseDelivery;

  @ManyToOne
  @JoinColumn(name = "purchaseProductId")
  private PurchaseItem purchaseItem;

  @PrePersist
  public void prePersist() {
    this.setClassName("PurchaseDeliveryPurchaseProduct");
  }
}
