package com.kiga.shop.backend.service;

import com.kiga.accounting.domain.Invoice;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.repository.CountryShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.temporal.ChronoUnit;

/**
 * Creates the invoice from a given purchase.
 */
@Service
public class PurchaseInvoiceMapper {
  private CountryShopRepository countryShopRepository;

  @Autowired
  public PurchaseInvoiceMapper(CountryShopRepository countryShopRepository) {
    this.countryShopRepository = countryShopRepository;
  }

  /**
   * maps the Purchase.
   */
  public Invoice map(Purchase purchase) {
    int dueDays = countryShopRepository.findByCountryId(purchase.getCountry().getId())
      .getDaysToPayInvoice();
    Invoice returner = new Invoice();
    returner.setInvoiceNumber(purchase.getInvoiceNr());
    returner.setInvoiceDate(Date.from(purchase.getInvoiceDate()));
    returner.setDueDate(Date.from(purchase.getInvoiceDate().plus(dueDays, ChronoUnit.DAYS)));
    returner.setCustomerName(purchase.getLastname());
    returner.setCustomerEmail(purchase.getEmail());
    returner.setCurrency(purchase.getCurrency());
    returner.setAmount(purchase.getPriceGrossTotal());
    returner.setSettled(false);
    returner.setCountryCode(purchase.getCountry().getCode());
    returner.setOrderNumber(purchase.getOrderNr().toString());
    returner.setOrderDate(Date.from(purchase.getOrderDate()));
    returner.setDunningEmailSent(false);

    return returner;
  }
}
