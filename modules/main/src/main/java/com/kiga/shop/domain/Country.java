package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** Created by rainerh on 03.04.16. */
@Entity
@Data
@EqualsAndHashCode(callSuper = true, exclude = "countryShop")
@AllArgsConstructor
@Builder
@Inheritance(strategy = InheritanceType.JOINED)
public class Country extends KigaEntityModel {
  private BigDecimal vat;
  private String code;
  private String code3;
  private String currency;
  private Integer sequence;

  @OneToOne(mappedBy = "country")
  private CountryShop countryShop;

  @ManyToOne
  @JoinColumn(name = "countryGroupID", referencedColumnName = "id")
  private CountryGroup countryGroup;

  public Country() {
    this.setClassName("Country");
  }

  /** getters are required for Scala compatibility. */
  public BigDecimal getVat() {
    return vat;
  }

  public String getCode() {
    return code;
  }

  public String getCode3() {
    return code3;
  }

  public String getCurrency() {
    return currency;
  }

  public CountryShop getCountryShop() {
    return countryShop;
  }
}
