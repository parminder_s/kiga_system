package com.kiga.shop.backend;

import com.kiga.security.services.SecurityService;
import com.kiga.shop.backend.exception.IllegalPaginationArgumentException;
import com.kiga.shop.backend.exception.NonexistentEntityUpdateException;
import com.kiga.shop.backend.web.request.ProductDetailRequest;
import com.kiga.shop.backend.web.request.ProductDetailsListRequest;
import com.kiga.shop.backend.web.request.ToggleActivateProductDetailRequest;
import com.kiga.shop.backend.web.response.ProductDetailViewModel;
import com.kiga.shop.service.ProductDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 10/8/16.
 */
@RestController
@RequestMapping("shop/backend/products/details")
public class ProductDetailsController {
  private static final int DEFAULT_PAGE_SIZE = 50;
  private ProductDetailService productDetailService;
  private SecurityService securityService;

  @Autowired
  public ProductDetailsController(
    ProductDetailService productDetailService, SecurityService securityService) {
    this.productDetailService = productDetailService;
    this.securityService = securityService;
  }

  /**
   * Gets list of product details.
   *
   * @param productDetailsListRequest request object with filtering parameters
   * @return list of matching product details
   * @throws IllegalPaginationArgumentException check
   *                                            {@link ProductDetailService#getList(long, int, int)}
   */
  @RequestMapping("list")
  public List<ProductDetailViewModel> getList(
    @RequestBody ProductDetailsListRequest productDetailsListRequest)
    throws IllegalPaginationArgumentException {
    securityService.requireShopAdmin();
    if (productDetailsListRequest == null) {
      productDetailsListRequest = new ProductDetailsListRequest();
    }
    if (productDetailsListRequest.getPage() < 0) {
      productDetailsListRequest.setPage(0);
    }
    if (productDetailsListRequest.getPageSize() < 1) {
      productDetailsListRequest.setPageSize(DEFAULT_PAGE_SIZE);
    }
    return productDetailService
      .getList(productDetailsListRequest.getProductId(), productDetailsListRequest.getPage(),
        productDetailsListRequest.getPageSize());
  }

  @RequestMapping(value = "get")
  public ProductDetailViewModel getOne(
    @RequestBody ProductDetailRequest productDetailRequest) {
    securityService.requireShopAdmin();
    return productDetailService.findOne(productDetailRequest.getProductDetailId());
  }

  @RequestMapping("save")
  public void save(
    @RequestBody ProductDetailViewModel productDetailViewModel) throws
    NonexistentEntityUpdateException {
    securityService.requireShopAdmin();
    productDetailService.save(productDetailViewModel);
  }

  @RequestMapping("deactivate")
  public void deactivate(
    @RequestBody ToggleActivateProductDetailRequest toggleActivateProductDetailRequest) throws
    NonexistentEntityUpdateException {
    securityService.requireShopAdmin();
    productDetailService.toggleActivation(toggleActivateProductDetailRequest.getId(), false);
  }

  @RequestMapping("activate")
  public void activate(
    @RequestBody ToggleActivateProductDetailRequest toggleActivateProductDetailRequest) throws
    NonexistentEntityUpdateException {
    securityService.requireShopAdmin();
    productDetailService.toggleActivation(toggleActivateProductDetailRequest.getId(), true);
  }
}
