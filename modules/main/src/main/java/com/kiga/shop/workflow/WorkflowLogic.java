package com.kiga.shop.workflow;

import com.kiga.shop.spec.Workflowable;


import java.util.function.BiConsumer;

/**
 * Created by faxxe on 1/10/17.
 */
public interface WorkflowLogic<T extends Workflowable, S extends Enum> {

  BiConsumer<T, S> getStageChanger(T element, S statusEnum);

}
