package com.kiga.shop.documents.service;

import com.kiga.shop.domain.Purchase;
import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by peter on 13.02.17.
 */
@AllArgsConstructor
public class JAddressFormatter {

  Purchase purchase;

  List<String> getShippingAddress() {
    return Arrays.asList(
      formatAddressHeader(purchase.getShippingAddressName1(), purchase.getShippingAddressName2()),
      formatAddressStreet(purchase.getShippingStreet(), purchase.getShippingStreetNumber(),
        purchase.getShippingFloorNumber(), purchase.getShippingDoorNumber()),
      formatCity(purchase.getShippingZip(), purchase.getShippingCity()),
      purchase.getCountryTitle());
  }


  /**
   *
   * @return return the billingAddress as list of strings.
   */
  public List<String> getBillAddress() {
    List<String> returner = Arrays.asList(
      formatAddressHeader(purchase.getBillAddressName1(), purchase.getBillAddressName2()),
      formatAddressStreet(purchase.getBillStreet(), purchase.getBillStreetNumber(),
        purchase.getBillFloorNumber(), purchase.getBillDoorNumber()),
      formatCity(purchase.getBillZip(), purchase.getBillCity()),
      purchase.getCountryTitle());
    returner = returner.stream().filter(line -> line != null).collect(Collectors.toList());
    if (returner.size() < 4) {
      return Collections.emptyList();
    } else {
      return returner;
    }

  }

  /**
   *
   * @return @return return the address as list of strings.
   */
  public List<String> getAddress() {
    return Arrays.asList(
      formatAddressHeader(purchase.getAddressName1(), purchase.getAddressName2()),
      formatAddressName(purchase.getFirstname(), purchase.getLastname()),
      formatAddressStreet(purchase.getStreet(), purchase.getStreetNumber(),
        purchase.getFloorNumber(), purchase.getDoorNumber()),
      formatCity(purchase.getZip(), purchase.getCity()),
      purchase.getCountryTitle()).stream()
      .filter(line -> line != null)
      .collect(Collectors.toList());
  }

  String formatAddressName(String firstname, String lastname) {
    return firstname + " " + lastname;
  }

  String formatCity(String zip, String city) {
    return zip + " " + city;
  }

  String formatAddressHeader(String addressName1, String addressName2) {
    if (addressName1 == null || addressName2 == null) {
      return null;
    }
    return addressName1 + " " + addressName2;
  }

  String formatAddressStreet(String street, String streetNumber, String floorNumber,
                                   String doorNumber) {
    return Optional.ofNullable(street).orElse("") + " "
      + Optional.ofNullable(streetNumber).orElse("")
      + Optional.ofNullable(formatAddessNumber(floorNumber)).orElse("")
      + Optional.ofNullable(formatAddessNumber(doorNumber)).orElse("");
  }

  String formatAddessNumber(String number) {
    if (number == null) {
      return null;
    }
    return "/" + number;
  }

}
