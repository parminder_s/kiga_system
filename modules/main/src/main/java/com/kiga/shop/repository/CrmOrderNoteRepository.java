package com.kiga.shop.repository;

import com.kiga.shop.domain.CrmOrderNote;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 30.04.16.
 */
public interface CrmOrderNoteRepository extends CrudRepository<CrmOrderNote, Long> {
}
