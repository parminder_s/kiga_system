package com.kiga.shop.backend.web.request;

import lombok.Data;

/**
 * @author bbs
 * @since 10/8/16.
 */
@Data
public class ProductDetailRequest {
  private long productDetailId;
}
