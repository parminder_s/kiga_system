package com.kiga.shop.backend.web.request;

import lombok.Data;

/**
 * @author Florian Schneider
 * @since 20/11/17.
 */
@Data
public class VoucherFormRequest {
  private long voucherId;
}
