package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.shop.converter.ShippingPartnerConverter;
import com.kiga.shop.converter.ShippingStatusConverter;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class PurchaseDelivery extends KigaEntityModel {

  @ManyToOne private Purchase purchase;

  @Convert(converter = ShippingPartnerConverter.class)
  private ShippingPartner shippingPartner;

  private String shippingNoteNr;
  private Integer shippingNoteId;
  private String shippingNoteS3Url;

  @Convert(converter = ShippingStatusConverter.class)
  private ShippingStatus status;

  @Builder.Default
  @OneToMany(mappedBy = "purchaseDelivery")
  private List<PurchaseDeliveryPurchaseProduct> purchaseProducts = new ArrayList<>();

  public PurchaseDelivery() {
    this.purchaseProducts = new ArrayList<>();
  }

  @PrePersist
  public void prePersist() {
    this.setClassName("PurchaseDelivery");
  }
}
