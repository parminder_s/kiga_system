package com.kiga.shop.converter;

import static com.kiga.shop.domain.ShippingStatus.CsvExported;
import static com.kiga.shop.domain.ShippingStatus.CsvFinished;
import static com.kiga.shop.domain.ShippingStatus.NONE;
import static com.kiga.shop.domain.ShippingStatus.ReadyForShipping;
import static com.kiga.shop.domain.ShippingStatus.Shipped;
import static com.kiga.shop.domain.ShippingStatus.ShippingInformationCreated;

import com.kiga.shop.domain.ShippingStatus;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ShippingStatusConverter implements AttributeConverter<ShippingStatus, String> {

  @Override
  public String convertToDatabaseColumn(ShippingStatus attribute) {
    if (attribute == null) {
      return null;
    }
    switch (attribute) {
      case NONE:
        return "none";
      case ShippingInformationCreated:
        return "shippingInformationCreated";
      case CsvExported:
        return "csvExported";
      case CsvFinished:
        return "csvFinished";
      case ReadyForShipping:
        return "readyForShipping";
      case Shipped:
        return "shipped";
      default:
        throw new RuntimeException("string: " + attribute);
    }
  }

  @Override
  public ShippingStatus convertToEntityAttribute(String dbData) {
    if (dbData == null) {
      return null;
    }
    switch (dbData) {
      case "none":
        return NONE;
      case "shippingInformationCreated":
        return ShippingInformationCreated;
      case "csvExported":
        return CsvExported;
      case "csvFinished":
        return CsvFinished;
      case "readyForShipping":
        return ReadyForShipping;
      case "shipped":
        return Shipped;
      default:
        throw new RuntimeException("string: " + dbData);
    }
  }
}
