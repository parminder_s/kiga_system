package com.kiga.shop.basket.web.service;

import static java.math.RoundingMode.HALF_UP;

import com.kiga.shop.basket.web.response.BasketProduct;
import com.kiga.shop.basket.web.response.BasketResponse;
import com.kiga.shop.basket.web.response.Shipping;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.repository.CountryLocaleRepository;
import com.kiga.shop.service.ProductInfoCreator;
import com.kiga.shop.service.VatRateCalculator;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** This class converts a given purchase to a BasketResponse. */
@Service
public class BasketMapper {
  private CountryLocaleRepository countryLocaleRepository;
  private VatRateCalculator vatRateCalculator;
  private ProductInfoCreator productInfoCreator;

  /** inject beans. */
  @Autowired
  public BasketMapper(
      CountryLocaleRepository countryLocaleRepository,
      VatRateCalculator vatRateCalculator,
      ProductInfoCreator productInfoCreator) {
    this.countryLocaleRepository = countryLocaleRepository;
    this.vatRateCalculator = vatRateCalculator;
    this.productInfoCreator = productInfoCreator;
  }

  /** main entry point. */
  public BasketResponse map(Purchase purchase) {
    BigDecimal totalWithoutDelivery =
        purchase
            .getPriceGrossTotal()
            .subtract(purchase.getPriceShippingNet())
            .subtract(purchase.getVatShipping())
            .setScale(2, HALF_UP);

    return BasketResponse.builder()
        .id(purchase.getId())
        .totalWithoutDelivery(totalWithoutDelivery)
        .total(purchase.getPriceGrossTotal())
        .currency(purchase.getCurrency())
        .country(purchase.getCountry().getCode())
        .shipping(getShipping(purchase))
        .products(getBasketProducts(purchase))
        .build();
  }

  private Shipping getShipping(Purchase purchase) {
    String to =
        countryLocaleRepository
            .findByCountryAndLocale(purchase.getCountry(), purchase.getLocale())
            .getTitle();
    BigDecimal cost =
        purchase.getPriceShippingNet().add(purchase.getVatShipping()).setScale(2, HALF_UP);

    return Shipping.builder().to(to).cost(cost).build();
  }

  private List<BasketProduct> getBasketProducts(Purchase purchase) {
    return purchase
        .getPurchaseItems()
        .stream()
        .map(purchaseItem -> this.getBasketProduct(purchase, purchaseItem))
        .collect(Collectors.toList());
  }

  private BasketProduct getBasketProduct(Purchase purchase, PurchaseItem purchaseItem) {
    Product product = purchaseItem.getProduct();
    CountryShop countryShop = purchase.getCountry().getCountryShop();

    BigDecimal price =
        vatRateCalculator.calculateGrossPrice(
            purchaseItem.getPriceNetSingle(), purchaseItem.getVatRate());

    BasketProduct returner =
        BasketProduct.builder()
            .id(purchaseItem.getId())
            .productId(product.getId())
            .title(purchaseItem.getTitle())
            .amount(purchaseItem.getAmount())
            .sum(purchaseItem.getPriceGrossTotal())
            .minDeliveryDays(countryShop.getMinDeliveryDays())
            .maxDeliveryDays(countryShop.getMaxDeliveryDays())
            .price(price)
            .build();

    if (purchaseItem.getVoucher() == null) {
      returner.setOriginalPrice(price);
      returner.setOriginalSum(returner.getSum());
      returner.setHasDiscount(false);
    } else {
      returner.setOriginalPrice(
          productInfoCreator
              .create(purchaseItem.getProduct().getId(), purchase.getCountry().getCode())
              .get()
              .getPriceGross());
      returner.setOriginalSum(
          returner.getOriginalPrice().multiply(new BigDecimal(purchaseItem.getAmount())));
      returner.setHasDiscount(true);
    }

    return returner;
  }
}
