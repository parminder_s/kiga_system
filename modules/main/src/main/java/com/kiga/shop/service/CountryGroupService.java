package com.kiga.shop.service;

import com.kiga.shop.domain.CountryGroup;
import com.kiga.shop.repository.CountryGroupRepository;
import com.kiga.shop.web.converter.CountryGroupToViewModelConverter;
import com.kiga.shop.web.response.CountryGroupViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 10/16/16.
 */
public class CountryGroupService {
  private CountryGroupRepository countryGroupRepository;
  private CountryGroupToViewModelConverter countryGroupToViewModelConverter;

  public CountryGroupService(CountryGroupRepository countryGroupRepository,
    CountryGroupToViewModelConverter countryGroupToViewModelConverter) {
    this.countryGroupRepository = countryGroupRepository;
    this.countryGroupToViewModelConverter = countryGroupToViewModelConverter;
  }

  /**
   * Get all country groups.
   *
   * @return All country groups
   */
  public List<CountryGroupViewModel> getAll() {
    List<CountryGroup> countryGroups = countryGroupRepository.findAll();
    List<CountryGroupViewModel> countryGroupViewModels = new ArrayList<>();
    if (countryGroups != null) {
      countryGroupViewModels = countryGroups.stream().filter(group -> group != null)
        .map(countryGroupToViewModelConverter::convertToResponse).collect(Collectors.toList());
    }
    return countryGroupViewModels;
  }
}
