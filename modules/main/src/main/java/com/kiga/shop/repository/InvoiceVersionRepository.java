package com.kiga.shop.repository;

import com.kiga.shop.domain.InvoiceVersion;
import com.kiga.shop.domain.Purchase;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by rainerh on 30.04.16.
 */
public interface InvoiceVersionRepository extends CrudRepository<InvoiceVersion, Long> {
  InvoiceVersion findTopByPurchaseOrderByInvoiceVersionDesc(Purchase purchase);

  List<InvoiceVersion> findAllByPurchase(Purchase purchase);

  InvoiceVersion findById(Long id);
}
