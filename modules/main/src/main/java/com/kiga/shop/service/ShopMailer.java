package com.kiga.shop.service;

import static com.kiga.shop.service.ShopMailSubCategory.INVOICE;

import com.kiga.mail.Mailer;
import com.kiga.shop.EmailTemplates;
import com.kiga.shop.ShopProperties;
import com.kiga.shop.domain.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

/** Created by faxxe on 1/4/17. */
@Service
public class ShopMailer {
  public static String SHOP_MAIL_CATEGORY = "shop";

  private Mailer mailer;
  private ShopUrlGenerator shopUrlGenerator;
  private ShopProperties shopProperties;

  /** constructor. */
  @Autowired
  public ShopMailer(
      Mailer mailer, ShopUrlGenerator shopUrlGenerator, ShopProperties shopProperties) {
    this.mailer = mailer;
    this.shopUrlGenerator = shopUrlGenerator;
    this.shopProperties = shopProperties;
  }

  /** mail that invoice. */
  public void emailInvoice(Purchase purchase) {
    String salutation = purchase.getGender().equalsIgnoreCase("male") ? "Herr" : "Frau";
    String invoiceUrl = shopUrlGenerator.generateInvoiceUrl(purchase);
    String body = EmailTemplates.invoiceEmail(salutation, purchase.getLastname(), invoiceUrl);

    mailer.send(
        SHOP_MAIL_CATEGORY,
        INVOICE,
        purchase.getId().toString(),
        email ->
            email
                .withTo(purchase.getEmail())
                .withFrom(shopProperties.getShopEmail())
                .withSubject("Ihre KiGaPortal Rechnung")
                .withBody(body)
                .withInline("idkigalogo", new ClassPathResource("/print/kigalogocolor.png")));
  }
}
