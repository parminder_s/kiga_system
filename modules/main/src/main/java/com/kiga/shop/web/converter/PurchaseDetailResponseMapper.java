package com.kiga.shop.web.converter;

import com.kiga.shop.domain.Purchase;
import com.kiga.shop.web.response.PurchaseDetailResponse;
import com.kiga.util.mapper.InstantStringMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = InstantStringMapper.class)
public interface PurchaseDetailResponseMapper {
  PurchaseDetailResponseMapper INSTANCE = Mappers.getMapper(PurchaseDetailResponseMapper.class);

  @Mapping(target = "country",  source = "countryTitle")
  @Mapping(target = "billCountry", source = "billCountry.code")
  @Mapping(target = "purchaseItems", ignore = true)
  PurchaseDetailResponse purchaseToPurchaseResponse(Purchase purchase);
}
