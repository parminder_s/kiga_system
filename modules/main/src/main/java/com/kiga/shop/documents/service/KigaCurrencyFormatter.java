package com.kiga.shop.documents.service;

import freemarker.core.TemplateNumberFormat;
import freemarker.core.TemplateValueFormatException;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by peter on 3/1/17.
 */
public class KigaCurrencyFormatter  extends TemplateNumberFormat {

  DecimalFormat formatter;
  String currency;

  public KigaCurrencyFormatter(String currency) {
    this.currency = currency;
    formatter = new DecimalFormat( "0.00" , new DecimalFormatSymbols(Locale.GERMAN));
  }

  @Override
  public String formatToPlainText(TemplateNumberModel numberModel)
    throws TemplateValueFormatException, TemplateModelException {
    return currency + " " + formatter.format(numberModel.getAsNumber());
  }

  @Override
  public boolean isLocaleBound() {
    return false;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
