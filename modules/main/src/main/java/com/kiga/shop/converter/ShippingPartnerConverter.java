package com.kiga.shop.converter;

import com.kiga.shop.domain.ShippingPartner;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by peter on 11.01.17.
 */
@Converter
public class ShippingPartnerConverter implements AttributeConverter<ShippingPartner, String> {
  @Override
  public String convertToDatabaseColumn(ShippingPartner attribute) {
    if (attribute == null) {
      return null;
    } else {
      switch (attribute) {
        case DHL:
          return "DHL";
        case POST:
          return "POST";
        case UNKOWN:
          return null;
        default:
          return null;
      }
    }
  }

  @Override
  public ShippingPartner convertToEntityAttribute(String dbData) {
    if ("DHL".equals(dbData)) {
      return ShippingPartner.DHL;
    }
    if ("POST".equals(dbData)) {
      return ShippingPartner.POST;
    }
    if (dbData == null) {
      return ShippingPartner.UNKOWN;
    }
    throw new IllegalArgumentException("Unkown shipping partner: " + dbData);
  }
}
