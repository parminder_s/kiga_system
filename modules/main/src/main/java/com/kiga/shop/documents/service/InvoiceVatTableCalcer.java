package com.kiga.shop.documents.service;

import com.kiga.shop.documents.model.InvoiceVatRow;
import com.kiga.shop.domain.CreditVoucherItem;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by peter on 14.02.17.
 */
public class InvoiceVatTableCalcer {
  /**
   *
   * @param purchase purchase for what to create InvioceTableVats
   * @return list of InvoiceTableVats, sumed up by rate.
   */
  public List<InvoiceVatRow> calc(Purchase purchase) {
    List<InvoiceVatRow> allVatsAllProducts = mapProduct(purchase.getPurchaseItems());
    if (shouldAddShipping(purchase)) {
      allVatsAllProducts.add(new InvoiceVatRow(purchase.getVatRateShipping(),
        purchase.getVatShipping()));
    }
    return sumUpVatRows(allVatsAllProducts);
  }

  public List<InvoiceVatRow> calcCreditVoucher(List<CreditVoucherItem> creditVoucherItems) {
    return sumUpVatRows(mapProductCreditVoucher(creditVoucherItems));
  }

  List<InvoiceVatRow> mapProduct(List<PurchaseItem> purchaseItems) {
    return purchaseItems.stream().map(purchaseItem -> new InvoiceVatRow(
      purchaseItem.getVatRate(), purchaseItem.getVatTotal()))
      .collect(Collectors.toList());
  }

  boolean shouldAddShipping(Purchase purchase) {
    return  (purchase.getVatShipping().compareTo(BigDecimal.ZERO) > 0
      && purchase.getVatRateShipping().compareTo(BigDecimal.ZERO) > 0);
  }

  List<InvoiceVatRow> sumUpVatRows(List<InvoiceVatRow> invoiceVatRows) {
    List<BigDecimal> onlyVats = invoiceVatRows.stream()
      .map(invoiceVatRow -> invoiceVatRow.getRate())
      .distinct()
      .collect(Collectors.toList());

    return onlyVats.stream()
      .map(vat ->
        new InvoiceVatRow(vat, invoiceVatRows.stream()
          .filter(invoiceVatRow -> invoiceVatRow.getRate().compareTo(vat) == 0)
          .map(invoiceVatRow -> invoiceVatRow.getTotal())
          .reduce(BigDecimal.ZERO, BigDecimal::add)))
      .sorted((invoiceVatRow, t1) -> invoiceVatRow.getRate().compareTo(t1.getRate()))
      .collect(Collectors.toList());
  }

  List<InvoiceVatRow> mapProductCreditVoucher(List<CreditVoucherItem> creditVoucherItems) {
    return creditVoucherItems.stream().map(creditVoucherItem -> new InvoiceVatRow(
      creditVoucherItem.getVatRate(), creditVoucherItem.getVatTotal()))
      .collect(Collectors.toList());
  }
}
