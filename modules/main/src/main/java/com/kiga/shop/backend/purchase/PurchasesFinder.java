package com.kiga.shop.backend.purchase;

import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseStatus;
import com.kiga.shop.domain.QPurchase;
import com.mysema.query.types.expr.BooleanExpression;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Service;

@Service
public class PurchasesFinder {
  private QueryDslPredicateExecutor<Purchase> purchaseExecutor;

  @Autowired
  public PurchasesFinder(QueryDslPredicateExecutor<Purchase> purchaseExecutor) {
    this.purchaseExecutor = purchaseExecutor;
  }

  public List<Purchase> find(boolean hideFinished) {
    BooleanExpression expression;

    if (hideFinished) {
      expression =
          QPurchase.purchase.status.in(
              PurchaseStatus.Ordered,
              PurchaseStatus.ShippingInformationCreated,
              PurchaseStatus.CsvExported,
              PurchaseStatus.CsvFinished,
              PurchaseStatus.ReadyForShipping,
              PurchaseStatus.CreditVoucherPending);
    } else {
      expression =
          QPurchase.purchase.status.in(
              PurchaseStatus.Ordered,
              PurchaseStatus.ShippingInformationCreated,
              PurchaseStatus.CsvExported,
              PurchaseStatus.CsvFinished,
              PurchaseStatus.ReadyForShipping,
              PurchaseStatus.Shipped,
              PurchaseStatus.CreditVoucherPending,
              PurchaseStatus.CreditVoucherFinalized,
              PurchaseStatus.Cancelled);
    }

    return (List<Purchase>) purchaseExecutor.findAll(expression);
  }
}
