package com.kiga.shop.workflow;

import static com.kiga.main.paymentmethod.PaymentMethod.INVOICE;
import static com.kiga.shop.domain.PurchaseStatus.Cancelled;
import static com.kiga.shop.domain.PurchaseStatus.CreditVoucherFinalized;
import static com.kiga.shop.domain.PurchaseStatus.CreditVoucherPending;

import com.kiga.shop.backend.purchase.PurchaseCanceller;
import com.kiga.shop.backend.service.InvoiceCreator;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseStatus;
import com.kiga.shop.service.DocumentService;
import com.kiga.shop.service.ShopMailer;
import com.kiga.shop.spec.WorkflowException;
import java.util.EnumSet;
import java.util.function.BiConsumer;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Created by faxxe on 1/10/17. */
@Service
public class PurchaseWorkflowLogic implements WorkflowLogic<Purchase, PurchaseStatus> {

  private final DocumentService documentService;
  private final ShopMailer shopMailer;
  private final InvoiceCreator invoiceCreator;
  private final PurchaseCanceller cancellor;
  private String invoiceLiteral = "invoice";

  @Autowired
  public PurchaseWorkflowLogic(
      DocumentService documentService,
      ShopMailer shopMailer,
      InvoiceCreator invoiceCreator,
      PurchaseCanceller cancellor) {
    this.documentService = documentService;
    this.shopMailer = shopMailer;
    this.invoiceCreator = invoiceCreator;
    this.cancellor = cancellor;
  }

  @Override
  public BiConsumer<Purchase, PurchaseStatus> getStageChanger(
      Purchase element, PurchaseStatus targetStatus) {
    if (element.getStatus() == targetStatus) {
      return this::doNothing;
    }

    if (element.getStatus() == PurchaseStatus.OrderedPending
        && targetStatus == PurchaseStatus.Ordered) {
      return this::finishPurchase;
    }
    if (element.getStatus() == PurchaseStatus.Ordered
        && targetStatus == PurchaseStatus.ShippingInformationCreated) {
      return this::createShippingInformation;
    }

    if (element.getStatus() == PurchaseStatus.ShippingInformationCreated
        && targetStatus == PurchaseStatus.CsvExported) {
      return this::exportCsv;
    }

    if (element.getStatus() == PurchaseStatus.InPayment
        && targetStatus == PurchaseStatus.OrderedPending) {
      return this::finishPurchaseFromChecker;
    }
    if (element.getStatus() == PurchaseStatus.CsvExported
        && targetStatus == PurchaseStatus.CsvFinished) {
      return this::exportCsvFinished;
    }

    if (element.getStatus() == PurchaseStatus.CsvFinished
        && targetStatus == PurchaseStatus.ReadyForShipping) {
      return this::readyForShipping;
    }

    if (element.getStatus() == PurchaseStatus.ReadyForShipping
        && targetStatus == PurchaseStatus.Shipped) {
      return this::ship;
    }

    if (element.getStatus() == PurchaseStatus.Shipped
        && targetStatus == PurchaseStatus.CreditVoucherPending) {
      return this::createCreditVoucher;
    }

    if (element.getStatus() == PurchaseStatus.CreditVoucherPending
        && targetStatus == PurchaseStatus.CreditVoucherFinalized) {
      return this::creditVoucherFinish;
    }

    if (targetStatus.equals(Cancelled)
        && element.getPaymentMethod().equals(INVOICE)
        && Stream.of(CreditVoucherPending, CreditVoucherFinalized, Cancelled)
            .noneMatch(status -> element.getStatus().equals(status))) {
      return this::cancel;
    }

    if ((targetStatus == PurchaseStatus.ShippingInformationCreated)
        && !EnumSet.of(PurchaseStatus.Ordered).contains(element.getStatus())) {
      // TODO: complete and beautify the Set
      return this::doNothing;
    }

    if ((targetStatus == PurchaseStatus.CsvExported)
        && !EnumSet.of(PurchaseStatus.Ordered).contains(element.getStatus())) {
      // TODO: complete and beautify the Set
      return this::doNothing;
    }

    throw new WorkflowException(
        "Cannot change from State <" + element.getStatus() + "> to: <" + targetStatus + ">");
  }

  private void createShippingInformation(Purchase purchase, PurchaseStatus targetStatus) {
    documentService.createShippingNote(purchase);
    if (purchase.getPaymentMethod() == INVOICE) {
      invoiceCreator.createInvoice(purchase);
    }
    purchase.setStatus(targetStatus);
  }

  private void exportCsv(Purchase purchase, PurchaseStatus targetStatus) {
    // do nothing
    purchase.setStatus(targetStatus);
  }

  private void exportCsvFinished(Purchase purchase, PurchaseStatus targetStatus) {
    // do nothing
    purchase.setStatus(targetStatus);
  }

  private void readyForShipping(Purchase purchase, PurchaseStatus targetStatus) {
    // do nothing
    purchase.setStatus(targetStatus);
  }

  private void ship(Purchase purchase, PurchaseStatus targetStatus) {
    if (purchase.getPaymentMethod() != INVOICE) {
      invoiceCreator.createInvoice(purchase);
      shopMailer.emailInvoice(purchase);
    }
    purchase.setStatus(targetStatus);
  }

  private void createCreditVoucher(Purchase purchase, PurchaseStatus targetStatus) {
    // create CreditVoucher
    purchase.setStatus(targetStatus);
  }

  private void creditVoucherFinish(Purchase purchase, PurchaseStatus targetStatus) {
    // finish Creditvoucher
    purchase.setStatus(targetStatus);
  }

  private void finishPurchase(Purchase purchase, PurchaseStatus targetStatus) {
    purchase.setStatus(targetStatus);
  }

  private void finishPurchaseFromChecker(Purchase purchase, PurchaseStatus targetStatus) {
    purchase.setStatus(targetStatus);
  }

  private void cancel(Purchase purchase, PurchaseStatus targetStatus) {
    cancellor.cancel(purchase);
    purchase.setStatus(targetStatus);
  }

  private void doNothing(Purchase purchase, PurchaseStatus targetStatus) {}
}
