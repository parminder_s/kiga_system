package com.kiga.shop.backend.purchase.web;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.shop.backend.api.model.PurchaseResponse;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchaseResponseConverter {
  private InvoiceRepository invoiceRepository;

  @Autowired
  public PurchaseResponseConverter(InvoiceRepository invoiceRepository) {
    this.invoiceRepository = invoiceRepository;
  }

  public PurchaseResponse convertToResponse(Purchase purchase) {
    PurchaseResponse returner =
        PurchaseResponseMapper.INSTANCE.purchaseToPurchaseResponse(purchase);

    returner.setName(purchase.getLastname() + " " + purchase.getFirstname());
    returner.setAmount(
        purchase.getPurchaseItems().stream().mapToInt(PurchaseItem::getAmount).sum());

    Optional<Invoice> invoice =
        Optional.ofNullable(returner.getInvoiceNumber())
            .filter(StringUtils::isNotEmpty)
            .map(this.invoiceRepository::findByInvoiceNumber);
    returner.setInvoiceId(invoice.map(Invoice::getId).orElse(0L));
    returner.setInvoiceSettled(invoice.map(Invoice::isSettled).orElse(true));

    returner.setCountry(returner.getCountry().toUpperCase());

    return returner;
  }
}
