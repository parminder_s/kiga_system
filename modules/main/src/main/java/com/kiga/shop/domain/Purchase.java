package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;
import com.kiga.db.LocaleToLongNotationConverter;
import com.kiga.db.PaymentMethodConverter;
import com.kiga.main.locale.Locale;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.security.domain.Member;
import com.kiga.shop.converter.PurchaseStatusEnumConverter;
import com.kiga.shop.converter.ShippingPartnerConverter;
import com.kiga.shop.spec.Workflowable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by rainerh on 03.04.16.
 *
 * <p>The purchase entity is the main of the shop module and runs through various status, beginning
 * from putting the first product into the basket until final delivery of the complete order.
 *
 * <p>In contrast of having separate tables for basket, checkout and delivery this approach tries to
 * save all relevant data into one single table.
 *
 * <p>From a process oriented view the services that operate on that entity are named after the
 * entity's context, e.g. Basket (no checkout yet), Checkout, Shipping.
 *
 * <p>The purchased products are referenced in PurchaseItem that in contrast to common db modelling
 * techniques save calculated values. This is done to avoid repeated calculations and potential
 * resulting errors during the purchase process.
 *
 * <p>It is possible to have multiple deliveries per purchase. Delivery data is managed in {@link
 * PurchaseDelivery}.
 */
@Entity
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class Purchase extends KigaEntityModel implements Workflowable<PurchaseStatus> {

  @OneToMany(mappedBy = "purchase")
  @OrderBy("id")
  List<PurchaseItem> purchaseItems;

  @Convert(converter = PurchaseStatusEnumConverter.class)
  private PurchaseStatus status;

  private BigDecimal priceNetTotal;
  private BigDecimal priceGrossTotal;
  private BigDecimal vatShipping;
  private BigDecimal vatRateShipping;
  private BigDecimal priceShippingNet;
  private String currency;

  @Convert(converter = LocaleToLongNotationConverter.class)
  private Locale locale;

  private String sessionId;
  private String currentStep;
  private String gender;
  private String firstname;
  private String lastname;
  private String birthday;
  private String email;
  private String addressName1;
  private String addressName2;
  private String phoneNr;
  private String street;
  private String streetNumber;
  private String floorNumber;
  private String doorNumber;
  private String zip;
  private String city;
  private String countryTitle;
  private Boolean billOn;
  private String billAddressName1;
  private String billAddressName2;
  private String billStreet;
  private String billStreetNumber;
  private String billFloorNumber;
  private String billDoorNumber;
  private String billZip;
  private String billCity;
  private Boolean shippingOn;
  private String shippingAddressName1;
  private String shippingAddressName2;
  private String shippingStreet;
  private String shippingStreetNumber;
  private String shippingFloorNumber;
  private String shippingDoorNumber;
  private String shippingZip;
  private String shippingCity;

  @Convert(converter = PaymentMethodConverter.class)
  private PaymentMethod paymentMethod;

  @Convert(converter = ShippingPartnerConverter.class)
  private ShippingPartner shippingPartner;

  private String invoiceOption;
  private Boolean security;
  private Boolean agb;
  private Boolean newsletter;
  private String invoiceNr;
  private Integer invoiceId;
  private Instant invoiceDate;
  private String invoiceS3Url;
  private String invoiceHash;
  private Integer pickingNoteId;
  private String shippingNoteNr;
  private Integer shippingNoteId;
  private String shippingNoteS3Url;
  private String creditVoucherNr;
  private String creditVoucherS3Bucket;
  private String creditVoucherS3Url;
  private Instant creditVoucherDate;
  private Instant orderDate;
  private Long orderNr;
  private String paymentPspId;
  private String comment;

  @ManyToOne
  @JoinColumn(name = "countryId", referencedColumnName = "id")
  private Country country;

  @ManyToOne
  @JoinColumn(name = "billCountryID", referencedColumnName = "id")
  private Country billCountry;

  @ManyToOne
  @JoinColumn(name = "customerID", referencedColumnName = "id")
  private Member customer;

  @OneToMany(mappedBy = "purchase")
  private List<PurchaseWorkflow> purchaseWorkflows;

  @OneToMany private List<PurchaseDelivery> purchaseDeliveries;

  public Purchase() {
    this.setClassName("Purchase");
  }

  public PurchaseStatus getStatus() {
    return status;
  }

  public void setStatus(PurchaseStatus status) {
    this.status = status;
  }

  public BigDecimal getPriceNetTotal() {
    return priceNetTotal;
  }

  public void setPriceNetTotal(BigDecimal priceNetTotal) {
    this.priceNetTotal = priceNetTotal;
  }

  public BigDecimal getPriceGrossTotal() {
    return priceGrossTotal;
  }

  public void setPriceGrossTotal(BigDecimal priceGrossTotal) {
    this.priceGrossTotal = priceGrossTotal;
  }

  public BigDecimal getVatShipping() {
    return vatShipping;
  }

  public void setVatShipping(BigDecimal vatShipping) {
    this.vatShipping = vatShipping;
  }

  public BigDecimal getVatRateShipping() {
    return vatRateShipping;
  }

  public void setVatRateShipping(BigDecimal vatRateShipping) {
    this.vatRateShipping = vatRateShipping;
  }

  public BigDecimal getPriceShippingNet() {
    return priceShippingNet;
  }

  public void setPriceShippingNet(BigDecimal priceShippingNet) {
    this.priceShippingNet = priceShippingNet;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getCurrentStep() {
    return currentStep;
  }

  public void setCurrentStep(String currentStep) {
    this.currentStep = currentStep;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getBirthday() {
    return birthday;
  }

  public void setBirthday(String birthday) {
    this.birthday = birthday;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddressName1() {
    return addressName1;
  }

  public void setAddressName1(String addressName1) {
    this.addressName1 = addressName1;
  }

  public String getAddressName2() {
    return addressName2;
  }

  public void setAddressName2(String addressName2) {
    this.addressName2 = addressName2;
  }

  public String getPhoneNr() {
    return phoneNr;
  }

  public void setPhoneNr(String phoneNr) {
    this.phoneNr = phoneNr;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getStreetNumber() {
    return streetNumber;
  }

  public void setStreetNumber(String streetNumber) {
    this.streetNumber = streetNumber;
  }

  public String getFloorNumber() {
    return floorNumber;
  }

  public void setFloorNumber(String floorNumber) {
    this.floorNumber = floorNumber;
  }

  public String getDoorNumber() {
    return doorNumber;
  }

  public void setDoorNumber(String doorNumber) {
    this.doorNumber = doorNumber;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCountryTitle() {
    return countryTitle;
  }

  public void setCountryTitle(String countryTitle) {
    this.countryTitle = countryTitle;
  }

  public Boolean getBillOn() {
    return billOn;
  }

  public void setBillOn(Boolean billOn) {
    this.billOn = billOn;
  }

  public String getBillAddressName1() {
    return billAddressName1;
  }

  public void setBillAddressName1(String billAddressName1) {
    this.billAddressName1 = billAddressName1;
  }

  public String getBillAddressName2() {
    return billAddressName2;
  }

  public void setBillAddressName2(String billAddressName2) {
    this.billAddressName2 = billAddressName2;
  }

  public String getBillStreet() {
    return billStreet;
  }

  public void setBillStreet(String billStreet) {
    this.billStreet = billStreet;
  }

  public String getBillStreetNumber() {
    return billStreetNumber;
  }

  public void setBillStreetNumber(String billStreetNumber) {
    this.billStreetNumber = billStreetNumber;
  }

  public String getBillFloorNumber() {
    return billFloorNumber;
  }

  public void setBillFloorNumber(String billFloorNumber) {
    this.billFloorNumber = billFloorNumber;
  }

  public String getBillDoorNumber() {
    return billDoorNumber;
  }

  public void setBillDoorNumber(String billDoorNumber) {
    this.billDoorNumber = billDoorNumber;
  }

  public String getBillZip() {
    return billZip;
  }

  public void setBillZip(String billZip) {
    this.billZip = billZip;
  }

  public String getBillCity() {
    return billCity;
  }

  public void setBillCity(String billCity) {
    this.billCity = billCity;
  }

  public Boolean getShippingOn() {
    return shippingOn;
  }

  public void setShippingOn(Boolean shippingOn) {
    this.shippingOn = shippingOn;
  }

  public String getShippingAddressName1() {
    return shippingAddressName1;
  }

  public void setShippingAddressName1(String shippingAddressName1) {
    this.shippingAddressName1 = shippingAddressName1;
  }

  public String getShippingAddressName2() {
    return shippingAddressName2;
  }

  public void setShippingAddressName2(String shippingAddressName2) {
    this.shippingAddressName2 = shippingAddressName2;
  }

  public String getShippingStreet() {
    return shippingStreet;
  }

  public void setShippingStreet(String shippingStreet) {
    this.shippingStreet = shippingStreet;
  }

  public String getShippingStreetNumber() {
    return shippingStreetNumber;
  }

  public void setShippingStreetNumber(String shippingStreetNumber) {
    this.shippingStreetNumber = shippingStreetNumber;
  }

  public String getShippingFloorNumber() {
    return shippingFloorNumber;
  }

  public void setShippingFloorNumber(String shippingFloorNumber) {
    this.shippingFloorNumber = shippingFloorNumber;
  }

  public String getShippingDoorNumber() {
    return shippingDoorNumber;
  }

  public void setShippingDoorNumber(String shippingDoorNumber) {
    this.shippingDoorNumber = shippingDoorNumber;
  }

  public String getShippingZip() {
    return shippingZip;
  }

  public void setShippingZip(String shippingZip) {
    this.shippingZip = shippingZip;
  }

  public String getShippingCity() {
    return shippingCity;
  }

  public void setShippingCity(String shippingCity) {
    this.shippingCity = shippingCity;
  }

  public PaymentMethod getPaymentMethod() {
    return paymentMethod;
  }

  public void setPaymentMethod(PaymentMethod paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  public ShippingPartner getShippingPartner() {
    return shippingPartner;
  }

  public void setShippingPartner(ShippingPartner shippingPartner) {
    this.shippingPartner = shippingPartner;
  }

  public String getInvoiceOption() {
    return invoiceOption;
  }

  public void setInvoiceOption(String invoiceOption) {
    this.invoiceOption = invoiceOption;
  }

  public Boolean getSecurity() {
    return security;
  }

  public void setSecurity(Boolean security) {
    this.security = security;
  }

  public Boolean getAgb() {
    return agb;
  }

  public void setAgb(Boolean agb) {
    this.agb = agb;
  }

  public Boolean getNewsletter() {
    return newsletter;
  }

  public void setNewsletter(Boolean newsletter) {
    this.newsletter = newsletter;
  }

  public String getInvoiceNr() {
    return invoiceNr;
  }

  public void setInvoiceNr(String invoiceNr) {
    this.invoiceNr = invoiceNr;
  }

  public Integer getInvoiceId() {
    return invoiceId;
  }

  public void setInvoiceId(Integer invoiceId) {
    this.invoiceId = invoiceId;
  }

  public Instant getInvoiceDate() {
    return invoiceDate;
  }

  public void setInvoiceDate(Instant invoiceDate) {
    this.invoiceDate = invoiceDate;
  }

  public String getInvoiceS3Url() {
    return invoiceS3Url;
  }

  public void setInvoiceS3Url(String invoiceS3Url) {
    this.invoiceS3Url = invoiceS3Url;
  }

  public String getInvoiceHash() {
    return invoiceHash;
  }

  public void setInvoiceHash(String invoiceHash) {
    this.invoiceHash = invoiceHash;
  }

  public Integer getPickingNoteId() {
    return pickingNoteId;
  }

  public void setPickingNoteId(Integer pickingNoteId) {
    this.pickingNoteId = pickingNoteId;
  }

  public String getShippingNoteNr() {
    return shippingNoteNr;
  }

  public void setShippingNoteNr(String shippingNoteNr) {
    this.shippingNoteNr = shippingNoteNr;
  }

  public Integer getShippingNoteId() {
    return shippingNoteId;
  }

  public void setShippingNoteId(Integer shippingNoteId) {
    this.shippingNoteId = shippingNoteId;
  }

  public String getShippingNoteS3Url() {
    return shippingNoteS3Url;
  }

  public void setShippingNoteS3Url(String shippingNoteS3Url) {
    this.shippingNoteS3Url = shippingNoteS3Url;
  }

  public String getCreditVoucherNr() {
    return creditVoucherNr;
  }

  public void setCreditVoucherNr(String creditVoucherNr) {
    this.creditVoucherNr = creditVoucherNr;
  }

  public String getCreditVoucherS3Bucket() {
    return creditVoucherS3Bucket;
  }

  public void setCreditVoucherS3Bucket(String creditVoucherS3Bucket) {
    this.creditVoucherS3Bucket = creditVoucherS3Bucket;
  }

  public String getCreditVoucherS3Url() {
    return creditVoucherS3Url;
  }

  public void setCreditVoucherS3Url(String creditVoucherS3Url) {
    this.creditVoucherS3Url = creditVoucherS3Url;
  }

  public Instant getCreditVoucherDate() {
    return creditVoucherDate;
  }

  public void setCreditVoucherDate(Instant creditVoucherDate) {
    this.creditVoucherDate = creditVoucherDate;
  }

  public Instant getOrderDate() {
    return orderDate;
  }

  public void setOrderDate(Instant orderDate) {
    this.orderDate = orderDate;
  }

  public Long getOrderNr() {
    return orderNr;
  }

  public void setOrderNr(Long orderNr) {
    this.orderNr = orderNr;
  }

  public String getPaymentPspId() {
    return paymentPspId;
  }

  public void setPaymentPspId(String paymentPspId) {
    this.paymentPspId = paymentPspId;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public List<PurchaseItem> getPurchaseItems() {
    return purchaseItems;
  }

  public void setPurchaseItems(List<PurchaseItem> purchaseItems) {
    this.purchaseItems = purchaseItems;
  }

  public Country getCountry() {
    return country;
  }

  public void setCountry(Country country) {
    this.country = country;
  }

  public Country getBillCountry() {
    return billCountry;
  }

  public void setBillCountry(Country billCountry) {
    this.billCountry = billCountry;
  }

  public Member getCustomer() {
    return customer;
  }

  public void setCustomer(Member customer) {
    this.customer = customer;
  }

  public List<PurchaseWorkflow> getPurchaseWorkflows() {
    return purchaseWorkflows;
  }

  public void setPurchaseWorkflows(List<PurchaseWorkflow> purchaseWorkflows) {
    this.purchaseWorkflows = purchaseWorkflows;
  }
}
