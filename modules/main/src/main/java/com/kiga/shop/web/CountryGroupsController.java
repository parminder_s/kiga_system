package com.kiga.shop.web;

import com.kiga.shop.domain.CountryGroup;
import com.kiga.shop.service.CountryGroupService;
import com.kiga.shop.web.response.CountryGroupViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bbs
 * @since 10/16/16.
 */
@RestController
@RequestMapping("/countries/groups")
public class CountryGroupsController {
  private CountryGroupService countryGroupService;

  @Autowired
  public CountryGroupsController(CountryGroupService countryGroupService) {
    this.countryGroupService = countryGroupService;
  }

  @RequestMapping("list")
  public List<CountryGroupViewModel> list() {
    return countryGroupService.getAll();
  }
}
