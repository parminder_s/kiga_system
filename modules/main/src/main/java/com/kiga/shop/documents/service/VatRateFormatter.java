package com.kiga.shop.documents.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class VatRateFormatter {
  public static String format(BigDecimal vatRate) {
    return new DecimalFormat("#0.#", DecimalFormatSymbols.getInstance(Locale.GERMAN))
        .format(vatRate.multiply(new BigDecimal(100), new MathContext(3)));
  }
}
