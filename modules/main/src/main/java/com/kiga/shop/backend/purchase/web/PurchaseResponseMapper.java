package com.kiga.shop.backend.purchase.web;

import com.kiga.shop.backend.api.model.PurchaseResponse;
import com.kiga.shop.domain.Purchase;
import com.kiga.util.mapper.InstantLocalDateMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper(uses = InstantLocalDateMapper.class)
public interface PurchaseResponseMapper {
  PurchaseResponseMapper INSTANCE = Mappers.getMapper(PurchaseResponseMapper.class);

  @Mappings({
    @Mapping(target = "country", source = "country.code"),
    @Mapping(target = "invoiceNumber", source = "invoiceNr"),
    @Mapping(target = "memberEmail", source = "email"),
    @Mapping(target = "memberId", source = "customer.customerId"),
    @Mapping(target = "shippingNoteNumber", source = "shippingNoteNr"),
    @Mapping(target = "voucherNumber", source = "creditVoucherNr"),
    @Mapping(target = "purchaseDate", source = "orderDate")
  })
  PurchaseResponse purchaseToPurchaseResponse(Purchase purchase);
}
