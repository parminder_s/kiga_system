package com.kiga.shop.repository;

import com.kiga.shop.domain.CreditVoucherItem;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 30.04.16.
 */
public interface CreditVoucherItemRepository extends CrudRepository<CreditVoucherItem, Long> {
}
