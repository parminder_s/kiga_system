package com.kiga.shop.backend.web.response;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Voucher ViewModel.
 */
@Data
public final class VoucherViewModel {
  private Long id;
  private String code;
  private BigDecimal discountRate;
  private Long productId;
  private String productDisplayString;
  private String createdOn;
  private String lastEditedOn;
  private Date validFrom;
  private Date validTo;
}
