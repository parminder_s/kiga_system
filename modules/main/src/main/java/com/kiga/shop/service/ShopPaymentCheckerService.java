package com.kiga.shop.service;

import com.kiga.payment.PaymentCheckerService;
import com.kiga.payment.mpay24.PaymentCheckerResponse;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseStatus;
import com.kiga.shop.repository.PurchaseRepository;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Created by faxxe on 3/30/16. */
public class ShopPaymentCheckerService {

  private PaymentCheckerService paymentCheckerService;
  private PurchaseRepository purchaseRepository;
  private FinishOrderService finishOrderService;
  private Logger logger = LoggerFactory.getLogger(getClass());

  /** constructer. */
  public ShopPaymentCheckerService(
      PaymentCheckerService paymentCheckerService,
      PurchaseRepository purchaseRepository,
      FinishOrderService finishOrderService) {
    this.paymentCheckerService = paymentCheckerService;
    this.purchaseRepository = purchaseRepository;
    this.finishOrderService = finishOrderService;
  }

  /** return string for display only. */
  public List<String> checkSuspendedPayments() {
    List<Purchase> suspendedPurchases = purchaseRepository.findByStatus(PurchaseStatus.InPayment);
    return suspendedPurchases
        .stream()
        .map(purchase -> checkPaymentForPurchase(purchase.getId(), purchase.getLastEdited()))
        .collect(Collectors.toList());
  }

  private String checkPaymentForPurchase(long purchaseId, Date purchaseLastEdited) {
    PaymentCheckerResponse paymentCheckerResponse =
        paymentCheckerService.checkShopPayments(purchaseId, false, purchaseLastEdited);
    return "purchase: "
        + purchaseId
        + ", status: "
        + paymentCheckerResponse.getPaymentStatus().toString()
        + ", pspId: "
        + paymentCheckerResponse.getPspId();
  }

  /** check all Purchases with status = 'inPayment' if the payment has already completed. */
  @Transactional
  public List<String> checkAndFinishedSuspendedPayments() {
    List<Purchase> suspendedPurchases = purchaseRepository.findByStatus(PurchaseStatus.InPayment);
    List lstrings =
        suspendedPurchases
            .stream()
            .map(
                purchase ->
                    checkAndHandlePaymentForPurchase(purchase.getId(), purchase.getLastEdited()))
            .collect(Collectors.toList());
    return lstrings;
  }

  private String checkAndHandlePaymentForPurchase(long purchaseId, Date purchaseLastEdited) {
    PaymentCheckerResponse paymentCheckerResponse =
        paymentCheckerService.checkShopPayments(purchaseId, true, purchaseLastEdited);

    switch (paymentCheckerResponse.getPaymentStatus()) {
      case OK:
        handlePaymentOk(purchaseId, paymentCheckerResponse.getPspId());
        break;
      case SUSPENDED:
        handlePaymentSuspended(purchaseId);
        break;
      case NOT_FOUND:
      case FAILED:
        handlePaymentFailed(purchaseId);
        break;
      default:
      case ERROR:
        handlePaymentError(purchaseId);
    }
    return "purchase: "
        + purchaseId
        + ", status: "
        + paymentCheckerResponse.getPaymentStatus().toString()
        + ", pspId: "
        + paymentCheckerResponse.getPspId();
  }

  private void handlePaymentOk(long purchaseId, String pspId) {
    finishOrderService.finishPurchaseFromSuspendedPayment(purchaseId, pspId);
  }

  private void handlePaymentSuspended(long purchaseId) {
    throw new RuntimeException("unknown status payment suspended for purchase: " + purchaseId);
  }

  private void handlePaymentError(long purchaseId) {
    throw new RuntimeException("unknown status payment error for purchase: " + purchaseId);
  }

  private void handlePaymentFailed(long purchaseId) {
    Purchase purchase = purchaseRepository.findOne(purchaseId);
    purchase.setStatus(PurchaseStatus.Resumed);
    purchaseRepository.save(purchase);
  }
}
