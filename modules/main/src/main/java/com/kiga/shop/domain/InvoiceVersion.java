package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by rainerh on 22.04.16.
 */
@Entity
public class InvoiceVersion extends KigaEntityModel {
  private Long invoiceVersion;
  private String invoiceS3Url;
  private String invoiceS3Bucket;
  @ManyToOne
  @JoinColumn(name = "purchaseId", referencedColumnName = "id")
  private Purchase purchase;

  public InvoiceVersion() {
    this.setClassName("InvoiceVersion");
  }

  public Long getInvoiceVersion() {
    return invoiceVersion;
  }

  public void setInvoiceVersion(Long invoiceVersion) {
    this.invoiceVersion = invoiceVersion;
  }

  public String getInvoiceS3Url() {
    return invoiceS3Url;
  }

  public void setInvoiceS3Url(String invoiceS3Url) {
    this.invoiceS3Url = invoiceS3Url;
  }

  public String getInvoiceS3Bucket() {
    return invoiceS3Bucket;
  }

  public void setInvoiceS3Bucket(String invoiceS3Bucket) {
    this.invoiceS3Bucket = invoiceS3Bucket;
  }

  public Purchase getPurchase() {
    return purchase;
  }

  public void setPurchase(Purchase purchase) {
    this.purchase = purchase;
  }
}
