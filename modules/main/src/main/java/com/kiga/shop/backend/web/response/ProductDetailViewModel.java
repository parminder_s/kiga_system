package com.kiga.shop.backend.web.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author bbs
 * @since 10/8/16.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDetailViewModel {
  private long id;
  private long productId;
  private long countryId;
  private BigDecimal priceGross;
  private boolean activated;
  private Integer maxOrderAmount;
  private BigDecimal vatProduct;
  private String shippingDiscountType;
  private String currencyCode;
  private String country;
}
