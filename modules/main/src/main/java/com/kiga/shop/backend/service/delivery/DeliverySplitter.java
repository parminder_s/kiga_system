package com.kiga.shop.backend.service.delivery;

import com.kiga.shop.domain.PurchaseDelivery;
import com.kiga.spec.Fetcher;
import java.util.List;

public class DeliverySplitter {
  private final PurchaseDeliveryMapper mapper;
  private final DeliverySplitterChecker checker;
  private final DeliverySplitterExecutor splitter;
  private Fetcher<PurchaseDelivery> fetcher;
  private DeliveryPersister deliveryPersister;

  public DeliverySplitter(
      PurchaseDeliveryMapper mapper,
      DeliverySplitterChecker checker,
      DeliverySplitterExecutor splitter,
      Fetcher<PurchaseDelivery> fetcher,
      DeliveryPersister deliveryPersister) {
    this.mapper = mapper;
    this.checker = checker;
    this.splitter = splitter;
    this.fetcher = fetcher;
    this.deliveryPersister = deliveryPersister;
  }

  /** splits by adding purchaseProducts from one to another. */
  public void splitDelivery(
      long purchaseDeliveryId, List<PurchaseProductIdAmount> purchaseProductIdAmounts) {
    PurchaseDelivery purchaseDelivery = fetcher.find(purchaseDeliveryId);
    if (!checker.isValid(purchaseDelivery, purchaseProductIdAmounts)) {
      throw new RuntimeException(
          String.format(
              "Cannot split PurchaseDelivery {0} in new delivery containing: {1}",
              purchaseDelivery.getId(), purchaseProductIdAmounts.toString()));
    }

    PurchaseDelivery newDelivery = mapper.purchaseDelivery(purchaseDelivery);
    splitter.split(purchaseDelivery, newDelivery, purchaseProductIdAmounts);
    deliveryPersister.persist(purchaseDelivery, newDelivery);
  }
}
