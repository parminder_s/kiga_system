package com.kiga.shop;

import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by faxxe on 1/3/17.
 */
@Service
public class ShopBackendService {

  private DocumentService documentService;
  private DocumentHelper documentHelper;

  @Autowired
  public ShopBackendService(DocumentService documentService, DocumentHelper documentHelper) {
    this.documentService = documentService;
    this.documentHelper = documentHelper;
  }

  /**
   * generate pdfs and merge documents for selected orders.
   */
  public File printSelectedOrders(List<Purchase> purchases) {

    List<File> files = new ArrayList<>();
    for (Purchase purchase: purchases) {
      files.addAll(printSelectedOrder(purchase));
    }

    String filename = "/tmp/si_" + System.currentTimeMillis() + ".pdf";
    documentHelper.mergeFiles(files, filename);
    return new File(filename);

  }


  /**
   * print orders.
   */
  public List<File> printSelectedOrder(Purchase purchase) {

    List<File> files = new ArrayList<>();

    File shippingNote = new File(documentService.downloadShippingNote(purchase));

    files.add(shippingNote);

    if (purchase.getPaymentMethod() == PaymentMethod.INVOICE) {
      files.add(new File(documentService.downloadInvoice(purchase)));
    }

    return files;
  }

}
