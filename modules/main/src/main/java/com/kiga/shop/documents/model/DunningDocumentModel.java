package com.kiga.shop.documents.model;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.dunning.domain.DunningLevel;
import com.kiga.accounting.dunning.web.response.DunningLevelViewModel;
import com.kiga.accounting.transactions.domain.PaymentTransaction;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.domain.Purchase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Map;

/**
 * @author bbs
 * @since 2/22/17.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DunningDocumentModel extends InvoiceDocumentModel {

  private int dunningLevel;
  private String dunningDate;
  private BigDecimal alreadyPaid;
  /**
   *
   * @param purchase purchase.
   * @param productDetails productDetails.
   * @param invoice for the dunning document.
   */

  public DunningDocumentModel(Purchase purchase, Map<Long, ProductDetail> productDetails,
                              Invoice invoice) {
    super(purchase, productDetails);
    dunningDate = formatter.format(Instant.now());
    this.daysToPay = invoice.getDunningLevel().getDays();
    this.dunningLevel = invoice.getDunningLevel().getDunningLevel();
    if (invoice.getDunningLevel().getFeesAmount().compareTo(BigDecimal.ZERO) != 0) {
      this.products.add(new PurchaseItemRow(null, "Mahngebühren", "",
        invoice.getDunningLevel().getFeesAmount(),
        invoice.getDunningLevel().getFeesAmount(), "0%", null, null, null,
        invoice.getDunningLevel().getFeesAmount()));
      this.taxTable.add(new InvoiceVatRow(BigDecimal.ZERO, BigDecimal.ZERO));
      this.taxTable.sort((invoiceVatRow, toCompare) ->
        invoiceVatRow.getRate().compareTo(toCompare.getRate()));
      this.priceGrossTotal = this.getPriceGrossTotal()
        .add(invoice.getDunningLevel().getFeesAmount());
      this.priceNetTotal = this.priceNetTotal.add(invoice.getDunningLevel().getFeesAmount());
    }
    this.alreadyPaid = invoice.getPaymentTransactions().stream()
      .map(PaymentTransaction::getAmount)
      .reduce(BigDecimal.ZERO, BigDecimal::add);
    this.transferalSum = this.getTransferalSum()
      .add(invoice.getDunningLevel().getFeesAmount())
      .subtract(this.alreadyPaid);
    if (this.alreadyPaid.compareTo(BigDecimal.ZERO) == 0) {
      this.alreadyPaid = null;
    }
  }
}
