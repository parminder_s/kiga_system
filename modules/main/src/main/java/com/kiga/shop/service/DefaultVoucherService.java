package com.kiga.shop.service;

import com.kiga.shop.backend.exception.IllegalPaginationArgumentException;
import com.kiga.shop.backend.exception.NonexistentEntityUpdateException;
import com.kiga.shop.backend.web.converter.VoucherToViewModelConverter;
import com.kiga.shop.backend.web.response.VoucherViewModel;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Voucher;
import com.kiga.shop.repository.VoucherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Florian Schneider
 * @since 20/11/17.
 */
@Service
public class DefaultVoucherService {
  private VoucherRepository voucherRepository;
  private DefaultProductService defaultProductService;
  private VoucherToViewModelConverter voucherToViewModelConverter;

  /**
   * Constructor.
   */
  @Autowired
  public DefaultVoucherService(VoucherRepository voucherRepository,
                               VoucherToViewModelConverter voucherToViewModelConverter,
                               DefaultProductService defaultProductService) {
    this.voucherRepository = voucherRepository;
    this.voucherToViewModelConverter = voucherToViewModelConverter;
    this.defaultProductService = defaultProductService;
  }

  /**
   * Get vouchers.
   *
   * @param page page number
   * @param pageSize page size
   * @return list of voucher viewmodels
   * @throws IllegalPaginationArgumentException if any of the arguments is invalid
   */
  public List<VoucherViewModel> getVouchers(int page, int pageSize) throws
    IllegalPaginationArgumentException {
    if (page < 0) {
      throw new IllegalPaginationArgumentException("Page must be 0 or greater");
    }

    if (pageSize <= 0) {
      throw new IllegalPaginationArgumentException("Page size must be greater than 0");
    }

    List<Voucher> vouchers = voucherRepository
      .findByActivatedIsTrue(new PageRequest(page, pageSize));

    if (vouchers != null) {
      return vouchers.stream().filter(voucher -> voucher != null)
        .map(voucherToViewModelConverter::convertToResponse).collect(Collectors.toList());
    }

    return new ArrayList<>();
  }

  public VoucherViewModel getVoucher(long id) {
    Voucher voucher = getVoucherEntity(id);
    return voucherToViewModelConverter.convertToResponse(voucher);
  }

  public Voucher getVoucherEntity(long id) {
    return voucherRepository.findByActivatedTrueAndId(id);
  }

  /**
   * Save voucher.
   *
   * @param voucherViewModel voucher to be saved
   * @throws NonexistentEntityUpdateException if no voucher of provided id can be found
   */
  public void save(VoucherViewModel voucherViewModel) throws NonexistentEntityUpdateException {
    if (voucherViewModel != null) {
      Long id = voucherViewModel.getId();
      Voucher voucher;
      Product product;
      if (id == null) {
        voucher = new Voucher();
        voucher.setClassName("Voucher");
        voucher.setCreated(new Date());
        voucher.setActivated(true);

        product = defaultProductService.getProductEntityById(voucherViewModel.getProductId());
        if (product == null) {
          throw new NonexistentEntityUpdateException(
                  "The product with ID=" + voucherViewModel.getProductId() + " does not exist!");
        }

      } else {
        voucher = voucherRepository.findByActivatedTrueAndId(id);
        if (voucher == null) {
          throw new NonexistentEntityUpdateException(
            "The voucher with ID=" + id + " does not exist!");
        }
        product = defaultProductService.getProductEntityById(voucherViewModel.getProductId());
        if (product == null) {
          throw new NonexistentEntityUpdateException(
                  "The product with ID=" + voucherViewModel.getProductId() + " does not exist!");
        }
      }
      voucher.setProduct(product);
      voucher.setDiscountRate(voucherViewModel.getDiscountRate());
      voucher.setCode(voucherViewModel.getCode());

      voucher.setValidFromDate(voucherViewModel.getValidFrom().toInstant());
      voucher.setValidToDate(voucherViewModel.getValidTo().toInstant());
      voucher.setLastEdited(new Date());

      voucherRepository.save(voucher);
    }
  }

  /**
   * Deactivate voucher.
   *
   * @param id voucher id
   * @throws NonexistentEntityUpdateException exception when no voucher of given id found
   */
  public void deactivate(long id) throws NonexistentEntityUpdateException {
    Voucher voucher = voucherRepository.findByActivatedTrueAndId(id);
    if (voucher == null) {
      throw new NonexistentEntityUpdateException("Voucher with Id=" + id + " does not exists");
    }

    voucher.setActivated(false);
    voucherRepository.save(voucher);
  }
}
