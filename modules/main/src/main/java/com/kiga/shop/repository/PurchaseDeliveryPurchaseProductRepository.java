package com.kiga.shop.repository;

import com.kiga.shop.domain.PurchaseDeliveryPurchaseProduct;
import org.springframework.data.repository.CrudRepository;

public interface PurchaseDeliveryPurchaseProductRepository
    extends CrudRepository<PurchaseDeliveryPurchaseProduct, Long> {}
