package com.kiga.shop.backend;

import com.kiga.security.services.SecurityService;
import com.kiga.shop.backend.exception.IllegalPaginationArgumentException;
import com.kiga.shop.backend.exception.NonexistentEntityUpdateException;
import com.kiga.shop.backend.web.request.DeactivateProductRequest;
import com.kiga.shop.backend.web.request.ProductCloneRequest;
import com.kiga.shop.backend.web.request.ProductFormRequest;
import com.kiga.shop.backend.web.request.ProductsListRequest;
import com.kiga.shop.backend.web.response.CloneIdResponse;
import com.kiga.shop.backend.web.response.ProductViewModel;
import com.kiga.shop.domain.Product;
import com.kiga.shop.repository.ProductDetailRepository;
import com.kiga.shop.repository.ProductRepository;
import com.kiga.shop.service.DefaultProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bbs
 * @since 10/7/16.
 */
@RestController
@RequestMapping("shop/backend/products")
class ProductsController {
  private static final int DEFAULT_PAGE_SIZE = 50;
  private DefaultProductService defaultProductService;
  private SecurityService securityService;
  private ProductRepository productRepository;
  private ProductDetailRepository productDetailRepository;

  @Autowired
  ProductsController(DefaultProductService defaultProductService,
                     SecurityService securityService, ProductRepository productRepository,
                     ProductDetailRepository productDetailRepository) {

    this.defaultProductService = defaultProductService;
    this.securityService = securityService;
    this.productRepository = productRepository;
    this.productDetailRepository = productDetailRepository;
  }

  @RequestMapping("clone")
  public CloneIdResponse cloneProduct(
          @RequestBody ProductCloneRequest productCloneRequest)
          throws NonexistentEntityUpdateException {
    this.securityService.requireShopAdmin();
    long productId = productCloneRequest.getProductId();
    Product product = productRepository.findOne(productId);
    ProductCloner productCloner = new ProductCloner();
    Product clonedProduct = productCloner.cloneProduct(product);
    // possible stream variation of this? better/safer way?
    while (productRepository.findByCode(clonedProduct.getCode()) != null) {
      clonedProduct.setCode(clonedProduct.getCode() + "_C");
    }
    productRepository.save(clonedProduct);
    clonedProduct.getProductDetails().stream()
            .forEach(productDetail -> productDetailRepository.save(productDetail));
    CloneIdResponse response = new CloneIdResponse(clonedProduct.getId());
    return response;
  }

  @RequestMapping("list")
  List<ProductViewModel> getList() {
    this.securityService.requireShopAdmin();

    List<ProductViewModel> productViewModels = defaultProductService
      .getProducts();

    if (productViewModels == null) {
      productViewModels = new ArrayList<>();
    }

    return productViewModels;
  }

  @RequestMapping("get")
  public ProductViewModel getProduct(
    @RequestBody ProductFormRequest productFormRequest) {
    this.securityService.requireShopAdmin();
    return defaultProductService.getProductById(productFormRequest.getProductId());
  }

  @RequestMapping("save")
  public void save(
    @RequestBody ProductViewModel productViewModel) throws NonexistentEntityUpdateException {
    this.securityService.requireShopAdmin();
    productViewModel.setActivated(true);
    defaultProductService.save(productViewModel);
  }

  @RequestMapping("deactivate")
  public void deactivate(@RequestBody DeactivateProductRequest deactivateProductRequest)
    throws NonexistentEntityUpdateException {
    this.securityService.requireShopAdmin();
    defaultProductService.deactivate(deactivateProductRequest.getId());
  }
}
