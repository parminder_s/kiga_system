package com.kiga.shop;

import com.kiga.shop.service.CountryService;
import com.kiga.shop.web.response.CountryViewModel;
import com.kiga.web.service.GeoIpService;
import com.kiga.web.web.request.SwitchCountryRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 10/16/16.
 */
@RestController
@RequestMapping("/countries")
public class CountriesController {
  private CountryService countryService;
  private GeoIpService geoIpService;

  @Autowired
  public CountriesController(CountryService countryService, GeoIpService geoIpService) {
    this.countryService = countryService;
    this.geoIpService = geoIpService;
  }

  @RequestMapping("list")
  public List<CountryViewModel> list() {
    return countryService.getAll();
  }

  @RequestMapping("switch")
  public void switchCountry(@RequestBody SwitchCountryRequest switchCountryRequest) {
    geoIpService.changeCountryCode(switchCountryRequest.getNewCountryCode());
  }

  @RequestMapping(value = "remove", method = RequestMethod.POST)
  public void remove() {
    geoIpService.removeCountryCode();
  }
}
