package com.kiga.shop.basket.web.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BasketResponse {
  private long id;
  private List<BasketProduct> products;
  private Shipping shipping;
  private BigDecimal totalWithoutDelivery;
  private BigDecimal total;
  private String currency;
  private String country;
}
