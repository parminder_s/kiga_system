package com.kiga.shop.documents.model;

import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.shop.documents.service.JAddressFormatter;
import com.kiga.shop.documents.service.JPurchaseItemRowFactory;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.domain.Purchase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Map;

/**
 * Created by peter on 13.02.17.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class InvoiceDocumentModel extends ShopDocumentBaseModel {

  PaymentMethod paymentMethod;
  String iban;
  String bic;
  String bankName;

  /**
   *
   * @param purchase purchase for which to create ShopInvoiceDocument
   * @param productDetails productDetails for the purchased Products.
   */
  public  InvoiceDocumentModel(Purchase purchase, Map<Long, ProductDetail> productDetails) {
    super(purchase, productDetails, true);
    this.paymentMethod = purchase.getPaymentMethod();
    this.products = new JPurchaseItemRowFactory().createMultiple(purchase, productDetails, true);
    this.paymentMethod = purchase.getPaymentMethod();
    this.daysToPay = purchase.getCountry().getCountryShop().getDaysToPayInvoice();

    this.iban = purchase.getCountry().getCountryShop().getIban();
    this.bic = purchase.getCountry().getCountryShop().getBic();
    this.bankName = purchase.getCountry().getCountryShop().getBankName();
    this.shippingAddress = new JAddressFormatter(purchase).getAddress();
    this.billingAddress = new JAddressFormatter(purchase).getBillAddress();
  }

}
