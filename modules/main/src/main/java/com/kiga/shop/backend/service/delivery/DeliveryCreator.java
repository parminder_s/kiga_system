package com.kiga.shop.backend.service.delivery;

import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseDelivery;
import com.kiga.shop.domain.PurchaseDeliveryPurchaseProduct;
import com.kiga.shop.domain.ShippingStatus;
import com.kiga.spec.Persister;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeliveryCreator {
  private Persister<PurchaseDelivery> deliveryPersister;
  private Persister<PurchaseDeliveryPurchaseProduct> productPersister;

  @Autowired
  public DeliveryCreator(
      Persister<PurchaseDelivery> deliveryPersister,
      Persister<PurchaseDeliveryPurchaseProduct> productPersister) {
    this.deliveryPersister = deliveryPersister;
    this.productPersister = productPersister;
  }

  public void create(Purchase purchase) {
    PurchaseDelivery delivery = new PurchaseDelivery();
    delivery.setPurchase(purchase);
    delivery.setStatus(ShippingStatus.NONE);
    deliveryPersister.persist(delivery);

    delivery.setPurchaseProducts(
        purchase
            .getPurchaseItems()
            .stream()
            .map(
                purchaseItem ->
                    new PurchaseDeliveryPurchaseProduct(
                        purchaseItem.getAmount(), delivery, purchaseItem))
            .peek(productPersister::persist)
            .collect(Collectors.toList()));
  }
}
