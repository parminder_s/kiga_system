package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by rainerh on 22.04.16.
 */
@Entity
public class CreditVoucherItem extends KigaEntityModel {
  private Long amount;
  private String title;
  private String code;
  private BigDecimal priceGrossTotal;
  private BigDecimal priceNetTotal;
  private BigDecimal vatRate;
  private BigDecimal vatTotal;

  @ManyToOne
  @JoinColumn(name = "purchaseID", referencedColumnName = "id")
  private Purchase purchase;

  @ManyToOne
  @JoinColumn(name = "productID", referencedColumnName = "id")
  private Product product;

  public CreditVoucherItem() {
    this.setClassName("CreditVoucherItem");
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public BigDecimal getPriceGrossTotal() {
    return priceGrossTotal;
  }

  public void setPriceGrossTotal(BigDecimal priceGrossTotal) {
    this.priceGrossTotal = priceGrossTotal;
  }

  public BigDecimal getPriceNetTotal() {
    return priceNetTotal;
  }

  public void setPriceNetTotal(BigDecimal priceNetTotal) {
    this.priceNetTotal = priceNetTotal;
  }

  public BigDecimal getVatRate() {
    return vatRate;
  }

  public void setVatRate(BigDecimal vatRate) {
    this.vatRate = vatRate;
  }

  public BigDecimal getVatTotal() {
    return vatTotal;
  }

  public void setVatTotal(BigDecimal vatTotal) {
    this.vatTotal = vatTotal;
  }

  public Purchase getPurchase() {
    return purchase;
  }

  public void setPurchase(Purchase purchase) {
    this.purchase = purchase;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }
}
