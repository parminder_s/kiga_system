package com.kiga.shop.service;

import com.kiga.shop.domain.Country;
import com.kiga.shop.repository.CountryShopRepository;
import com.kiga.shop.web.converter.CountryShopToViewModelConverter;
import com.kiga.shop.web.response.CountryShopViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author bbs
 * @since 2/18/17.
 */
@Service
public class CountryShopService {
  private CountryService countryService;
  private CountryShopRepository countryShopRepository;
  private CountryShopToViewModelConverter countryShopToViewModelConverter;

  /**
   * Constructor.
   *
   * @param countryService                  service
   * @param countryShopRepository           repository
   * @param countryShopToViewModelConverter converter
   */
  @Autowired
  public CountryShopService(CountryService countryService,
    CountryShopRepository countryShopRepository,
    CountryShopToViewModelConverter countryShopToViewModelConverter) {
    this.countryService = countryService;
    this.countryShopRepository = countryShopRepository;
    this.countryShopToViewModelConverter = countryShopToViewModelConverter;
  }

  public List<CountryShopViewModel> getAll() {
    return countryShopToViewModelConverter.convertToResponse(countryShopRepository.findAll());
  }

  public CountryShopViewModel getByCode(String countryCode) {
    Country entityByCode = countryService.getEntityByCode(countryCode);
    return countryShopToViewModelConverter.convertToResponse(entityByCode.getCountryShop());
  }
}
