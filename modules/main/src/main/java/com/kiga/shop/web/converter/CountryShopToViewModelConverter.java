package com.kiga.shop.web.converter;

import com.kiga.main.locale.Locale;
import com.kiga.shop.domain.CountryLocale;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.web.response.CountryShopViewModel;
import com.kiga.shop.web.response.CountryViewModel;
import com.kiga.web.converter.DefaultViewModelConverter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author bbs
 * @since 2/18/17.
 */
@Service
public class CountryShopToViewModelConverter
  extends DefaultViewModelConverter<CountryShop, CountryShopViewModel> {
  private ModelMapper modelMapper;

  @Autowired
  public CountryShopToViewModelConverter(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  @Override
  public CountryShopViewModel convertToResponse(CountryShop entity) {
    if (entity == null) {
      return null;
    }

    CountryShopViewModel returner = modelMapper.map(entity, CountryShopViewModel.class);
    returner.setBankName(entity.getBankName());

    return returner;
  }
}
