package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Same principle as Purchase. We are storing each data-field, risking denormalization
 * but are safe when prices, etc. change and we have to trace back to the original settings.
 */
@Entity
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class PurchaseItem extends KigaEntityModel {
  private Integer amount;
  private String title;
  private String code;
  private BigDecimal priceNetSingle;
  private BigDecimal priceGrossTotal;
  private BigDecimal volumeLitreSingle;
  private Integer weightGramSingle;
  private BigDecimal volumeLitreTotal;
  private Integer weightGramTotal;

  private BigDecimal vatRate;
  private BigDecimal vatTotal;

  private BigDecimal priceShippingNetSingle;
  private BigDecimal priceShippingNetTotal;
  private BigDecimal vatShippingRate;
  private BigDecimal vatShippingTotal;

  @ManyToOne
  @JoinColumn(name = "purchaseID", referencedColumnName = "id")
  private Purchase purchase;

  @ManyToOne
  @JoinColumn(name = "productID", referencedColumnName = "id")
  private Product product;

  @ManyToOne
  private Voucher voucher;

  @Builder.Default
  private String voucherCode = "";

  @Builder.Default
  private BigDecimal appliedDiscountGrossTotal = BigDecimal.ZERO;

  @Builder.Default
  private BigDecimal appliedDiscountRate = BigDecimal.ZERO;

  /**
   * no args constructor setting default values.
   */
  public PurchaseItem() {
    this.setClassName("PurchaseItem");
    this.voucherCode = "";
    this.appliedDiscountGrossTotal = BigDecimal.ZERO;
    this.appliedDiscountRate = BigDecimal.ZERO;
  }

  public Integer getAmount() {
    return amount;
  }

  public String getTitle() {
    return title;
  }

  public String getCode() {
    return code;
  }

  public BigDecimal getPriceNetSingle() {
    return priceNetSingle;
  }

  public BigDecimal getPriceGrossTotal() {
    return priceGrossTotal;
  }

  public BigDecimal getVolumeLitreSingle() {
    return volumeLitreSingle;
  }

  public Integer getWeightGramSingle() {
    return weightGramSingle;
  }

  public BigDecimal getVolumeLitreTotal() {
    return volumeLitreTotal;
  }

  public Integer getWeightGramTotal() {
    return weightGramTotal;
  }

  public BigDecimal getVatRate() {
    return vatRate;
  }

  public BigDecimal getVatTotal() {
    return vatTotal;
  }

  public BigDecimal getPriceShippingNetSingle() {
    return priceShippingNetSingle;
  }

  public BigDecimal getPriceShippingNetTotal() {
    return priceShippingNetTotal;
  }

  public BigDecimal getVatShippingRate() {
    return vatShippingRate;
  }

  public BigDecimal getVatShippingTotal() {
    return vatShippingTotal;
  }

  public Purchase getPurchase() {
    return purchase;
  }

  public Product getProduct() {
    return product;
  }
}
