package com.kiga.shop.documents.model;

import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.domain.Purchase;

import java.util.Map;

/**
 * Created by peter on 2/15/17.
 */
public class PaymentReminderModel extends ShopDocumentBaseModel {
  public PaymentReminderModel(Purchase purchase, Map<Long, ProductDetail> productDetails) {
    super(purchase, productDetails, true);
  }
}
