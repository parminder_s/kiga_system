package com.kiga.shop.service;

import com.kiga.shop.web.responses.InvoicesResponse;

import java.util.List;

/**
 * Created by faxxe on 4/13/16.
 */
public interface ShopInvoiceService {
  List<InvoicesResponse> getInvoicesForCustomer(Long customerId);
}
