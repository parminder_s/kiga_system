package com.kiga.shop.workflow;

import com.kiga.shop.spec.Workflowable;


import java.util.List;
import java.util.function.BiConsumer;

import javax.transaction.Transactional;

/**
 * Created by faxxe on 1/11/17.
 */
public class Workflow<T extends Workflowable, S extends Enum> {

  private List<WorkflowLogic<T, S>> logics;

  public Workflow(List<WorkflowLogic<T, S>> logics) {
    this.logics = logics;
  }

  public void proceedToState(List<T> workflowables, S targetStatus) {
    workflowables.forEach(workflowable -> proceedToState(workflowable, targetStatus));
  }

  /**
   * move the workflowable to the next state.
   */
  @Transactional
  public void proceedToState(T workflowable, S targetStatus) {
    BiConsumer<T, S> consumer = getConsumerFromLogics(workflowable, targetStatus);
    consumer.accept(workflowable, targetStatus);
  }

  private BiConsumer<T, S> getConsumerFromLogics(T element, S targetStatus) {
    return logics
      .stream()
      .map(logic -> logic.getStageChanger(element, targetStatus))
      .findFirst().get();
  }

}
