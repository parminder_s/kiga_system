package com.kiga.shop.backend.service.delivery;

import com.kiga.shop.domain.PurchaseDelivery;
import com.kiga.util.mapper.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapStructConfig.class)
public interface PurchaseDeliveryMapper {
  PurchaseDeliveryMapper INSTANCE = Mappers.getMapper(PurchaseDeliveryMapper.class);

  @Mappings(
    {
      @Mapping(target = "purchaseProducts", ignore = true),
      @Mapping(target = "status", ignore = true)
    }
  )
  PurchaseDelivery purchaseDelivery(PurchaseDelivery purchaseDelivery);
}
