package com.kiga.shop.backend.web.request;

import lombok.Data;

/**
 * @author bbs
 * @since 10/8/16.
 */
@Data
public class ProductDetailsListRequest {
  private int page;
  private int pageSize;
  private long productId;
}
