package com.kiga.shop.documents.model;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;

/** Created by peter on 2/14/17. */
@Data
@AllArgsConstructor
public class PurchaseItemRow {
  private String amount = "";
  private String title = "";
  private String code = "";
  private BigDecimal priceGrossSingle;
  private BigDecimal priceGrossTotal;
  private String vatRateString;
  private String weightGramTotal;
  private String maxDeliveryDays;
  private String minDeliveryDays;
  private BigDecimal price;

  public String getAmount() {
    return amount;
  }

  public String getTitle() {
    return title;
  }

  public String getCode() {
    return code;
  }

  public BigDecimal getPriceGrossSingle() {
    return priceGrossSingle;
  }

  public BigDecimal getPriceGrossTotal() {
    return priceGrossTotal;
  }

  public String getVatRateString() {
    return vatRateString;
  }

  public String getWeightGramTotal() {
    return weightGramTotal;
  }

  public String getMaxDeliveryDays() {
    return maxDeliveryDays;
  }

  public String getMinDeliveryDays() {
    return minDeliveryDays;
  }

  public BigDecimal getPrice() {
    return price;
  }
}
