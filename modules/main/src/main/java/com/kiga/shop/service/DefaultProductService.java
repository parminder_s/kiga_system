package com.kiga.shop.service;

import com.kiga.shop.backend.exception.NonexistentEntityUpdateException;
import com.kiga.shop.backend.web.converter.ProductToViewModelConverter;
import com.kiga.shop.backend.web.response.ProductViewModel;
import com.kiga.shop.domain.Product;
import com.kiga.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bbs
 * @since 10/7/16.
 */
@Service
public class DefaultProductService {
  private ProductRepository productRepository;
  private ProductToViewModelConverter productToViewModelConverter;


  /**
   * Constructor.
   */
  @Autowired
  public DefaultProductService(ProductRepository productRepository,
                               ProductToViewModelConverter productToViewModelConverter) {
    this.productRepository = productRepository;
    this.productToViewModelConverter = productToViewModelConverter;
  }

  /**
   * Get products.
   *
   * @return list of product viewModels
   */
  public List<ProductViewModel> getProducts() {


    List<Product> products = productRepository
      .findByActivatedIsTrue();

    if (products != null) {
      return products.stream().filter(product -> product != null)
        .map(productToViewModelConverter::convertToResponse).collect(Collectors.toList());
    }

    return new ArrayList<>();
  }

  public ProductViewModel getProductById(long id) {
    Product product = getProductEntityById(id);
    return productToViewModelConverter.convertToResponse(product);
  }

  Product getProductEntityById(long id) {
    return productRepository.findByActivatedTrueAndId(id);
  }

  /**
   * Save product.
   *
   * @param productViewModel product to be saved
   * @throws NonexistentEntityUpdateException if no product of provided id can be found
   */
  public void save(ProductViewModel productViewModel) throws NonexistentEntityUpdateException {
    if (productViewModel != null) {
      Long id = productViewModel.getId();
      Product product;
      if (id == null) {
        product = new Product();
      } else {
        product = productRepository.findByActivatedTrueAndId(id);
        if (product == null) {
          throw new NonexistentEntityUpdateException(
            "The product with ID=" + id + " does not exist!");
        }
      }

      product.setVolumeLitre(productViewModel.getVolumeLitre());
      product.setProductTitle(productViewModel.getProductTitle());
      product.setCode(productViewModel.getCode());
      product.setWeightGram(productViewModel.getWeightGram());
      product.setActivated(productViewModel.isActivated());

      productRepository.save(product);
    }
  }

  /**
   * Deactivate product.
   *
   * @param id product id
   * @throws NonexistentEntityUpdateException exception when no product of given id found
   */
  public void deactivate(long id) throws NonexistentEntityUpdateException {
    Product product = productRepository.findByActivatedTrueAndId(id);
    if (product == null) {
      throw new NonexistentEntityUpdateException("Product with Id=" + id + " does not exists");
    }

    product.setActivated(false);
    productRepository.save(product);
  }
}
