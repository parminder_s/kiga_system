package com.kiga.shop.service;

import com.kiga.shop.backend.web.converter.ProductDetailsToViewModelConverter;
import com.kiga.shop.backend.web.response.ProductDetailViewModel;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.repository.ProductDetailRepository;
import com.kiga.shop.repository.ProductRepository;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author bbs
 * @since 10/8/16.
 */
@Service
@Builder
public class ProductDetailService {
  private ProductDetailsToViewModelConverter productDetailsToViewModelConverter;
  private ProductDetailRepository productDetailRepository;
  private ProductRepository productRepository;
  private ProductDetailUpdater productDetailUpdater;

  /** Add dependencies. */
  @Autowired
  public ProductDetailService(
      ProductDetailsToViewModelConverter productDetailsToViewModelConverter,
      ProductDetailRepository productDetailRepository,
      ProductRepository productRepository,
      ProductDetailUpdater productDetailUpdater) {
    this.productDetailsToViewModelConverter = productDetailsToViewModelConverter;
    this.productDetailRepository = productDetailRepository;
    this.productRepository = productRepository;
    this.productDetailUpdater = productDetailUpdater;
  }

  /** returns the ProductDetail for a product in paged version. */
  public List<ProductDetailViewModel> getList(long productId, int page, int pageSize) {
    Pageable pageable = new PageRequest(page, pageSize);
    return productDetailRepository
        .findByProductId(productId, pageable)
        .stream()
        .map(productDetailsToViewModelConverter::convertToResponse)
        .collect(Collectors.toList());
  }

  public ProductDetailViewModel findOne(long id) {
    ProductDetail productDetail = productDetailRepository.findOne(id);
    return productDetailsToViewModelConverter.convertToResponse(productDetail);
  }

  /** saves a or creates a new ProductDetail. */
  public void save(ProductDetailViewModel productDetailViewModel) {
    productDetailViewModel.setPriceGross(productDetailViewModel.getPriceGross().setScale(2));
    productDetailViewModel.setVatProduct(productDetailViewModel.getVatProduct().setScale(3));
    if (productDetailViewModel.getId() == 0) {
      this.productDetailUpdater.update(productDetailViewModel, new ProductDetail());
    } else {
      this.productDetailUpdater.update(
          productDetailViewModel, productDetailRepository.findOne(productDetailViewModel.getId()));
    }
  }

  /** de- or activates a productdetail given its current status. */
  public void toggleActivation(long id, boolean newState) {
    ProductDetail productDetail = productDetailRepository.findOne(id);
    productDetail.setActivated(newState);
    productDetailRepository.save(productDetail);
  }

  public List<ProductDetail> findByProductId(long productId) {
    Product product = productRepository.findOne(productId);
    return product.getProductDetails();
  }
}
