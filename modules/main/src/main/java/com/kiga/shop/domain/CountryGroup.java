package com.kiga.shop.domain;

import com.kiga.db.KigaEntityModel;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by rainerh on 03.04.16.
 */
@Entity
public class CountryGroup extends KigaEntityModel {
  private String title;

  @ManyToOne
  @JoinColumn(name = "countryGroupID", referencedColumnName = "id")
  private CountryGroup countryGroup;

  public CountryGroup() {
    this.setClassName("CountryGroup");
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public CountryGroup getCountryGroup() {
    return countryGroup;
  }

  public void setCountryGroup(CountryGroup countryGroup) {
    this.countryGroup = countryGroup;
  }
}
