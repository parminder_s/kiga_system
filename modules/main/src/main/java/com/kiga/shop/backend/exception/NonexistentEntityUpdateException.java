package com.kiga.shop.backend.exception;

/**
 * @author bbs
 * @since 10/13/16.
 */
public class NonexistentEntityUpdateException extends Exception {
  public NonexistentEntityUpdateException(String message) {
    super(message);
  }
}
