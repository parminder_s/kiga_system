package com.kiga.shop.basket.service;

import com.kiga.shop.basket.service.price.DiscountSetter;
import com.kiga.shop.basket.service.price.PriceGrossWithoutShippingCalcer;
import com.kiga.shop.basket.service.price.PurchaseItemSetter;
import com.kiga.shop.basket.service.price.ShippingCostsSetter;
import com.kiga.shop.domain.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PricesSetter {
  private ShippingCostsSetter shippingCostsSetter;
  private PurchaseItemSetter purchaseItemSetter;
  private PriceGrossWithoutShippingCalcer priceGrossWithoutShippingCalcer;
  private DiscountSetter discountSetter;


  /**
   * inject beans.
   */
  @Autowired
  public PricesSetter(
    ShippingCostsSetter shippingCostsSetter, PurchaseItemSetter purchaseItemSetter,
    PriceGrossWithoutShippingCalcer priceGrossWithoutShippingCalcer,
    DiscountSetter discountSetter) {
    this.shippingCostsSetter = shippingCostsSetter;
    this.purchaseItemSetter = purchaseItemSetter;
    this.priceGrossWithoutShippingCalcer = priceGrossWithoutShippingCalcer;
    this.discountSetter = discountSetter;
  }

  /**
   * sets the prices for all purchaseItems and the Purchase itself.
   */
  public void setPrices(Purchase purchase) {
    purchaseItemSetter.setPurchaseItems(purchase);
    discountSetter.applyDiscounts(purchase.getPurchaseItems());
    shippingCostsSetter.setCosts(purchase);

    purchase.setCurrency(purchase.getCountry().getCurrency());
    purchase.setPriceGrossTotal(
      priceGrossWithoutShippingCalcer.calc(purchase)
        .add(purchase.getPriceShippingNet())
        .add(purchase.getVatShipping())
        .setScale(2));
    purchase.setPriceNetTotal(getPriceNetTotal(purchase));
  }

  private BigDecimal getPriceNetTotal(Purchase purchase) {
    return purchase.getPurchaseItems().stream()
      .map(purchaseItem ->
        purchaseItem.getPriceNetSingle().multiply(new BigDecimal(purchaseItem.getAmount())))
      .reduce(BigDecimal::add)
      .orElse(BigDecimal.ZERO)
      .add(purchase.getPriceShippingNet());
  }
}
