package com.kiga.shop.web.response;

import com.kiga.cms.web.response.ViewModel;
import com.kiga.shopcontent.viewmodel.ShopCategoryViewModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * @author bbs
 * @since 9/9/16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ShopViewModel extends ViewModel {
  private ShopCategoryViewModel activeCategory;
  private List<ShopCategoryViewModel> subcategories;
  private boolean knownCountry;

}
