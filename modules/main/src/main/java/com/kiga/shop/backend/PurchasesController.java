package com.kiga.shop.backend;

import com.kiga.shop.backend.api.controller.ShopBackendApi;
import com.kiga.shop.backend.api.model.PurchaseResponse;
import com.kiga.shop.backend.purchase.web.CancelPurchaseService;
import com.kiga.shop.backend.purchase.web.FindPurchasesService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PurchasesController implements ShopBackendApi {
  private final FindPurchasesService findPurchasesService;
  private CancelPurchaseService cancelPurchaseService;

  @Autowired
  public PurchasesController(
      FindPurchasesService findPurchasesService, CancelPurchaseService cancelPurchaseService) {
    this.findPurchasesService = findPurchasesService;
    this.cancelPurchaseService = cancelPurchaseService;
  }

  @Override
  public ResponseEntity<List<PurchaseResponse>> listPurchases(Boolean hideFinished) {
    return ResponseEntity.ok(findPurchasesService.findPurchases(hideFinished));
  }

  @Override
  public ResponseEntity<PurchaseResponse> cancelPurchase(Integer purchaseId) {
    return ResponseEntity.ok(cancelPurchaseService.cancel(purchaseId));
  }
}
