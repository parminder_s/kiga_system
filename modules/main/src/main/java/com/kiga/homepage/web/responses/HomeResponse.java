package com.kiga.homepage.web.responses;

import com.kiga.forum.web.responses.NewestForumResponse;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Created by peter on 22.06.16. */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HomeResponse {
  private List<IdeasViewModelIdea> newIdeas;
  private NewestForumResponse ideaForum;
  private NewestForumResponse activityForum;
  private List<TopicResponse> thema;
  private List<SliderResponse> slider;
  private String newIdeasUrl;
}
