package com.kiga.homepage.web.requests;

import com.kiga.homepage.domain.HomePageElementContext;
import com.kiga.main.locale.Locale;
import com.kiga.web.message.EndpointRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by peter on 01.08.17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PublishAllReqTest extends EndpointRequest {
  HomePageElementContext context;

  public PublishAllReqTest(HomePageElementContext context, Locale locale) {
    super(locale);
    this.context = context;
  }
}
