package com.kiga.homepage.web.responses;

import com.kiga.homepage.domain.HomePageElement;
import lombok.Data;

/** Created by peter on 22.06.16. */
@Data
public class TopicResponse {
  private String bigImageUrl;
  private String smallImageUrl;
  private String linkUrl;
  private String title;
  private String titleShort;
  private String color;
  private String descriptionTitle;
  private String description;

  /** Default constructor, needed cache. */
  public TopicResponse() {}

  /** @param homePageElement which data should be in the slider response. */
  public TopicResponse(HomePageElement homePageElement) {
    this.bigImageUrl = homePageElement.getBig().getUrl();
    this.smallImageUrl = homePageElement.getSmall().getUrl();
    this.linkUrl = homePageElement.getUrl();
    this.title = homePageElement.getTitle();
    this.color = homePageElement.getColor();
    this.titleShort = homePageElement.getTitleShort();
    this.descriptionTitle = homePageElement.getDescriptionTitle();
    this.description = homePageElement.getDescription();
  }
}
