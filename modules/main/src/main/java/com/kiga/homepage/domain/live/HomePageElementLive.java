package com.kiga.homepage.domain.live;

import com.kiga.homepage.domain.HomePageElement;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;

/** Created by peter on 30.08.16. */
@Entity
@Table(name = "HomePagePreview_Live")
@NamedEntityGraph(
    name = "homePagePreviewGetHomeLive",
    attributeNodes = {@NamedAttributeNode(value = "big"), @NamedAttributeNode(value = "small")})
public class HomePageElementLive extends HomePageElement implements com.kiga.db.fixture.Entity {
  @Id
  @Column(name = "ID")
  private Long id;

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }
}
