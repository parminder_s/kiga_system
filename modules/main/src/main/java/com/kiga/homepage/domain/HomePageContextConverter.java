package com.kiga.homepage.domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by peter on 31.07.17.
 */
@Converter(autoApply = true)
public class HomePageContextConverter implements
  AttributeConverter<HomePageElementContext, String> {
  @Override
  public String convertToDatabaseColumn(HomePageElementContext attribute) {
    switch (attribute) {
      case shop:
        return "shop";
      case homepage:
        return "homepage";
      default:
        throw new IllegalArgumentException("Unknown " + attribute);
    }
  }

  @Override
  public HomePageElementContext convertToEntityAttribute(String dbData) {
    switch (dbData) {
      case "shop":
        return HomePageElementContext.shop;
      case "homepage":
        return HomePageElementContext.homepage;
      default:
        throw new IllegalArgumentException("Unknown " + dbData);
    }
  }
}
