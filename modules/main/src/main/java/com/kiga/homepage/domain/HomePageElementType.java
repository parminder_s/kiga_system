package com.kiga.homepage.domain;

/**
 * Created by peter on 21.06.16.
 */
public enum HomePageElementType {
  slider("slider"), theme("thema");
  private final String stringValue;
  private HomePageElementType(final String stringValue) {
    this.stringValue = stringValue;
  }

  public String toString() {
    return stringValue;
  }

}
