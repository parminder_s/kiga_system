package com.kiga.homepage.service;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.kiga.homepage.domain.HomePageElement;
import com.kiga.homepage.domain.HomePageElementContext;
import com.kiga.homepage.domain.HomePageElementType;
import com.kiga.homepage.domain.draft.HomePageElementDraft;
import com.kiga.homepage.domain.live.HomePageElementLive;
import com.kiga.homepage.repository.draft.HomePageElementRepositoryDraft;
import com.kiga.homepage.repository.live.HomePageElementRepositoryLive;
import com.kiga.main.locale.Locale;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;
import com.kiga.security.domain.Member;
import com.kiga.upload.UploadParameters;
import com.kiga.upload.factory.AbstractFileUploader;
import com.kiga.upload.factory.UploadFactory;
import org.modelmapper.ModelMapper;
import org.modelmapper.internal.util.Lists;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by peter on 15.09.16.
 */
public class HomePageEditService {
  HomePageElementRepositoryDraft homePageElementRepositoryDraft;
  HomePageElementRepositoryLive homePageElementRepositoryLive; //replaced with publish-process
  UploadFactory uploadFactory;

  /**
   *
   * @param homePageElementRepositoryDraft draft repository.
   * @param homePageElementRepositoryLive live repository.
   * @param uploadFactory uploadFactory.
   */
  public HomePageEditService(HomePageElementRepositoryDraft homePageElementRepositoryDraft,
                             HomePageElementRepositoryLive homePageElementRepositoryLive,
                             UploadFactory uploadFactory) {
    this.homePageElementRepositoryDraft = homePageElementRepositoryDraft;
    this.homePageElementRepositoryLive = homePageElementRepositoryLive;
    this.uploadFactory = uploadFactory;
  }

  /**
   *
   * @param homePagePreview new homepagepreview.
   */
  public boolean add(HomePageElementDraft homePagePreview) {
    if (homePagePreview.getLocale() == null) {
      return false;
    }
    Integer oldPosition = homePagePreview.getPosition();
    homePagePreview.setPosition(Lists.from(homePageElementRepositoryDraft
      .findByContext(homePagePreview.getContext()).iterator())
      .size());
    homePageElementRepositoryDraft.save(homePagePreview); //save it to last place, then update!
    homePagePreview.setPosition(oldPosition);
    update(homePagePreview); //so that all positions are updated if needed
    return true;
  }

  /**
   *
   * @param id id of HomePagePreview which to delete from Draft.
   */
  public void delete(Long id) {
    if (homePageElementRepositoryDraft.exists(id)) {
      homePageElementRepositoryDraft.delete(id);
    }
  }

  /**
   *
   * @param id id of HomePagePreview which to delete from Life.
   */
  public void unpublish(Long id) {
    if (homePageElementRepositoryLive.exists(id)) {
      homePageElementRepositoryLive.delete(id);
    }
  }

  /**
   *
   * @param homePagePreviewDraft homePagePreviewDraft to update.
   */
  public  void update(HomePageElementDraft homePagePreviewDraft) {
    HomePageElement old = homePageElementRepositoryDraft.findOne(
      homePagePreviewDraft.getId());

    if (!old.getPosition().equals(homePagePreviewDraft.getPosition())) {
      List<HomePageElement> drafts = homePageElementRepositoryDraft
        .findByTypeAndContextAndLocaleOrderByPositionAsc(
          old.getType(), old.getContext(), old.getLocale());
      //move right
      if (old.getPosition() < homePagePreviewDraft.getPosition()) {
        drafts.stream()
          .filter((draft) -> draft.getPosition() <= homePagePreviewDraft.getPosition())
          .filter((draft) -> draft.getPosition() > old.getPosition())
          .forEach((draft) -> draft.setPosition(draft.getPosition() - 1));
      } else { //move left
        drafts.stream()
          .filter((draft) -> draft.getPosition() >= homePagePreviewDraft.getPosition())
          .filter((draft) -> draft.getPosition() < old.getPosition())
          .forEach((draft) -> draft.setPosition(draft.getPosition() + 1));
      }
      old.setPosition(homePagePreviewDraft.getPosition());
      drafts.forEach((draft) -> homePageElementRepositoryDraft.save((HomePageElementDraft)draft));
    }
    homePageElementRepositoryDraft.save(homePagePreviewDraft);
  }

  public List<HomePageElement> list(Locale locale, HomePageElementType type,
                                    HomePageElementContext context) {
    return homePageElementRepositoryDraft.findByTypeAndContextAndLocaleOrderByPositionAsc(
      type,context, locale);
  }

  /**
   * publishes all.
   */
  public boolean publishAll(HomePageElementContext context) {
    homePageElementRepositoryDraft.findByContext(context)
      .forEach((draft) -> publish(draft.getId()));
    clearLiveRepo(context);
    return true;
  }

  /**
   * uploads an s3Image.
   */
  public S3Image uploadS3Image(MultipartFile file, Member member) throws Exception {
    AbstractFileUploader fileUploader = uploadFactory.getUploaderInstance(file, member);
    UploadParameters uploadParameters = new UploadParameters();
    uploadParameters.setCannedAccessControlList(CannedAccessControlList.PublicRead);
    uploadParameters.setImageVariants(new String[0]);
    fileUploader.setUploadParameters(uploadParameters);
    KigaS3File kigaS3File = fileUploader.upload();
    if (kigaS3File != null) {
      return (S3Image)kigaS3File;
    } else {
      return null;
    }
  }

  /**
   * publishes the homepage elements.
   */
  public HomePageElement publish(Long id) {
    HomePageElementDraft draft = homePageElementRepositoryDraft.findOne(id);
    HomePageElementLive live = homePageElementRepositoryLive.findOne(id);
    //how to best sync tables
    //
    if (live == null) {
      live = new HomePageElementLive();
    }
    ModelMapper modelMapper = new ModelMapper();
    modelMapper.map(draft, live);
    return homePageElementRepositoryLive.save(live);
  }

  private void clearLiveRepo(HomePageElementContext context) {
    homePageElementRepositoryLive.findByContext(context)
      .forEach((live) -> checklive(live.getId()));
  }

  private void checklive(Long id) {
    if (homePageElementRepositoryDraft.exists(id) == false) {
      homePageElementRepositoryLive.delete(id);
    }
  }
}
