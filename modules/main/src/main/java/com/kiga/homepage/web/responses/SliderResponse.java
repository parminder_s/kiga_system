package com.kiga.homepage.web.responses;

import com.kiga.homepage.domain.HomePageElement;
import lombok.Data;

/**
 * Created by peter on 22.06.16.
 */
@Data
public class SliderResponse {
  private String bigImageUrl;
  private String smallImageUrl;
  private String linkUrl;
  private String alternateText;


  /**
   *
   * @param homePageElement which data should be in the slider response.
   */
  public SliderResponse(HomePageElement homePageElement) {
    this.bigImageUrl = homePageElement.getBig().getUrl();
    this.smallImageUrl = homePageElement.getSmall().getUrl();
    this.linkUrl = homePageElement.getUrl();
    this.alternateText = homePageElement.getTitle();
  }

  public SliderResponse() {

  }
}
