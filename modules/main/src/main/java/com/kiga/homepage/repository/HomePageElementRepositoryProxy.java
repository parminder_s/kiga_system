package com.kiga.homepage.repository;

import com.kiga.content.repository.ss.AbstractRepositoryProxy;
import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.homepage.domain.HomePageElement;
import com.kiga.homepage.domain.HomePageElementContext;
import com.kiga.homepage.domain.HomePageElementType;
import com.kiga.homepage.repository.abstraction.HomePageElementRepository;
import com.kiga.homepage.repository.draft.HomePageElementRepositoryDraft;
import com.kiga.homepage.repository.live.HomePageElementRepositoryLive;
import com.kiga.main.locale.Locale;

import java.util.List;

/**
 * Created by peter on 15.09.16.
 */
public class HomePageElementRepositoryProxy
  extends AbstractRepositoryProxy<HomePageElementRepository> implements HomePageElementRepository {

  /**
   * Constructor for the Model.
   *
   * @param liveRepository    live repository
   * @param draftRepository   draft repository
   * @param repositoryContext repository context holder
   */
  public HomePageElementRepositoryProxy(HomePageElementRepositoryLive liveRepository,
                                        HomePageElementRepositoryDraft draftRepository,
                                        RepositoryContext repositoryContext) {
    super(liveRepository, draftRepository, repositoryContext);
  }

  @Override
  public List<HomePageElement> findByPositionGreaterThanAndTypeAndContext(Integer position,
                                                                HomePageElementType type,
                                                                HomePageElementContext context) {
    return getRepository().findByPositionGreaterThanAndTypeAndContext(position, type, context);
  }

  @Override
  public List<HomePageElement> findByTypeAndContextAndLocaleOrderByPositionAsc(
    HomePageElementType type, HomePageElementContext context, Locale locale) {
    return getRepository().findByTypeAndContextAndLocaleOrderByPositionAsc(type, context, locale);
  }

  @Override
  public List<HomePageElement> findByContext(HomePageElementContext context) {
    return getRepository().findByContext(context);
  }

}
