package com.kiga.homepage.service;

import com.kiga.forum.service.NewestForumService;
import com.kiga.homepage.domain.HomePageElementContext;
import com.kiga.homepage.web.responses.HomeResponse;
import com.kiga.ideas.properties.IdeasRetrieveProperties;
import com.kiga.ideas.service.NewIdeasGroupService;
import com.kiga.main.locale.Locale;

/** Created by peter on 21.06.16. */
public class HomePageFacade {
  private NewIdeasGroupService newIdeasGroupService;
  private HomePageElementService homePageElementService;
  private NewestForumService newestForumService;

  public HomePageFacade(
      NewIdeasGroupService newIdeasGroupService,
      HomePageElementService homePageElementService,
      NewestForumService newestForumService) {
    this.newIdeasGroupService = newIdeasGroupService;
    this.homePageElementService = homePageElementService;
    this.newestForumService = newestForumService;
  }

  /** return the complete data required for rendering home. */
  public HomeResponse getHome(Locale locale) {
    return HomeResponse.builder()
        .activityForum(newestForumService.getNewestForum("idea", locale))
        .ideaForum(newestForumService.getNewestForum("article", locale))
        .newIdeas(
            newIdeasGroupService.getIdeasForAllAgeGroups(
                IdeasRetrieveProperties.builder().locale(locale).ageGroupFilter(0).build()))
        .newIdeasUrl(newIdeasGroupService.getNewIdeasUrl(locale))
        .slider(homePageElementService.getSlider(locale, HomePageElementContext.homepage))
        .thema(homePageElementService.getTopic(locale, HomePageElementContext.homepage))
        .build();
  }
}
