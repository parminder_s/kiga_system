package com.kiga.homepage.repository.draft;

import com.kiga.homepage.domain.draft.HomePageElementDraft;
import com.kiga.homepage.repository.abstraction.HomePageElementRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by peter on 30.08.16.
 */
public interface HomePageElementRepositoryDraft extends CrudRepository<HomePageElementDraft, Long>,
  HomePageElementRepository {
}
