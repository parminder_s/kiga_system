package com.kiga.homepage.web;

import com.kiga.homepage.domain.HomePageContextConverter;
import com.kiga.homepage.domain.HomePageElement;
import com.kiga.homepage.domain.HomePageElementConverter;
import com.kiga.homepage.service.HomePageEditService;
import com.kiga.homepage.web.requests.HomePageElementRequest;
import com.kiga.homepage.web.requests.PublishAllReqTest;
import com.kiga.s3.domain.S3Image;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import com.kiga.web.message.EndpointRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

/**
 * Created by peter on 15.09.16.
 */
@RestController
@RequestMapping("/homepage")
public class HomePageEditController {
  private HomePageEditService homePageEditService;
  private SecurityService securityService;
  private HomePageElementConverter homePageElementConverter;
  private HomePageContextConverter homePageContextConverter;

  /**
   *
   * @param homePageEditService homePageEditService.
   * @param securityService securityService.
   */
  @Autowired
  public HomePageEditController(
    HomePageEditService homePageEditService, SecurityService securityService) {
    this.homePageEditService = homePageEditService;
    this.securityService = securityService;
    this.homePageElementConverter = new HomePageElementConverter();
    this.homePageContextConverter = new HomePageContextConverter();
  }

  /**
   *
   * @param id of HomePagePreview which should be deleted.
   */
  @RequestMapping(
    value = "/delete/{id}",
    method = RequestMethod.POST
  )
  public void delete(@PathVariable("id") Long id, @RequestBody EndpointRequest endpointRequest) {
    securityService.requiresPublisher(endpointRequest.getLocale());
    homePageEditService.delete(id);
  }

  @Caching(
    evict = {
      @CacheEvict(value = "default",key = "'homepage-' + #endpointRequest.getLocale()"),
      @CacheEvict(value = "default", key = "'shop-slider-de'")
    })
  @RequestMapping(
    value = "/publish/{id}",
    method = RequestMethod.POST
  )
  public void publish(@PathVariable("id") Long id, @RequestBody EndpointRequest endpointRequest) {
    securityService.requiresPublisher(endpointRequest.getLocale());
    homePageEditService.publish(id);
  }

  @Caching(
    evict = {
      @CacheEvict(value = "default", key = "'homepage-de'"),
      @CacheEvict(value = "default", key = "'homepage-en'"),
      @CacheEvict(value = "default", key = "'homepage-it'"),
      @CacheEvict(value = "default", key = "'homepage-tr'"),
      @CacheEvict(value = "default", key = "'shop-slider-de'")

    })
  @RequestMapping(
    value = "/publish/all",
    method = RequestMethod.POST
  )
  public void publish(@RequestBody PublishAllReqTest publishAllRequest) {
    securityService.requiresPublisher(publishAllRequest.getLocale());
    homePageEditService.publishAll(publishAllRequest.getContext());
  }

  @Caching(
    evict = {
    @CacheEvict(value = "default",key = "'homepage-' + #endpointRequest.getLocale()"),
    @CacheEvict(value = "default", key = "'shop-slider-de'")
    })
  @RequestMapping(
    value = "/unpublish/{id}",
    method = RequestMethod.POST)
  public void unpublish(@PathVariable("id") Long id,@RequestBody EndpointRequest endpointRequest) {
    securityService.requiresPublisher(endpointRequest.getLocale());
    homePageEditService.unpublish(id);
  }

  /**
   *
   * @param homePageElementRequest for updating a new HomePagePreview in the DB.
   */
  @RequestMapping(
    value = "/update",
    method = RequestMethod.POST
  )
  public void update(@RequestBody HomePageElementRequest homePageElementRequest) {
    securityService.requiresPublisher(homePageElementRequest.getLocale());
    homePageEditService.update(homePageElementRequest.getHomePageElement());
  }

  /**
   * uploader for new images.
   */
  @Transactional
  @RequestMapping(
    value = "/uploadImage",
    method = RequestMethod.POST
  )
  public S3Image uploadImage(@RequestParam MultipartFile file) throws Exception {
    securityService.requiresCmsMember();
    Optional<Member> member = securityService.getSessionMember();
    if (member.isPresent()) {
      return homePageEditService.uploadS3Image(file, member.get());
    } else {
      return null;
    }
  }

  /**
   *
   * @param homePageElementRequest  for adding a new HomePagePreview in the DB.
   */
  @RequestMapping(
    value = "/add",
    method = RequestMethod.POST
  )
  public boolean add(@RequestBody HomePageElementRequest homePageElementRequest) {
    securityService.requiresPublisher(homePageElementRequest.getLocale());
    homePageElementRequest.getHomePageElement()
      .setLocale(homePageElementRequest.getLocale());
    return homePageEditService.add(homePageElementRequest.getHomePageElement());
  }

  /**
   *
   * @param endpointRequest request with locale.
   * @return list of homePagePreview in locale.
   */
  @RequestMapping(
    value = "/list/{type}/{context}",
    method = RequestMethod.POST
  )
  public List<HomePageElement> list(@RequestBody EndpointRequest endpointRequest,
                                    @PathVariable("type") String type,
                                    @PathVariable("context") String context) {
    securityService.requiresPublisher(endpointRequest.getLocale());
    return homePageEditService.list(endpointRequest.getLocale(),
      homePageElementConverter.convertToEntityAttribute(type),
      homePageContextConverter.convertToEntityAttribute(context));
  }
}
