package com.kiga.homepage.domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by peter on 15.09.16.
 */
@Converter(autoApply = true)
public class HomePageElementConverter implements
  AttributeConverter<HomePageElementType, String> {
  @Override
  public String convertToDatabaseColumn(HomePageElementType attribute) {
    switch (attribute) {
      case slider:
        return "slider";
      case theme:
        return "thema";
      default:
        throw new IllegalArgumentException("Unknown " + attribute);
    }
  }

  @Override
  public HomePageElementType convertToEntityAttribute(String dbData) {
    switch (dbData) {
      case "slider":
        return HomePageElementType.slider;
      case "thema":
        return HomePageElementType.theme;
      default:
        throw new IllegalArgumentException("Unknown " + dbData);
    }
  }
}
