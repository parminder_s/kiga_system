package com.kiga.homepage.service;

import com.kiga.homepage.domain.HomePageElementContext;
import com.kiga.homepage.domain.HomePageElementType;
import com.kiga.homepage.repository.HomePageElementRepositoryProxy;
import com.kiga.homepage.web.responses.SliderResponse;
import com.kiga.homepage.web.responses.TopicResponse;
import com.kiga.main.locale.Locale;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by peter on 15.09.16.
 */
public class HomePageElementService {

  HomePageElementRepositoryProxy homePageElementRepositoryProxy;

  public HomePageElementService(HomePageElementRepositoryProxy homePageElementRepositoryProxy) {
    this.homePageElementRepositoryProxy = homePageElementRepositoryProxy;
  }

  /**
   *
   * @param locale locale,.
   * @return list of the slider elements
   */
  public List<SliderResponse> getSlider(Locale locale, HomePageElementContext context) {
    return homePageElementRepositoryProxy.findByTypeAndContextAndLocaleOrderByPositionAsc(
      HomePageElementType.slider, context, locale).stream()
      .map(SliderResponse::new)
      .collect(Collectors.toList());
  }

  /**
   *
   * @param locale locale.
   * @return list of the topic elements.
   */
  public List<TopicResponse> getTopic(Locale locale, HomePageElementContext context) {
    return homePageElementRepositoryProxy.findByTypeAndContextAndLocaleOrderByPositionAsc(
      HomePageElementType.theme, context, locale).stream()
      .map(TopicResponse::new)
      .collect(Collectors.toList());
  }
}
