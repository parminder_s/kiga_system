package com.kiga.homepage.domain;

/**
 * Created by peter on 31.07.17.
 */
public enum HomePageElementContext {
  homepage("homepage"), shop("shop");
  private final String stringValue;
  private HomePageElementContext(final String stringValue) {
    this.stringValue = stringValue;
  }

  public String toString() {
    return stringValue;
  }

}
