package com.kiga.homepage.repository.abstraction;

import com.kiga.homepage.domain.HomePageElement;
import com.kiga.homepage.domain.HomePageElementContext;
import com.kiga.homepage.domain.HomePageElementType;
import com.kiga.main.locale.Locale;
import org.springframework.data.jpa.repository.EntityGraph;

import java.util.List;

/**
 * Created by peter on 30.08.16.
 */
public interface HomePageElementRepository {
  @EntityGraph(value = "homePagePreviewGetHomeLive", type = EntityGraph.EntityGraphType.LOAD)
  List<HomePageElement> findByPositionGreaterThanAndTypeAndContext(Integer position,
                                                         HomePageElementType type,
                                                         HomePageElementContext context);

  @EntityGraph(value = "homePagePreviewGetHomeLive", type = EntityGraph.EntityGraphType.LOAD)
  List<HomePageElement> findByTypeAndContextAndLocaleOrderByPositionAsc(HomePageElementType type,
                                                              HomePageElementContext context,
                                                              Locale locale);

  @EntityGraph(value = "homePagePreviewGetHomeLive", type = EntityGraph.EntityGraphType.LOAD)
  List<HomePageElement> findByContext(HomePageElementContext context);
}
