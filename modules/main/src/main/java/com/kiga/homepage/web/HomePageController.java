package com.kiga.homepage.web;

import com.kiga.homepage.service.HomePageFacade;
import com.kiga.homepage.web.responses.HomeResponse;
import com.kiga.web.message.EndpointRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/** Created by peter on 21.06.16. */
@RestController
@RequestMapping("/homepage")
public class HomePageController {

  private HomePageFacade homePageFacade;

  @Autowired
  public HomePageController(HomePageFacade homePageFacade) {
    this.homePageFacade = homePageFacade;
  }

  @Cacheable(value = "default", key = "'homepage-' + #endpointRequest.getLocale()")
  @RequestMapping(value = "/getHome", method = RequestMethod.POST)
  public HomeResponse getHome(@RequestBody EndpointRequest endpointRequest) {
    return homePageFacade.getHome(endpointRequest.getLocale());
  }
}
