package com.kiga.homepage.domain;

import com.kiga.db.KigaEntityModelWithoutId;
import com.kiga.db.LocaleToLongNotationConverter;
import com.kiga.main.locale.Locale;
import com.kiga.s3.domain.S3Image;
import javax.persistence.Convert;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** Created by peter on 30.08.16. */
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
@Data
public abstract class HomePageElement extends KigaEntityModelWithoutId {
  String url;

  // http://stackoverflow.com/questions/13539050/entitynotfoundexception-in-hibernate-many-to-one-mapping-however-data-exist
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "bigImageId", referencedColumnName = "ID")
  private S3Image big;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "smallImageId", referencedColumnName = "ID")
  private S3Image small;

  private Integer position;

  @Convert(converter = LocaleToLongNotationConverter.class)
  private Locale locale;

  private String title;

  private String titleShort;

  private HomePageElementType type;

  private HomePageElementContext context;

  private String color = "white";

  private String descriptionTitle;

  private String description;

  public HomePageElement() {
    this.setClassName("HomePagePreview");
  }
}
