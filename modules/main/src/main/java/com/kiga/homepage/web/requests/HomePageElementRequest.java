package com.kiga.homepage.web.requests;

import com.kiga.homepage.domain.draft.HomePageElementDraft;
import com.kiga.web.message.EndpointRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by peter on 21.06.16.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class HomePageElementRequest extends EndpointRequest {
  private HomePageElementDraft homePageElement;
}
