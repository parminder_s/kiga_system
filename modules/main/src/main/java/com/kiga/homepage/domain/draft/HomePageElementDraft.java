package com.kiga.homepage.domain.draft;

import com.kiga.homepage.domain.HomePageElement;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;

/**
 * Created by peter on 21.06.16.
 */
@Table(name = "HomePagePreview")
@Entity
@NamedEntityGraph(
  name = "homePagePreviewGetHomeDraft",
  attributeNodes = {
    @NamedAttributeNode(value = "big"),
    @NamedAttributeNode(value = "small")
  })
public class HomePageElementDraft extends HomePageElement {
  @Id
  @Column(name = "ID")
  @GeneratedValue
  private Long id;

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }
}
