package com.kiga.homepage;

import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.forum.service.NewestForumService;
import com.kiga.homepage.repository.HomePageElementRepositoryProxy;
import com.kiga.homepage.repository.draft.HomePageElementRepositoryDraft;
import com.kiga.homepage.repository.live.HomePageElementRepositoryLive;
import com.kiga.homepage.service.HomePageEditService;
import com.kiga.homepage.service.HomePageElementService;
import com.kiga.homepage.service.HomePageFacade;
import com.kiga.ideas.service.NewIdeasGroupService;
import com.kiga.upload.factory.UploadFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HomePageConfig {
  @Bean
  HomePageFacade homePageFacade(
      NewIdeasGroupService newIdeasGroupService,
      HomePageElementService homePageElementService,
      NewestForumService newestForumService) {
    return new HomePageFacade(newIdeasGroupService, homePageElementService, newestForumService);
  }

  @Bean
  HomePageElementService homePageElementService(
      HomePageElementRepositoryProxy homePageElementRepositoryProxy) {
    return new HomePageElementService(homePageElementRepositoryProxy);
  }

  @Bean
  HomePageElementRepositoryProxy homePageElementRepositoryProxy(
      HomePageElementRepositoryLive homePageElementRepositoryLive,
      HomePageElementRepositoryDraft homePageElementRepositoryDraft,
      RepositoryContext repositoryContext) {
    return new HomePageElementRepositoryProxy(
        homePageElementRepositoryLive, homePageElementRepositoryDraft, repositoryContext);
  }

  @Bean
  HomePageEditService homePageEditService(
      HomePageElementRepositoryLive homePageElementRepositoryLive,
      HomePageElementRepositoryDraft homePageElementRepositoryDraft,
      UploadFactory uploadFactory) {
    return new HomePageEditService(
        homePageElementRepositoryDraft, homePageElementRepositoryLive, uploadFactory);
  }
}
