package com.kiga.homepage.repository.live;

import com.kiga.homepage.domain.live.HomePageElementLive;
import com.kiga.homepage.repository.abstraction.HomePageElementRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by peter on 21.06.16.
 */
public interface HomePageElementRepositoryLive extends CrudRepository<HomePageElementLive, Long>,
  HomePageElementRepository {
}
