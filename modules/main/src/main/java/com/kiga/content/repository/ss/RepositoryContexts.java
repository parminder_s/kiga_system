package com.kiga.content.repository.ss;

import java.io.Serializable;

/**
 * @author bbs
 * @since 7/22/16.
 */
public enum RepositoryContexts implements Serializable {
  LIVE, DRAFT
}
