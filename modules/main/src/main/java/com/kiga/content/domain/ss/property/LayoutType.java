package com.kiga.content.domain.ss.property;

/**
 * Created by peter on 22.02.17.
 */
public enum LayoutType {
  STATIC_OLD,
  DYNAMIC,
  A4_PDF
}
