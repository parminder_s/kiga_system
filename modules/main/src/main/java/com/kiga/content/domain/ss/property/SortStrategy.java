package com.kiga.content.domain.ss.property;

/**
 * We are having three different SortingStrategie for IdeasGroup and IdeaGroupContainer.
 * <ul>
 *   <li>Inherited: Go up to the parent and check it's value</li>
 *   <li>Date: Go after the SortOrder property of KigaIdea</li>
 *   <li>Sequence: Go after the sort property in the m:n entity IdeaGroupToIdea</li>
 * </ul>
 */
public enum SortStrategy {
  INHERITED,
  DATE,
  SEQUENCE
}
