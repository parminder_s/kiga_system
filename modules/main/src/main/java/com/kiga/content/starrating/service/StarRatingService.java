package com.kiga.content.starrating.service;

import com.kiga.content.domain.ss.Rating;
import com.kiga.content.domain.ss.live.KigaPageLive;
import com.kiga.content.repository.ss.RatingRepository;
import com.kiga.content.repository.ss.live.KigaPageRepositoryLive;
import com.kiga.ideas.service.RatingCalculatorService;
import com.kiga.ideas.web.response.IdeasResponseRating;
import com.kiga.security.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import javax.transaction.Transactional;

/**
 * @author asv
 * @since 21/02/17.
 */
@Service
public class StarRatingService {
  private RatingRepository ratingRepository;
  private MemberRepository memberRepository;
  private KigaPageRepositoryLive kigaPageRepositoryLive;
  private RatingCalculatorService ratingCalculatorService;

  /**
   * Constructor.
   */
  @Autowired
  public StarRatingService(
    RatingRepository ratingRepository,
    MemberRepository memberRepository, KigaPageRepositoryLive kigaPageRepositoryLive,
    RatingCalculatorService ratingCalculatorService) {
    this.ratingRepository = ratingRepository;
    this.memberRepository = memberRepository;
    this.kigaPageRepositoryLive = kigaPageRepositoryLive;
    this.ratingCalculatorService = ratingCalculatorService;
  }


  /**
   * Get the KigaPage entity.
   */
  public KigaPageLive getKigaPage(Long id) {
    return kigaPageRepositoryLive.findOne(id);
  }

  /**
   * Set new rating[1-5] value.
   */
  private KigaPageLive updateRatingCount(int rating, KigaPageLive kigaPage, int diff) {
    switch (rating) {
      case 1:
        kigaPage.setRating1(kigaPage.getRating1() + diff);
        break;
      case 2:
        kigaPage.setRating2(kigaPage.getRating2() + diff);
        break;
      case 3:
        kigaPage.setRating3(kigaPage.getRating3() + diff);
        break;
      case 4:
        kigaPage.setRating4(kigaPage.getRating4() + diff);
        break;
      case 5:
        kigaPage.setRating5(kigaPage.getRating5() + diff);
        break;
      default:
        break;
    }
    return kigaPage;
  }

  /**
   * Set Idea user-rating.
   */
  @Transactional
  public IdeasResponseRating addRatingToKigaPage(
    Long id, int rating, long userId) {
    KigaPageLive kigaPage = getKigaPage(id);
    Rating userRating = ratingRepository.findByMemberIdAndKigaPageId(userId, id);
    if (userRating == null) {
      Rating newRating = new Rating(kigaPage, memberRepository.findOne(userId), rating);
      updateRatingCount(rating, kigaPage, 1);
      this.ratingRepository.save(newRating);
    }
    return ratingCalculatorService.getIdeaResponseRating(kigaPage);
  }

  /**
   * Remove Idea user-rating.
   */
  public IdeasResponseRating clearUserRating(Long id, long userId) {
    Rating userRating = ratingRepository.findByMemberIdAndKigaPageId(userId, id);
    KigaPageLive kigaPage = getKigaPage(id);
    updateRatingCount(userRating.getRatingValue(), kigaPage, -1);
    ratingRepository.delete(userRating);
    return ratingCalculatorService.getIdeaResponseRating(kigaPage);
  }

  /**
   * Get Idea rating.
   */
  public IdeasResponseRating getKigaPageRating(Long id, Long userId) {
    KigaPageLive kigaPage = getKigaPage(id);
    IdeasResponseRating returner = ratingCalculatorService.getIdeaResponseRating(kigaPage);
    Rating userRating = ratingRepository.findByMemberIdAndKigaPageId(userId, id);
    if (userRating != null) {
      returner.setUserRating(userRating.getRatingValue());
    }
    return returner;
  }
}
