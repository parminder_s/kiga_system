package com.kiga.content.repository.ss.live;

import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 14.08.16.
 */
public interface IdeaGroupContainerRepositoryLive extends
  IdeaGroupContainerRepository<IdeaGroupContainerLive>,
  CrudRepository<IdeaGroupContainerLive, Long> {
}
