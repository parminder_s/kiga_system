package com.kiga.content.repository.ss.live;

import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.main.locale.Locale;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by peter on 10.05.16.
 */
public interface SiteTreeLiveRepository extends
  CrudRepository<SiteTreeLive, java.lang.Long>, QueryDslPredicateExecutor<SiteTreeLive> {
  SiteTreeLive findTopByClassNameAndLocale(String className, Locale locale);

  List<SiteTreeLive> findByClassNameAndLocale(String className, Locale locale);

  List<SiteTreeLive> findByClassName(String className);
}
