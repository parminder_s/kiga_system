package com.kiga.content.domain.ss.draft;

import com.kiga.content.domain.ss.LinkedKigaPageGroup;
import com.kiga.db.KigaEntityModel;
import com.kiga.db.LocaleToLongNotationConverter;
import com.kiga.main.locale.Locale;

import java.util.List;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Created by rainerh on 22.10.16.
 */
@Entity
@Table(name = "KigaPageGroup")
public class LinkedKigaPageGroupDraft extends KigaEntityModel
  implements LinkedKigaPageGroup<LinkedKigaPageGroupDraft, KigaPageDraft> {
  private String title;

  private String description;

  @Convert(converter = LocaleToLongNotationConverter.class)
  private Locale locale;

  @ManyToMany
  @JoinTable(
    name = "KigaPageGroup_Articles",
    joinColumns = @JoinColumn(name = "KigaPageGroupID"),
    inverseJoinColumns = @JoinColumn(name = "ArticleID")
  )
  private List<KigaPageDraft> kigaPages;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "referenceGroupID", referencedColumnName = "id")
  private LinkedKigaPageGroupDraft referenceLinkedKigaPageGroup;

  @OneToMany(mappedBy = "referenceLinkedKigaPageGroup")
  List<LinkedKigaPageGroupDraft> linkedKigaPageGroupDrafts;

  public LinkedKigaPageGroupDraft() {
    this.setClassName("KigaPageGroup");
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setDescription(String description) {
    this.description = description;
  }

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  @Override
  public List<KigaPageDraft> getKigaPages() {
    return kigaPages;
  }

  @Override
  public void setKigaPages(List<KigaPageDraft> kigaPages) {
    this.kigaPages = kigaPages;
  }

  @Override
  public LinkedKigaPageGroupDraft getReferenceLinkedKigaPageGroup() {
    return referenceLinkedKigaPageGroup;
  }

  @Override
  public void setReferenceLinkedKigaPageGroup(
    LinkedKigaPageGroupDraft referenceLinkedKigaPageGroup) {
    this.referenceLinkedKigaPageGroup = referenceLinkedKigaPageGroup;
  }

  @Override
  public List<LinkedKigaPageGroupDraft> getLinkedKigaPageGroups() {
    return linkedKigaPageGroupDrafts;
  }

  @Override
  public void setLinkedKigaPageGroups(List<LinkedKigaPageGroupDraft> linkedKigaPageGroupDrafts) {
    this.linkedKigaPageGroupDrafts = linkedKigaPageGroupDrafts;
  }
}
