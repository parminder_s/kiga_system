package com.kiga.content.repository.ss.draft;

import com.kiga.content.domain.ss.draft.IdeaGroupDraft;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Evgeny Zhoga on 31.05.15.
 */
public interface IdeaGroupRepository
  extends QueryDslPredicateExecutor<IdeaGroupDraft>, CrudRepository<IdeaGroupDraft, Long> {
  List<IdeaGroupDraft> findAll();

  List<IdeaGroupDraft> findByParentId(Long parentId);

  List<IdeaGroupDraft> findByIdAndClassName(Long id, String className);
}
