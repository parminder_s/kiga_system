package com.kiga.content.domain.ss.draft;

import com.kiga.content.domain.ss.SiteTreeTranslationGroupsModel;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * Created by peter on 17.08.15.
 */

@Entity
@Table(name = "SiteTree_translationgroups")
public class SiteTreeTranslationGroups extends SiteTreeTranslationGroupsModel {
}
