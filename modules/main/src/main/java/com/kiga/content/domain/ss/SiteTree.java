package com.kiga.content.domain.ss;

import com.kiga.content.sorttree.spec.SortTree;
import com.kiga.db.KigaEntity;
import com.kiga.db.fixture.Entity;
import com.kiga.main.locale.Locale;
import java.util.Date;
import java.util.List;

/**
 * Created by rainerh on 11.03.16.
 *
 * <p>Basis interface for a SiteTree type which represents the core entity for the CMS module. We
 * are violating here against Interface segregation principle by extending KigaEntity and SortTree
 * since SiteTree should be a class. Because of the existence of an Live, Draft and Versions table
 * we had to implement as interface.
 */
public interface SiteTree<T extends SiteTree> extends KigaEntity, SortTree, Entity {
  String getCanPublishType();

  void setCanPublishType(String canPublishType);

  String getUrlSegment();

  void setUrlSegment(String urlSegment);

  String getTitle();

  void setTitle(String title);

  String getMenuTitle();

  void setMenuTitle(String menuTitle);

  String getContent();

  void setContent(String content);

  String getMetaTitle();

  void setMetaTitle(String metaTitle);

  String getMetaDescription();

  void setMetaDescription(String metaDescription);

  String getMetaKeywords();

  void setMetaKeywords(String metaKeywords);

  String getExtraMeta();

  void setExtraMeta(String extraMeta);

  Integer getShowInMenus();

  void setShowInMenus(Integer showInMenus);

  Integer getShowInSearch();

  void setShowInSearch(Integer showInSearch);

  String getHomePageForDomain();

  void setHomePageForDomain(String homePageForDomain);

  Integer getProvideComments();

  void setProvideComments(Integer provideComments);

  Integer getHasBrokenFile();

  void setHasBrokenFile(Integer hasBrokenFile);

  Integer getHasBrokenLink();

  void setHasBrokenLink(Integer hasBrokenLink);

  String getStatus();

  void setStatus(String status);

  String getReportClass();

  void setReportClass(String reportClass);

  String getCanViewType();

  void setCanViewType(String canViewType);

  String getCanEditType();

  void setCanEditType(String canEditType);

  String getToDo();

  void setToDo(String toDo);

  Integer getVersion();

  void setVersion(Integer version);

  String getPriority();

  void setPriority(String priority);

  Locale getLocale();

  void setLocale(Locale locale);

  Date getExpiryDate();

  void setExpiryDate(Date expiryDate);

  T getParent();

  void setParent(T parent);

  List<T> getChildren();

  void setChildren(List<T> children);
}
