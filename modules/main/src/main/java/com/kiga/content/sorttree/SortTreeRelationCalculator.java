package com.kiga.content.sorttree;

import com.kiga.ideas.task.sorttree.SortTreeOverflowException;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * Created by faxxe on 5/9/17.
 */
@Log4j2
@Service
class SortTreeRelationCalculator {

  static final String sortTreeInit = "AA";

  /**
   * calculate SortTree for right Sibling. Increments SortTree. Checks for Overflow.
   */
  String getSortTreeForRightSibling(String leftSibling) {
    char[] rightOf = leftSibling.toCharArray();
    boolean over;
    int index = rightOf.length - 1;

    do {
      if (rightOf[index] == 'Z') {
        rightOf[index] = 'A';
        over = true;
        index--;
      } else {
        rightOf[index]++;
        over = false;
      }
    }
    while (over);

    String sortTree = new String(rightOf);
    if (sortTree.matches(".*" + sortTreeInit + "$")) {
      throw new SortTreeOverflowException();
    } else {
      return sortTree;
    }
  }

  /**
   * Returns sort-tree path for the first child of the passed-in parent.
   */
  String getSortTreeForFirstChild(String parent) {
    if (parent == null) {
      parent = "";
    }
    return parent + sortTreeInit;
  }

}
