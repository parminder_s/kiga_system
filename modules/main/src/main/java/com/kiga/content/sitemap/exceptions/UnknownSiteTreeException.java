package com.kiga.content.sitemap.exceptions;

public class UnknownSiteTreeException extends RuntimeException {
  public UnknownSiteTreeException(String message) {
    super(message);
  }
}
