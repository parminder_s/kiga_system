package com.kiga.content.domain.ss;

import com.kiga.content.domain.ss.draft.KigaIdeaDraft;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

/**
 * Created by peter on 17.08.15.
 */
@MappedSuperclass
public class SiteTreeTranslationGroupsModel {
  @Id
  @GeneratedValue
  @Column(name = "ID")
  private Long id;

  @Column(name = "TranslationGroupID")
  private Long translationGroupId;

  @Column(name = "OriginalID")
  private Long originalId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getOriginalId() {
    return originalId;
  }

  public void setOriginalId(Long originalId) {
    this.originalId = originalId;
  }

  public Long getTranslationGroupId() {
    return translationGroupId;
  }

  public void setTranslationGroupId(Long translationGroupId) {
    this.translationGroupId = translationGroupId;
  }
}
