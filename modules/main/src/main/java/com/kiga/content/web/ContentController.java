package com.kiga.content.web;

import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.repository.ss.draft.SiteTreeRepository;
import com.kiga.content.repository.ss.live.IdeaGroupLiveRepository;
import com.kiga.content.sorttree.SortTreeManager;
import com.kiga.search.services.IndexerService;
import com.kiga.shop.web.requests.SingleIdRequest;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.stream.Collectors;

import javax.inject.Inject;

/**
 * Created by faxxe on 6/26/17.
 */
@RestController
@RequestMapping("/content")
@Log4j2
public class ContentController {

  private IndexerService indexer;
  private SiteTreeRepository siteTreeRepository;
  private IdeaGroupLiveRepository ideaGroupRepository;
  private SortTreeManager sortTreeManager;


  /**
   * Constructor.
   */
  @Inject
  public ContentController(
    IndexerService indexer,
    SiteTreeRepository siteTreeRepository,
    IdeaGroupLiveRepository ideaGroupRepository,
    SortTreeManager sortTreeManager) {
    this.indexer = indexer;
    this.siteTreeRepository = siteTreeRepository;
    this.ideaGroupRepository = ideaGroupRepository;
    this.sortTreeManager = sortTreeManager;
  }

  /**
   * called from Silverstripe when publishing an idea.
   */
  @RequestMapping("/publish")
  public String publishSiteTree(@RequestBody SingleIdRequest singleIdRequest) {
    log.info("publishing idea!" + singleIdRequest.getId());
    SiteTree siteTree = siteTreeRepository.findOne(singleIdRequest.getId());
    if ("Article".equals(siteTree.getClassName())) {
      indexer.indexIdeas(Collections.singletonList(singleIdRequest.getId()));
    }
    if ("IdeaGroup".equals(siteTree.getClassName())) {
      IdeaGroupLive ideaGroup = ideaGroupRepository.findOne(singleIdRequest.getId());
      indexer.indexIdeas(ideaGroup.getIdeaGroupToIdeaList().stream()
        .map(ideaGroupToIdeaLive -> ideaGroupToIdeaLive.getArticle().getId())
        .collect(Collectors.toList()));
    }
    sortTreeManager.recalculateSortTree();
    return "Published SiteTree Element with id: " + singleIdRequest.getId();
  }

  /**
   * called from Silverstripe when publishing an idea.
   */
  @RequestMapping("/unPublish")
  public String unPublishSiteTree(@RequestBody SingleIdRequest singleIdRequest) {
    log.info("unPublishing idea!" + singleIdRequest.getId());
    indexer.deleteIdeaFromIndex(singleIdRequest.getId());
    return "unPublished SiteTree Element with id: " + singleIdRequest.getId();
  }

}
