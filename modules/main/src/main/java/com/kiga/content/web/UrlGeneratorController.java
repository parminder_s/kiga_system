package com.kiga.content.web;

import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.content.web.response.UrlGeneratorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller to help in mapping SiteTree elements (by id) to URLs.
 * @author mfit
 */
@RestController
@RequestMapping("url-generator")
public class UrlGeneratorController {
  private SiteTreeUrlGenerator siteTreeUrlGenerator;
  private SiteTreeLiveRepository siteTreeLiveRepository;

  @Autowired
  public UrlGeneratorController(SiteTreeUrlGenerator siteTreeUrlGenerator,
                                SiteTreeLiveRepository siteTreeLiveRepository) {
    this.siteTreeUrlGenerator = siteTreeUrlGenerator;
    this.siteTreeLiveRepository = siteTreeLiveRepository;
  }

  /**
   * Create the URL for a category id.
   * Helps in redirecting old category-URLs to the new URLs.
   * @param categoryId the id of the site-tree category element to create the URL for.
   * @return the absolute url to the category page.
   */
  @RequestMapping("category/{categoryId}")
  public UrlGeneratorResponse getUrlForCategoryId(@PathVariable Long categoryId) {
    String url = "";
    SiteTree siteTreeCategory = this.siteTreeLiveRepository.findOne(categoryId);
    url = this.siteTreeUrlGenerator.getNgUrl(siteTreeCategory);
    String categorySegment = siteTreeCategory.getUrlSegment();

    return new UrlGeneratorResponse(url, categorySegment);
  }
}
