package com.kiga.content.domain.ss.draft;

import com.kiga.content.domain.ss.KigaCategory;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by rainerh on 27.06.16.
 */
@Entity
@Table(name = "KigaCategory")
public class KigaCategoryDraft extends PageDraft implements KigaCategory<SiteTreeDraft> {
  private String infoText;

  public KigaCategoryDraft() {
    this.setClassName("KigaCategory");
  }

  public String getInfoText() {
    return infoText;
  }

  public void setInfoText(String infoText) {
    this.infoText = infoText;
  }
}
