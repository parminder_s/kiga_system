package com.kiga.content.domain.ss.draft;

import com.kiga.content.domain.ss.SiteTreeModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/** Created by rainerh on 11.03.16. */
@Table(name = "SiteTree")
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class SiteTreeDraft extends SiteTreeModel<SiteTreeDraft> {
  @Id
  @Column(name = "ID")
  @GeneratedValue
  private Long id;

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }
}
