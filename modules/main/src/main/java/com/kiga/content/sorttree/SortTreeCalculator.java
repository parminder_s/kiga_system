package com.kiga.content.sorttree;

import com.kiga.content.sorttree.spec.SortTreeElement;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by rainerh on 29.03.17.
 * Modified by Florian Schneider on 07.11.17
 */
@Service
public class SortTreeCalculator {

  private SortTreeRelationCalculator sortTreeRelationCalculator;

  @Inject
  public SortTreeCalculator(SortTreeRelationCalculator sortTreeRelationCalculator) {
    this.sortTreeRelationCalculator = sortTreeRelationCalculator;
  }


  /**
   *  Updates the SortTree for the given tree. Uses parent's SortTree to calculate.
   */
  void updateBranch(SortTreeElement sortTreeElement) {

    String initialSortTree = sortTreeElement.getSortTree().getSortTree();
    if (initialSortTree == null || initialSortTree.isEmpty()) {
      initialSortTree = "";
      sortTreeElement.getSortTree().setSortTree(initialSortTree);
    }

    calculateSortTree(initialSortTree, sortTreeElement.getChildren());
  }

  /**
   *  Calculates the SortTree Recursively.
   */
  void calculateSortTree(String initialSortTree, List<SortTreeElement> sortTree) {
    // Sort branch according to getSort Value
    sortTree.sort(Comparator.comparingInt(item -> item.getSortTree().getSort()));

    sortTree
      .stream()
      .reduce(null, ((lastSortTree, currentSortTree) -> {
        if (lastSortTree == null) {
          currentSortTree.getSortTree().setSortTree(
            sortTreeRelationCalculator
              .getSortTreeForFirstChild(initialSortTree));
        } else {
          currentSortTree.getSortTree().setSortTree(
            sortTreeRelationCalculator
              .getSortTreeForRightSibling(lastSortTree.getSortTree().getSortTree()));
        }
        calculateSortTree(currentSortTree.getSortTree().getSortTree(),
          currentSortTree.getChildren());
        return currentSortTree;
      }));
  }
}
