package com.kiga.content;

import com.kiga.FeaturesProperties;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.KigaIdeaRepositoryProxy;
import com.kiga.content.repository.ss.NewIdeasRepositoryProxy;
import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepositoryProxy;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.content.repository.ss.draft.IdeaCategoryRepositoryDraft;
import com.kiga.content.repository.ss.draft.IdeaGroupContainerRepositoryDraft;
import com.kiga.content.repository.ss.draft.KigaIdeaRepositoryDraft;
import com.kiga.content.repository.ss.draft.NewIdeasRepositoryDraft;
import com.kiga.content.repository.ss.live.IdeaCategoryRepositoryLive;
import com.kiga.content.repository.ss.live.IdeaGroupContainerRepositoryLive;
import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive;
import com.kiga.content.repository.ss.live.NewIdeasRepositoryLive;
import com.kiga.content.service.KigaIdeaUrlGenerator;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.content.sitemap.service.SiteMapGenerator;
import com.kiga.web.service.UrlGenerator;
import javax.servlet.http.HttpServletRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class ContentConfig {

  @Bean
  public SiteTreeUrlGenerator getSiteTreeUrlGenerator(
      UrlGenerator urlGenerator, FeaturesProperties featuresProperties) {
    return new SiteTreeUrlGenerator(urlGenerator, featuresProperties.isAngularCmsEnabled());
  }

  @Bean
  public KigaIdeaUrlGenerator kigaIdeaUrlGenerator(
      SiteTreeUrlGenerator siteTreeUrlGenerator,
      IdeaGroupContainerRepositoryProxy ideaGroupContainerRepositoryProxy) {
    return new KigaIdeaUrlGenerator(siteTreeUrlGenerator, ideaGroupContainerRepositoryProxy);
  }

  @Bean
  public SiteMapGenerator siteMapGenerator(SiteTreeUrlGenerator siteTreeUrlGenerator) {
    return new SiteMapGenerator(siteTreeUrlGenerator);
  }

  @Bean
  public IdeaCategoryRepositoryProxy getIdeaCategoryModel(
      IdeaCategoryRepositoryDraft ideaCategoryRepositoryDraft,
      IdeaCategoryRepositoryLive ideaCategoryRepositoryLive,
      RepositoryContext repositoryContext) {
    return new IdeaCategoryRepositoryProxy(
        ideaCategoryRepositoryLive, ideaCategoryRepositoryDraft, repositoryContext);
  }

  @Bean
  public KigaIdeaRepositoryProxy getKigaIdeaModel(
      KigaIdeaRepositoryLive kigaIdeaRepositoryLive,
      KigaIdeaRepositoryDraft kigaIdeaRepositoryDraft,
      RepositoryContext repositoryContext) {
    return new KigaIdeaRepositoryProxy(
        kigaIdeaRepositoryLive, kigaIdeaRepositoryDraft, repositoryContext);
  }

  @Bean
  public NewIdeasRepositoryProxy getNewIdeaRepositoryModel(
      NewIdeasRepositoryLive newIdeasRepositoryLive,
      NewIdeasRepositoryDraft newIdeasRepositoryDraft,
      RepositoryContext repositoryContext) {
    return new NewIdeasRepositoryProxy(
        newIdeasRepositoryLive, newIdeasRepositoryDraft, repositoryContext);
  }

  @Bean
  public IdeaGroupContainerRepositoryProxy getIdeaGroupContainerRepositoryContext(
      IdeaGroupContainerRepositoryLive repositoryLive,
      IdeaGroupContainerRepositoryDraft repositoryDraft,
      RepositoryContext repositoryContext) {
    return new IdeaGroupContainerRepositoryProxy(
        repositoryLive, repositoryDraft, repositoryContext);
  }

  @Bean
  @Primary
  public NewIdeasRepository createAgeGroupIdeasModel(
      NewIdeasRepositoryLive ageGroupIdeasRepositoryLive,
      NewIdeasRepositoryDraft ageGroupIdeasRepositoryDraft,
      RepositoryContext repositoryContext) {
    return new NewIdeasRepositoryProxy(
        ageGroupIdeasRepositoryLive, ageGroupIdeasRepositoryDraft, repositoryContext);
  }

  @Bean
  public RepositoryContext createRepositoryContext(HttpServletRequest request) {
    return new RepositoryContext(request);
  }
}
