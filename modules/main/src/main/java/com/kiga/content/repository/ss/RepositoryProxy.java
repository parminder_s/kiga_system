package com.kiga.content.repository.ss;

import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 7/17/16.
 */
public interface RepositoryProxy<T> {
  T getRepository();

  CrudRepository getCrudRepository();

  RepositoryContexts getCurrentRepositoryContext();
}
