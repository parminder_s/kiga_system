package com.kiga.content.domain.ss;

/**
 * Created by rainerh on 12.08.16.
 */
public interface IdeaGroupContainer<T extends SiteTree> extends IdeaCategory<T> {
}
