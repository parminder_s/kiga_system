package com.kiga.content.domain.ss.draft;

import com.kiga.content.domain.ss.KigaPrintPreviewImage;
import com.kiga.db.KigaEntityModel;
import com.kiga.s3.domain.S3Image;

import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by peter on 27.04.16.
 */

@Entity
@Table(name = "KigaPrintPreviewImage")
public class KigaPrintPreviewImageDraft extends KigaEntityModel
  implements KigaPrintPreviewImage<KigaIdeaDraft> {
  @ManyToOne
  @JoinColumn(name = "s3ImageID", referencedColumnName = "id")
  private S3Image s3Image;

  @ManyToOne
  @JoinColumn(foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
  private KigaIdeaDraft kigaIdea;

  private int pageNumber;

  public KigaPrintPreviewImageDraft() {
    this.setClassName("KigaPrintPreviewImage");
  }

  public S3Image getS3Image() {
    return s3Image;
  }

  public void setS3Image(S3Image s3Image) {
    this.s3Image = s3Image;
  }

  public int getPageNumber() {
    return pageNumber;
  }

  public void setPageNumber(int pageNumber) {
    this.pageNumber = pageNumber;
  }

  public KigaIdeaDraft getKigaIdea() {
    return this.kigaIdea;
  }

  public void setKigaIdea(KigaIdeaDraft kigaIdea) {
    this.kigaIdea = kigaIdea;
  }
}
