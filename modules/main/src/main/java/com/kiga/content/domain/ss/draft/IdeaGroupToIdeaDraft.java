package com.kiga.content.domain.ss.draft;

import com.kiga.content.domain.ss.IdeaGroupToIdea;
import com.kiga.db.KigaEntityModel;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * Created by rainerh on 27.06.16.
 */
@Entity
@Table(name = "IdeaGroup_Articles")
public class IdeaGroupToIdeaDraft extends KigaEntityModel
  implements IdeaGroupToIdea<SiteTreeDraft, KigaIdeaDraft, IdeaGroupDraft> {
  private Integer sortOrder;
  private String sortTree;

  @ManyToOne
  @JoinColumn(name = "articleId", referencedColumnName = "id")
  @OrderBy("sortOrder ASC")
  @NotFound(action = NotFoundAction.IGNORE)
  @ForeignKey(name = "none")
  private KigaIdeaDraft article;

  @ManyToOne
  @JoinColumn(name = "ideaGroupId", referencedColumnName = "id")
  @ForeignKey(name = "none")
  @NotFound(action = NotFoundAction.IGNORE)
  private IdeaGroupDraft ideaGroup;

  public IdeaGroupToIdeaDraft() {
    this.setClassName("IdeaGroupToIdea");
  }

  public int getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(int sortOrder) {
    this.sortOrder = sortOrder;
  }

  public KigaIdeaDraft getArticle() {
    return article;
  }

  public void setArticle(KigaIdeaDraft article) {
    this.article = article;
  }

  public IdeaGroupDraft getIdeaGroup() {
    return ideaGroup;
  }

  public void setIdeaGroup(IdeaGroupDraft ideaGroup) {
    this.ideaGroup = ideaGroup;
  }

  @Override
  public String getSortTree() {
    return sortTree;
  }

  @Override
  public void setSortTree(String sortTreeValue) {
    this.sortTree = sortTreeValue;
  }

  @Override
  public int getSort() {
    return this.sortOrder;
  }

  @Override
  public void setSort(int sort) {
    this.sortOrder = sort;
  }
}
