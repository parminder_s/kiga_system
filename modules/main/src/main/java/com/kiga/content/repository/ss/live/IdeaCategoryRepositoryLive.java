package com.kiga.content.repository.ss.live;

import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.content.repository.ss.abstraction.IdeaCategoryRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 7/2/16.
 */
public interface IdeaCategoryRepositoryLive
  extends CrudRepository<IdeaCategoryLive, Long>, QueryDslPredicateExecutor<IdeaCategoryLive>,
  IdeaCategoryRepository {
}
