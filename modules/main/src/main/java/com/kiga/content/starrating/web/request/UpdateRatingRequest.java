package com.kiga.content.starrating.web.request;


import lombok.Data;

/**
 * @author asv
 * @since 27/09/16.
 */
@Data
public class UpdateRatingRequest {
  private long kigaPageId;
  private int userRating;
}
