package com.kiga.content.repository.ss.draft;

import com.kiga.content.domain.ss.draft.KigaCategoryDraft;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author bbs
 * @since 11/24/15.
 */
public interface KigaCategoryRepositoryDraft extends CrudRepository<KigaCategoryDraft, Long> {
  @Query("select kc from KigaCategoryDraft kc where kc.parent.className = 'MainDownloadCategory'"
    + " and kc.locale = :locale")
  List<KigaCategoryDraft> findTopCategories(@Param("locale") String locale);

  @Query("select kc from KigaCategoryDraft kc where kc.parent.urlSegment = :urlSegment and"
    + " kc.locale = :locale")
  List<KigaCategoryDraft> findSubCategories(@Param("locale") String locale, @Param("urlSegment")
    String urlSegment);

  @Query("select kc from KigaCategoryDraft kc where kc.urlSegment = :urlSegment "
    + "and kc.locale = :locale")
  KigaCategoryDraft findCategory(
    @Param("locale") String locale, @Param("urlSegment") String urlSegment);
}
