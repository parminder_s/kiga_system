package com.kiga.content.repository.ss.draft;

import com.kiga.content.domain.ss.draft.SiteTreeDraft;
import com.kiga.main.locale.Locale;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by robert on 22.04.17.
 */
public interface SiteTreeRepository extends CrudRepository<SiteTreeDraft, Long> {
  List<SiteTreeDraft> findAll();

  List<SiteTreeDraft> findByClassName(String className);

  List<SiteTreeDraft> findByLocale(String locale);

  List<SiteTreeDraft> findByClassNameAndLocale(String className, Locale locale);

  SiteTreeDraft findByLocaleAndUrlSegmentAndParentIdAndIdNot(Locale locale, String urlSegment,
    Long parentId, Long id);
}
