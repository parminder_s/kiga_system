package com.kiga.content.repository.ss.live;

import com.kiga.content.domain.ss.live.KigaCategoryLive;
import com.kiga.main.locale.Locale;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author bbs
 * @since 11/24/15.
 */
public interface KigaCategoryRepositoryLive extends CrudRepository<KigaCategoryLive, Long> {
  @Query("select kc from KigaCategoryLive kc where kc.parent.className = 'MainDownloadCategory' "
    + "and kc.locale = :locale")
  List<KigaCategoryLive> findTopDownloadsCategories(@Param("locale") Locale locale);

  @Query("select kc from KigaCategoryLive kc where kc.parent.className = 'MainArticleCategory' "
    + "and kc.locale = :locale")
  List<KigaCategoryLive> findTopIdeasCategories(@Param("locale") Locale locale);

  @Query("select kc from KigaCategoryLive kc where kc.parent.urlSegment = :urlSegment and kc"
    + ".locale = :locale")
  List<KigaCategoryLive> findSubCategories(@Param("locale") Locale locale, @Param("urlSegment")
    String urlSegment);

  @Query("select kc from KigaCategoryLive kc where kc.urlSegment = :urlSegment and kc.locale = "
    + ":locale")
  KigaCategoryLive findCategory(
    @Param("locale") Locale locale, @Param("urlSegment") String urlSegment
  );

  KigaCategoryLive findByIdAndClassName(Long id, String className);
}
