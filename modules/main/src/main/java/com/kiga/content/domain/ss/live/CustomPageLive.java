package com.kiga.content.domain.ss.live;

import com.kiga.content.domain.ss.CustomPage;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by rainerh on 22.08.16.
 */
@Entity
@Table(name = "CustomPage_Live")
public class CustomPageLive extends PageLive implements CustomPage<SiteTreeLive> {
  private String customCode;

  public CustomPageLive() {
    this.setClassName("CustomPage");
  }

  public String getCustomCode() {
    return customCode;
  }

  public void setCustomCode(String customCode) {
    this.customCode = customCode;
  }
}
