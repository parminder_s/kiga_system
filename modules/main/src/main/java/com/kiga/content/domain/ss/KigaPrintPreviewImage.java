package com.kiga.content.domain.ss;

import com.kiga.db.KigaEntity;
import com.kiga.s3.domain.S3Image;

/**
 * Created by peter on 27.04.16.
 */

public interface KigaPrintPreviewImage<T extends KigaIdea> extends KigaEntity {
  S3Image getS3Image();

  void setS3Image(S3Image s3Image);

  T getKigaIdea();

  void setKigaIdea(T kigaIdea);

  int getPageNumber();

  void setPageNumber(int pageNumber);
}
