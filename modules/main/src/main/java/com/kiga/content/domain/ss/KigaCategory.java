package com.kiga.content.domain.ss;

/**
 * Created by rainerh on 27.06.16.
 */
public interface KigaCategory<T extends SiteTree> extends Page<T> {
  String getInfoText();

  void setInfoText(String infoText);
}
