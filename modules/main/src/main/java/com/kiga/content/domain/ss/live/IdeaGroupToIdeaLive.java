package com.kiga.content.domain.ss.live;

import com.kiga.content.domain.ss.IdeaGroupToIdea;
import com.kiga.db.KigaEntityModel;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;


import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * Created by rainerh on 27.06.16.
 */
@Entity
@Table(name = "IdeaGroup_Articles")
public class IdeaGroupToIdeaLive extends KigaEntityModel
  implements IdeaGroupToIdea<SiteTreeLive, KigaIdeaLive, IdeaGroupLive> {
  private int sortOrder;
  private String sortTree;

  @ManyToOne
  @JoinColumn(name = "articleId", referencedColumnName = "id")
  @OrderBy("sortOrder ASC")
  @NotFound(action = NotFoundAction.IGNORE)
  private KigaIdeaLive article;

  @ManyToOne
  @JoinColumn(name = "ideaGroupId", referencedColumnName = "id")
  @NotFound(action = NotFoundAction.IGNORE)
  private IdeaGroupLive ideaGroup;

  public IdeaGroupToIdeaLive() {
    this.setClassName("IdeaGroupToIdea");
  }

  public int getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(int sortOrder) {
    this.sortOrder = sortOrder;
  }

  public KigaIdeaLive getArticle() {
    return article;
  }

  public void setArticle(KigaIdeaLive article) {
    this.article = article;
  }

  public IdeaGroupLive getIdeaGroup() {
    return ideaGroup;
  }

  public void setIdeaGroup(IdeaGroupLive ideaGroup) {
    this.ideaGroup = ideaGroup;
  }

  @Override
  public String getSortTree() {
    return sortTree;
  }

  @Override
  public void setSortTree(String sortTreeValue) {
    this.sortTree = sortTreeValue;
  }

  @Override
  public int getSort() {
    return this.sortOrder;
  }

  @Override
  public void setSort(int sort) {
    this.sortOrder = sort;
  }
}
