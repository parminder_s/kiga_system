package com.kiga.content.service;

import static java.util.Arrays.asList;

import com.kiga.content.domain.ss.SiteTree;
import com.kiga.web.service.UrlGenerator;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Function;
import javax.persistence.EntityNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

@Log4j2
public class SiteTreeUrlGenerator {
  private UrlGenerator urlGenerator;
  private boolean angularCmsEnabled;

  public SiteTreeUrlGenerator(UrlGenerator urlGenerator, boolean angularCmsEnabled) {
    this.urlGenerator = urlGenerator;
    this.angularCmsEnabled = angularCmsEnabled;
  }

  /** returns full url part, like /kategorie/bereich/idea-1, if siteTree is idea-1 */
  public String getUrl(SiteTree siteTree) {
    try {
      if (siteTree.getClassName().equals("HomePage")) {
        return urlGenerator.getUrl(siteTree.getUrlSegment());
      } else {
        String returner = getUrl(siteTree.getParent());
        if (!siteTree.getUrlSegment().startsWith("/")) {
          returner += "/";
        }
        return returner + siteTree.getUrlSegment();
      }
    } catch (EntityNotFoundException | NullPointerException enfe) {
      log.error("error creating url for " + siteTree.getId(), enfe);
      return "";
    }
  }

  /** returns the url for a siteTree element to be rendered via the cms module. */
  public String getNgUrl(SiteTree siteTree) {

    if (angularCmsEnabled
        && asList("IdeaGroupContainer", "IdeaGroupCategory", "IdeaGroup", "IdeaGroupMainContainer")
            .contains(siteTree.getClassName())) {
      return traverseForNgUrl(siteTree, true);
    } else {
      return traverseForNgUrl(siteTree, false);
    }
  }

  /** In contrast to getUrl which returns absolute, this returns a relative one. */
  public String getRelativeUrl(SiteTree siteTree) {
    return this.getRelativeUrlWith(siteTree, this::getUrl);
  }

  /** In contrast to getUrl which returns absolute, this returns a relative one. */
  public String getRelativeNgUrl(SiteTree siteTree) {
    return this.getRelativeUrlWith(siteTree, this::getNgUrl);
  }

  private String getRelativeUrlWith(SiteTree siteTree, Function<SiteTree, String> urlProvider) {
    String url = urlProvider.apply(siteTree);
    try {
      URL urlObject = new URL(url);
      String returner = urlObject.getFile();
      if (!StringUtils.isEmpty(urlObject.getRef())) {
        returner += "#" + urlObject.getRef();
      }
      return returner;
    } catch (MalformedURLException mue) {
      return url;
    }
  }

  private String traverseForNgUrl(SiteTree siteTree, boolean angularCms) {
    String className = siteTree.getClassName();
    try {
      if ("HomePage".equals(className)) {
        if (angularCms) {
          return urlGenerator.getNgUrl("ng6/" + siteTree.getLocale().getValue());
        } else {
          return urlGenerator.getNgUrl(siteTree.getLocale().getValue() + "/cms");
        }

      } else {
        String returner = traverseForNgUrl(siteTree.getParent(), angularCms);
        if (!siteTree.getUrlSegment().startsWith("/")) {
          returner += "/";
        }
        return returner + siteTree.getUrlSegment();
      }
    } catch (EntityNotFoundException | NullPointerException exception) {
      log.error("error in in creating the url for " + siteTree.getId(), exception);
      return "";
    }
  }
}
