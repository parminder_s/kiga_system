package com.kiga.content.domain.ss.live;

import com.kiga.content.domain.ss.IdeaGroup;
import com.kiga.content.domain.ss.property.SortStrategy;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Created by rainerh on 27.06.16.
 */
@Entity
@Table(name = "IdeaGroup_Live")
public class IdeaGroupLive extends IdeaCategoryLive
  implements IdeaGroup<SiteTreeLive, IdeaGroupToIdeaLive> {
  @OneToMany(mappedBy = "ideaGroup")
  private List<IdeaGroupToIdeaLive> ideaGroupToIdeaList;

  public IdeaGroupLive() {
    this.setClassName("IdeaGroup");
  }

  @Override
  public List<IdeaGroupToIdeaLive> getIdeaGroupToIdeaList() {
    return ideaGroupToIdeaList;
  }

  @Override
  public void setIdeaGroupToIdeaList(List<IdeaGroupToIdeaLive> ideaGroupToIdeaList) {
    this.ideaGroupToIdeaList = ideaGroupToIdeaList;
  }
}
