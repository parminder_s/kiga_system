package com.kiga.content.domain.ss.live;

import com.kiga.content.domain.ss.KigaCategory;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by rainerh on 27.06.16.
 */
@Entity
@Table(name = "KigaCategory_Live")
public class KigaCategoryLive extends PageLive implements KigaCategory<SiteTreeLive> {
  private String infoText;

  public KigaCategoryLive() {
    this.setClassName("KigaCategory");
  }

  public String getInfoText() {
    return infoText;
  }

  public void setInfoText(String infoText) {
    this.infoText = infoText;
  }
}
