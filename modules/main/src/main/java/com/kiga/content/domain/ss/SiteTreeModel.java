package com.kiga.content.domain.ss;

import com.kiga.db.KigaEntityModelWithoutId;
import com.kiga.db.LocaleToLongNotationConverter;
import com.kiga.main.locale.Locale;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

/**
 * Created by rainerh on 12.03.16.
 */
@MappedSuperclass
public abstract class SiteTreeModel<T extends SiteTree> extends KigaEntityModelWithoutId
  implements SiteTree<T> {
  private String urlSegment;
  private String title;
  private String menuTitle;
  @Column(columnDefinition = "mediumtext")
  private String content;
  private String metaTitle;
  private String metaDescription;
  private String metaKeywords;
  private String extraMeta;
  private int showInMenus = 1;
  private int showInSearch = 1;
  private String homePageForDomain;
  private int provideComments = 1;
  private int sort = 0;
  private String sortTree = "";
  private int hasBrokenFile = 0;
  private int hasBrokenLink = 0;
  private String status;
  private String reportClass;
  private String canViewType;
  private String canEditType;
  private String toDo;
  private Integer version;
  private String priority;
  @Convert(converter = LocaleToLongNotationConverter.class)
  private Locale locale;
  private Date expiryDate;
  private String canPublishType;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "parentID", referencedColumnName = "id")
  @NotFound(action = NotFoundAction.IGNORE)
  private T parent;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
  @OrderBy("sort")
  private List<T> children = new ArrayList<>();

  public SiteTreeModel() {
    this.setClassName("SiteTree");
  }

  @Override
  public String getUrlSegment() {
    return urlSegment;
  }

  @Override
  public void setUrlSegment(String urlSegment) {
    this.urlSegment = urlSegment;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public String getMenuTitle() {
    return menuTitle;
  }

  @Override
  public void setMenuTitle(String menuTitle) {
    this.menuTitle = menuTitle;
  }

  @Override
  public String getContent() {
    return content;
  }

  @Override
  public void setContent(String content) {
    this.content = content;
  }

  @Override
  public String getMetaTitle() {
    return metaTitle;
  }

  @Override
  public void setMetaTitle(String metaTitle) {
    this.metaTitle = metaTitle;
  }

  @Override
  public String getMetaDescription() {
    return metaDescription;
  }

  @Override
  public void setMetaDescription(String metaDescription) {
    this.metaDescription = metaDescription;
  }

  @Override
  public String getMetaKeywords() {
    return metaKeywords;
  }

  @Override
  public void setMetaKeywords(String metaKeywords) {
    this.metaKeywords = metaKeywords;
  }

  @Override
  public String getExtraMeta() {
    return extraMeta;
  }

  @Override
  public void setExtraMeta(String extraMeta) {
    this.extraMeta = extraMeta;
  }

  @Override
  public Integer getShowInMenus() {
    return showInMenus;
  }

  @Override
  public void setShowInMenus(Integer showInMenus) {
    this.showInMenus = showInMenus;
  }

  @Override
  public Integer getShowInSearch() {
    return showInSearch;
  }

  @Override
  public void setShowInSearch(Integer showInSearch) {
    this.showInSearch = showInSearch;
  }

  @Override
  public String getHomePageForDomain() {
    return homePageForDomain;
  }

  @Override
  public void setHomePageForDomain(String homePageForDomain) {
    this.homePageForDomain = homePageForDomain;
  }

  @Override
  public Integer getProvideComments() {
    return provideComments;
  }

  @Override
  public void setProvideComments(Integer provideComments) {
    this.provideComments = provideComments;
  }

  @Override
  public int getSort() {
    return sort;
  }

  @Override
  public void setSort(int sort) {
    this.sort = sort;
  }

  @Override
  public Integer getHasBrokenFile() {
    return hasBrokenFile;
  }

  @Override
  public void setHasBrokenFile(Integer hasBrokenFile) {
    this.hasBrokenFile = hasBrokenFile;
  }

  @Override
  public Integer getHasBrokenLink() {
    return hasBrokenLink;
  }

  @Override
  public void setHasBrokenLink(Integer hasBrokenLink) {
    this.hasBrokenLink = hasBrokenLink;
  }

  @Override
  public String getStatus() {
    return status;
  }

  @Override
  public void setStatus(String status) {
    this.status = status;
  }

  @Override
  public String getReportClass() {
    return reportClass;
  }

  @Override
  public void setReportClass(String reportClass) {
    this.reportClass = reportClass;
  }

  @Override
  public String getCanViewType() {
    return canViewType;
  }

  @Override
  public void setCanViewType(String canViewType) {
    this.canViewType = canViewType;
  }

  @Override
  public String getCanEditType() {
    return canEditType;
  }

  @Override
  public void setCanEditType(String canEditType) {
    this.canEditType = canEditType;
  }

  @Override
  public String getToDo() {
    return toDo;
  }

  @Override
  public void setToDo(String toDo) {
    this.toDo = toDo;
  }

  @Override
  public Integer getVersion() {
    return version;
  }

  @Override
  public void setVersion(Integer version) {
    this.version = version;
  }

  @Override
  public String getPriority() {
    return priority;
  }

  @Override
  public void setPriority(String priority) {
    this.priority = priority;
  }

  @Override
  public Locale getLocale() {
    return locale;
  }

  @Override
  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  @Override
  public Date getExpiryDate() {
    return expiryDate;
  }

  @Override
  public void setExpiryDate(Date expiryDate) {
    this.expiryDate = expiryDate;
  }

  @Override
  public String getCanPublishType() {
    return canPublishType;
  }

  @Override
  public void setCanPublishType(String canPublishType) {
    this.canPublishType = canPublishType;
  }

  @Override
  public T getParent() {
    return parent;
  }

  @Override
  public void setParent(T parent) {
    this.parent = parent;
  }

  @Override
  public List<T> getChildren() {
    return children;
  }

  @Override
  public void setChildren(List<T> children) {
    this.children = children;
  }

  @Override
  public String getSortTree() {
    return sortTree;
  }

  @Override
  public void setSortTree(String sortTreeValue) {
    this.sortTree = sortTreeValue;
  }
}
