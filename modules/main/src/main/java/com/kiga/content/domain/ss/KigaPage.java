package com.kiga.content.domain.ss;

import com.kiga.s3.domain.KigaS3File;

import java.util.List;

/**
 * Created by rainerh on 26.06.16.
 */
public interface KigaPage<T extends SiteTree, G extends LinkedKigaPageGroup> extends Page<T> {
  String getDescription();

  void setDescription(String description);

  String getOldId();

  void setOldId(String oldId);

  Integer getInPool();

  void setInPool(Integer inPool);

  String getTagsSummarized();

  void setTagsSummarized(String tagsSummarized);

  float getRatingComplete();

  void setRatingComplete(float ratingComplete);

  int getRating1();

  void setRating1(int rating1);

  int getRating2();

  void setRating2(int rating2);

  int getRating3();

  void setRating3(int rating3);

  int getRating4();

  void setRating4(int rating4);

  int getRating5();

  void setRating5(int rating5);

  Integer getDirty();

  void setDirty(Integer dirty);

  String getTagsText();

  void setTagsText(String tagsText);

  Integer getSortMonth();

  void setSortMonth(Integer sortMonth);

  Integer getSortYear();

  void setSortYear(Integer sortYear);

  Integer getSortIndex();

  void setSortIndex(Integer sortIndex);

  Integer getSortMonthMini();

  void setSortMonthMini(Integer sortMonthMini);

  Integer getSortYearMini();

  void setSortYearMini(Integer sortYearMini);

  Integer getSortIndexMini();

  void setSortIndexMini(Integer sortIndexMini);

  Integer getSortDayMini();

  void setSortDayMini(Integer sortDayMini);

  Integer getPdfFileId();

  void setPdfFileId(Integer pdfFileId);

  String getAuthor();

  void setAuthor(String author);

  Integer getScreenshotId();

  void setScreenshotId(Integer screenshotId);

  Integer getFacebookShare();

  void setFacebookShare(Integer facebookShare);

  Integer getAutomaticMetaDescription();

  void setAutomaticMetaDescription(Integer automaticMetaDescription);

  Integer getForParents();

  void setForParents(Integer forParents);

  KigaS3File getPdfWoImage();

  void setPdfWoImage(KigaS3File pdfWoImage);

  KigaPageImage getPreviewImage();

  void setPreviewImage(KigaPageImage previewImage);

  List<KigaPageImage> getKigaPageImages();

  void setKigaPageImages(List<KigaPageImage> kigaPageImages);

  List<Rating> getRatings();

  void setRatings(List<Rating> ratings);

  List<G> getLinkedKigaPageGroups();

  void setLinkedKigaPageGroups(List<G> linkedKigaPageGroups);
}
