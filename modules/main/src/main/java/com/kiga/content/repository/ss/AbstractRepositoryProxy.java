package com.kiga.content.repository.ss;

import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 7/17/16.
 */
public class AbstractRepositoryProxy<T> implements RepositoryProxy<T> {
  private T liveRepository;
  private T draftRepository;
  private RepositoryContext repositoryContext;

  /**
   * Constructor for the Model.
   *
   * @param liveRepository    live repository
   * @param draftRepository   draft repository
   * @param repositoryContext repository context holder
   */
  public AbstractRepositoryProxy(T liveRepository, T draftRepository,
    RepositoryContext repositoryContext) {
    this.liveRepository = liveRepository;
    this.draftRepository = draftRepository;
    this.repositoryContext = repositoryContext;
  }

  @Override
  public T getRepository() {
    if (repositoryContext.getContext().equals(RepositoryContexts.DRAFT)) {
      return draftRepository;
    } else {
      return liveRepository;
    }
  }

  @Override
  public CrudRepository getCrudRepository() {
    return (CrudRepository) getRepository();
  }

  @Override
  public RepositoryContexts getCurrentRepositoryContext() {
    return repositoryContext.getContext();
  }
}
