package com.kiga.content.repository.ss.abstraction;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.main.locale.Locale;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

/**
 * @author bbs
 * @since 7/17/16.
 */
public interface IdeaCategoryRepository {
  IdeaCategory findOne(long id);

  IdeaCategory findByIdAndClassName(Long categoryId, String className);

  List<IdeaCategory> findByParentIdOrderBySortAsc(Long parentId);

  List<IdeaCategory> findByLocale(Locale locale);

  List<IdeaCategory> findById(Long id);

  List<IdeaCategory> findByIdIn(Collection<Long> ids);

  List<IdeaCategory> findByClassName(String className);

  IdeaCategory findByCodeAndLocale(String code, Locale locale);

  @Query("select ic from #{#entityName} ic where ic.parent.className = 'MainArticleCategory' and "
    + "Locale = :locale")
  List<IdeaCategory> findLevel1(@Param("locale") Locale locale);

  IdeaCategory findByLocaleAndUrlSegment(Locale locale, String urlSegment);

  @Query("FROM #{#entityName} g left join g.parent p1 left join p1.parent p2 left join p2.parent "
    + "p3 WHERE g.locale = :locale and g.urlSegment = :urlSegment and 'IdeaGroupMainContainer' = "
    + "(CASE WHEN :level = 0 THEN g.urlSegment WHEN :level = 1 THEN p1"
    + ".urlSegment WHEN :level = 2"
    + " THEN p2.urlSegment WHEN :level = 3 THEN p3.urlSegment ELSE '' END) ")
  IdeaCategory findByLocaleAndUrlSegment(@Param("locale") Locale locale,
    @Param("urlSegment") String urlSegment, @Param("level") Integer level);

  @Query("FROM #{#entityName} g WHERE g.sortTree LIKE CONCAT(:sortTree,'__') and g.locale = "
    + ":locale ORDER BY g.sort asc")
  List<IdeaCategory> findBySortTreeStartsWithAndLocaleLikeOrderBySortAsc(
    @Param("sortTree") String sortTree, @Param("locale") Locale locale);
}
