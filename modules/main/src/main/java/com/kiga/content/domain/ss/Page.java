package com.kiga.content.domain.ss;

/**
 * Created by rainerh on 16.06.16.
 */
public interface Page<T extends SiteTree> extends SiteTree<T> {
  Integer getLegacyId();

  void setLegacyId(Integer legacyId);

  String getSubTitle();

  void setSubTitle(String subTitle);

  Integer getNoRobotIndex();

  void setNoRobotIndex(Integer noRobotIndex);

  Integer getInSync();

  void setInSync(Integer inSync);

  Integer getOutOfSync();

  void setOutOfSync(Integer outOfSync);

  String getCode();

  void setCode(String code);
}
