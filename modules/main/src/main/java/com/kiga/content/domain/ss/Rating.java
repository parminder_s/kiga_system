package com.kiga.content.domain.ss;

import com.kiga.content.domain.ss.live.KigaPageLive;
import com.kiga.db.KigaEntityModel;
import com.kiga.security.domain.Member;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by faxxe on 8/16/16.
 *
 * <p>We are linking here directly to KigaPageLive, since Ratings can
 * only be done against live version.</p>
 */

@Entity
public class Rating extends KigaEntityModel {

  @ManyToOne()
  @NotFound(action = NotFoundAction.IGNORE)
  private KigaPageLive kigaPage;

  @ManyToOne
  @JoinColumn(name = "ownerId")
  private Member member;

  private int ratingValue;

  public Rating() {
    this.setClassName("Rating");
  }


  /**
   * Ctor.
   */
  public Rating(KigaPageLive kigaPage, Member member, int ratingValue) {
    this.kigaPage = kigaPage;
    this.member = member;
    this.ratingValue = ratingValue;
    this.setClassName("Rating");
  }

  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public KigaPageLive getKigaPage() {
    return kigaPage;
  }

  public void setKigaPage(KigaPageLive kigaPage) {
    this.kigaPage = kigaPage;
  }

  public int getRatingValue() {
    return ratingValue;
  }

  public void setRatingValue(int ratingValue) {
    this.ratingValue = ratingValue;
  }

}
