package com.kiga.content.domain.ss.live;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.property.SortStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by rainerh on 27.06.16.
 */
@Entity
@Table(name = "ArticleCategory_Live")
public class IdeaCategoryLive extends KigaCategoryLive implements IdeaCategory<SiteTreeLive> {
  private Integer homepageCol;
  private Integer position;
  private String iconCode;
  private SortStrategy sortStrategy = SortStrategy.INHERITED;
  private boolean disableForFavourites;

  public IdeaCategoryLive() {
    this.setClassName("ArticleCategory");
  }

  public Integer getHomepageCol() {
    return homepageCol;
  }

  public void setHomepageCol(Integer homepageCol) {
    this.homepageCol = homepageCol;
  }

  public Integer getPosition() {
    return position;
  }

  public void setPosition(Integer position) {
    this.position = position;
  }

  public String getIconCode() {
    return iconCode;
  }

  public void setIconCode(String iconCode) {
    this.iconCode = iconCode;
  }

  public SortStrategy getSortStrategy() {
    return sortStrategy;
  }

  public void setSortStrategy(SortStrategy sortStrategy) {
    this.sortStrategy = sortStrategy;
  }

  @Override
  public boolean isDisableForFavourites() {
    return disableForFavourites;
  }

  @Override
  public void setDisableForFavourites(boolean disableForFavourites) {
    this.disableForFavourites = disableForFavourites;
  }
}

