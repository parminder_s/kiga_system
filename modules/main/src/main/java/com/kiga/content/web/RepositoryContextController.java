package com.kiga.content.web;

import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.web.response.ToggleContextResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author bbs
 * @since 7/23/16.
 */
@RestController
@RequestMapping("content/repository-context")
public class RepositoryContextController {
  private RepositoryContext repositoryContext;

  @Autowired
  public RepositoryContextController(RepositoryContext repositoryContext) {
    this.repositoryContext = repositoryContext;
  }

  @RequestMapping("toggle")
  public ToggleContextResponse toggleContext() {
    repositoryContext.toggleContext();
    return new ToggleContextResponse(repositoryContext.getContext());
  }

  @RequestMapping("current")
  public ToggleContextResponse currentContext() {
    return new ToggleContextResponse(repositoryContext.getContext());
  }
}
