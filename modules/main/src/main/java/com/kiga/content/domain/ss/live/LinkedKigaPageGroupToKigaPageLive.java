package com.kiga.content.domain.ss.live;

import com.kiga.content.domain.ss.LinkedKigaPageGroupToKigaPage;
import com.kiga.db.KigaEntityModel;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by rainerh on 23.10.16.
 */
@Entity
@Table(name = "KigaPageGroup_Articles")
public class LinkedKigaPageGroupToKigaPageLive extends KigaEntityModel
  implements LinkedKigaPageGroupToKigaPage<LinkedKigaPageGroupLive, KigaPageLive> {

  @ManyToOne
  @JoinColumn(name = "articleId",
    foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
  private KigaPageLive kigaPage;

  @ManyToOne
  @JoinColumn(name = "kigaPageGroupId")
  private LinkedKigaPageGroupLive linkedKigaPageGroup;

  public LinkedKigaPageGroupToKigaPageLive() {
    this.setClassName("KigaPageGroup_Articles");
  }

  public KigaPageLive getKigaPage() {
    return kigaPage;
  }

  public void setKigaPage(KigaPageLive kigaPage) {
    this.kigaPage = kigaPage;
  }

  public LinkedKigaPageGroupLive getLinkedKigaPageGroup() {
    return linkedKigaPageGroup;
  }

  public void setLinkedKigaPageGroup(LinkedKigaPageGroupLive linkedKigaPageGroup) {
    this.linkedKigaPageGroup = linkedKigaPageGroup;
  }
}
