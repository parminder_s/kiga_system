package com.kiga.content.web.response;

import com.kiga.content.repository.ss.RepositoryContexts;
import lombok.Data;

import java.io.Serializable;

/**
 * @author bbs
 * @since 7/23/16.
 */
@Data
public class ToggleContextResponse {
  private RepositoryContexts repositoryContexts;

  public ToggleContextResponse() {
  }

  public ToggleContextResponse(RepositoryContexts repositoryContexts) {
    this.repositoryContexts = repositoryContexts;
  }
}
