package com.kiga.content.sitemap;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.sitemap.service.SiteMapGenerator;
import com.kiga.s3.S3Properties;
import com.kiga.spec.Job;
import com.kiga.spec.Jobable;
import lombok.extern.log4j.Log4j2;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import java.util.Date;

/**
 * Created by peter on 10.05.16.
 */
@Log4j2
@Job(cronProperty = "siteMapJob")
public class SiteMapJob implements Jobable {

  public static String SITEMAP_KEY = "sitemap.xml";
  private AmazonS3Client amazonS3Client;
  private SiteMapGenerator siteMapGenerator;
  private SiteTreeLiveRepository siteTreeLiveRepository;
  private S3Properties s3Properties;

  /**
   * constructor.
   */
  public SiteMapJob(S3Properties s3Properties, SiteMapGenerator siteMapGenerator,
                    SiteTreeLiveRepository siteTreeRepository,
                    AmazonS3Client amazonS3Client) {
    this.siteTreeLiveRepository = siteTreeRepository;
    this.siteMapGenerator = siteMapGenerator;
    this.s3Properties = s3Properties;
    this.amazonS3Client = amazonS3Client;
  }

  @Override
  public void runJob() {

    log.info("Started Creation of SiteMap");
    String siteMapXml = siteMapGenerator.getSiteMap(siteTreeLiveRepository.findAll());
    log.info("finished SiteMap XML");
    InputStream stream = new ByteArrayInputStream(siteMapXml.getBytes(StandardCharsets.UTF_8));
    ObjectMetadata metadata = new ObjectMetadata();
    metadata.setLastModified(new Date());
    String bucketName = s3Properties.getBucketName();
    log.info("Start Uploading SiteMap to S3, bucket: " + bucketName);
    amazonS3Client.putObject(bucketName, SITEMAP_KEY, stream, metadata);
    log.info("Finished Uploading SiteMap");
  }
}
