package com.kiga.content.web;

import com.kiga.idea.member.provider.MemberIdeaCategoryService;
import com.kiga.ideas.web.response.IdeaCategoryHierarchyViewModel;
import com.kiga.main.locale.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author bbs
 * @since 4/13/17.
 */
@RestController
@RequestMapping("idea-category")
public class IdeaCategoryController {
  private MemberIdeaCategoryService memberIdeaCategoryService;

  @Autowired
  public IdeaCategoryController(MemberIdeaCategoryService memberIdeaCategoryService) {
    this.memberIdeaCategoryService = memberIdeaCategoryService;
  }

  /**
   * Get categories based on locale.
   *
   * @param localeString locale of categories as string
   * @return categories for given locale
   */
  @RequestMapping("list/{locale}")
  public List<IdeaCategoryHierarchyViewModel> getCategories(
    @PathVariable("locale") String localeString) {
    Locale locale = Locale.valueOf(localeString);
    Assert.notNull(locale);
    return memberIdeaCategoryService
      .listCategoriesHierarchyForDropdown(locale);
  }
}
