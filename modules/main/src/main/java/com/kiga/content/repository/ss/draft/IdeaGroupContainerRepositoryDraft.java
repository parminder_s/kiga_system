package com.kiga.content.repository.ss.draft;

import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.draft.IdeaGroupContainerDraft;
import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 14.08.16.
 */
public interface IdeaGroupContainerRepositoryDraft extends
  IdeaGroupContainerRepository<IdeaGroupContainerDraft>,
  CrudRepository<IdeaGroupContainerDraft, Long> {
}
