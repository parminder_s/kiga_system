package com.kiga.content.starrating.web;

import com.kiga.content.starrating.service.StarRatingService;
import com.kiga.content.starrating.web.request.UpdateRatingRequest;
import com.kiga.ideas.service.RatingCalculatorService;
import com.kiga.ideas.web.response.IdeasResponseRating;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

/**
 * @author asv
 * @since 21/02/17.
 */

@RestController
@RequestMapping({"/starRating"})
public class StarRatingController {
  private StarRatingService starRatingService;
  private SecurityService securityService;
  private RatingCalculatorService ratingCalculatorService;


  /**
   * Constructor to pass all dependencies.
   */
  @Autowired
  public StarRatingController(
    StarRatingService starRatingService, SecurityService securityService,
    RatingCalculatorService ratingCalculatorService) {
    this.starRatingService = starRatingService;
    this.securityService = securityService;
    this.ratingCalculatorService = ratingCalculatorService;
  }

  /**
   Get KigaPage rating.
   */
  @ResponseBody
  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public IdeasResponseRating getKigaPageRating(@PathVariable("id") long id) {
    Optional<Member> member = securityService.getSessionMember();
    if (member.isPresent()) {
      long userId = member.get().getId();
      return starRatingService.getKigaPageRating(id, userId);
    }
    return ratingCalculatorService
      .getIdeaResponseRating(starRatingService.getKigaPage(id));
  }

  /**
   Add idea rating.
   */
  @RequestMapping(value = "/updateRating/", method = RequestMethod.POST)
  public IdeasResponseRating updateRating(@RequestBody(required = false)
         UpdateRatingRequest updateRatingRequest) {
    securityService.requiresLogin();
    long userId = securityService.getSessionMember().get().getId();
    Long kigaPageId = updateRatingRequest.getKigaPageId();
    int userRating = updateRatingRequest.getUserRating();

    return starRatingService.addRatingToKigaPage(kigaPageId, userRating, userId);
  }

  /**
   Clear user rating.
   */
  @RequestMapping(value = "/clearRating/", method = RequestMethod.POST)
  public IdeasResponseRating clearRating(@RequestBody(required = false)
         UpdateRatingRequest updateRatingRequest) {
    securityService.requiresLogin();
    long userId = securityService.getSessionMember().get().getId();
    Long kigaPageId = updateRatingRequest.getKigaPageId();

    return starRatingService.clearUserRating(kigaPageId, userId);
  }
}
