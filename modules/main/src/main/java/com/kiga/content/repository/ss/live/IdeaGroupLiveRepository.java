package com.kiga.content.repository.ss.live;

import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.main.locale.Locale;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author bbs
 * @since 5/13/16.
 */
public interface IdeaGroupLiveRepository extends CrudRepository<IdeaGroupLive, Long>,
  QueryDslPredicateExecutor<IdeaGroupLive> {
  List<IdeaGroupLive> findAll();

  List<IdeaGroupLive> findByParentId(Long parentId);

  List<IdeaGroupLive> findByIdAndClassName(Long id, String className);

  List<IdeaGroupLive> findByClassName(String className);

  List<IdeaGroupLive> findByLocaleAndUrlSegment(Locale locale, String urlSegment);

  @Query("FROM #{#entityName} g WHERE g.locale = :locale and g.parent.urlSegment = :urlSegment and "
    + "'IdeaGroupMainContainer' = (CASE WHEN :level = 0 THEN g.parent.urlSegment "
    + "WHEN :level = 1 THEN g.parent.parent.urlSegment "
    + "WHEN :level = 2 THEN g.parent.parent.parent.urlSegment "
    + "WHEN :level = 3 THEN g.parent.parent.parent.parent.urlSegment "
    + "ELSE '' END) ")
  List<IdeaGroupLive> findByLocaleAndUrlSegment(@Param("locale") Locale locale, @Param
    ("urlSegment") String urlSegment, @Param("level") Integer level);

  @Query("FROM #{#entityName} g WHERE g.locale = :locale and g.parent.urlSegment = :urlSegment and "
    + "'IdeaGroupMainContainer' = (CASE WHEN :level = 1 THEN g.parent.parent.urlSegment "
    + "WHEN :level = 2 THEN g.parent.parent.parent.urlSegment "
    + "WHEN :level = 3 THEN g.parent.parent.parent.parent.urlSegment "
    + "ELSE '' END) ")
  List<IdeaGroupLive> findSubgroups(@Param("locale") Locale locale, @Param("urlSegment") String
    urlSegment, @Param("level") Integer level);
}
