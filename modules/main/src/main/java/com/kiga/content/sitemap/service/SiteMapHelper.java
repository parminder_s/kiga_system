package com.kiga.content.sitemap.service;

import java.util.Date;

/**
 * Created by peter on 10.05.16.
 */
public class SiteMapHelper {
  /**
   * returns the frequency where with now set in realtime.
   */
  public static String getChangeFreq(Date lastEdited) {
    return getChangeFreq(lastEdited, new Date());
  }

  /**
   * returns the frequency based on the difference between two dates.
   */
  public static String getChangeFreq(Date lastEdited, Date now) {
    long difference = (now.getTime() - lastEdited.getTime()) / 1000;
    String changeFreq = "always"; // < 1 hour;
    if (difference >= 60 * 60 * 24 * 365) {
      changeFreq = "yearly";
    } else if (difference >= 60 * 60 * 24 * 30) {
      changeFreq = "monthly";
    } else if (difference >= 60 * 60 * 24 * 7) {
      changeFreq = "weekly";
    } else if (difference >= 60 * 60 * 24) {
      changeFreq = "daily";
    } else if (difference >= 60 * 60) {
      changeFreq = "hourly";
    }
    return changeFreq;
  }

  /**
   *
   * @param priority value from the SiteMap entity.
   * @return if priority is valid, returns the priority, else
   *        returns the default priority of 0.5.
     */
  public static String getPriority(String priority) {
    if (priority == null) {
      return "0.5";
    } else {
      return priority;
    }
  }
}
