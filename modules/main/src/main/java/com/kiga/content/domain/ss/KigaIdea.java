package com.kiga.content.domain.ss;

import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.content.domain.ss.property.LayoutType;
import com.kiga.favourites.domain.IdeaFavourite;
import com.kiga.s3.domain.KigaS3File;

import java.util.List;
import java.util.Set;

/**
 * Created by rainerh on 27.06.16.
 */
public interface KigaIdea<T extends SiteTree, I extends IdeaGroupToIdea,
  P extends KigaPrintPreviewImage, G extends LinkedKigaPageGroup> extends KigaPage<T, G> {
  String getExperiences();

  void setExperiences(String experiences);

  String getMaterials();

  void setMaterials(String materials);

  String getPictures();

  void setPictures(String pictures);

  Integer getIncludesAudio();

  void setIncludesAudio(Integer includesAudio);

  LayoutType getLayoutType();

  void setLayoutType(LayoutType layoutType);

  Integer getColsInLayout();

  void setColsInLayout(Integer colsInLayout);

  Integer getArticleScreenshotId();

  void setArticleScreenshotId(Integer articleScreenshotId);

  ArticleType getArticleType();

  void setArticleType(ArticleType articleType);

  Integer getAgeGroup();

  void setAgeGroup(Integer ageGroup);

  Set<I> getIdeaGroupToIdeaList();

  void setIdeaGroupToIdeaList(Set<I> ideaGroupToIdeaList);

  List<P> getKigaPrintPreviewImages();

  void setKigaPrintPreviewImages(List<P> kigaPrintPreviewImages);

  List<IdeaFavourite> getFavourites();

  void setFavourites(List<IdeaFavourite> ideaFavourite);

  KigaS3File getUploadedFile();

  void setUploadedFile(KigaS3File uploadedFile);
}
