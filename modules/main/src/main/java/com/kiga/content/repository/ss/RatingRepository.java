package com.kiga.content.repository.ss;

import com.kiga.content.domain.ss.Rating;
import com.kiga.security.domain.Member;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author asv
 * @since 9/28/16.
 */
public interface RatingRepository extends CrudRepository<Rating, Long> {

  Rating findByMemberIdAndKigaPageId(long memberId, Long pageId);

  List<Rating> findByMember(Member member);
}
