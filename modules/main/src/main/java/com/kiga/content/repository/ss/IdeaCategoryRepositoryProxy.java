package com.kiga.content.repository.ss;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.repository.ss.abstraction.IdeaCategoryRepository;
import com.kiga.content.repository.ss.draft.IdeaCategoryRepositoryDraft;
import com.kiga.content.repository.ss.live.IdeaCategoryRepositoryLive;
import com.kiga.main.locale.Locale;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author bbs
 * @since 7/17/16.
 */
public class IdeaCategoryRepositoryProxy
  extends AbstractRepositoryProxy<IdeaCategoryRepository> implements IdeaCategoryRepository {

  public IdeaCategoryRepositoryProxy(IdeaCategoryRepositoryLive liveRepository,
    IdeaCategoryRepositoryDraft draftRepository, RepositoryContext repositoryContext) {
    super(liveRepository, draftRepository, repositoryContext);
  }

  @Override
  public IdeaCategory findByIdAndClassName(Long categoryId, String className) {
    return getRepository().findByIdAndClassName(categoryId, className);
  }

  @Override
  public List<IdeaCategory> findByParentIdOrderBySortAsc(Long parentId) {
    return getRepository().findByParentIdOrderBySortAsc(parentId);
  }

  @Override
  public List<IdeaCategory> findByLocale(Locale locale) {
    return getRepository().findByLocale(locale);
  }

  @Override
  public List<IdeaCategory> findById(Long id) {
    return getRepository().findById(id);
  }

  @Override
  public List<IdeaCategory> findByIdIn(Collection<Long> ids) {
    return getRepository().findByIdIn(ids);
  }

  @Override
  public List<IdeaCategory> findByClassName(String className) {
    return getRepository().findByClassName(className);
  }

  @Override
  public IdeaCategory findByCodeAndLocale(String code, Locale locale) {
    return getRepository().findByCodeAndLocale(code, locale);
  }

  @Override
  public List<IdeaCategory> findLevel1(Locale locale) {
    return getRepository().findLevel1(locale);
  }

  @Override
  public IdeaCategory findByLocaleAndUrlSegment(Locale locale, String urlSegment) {
    return getRepository().findByLocaleAndUrlSegment(locale, urlSegment);
  }

  @Override
  public IdeaCategory findByLocaleAndUrlSegment(Locale locale, String urlSegment,
    Integer level) {
    return getRepository().findByLocaleAndUrlSegment(locale, urlSegment, level);
  }

  @Override
  public List<IdeaCategory> findBySortTreeStartsWithAndLocaleLikeOrderBySortAsc(
    @Param("sortTree") String sortTree,
    @Param("locale") Locale locale) {
    return getRepository().findBySortTreeStartsWithAndLocaleLikeOrderBySortAsc(
      sortTree, locale);
  }

  @Override
  public IdeaCategory findOne(long id) {
    return getRepository().findOne(id);
  }

  /**
   * Get all idea categories.
   *
   * @return all idea categories
   */
  public List<IdeaCategory> findAll() {
    Iterable<IdeaCategory> all = getCrudRepository().findAll();
    List<IdeaCategory> ideaCategoryList = new ArrayList<>();
    all.forEach(ideaCategoryList::add);
    return ideaCategoryList;
  }

  public QueryDslPredicateExecutor<IdeaCategory> getQueryDslPredicateExecutor() {
    return (QueryDslPredicateExecutor<IdeaCategory>) getRepository();
  }
}
