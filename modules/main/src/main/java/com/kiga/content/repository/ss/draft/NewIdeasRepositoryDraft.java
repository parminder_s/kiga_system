package com.kiga.content.repository.ss.draft;

import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 7/12/16.
 */
public interface NewIdeasRepositoryDraft
  extends NewIdeasRepository, CrudRepository<KigaIdeaDraft, Long> {

}
