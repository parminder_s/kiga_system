package com.kiga.content.domain.ss.live;

import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.property.SortStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by rainerh on 12.08.16.
 */
@Entity
@Table(name = "IdeaGroupContainer_Live")
public class IdeaGroupContainerLive extends IdeaCategoryLive
  implements IdeaGroupContainer<SiteTreeLive> {

  public IdeaGroupContainerLive() {
    this.setClassName("IdeaGroupContainer");
  }
}
