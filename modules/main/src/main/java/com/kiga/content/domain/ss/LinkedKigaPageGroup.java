package com.kiga.content.domain.ss;

import com.kiga.db.KigaEntity;
import com.kiga.main.locale.Locale;

import java.util.List;

/**
 * Created by rainerh on 22.10.16.
 */
public interface LinkedKigaPageGroup<T extends LinkedKigaPageGroup, K extends KigaPage>
  extends KigaEntity {
  String getTitle();

  void setTitle(String title);

  String getDescription();

  void setDescription(String description);

  Locale getLocale();

  void setLocale(Locale locale);

  List<K> getKigaPages();

  void setKigaPages(List<K> kigaPages);

  T getReferenceLinkedKigaPageGroup();

  void setReferenceLinkedKigaPageGroup(T referenceLinkedKigaPageGroup);

  List<T> getLinkedKigaPageGroups();

  void setLinkedKigaPageGroups(List<T> linkedKigaPageGroups);
}
