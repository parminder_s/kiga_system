package com.kiga.content.repository.ss.abstraction;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.main.locale.Locale;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * @author bbs
 * @since 7/17/16.
 */
public interface KigaIdeaRepository<T extends KigaIdea> extends CrudRepository<T, Long> {
  T findOne(Long id);

  List<T> findByClassName(String className);

  List<T> findAll();

  List<T> findAll(Iterable<Long> ids);

  @Modifying
  @Query("update #{#entityName} e set e.uploadedFile = null where e.uploadedFileId = 0")
  void updateUploadedFileIdZeroToNull();

  /**
   * List of all ideas together with ideaGroupToIdeas relations. We orders by property of
   * ideaGroupToIdea and hsqldb requires all columns from order by clause to be also provided on
   * select clause. Therefore we need to return Object[] where Object[0] is KigaIdea and Object[1]
   * is IdeaGroupToIdea
   *
   * @param id id of the group
   * @param ageGroupFilter ageGroupFilter
   * @param sort sorting option
   * @return Object[] please check description for more info
   */
  @EntityGraph(value = "fetchForIdeaGroupsLive", type = EntityGraph.EntityGraphType.LOAD)
  @Query(
      "select distinct f from #{#entityName} f join f.ideaGroupToIdeaList igti "
          + "join igti.ideaGroup g "
          + "WHERE g.id = :id AND f.inPool = 1"
          + "and (:ageGroupFilter = 0 or in_group(f.ageGroup, :ageGroupFilter) = true) ")
  List<KigaIdea> findAllForGroup(
      @Param("id") Long id, @Param("ageGroupFilter") int ageGroupFilter, Sort sort);

  @EntityGraph(value = "fetchForIdeaGroupsLive", type = EntityGraph.EntityGraphType.LOAD)
  @Query(
      "select distinct f from #{#entityName} f join f.ideaGroupToIdeaList igti "
          + "join igti.ideaGroup g left join g.parent p1 left join p1.parent p2 left "
          + "join p2.parent p3 left join p3.parent p4 WHERE f.locale= :locale and f.inPool = 1 "
          + "and (:ageGroupFilter = 0 or in_group(f.ageGroup, :ageGroupFilter) = true) "
          + "and ((:level = 0 and :id in (p1.id, p2.id, p3.id, p4.id)) "
          + "or (:level = 1 and :id in (g.id, p1.id, p2.id, p3.id)) "
          + "or (:level = 2 and :id in (g.id, p1.id, p2.id)) "
          + "or (:level = 3 and :id in (g.id, p1.id)) "
          + "or (:level = 4 and :id = g.id)) ")
  List<KigaIdea> findPagedForGroup(
      @Param("locale") Locale locale,
      @Param("id") Long id,
      @Param("level") Integer level,
      @Param("ageGroupFilter") int ageGroupFilter,
      Pageable pageable);

  @EntityGraph(value = "fetchForIdeaGroupsLive", type = EntityGraph.EntityGraphType.LOAD)
  @Query(
      "select distinct f from #{#entityName} f join f.ideaGroupToIdeaList igti "
          + ""
          + "WHERE f.locale= :locale and f.inPool = 1 "
          + "and igti.sortTree LIKE CONCAT(:sortTree, '%') "
          + "and (:ageGroupFilter = 0 or in_group(f.ageGroup, :ageGroupFilter) = true) ")
  List<KigaIdea> findPagedForGroupSortTree(
      @Param("locale") Locale locale,
      @Param("sortTree") String sortTree,
      @Param("ageGroupFilter") int ageGroupFilter,
      Pageable pageable);

  @Query(
      "select distinct count(f) from #{#entityName} f join f.ideaGroupToIdeaList igti "
          + ""
          + "WHERE f.locale= :locale and f.inPool = 1 "
          + "and igti.sortTree LIKE CONCAT(:sortTree, '%') "
          + "and (:ageGroupFilter = 0 or in_group(f.ageGroup, :ageGroupFilter) = true) ")
  int getGroupCount(
      @Param("locale") Locale locale,
      @Param("sortTree") String sortTree,
      @Param("ageGroupFilter") int ageGroupFilter);
}
