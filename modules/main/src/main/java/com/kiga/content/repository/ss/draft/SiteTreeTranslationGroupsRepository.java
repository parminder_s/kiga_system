package com.kiga.content.repository.ss.draft;

import com.kiga.content.domain.ss.draft.SiteTreeTranslationGroups;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by peter on 17.08.15.
 */
public interface SiteTreeTranslationGroupsRepository
  extends CrudRepository<SiteTreeTranslationGroups, Long> {
  SiteTreeTranslationGroups findByOriginalId(Long originalId);

  List<SiteTreeTranslationGroups> findAllByTranslationGroupId(Long translationGroupId);

  @Query("select max(t.translationGroupId)+1 from #{#entityName} t")
  Long findNextAvailableTranslationGroupId();
}
