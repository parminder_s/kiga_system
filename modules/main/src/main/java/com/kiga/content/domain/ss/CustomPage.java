package com.kiga.content.domain.ss;

/**
 * Created by rainerh on 22.08.16.
 */
public interface CustomPage<T extends SiteTree> extends Page<T> {
  String getCustomCode();

  void setCustomCode(String customCode);
}
