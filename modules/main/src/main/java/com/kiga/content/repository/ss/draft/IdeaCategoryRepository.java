package com.kiga.content.repository.ss.draft;

import com.kiga.content.domain.ss.draft.IdeaCategoryDraft;
import com.kiga.main.locale.Locale;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

/**
 * Created by Evgeny Zhoga on 31.05.15.
 */
public interface IdeaCategoryRepository extends CrudRepository<IdeaCategoryDraft, Long> {
  List<IdeaCategoryDraft> findAll();

  List<IdeaCategoryDraft> findByParentId(Long parentId);

  IdeaCategoryDraft findById(Long id);

  IdeaCategoryDraft findByIdAndClassName(Long id, String className);

  List<IdeaCategoryDraft> findByIdIn(Collection<Long> ids);

  List<IdeaCategoryDraft> findByClassName(String className);

  IdeaCategoryDraft findByLocaleAndIdIn(Locale locale, Collection<Long> ids);

  @Query(
    "select ic from IdeaCategoryDraft ic where ic.parent.className = 'MainArticleCategory' "
      + "and Locale = :locale")
  List<IdeaCategoryDraft> findLevel1(@Param("locale") String locale);
}
