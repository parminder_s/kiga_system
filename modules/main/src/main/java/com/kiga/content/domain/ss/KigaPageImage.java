package com.kiga.content.domain.ss;

import com.kiga.db.KigaEntityModel;
import com.kiga.s3.domain.S3Image;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by rainerh on 06.03.16.
 */
@Entity
public class KigaPageImage extends KigaEntityModel {
  @Column(name = "name")
  private String title;
  private String locale;
  private Long imageId;

  @ManyToOne
  @JoinColumn(name = "s3ImageID", referencedColumnName = "id")
  @NotFound(action = NotFoundAction.IGNORE)
  private S3Image s3Image;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public Long getImageId() {
    return imageId;
  }

  public void setImageId(Long imageId) {
    this.imageId = imageId;
  }

  public S3Image getS3Image() {
    return s3Image;
  }

  public void setS3Image(S3Image s3Image) {
    this.s3Image = s3Image;
  }
}
