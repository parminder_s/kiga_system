package com.kiga.content.domain.ss.draft;

import com.kiga.content.domain.ss.LinkedKigaPageGroupToKigaPage;
import com.kiga.db.KigaEntityModel;

import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by rainerh on 23.10.16.
 */
@Entity
@Table(name = "KigaPageGroup_Articles")
public class LinkedKigaPageGroupToKigaPageDraft extends KigaEntityModel
  implements LinkedKigaPageGroupToKigaPage<LinkedKigaPageGroupDraft, KigaIdeaDraft> {

  @ManyToOne
  @JoinColumn(name = "articleId",
    foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
  private KigaIdeaDraft kigaPage;

  @ManyToOne
  @JoinColumn(name = "kigaPageGroupId", referencedColumnName = "id")
  private LinkedKigaPageGroupDraft linkedKigaPageGroup;

  public LinkedKigaPageGroupToKigaPageDraft() {
  }

  public KigaPageDraft getKigaPage() {
    return kigaPage;
  }

  public void setKigaPage(KigaIdeaDraft kigaPage) {
    this.kigaPage = kigaPage;
  }

  public LinkedKigaPageGroupDraft getLinkedKigaPageGroup() {
    return linkedKigaPageGroup;
  }

  public void setLinkedKigaPageGroup(LinkedKigaPageGroupDraft linkedKigaPageGroup) {
    this.linkedKigaPageGroup = linkedKigaPageGroup;
  }
}
