package com.kiga.content;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by peter on 10.05.16.
 */
@Configuration
@ConfigurationProperties("content")
public class ContentProperties {
  private String siteMapCron;
  private Boolean siteMapEnabled;

  public String getSiteMapCron() {
    return siteMapCron;
  }

  public void setSiteMapCron(String siteMapCron) {
    this.siteMapCron = siteMapCron;
  }

  public Boolean getSiteMapEnabled() {
    return siteMapEnabled;
  }

  public void setSiteMapEnabled(Boolean siteMapEnabled) {
    this.siteMapEnabled = siteMapEnabled;
  }
}
