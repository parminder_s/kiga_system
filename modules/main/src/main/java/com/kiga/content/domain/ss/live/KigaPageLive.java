package com.kiga.content.domain.ss.live;


import com.kiga.content.domain.ss.KigaPage;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.Rating;
import com.kiga.s3.domain.KigaS3File;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;


import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;


/**
 * Created by rainerh on 26.06.16.
 */
@Entity(name = "KigaPage-Live")
@Table(name = "KigaPage_Live")
public class KigaPageLive extends PageLive
  implements KigaPage<SiteTreeLive, LinkedKigaPageGroupLive> {
  private String description;
  private String oldId;
  private Integer inPool = 0;
  private String tagsSummarized;
  private float ratingComplete = 0.0f;
  private int rating1 = 0;
  private int rating2 = 0;
  private int rating3 = 0;
  private int rating4 = 0;
  private int rating5 = 0;
  private int dirty = 0;
  private String tagsText;

  private Integer sortMonth;
  private Integer sortYear;
  private Integer sortIndex = 0;
  private Integer sortMonthMini;
  private Integer sortYearMini;
  private Integer sortIndexMini = 0;
  private Integer sortDayMini;
  private Integer pdfFileId;
  private String author;
  private Integer screenshotId;
  private Integer facebookShare = 0;
  private int automaticMetaDescription = 0;
  private Integer forParents;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PDFWOImageFileID", referencedColumnName = "id")
  private KigaS3File pdfWoImage;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PreviewImageID", referencedColumnName = "id")
  @NotFound(action = NotFoundAction.IGNORE)
  private KigaPageImage previewImage;

  @OneToMany()
  @JoinColumn(name = "kigaPageID")
  @NotFound(action = NotFoundAction.IGNORE)
  private List<KigaPageImage> kigaPageImages;

  @OneToMany(mappedBy = "kigaPage", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @NotFound(action = NotFoundAction.IGNORE)
  private List<Rating> ratings;

  @ManyToMany
  @JoinTable(
    name = "KigaPageGroup_Articles",
    joinColumns = @JoinColumn(name = "ArticleID"),
    inverseJoinColumns = @JoinColumn(name = "KigaPageGroupID")
  )
  private List<LinkedKigaPageGroupLive> linkedKigaPageGroups;

  public KigaPageLive() {
    this.setClassName("KigaPage");
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getOldId() {
    return oldId;
  }

  public void setOldId(String oldId) {
    this.oldId = oldId;
  }

  public Integer getInPool() {
    return inPool;
  }

  public void setInPool(Integer inPool) {
    this.inPool = inPool;
  }

  public String getTagsSummarized() {
    return tagsSummarized;
  }

  public void setTagsSummarized(String tagsSummarized) {
    this.tagsSummarized = tagsSummarized;
  }

  @Override
  public float getRatingComplete() {
    return ratingComplete;
  }

  @Override
  public void setRatingComplete(float ratingComplete) {
    this.ratingComplete = ratingComplete;
  }

  @Override
  public int getRating1() {
    return rating1;
  }

  @Override
  public void setRating1(int rating1) {
    this.rating1 = rating1;
  }

  @Override
  public int getRating2() {
    return rating2;
  }

  @Override
  public void setRating2(int rating2) {
    this.rating2 = rating2;
  }

  @Override
  public int getRating3() {
    return rating3;
  }

  @Override
  public void setRating3(int rating3) {
    this.rating3 = rating3;
  }

  @Override
  public int getRating4() {
    return rating4;
  }

  @Override
  public void setRating4(int rating4) {
    this.rating4 = rating4;
  }

  @Override
  public int getRating5() {
    return rating5;
  }

  @Override
  public void setRating5(int rating5) {
    this.rating5 = rating5;
  }

  public Integer getDirty() {
    return dirty;
  }

  public void setDirty(Integer dirty) {
    this.dirty = dirty;
  }

  public String getTagsText() {
    return tagsText;
  }

  public void setTagsText(String tagsText) {
    this.tagsText = tagsText;
  }

  public Integer getSortMonth() {
    return sortMonth;
  }

  public void setSortMonth(Integer sortMonth) {
    this.sortMonth = sortMonth;
  }

  public Integer getSortYear() {
    return sortYear;
  }

  public void setSortYear(Integer sortYear) {
    this.sortYear = sortYear;
  }

  public Integer getSortIndex() {
    return sortIndex;
  }

  public void setSortIndex(Integer sortIndex) {
    this.sortIndex = sortIndex;
  }

  public Integer getSortMonthMini() {
    return sortMonthMini;
  }

  public void setSortMonthMini(Integer sortMonthMini) {
    this.sortMonthMini = sortMonthMini;
  }

  public Integer getSortYearMini() {
    return sortYearMini;
  }

  public void setSortYearMini(Integer sortYearMini) {
    this.sortYearMini = sortYearMini;
  }

  public Integer getSortIndexMini() {
    return sortIndexMini;
  }

  public void setSortIndexMini(Integer sortIndexMini) {
    this.sortIndexMini = sortIndexMini;
  }

  public Integer getSortDayMini() {
    return sortDayMini;
  }

  public void setSortDayMini(Integer sortDayMini) {
    this.sortDayMini = sortDayMini;
  }

  public Integer getPdfFileId() {
    return pdfFileId;
  }

  public void setPdfFileId(Integer pdfFileId) {
    this.pdfFileId = pdfFileId;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public Integer getScreenshotId() {
    return screenshotId;
  }

  public void setScreenshotId(Integer screenshotId) {
    this.screenshotId = screenshotId;
  }

  public Integer getFacebookShare() {
    return facebookShare;
  }

  public void setFacebookShare(Integer facebookShare) {
    this.facebookShare = facebookShare;
  }

  public Integer getAutomaticMetaDescription() {
    return automaticMetaDescription;
  }

  public void setAutomaticMetaDescription(Integer automaticMetaDescription) {
    this.automaticMetaDescription = automaticMetaDescription;
  }

  public Integer getForParents() {
    return forParents;
  }

  public void setForParents(Integer forParents) {
    this.forParents = forParents;
  }

  public KigaS3File getPdfWoImage() {
    return pdfWoImage;
  }

  public void setPdfWoImage(KigaS3File pdfWoImage) {
    this.pdfWoImage = pdfWoImage;
  }

  public KigaPageImage getPreviewImage() {
    return previewImage;
  }

  public void setPreviewImage(KigaPageImage previewImage) {
    this.previewImage = previewImage;
  }

  public List<KigaPageImage> getKigaPageImages() {
    return kigaPageImages;
  }

  public void setKigaPageImages(List<KigaPageImage> kigaPageImages) {
    this.kigaPageImages = kigaPageImages;
  }

  public List<Rating> getRatings() {
    return ratings;
  }

  public void setRatings(List<Rating> ratings) {
    this.ratings = ratings;
  }


  public List<LinkedKigaPageGroupLive> getLinkedKigaPageGroups() {
    return this.linkedKigaPageGroups;
  }

  public void setLinkedKigaPageGroups(List<LinkedKigaPageGroupLive> linkedKigaPageGroups) {
    this.linkedKigaPageGroups = linkedKigaPageGroups;
  }
}
