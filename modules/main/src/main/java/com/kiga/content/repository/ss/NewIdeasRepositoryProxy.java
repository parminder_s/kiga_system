package com.kiga.content.repository.ss;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.main.locale.Locale;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

/**
 * @author bbs
 * @since 7/25/16.
 */
public class NewIdeasRepositoryProxy extends AbstractRepositoryProxy<NewIdeasRepository>
  implements NewIdeasRepository {
  /**
   * Constructor for the Model.
   *
   * @param liveRepository    live repository
   * @param draftRepository   draft repository
   * @param repositoryContext repository context holder
   */
  public NewIdeasRepositoryProxy(
    NewIdeasRepository liveRepository,
    NewIdeasRepository draftRepository,
    RepositoryContext repositoryContext) {
    super(liveRepository, draftRepository, repositoryContext);
  }

  @Override
  public List<KigaIdea> getIdeasForAllAgeGroups(Locale locale, int ageGroupFilter,
                                                Pageable pageable) {
    return getRepository()
      .getIdeasForAllAgeGroups(locale, ageGroupFilter, pageable);
  }

  @Override
  public Set<KigaIdea> getIdeasForAgeGroupByBitPosition(int ageGroupCode, Locale locale,
    int ageGroupFilter) {
    return getRepository().getIdeasForAgeGroupByBitPosition(ageGroupCode, locale, ageGroupFilter);
  }

  @Override
  public List<KigaIdea> getIdeasForAgeGroupByBitPosition(int ageGroupCode,
                                                         Locale locale, Pageable pageable) {
    return getRepository()
      .getIdeasForAgeGroupByBitPosition(ageGroupCode, locale, pageable);
  }

  @Override
  public List<KigaIdea> getAllIdeas(Locale locale, int ageGroupFilter) {
    return getRepository()
      .getAllIdeas(locale, ageGroupFilter);
  }
}
