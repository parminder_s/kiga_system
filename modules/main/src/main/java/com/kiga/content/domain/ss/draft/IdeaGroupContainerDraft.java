package com.kiga.content.domain.ss.draft;

import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.property.SortStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by rainerh on 13.08.16.
 */
@Entity
@Table(name = "IdeaGroupContainer")
public class IdeaGroupContainerDraft extends IdeaCategoryDraft
  implements IdeaGroupContainer<SiteTreeDraft> {
  public IdeaGroupContainerDraft() {
    this.setClassName("IdeaGroupContainer");
  }
}

