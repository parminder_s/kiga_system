package com.kiga.content.domain.ss;

import com.kiga.content.domain.ss.property.SortStrategy;

/**
 * Created by rainerh on 27.06.16.
 */
public interface IdeaCategory<T extends SiteTree> extends KigaCategory<T> {
  Integer getHomepageCol();

  void setHomepageCol(Integer homepageCol);

  Integer getPosition();

  String getIconCode();

  void setPosition(Integer position);

  void setIconCode(String iconCode);

  SortStrategy getSortStrategy();

  void setSortStrategy(SortStrategy sortStrategy);

  boolean isDisableForFavourites();

  void setDisableForFavourites(boolean disableForFavourites);
}
