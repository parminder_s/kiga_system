package com.kiga.content.service;

import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.draft.SiteTreeTranslationGroups;
import com.kiga.content.repository.ss.draft.SiteTreeTranslationGroupsRepository;
import com.kiga.main.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by robert on 31.08.17.
 */
@Service
public class SiteTreeTranslationGroupService {
  private SiteTreeTranslationGroupsRepository siteTreeTranslationGroupsRepository;

  @Autowired
  public SiteTreeTranslationGroupService(
    SiteTreeTranslationGroupsRepository siteTreeTranslationGroupsRepository) {
    this.siteTreeTranslationGroupsRepository = siteTreeTranslationGroupsRepository;
  }

  /**
   * Creates translation group for given SiteTree.
   *
   * @param siteTree SiteTree for which to create translation group.
   */
  public void createTranslationGroup(SiteTree siteTree) {
    Long nextAvailableTranslationGroupId =
      siteTreeTranslationGroupsRepository.findNextAvailableTranslationGroupId();
    if (NumberUtil.isNotValidId(nextAvailableTranslationGroupId)) {
      nextAvailableTranslationGroupId = 1L;
    }

    addToTranslationGroup(nextAvailableTranslationGroupId, siteTree);
  }

  /**
   * Adds SiteTree element to the translation group.
   *
   * @param translationGroupId id of translation group to which the SiteTree element will be added
   * @param siteTree           SiteTree element which will be added to translation group.
   */
  public void addToTranslationGroup(long translationGroupId, SiteTree siteTree) {
    SiteTreeTranslationGroups siteTreeTranslationGroups = new SiteTreeTranslationGroups();
    siteTreeTranslationGroups.setTranslationGroupId(translationGroupId);
    siteTreeTranslationGroups.setOriginalId(siteTree.getId());
    siteTreeTranslationGroupsRepository.save(siteTreeTranslationGroups);
  }
}
