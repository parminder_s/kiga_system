package com.kiga.content.domain.ss.live;

import com.kiga.content.domain.ss.LinkedKigaPageGroup;
import com.kiga.db.KigaEntityModel;
import com.kiga.db.LocaleToLongNotationConverter;
import com.kiga.main.locale.Locale;

import java.util.List;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Created by rainerh on 22.10.16.
 */
@Entity
@Table(name = "KigaPageGroup")
public class LinkedKigaPageGroupLive extends KigaEntityModel
  implements LinkedKigaPageGroup<LinkedKigaPageGroupLive, KigaPageLive> {
  private String title;

  private String description;

  @Convert(converter = LocaleToLongNotationConverter.class)
  private Locale locale;

  @ManyToMany
  @JoinTable(
    name = "KigaPageGroup_Articles",
    joinColumns = @JoinColumn(name = "KigaPageGroupID"),
    inverseJoinColumns = @JoinColumn(name = "ArticleID")
  )
  private List<KigaPageLive> kigaPages;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "referenceGroupID", referencedColumnName = "id")
  private LinkedKigaPageGroupLive referenceLinkedKigaPageGroup;

  @OneToMany(mappedBy = "referenceLinkedKigaPageGroup")
  List<LinkedKigaPageGroupLive> linkedKigaPageGroupLives;

  public LinkedKigaPageGroupLive() {
    this.setClassName("KigaPageGroup");
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setDescription(String description) {
    this.description = description;
  }

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  @Override
  public List<KigaPageLive> getKigaPages() {
    return kigaPages;
  }

  @Override
  public void setKigaPages(List<KigaPageLive> kigaPages) {
    this.kigaPages = kigaPages;
  }

  @Override
  public LinkedKigaPageGroupLive getReferenceLinkedKigaPageGroup() {
    return referenceLinkedKigaPageGroup;
  }

  @Override
  public void setReferenceLinkedKigaPageGroup(
    LinkedKigaPageGroupLive referenceLinkedKigaPageGroup) {
    this.referenceLinkedKigaPageGroup = referenceLinkedKigaPageGroup;
  }

  @Override
  public List<LinkedKigaPageGroupLive> getLinkedKigaPageGroups() {
    return linkedKigaPageGroupLives;
  }

  @Override
  public void setLinkedKigaPageGroups(List<LinkedKigaPageGroupLive> linkedKigaPageGroupLives) {
    this.linkedKigaPageGroupLives = linkedKigaPageGroupLives;
  }
}
