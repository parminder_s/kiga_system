package com.kiga.content.sorttree.spec;

import java.util.List;

/**
 * Created by rainerh on 29.03.17.
 * Modified by Florian Schneider on 07.11.17
 *
 * <p>The interface on which the calculator for SortTree operates. It was necessary to
 * fully abstract and not integrate it into SiteTree since in the case of IdeaHierarchy,
 * we have a tree that is not represented by the SiteTree model. IdeaGroupToIdea is not a child
 * of SiteTree but handled like one.
 */
public interface SortTreeElement {
  List<SortTreeElement> getChildren();

  SortTree getSortTree();
}
