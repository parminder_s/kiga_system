package com.kiga.content.sorttree.helper;

import com.kiga.content.domain.ss.draft.IdeaGroupDraft;
import com.kiga.content.repository.ss.draft.IdeaGroupToIdeaRepository;
import com.kiga.content.repository.ss.live.IdeaGroupLiveRepository;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UnusedSortTreeNullSetter {
  private IdeaGroupToIdeaRepository ideaGroupToIdeaRepository;
  private IdeaGroupLiveRepository ideaGroupLiveRepository;

  @Autowired
  public UnusedSortTreeNullSetter(
      IdeaGroupToIdeaRepository ideaGroupToIdeaRepository,
      IdeaGroupLiveRepository ideaGroupLiveRepository) {
    this.ideaGroupToIdeaRepository = ideaGroupToIdeaRepository;
    this.ideaGroupLiveRepository = ideaGroupLiveRepository;
  }

  public void setUnusedToNull() {
    Set<Long> ideaGroupLiveIDmap = new HashSet<>();
    ideaGroupLiveRepository
        .findByClassName("IdeaGroup")
        .stream()
        .forEach(ideaGroupLive -> ideaGroupLiveIDmap.add(ideaGroupLive.getId()));

    ideaGroupToIdeaRepository
        .findAll()
        .stream()
        .forEach(
            ideaGroupToIdeaDraft -> {
              IdeaGroupDraft ideaGroupDraft = ideaGroupToIdeaDraft.getIdeaGroup();

              if (ideaGroupDraft != null
                  && !ideaGroupLiveIDmap.contains(ideaGroupDraft.getId())
                  && ideaGroupToIdeaDraft.getSortTree() != null) {
                ideaGroupToIdeaDraft.setSortTree(null);
                ideaGroupToIdeaRepository.save(ideaGroupToIdeaDraft);
              }
            });
  }
}
