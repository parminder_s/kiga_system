package com.kiga.content.sitemap.web;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.IOUtils;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.sitemap.SiteMapJob;
import com.kiga.content.sitemap.service.SiteMapGenerator;
import com.kiga.s3.S3Properties;
import com.kiga.security.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by peter on 11.05.16.
 */
@RestController
@RequestMapping("/")
public class SiteMapController {


  S3Properties s3Properties;

  SecurityService securityService;

  SiteMapGenerator siteMapGenerator;

  SiteTreeLiveRepository siteTreeLiveRepository;

  AmazonS3Client amazonS3Client;

  /**
   *
   * @param s3Properties Amazon properties.
   * @param securityService Security service to check user permissions.
   * @param siteMapGenerator Generator for the SiteMap.
   * @param siteTreeLiveRepository repository to get SiteTreeElements
   * @param amazonS3Client Client to download the cached sitemap from s3.
   */
  @Autowired
  public SiteMapController(S3Properties s3Properties, SecurityService securityService,
                           SiteMapGenerator siteMapGenerator,
                           SiteTreeLiveRepository siteTreeLiveRepository,
                           AmazonS3Client amazonS3Client) {
    this.s3Properties = s3Properties;
    this.securityService = securityService;
    this.siteMapGenerator = siteMapGenerator;
    this.siteTreeLiveRepository = siteTreeLiveRepository;
    this.amazonS3Client = amazonS3Client;
  }


  /**
   *
   * @param response httpServletResponse sitemap xml is written to.
   */
  @RequestMapping(
    value = "sitemap.xml",
    method = RequestMethod.GET
  )
  public void siteMapXml(HttpServletResponse response) throws IOException {
    S3Object sitemap = amazonS3Client.getObject(s3Properties.getBucketName(),
      SiteMapJob.SITEMAP_KEY);
    IOUtils.copy(sitemap.getObjectContent(), response.getOutputStream());
    response.setContentType("text/xml");
  }

  /**
   * generate and return live sitemap.
   */
  @RequestMapping(value = "generateSitemap", method = RequestMethod.GET)
  public void generateSiteMap(HttpServletResponse response)
    throws IOException {
    securityService.requiresContentAdmin();
    response.setContentType("text/xml");
    PrintWriter writer = response.getWriter();
    writer.write(siteMapGenerator.getSiteMap(siteTreeLiveRepository.findAll()));
    writer.close();
  }
}
