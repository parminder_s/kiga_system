package com.kiga.content.repository.ss.live;

import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 7/12/16.
 */
public interface NewIdeasRepositoryLive
  extends NewIdeasRepository, CrudRepository<KigaIdeaLive, Long> {

}
