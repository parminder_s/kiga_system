package com.kiga.content.repository.ss.draft;

import com.kiga.content.domain.ss.draft.IdeaGroupToIdeaDraft;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface IdeaGroupToIdeaRepository extends CrudRepository<IdeaGroupToIdeaDraft, Long> {
  List<IdeaGroupToIdeaDraft> findByIdeaGroupId(long ideaGroupId);

  List<IdeaGroupToIdeaDraft> findByArticleId(long articleId);

  List<IdeaGroupToIdeaDraft> findById(long id);

  List<IdeaGroupToIdeaDraft> findAll();
}
