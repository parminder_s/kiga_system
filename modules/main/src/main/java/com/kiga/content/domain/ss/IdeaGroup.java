package com.kiga.content.domain.ss;

import java.util.List;

/**
 * Created by rainerh on 27.06.16.
 */
public interface IdeaGroup<T extends SiteTree, I extends IdeaGroupToIdea> extends IdeaCategory<T> {
  List<I> getIdeaGroupToIdeaList();

  void setIdeaGroupToIdeaList(List<I> ideaGroupToIdeaList);
}
