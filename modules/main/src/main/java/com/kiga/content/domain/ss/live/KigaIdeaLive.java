package com.kiga.content.domain.ss.live;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.content.domain.ss.property.LayoutType;
import com.kiga.db.ArticleTypeConverter;
import com.kiga.db.LayoutTypeConverter;
import com.kiga.favourites.domain.IdeaFavourite;
import com.kiga.s3.domain.KigaS3File;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * Created by rainerh on 27.06.16.
 *
 * <p>Please note the entityGraph that is required for loading the ideas on the overview page. We
 * are including IdeaGroupToIdeaList here, since in hsqldb each column which is included in the
 * order by clause has also to be included in the select clause.
 */
@Entity
@Table(name = "Article_Live")
@NamedEntityGraph(
    name = "fetchForIdeaGroupsLive",
    attributeNodes = {
      @NamedAttributeNode(value = "previewImage", subgraph = "s3Image"),
      @NamedAttributeNode(value = "kigaPrintPreviewImages", subgraph = "s3Image"),
      @NamedAttributeNode(value = "ideaGroupToIdeaList")
    },
    subgraphs = @NamedSubgraph(name = "s3Image", attributeNodes = @NamedAttributeNode("s3Image")))
public class KigaIdeaLive extends KigaPageLive
    implements KigaIdea<
        SiteTreeLive, IdeaGroupToIdeaLive, KigaPrintPreviewImageLive, LinkedKigaPageGroupLive> {
  private String experiences;
  private String materials;
  private String pictures;
  private int includesAudio = 0;

  @Convert(converter = LayoutTypeConverter.class)
  private LayoutType layoutType;

  private int colsInLayout = 0;
  private Integer articleScreenshotId;

  @Convert(converter = ArticleTypeConverter.class)
  private ArticleType articleType;

  // TODO: Have two times the same column, because SilverStripe sometimes uses a 0 as value and we
  // have a job that needs to query for that value.
  @Column(name = "UploadedFileId", insertable = false, updatable = false)
  private Integer uploadedFileId;

  @Column(name = "nAgeGroup")
  private Integer ageGroup;

  @OneToMany(mappedBy = "article")
  private Set<IdeaGroupToIdeaLive> ideaGroupToIdeaList;

  @OneToMany(mappedBy = "kigaIdeaLive")
  private List<IdeaFavourite> favourites;

  @ManyToOne
  @NotFound(action = NotFoundAction.IGNORE)
  private KigaS3File uploadedFile;

  /**
   * https://hibernate.atlassian.net/browse/HHH-8805 no constraint on auto-ddl since same table used
   * by two different entities.
   */
  @OneToMany(mappedBy = "kigaIdea")
  @OrderBy("pageNumber ASC")
  @ForeignKey(name = "none")
  private List<KigaPrintPreviewImageLive> kigaPrintPreviewImages;

  public KigaIdeaLive() {
    this.setClassName("Article");
  }

  public String getExperiences() {
    return experiences;
  }

  public void setExperiences(String experiences) {
    this.experiences = experiences;
  }

  public String getMaterials() {
    return materials;
  }

  public void setMaterials(String materials) {
    this.materials = materials;
  }

  public String getPictures() {
    return pictures;
  }

  public void setPictures(String pictures) {
    this.pictures = pictures;
  }

  public Integer getIncludesAudio() {
    return includesAudio;
  }

  public void setIncludesAudio(Integer includesAudio) {
    this.includesAudio = includesAudio;
  }

  public LayoutType getLayoutType() {
    return layoutType;
  }

  public void setLayoutType(LayoutType layoutType) {
    this.layoutType = layoutType;
  }

  public Integer getColsInLayout() {
    return colsInLayout;
  }

  public void setColsInLayout(Integer colsInLayout) {
    this.colsInLayout = colsInLayout;
  }

  public Integer getArticleScreenshotId() {
    return articleScreenshotId;
  }

  public void setArticleScreenshotId(Integer articleScreenshotId) {
    this.articleScreenshotId = articleScreenshotId;
  }

  public ArticleType getArticleType() {
    return articleType;
  }

  public void setArticleType(ArticleType articleType) {
    this.articleType = articleType;
  }

  public Integer getAgeGroup() {
    return ageGroup;
  }

  public void setAgeGroup(Integer ageGroup) {
    this.ageGroup = ageGroup;
  }

  public Set<IdeaGroupToIdeaLive> getIdeaGroupToIdeaList() {
    return ideaGroupToIdeaList;
  }

  public void setIdeaGroupToIdeaList(Set<IdeaGroupToIdeaLive> ideaGroupToIdeaList) {
    this.ideaGroupToIdeaList = ideaGroupToIdeaList;
  }

  public List<KigaPrintPreviewImageLive> getKigaPrintPreviewImages() {
    return kigaPrintPreviewImages;
  }

  public void setKigaPrintPreviewImages(List<KigaPrintPreviewImageLive> kigaPrintPreviewImages) {
    this.kigaPrintPreviewImages = kigaPrintPreviewImages;
  }

  @Override
  public List<IdeaFavourite> getFavourites() {
    return favourites;
  }

  @Override
  public void setFavourites(List<IdeaFavourite> ideaFavourite) {
    this.favourites = ideaFavourite;
  }

  public KigaS3File getUploadedFile() {
    return uploadedFile;
  }

  public void setUploadedFile(KigaS3File uploadedFile) {
    this.uploadedFile = uploadedFile;
  }

  public Integer getUploadedFileId() {
    return uploadedFileId;
  }

  public void setUploadedFileId(Integer uploadedFileId) {
    this.uploadedFileId = uploadedFileId;
  }
}
