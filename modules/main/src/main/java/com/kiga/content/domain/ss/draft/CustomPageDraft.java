package com.kiga.content.domain.ss.draft;

import com.kiga.content.domain.ss.CustomPage;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by rainerh on 22.08.16.
 */
@Entity
@Table(name = "CustomPage")
public class CustomPageDraft extends PageDraft implements CustomPage<SiteTreeDraft> {
  private String customCode;

  public CustomPageDraft() {
    this.setClassName("CustomPage");
  }

  public String getCustomCode() {
    return customCode;
  }

  public void setCustomCode(String customCode) {
    this.customCode = customCode;
  }
}
