package com.kiga.content.repository.ss;

import com.kiga.content.domain.ss.KigaPageImage;
import org.springframework.data.repository.CrudRepository;

public interface KigaPageImageRepository extends CrudRepository<KigaPageImage, Long> {}
