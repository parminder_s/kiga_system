package com.kiga.content.domain.ss.property;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by rainerh on 09.09.16.
 */
@Converter(autoApply = true)
public class SortStrategyConverter implements
  AttributeConverter<SortStrategy, Integer> {
  @Override
  public Integer convertToDatabaseColumn(SortStrategy attribute) {
    switch (attribute) {
      case INHERITED:
        return 1;
      case DATE:
        return 2;
      case SEQUENCE:
        return 3;
      default:
        throw new IllegalArgumentException("Unknown " + attribute);
    }
  }

  @Override
  public SortStrategy convertToEntityAttribute(Integer dbData) {
    switch (dbData) {
      case 1:
        return SortStrategy.INHERITED;
      case 2:
        return SortStrategy.DATE;
      case 3:
        return SortStrategy.SEQUENCE;
      default:
        throw new IllegalArgumentException("Unknown " + dbData);
    }
  }
}
