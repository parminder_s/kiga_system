package com.kiga.content.repository.ss.live;

import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.repository.ss.abstraction.KigaIdeaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author bbs
 * @since 4/21/16.
 */
public interface KigaIdeaRepositoryLive extends
  QueryDslPredicateExecutor<KigaIdeaLive>,
  PagingAndSortingRepository<KigaIdeaLive,
    Long>, KigaIdeaRepository<KigaIdeaLive> {
}
