package com.kiga.content.domain.ss.draft;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.content.domain.ss.property.LayoutType;
import com.kiga.db.ArticleTypeConverter;
import com.kiga.db.LayoutTypeConverter;
import com.kiga.favourites.domain.IdeaFavourite;
import com.kiga.s3.domain.KigaS3File;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/** Created by rainerh on 27.06.16. */
@Entity(name = "KigaIdeaDraft")
@Table(name = "Article")
@NamedEntityGraph(
    name = "fetchForIdeaGroupsDraft",
    attributeNodes = {
      @NamedAttributeNode(value = "previewImage", subgraph = "s3Image"),
      @NamedAttributeNode(value = "kigaPrintPreviewImages", subgraph = "s3Image")
    },
    subgraphs = @NamedSubgraph(name = "s3Image", attributeNodes = @NamedAttributeNode("s3Image")))
public class KigaIdeaDraft extends KigaPageDraft
    implements KigaIdea<
        SiteTreeDraft, IdeaGroupToIdeaDraft, KigaPrintPreviewImageDraft, LinkedKigaPageGroupDraft> {
  private String experiences;
  private String materials;
  private String pictures;
  private int includesAudio = 0;

  @Convert(converter = LayoutTypeConverter.class)
  private LayoutType layoutType;

  private int colsInLayout = 0;
  private Integer articleScreenshotId;

  @Convert(converter = ArticleTypeConverter.class)
  private ArticleType articleType;

  // TODO: Have two times the same column, because SilverStripe sometimes uses a 0 as value and we
  // have a job that needs to query for that value.
  @Column(name = "UploadedFileId", insertable = false, updatable = false)
  private Integer uploadedFileId;

  @Column(name = "nAgeGroup")
  private Integer ageGroup;

  @OneToMany(mappedBy = "article")
  private Set<IdeaGroupToIdeaDraft> ideaGroupToIdeaList;

  @Transient private List<IdeaFavourite> favourites;

  @ManyToOne
  @NotFound(action = NotFoundAction.IGNORE)
  private KigaS3File uploadedFile;

  /**
   * https://hibernate.atlassian.net/browse/HHH-8805 no constraint on auto-ddl since same table used
   * by two different entities.
   */
  @OneToMany(mappedBy = "kigaIdea")
  @ForeignKey(name = "none")
  @OrderBy("pageNumber ASC")
  private List<KigaPrintPreviewImageDraft> kigaPrintPreviewImages;

  public KigaIdeaDraft() {
    this.setClassName("Article");
  }

  public String getExperiences() {
    return experiences;
  }

  public void setExperiences(String experiences) {
    this.experiences = experiences;
  }

  public String getMaterials() {
    return materials;
  }

  public void setMaterials(String materials) {
    this.materials = materials;
  }

  public String getPictures() {
    return pictures;
  }

  public void setPictures(String pictures) {
    this.pictures = pictures;
  }

  public Integer getIncludesAudio() {
    return includesAudio;
  }

  public void setIncludesAudio(Integer includesAudio) {
    this.includesAudio = includesAudio;
  }

  public LayoutType getLayoutType() {
    return layoutType;
  }

  public void setLayoutType(LayoutType layoutType) {
    this.layoutType = layoutType;
  }

  public Integer getColsInLayout() {
    return colsInLayout;
  }

  public void setColsInLayout(Integer colsInLayout) {
    this.colsInLayout = colsInLayout;
  }

  public Integer getArticleScreenshotId() {
    return articleScreenshotId;
  }

  public void setArticleScreenshotId(Integer articleScreenshotId) {
    this.articleScreenshotId = articleScreenshotId;
  }

  public ArticleType getArticleType() {
    return articleType;
  }

  public void setArticleType(ArticleType articleType) {
    this.articleType = articleType;
  }

  public Set<IdeaGroupToIdeaDraft> getIdeaGroupToIdeaList() {
    return ideaGroupToIdeaList;
  }

  public void setIdeaGroupToIdeaList(Set<IdeaGroupToIdeaDraft> ideaGroupToIdeaList) {
    this.ideaGroupToIdeaList = ideaGroupToIdeaList;
  }

  public Integer getAgeGroup() {
    return ageGroup;
  }

  public void setAgeGroup(Integer ageGroup) {
    this.ageGroup = ageGroup;
  }

  public List<KigaPrintPreviewImageDraft> getKigaPrintPreviewImages() {
    return kigaPrintPreviewImages;
  }

  public void setKigaPrintPreviewImages(List<KigaPrintPreviewImageDraft> kigaPrintPreviewImages) {
    this.kigaPrintPreviewImages = kigaPrintPreviewImages;
  }

  @Override
  public List<IdeaFavourite> getFavourites() {
    return favourites;
  }

  @Override
  public void setFavourites(List<IdeaFavourite> ideaFavourites) {
    this.favourites = ideaFavourites;
  }

  @Override
  public KigaS3File getUploadedFile() {
    return uploadedFile;
  }

  @Override
  public void setUploadedFile(KigaS3File uploadedFile) {
    this.uploadedFile = uploadedFile;
  }

  public Integer getUploadedFileId() {
    return uploadedFileId;
  }

  public void setUploadedFileId(Integer uploadedFileId) {
    this.uploadedFileId = uploadedFileId;
  }
}
