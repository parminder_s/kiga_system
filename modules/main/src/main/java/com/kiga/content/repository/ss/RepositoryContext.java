package com.kiga.content.repository.ss;

import static com.kiga.content.repository.ss.RepositoryContexts.LIVE;

import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 7/22/16.
 */
public class RepositoryContext {
  private final String sessionKey = this.getClass().getCanonicalName() + "::context";
  private HttpServletRequest request;

  public RepositoryContext(HttpServletRequest request) {
    this.request = request;
  }

  public void toggleContext() {
    //request.getSession().setAttribute(sessionKey, getContext().equals(LIVE) ? DRAFT : LIVE);
  }

  /**
   * Return current repository context (for live or draft repository).
   *
   * @return repository context
   */
  public RepositoryContexts getContext() {
    return LIVE;
    /*
    ERepositoryContexts context = (ERepositoryContexts) request.getSession()
      .getAttribute(sessionKey);
    if (context == null) {
      return LIVE;
    }
    return context;
    */
  }
}
