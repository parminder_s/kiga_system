package com.kiga.content.domain.ss.draft;


import com.kiga.content.domain.ss.IdeaGroup;
import com.kiga.content.domain.ss.property.SortStrategy;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Created by rainerh on 27.06.16.
 */
@Entity
@Table(name = "IdeaGroup")
public class IdeaGroupDraft extends IdeaCategoryDraft
  implements IdeaGroup<SiteTreeDraft, IdeaGroupToIdeaDraft> {
  @OneToMany(mappedBy = "ideaGroup")
  private List<IdeaGroupToIdeaDraft> ideaGroupToIdeaList;

  public IdeaGroupDraft() {
    this.setClassName("IdeaGroup");
  }

  @Override
  public List<IdeaGroupToIdeaDraft> getIdeaGroupToIdeaList() {
    return ideaGroupToIdeaList;
  }

  @Override
  public void setIdeaGroupToIdeaList(List<IdeaGroupToIdeaDraft> ideaGroupToIdeaList) {
    this.ideaGroupToIdeaList = ideaGroupToIdeaList;
  }
}
