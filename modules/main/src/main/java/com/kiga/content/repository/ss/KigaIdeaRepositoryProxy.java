package com.kiga.content.repository.ss;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.abstraction.KigaIdeaRepository;
import com.kiga.main.locale.Locale;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;

/**
 * @author bbs
 * @since 7/18/16.
 */
@SuppressWarnings("unchecked")
public class KigaIdeaRepositoryProxy extends AbstractRepositoryProxy<KigaIdeaRepository>
    implements KigaIdeaRepository<KigaIdea> {
  public KigaIdeaRepositoryProxy(
      KigaIdeaRepository liveRepository,
      KigaIdeaRepository draftRepository,
      RepositoryContext repositoryContext) {
    super(liveRepository, draftRepository, repositoryContext);
  }

  @Override
  public List<KigaIdea> findAll() {
    return (List<KigaIdea>) getCrudRepository().findAll();
  }

  @Override
  public List<KigaIdea> findAll(Iterable<Long> ids) {
    return this.getRepository().findAll(ids);
  }

  @Override
  public void updateUploadedFileIdZeroToNull() {
    getRepository().updateUploadedFileIdZeroToNull();
  }

  @Override
  public List<KigaIdea> findByClassName(String className) {
    return getRepository().findByClassName(className);
  }

  @Override
  public KigaIdea findOne(Long id) {
    return getRepository().findOne(id);
  }

  @Override
  public List<KigaIdea> findAllForGroup(Long id, int ageGroupFilter, Sort sort) {
    return getRepository().findAllForGroup(id, ageGroupFilter, sort);
  }

  @Override
  public List<KigaIdea> findPagedForGroup(
      Locale locale, Long id, Integer level, int ageGroupFilter, Pageable pageable) {
    return getRepository().findPagedForGroup(locale, id, level, ageGroupFilter, pageable);
  }

  @Override
  public List<KigaIdea> findPagedForGroupSortTree(
      @Param("locale") Locale locale,
      @Param("sortTree") String sortTree,
      @Param("ageGroupFilter") int ageGroupFilter,
      Pageable pageable) {
    return getRepository().findPagedForGroupSortTree(locale, sortTree, ageGroupFilter, pageable);
  }

  @Override
  public int getGroupCount(
      @Param("locale") Locale locale,
      @Param("sortTree") String sortTree,
      @Param("ageGroupFilter") int ageGroupFilter) {
    return getRepository().getGroupCount(locale, sortTree, ageGroupFilter);
  }

  @Override
  public void deleteAll() {
    this.getRepository().deleteAll();
  }

  @Override
  public void delete(Iterable<? extends KigaIdea> entities) {
    this.getRepository().delete(entities);
  }

  @Override
  public void delete(KigaIdea kigaIdea) {
    this.getRepository().delete(kigaIdea);
  }

  @Override
  public void delete(Long id) {
    this.getRepository().delete(id);
  }

  @Override
  public long count() {
    return this.getRepository().count();
  }

  @Override
  public <S extends KigaIdea> S save(S entity) {
    return (S) this.getRepository().save(entity);
  }

  @Override
  public <S extends KigaIdea> Iterable<S> save(Iterable<S> entities) {
    return (Iterable<S>) this.getRepository().save(entities);
  }

  @Override
  public boolean exists(Long id) {
    return this.getRepository().exists(id);
  }
}
