package com.kiga.content.sorttree.spec;

/**
 * Created by rainerh on 29.03.17.
 */
public interface SortTree extends Comparable<SortTree> {
  String getSortTree();

  void setSortTree(String sortTreeValue);

  int getSort();

  void setSort(int sort);


  @Override
  default int compareTo(SortTree sortTree) {
    return this.getSort() > sortTree.getSort() ? 1 : -1;
  }
}
