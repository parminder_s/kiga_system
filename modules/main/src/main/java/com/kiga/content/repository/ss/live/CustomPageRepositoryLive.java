package com.kiga.content.repository.ss.live;

import com.kiga.content.domain.ss.live.CustomPageLive;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 22.08.16.
 */
public interface CustomPageRepositoryLive extends CrudRepository<CustomPageLive, Long> {
}
