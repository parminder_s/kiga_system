package com.kiga.content.repository.ss.live;

import com.kiga.content.domain.ss.live.KigaPageLive;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by asv on 19.01.17.
 */
public interface KigaPageRepositoryLive extends PagingAndSortingRepository<KigaPageLive, Long> {
}
