package com.kiga.content.domain.ss.draft;

import com.kiga.content.domain.ss.Page;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * Created by rainerh on 16.06.16.
 */
@Entity
@Table(name = "Page")
@PrimaryKeyJoinColumn(name = "ID")
public class PageDraft extends SiteTreeDraft implements Page<SiteTreeDraft> {
  private Integer legacyId;
  private String subTitle;
  private int noRobotIndex = 0;
  private Integer inSync;
  private Integer outOfSync;
  private String code;

  public PageDraft() {
    this.setClassName("Page");
  }

  public Integer getLegacyId() {
    return legacyId;
  }

  public void setLegacyId(Integer legacyId) {
    this.legacyId = legacyId;
  }

  public String getSubTitle() {
    return subTitle;
  }

  public void setSubTitle(String subTitle) {
    this.subTitle = subTitle;
  }

  public Integer getNoRobotIndex() {
    return noRobotIndex;
  }

  public void setNoRobotIndex(Integer noRobotIndex) {
    this.noRobotIndex = noRobotIndex;
  }

  public Integer getInSync() {
    return inSync;
  }

  public void setInSync(Integer inSync) {
    this.inSync = inSync;
  }

  public Integer getOutOfSync() {
    return outOfSync;
  }

  public void setOutOfSync(Integer outOfSync) {
    this.outOfSync = outOfSync;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
