package com.kiga.content.service;

import com.kiga.content.domain.ss.IdeaGroupToIdea;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepositoryProxy;
import lombok.AllArgsConstructor;

import java.util.Set;

/**
 * Created by peter on 29.03.17.
 */
@AllArgsConstructor
public class KigaIdeaUrlGenerator {
  private SiteTreeUrlGenerator siteTreeUrlGenerator;
  private IdeaGroupContainerRepositoryProxy ideaGroupContainerRepositoryProxy;

  /**
   * @param idea idea to generate the preview url on deepest layer.
   * @return the url to the preview, or / if there is no ideaGroup.
   */
  public String getRelativePreviewUrl(KigaIdea idea) {
    Set<IdeaGroupToIdea> ideaGoupIdeas = idea.getIdeaGroupToIdeaList();
    IdeaGroupToIdea ideaGroupToIdea = ideaGoupIdeas.stream()
      .filter(item -> item.getIdeaGroup() != null).findFirst().orElse(null);
    if (ideaGroupToIdea == null) {
      return siteTreeUrlGenerator.getRelativeNgUrl(
        ideaGroupContainerRepositoryProxy.findIdeaGroupMainContainer(idea.getLocale()));
    }
    return siteTreeUrlGenerator.getRelativeNgUrl(ideaGroupToIdea.getIdeaGroup()) + "?preview="
      + idea.getUrlSegment() + "&previewCategory=0";
  }
}
