package com.kiga.content.sitemap.service;

import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.content.sitemap.exceptions.UnknownSiteTreeException;
import lombok.extern.log4j.Log4j2;

import java.text.SimpleDateFormat;

import java.util.Arrays;
import java.util.List;

/**
 * Created by peter on 10.05.16.
 */
@Log4j2
public class SiteMapGenerator {

  SiteTreeUrlGenerator siteTreeUrlGenerator;
  SimpleDateFormat simpleDateFormat;
  private List<String> oldSiteTreeList;
  private List<String> ngSiteTreeList;
  private List<String> ignoredSiteTreeList;

  /**
   * inject dependencies.
   */
  public SiteMapGenerator(
    SiteTreeUrlGenerator siteTreeUrlGenerator) {
    this.siteTreeUrlGenerator = siteTreeUrlGenerator;
    this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    // Silverstripe SiteTrees getting the old link format
    this.oldSiteTreeList = Arrays.asList(
      "ErrorPage",
      "HomePage",
      "LostPassword",
      "Forum",
      "FooterPage",
      "ForumHolder"
    );

    this.ignoredSiteTreeList = Arrays.asList(
      "Page",
      "Newsletter",
      "MainDownloadCategory",
      "AphorismusCategory",
      "Aphorismus",
      "CalendarCategory",
      "Calendar",
      "DimensionPage",
      "RegistrationPage",
      "DownloadCategory",
      "ECardCategory",
      "FormularCategory",
      "Formular",
      "GalleryCategory",
      "Gallery",
      "MandalaCategory",
      "Mandala",
      "CartoonCategory",
      "Cartoon",
      "DraftContainer",
      "DraftFolder",
      "ECard",
      "ECardCategory",
      "GiftActivator",
      "JobPage",
      "EventPage",
      "FeedbackPage",
      "MainArticleCategory",
      "ArticleCategory",
      "ArticleCategoryContainer",
      "KigaCategory",
      "KigaDownload",
      "KigaDownloadFile",
      "KigaDownloadImage",
      "CustomPage",
      "KigaPage",
      "MainServicePage",
      "Newsletter",
      "NewsletterArchive",
      "NewsletterContainer",
      "NewsletterSubscribe",
      "NewsletterUnsubscribe",
      "NoJavaScript",
      "PaymentPage",
      "PayPalPage",
      "PlanPage",
      "ProductPageHolder",
      "RedirectorPage",
      "ProductPage",
      "RegistrationJob",
      "RegistrationOrg",
      "RegTypeContainer",
      "Service_BillAddress",
      "Service_BillData",
      "Service_ChangeType",
      "Service_CustomerData",
      "Service_EMail",
      "Service_Invoices",
      "Service_IP",
      "Service_LicenceUser",
      "Service_Locale",
      "Service_MiscData",
      "Service_Overview",
      "Service_Password",
      "Service_Profile",
      "ServiceLicence",
      "ServicePage",
      "SiteTree",
      "VirtualPage",
      ""
    );

    this.ngSiteTreeList = Arrays.asList(
      "Article",
      "HomePage",
      "RootPage",
      "IdeaGroupMainContainer",
      "IdeaGroupContainer",
      "IdeaGroupCategory",
      "IdeaGroup",
      "ShopArticlePage",
      "ShopMainContainer",
      "ShopCategory"
    );
  }

  /**
   *
   * @param siteTreeList list of siteTreeElements, from which to create a sitemap.
   * @return the sitemap in xml as String.
   */
  public String getSiteMap(Iterable<? extends SiteTree> siteTreeList) {
    StringBuilder returner = new StringBuilder();
    returner.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      + "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");

    for (SiteTree siteTree : siteTreeList) {
      returner.append(generateXmlForSiteTree(siteTree));
    }

    returner.append("\n</urlset>");
    return returner.toString();
  }

  /**
   *
   * @param siteTree the current siteTree element to create the sitemap entry.
   * @return the sitemap xml entry for that siteTree element.
   */
  public String generateXmlForSiteTree(SiteTree siteTree) {

    if (siteTree.getUrlSegment() == null) {
      return "";
    }

    String url = getUrl(siteTree);

    if (url.equals("")) {
      return "";
    }

    return "<url>"
      + "\n<loc>"
      + url
      + "</loc>"
      + "\n<lastmod>"
      + simpleDateFormat.format(siteTree.getLastEdited())
      + "</lastmod>"
      + "\n<changefreq>"
      + SiteMapHelper.getChangeFreq(siteTree.getLastEdited())
      + "</changefreq>"
      + "\n<priority>"
      + SiteMapHelper.getPriority(siteTree.getPriority())
      + "</priority>"
      + "\n</url>\n";
  }

  /**
   * return Url based on siteTreeLists.
   */
  private String getUrl(SiteTree siteTree) throws UnknownSiteTreeException {

    if (ngSiteTreeList.contains(siteTree.getClassName())) {
      return siteTreeUrlGenerator.getNgUrl(siteTree);
    } else if (oldSiteTreeList.contains(siteTree.getClassName())) {
      return siteTreeUrlGenerator.getUrl(siteTree);
    } else if (ignoredSiteTreeList.contains(siteTree.getClassName())) {
      return "";
    }
    throw new UnknownSiteTreeException(
      "SiteTree extending class \""
        + siteTree.getClassName() + "\" is neither old, angular nor ignored");
  }

  /**
   * Test only.
   */
  public List<String> getOldSiteTreeList() {
    return oldSiteTreeList;
  }

  /**
   * Test only.
   */
  public List<String> getNgSiteTreeList() {
    return ngSiteTreeList;
  }
}
