package com.kiga.content.repository.ss.live;

import com.kiga.content.domain.ss.live.IdeaGroupToIdeaLive;
import org.springframework.data.repository.CrudRepository;


import java.util.List;

/**
 * @author bbs
 * @since 5/13/16.
 */
public interface IdeaGroupToIdeaRepositoryLive extends CrudRepository<IdeaGroupToIdeaLive, Long> {
  List<IdeaGroupToIdeaLive> findById(Long id);

  List<IdeaGroupToIdeaLive> findByIdeaGroupId(Long ideaGroupId);

  List<IdeaGroupToIdeaLive> findByArticleId(Long articleId);

}
