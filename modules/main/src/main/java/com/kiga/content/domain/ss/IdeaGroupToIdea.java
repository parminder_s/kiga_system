package com.kiga.content.domain.ss;

import com.kiga.content.sorttree.spec.SortTree;

/**
 * Created by rainerh on 27.06.16.
 */
public interface IdeaGroupToIdea<T extends SiteTree, K extends KigaIdea, I extends IdeaGroup>
  extends SortTree {
  K getArticle();

  void setArticle(K article);

  I getIdeaGroup();

  void setIdeaGroup(I ideaGroup);

  int getSortOrder();

  void setSortOrder(int sortOrder);
}
