package com.kiga.content.repository.ss.abstraction;

import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.repository.ss.AbstractRepositoryProxy;
import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.main.locale.Locale;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by rainerh on 14.08.16.
 */
public class IdeaGroupContainerRepositoryProxy
  extends AbstractRepositoryProxy<IdeaGroupContainerRepository>
  implements IdeaGroupContainerRepository<IdeaGroupContainer> {

  public IdeaGroupContainerRepositoryProxy(
    IdeaGroupContainerRepository repositoryLive,
    IdeaGroupContainerRepository repositoryDraft,
    RepositoryContext repositoryContext) {
    super(repositoryLive, repositoryDraft, repositoryContext);
  }

  @Override
  public IdeaGroupContainer findOne(Long id) {
    return this.getRepository().findOne(id);
  }

  @Override
  public List<IdeaGroupContainer> findByIdAndClassName(Long id, String className) {
    return this.getRepository().findByIdAndClassName(id, className);
  }

  @Override
  public List<IdeaGroupContainer> findRootSubgroups(@Param("locale") Locale locale) {
    return this.getRepository().findRootSubgroups(locale);
  }

  @Override
  public List<IdeaGroupContainer> findSubgroups(
    @Param("locale") Locale locale, @Param("urlSegment") String urlSegment,
    @Param("level") int level) {
    return this.getRepository().findSubgroups(locale, urlSegment, level);
  }

  @Override
  public IdeaGroupContainer findIdeaGroupMainContainer(@Param("locale") Locale locale) {
    return this.getRepository().findIdeaGroupMainContainer(locale);
  }
}
