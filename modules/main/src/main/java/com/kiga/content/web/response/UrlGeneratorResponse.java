package com.kiga.content.web.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mfit on 16.08.17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UrlGeneratorResponse {
  String url;
  String categorySegment;
}
