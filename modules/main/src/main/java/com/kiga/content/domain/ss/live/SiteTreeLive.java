package com.kiga.content.domain.ss.live;

import com.kiga.content.domain.ss.SiteTreeModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/** Created by rainerh on 11.03.16. */
@Table(name = "SiteTree_Live")
@Inheritance(strategy = InheritanceType.JOINED)
@Entity
public class SiteTreeLive extends SiteTreeModel<SiteTreeLive> {
  @Id
  @Column(name = "ID")
  private Long id;

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }
}
