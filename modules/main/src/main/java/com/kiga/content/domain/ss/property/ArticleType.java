package com.kiga.content.domain.ss.property;

/**
 * @author bbs
 * @since 3/30/17.
 */
public enum ArticleType {
  DEFAULT(0),
  ONE(1),  //todo find out meaning, add correct names!
  TWO(2),
  THREE(3),
  FOUR(4),
  FIVE(5),
  SIX(6),
  DOWNLOAD(7);

  private Integer databaseValue;

  ArticleType(Integer databaseValue) {
    this.databaseValue = databaseValue;
  }

  public Integer getDatabaseValue() {
    return databaseValue;
  }

  /**
   * Find ArticleType by databaseValue (as opposite to the enum name itself).
   *
   * @param databaseValue database value
   * @return enum
   */
  public static ArticleType valueOf(Integer databaseValue) {
    if (databaseValue == null) {
      return DEFAULT;
    }

    ArticleType[] values = ArticleType.values();
    for (ArticleType value : values) {
      if (databaseValue.equals(value.databaseValue)) {
        return value;
      }
    }

    return null;
  }
}
