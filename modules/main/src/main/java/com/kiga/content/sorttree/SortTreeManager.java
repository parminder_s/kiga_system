package com.kiga.content.sorttree;

import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.sorttree.helper.UnusedSortTreeNullSetter;
import com.kiga.content.sorttree.spec.SortTreeElement;
import com.kiga.ideas.task.sorttree.IdeaHierarchySortTreeCalculatorMapper;
import java.util.List;
import javax.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/** Created by Florian Schneider on 07.11.17. */
@Service
@Log4j2
public class SortTreeManager {

  protected SiteTreeLiveRepository siteTreeLiveRepository;
  IdeaCategoryLive sortTreeParent;
  SortTreeElement sortTreeHierarchy;
  private UnusedSortTreeNullSetter unusedSortTreeNullSetter;

  /** constructor. */
  @Autowired
  public SortTreeManager(
      SiteTreeLiveRepository siteTreeLiveRepository,
      UnusedSortTreeNullSetter unusedSortTreeNullSetter) {
    this.siteTreeLiveRepository = siteTreeLiveRepository;
    this.unusedSortTreeNullSetter = unusedSortTreeNullSetter;
  }

  /** Recalculate whole SortTree. */
  @Async
  @Transactional
  public void recalculateSortTree() {
    log.info("SortTreeManager: Recalculate SortTree");
    retrieveRepositoryData();
    mapSortTree();
    updateSortTree();
    nullifyUnusedSortTrees();
    log.info("SortTreeManager: Recalculate SortTree finished");
  }

  /** Retrieve the MainContainer from the SiteTreeLive Repository. */
  void retrieveRepositoryData() {
    List<SiteTreeLive> treeLive = siteTreeLiveRepository.findByClassName("IdeaGroupMainContainer");
    sortTreeParent = new IdeaCategoryLive();
    sortTreeParent.setChildren(treeLive);
  }

  /** Map SortTree Classes to SortTreeElements. */
  void mapSortTree() {
    IdeaHierarchySortTreeCalculatorMapper mapper = new IdeaHierarchySortTreeCalculatorMapper();
    sortTreeHierarchy = mapper.map(sortTreeParent);
  }

  /** Forward SortTree to Calculator and update SortTree. */
  void updateSortTree() {
    SortTreeRelationCalculator sortTreeRelationCalculator = new SortTreeRelationCalculator();
    SortTreeCalculator sortTreeCalculator = new SortTreeCalculator(sortTreeRelationCalculator);
    sortTreeCalculator.updateBranch(sortTreeHierarchy);
  }

  void nullifyUnusedSortTrees() {
    unusedSortTreeNullSetter.setUnusedToNull();
  }
}
