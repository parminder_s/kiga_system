package com.kiga.content.repository.ss.draft;

import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.repository.ss.abstraction.KigaIdeaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author bbs
 * @since 4/21/16.
 */
public interface KigaIdeaRepositoryDraft extends
  QueryDslPredicateExecutor<KigaIdeaDraft>,
  KigaIdeaRepository<KigaIdeaDraft>,
  PagingAndSortingRepository<KigaIdeaDraft, Long> {
}
