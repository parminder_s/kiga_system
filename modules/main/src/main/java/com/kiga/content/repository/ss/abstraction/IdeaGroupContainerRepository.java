package com.kiga.content.repository.ss.abstraction;

import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.main.locale.Locale;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by rainerh on 13.08.16.
 */
public interface IdeaGroupContainerRepository<T extends IdeaGroupContainer> {
  T findOne(Long id);

  List<T> findByIdAndClassName(Long id, String className);

  @Query("FROM #{#entityName} ig WHERE ig.locale = :locale "
    + "and ig.className = 'IdeaGroupMainContainer'")
  T findIdeaGroupMainContainer(@Param("locale") Locale locale);

  @Query("FROM #{#entityName} g WHERE g.locale = :locale and g.parent.className = "
    + "'IdeaGroupMainContainer'")
  List<T> findRootSubgroups(@Param("locale") Locale locale);

  @Query("FROM #{#entityName} g WHERE g.locale = :locale and g.parent.urlSegment = :urlSegment "
    + "and 'IdeaGroupMainContainer' = (CASE WHEN :level = 0 THEN g.parent.urlSegment "
    + "WHEN :level = 1 THEN g.parent.parent.urlSegment "
    + "WHEN :level = 2 THEN g.parent.parent.parent.urlSegment "
    + "WHEN :level = 3 THEN g.parent.parent.parent.parent.urlSegment "
    + "ELSE '' END) ")
  List<T> findSubgroups(
    @Param("locale") Locale locale, @Param("urlSegment") String urlSegment,
    @Param("level") int level);
}
