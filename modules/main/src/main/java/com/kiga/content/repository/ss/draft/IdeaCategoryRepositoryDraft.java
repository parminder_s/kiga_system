package com.kiga.content.repository.ss.draft;

import com.kiga.content.domain.ss.draft.IdeaCategoryDraft;
import com.kiga.content.repository.ss.abstraction.IdeaCategoryRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * @author bbs
 * @since 7/2/16.
 */
public interface IdeaCategoryRepositoryDraft
  extends IdeaCategoryRepository, CrudRepository<IdeaCategoryDraft, Long> {

}
