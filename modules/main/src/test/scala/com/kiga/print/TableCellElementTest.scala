package com.kiga.print

import com.kiga.print.model.{Block, TableCellElement}
import org.junit.{Assert, Test}

/**
  * Created by peter on 24.02.16.
  */
class TableCellElementTest {
  @Test def tableCellElementTest: Unit = {
    var t1 = new TableCellElement("text t1",true)
    var t2 = new TableCellElement("text t1",false, true, "left", "5cm")
    var t3 = new TableCellElement("")

    var block = new Block("block text")

    t1.setChildren(List(block))
    t2.setChildren(List(block))

    Assert.assertEquals("tableCell 1", <fo:table-cell font-weight="normal" text-align="right" border="solid 0.2mm gray" padding="0.5mm" width=""><fo:block>text t1</fo:block><fo:block>block text</fo:block></fo:table-cell>, t1.getFop())
    Assert.assertEquals("tableCell 2", <fo:table-cell font-weight="bold" text-align="left" border="" padding="0.5mm"><fo:block>text t1</fo:block><fo:block>block text</fo:block></fo:table-cell>, t2.getFop())
    Assert.assertEquals("tableCell 3", <fo:table-cell font-weight="normal" text-align="right" border="solid 0.2mm gray" padding="0.5mm" width=""><fo:block></fo:block></fo:table-cell>, t3.getFop())
  }
}
