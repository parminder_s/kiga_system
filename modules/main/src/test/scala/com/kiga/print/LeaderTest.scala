package com.kiga.print

import com.kiga.print.model.Leader
import org.junit.{Test, Assert}
import org.slf4j.LoggerFactory

/**
  * Created by peter on 24.02.16.
  */
class LeaderTest {
  val log = LoggerFactory.getLogger(getClass)

  @Test def leaderTest = {
    var l1 = new Leader()
    var l2 = new Leader()
    var l3 = new Leader()

    l1.setLeaderLenghtMM(1)
    l1.setLeaderPattern("pattern")
    l1.setLeaderPatternWidthPT(2)

    l3.setLeaderLenghtPercent(100)

    var x = l3.getFop().toString()

    Assert.assertEquals("leader 1", <fo:leader leader-pattern="pattern" leader-pattern-width="2pt" leader-length="1mm"></fo:leader>, l1.getFop())
    Assert.assertEquals("leader 2", <fo:leader></fo:leader>, l2.getFop())
    Assert.assertEquals("leader 3", <fo:leader leader-length="100%"></fo:leader>, l3.getFop())
  }
}
