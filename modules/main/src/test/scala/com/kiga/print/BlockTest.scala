package com.kiga.print

import com.kiga.print.model.{Block}
import org.junit.{Assert, Test}
import org.slf4j.LoggerFactory

/**
  * Created by peter on 23.02.16.
  */
class BlockTest{

  val log = LoggerFactory.getLogger(getClass)

  @Test def blockTest{
    var b1: Block = new Block()

    b1.setText("Der Text in einem Block")
    b1.setBorder("1")
    b1.setBorderTopStyle("solid")
    b1.setColor("gray")
    b1.setFontSize(2)
    b1.setFontWeight("bold")
    b1.setMarginBottomMM(3)
    b1.setMarginLeftMM(4)
    b1.setMarginMM(5)
    b1.setMarginRightMM(6)
    b1.setMarginTopMM(7)
    b1.setPaddingMM(8)
    b1.setTextAlign("right")

    var b2: Block = new Block("b2 text")
    var b3: Block = new Block("b3 text")

    b2.setFontSize(9)
    b3.setFontWeight("bold")

    b1.setChildren(List(b2,b3))

    Assert.assertEquals("block 2", <fo:block font-size="9">b2 text</fo:block>, b2.getFop() )
    Assert.assertEquals("block 3", <fo:block font-weight="bold">b3 text</fo:block>, b3.getFop())
    Assert.assertEquals("block 1", <fo:block color="gray" font-weight="bold" margin="5mm" padding="8mm" font-size="2" margin-top="7mm" margin-bottom="3mm" margin-left="4mm" margin-right="6mm" text-align="right" border-top-style="solid" border="1">Der Text in einem Block<fo:block font-size="9">b2 text</fo:block><fo:block font-weight="bold">b3 text</fo:block></fo:block>, b1.getFop())
  }
}
