package com.kiga.print

import com.kiga.print.model.Inline
import org.junit.{Assert, Test}

/**
  * Created by peter on 24.02.16.
  */
class InlineTest {

  @Test def inlineTest: Unit = {
    var i1:Inline = new Inline()
    var i2:Inline = new Inline()
    var i3:Inline = new Inline()

    i1.setColor("gray")
    i1.setFontFamily("arial")
    i1.setFontSize(15)
    i1.setText("First")

    i2.setColor("blue")
    i2.setText("Second")

    i3.setText("Third")

    i1.setChildren(List(i2,i3))

    var x = i1.getFop();

    Assert.assertEquals("inline 1", <inline font-family="arial" font-size="15" color="gray">First<inline color="blue">Second</inline><inline>Third</inline></inline>, i1.getFop()) //<inline font-family="arial" font-size="15" color="gray"></inline>
    Assert.assertEquals("inline 2", <inline color="blue">Second</inline>, i2.getFop())
    Assert.assertEquals("inline 3", <inline>Third</inline>, i3.getFop())
  }
}
