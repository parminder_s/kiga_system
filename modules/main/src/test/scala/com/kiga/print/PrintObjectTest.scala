package com.kiga.print

import java.io.BufferedInputStream

import com.kiga.print.article.FopXml
import com.kiga.print.model.{PrintObject, Block}
import org.junit.{Test, Assert}
import org.slf4j.LoggerFactory

/**
  * Created by peter on 24.02.16.
  */
class PrintObjectTest {

  @Test def printObjectTest = {

    val log = LoggerFactory.getLogger(getClass)

    var header = new Block()
    var body = new Block()
    var footer = new Block()
    header.setText("header")
    body.setText("body")
    footer.setText("footer")

    var obj = new PrintObject(List(header),List(body),List(footer))
    var outStream = FopXml.getFopStream(obj.getFop().toString())
    outStream.close()
  }
}
