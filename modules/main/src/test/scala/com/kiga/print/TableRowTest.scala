package com.kiga.print

import com.kiga.print.model.{TableCellElement, TableRow}
import org.junit.{Test,Assert}

/**
  * Created by peter on 24.02.16.
  */
class TableRowTest {

  @Test def tableRowTest() = {
    var t1 = new TableRow()
    var t2 = new TableRow()
    var t3 = new TableRow()

    t1.setBorder("1")
    t1.setKeepTogetherWithinColumn("keepTogether")
    t1.setMarginBottomMM(2)
    t1.setMarginLeftMM(3)
    t1.setMarginRightMM(4)
    t1.setMarginTopMM(5)

    t1.setText("TableRow 1 text") //should be ignored
    t2.setText("TableRow 2 text") //should be ignored

    var cell = new TableCellElement("Table Cell")

    t1.setChildren(List(cell))
    t3.setChildren(List(cell))

    var x = t3.getFop().toString()
    Assert.assertEquals("tableRow 1", <fo:table-row keep-together.within-column="keepTogether" border="1" margin-top="5mm" margin-bottom="2mm" margin-left="3mm" margin-right="4mm"><fo:table-cell font-weight="normal" text-align="right" border="solid 0.2mm gray" padding="0.5mm" width=""><fo:block>Table Cell</fo:block></fo:table-cell></fo:table-row>, t1.getFop())
    Assert.assertEquals("tableRow 2", <fo:table-row></fo:table-row>, t2.getFop())
    Assert.assertEquals("tableRow 3", <fo:table-row><fo:table-cell font-weight="normal" text-align="right" border="solid 0.2mm gray" padding="0.5mm" width=""><fo:block>Table Cell</fo:block></fo:table-cell></fo:table-row>, t3.getFop())

  }

}
