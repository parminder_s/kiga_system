package com.kiga.print

import com.kiga.print.autocorrect.{AutoCorrect, InvalidXmlException}
import com.kiga.print.invoice.InvoiceToPdf
import com.kiga.print.validation.{DefaultValidateXML, ValidateXML, ValidationHelper}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FlatSpec, Matchers}
import org.slf4j.LoggerFactory


/**
 * Created by peter on 24.06.15.
 */
@RunWith(classOf[JUnitRunner])
class PrintTest extends FlatSpec with Matchers {
  val log = LoggerFactory.getLogger(getClass)
  var validateXML: ValidateXML = new DefaultValidateXML()
  var invoicetToPdf: InvoiceToPdf = new InvoiceToPdf()

  it should "test validation with invalid xml" in {
    var gotException = false
    try {
      var xml = validateXML.loadXmlString("<outer><test>asdf</test><someother>")
    }
    catch {
      case ex: InvalidXmlException => gotException = true
      case e: Exception => gotException = true
    }
    gotException shouldBe true
  }

  it should "test validation with valid xml" in {
    var gotException = true
    try {
      var xml = validateXML.loadXmlString("<outer><test>asdf</test></outer>")
      gotException = false
    }
    catch {
      case ex: InvalidXmlException => gotException = true
    }
    gotException shouldBe false
  }

  it should "test removeAttribue" in {
    val input1 = """<span class="zeilenabstand5" style="test">This is Text</span>"""
    val res1 = AutoCorrect.removeAttribute(
      xml.XML.loadString(ValidationHelper.prettyFormatXML(input1)),
      "span",
      "class")
    println(res1.toString())
    res1 shouldBe <span style="test">This is Text</span>
  }

  it should "test removeInvalidElement" in {

    var x10 = """<span delete="true"><span delete="true">a 3:<span style="white-space: pre;">Â </span></span></span>"""
    var t10 = AutoCorrect.removeInvalidElement(xml.XML.loadString(x10), "span", Array[String]("All"))
    t10 shouldBe """a 3:<span style="white-space: pre;">Â </span>"""

    var x5 = """<div><div><hr/></div></div>"""
    var t5 = AutoCorrect.removeInvalidElement(xml.XML.loadString(x5), "div",
      Array[String](""))
    t5.toString shouldBe "<hr/>"

    var x1 = """<span style="font-size: x-small;"><br/></span>"""
    var t1 = AutoCorrect.removeInvalidElement(xml.XML.loadString(x1), "span",
      Array[String](""))
    t1.toString shouldBe """"""

    val input2 = """<li><span class="zeilenabstand5" style="test">This is Text</span></li>"""
    val res2 = AutoCorrect.removeInvalidElement(
      xml.XML.loadString(ValidationHelper.prettyFormatXML(input2)),
      "li",
      Array[String]("span"))
    var x = res2.toString()
    res2.toString shouldBe "\n<span class=\"zeilenabstand5\" style=\"test\">This is Text</span>\n" // \n caused by prettyFormat
    val input3 = """<span> </span>"""
    val res3 = AutoCorrect.removeInvalidElement(
      xml.XML.loadString(ValidationHelper.prettyFormatXML(input3)),
      "span",
      Array[String]())
    res3.toString shouldBe " "

    val input4 = """<span> </span>"""
    val res4 = AutoCorrect.removeInvalidElement(
      xml.XML.loadString(ValidationHelper.prettyFormatXML(input3)),
      "span",
      Array[String]())
    res4.toString shouldBe " "

  }

  it should "test autocorrect recursiveRemove" in {
    var x5 = """<div><div><hr/></div></div>"""
    var t5 = AutoCorrect.recursiveRemove(xml.XML.loadString(x5), "div")
    t5._2 shouldBe "<hr/>"

    var x4 = """<div><div><div><p class="zeilenabstand1"><strong>Basi</strong></p><span class="zeilenabstand1"> </span><ul><span class="zeilenabstand1"> </span><li><span class="zeilenabstand1">Be careful with your partner!<br/></span><p class="zeilenabstand1">Pressing too firmly, pinching and tickling are not allowed!</p></li><span class="zeilenabstand1"> </span><li class="zeilenabstand1"> Any child who is being massaged may stop the massage if it is taking too long or is uncomfortable. </li><span class="zeilenabstand1"> </span><li class="zeilenabstand1"> Do not massage the spine!</li><span class="zeilenabstand1"> </span><li class="zeilenabstand1"> The teacher should demonstrate proper massage techniques on a volunteer child for the rest of the class to watch. </li><span class="zeilenabstand1"> </span><li><span class="zeilenabstand1">Â Some children may prefer to be massaged by the teacher.</span></li></ul></div></div></div>"""
    var t4 = AutoCorrect.recursiveRemove(xml.XML.loadString(x4), "div")
    t4._2 shouldBe """<p class="zeilenabstand1"><strong>Basi</strong></p><span class="zeilenabstand1"> </span><ul><span class="zeilenabstand1"> </span><li><span class="zeilenabstand1">Be careful with your partner!<br/></span><p class="zeilenabstand1">Pressing too firmly, pinching and tickling are not allowed!</p></li><span class="zeilenabstand1"> </span><li class="zeilenabstand1"> Any child who is being massaged may stop the massage if it is taking too long or is uncomfortable. </li><span class="zeilenabstand1"> </span><li class="zeilenabstand1"> Do not massage the spine!</li><span class="zeilenabstand1"> </span><li class="zeilenabstand1"> The teacher should demonstrate proper massage techniques on a volunteer child for the rest of the class to watch. </li><span class="zeilenabstand1"> </span><li><span class="zeilenabstand1">Â Some children may prefer to be massaged by the teacher.</span></li></ul>"""
    t4._1 shouldBe 33

    var x3 = """<span class="test">1<span class="test">2<span>3<span>4<span>5<span>6</span></span></span></span></span></span>"""
    var t3 = AutoCorrect.recursiveRemove(xml.XML.loadString(x3), "span")
    t3._2 shouldBe "123456" //new content
    t3._1 shouldBe 78 + 2 * 13 //removed length
    var x2 = """<span>1<span>2<span>3<span>4<span>5<span>6</span></span></span></span></span></span>"""
    var t2 = AutoCorrect.recursiveRemove(xml.XML.loadString(x2), "span")
    t2._2 shouldBe "123456" //new content
    t2._1 shouldBe 78 //removed length
    var x1 = """<span><span><span><span><span><span>A</span></span></span></span></span></span>"""
    var t1 = AutoCorrect.recursiveRemove(xml.XML.loadString(x1), "span")
    t1._1 shouldBe 78 //removed length
    t1._2 shouldBe "A" //new content
    var x = """<span>1<span>2<span>3<span>4<span>5<span>6</span></span>7</span>8</span>9</span>0</span>"""
    var t = AutoCorrect.recursiveRemove(xml.XML.loadString(x), "span")
    t._2 shouldBe "1234567890" //new content
    t._1 shouldBe 78 //removed length

  }

  it should "test autocorrect Whitespaces" in {
    var x17 = """<p>He said:" <span> Yeeah </span> " . Test  . </p>"""
    var t17 = AutoCorrect.validateSpaceAndSpecialCharacters(x17)
    t17._1 shouldBe """<p>He said: "<span>Yeeah</span>". Test. </p>"""

    var x8 = """<p>He said:" <span> Yeeah </span> "  </p> """
    var t8 = AutoCorrect.validateSpaceAndSpecialCharacters(x8)
    t8._1 shouldBe """<p>He said: "<span>Yeeah</span>" </p>"""

    var x5 = """<p class="zeilenabstand2"><span><img src="http://kiga.s3.amazonaws.com" /> As the  rose was  very  tall , Charly  asked : " Howdy ,  Rose !  Can you  please tell me  if there are  rain  clouds  in the sky? I am sure y ou can  see  very  far  up there ! "    " Oh my goodness, I should talk to a green sticky, wet frog? What does this animal think who it is?" And without saying a word,the rose began to sway in the wind. </span></p>"""
    var t5 = AutoCorrect.validateSpaceAndSpecialCharacters(x5)
    t5._1 shouldBe """<p class="zeilenabstand2"><span><img src="http://kiga.s3.amazonaws.com" />As the rose was very tall, Charly asked: "Howdy, Rose! Can you please tell me if there are rain clouds in the sky? I am sure y ou can see very far up there!" "Oh my goodness, I should talk to a green sticky, wet frog? What does this animal think who it is?" And without saying a word, the rose began to sway in the wind. </span></p>"""

    var x4 = """<p class="zeilenabstand2"><span>One morning,"asdf  "  </span></p>"""
    var t4 = AutoCorrect.validateSpaceAndSpecialCharacters(x4)
    t4._1 shouldBe """<p class="zeilenabstand2"><span>One morning, "asdf" </span></p>"""

    var x16 = """<div><p>"  no ending  quote, </p>  <p>"but this with quote "  .</p></div>"""
    var u16 = AutoCorrect.validateSpaceAndSpecialCharacters(x16)
    u16._1 shouldBe """<div><p>"no ending quote, </p><p>"but this with quote". </p></div>"""

    var x15 = """<p>test  !  </p>"""
    var u15 = AutoCorrect.validateSpaceAndSpecialCharacters(x15)
    u15._1 shouldBe """<p>test! </p>"""

    var x11 = """<p>"attentively   , then she giggled  !"</p> """
    var t11 = AutoCorrect.validateSpaceAndSpecialCharacters(x11)
    t11._1 shouldBe """<p>"attentively, then she giggled!" </p>"""

    var x14 = """<p> </p>"""
    var t14 = AutoCorrect.validateSpaceAndSpecialCharacters(x14)
    t14._1 shouldBe """<p> </p>"""

    var x13 = """<p><span> away.   </span> <span> How </span></p>"""
    var t13 = AutoCorrect.validateSpaceAndSpecialCharacters(x13)
    t13._1 shouldBe """<p><span>away. </span><span>How </span></p>"""

    var x3 = """<p class="zeilenabstand2"><span> The rose was outraged ! Quick as a flash , the rose folded its petals and chased Erika away.   </span> <span> How could this flying bluebottle with its dirty , pollen -covered petals   ! </span></p>"""
    var t3 = AutoCorrect.validateSpaceAndSpecialCharacters(x3)
    t3._1 shouldBe """<p class="zeilenabstand2"><span>The rose was outraged! Quick as a flash, the rose folded its petals and chased Erika away. </span><span>How could this flying bluebottle with its dirty, pollen-covered petals! </span></p>"""

    var x = """<p><span> Contemptuously   the rose </span>     <span> turned off </span> <span> . Did this slimy snail really think that the noble , fine rose would talk to her ?Ugh, how horrible! </span></p>"""
    var t = AutoCorrect.validateSpaceAndSpecialCharacters(x)
    t._1 shouldBe """<p><span>Contemptuously the rose </span><span>turned off</span><span>. Did this slimy snail really think that the noble, fine rose would talk to her? Ugh, how horrible! </span></p>"""

    var x12 = """<p>Test     and    nibbled   at her   . It was very uncomfortable   .</p>"""
    var t12 = AutoCorrect.validateSpaceAndSpecialCharacters(x12)
    t12._1 shouldBe """<p>Test and nibbled at her. It was very uncomfortable. </p>"""

    var x1 = """<p><span> Contemptuously the rose   </span>   <span>     turned off  </span> <span> . Did this slimy snail really think that the noble , fine rose would talk to her ?Ugh, how horrible      !   </span></p>"""
    var t1 = AutoCorrect.validateSpaceAndSpecialCharacters(x)
    t1._1 shouldBe """<p><span>Contemptuously the rose </span><span>turned off</span><span>. Did this slimy snail really think that the noble, fine rose would talk to her? Ugh, how horrible! </span></p>"""

    var x2 = """<p><span> Contemptuously   the rose </span>    <span> turned off </span>   <span>  . Did this slimy snail really think that the noble , fine rose would talk to her   ?Ugh, how horrible!   </span> </p>"""
    var t2 = AutoCorrect.validateSpaceAndSpecialCharacters(x)
    t2._1 shouldBe """<p><span>Contemptuously the rose </span><span>turned off</span><span>. Did this slimy snail really think that the noble, fine rose would talk to her? Ugh, how horrible! </span></p>"""

    var x9 = """<p>Yellow - <span> like the sun and the ripe ears of corn </span> .</p>"""
    var t9 = AutoCorrect.validateSpaceAndSpecialCharacters(x9)
    t9._1 shouldBe """<p>Yellow-<span>like the sun and the ripe ears of corn</span>. </p>"""

    var x7 = """<p>He  <span> said: </span> "  Yeeah"</p> """
    var t7 = AutoCorrect.validateSpaceAndSpecialCharacters(x7)
    t7._1 shouldBe """<p>He <span>said: </span>"Yeeah" </p>"""

    var x6 = """<p>Hello<p> this   </p>  Is  <span> a   </span>  hard<p> Test    <span>      For <em> my </em></span>   </p> For AutoCorrect.</p>"""
    var t6 = AutoCorrect.validateSpaceAndSpecialCharacters(x6)
    t6._1 shouldBe """<p>Hello <p>this </p>Is <span>a </span>hard <p>Test <span>For <em>my </em></span></p>For AutoCorrect. </p>"""

    var red01 = """<p>a <strong>b</strong> <strong> </strong> c .</p> """
    var red01Corr = AutoCorrect.validateSpaceAndSpecialCharacters(red01)
    red01Corr._1 shouldBe """<p>a <strong>b </strong>c. </p>"""

    var red02 = """<p>of Jeremy 's accident, carefully examined.</p>"""
    var red02Corr = AutoCorrect.validateSpaceAndSpecialCharacters(red02)
    red02Corr._1 shouldBe("""<p>of Jeremy's accident, carefully examined. </p>""")

    var red03 = """<p>"I was really lucky" , Jeremy murmured and wanted to jump away.</p>"""
    var red03Corr = AutoCorrect.validateSpaceAndSpecialCharacters(red03)
    red03Corr._1 shouldBe """<p>"I was really lucky", Jeremy murmured and wanted to jump away. </p>"""

    var red04 = """<p>test -asdf!</p>"""
    var red04Corr = AutoCorrect.validateSpaceAndSpecialCharacters(red04);
    red04Corr._1 shouldBe """<p>test-asdf! </p>"""

    var red05 = """<p>test- asdf!</p>"""
    var red05Corr = AutoCorrect.validateSpaceAndSpecialCharacters(red05);
    red05Corr._1 shouldBe """<p>test-asdf! </p>"""

    var red06 = """<p>test - asdf!</p>"""
    var red06Corr = AutoCorrect.validateSpaceAndSpecialCharacters(red06);
    red06Corr._1 shouldBe """<p>test-asdf! </p>"""

    var red07 = """<ul><li>Collect. <p class="zeilenabstand5">Children notice which"treasures" have.</p></li></ul>"""
    var red07Corr = AutoCorrect.validateSpaceAndSpecialCharacters(red07);
    red07Corr._1 shouldBe """<ul><li>Collect. <p class="zeilenabstand5">Children notice which "treasures" have. </p></li></ul>"""

    var red08 = """<p><img src="http://k.m/Hl.-Martin-A.png"/></p>"""
    var red08Corr = AutoCorrect.validateSpaceAndSpecialCharacters(red08);
    red08Corr._1 shouldBe """<p><img src="http://k.m/Hl.-Martin-A.png" /> </p>"""

    <p><img src="http://k.m/Hl.Martin-A.png" /> </p>
  }
}
