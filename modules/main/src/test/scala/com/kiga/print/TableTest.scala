package com.kiga.print

import com.kiga.print.model.{TableRow, Table}
import org.junit.{Test,Assert}

/**
  * Created by peter on 24.02.16.
  */
class TableTest {

  @Test def tableTest() = {
    var t1 = new Table()
    var t2 = new Table()

    var row = new TableRow()

    t1.setColumWidthsMM(List(1))
    t1.setPageBreakBefore("break")
    t1.setTableLayout("layout")
    t1.setWidthMM(2)

    t1.setChildren(List(row))

    var x = t1.getFop().toString()
    Assert.assertEquals("table 1", <fo:table table-layout="layout" width="2mm" page-break-before="break"><fo:table-column column-width="1mm"></fo:table-column><fo:table-body><fo:table-row></fo:table-row></fo:table-body></fo:table>, t1.getFop())
    Assert.assertEquals("table 2", <fo:table><fo:table-body></fo:table-body></fo:table>, t2.getFop())
  }

}
