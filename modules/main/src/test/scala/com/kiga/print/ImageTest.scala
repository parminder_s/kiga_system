package com.kiga.print

import com.kiga.print.model.Image
import org.junit.{Assert, Test}
import org.slf4j.LoggerFactory

/**
  * Created by peter on 24.02.16.
  */
class ImageTest {

  val log = LoggerFactory.getLogger(getClass)

  @Test def imageTest: Unit ={
    var i1 = new Image("link")
    i1.setMarginMM(1)
    i1.setContentWidthPx(2)
    i1.setText("testtext") // should be ignored
    var i2 = new Image("otherlink")

    Assert.assertEquals("image 1", <fo:external-graphic margin="1mm" content-width="2px" src="link"></fo:external-graphic>, i1.getFop())
    Assert.assertEquals("image 2", <fo:external-graphic src="otherlink"></fo:external-graphic>, i2.getFop())

  }

}
