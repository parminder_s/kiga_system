package com.kiga.print

import com.kiga.print.model.{Inline, ListItem}
import org.junit.{Test, Assert}

/**
  * Created by peter on 24.02.16.
  */
class ListItemTest {

  @Test def listItemTest() = {

    var body = new Inline()
    body.setText("body text")

    var label = new Inline()
    label.setText("label text")

    var l1 = new ListItem(label, body)
    var l2 = new ListItem(label, body)


    l1.setBody(body)
    l1.setLabel(label)
    l1.setMarginBottomMM(1)
    l1.setMarginLeftMM(2)
    l1.setMarginMM(3)
    l1.setMarginRightMM(4)
    l1.setMarginTopMM(5)
    l1.setPaddingMM(6)

    Assert.assertEquals("listItem 1", <list-item margin="3mm" margin-top="5mm" margin-bottom="1mm" margin-left="2mm" margin-right="4mm"><list-item-label end-indent="label-end()"><inline>label text</inline></list-item-label><list-item-body start-indent="body-start()"><inline>body text</inline></list-item-body></list-item>, l1.getFop())
    Assert.assertEquals("listItem 2", <list-item><list-item-label end-indent="label-end()"><inline>label text</inline></list-item-label><list-item-body start-indent="body-start()"><inline>body text</inline></list-item-body></list-item>, l2.getFop())
  }

}
