package com.kiga.accounting

import com.prowidesoftware.swift.model.MtSwiftMessage
import com.prowidesoftware.swift.model.mt.mt9xx.MT940
import org.scalatest.{FlatSpec, Matchers}
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._

/**
 * Created by rainerh on 14.07.15.
 */
class MT940Test extends FlatSpec with Matchers {
  val logger = LoggerFactory.getLogger(classOf[MT940Test])
  "it" should "parse the test file from Garanti" in {
    val mt940 = scala.io.Source.
      fromFile("src/test/resources/com.kiga.accounting/mt940-garanti.txt", "ISO-8859-9").
      mkString

    val parser = new MT940(new MtSwiftMessage(mt940))
    val field61 = parser.getField61
    logger.info(field61.size().toString)
    field61.map(x => logger.info(x.getComponent1))
  }
}
