package com.kiga.accounting.transactionimport

import java.io.ByteArrayOutputStream
import java.text.{DateFormat, SimpleDateFormat}

import com.kiga.accounting.formats.export.SimpleReportExtractor
import com.kiga.types.{JBigDecimal, JList}
import com.kiga.accounting.helper.CsvWriterFactory
import org.junit.{Assert, Test}


import scala.collection.JavaConversions._
/**
  * Created by mfit on 09.03.16.
  */
class ExportTest {
  val dtf: DateFormat  = new SimpleDateFormat("yyyyMMdd")

  @Test def shopPurchasePaymentsTransactionsExportTest(): Unit = {
    val items = List(
      Array(
        "Order123456",
        dtf.parse("20160316"),
        "PSP45678",
        "at",
        new JBigDecimal(8.40),
        new JBigDecimal(9.40),
        "04312345",
        new JBigDecimal(1.00),
        "MP-CC",
        "EUR",
        "98765"
      ),
      Array(
        "Order123457",
        null,
        "PSP456781",
        "at",
        new JBigDecimal(8.40),
        new JBigDecimal(9.40),
        "04312345",
        new JBigDecimal(1.00),
        "MP-CC",
        "EUR",
        "987651"
      ),
      Array(
        "Order123458",
        dtf.parse("20160316"),
        "PSP456782",
        "at",
        null,
        null,
        "04312345",
        null,
        "MP-CC",
        "EUR",
        "987652"
      )
    )
    val csvWriterFactory = new CsvWriterFactory[Object]
    val responseWriter = csvWriterFactory.getStreamItemWriter(new SimpleReportExtractor)

    val out = new ByteArrayOutputStream()
    responseWriter.setOutputStream(out)
    responseWriter.writeHeader(List("OrderNr", "Date", "PspID", "Country", "Net", "Gross", "InvoiceNr", "Tax",
      "PaymentMethod", "Currency", "AcquirerID"))
    responseWriter.write(items)
    out.flush()

    val lines = out.toString().split("\n")

    Assert.assertEquals("OrderNr;Date;PspID;Country;Net;Gross;InvoiceNr;Tax;PaymentMethod;Currency;AcquirerID", lines{0})
    // Assert.assertEquals("Order123456;16.03.2016;PSP45678;at;8,4;9,4;04312345;1;MP-CC;EUR;98765", lines{1})
    Assert.assertEquals("test;not implemented", lines{1})
  }

}

