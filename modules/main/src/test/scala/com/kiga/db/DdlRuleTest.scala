package com.kiga.db

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FlatSpec, Matchers}

/**
 * Created by rainerh on 21.11.15.
 */
@RunWith(classOf[JUnitRunner])
class DdlRuleTest extends FlatSpec with Matchers {
  "it" should "not allow non-hsqldb jdbc urls" in {
    DdlRule.allowDdl("jdbc:mysql://localhost:13306/kiga") shouldBe false
  }

  "it" should "allow non-hsqldb jdbc urls" in {
    DdlRule.allowDdl("jdbc:hsqldb:mem:ss;db_integration;sql.syntax_mys=true") shouldBe true
  }

  "it" should "properly recognize mysql jdbc url" in {
    DdlRule.isMysqlDb("jdbc:mysql://localhost:13306/kiga") shouldBe true
  }

  "it" should "properly recognize hsqldb jdbc url" in {
    DdlRule.isHsqlDb("jdbc:hsqldb:mem:ss;db_integration;sql.syntax_mys=true") shouldBe true
  }

  "it" should "properly recognize that's not a mysql jdbc url" in {
    DdlRule.isMysqlDb("jdbc:hsqldb:mem:ss;db_integration;sql.syntax_mys=true") shouldBe false
  }

  "it" should "properly recognize that's not a hsqldb jdbc url" in {
    DdlRule.isHsqlDb("jdbc:mysql://localhost:13306/kiga") shouldBe false
  }
}
