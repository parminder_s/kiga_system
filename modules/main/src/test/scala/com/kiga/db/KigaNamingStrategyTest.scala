package com.kiga.db

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FlatSpec, Matchers}

/**
 * Created by rainerh on 21.11.15.
 */
@RunWith(classOf[JUnitRunner])
class KigaNamingStrategyTest extends FlatSpec with Matchers {
  var kigaNamingStrategy = new KigaNamingStrategy()

  "it" should "capitalize table names" in {
    kigaNamingStrategy.classToTableName("kigaEntity") shouldBe "KigaEntity"
    kigaNamingStrategy.classToTableName("KigaEntity") shouldBe "KigaEntity"
  }

  "it" should "capitalize column names" in {
    kigaNamingStrategy.propertyToColumnName("kigaColumn") shouldBe "KigaColumn"
    kigaNamingStrategy.propertyToColumnName("kigaColumn_foo_BAR") shouldBe "KigaColumn_foo_BAR"
  }

  "it" should "uppercase ID in mainly foreign key columns" in {
    kigaNamingStrategy.propertyToColumnName("kigaTableId") shouldBe "KigaTableID"
  }

  "it" should "not accidentally uppercase invalid id endings" in {
    kigaNamingStrategy.propertyToColumnName("forbid") shouldBe "Forbid"
  }

  "it" should "set id to uppercase" in {
    kigaNamingStrategy.propertyToColumnName("id") shouldBe "ID"
    kigaNamingStrategy.propertyToColumnName("Id") shouldBe "ID"
  }

}
