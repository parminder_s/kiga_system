package com.kiga

/**
 * Created by rainerh on 24.02.15.
 */
object ClassMapEqualizer {
  def equals(cc: AnyRef, hashMap: Map[String, Any]) = {
    val otherHashMap = anyToMap(cc)
    hashMap.keys.foreach((key) => {
      if (otherHashMap.contains(key)) {
        if (!hashMap.get(key).get.equals(otherHashMap.get(key).get)) {
          throw new Exception("wrong value at object, should: " + hashMap.get(key).get + ", is: " + otherHashMap.get(key).get)
        }
      }
      else {
        val keys = otherHashMap.keySet.toString
        throw new Exception(s"object does not contain key ${key} only ${keys}")
      }
    })
  }

  //http://stackoverflow.com/questions/1226555/case-class-to-map-in-scala
  def anyToMap(cc: AnyRef) =
    (Map[String, Any]() /: cc.getClass.getDeclaredFields) {(a, f) =>
      f.setAccessible(true)
      a + (f.getName -> f.get(cc))
    }
}
