package com.kiga.test

import org.springframework.boot.autoconfigure.SpringBootApplication

/**
 * Created by rainerh on 30.09.15.
 */
@SpringBootApplication class SimpleConfig
