package com.kiga.shop

import java.math.MathContext

import com.kiga.shop.document.model.sub.InvoiceVatRow
import com.kiga.shop.document.service.InvoiceVatTableCalcer
import com.kiga.shop.documents.service.VatRateFormatter
import com.kiga.shop.domain.{Purchase, PurchaseItem}
import com.kiga.types.JBigDecimal
import org.junit.Test

import scala.collection.JavaConversions._

/**
  * Created by faxxe on 1/28/16.
  */
class InvoiceVatTableCalcerTest {
  @Test def testThreeRates() = {
    val purchaseItems = List(
      createPI(.16, 22, .2, 5),
      createPI(.11, 15, .2, 10),
      createPI(.2, 12, .2, 1)
    )

    val purchase = createPurchase(purchaseItems, .2, 5)

    val invoiceVatTableCalcer = new InvoiceVatTableCalcer()
    val result: List[InvoiceVatRow] = invoiceVatTableCalcer.calc(purchase)
    val res = List(createIVR(.11, 15), createIVR(.16, 22), createIVR(.2, 17))

    assert(result(0) == res(0))
    assert(result(1) == res(1))
    assert(result(2) == res(2))


  }

  @Test def testSorting() = {
    val purchaseItems = List(
      createPI(0.16, 22, 0.2, 5),
      createPI(.11, 15, .2, 10),
      createPI(.2, 12, .2, 1)
    )

    val purchase = createPurchase(purchaseItems, .2, 5)

    val invoiceVatTableCalcer = new InvoiceVatTableCalcer()
    val result: List[InvoiceVatRow] = invoiceVatTableCalcer.calc(purchase)
    val res = List(createIVR(11, 15), createIVR(.16, 22), createIVR(.2, 17))

    assert(result(0) != res(1))
    assert(result(1) != res(0))
    assert(result(2) == res(2))


  }

  @Test def testSingleRate() = {
    val purchaseItems = List(
      createPI(.2, 22, .2, 5),
      createPI(.2, 15, .2, 10),
      createPI(.2, 12, .2, 1)
    )
    val purchase = createPurchase(purchaseItems, .2, 5)

    val invoiceVatTableCalcer = new InvoiceVatTableCalcer()
    val result: List[InvoiceVatRow] = invoiceVatTableCalcer.calc(purchase)
    val res = List(createIVR(.2, 54))

    assert(result(0) == res(0))
  }

  def createIVR(rate: Double, total: Double) = {
    val returner = new InvoiceVatRow(
      VatRateFormatter.format(new JBigDecimal(rate)),
      new JBigDecimal(total, new MathContext(2))
    )
    returner
  }

  def createPI(vatRate: Double, vatTotal: Double, vatShippingRate: Double, vatShippingTotal: Double) = {
    val returner = new PurchaseItem
    returner.setVatRate(new JBigDecimal(vatRate, new MathContext(2)))
    returner.setVatTotal(new JBigDecimal(vatTotal, new MathContext(2)))
    returner.setVatShippingRate(new JBigDecimal(vatShippingRate, new MathContext(2)))
    returner.setVatShippingTotal(new JBigDecimal(vatShippingTotal, new MathContext(2)))
    returner
  }

  def createPurchase(purchaseItems: List[PurchaseItem], vatShippingRate: Double, vatShipping: Double): Purchase = {
    val returner = new Purchase;
    returner.setPurchaseItems(purchaseItems)
    returner.setVatRateShipping(new JBigDecimal(vatShippingRate, new MathContext(2)))
    returner.setVatShipping(new JBigDecimal(vatShipping, new MathContext(2)))
    returner
  }

  @Test def testOneProduct() = {
    val purchaseItems = List(
      createPI(.2, 12, .2, 1)
    )
    val purchase = createPurchase(purchaseItems, .2, 13)

    val invoiceVatTableCalcer = new InvoiceVatTableCalcer()
    val result: List[InvoiceVatRow] = invoiceVatTableCalcer.calc(purchase)
    val res = List(createIVR(.2, 25))

    assert(result(0) == res(0))


  }
}
