package com.kiga.shop

import com.kiga.shop.numbers.{CreditVoucherOverflowException, InvoiceNrOverflowException, NumberGenerator, ShippingNoteOverflowException}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by peter on 20.01.16.
  */
@RunWith(classOf[JUnitRunner])
class GenerateNumberTest extends FlatSpec with Matchers{
  val generateShopNumbers = new NumberGenerator()

  it should "Generate Invoice Number" in {
    var reach = false
    var toFalse = true
    try {
      generateShopNumbers.generateInvoiceNr("", "DE", 2016) shouldBe "DE6000001"
      generateShopNumbers.generateInvoiceNr("DE6000002", "DE",
        2016) shouldBe "DE6000003"
      generateShopNumbers.generateInvoiceNr("DE6999998", "DE",
        2016) shouldBe "DE6999999"
      reach = true
      generateShopNumbers.generateInvoiceNr("DE6999999","DE",2016) shouldBe "DE9999999"
    }
    catch {
      case ex:InvoiceNrOverflowException =>{
        toFalse = false
      }
    }
    toFalse shouldBe false
    reach shouldBe true
  }

  it should "generate invoice number with former year" in {
    generateShopNumbers.generateInvoiceNr("AT7000001", "AT", 2016) shouldBe "AT6000002"
  }

  it should "Generate CreditVoucher Number" in {
    var reach = false
    var toFalse = true
    try {
      generateShopNumbers.generateCreditVoucherNr("") shouldBe "G0000001"
      generateShopNumbers.generateCreditVoucherNr("G9999998") shouldBe "G9999999"
      reach = true
      generateShopNumbers.generateCreditVoucherNr("G9999999") shouldBe "G9999999"
    }
    catch {
      case ex:CreditVoucherOverflowException =>{
        toFalse = false
      }
    }
    toFalse shouldBe false
    reach shouldBe true
  }

  it should "generate shipping number at 999" in {
    generateShopNumbers.generateShippingNr(Option("000999")) shouldBe "001000"
  }

  it should "generate shipping number at 1000" in {
    generateShopNumbers.generateShippingNr(Option("001000")) shouldBe "001001"
  }
  it should "generate shipping number at 10000" in {
    generateShopNumbers.generateShippingNr(Option("010000")) shouldBe "010001"
  }

  it should "fail on invalid integer" in {
    an [NumberFormatException] should be thrownBy
      generateShopNumbers.generateShippingNr(Option("foobar"))
  }

  it should "fail on max number" in {
    an [ShippingNoteOverflowException] should be thrownBy
      generateShopNumbers.generateShippingNr(Option("999999"))
  }

  it should "fail on an unreachable number" in {
    an [ShippingNoteOverflowException] should be thrownBy
      generateShopNumbers.generateShippingNr(Option("1000000"))
  }

  it should "work with null as string" in {
    generateShopNumbers.generateShippingNr(None) shouldBe "000001"
  }

  it should "work with empty string" in {
    generateShopNumbers.generateShippingNr(None) shouldBe "000001"
  }
}
