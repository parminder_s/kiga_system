package com.kiga.shop

import com.kiga.shop.domain.Purchase
import com.kiga.shop.service.ShopUrlGenerator
import com.kiga.web.service.{DefaultUrlGenerator, UrlGenerator}
import org.junit.{Assert, Test}

/**
  * Created by rainerh on 27.02.16.
  */
class ShopUrlGeneratorTest {
  @Test def testInvoiceUrl(): Unit = {
    val purchase = new Purchase()
    purchase.setId(5L)
    purchase.setInvoiceHash("simpleHash")

    val shopUrlGenerator = new ShopUrlGenerator(
      new DefaultUrlGenerator("http://www.kigaportal.com", "http://wrongurl.com", "")
    )

    Assert.assertEquals(
      "http://www.kigaportal.com/shop/getInvoice/5/simpleHash",
      shopUrlGenerator.generateInvoiceUrl(purchase)
    )
  }
}
