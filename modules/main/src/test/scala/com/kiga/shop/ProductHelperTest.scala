package com.kiga.shop

import java.math.BigDecimal

import com.kiga.shop.domain.PurchaseItem
import com.kiga.shop.service.DefaultProductHelper
import com.kiga.types.JBigDecimal
import org.junit.{Assert, Test}

import scala.math.BigDecimal.RoundingMode

/**
  * Created by peter on 29.03.16.
  */
class ProductHelperTest {
  @Test def purchaseItem(): Unit = {
    val brutto = new JBigDecimal(79.80).setScale(2, java.math.RoundingMode.HALF_UP)
    val vat = new JBigDecimal(0.1)
    val net = new java.math.BigDecimal(72.54).setScale(2, java.math.RoundingMode.HALF_UP)
    val vatSum = new JBigDecimal(7.26)

    val helper = new DefaultProductHelper(null, null)

    val purchaseItem = new PurchaseItem()
    purchaseItem.setPriceNetSingle(new JBigDecimal(36.27))
    purchaseItem.setAmount(2)
    purchaseItem.setVatRate(vat)
    purchaseItem.setPriceGrossTotal(brutto)


    val netCalculated = helper.getNetPrice(brutto, purchaseItem)
    Assert.assertEquals(0, netCalculated.compareTo(net))

  }
}
