package com.kiga.shop


import com.kiga.shop.document.service.DhlCsvGenerator
import com.kiga.shop.domain.{Country, CountryShop, Purchase, PurchaseItem}
import org.junit.{Assert, Test}

import scala.collection.JavaConversions._

class DhlCsvGeneratorTest {


  val header = new DhlCsvGenerator().header

  val contentDEU =
    """2;;Hohe Medien;;;Klosterwiesgasse;101b;8010;Graz;;AUT;;kundenservice@kigaportal.com;;Max Mustermann; ;;Am Platzl;22A;7000;Eisenstadt;;DEU;;max.mustermann@k.tt4.at;;0,002;;;;V87PARCEL;;;;;;;;;;;;;62901436698701;;;;;;;;;;;;;;;;;;OTHER;LAPBAG;;;someTitle;2;;;;0,002;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;""" + "\r\n"
  val contentITA =
    """2;;Hohe Medien;;;Klosterwiesgasse;101b;8010;Graz;;AUT;;kundenservice@kigaportal.com;;Max Mustermann; ;;Am Platzl;22A;7000;Eisenstadt;;ITA;;max.mustermann@k.tt4.at;;0,002;;;;V82PARCEL;;;;;;;;;;;;;62901436698201;;;;;;;;;;;;;;;;;;OTHER;LAPBAG;;;someTitle;2;;;;0,002;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;""" + "\r\n"
  val contentLIE =
    """2;;Hohe Medien;;;Klosterwiesgasse;101b;8010;Graz;;AUT;;kundenservice@kigaportal.com;;Max Mustermann; ;;Am Platzl;22A;7000;Eisenstadt;;LIE;;max.mustermann@k.tt4.at;;0,002;;;;V82PARCEL;;;;;;;;;;;;;62901436698201;;;;;;;;;;;;;;;;;;OTHER;LAPBAG;;;someTitle;2;;;;0,002;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;""" + "\r\n"
  val contentLUX =
    """2;;Hohe Medien;;;Klosterwiesgasse;101b;8010;Graz;;AUT;;kundenservice@kigaportal.com;;Max Mustermann; ;;Am Platzl;22A;7000;Eisenstadt;;LUX;;max.mustermann@k.tt4.at;;0,002;;;;V87PARCEL;;;;;;;;;;;;;62901436698701;;;;;;;;;;;;;;;;;;OTHER;LAPBAG;;;someTitle;2;;;;0,002;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;""" + "\r\n"
  val contentCHE =
    """2;;Hohe Medien;;;Klosterwiesgasse;101b;8010;Graz;;AUT;;kundenservice@kigaportal.com;;Max Mustermann; ;;Am Platzl;22A;7000;Eisenstadt;;CHE;;max.mustermann@k.tt4.at;;0,002;;;;V82PARCEL;;;;;;;;;;;;;62901436698201;;;;;;;;;;;;;;;;;;OTHER;LAPBAG;;;someTitle;2;;;;0,002;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;""" + "\r\n"
  val contentAUT =
    """2;;Hohe Medien;;;Klosterwiesgasse;101b;8010;Graz;;AUT;;kundenservice@kigaportal.com;;Max Mustermann; ;;Am Platzl;22A;7000;Eisenstadt;;AUT;;max.mustermann@k.tt4.at;;0,002;;;;V86PARCEL;;;;;;;;;;;;;62901436698601;;;;;;;;;;;;;;;;;;OTHER;LAPBAG;;;someTitle;2;;;;0,002;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;""" + "\r\n"


  @Test def singlePurchaseAUT(): Unit = {
    val purchase = getPurchaseAUT()
    val result = new DhlCsvGenerator().getCsvData(List(purchase))

    Assert.assertEquals(2, result.size)
    Assert.assertEquals(header, result(0))
    Assert.assertEquals(contentAUT, result(1))
  }

  @Test def singlePurchaseITA(): Unit = {
    val purchase = getPurchaseITA()
    val result = new DhlCsvGenerator().getCsvData(List(purchase))

    Assert.assertEquals(2, result.size)
    Assert.assertEquals(header, result(0))
    Assert.assertEquals(contentITA, result(1))
  }

  @Test def singlePurchaseDEU(): Unit = {
    val purchase = getPurchaseDEU()
    val result = new DhlCsvGenerator().getCsvData(List(purchase))

    Assert.assertEquals(2, result.size)
    Assert.assertEquals(header, result(0))
    Assert.assertEquals(contentDEU, result(1))
  }

  @Test def singlePurchaseLIE(): Unit = {
    val purchase = getPurchaseLIE()
    val result = new DhlCsvGenerator().getCsvData(List(purchase))

    Assert.assertEquals(2, result.size)
    Assert.assertEquals(header, result(0))
    Assert.assertEquals(contentLIE, result(1))
  }

  @Test def singlePurchaseLUX(): Unit = {
    val purchase = getPurchaseLUX()
    val result = new DhlCsvGenerator().getCsvData(List(purchase))

    Assert.assertEquals(2, result.size)
    Assert.assertEquals(header, result(0))
    Assert.assertEquals(contentLUX, result(1))
  }

  @Test def singlePurchaseCHE(): Unit = {
    val purchase = getPurchaseCHE()
    val result = new DhlCsvGenerator().getCsvData(List(purchase))

    Assert.assertEquals(2, result.size)
    Assert.assertEquals(header, result(0))
    Assert.assertEquals(contentCHE, result(1))
  }

  @Test def multiPurchaseAUT(): Unit = {
    val purchases = List(getPurchaseAUT(), getPurchaseAUT(), getPurchaseAUT())
    val result = new DhlCsvGenerator().getCsvData(purchases)

    Assert.assertEquals(4, result.size)
    Assert.assertEquals(header, result(0))
    Assert.assertEquals(contentAUT, result(1))
    Assert.assertEquals(contentAUT, result(2))
    Assert.assertEquals(contentAUT, result(3))
  }

  @Test def multiPurchaseDEU(): Unit = {
    val purchases = List(getPurchaseDEU(), getPurchaseDEU(), getPurchaseDEU())
    val result = new DhlCsvGenerator().getCsvData(purchases)

    Assert.assertEquals(4, result.size)
    Assert.assertEquals(header, result(0))
    Assert.assertEquals(contentDEU, result(1))
    Assert.assertEquals(contentDEU, result(2))
    Assert.assertEquals(contentDEU, result(3))
  }

  @Test def multiPurchaseITA(): Unit = {
    val purchases = List(getPurchaseITA(), getPurchaseITA(), getPurchaseITA())
    val result = new DhlCsvGenerator().getCsvData(purchases)

    Assert.assertEquals(4, result.size)
    Assert.assertEquals(header, result(0))
    Assert.assertEquals(contentITA, result(1))
    Assert.assertEquals(contentITA, result(2))
    Assert.assertEquals(contentITA, result(3))
  }

  @Test def multiPurchaseCHE(): Unit = {
    val purchases = List(getPurchaseCHE(), getPurchaseCHE(), getPurchaseCHE())
    val result = new DhlCsvGenerator().getCsvData(purchases)

    Assert.assertEquals(4, result.size)
    Assert.assertEquals(header, result(0))
    Assert.assertEquals(contentCHE, result(1))
    Assert.assertEquals(contentCHE, result(2))
    Assert.assertEquals(contentCHE, result(3))
  }

  @Test def multiPurchaseMix(): Unit = {
    val purchases = List(getPurchaseAUT(), getPurchaseDEU(), getPurchaseCHE(), getPurchaseITA(), getPurchaseLIE(), getPurchaseLUX())
    val result = new DhlCsvGenerator().getCsvData(purchases)

    Assert.assertEquals(7, result.size)
    Assert.assertEquals(header, result(0))
    Assert.assertEquals(contentAUT, result(1))
    Assert.assertEquals(contentDEU, result(2))
    Assert.assertEquals(contentCHE, result(3))
    Assert.assertEquals(contentITA, result(4))
    Assert.assertEquals(contentLIE, result(5))
    Assert.assertEquals(contentLUX, result(6))
  }

  def getPurchaseAUT(): Purchase = {
    val purchase = new Purchase
    purchase.setOrderNr(2L)
    purchase.setFirstname("Max")
    purchase.setLastname("Mustermann")
    purchase.setCountry(getCountry("at", "AUT"))
    purchase.setPurchaseItems(getPurchaseItem())
    purchase.setStreet("Am Platzl")
    purchase.setStreetNumber("22A")
    purchase.setFloorNumber("2")
    purchase.setDoorNumber("3")
    purchase.setZip("7000")
    purchase.setCity("Eisenstadt")
    purchase.setEmail("max.mustermann@k.tt4.at")
    purchase
  }

  def getPurchaseItem(): List[PurchaseItem] = {
    val purchaseItem = new PurchaseItem
    purchaseItem.setWeightGramTotal(2)
    purchaseItem.setWeightGramSingle(2)
    purchaseItem.setTitle("someTitle")
    purchaseItem.setAmount(2)
    List(purchaseItem)
  }

  def getCountry(code2: String, code3: String): Country = {
    val c = new Country
    c.setCode(code2)
    c.setCode3(code3)
    c
  }

  def getPurchaseITA(): Purchase = {
    val purchase = new Purchase
    purchase.setOrderNr(2L)
    purchase.setFirstname("Max")
    purchase.setLastname("Mustermann")
    purchase.setCountry(getCountry("it", "ITA"))
    purchase.setPurchaseItems(getPurchaseItem())
    purchase.setStreet("Am Platzl")
    purchase.setStreetNumber("22A")
    purchase.setFloorNumber("2")
    purchase.setDoorNumber("3")
    purchase.setZip("7000")
    purchase.setCity("Eisenstadt")
    purchase.setEmail("max.mustermann@k.tt4.at")
    purchase
  }

  def getPurchaseDEU(): Purchase = {
    val purchase = new Purchase
    purchase.setOrderNr(2L)
    purchase.setFirstname("Max")
    purchase.setLastname("Mustermann")
    purchase.setCountry(getCountry("de", "DEU"))
    purchase.setPurchaseItems(getPurchaseItem())
    purchase.setStreet("Am Platzl")
    purchase.setStreetNumber("22A")
    purchase.setFloorNumber("2")
    purchase.setDoorNumber("3")
    purchase.setZip("7000")
    purchase.setCity("Eisenstadt")
    purchase.setEmail("max.mustermann@k.tt4.at")
    purchase
  }

  def getPurchaseLIE(): Purchase = {
    val purchase = new Purchase
    purchase.setOrderNr(2L)
    purchase.setFirstname("Max")
    purchase.setLastname("Mustermann")
    purchase.setCountry(getCountry("li", "LIE"))
    purchase.setPurchaseItems(getPurchaseItem())
    purchase.setStreet("Am Platzl")
    purchase.setStreetNumber("22A")
    purchase.setFloorNumber("2")
    purchase.setDoorNumber("3")
    purchase.setZip("7000")
    purchase.setCity("Eisenstadt")
    purchase.setEmail("max.mustermann@k.tt4.at")
    purchase
  }

  def getPurchaseLUX(): Purchase = {
    val purchase = new Purchase
    purchase.setOrderNr(2L)
    purchase.setFirstname("Max")
    purchase.setLastname("Mustermann")
    purchase.setCountry(getCountry("lu", "LUX"))
    purchase.setPurchaseItems(getPurchaseItem())
    purchase.setStreet("Am Platzl")
    purchase.setStreetNumber("22A")
    purchase.setFloorNumber("2")
    purchase.setDoorNumber("3")
    purchase.setZip("7000")
    purchase.setCity("Eisenstadt")
    purchase.setEmail("max.mustermann@k.tt4.at")
    purchase
  }

  def getPurchaseCHE(): Purchase = {
    val purchase = new Purchase
    purchase.setOrderNr(2L)
    purchase.setFirstname("Max")
    purchase.setLastname("Mustermann")
    purchase.setCountry(getCountry("ch", "CHE"))
    purchase.setPurchaseItems(getPurchaseItem())
    purchase.setStreet("Am Platzl")
    purchase.setStreetNumber("22A")
    purchase.setFloorNumber("2")
    purchase.setDoorNumber("3")
    purchase.setZip("7000")
    purchase.setCity("Eisenstadt")
    purchase.setEmail("max.mustermann@k.tt4.at")
    purchase
  }

}
