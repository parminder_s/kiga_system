package com.kiga.shop


import com.kiga.shop.domain.Purchase
import com.kiga.shop.service.AddressFormatter
import org.junit.{Assert, Test}

class AddressFormatterTest {
  @Test def testAddress(): Unit={
    val purchase = new Purchase
    purchase.setFirstname("Max")
    purchase.setLastname("Mustermann")
    purchase.setStreet("Klosterwiesgasse")
    purchase.setStreetNumber("101B")
    purchase.setFloorNumber("3")
    purchase.setDoorNumber("2")
    purchase.setZip("8010")
    purchase.setCity("Graz")

    val result = AddressFormatter.getAddress(purchase)

    Assert.assertEquals("name", "Max Mustermann", result(0))
    Assert.assertEquals("address", "Klosterwiesgasse 101B/3/2", result(1))
    Assert.assertEquals("city", "8010 Graz", result(2))
  }

  @Test def testShippingAddress() = {
    val purchase = new Purchase
    purchase.setShippingAddressName1("Max")
    purchase.setShippingAddressName2("Mustermann")
    purchase.setShippingStreet("Klosterwiesgasse")
    purchase.setShippingStreetNumber("101B")
    purchase.setShippingFloorNumber("3")
    purchase.setShippingDoorNumber("2")
    purchase.setShippingZip("8010")
    purchase.setShippingCity("Graz")

    val result = AddressFormatter.getShippingAddress(purchase)

    Assert.assertEquals("firstname", "Max", result(0))
    Assert.assertEquals("surename", "Mustermann", result(1))
    Assert.assertEquals("address", "Klosterwiesgasse 101B/3/2", result(2))
    Assert.assertEquals("city", "8010 Graz", result(3))
    Assert.assertEquals("last", "", result(4))
    Assert.assertEquals("size", 5, result.size)
  }

  @Test def testBillAddress(): Unit = {
    val purchase = new Purchase
    purchase.setBillAddressName1("Max")
    purchase.setBillAddressName2("Mustermann")
    purchase.setBillStreet("Klosterwiesgasse")
    purchase.setBillStreetNumber("101B")
    purchase.setBillFloorNumber("3")
    purchase.setBillDoorNumber("2")
    purchase.setBillZip("8010")
    purchase.setBillCity("Graz")

    val result = AddressFormatter.getBillAddress(purchase)

    Assert.assertEquals("firstname", "Max", result(0))
    Assert.assertEquals("surename", "Mustermann", result(1))
    Assert.assertEquals("address", "Klosterwiesgasse 101B/3/2", result(2))
    Assert.assertEquals("city", "8010 Graz", result(3))
    Assert.assertEquals("size", 4, result.size)
  }

  @Test def testBillAddressNull(): Unit = {
    val purchase = new Purchase
    purchase.setBillAddressName1("Max")
    purchase.setBillAddressName2(null)
    purchase.setBillStreet("Klosterwiesgasse")
    purchase.setBillStreetNumber("101B")
    purchase.setBillFloorNumber("3")
    purchase.setBillDoorNumber("2")
    purchase.setBillZip("8010")
    purchase.setBillCity("Graz")

    val result = AddressFormatter.getBillAddress(purchase)

    Assert.assertEquals("firstname", "Max", result(0))
    Assert.assertEquals("address", "Klosterwiesgasse 101B/3/2", result(1))
    Assert.assertEquals("city", "8010 Graz", result(2))
    Assert.assertEquals("size", 3, result.size)
  }



}
