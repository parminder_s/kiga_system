package com.kiga.shop

import org.junit.{Assert, Test}

class PurchaseWorkflowEnumTest {
  object testEnum extends Enumeration{
    val Resumed = Value("resumed")
    val FailedCheckout = Value("failedCheckout")
    val Checkout = Value("checkout")
    val InPayment = Value("inPayment")

    val Ordered = Value("ordered")

    val ShippingNoteCreated = Value("shippingnote_created")
    val ReadyForShipping = Value("ready_to_ship")
    val ExportFin = Value("export_fin")
    val Shipped = Value("shipped")
    val InvoiceCreated = Value("invoice_created")
    val ShippingFin = Value("shipping_fin")

    val OrderPrinted = Value("order_printed")
  }

  val enums = Array(testEnum.Checkout, testEnum.ExportFin, testEnum.FailedCheckout)

  @Test def enumToString(): Unit = {
    Assert.assertEquals(testEnum.Checkout.toString, "checkout")
    Assert.assertNotEquals(testEnum.Checkout.toString, "wrongstring")
  }

  @Test def enumFromString(): Unit = {
    Assert.assertEquals(testEnum.Checkout, testEnum.withName("checkout"))
  }

  @Test(expected = classOf[java.util.NoSuchElementException]) def wrongStringThrowsException(): Unit = {
    Assert.assertNotEquals(testEnum.Checkout, testEnum.withName("wrongStringforEnum"))
  }

  @Test def arrayContainsElement(): Unit = {
    Assert.assertEquals(enums contains testEnum.ExportFin, true)
    Assert.assertEquals(enums contains testEnum.InvoiceCreated, false)
    Assert.assertEquals(enums contains testEnum.withName("checkout"),true)
    Assert.assertEquals(enums contains testEnum.withName("inPayment"), false)
  }

  @Test(expected = classOf[java.util.NoSuchElementException]) def noStringEnum(): Unit = {
    Assert.assertEquals(testEnum.withName(""), true)
  }

}
