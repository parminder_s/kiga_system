package integration.com.kiga.integration.sitetree

import javax.inject.Inject
import javax.sql.DataSource

import com.kiga.content.ContentConfig
import com.kiga.content.repository.ss.draft.SiteTreeRepository
import com.kiga.main.locale.Locale
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FlatSpec, Matchers}
import org.springframework.beans.factory.annotation.{Autowired, Qualifier}
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.test.context.{ActiveProfiles, TestContextManager}

/**
 * Created by peter on 12.08.15.
 */
@SpringApplicationConfiguration(classes = Array(classOf[ContentConfig]))
@RunWith(classOf[JUnitRunner])
@ActiveProfiles(Array("dev"))
class SiteTreeLiveDraftRepositoryTest extends FlatSpec with Matchers{
  new TestContextManager(classOf[SiteTreeLiveDraftRepositoryTest]).prepareTestInstance(this)

  @Inject var siteTreeRepository: SiteTreeRepository = _
  @Autowired
  @Qualifier("dsSs")
  var dataSource: DataSource = _

  it should "test SiteTreeRepositry find by ClassName" in {
   siteTreeRepository.findByClassName("RegistrationOrg")
   siteTreeRepository.findByLocale("de_DE")
   siteTreeRepository.findByClassNameAndLocale("RegistrationOrg", Locale.de)
  }
}


