package integration.com.kiga.integration.dbtest.entity

import javax.inject.Inject
import javax.persistence.Entity
import javax.validation.ConstraintViolationException

import com.kiga.db.{KigaEntityModelWithoutId, KigaEntityModel}
import com.kiga.types.JLong
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FlatSpec, Matchers}
import org.slf4j.LoggerFactory
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.data.repository.CrudRepository
import org.springframework.test.context.TestContextManager
import org.springframework.transaction.annotation.Transactional

/**
 * Created by rainerh on 27.10.15.
 */

@Entity class TestEntityWithoutSetClassName extends KigaEntityModel
@Entity class TestEntityWithSetClassName extends KigaEntityModel {
  this.setClassName("TestEntityWithSetClassName")
}

trait TestEntityWithoutSetClassNameRepository extends CrudRepository[TestEntityWithoutSetClassName, JLong]
trait TestEntityWithSetClassNameRepository extends CrudRepository[TestEntityWithSetClassName, JLong]

@Transactional
@RunWith(classOf[JUnitRunner])
@SpringApplicationConfiguration(classes = Array(classOf[TestDbConfig]))
class KigaEntityTest extends FlatSpec with Matchers {
  new TestContextManager(classOf[KigaEntityTest]).prepareTestInstance(this)
  val logger = LoggerFactory.getLogger(classOf[KigaEntityTest])

  @Inject var repoWithClass: TestEntityWithSetClassNameRepository = _
  @Inject var repoWithoutClass: TestEntityWithoutSetClassNameRepository = _

  "TestEntity without set className" should "fail to save when no classname set" in {
    intercept[ConstraintViolationException] {
      repoWithoutClass.save(new TestEntityWithoutSetClassName())
    }
  }

  "TestEntity without set className" should "save when classname is set" in {
    val testEntity = new TestEntityWithoutSetClassName
    testEntity.setClassName("TestEntity")
    repoWithoutClass.save(testEntity)
  }

  "TestEntity with constructor based classname" should "save" in {
    repoWithClass.save(new TestEntityWithSetClassName)
  }
}

