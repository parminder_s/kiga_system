/**
 * Created by rainerh on 27.10.15.
 */
package integration.com.kiga.integration.dbtest.entity

import javax.sql.DataSource

import org.springframework.beans.factory.annotation.{Autowired, Qualifier}
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.{Bean, Configuration, Primary}
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.{JpaTransactionManager, LocalContainerEntityManagerFactoryBean}

import scala.collection.JavaConversions._

/**
 * Created by rainerh on 27.01.15.
 */


@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories(
  basePackageClasses = Array(
    classOf[TestEntityWithoutSetClassNameRepository],
    classOf[TestEntityWithSetClassNameRepository]
  ),
  entityManagerFactoryRef = "emfTest",
  transactionManagerRef = "txTest")
class TestDbConfig {
  val jdbcUrl: String = "jdbc:hsqldb:mem:test;db_integration;sql.syntax_mys=true"

  @Bean(name = Array("dsTest"))
  def dataSource(): DataSource = {
    DataSourceBuilder.create.url(jdbcUrl).build
  }

  @Bean(name = Array("emfTest"))
  @Primary
  def emf(builder: EntityManagerFactoryBuilder): LocalContainerEntityManagerFactoryBean = {
    builder.
      dataSource(dataSource()).
      properties(Map[String, String]("hibernate.hbm2ddl.auto" -> "create-drop")).
      packages(
        classOf[TestEntityWithoutSetClassName],
        classOf[TestEntityWithSetClassName]
      ).
      persistenceUnit("test").
      build()
  }

  @Bean(name = Array("txTest"))
  def txTest(@Autowired @Qualifier("emfTest") emf: LocalContainerEntityManagerFactoryBean) = {
    new JpaTransactionManager(emf.getObject)
  }
}

