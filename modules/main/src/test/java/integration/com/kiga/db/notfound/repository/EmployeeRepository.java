package integration.com.kiga.db.notfound.repository;

import integration.com.kiga.db.notfound.domain.Employee;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 14.06.16.
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
