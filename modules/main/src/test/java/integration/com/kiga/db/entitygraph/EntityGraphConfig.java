package integration.com.kiga.db.entitygraph;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;

/**
 * Created by rainerh on 19.06.16.
 */
@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories
public class EntityGraphConfig {}
