package integration.com.kiga.db.recursion;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 22.06.16.
 */
public interface PersonRepository extends CrudRepository<Person, Long> {
}
