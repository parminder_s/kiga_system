package integration.com.kiga.db.entitygraph;

import integration.com.kiga.db.entitygraph.repository.ManufacturerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by rainerh on 19.06.16.
 *
 * <p>This integration test has been made for testing and experimenting with
 * JPA/Hibernate's fetching (eager/lazy) algorithms for optimizing the queries
 * by using EntityGraph.</p>
 *
 * <p>By enabling the show-sql property for hibernate one can see, that
 * EntityGraph generates a single query with two joins to Car and CarVariety
 * tables.</p>
 *
 * <p>Unfortunately the EntityGraph has to be attached directly to entity and
 * cannot be registered in a separate file. In Registrar class a new EntityGraph
 * should be registered, but when referencing to that one in the repostory, the
 * test fails.</p>
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTest
@SpringApplicationConfiguration(EntityGraphConfig.class)
@Sql("init.sql")
public class TestEntityGraphTest {
  @Autowired
  private ManufacturerRepository manufacturerRepository;

  @Test
  public void testFetching() {
    Logger logger = LoggerFactory.getLogger(getClass());
    manufacturerRepository.findAll().forEach(manufacturer ->
      manufacturer.getCars().forEach(car ->
        car.getCarVarieties().forEach(carVariety ->
          logger.info(car.getName() + " - " + carVariety.getName())
        )
      )
    );
  }
}
