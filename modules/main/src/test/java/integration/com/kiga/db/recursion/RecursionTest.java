package integration.com.kiga.db.recursion;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by rainerh on 22.06.16.
 *
 * <p>This integration test should check how hibernate behaves when it has
 * recursive entities. In default mode it would eager load the entity it
 * references to. Unfortunately that is done via a loop by subselects.</p>
 *
 * <p>This test setup works with 4 person entities where person 1 is the father
 * of person 2. Person 2 is the father of #3 and the same for Person 3 to 4.</p>
 *
 * <p>Person 4 is loaded first and and then all 3 ancestors are printed out by
 * the logger. If sql queries are shown one can see that Hibernate executes
 * 4 queries before it prints out the first name 4. That would mean it goes up
 * all the way until it reaches an entity which is null, regradless if that entity
 * is requested or not.</p>
 *
 * <p>That can lead to huge performance issues, especially in SiteTree with 5 or 6
 * levels deep. Reason for this behaviour is @NotFound which disables lazy loading
 * at all</p>
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTest
@SpringApplicationConfiguration(RecursionConfig.class)
@Sql("init.sql")
public class RecursionTest {
  @Autowired
  PersonRepository repository;

  @Test public void runTest() {
    Logger logger = LoggerFactory.getLogger(getClass());
    Person martin = repository.findOne(4L);
    logger.info(martin.getName());
    logger.info(martin.getFather().getName());
    logger.info(martin.getFather().getFather().getName());
    logger.info(martin.getFather().getFather().getFather().getName());
  }
}

