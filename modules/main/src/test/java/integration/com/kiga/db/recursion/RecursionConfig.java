package integration.com.kiga.db.recursion;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by rainerh on 22.06.16.
 */
@Configuration
@EnableJpaRepositories
@EnableAutoConfiguration
public class RecursionConfig {
}
