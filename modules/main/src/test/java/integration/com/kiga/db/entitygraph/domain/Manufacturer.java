package integration.com.kiga.db.entitygraph.domain;

import integration.com.kiga.db.entitygraph.TestEntityGraph;
import org.springframework.data.jpa.domain.AbstractPersistable;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;

/**
 * Created by rainerh on 19.06.16.
 */
@Entity
@NamedEntityGraph(
  name = "fetchTest",
  attributeNodes = @NamedAttributeNode(value = "cars", subgraph = "carGraph"),
  subgraphs = @NamedSubgraph(
    name = "carGraph",
    attributeNodes = @NamedAttributeNode("carVarieties")
  )
)
public class Manufacturer extends AbstractPersistable<Long> {
  private String name;

  @OneToMany(mappedBy = "manufacturer")
  private Set<Car> cars;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<Car> getCars() {
    return cars;
  }

  public void setCars(Set<Car> cars) {
    this.cars = cars;
  }
}
