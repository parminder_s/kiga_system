package integration.com.kiga.db.notfound.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.AbstractPersistable;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/**
 * Created by rainerh on 14.06.16.
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Company extends AbstractPersistable<Long> {
  private String title;

  @OneToMany(mappedBy = "company")
  private List<Employee> employees;
}
