package integration.com.kiga.db.notfound.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;

/**
 * Created by rainerh on 14.06.16.
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Car extends AbstractPersistable<Long> {
  private String name;
}
