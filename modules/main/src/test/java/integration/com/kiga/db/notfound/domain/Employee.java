package integration.com.kiga.db.notfound.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by rainerh on 14.06.16.
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Employee extends AbstractPersistable<Long> {
  private String firstname;
  private String lastname;

  @ManyToOne
  @JoinColumn(name = "company_id", referencedColumnName = "id")
  @NotFound(action = NotFoundAction.IGNORE)
  private Company company;

  @ManyToOne
  @NotFound(action = NotFoundAction.IGNORE)
  private Car car;
}
