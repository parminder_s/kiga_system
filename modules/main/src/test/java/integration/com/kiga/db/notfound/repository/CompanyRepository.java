package integration.com.kiga.db.notfound.repository;

import integration.com.kiga.db.notfound.domain.Company;
import integration.com.kiga.db.notfound.domain.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by rainerh on 14.06.16.
 */
public interface CompanyRepository extends CrudRepository<Company, Long> {
  List<Company> findAll();
}
