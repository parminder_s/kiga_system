package integration.com.kiga.db.notfound;

import static org.hamcrest.MatcherAssert.assertThat;

import integration.com.kiga.db.notfound.domain.Car;
import integration.com.kiga.db.notfound.domain.Company;
import integration.com.kiga.db.notfound.domain.Employee;
import integration.com.kiga.db.notfound.repository.CompanyRepository;
import integration.com.kiga.db.notfound.repository.EmployeeRepository;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by rainerh on 14.06.16.
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTest
@SpringApplicationConfiguration(NotFoundConfiguration.class)
@SqlGroup({
  @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "schema.sql"),
  @Sql("init.sql")}
)
public class NotFoundTest {
  @Autowired
  CompanyRepository companyRepository;

  @Test
  public void bootUp() {
    Logger logger = LoggerFactory.getLogger(getClass());
    List<String> names = companyRepository.findAll().stream()
      .map(Company::getEmployees)
      .flatMap(List::stream)
      .map(e -> Optional.ofNullable(e.getCar()))
      .filter(Optional::isPresent)
      .map(Optional::get)
      .map(Car::getName)
      .collect(Collectors.toList());

    assertThat(names, CoreMatchers.is(Arrays.asList("Mercedes E-Klasse (1997)")));
  }
}
