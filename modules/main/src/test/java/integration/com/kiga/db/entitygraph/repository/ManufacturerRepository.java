package integration.com.kiga.db.entitygraph.repository;

import integration.com.kiga.db.entitygraph.domain.Manufacturer;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by rainerh on 19.06.16.
 */
public interface ManufacturerRepository extends CrudRepository<Manufacturer, Long> {
  @EntityGraph(value = "fetchTest", type = EntityGraph.EntityGraphType.LOAD)
  List<Manufacturer> findAll();
}
