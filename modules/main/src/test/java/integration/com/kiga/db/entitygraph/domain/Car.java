package integration.com.kiga.db.entitygraph.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Created by rainerh on 19.06.16.
 */
@Entity
public class Car extends AbstractPersistable<Long> {
  private String name;

  @ManyToOne
  private Manufacturer manufacturer;

  @OneToMany(mappedBy = "car")
  private Set<CarVariety> carVarieties;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Manufacturer getManufacturer() {
    return manufacturer;
  }

  public void setManufacturer(Manufacturer manufacturer) {
    this.manufacturer = manufacturer;
  }

  public Set<CarVariety> getCarVarieties() {
    return carVarieties;
  }

  public void setCarVarieties(Set<CarVariety> carVarieties) {
    this.carVarieties = carVarieties;
  }
}
