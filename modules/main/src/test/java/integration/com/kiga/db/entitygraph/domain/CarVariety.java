package integration.com.kiga.db.entitygraph.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by rainerh on 19.06.16.
 */
@Entity
public class CarVariety extends AbstractPersistable<Long> {
  private String name;

  @ManyToOne
  private Car car;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Car getCar() {
    return car;
  }

  public void setCar(Car car) {
    this.car = car;
  }
}
