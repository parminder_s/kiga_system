package integration.com.kiga.db.entitygraph;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by rainerh on 19.06.16.
 */
@Transactional
@Component
public class Registrar implements ApplicationListener<ContextRefreshedEvent> {
  @PersistenceContext(unitName = "persistenceUnit")
  private EntityManager em;

  /**
   * get entityGraph.
   */
  public void onApplicationEvent(ContextRefreshedEvent cre) {
    EntityGraph eg = em.createEntityGraph("testEntityGraph");
    eg.addAttributeNodes("cars");
  }


}
