package integration.com.kiga.db.entitygraph.repository;

import integration.com.kiga.db.entitygraph.domain.Car;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rainerh on 19.06.16.
 */
public interface CarRepository extends CrudRepository<Car, Long> {
}
