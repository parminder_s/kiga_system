package com.kiga.test;

import com.kiga.web.service.UrlGenerator;

/**
 * Created by rainerh on 11.05.16.
 *
 * <p>For testing purposes returns exactly the urlPart.
 */
public class DummyUrlGenerator implements UrlGenerator {
  @Override
  public String getBaseUrl(String urlPart) {
    return urlPart;
  }

  @Override
  public String getNgUrl(String urlPart) {
    return urlPart;
  }

  @Override
  public String getUrl(String urlPart) {
    return urlPart;
  }
}
