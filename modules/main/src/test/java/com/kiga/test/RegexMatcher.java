package com.kiga.test;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

/**
 * Created by rainerh on 29.05.16.
 */

public class RegexMatcher extends BaseMatcher {
  private final String regex;

  public RegexMatcher(String regex) {
    this.regex = regex;
  }

  public boolean matches(Object object) {
    return ((String)object).matches(regex);
  }

  public static RegexMatcher matches(String regex) {
    return new RegexMatcher(regex);
  }

  public void describeTo(Description description) {
    description.appendText("matches regex=");
  }
}


