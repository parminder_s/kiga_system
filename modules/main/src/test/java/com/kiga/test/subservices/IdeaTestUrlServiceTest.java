package com.kiga.test.subservices;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.repository.ss.KigaIdeaRepositoryProxy;
import com.kiga.content.repository.ss.NewIdeasRepositoryProxy;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper.IdeaEngineHelper;
import com.kiga.main.locale.Locale;
import java.util.Collections;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

public class IdeaTestUrlServiceTest {

  private IdeaTestUrlService ideaTestUrlService;
  private NewIdeasRepositoryProxy newIdeasRepositoryProxy;
  private KigaIdeaRepositoryProxy kigaIdeaRepositoryProxyMock;
  private IdeaEngineHelper ideaEngineHelperMock;
  private CacheManager cacheManagerMock;

  /** preparing tests. */
  @Before
  public void prepareTests() {

    ideaEngineHelperMock = mock(IdeaEngineHelper.class);
    kigaIdeaRepositoryProxyMock = mock(KigaIdeaRepositoryProxy.class);
    newIdeasRepositoryProxy = mock(NewIdeasRepositoryProxy.class);
    cacheManagerMock = mock(CacheManager.class);
    when(cacheManagerMock.getCache(anyString())).thenReturn(mock(Cache.class));
    ideaTestUrlService =
        new IdeaTestUrlService(
            newIdeasRepositoryProxy, kigaIdeaRepositoryProxyMock,
            ideaEngineHelperMock, cacheManagerMock);

    when(ideaEngineHelperMock.hasValidParent(any(KigaIdea.class))).thenReturn(true);

    KigaIdea kigaIdea = new KigaIdeaLive();
    kigaIdea.setAgeGroup(2);
    kigaIdea.setTitle("TEST_IDEA");
    when(newIdeasRepositoryProxy.getAllIdeas(any(Locale.class), eq(0)))
        .thenReturn(Collections.singletonList(kigaIdea));
  }

  @Test
  public void testGetTestIdeaWithAgeGroup() {
    String testAgeGroup = "1to3";

    String returnedString = ideaTestUrlService.getTestIdeaWithAgeGroup(testAgeGroup);

    Assert.assertEquals("TEST_IDEA", returnedString);
  }
}
