package com.kiga.test.matchers;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.matcher.ElementMatchers;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.lang.reflect.Modifier;

/**
 * Created by rainerh on 04.03.17.
 *
 * <p>Used for lombok generated beans where Google's BeanMatchers do not cover the canEqual method.
 */
public class CanEqualMatcher extends TypeSafeMatcher<Class> {
  @Override
  public boolean matchesSafely(Class clazz) {
    if (Modifier.isFinal(clazz.getModifiers())) {
      return true;
    }

    Class subClass = new ByteBuddy()
      .subclass(clazz)
      .method(ElementMatchers.named("canEqual"))
      .intercept(FixedValue.value(false))
      .make()
      .load(getClass().getClassLoader())
      .getLoaded();

    try {
      return !clazz.newInstance().equals(subClass.newInstance());
    } catch (Exception exception) {
      return false;
    }
  }

  @Override
  public void describeTo(Description description) {
    description.appendText("equals should be false on sublasses");
  }

  @Factory
  public static Matcher hasValidCanEqual() {
    return new CanEqualMatcher();
  }
}
