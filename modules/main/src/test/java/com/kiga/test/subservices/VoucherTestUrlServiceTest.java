package com.kiga.test.subservices;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.matches;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Voucher;
import com.kiga.shop.repository.ProductRepository;
import com.kiga.shop.repository.VoucherRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class VoucherTestUrlServiceTest {
  private VoucherTestUrlService voucherTestUrlService;

  private VoucherRepository voucherRepositoryMock;
  private ProductRepository productRepositoryMock;
  private Product productCodeProduct;
  private Product productNotFoundProduct;
  private Voucher voucherCodeVoucher;
  private Voucher voucherNotFoundVoucher;

  /**
   * prepare tests.
   */
  @Before
  public void prepareTests() {

    productRepositoryMock = mock(ProductRepository.class);
    productNotFoundProduct = Product.builder()
      .code("product_not_found_code")
      .activated(true)
      .build();
    productCodeProduct = Product.builder()
      .code("product_code")
      .activated(true)
      .build();
    voucherCodeVoucher = spy(Voucher.builder()
      .code("voucher_code")
      .activated(true)
      .discountRate(new BigDecimal("0.3"))
      .product(productCodeProduct)
      .build());
    voucherNotFoundVoucher = Voucher.builder()
      .code("voucher_not_found_code")
      .activated(true)
      .discountRate(new BigDecimal("0.4"))
      .product(productCodeProduct)
      .build();
    when(productRepositoryMock.findByCode(matches("product_code")))
      .thenReturn(productCodeProduct);
    when(productRepositoryMock.findByCode(matches("not_existing_product")))
      .thenReturn(productNotFoundProduct);
    voucherRepositoryMock = mock(VoucherRepository.class);
    when(voucherRepositoryMock.findByCode(matches("voucher_code")))
      .thenReturn(voucherCodeVoucher);
    when(voucherRepositoryMock.findByCode(matches("not_existing_voucher")))
      .thenReturn(voucherNotFoundVoucher);

    voucherTestUrlService =
      new VoucherTestUrlService(voucherRepositoryMock, productRepositoryMock);

  }

  @Test
  public void testGetVoucherNotModified() {

    String testString = voucherTestUrlService.getTestVoucherWithProperties(
      "voucher_code", 0.3f, "product_code"
    );

    Assert.assertEquals("VoucherCode: voucher_code\nProductCode: product_code", testString);
  }

  @Test
  public void testGetVoucherNotExisting() {
    String testString = voucherTestUrlService.getTestVoucherWithProperties(
      "not_existing_voucher", 0.1f, "product_code"
    );

    Assert
      .assertEquals("VoucherCode: voucher_not_found_code\nProductCode: product_code", testString);
  }

  @Test
  public void testGetVoucherProductNotExisting() {
    String testString = voucherTestUrlService.getTestVoucherWithProperties(
      "voucher_code", 0.1f, "not_existing_product"
    );

    Assert
      .assertEquals("VoucherCode: voucher_code\nProductCode: product_not_found_code", testString);
  }

  @Test
  public void testGetVoucherModified() {
    String testString = voucherTestUrlService.getTestVoucherWithProperties(
      "voucher_code", 0.1f, "product_code"
    );

    Assert
      .assertEquals("VoucherCode: voucher_code\nProductCode: product_code", testString);
    verify(voucherCodeVoucher).setDiscountRate(any(BigDecimal.class));
  }
}