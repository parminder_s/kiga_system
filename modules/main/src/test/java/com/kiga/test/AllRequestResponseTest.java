package com.kiga.test;

import com.google.code.beanmatchers.BeanMatchers;
import com.google.code.beanmatchers.ValueGenerator;

import org.junit.Test;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by faxxe on 10/11/16.
 */
public class AllRequestResponseTest {
  String pathPrefix = "./src/main/java/";

  /**
   *  This test is used to ensure that every Request and Response has the necessary getters/setters
   *  for all members and the no-arg default constructor. In case we want to use caching we need
   *  these methods to for proper serialisation.
   */
  @Test
  public void requestResponseDefaultTest() throws IOException, ClassNotFoundException {
    List<String> javaFiles = getJavaClasses();

    BeanMatchers.registerValueGenerator(new ValueGenerator<Instant>() {
      public Instant generate() {
        return Instant.now().plusSeconds((long)(Math.random() * 1000));
      }
    }, Instant.class);

    new Reflections("com.kiga", new SubTypesScanner(false))
      .getSubTypesOf(Object.class)
      .stream()
      .filter(clazz -> javaFiles.contains(clazz.getName()))
      .sorted(Comparator.comparing(Class::getName))
      .forEach(FullBeanTest::test);
  }

  private List<String> getJavaClasses() throws IOException {
    return Files.walk(Paths.get(pathPrefix + "com/kiga/"))
      .filter(filePath -> filePath.toString().contains(".java"))
      .filter(Files::isRegularFile)
      .map(filePath ->
        filePath.toString()
          .replace(pathPrefix, "")
          .replace("/", ".")
          .replace(".java", "")
      )
      .filter(filename -> filename.endsWith("Response") || filename.endsWith("Request")
        || filename.endsWith("ViewModel"))
      .collect(Collectors.toList());
  }
}
