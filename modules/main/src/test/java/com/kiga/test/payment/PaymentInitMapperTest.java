package com.kiga.test.payment;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.hamcrest.MatcherAssert.assertThat;

import com.kiga.main.locale.Locale;
import com.kiga.payment.PaymentInitiatorConfig;
import com.kiga.payment.PaymentType;
import com.kiga.payment.api.model.Money;
import com.kiga.payment.api.model.PaymentInitConfig;
import org.junit.Test;

import java.math.BigDecimal;

public class PaymentInitMapperTest {
  @Test
  public void testMapping() {
    PaymentInitiatorConfig config =
      new PaymentInitMapper().purchaseToPurchaseResponse(
        new PaymentInitConfig()
          .paymentType(PaymentInitConfig.PaymentTypeEnum.MP_VISA)
          .successUrl("http://success").failureUrl("http://failure")
          .price(new Money().amount(10).currency("EUR"))
          .countryCode("de").locale("de")
          .referenceId("123").referenceCode("subscription")
          .description("foo bar"));

    assertThat(new PaymentInitiatorConfig(
      PaymentType.MP_VISA, "http://success", "http://failure",
      new com.kiga.money.Money(BigDecimal.TEN, "EUR"), "de", Locale.de),
      sameBeanAs(config));
  }
}
