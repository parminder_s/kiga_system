package com.kiga.test;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanEquals;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanHashCode;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanToString;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static com.kiga.test.matchers.CanEqualMatcher.hasValidCanEqual;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.AllOf.allOf;

import lombok.extern.log4j.Log4j2;

/**
 * Created by rainerh on 04.03.17.
 */
@Log4j2
public class FullBeanTest {
  /**
   * makes a test on a bean.
   */
  public static void test(Class clazz) {
    assertThat(clazz.getName(), clazz, allOf(
      hasValidGettersAndSetters(), hasValidBeanEquals(), hasValidBeanConstructor(),
      hasValidBeanHashCode(), hasValidBeanToString(), hasValidCanEqual()));
  }
}
