package com.kiga.test;

import com.kiga.db.KigaEntity;
import com.kiga.spec.Fetcher;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rainerh on 29.03.16.
 */
public class MapFetcher<T extends KigaEntity> implements Fetcher<T> {
  private Map<Long, T> storage = new HashMap<Long, T>();

  public MapFetcher() {

  }

  public MapFetcher(T entity) {
    this.storage.put(entity.getId(), entity);
  }

  @Override
  public T find(Long id) {
    return this.storage.get(id);
  }
}
