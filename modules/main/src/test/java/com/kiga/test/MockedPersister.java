package com.kiga.test;

import com.kiga.spec.Persister;

/**
 * Created by rainerh on 03.04.16.
 */
public class MockedPersister<T> implements Persister<T> {
  @Override
  public T persist(T entity) {
    return entity;
  }
}
