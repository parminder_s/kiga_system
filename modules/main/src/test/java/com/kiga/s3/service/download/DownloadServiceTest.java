package com.kiga.s3.service.download;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.kiga.s3.domain.S3File;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.download.DownloadedS3Image;
import com.kiga.s3.service.KigaAmazonS3Client;
import java.io.IOException;
import java.io.InputStream;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author bbs
 * @since 9/25/16.
 */
public class DownloadServiceTest {
  private InputStream inputStream;
  private KigaAmazonS3Client kigaAmazonS3Client;

  /** Init the tests mocks. */
  @Before
  public void init() {
    inputStream = this.getClass().getResourceAsStream("/com.kiga.ideas/test.png");
    kigaAmazonS3Client = mock(KigaAmazonS3Client.class);

    S3Object s3Object = mock(S3Object.class);
    ObjectMetadata objectMetadata = mock(ObjectMetadata.class);
    when(s3Object.getObjectMetadata()).thenReturn(objectMetadata);
    when(objectMetadata.getContentType()).thenReturn("image/png");

    when(kigaAmazonS3Client.stream(any(), any())).thenReturn(s3Object);
  }

  @Test
  public void testDownloadUsingRequestService() throws IOException {
    DownloadUsingRequestService mock = new DownloadUsingRequestServiceMock();
    S3Image image = new S3Image();
    image.setName("ABC");
    DownloadedS3Image downloadedS3Image = mock.download(image);

    Assert.assertEquals("image/png", downloadedS3Image.getContentType());
    Assert.assertEquals(inputStream, downloadedS3Image.getStreamedImage());
  }

  class DownloadUsingRequestServiceMock extends DownloadUsingRequestService {
    @Override
    protected InputStream getStreamedImage(S3File image) throws IOException {
      return inputStream;
    }
  }
}
