package com.kiga.s3.web.converter;

import static org.junit.Assert.assertEquals;

import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.web.response.KigaS3FileViewModel;
import org.junit.Test;

/**
 * @author bbs
 * @since 4/17/17.
 */
public class KigaS3FileViewModelConverterTest {
  @Test
  public void convertToResponse() {
    KigaS3FileViewModelConverter kigaS3FileViewModelConverter = new KigaS3FileViewModelConverter();
    KigaS3File entity = new KigaS3File();
    KigaS3FileViewModel received = kigaS3FileViewModelConverter.convertToResponse(entity);
    assertEquals(entity.getName(), received.getFileName());
  }
}
