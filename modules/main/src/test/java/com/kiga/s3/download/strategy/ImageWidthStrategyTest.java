package com.kiga.s3.download.strategy;

import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.ImageHeightStrategy;
import com.kiga.s3.service.resize.ImageWidthStrategy;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author bbs
 * @since 5/11/16.
 */
public class ImageWidthStrategyTest {
  private static final int WIDTH = 123;
  private static final String PREFIX = "w";

  @Test
  public void test() {
    S3Image s3Image = new S3Image();
    s3Image.setWidth(1200);
    s3Image.setHeight(800);

    double originalHeight = s3Image.getHeight();
    double originalWidth = s3Image.getWidth();
    double originalRatio = originalWidth / originalHeight;
    double outputHeight = (WIDTH / originalRatio);

    ImageWidthStrategy imageWidthStrategy = new ImageWidthStrategy(s3Image, WIDTH);
    Assert.assertEquals(imageWidthStrategy.getWidth(), WIDTH);
    Assert.assertEquals(imageWidthStrategy.getHeight(), (int) outputHeight);
    Assert.assertEquals(imageWidthStrategy.getCachedDataKey(), PREFIX + WIDTH);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNullImage() {
    new ImageWidthStrategy(null, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNullWidth() {
    S3Image s3Image = new S3Image();
    s3Image.setWidth(null);
    new ImageWidthStrategy(s3Image, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNullHeight() {
    S3Image s3Image = new S3Image();
    s3Image.setHeight(null);
    s3Image.setWidth(100);
    new ImageWidthStrategy(s3Image, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInvalidWidth() {
    S3Image s3Image = new S3Image();
    s3Image.setWidth(-1);
    new ImageWidthStrategy(s3Image, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInvalidHeight() {
    S3Image s3Image = new S3Image();
    s3Image.setHeight(-1);
    s3Image.setWidth(100);
    new ImageWidthStrategy(s3Image, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInvalidDestWidth() {
    S3Image s3Image = new S3Image();
    s3Image.setHeight(100);
    s3Image.setWidth(100);
    new ImageWidthStrategy(s3Image, -1);
  }
}
