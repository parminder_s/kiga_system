package com.kiga.s3.util;

import com.kiga.s3.url.UrlBuilder;
import com.kiga.s3.url.UrlBuilderFactory;
import com.kiga.s3.url.UrlNamingStrategy;
import org.junit.Assert;
import org.junit.Test;

import java.net.MalformedURLException;

/**
 * @author bbs
 * @since 5/27/16.
 */
public class UrlBuilderTest {
  private static final String MOCK_URL = "https://s3-eu-west-1.amazonaws.com/kigadev/a/b/c/IMG.jpg";
  private static final String MOCK_URL_MATCHER =
    "https://s3-eu-west-1.amazonaws.com/kigadev/a/b/c/IMG[A-Za-z0-9]{5}.jpg";
  private static final String MOCK_URL_NO_FOLDER = "https://s3-eu-west-1.amazonaws.com/kigadev/IMG.jpg";

  @Test
  public void urlParserNew() {
    String url = "https://s3-region.amazonaws.com/bucket/folder1/folder2/image.jpg";
    UrlBuilder urlBuilder = getUrlBuilder(url);

    Assert.assertEquals("https://s3-region.amazonaws.com", urlBuilder.getBaseUrl());
    Assert.assertEquals("bucket", urlBuilder.getBucketName());
    Assert.assertEquals("folder1/folder2", urlBuilder.getFolder());
    Assert.assertTrue(urlBuilder.getFileName().matches("image[A-Za-z0-9]{5}.jpg"));
    Assert.assertEquals(UrlNamingStrategy.BucketInPath, urlBuilder.getUrlNamingStrategy());
  }

  @Test
  public void urlParserOld() {
    String url = "https://bucket.s3.amazonaws.com/folder1/folder2/image.jpg";

    UrlBuilder urlBuilder = getUrlBuilder(url);

    Assert.assertEquals("https://s3.amazonaws.com", urlBuilder.getBaseUrl());
    Assert.assertEquals("bucket", urlBuilder.getBucketName());
    Assert.assertEquals("folder1/folder2", urlBuilder.getFolder());
    Assert.assertTrue(urlBuilder.getFileName().matches("image[A-Za-z0-9]{5}.jpg"));
  }

  @Test public void urlBucketInHostBucketChange() {
    String url = "https://bucket.s3.amazonaws.com/folder1/folder2/image.jpg";

    UrlBuilder urlBuilder = getUrlBuilder(url).useBucket("otherBucket");

    Assert.assertTrue(urlBuilder.getUrl().matches(
      "https:\\/\\/otherBucket.s3.amazonaws.com\\/folder1\\/folder2\\/image[A-Za-z0-9]{5}.jpg"));
  }

  @Test
  public void testNoChange() {
    String inputUrl = MOCK_URL;
    String outputUrl = getUrlBuilder(inputUrl).getUrl();
    Assert.assertTrue(outputUrl.matches(MOCK_URL_MATCHER));
  }

  @Test
  public void testFolderChange() {
    String inputUrl = MOCK_URL;
    String outputUrl = getUrlBuilder(inputUrl).useFolder("d/e/f").getUrl();
    String matcher = "https:\\/\\/s3-eu-west-1.amazonaws"
      + ".com\\/kigadev\\/d\\/e\\/f\\/IMG[A-Za-z0-9]{5}.jpg";
    Assert.assertTrue(outputUrl.matches(matcher));
  }

  @Test
  public void testBucketChange() {
    String inputUrl = MOCK_URL;
    String outputUrl = getUrlBuilder(inputUrl).useBucket("kigaprod").getUrl();
    Assert.assertTrue(outputUrl.matches(
      "https:\\/\\/s3-eu-west-1.amazonaws.com\\/kigaprod\\/a\\/b\\/c\\/IMG[A-Za-z0-9]{5}.jpg"));
  }

  @Test
  public void testFilenameSuffix() {
    String inputUrl = MOCK_URL;
    String outputUrl = getUrlBuilder(inputUrl).appendFilenameSuffix("p150139").getUrl();
    Assert.assertTrue(outputUrl.matches(
      "https:\\/\\/s3-eu-west-1.amazonaws.com\\/kigadev\\/a\\/b\\/c\\/IMG[A-Za-z0-9]{5}_p150139.jpg"
    ));
  }

  @Test
  public void testAllChanged() {
    String inputUrl = MOCK_URL;
    String outputUrl = getUrlBuilder(inputUrl).useFolder("q/w/e").useBucket("kigaprod")
      .appendFilenameSuffix("p150139").getUrl();
    Assert.assertTrue(outputUrl.matches(
      "https:\\/\\/s3-eu-west-1.amazonaws.com\\/kigaprod\\/q\\/w\\/e\\/IMG[A-Za-z0-9]{5}"
        + "_p150139.jpg"));
  }

  @Test
  public void testNoInputFolder() {
    String inputUrl = MOCK_URL_NO_FOLDER;
    String outputUrl = getUrlBuilder(inputUrl).useFolder("q/w/e").useBucket("kigaprod")
      .appendFilenameSuffix("p150139").getUrl();
    Assert.assertTrue(outputUrl.matches(
      "https:\\/\\/s3-eu-west-1.amazonaws.com\\/kigaprod\\/q\\/w\\/e\\/IMG[A-Za-z0-9]{5}"
        + "_p150139.jpg"));
  }

  @Test
  public void testGetFileName() {
    String inputUrl = MOCK_URL_NO_FOLDER;
    String outputFileName1 = getUrlBuilder(inputUrl).useFolder("q/w/e").useBucket("kigaprod")
      .getFileName();
    String outputFileName2 = getUrlBuilder(inputUrl).useFolder("q/w/e").useBucket("kigaprod")
      .appendFilenameSuffix("p150139").getFileName();

    Assert.assertTrue(outputFileName1.matches("IMG[A-Za-z0-9]{5}.jpg"));
    Assert.assertTrue(outputFileName2.matches("IMG[A-Za-z0-9]{5}_p150139.jpg"));
  }

  @Test
  public void testGetFileNameWithFolder() {
    String inputUrl = MOCK_URL_NO_FOLDER;
    String outputFileNameWithFolder = getUrlBuilder(inputUrl).useFolder("q/w/e")
      .useBucket("kigaprod").appendFilenameSuffix("p150139").getFileNameWithFolder();
    Assert.assertTrue(
      outputFileNameWithFolder.matches("q\\/w\\/e\\/IMG[A-Za-z0-9]{5}_p150139.jpg"));
  }

  @Test
  public void testGetFileNameWithFolderFromUnknownBucket() {
    String inputUrl = "https://s3-eu-west-1.amazonaws.com/kigadev/a/b/c/IMG.jpg";
    String outputFile = getUrlBuilder(inputUrl).getFileNameWithFolder();
    Assert.assertTrue(outputFile.matches("a\\/b\\/c\\/IMG[A-Za-z0-9]{5}.jpg"));
  }

  private UrlBuilder getUrlBuilder(String url) {
    try {
      return UrlBuilderFactory.create(url);
    } catch (MalformedURLException mue) {
      return null;
    }
  }
}
