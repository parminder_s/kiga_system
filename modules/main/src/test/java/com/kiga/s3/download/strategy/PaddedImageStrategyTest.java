package com.kiga.s3.download.strategy;

import com.kiga.s3.service.resize.PaddedImageStrategy;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author bbs
 * @since 5/11/16.
 */
public class PaddedImageStrategyTest {
  private static final int WIDTH = 123;
  private static final int HEIGHT = 456;
  private static final String PREFIX = "p";

  @Test
  public void test() {
    PaddedImageStrategy paddedImageStrategy = new PaddedImageStrategy(WIDTH, HEIGHT);
    Assert.assertEquals(paddedImageStrategy.getHeight(), HEIGHT);
    Assert.assertEquals(paddedImageStrategy.getWidth(), WIDTH);
    Assert.assertEquals(paddedImageStrategy.getCachedDataKey(), PREFIX + WIDTH + HEIGHT);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInvalidWidth() {
    new PaddedImageStrategy(-1, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInvalidHeight() {
    new PaddedImageStrategy(100, -1);
  }
}
