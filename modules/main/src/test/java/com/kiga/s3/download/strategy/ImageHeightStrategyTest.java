package com.kiga.s3.download.strategy;

import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.ImageHeightStrategy;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author bbs
 * @since 5/11/16.
 */
public class ImageHeightStrategyTest {
  private static final int HEIGHT = 123;
  private static final String PREFIX = "h";

  @Test
  public void test() {
    S3Image s3Image = new S3Image();
    s3Image.setWidth(1200);
    s3Image.setHeight(800);

    double originalHeight = s3Image.getHeight();
    double originalWidth = s3Image.getWidth();
    double originalRatio = originalWidth / originalHeight;
    double outputWidth = (HEIGHT / originalRatio);

    ImageHeightStrategy imageWidthStrategy = new ImageHeightStrategy(s3Image, HEIGHT);
    Assert.assertEquals(imageWidthStrategy.getHeight(), HEIGHT);
    Assert.assertEquals(imageWidthStrategy.getWidth(), (int) outputWidth);
    Assert.assertEquals(imageWidthStrategy.getCachedDataKey(), PREFIX + HEIGHT);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNullImage() {
    new ImageHeightStrategy(null, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNullWidth() {
    S3Image s3Image = new S3Image();
    s3Image.setWidth(null);
    new ImageHeightStrategy(s3Image, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNullHeight() {
    S3Image s3Image = new S3Image();
    s3Image.setHeight(null);
    s3Image.setWidth(100);
    new ImageHeightStrategy(s3Image, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInvalidWidth() {
    S3Image s3Image = new S3Image();
    s3Image.setWidth(-1);
    new ImageHeightStrategy(s3Image, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInvalidHeight() {
    S3Image s3Image = new S3Image();
    s3Image.setHeight(-1);
    s3Image.setWidth(100);
    new ImageHeightStrategy(s3Image, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInvalidDestHeight() {
    S3Image s3Image = new S3Image();
    s3Image.setHeight(100);
    s3Image.setWidth(100);
    new ImageHeightStrategy(s3Image, -1);
  }
}
