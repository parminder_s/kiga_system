package com.kiga.s3.download;

import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

/**
 * @author bbs
 * @since 9/25/16.
 */
public class DownloadedS3ImageTest {
  @Test
  public void test() {
    assertThat(DownloadedS3Image.class, hasValidGettersAndSetters());
  }
}
