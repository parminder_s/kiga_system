package com.kiga.s3.service.resize;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.kiga.integration.amazon.s3.config.AmazonS3Parameters;
import com.kiga.s3.EAmazonS3Acl;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.download.DownloadedS3Image;
import com.kiga.s3.service.KigaAmazonS3Client;
import com.kiga.s3.service.download.DownloadService;
import com.kiga.s3.service.resize.exception.ResizeException;
import com.kiga.s3.wrapper.DefaultImageIoWrapper;
import com.kiga.s3.wrapper.ImageIoWrapper;
import com.kiga.spec.Persister;
import com.kiga.upload.ImageProcessor;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * This service is required for generating resized images for a given S3Image.
 * By giving the required dimensions that service checks the internal cache
 * and if the image is not yet available downloads it from S3, resizes and uploads
 * the new thumbnail to S3 along storing into the cache (property of S3Image entity).
 *
 * @author bbs
 * @since 4/24/16.
 */
public class S3ImageResizeServiceTest {
  private static final String BUCKET_NAME = "DummyBucketName";
  private static final int WIDTH = 100;
  private static final int HEIGHT = 100;
  private static final String CACHED_KEY = "p100100";
  private static final String FILE_EXTENSION = "png";
  private static final String CONTENT_TYPE = "image/png";
  private static final String DUMMY_URL = "http://example.com/DummyBucketName/test.png";
  private S3ImageResizeService s3ImageResizeService;
  private ImageIoWrapper imageIoWrapper = mock(DefaultImageIoWrapper.class);
  private KigaAmazonS3Client kigaAmazonS3Client = mock(KigaAmazonS3Client.class);
  private BufferedImage downloadedImage;
  private ImageProcessor imageProcessor = mock(ImageProcessor.class);
  private DownloadService downloadService = mock(DownloadService.class);

  @SuppressWarnings("unchecked")
  private Persister<S3Image> persister = mock(Persister.class);

  /**
   * Init the tests mocks.
   *
   * @throws IOException exception thrown
   */
  @Before
  public void init() throws IOException {
    AmazonS3Parameters amazonS3Parameters = mock(AmazonS3Parameters.class);

    s3ImageResizeService = new S3ImageResizeService(amazonS3Parameters, imageProcessor, persister,
      kigaAmazonS3Client, downloadService, imageIoWrapper);

    InputStream streamedImage = this.getClass().getResourceAsStream("/com.kiga.ideas/test.png");
    DownloadedS3Image downloadedS3Image = new DownloadedS3Image(streamedImage, CONTENT_TYPE);
    when(downloadService.download(any())).thenReturn(downloadedS3Image);

    downloadedImage = new DefaultImageIoWrapper().read(streamedImage);
    when(imageIoWrapper.read(any())).thenReturn(downloadedImage);

    when(amazonS3Parameters.getBucketName()).thenReturn(BUCKET_NAME);

    when(imageProcessor.resize(downloadedImage, WIDTH, HEIGHT)).thenReturn(downloadedImage);
  }

  @Test
  public void testNullImage() throws IOException {
    String url = s3ImageResizeService
      .getResizedImageUrl(null, new PaddedImageStrategy(WIDTH, HEIGHT));
    Assert.assertNull(url);
  }

  @Test
  public void testEmptyImageUrl() throws IOException {
    S3Image s3Image = new S3Image();
    String url = s3ImageResizeService
      .getResizedImageUrl(s3Image, new PaddedImageStrategy(WIDTH, HEIGHT));
    Assert.assertNull(url);
  }

  @Test
  public void testZeroedStream() throws IOException {
    InputStream inputStream = mock(InputStream.class);
    when(inputStream.available()).thenReturn(0);
    DownloadedS3Image downloadedS3Image = new DownloadedS3Image(inputStream, CONTENT_TYPE);
    when(downloadService.download(any())).thenReturn(downloadedS3Image);
    S3Image image = new S3Image();
    image.setUrl(DUMMY_URL);
    String url = s3ImageResizeService
      .getResizedImageUrl(image, new PaddedImageStrategy(WIDTH, HEIGHT));
    Assert.assertNotNull(url);
    Assert.assertTrue(StringUtils.isEmpty(url));
  }

  @Test
  public void testImageIoWrapperCouldNotReadImage() throws IOException {
    when(imageIoWrapper.read(any())).thenReturn(null);
    S3Image image = new S3Image();
    image.setUrl(DUMMY_URL);
    String url = s3ImageResizeService
      .getResizedImageUrl(image, new PaddedImageStrategy(WIDTH, HEIGHT));
    Assert.assertNotNull(url);
    Assert.assertTrue(StringUtils.isEmpty(url));
  }

  @Test
  public void testGetResizedImageUrl() throws IOException {
    S3Image image = new S3Image();
    image.setUrl(DUMMY_URL);
    String url = s3ImageResizeService
      .getResizedImageUrl(image, new PaddedImageStrategy(WIDTH, HEIGHT));
    Assert.assertNotNull(url);
    Assert.assertTrue(StringUtils.isNotBlank(url));
    verify(imageIoWrapper).write(eq(downloadedImage), eq(FILE_EXTENSION), any());
    Assert.assertNotNull(image.getCachedDataJson());
    Assert.assertNotNull(image.getCachedDataJson().get(CACHED_KEY));
    Assert.assertTrue(StringUtils.isNotBlank(image.getCachedDataJson().get(CACHED_KEY)));
    verify(kigaAmazonS3Client).upload(any(), notNull(String.class), eq(CONTENT_TYPE),
      eq(EAmazonS3Acl.READ_ALL.getAcl())); // urlBuilder.getFileNameWithFolder()
    verify(persister).persist(image);
  }

  @Test
  public void testGetResizedImageUrlFromCachedData() throws IOException {
    S3Image image = new S3Image();
    image.setUrl(DUMMY_URL);

    Map<String, String> cachedDataJson = new HashMap<>();
    String cachedDummyUrl = "CachedDummyUrl";
    cachedDataJson.put(CACHED_KEY, cachedDummyUrl);

    image.setCachedDataJson(cachedDataJson);

    String url = s3ImageResizeService
      .getResizedImageUrl(image, new PaddedImageStrategy(WIDTH, HEIGHT));
    Assert.assertEquals(cachedDummyUrl, url);
  }

  @Test
  public void testAmazonS3ExceptionIsHandledNicely() throws IOException {
    when(kigaAmazonS3Client.upload(any(), any(), any(), any())).thenThrow(AmazonS3Exception.class);

    S3Image image = new S3Image();
    image.setUrl(DUMMY_URL);
    s3ImageResizeService.getResizedImageUrl(image, new PaddedImageStrategy(WIDTH, HEIGHT));
  }

  @Test
  public void testResizeExceptionIsHandledNicely() throws IOException {
    when(imageProcessor.resize(downloadedImage, WIDTH, HEIGHT)).thenThrow(ResizeException.class);
    S3Image image = new S3Image();
    image.setUrl(DUMMY_URL);
    s3ImageResizeService.getResizedImageUrl(image, new PaddedImageStrategy(WIDTH, HEIGHT));
  }
}
