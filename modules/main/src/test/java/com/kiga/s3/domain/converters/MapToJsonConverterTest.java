package com.kiga.s3.domain.converters;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bbs
 * @since 5/11/16.
 */
public class MapToJsonConverterTest {
  @Test
  public void test() {
    MapToJsonConverter converter = new MapToJsonConverter();

    Map<String, String> map = new HashMap<>();
    map.put("key1", "value1");
    map.put("key2", "value2");

    String json = "{\"key1\":\"value1\",\"key2\":\"value2\"}";

    String converted = converter.convertToDatabaseColumn(map);
    Map<String, String> convertedMap = converter
      .convertToEntityAttribute(json);

    Assert.assertEquals(json, converted);
    Assert.assertEquals(map, convertedMap);
  }
}
