package com.kiga.payment.service;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MdxiTestClassSub {
  private String bar;
  private String foo;
}
