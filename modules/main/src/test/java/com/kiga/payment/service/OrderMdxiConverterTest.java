package com.kiga.payment.service;

import static com.kiga.payment.mpay24.properties.Mpay24Brand.HOBEX_AT;
import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.kiga.payment.mpay24.model.Order;
import com.kiga.payment.mpay24.model.sub.Customer;
import com.kiga.payment.mpay24.model.sub.Payment;
import com.kiga.payment.mpay24.model.sub.PaymentTypes;
import com.kiga.payment.mpay24.model.sub.ShoppingCart;
import com.kiga.payment.mpay24.model.sub.TemplateSet;
import com.kiga.payment.mpay24.model.sub.Url;
import com.kiga.payment.mpay24.properties.Mpay24Type;
import org.junit.Test;

public class OrderMdxiConverterTest {
  @Test
  public void testNormal() {
    Order order = Order.builder()
      .tid("15904")
      .templateSet(TemplateSet.builder().cssName("WEB").language("DE").build())
      .paymentTypes(PaymentTypes.builder()
        .enable(1)
        .payment(Payment.builder().brand(HOBEX_AT).type(Mpay24Type.ELV).build()
        ).build())
      .shoppingCart(ShoppingCart.builder()
        .description("kigaportal KiGa-Abo Stud. - 12 Monate, Abbuchungsauftrag: EUR 45,90")
        .build())
      .price("45.90")
      .currency("EUR")
      .customer(Customer.builder().id("15904").useProfile(true).build())
      .url(Url.builder()
        .success("https://www.kigaportal.com/de/payment/success/15904")
        .cancel("https://www.kigaportal.com/de/payment/failure/15904")
        .error("https://www.kigaportal.com/de/payment/failure/15904")
        .build())
      .build();

    String expectedMdxi = ""
      + "<Order>"
      + "<Tid>15904</Tid>"
      + "<TemplateSet Language=\"DE\">WEB</TemplateSet><PaymentTypes Enable=\"1\">"
      + "<Payment Type=\"ELV\" Brand=\"HOBEX-AT\"></Payment></PaymentTypes>"
      + "<ShoppingCart><Description>"
      + "kigaportal KiGa-Abo Stud. - 12 Monate, Abbuchungsauftrag: EUR 45,90"
      + "</Description></ShoppingCart>"
      + "<Price>45.90</Price>"
      + "<Currency>EUR</Currency>"
      + "<Customer Id=\"15904\" UseProfile=\"true\"></Customer>"
      + "<URL>"
      + "<Success>https://www.kigaportal.com/de/payment/success/15904</Success>"
      + "<Error>https://www.kigaportal.com/de/payment/failure/15904</Error>"
      + "<Cancel>https://www.kigaportal.com/de/payment/failure/15904</Cancel>"
      + "</URL>"
      + "</Order>";

    assertEquals(
      expectedMdxi,
      new MdxiConverter(new XmlMapper()).convertToMdxi(order));
  }
}
