package com.kiga.payment.service;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class MdxiTestClass {
  @JacksonXmlProperty(isAttribute = true)
  private String attribute1;
  @JacksonXmlProperty(isAttribute = true)
  private int attribute2;

  private String child1;
  private int child2;
}
