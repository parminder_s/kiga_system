package com.kiga.payment.service;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class MdxiTestClassSuper {
  @JacksonXmlElementWrapper(localName = "subs", useWrapping = false)
  @JacksonXmlProperty(localName = "sub")
  private List<MdxiTestClassSub> subsWithoutWrapping;

  private List<MdxiTestClassSub> defaultSubs;
}
