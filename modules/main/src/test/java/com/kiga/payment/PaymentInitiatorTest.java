package com.kiga.payment;

import static com.kiga.payment.PaymentType.MP_VISA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;

import com.kiga.main.locale.Locale;
import com.kiga.money.Money;
import com.kiga.payment.mpay24.properties.Mpay24AuthProperties;
import com.kiga.payment.mpay24.properties.Mpay24Properties;
import com.kiga.payment.mpay24.service.OrderCreator;
import com.kiga.payment.service.MdxiConverter;
import com.mpay24.etp.ETP;
import org.junit.Test;

public class PaymentInitiatorTest {
  @Test
  public void simpleTest() {
    PaymentInitiatorConfig config = new PaymentInitiatorConfig(
      MP_VISA, "https://endpoint.kigaportal.com/finished", "ups",
      new Money("12.50", "EUR"), "at", Locale.de);
    Mpay24Properties properties = new Mpay24Properties();
    properties.setAbo(Mpay24AuthProperties.builder().merchantId(1L).build());

    assertEquals("https://endpoint.kigaportal.com/finished",
      new PaymentInitiator(
        mock(ETP.class), properties, mock(OrderCreator.class),
        mock(MdxiConverter.class))
        .initiate(config));
  }
}
