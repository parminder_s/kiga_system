package com.kiga.payment.mpay24.service;

import static com.kiga.payment.PaymentType.MP_CC;
import static com.kiga.payment.PaymentType.MP_MC;
import static com.kiga.payment.PaymentType.MP_VISA;
import static com.kiga.payment.PaymentType.PAYPAL;
import static com.kiga.payment.mpay24.properties.Mpay24Brand.HOBEX_AT;
import static com.kiga.payment.mpay24.properties.Mpay24Brand.HOBEX_DE;
import static com.kiga.payment.mpay24.properties.Mpay24Brand.MASTERCARD;
import static com.kiga.payment.mpay24.properties.Mpay24Brand.VISA;
import static com.kiga.payment.mpay24.properties.Mpay24Type.CC;
import static com.kiga.payment.mpay24.properties.Mpay24Type.ELV;
import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertEquals;

import com.kiga.main.locale.Locale;
import com.kiga.money.Money;
import com.kiga.payment.PaymentInitiatorConfig;
import com.kiga.payment.PaymentInitiatorConfigFactory;
import com.kiga.payment.PaymentType;
import com.kiga.payment.mpay24.model.Order;
import com.kiga.payment.mpay24.model.sub.Customer;
import com.kiga.payment.mpay24.model.sub.Payment;
import com.kiga.payment.mpay24.model.sub.PaymentTypes;
import com.kiga.payment.mpay24.model.sub.ShoppingCart;
import com.kiga.payment.mpay24.model.sub.TemplateSet;
import com.kiga.payment.mpay24.model.sub.Url;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class OrderCreatorTest {
  @Rule
  public ExpectedException exception = ExpectedException.none();

  @Test
  public void test() {
    PaymentInitiatorConfig config = new PaymentInitiatorConfig(
      PaymentType.MP_ELV, "/somewhere/success", "/somewhere/failure",
      new Money("25", "EUR"), "at", Locale.de);
    config.setDescription("payment for a product");

    Order order = new OrderCreator().create(config, "some_tid");

    assertThat(order, sameBeanAs(Order.builder()
      .tid("some_tid")
      .templateSet(TemplateSet.builder().cssName("WEB").language("DE").build())
      .paymentTypes(PaymentTypes.builder()
        .enable(1)
        .payment(Payment.builder().type(ELV).brand(HOBEX_AT).build())
        .build())
      .shoppingCart(ShoppingCart.builder()
        .description("payment for a product")
        .build())
      .price("25.00")
      .currency("EUR")
      .customer(Customer.builder().id("some_tid").useProfile(true).build())
      .url(Url.builder()
        .success("/somewhere/success")
        .cancel("/somewhere/failure")
        .error("/somewhere/failure")
        .build())
      .build()
    ));
  }

  @Test
  public void testVerifyWorkingWithOnlyConstructorCall() {
    PaymentInitiatorConfig config = new PaymentInitiatorConfig(
      PaymentType.MP_ELV, "/somewhere/success", "/somewhere/failure",
      new Money("25", "EUR"), "at", Locale.de);

    assertThat(new OrderCreator().create(config, "some_tid"), instanceOf(Order.class));
  }

  @Test
  public void testWithInvalidHobexCountry() {
    PaymentInitiatorConfig config = PaymentInitiatorConfigFactory.create();
    config.setCountryCode("tr");

    exception.expect(RuntimeException.class);

    new OrderCreator().create(config, "");
  }

  @Test
  public void testWithUpperCaseCountry() {
    PaymentInitiatorConfig config = PaymentInitiatorConfigFactory.create();
    config.setCountryCode("DE");

    Order order = new OrderCreator().create(config, "");

    assertEquals(
      HOBEX_DE,
      order.getPaymentTypes().getPayment().getBrand());
  }

  @Test
  public void testVisa() {
    PaymentInitiatorConfig config = PaymentInitiatorConfigFactory.create();
    config.setPaymentType(MP_VISA);

    Order order = new OrderCreator().create(config, "");

    assertEquals(
      Payment.builder().type(CC).brand(VISA).build(),
      order.getPaymentTypes().getPayment());
  }

  @Test
  public void testMasterCard() {
    PaymentInitiatorConfig config = PaymentInitiatorConfigFactory.create();
    config.setPaymentType(MP_MC);

    Order order = new OrderCreator().create(config, "");

    assertEquals(
      Payment.builder().type(CC).brand(MASTERCARD).build(),
      order.getPaymentTypes().getPayment());
  }

  @Test
  public void invalidPaymentMethod() {
    PaymentInitiatorConfig config = PaymentInitiatorConfigFactory.create();
    config.setPaymentType(PAYPAL);

    exception.expect(RuntimeException.class);

    new OrderCreator().create(config, "");
  }

  @Test
  public void nullPaymentMethod() {
    PaymentInitiatorConfig config = PaymentInitiatorConfigFactory.create();
    config.setPaymentType(null);

    exception.expect(NullPointerException.class);

    new OrderCreator().create(config, "");
  }
}
