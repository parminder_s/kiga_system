package com.kiga.payment.service;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

public class MdxiConverterTest {
  @Test
  public void testSimple() {
    MdxiTestClass testClass = MdxiTestClass.builder()
      .attribute1("foo").attribute2(5).child1("bar").child2(1)
      .build();

    String mdxi = new MdxiConverter(new XmlMapper()).convertToMdxi(testClass);

    assertEquals(""
      + "<MdxiTestClass attribute1=\"foo\" attribute2=\"5\">"
      + "<child1>bar</child1>"
      + "<child2>1</child2>"
      + "</MdxiTestClass>", mdxi);
  }

  @Test
  public void testSubClass() {
    MdxiTestClassSuper testClass = MdxiTestClassSuper.builder()
      .subsWithoutWrapping(Arrays.asList(
        MdxiTestClassSub.builder().bar("bar1").foo("foo1").build(),
        MdxiTestClassSub.builder().bar("bar2").foo("foo2").build()
      ))
      .defaultSubs(Collections.singletonList(
        MdxiTestClassSub.builder().bar("bar").foo("foo").build()
      )).build();

    String mdxi = new MdxiConverter(new XmlMapper()).convertToMdxi(testClass);

    assertEquals(""
      + "<MdxiTestClassSuper>"
      + "<defaultSubs><defaultSubs><bar>bar</bar><foo>foo</foo></defaultSubs></defaultSubs>"
      + "<sub><bar>bar1</bar><foo>foo1</foo></sub>"
      + "<sub><bar>bar2</bar><foo>foo2</foo></sub>"
      + "</MdxiTestClassSuper>", mdxi);
  }

}
