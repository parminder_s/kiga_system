package com.kiga.payment;

import com.kiga.main.locale.Locale;
import com.kiga.money.Money;

public class PaymentInitiatorConfigFactory {
  /**
   * Test Factory for PaymentInitiatorConfig.
   */
  public static PaymentInitiatorConfig create() {
    PaymentInitiatorConfig config = new PaymentInitiatorConfig(
      PaymentType.MP_ELV, "/somewhere/success", "/somewhere/failure",
      new Money("25", "EUR"), "at", Locale.de);
    config.setDescription("payment for a product");

    return config;
  }
}
