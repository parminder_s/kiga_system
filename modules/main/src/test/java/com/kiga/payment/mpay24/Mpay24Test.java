package com.kiga.payment.mpay24;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import com.kiga.mail.Mailer;
import com.kiga.payment.PaymentCheckerService;
import com.kiga.payment.domain.Payment;
import com.kiga.payment.mpay24.properties.MPay24ParameterEnum;
import com.kiga.payment.mpay24.properties.Mpay24AuthProperties;
import com.kiga.payment.mpay24.properties.Mpay24Properties;
import com.kiga.payment.repository.PaymentRepository;
import com.mpay24.etp.ETP;
import com.mpay24.etp.Parameter;
import com.mpay24.etp.Status;
import org.junit.Assert;
import org.junit.Test;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by faxxe on 4/5/16.
 */

public class Mpay24Test {
  PaymentRepository paymentRepository = mock(PaymentRepository.class);
  Mpay24Properties mpay24Properties = mock(Mpay24Properties.class);
  Mpay24AuthProperties shopAuth = mock(Mpay24AuthProperties.class);
  Mailer mailer = mock(Mailer.class);

  ETP shopEtp = mock(ETP.class);
  Payment payment = mock(Payment.class);
  Payment payment1 = mock(Payment.class);
  Payment payment2 = mock(Payment.class);
  Payment payment3 = mock(Payment.class);
  Payment payment4 = mock(Payment.class);

  @Test
  public void checkShopPaymentTest() {
    PaymentCheckerService paymentCheckerService1 = new PaymentCheckerService(
      paymentRepository, mpay24Properties, shopEtp, mailer);

    when(payment.getTransactionId()).thenReturn("1111");
    when(mpay24Properties.getShop()).thenReturn(shopAuth);
    when(shopAuth.getMerchantId()).thenReturn(222L);

    final PaymentCheckerService paymentCheckerService = spy(paymentCheckerService1);
    Payment payment = new Payment();
    payment.setId(11L);
    payment.setCreated(new Date(0));
    List<Parameter> parameters = new ArrayList<>();
    Parameter param = new Parameter();
    param.setName("STATUS");
    param.setValue("BILLED");
    parameters.add(param);

    Mpay24Response mpay24Response = new Mpay24Response(Status.OK, "OK", parameters);

    doReturn(mpay24Response).when(paymentCheckerService).getMpay24TransactionStatus(any(), any());
    PaymentCheckerResponse paymentCheckerResponse = paymentCheckerService
      .checkShopPayment(payment, false);

    Assert.assertEquals(PaymentStatusEnum.OK, paymentCheckerResponse.getPaymentStatus());

  }

  @Test
  public void alreadySucceededTest() {
    List<Payment> payments = new ArrayList<>();
    payment.setStatus("initialized");
    payments.add(payment);

    PaymentCheckerService paymentCheckerService1 = new PaymentCheckerService(
      paymentRepository, mpay24Properties, shopEtp, mailer);

    PaymentCheckerService paymentCheckerService = spy(paymentCheckerService1);
    when(paymentRepository.findByReferenceCodeAndReferenceId(any(), any())).thenReturn(payments);

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.OK)).when(paymentCheckerService)
      .checkShopPayment(payment, false);
  }

  @Test
  public void failedTest() {


    payment.setStatus("initialized");
    when(payment.getLastEdited()).thenReturn(new Date());
    payment1.setStatus("initialized");
    when(payment1.getLastEdited()).thenReturn(new Date());

    List<Payment> payments = new ArrayList<>();
    payments.add(payment);
    payments.add(payment1);


    PaymentCheckerService paymentCheckerService1 = new PaymentCheckerService(
      paymentRepository, mpay24Properties, shopEtp, mailer);

    PaymentCheckerService paymentCheckerService = spy(paymentCheckerService1);
    when(paymentRepository.findByReferenceCodeAndReferenceId(any(), any())).thenReturn(payments);

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.NOT_FOUND))
      .when(paymentCheckerService).checkShopPayment(payment, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.OK)).when(paymentCheckerService)
      .checkShopPayment(payment1, false);

    Assert.assertEquals(PaymentStatusEnum.OK, paymentCheckerService
      .checkShopPayments(1111L, false, new Date(0)).getPaymentStatus());

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.OK)).when(paymentCheckerService)
      .checkShopPayment(payment, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.FAILED)).when(paymentCheckerService)
      .checkShopPayment(payment1, false);

    Assert.assertEquals(PaymentStatusEnum.OK, paymentCheckerService
      .checkShopPayments(1111L, false, new Date(0)).getPaymentStatus());

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.ERROR)).when(paymentCheckerService)
      .checkShopPayment(payment, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.OK)).when(paymentCheckerService)
      .checkShopPayment(payment1, false);

    Assert.assertEquals(PaymentStatusEnum.OK,
      paymentCheckerService.checkShopPayments(1111L, false, new Date(0)).getPaymentStatus());

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.OK)).when(paymentCheckerService)
      .checkShopPayment(payment, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.ERROR)).when(paymentCheckerService)
      .checkShopPayment(payment1, false);

    Assert.assertEquals(PaymentStatusEnum.OK, paymentCheckerService
      .checkShopPayments(1111L, false, new Date(0)).getPaymentStatus());

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.SUSPENDED))
      .when(paymentCheckerService).checkShopPayment(payment, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.OK)).when(paymentCheckerService)
      .checkShopPayment(payment1, false);

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.OK)).when(paymentCheckerService)
      .checkShopPayment(payment, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.SUSPENDED))
      .when(paymentCheckerService).checkShopPayment(payment1, false);

    Assert.assertEquals(PaymentStatusEnum.OK, paymentCheckerService
      .checkShopPayments(1111L, false, new Date(0)).getPaymentStatus());

  }

  @Test
  public void multiPaymentTest() {

    payment.setStatus("initialized");
    payment.setLastEdited(new Date());
    when(payment.getLastEdited()).thenReturn(new Date());
    payment1.setStatus("initialized");
    when(payment1.getLastEdited()).thenReturn(new Date());
    payment2.setStatus("initialized");
    when(payment2.getLastEdited()).thenReturn(new Date());
    payment3.setStatus("initialized");
    when(payment3.getLastEdited()).thenReturn(new Date());
    payment4.setStatus("initialized");
    when(payment4.getLastEdited()).thenReturn(new Date());

    final List<Payment> payments = new ArrayList<>();
    payments.add(payment);
    payments.add(payment1);
    payments.add(payment2);
    payments.add(payment3);
    payments.add(payment4);


    PaymentCheckerService paymentCheckerService1 = new PaymentCheckerService(
      paymentRepository, mpay24Properties, shopEtp, mailer);

    PaymentCheckerService paymentCheckerService = spy(paymentCheckerService1);
    when(paymentRepository.findByReferenceCodeAndReferenceId(
      any(), any())).thenReturn(payments);

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.OK)).when(paymentCheckerService)
      .checkShopPayment(payment, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.SUSPENDED))
      .when(paymentCheckerService).checkShopPayment(payment1, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.NOT_FOUND))
      .when(paymentCheckerService).checkShopPayment(payment2, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.FAILED)).when(paymentCheckerService)
      .checkShopPayment(payment3, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.ERROR)).when(paymentCheckerService)
      .checkShopPayment(payment4, false);

    Assert.assertEquals(PaymentStatusEnum.OK,
      paymentCheckerService.checkShopPayments(1111L, false, new Date(0)).getPaymentStatus());

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.SUSPENDED))
      .when(paymentCheckerService).checkShopPayment(payment, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.SUSPENDED))
      .when(paymentCheckerService).checkShopPayment(payment1, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.NOT_FOUND))
      .when(paymentCheckerService).checkShopPayment(payment2, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.FAILED)).when(paymentCheckerService)
      .checkShopPayment(payment3, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.ERROR)).when(paymentCheckerService)
      .checkShopPayment(payment4, false);

    Assert.assertEquals(PaymentStatusEnum.SUSPENDED,
      paymentCheckerService.checkShopPayments(1111L, false, new Date(0)).getPaymentStatus());

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.NOT_FOUND))
      .when(paymentCheckerService).checkShopPayment(payment, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.NOT_FOUND))
      .when(paymentCheckerService).checkShopPayment(payment1, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.NOT_FOUND))
      .when(paymentCheckerService).checkShopPayment(payment2, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.FAILED)).when(paymentCheckerService)
      .checkShopPayment(payment3, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.ERROR)).when(paymentCheckerService)
      .checkShopPayment(payment4, false);

    Assert.assertEquals(PaymentStatusEnum.NOT_FOUND,
      paymentCheckerService.checkShopPayments(1111L, false, new Date(0)).getPaymentStatus());

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.FAILED)).when(paymentCheckerService)
      .checkShopPayment(payment, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.FAILED)).when(paymentCheckerService)
      .checkShopPayment(payment1, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.FAILED)).when(paymentCheckerService)
      .checkShopPayment(payment2, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.FAILED)).when(paymentCheckerService)
      .checkShopPayment(payment3, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.ERROR)).when(paymentCheckerService)
      .checkShopPayment(payment4, false);

    Assert.assertEquals(PaymentStatusEnum.FAILED,
      paymentCheckerService.checkShopPayments(1111L, false, new Date(0)).getPaymentStatus());

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.ERROR)).when(paymentCheckerService)
      .checkShopPayment(payment, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.ERROR)).when(paymentCheckerService)
      .checkShopPayment(payment1, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.ERROR)).when(paymentCheckerService)
      .checkShopPayment(payment2, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.ERROR)).when(paymentCheckerService)
      .checkShopPayment(payment3, false);
    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.ERROR)).when(paymentCheckerService)
      .checkShopPayment(payment4, false);

    Assert.assertEquals(PaymentStatusEnum.ERROR,
      paymentCheckerService.checkShopPayments(1111L, false, new Date(0)).getPaymentStatus());

  }

  @Test
  public void parameterToListTest() {
    Parameter parameter = new Parameter();
    Parameter parameter1 = new Parameter();
    parameter.setName("BRAND");
    parameter.setValue("VALUE");

    parameter1.setName("BRAND");
    parameter1.setValue("VALUE");

    List<Parameter> parameters = new ArrayList<>();
    parameters.add(parameter);
    parameters.add(parameter1);

    PaymentCheckerService paymentCheckerService1 = new PaymentCheckerService(
      paymentRepository,
      mpay24Properties,
      shopEtp,
      mailer);
  }

  @Test
  public void singlePaymentTest() {

    PaymentCheckerService paymentCheckerService1 = new PaymentCheckerService(
      paymentRepository, mpay24Properties, shopEtp, mailer);
    PaymentCheckerService paymentCheckerService = spy(paymentCheckerService1);
    when(payment.getTransactionId()).thenReturn("1111");
    when(payment.getCreated()).thenReturn(new Date(0));
    when(mpay24Properties.getShop()).thenReturn(shopAuth);
    when(shopAuth.getMerchantId()).thenReturn(222L);

    doReturn(new PaymentCheckerResponse("", PaymentStatusEnum.OK))
      .when(paymentCheckerService).checkMpayResponse(any(), any(), any());

    List<Parameter> parameters = new ArrayList<>();
    Mpay24Response mpay24Response = new Mpay24Response(Status.OK, "OK", parameters);
    doReturn(mpay24Response).when(paymentCheckerService).getMpay24TransactionStatus(any(), any());
    Assert.assertEquals(PaymentStatusEnum.OK,
      paymentCheckerService.checkShopPayment(payment, false).getPaymentStatus());
  }

  @Test
  public void actualMpay24CallTest() {

    PaymentCheckerService paymentCheckerService1 = new PaymentCheckerService(
      paymentRepository, mpay24Properties, shopEtp, mailer);
    PaymentCheckerService paymentCheckerService = spy(paymentCheckerService1);

    List<Parameter> parameters = new ArrayList<>();
    Mpay24Response mpay24Response = new Mpay24Response(Status.OK, "OK", parameters);

    doReturn(mpay24Response).when(paymentCheckerService).getMpay24TransactionStatus(any(), any());


  }

  @Test
  public void verifyPaymentFailedTest() {
    Status status = Status.OK;
    String returnCode = "OK";
    Map<MPay24ParameterEnum, String> parameters = new HashMap<>();
    parameters.put(MPay24ParameterEnum.STATUS, "FAILED");

    PaymentCheckerService paymentCheckerService = new PaymentCheckerService(
      paymentRepository, mpay24Properties, shopEtp, mailer);

    Mpay24Response response = paymentCheckerService.getMpay24TransactionStatus(0L, "wupp");

  }

  @Test
  public void verifyPaymentRequestFailedTest() {
    Status status = Status.ERROR;
    String returnCode = "ERROR";
    Map<MPay24ParameterEnum, String> parameters = new HashMap<>();
    parameters.put(MPay24ParameterEnum.STATUS, "FAILED");

    PaymentCheckerService paymentCheckerService = new PaymentCheckerService(
      null, null, null, null);

    PaymentCheckerResponse response = paymentCheckerService.checkMpayResponse(
      status, returnCode, parameters);

    Assert.assertEquals(PaymentStatusEnum.ERROR, response.getPaymentStatus());

  }

  @Test
  public void verifyPaymentErrorTest() {
    Status status = Status.OK;
    String returnCode = "OK";
    Map<MPay24ParameterEnum, String> parameters = new HashMap<>();
    parameters.put(MPay24ParameterEnum.STATUS, "ERROR");

    PaymentCheckerService paymentCheckerService = new PaymentCheckerService(
      null, null, null, null);

    PaymentCheckerResponse response = paymentCheckerService
      .checkMpayResponse(status, returnCode, parameters);

    Assert.assertEquals(PaymentStatusEnum.FAILED, response.getPaymentStatus());

  }

  @Test
  public void paymentNotFoundTest() {
    Status status = Status.ERROR;
    String returnCode = "NOT_FOUND";
    Map<MPay24ParameterEnum, String> parameters = new HashMap<>();
    parameters.put(MPay24ParameterEnum.STATUS, "SUSPENDED");

    PaymentCheckerService paymentCheckerService = new PaymentCheckerService(
      null, null, null, null);

    PaymentCheckerResponse response = paymentCheckerService.checkMpayResponse(
      status, returnCode, parameters);

    Assert.assertEquals(PaymentStatusEnum.NOT_FOUND, response.getPaymentStatus());

  }

  @Test
  public void checkReturnCodes() {
    Status status = Status.ERROR;
    Map<MPay24ParameterEnum, String> parameters = new HashMap<>();
    parameters.put(MPay24ParameterEnum.STATUS, "SUSPENDED");

    PaymentCheckerService paymentCheckerService = new PaymentCheckerService(
      null, null, null, null);

    PaymentCheckerResponse response = paymentCheckerService
      .checkMpayResponse(status, "NOT_FOUND", parameters);
    Assert.assertEquals(PaymentStatusEnum.NOT_FOUND, response.getPaymentStatus());

    response = paymentCheckerService.checkMpayResponse(status, "NOT_FOUND", parameters);
    Assert.assertEquals(PaymentStatusEnum.NOT_FOUND, response.getPaymentStatus());
    response = paymentCheckerService.checkMpayResponse(status, "DECLINED", parameters);
    Assert.assertEquals(PaymentStatusEnum.ERROR, response.getPaymentStatus());
    response = paymentCheckerService.checkMpayResponse(status, "BLOCKED", parameters);
    Assert.assertEquals(PaymentStatusEnum.ERROR, response.getPaymentStatus());
    response = paymentCheckerService
      .checkMpayResponse(status, "ACCESS_DENIED", parameters);
    Assert.assertEquals(PaymentStatusEnum.ERROR, response.getPaymentStatus());
    response = paymentCheckerService
      .checkMpayResponse(status, "MERCHANT_LOCKED", parameters);
    Assert.assertEquals(PaymentStatusEnum.ERROR, response.getPaymentStatus());
    response = paymentCheckerService
      .checkMpayResponse(status, "PAYMENT_METHOD_LOCKED", parameters);
    Assert.assertEquals(PaymentStatusEnum.ERROR, response.getPaymentStatus());

  }

  @Test
  public void verifyPaymentSuspendedTest() {
    Status status = Status.OK;
    String returnCode = "OK";
    Map<MPay24ParameterEnum, String> parameters = new HashMap<>();
    parameters.put(MPay24ParameterEnum.STATUS, "SUSPENDED");

    PaymentCheckerService paymentCheckerService = new PaymentCheckerService(
      null, null, null, null);

    PaymentCheckerResponse response = paymentCheckerService
      .checkMpayResponse(status, returnCode, parameters);

    Assert.assertEquals(PaymentStatusEnum.SUSPENDED, response.getPaymentStatus());

  }

  @Test
  public void verifyPaymentBilledTest() {
    Status status = Status.OK;
    String returnCode = "OK";
    Map<MPay24ParameterEnum, String> parameters = new HashMap<>();
    parameters.put(MPay24ParameterEnum.STATUS, "BILLED");

    PaymentCheckerService paymentCheckerService = new PaymentCheckerService(
      null, null, null, null);

    PaymentCheckerResponse response = paymentCheckerService
      .checkMpayResponse(status, returnCode, parameters);

    Assert.assertEquals(PaymentStatusEnum.OK, response.getPaymentStatus());

  }

  @Test
  public void verifyPaymentReturnCodeErrorTest() {
    Status status = Status.OK;
    String returnCode = "argl";
    Map<MPay24ParameterEnum, String> parameters = new HashMap<>();
    parameters.put(MPay24ParameterEnum.STATUS, "FAILED");

    PaymentCheckerService paymentCheckerService = new PaymentCheckerService(
      null, null, null, null);

    PaymentCheckerResponse response = paymentCheckerService
      .checkMpayResponse(status, returnCode, parameters);
    Assert.assertEquals(PaymentStatusEnum.ERROR, response.getPaymentStatus());

  }
}
