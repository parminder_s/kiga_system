package com.kiga.crm.messages;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.crm.messages.response.PaymentOption;
import com.kiga.crm.service.ObjectMapperFactory;
import org.junit.Test;

import java.io.IOException;


/**
 * Created by mfit on 05.10.17.
 */
public class PaymentOptionTest {
  ObjectMapper objectMapper;

  /**
   * Set up object mapper.
   */
  public PaymentOptionTest() {
    objectMapper = ObjectMapperFactory.getObjectMapper();
  }

  @Test
  public void deserializePaymentOption() throws IOException {
    String paymentOptionSerialized = "{\"payment_id\":\"11\","
      + "\"payment_code\":\"MP-CC\","
      + "\"payment_title\":\"Mpay Credit Card\","
      + "\"uses_checkout\":1,"
      + "\"payment_psp\":\"mpay\","
      + "\"payment_acquirer\":\"cardcomplete\"}";
    PaymentOption paymentOption = objectMapper.readValue(paymentOptionSerialized,
      PaymentOption.class);

    assertEquals("MP-CC", paymentOption.getPaymentCode());
    assertEquals((Integer) 1, paymentOption.getUsesCheckout());
    assertEquals((Integer) 11, paymentOption.getPaymentId());
  }
}
