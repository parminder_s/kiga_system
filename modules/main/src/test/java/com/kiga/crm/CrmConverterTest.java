package com.kiga.crm;

import static org.junit.Assert.assertEquals;

import com.kiga.crm.messages.requests.ChangeSubscriptionContainerRequest;
import com.kiga.crm.messages.response.ChangeSubscriptionResp;
import com.kiga.crm.service.AuthenticationException;
import com.kiga.crm.service.CrmConverter;
import com.kiga.crm.service.HttpClientFactory;
import com.kiga.crm.service.ObjectMapperFactory;
import com.kiga.web.security.session.Session;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class CrmConverterTest {
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void testAuthErrorResponse() throws Exception {
    HttpClientFactory httpClientFactory = preparedRequestFactory(
      "{\"error\":\"Session invalid or expired\",\"code\":902}",
      401);

    CrmConverter crmConverter = new CrmConverter(
      "/",
      Mockito.mock(Session.class),
      ObjectMapperFactory.getObjectMapper(),
      httpClientFactory);

    thrown.expect(AuthenticationException.class);
    thrown.expectMessage("Session Expired");

    crmConverter.customerGetData(0);
  }

  @Test
  public void testResponseParseError() throws Exception {
    HttpClientFactory httpClientFactory = preparedRequestFactory(
      "this is not \"js\"on", 200);

    CrmConverter crmConverter = new CrmConverter(
      "/",
      Mockito.mock(Session.class),
      ObjectMapperFactory.getObjectMapper(),
      httpClientFactory
    );

    thrown.expect(Exception.class);
    thrown.expectMessage("Crm response parse error");

    crmConverter.customerGetData(0);
  }

  @Test
  public void testResponseWithTransactionAndCheckoutCanBeParsedCorrectly() throws Exception {
    String rawResponse =
      "{\"transaction_id\": \"ad7f087b9e865d861e0bbbc0ef84ea809451df48\",\n"
      + "\"transaction_status\": \"start\",\n"
      + "\"checkout\": {\n"
      + "  \"payment_option\": \"4\",\n"
      + "  \"payment_option_code\": \"TEST\",\n"
      + "  \"payment_data\": {\n"
      + "    \"token\": \"46d7be9108badbf52c887d353b90e496\"\n"
      + "  },\n"
      + "  \"payment_acquirer\": \"\",\n"
      + "  \"payment_psp\": \"\",\n"
      + "  \"cart\": {\n"
      + "    \"description\": \"name2 12 Monate Abo, Abbuchungsauftrag: EUR 44,00\",\n"
      + "    \"currencyCode\": \"EUR\",\n"
      + "    \"currentAmount\": 44,\n"
      + "    \"currentTax\": 0,\n"
      + "    \"currentDescription\": \"\",\n"
      + "    \"recurringStartDate\": \"2019-04-18\",\n"
      + "    \"recurringCycle\": \"Month\",\n"
      + "    \"recurringFrequency\": \"12\",\n"
      + "    \"recurringAmount\": 36.68,\n"
      + "    \"recurringTax\": 7.32,\n"
      + "    \"recurringRepeats\": 0,\n"
      + "    \"recurringDescription\": \"12 Monate Abo\",\n"
      + "    \"hasTrialPeriod\": 0,\n"
      + "    \"trialCycle\": \"Month\",\n"
      + "    \"trialFrequency\": 0,\n"
      + "    \"trialAmount\": 0,\n"
      + "    \"trialTax\": 0,\n"
      + "    \"trialRepeats\": 0,\n"
      + "    \"newProduct\": false,\n"
      + "    \"checkoutAmount\": 44,\n"
      + "    \"checkoutTax\": 0,\n"
      + "    \"checkoutAmountGross\": 44\n"
      + "  }\n"
      + "},\n"
      + "\"needs_checkout\": 1}";

    HttpClientFactory httpClientFactory = preparedRequestFactory(rawResponse, 200);

    CrmConverter crmConverter = new CrmConverter(
      "/",
      Mockito.mock(Session.class),
      ObjectMapperFactory.getObjectMapper(),
      httpClientFactory
    );

    ChangeSubscriptionContainerRequest emptyRequest
      = new ChangeSubscriptionContainerRequest();
    ChangeSubscriptionResp response =
      crmConverter.transactionalChangeSubscription(emptyRequest);

    assertEquals(1, (long) response.getNeedsCheckout());
    assertEquals("ad7f087b9e865d861e0bbbc0ef84ea809451df48", response.getTransactionId());
    assertEquals(BigDecimal.valueOf(44), response.getCheckout().getCart().getCheckoutAmount());
    assertEquals(36.68, response.getCheckout().getCart().getRecurringAmount(), 0.001);

  }

  /**
   * Set up  a HttpClientFactory that will produce the provided response.
   */
  private HttpClientFactory preparedRequestFactory(String jsonResponse, Integer responseCode)
    throws Exception {

    StatusLine statusLine = Mockito.mock(StatusLine.class);
    Mockito.when(statusLine.getStatusCode()).thenReturn(responseCode);

    CloseableHttpResponse httpResponse = Mockito.mock(CloseableHttpResponse.class);
    Mockito.when(httpResponse.getStatusLine()).thenReturn(statusLine);
    Mockito.when(httpResponse.getEntity()).thenReturn(new StringEntity(jsonResponse));

    CloseableHttpClient client = Mockito.mock(CloseableHttpClient.class);
    Mockito.when(client.execute(Mockito.any())).thenReturn(httpResponse);

    HttpClientFactory httpClientFactory = Mockito.mock(HttpClientFactory.class);
    Mockito.when(httpClientFactory.getDefaultClient()).thenReturn(client);

    return httpClientFactory;
  }
}
