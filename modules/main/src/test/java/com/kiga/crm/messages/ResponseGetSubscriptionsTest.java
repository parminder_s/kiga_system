package com.kiga.crm.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.crm.messages.response.ResponseGetSubscriptions;
import com.kiga.crm.service.ObjectMapperFactory;
import com.kiga.util.ResourceLoader;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/** Created by mfit on 05.10.17. */
public class ResponseGetSubscriptionsTest {
  ObjectMapper objectMapper;

  /**
   * Set up object mapper. Sets up ObjectMapper properly as required, @see com.kiga.crm.CrmConfig
   */
  public ResponseGetSubscriptionsTest() {
    objectMapper = ObjectMapperFactory.getObjectMapper();
  }

  @Test
  public void deserializeGetSubscripitonsResponse() throws IOException {
    String responseContent =
        IOUtils.toString(ResourceLoader.getResource("com.kiga.crm/getSubscriptions.json"));

    ResponseGetSubscriptions responseGetSubscriptions =
        objectMapper.readValue(responseContent, ResponseGetSubscriptions.class);

    assertTrue("At least two products exist", responseGetSubscriptions.getProducts().size() > 1);
    assertTrue(
        "At least two invoice options exist",
        responseGetSubscriptions.getInvoiceOptions().getInvoiceOptions().size() > 1);

    assertEquals(8, responseGetSubscriptions.getProductGroups().size());
    assertEquals("EUR", responseGetSubscriptions.getCurrency());
    assertEquals(20, (double) responseGetSubscriptions.getVat(), 0.1);
    assertEquals(13, (long) responseGetSubscriptions.getAgeLimit());
  }

  @Test
  public void testProducts() throws IOException {
    String responseContent =
        IOUtils.toString((ResourceLoader.getResource("com.kiga.crm/getSubscriptions.json")));
    ResponseGetSubscriptions responseGetSubscriptions =
        objectMapper.readValue(responseContent, ResponseGetSubscriptions.class);

    assertEquals("19 products expected", 19, responseGetSubscriptions.getProducts().size());

    assertEquals("GIFT", responseGetSubscriptions.getProducts().get(0).getProductGroupId());

    assertEquals("NA", responseGetSubscriptions.getProducts().get(2).getProductGroupId());

    assertEquals(20.9, (double) responseGetSubscriptions.getProducts().get(3).getPrice(), 0.01);
  }

  @Test
  public void testCountryPaymentOptions() throws IOException {
    String responseContent =
        IOUtils.toString((ResourceLoader.getResource("com.kiga.crm/getSubscriptions.json")));

    ResponseGetSubscriptions responseGetSubscriptions =
        objectMapper.readValue(responseContent, ResponseGetSubscriptions.class);

    assertTrue(responseGetSubscriptions.getPaymentOptions().size() > 1);
    assertEquals(
        Arrays.asList("RE", "PAYPAL", "MP-ELV", "MP-CC"),
        responseGetSubscriptions
            .getPaymentOptions()
            .stream()
            .map(opt -> opt.getPaymentCode())
            .collect(Collectors.toList()));
  }

  @Test
  public void testProductPaymentOptions() throws IOException {
    String responseContent =
        IOUtils.toString(ResourceLoader.getResource("com.kiga.crm/getSubscriptions.json"));

    ResponseGetSubscriptions responseGetSubscriptions =
        objectMapper.readValue(responseContent, ResponseGetSubscriptions.class);
  }
}
