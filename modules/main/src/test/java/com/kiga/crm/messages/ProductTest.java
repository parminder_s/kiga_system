package com.kiga.crm.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.crm.messages.response.InvoiceOptionsResponse;
import com.kiga.crm.messages.response.Product;
import com.kiga.crm.service.ObjectMapperFactory;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.payment.PaymentType;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by mfit on 12.10.17.
 */
public class ProductTest {
  ObjectMapper objectMapper;
  String serialized;

  /**
   * Set up object mapper.
   * Sets up ObjectMapper properly as required, @see com.kiga.crm.CrmConfig
   */
  public ProductTest() {
    objectMapper = ObjectMapperFactory.getObjectMapper();

    serialized = "{"
      + "\"product_id\": \"24\","
      + "\"product_group_id\": \"PAR\","
      + "\"customer_type\": \"standard\","
      + "\"duration\": 12,"
      + "\"price\": 69.9,"
      + "\"orig_price\": 69.9,"
      + "\"licence_price\": 0,"
      + "\"max_groups\": 0,"
      + "\"payment_options\": {"
      + "    \"1\": \"RE\","
      + "    \"3\": \"PAYPAL\","
      + "    \"7\": \"MP-ELV\","
      + "    \"11\": \"MP-CC\""
      + "}}";
  }

  @Test
  public void testProductDeserialization() throws IOException {
    Product product = objectMapper.readValue(serialized, Product.class);

    assertEquals("PAR", product.getProductGroupId());
    assertEquals("standard", product.getCustomerType());
  }

  @Test
  public void testProductAvailablePaymentCodesDeserialization() throws IOException {
    Product product = objectMapper.readValue(serialized, Product.class);

    // assertEquals(4, product.getPaymentOptions().size());
    assertEquals(Arrays.asList(PaymentType.RE, PaymentType.PAYPAL,
      PaymentType.MP_ELV, PaymentType.MP_CC), product.getPaymentOptions());
  }

  @Test
  public void testDoesNotThrowOnUnknownPaymentMethodCodes() throws IOException {
    String serialized = "{"
      + "\"product_id\": \"24\","
      + "\"product_group_id\": \"PAR\","
      + "\"customer_type\": \"standard\","
      + "\"duration\": 12,"
      + "\"price\": 69.9,"
      + "\"orig_price\": 69.9,"
      + "\"licence_price\": 0,"
      + "\"max_groups\": 0,"
      + "\"payment_options\": {"
      + "    \"1\": \"RE\","
      + "    \"3\": \"PAYPAL\","
      + "    \"7\": \"MP-ELV\","
      + "    \"8\": \"SOME-NEW-OPTION\","
      + "    \"11\": \"MP-CC\""
      + "}}";


    Product product = objectMapper.readValue(serialized, Product.class);

    // assertEquals(4, product.getPaymentOptions().size());
    assertEquals(Arrays.asList(PaymentType.RE, PaymentType.PAYPAL,
      PaymentType.MP_ELV, PaymentType.MP_CC), product.getPaymentOptions());
  }
}
