package com.kiga.db;

import static org.mockito.Mockito.mock;

import com.kiga.main.locale.Locale;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 08.11.16.
 */
public class LocaleToLongNotationConverterTest {
  private LocaleToLongNotationConverter converter = new LocaleToLongNotationConverter();

  @Test
  public void fromEnum() {
    Assert.assertEquals("de_DE", converter.convertToDatabaseColumn(Locale.de));
  }

  @Test
  public void fromString() {
    Assert.assertEquals(Locale.de, converter.convertToEntityAttribute("de_DE"));
  }
}
