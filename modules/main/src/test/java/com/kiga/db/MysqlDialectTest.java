package com.kiga.db;

import org.hibernate.dialect.function.SQLFunction;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * @author bbs
 * @since 7/21/16.
 */
public class MysqlDialectTest {
  @Test
  public void testCustomFunctionsRegistered() {
    MySqlDialect dialect = new MySqlDialect();
    Map<String, SQLFunction> functions = dialect.getFunctions();
    Assert.assertNotNull(functions.get("in_group"));
  }
}
