package com.kiga.db;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;

import com.kiga.main.locale.Locale;
import org.junit.Test;

/**
 * Created by rainerh on 08.11.16.
 */
public class LocaleToShortNotationConverterTest {
  private LocaleToShortNotationConverter converter = new LocaleToShortNotationConverter();

  @Test
  public void fromEnum() {
    assertEquals("de", converter.convertToDatabaseColumn(Locale.de));
  }

  @Test
  public void fromString() {
    assertEquals(Locale.de, converter.convertToEntityAttribute("de"));
  }

  @Test
  public void nullTest() {
    assertNull(converter.convertToDatabaseColumn(null));
    assertNull(converter.convertToEntityAttribute(null));

  }
}
