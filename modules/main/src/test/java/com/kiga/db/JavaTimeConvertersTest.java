package com.kiga.db;

import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by rainerh on 04.04.16.
 */
public class JavaTimeConvertersTest {
  @Test
  public void instantTest() {
    Instant instant = Instant.parse("2015-01-01T00:00:00Z");
    InstantConverter converter = new InstantConverter();
    Instant calcedInstant = converter.convertToEntityAttribute(
      converter.convertToDatabaseColumn(instant)
    );

    Assert.assertEquals(instant, calcedInstant);
    Assert.assertNull(converter.convertToDatabaseColumn(null));
    Assert.assertNull(converter.convertToEntityAttribute(null));
  }

  @Test
  public void localDateTest() {
    LocalDate localDate = LocalDate.parse("2015-01-01");
    LocalDateConverter converter = new LocalDateConverter();
    LocalDate calcedLocalDate = converter.convertToEntityAttribute(
      converter.convertToDatabaseColumn(localDate)
    );

    Assert.assertEquals(localDate, calcedLocalDate);
    Assert.assertNull(converter.convertToDatabaseColumn(null));
    Assert.assertNull(converter.convertToEntityAttribute(null));
  }

  @Test
  public void localDateTimeTest() {
    LocalDateTime localDateTime = LocalDateTime.parse("2015-01-01T12:00:00");
    LocalDateTimeConverter converter = new LocalDateTimeConverter();
    LocalDateTime calcedLocalDateTime = converter.convertToEntityAttribute(
      converter.convertToDatabaseColumn(localDateTime)
    );

    Assert.assertEquals(localDateTime, calcedLocalDateTime);
    Assert.assertNull(converter.convertToDatabaseColumn(null));
    Assert.assertNull(converter.convertToEntityAttribute(null));
  }
}
