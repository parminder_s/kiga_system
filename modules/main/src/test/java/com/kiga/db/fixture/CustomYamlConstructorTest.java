package com.kiga.db.fixture;

import com.kiga.main.locale.Locale;
import com.kiga.util.InstantCreator;
import java.time.Instant;
import org.junit.Assert;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

/** Created by rainerh on 09.11.16. */
public class CustomYamlConstructorTest {
  @Test
  public void testLocaleConstructor() {
    Assert.assertEquals(
        Locale.de, new Yaml(new CustomYamlConstructor()).loadAs("de", Locale.class));

    Assert.assertEquals("de", new Yaml(new CustomYamlConstructor()).loadAs("de", String.class));
  }

  @Test
  public void testInstant() {
    Assert.assertEquals(
        InstantCreator.create(2019, 1, 1),
        new Yaml(new CustomYamlConstructor()).loadAs("2019-01-01", Instant.class));
  }
}
