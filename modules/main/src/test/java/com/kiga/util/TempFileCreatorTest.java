package com.kiga.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import org.junit.Test;

public class TempFileCreatorTest {
  @Test
  public void testCreateFilename() throws Exception {
    File file = new TempFileCreator().createFile("foo.bar");

    assertThat(file).hasName("foo.bar");
  }

  @Test
  public void testCreate() throws Exception {
    File file = new TempFileCreator().createFile();

    assertThat(file).doesNotExist();
  }
}
