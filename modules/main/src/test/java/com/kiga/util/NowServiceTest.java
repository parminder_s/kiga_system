package com.kiga.util;

import static com.kiga.util.NowService.KIGAZONE;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;
import org.junit.Test;

public class NowServiceTest {
  @Test
  public void testInstant() {
    Instant past = Instant.now();
    Instant now = new NowService().getInstant();
    Instant future = Instant.now();
    assertThat(now).isAfterOrEqualTo(past);
    assertThat(now).isBeforeOrEqualTo(future);
  }

  @Test
  public void testDate() {
    Date past = new Date();
    Date now = new NowService().getDate();
    Date future = new Date();
    assertThat(now).isAfterOrEqualsTo(past);
    assertThat(now).isBeforeOrEqualsTo(future);
  }

  @Test
  public void testZoned() {
    ZonedDateTime past = ZonedDateTime.now(KIGAZONE);
    ZonedDateTime now = new NowService().getZoned();
    ZonedDateTime future = ZonedDateTime.now(KIGAZONE);
    assertThat(now).isAfterOrEqualTo(past);
    assertThat(now).isBeforeOrEqualTo(future);
  }
}
