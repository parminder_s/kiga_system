package com.kiga.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

/**
 * @author bbs
 * @since 1/22/17.
 */
public class AnnotatedClassFinderTest {
  @Test
  public void testLookup() {
    AnnotatedClassFinder annotatedClassFinder = new AnnotatedClassFinder();
    List<Class<?>> classes = annotatedClassFinder
      .lookup(DummyAnnotationForTests.class, AnnotatedTestClass.class);
    assertTrue(classes.size() == 1);
    assertEquals(AnnotatedTestClass.class, classes.get(0));
  }

  @Test
  public void testLookupNotReturningNullIfNoClassesFound() {
    AnnotatedClassFinder annotatedClassFinder = new AnnotatedClassFinder();
    List<Class<?>> classes = annotatedClassFinder
      .lookup(DummyAnnotationForTests.class, NotAnnotatedTestClass.class);
    assertNotNull(classes);
    assertTrue(classes.size() == 0);
  }

  @Retention(RetentionPolicy.RUNTIME)
  public @interface DummyAnnotationForTests {
  }

  @DummyAnnotationForTests
  private class AnnotatedTestClass {
  }

  private class NotAnnotatedTestClass {
  }
}
