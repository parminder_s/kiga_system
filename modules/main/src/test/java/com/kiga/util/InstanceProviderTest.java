package com.kiga.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.context.ApplicationContext;

/**
 * @author bbs
 * @since 3/5/17.
 */
public class InstanceProviderTest {
  private ApplicationContext appContextMock = mock(ApplicationContext.class);
  private InstanceProvider instanceProvider;

  @Before
  public void beforeTests() {
    instanceProvider = new InstanceProvider(appContextMock);
  }

  @Test
  public void testGetInstanceFromBean() {
    TestingClass testingClass = new TestingClass();
    when(appContextMock.getBean(TestingClass.class)).thenReturn(testingClass);
    TestingClass instance = instanceProvider.getInstance(TestingClass.class);
    assertSame(testingClass, instance);
  }

  @Test
  public void testGetInstanceByByBeanThrowingBeansException() {
    when(appContextMock.getBean(TestingClass.class)).thenThrow(mock(BeansException.class));
    TestingClass instance1 = instanceProvider.getInstance(TestingClass.class);
    TestingClass instance2 = instanceProvider.getInstance(TestingClass.class);
    assertNotNull(instance1);
    assertNotNull(instance2);
    assertNotSame(instance1, instance2);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testGetInstanceByByBeanThrowingNoUniqueBeanDefinitionException() {
    when(appContextMock.getBean(TestingClass.class))
      .thenThrow(NoUniqueBeanDefinitionException.class);
    TestingClass instance1 = instanceProvider.getInstance(TestingClass.class);
    TestingClass instance2 = instanceProvider.getInstance(TestingClass.class);
    assertNotNull(instance1);
    assertNotNull(instance2);
    assertNotSame(instance1, instance2);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testGetInstanceByByBeanThrowingNoSuchBeanDefinitionException() {
    when(appContextMock.getBean(TestingClass.class)).thenThrow(NoSuchBeanDefinitionException.class);
    TestingClass instance1 = instanceProvider.getInstance(TestingClass.class);
    TestingClass instance2 = instanceProvider.getInstance(TestingClass.class);
    assertNotNull(instance1);
    assertNotNull(instance2);
    assertNotSame(instance1, instance2);
  }

  @Test
  public void testGetinstanceWhenCreatingNewInstanceThrowsExceptions() {
    AbstractTestingClass instance = instanceProvider.getInstance(AbstractTestingClass.class);
    assertNull(instance);
  }

  public abstract static class AbstractTestingClass {

  }

  public static class TestingClass {
  }
}
