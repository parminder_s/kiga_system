package com.kiga.util;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author bbs
 * @since 10/25/16.
 */
public class ResourceLoaderTest {
  @Test
  public void testGetResource() throws IOException {
    InputStream inputStream = ResourceLoader.getResource("com.kiga.upload/test.svg");
    assertNotNull(inputStream);
    inputStream.close();
  }
}
