package com.kiga.util;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 4/10/17.
 */
public class BrowserPusherServiceTest {

  @Test
  public void testPushFile() throws IOException {
    InputStream inputStreamMock = mock(InputStream.class);
    ServletOutputStream outputStreamMock = mock(ServletOutputStream.class);
    HttpServletResponse responseMock = mock(HttpServletResponse.class);
    when(responseMock.getOutputStream()).thenReturn(outputStreamMock);
    String fileName = "mock.jpg";
    String contentType = "image/jpeg";

    doCallRealMethod().when(outputStreamMock)
      .write(any(byte[].class), any(int.class), any(int.class));
    when(inputStreamMock.read(any())).thenAnswer(new Answer<Integer>() {
      @Override
      public Integer answer(InvocationOnMock invocation) throws Throwable {
        byte[] byteArray = (byte[]) invocation.getArguments()[0];
        byteArray[0] = 123;
        return 1;
      }
    }).thenAnswer(new Answer<Integer>() {
      @Override
      public Integer answer(InvocationOnMock invocation) throws Throwable {
        return -1;
      }
    });

    new BrowserPusherService().pushFile(responseMock, inputStreamMock, fileName, contentType);

    verify(responseMock).setContentType(contentType);
    verify(responseMock)
      .setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
    verify(responseMock).flushBuffer();
    verify(outputStreamMock).write(123);
  }

  @Test(expected = IOException.class)
  public void testPushFileWithIoExceptionThrow() throws IOException {
    InputStream inputStreamMock = mock(InputStream.class);
    ServletOutputStream outputStreamMock = mock(ServletOutputStream.class);
    HttpServletResponse responseMock = mock(HttpServletResponse.class);
    when(responseMock.getOutputStream()).thenReturn(outputStreamMock);
    String fileName = "mock.jpg";
    String contentType = "image/jpeg";

    doThrow(IOException.class).when(outputStreamMock)
      .write(any(byte[].class), any(int.class), any(int.class));

    new BrowserPusherService().pushFile(responseMock, inputStreamMock, fileName, contentType);
  }
}
