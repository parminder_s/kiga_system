package com.kiga.print;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import com.kiga.print.validation.ScheduledValidation;
import com.kiga.test.FullBeanTest;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.junit.Test;

/**
 * Created by rainerh on 22.03.17.
 */
public class PrintConfigTest {
  @Test
  public void beanTest() {
    FullBeanTest.test(PrintConfig.class);
  }

  @Test
  public void scheduleTest() {
    PrintConfig config = new PrintConfig();

    config.setEnableValidation(true);
    config.setCronExpression("* * * * * *");
    assertThat(config.scheduleValidation(), instanceOf(ScheduledValidation.class));

    config.setEnableValidation(false);
    assertNull(config.scheduleValidation());
  }

  @Test
  public void testSupplier() {
    assertThat(new PrintConfig().pdfMergerUtilityProducer().get(),
      instanceOf(PDFMergerUtility.class));
  }
}
