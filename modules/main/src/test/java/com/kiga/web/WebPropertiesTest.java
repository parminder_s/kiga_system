package com.kiga.web;

import com.google.code.beanmatchers.BeanMatchers;
import com.kiga.web.props.WebProperties;
import org.junit.Assert;
import org.junit.Test;

/** Created by rainerh on 04.10.16. */
public class WebPropertiesTest {
  @Test
  public void beanTest() {
    Assert.assertThat(WebProperties.class, BeanMatchers.hasValidGettersAndSetters());
  }
}
