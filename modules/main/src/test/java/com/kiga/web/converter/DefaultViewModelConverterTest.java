package com.kiga.web.converter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author bbs
 * @since 10/16/16.
 */
public class DefaultViewModelConverterTest {
  private TestConverter testConverter = new TestConverter();

  @Test
  public void testConvertList() {
    List<Entity> list = Arrays.asList(new Entity("A"), new Entity("B"), new Entity("C"));
    List<Response> countryViewModels = testConverter.convertToResponse(list);
    assertEquals(3, countryViewModels.size());
    assertEquals("A", countryViewModels.get(0).dummyValue);
    assertEquals("B", countryViewModels.get(1).dummyValue);
    assertEquals("C", countryViewModels.get(2).dummyValue);
  }

  @Test
  public void testConvertIterable() {
    Iterable<Entity> iterable = Arrays.asList(new Entity("A"), new Entity("B"), new Entity("C"));
    List<Response> countryViewModels = testConverter.convertToResponse(iterable);
    assertEquals(3, countryViewModels.size());
    assertEquals("A", countryViewModels.get(0).dummyValue);
    assertEquals("B", countryViewModels.get(1).dummyValue);
    assertEquals("C", countryViewModels.get(2).dummyValue);
  }

  private class Entity {
    String dummyValue;

    Entity(String dummyValue) {
      this.dummyValue = dummyValue;
    }
  }

  private class Response {
    String dummyValue;
  }

  private class TestConverter extends DefaultViewModelConverter<Entity, Response> {
    @Override
    public Response convertToResponse(Entity entity) {
      Response response = new Response();
      response.dummyValue = entity.dummyValue;
      return response;
    }
  }
}
