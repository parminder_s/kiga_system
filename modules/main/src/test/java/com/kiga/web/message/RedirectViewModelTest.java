package com.kiga.web.message;

import com.google.code.beanmatchers.BeanMatchers;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 13.10.16.
 */
public class RedirectViewModelTest {
  @Test
  public void beanTest() {
    Assert.assertEquals("foobar", new RedirectResponse("foobar").getRedirect());
  }
}
