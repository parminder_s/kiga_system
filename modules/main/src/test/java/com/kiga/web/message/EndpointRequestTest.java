package com.kiga.web.message;

import static org.junit.Assert.assertEquals;

import com.kiga.main.locale.Locale;
import org.junit.Test;

/**
 * Created by rainerh on 04.03.17.
 */
public class EndpointRequestTest {
  @Test
  public void testConstructor() {
    EndpointRequest endpointRequest = new EndpointRequest(Locale.en);
    assertEquals(Locale.en, endpointRequest.getLocale());
  }
}
