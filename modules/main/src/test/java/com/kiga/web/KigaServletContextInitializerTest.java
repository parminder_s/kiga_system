package com.kiga.web;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.web.props.WebProperties;
import com.kiga.web.props.WebSecurityProperties;
import com.kiga.web.web.ResettableInputStreamFilter;
import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.SessionCookieConfig;
import org.junit.Test;
import org.springframework.boot.context.embedded.ServletContextInitializer;

/** Created by rainerh on 13.10.16. */
public class KigaServletContextInitializerTest {
  @Test
  public void onStartup() throws Exception {
    WebProperties webProperties = new WebProperties();
    webProperties.setWebSecurityProperties(new WebSecurityProperties());
    ServletContextInitializer initializer = new KigaServletContextInitializer(webProperties);
    ServletContext context = mock(ServletContext.class);
    when(context.addFilter(anyString(), any(Filter.class)))
        .thenReturn(mock(FilterRegistration.Dynamic.class));
    when(context.getSessionCookieConfig()).thenReturn(mock(SessionCookieConfig.class));
    initializer.onStartup(context);
    verify(context)
        .addFilter(eq("resettableInputStreamFilter"), any(ResettableInputStreamFilter.class));
    verify(context).getSessionCookieConfig();
  }
}
