package com.kiga.web.service;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CountryResponse;
import com.maxmind.geoip2.record.Country;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetAddress;

/**
 * @author bbs
 * @since 9/29/16.
 */
public class GeoIp2DatabaseResolverTest {
  private static final String MOCK_IP_EXAMPLE_COM = "93.184.216.34";
  private DatabaseReader databaseReader = mock(DatabaseReader.class);
  private GeoIp2DatabaseResolver geoIp2DatabaseResolver;

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    try {
      geoIp2DatabaseResolver = new GeoIp2DatabaseResolver(databaseReader);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
  }

  @Test
  public void testIfDatabaseReaderCalled() throws IOException, GeoIp2Exception {
    Country country = mock(Country.class);
    when(country.getIsoCode()).thenReturn("");
    CountryResponse countryResponse = new CountryResponse(null, country, null, null, null, null);
    when(databaseReader.country(any())).thenReturn(countryResponse);
    geoIp2DatabaseResolver.getCountryIsoCode(MOCK_IP_EXAMPLE_COM);
    InetAddress ipAddress = InetAddress.getByName(MOCK_IP_EXAMPLE_COM);
    verify(databaseReader).country(ipAddress);
  }

  @Test
  public void testNotMatchedIpFallsbackToDefaultDe() {
    String countryIsoCode = geoIp2DatabaseResolver.getCountryIsoCode("0.0.0.0");
    assertNull(countryIsoCode);
  }

  @Test
  public void testProperExceptionHandling() throws IOException {
    GeoIp2DatabaseResolverThrowingIoException geoIp2DatabaseResolverThrowingIoException = new
      GeoIp2DatabaseResolverThrowingIoException(databaseReader);
    geoIp2DatabaseResolverThrowingIoException.getCountryIsoCode(null);
    GeoIp2DatabaseResolverThrowingGeoIp2Exception geoIp2DatabaseResolverThrowingGeoIp2Exception =
      new GeoIp2DatabaseResolverThrowingGeoIp2Exception(databaseReader);
    geoIp2DatabaseResolverThrowingGeoIp2Exception.getCountryIsoCode(null);
  }

  private class GeoIp2DatabaseResolverThrowingIoException extends GeoIp2DatabaseResolver {
    GeoIp2DatabaseResolverThrowingIoException(DatabaseReader databaseReader) throws IOException {
      super(databaseReader);
    }
  }

  private class GeoIp2DatabaseResolverThrowingGeoIp2Exception extends GeoIp2DatabaseResolver {
    GeoIp2DatabaseResolverThrowingGeoIp2Exception(DatabaseReader databaseReader) throws
      IOException {
      super(databaseReader);
    }
  }
}
