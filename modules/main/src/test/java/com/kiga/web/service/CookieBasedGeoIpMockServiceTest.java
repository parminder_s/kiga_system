package com.kiga.web.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.main.service.CookieService;
import org.junit.Test;

/**
 * @author bbs
 * @since 9/29/16.
 */
public class CookieBasedGeoIpMockServiceTest {
  private static final String COUNTRY_COOKIE_KEY = "countryCode";
  private DefaultGeoIpService defaultGeoIpService = mock(DefaultGeoIpService.class);
  private CookieService cookieService = mock(CookieService.class);

  @Test
  public void test() {
    CookieBasedGeoIpMockService cookieBasedGeoIpMockService = new CookieBasedGeoIpMockService(
      defaultGeoIpService, cookieService);

    when(cookieService.get(COUNTRY_COOKIE_KEY)).thenReturn("US");
    assertEquals("US", cookieBasedGeoIpMockService.resolveCountryCode());

    when(cookieService.get(COUNTRY_COOKIE_KEY)).thenReturn(null);
    when(defaultGeoIpService.resolveCountryCode()).thenReturn("DE");
    assertEquals("DE", cookieBasedGeoIpMockService.resolveCountryCode());
  }

  @Test
  public void testRemoval() {
    CookieService cookieService = mock(CookieService.class);
    new CookieBasedGeoIpMockService(null, cookieService).removeCountryCode();
    verify(cookieService).remove(COUNTRY_COOKIE_KEY);
  }
}
