package com.kiga.web.security.session;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 13.10.16.
 */
public class RedisSessionTest {
  @Test
  public void testBean() {
    StatefulRedisConnection connection = mock(StatefulRedisConnection.class);
    RedisSession redisSession = new RedisSession(connection);
    RedisCommands commands = mock(RedisCommands.class);
    when(connection.sync()).thenReturn(commands);
    when(commands.get("foo")).thenReturn("bar");

    Assert.assertEquals("bar", redisSession.get("foo"));
  }

}
