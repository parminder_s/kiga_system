package com.kiga.web;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.AbstractXmlHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rainerh on 30.10.16.
 */
public class KigaWebMvcConfigurerTest {
  @Test
  public void testCors() {
    CorsRegistry registry = mock(CorsRegistry.class);
    new KigaWebMvcConfigurer().addCorsMappings(registry);
    verify(registry).addMapping("/**");
  }

  @Test
  public void testSingleStringConverter() {
    HttpMessageConverter stringConverter = mock(StringHttpMessageConverter.class);
    HttpMessageConverter otherConverter = mock(AbstractXmlHttpMessageConverter.class);

    List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
    converters.add(stringConverter);
    converters.add(otherConverter);
    new KigaWebMvcConfigurer().extendMessageConverters(converters);

    assertEquals(otherConverter, converters.get(0));
    assertEquals(stringConverter, converters.get(1));
  }
}
