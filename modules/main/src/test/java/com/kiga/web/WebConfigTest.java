package com.kiga.web;

import static org.hamcrest.core.IsInstanceOf.instanceOf;

import com.fasterxml.jackson.module.scala.DefaultScalaModule;
import com.kiga.web.props.WebProperties;
import com.kiga.web.security.session.DefaultSession;
import com.kiga.web.service.DefaultGeoIpService;
import com.kiga.web.service.DefaultUrlGenerator;
import com.kiga.web.service.GeoIp2DatabaseResolver;
import com.kiga.web.service.UserIpService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import java.io.IOException;

/** Created by rainerh on 12.10.16. */
public class WebConfigTest {
  @Test
  public void testBeans() throws IOException {
    WebConfig webConfig = new WebConfig();

    Assert.assertThat(webConfig.getDispatcherServlet(), instanceOf(DispatcherServlet.class));
    Assert.assertThat(webConfig.getScalaJackson(), instanceOf(DefaultScalaModule.class));
    Assert.assertThat(
        webConfig.getUrlGenerator(new WebProperties()), instanceOf(DefaultUrlGenerator.class));
    Assert.assertThat(webConfig.getSession(null, null), instanceOf(DefaultSession.class));
    Assert.assertThat(
        webConfig.getDelegatingProxyFilter(), instanceOf(DelegatingFilterProxy.class));
    Assert.assertThat(
        webConfig.getGeoIpService(null, null, null, new WebProperties(), null, null),
        instanceOf(DefaultGeoIpService.class));
    Assert.assertThat(
        webConfig.getServletContextInitializer(null),
        instanceOf(KigaServletContextInitializer.class));
  }

  @Test
  public void testGeoIpService() throws IOException {
    WebConfig webConfig = new WebConfig();
    WebProperties webProperties = new WebProperties();
    webProperties.setGeoIp2CountryDatabase("GeoLite2-Country.mmdb");
    Assert.assertThat(
        webConfig.getGeoIp2DatabaseResolver(webProperties),
        instanceOf(GeoIp2DatabaseResolver.class));
  }

  @Test
  public void testUserIpService() {
    WebProperties webProperties = new WebProperties();
    WebConfig webConfig = new WebConfig();
    Assert.assertThat(webConfig.getUserIpService(webProperties), instanceOf(UserIpService.class));
  }
}
