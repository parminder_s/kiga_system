package com.kiga.web.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author bbs
 * @since 9/29/16.
 */
public class ConfigurationBasedGeoIpMockServiceTest {
  @Test
  public void test() {
    ConfigurationBasedGeoIpMockService configurationBasedGeoIpMockService1 = new
      ConfigurationBasedGeoIpMockService("AT");
    assertEquals("AT", configurationBasedGeoIpMockService1.resolveCountryCode());
    configurationBasedGeoIpMockService1.changeCountryCode("US");
    assertEquals("US", configurationBasedGeoIpMockService1.resolveCountryCode());
    ConfigurationBasedGeoIpMockService configurationBasedGeoIpMockService2 = new
      ConfigurationBasedGeoIpMockService("DE");
    assertEquals("DE", configurationBasedGeoIpMockService2.resolveCountryCode());
    configurationBasedGeoIpMockService2.changeCountryCode("CH");
    assertEquals("CH", configurationBasedGeoIpMockService2.resolveCountryCode());
  }
}
