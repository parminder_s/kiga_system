package com.kiga.web.security;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.Mockito.mock;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * Created by rainerh on 02.11.16.
 *
 * <p>This is only for test coverage purposes, no real testing is done here.
 */
public class WebSecurityConfigTest {
  @Test
  public void test() throws Exception {
    WebSecurityConfig config = new WebSecurityConfig();
    Assert.assertThat(
        config.getSecurityContextRepository(),
        instanceOf(SecurityContextRepositoryWithoutSessionCreation.class));

    config.configure(
        new HttpSecurity(
            mock(ObjectPostProcessor.class),
            new AuthenticationManagerBuilder(mock(ObjectPostProcessor.class)),
            new HashedMap()));
  }

  @Test
  public void testRepository() {
    SecurityContextRepositoryWithoutSessionCreation repositoryWithoutSessionCreation =
        new SecurityContextRepositoryWithoutSessionCreation();
  }
}
