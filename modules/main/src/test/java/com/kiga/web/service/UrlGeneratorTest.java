package com.kiga.web.service;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 10.04.16.
 */
public class UrlGeneratorTest {
  @Test
  public  void noSlashes() {
    DefaultUrlGenerator generator = new DefaultUrlGenerator(
      "https://www.kigaportal.com/endpoint",
      "https://www.kigaportal.com",
      "https://www.kigaportal.com/ng"
    );

    Assert.assertEquals(generator.getBaseUrl("method"), "https://www.kigaportal.com/endpoint/method");
    Assert.assertEquals(generator.getUrl("method"), "https://www.kigaportal.com/method");
    Assert.assertEquals(generator.getNgUrl("ngMethod"), "https://www.kigaportal.com/ng/ngMethod");
  }

  @Test
  public void twoSlashes() {
    DefaultUrlGenerator generator = new DefaultUrlGenerator(
      "https://www.kigaportal.com/endpoint",
      "https://www.kigaportal.com/",
      ""
    );

    Assert.assertEquals(generator.getBaseUrl("/method"), "https://www.kigaportal.com/endpoint/method");
    Assert.assertEquals(generator.getUrl("/method"), "https://www.kigaportal.com/method");
  }

  @Test
  public void firstOnlySlash() {
    DefaultUrlGenerator generator = new DefaultUrlGenerator(
      "https://www.kigaportal.com/endpoint",
      "https://www.kigaportal.com/",
      ""
    );

    Assert.assertEquals(generator.getBaseUrl("method"), "https://www.kigaportal.com/endpoint/method");
    Assert.assertEquals(generator.getUrl("method"), "https://www.kigaportal.com/method");
  }

  @Test
  public void secondOnlySlash() {
    DefaultUrlGenerator generator = new DefaultUrlGenerator(
      "https://www.kigaportal.com/endpoint",
      "https://www.kigaportal.com",
      ""
    );

    Assert.assertEquals(generator.getBaseUrl("/method"), "https://www.kigaportal.com/endpoint/method");
    Assert.assertEquals(generator.getUrl("/method"), "https://www.kigaportal.com/method");
  }
}
