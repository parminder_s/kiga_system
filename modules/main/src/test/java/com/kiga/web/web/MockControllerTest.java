package com.kiga.web.web;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * Created by rainerh on 23.11.16.
 */
public class MockControllerTest {
  @Test
  public void testCheapestPrice() {
    Map<String, Object> cheapestPrice = new MockController().getCheapestPrice();
    Assert.assertEquals("EUR", cheapestPrice.get("currency"));
    Assert.assertEquals(7.9, cheapestPrice.get("price"));
    Assert.assertEquals(1, cheapestPrice.get("duration"));
  }
}
