package com.kiga.web.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.main.locale.Locale;
import org.junit.Assert;
import org.junit.Test;

/** Created by peter on 10.05.16. */
public class SiteTreeUrlGeneratorTest {
  @Test
  public void testUrl() {
    final KigaIdeaDraft kigaIdea1 = new KigaIdeaDraft();
    final KigaIdeaDraft kigaIdea2 = new KigaIdeaDraft();
    final KigaIdeaDraft kigaIdea3 = new KigaIdeaDraft();
    final KigaIdeaDraft kigaIdea4 = new KigaIdeaDraft();

    kigaIdea4.setUrlSegment("idea");
    kigaIdea4.setParent(kigaIdea3);
    kigaIdea3.setUrlSegment("bereich");
    kigaIdea3.setParent(kigaIdea2);
    kigaIdea2.setUrlSegment("kategorie");
    kigaIdea2.setParent(kigaIdea1);
    kigaIdea1.setUrlSegment("locale");
    kigaIdea1.setClassName("HomePage");
    kigaIdea1.setLocale(Locale.en);
    kigaIdea1.setParent(null);

    UrlGenerator urlGenerator = mock(UrlGenerator.class);
    when(urlGenerator.getUrl(anyString()))
        .then(answer -> "www.kigaportal.com/" + answer.getArguments()[0]);
    when(urlGenerator.getNgUrl(anyString()))
        .then(answer -> "www.kigaportal.com/" + answer.getArguments()[0]);
    SiteTreeUrlGenerator siteTreeUrlGenerator = new SiteTreeUrlGenerator(urlGenerator, false);
    Assert.assertEquals(
        "www.kigaportal.com/locale/kategorie/bereich/idea", siteTreeUrlGenerator.getUrl(kigaIdea4));
  }

  @Test
  public void testRelativeUrlWithAnchor() {
    UrlGenerator urlGenerator = mock(UrlGenerator.class);
    when(urlGenerator.getUrl(anyString()))
        .then(answer -> "http://www.kigaportal.com" + answer.getArguments()[0]);
    SiteTree siteTree = new SiteTreeLive();
    siteTree.setClassName("HomePage");
    siteTree.setUrlSegment("/ng/#!/en/state?id=5");
    siteTree.setLocale(Locale.en);
    SiteTreeUrlGenerator siteTreeUrlGenerator = new SiteTreeUrlGenerator(urlGenerator, false);
    Assert.assertEquals("/ng/#!/en/state?id=5", siteTreeUrlGenerator.getRelativeUrl(siteTree));
  }

  @Test
  public void testMalformedRelativeUrl() {
    UrlGenerator urlGenerator = mock(UrlGenerator.class);
    when(urlGenerator.getUrl(anyString()))
        .then(answer -> "malformedUrl" + answer.getArguments()[0]);
    SiteTree siteTree = new SiteTreeLive();
    siteTree.setClassName("HomePage");
    siteTree.setUrlSegment("#@");
    siteTree.setLocale(Locale.en);
    SiteTreeUrlGenerator siteTreeUrlGenerator = new SiteTreeUrlGenerator(urlGenerator, false);
    Assert.assertEquals("malformedUrl#@", siteTreeUrlGenerator.getRelativeUrl(siteTree));
  }

  @Test
  public void testRelativeUrlWithoutRef() {
    UrlGenerator urlGenerator = mock(UrlGenerator.class);
    when(urlGenerator.getUrl(anyString()))
        .then(answer -> "http://www.kigaportal.com" + answer.getArguments()[0]);
    SiteTree siteTree = new SiteTreeLive();
    siteTree.setClassName("HomePage");
    siteTree.setUrlSegment("/ng/");
    siteTree.setLocale(Locale.en);
    SiteTreeUrlGenerator siteTreeUrlGenerator = new SiteTreeUrlGenerator(urlGenerator, false);
    Assert.assertEquals("/ng/", siteTreeUrlGenerator.getRelativeUrl(siteTree));
  }

  @Test
  public void testRelativeNgUrl() {
    UrlGenerator urlGenerator = mock(UrlGenerator.class);
    when(urlGenerator.getNgUrl(anyString()))
        .then(answer -> "http://localhost:8080/#!/" + answer.getArguments()[0]);
    SiteTree homePage = new SiteTreeLive();
    homePage.setLocale(Locale.en);
    homePage.setClassName("HomePage");
    homePage.setUrlSegment("home");

    SiteTree subjects = new SiteTreeLive();
    subjects.setParent(homePage);
    subjects.setClassName("KigaIdea");
    subjects.setUrlSegment("idea");

    SiteTreeUrlGenerator siteTreeUrlGenerator = new SiteTreeUrlGenerator(urlGenerator, false);
    Assert.assertEquals("/#!/en/cms/idea", siteTreeUrlGenerator.getRelativeNgUrl(subjects));
  }

  @Test
  public void testAngularCmsEnabled() {
    UrlGenerator urlGenerator = mock(UrlGenerator.class);
    when(urlGenerator.getNgUrl(anyString()))
        .then(answer -> "http://localhost:8080/ng/" + answer.getArguments()[0]);
    SiteTree homePage = new SiteTreeLive();
    homePage.setLocale(Locale.en);
    homePage.setClassName("HomePage");
    homePage.setUrlSegment("home");

    SiteTree subjects = new SiteTreeLive();
    subjects.setParent(homePage);
    subjects.setClassName("IdeaGroupContainer");
    subjects.setUrlSegment("idea");

    SiteTreeUrlGenerator siteTreeUrlGenerator = new SiteTreeUrlGenerator(urlGenerator, true);

    assertThat(siteTreeUrlGenerator.getRelativeNgUrl(subjects))
        .isEqualToIgnoringWhitespace("/ng/ng6/en/idea");
  }
}
