package com.kiga.web.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.maxmind.geoip2.exception.GeoIp2Exception;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 9/29/16.
 */
public class DefaultGeoIpServiceTest {
  private static final String MOCK_COUNTRY_CODE = "DE";
  private static final String NEW_COUNTRY_CODE = "US";
  private static final String MOCK_IP = "0.0.0.0";
  private DefaultGeoIpService defaultGeoIpService;
  private HttpServletRequest httpServletRequestMock;
  private LocateByIpResolver locateByIpResolverMock;
  private UserIpService userIpService;

  @Rule
  public ExpectedException exception = ExpectedException.none();

  /**
   * just prepare tests.
   */
  @Before
  public void prepareTests() {
    httpServletRequestMock = mock(HttpServletRequest.class);
    locateByIpResolverMock = mock(GeoIp2DatabaseResolver.class);
    userIpService = mock(UserIpService.class);
    defaultGeoIpService = new DefaultGeoIpService(locateByIpResolverMock, userIpService,
      httpServletRequestMock);
  }

  @Test
  public void testResolveCountryCodeFromRequest() throws IOException, GeoIp2Exception {
    Cookie[] cookies = new Cookie[]{new Cookie("dummyNonMatchingName", MOCK_COUNTRY_CODE)};
    when(httpServletRequestMock.getCookies()).thenReturn(cookies);
    when(userIpService.getUserIp(httpServletRequestMock)).thenReturn(MOCK_IP);
    when(locateByIpResolverMock.getCountryIsoCode(MOCK_IP)).thenReturn(MOCK_COUNTRY_CODE);
    String countryCode = defaultGeoIpService.resolveCountryCode();

    assertNotNull(countryCode);
    assertEquals(MOCK_COUNTRY_CODE, countryCode);
    verify(userIpService).getUserIp(httpServletRequestMock);
    verify(locateByIpResolverMock).getCountryIsoCode(MOCK_IP);
  }

  @Test
  public void testChangeCountryThrowsException() {
    exception.expect(NotImplementedException.class);
    new DefaultGeoIpService(null, null, null).removeCountryCode();
  }


}
