package com.kiga.web.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 10/1/16.
 */
public class UserIpServiceTest {
  private static final String MOCKED_IP = "127.0.0.1";
  private static final String IP_HTTP_HEADER_NAME = "X-CUSTOM-HEADER";
  private final HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
  private UserIpService userIpService;

  /**
   * Test preparation.
   */
  @Before
  public void prepareTests() {
    userIpService = new UserIpService(IP_HTTP_HEADER_NAME);
    when(httpServletRequest.getHeader(IP_HTTP_HEADER_NAME)).thenReturn(MOCKED_IP);
  }

  @Test
  public void test() {
    assertEquals(MOCKED_IP, userIpService.getUserIp(httpServletRequest));
    verify(httpServletRequest).getHeader(IP_HTTP_HEADER_NAME);
  }

  @Test
  public void testDefault() {
    HttpServletRequest request = mock(HttpServletRequest.class);
    when(request.getRemoteAddr()).thenReturn("212.168.234.12");
    assertEquals("212.168.234.12", new UserIpService("default").getUserIp(request));
    verify(request).getRemoteAddr();
  }
}
