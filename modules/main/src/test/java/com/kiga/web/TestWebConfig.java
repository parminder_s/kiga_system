package com.kiga.web;

import com.kiga.content.repository.ss.RepositoryContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 7/25/16.
 */
@SpringBootApplication
public class TestWebConfig {
  @Bean
  public HttpServletRequest createHttpServletRequestMock() {
    return new MockHttpServletRequest();
  }

  @Bean
  public RepositoryContext createRepositoryContext(HttpServletRequest request) {
    return new RepositoryContext(request);
  }
}
