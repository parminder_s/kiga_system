package com.kiga.mail;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isEmpty;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.kiga.mail.config.MailProperties;
import com.kiga.mail.config.MailPropertiesAws;
import com.kiga.mail.config.MailerType;
import com.kiga.mail.logic.LogMailer;
import com.kiga.mail.logic.MailHelper;
import com.kiga.mail.model.EMailFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class MailConfigTest {
  @Rule public ExpectedException thrown = ExpectedException.none();

  @Test
  public void emailService() {
    MailProperties mailProperties =
        MailProperties.builder()
            .type(MailerType.AWS)
            .aws(
                MailPropertiesAws.builder()
                    .key("accessKey")
                    .secret("secretKey")
                    .region(Regions.US_WEST_1)
                    .build())
            .build();

    AmazonSimpleEmailServiceClientBuilder builder =
        new MailConfig().awsClientBuilder(mailProperties);

    assertThat(
        builder,
        allOf(
            hasProperty("region", equalTo("us-west-1")),
            hasProperty(
                "credentials",
                allOf(
                    hasProperty(
                        "credentials",
                        allOf(
                            hasProperty("AWSAccessKeyId", equalTo("accessKey")),
                            hasProperty("AWSSecretKey", equalTo("secretKey"))))))));
  }

  /** Unfortunately we can only mock classes in Mockito v2. */
  @Test
  public void getAwsMailer() {
    thrown.expect(NullPointerException.class);

    new MailConfig()
        .getMailer(MailProperties.builder().type(MailerType.AWS).build(), null, null, null);
  }

  @Test
  public void getLogMailer() {
    assertTrue(
        new MailConfig()
                .getMailer(MailProperties.builder().type(MailerType.LOG).build(), null, null, null)
            instanceof LogMailer);
  }

  @Test
  public void getLogMailerWithNullType() {
    thrown.expect(NullPointerException.class);
    new MailConfig().getMailer(null, null, null, null);
  }

  @Test
  public void getEMailFactory() {
    assertNotNull(new MailConfig().getEMailFactory());
  }

  @Test
  public void getMailHelperWithoutRedirection() {
    MailProperties properties =
        MailProperties.builder()
            .redirectMails(false)
            .defaultSender("somebody@kigaportal.com")
            .build();
    MailHelper helper = new MailConfig().getMailHelper(properties, mock(EMailFactory.class));

    assertTrue(!helper.getRedirectAddress().isPresent());
    assertThat(
        helper,
        allOf(
            hasProperty("redirectAddress", isEmpty()),
            hasProperty("defaultSender", equalTo("somebody@kigaportal.com"))));
  }

  @Test
  public void getMailHelperWithRedirection() {
    MailProperties properties =
        MailProperties.builder()
            .redirectMails(true)
            .redirectAddress("redirect@kigaportal.com")
            .build();
    MailHelper helper = new MailConfig().getMailHelper(properties, mock(EMailFactory.class));

    assertEquals("redirect@kigaportal.com", helper.getRedirectAddress().get());
  }

  @Test
  public void getMailHelperWithRedirectionAndInvalidEMail() {
    MailProperties properties =
        MailProperties.builder().redirectMails(true).redirectAddress("").build();
    MailHelper helper = new MailConfig().getMailHelper(properties, mock(EMailFactory.class));

    assertTrue(!helper.getRedirectAddress().isPresent());
  }
}
