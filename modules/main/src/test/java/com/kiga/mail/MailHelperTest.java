package com.kiga.mail;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.mail.logic.MailHelper;
import com.kiga.mail.model.EMail;
import com.kiga.mail.model.EMailFactory;
import org.junit.Test;

import java.util.Optional;

public class MailHelperTest {
  @Test
  public void validateWithoutChange() {
    MailHelper mailer = new MailHelper(null, null, Optional.empty());
    EMail email = new TestEMail().withTo("receiver@test.com").withFrom("sender@test.com");

    mailer.preProcess(email);

    assertEquals("receiver@test.com", email.getTo().get());
    assertEquals("sender@test.com", email.getFrom().get());
  }

  @Test
  public void validateWithRedirect() {
    MailHelper mailer = new MailHelper(null, null, Optional.of("foo@bar.com"));
    EMail email = new TestEMail().withTo("receiver@test.com").withFrom("sender@test.com");

    mailer.preProcess(email);

    assertEquals("foo@bar.com", email.getTo().get());
    assertEquals("sender@test.com", email.getFrom().get());
  }

  @Test
  public void validateWithDefaultSender() {
    MailHelper mailer = new MailHelper("foo@bar.com", null, Optional.empty());
    EMail email = new TestEMail().withTo("receiver@test.com");

    mailer.preProcess(email);

    assertEquals("receiver@test.com", email.getTo().get());
    assertEquals("foo@bar.com", email.getFrom().get());
  }

  @Test
  public void createMessage() {
    EMailFactory factory = mock(EMailFactory.class);
    EMail email = new TestEMail();
    when(factory.createEMail()).thenReturn(email);

    assertEquals(email, new MailHelper("", factory, Optional.empty()).createEMail());
  }

  @Test
  public void createEMail() {
    EMailFactory factory = mock(EMailFactory.class);
    new MailHelper(null, factory, null).createEMail();

    verify(factory).createEMail();
  }
}
