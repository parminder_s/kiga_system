package com.kiga.mail;

import com.kiga.mail.model.EMail;
import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.OutputStream;
import java.util.Optional;

public class TestEMail implements EMail {
  private Optional<String> from = Optional.empty();
  private Optional<String> to = Optional.empty();
  private String replyTo;
  private String subject;
  private String body;
  private String logCategory;
  private String logSubCategory;
  private String logReferenceId;

  @Override
  public EMail withFrom(String from) {
    this.from = Optional.of(from);
    return this;
  }

  @Override
  public Optional<String> getFrom() {
    return from;
  }

  @Override
  public EMail withTo(String to) {
    this.to = Optional.of(to);
    return this;
  }

  @Override
  public Optional<String> getTo() {
    return this.to;
  }

  @Override
  public EMail withSubject(String subject) {
    this.subject = subject;
    return this;
  }

  @Override
  public String getSubject() {
    return subject;
  }

  @Override
  public EMail withBody(String body) {
    this.body = body;
    return this;
  }

  @Override
  public String getBody() {
    return this.body;
  }

  @Override
  public String getLogCategory() {
    return this.logCategory;
  }

  @Override
  public String getLogSubCategory() {
    return this.logSubCategory;
  }

  @Override
  public String getLogReferenceId() {
    return this.logReferenceId;
  }

  @Override
  public EMail withLogData(String category, String subCategory, String referenceId) {
    this.logCategory = category;
    this.logSubCategory = subCategory;
    this.logReferenceId = referenceId;
    return this;
  }

  @Override
  public EMail withAttachment(String name, InputStreamSource inputStreamSource) {
    return this;
  }

  @Override
  public EMail withAttachment(String name, File file) {
    return this;
  }

  @Override
  public EMail withReplyTo(String replyTo) {
    this.replyTo = replyTo;
    return this;
  }

  @Override
  public EMail withInline(String contentId, Resource resource) {
    return null;
  }

  @Override
  public void writeTo(OutputStream outputStream) {}
}
