package com.kiga.mail.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.MimeMessageHelper;

import java.io.IOException;
import java.io.OutputStream;
import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class MimeEMailTest {
  @Test
  public void testSetFrom() throws MessagingException {
    MimeMessageHelper helper = mock(MimeMessageHelper.class);
    EMail email = new MimeEMail(null, helper);
    email.withFrom("foo@bar.com");

    verify(helper).setFrom("foo@bar.com");
  }

  @Test
  public void getFrom() throws MessagingException {
    MimeMessage msg = mock(MimeMessage.class);
    when(msg.getFrom()).thenReturn(new Address[] {});
    assertFalse(new MimeEMail(msg, null).getFrom().isPresent());
  }

  @Test
  public void getFromEmpty() throws MessagingException {
    MimeMessage msg = mock(MimeMessage.class);
    Address[] addresses = createAddress("foo@bar.com");
    when(msg.getFrom()).thenReturn(addresses);

    assertEquals(
      "foo@bar.com",
      new MimeEMail(msg, null).getFrom().get()
    );
  }

  @Test
  public void setTo() throws MessagingException {
    MimeMessageHelper helper = mock(MimeMessageHelper.class);
    EMail email = new MimeEMail(null, helper);
    email.withTo("foo@bar.com");

    verify(helper).setTo("foo@bar.com");
  }

  @Test
  public void getToEmpty() throws MessagingException {
    MimeMessage msg = mock(MimeMessage.class);
    when(msg.getAllRecipients()).thenReturn(new Address[] {});
    assertFalse(new MimeEMail(msg, null).getTo().isPresent());
  }

  @Test
  public void getTo() throws MessagingException {
    MimeMessage msg = mock(MimeMessage.class);
    Address[] addresses = createAddress("to_foo@bar.com");
    when(msg.getAllRecipients()).thenReturn(addresses);

    assertEquals(
      "to_foo@bar.com",
      new MimeEMail(msg, null).getTo().get()
    );
  }

  @Test
  public void setSubject() throws MessagingException {
    MimeMessageHelper helper = mock(MimeMessageHelper.class);
    new MimeEMail(null, helper).withSubject("Hello");
    verify(helper).setSubject("Hello");
  }

  @Test
  public void getSubject() throws MessagingException {
    MimeMessage msg = mock(MimeMessage.class);
    when(msg.getSubject()).thenReturn("Hello");

    assertEquals("Hello", new MimeEMail(msg, null).getSubject());
  }

  @Test
  public void withBody() throws MessagingException {
    MimeMessageHelper helper = mock(MimeMessageHelper.class);
    new MimeEMail(null, helper).withBody("Dear Sir/Madam");
    verify(helper).setText("Dear Sir/Madam", true);
  }

  @Test
  public void getBody() throws IOException, MessagingException {
    EMail email = new MimeEMail(null, mock(MimeMessageHelper.class))
      .withBody("hello");

    assertEquals("hello", email.getBody());
  }

  @Test
  public void addAttachment() throws MessagingException {
    MimeMessageHelper helper = mock(MimeMessageHelper.class);
    InputStreamSource iss = mock(InputStreamSource.class);

    new MimeEMail(null, helper).withAttachment("test", iss);
    verify(helper).addAttachment("test", iss);
  }

  @Test
  public void writeTo() throws IOException, MessagingException {
    OutputStream outputStream = mock(OutputStream.class);
    MimeMessage mimeMessage = mock(MimeMessage.class);
    EMail email = new MimeEMail(mimeMessage, null);

    email.writeTo(outputStream);

    verify(mimeMessage).writeTo(outputStream);
  }

  private Address[] createAddress(String address) {
    Address returner = mock(Address.class);
    when(returner.toString()).thenReturn(address);
    return new Address[] {returner};
  }
}
