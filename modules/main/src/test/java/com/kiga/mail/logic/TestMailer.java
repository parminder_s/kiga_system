package com.kiga.mail.logic;

import static org.junit.Assert.assertNotNull;

import com.kiga.mail.Mailer;
import com.kiga.mail.TestEMail;
import com.kiga.mail.model.EMail;

import java.util.function.UnaryOperator;

/**
 * This is a mocked version of the mailer. It allows to test the email data passed as lambda to the
 * send function.
 */
public class TestMailer implements Mailer {
  private TestEMail testEMail;

  @Override
  public EMail createEMail() {
    return new TestEMail();
  }

  @Override
  public void send(String logCategory, String logSubCategory, String logReferenceId, EMail email) {
    if (email instanceof TestEMail) {
      this.testEMail = (TestEMail) email;
    } else {
      throw new RuntimeException("TestMailer only works with TestEMail");
    }
  }

  @Override
  public void send(
      String logCategory,
      String logSubCategory,
      String logReferenceId,
      UnaryOperator<EMail> unaryOperator) {
    TestEMail testEMail = new TestEMail();
    unaryOperator.apply(testEMail);
    this.send(logCategory, logSubCategory, logReferenceId, testEMail);
  }

  public TestEMail getSentEMail() {
    return this.testEMail;
  }

  public void verifySent() {
    assertNotNull("mail should have been sent", testEMail);
  }
}
