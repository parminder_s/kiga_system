package com.kiga.memberadmin;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.crm.messages.Address;
import com.kiga.crm.messages.response.Invoice;
import com.kiga.crm.messages.response.InvoiceOptionsResponse;
import com.kiga.crm.messages.response.LicenceType;
import com.kiga.crm.messages.response.PaymentOption;
import com.kiga.crm.messages.response.Product;
import com.kiga.crm.messages.response.ProductGroup;
import com.kiga.crm.messages.response.ResponseGetCustomerData;
import com.kiga.crm.messages.response.ResponseGetSubscriptions;
import com.kiga.crm.messages.response.Status;
import com.kiga.crm.messages.response.Subscription;
import com.kiga.crm.messages.response.SubscriptionType;
import com.kiga.crm.service.CrmConverter;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.memberadmin.domain.InvoiceOption;
import com.kiga.payment.PaymentType;
import com.kiga.security.domain.Member;
import com.kiga.shop.web.responses.InvoicesResponse;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by peter on 25.01.17.
 */
public class MemberAdminTestData {

  public static ResponseGetCustomerData responseGetCustomerData;

  /**
   *
   * @throws ParseException exeption on time parsing.
   */
  public static void reinit() throws ParseException {

    shopInvoiceList = new ArrayList<>();
    InvoicesResponse  invoiceResponse = new InvoicesResponse("12345",
      formatter.parse("2017-02-08").toInstant(), "https://shopinvoice",
      new BigDecimal(99.90), "EUR", false);
    shopInvoiceList.add(invoiceResponse);


    responseGetCustomerData = new ResponseGetCustomerData();
    responseGetCustomerData.setPaymentOption(PaymentMethod.CREDITCARD);
    responseGetCustomerData.setInvoiceOption(InvoiceOption.email_customer);
    responseGetCustomerData.setDateOfBirth(formatter.parse("01-11-1994"));
    responseGetCustomerData.setJob("atomkraftwerk");
    responseGetCustomerData.setOrgType("company");

    Status status = new Status();
    status.setPaid(1);
    status.setNumber("12345");
    responseGetCustomerData.setStatus(status);

    SubscriptionType subscriptionType = new SubscriptionType();
    responseGetCustomerData.setSubscriptionType(subscriptionType);

    Subscription subscription = new Subscription("1", 1, 3, formatter.parse("01-01-2017"),
      formatter.parse("01-03-2017"), "at", null, null, null, 0, 0, "xx", null, null, null, null,
      null, null);

    responseGetCustomerData.setSubscription(subscription);
    Address billing = new Address("Ned", "Flanders", "title", "male", "Evergreen Terrae", "743",
      null, null, "8010", "Springfield", "Austria", "at", null, null, "ned@flanders.at",
      "+43-6543-213", "987654321");
    responseGetCustomerData.setShippingAddress(shipping);
    responseGetCustomerData.setBillingAddress(billing);



    responseGetSubscription = new ResponseGetSubscriptions();
    responseGetSubscription.setCurrency("EUR");
    responseGetSubscription.setAgeLimit(18);

    InvoiceOptionsResponse invoiceOptionsResponse = new InvoiceOptionsResponse();
    invoiceOptionsResponse.setInvoiceOptions(new ArrayList<>());
    invoiceOptionsResponse.getInvoiceOptions().add(InvoiceOption.post_customer);
    invoiceOptionsResponse.getInvoiceOptions().add(InvoiceOption.email_customer);


    List<LicenceType> licenceTypes = new ArrayList<>();
    licenceTypes.add(new LicenceType("at", 1, 2, 3.0, 4, 5, 6));
    licenceTypes.add(new LicenceType("at", 7, 8, 9.0, 10, 11, 12));
    responseGetSubscription.setInvoiceOptions(invoiceOptionsResponse);
    responseGetSubscription.setLicenceTypes(licenceTypes);


    ArrayList<PaymentOption> paymentOptions = new ArrayList<PaymentOption>(Arrays.asList(
      new PaymentOption(1, "RE", "Rechnung", 0, null, null),
      new PaymentOption(7, "MP-ELV", "Elektronisches Lastschriftverfahren", 1, null, null)
    ));

    responseGetSubscription.setPaymentOptions(paymentOptions);

    List<ProductGroup> productGroups = new ArrayList<>();
    productGroups.add(new ProductGroup("GIFT", "standard"));
    productGroups.add(new ProductGroup("SITE", "forum"));
    productGroups.add(new ProductGroup("OL", "licence"));


    responseGetSubscription.setProductGroups(productGroups);


    List<Product> products = new ArrayList<>();
    products.add(new Product(1, "1", "standard", "3", 10.0, 10.0, "0", 0,
      Arrays.asList(PaymentType.RE, PaymentType.MP_ELV, PaymentType.MP_CC,
                    PaymentType.PAYPAL)));

    products.add(new Product(2, "1", "standard", "6", 15.0, 15.0, "0", 0,
      Arrays.asList(PaymentType.RE, PaymentType.MP_ELV, PaymentType.MP_CC,
                    PaymentType.PAYPAL)));

    responseGetSubscription.setProducts(products);


    responseGetSubscription.setVat(20.00);
    responseGetSubscription.setVatSubtractable(10.00);

  }

  public static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");


  public static Address shipping = createAddress();

  /**
   * Create a new, pre-filled address for use in tests.
   * @return the address object.
   */
  public static Address createAddress() {
    return new Address("Homer", "Simpson", "title", "male",
      "Evergreen Terrae", "742", null, null, "8010", "Springfield", "Austria", "at", null, null,
      "homer@simpson.at","+43-1234-56", "123456789");
  }

  public static List<InvoicesResponse> shopInvoiceList;


  /**
   *
   * @return responseGetSubscriptions for Unit tests.
   */
  public static ResponseGetSubscriptions responseGetSubscription;

  /**
   *
   * @return crmInvoices list for unit tests.
   */
  public static List<Invoice> crmInvoices() throws ParseException {
    List<Invoice> invoiceList = new ArrayList<>();
    invoiceList.add(
      new Invoice("paid", formatter.parse("2017-02-10"), 19.90,"EUR", 1, 1, "AT0123", 19.90,
        formatter.parse("2017-02-12"), 0, "https://invoice1"));

    invoiceList.add(
      new Invoice("open",formatter.parse("2017-02-08"), 29.90,"EUR", 1, 1, "AT0456", 29.90,
        formatter.parse("2017-02-08"), 1, "https://invoice2"));

    return invoiceList;

  }

  /**
   *
   * @return crmConverter for Unit tests.
   * @throws Exception exp.
   */
  public static CrmConverter getCrmConverterMock() throws Exception {
    CrmConverter returner = mock(CrmConverter.class);
    when(returner.customerGetData(any())).thenReturn(MemberAdminTestData.responseGetCustomerData);
    when(returner.masterGetSubscriptions(any())).thenReturn(
      MemberAdminTestData.responseGetSubscription);
    when(returner.customerGetInvoice()).thenReturn(MemberAdminTestData.crmInvoices());
    return returner;
  }

  /**
   *
   * @return Member for MemberAdminUnitTests.
   */
  public static Member getMember() {
    Member returner = new Member();
    returner.setNickname("hsimpson");
    returner.setEmail("homer@simpson.at");
    returner.setCountry("at");
    returner.setId(1L);
    returner.setLocale("en");
    return returner;
  }
}
