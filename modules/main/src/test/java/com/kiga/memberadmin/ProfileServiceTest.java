package com.kiga.memberadmin;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.crm.messages.Address;
import com.kiga.crm.service.CrmConverter;
import com.kiga.homepage.domain.draft.HomePageElementDraft;
import com.kiga.main.locale.Locale;
import com.kiga.memberadmin.domain.AddressType;
import com.kiga.memberadmin.service.ProfileService;
import com.kiga.memberadmin.web.request.Email;
import com.kiga.memberadmin.web.request.ProfileAdditionalRequest;
import com.kiga.memberadmin.web.response.LanguageResponse;
import com.kiga.memberadmin.web.response.ProfileAdditionalResponse;
import com.kiga.memberadmin.web.response.ProfileResponse;
import com.kiga.s3.domain.S3Image;
import com.kiga.security.domain.Member;
import com.kiga.security.repository.MemberRepository;
import com.kiga.translation.TranslationProperties;
import com.mpay24.etp.Profile;
import org.elasticsearch.monitor.jvm.JvmInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.text.ParseException;
import java.util.HashMap;

/**
 * Created by peter on 08.02.17.
 */

public class ProfileServiceTest {

  @Before
  public void init() throws ParseException {
    MemberAdminTestData.reinit();
  }

  @Test
  public void getProfileTest() throws Exception {

    com.kiga.forum.service.ProfileService forumProfileService = mock(
      com.kiga.forum.service.ProfileService.class);
    when(forumProfileService.getUrl(any())).thenReturn("https://forumurl");

    CrmConverter crmConverter = MemberAdminTestData.getCrmConverterMock();

    ProfileService profileService = new ProfileService(crmConverter,
      null, null, null, null, forumProfileService);

    ProfileResponse profileResponse = profileService.getProfile(MemberAdminTestData.getMember());

    Assert.assertEquals("avatar nickname", "hsimpson", profileResponse.getNickname());
    Assert.assertEquals("avatar address", MemberAdminTestData.shipping,
      profileResponse.getAddress());
    Assert.assertEquals("avatar avatar url", "https://forumurl",
      profileResponse.getAvatar().getUrl());
    Assert.assertEquals("avatar birthday", MemberAdminTestData.formatter.parse("01-11-1994"),
      profileResponse.getBirthday());
    Assert.assertEquals("avatar minimum age", 18, profileResponse.getMinimumAge());
  }

  @Test
  public void updateLocaleTest() throws Exception {
    Member member = MemberAdminTestData.getMember();
    MemberRepository memberRepository = mock(MemberRepository.class);
    when(memberRepository.findOne(any())).thenReturn(member);
    CrmConverter crmConverter = MemberAdminTestData.getCrmConverterMock();

    ProfileService profileService = new ProfileService(crmConverter, null, memberRepository,
      null, null, null);

    Assert.assertEquals(member.getLocale(), "en"); //verify language before update

    profileService.updateLocale(member, "it");

    ArgumentCaptor<Member> memberArgumentCaptor = ArgumentCaptor.forClass(
      Member.class);
    verify(memberRepository, times(1)).save(memberArgumentCaptor.capture());
    Member saveParameter =  memberArgumentCaptor.getValue();

    verify(memberRepository).save(eq(member));
    verify(crmConverter).userSetLocale(eq("it"));
    Assert.assertEquals(saveParameter.getLocale(), "it"); //verify language after update

  }

  @Test
  public void updateProfileTest() throws Exception {
    Member member = MemberAdminTestData.getMember();
    Address address = MemberAdminTestData.shipping;
    CrmConverter crmConverter = MemberAdminTestData.getCrmConverterMock();
    MemberRepository memberRepository = mock(MemberRepository.class);
    when(memberRepository.findOne(any())).thenReturn(member);
    ProfileService profileService = new ProfileService(crmConverter, null, memberRepository, null,
      null, null);
    S3Image avatar = new S3Image();

    profileService.updateProfile(member, address,
      MemberAdminTestData.formatter.parse("01-01-2001"), "nflanders", avatar);

    ArgumentCaptor<Member> memberArgumentCaptor = ArgumentCaptor.forClass(
      Member.class);
    verify(memberRepository, times(1)).save(memberArgumentCaptor.capture());
    Member saveParameter =  memberArgumentCaptor.getValue();

    Assert.assertEquals("member nickname", "nflanders", saveParameter.getNickname());
    Assert.assertEquals("member s3Avatar", avatar, saveParameter.getS3Avatar());

  }

  @Test
  public void languageResponseTest() {
    HashMap localeMap = new HashMap<Locale, String>();
    localeMap.put(Locale.de, "DE");
    localeMap.put(Locale.en, "EN");

    TranslationProperties translationProperties = new TranslationProperties();
    translationProperties.setLocales(localeMap);

    ProfileService profileService = new ProfileService(null, translationProperties, null, null,
      null, null);
    LanguageResponse response = profileService.languageResponse(MemberAdminTestData.getMember());

    Assert.assertEquals("language value", "EN", response.getValue());
    Assert.assertEquals("language array size", 2, response.getCode().size());
    Assert.assertEquals("contains DE", true, response.getCode().contains("DE"));
    Assert.assertEquals("contains EN", true, response.getCode().contains("EN"));
  }

  @Test
  public void profileAdditionalTest() throws Exception {
    ProfileService profileService = new ProfileService(MemberAdminTestData.getCrmConverterMock(),
      null, null, null, null, null);
    ProfileAdditionalResponse response = profileService.profileAdditional();

    Assert.assertEquals("profile additional job", "atomkraftwerk", response.getJob());
    Assert.assertEquals("profile additional org type", "company", response.getOrganization());
  }

  @Test
  public void profileAdditionalSaveTest() throws Exception {
    CrmConverter crmConverter = MemberAdminTestData.getCrmConverterMock();
    ProfileAdditionalRequest profileAdditionalRequest = new ProfileAdditionalRequest();
    profileAdditionalRequest.setOrganization("org");
    profileAdditionalRequest.setJob("job");
    profileAdditionalRequest.setJobName("jobname123");

    ProfileService profileService = new ProfileService(crmConverter, null, null, null, null, null);
    profileService.profileAdditionalSave(
      profileAdditionalRequest);

    verify(crmConverter, times(1))
      .customerSetOptions(eq("job"), eq("org"), eq("jobname123"));
  }

  @Test
  public void findEmailTest() {
    Member member = MemberAdminTestData.getMember();
    MemberRepository memberRepository = mock(MemberRepository.class);
    when(memberRepository.findOne(any())).thenReturn(member);

    ProfileService profileService = new ProfileService(null, null, memberRepository, null, null,
      null);

    Email email = profileService.findEmail(member);
    Assert.assertEquals("verify find Email", "homer@simpson.at", email.getEmail());
  }

  @Test
  public void checkEmailtest() throws Exception {
    CrmConverter crmConverter = MemberAdminTestData.getCrmConverterMock();
    ProfileService profileService = new ProfileService(crmConverter,
      null, null, null, null, null);
    profileService.checkEmail("myemail");
    verify(crmConverter, times(1)).userTestUserName("myemail");
  }

  @Test
  public void updateEmailTest() throws Exception {
    MemberRepository memberRepository = mock(MemberRepository.class);
    Member member = MemberAdminTestData.getMember();
    when(memberRepository.findOne(any())).thenReturn(member);
    CrmConverter crmConverter = MemberAdminTestData.getCrmConverterMock();
    ProfileService profileService = new ProfileService(crmConverter, null, memberRepository, null,
      null, null);
    Assert.assertEquals("email before update", "homer@simpson.at", member.getEmail());

    profileService.updateEmail(member, "newEmail");

    ArgumentCaptor<Member> memberArgumentCaptor = ArgumentCaptor.forClass(
      Member.class);
    verify(memberRepository, times(1)).save(memberArgumentCaptor.capture());
    verify(crmConverter).userSetLogin(eq("newEmail"));
    Assert.assertEquals("email after update", "newEmail", memberArgumentCaptor.getValue()
      .getEmail());
  }
}
