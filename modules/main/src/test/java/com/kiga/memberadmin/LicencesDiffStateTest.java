package com.kiga.memberadmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.kiga.memberadmin.service.LicencesDiffState;
import com.kiga.memberadmin.web.request.RequestUpdateSubUsers;
import com.kiga.memberadmin.web.request.Subuser;
import com.kiga.memberadmin.web.response.LicenceDataResponse;
import com.kiga.memberadmin.web.response.LicenceResponse;
import com.kiga.memberadmin.web.response.UserDataLicenceResponse;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LicencesDiffStateTest {

  @Test
  public void testDetectResetGroupCountChange() {
    LicenceResponse current = new LicenceResponse();
    current.setMaxPossibleSubUsersAfterTransition(5);
    current.setLicences(7);
    RequestUpdateSubUsers newState = new RequestUpdateSubUsers();
    newState.setMaxUsers(7);
    LicencesDiffState service = new LicencesDiffState(current, newState);
    assertTrue(service.shouldResetTransition());
  }

  @Test
  public void testDetectNoResetGroupCountChange() {
    LicenceResponse current = new LicenceResponse();
    current.setMaxPossibleSubUsersAfterTransition(5);
    current.setLicences(7);
    RequestUpdateSubUsers newState = new RequestUpdateSubUsers();
    newState.setMaxUsers(3);
    LicencesDiffState service = new LicencesDiffState(current, newState);
    assertFalse(service.shouldResetTransition());
  }

  @Test
  public void testDetectNoResetGroupCountChangeWhenNoTransition() {
    LicenceResponse current = new LicenceResponse();
    current.setLicences(5);
    RequestUpdateSubUsers newState = new RequestUpdateSubUsers();
    newState.setMaxUsers(5);
    LicencesDiffState service = new LicencesDiffState(current, newState);
    assertFalse(service.shouldResetTransition());
  }

  @Test
  public void testDetectRequiresTransition() {
    LicenceResponse current = new LicenceResponse();
    current.setLicences(7);
    current.setMaxPossibleSubUsersAfterTransition(7);
    RequestUpdateSubUsers newState = new RequestUpdateSubUsers();
    newState.setMaxUsers(3);
    LicencesDiffState service = new LicencesDiffState(current, newState);
    assertTrue(service.shouldCreateTransition());
  }

  @Test
  public void testDetectDoesNotRequireTransition() {
    LicenceResponse current = new LicenceResponse();
    current.setLicences(7);
    current.setMaxPossibleSubUsersAfterTransition(3);
    RequestUpdateSubUsers newState = new RequestUpdateSubUsers();
    newState.setMaxUsers(3);
    LicencesDiffState service = new LicencesDiffState(current, newState);
    assertFalse(service.shouldCreateTransition());
  }

  @Test
  public void testDetectDoesNotRequireTransitionDueToIncrease() {
    LicenceResponse current = new LicenceResponse();
    current.setLicences(3);
    RequestUpdateSubUsers newState = new RequestUpdateSubUsers();
    newState.setMaxUsers(5);
    LicencesDiffState service = new LicencesDiffState(current, newState);
    assertFalse(service.shouldCreateTransition());
  }

  @Test
  public void testDetectGroupCountIncrease() {
    LicenceResponse current = new LicenceResponse();
    current.setLicences(1);
    RequestUpdateSubUsers newState = new RequestUpdateSubUsers();
    newState.setMaxUsers(3);
    LicencesDiffState service = new LicencesDiffState(current, newState);
    assertEquals(2, service.getAdditionalLicensesNum());
  }

  @Test
  public void testDetectNoGroupCountIncrease() {
    LicenceResponse current = new LicenceResponse();
    current.setLicences(4);
    RequestUpdateSubUsers newState = new RequestUpdateSubUsers();
    newState.setMaxUsers(3);
    LicencesDiffState service = new LicencesDiffState(current, newState);
    assertEquals(0, service.getAdditionalLicensesNum());
  }

  @Test
  public void testRetrieveUsersToCreateAllUsers() {
    LicenceResponse current = new LicenceResponse();
    current.setLicences(4);
    List<UserDataLicenceResponse> currents = Arrays.asList(
      new UserDataLicenceResponse(1, false, "user1@test.at"),
      new UserDataLicenceResponse(2, false, "user2@test.at")
    );
    List<Subuser> creates = Arrays.asList(
      new Subuser(false, 3, "user3@test.at", ""),
      new Subuser(false, 4, "user4@test.at", "")
    );

    current.setLicenceData(new LicenceDataResponse(currents, 4));
    RequestUpdateSubUsers newState = new RequestUpdateSubUsers(creates,
      new ArrayList<Subuser>(), 4);
    LicencesDiffState service = new LicencesDiffState(current, newState);
    assertNotNull(service.getUsersToCreate());
    assertEquals(
      Arrays.asList("user3@test.at", "user4@test.at"),
      service.getUsersToCreate().stream().map(v -> v.getEmail())
        .collect(Collectors.toList())
    );
  }

  @Test
  public void testRetrieveUsersToCreateSomeUsersAlreadyExist() {
    LicenceResponse current = new LicenceResponse();
    current.setLicences(4);
    List<UserDataLicenceResponse> currents = Arrays.asList(
      new UserDataLicenceResponse(1, false, "user1@test.at"),
      new UserDataLicenceResponse(2, false, "user2@test.at")
    );
    List<Subuser> creates = Arrays.asList(
      new Subuser(false, 3, "user1@test.at", ""),
      new Subuser(false, 4, "user4@test.at", "")
    );

    current.setLicenceData(new LicenceDataResponse(currents, 4));
    RequestUpdateSubUsers newState = new RequestUpdateSubUsers(creates,
      new ArrayList<Subuser>(), 4);
    LicencesDiffState service = new LicencesDiffState(current, newState);
    assertNotNull(service.getUsersToCreate());
    assertEquals(
      Arrays.asList("user4@test.at"),
      service.getUsersToCreate().stream().map(v -> v.getEmail())
        .collect(Collectors.toList())
    );
  }

  @Test
  public void testRetrieveUsersToRemove() {
    LicenceResponse current = new LicenceResponse();
    current.setLicences(4);
    List<UserDataLicenceResponse> currents = Arrays.asList(
      new UserDataLicenceResponse(1, false, "user1@test.at"),
      new UserDataLicenceResponse(2, false, "user2@test.at"),
      new UserDataLicenceResponse(3, false, "user3@test.at")
    );
    List<Subuser> removes = Arrays.asList(
      new Subuser(false, 3, "user3@test.at", ""),
      new Subuser(false, 4, "user4@test.at", "")
    );

    current.setLicenceData(new LicenceDataResponse(currents, 4));
    RequestUpdateSubUsers newState = new RequestUpdateSubUsers(
      new ArrayList<Subuser>(), removes, 4);
    LicencesDiffState service = new LicencesDiffState(current, newState);
    assertNotNull(service.getUsersToRemove());
    assertEquals(
      Arrays.asList("user3@test.at"),
      service.getUsersToRemove().stream().map(v -> v.getEmail())
        .collect(Collectors.toList())
    );
  }

  @Test
  public void testRetrieveUsersWithChangedMarkedForDeletionStatus() {
    LicenceResponse current = new LicenceResponse();
    current.setLicences(4);
    List<UserDataLicenceResponse> currents = Arrays.asList(
      new UserDataLicenceResponse(1, true, "user1@test.at"),
      new UserDataLicenceResponse(2, false, "user2@test.at"),
      new UserDataLicenceResponse(3, false, "user3@test.at"),
      new UserDataLicenceResponse(4, true, "user4@test.at")
    );
    List<Subuser> changes = Arrays.asList(
      new Subuser(false, 1, "user1@test.at", ""),
      new Subuser(false, 2, "user2@test.at", ""),
      new Subuser(true, 3, "user3@test.at", ""),
      new Subuser(true, 4, "user4@test.at", "")
    );

    current.setLicenceData(new LicenceDataResponse(currents, 4));
    RequestUpdateSubUsers newState = new RequestUpdateSubUsers(changes,
      new ArrayList<Subuser>(), 4);
    LicencesDiffState service = new LicencesDiffState(current, newState);
    Map<Subuser, Boolean> result = service.getUsersToChangeDeltionMark();
    assertEquals(2, result.size());
    assertFalse(result.get(changes.get(0)));
    assertTrue(result.get(changes.get(2)));
  }
}
