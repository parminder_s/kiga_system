package com.kiga.memberadmin;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.crm.messages.Address;
import com.kiga.crm.messages.response.Invoice;
import com.kiga.crm.messages.response.ResponseGetCustomerData;
import com.kiga.crm.service.CrmConverter;
import com.kiga.crm.web.response.InvoiceResponse;
import com.kiga.memberadmin.domain.AddressType;
import com.kiga.memberadmin.service.AccountService;
import com.kiga.memberadmin.web.request.UpdateAddressRequest;
import com.kiga.memberadmin.web.response.InvoiceDetailResponse;
import com.kiga.memberadmin.web.response.MemberAdminOverviewResponse;
import com.kiga.security.domain.Member;
import com.kiga.shop.service.ShopInvoiceService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by peter on 25.01.17.
 */
public class AccountServiceTest {

  @Before
  public void reinit() throws ParseException {
    MemberAdminTestData.reinit();
  }

  @Test
  public void account() throws Exception {

    ShopInvoiceService shopInvoiceService = mock(ShopInvoiceService.class);

    when(shopInvoiceService.getInvoicesForCustomer(any()))
      .thenReturn(MemberAdminTestData.shopInvoiceList);
    CrmConverter crmConverter = MemberAdminTestData.getCrmConverterMock();
    AccountService accountService = new AccountService(crmConverter, shopInvoiceService, null);
    Member member = new Member();
    InvoiceResponse invoiceResponse = accountService.invoicesShop(member);

    InvoiceDetailResponse shopInvoice = invoiceResponse.getInvoiceList().stream()
      .filter(invoiceDetailResponse -> invoiceDetailResponse.getType() == "shop")
      .collect(Collectors.toList()).get(0);

    /*
    InvoiceDetailResponse firstAboInvoice = invoiceResponse.getInvoiceList().stream()
      .filter(invoiceDetailResponse -> invoiceDetailResponse.getInvoiceNr() == "AT0123")
      .collect(Collectors.toList()).get(0);

    Assert.assertEquals("number of invoices", 3, invoiceResponse.getInvoiceList().size());

    //Shop Invoice
    Assert.assertEquals("shop invoice amount", new Double(99.90), shopInvoice.getAmount());
    Assert.assertEquals("shop invoice currency", "EUR", shopInvoice.getCurrency());
    Assert.assertEquals("shop invoice date", MemberAdminTestData.formatter.parse("2017-02-08"),
      shopInvoice.getDate());
    Assert.assertEquals("shop invoice invoiceNumber", "12345", shopInvoice.getInvoiceNr());
    Assert.assertEquals("shop invoice pdfUrl", "https://shopinvoice", shopInvoice.getPdfUrl());
    Assert.assertEquals("shop invoice status", "paid", shopInvoice.getStatus());

    //Abo Invoice
    Assert.assertEquals("abo invoice amount", new Double(19.90), firstAboInvoice.getAmount());
    Assert.assertEquals("abo invoice currency", "EUR", firstAboInvoice.getCurrency());
    Assert.assertEquals("abo invoice date", MemberAdminTestData.formatter.parse("2017-02-10"),
      firstAboInvoice.getDate());
    Assert.assertEquals("abo invoice invoiceNumber", "AT0123", firstAboInvoice.getInvoiceNr());
    Assert.assertEquals("abo invoice pdfUrl", "https://invoice1", firstAboInvoice.getPdfUrl());
    Assert.assertEquals("abo invoice status", "paid", firstAboInvoice.getStatus());
    */
  }

  @Test
  public void updateAddressTest() throws Exception {
    Address newAddress = MemberAdminTestData.shipping;
    UpdateAddressRequest request = new UpdateAddressRequest();
    request.setShippingAddress(newAddress);
    CrmConverter crmConverter = MemberAdminTestData.getCrmConverterMock();

    AccountService accountService = new AccountService(crmConverter, null, null);
    accountService.updateAddress(request);
    verify(crmConverter).customerSetAddress(eq(AddressType.SHIPPING), eq(newAddress), any());
    verify(crmConverter).customerRemoveBillingAddress();
  }

  @Test
  public void updateShippingAndBillingAddress() throws Exception {
    final Address newAddress = MemberAdminTestData.createAddress();
    final Address newBillingAddress = MemberAdminTestData.createAddress();
    UpdateAddressRequest request = new UpdateAddressRequest();
    request.setShippingAddress(newAddress);
    request.setBillingAddress(newBillingAddress);
    CrmConverter crmConverter = MemberAdminTestData.getCrmConverterMock();

    AccountService accountService = new AccountService(crmConverter, null, null);
    accountService.updateAddress(request);
    verify(crmConverter).customerSetAddress(eq(AddressType.SHIPPING), eq(newAddress), any());
    verify(crmConverter).customerSetAddress(eq(AddressType.BILLING), eq(newBillingAddress), any());
  }

  @Test
  public void accountTest() throws Exception {
    ResponseGetCustomerData responseGetCustomerData = MemberAdminTestData.responseGetCustomerData;
    CrmConverter crmConverter = MemberAdminTestData.getCrmConverterMock();
    when(crmConverter.customerGetData(0)).thenReturn(responseGetCustomerData);
    List<Invoice> invoiceList = Collections.singletonList(Invoice.builder()
      .status("paid").invoiceNr("12345").url("urlToInvoice").build());
    when(crmConverter.customerGetInvoice()).thenReturn(invoiceList);
    ShopInvoiceService shopInvoiceService = mock(ShopInvoiceService.class);
    com.kiga.shop.web.responses.InvoicesResponse invoiceResponse = new com.kiga.shop.web.responses
      .InvoicesResponse("12345", Instant.now(), "url", BigDecimal.ONE, "EUR", true);
    List<com.kiga.shop.web.responses.InvoicesResponse> invoiceResponseList = Collections
      .singletonList(invoiceResponse);
    when(shopInvoiceService.getInvoicesForCustomer(any())).thenReturn(invoiceResponseList);

    InvoiceRepository invoiceRepository = mock(InvoiceRepository.class);
    com.kiga.accounting.domain.Invoice shopInvoice = new com.kiga.accounting.domain.Invoice();
    shopInvoice.setSettled(true);
    when(invoiceRepository.findByInvoiceNumber(any())).thenReturn(shopInvoice);

    AccountService accountService = new AccountService(crmConverter, shopInvoiceService,
      invoiceRepository);

    MemberAdminOverviewResponse overviewResponse = accountService.account(
      MemberAdminTestData.getMember());
    verify(shopInvoiceService).getInvoicesForCustomer(any());

    Assert.assertEquals(true, overviewResponse.isShopBillOk());
    Assert.assertEquals(true, overviewResponse.isSubscriptionBillOk());
    Assert.assertEquals("12345", overviewResponse.getCurrentInvoiceNr());
    Assert.assertEquals("urlToInvoice", overviewResponse.getAboBillUrl());
  }

}
