package com.kiga.survey;

import static org.hamcrest.core.IsInstanceOf.instanceOf;

import com.kiga.shop.ShopProperties;
import com.kiga.survey.service.DefaultEmailNotifier;
import com.kiga.survey.web.service.DefaultEmailNotifierRunner;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 30.05.16.
 */
public class SurveyConfigTest {
  @Test
  public void emailNotifer() throws Exception {
    ShopProperties shopProperties = new ShopProperties();
    shopProperties.setShopEmail("shop@k.tt4.at");
    Assert.assertThat(
      new SurveyConfig().getEmailNotifier(null, null, shopProperties),
      instanceOf(DefaultEmailNotifier.class)
    );
  }

  @Test
  public void emailNotifierRunner() throws Exception {
    Assert.assertThat(
      new SurveyConfig().getEmailNotifierRunner(null, null),
      instanceOf(DefaultEmailNotifierRunner.class)
    );

  }

}
