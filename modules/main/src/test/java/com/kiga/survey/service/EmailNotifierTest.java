package com.kiga.survey.service;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.mail.Mailer;
import com.kiga.test.DummyUrlGenerator;
import com.kiga.web.service.UrlGenerator;
import org.junit.Test;

import java.util.function.UnaryOperator;
import javax.mail.internet.MimeMessage;

/** Created by rainerh on 11.05.16. */
public class EmailNotifierTest {
  @Test
  public void notifyTest() {
    MimeMessage message = mock(MimeMessage.class);
    Mailer mailer = mock(Mailer.class);
    UrlGenerator urlGenerator = new DummyUrlGenerator();

    EmailNotifier emailNotifier = new DefaultEmailNotifier(mailer, urlGenerator, "sender@k.tt4.at");
    emailNotifier.notify(1L, "someSalt", "kiga@k.tt4.at", "link=$basePath");

    verify(mailer).send(eq("survey"), eq("lapbag"), eq("1"), any(UnaryOperator.class));
  }
}
