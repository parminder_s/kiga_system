package com.kiga.survey.service;

import com.kiga.security.domain.Member;
import com.kiga.survey.domain.Questionnaire;
import com.kiga.survey.domain.Survey;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 11.05.16.
 */
public class QuestionnaireFactoryTest {
  @Test
  public void create() throws Exception {
    Survey survey = new Survey();
    survey.setUrlSegment("survey1");

    Member member = new Member();
    member.setFirstname("Rainer");
    member.setLastname("Hahnekamp");

    QuestionnaireFactory factory = new QuestionnaireFactory();
    Questionnaire questionnaire = factory.create("shop-survey-1", "5", survey, member);

    Assert.assertEquals("member", "Hahnekamp", questionnaire.getMember().getLastname());
    Assert.assertEquals("survey", "survey1", questionnaire.getSurvey().getUrlSegment());
    Assert.assertFalse(questionnaire.getIsCompleted());
    Assert.assertFalse(questionnaire.getIsNotifiedByMessage());
    Assert.assertFalse(questionnaire.getIsNotifiedByEMail());
    Assert.assertNotNull(questionnaire.getSalt());
  }
}
