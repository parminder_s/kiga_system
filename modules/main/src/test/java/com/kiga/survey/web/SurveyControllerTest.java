package com.kiga.survey.web;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.survey.domain.Questionnaire;
import com.kiga.survey.repository.QuestionnaireRepository;
import com.kiga.survey.service.EmailNotifier;
import com.kiga.survey.web.request.FindQuestionnaireRequest;
import com.kiga.survey.web.request.SaveRequest;
import com.kiga.survey.web.response.FindQuestionnaireResponse;
import com.kiga.survey.web.service.EmailNotifierRunner;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by rainerh on 29.05.16.
 */
public class SurveyControllerTest {
  @Test
  public void saveExisting() throws Exception {
    QuestionnaireRepository repository = mock(QuestionnaireRepository.class);
    Questionnaire questionnaire = new Questionnaire();
    when(repository.findOpened(5L, "salt123", false)).thenReturn(questionnaire);
    SaveRequest saveRequest = new SaveRequest();

    saveRequest.setId(5L);
    saveRequest.setSalt("salt123");
    HashMap<String, String> questionnaireData = new HashMap();
    questionnaireData.put("question", "answer");
    saveRequest.setQuestionnaireData(questionnaireData);
    saveRequest.setFinish(true);

    Assert.assertTrue(new SurveyController(repository, null).save(saveRequest));
    Assert.assertTrue(questionnaire.getIsCompleted());
    Assert.assertTrue(questionnaire.getQuestionnaireData().containsKey("question"));
    verify(repository).save(any(Questionnaire.class));
  }

  @Test
  public void saveRunning() throws Exception {
    QuestionnaireRepository repository = mock(QuestionnaireRepository.class);
    Questionnaire questionnaire = new Questionnaire();
    questionnaire.setIsCompleted(false);
    when(repository.findOpened(any(), any(), any())).thenReturn(questionnaire);
    SaveRequest saveRequest = new SaveRequest();
    saveRequest.setFinish(false);

    Assert.assertTrue(new SurveyController(repository, null).save(saveRequest));
    Assert.assertFalse(questionnaire.getIsCompleted());
  }

  @Test
  public void saveNotExisting() throws Exception {
    QuestionnaireRepository repository = mock(QuestionnaireRepository.class);
    Questionnaire questionnaire = new Questionnaire();
    SaveRequest saveRequest = new SaveRequest();
    Assert.assertFalse(new SurveyController(repository, null).save(saveRequest));
  }

  @Test
  public void runEMailNotifications() throws Exception {
    EmailNotifierRunner emailNotifierRunner = mock(EmailNotifierRunner.class);
    new SurveyController(null, emailNotifierRunner).runEMailNotifications();
    verify(emailNotifierRunner).execute();
  }

  @Test
  public void findExistingQuestionnaire() throws Exception {
    QuestionnaireRepository repository = mock(QuestionnaireRepository.class);
    Questionnaire questionnaire = new Questionnaire();
    when(repository.findOpened(5L, "salt123", false)).thenReturn(questionnaire);

    FindQuestionnaireRequest request = new FindQuestionnaireRequest();
    request.setId(5L);
    request.setSalt("salt123");

    FindQuestionnaireResponse response =
      new SurveyController(repository, null).findQuestionnaire(request);

    Assert.assertEquals("opened", response.getStatus());
    Assert.assertNotNull(response.getSurveyData());
  }

  @Test
  public void findNonExistingQuestionnaire() throws IOException {
    QuestionnaireRepository repository = mock(QuestionnaireRepository.class);
    Questionnaire questionnaire = new Questionnaire();
    FindQuestionnaireRequest request = new FindQuestionnaireRequest();

    FindQuestionnaireResponse response =
      new SurveyController(repository, null).findQuestionnaire(request);

    Assert.assertEquals("", response.getSurveyData());
    Assert.assertEquals("completed", response.getStatus());
  }

}
