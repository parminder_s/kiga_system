package com.kiga.survey.service;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.security.domain.Member;
import com.kiga.survey.domain.Questionnaire;
import com.kiga.survey.repository.QuestionnaireRepository;
import com.kiga.survey.web.service.DefaultEmailNotifierRunner;
import com.kiga.survey.web.service.EmailNotifierRunner;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rainerh on 11.05.16.
 */
public class EmailNotifierRunnerTest {
  @Test
  public void execute() throws Exception {

    Member member = new Member();
    member.setEmail("kiga@k.tt4.at");
    Questionnaire questionnaire = new Questionnaire();
    questionnaire.setId(2L);
    questionnaire.setSalt("someSalt");
    questionnaire.setMember(member);

    List<Questionnaire> questionnaires = new ArrayList<>();
    questionnaires.add(questionnaire);
    QuestionnaireRepository questionnaireRepository = mock(QuestionnaireRepository.class);

    when(questionnaireRepository.findFirst10ByIsNotifiedByEMail(false))
      .then((answer) -> questionnaires);

    EmailNotifier emailNotifier = mock(EmailNotifier.class);

    EmailNotifierRunner emailNotifierRunner =
      new DefaultEmailNotifierRunner(emailNotifier, questionnaireRepository);
    emailNotifierRunner.execute();

    verify(emailNotifier).notify(eq(2L), eq("someSalt"), eq("kiga@k.tt4.at"), anyString());
    verify(questionnaireRepository).save(questionnaire);
    Assert.assertTrue(questionnaire.getIsNotifiedByEMail());
  }

}
