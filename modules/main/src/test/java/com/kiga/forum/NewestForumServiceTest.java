package com.kiga.forum;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.forum.domain.ForumThread;
import com.kiga.forum.domain.Post;
import com.kiga.forum.service.ForumService;
import com.kiga.forum.service.NewestForumService;
import com.kiga.forum.web.responses.NewestForumResponse;
import com.kiga.main.locale.Locale;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by peter on 22.09.16.
 */
public class NewestForumServiceTest {
  @Test
  public void testNewestForum() {
    ForumThread forumThread = new ForumThread();
    forumThread.setTitle("thread");
    forumThread.setTitle("title");
    Post post = new Post();
    post.setContent("postcontent");
    List<Post> postList = new ArrayList<>();
    postList.add(post);
    forumThread.setPosts(postList);
    ForumService forumService = mock(ForumService.class);
    when(forumService.getNewest(eq("type"), eq(Locale.en))).thenReturn(forumThread);
    when(forumService.getUrl(any())).thenReturn("url");
    NewestForumService newestForumService = new NewestForumService(forumService);
    NewestForumResponse newestForumResponse = newestForumService.getNewestForum(
      "type", Locale.en);
    Assert.assertEquals("postcontent", newestForumResponse.getContent());
    Assert.assertEquals("title", newestForumResponse.getTitle());
    Assert.assertEquals("url", newestForumResponse.getUrl());
  }
}
