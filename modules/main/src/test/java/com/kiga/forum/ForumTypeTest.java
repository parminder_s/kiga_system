package com.kiga.forum;

import com.kiga.forum.domain.ForumType;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by peter on 28.07.16.
 */
public class ForumTypeTest {

  @Test
  public void testCreation() {
    ForumType idea = ForumType.valueOf("IDEA");
    ForumType activity = ForumType.valueOf("ACTIVITY");
    ForumType kiga = ForumType.valueOf("KIGA");
    ForumType job = ForumType.valueOf("JOB");

    Assert.assertEquals(ForumType.IDEA, idea);
    Assert.assertEquals(ForumType.ACTIVITY, activity);
    Assert.assertEquals(ForumType.KIGA, kiga);
    Assert.assertEquals(ForumType.JOB, job);
  }

  @Test
  public void testToString() {
    Assert.assertEquals(ForumType.IDEA.toString(), "article");
    Assert.assertEquals(ForumType.ACTIVITY.toString(), "idea");
    Assert.assertEquals(ForumType.KIGA.toString(), "kiga");
    Assert.assertEquals(ForumType.JOB.toString(), "job");
  }
}
