package com.kiga.forum;

import static org.hamcrest.core.IsInstanceOf.instanceOf;

import com.kiga.forum.service.NewestForumService;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by peter on 14.06.16.
 */
public class ForumConfigTest {
  @Test
  public void testIdeaForumServiceBean() {
    ForumConfig forumConfig = new ForumConfig();
    Assert.assertNotNull(forumConfig.ideaForumService(null, null, null, null, null, null));
  }

  @Test
  public void testNewestForumServiceBean() {
    ForumConfig forumConfig = new ForumConfig();
    Assert.assertThat(forumConfig.newestForumService(null), instanceOf(NewestForumService.class));
  }
}
