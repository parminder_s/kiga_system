package com.kiga.forum.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.forum.domain.Forum;
import com.kiga.forum.domain.ForumThread;
import com.kiga.forum.repository.ForumRepository;
import com.kiga.forum.repository.ForumThreadRepository;
import com.kiga.main.locale.Locale;
import org.junit.Test;

public class ForumServiceTest {
  @Test
  public void testNewest() {
    ForumRepository forumRepository = mock(ForumRepository.class);
    ForumThreadRepository threadRepository = mock(ForumThreadRepository.class);
    Forum forum = new Forum();
    ForumThread thread = new ForumThread();
    when(forumRepository.findByForumCodeAndLocale("foo", Locale.de)).thenReturn(forum);
    when(threadRepository.findTopByForumOrderByRealLastEditedDesc(forum)).thenReturn(thread);
    ForumService forumService = new ForumService(threadRepository, forumRepository, null);

    assertEquals(thread, forumService.getNewest("foo", Locale.de));
  }

  @Test
  public void testUrl() {
    SiteTreeUrlGenerator generator = mock(SiteTreeUrlGenerator.class);
    Forum forum = new Forum();
    ForumThread thread = new ForumThread();
    thread.setForum(forum);
    thread.setId(5L);
    when(generator.getRelativeUrl(forum)).thenReturn("http://forum");
    ForumService forumService = new ForumService(null, null, generator);

    assertEquals("http://forum/show/5", forumService.getUrl(thread));
  }
}
