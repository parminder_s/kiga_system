package com.kiga.forum.idea;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.forum.web.requests.AddPostRequest;
import com.kiga.forum.web.responses.ForumThreadResponse;
import com.kiga.security.services.SecurityService;
import org.junit.Assert;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by peter on 14.06.16.
 */
public class IdeaForumControllerTest {
  @Test
  public void getForumThreadTest() {
    ForumThreadResponse forumThreadResponse = new ForumThreadResponse();
    forumThreadResponse.setRequests(2L);
    forumThreadResponse.setTitle("title");
    forumThreadResponse.setPosts(null);

    IdeaForumService ideaForumService = mock(IdeaForumService.class);
    SecurityService securityService = mock(SecurityService.class);
    when(ideaForumService.getThread(eq(1L))).thenReturn(forumThreadResponse);
    IdeaForumController ideaForumController = new IdeaForumController(ideaForumService,
      securityService);
    ForumThreadResponse response = ideaForumController.getThread(1L);
    verify(ideaForumService).getThread(eq(1L));
    Assert.assertEquals(response.getTitle(), "title");
    Assert.assertNull(response.getPosts());
    Assert.assertEquals(response.getRequests(), 2);
  }

  @Test
  public void addPostTest() {
    IdeaForumService ideaForumService = mock(IdeaForumService.class);
    SecurityService securityService = mock(SecurityService.class);
    HttpServletRequest request = mock(HttpServletRequest.class);
    IdeaForumController ideaForumController = new IdeaForumController(ideaForumService,
      securityService);
    AddPostRequest addPostRequest = new AddPostRequest();
    addPostRequest.setContent("content");
    // addPostRequest.setMemberId(1L);
    // ideaForumController.addPost(addPostRequest, 1L, request);
    // verify(ideaForumService).addPost(eq(1L), eq(1L), eq("content"));
  }
}
