package com.kiga.forum.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.security.domain.Member;
import org.junit.Assert;
import org.junit.Test;

import javax.persistence.EntityNotFoundException;

/**
 * Created by rainerh on 04.12.16.
 */
public class ProfileServiceTest {
  @Test
  public void testNullMemberNickname() {
    assertEquals("anonymous", new ProfileService(null).getNickname(null));
  }

  @Test
  public void testNullNickname() {
    assertEquals("anonymous", new ProfileService(null).getNickname(new Member()));
  }

  @Test
  public void testEmptyNickname() {
    Member member = new Member();
    member.setNickname("");
    assertEquals("anonymous", new ProfileService(null).getNickname(member));
  }

  @Test
  public void testNickname() {
    Member member = new Member();
    member.setNickname("foo");
    assertEquals("foo", new ProfileService(null).getNickname(member));
  }

  @Test
  public void testS3Avatar() throws Exception {
    Member member = new Member();
    S3Image s3Image = new S3Image();
    member.setS3Avatar(s3Image);
    S3ImageResizeService resizeService = mock(S3ImageResizeService.class);
    when(resizeService.getResizedImageUrl(eq(s3Image), any())).thenReturn("aUrl");
    assertEquals("aUrl", new ProfileService(resizeService).getUrl(member));
  }

  @Test
  public void testS3AvatarWithCatch() throws Exception {
    Member member = new Member();
    S3Image s3Image = new S3Image();
    member.setS3Avatar(s3Image);
    S3ImageResizeService resizeService = mock(S3ImageResizeService.class);
    when(resizeService.getResizedImageUrl(eq(s3Image), any()))
      .thenThrow(EntityNotFoundException.class);
    assertEquals("", new ProfileService(resizeService).getUrl(member));
  }

  @Test
  public void testS3AvatarWithNullMember() {
    new ProfileService(null).getUrl(null);
  }
}
