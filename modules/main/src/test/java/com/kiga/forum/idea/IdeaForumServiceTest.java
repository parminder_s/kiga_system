package com.kiga.forum.idea;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive;
import com.kiga.forum.domain.Forum;
import com.kiga.forum.domain.ForumThread;
import com.kiga.forum.domain.Post;
import com.kiga.forum.repository.ForumRepository;
import com.kiga.forum.repository.ForumThreadRepository;
import com.kiga.forum.repository.PostRepository;
import com.kiga.forum.service.ProfileService;
import com.kiga.forum.web.responses.ForumThreadResponse;
import com.kiga.main.locale.Locale;
import com.kiga.s3.domain.S3Image;
import com.kiga.security.domain.Member;
import com.kiga.security.repository.MemberRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by peter on 14.06.16.
 */
public class IdeaForumServiceTest {
  private Logger logger = LoggerFactory.getLogger(getClass());

  private Forum forumArticle;
  private Forum forumKiga;

  /**
   * setting up forum instances.
   */
  @Before
  public void setup() {
    forumArticle = new Forum();
    forumArticle.setForumCode("article");

    forumKiga = new Forum();
    forumKiga.setForumCode("kiga");
  }

  @Test
  public void testConstructor() {
    Assert.assertNotNull(new IdeaForumService(null, null, null, null, null, null));
  }

  //test with normal input
  @Test
  public void testGetThread() {
    Member author1 = new Member();
    S3Image s3Avater1 = new S3Image();
    s3Avater1.setUrl("url1");
    author1.setS3Avatar(s3Avater1);
    author1.setNickname("nick1");
    Post post1 = new Post();
    post1.setAuthor(author1);
    post1.setContent("content1");
    post1.setCreated(new Date(
      Instant.parse("2014-12-03T10:15:30.00Z").toEpochMilli()));

    Member author2 = new Member();
    S3Image s3Avater2 = new S3Image();
    s3Avater2.setUrl("url2");
    author2.setS3Avatar(s3Avater2);
    author2.setNickname("nick2");
    Post post2 = new Post();
    post2.setAuthor(author2);
    post2.setContent("content2");
    post2.setCreated(new Date(
      Instant.parse("2015-12-03T10:17:30.00Z").toEpochMilli()));

    List<Post> postListThread = new ArrayList<>();
    postListThread.add(post1);
    postListThread.add(post2);

    List<Post> postListAuthor = new ArrayList<>();

    ForumThreadResponse forumThreadResponse = runGetThreadTest(1L, "title", 2L,
      postListThread, postListAuthor, forumKiga);

    Assert.assertEquals("content1", forumThreadResponse.getPosts().get(0).getContent());
    //logger.info("nickname:" + forumThreadResponse.getPosts().get(0).getAuthorName());
    Assert.assertEquals("nick1",
      forumThreadResponse.getPosts().get(0).getAuthorName());
    Assert.assertEquals(post1.getCreated().toString(),
      forumThreadResponse.getPosts().get(0).getDateWritten().toString());
    Assert.assertEquals( post2.getCreated().toString(),
      forumThreadResponse.getPosts().get(1).getDateWritten().toString());
  }

  //test remove oldert entry if it is from KigaPOrtal
  @Test
  public void testRemoveFirstEntry() {
    Member author1 = new Member();
    S3Image s3Avater1 = null;
    Post post1 = new Post();
    post1.setAuthor(author1);
    post1.setContent("content1");
    post1.setCreated(new Date(
      Instant.parse("2015-12-03T10:15:30.00Z").toEpochMilli()));

    Member author2 = new Member();
    author2.setNickname("somebody");
    S3Image s3Avater2 = null;
    Post post2 = new Post();
    post2.setAuthor(author2);
    post2.setContent("content2");
    post2.setCreated(new Date(
      Instant.parse("2015-12-03T10:17:30.00Z").toEpochMilli()));

    List<Post> postListThread = new ArrayList<>();
    postListThread.add(post1);
    postListThread.add(post2);

    List<Post> postListAuthor = new ArrayList<>();

    ForumThreadResponse forumThreadResponse = runGetThreadTest(1L, "title", 2L,
      postListThread, postListAuthor, forumArticle);

    Assert.assertEquals(1, forumThreadResponse.getPosts().size());
    Assert.assertEquals(forumThreadResponse.getPosts().get(0).getContent(), "content2");
  }

  //test remove oldert entry if it is from KigaPOrtal
  @Test
  public void testRemoveNotFirstEntry() {
    Member author1 = new Member();
    S3Image s3Avater1 = null;
    Post post1 = new Post();
    post1.setAuthor(author1);
    post1.setContent("content1");
    post1.setCreated(new Date(
      Instant.parse("2015-12-03T10:17:30.00Z").toEpochMilli()));

    Member author2 = new Member();
    author2.setNickname("KigaPortal");
    S3Image s3Avater2 = null;
    Post post2 = new Post();
    post2.setAuthor(author2);
    post2.setContent("content2");
    post2.setCreated(new Date(
      Instant.parse("2015-12-03T10:18:31.00Z").toEpochMilli()));

    List<Post> postListThread = new ArrayList<>();
    postListThread.add(post1);
    postListThread.add(post2);

    List<Post> postListAuthor = new ArrayList<>();

    ForumThreadResponse forumThreadResponse = runGetThreadTest(1L, "title", 2L,
      postListThread, postListAuthor, forumKiga);

    Assert.assertEquals(2, forumThreadResponse.getPosts().size());
    Assert.assertEquals(forumThreadResponse.getPosts().get(0).getContent(), "content1");
  }

  private ForumThreadResponse runGetThreadTest(Long kigaPageId,
                                               String title,
                                               Long numViews,
                                               List<Post> postListThread,
                                               List<Post> postListAuthor,
                                               Forum forum) {
    ForumThread forumThread = new ForumThread();
    forumThread.setId(1L);
    forumThread.setPosts(postListThread);
    forumThread.setNumViews(numViews);
    forumThread.setTitle(title);

    forumThread.setSticky(true);
    forumThread.setReadOnly(false);
    forumThread.setGlobalSticky(true);
    forumThread.setKigaPage(null);
    forumThread.setForum(forum);


    ForumThreadRepository forumThreadRepository = mock(ForumThreadRepository.class);
    when(forumThreadRepository.findByKigaPageId(eq(kigaPageId))).thenReturn(forumThread);

    PostRepository postRepository = mock(PostRepository.class);
    when(postRepository.findByAuthor(any())).thenReturn(postListAuthor);


    Assert.assertNull(forumThread.getKigaPage());
    Assert.assertTrue(forumThread.isGlobalSticky());
    Assert.assertFalse(forumThread.isReadOnly());
    Assert.assertTrue(forumThread.isSticky());

    ForumRepository forumRepository = mock(ForumRepository.class);
    ProfileService profileService = mock(ProfileService.class);
    when(profileService.getNickname(any())).thenCallRealMethod();
    when(profileService.getUrl(any())).thenReturn("");

    KigaIdeaRepositoryLive kigaIdeaRepository = mock(KigaIdeaRepositoryLive.class);
    IdeaForumService ideaForumService = new IdeaForumService(
      forumRepository, forumThreadRepository, kigaIdeaRepository, postRepository,
      profileService, null);
    return ideaForumService.getThread(kigaPageId);
  }

  @Test
  public void addPostTest() {
    Member author = new Member();
    author.setNickname("nick");
    author.setId(1L);
    Post post = testAddPost(author);
    Assert.assertEquals(post.getAnonNickname(), "nick");
    Assert.assertEquals(post.getContent(), "msg");
    Assert.assertEquals(post.getAuthor().getId(), new Long(1));
  }

  @Test
  public void addPostTestNull() {
    Member author = new Member();
    author.setNickname(null);
    Post post = testAddPost(author);
    Assert.assertEquals(post.getAnonNickname(), "anonymous");
    Assert.assertEquals(post.getContent(), "msg");
  }

  private Post testAddPost(Member author) {
    ForumThread forumThread = new ForumThread();
    forumThread.setId(1L);
    ForumThreadRepository forumThreadRepository = mock(ForumThreadRepository.class);
    when(forumThreadRepository.findByKigaPageId(any())).thenReturn(forumThread);

    ForumRepository forumRepository = mock(ForumRepository.class);
    KigaIdeaRepositoryLive kigaIdeaRepository = mock(KigaIdeaRepositoryLive.class);

    PostRepository postRepository = mock(PostRepository.class);
    MemberRepository memberRepository = mock(MemberRepository.class);
    when(memberRepository.findOne(any())).thenReturn(author);
    IdeaForumService ideaForumService = new IdeaForumService(
      forumRepository, forumThreadRepository, kigaIdeaRepository, postRepository, null, null);

    ideaForumService.addPost(1L, author, "msg", Locale.en);

    ArgumentCaptor<Post> captor = ArgumentCaptor.forClass(Post.class);
    verify(postRepository).save(captor.capture());
    return captor.getValue();
  }

}
