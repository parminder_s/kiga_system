package com.kiga.upload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.File;

/**
 * Created by bbs on 8/6/17.
 */
public class FileTypeTest {
  @Test
  public void testIsSvg() {
    assertTrue(FileType.isSvg("image/svg+xml"));

    assertFalse(FileType.isSvg("application/vnd.ms-powerpoint"));
    assertFalse(
      FileType.isSvg("application/vnd.openxmlformats-officedocument.presentationml.presentation"));
    assertFalse(FileType.isSvg("application/vnd.oasis.opendocument.presentation"));

    assertFalse(FileType.isSvg("image/png"));

    assertFalse(FileType.isSvg("application/pdf"));

    assertFalse(FileType.isSvg("image/jpeg"));
    assertFalse(FileType.isSvg("image/pjpeg"));

    assertFalse(FileType.isSvg("audio/mp4"));
    assertFalse(FileType.isSvg("audio/x-aac"));
    assertFalse(FileType.isSvg("audio/aac"));
    assertFalse(FileType.isSvg("audio/aacp"));
    assertFalse(FileType.isSvg("audio/mpeg3"));
    assertFalse(FileType.isSvg("audio/x-mpeg-3"));
    assertFalse(FileType.isSvg("video/mpeg"));
    assertFalse(FileType.isSvg("video/x-mpeg"));
    assertFalse(FileType.isSvg("audio/ogg"));
    assertFalse(FileType.isSvg("audio/wav"));
    assertFalse(FileType.isSvg("audio/x-wav"));

    assertFalse(FileType.isSvg("application/vnd.ms-excel"));
    assertFalse(
      FileType.isSvg("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    assertFalse(FileType.isSvg("application/vnd.oasis.opendocument.spreadsheet"));

    assertFalse(FileType.isSvg("image/gif"));

    assertFalse(FileType.isSvg("application/x-troff-msvideo"));
    assertFalse(FileType.isSvg("video/avi"));
    assertFalse(FileType.isSvg("video/msvideo"));
    assertFalse(FileType.isSvg("video/x-msvideo"));
    assertFalse(FileType.isSvg("video/mp4"));

    assertFalse(FileType.isSvg("application/msword"));
    assertFalse(
      FileType.isSvg("application/vnd.openxmlformats-officedocument.wordprocessingml.document"));
    assertFalse(FileType.isSvg("application/vnd.oasis.opendocument.text"));
    assertFalse(FileType.isSvg("application/x-tika-msoffice"));
    assertFalse(FileType.isSvg("application/zip"));
  }

  @Test
  public void testIsGif() {
    assertFalse(FileType.isGif("image/svg+xml"));

    assertFalse(FileType.isGif("application/vnd.ms-powerpoint"));
    assertFalse(
      FileType.isGif("application/vnd.openxmlformats-officedocument.presentationml.presentation"));
    assertFalse(FileType.isGif("application/vnd.oasis.opendocument.presentation"));

    assertFalse(FileType.isGif("image/png"));

    assertFalse(FileType.isGif("application/pdf"));

    assertFalse(FileType.isGif("image/jpeg"));
    assertFalse(FileType.isGif("image/pjpeg"));

    assertFalse(FileType.isGif("audio/mp4"));
    assertFalse(FileType.isGif("audio/x-aac"));
    assertFalse(FileType.isGif("audio/aac"));
    assertFalse(FileType.isGif("audio/aacp"));
    assertFalse(FileType.isGif("audio/mpeg3"));
    assertFalse(FileType.isGif("audio/x-mpeg-3"));
    assertFalse(FileType.isGif("video/mpeg"));
    assertFalse(FileType.isGif("video/x-mpeg"));
    assertFalse(FileType.isGif("audio/ogg"));
    assertFalse(FileType.isGif("audio/wav"));
    assertFalse(FileType.isGif("audio/x-wav"));

    assertFalse(FileType.isGif("application/vnd.ms-excel"));
    assertFalse(
      FileType.isGif("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    assertFalse(FileType.isGif("application/vnd.oasis.opendocument.spreadsheet"));

    assertTrue(FileType.isGif("image/gif"));

    assertFalse(FileType.isGif("application/x-troff-msvideo"));
    assertFalse(FileType.isGif("video/avi"));
    assertFalse(FileType.isGif("video/msvideo"));
    assertFalse(FileType.isGif("video/x-msvideo"));
    assertFalse(FileType.isGif("video/mp4"));

    assertFalse(FileType.isGif("application/msword"));
    assertFalse(
      FileType.isGif("application/vnd.openxmlformats-officedocument.wordprocessingml.document"));
    assertFalse(FileType.isGif("application/vnd.oasis.opendocument.text"));
    assertFalse(FileType.isGif("application/x-tika-msoffice"));
    assertFalse(FileType.isGif("application/zip"));
  }

  @Test
  public void testIsPng() {
    assertFalse(FileType.isPng("image/svg+xml"));

    assertFalse(FileType.isPng("application/vnd.ms-powerpoint"));
    assertFalse(
      FileType.isPng("application/vnd.openxmlformats-officedocument.presentationml.presentation"));
    assertFalse(FileType.isPng("application/vnd.oasis.opendocument.presentation"));

    assertTrue(FileType.isPng("image/png"));

    assertFalse(FileType.isPng("application/pdf"));

    assertFalse(FileType.isPng("image/jpeg"));
    assertFalse(FileType.isPng("image/pjpeg"));

    assertFalse(FileType.isPng("audio/mp4"));
    assertFalse(FileType.isPng("audio/x-aac"));
    assertFalse(FileType.isPng("audio/aac"));
    assertFalse(FileType.isPng("audio/aacp"));
    assertFalse(FileType.isPng("audio/mpeg3"));
    assertFalse(FileType.isPng("audio/x-mpeg-3"));
    assertFalse(FileType.isPng("video/mpeg"));
    assertFalse(FileType.isPng("video/x-mpeg"));
    assertFalse(FileType.isPng("audio/ogg"));
    assertFalse(FileType.isPng("audio/wav"));
    assertFalse(FileType.isPng("audio/x-wav"));

    assertFalse(FileType.isPng("application/vnd.ms-excel"));
    assertFalse(
      FileType.isPng("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    assertFalse(FileType.isPng("application/vnd.oasis.opendocument.spreadsheet"));

    assertFalse(FileType.isPng("image/gif"));

    assertFalse(FileType.isPng("application/x-troff-msvideo"));
    assertFalse(FileType.isPng("video/avi"));
    assertFalse(FileType.isPng("video/msvideo"));
    assertFalse(FileType.isPng("video/x-msvideo"));
    assertFalse(FileType.isPng("video/mp4"));

    assertFalse(FileType.isPng("application/msword"));
    assertFalse(
      FileType.isPng("application/vnd.openxmlformats-officedocument.wordprocessingml.document"));
    assertFalse(FileType.isPng("application/vnd.oasis.opendocument.text"));
    assertFalse(FileType.isPng("application/x-tika-msoffice"));
    assertFalse(FileType.isPng("application/zip"));
  }


  @Test
  public void testIsJpg() {
    assertFalse(FileType.isJpg("image/svg+xml"));

    assertFalse(FileType.isJpg("application/vnd.ms-powerpoint"));
    assertFalse(
      FileType.isJpg("application/vnd.openxmlformats-officedocument.presentationml.presentation"));
    assertFalse(FileType.isJpg("application/vnd.oasis.opendocument.presentation"));

    assertFalse(FileType.isJpg("image/png"));

    assertFalse(FileType.isJpg("application/pdf"));

    assertTrue(FileType.isJpg("image/jpeg"));
    assertTrue(FileType.isJpg("image/pjpeg"));

    assertFalse(FileType.isJpg("audio/mp4"));
    assertFalse(FileType.isJpg("audio/x-aac"));
    assertFalse(FileType.isJpg("audio/aac"));
    assertFalse(FileType.isJpg("audio/aacp"));
    assertFalse(FileType.isJpg("audio/mpeg3"));
    assertFalse(FileType.isJpg("audio/x-mpeg-3"));
    assertFalse(FileType.isJpg("video/mpeg"));
    assertFalse(FileType.isJpg("video/x-mpeg"));
    assertFalse(FileType.isJpg("audio/ogg"));
    assertFalse(FileType.isJpg("audio/wav"));
    assertFalse(FileType.isJpg("audio/x-wav"));

    assertFalse(FileType.isJpg("application/vnd.ms-excel"));
    assertFalse(
      FileType.isJpg("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    assertFalse(FileType.isJpg("application/vnd.oasis.opendocument.spreadsheet"));

    assertFalse(FileType.isJpg("image/gif"));

    assertFalse(FileType.isJpg("application/x-troff-msvideo"));
    assertFalse(FileType.isJpg("video/avi"));
    assertFalse(FileType.isJpg("video/msvideo"));
    assertFalse(FileType.isJpg("video/x-msvideo"));
    assertFalse(FileType.isJpg("video/mp4"));

    assertFalse(FileType.isJpg("application/msword"));
    assertFalse(
      FileType.isJpg("application/vnd.openxmlformats-officedocument.wordprocessingml.document"));
    assertFalse(FileType.isJpg("application/vnd.oasis.opendocument.text"));
    assertFalse(FileType.isJpg("application/x-tika-msoffice"));
    assertFalse(FileType.isJpg("application/zip"));
  }


  @Test
  public void testIsPdf() {
    assertFalse(FileType.isPdf("image/svg+xml"));

    assertFalse(FileType.isPdf("application/vnd.ms-powerpoint"));
    assertFalse(
      FileType.isPdf("application/vnd.openxmlformats-officedocument.presentationml.presentation"));
    assertFalse(FileType.isPdf("application/vnd.oasis.opendocument.presentation"));

    assertFalse(FileType.isPdf("image/png"));

    assertTrue(FileType.isPdf("application/pdf"));

    assertFalse(FileType.isPdf("image/jpeg"));
    assertFalse(FileType.isPdf("image/pjpeg"));

    assertFalse(FileType.isPdf("audio/mp4"));
    assertFalse(FileType.isPdf("audio/x-aac"));
    assertFalse(FileType.isPdf("audio/aac"));
    assertFalse(FileType.isPdf("audio/aacp"));
    assertFalse(FileType.isPdf("audio/mpeg3"));
    assertFalse(FileType.isPdf("audio/x-mpeg-3"));
    assertFalse(FileType.isPdf("video/mpeg"));
    assertFalse(FileType.isPdf("video/x-mpeg"));
    assertFalse(FileType.isPdf("audio/ogg"));
    assertFalse(FileType.isPdf("audio/wav"));
    assertFalse(FileType.isPdf("audio/x-wav"));

    assertFalse(FileType.isPdf("application/vnd.ms-excel"));
    assertFalse(
      FileType.isPdf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    assertFalse(FileType.isPdf("application/vnd.oasis.opendocument.spreadsheet"));

    assertFalse(FileType.isPdf("image/gif"));

    assertFalse(FileType.isPdf("application/x-troff-msvideo"));
    assertFalse(FileType.isPdf("video/avi"));
    assertFalse(FileType.isPdf("video/msvideo"));
    assertFalse(FileType.isPdf("video/x-msvideo"));
    assertFalse(FileType.isPdf("video/mp4"));

    assertFalse(FileType.isPdf("application/msword"));
    assertFalse(
      FileType.isPdf("application/vnd.openxmlformats-officedocument.wordprocessingml.document"));
    assertFalse(FileType.isPdf("application/vnd.oasis.opendocument.text"));
    assertFalse(FileType.isPdf("application/x-tika-msoffice"));
    assertFalse(FileType.isPdf("application/zip"));
  }

  @Test
  public void testFindFileTypeByContentType() {
    assertEquals(FileType.SVG, FileType.findFileTypeByContentType("image/svg+xml"));

    assertEquals(FileType.POWERPOINT,
      FileType.findFileTypeByContentType("application/vnd.ms-powerpoint"));
    assertEquals(FileType.POWERPOINT, FileType
      .findFileTypeByContentType(
        "application/vnd.openxmlformats-officedocument.presentationml.presentation"));
    assertEquals(FileType.POWERPOINT,
      FileType.findFileTypeByContentType("application/vnd.oasis.opendocument.presentation"));

    assertEquals(FileType.PNG, FileType.findFileTypeByContentType("image/png"));

    assertEquals(FileType.PDF, FileType.findFileTypeByContentType("application/pdf"));

    assertEquals(FileType.JPG, FileType.findFileTypeByContentType("image/jpeg"));
    assertEquals(FileType.JPG, FileType.findFileTypeByContentType("image/pjpeg"));

    assertEquals(FileType.AUDIO, FileType.findFileTypeByContentType("audio/mp4"));
    assertEquals(FileType.AUDIO, FileType.findFileTypeByContentType("audio/x-aac"));
    assertEquals(FileType.AUDIO, FileType.findFileTypeByContentType("audio/aac"));
    assertEquals(FileType.AUDIO, FileType.findFileTypeByContentType("audio/aacp"));
    assertEquals(FileType.AUDIO, FileType.findFileTypeByContentType("audio/mpeg3"));
    assertEquals(FileType.AUDIO, FileType.findFileTypeByContentType("audio/x-mpeg-3"));
    assertEquals(FileType.AUDIO, FileType.findFileTypeByContentType("audio/ogg"));
    assertEquals(FileType.AUDIO, FileType.findFileTypeByContentType("audio/wav"));
    assertEquals(FileType.AUDIO, FileType.findFileTypeByContentType("audio/x-wav"));

    assertEquals(FileType.EXCEL, FileType.findFileTypeByContentType("application/vnd.ms-excel"));
    assertEquals(FileType.EXCEL, FileType
      .findFileTypeByContentType(
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    assertEquals(FileType.EXCEL,
      FileType.findFileTypeByContentType("application/vnd.oasis.opendocument.spreadsheet"));

    assertEquals(FileType.GIF, FileType.findFileTypeByContentType("image/gif"));

    assertEquals(FileType.VIDEO, FileType.findFileTypeByContentType("video/mpeg"));
    assertEquals(FileType.VIDEO, FileType.findFileTypeByContentType("video/x-mpeg"));
    assertEquals(FileType.VIDEO, FileType.findFileTypeByContentType("application/x-troff-msvideo"));
    assertEquals(FileType.VIDEO, FileType.findFileTypeByContentType("video/avi"));
    assertEquals(FileType.VIDEO, FileType.findFileTypeByContentType("video/msvideo"));
    assertEquals(FileType.VIDEO, FileType.findFileTypeByContentType("video/x-msvideo"));
    assertEquals(FileType.VIDEO, FileType.findFileTypeByContentType("video/mp4"));

    assertEquals(FileType.WORD, FileType.findFileTypeByContentType("application/msword"));
    assertEquals(FileType.WORD, FileType
      .findFileTypeByContentType(
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document"));
    assertEquals(FileType.WORD,
      FileType.findFileTypeByContentType("application/vnd.oasis.opendocument.text"));
    assertEquals(FileType.WORD, FileType.findFileTypeByContentType("application/x-tika-msoffice"));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNoFileTypeThrowsException() {
    FileType.findFileTypeByContentType("NoSuch/ContentType");
  }

  @Test
  public void testIsBitmapImage() {
    assertFalse(FileType.isBitmapImage("image/svg+xml"));

    assertFalse(FileType.isBitmapImage("application/vnd.ms-powerpoint"));
    assertFalse(
      FileType.isBitmapImage(
        "application/vnd.openxmlformats-officedocument.presentationml.presentation"));
    assertFalse(FileType.isBitmapImage("application/vnd.oasis.opendocument.presentation"));

    assertTrue(FileType.isBitmapImage("image/png"));

    assertFalse(FileType.isBitmapImage("application/pdf"));

    assertTrue(FileType.isBitmapImage("image/jpeg"));
    assertTrue(FileType.isBitmapImage("image/pjpeg"));

    assertFalse(FileType.isBitmapImage("audio/mp4"));
    assertFalse(FileType.isBitmapImage("audio/x-aac"));
    assertFalse(FileType.isBitmapImage("audio/aac"));
    assertFalse(FileType.isBitmapImage("audio/aacp"));
    assertFalse(FileType.isBitmapImage("audio/mpeg3"));
    assertFalse(FileType.isBitmapImage("audio/x-mpeg-3"));
    assertFalse(FileType.isBitmapImage("video/mpeg"));
    assertFalse(FileType.isBitmapImage("video/x-mpeg"));
    assertFalse(FileType.isBitmapImage("audio/ogg"));
    assertFalse(FileType.isBitmapImage("audio/wav"));
    assertFalse(FileType.isBitmapImage("audio/x-wav"));

    assertFalse(FileType.isBitmapImage("application/vnd.ms-excel"));
    assertFalse(
      FileType.isBitmapImage("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    assertFalse(FileType.isBitmapImage("application/vnd.oasis.opendocument.spreadsheet"));

    assertTrue(FileType.isBitmapImage("image/gif"));

    assertFalse(FileType.isBitmapImage("application/x-troff-msvideo"));
    assertFalse(FileType.isBitmapImage("video/avi"));
    assertFalse(FileType.isBitmapImage("video/msvideo"));
    assertFalse(FileType.isBitmapImage("video/x-msvideo"));
    assertFalse(FileType.isBitmapImage("video/mp4"));

    assertFalse(FileType.isBitmapImage("application/msword"));
    assertFalse(
      FileType
        .isBitmapImage("application/vnd.openxmlformats-officedocument.wordprocessingml.document"));
    assertFalse(FileType.isBitmapImage("application/vnd.oasis.opendocument.text"));
    assertFalse(FileType.isBitmapImage("application/x-tika-msoffice"));
    assertFalse(FileType.isBitmapImage("application/zip"));


    assertFalse(FileType.isBitmapImage("image/svg+xml"));

    assertFalse(FileType.isBitmapImage("application/vnd.ms-powerpoint"));
    assertFalse(
      FileType.isBitmapImage(
        "application/vnd.openxmlformats-officedocument.presentationml.presentation"));
    assertFalse(FileType.isBitmapImage("application/vnd.oasis.opendocument.presentation"));

    assertFalse(FileType.isBitmapImage("application/pdf"));

    assertFalse(FileType.isBitmapImage("audio/mp4"));
    assertFalse(FileType.isBitmapImage("audio/x-aac"));
    assertFalse(FileType.isBitmapImage("audio/aac"));
    assertFalse(FileType.isBitmapImage("audio/aacp"));
    assertFalse(FileType.isBitmapImage("audio/mpeg3"));
    assertFalse(FileType.isBitmapImage("audio/x-mpeg-3"));
    assertFalse(FileType.isBitmapImage("video/mpeg"));
    assertFalse(FileType.isBitmapImage("video/x-mpeg"));
    assertFalse(FileType.isBitmapImage("audio/ogg"));
    assertFalse(FileType.isBitmapImage("audio/wav"));
    assertFalse(FileType.isBitmapImage("audio/x-wav"));

    assertFalse(FileType.isBitmapImage("application/vnd.ms-excel"));
    assertFalse(
      FileType.isBitmapImage("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    assertFalse(FileType.isBitmapImage("application/vnd.oasis.opendocument.spreadsheet"));

    assertFalse(FileType.isBitmapImage("application/x-troff-msvideo"));
    assertFalse(FileType.isBitmapImage("video/avi"));
    assertFalse(FileType.isBitmapImage("video/msvideo"));
    assertFalse(FileType.isBitmapImage("video/x-msvideo"));
    assertFalse(FileType.isBitmapImage("video/mp4"));

    assertFalse(FileType.isBitmapImage("application/msword"));
    assertFalse(
      FileType
        .isBitmapImage("application/vnd.openxmlformats-officedocument.wordprocessingml.document"));
    assertFalse(FileType.isBitmapImage("application/vnd.oasis.opendocument.text"));
    assertFalse(FileType.isBitmapImage("application/x-tika-msoffice"));
    assertFalse(FileType.isBitmapImage("application/zip"));


    assertFalse(FileType.isBitmapImage("image/svg+xml"));

    assertFalse(FileType.isBitmapImage("application/vnd.ms-powerpoint"));
    assertFalse(
      FileType.isBitmapImage(
        "application/vnd.openxmlformats-officedocument.presentationml.presentation"));
    assertFalse(FileType.isBitmapImage("application/vnd.oasis.opendocument.presentation"));

    assertFalse(FileType.isBitmapImage("application/pdf"));

    assertFalse(FileType.isBitmapImage("audio/mp4"));
    assertFalse(FileType.isBitmapImage("audio/x-aac"));
    assertFalse(FileType.isBitmapImage("audio/aac"));
    assertFalse(FileType.isBitmapImage("audio/aacp"));
    assertFalse(FileType.isBitmapImage("audio/mpeg3"));
    assertFalse(FileType.isBitmapImage("audio/x-mpeg-3"));
    assertFalse(FileType.isBitmapImage("video/mpeg"));
    assertFalse(FileType.isBitmapImage("video/x-mpeg"));
    assertFalse(FileType.isBitmapImage("audio/ogg"));
    assertFalse(FileType.isBitmapImage("audio/wav"));
    assertFalse(FileType.isBitmapImage("audio/x-wav"));

    assertFalse(FileType.isBitmapImage("application/vnd.ms-excel"));
    assertFalse(
      FileType.isBitmapImage("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    assertFalse(FileType.isBitmapImage("application/vnd.oasis.opendocument.spreadsheet"));

    assertFalse(FileType.isBitmapImage("application/x-troff-msvideo"));
    assertFalse(FileType.isBitmapImage("video/avi"));
    assertFalse(FileType.isBitmapImage("video/msvideo"));
    assertFalse(FileType.isBitmapImage("video/x-msvideo"));
    assertFalse(FileType.isBitmapImage("video/mp4"));

    assertFalse(FileType.isBitmapImage("application/msword"));
    assertFalse(
      FileType
        .isBitmapImage("application/vnd.openxmlformats-officedocument.wordprocessingml.document"));
    assertFalse(FileType.isBitmapImage("application/vnd.oasis.opendocument.text"));
    assertFalse(FileType.isBitmapImage("application/x-tika-msoffice"));
    assertFalse(FileType.isBitmapImage("application/zip"));


    assertFalse(FileType.isBitmapImage("image/svg+xml"));

    assertFalse(FileType.isBitmapImage("application/vnd.ms-powerpoint"));
    assertFalse(
      FileType.isBitmapImage(
        "application/vnd.openxmlformats-officedocument.presentationml.presentation"));
    assertFalse(FileType.isBitmapImage("application/vnd.oasis.opendocument.presentation"));

    assertFalse(FileType.isBitmapImage("application/pdf"));

    assertFalse(FileType.isBitmapImage("audio/mp4"));
    assertFalse(FileType.isBitmapImage("audio/x-aac"));
    assertFalse(FileType.isBitmapImage("audio/aac"));
    assertFalse(FileType.isBitmapImage("audio/aacp"));
    assertFalse(FileType.isBitmapImage("audio/mpeg3"));
    assertFalse(FileType.isBitmapImage("audio/x-mpeg-3"));
    assertFalse(FileType.isBitmapImage("video/mpeg"));
    assertFalse(FileType.isBitmapImage("video/x-mpeg"));
    assertFalse(FileType.isBitmapImage("audio/ogg"));
    assertFalse(FileType.isBitmapImage("audio/wav"));
    assertFalse(FileType.isBitmapImage("audio/x-wav"));

    assertFalse(FileType.isBitmapImage("application/vnd.ms-excel"));
    assertFalse(
      FileType.isBitmapImage("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    assertFalse(FileType.isBitmapImage("application/vnd.oasis.opendocument.spreadsheet"));

    assertFalse(FileType.isBitmapImage("application/x-troff-msvideo"));
    assertFalse(FileType.isBitmapImage("video/avi"));
    assertFalse(FileType.isBitmapImage("video/msvideo"));
    assertFalse(FileType.isBitmapImage("video/x-msvideo"));
    assertFalse(FileType.isBitmapImage("video/mp4"));

    assertFalse(FileType.isBitmapImage("application/msword"));
    assertFalse(
      FileType
        .isBitmapImage("application/vnd.openxmlformats-officedocument.wordprocessingml.document"));
    assertFalse(FileType.isBitmapImage("application/vnd.oasis.opendocument.text"));
    assertFalse(FileType.isBitmapImage("application/x-tika-msoffice"));
    assertFalse(FileType.isBitmapImage("application/zip"));
  }
}
