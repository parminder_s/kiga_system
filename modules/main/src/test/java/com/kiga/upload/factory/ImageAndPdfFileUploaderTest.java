package com.kiga.upload.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.kiga.integration.amazon.s3.config.AmazonS3Parameters;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.repository.ss.S3FileRepository;
import com.kiga.security.domain.Member;
import com.kiga.upload.ImageProcessor;
import com.kiga.upload.UploadParameters;
import com.kiga.upload.converter.MultipartFileUtil;
import com.kiga.upload.exception.FiltersApplicationException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author robert.klodzinski@treevert.com
 * @since 8/6/17.
 */
public class ImageAndPdfFileUploaderTest {
  private static final String FOLDER = "folder/a/b/c";
  private static final String BUCKET_URL = "bucketUrl";
  private static final String BUCKET_NAME = "bucketName";
  private static final String MULTIPART_ORIGINAL_FILENAME_SVG = "multipartOriginalFilename.svg";
  private static final String MULTIPART_ORIGINAL_FILENAME_PDF = "multipartOriginalFilename.svg";
  private static final String MULTIPART_ORIGINAL_FILENAME_PNG = "multipartOriginalFilename.svg";
  private static final String CLASS_NAME = "KigaS3File";
  private static final long MEMBER_ID = 999L;
  private static final int FILE_SIZE = 1234;

  private static final String SAMPLE_SVG_PATH = "src/test/resources/com.kiga.upload/test.svg";
  private static final String SAMPLE_PNG_PATH = "src/test/resources/com.kiga.upload/test.png";
  private static final String SAMPLE_PDF_PATH = "src/test/resources/com.kiga.upload/test.pdf";

  private static final String TEMPORARY_PATH = "/tmp/temp-test-files";

  private UploadParameters uploadParametersMock = mock(UploadParameters.class);
  private MultipartFileUtil multipartFileUtilMock = mock(MultipartFileUtil.class);
  private S3FileRepository s3FileRepositoryMock = mock(S3FileRepository.class);
  private AmazonS3Parameters s3ConfigMock = mock(AmazonS3Parameters.class);
  private ImageProcessor imageProcessorMock = mock(ImageProcessor.class);
  private Member memberMock = mock(Member.class);
  private MultipartFile multipartFileMock = mock(MultipartFile.class);
  private AmazonS3Client amazonS3ClientMock = mock(AmazonS3Client.class);
  private CannedAccessControlList cannedAccessControlList = CannedAccessControlList.PublicRead;
  private File uploadedFileMock = mock(File.class);
  private File svgFileMock = mock(File.class);
  private File rasterizedSvgFile = new File(SAMPLE_PNG_PATH);

  private ImageAndPdfFileUploader fileUploader = new ImageAndPdfFileUploader();

  /** Prepare tests. */
  @Before
  public void init() throws Exception {
    fileUploader.setOriginalMultipartFile(multipartFileMock);
    fileUploader.setMember(memberMock);
    fileUploader.setS3Config(s3ConfigMock);
    fileUploader.setAmazonS3Client(amazonS3ClientMock);
    fileUploader.setS3FileRepository(s3FileRepositoryMock);
    fileUploader.setUploadParameters(uploadParametersMock);
    fileUploader.setMultipartFileUtil(multipartFileUtilMock);
    fileUploader.setImageProcessor(imageProcessorMock);
    fileUploader.setFolder("//" + FOLDER + "/");

    when(memberMock.getId()).thenReturn(MEMBER_ID);
    when(uploadParametersMock.getCannedAccessControlList()).thenReturn(cannedAccessControlList);
    when(uploadParametersMock.getTemporaryDirectory()).thenReturn(TEMPORARY_PATH);
    when(s3ConfigMock.getBucketUrl()).thenReturn(BUCKET_URL);
    when(s3ConfigMock.getBucketName()).thenReturn(BUCKET_NAME);

    when(imageProcessorMock.pdfToSvg(uploadedFileMock)).thenReturn(svgFileMock);
    when(imageProcessorMock.rasterizeSvg(svgFileMock)).thenReturn(rasterizedSvgFile);
    when(imageProcessorMock.rasterizeSvg(uploadedFileMock)).thenReturn(rasterizedSvgFile);
    when(uploadParametersMock.getImageVariants()).thenReturn(new String[] {"123x45", "678x901"});
  }

  @Test
  public void testUploadPng() throws Exception {
    File rasterizedSvgFileSpy = spy(rasterizedSvgFile);
    doReturn(true).when(rasterizedSvgFileSpy).delete();

    when(multipartFileMock.getOriginalFilename()).thenReturn(MULTIPART_ORIGINAL_FILENAME_PNG);
    when(multipartFileMock.getBytes()).thenReturn(new byte[FILE_SIZE]);
    when(multipartFileUtilMock.upload(eq(multipartFileMock), any()))
        .thenReturn(rasterizedSvgFileSpy);
    when(uploadedFileMock.getName())
        .thenReturn(
            FilenameUtils.getBaseName(SAMPLE_PNG_PATH)
                + "."
                + FilenameUtils.getExtension(SAMPLE_PNG_PATH));
    when(uploadedFileMock.getAbsolutePath()).thenReturn(SAMPLE_PNG_PATH);

    S3Image uploadedFileEntity = (S3Image) fileUploader.upload();

    verify(imageProcessorMock).resize(any(BufferedImage.class), eq(123), eq(45));
    verify(imageProcessorMock).resize(any(BufferedImage.class), eq(678), eq(901));

    verify(s3FileRepositoryMock).save(uploadedFileEntity);

    ArgumentCaptor<PutObjectRequest> putObjectRequestArgumentCaptor =
        ArgumentCaptor.forClass(PutObjectRequest.class);
    verify(amazonS3ClientMock, times(3)).putObject(putObjectRequestArgumentCaptor.capture());

    List<PutObjectRequest> captured = putObjectRequestArgumentCaptor.getAllValues();

    assertEquals(BUCKET_NAME, captured.get(0).getBucketName());
    assertEquals(
        FOLDER
            + "/"
            + FilenameUtils.getBaseName(SAMPLE_PNG_PATH)
            + "."
            + FilenameUtils.getExtension(SAMPLE_PNG_PATH),
        captured.get(0).getKey());
    assertEquals(rasterizedSvgFileSpy, captured.get(0).getFile());
    assertSame(cannedAccessControlList, captured.get(0).getCannedAcl());

    assertEquals(BUCKET_NAME, captured.get(1).getBucketName());
    assertEquals(FOLDER + "/" + "123x45_test.png", captured.get(1).getKey());
    assertSame(cannedAccessControlList, captured.get(1).getCannedAcl());

    assertEquals(BUCKET_NAME, captured.get(2).getBucketName());
    assertEquals(FOLDER + "/" + "678x901_test.png", captured.get(2).getKey());
    assertSame(cannedAccessControlList, captured.get(2).getCannedAcl());

    assertEquals(FILE_SIZE, uploadedFileEntity.getSize().intValue());
    assertEquals(BUCKET_NAME, uploadedFileEntity.getBucket());
    assertEquals(CLASS_NAME, uploadedFileEntity.getClassName());
    assertEquals(FOLDER, uploadedFileEntity.getFolder());
    assertEquals(
        FilenameUtils.getBaseName(SAMPLE_PNG_PATH)
            + "."
            + FilenameUtils.getExtension(SAMPLE_PNG_PATH),
        uploadedFileEntity.getName());
    assertEquals(MEMBER_ID, uploadedFileEntity.getOwnerId().longValue());
    assertEquals(
        BUCKET_URL
            + "/"
            + FOLDER
            + "/"
            + FilenameUtils.getBaseName(SAMPLE_PNG_PATH)
            + "."
            + FilenameUtils.getExtension(SAMPLE_PNG_PATH),
        uploadedFileEntity.getUrl());
    assertNull(uploadedFileEntity.getPdfUrl());
  }

  @Test(expected = FiltersApplicationException.class)
  public void testUploadPngThrowFiltersApplicationException() throws Exception {
    File rasterizedSvgFileSpy = spy(rasterizedSvgFile);
    doReturn(true).when(rasterizedSvgFileSpy).delete();
    fileUploader.upload();
  }

  @Test(expected = AmazonServiceException.class)
  public void testUploadWithRejection() throws Exception {
    when(amazonS3ClientMock.putObject(any(PutObjectRequest.class)))
        .thenThrow(AmazonServiceException.class);
    File rasterizedSvgFileSpy = spy(rasterizedSvgFile);
    doReturn(true).when(rasterizedSvgFileSpy).delete();

    when(multipartFileMock.getOriginalFilename()).thenReturn(MULTIPART_ORIGINAL_FILENAME_PNG);
    when(multipartFileMock.getBytes()).thenReturn(new byte[FILE_SIZE]);
    when(multipartFileUtilMock.upload(eq(multipartFileMock), any()))
        .thenReturn(rasterizedSvgFileSpy);

    fileUploader.upload();
  }

  @Test(expected = AmazonClientException.class)
  public void testUploadWithError() throws Exception {
    when(amazonS3ClientMock.putObject(any(PutObjectRequest.class)))
        .thenThrow(AmazonClientException.class);
    File rasterizedSvgFileSpy = spy(rasterizedSvgFile);
    doReturn(true).when(rasterizedSvgFileSpy).delete();

    when(multipartFileMock.getOriginalFilename()).thenReturn(MULTIPART_ORIGINAL_FILENAME_PNG);
    when(multipartFileMock.getBytes()).thenReturn(new byte[FILE_SIZE]);
    when(multipartFileUtilMock.upload(eq(multipartFileMock), any()))
        .thenReturn(rasterizedSvgFileSpy);

    fileUploader.upload();
  }

  @Test
  public void testGetFileUrl() {
    fileUploader.setOriginalFile(rasterizedSvgFile);
    assertEquals(
        BUCKET_URL + "/" + FOLDER + "/" + rasterizedSvgFile.getName(), fileUploader.getFileUrl());
  }
}
