package com.kiga.upload;

import static org.imgscalr.Scalr.Mode.FIT_TO_HEIGHT;
import static org.imgscalr.Scalr.Mode.FIT_TO_WIDTH;

import org.hamcrest.beans.SamePropertyValuesAs;
import org.junit.Assert;
import org.junit.Test;

import java.awt.image.BufferedImage;

/**
 * Created by rainerh on 28.08.16.
 */
public class ResizeCalcerTest {
  BufferedImage bufferedImage = new BufferedImage(150, 100, BufferedImage.TYPE_BYTE_GRAY);

  @Test
  public void calc() throws Exception {
    ResizeCalcerResult result = ResizeCalcer.calc(bufferedImage, 15, 10);

    Assert.assertThat(result, SamePropertyValuesAs.samePropertyValuesAs(
      new ResizeCalcerResult(FIT_TO_WIDTH, 15, 15, 10)));
  }

  @Test
  public void calcDifferentRatio() {
    ResizeCalcerResult result = ResizeCalcer.calc(bufferedImage, 50, 25);

    Assert.assertThat(result, SamePropertyValuesAs.samePropertyValuesAs(
      new ResizeCalcerResult(FIT_TO_HEIGHT, 25, 50, 25)));
  }

  @Test
  public void automaticWidth() {
    ResizeCalcerResult result = ResizeCalcer.calc(bufferedImage, 0, 20);
    Assert.assertEquals(30, result.getFinalWidth());
  }

  @Test
  public void automaticHeight() {
    ResizeCalcerResult result = ResizeCalcer.calc(bufferedImage, 10, 0);
    Assert.assertEquals(6, result.getFinalHeight());
  }

  @Test
  public void constructorTest() {
    new ResizeCalcer();
  }
}
