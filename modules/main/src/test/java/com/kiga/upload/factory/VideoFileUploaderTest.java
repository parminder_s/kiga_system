package com.kiga.upload.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author robert.klodzinski@treevert.com
 * @since 5/31/15.
 */
public class VideoFileUploaderTest {
  @Test
  public void testGetEntityClassName() {
    VideoFileUploader videoFileUploader = new VideoFileUploader();
    assertEquals("KigaS3File", videoFileUploader.getEntityClassName());
  }
}
