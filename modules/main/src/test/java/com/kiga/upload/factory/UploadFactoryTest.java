package com.kiga.upload.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.amazonaws.services.s3.AmazonS3Client;
import com.kiga.integration.amazon.s3.config.AmazonS3Parameters;
import com.kiga.s3.repository.ss.S3FileRepository;
import com.kiga.security.domain.Member;
import com.kiga.upload.ImageProcessor;
import com.kiga.upload.UploadParameters;
import com.kiga.upload.converter.MultipartFileUtil;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author robert.klodzinski@treevert.com
 * @since 8/6/17.
 */
public class UploadFactoryTest {
  private static final long MEMBER_ID = 999L;
  private S3FileRepository s3FileRepository = mock(S3FileRepository.class);
  private MultipartFileUtil multipartFileUtilMock = mock(MultipartFileUtil.class);
  private AmazonS3Parameters s3Config = mock(AmazonS3Parameters.class);
  private AmazonS3Client amazonS3ClientMock = mock(AmazonS3Client.class);
  private UploadParameters uploadParameters = mock(UploadParameters.class);
  private ImageProcessor imageProcessor = mock(ImageProcessor.class);
  private Member memberMock = mock(Member.class);
  private UploadFactory uploadFactory =
    new UploadFactory(s3FileRepository, multipartFileUtilMock, s3Config, uploadParameters,
      imageProcessor, amazonS3ClientMock);

  @Before
  public void initialize() {
    when(memberMock.getId()).thenReturn(MEMBER_ID);
  }

  @Test
  public void testDependencyInjection() throws Exception {
    MultipartFile multipartFileMock = mock(MultipartFile.class);
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("image/svg+xml");
    AbstractFileUploader fileUploader =
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock);

    assertSame(multipartFileMock, fileUploader.getOriginalMultipartFile());
    assertSame(multipartFileUtilMock, fileUploader.getMultipartFileUtil());
    assertSame(s3FileRepository, fileUploader.getS3FileRepository());
    assertSame(s3Config, fileUploader.getS3Config());
    assertSame(uploadParameters, fileUploader.getUploadParameters());
    assertSame(imageProcessor, fileUploader.getImageProcessor());
    assertSame(memberMock, fileUploader.getMember());
  }

  @Test
  public void testGetUploaderInstance() throws Exception {
    MultipartFile multipartFileMock = mock(MultipartFile.class);

    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("image/svg+xml");
    assertEquals(ImageAndPdfFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("image/gif");
    assertSame(ImageAndPdfFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("image/png");
    assertSame(ImageAndPdfFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("image/jpeg");
    assertSame(ImageAndPdfFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("image/pjpeg");
    assertSame(ImageAndPdfFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("application/pdf");
    assertSame(ImageAndPdfFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());

    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("application/msword");
    assertSame(WordFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock))
      .thenReturn("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    assertSame(WordFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock))
      .thenReturn("application/vnd.oasis.opendocument.text");
    assertSame(WordFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock))
      .thenReturn("application/x-tika-msoffice");
    assertSame(WordFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());

    when(multipartFileUtilMock.getContentType(multipartFileMock))
      .thenReturn("application/x-troff-msvideo");
    assertSame(VideoFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("video/avi");
    assertSame(VideoFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("video/msvideo");
    assertSame(VideoFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("video/x-msvideo");
    assertSame(VideoFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("video/mp4");
    assertSame(VideoFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("video/mpeg");
    assertSame(VideoFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("video/x-mpeg");
    assertSame(VideoFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());

    when(multipartFileUtilMock.getContentType(multipartFileMock))
      .thenReturn("application/vnd.ms-powerpoint");
    assertSame(PowerPointFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock))
      .thenReturn("application/vnd.openxmlformats-officedocument.presentationml.presentation");
    assertSame(PowerPointFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock))
      .thenReturn("application/vnd.oasis.opendocument.presentation");
    assertSame(PowerPointFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());

    when(multipartFileUtilMock.getContentType(multipartFileMock))
      .thenReturn("application/vnd.ms-excel");
    assertSame(ExcelFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock))
      .thenReturn("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    assertSame(ExcelFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock))
      .thenReturn("application/vnd.oasis.opendocument.spreadsheet");
    assertSame(ExcelFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());


    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("audio/mp4");
    assertSame(AudioFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("audio/x-aac");
    assertSame(AudioFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("audio/aac");
    assertSame(AudioFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("audio/aacp");
    assertSame(AudioFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("audio/mpeg3");
    assertSame(AudioFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("audio/x-mpeg-3");
    assertSame(AudioFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("audio/ogg");
    assertSame(AudioFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("audio/wav");
    assertSame(AudioFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("audio/x-wav");
    assertSame(AudioFileUploader.class,
      uploadFactory.getUploaderInstance(multipartFileMock, memberMock).getClass());
  }
}
