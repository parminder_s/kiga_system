package com.kiga.upload.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author robert.klodzinski@treevert.com
 * @since 5/31/15.
 */
public class WordFileUploaderTest {
  @Test
  public void testGetEntityClassName() {
    WordFileUploader wordFileUploader = new WordFileUploader();
    assertEquals("KigaS3File", wordFileUploader.getEntityClassName());
  }
}
