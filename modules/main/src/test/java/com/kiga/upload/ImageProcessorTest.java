package com.kiga.upload;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.s3.service.resize.exception.ResizeException;
import com.kiga.util.pdf2svg.Pdf2SvgWrapper;
import com.kiga.util.rasterizer.RasterizerWrapper;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by rainerh on 28.08.16.
 */
public class ImageProcessorTest {
  private RasterizerWrapper rasterizerWrapperMock = mock(RasterizerWrapper.class);
  private Pdf2SvgWrapper pdf2SvgWrapperMock = mock(Pdf2SvgWrapper.class);
  @Rule
  public final ExpectedException expectedException = ExpectedException.none();

  @Test
  public void pdfToSvg() throws Exception {
    File originalFileMock = mock(File.class);
    when(originalFileMock.getAbsolutePath()).thenReturn("mockedName");
    new ImageProcessor(null, pdf2SvgWrapperMock, rasterizerWrapperMock).pdfToSvg(originalFileMock);
    verify(pdf2SvgWrapperMock).convert(originalFileMock, "/mockedName" + ".svg");
  }

  @Test
  public void rasterizeSvg() throws Exception {
    File fileMock = mock(File.class);
    when(fileMock.getAbsolutePath()).thenReturn("/mockedPath/mockedFile.svg");
    new ImageProcessor(null, pdf2SvgWrapperMock, rasterizerWrapperMock).rasterizeSvg(fileMock);
    verify(rasterizerWrapperMock)
      .rasterize(fileMock, "/mockedPath/mockedFile.png", "image/png");
  }

  @Test
  public void resizeIllegalArgumentsImage() throws Exception {
    ImageProcessor imageProcessor = new ImageProcessor(null, pdf2SvgWrapperMock,
      rasterizerWrapperMock);
    expectedException.expect(IllegalArgumentException.class);
    imageProcessor.resize(null, 0, 0);
  }

  @Test
  public void resizeIllegalArgumentsDimensions() throws Exception {
    ImageProcessor imageProcessor = new ImageProcessor(null, pdf2SvgWrapperMock,
      rasterizerWrapperMock);

    expectedException.expect(IllegalArgumentException.class);
    imageProcessor.resize(new BufferedImage(1, 1, BufferedImage.TYPE_BYTE_GRAY), 0, 0);
  }

  @Test
  public void resizeNonIllegalArgumentsDimensions() throws Exception {
    ImageProcessor imageProcessor = new ImageProcessor(null, pdf2SvgWrapperMock,
      rasterizerWrapperMock);

    expectedException.expect(NullPointerException.class);
    imageProcessor.resize(new BufferedImage(1, 1, BufferedImage.TYPE_BYTE_GRAY), 0, 1);
  }

  @Test
  public void resizeWithoutPostManipulation() throws IOException {
    ScalrWrapper wrapper = mock(ScalrWrapper.class);
    ImageProcessor imageProcessor = new ImageProcessor(wrapper, pdf2SvgWrapperMock,
      rasterizerWrapperMock);
    BufferedImage mockedImage = new BufferedImage(10, 10, BufferedImage.TYPE_BYTE_GRAY);

    when(wrapper.resize(any(), any(), any(), anyInt())).thenReturn(mockedImage);
    BufferedImage resized =
      imageProcessor.resize(new BufferedImage(100, 100, BufferedImage.TYPE_BYTE_GRAY), 10, 10);

    Assert.assertEquals(10, resized.getWidth());
    Assert.assertEquals(10, resized.getHeight());
  }

  @Test
  public void resizeWithNullReturnedImage() throws IOException {
    ScalrWrapper wrapper = mock(ScalrWrapper.class);
    ImageProcessor imageProcessor = new ImageProcessor(wrapper, pdf2SvgWrapperMock,
      rasterizerWrapperMock);

    BufferedImage mockedImageWrongWidth = new BufferedImage(25, 10, BufferedImage.TYPE_BYTE_GRAY);
    when(wrapper.resize(any(), any(), any(), anyInt())).thenReturn(mockedImageWrongWidth);
    expectedException.expect(ResizeException.class);
    imageProcessor.resize(new BufferedImage(100, 100, BufferedImage.TYPE_BYTE_GRAY), 10, 10);
  }

  @Test
  public void resizeWithWrongHeight() throws IOException {
    ScalrWrapper wrapper = mock(ScalrWrapper.class);
    ImageProcessor imageProcessor = new ImageProcessor(wrapper, pdf2SvgWrapperMock,
      rasterizerWrapperMock);

    BufferedImage mockedImageWrongHeight = new BufferedImage(10, 25, BufferedImage.TYPE_BYTE_GRAY);
    when(wrapper.resize(any(), any(), any(), anyInt())).thenReturn(mockedImageWrongHeight);
    expectedException.expect(ResizeException.class);
    imageProcessor.resize(new BufferedImage(100, 100, BufferedImage.TYPE_BYTE_GRAY), 10, 10);
  }

  @Test
  public void resizeHeightWithCropAndPadding() throws IOException {
    resizeTest(new BufferedImage(10, 6, BufferedImage.TYPE_BYTE_GRAY));
  }

  @Test
  public void resizeWidthWithCropAndPadding() throws IOException {
    resizeTest(new BufferedImage(6, 10, BufferedImage.TYPE_BYTE_GRAY));
  }

  /**
   * helper function that executes a test where padding and cropping takes place.
   */
  public void resizeTest(BufferedImage mockedImage) throws IOException {
    ScalrWrapper wrapper = mock(ScalrWrapper.class);
    ImageProcessor imageProcessor = new ImageProcessor(wrapper, pdf2SvgWrapperMock,
      rasterizerWrapperMock);

    BufferedImage mockedImageFinal = new BufferedImage(10, 10, BufferedImage.TYPE_BYTE_GRAY);
    when(wrapper.resize(any(), any(), any(), anyInt())).thenReturn(mockedImage);
    when(wrapper.crop(any(), anyInt(), anyInt(), anyInt(), anyInt())).thenReturn(mockedImageFinal);

    imageProcessor.resize(new BufferedImage(100, 100, BufferedImage.TYPE_BYTE_GRAY), 10, 10);
    verify(wrapper).pad(mockedImage, 4, Color.WHITE);
    verify(wrapper).crop(null, 4, 2, 10, 10);
  }
}
