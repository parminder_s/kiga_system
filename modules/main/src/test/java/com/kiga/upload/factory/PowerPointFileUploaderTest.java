package com.kiga.upload.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author robert.klodzinski@treevert.com
 * @since 8/06/17.
 */
public class PowerPointFileUploaderTest {
  @Test
  public void testGetEntityClassName() {
    PowerPointFileUploader powerPointFileUploader = new PowerPointFileUploader();
    assertEquals("KigaS3File", powerPointFileUploader.getEntityClassName());
  }
}
