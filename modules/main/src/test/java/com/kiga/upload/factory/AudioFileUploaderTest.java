package com.kiga.upload.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author robert.klodzinski@treevert.com
 * @since 8/6/17.
 */
public class AudioFileUploaderTest {
  @Test
  public void testGetEntityClassName() {
    AudioFileUploader audioFileUploader = new AudioFileUploader();
    assertEquals("KigaS3File", audioFileUploader.getEntityClassName());
  }
}
