package com.kiga.upload.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.kiga.integration.amazon.s3.config.AmazonS3Parameters;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.repository.ss.S3FileRepository;
import com.kiga.security.domain.Member;
import com.kiga.upload.ImageProcessor;
import com.kiga.upload.UploadParameters;
import com.kiga.upload.converter.MultipartFileUtil;
import java.io.File;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author robert.klodzinski@treevert.com
 * @since 8/6/17.
 */
public class AbstractFileUploaderTest {
  private static final String FOLDER = "folder/a/b/c";
  private static final String MULTIPART_ORIGINAL_FILENAME = "multipartOriginalFilename.svg";
  private static final String CLASS_NAME = "AbstractClassName";
  private static final long MEMBER_ID = 999L;
  private static final int FILE_SIZE = 1234;
  private static final String UPLOADED_FILE_NAME = "uploadedFileName.svg";
  private static final String TEMPORARY_PATH = System.getProperty("java.io.tmpdir");

  private UploadParameters uploadParametersMock = mock(UploadParameters.class);
  private MultipartFileUtil multipartFileUtilMock = mock(MultipartFileUtil.class);
  private S3FileRepository s3FileRepositoryMock = mock(S3FileRepository.class);
  private AmazonS3Parameters s3ConfigMock = new AmazonS3Parameters();
  private ImageProcessor imageProcessorMock = mock(ImageProcessor.class);
  private Member memberMock = mock(Member.class);
  private MultipartFile multipartFileMock = mock(MultipartFile.class);
  private AmazonS3Client amazonS3ClientMock = mock(AmazonS3Client.class);
  private CannedAccessControlList cannedAccessControlList = CannedAccessControlList.PublicRead;
  private File uploadedFileMock = mock(File.class);

  private AbstractFileUploader fileUploader =
      new AbstractFileUploader() {

        @Override
        public String getEntityClassName() {
          return CLASS_NAME;
        }
      };

  /** Prepare tests. */
  @Before
  public void init() throws Exception {
    fileUploader.setOriginalMultipartFile(multipartFileMock);
    fileUploader.setMember(memberMock);
    fileUploader.setS3Config(s3ConfigMock);
    fileUploader.setAmazonS3Client(amazonS3ClientMock);
    fileUploader.setS3FileRepository(s3FileRepositoryMock);
    fileUploader.setUploadParameters(uploadParametersMock);
    fileUploader.setMultipartFileUtil(multipartFileUtilMock);
    fileUploader.setImageProcessor(imageProcessorMock);
    fileUploader.setFolder("//" + FOLDER + "/");

    when(memberMock.getId()).thenReturn(MEMBER_ID);
    when(uploadedFileMock.getName()).thenReturn(UPLOADED_FILE_NAME);
    when(uploadParametersMock.getCannedAccessControlList()).thenReturn(cannedAccessControlList);
    when(uploadParametersMock.getTemporaryDirectory()).thenReturn(TEMPORARY_PATH);
    s3ConfigMock.setBucketName("bucketName");
    s3ConfigMock.setBucketUrl("bucketUrl");
    when(multipartFileMock.getOriginalFilename()).thenReturn(MULTIPART_ORIGINAL_FILENAME);
    when(multipartFileMock.getBytes()).thenReturn(new byte[FILE_SIZE]);
    when(multipartFileUtilMock.upload(eq(multipartFileMock), any())).thenReturn(uploadedFileMock);
  }

  @Test
  public void testUpload() throws Exception {
    KigaS3File uploadedFileEntity = fileUploader.upload();

    verify(s3FileRepositoryMock).save(uploadedFileEntity);

    ArgumentCaptor<PutObjectRequest> putObjectRequestArgumentCaptor =
        ArgumentCaptor.forClass(PutObjectRequest.class);
    verify(amazonS3ClientMock).putObject(putObjectRequestArgumentCaptor.capture());

    PutObjectRequest captured = putObjectRequestArgumentCaptor.getValue();

    assertEquals("bucketName", captured.getBucketName());
    assertEquals(FOLDER + "/" + UPLOADED_FILE_NAME, captured.getKey());
    assertEquals(uploadedFileMock, captured.getFile());
    assertSame(cannedAccessControlList, captured.getCannedAcl());

    assertEquals(FILE_SIZE, uploadedFileEntity.getSize().intValue());
    assertEquals("bucketName", uploadedFileEntity.getBucket());
    assertEquals(CLASS_NAME, uploadedFileEntity.getClassName());
    assertEquals(FOLDER, uploadedFileEntity.getFolder());
    assertEquals(MULTIPART_ORIGINAL_FILENAME, uploadedFileEntity.getName());
    assertEquals(MEMBER_ID, uploadedFileEntity.getOwnerId().longValue());
    assertEquals(
        "bucketUrl/" + FOLDER + "/" + MULTIPART_ORIGINAL_FILENAME, uploadedFileEntity.getUrl());
  }

  @Test(expected = AmazonServiceException.class)
  public void testUploadWithRejection() throws Exception {
    when(amazonS3ClientMock.putObject(any(PutObjectRequest.class)))
        .thenThrow(AmazonServiceException.class);
    fileUploader.upload();
  }

  @Test(expected = AmazonClientException.class)
  public void testUploadWithError() throws Exception {
    when(amazonS3ClientMock.putObject(any(PutObjectRequest.class)))
        .thenThrow(AmazonClientException.class);
    fileUploader.upload();
  }

  @Test
  public void testGetFileUrl() {
    assertEquals(
        "bucketUrl/" + FOLDER + "/" + MULTIPART_ORIGINAL_FILENAME, fileUploader.getFileUrl());
  }
}
