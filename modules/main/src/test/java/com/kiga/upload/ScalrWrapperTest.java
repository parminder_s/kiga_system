package com.kiga.upload;

import org.imgscalr.Scalr;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 * Created by rainerh on 28.08.16.
 */
public class ScalrWrapperTest {
  ScalrWrapper wrapper = new ScalrWrapper();
  BufferedImage bi = new BufferedImage(10, 10, BufferedImage.TYPE_BYTE_GRAY);

  @Test
  public void resize() throws Exception {
    wrapper.resize(bi, Scalr.Method.AUTOMATIC, Scalr.Mode.FIT_TO_HEIGHT, 50);
  }

  @Test
  public void pad() throws Exception {
    wrapper.pad(bi, 1, Color.BLACK);
  }

  @Test
  public void crop() throws Exception {
    wrapper.crop(bi, 0, 0, 5, 5);
  }

}
