package com.kiga.upload.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author robert.klodzinski@treevert.com
 * @since 8/6/17.
 */
public class ExcelFileUploaderTest {
  @Test
  public void testGetEntityClassName() {
    ExcelFileUploader excelFileUploader = new ExcelFileUploader();
    assertEquals("KigaS3File", excelFileUploader.getEntityClassName());
  }
}
