package com.kiga.upload.converter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.util.ResourceLoader;
import com.kiga.util.TempFileCreator;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author robert.klodzinski@treevert.com
 * @since 5/26/15.
 */
public class MultipartFileUtilTest {
  public static final String CONTENT_TYPE = "image/svg+xml";
  private static final String PATH = "/tmp/kiga-tests/MultipartFileUtilTest.svg";
  private MultipartFileUtil multipartFileUtil = new MultipartFileUtil(new TempFileCreator());
  private MultipartFile multipartFileMock = mock(MultipartFile.class);

  /**
   * prepare tests
   *
   * @throws IOException from multipartfile.getbytes
   */
  @Before
  public void prepareTests() throws IOException {
    byte[] byteArray = IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/test.svg"));
    when(multipartFileMock.getBytes()).thenReturn(byteArray);
    when(multipartFileMock.getContentType()).thenReturn(CONTENT_TYPE);
  }

  @Test
  public void testAudioDetection() throws Exception {
    // when(multipartFileMock.getBytes())
    // .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.aac")));
    // when(multipartFileMock.getContentType()).thenReturn("audio/aac");
    // assertEquals("audio/aac", multipartFileUtil.getContentType(multipartFileMock));

    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.mp3")));
    when(multipartFileMock.getContentType()).thenReturn("audio/mpeg");
    assertEquals("audio/mpeg", multipartFileUtil.getContentType(multipartFileMock));

    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.ogg")));
    when(multipartFileMock.getContentType()).thenReturn("application/ogg");
    assertEquals("application/ogg", multipartFileUtil.getContentType(multipartFileMock));

    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.wav")));
    when(multipartFileMock.getContentType()).thenReturn("audio/x-wav");
    assertEquals("audio/x-wav", multipartFileUtil.getContentType(multipartFileMock));
  }

  @Test
  public void testExcelDetection() throws Exception {
    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.xls")));
    when(multipartFileMock.getContentType()).thenReturn("application/vnd.ms-excel");
    assertEquals("application/vnd.ms-excel", multipartFileUtil.getContentType(multipartFileMock));

    // when(multipartFileMock.getBytes())
    // .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.xlsx")));
    // when(multipartFileMock.getContentType()).thenReturn(CONTENT_TYPE);
    // assertEquals("audio/x-wav", multipartFileUtil.getContentType(multipartFileMock));

    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.ods")));
    when(multipartFileMock.getContentType())
        .thenReturn("application/vnd.oasis.opendocument.spreadsheet");
    assertEquals(
        "application/vnd.oasis.opendocument.spreadsheet",
        multipartFileUtil.getContentType(multipartFileMock));
  }

  @Test
  public void testSvgDetection() throws Exception {
    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/test.svg")));
    when(multipartFileMock.getContentType()).thenReturn("image/svg+xml");
    assertEquals("image/svg+xml", multipartFileUtil.getContentType(multipartFileMock));
  }

  @Test
  public void testGifDetection() throws Exception {
    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.gif")));
    when(multipartFileMock.getContentType()).thenReturn("image/gif");
    assertEquals("image/gif", multipartFileUtil.getContentType(multipartFileMock));
  }

  @Test
  public void testJpgDetection() throws Exception {
    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.jpg")));
    when(multipartFileMock.getContentType()).thenReturn("image/jpeg");
    assertEquals("image/jpeg", multipartFileUtil.getContentType(multipartFileMock));
  }

  @Test
  public void testPngDetection() throws Exception {
    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/test.pdf")));
    when(multipartFileMock.getContentType()).thenReturn("application/pdf");
    assertEquals("application/pdf", multipartFileUtil.getContentType(multipartFileMock));
  }

  @Test
  public void testPdfDetection() throws Exception {
    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/test.png")));
    when(multipartFileMock.getContentType()).thenReturn("image/png");
    assertEquals("image/png", multipartFileUtil.getContentType(multipartFileMock));
  }

  @Test
  public void testPowerPointDetection() throws Exception {
    // when(multipartFileMock.getBytes())
    // .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.ppt")));
    // when(multipartFileMock.getContentType()).thenReturn("application/x-tika-msoffice");
    // assertEquals("application/x-tika-msoffice",
    // multipartFileUtil.getContentType(multipartFileMock));

    // when(multipartFileMock.getBytes())
    // .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.pptx")));
    // when(multipartFileMock.getContentType()).thenReturn("application/vnd.openxmlformats-officedo
    // cument.presentationml.presentation");
    // assertEquals("application/vnd.openxmlformats-officedocument.presentationml.presentation",
    // multipartFileUtil.getContentType(multipartFileMock));

    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.odp")));
    when(multipartFileMock.getContentType())
        .thenReturn("application/vnd.oasis.opendocument.presentation");
    assertEquals(
        "application/vnd.oasis.opendocument.presentation",
        multipartFileUtil.getContentType(multipartFileMock));
  }

  @Test
  public void testVideoDetection() throws Exception {
    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.avi")));
    when(multipartFileMock.getContentType()).thenReturn("video/x-msvideo");
    assertEquals("video/x-msvideo", multipartFileUtil.getContentType(multipartFileMock));

    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.mp4")));
    when(multipartFileMock.getContentType()).thenReturn("video/mp4");
    assertEquals("video/mp4", multipartFileUtil.getContentType(multipartFileMock));

    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.mpg")));
    when(multipartFileMock.getContentType()).thenReturn("video/mpeg");
    assertEquals("video/mpeg", multipartFileUtil.getContentType(multipartFileMock));
  }

  @Test
  public void testWordDetection() throws Exception {
    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.odt")));
    when(multipartFileMock.getContentType()).thenReturn("application/vnd.oasis.opendocument.text");
    assertEquals(
        "application/vnd.oasis.opendocument.text",
        multipartFileUtil.getContentType(multipartFileMock));

    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.doc")));
    when(multipartFileMock.getContentType()).thenReturn("application/x-tika-msoffice");
    assertEquals(
        "application/x-tika-msoffice", multipartFileUtil.getContentType(multipartFileMock));

    when(multipartFileMock.getBytes())
        .thenReturn(IOUtils.toByteArray(ResourceLoader.getResource("com.kiga.upload/sample.docx")));
    when(multipartFileMock.getContentType()).thenReturn("application/x-tika-ooxml");
    assertEquals("application/x-tika-ooxml", multipartFileUtil.getContentType(multipartFileMock));
  }
}
