package com.kiga.shopcontent.repository.ss;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.RepositoryContexts;
import com.kiga.shopcontent.domain.ProductPage;
import com.kiga.shopcontent.domain.draft.ProductPageDraft;
import com.kiga.shopcontent.domain.live.ProductPageLive;
import com.kiga.shopcontent.repository.ProductPageRepositoryProxy;
import com.kiga.shopcontent.repository.draft.ProductPageRepositoryDraft;
import com.kiga.shopcontent.repository.live.ProductPageRepositoryLive;
import com.kiga.shopcontent.repository.predicates.ProductPagePredicates;
import com.kiga.shopcontent.repository.querydsl.ProductPageQTypeResolver;
import com.mysema.query.types.Predicate;
import org.junit.Test;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author bbs
 * @since 9/6/16.
 */
@SuppressWarnings("unchecked")
public class ProductPageRepositoryProxyTest {
  private static final String COUNTRY_CODE = "de";
  private RepositoryContext repositoryContextMock = mock(RepositoryContext.class);
  private ProductPageQTypeResolver productPageQTypeResolverMock = mock(
    ProductPageQTypeResolver.class);

  @Test
  public void testGetLiveProductsByCategoryId() {
    ProductPagePredicates<ProductPageLive> productPagePredicates = new ProductPagePredicates<>(
      ProductPageLive.class, "productPageLive");
    Predicate allMatchingProductsPredicate = productPagePredicates
      .getAllMatchingProductsPredicate(1L);
    Predicate countryCodeMatchingProductsPredicate = productPagePredicates
      .getMatchingProductsForCountryPredicate(1L, COUNTRY_CODE);

    when(repositoryContextMock.getContext()).thenReturn(RepositoryContexts.LIVE);

    ProductPageRepositoryLive productPageRepositoryLive = mock(ProductPageRepositoryLive.class);
    when(productPageRepositoryLive.findAll(allMatchingProductsPredicate))
      .thenReturn(new PageImpl<>(Collections.emptyList()));
    when(productPageRepositoryLive.findAll(countryCodeMatchingProductsPredicate))
      .thenReturn(new PageImpl<>(Collections.emptyList()));
    when(productPageQTypeResolverMock.getEntityClass()).thenReturn(ProductPageLive.class);
    when(productPageQTypeResolverMock.getQueryDslVariableName()).thenReturn("productPageLive");
    Pageable pageable = new PageRequest(0, 15);

    ProductPageRepositoryProxy proxy = new ProductPageRepositoryProxy(productPageRepositoryLive,
      null, repositoryContextMock, productPageQTypeResolverMock);
    proxy.getProductsByCategoryId(1L, COUNTRY_CODE, pageable);

    verify(productPageRepositoryLive).findAll(allMatchingProductsPredicate);
    verify(productPageRepositoryLive).findAll(countryCodeMatchingProductsPredicate);
  }

  @Test
  public void testGetDraftProductsByCategoryId() {
    when(repositoryContextMock.getContext()).thenReturn(RepositoryContexts.DRAFT);

    ProductPageDraft productPageWithoutCountry1 = new ProductPageDraft();
    productPageWithoutCountry1.setId(1L);
    productPageWithoutCountry1.setSort(1);

    ProductPageDraft productPageWithoutCountry2 = new ProductPageDraft();
    productPageWithoutCountry2.setId(2L);
    productPageWithoutCountry2.setSort(2);

    ProductPageDraft productPageWithCountry = new ProductPageDraft();
    productPageWithCountry.setId(2L);
    productPageWithCountry.setSort(2);

    ProductPagePredicates<ProductPageDraft> productPagePredicates = new ProductPagePredicates<>(
      ProductPageDraft.class, "productPageDraft");
    Predicate allMatchingProductsPredicate = productPagePredicates
      .getAllMatchingProductsPredicate(1L);
    Predicate countryCodeMatchingProductsPredicate = productPagePredicates
      .getMatchingProductsForCountryPredicate(1L, COUNTRY_CODE);

    Pageable pageable = new PageRequest(0, 15);
    ProductPageRepositoryDraft productPageRepositoryDraft = mock(ProductPageRepositoryDraft.class);
    when(productPageRepositoryDraft.findAll(allMatchingProductsPredicate))
      .thenReturn(
        new PageImpl<>(Arrays.asList(productPageWithoutCountry1, productPageWithoutCountry2)));
    when(productPageRepositoryDraft.findAll(countryCodeMatchingProductsPredicate))
      .thenReturn(new PageImpl<>(Collections.singletonList(productPageWithCountry)));
    when(productPageQTypeResolverMock.getEntityClass()).thenReturn(ProductPageDraft.class);
    when(productPageQTypeResolverMock.getQueryDslVariableName()).thenReturn("productPageDraft");

    ProductPageRepositoryProxy proxy = new ProductPageRepositoryProxy(null,
      productPageRepositoryDraft, repositoryContextMock, productPageQTypeResolverMock);
    List<ProductPage> productPageDraftList = proxy
      .getProductsByCategoryId(1L, COUNTRY_CODE, pageable);

    assertNotNull(productPageDraftList);
    assertEquals(productPageWithoutCountry1, productPageDraftList.get(0));
    assertEquals(productPageWithCountry, productPageDraftList.get(1));

    verify(productPageRepositoryDraft).findAll(allMatchingProductsPredicate);
    verify(productPageRepositoryDraft).findAll(countryCodeMatchingProductsPredicate);
  }
}
