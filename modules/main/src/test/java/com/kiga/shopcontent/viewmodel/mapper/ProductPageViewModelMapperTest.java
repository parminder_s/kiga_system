package com.kiga.shopcontent.viewmodel.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.shopcontent.domain.ProductPage;
import com.kiga.shopcontent.viewmodel.ProductPageViewModel;
import com.kiga.shopcontent.viewmodel.converter.ProductPageToResponseConverter;
import com.kiga.web.service.GeoIpService;
import org.junit.Test;

/**
 * Created by asv on 10.11.16.
 */
public class ProductPageViewModelMapperTest {
  private static final String COUNTRY_CODE = "DE";

  @Test
  public void test() throws Exception {
    GeoIpService geoIpServiceMock = mock(GeoIpService.class);
    when(geoIpServiceMock.resolveCountryCode()).thenReturn(COUNTRY_CODE);
    ProductPageToResponseConverter productPageToResponseConverterMock = mock(
      ProductPageToResponseConverter.class);

    ProductPage productPageMock = mock(ProductPage.class);
    ProductPageViewModel productPageViewModel = new ProductPageViewModel();

    ProductPageViewModelMapper productPageViewModelMapper = new ProductPageViewModelMapper(
      geoIpServiceMock, productPageToResponseConverterMock);
    when(productPageToResponseConverterMock.convertToResponse(productPageMock, COUNTRY_CODE))
      .thenReturn(productPageViewModel);

    QueryRequestParameters<ProductPage> queryRequestParametersMock = mock(
      QueryRequestParameters.class);
    when(queryRequestParametersMock.getEntity()).thenReturn(productPageMock);
    ProductPageViewModel returned = productPageViewModelMapper
      .buildViewModel(null, null, queryRequestParametersMock);

    assertNotNull(returned);
    assertEquals(productPageViewModel, returned);
  }
}
