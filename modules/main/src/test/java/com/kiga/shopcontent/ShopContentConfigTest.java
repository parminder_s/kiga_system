package com.kiga.shopcontent;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;

import com.kiga.shop.backend.web.converter.ProductDetailsToViewModelConverter;
import com.kiga.shop.backend.web.converter.ProductToViewModelConverter;
import com.kiga.shop.service.DefaultProductService;
import com.kiga.shopcontent.repository.ShopCategoryRepositoryProxy;
import com.kiga.shopcontent.service.ShopCategoryService;
import com.kiga.shopcontent.viewmodel.converter.ShopCategoryToResponseConverter;
import org.junit.Test;

/**
 * Created by rainerh on 08.11.16.
 */
public class ShopContentConfigTest {
  private ShopContentConfig shopContentConfig = new ShopContentConfig();

  @Test
  public void testCreateShopCategoryToResponseConverter() {
    ShopCategoryToResponseConverter shopCategoryToResponseConverter = shopContentConfig
      .createShopCategoryToResponseConverter();
    assertThat(shopCategoryToResponseConverter,
      instanceOf(ShopCategoryToResponseConverter.class));
  }

  @Test
  public void testCreateShopCategoryModel() {
    ShopCategoryRepositoryProxy shopCategoryModel = shopContentConfig
      .createShopCategoryModel(null, null, null);
    assertThat(shopCategoryModel, instanceOf(ShopCategoryRepositoryProxy.class));
  }

  @Test
  public void testCreateShopCategoryService() {
    ShopCategoryService shopCategoryService =
      shopContentConfig.createShopCategoryService(null, null);
    assertThat(shopCategoryService, instanceOf(ShopCategoryService.class));
  }

  @Test
  public void testCreateProductToViewModelConverter() {
    assertThat(shopContentConfig.createProductToViewModelConverter(),
      instanceOf(ProductToViewModelConverter.class));
  }
}
