package com.kiga.shopcontent.repository;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.RepositoryContexts;
import com.kiga.shopcontent.repository.abstraction.PromotionRepository;
import com.kiga.shopcontent.repository.draft.PromotionRepositoryDraft;
import com.kiga.shopcontent.repository.live.PromotionRepositoryLive;
import org.junit.Test;

/**
 * Created by asv on 29.11.16.
 */
public class PromotionRepositoryProxyTest {
  private static final long ID = 1L;

  @Test
  public void test() {
    PromotionRepository liveRepository = mock(PromotionRepositoryLive.class);
    PromotionRepository draftRepository = mock(PromotionRepositoryDraft.class);
    RepositoryContext repositoryContextMock = mock(RepositoryContext.class);
    when(repositoryContextMock.getContext()).thenReturn(RepositoryContexts.LIVE);
    PromotionRepositoryProxy promotionRepositoryProxy = new PromotionRepositoryProxy(liveRepository,
      draftRepository, repositoryContextMock);
    promotionRepositoryProxy.findByPageId(ID);
    verify(liveRepository).findByPageId(ID);
  }
}
