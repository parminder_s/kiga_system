package com.kiga.shopcontent.domain;

import com.google.code.beanmatchers.BeanMatchers;

import org.junit.Assert;

/**
 * Created by rainerh on 08.11.16.
 */
public class SliderFileTest {
  public void testBean() {
    Assert.assertThat(SliderFile.class, BeanMatchers.hasValidGettersAndSetters());
  }

}
