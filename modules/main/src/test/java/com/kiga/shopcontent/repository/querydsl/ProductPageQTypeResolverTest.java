package com.kiga.shopcontent.repository.querydsl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.RepositoryContexts;
import com.kiga.shopcontent.domain.draft.ProductPageDraft;
import com.kiga.shopcontent.domain.live.ProductPageLive;
import org.junit.Test;

/**
 * @author bbs
 * @since 3/23/17.
 */
public class ProductPageQTypeResolverTest {
  private RepositoryContext repositoryContextMock = mock(RepositoryContext.class);
  private ProductPageQTypeResolver productPageQTypeResolver = new ProductPageQTypeResolver(
    repositoryContextMock);

  @Test
  public void testGetEntityClassLive() {
    when(repositoryContextMock.getContext()).thenReturn(RepositoryContexts.LIVE);
    assertEquals(ProductPageLive.class, productPageQTypeResolver.getEntityClass());
  }

  @Test
  public void testGetEntityClassDraft() {
    when(repositoryContextMock.getContext()).thenReturn(RepositoryContexts.DRAFT);
    assertEquals(ProductPageDraft.class, productPageQTypeResolver.getEntityClass());
  }

  @Test
  public void testGetQueryDslVariableNameLive() {
    when(repositoryContextMock.getContext()).thenReturn(RepositoryContexts.LIVE);
    assertEquals("productPageLive", productPageQTypeResolver.getQueryDslVariableName());
  }

  @Test
  public void testGetQueryDslVariableNameDraft() {
    when(repositoryContextMock.getContext()).thenReturn(RepositoryContexts.DRAFT);
    assertEquals("productPageDraft", productPageQTypeResolver.getQueryDslVariableName());
  }
}
