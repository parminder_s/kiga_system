package com.kiga.shopcontent.viewmodel.mapper;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Iterables;

import com.kiga.cms.service.ViewModelMapper;
import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.cms.web.response.Breadcrumb;
import com.kiga.shop.web.response.ShopViewModel;
import com.kiga.shopcontent.domain.ShopCategory;
import com.kiga.shopcontent.domain.live.ShopCategoryLive;
import com.kiga.shopcontent.service.ProductPageService;
import com.kiga.shopcontent.service.ShopCategoryService;
import com.kiga.shopcontent.viewmodel.ShopCategoryProductViewModel;
import com.kiga.shopcontent.viewmodel.ShopCategoryViewModel;
import com.kiga.web.service.DefaultGeoIpService;
import com.kiga.web.service.GeoIpService;
import org.apache.commons.collections4.CollectionUtils;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 9/4/16.
 */
public class ShopCategoryViewModelMapperTest {
  private HttpServletRequest httpServletRequestMock = mock(HttpServletRequest.class);
  private HttpServletResponse httpServletResponseMock = mock(HttpServletResponse.class);
  private ViewModelMapper<ShopCategory, ShopViewModel> viewModelMapper;
  private ShopCategoryService shopCategoryService;
  private ProductPageService productPageService;

  /**
   * Prepare tests.
   */
  @Before
  public void init() {
    shopCategoryService = mock(ShopCategoryService.class);
    productPageService = mock(ProductPageService.class);
    GeoIpService geoIpService = mock(DefaultGeoIpService.class);
    when(geoIpService.resolveCountryCode()).thenReturn("DE");
    viewModelMapper = new ShopCategoryViewModelMapper(shopCategoryService, productPageService,
      geoIpService);
  }

  @Test
  public void testNoBreadcrumbsPreparation() throws Exception {
    ShopCategoryLive shopCategoryLive = new ShopCategoryLive();
    shopCategoryLive.setClassName("ShopMainContainer");
    shopCategoryLive.setUrlSegment("top-level");
    shopCategoryLive.setTitle("top-level-title");

    ShopCategoryLive shopCategoryLiveParent = new ShopCategoryLive();
    shopCategoryLiveParent.setUrlSegment("homepage");
    shopCategoryLiveParent.setTitle("homepage");
    shopCategoryLive.setParent(shopCategoryLiveParent);

    QueryRequestParameters queryRequestParameters =
      QueryRequestParameters.builder()
        .entity(shopCategoryLive)
        .build();

    ShopCategoryViewModel shopCategoryResponse = new ShopCategoryViewModel();
    when(shopCategoryService.getCategory(Matchers.anyLong()))
      .thenReturn(shopCategoryResponse);

    ShopViewModel response = viewModelMapper
      .getViewModel(httpServletRequestMock, httpServletResponseMock, queryRequestParameters);

    Assert.assertTrue(CollectionUtils.isEmpty(response.getBreadcrumbs()));
  }

  @Test
  public void testBreadcrumbsPreparation() throws Exception {
    ShopCategoryLive shopCategoryLive = new ShopCategoryLive();
    shopCategoryLive.setClassName("ShopCategory");
    shopCategoryLive.setUrlSegment("top-level");
    shopCategoryLive.setTitle("top-level-title");

    ShopCategoryLive shopCategoryLiveParent = new ShopCategoryLive();
    shopCategoryLive.setClassName("ShopMainContainer");
    shopCategoryLiveParent.setUrlSegment("parent");
    shopCategoryLiveParent.setTitle("parent-title");
    shopCategoryLive.setParent(shopCategoryLiveParent);

    ShopCategoryLive shopCategoryLiveParentParent = new ShopCategoryLive();
    shopCategoryLiveParentParent.setUrlSegment("ShopMainContainer");
    shopCategoryLiveParentParent.setTitle("ShopMainContainer");
    shopCategoryLiveParent.setParent(shopCategoryLiveParentParent);

    ShopCategoryLive shopCategoryLiveParentParentParent = new ShopCategoryLive();
    shopCategoryLiveParentParentParent.setUrlSegment("homepage");
    shopCategoryLiveParentParent.setParent(shopCategoryLiveParentParentParent);

    QueryRequestParameters queryRequestParameters =
      QueryRequestParameters.builder()
        .entity(shopCategoryLive)
        .build();

    ShopCategoryViewModel shopCategoryResponse = new ShopCategoryViewModel();
    when(shopCategoryService.getCategory(Matchers.anyLong())).thenReturn(shopCategoryResponse);

    ShopViewModel response = viewModelMapper
      .getViewModel(httpServletRequestMock, httpServletResponseMock, queryRequestParameters);

    List<Breadcrumb> expectedBreadcrumbs = new ArrayList<>();
    expectedBreadcrumbs.add(new Breadcrumb("parent-title", "parent"));
    expectedBreadcrumbs.add(new Breadcrumb("top-level-title", "top-level"));

    Assert.assertTrue(Iterables.elementsEqual(expectedBreadcrumbs, response.getBreadcrumbs()));
  }

  @Test
  public void testNotLastLevelCategory() throws Exception {
    ShopCategoryLive shopCategoryLive = new ShopCategoryLive();
    shopCategoryLive.setId(123L);
    shopCategoryLive.setClassName("ShopMainContainer");
    shopCategoryLive.setUrlSegment("top-level");

    ShopCategoryLive shopCategoryLiveParent = new ShopCategoryLive();
    shopCategoryLiveParent.setUrlSegment("parent");
    shopCategoryLive.setParent(shopCategoryLiveParent);

    ShopCategoryLive shopCategoryLiveParentParent = new ShopCategoryLive();
    shopCategoryLiveParentParent.setUrlSegment("parent-parent");
    shopCategoryLiveParent.setParent(shopCategoryLiveParentParent);


    ShopCategoryViewModel subCategory1 = new ShopCategoryViewModel();
    subCategory1.setIcon("sub1");
    ShopCategoryViewModel subCategory2 = new ShopCategoryViewModel();
    subCategory2.setIcon("sub2");
    ShopCategoryViewModel subCategory3 = new ShopCategoryViewModel();
    subCategory3.setIcon("sub3");

    QueryRequestParameters queryRequestParameters =
      QueryRequestParameters.builder()
        .entity(shopCategoryLive)
        .build();

    List<ShopCategoryViewModel> subCategories = Arrays
      .asList(subCategory1, subCategory2, subCategory3);
    ShopCategoryViewModel shopCategoryResponse = new ShopCategoryViewModel();
    when(shopCategoryService.getCategory(shopCategoryLive.getId()))
      .thenReturn(shopCategoryResponse);
    when(shopCategoryService.getSubcategories(shopCategoryResponse)).thenReturn(subCategories);

    ShopCategoryProductViewModel productPage = new ShopCategoryProductViewModel();
    when(productPageService.getProductsByCategory(subCategory1, "DE"))
      .thenReturn(Collections.singletonList(productPage));

    when(productPageService.getProductsByCategory(subCategory2, "DE"))
      .thenReturn(Collections.emptyList());
    when(productPageService.getProductsByCategory(subCategory3, "DE"))
      .thenReturn(Collections.singletonList(productPage));

    ShopViewModel response = viewModelMapper
      .getViewModel(httpServletRequestMock, httpServletResponseMock, queryRequestParameters);
    Assert.assertNotNull(response);
    assertEquals(shopCategoryResponse, response.getActiveCategory());
    Assert.assertThat(response, IsInstanceOf.instanceOf(ShopViewModel.class));
    Assert.assertFalse(response.getActiveCategory().isLastCategoryLevel());
    Assert.assertTrue(Iterables
      .elementsEqual(Arrays.asList(subCategory1, subCategory3), response.getSubcategories()));

    verify(shopCategoryService).getCategory(Matchers.eq(shopCategoryLive.getId()));
    verify(shopCategoryService).getSubcategories(shopCategoryResponse);
    verify(productPageService).getProductsByCategory(shopCategoryResponse, "DE");
    verify(productPageService).getProductsByCategory(subCategory1, "DE");
    verify(productPageService).getProductsByCategory(subCategory2, "DE");
    verify(productPageService).getProductsByCategory(subCategory3, "DE");
  }

  @Test
  public void testLastLevelCategory() throws Exception {
    ShopCategoryLive shopCategoryLive = new ShopCategoryLive();
    shopCategoryLive.setId(123L);
    shopCategoryLive.setClassName("ShopMainContainer");
    shopCategoryLive.setUrlSegment("top-level");

    ShopCategoryLive shopCategoryLiveParent = new ShopCategoryLive();
    shopCategoryLiveParent.setUrlSegment("parent");
    shopCategoryLive.setParent(shopCategoryLiveParent);

    ShopCategoryLive shopCategoryLiveParentParent = new ShopCategoryLive();
    shopCategoryLiveParentParent.setUrlSegment("parent-parent");
    shopCategoryLiveParent.setParent(shopCategoryLiveParentParent);

    QueryRequestParameters queryRequestParameters =
      QueryRequestParameters.builder()
        .entity(shopCategoryLive)
        .build();

    ShopCategoryViewModel subCategory = new ShopCategoryViewModel();
    List<ShopCategoryViewModel> subCategories = Collections.singletonList(subCategory);
    ShopCategoryViewModel shopCategoryResponse = new ShopCategoryViewModel();
    when(shopCategoryService.getCategory(shopCategoryLive.getId()))
      .thenReturn(shopCategoryResponse);
    when(shopCategoryService.getSubcategories(shopCategoryResponse)).thenReturn(subCategories);
    when(productPageService.getProductsByCategory(subCategory, "DE"))
      .thenReturn(Collections.emptyList());

    ShopViewModel response = viewModelMapper
      .getViewModel(httpServletRequestMock, httpServletResponseMock, queryRequestParameters);

    Assert.assertNotNull(response);
    assertEquals(shopCategoryResponse, response.getActiveCategory());
    Assert.assertThat(response, IsInstanceOf.instanceOf(ShopViewModel.class));
    Assert.assertTrue(response.getActiveCategory().isLastCategoryLevel());
    assertEquals(1, response.getSubcategories().size());
    assertEquals(shopCategoryResponse, response.getSubcategories().get(0));

    verify(shopCategoryService).getCategory(Matchers.eq(shopCategoryLive.getId()));
    verify(shopCategoryService).getSubcategories(shopCategoryResponse);
    verify(productPageService).getProductsByCategory(shopCategoryResponse, "DE");
    verify(productPageService).getProductsByCategory(subCategory, "DE");
  }
}
