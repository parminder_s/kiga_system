package com.kiga.shopcontent.viewmodel;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanHashCode;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * @author bbs
 * @since 9/4/16.
 */
public class ShopCategoryProductViewModelTest {
  @Test
  public void testBean() {
    assertThat(ShopCategoryProductViewModel.class,
      allOf(hasValidGettersAndSetters(), hasValidBeanHashCode()));
  }
}
