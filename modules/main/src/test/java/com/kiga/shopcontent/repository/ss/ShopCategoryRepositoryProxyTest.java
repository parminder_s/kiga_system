package com.kiga.shopcontent.repository.ss;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.shopcontent.repository.ShopCategoryRepositoryProxy;
import com.kiga.shopcontent.repository.abstraction.ShopCategoryRepository;
import com.kiga.shopcontent.repository.live.ShopCategoryRepositoryLive;
import org.junit.Before;
import org.junit.Test;

/**
 * @author bbs
 * @since 9/6/16.
 */
public class ShopCategoryRepositoryProxyTest {
  private ShopCategoryRepositoryProxy proxy;
  private ShopCategoryRepository shopCategoryRepositoryLive = mock(
    ShopCategoryRepositoryLive.class);

  @Before
  public void init() {
    proxy = new ShopCategoryRepositoryProxy(shopCategoryRepositoryLive, null,
      new RepositoryContext(null));
  }

  @Test
  public void testGetSubcategories() {
    proxy.getSubcategories(1L);
    verify(shopCategoryRepositoryLive).getSubcategories(1L);
  }

  @Test
  public void testFindOne() {
    proxy.findOne(2L);
    verify(shopCategoryRepositoryLive).findOne(2L);
  }
}
