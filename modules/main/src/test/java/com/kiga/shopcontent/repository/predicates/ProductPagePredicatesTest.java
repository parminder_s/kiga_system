package com.kiga.shopcontent.repository.predicates;

import static org.junit.Assert.assertEquals;

import com.kiga.shopcontent.domain.draft.ProductPageDraft;
import com.mysema.query.types.Predicate;
import org.junit.Test;

/**
 * @author bbs
 * @since 2/4/17.
 */
public class ProductPagePredicatesTest {

  public static final String EXPECTED_ALL_MATCHING_PREDICATE = "productPageDraft.parent.id = 1 ||"
    + " productPageDraft.parent.parent.id = 1 || productPageDraft.parent.parent.parent.id = 1";
  public static final String EXPECTED_MATCHING_FOR_COUNTRY_PREDICATE = "upper(any"
    + "(productPageDraft.product.productDetails).country.code) = DE && (productPageDraft.parent"
    + ".id = 1 || productPageDraft.parent.parent.id = 1 || productPageDraft.parent.parent.parent"
    + ".id = 1)";

  @Test
  public void test() {
    ProductPagePredicates<ProductPageDraft> productPagePredicates = new ProductPagePredicates<>(
      ProductPageDraft.class, "productPageDraft");

    Predicate allMAtchingProducts = productPagePredicates.getAllMatchingProductsPredicate(1L);
    Predicate matchingProductsForCountryPredicate = productPagePredicates
      .getMatchingProductsForCountryPredicate(1L, "DE");

    assertEquals(EXPECTED_ALL_MATCHING_PREDICATE, allMAtchingProducts.toString());
    assertEquals(EXPECTED_MATCHING_FOR_COUNTRY_PREDICATE,
      matchingProductsForCountryPredicate.toString());
  }
}
