package com.kiga.logging;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

import com.kiga.logging.domain.LinkLog;
import com.kiga.logging.domain.PrintLog;
import com.kiga.logging.domain.SearchTerm;
import com.kiga.logging.domain.TransitionLog;
import com.kiga.logging.repositories.LinkLogRepository;
import com.kiga.logging.repositories.PrintLogRepository;
import com.kiga.logging.repositories.SearchTermRepository;
import com.kiga.logging.repositories.TransitionLogRepository;
import com.kiga.logging.requests.LinkLogRequest;
import com.kiga.logging.requests.PrintLogRequest;
import com.kiga.logging.requests.PrintLogSsRequest;
import com.kiga.logging.requests.TransitionRequest;
import com.kiga.logging.services.LogService;
import com.kiga.main.locale.Locale;
import com.kiga.security.services.SecurityService;
import com.kiga.web.service.GeoIpService;
import com.kiga.web.service.LocateByIpResolver;
import com.kiga.web.service.UserIpService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by faxxe on 10/12/16.
 */
public class LogServiceTest {

  private LogService logService;
  private SecurityService securityService;
  private UserIpService userIpService;
  private GeoIpService geoIpService;
  private LogController logController;

  /**
   * setup.
   */
  @Before
  public void setUpTests() {
    securityService = Mockito.mock(SecurityService.class);
    LinkLogRepository linkLogRepository = Mockito.mock(LinkLogRepository.class);
    SearchTermRepository searchTermRepository = Mockito.mock(SearchTermRepository.class);
    PrintLogRepository printLogRepository = Mockito.mock(PrintLogRepository.class);
    TransitionLogRepository transitionLogRepository = Mockito.mock(TransitionLogRepository.class);

    userIpService = Mockito.mock(UserIpService.class);
    geoIpService = Mockito.mock(GeoIpService.class);
    LocateByIpResolver locateByIpResolver = Mockito.mock(LocateByIpResolver.class);

    logService = new LogService(null, securityService, linkLogRepository,
      searchTermRepository, printLogRepository,
      transitionLogRepository, userIpService, geoIpService, locateByIpResolver);

    logController = new LogController(logService);
  }

  @Test
  public void testLogServiceLogLink() {
    HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
    LinkLogRequest linkLogRequest = Mockito.mock(LinkLogRequest.class);
    when(linkLogRequest.getLocale()).thenReturn(Locale.en);
    logController.logLink(httpServletRequest, linkLogRequest);

    Mockito.verify(geoIpService, Mockito.times(1)).resolveCountryCode();
    Mockito.verify(userIpService, Mockito.times(1)).getUserIp(httpServletRequest);
    Mockito.verify(securityService, Mockito.times(1)).getSessionIdFromCookie();
  }

  @Test
  public void testLogSearchTerm() {
    HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
    SearchTerm searchTerm = Mockito.mock(SearchTerm.class);
    logService.logSearchTerm(httpServletRequest, Locale.de, searchTerm);

    Mockito.verify(geoIpService, Mockito.times(1)).resolveCountryCode();
    Mockito.verify(userIpService, Mockito.times(1)).getUserIp(httpServletRequest);
    Mockito.verify(securityService, Mockito.times(1)).getSessionIdFromCookie();
  }

  @Test
  public void testLogPrint() {
    HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
    PrintLogRequest printLogRequest = new PrintLogRequest();
    printLogRequest.setLocale(Locale.en);
    logController.logPrint(httpServletRequest, printLogRequest);

    Mockito.verify(geoIpService, Mockito.times(1)).resolveCountryCode();
    Mockito.verify(userIpService, Mockito.times(1)).getUserIp(httpServletRequest);
    Mockito.verify(securityService, Mockito.times(1)).getSessionIdFromCookie();
  }

  @Test
  public void testLogPrintSs() {
    PrintLogSsRequest printLogSsRequest = Mockito.mock(PrintLogSsRequest.class);
    when(printLogSsRequest.getLocale()).thenReturn(Locale.en);

    logController.logPrintSs(printLogSsRequest);

    Mockito.verify(printLogSsRequest, Mockito.times(1)).getArticleId();
    Mockito.verify(printLogSsRequest, Mockito.times(2)).getIp();
    Mockito.verify(printLogSsRequest, Mockito.times(1)).getSessionId();
    Mockito.verify(printLogSsRequest, Mockito.times(1)).getLocale();
    Mockito.verify(printLogSsRequest, Mockito.times(1)).getTitle();
    Mockito.verify(printLogSsRequest, Mockito.times(1)).getUrl();

  }

  @Test
  public void testTransitionLog() {
    HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
    TransitionRequest transitionRequest = Mockito.mock(TransitionRequest.class);

    when(transitionRequest.getLocale()).thenReturn(Locale.en);

    logController.logTransition(httpServletRequest, transitionRequest);
  }

  @Test
  public void testLogConfig() {
    LogConfig logConfig = new LogConfig();

    LogService logService = logConfig.logService(null, null, null, null, null, null,
      null, null, null);
    Assert.assertThat(logService, instanceOf(LogService.class));

  }

  @Test
  public void testBean() {
    List<Class> classesToTest = Arrays.asList(
      LinkLog.class,
      PrintLog.class,
      SearchTerm.class,
      TransitionLog.class
    );

    classesToTest.forEach(x ->
      assertThat(x, allOf(hasValidBeanConstructor(), hasValidGettersAndSetters())));

  }

}
