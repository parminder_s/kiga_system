package com.kiga.accounting.transactions.domain;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanEqualsExcluding;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanHashCodeExcluding;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanToStringExcluding;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * @author bbs
 * @since 2/8/17.
 */
public class PaymentTransactionTest {
  @Test
  public void testBean() {
    assertThat(PaymentTransaction.class, allOf(hasValidGettersAndSetters(),
      hasValidBeanEqualsExcluding("className", "created", "lastEdited", "id"),
      hasValidBeanHashCodeExcluding("className", "created", "lastEdited", "id"),
      hasValidBeanToStringExcluding("className", "created", "lastEdited", "id")));
  }
}
