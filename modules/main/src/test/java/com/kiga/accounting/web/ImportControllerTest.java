package com.kiga.accounting.web;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;

import com.kiga.accounting.domain.AcquirerTransaction;
import com.kiga.accounting.formats.ImportFormat;
import com.kiga.accounting.repository.AcquirerTransactionRepository;
import com.kiga.accounting.web.request.ImportRequest;
import com.kiga.accounting.web.response.ImportResponse;
import com.kiga.payment.repository.PaymentRepository;
import com.kiga.shop.repository.PurchaseRepository;
import java.io.UnsupportedEncodingException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/** Tests for the (import)transactions controller. Created by mfit on 14.06.16. */
public class ImportControllerTest {
  @Rule public ExpectedException thrown = ExpectedException.none();
  PurchaseRepository purchaseRepository = mock(PurchaseRepository.class);
  PaymentRepository paymentRepository = mock(PaymentRepository.class);
  AcquirerTransactionRepository acquirerTransactionRepository =
      mock(AcquirerTransactionRepository.class);
  private AcquirerTransactionController controller;

  private String cardCompleteValidContent =
      "Chain;Vertragsnummer;Terminal-ID;Umsatzdatum;Einreichdatum;Verarbeitungsdatum;"
          + "Abrechnungsdatum;Einreichsumme;Produkt;Batchnummer;Kartennummer;Belegnummer;"
          + "Genehmigungsnr.;Kauf-/Umsatzbetrag;W�hrung;Originalbetrag;Orig. W�hrung;"
          + "Akzeptanzpartner;Auftragsnummer;Stammkundennummer;Interchange-Kategorie;"
          + "Interchange Fee*;Scheme Fee*;Acquiring Fee (%)*;Acquiring Fee (EUR)*;"
          + "Special Fee (%)*;Special Fee (EUR)*;Interchange (%);Scheme Fee (%);Acquiring Fee (%);"
          + "Acquiring Fee (EUR);Special Fee (%);Special Fee (EUR)\n"
          + ";100528702;;01.03.2016;01.03.2016;02.03.2016;30.03.2016;338,27;VISA;10896103849;"
          + "4920 XXXX XXXX 4671;104928;015340;7,90;EUR;7,90;EUR;WWW.KIGAPORTAL.COM       GRAZ;"
          + "64172744;;;0,000000;0,000000;0,000000;0,000000;0,000000;0,000000;0,0000;0,0000;0,0000;"
          + "0,00;0,0000;0,00";

  private String cardCompleteAlternateContent =
      "Chain;Vertragsnummer;Umsatzdatum;Einreichdatum;Verarbeitungsdatum;"
          + "Abrechnungsdatum;Einreichsumme;Produkt;Batchnummer;Kartennummer;Belegnummer;"
          + "Genehmigungsnr.;Kauf-/Umsatzbetrag;Währung;Originalbetrag;Orig. Währung;"
          + "Akzeptanzpartner;Auftragsnummer;Stammkundennummer;Interchange-Kategorie;"
          + "Interchange Fee*\n"
          + "100609304;100609338;3/20/2017;3/20/2017;3/21/2017;4/4/2017;40.99;Visa;"
          + "14252871489;4906 XXXX XXXX 9100;533577;6354;40.99;EUR;40.99;EUR;"
          + "KIGAPORTAL WEBSHOP       GRAZ;85740357;;ECS;-0.12297";

  private String sofortValidContent =
      "Transaction.user_id;Transaction.project_id;Transaction.transaction;"
          + "Transaction.sender_holder;Transaction.sender_bank_holder;"
          + "Transaction.sender_account_number;Transaction.sender_bank_code;"
          + "Transaction.sender_bank_name;Transaction.sender_bank_bic;Transaction.sender_iban;"
          + "Transaction.sender_country_id;Transaction.recipient_holder;"
          + "Transaction.recipient_account_number;Transaction.recipient_bank_code;"
          + "Transaction.recipient_bank_name;Transaction.recipient_bank_bic;Transaction.recipient_iban;"
          + "Transaction.recipient_country_id;Transaction.amount;Transaction.amount_refunded;"
          + "Transaction.currency_id;Transaction.exchange_rate;Transaction.reason_1;"
          + "Transaction.reason_2;Transaction.test_transaction;Transaction.notification_status;"
          + "Transaction.notification_attempts;Transaction.notification_last_attempt;"
          + "Transaction.status;Transaction.status_modified;Transaction.user_costs;"
          + "Transaction.user_costs_currency_id;Transaction.user_costs_guarantee;Transaction.created;"
          + "Transaction.internationalization_fees\n"
          + "98641;203249;98641-203249-56F3C72F-A9B6;;Magdalena Leder;12983731301;20715;"
          + "SPARKASSE / ERSTE BANK;STSPAT2GXXX;AT262081512562301301;AT;"
          + "HOHE Medien OG;00040874216;20815;SPARKASSE / ERSTE BANK;STSPAT2GXXX;AT742081500040874216;"
          + "AT;39,90;0,00;EUR;1,0000;000065329340MPAY24;;;1;1;2016-03-24 11:58:14;untraceable;"
          + "2016-03-24 11:58:12;1,05;EUR;0.00;2016-03-24 11:58:12;0\n";

  private String invalidContent = "Some;Header;Fields\n" + "ABC;1000;2000";

  @Test
  public void emptyRequestThrowsTest() throws UnsupportedEncodingException {
    thrown.expect(NullPointerException.class);
    controller =
        new AcquirerTransactionController(
            purchaseRepository, paymentRepository, acquirerTransactionRepository);
    controller.importCsv(new ImportRequest());
  }

  @Test
  public void parseInvalidCsvReturnsNumberOfErrorsTest() throws UnsupportedEncodingException {
    controller =
        new AcquirerTransactionController(
            purchaseRepository, paymentRepository, acquirerTransactionRepository);
    ImportRequest request = new ImportRequest();
    request.setCsv(invalidContent);
    request.setDoImport(false);
    request.setFormatCode(ImportFormat.CC);

    ImportResponse response = controller.importCsv(request);

    Assert.assertEquals(1, response.getRowsParsed());
    Assert.assertEquals(0, response.getRowsPersisted());
    Assert.assertEquals(0, response.getRowsError());
  }

  @Test
  public void parseCardCompleteRowsTest() throws UnsupportedEncodingException {

    AcquirerTransactionRepository acquirerTransactionRepository =
        mock(AcquirerTransactionRepository.class);
    controller =
        new AcquirerTransactionController(
            purchaseRepository, paymentRepository, acquirerTransactionRepository);

    ImportRequest request = new ImportRequest();
    request.setCsv(cardCompleteValidContent);
    request.setDoImport(false);
    request.setFormatCode(ImportFormat.CC);

    ImportResponse response = controller.importCsv(request);

    Assert.assertEquals(1, response.getRowsParsed());
    Assert.assertEquals(0, response.getRowsPersisted());
    Assert.assertEquals(0, response.getRowsError());

    AcquirerTransaction transaction = response.getData().get(0);
    Assert.assertEquals("cardcomplete", transaction.getAcquirerName());

    verify(acquirerTransactionRepository, never()).save(any(AcquirerTransaction.class));
  }

  @Test
  public void persistCardCompleteTransactionsTest() throws UnsupportedEncodingException {

    AcquirerTransactionRepository acquirerTransactionRepository =
        mock(AcquirerTransactionRepository.class);
    controller =
        new AcquirerTransactionController(
            purchaseRepository, paymentRepository, acquirerTransactionRepository);

    ImportRequest request = new ImportRequest();
    request.setCsv(cardCompleteValidContent);
    request.setFormatCode(ImportFormat.CC);
    request.setDoImport(true);

    ImportResponse response = controller.importCsv(request);

    Assert.assertEquals(1, response.getRowsParsed());
    Assert.assertEquals(1, response.getRowsPersisted());
    Assert.assertEquals(0, response.getRowsError());
    verify(acquirerTransactionRepository, atLeastOnce()).save(any(AcquirerTransaction.class));
  }

  @Test
  public void persistSofortTransactionsTest() throws UnsupportedEncodingException {

    AcquirerTransactionRepository acquirerTransactionRepository =
        mock(AcquirerTransactionRepository.class);
    controller =
        new AcquirerTransactionController(
            purchaseRepository, paymentRepository, acquirerTransactionRepository);

    ImportRequest request = new ImportRequest();
    request.setCsv(sofortValidContent);
    request.setFormatCode(ImportFormat.SOFORT);
    request.setDoImport(true);

    ImportResponse response = controller.importCsv(request);

    Assert.assertEquals(1, response.getRowsParsed());
    Assert.assertEquals(1, response.getRowsPersisted());
    Assert.assertEquals(0, response.getRowsError());
    verify(acquirerTransactionRepository, atLeastOnce()).save(any(AcquirerTransaction.class));
  }
}
