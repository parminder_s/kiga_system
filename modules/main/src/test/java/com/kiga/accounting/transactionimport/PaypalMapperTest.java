package com.kiga.accounting.transactionimport;

import com.kiga.accounting.domain.AcquirerTransaction;
import com.kiga.accounting.formats.CsvTransactionImporter;
import com.kiga.accounting.formats.ImportFormat;
import com.kiga.accounting.formats.TransactionImporterFactory;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by mfit on 14.06.17.
 */
public class PaypalMapperTest {

  public static DateFormat dtf = new SimpleDateFormat("yyyyMMdd");

  @Test
  public void paypalImportFromString() throws UnsupportedEncodingException,
    ParseException {
    String content = "\"Datum\",\"Uhrzeit\",\"Zeitzone\",\"Beschreibung\",\"Währung\",\"Brutto\","
      + "\"Gebühr\",\"Netto\",\"Guthaben\",\"Transaktionscode\",\"Absender E-Mail-Adresse\","
      + "\"Name\",\"Name der Bank\",\"Bankkonto\",\"Versand- und Bearbeitungsgebühr\","
      + "\"Umsatzsteuer\",\"Rechnungsnummer\",\"Zugehöriger Transaktionscode\"\n"

      + "\"5/10/2017\",\"06:14:04\",\"Europe/Berlin\",\"PayPal Express-Zahlung\",\"EUR\","
      + "\"16.90\",\"-0.92\",\"15.98\",\"3,229.55\",\"0900083133707971E\",\"seefahrer1@chello.at\","
      + "\"Heinrich Löwy\",\"\",\"\",\"0.00\",\"0.00\",\"30540 / 88648452\",\"\"\n"

      + "\"5/10/2017\",\"06:54:51\",\"Europe/Berlin\",\"PayPal Express-Zahlung\",\"EUR\","
      + "\"28.80\",\"-1.33\",\"27.47\",\"3,257.02\",\"61S49182A0251935X\","
      + "\"vendula_rosan@online.de\",\"Vendula Rosan\",\"\",\"\",\"0.00\",\"0.00\","
      + "\"30541 / 88649262\",\"\"\n"

      ;

    ArrayList<AcquirerTransaction> items =
      (ArrayList<AcquirerTransaction>) CsvTransactionImporter.readItemsFromString(content,
        ImportFormat.PAYPAL);

    Assert.assertEquals(2, items.size());

    Assert.assertEquals(16.90, items.get(0).getAmount().doubleValue(), 0.01);

    Assert.assertEquals(Arrays.asList(16.90, 28.80),
      items.stream().map(ta -> ta.getAmountOrig().doubleValue()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("EUR", "EUR"),
      items.stream().map(ta -> ta.getCurrencyOrig()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("EUR", "EUR"),
      items.stream().map(ta -> ta.getCurrency()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("88648452", "88649262"),
      items.stream().map(ta -> ta.getPspTransactionId()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(dtf.parse("20170510"), dtf.parse("20170510")),
      items.stream().map(ta -> ta.getSettlementDate()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(dtf.parse("20170510"), dtf.parse("20170510")),
      items.stream().map(ta -> ta.getTransactionDate()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("paypal", "paypal"),
      items.stream().map(ta -> ta.getAcquirerName()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("", ""),
      items.stream().map(ta -> ta.getCardType()).collect(Collectors.toList()));
  }

  @Test
  public void correctlyProcessCurrencyConversionTransactions() throws UnsupportedEncodingException {

    String content = "\"Datum\",\"Uhrzeit\",\"Zeitzone\",\"Beschreibung\",\"Währung\",\"Brutto\","
      + "\"Gebühr\",\"Netto\",\"Guthaben\",\"Transaktionscode\",\"Absender E-Mail-Adresse\","
      + "\"Name\",\"Name der Bank\",\"Bankkonto\",\"Versand- und Bearbeitungsgebühr\","
      + "\"Umsatzsteuer\",\"Rechnungsnummer\",\"Zugehöriger Transaktionscode\"\n"

      + "\"7/14/2017\",\"15:04:22\",\"Europe/Berlin\",\"Allgemeine Währungsumrechnung\",\"EUR\","
      + "\"34.51\",\"0.00\",\"34.51\",\"4,151.39\",\"6H955370SP572164D\",\"\",\"\",\"\",\"\","
      + "\"0.00\",\"0.00\",\"32232 / 92535023\",\"30J69245SE840053M\"\n"

      + "\"7/19/2017\",\"07:37:46\",\"Europe/Berlin\",\"PayPal Express-Zahlung\",\"EUR\",\"29.80\","
      + "\"-1.36\",\"28.44\",\"4,179.83\",\"128374745K7829803\",\"c.email@gmx.at\","
      + "\"Bettina Client\",\"\",\"\",\"0.00\",\"0.00\",\"32352 / 92809751\",\"\"\n"

      + "\"7/23/2017\",\"22:53:18\",\"Europe/Berlin\",\"Allgemeine Währungsumrechnung\",\"EUR\","
      + "\"27.90\",\"0.00\",\"27.90\",\"4,207.73\",\"0NR89705K9239832W\",\"\",\"\",\"\",\"\","
      + "\"0.00\",\"0.00\",\"32451 / 93092849\",\"4T059135JJ095493A\"\n"

      + "\"7/14/2017\",\"15:04:22\",\"Europe/Berlin\",\"PayPal Express-Zahlung\",\"CHF\",\"41.70\","
      + "\"-2.38\",\"39.32\",\"39.32\",\"30J69245SE840053M\",\"m.muster@hotmail.com\","
      + "\"Monika Muster\",\"\",\"\",\"0.00\",\"0.00\",\"32232 / 92535023\",\"\"\n"

      + "\"7/14/2017\",\"15:04:22\",\"Europe/Berlin\",\"Allgemeine Währungsumrechnung\","
      + "\"CHF\",\"-39.32\",\"0.00\",\"-39.32\",\"0.00\",\"03830821879687225\",\"\","
      + "\"\",\"\",\"\",\"0.00\",\"0.00\",\"32232 / 92535023\",\"30J69245SE840053M\"\n"

      + "\"7/23/2017\",\"22:53:18\",\"Europe/Berlin\",\"PayPal Express-Zahlung\",\"CHF\","
      + "\"33.80\",\"-2.04\",\"31.76\",\"31.76\",\"4T059135JJ095493A\","
      + "\"rolf.kunde@hispeed.ch\",\"Rolf Kunde\",\"\",\"\",\"0.00\",\"0.00\","
      + "\"32451 / 93092849\",\"\"\n"

      + "\"7/23/2017\",\"22:53:18\",\"Europe/Berlin\",\"Allgemeine Währungsumrechnung\","
      + "\"CHF\",\"-31.76\",\"0.00\",\"-31.76\",\"0.00\",\"4XP407259C4326408\","
      + "\"\",\"\",\"\",\"\",\"0.00\",\"0.00\",\"32451 / 93092849\",\"4T059135JJ095493A\"\n";

    ArrayList<AcquirerTransaction> items =
      (ArrayList<AcquirerTransaction>) CsvTransactionImporter.readItemsFromString(content,
        ImportFormat.PAYPAL);

    //    System.out.println(items.stream().map(ta -> ta.getPspTransactionId())
    //      .collect(Collectors.toList()));
    //    System.out.println(items.stream().map(ta -> ta.getAcquirerTransactionId())
    //      .collect(Collectors.toList()));

    Assert.assertEquals(3, items.stream()
      .filter(ta -> ta.getIsPrimaryTransaction()).count());

    Assert.assertEquals(
      Arrays.asList("128374745K7829803", "30J69245SE840053M", "4T059135JJ095493A"),
      items.stream()
        .filter(ta -> ta.getIsPrimaryTransaction()).map(ta -> ta.getAcquirerTransactionId())
        .collect(Collectors.toList()));

    Assert.assertTrue(items.get(0).getSourceContent().length() > 2);
  }
}
