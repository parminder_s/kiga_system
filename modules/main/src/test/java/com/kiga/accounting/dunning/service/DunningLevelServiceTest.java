package com.kiga.accounting.dunning.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.dunning.domain.DunningLevel;
import com.kiga.accounting.dunning.repository.DunningLevelRepository;
import com.kiga.accounting.dunning.web.converter.DunningLevelToViewModelConverter;
import com.kiga.accounting.dunning.web.request.AddDunningLevelRequest;
import com.kiga.accounting.dunning.web.request.DecreaseLevelRequest;
import com.kiga.accounting.dunning.web.request.IncreaseLevelRequest;
import com.kiga.accounting.dunning.web.request.RemoveDunningLevelRequest;
import com.kiga.accounting.dunning.web.request.UpdateDunningLevelRequest;
import com.kiga.accounting.service.InvoiceService;
import com.kiga.shop.domain.Country;
import com.kiga.shop.service.CountryService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.math.BigDecimal;
import java.util.Collections;

/**
 * @author bbs
 * @since 2/19/17.
 */
public class DunningLevelServiceTest {
  private static final long ID = 1L;
  private static final long COUNTRY_ID = ID;
  private static final long DUNNING_LEVEL_ID = 2L;
  private static final BigDecimal FEES_AMOUNT = BigDecimal.TEN;
  private static final String DUNNING_CODE = "DunningLevel1";
  private static final int DUNNING_LEVEL = 1;
  private static final int DAYS = 7;
  private static final String COUNTRY_CODE = "US";
  private DunningLevelService dunningLevelService;
  private DunningLevelRepository dunningLevelRepositoryMock = mock(DunningLevelRepository.class);
  private DunningLevelToViewModelConverter dunningLevelToViewModelConverterMock = mock(
    DunningLevelToViewModelConverter.class);
  private InvoiceService invoiceServiceMock = mock(InvoiceService.class);
  private CountryService countryServiceMock = mock(CountryService.class);

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    dunningLevelService = new DunningLevelService(invoiceServiceMock, countryServiceMock,
      dunningLevelRepositoryMock,
      dunningLevelToViewModelConverterMock);
  }

  @Test
  public void testGetAll() {
    Iterable<DunningLevel> dunningLevels = Collections.singletonList(new DunningLevel());
    when(dunningLevelRepositoryMock.findAll()).thenReturn(dunningLevels);
    dunningLevelService.getAll();
    verify(dunningLevelRepositoryMock).findAll();
    verify(dunningLevelToViewModelConverterMock).convertToResponse(dunningLevels);
  }

  @Test
  public void testAdd() {
    Country country = new Country();
    when(countryServiceMock.getEntityById(COUNTRY_ID)).thenReturn(country);
    AddDunningLevelRequest addDunningLevelRequest = new AddDunningLevelRequest();
    addDunningLevelRequest.setFeesAmount(BigDecimal.TEN);
    addDunningLevelRequest.setCode("DunningLevel1");
    addDunningLevelRequest.setDunningLevel(1);
    addDunningLevelRequest.setDays(7);
    addDunningLevelRequest.setCountryId(COUNTRY_ID);
    dunningLevelService.add(addDunningLevelRequest);

    ArgumentCaptor<DunningLevel> dunningLevelArgumentCaptor = ArgumentCaptor
      .forClass(DunningLevel.class);
    verify(dunningLevelRepositoryMock).save(dunningLevelArgumentCaptor.capture());

    DunningLevel dunningLevel = dunningLevelArgumentCaptor.getValue();
    assertEquals(addDunningLevelRequest.getFeesAmount(), dunningLevel.getFeesAmount());
    assertEquals(addDunningLevelRequest.getCode(), dunningLevel.getCode());
    assertEquals(addDunningLevelRequest.getDunningLevel(), dunningLevel.getDunningLevel());
    assertEquals(addDunningLevelRequest.getDays(), dunningLevel.getDays());
    assertEquals(country, dunningLevel.getCountry());
  }

  @Test
  public void testEdit() {
    DunningLevel dunningLevel = new DunningLevel();
    dunningLevel.setId(DUNNING_LEVEL_ID);
    when(dunningLevelRepositoryMock.findOne(DUNNING_LEVEL_ID)).thenReturn(dunningLevel);
    Country country = new Country();
    when(countryServiceMock.getEntityById(COUNTRY_ID)).thenReturn(country);
    UpdateDunningLevelRequest updateDunningLevelRequest = new UpdateDunningLevelRequest();
    updateDunningLevelRequest.setId(DUNNING_LEVEL_ID);
    updateDunningLevelRequest.setFeesAmount(FEES_AMOUNT);
    updateDunningLevelRequest.setCode(DUNNING_CODE);
    updateDunningLevelRequest.setDunningLevel(DUNNING_LEVEL);
    updateDunningLevelRequest.setDays(DAYS);
    updateDunningLevelRequest.setCountryId(COUNTRY_ID);
    dunningLevelService.update(updateDunningLevelRequest);

    ArgumentCaptor<DunningLevel> dunningLevelArgumentCaptor = ArgumentCaptor
      .forClass(DunningLevel.class);
    verify(dunningLevelRepositoryMock).save(dunningLevelArgumentCaptor.capture());

    DunningLevel dunningLevelCaptured = dunningLevelArgumentCaptor.getValue();
    assertEquals(updateDunningLevelRequest.getId(), dunningLevelCaptured.getId().longValue());
    assertEquals(updateDunningLevelRequest.getFeesAmount(), dunningLevelCaptured.getFeesAmount());
    assertEquals(updateDunningLevelRequest.getCode(), dunningLevelCaptured.getCode());
    assertEquals(updateDunningLevelRequest.getDunningLevel(),
      dunningLevelCaptured.getDunningLevel());
    assertEquals(updateDunningLevelRequest.getDays(), dunningLevelCaptured.getDays());
    assertEquals(country, dunningLevelCaptured.getCountry());
  }

  @Test
  public void testRemove() {
    Invoice invoice = new Invoice();
    DunningLevel dunningLevel = new DunningLevel();
    invoice.setDunningLevel(dunningLevel);
    dunningLevel.setInvoices(Collections.singletonList(invoice));
    when(dunningLevelRepositoryMock.findOne(ID)).thenReturn(dunningLevel);
    RemoveDunningLevelRequest removeDunningLevelRequest = new RemoveDunningLevelRequest();
    removeDunningLevelRequest.setIds(Collections.singletonList(ID));
    dunningLevelService.remove(removeDunningLevelRequest);
    verify(dunningLevelRepositoryMock).delete(dunningLevel);

    ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
    verify(invoiceServiceMock).save(invoiceArgumentCaptor.capture());
    Invoice invoiceCaptured = invoiceArgumentCaptor.getValue();
    assertNull(invoiceCaptured.getDunningLevel());
  }

  @Test
  public void testGetOne() {
    DunningLevel dunningLevel = new DunningLevel();
    when(dunningLevelRepositoryMock.findOne(ID)).thenReturn(dunningLevel);
    dunningLevelService.getOne(ID);
    verify(dunningLevelRepositoryMock).findOne(ID);
    verify(dunningLevelToViewModelConverterMock).convertToResponse(dunningLevel);
  }

  @Test
  public void testIncrease() {
    Country country = new Country();
    DunningLevel oldDunningLevel = new DunningLevel();
    oldDunningLevel.setCountry(country);
    Invoice invoice = new Invoice();
    invoice.setDunningLevel(oldDunningLevel);
    when(invoiceServiceMock.getEntity(ID)).thenReturn(invoice);

    DunningLevel newDunningLevel = new DunningLevel();
    when(dunningLevelRepositoryMock
      .findByDunningLevelAndCountry(oldDunningLevel.getDunningLevel() + 1, country))
      .thenReturn(newDunningLevel);

    IncreaseLevelRequest increaseLevelRequest = new IncreaseLevelRequest();
    increaseLevelRequest.setInvoiceIds(Collections.singletonList(ID));
    dunningLevelService.increase(increaseLevelRequest);

    ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
    verify(invoiceServiceMock).save(invoiceArgumentCaptor.capture());
    Invoice capturedInvoice = invoiceArgumentCaptor.getValue();
    assertEquals(newDunningLevel, capturedInvoice.getDunningLevel());
  }

  @Test
  public void testIncreaseNoOldLevel() {
    Invoice invoice = new Invoice();
    invoice.setCountryCode(COUNTRY_CODE);
    when(invoiceServiceMock.getEntity(ID)).thenReturn(invoice);

    Country country = new Country();
    when(countryServiceMock.getEntityByCode(COUNTRY_CODE)).thenReturn(country);

    DunningLevel newDunningLevel = new DunningLevel();
    when(dunningLevelRepositoryMock
      .findByDunningLevelAndCountry(1, country)).thenReturn(newDunningLevel);

    IncreaseLevelRequest increaseLevelRequest = new IncreaseLevelRequest();
    increaseLevelRequest.setInvoiceIds(Collections.singletonList(ID));
    dunningLevelService.increase(increaseLevelRequest);

    ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
    verify(invoiceServiceMock).save(invoiceArgumentCaptor.capture());
    Invoice capturedInvoice = invoiceArgumentCaptor.getValue();
    assertEquals(newDunningLevel, capturedInvoice.getDunningLevel());
  }

  @Test
  public void testDecrease() {
    Country country = new Country();
    DunningLevel oldDunningLevel = new DunningLevel();
    oldDunningLevel.setCountry(country);
    Invoice invoice = new Invoice();
    invoice.setDunningLevel(oldDunningLevel);
    when(invoiceServiceMock.getEntity(ID)).thenReturn(invoice);

    DunningLevel newDunningLevel = new DunningLevel();
    when(dunningLevelRepositoryMock
      .findByDunningLevelAndCountry(oldDunningLevel.getDunningLevel() - 1, country))
      .thenReturn(newDunningLevel);

    DecreaseLevelRequest decreaseLevelRequest = new DecreaseLevelRequest();
    decreaseLevelRequest.setInvoiceIds(Collections.singletonList(ID));
    dunningLevelService.decrease(decreaseLevelRequest);

    ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
    verify(invoiceServiceMock).save(invoiceArgumentCaptor.capture());
    Invoice capturedInvoice = invoiceArgumentCaptor.getValue();
    assertEquals(newDunningLevel, capturedInvoice.getDunningLevel());
  }

  @Test
  public void testDecreaseNoOldLevel() {
    Invoice invoice = new Invoice();
    invoice.setCountryCode(COUNTRY_CODE);
    when(invoiceServiceMock.getEntity(ID)).thenReturn(invoice);

    Country country = new Country();
    when(countryServiceMock.getEntityByCode(COUNTRY_CODE)).thenReturn(country);

    DunningLevel newDunningLevel = new DunningLevel();
    when(dunningLevelRepositoryMock
      .findByDunningLevelAndCountry(1, country)).thenReturn(newDunningLevel);

    DecreaseLevelRequest decreaseLevelRequest = new DecreaseLevelRequest();
    decreaseLevelRequest.setInvoiceIds(Collections.singletonList(ID));
    dunningLevelService.decrease(decreaseLevelRequest);

    ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
    verify(invoiceServiceMock).save(invoiceArgumentCaptor.capture());
    Invoice capturedInvoice = invoiceArgumentCaptor.getValue();
    assertEquals(newDunningLevel, capturedInvoice.getDunningLevel());
  }

  @Test
  public void testDecreaseNoDunningLevel() {
    Country country = new Country();
    DunningLevel oldDunningLevel = new DunningLevel();
    oldDunningLevel.setCountry(country);
    Invoice invoice = new Invoice();
    invoice.setDunningLevel(oldDunningLevel);
    when(invoiceServiceMock.getEntity(ID)).thenReturn(invoice);

    when(dunningLevelRepositoryMock
      .findByDunningLevelAndCountry(oldDunningLevel.getDunningLevel() - 1, country))
      .thenReturn(null);

    DecreaseLevelRequest decreaseLevelRequest = new DecreaseLevelRequest();
    decreaseLevelRequest.setInvoiceIds(Collections.singletonList(ID));
    dunningLevelService.decrease(decreaseLevelRequest);

    verify(invoiceServiceMock, never()).save(any());
  }

  @Test
  public void testIncreaseNoDunningLevel() {
    Country country = new Country();
    DunningLevel oldDunningLevel = new DunningLevel();
    oldDunningLevel.setCountry(country);
    Invoice invoice = new Invoice();
    invoice.setDunningLevel(oldDunningLevel);
    when(invoiceServiceMock.getEntity(ID)).thenReturn(invoice);

    when(dunningLevelRepositoryMock
      .findByDunningLevelAndCountry(oldDunningLevel.getDunningLevel() + 1, country))
      .thenReturn(null);

    IncreaseLevelRequest increaseLevelRequest = new IncreaseLevelRequest();
    increaseLevelRequest.setInvoiceIds(Collections.singletonList(ID));
    dunningLevelService.increase(increaseLevelRequest);

    verify(invoiceServiceMock, never()).save(any());
  }
}
