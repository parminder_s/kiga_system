package com.kiga.accounting.transactions.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.service.InvoiceService;
import com.kiga.accounting.transactions.web.request.FindPaymentTransactionRequest;
import com.kiga.accounting.transactions.web.response.BankViewModel;
import com.kiga.accounting.transactions.web.response.PaymentTransactionViewModel;
import com.kiga.shop.service.CountryShopService;
import com.kiga.shop.web.response.CountryShopViewModel;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author bbs
 * @since 2/18/17.
 */
public class BankServiceTest {
  public static final String BANK_NAME = "DUMMYBANKNAME";
  private static final String COUNTRY_CODE = "DEF";
  private static final long PAYMENT_TRANSACTION_ID = 1L;
  private static final String INVOICE = "ABC";
  private BankService bankService;
  private InvoiceService invoiceServiceMock = mock(InvoiceService.class);
  private PaymentTransactionService paymentTransactionServiceMock = mock(
    PaymentTransactionService.class);
  private CountryShopService countryShopServiceMock = mock(CountryShopService.class);

  @Before
  public void beforeTests() {
    bankService = new BankService(invoiceServiceMock, paymentTransactionServiceMock,
      countryShopServiceMock);
  }

  @Test
  public void testGetAllBanks() {
    CountryShopViewModel countryShopViewModelA = new CountryShopViewModel();
    countryShopViewModelA.setBankName("A");

    CountryShopViewModel countryShopViewModelB = new CountryShopViewModel();
    countryShopViewModelB.setBankName("B");

    CountryShopViewModel countryShopViewModelC = new CountryShopViewModel();
    countryShopViewModelC.setBankName("C");

    CountryShopViewModel countryShopViewModelEmpty = new CountryShopViewModel();
    countryShopViewModelEmpty.setBankName("");

    CountryShopViewModel countryShopViewModelBlank = new CountryShopViewModel();
    countryShopViewModelBlank.setBankName(" ");

    CountryShopViewModel countryShopViewModelNullBank = new CountryShopViewModel();
    countryShopViewModelNullBank.setBankName(null);

    List<CountryShopViewModel> countryShopViewModels = Arrays.asList(
      countryShopViewModelA,
      countryShopViewModelB,
      countryShopViewModelC,
      countryShopViewModelA,
      countryShopViewModelB,
      countryShopViewModelC,
      countryShopViewModelEmpty,
      countryShopViewModelBlank,
      countryShopViewModelNullBank
    );

    when(countryShopServiceMock.getAll()).thenReturn(countryShopViewModels);
    List<BankViewModel> bankViewModels = bankService.getAllBanks();

    assertEquals(3, bankViewModels.size());
    assertEquals(countryShopViewModelA.getBankName(), bankViewModels.get(0).getBankName());
    assertEquals(countryShopViewModelB.getBankName(), bankViewModels.get(1).getBankName());
    assertEquals(countryShopViewModelC.getBankName(), bankViewModels.get(2).getBankName());
  }

  @Test
  public void testGetByInvoiceNumber() {
    Invoice invoice = new Invoice();
    invoice.setCountryCode(COUNTRY_CODE);
    when(invoiceServiceMock.getEntityByNumber(INVOICE)).thenReturn(invoice);

    CountryShopViewModel countryShopViewModel = new CountryShopViewModel();
    when(countryShopServiceMock.getByCode(COUNTRY_CODE)).thenReturn(countryShopViewModel);

    BankViewModel bankViewModel = bankService.getByInvoiceNumber(INVOICE);

    assertNotNull(bankViewModel);
  }

  @Test
  public void testGetByPaymentTransactionId() {
    FindPaymentTransactionRequest findPaymentTransactionRequest = new FindPaymentTransactionRequest(
      PAYMENT_TRANSACTION_ID);

    PaymentTransactionViewModel paymentTransactionViewModel = new PaymentTransactionViewModel();
    paymentTransactionViewModel.setPaidInvoiceNumber(INVOICE);

    when(paymentTransactionServiceMock.getOne(findPaymentTransactionRequest))
      .thenReturn(paymentTransactionViewModel);

    Invoice invoice = new Invoice();
    when(invoiceServiceMock.getEntityByNumber(INVOICE)).thenReturn(invoice);

    CountryShopViewModel countryShopViewModel = new CountryShopViewModel();
    countryShopViewModel.setBankName(BANK_NAME);
    when(countryShopServiceMock.getByCode(invoice.getCountryCode()))
      .thenReturn(countryShopViewModel);

    BankViewModel bankViewModel = bankService.getByPaymentTransactionId(PAYMENT_TRANSACTION_ID);
    assertEquals(BANK_NAME, bankViewModel.getBankName());
  }
}
