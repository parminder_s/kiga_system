package com.kiga.accounting.dunning.service;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAndIs;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import com.kiga.accounting.dunning.web.request.SendEmailRequest;
import com.kiga.accounting.service.InvoiceService;
import com.kiga.accounting.web.response.InvoiceViewModel;
import com.kiga.mail.logic.TestMailer;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

/**
 * @author bbs
 * @since 2/20/17.
 */
public class DunningEmailServiceTest {
  public static final String INVOICE_NUMBER = "123";
  private DunningEmailService dunningEmailService;
  private TestMailer mailerMock = new TestMailer();
  private InvoiceService invoiceServiceMock = mock(InvoiceService.class);
  private DunningDocumentService documentServiceMock = mock(DunningDocumentService.class);
  private InvoiceViewModel invoiceViewModel;


  /**
   * .
   */
  @Before
  public void beforeTest() {
    invoiceViewModel = new InvoiceViewModel();
    invoiceViewModel.setCustomerEmail("to@example.com");
    when(invoiceServiceMock.getOne(anyString())).thenReturn(invoiceViewModel);
    Supplier<PDFMergerUtility> supplier = () -> mock(PDFMergerUtility.class);
    dunningEmailService = new DunningEmailService(mailerMock, invoiceServiceMock,
      documentServiceMock, supplier);
    when(invoiceServiceMock.getOne(anyLong())).thenReturn(invoiceViewModel);
  }

  @Test
  public void testSendEmails() {
    SendEmailRequest sendEmailRequest = new SendEmailRequest();
    sendEmailRequest.setInvoiceNumbers(Collections.singletonList(INVOICE_NUMBER));
    when(documentServiceMock.createDunningDocument(any())).thenReturn(mock(File.class));
    dunningEmailService.sendEmails(sendEmailRequest);
    verify(documentServiceMock).createDunningDocument(eq(INVOICE_NUMBER));

    verify(invoiceServiceMock).markDunningEmailSent(INVOICE_NUMBER);
    assertThat(mailerMock.getSentEMail(), allOf(
      hasProperty("from", isPresentAndIs("system@kigaportal.com")),
      hasProperty("to", isPresentAndIs("to@example.com")),
      hasProperty("subject", equalTo("Zahlungserinnerung KiGaPortal"))
    ));
  }

  @Test
  public void testSendEmailsNoPdf() {
    SendEmailRequest sendEmailRequest = new SendEmailRequest();
    sendEmailRequest.setInvoiceNumbers(Collections.singletonList(INVOICE_NUMBER));
    dunningEmailService.sendEmails(sendEmailRequest);
    verify(invoiceServiceMock).markDunningEmailSent(INVOICE_NUMBER);

    mailerMock.verifySent();
  }

  @Test
  public void testGeneratePdf() {
    List<Long> invoiceIdList = Arrays.asList(1L, 2L, 3L);
    dunningEmailService.dunningDocumentForInvoices(invoiceIdList);
    verify(documentServiceMock, times(invoiceIdList.size())).createDunningDocument(any());
  }
}
