package com.kiga.accounting.transactions.web;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.accounting.transactions.service.PaymentTransactionService;
import com.kiga.accounting.transactions.web.request.AddPaymentTransactionRequest;
import com.kiga.accounting.transactions.web.request.FindPaymentTransactionRequest;
import com.kiga.accounting.transactions.web.request.RemovePaymentTransactionRequest;
import com.kiga.accounting.transactions.web.request.UpdatePaymentTransactionRequest;
import com.kiga.accounting.transactions.web.response.PaymentTransactionViewModel;
import com.kiga.accounting.web.request.SingleInvoiceRequest;
import java.util.Collections;
import org.junit.Before;
import org.junit.Test;

/**
 * @author bbs
 * @since 2/9/17.
 */
public class PaymentTransactionControllerTest {
  private static final Long VALID_ID = 1L;
  private PaymentTransactionService paymentTransactionService =
      mock(PaymentTransactionService.class);
  private PaymentTransactionController paymentTransactionController;

  @Before
  public void prepareTests() {
    paymentTransactionController = new PaymentTransactionController(paymentTransactionService);
  }

  @Test
  public void testList() {
    paymentTransactionController.list("all");
    verify(paymentTransactionService).getAll("all");
  }

  @Test
  public void testOne() {
    FindPaymentTransactionRequest findPaymentTransactionRequest =
        new FindPaymentTransactionRequest();
    findPaymentTransactionRequest.setId(1L);
    PaymentTransactionViewModel paymentTransactionViewModel = new PaymentTransactionViewModel();
    when(paymentTransactionService.getOne(findPaymentTransactionRequest))
        .thenReturn(paymentTransactionViewModel);
    paymentTransactionController.one(findPaymentTransactionRequest);
    verify(paymentTransactionService).getOne(findPaymentTransactionRequest);
  }

  @Test
  public void testAdd() {
    AddPaymentTransactionRequest addPaymentTransactionRequest = new AddPaymentTransactionRequest();
    paymentTransactionController.add(addPaymentTransactionRequest);
    verify(paymentTransactionService).add(addPaymentTransactionRequest);
  }

  @Test
  public void testUpdate() {
    UpdatePaymentTransactionRequest updatePaymentTransactionRequest =
        new UpdatePaymentTransactionRequest();
    paymentTransactionController.update(updatePaymentTransactionRequest);
    verify(paymentTransactionService).update(updatePaymentTransactionRequest);
  }

  @Test
  public void testRemove() {
    RemovePaymentTransactionRequest removePaymentTransactionRequest =
        new RemovePaymentTransactionRequest();
    removePaymentTransactionRequest.setIds(Collections.singletonList(VALID_ID));
    paymentTransactionController.remove(removePaymentTransactionRequest);
    verify(paymentTransactionService).remove(Collections.singletonList(VALID_ID));
  }

  @Test
  public void testGetModelByInvoice() {
    SingleInvoiceRequest singleInvoiceRequest = new SingleInvoiceRequest();
    paymentTransactionController.getModelByInvoice(singleInvoiceRequest);
    verify(paymentTransactionService).getModelByInvoice(singleInvoiceRequest);
  }
}
