package com.kiga.accounting.dunning.web.converter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.accounting.dunning.domain.DunningLevel;
import com.kiga.accounting.dunning.web.response.DunningLevelViewModel;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.web.converter.CountryToViewModelConverter;
import com.kiga.shop.web.response.CountryViewModel;
import org.junit.Before;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;

/**
 * @author bbs
 * @since 2/19/17.
 */
public class DunningLevelToViewModelConverterTest {
  private static final BigDecimal FEES_AMOUNT = BigDecimal.TEN;
  private static final String FEES_CURRENCY = "EUR";
  private static final int DAYS = 7;
  private static final int DUNNING_LEVEL = 1;
  private static final String CODE = "ABC";
  private static final long ID = 1L;
  private static final long COUNTRY_ID = 2L;

  private DunningLevelToViewModelConverter dunningLevelToViewModelConverter;
  private CountryToViewModelConverter countryToViewModelConverterMock = mock(
    CountryToViewModelConverter.class);

  /**
   * Prepare before tests.
   */
  @Before
  public void before() {
    CountryViewModel countryViewModel = new CountryViewModel();
    countryViewModel.setId(COUNTRY_ID);
    countryViewModel.setCurrency(FEES_CURRENCY);
    when(countryToViewModelConverterMock.convertToResponse(any(Country.class)))
      .thenReturn(countryViewModel);
    dunningLevelToViewModelConverter = new DunningLevelToViewModelConverter(
      countryToViewModelConverterMock, new ModelMapper());
  }

  @Test
  public void testConvertEntity() {
    Country country = new Country();
    country.setCountryShop(new CountryShop());
    DunningLevel dunningLevel = new DunningLevel();
    dunningLevel.setId(ID);
    dunningLevel.setCode(CODE);
    dunningLevel.setCountry(country);
    dunningLevel.setDunningLevel(DUNNING_LEVEL);
    dunningLevel.setFeesAmount(FEES_AMOUNT);
    dunningLevel.setDays(DAYS);

    DunningLevelViewModel viewModel = dunningLevelToViewModelConverter
      .convertToResponse(dunningLevel);
    assertEquals(CODE, viewModel.getCode());
    assertEquals(COUNTRY_ID, viewModel.getCountry().getId());
    assertEquals(FEES_AMOUNT, viewModel.getFeesAmount());
    assertEquals(DAYS, viewModel.getDays());
    assertEquals(DUNNING_LEVEL, viewModel.getDunningLevel());
  }
}
