package com.kiga.accounting.transactionimport;

import com.kiga.accounting.domain.AcquirerTransaction;
import com.kiga.accounting.formats.CsvTransactionImporter;
import com.kiga.accounting.formats.ImportFormat;
import com.kiga.accounting.formats.TransactionImporterFactory;
import com.kiga.accounting.formats.export.AccountingRow;
import com.kiga.accounting.helper.StreamItemWriter;
import com.kiga.accounting.lookup.PaymentLookupMock;
import com.kiga.accounting.lookup.PurchaseLookupMock;
import com.kiga.accounting.match.TransactionMatcher;
import com.kiga.payment.domain.Payment;
import com.kiga.shop.domain.Purchase;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.batch.item.file.transform.FieldExtractor;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.core.io.InputStreamResource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by mfit on 25.05.16.
 */
public class CsvTransactionImporterTest {
  public static DateFormat dtf = new SimpleDateFormat("yyyyMMdd");

  @Test
  public void cardCompleteImportFromFileTest() throws ParseException {
    // DateFormat dtf = new SimpleDateFormat("yyyyMMdd");

    String fileName = "/com.kiga.accounting/cardcomplete-report.csv";
    String uploadFileName = this.getClass().getResource(fileName).getPath();
    ArrayList<AcquirerTransaction> result =
      (ArrayList<AcquirerTransaction>) CsvTransactionImporter.readItemsFromFile(uploadFileName,
      ImportFormat.CC);

    Assert.assertEquals(6, result.size());

    Assert.assertEquals(Arrays.asList("cardcomplete-report.csv", "cardcomplete-report.csv",
      "cardcomplete-report.csv", "cardcomplete-report.csv", "cardcomplete-report.csv",
      "cardcomplete-report.csv"),
      result.stream().map(AcquirerTransaction::getSourceFileName)
        .collect(Collectors.toList()));


    Assert.assertEquals(Arrays.asList(7.90, 39.90, 45.90, 9.18, 7.90, 7.90),
                        result.stream().map(el -> el.getAmount().doubleValue())
                                       .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("EUR", "EUR", "EUR", "EUR", "EUR", "EUR"),
                        result.stream().map(AcquirerTransaction::getCurrency)
                                       .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(7.90, 39.90, 45.90, 9.99, 7.90, 7.90),
                        result.stream().map(el -> el.getAmountOrig().doubleValue())
                                       .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("EUR", "EUR", "EUR", "CHF", "EUR", "EUR"),
                        result.stream().map(AcquirerTransaction::getCurrencyOrig)
                                       .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("cardcomplete", "cardcomplete", "cardcomplete",
                                      "cardcomplete", "cardcomplete", "cardcomplete"),
                        result.stream().map(AcquirerTransaction::getAcquirerName)
                                       .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("64172744", "64166196", "64134436", "64126836",
                                      "64126828", "64126826"),
                        result.stream().map(AcquirerTransaction::getPspTransactionId)
                                       .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("VISA", "VISA", "VISA", "MasterCard", "VISA", "MasterCard"),
                        result.stream().map(AcquirerTransaction::getCardType)
                                       .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(dtf.parse("20160301"), dtf.parse("20160301"),
                                      dtf.parse("20160301"), dtf.parse("20160301"),
                                      dtf.parse("20160301"), dtf.parse("20160301")),
                        result.stream().map(AcquirerTransaction::getTransactionDate)
                                       .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(dtf.parse("20160330"), dtf.parse("20160330"),
                                      dtf.parse("20160330"), dtf.parse("20160330"),
                                      dtf.parse("20160330"), dtf.parse("20160330")),
                        result.stream().map(AcquirerTransaction::getSettlementDate)
                                       .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("104928", "025758", "353017", "242667", "242664", "242663"),
                        result.stream().map(AcquirerTransaction::getAcquirerTransactionId)
                                       .collect(Collectors.toList()));
  }

  @Test
  public void cardCompleteImportFromString() throws ParseException,
    UnsupportedEncodingException {
    String content =
        "Chain;Vertragsnummer;Terminal-ID;Umsatzdatum;Einreichdatum;Verarbeitungsdatum;"
          + "Abrechnungsdatum;Einreichsumme;Produkt;Batchnummer;Kartennummer;Belegnummer;"
          + "Genehmigungsnr.;Kauf-/Umsatzbetrag;W�hrung;Originalbetrag;Orig. W�hrung;"
          + "Akzeptanzpartner;Auftragsnummer;Stammkundennummer;Interchange-Kategorie;"
          + "Interchange Fee*;Scheme Fee*;Acquiring Fee (%)*;Acquiring Fee (EUR)*;"
          + "Special Fee (%)*;Special Fee (EUR)*;Interchange (%);Scheme Fee (%);Acquiring Fee (%);"
          + "Acquiring Fee (EUR);Special Fee (%);Special Fee (EUR)\n"
      + ";100528702;;01.03.2016;01.03.2016;02.03.2016;30.03.2016;338,27;VISA;10896103849;"
          + "4920 XXXX XXXX 4671;104928;015340;7,90;EUR;7,90;EUR;WWW.KIGAPORTAL.COM       GRAZ;"
          + "64172744;;;0,000000;0,000000;0,000000;0,000000;0,000000;0,000000;0,0000;0,0000;0,0000;"
          + "0,00;0,0000;0,00\n"
      + ";100528702;;01.03.2016;01.03.2016;02.03.2016;30.03.2016;338,27;VISA;10896103849;"
          + "4170 XXXX XXXX 5002;025758;429906;39,90;EUR;39,90;EUR;WWW.KIGAPORTAL.COM       GRAZ;"
          + "64166196;;;0,000000;0,000000;0,000000;0,000000;0,000000;0,000000;0,0000;0,0000;0,0000;"
          + "0,00;0,0000;0,00\n"
      + ";100528702;;01.03.2016;01.03.2016;02.03.2016;30.03.2016;338,27;VISA;10896103849;"
          + "4240 XXXX XXXX 0311;353017;696307;45,90;EUR;45,90;EUR;WWW.KIGAPORTAL.COM       GRAZ;"
          + "64134436;;;0,000000;0,000000;0,000000;0,000000;0,000000;0,000000;0,0000;0,0000;0,0000;"
          + "0,00;0,0000;0,00\n"
      + ";100528702;;01.03.2016;01.03.2016;02.03.2016;30.03.2016;24,70;MasterCard;10895103848;"
          + "5484 XXXX XXXX 4442;242667;052075;9,18;EUR;9,99;CHF;WWW.KIGAPORTAL.COM     GRAZ;"
          + "64126836;;;0,000000;0,000000;0,000000;0,000000;0,000000;0,000000;0,0000;0,0000;0,0000;"
          + "0,00;0,0000;0,00\n"
      + ";100528702;;01.03.2016;01.03.2016;02.03.2016;30.03.2016;338,27;VISA;10896103849;"
          + "4228 XXXX XXXX 1519;242664;353035;7,90;EUR;7,90;EUR;WWW.KIGAPORTAL.COM       GRAZ;"
          + "64126828;;;0,000000;0,000000;0,000000;0,000000;0,000000;0,000000;0,0000;0,0000;0,0000;"
          + "0,00;0,0000;0,00\n"
      + ";100528702;;01.03.2016;01.03.2016;02.03.2016;30.03.2016;87,18;MasterCard;10897103850;"
          + "5337 XXXX XXXX 5123;242663;357663;7,90;EUR;7,90;EUR;WWW.KIGAPORTAL.COM     GRAZ;"
          + "64126826;;;0,000000;0,000000;0,000000;0,000000;0,000000;0,000000;0,0000;0,0000;0,0000;"
          + "0,00;0,0000;0,00\n"

      // Most recent format
      + "100609304;100609338;;15.05.17;15.05.17;16.05.17;30.05.17;16,9;Mastercard;14370058972;"
          + "1111 XXXX XXXX 2331;626645;612561;16,9;EUR;16,9;EUR;KIGAPORTAL WEBSHOP     GRAZ;"
          + "88930011;;DCS;-0,0507;0;0;0;0;0;0;0;0;0;0;0;Nein;Nein;0;0;0;0;0;0";

    ArrayList<AcquirerTransaction> items =
      (ArrayList<AcquirerTransaction>) CsvTransactionImporter.readItemsFromString(content,
      ImportFormat.CC);

    Assert.assertEquals(7, items.size());
    Assert.assertEquals(items.get(0).getAmount(), new BigDecimal("7.9"));
    Assert.assertEquals(items.get(0).getSettlementDate(), dtf.parse("20160330"));
    Assert.assertEquals(items.get(1).getPspTransactionId(), "64166196");
    Assert.assertEquals(items.get(1).getTransactionDate(), dtf.parse("20160301"));
    Assert.assertEquals(items.get(2).getCardType(), "VISA");
    Assert.assertEquals(items.get(3).getCurrencyOrig(), "CHF");
    Assert.assertEquals(items.get(0).getAcquirerName(), "cardcomplete");

    Assert.assertEquals(items.get(6).getCardType(), "Mastercard");
    Assert.assertEquals(items.get(6).getSettlementDate(), dtf.parse("20170530"));
  }

  @Test
  public void cardCompleteImportAndExportTest() throws Exception {
    TransactionMatcher converter = new TransactionMatcher();

    PaymentLookupMock paymentLookup = new PaymentLookupMock();
    for (Payment p : this.getMockPayments()) {
      paymentLookup.getItems().put(p.getRemoteTransactionId(), p);
    }
    PurchaseLookupMock purchaseLookup = new PurchaseLookupMock();
    for (Purchase p : this.getMockPurchases()) {
      purchaseLookup.getItems().put(p.getId(), p);
    }

    converter.setPaymentLookup(paymentLookup);
    converter.setPurchaseLookup(purchaseLookup);

    CompositeItemProcessor<AcquirerTransaction, AccountingRow>
      processor = converter.processorFactory();
    FieldExtractor<AccountingRow> extractor = converter.extractorFactory();

    String uploadFileName = this.getClass()
      .getResource("/com.kiga.accounting/cardcomplete-report.csv").getPath();
    ArrayList<AcquirerTransaction> items =
      (ArrayList<AcquirerTransaction>) CsvTransactionImporter.readItemsFromFile(uploadFileName,
      ImportFormat.CC);

    List<Object[]> resultRows = new ArrayList<Object[]>();
    for (AcquirerTransaction ta : items) {
      resultRows.add(extractor.extract(processor.process(ta)));
    }

    Assert.assertEquals(6, resultRows.size());
    Assert.assertEquals(Arrays.asList("02.03.2016", "02.03.2016", "02.03.2016", "02.03.2016",
                                      "02.03.2016", "02.03.2016"),
                        resultRows.stream().map(el -> el[0]).collect(Collectors.toList()));
    Assert.assertEquals(Arrays.asList("30.03.2016", "30.03.2016", "30.03.2016", "30.03.2016",
                                      "30.03.2016", "30.03.2016"),
                        resultRows.stream().map(el -> el[1]).collect(Collectors.toList()));

    StreamItemWriter<AccountingRow> responseWriter = converter.getStreamItemWriter();

    ByteArrayOutputStream out = new ByteArrayOutputStream();
    responseWriter.setOutputStream(out);

    List<AccountingRow> processedRows = new ArrayList<>();
    for (AcquirerTransaction ta : items) {
      processedRows.add(processor.process(ta));
    }
    responseWriter.write(processedRows);

    String[] lines = out.toString().split("\n");
    Assert.assertEquals("02.03.2016;30.03.2016;;7,9;EUR;123049;02.03.2016;7,9;1,32;EUR;104928",
                        lines[0]);
    Assert.assertEquals("02.03.2016;30.03.2016;;39,9;EUR;;;;;;025758", lines[1]);
    Assert.assertEquals("02.03.2016;30.03.2016;;9,99;CHF;111041;02.03.2016;9,99;1,66;CHF;242667",
                        lines[3]);
  }

  @Test
  public void sofortImportAndExportTest() throws Exception {
    String content =
      "Transaction.user_id;Transaction.project_id;Transaction.transaction;"
      + "Transaction.sender_holder;Transaction.sender_bank_holder;"
      + "Transaction.sender_account_number;Transaction.sender_bank_code;"
      + "Transaction.sender_bank_name;Transaction.sender_bank_bic;Transaction.sender_iban;"
      + "Transaction.sender_country_id;Transaction.recipient_holder;"
      + "Transaction.recipient_account_number;Transaction.recipient_bank_code;"
      + "Transaction.recipient_bank_name;Transaction.recipient_bank_bic;Transaction.recipient_iban;"
      + "Transaction.recipient_country_id;Transaction.amount;Transaction.amount_refunded;"
      + "Transaction.currency_id;Transaction.exchange_rate;Transaction.reason_1;"
      + "Transaction.reason_2;Transaction.test_transaction;Transaction.notification_status;"
      + "Transaction.notification_attempts;Transaction.notification_last_attempt;"
      + "Transaction.status;Transaction.status_modified;Transaction.user_costs;"
      + "Transaction.user_costs_currency_id;Transaction.user_costs_guarantee;Transaction.created;"
      + "Transaction.internationalization_fees\n"
      + "98641;203249;98641-203249-56F3C72F-A9B6;;Magdalena Leder;12983731301;20715;"
      + "SPARKASSE / ERSTE BANK;STSPAT2GXXX;AT262081512562301301;AT;"
      + "HOHE Medien OG;00040874216;20815;SPARKASSE / ERSTE BANK;STSPAT2GXXX;AT742081500040874216;"
      + "AT;39,90;0,00;EUR;1,0000;000065329340MPAY24;;;1;1;2016-03-24 11:58:14;untraceable;"
      + "2016-03-24 11:58:12;1,05;EUR;0.00;2016-03-24 11:58:12;0\n"
      + "98641;203249;98641-203249-56EE4CDF-F9A1;;Katharina Grugger;35201211111;42000;"
      + "VOLKSBANK BIEN-BAD;VBOEATWWXXX;AT214300032345232011;AT;HOHE Medien OG;00040874216;20815;"
      + "SPARKASSE / ERSTE BANK;STSPAT2GXXX;AT742081500040874216;AT;39,90;0,00;"
      + "EUR;1,0000;000065113896MPAY24;;;1;1;2016-03-20 08:13:38;untraceable;"
      + "2016-03-20 08:13:36;1,05;EUR;0.00;2016-03-20 08:13:36;0\n"
      + "98641;203249;98641-203249-56E83819-340F;;LABUCHER KARIN;0828559064632303242340;08185;"
      + "RAIFFEISENKASSE TAURERER-AHRNTAL / CASSA RAIFFEISEN TURES-AURINA;RZSBIT21440;"
      + "IT80L0828559064000303242340;IT;HOHE Medien OG;0603312001;10070024;Deutsche Bank;"
      + "DEUTDEDBBER;DE70100700240603312001;DE;39,90;0,00;EUR;1,0000;000064895362MPAY24;;;1;1;"
      + "2016-03-15 17:35:21;untraceable;2016-03-15 17:35:20;1,05;EUR;0.00;2016-03-15 17:35:20;0\n"
      + "98641;203249;98641-203249-56E54179-B133;;Sabine Meischberger;123459300;61410000;"
      + "Volksbank Vippingen-Schwenningen;GENODE61VS1;DE70621300000149059300;DE;HOHE Medien OG;"
      + "0603312001;10070024;Deutsche Bank;DEUTDEDBBER;DE70100700240603312001;DE;39,90;0,00;"
      + "EUR;1,0000;000064765620MPAY24;;;1;1;2016-03-13 11:33:56;untraceable;2016-03-13 11:33:55;"
      + "1,05;EUR;0.00;2016-03-13 11:33:55;0\n"
      + "98641;203249;98641-203249-56E1AA1B-EE96;;Luposcips Kathrin;103062830;25720001;"
      + "Sparkasse Pelle;NOLADE21CEL;DE71257500012152077830;DE;HOHE Medien OG;"
      + "0603312001;10070024;Deutsche Bank;DEUTDEDBBER;DE70100700240603312001;"
      + "DE;39,90;0,00;EUR;1,0000;000064637050MPAY24;;;1;1;2016-03-10 18:10:59;untraceable;"
      + "2016-03-10 18:10:58;1,05;EUR;0.00;2016-03-10 18:10:58;0\n"
      + "98641;203249;98641-203249-56D539D2-D43C;;Tatjana Posch;580352363;45850;"
      + "VOLKSBANK LANGECK;VBOEATWWLAN;AT284585000340370763;AT;HOHE Medien OG;00040874216;20815;"
      + "SPARKASSE / ERSTE BANK;STSPAT2GXXX;AT742081500040874216;AT;39,90;0,00;EUR;1,0000;"
      + "000064129862MPAY24;;;1;1;2016-03-01 07:43:50;untraceable;2016-03-01 07:43:48;1,05;"
      + "EUR;0.00;2016-03-01 07:43:48;0\n";

    List<AcquirerTransaction> items = CsvTransactionImporter.readItemsFromString(content,
      ImportFormat.SOFORT);
    Assert.assertEquals(6, items.size());
    TransactionMatcher converter = new TransactionMatcher();
    PaymentLookupMock paymentLookup = new PaymentLookupMock();
    for (Payment p : this.getMockPayments()) {
      paymentLookup.getItems().put(p.getRemoteTransactionId(), p);
    }
    PurchaseLookupMock purchaseLookup = new PurchaseLookupMock();
    for (Purchase p : this.getMockPurchases()) {
      purchaseLookup.getItems().put(p.getId(), p);
    }

    converter.setPaymentLookup(paymentLookup);
    converter.setPurchaseLookup(purchaseLookup);
    CompositeItemProcessor<AcquirerTransaction, AccountingRow>
      processor = converter.processorFactory();
    FieldExtractor<AccountingRow> extractor = converter.extractorFactory();
    StreamItemWriter<AccountingRow> responseWriter = converter.getStreamItemWriter();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    responseWriter.setOutputStream(out);

    List<AccountingRow> processedRows = new ArrayList<>();
    for (AcquirerTransaction ta : items) {
      processedRows.add(processor.process(ta));
    }
    responseWriter.write(processedRows);
    String[] lines = out.toString().split("\n");
    Assert.assertEquals("24.03.2016;24.03.2016;;39,9;EUR;654321043;24.03.2016;39,9;6,65;EUR;"
                        + "98641-203249-56F3C72F-A9B6", lines[0]);
  }

  private List<Payment> getMockPayments()  {

    // Cardcomplete row 1
    Payment pm1 = new Payment();
    pm1.setRemoteTransactionId("64172744");
    pm1.setPrice(new BigDecimal(7.90));
    pm1.setCurrency("EUR");
    pm1.setReferenceCode("shop");
    pm1.setReferenceId("12345678");

    // Cardcomplete row 4
    Payment pm2 = new Payment();
    pm2.setRemoteTransactionId("64126836");
    pm2.setPrice(new BigDecimal("9.99"));
    pm2.setCurrency("CHF");
    pm2.setReferenceCode("shop");
    pm2.setReferenceId("12345679");

    // Sofort row 1
    Payment pm3 = new Payment();
    pm3.setRemoteTransactionId("65329340");
    pm3.setPrice(new BigDecimal("39.90"));
    pm3.setCurrency("EUR");
    pm3.setReferenceCode("shop");
    pm3.setReferenceId("87654321");

    return Arrays.asList(pm1, pm2, pm3);
  }

  private Instant parse(DateTimeFormatter dtf, String str) {
    return LocalDate.parse(str, dtf).atStartOfDay().toInstant(ZoneOffset.UTC);
  }

  private List<Purchase> getMockPurchases() {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    // Cardcomplete row 1
    Purchase pu1 = new Purchase();
    pu1.setId(12345678L);
    pu1.setInvoiceDate(this.parse(dtf, "02.03.2016"));
    pu1.setPriceGrossTotal(new BigDecimal(7.90));
    pu1.setPriceNetTotal(new BigDecimal(6.58));
    pu1.setCurrency("EUR");
    pu1.setInvoiceNr("123049");

    // Cardcomplete row 4
    Purchase pu2 = new Purchase();
    pu2.setId(12345679L);
    pu2.setInvoiceDate(this.parse(dtf, "02.03.2016"));
    pu2.setPriceGrossTotal(new BigDecimal(9.99));
    pu2.setPriceNetTotal(new BigDecimal(8.33));
    pu2.setCurrency("CHF");
    pu2.setInvoiceNr("111041");

    // Sofort row 1
    Purchase pu3 = new Purchase();
    pu3.setId(87654321L);
    pu3.setInvoiceDate(this.parse(dtf, "24.03.2016"));
    pu3.setPriceGrossTotal(new BigDecimal(39.90));
    pu3.setPriceNetTotal(new BigDecimal(33.25));
    pu3.setCurrency("EUR");
    pu3.setInvoiceNr("654321043");

    return Arrays.asList(pu1, pu2, pu3);
  }
}

