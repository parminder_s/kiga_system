package com.kiga.accounting.transactionimport;

import com.kiga.accounting.domain.AcquirerTransaction;
import com.kiga.accounting.formats.CsvTransactionImporter;
import com.kiga.accounting.formats.ImportFormat;
import com.kiga.accounting.formats.TransactionImporterFactory;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by mfit on 14.06.17.
 */
public class AlternativeOldPaypalMapperTest {

  public static DateFormat dtf = new SimpleDateFormat("yyyyMMdd");

  @Ignore("need to deal with extra header row")
  @Test
  public void paypalImportFromStringUncleaned() throws UnsupportedEncodingException,
    ParseException {
    String content =
      "\"Ergebnisse der Transaktionssuche\"\n"

        + "\"Transaktionscode\",\"Zugehöriger Transaktionscode\",\"Datum\",\"Typ\",\"Betreff\","
        + "\"Artikelnummer\",\"Artikelbezeichnung\",\"Rechnungsnummer\",\"Name\",\"E-Mail\","
        + "\"Versandname\",\"Versandadresse Zeile 1\",\"Versandadresse Zeile 2\","
        + "\"Lieferadresse Ort\",\"Versandadresse Staat/Provinz\",\"Versandadresse PLZ\","
        + "\"Lieferadresse Land\",\"Versandmethode\",\"Adress-Status\",\"Telefon\","
        + "\"Bruttobetrag\",\"Empfangsnummer\",\"Benutzerdefiniertes Feld\","
        + "\"Name Option 1\",\"Wert Option 1\",\"Name Option 2\",\"Wert Option 2\","
        + "\"Hinweis\",\"Auktions-Site\",\"eBay-Mitgliedsname\",\"Artikel-URL\",\"Auktionsende\","
        + "\"Versicherungsbetrag\",\"Währung\",\"Gebühren\",\"Nettobetrag\",\"Versandbetrag\","
        + "\"Umsatzsteuerbetrag\",\"An E-Mail\",\"Uhrzeit\",\"Zeitzone\","
        + "\"Rechnungsadresse Zeile 1\",\"Rechnungsadresse Zeile 2\",\"Rechnungsadresse Ort\","
        + "\"Rechnungsadresse Staat/Provinz\",\"Rechnungsadresse PLZ\",\"Rechnungsadresse Land\","
        + "\"Filiale\",\"Kasse\"\n"

        + "\n"

        + "5D176369R2459315J,25303514P1331063E,27.04.2017,Rückzahlung,,,\"74047*74.85*CHF\","
        + "\"28540 / 84826431\",\"Lisa Gelz\",gelz.lisa@gmail.com,\"Lisa, Gelz\",,,,,,,,N,,"
        + "\"-74,85\",0,,,,,,\"refund issued for the case\",,,,,\"0,00\",CHF,\"3,29\",\"-71,56\","
        + "\"0,00\",\"0,00\",gelz.lisa@gmail.com,,Europe/Berlin,,,,,,,\"\",\"\"\n"

        + "0900083133707971E,,10.05.2017,PayPal Express-Zahlung,\"shop checkout\",,"
        + "\"shop checkout\",\"30540 / 88648452\",\"Heinrich Löwy\",seefahrer1@chello.at,"
        + "\"Frau Tanja Löwy\",\"Linzerstrasse\",\"30-32/3/3\",Purkersdorf,,3002,AT,,Y,,\"16,90\","
        + "0,,,,,,,,,,,\"0,00\",EUR,\"-0,92\",\"15,98\",\"0,00\",\"0,00\",seefahrer1@chello.at,"
        + ",Europe/Berlin,,,,,,,\"\",\"\"\n"

        + "61S49182A0251935X,,10.05.2017,PayPal Express-Zahlung,\"shop checkout\",,"
        + "\"shop checkout\",\"30541 / 88649262\",\"Vendula Rosan\",vendula_rosan@online.de,"
        + "\"Vendula, Rosan\","
        + "\"Am Entenpfad 11\",,Bürstadt,,68642,DE,,Y,,\"28,80\",0,,,,,,,,,,,\"0,00\",EUR,"
        + "\"-1,33\",\"27,47\",\"0,00\",\"0,00\",vendula_rosan@online.de,,Europe/Berlin,,,,,,,"
        + "\"\",\"\"\n";

    ArrayList<AcquirerTransaction> items =
      (ArrayList<AcquirerTransaction>) CsvTransactionImporter.readItemsFromString(content,
        ImportFormat.OTHERPAYPAL);

    Assert.assertEquals(3, items.size());

  }

  @Test
  public void paypalImportFromStringCleaned() throws UnsupportedEncodingException,
    ParseException {
    String content =
        "\"Transaktionscode\",\"Zugehöriger Transaktionscode\",\"Datum\",\"Typ\",\"Betreff\","
        + "\"Artikelnummer\",\"Artikelbezeichnung\",\"Rechnungsnummer\",\"Name\",\"E-Mail\","
        + "\"Versandname\",\"Versandadresse Zeile 1\",\"Versandadresse Zeile 2\","
        + "\"Lieferadresse Ort\",\"Versandadresse Staat/Provinz\",\"Versandadresse PLZ\","
        + "\"Lieferadresse Land\",\"Versandmethode\",\"Adress-Status\",\"Telefon\","
        + "\"Bruttobetrag\",\"Empfangsnummer\",\"Benutzerdefiniertes Feld\","
        + "\"Name Option 1\",\"Wert Option 1\",\"Name Option 2\",\"Wert Option 2\","
        + "\"Hinweis\",\"Auktions-Site\",\"eBay-Mitgliedsname\",\"Artikel-URL\",\"Auktionsende\","
        + "\"Versicherungsbetrag\",\"Währung\",\"Gebühren\",\"Nettobetrag\",\"Versandbetrag\","
        + "\"Umsatzsteuerbetrag\",\"An E-Mail\",\"Uhrzeit\",\"Zeitzone\","
        + "\"Rechnungsadresse Zeile 1\",\"Rechnungsadresse Zeile 2\",\"Rechnungsadresse Ort\","
        + "\"Rechnungsadresse Staat/Provinz\",\"Rechnungsadresse PLZ\",\"Rechnungsadresse Land\","
        + "\"Filiale\",\"Kasse\"\n"

        + "5D176369R2459315J,25303514P1331063E,27.04.2017,Rückzahlung,,,\"74047*74.85*CHF\","
        + "\"28540 / 84826431\",\"Lisa Gelz\",gelz.lisa@gmail.com,\"Lisa, Gelz\",,,,,,,,N,,"
        + "\"-74,85\",0,,,,,,\"refund issued for the case\",,,,,\"0,00\",CHF,\"3,29\",\"-71,56\","
        + "\"0,00\",\"0,00\",gelz.lisa@gmail.com,,Europe/Berlin,,,,,,,\"\",\"\"\n"

        + "0900083133707971E,,10.05.2017,PayPal Express-Zahlung,\"shop checkout\",,"
        + "\"shop checkout\",\"30540 / 88648452\",\"Heinrich Löwy\",seefahrer1@chello.at,"
        + "\"Frau Tanja Löwy\",\"Linzerstrasse\",\"30-32/3/3\",Purkersdorf,,3002,AT,,Y,,\"16,90\","
        + "0,,,,,,,,,,,\"0,00\",EUR,\"-0,92\",\"15,98\",\"0,00\",\"0,00\",seefahrer1@chello.at,"
        + ",Europe/Berlin,,,,,,,\"\",\"\"\n"

        + "61S49182A0251935X,,10.05.2017,PayPal Express-Zahlung,\"shop checkout\",,"
        + "\"shop checkout\",\"30541 / 88649262\",\"Vendula Rosan\",vendula_rosan@online.de,"
        + "\"Vendula, Rosan\","
        + "\"Am Entenpfad 11\",,Bürstadt,,68642,DE,,Y,,\"28,80\",0,,,,,,,,,,,\"0,00\",EUR,"
        + "\"-1,33\",\"27,47\",\"0,00\",\"0,00\",vendula_rosan@online.de,,Europe/Berlin,,,,,,,"
        + "\"\",\"\"\n";

    ArrayList<AcquirerTransaction> items =
      (ArrayList<AcquirerTransaction>) CsvTransactionImporter.readItemsFromString(content,
        ImportFormat.OTHERPAYPAL);

    Assert.assertEquals(3, items.size());

    Assert.assertEquals(null, items.get(0).getAmount());
    Assert.assertEquals(16.90, items.get(1).getAmount().doubleValue(), 0.01);
    Assert.assertEquals(28.80, items.get(2).getAmount().doubleValue(), 0.01);

    Assert.assertEquals(Arrays.asList(-74.85, 16.90, 28.80),
      items.stream().map(ta -> ta.getAmountOrig().doubleValue()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("CHF", "EUR", "EUR"),
      items.stream().map(ta -> ta.getCurrencyOrig()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(null, "EUR", "EUR"),
      items.stream().map(ta -> ta.getCurrency()).collect(Collectors.toList()));


    Assert.assertEquals(Arrays.asList("84826431", "88648452", "88649262"),
      items.stream().map(ta -> ta.getPspTransactionId()).collect(Collectors.toList()));


    Assert.assertEquals(Arrays.asList(dtf.parse("20170427"), dtf.parse("20170510"),
      dtf.parse("20170510")),
      items.stream().map(ta -> ta.getSettlementDate()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(dtf.parse("20170427"), dtf.parse("20170510"),
      dtf.parse("20170510")),
      items.stream().map(ta -> ta.getTransactionDate()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("paypal", "paypal", "paypal"),
      items.stream().map(ta -> ta.getAcquirerName()).collect(Collectors.toList()));
    Assert.assertEquals(Arrays.asList("", "", ""),
      items.stream().map(ta -> ta.getCardType()).collect(Collectors.toList()));
  }
}
