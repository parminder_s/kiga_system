package com.kiga.accounting.dunning.web;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.accounting.service.InvoiceService;
import com.kiga.security.services.SecurityService;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 2/19/17.
 */
public class OverdueInvoicesControllerTest {
  private InvoiceService invoiceServiceMock = mock(InvoiceService.class);
  private SecurityService securityServiceMock = mock(SecurityService.class);
  private OverdueInvoicesController overdueInvoicesController;

  @Before
  public void beforeTests() {
    overdueInvoicesController = new OverdueInvoicesController(invoiceServiceMock,
      securityServiceMock);
  }

  @Test
  public void testGetAll() {
    overdueInvoicesController.getAll();
    verify(invoiceServiceMock).getAllOverdue();
  }
}
