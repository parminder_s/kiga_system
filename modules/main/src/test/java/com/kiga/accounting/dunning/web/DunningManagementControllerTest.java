package com.kiga.accounting.dunning.web;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.accounting.dunning.service.DunningEmailService;
import com.kiga.accounting.dunning.service.DunningLevelService;
import com.kiga.accounting.dunning.web.request.DecreaseLevelRequest;
import com.kiga.accounting.dunning.web.request.IncreaseLevelRequest;
import com.kiga.accounting.dunning.web.request.PdfRequest;
import com.kiga.accounting.dunning.web.request.SendEmailRequest;
import com.kiga.security.services.SecurityService;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author bbs
 * @since 2/20/17.
 */
public class DunningManagementControllerTest {
  private DunningEmailService dunningEmailServiceMock = mock(DunningEmailService.class);
  private DunningLevelService dunningLevelServiceMock = mock(DunningLevelService.class);
  private SecurityService securityService = mock(SecurityService.class);
  private DunningManagementController dunningManagementController;

  @Before
  public void beforeTests() {
    dunningManagementController = new DunningManagementController(dunningEmailServiceMock,
      dunningLevelServiceMock, securityService);
  }

  @Test
  public void testSendEmail() {
    SendEmailRequest sendEmailRequest = new SendEmailRequest();
    dunningManagementController.sendEmail(sendEmailRequest);
    verify(dunningEmailServiceMock).sendEmails(sendEmailRequest);
  }

  @Test
  public void testDecreaseLevel() {
    DecreaseLevelRequest decreaseLevelRequest = new DecreaseLevelRequest();
    dunningManagementController.decreaseLevel(decreaseLevelRequest);
    verify(dunningLevelServiceMock).decrease(decreaseLevelRequest);
  }

  @Test
  public void testIncreaseLevel() {
    IncreaseLevelRequest increaseLevelRequest = new IncreaseLevelRequest();
    dunningManagementController.increaseLevel(increaseLevelRequest);
    verify(dunningLevelServiceMock).increase(increaseLevelRequest);
  }

  @Ignore
  @Test
  public void testDownloadPdf() throws IOException, COSVisitorException {
    List<Long> invoiceIds = new ArrayList<Long>();
    when(dunningEmailServiceMock.dunningDocumentForInvoices(eq(invoiceIds)))
      .thenReturn(null);
    dunningManagementController.downloadPdf(invoiceIds, new MockHttpServletResponse());
    verify(dunningEmailServiceMock).dunningDocumentForInvoices(eq(invoiceIds));
  }
}
