package com.kiga.accounting.dunning.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.accounting.dunning.service.DunningLevelService;
import com.kiga.accounting.dunning.web.request.AddDunningLevelRequest;
import com.kiga.accounting.dunning.web.request.GetDunningLevelRequest;
import com.kiga.accounting.dunning.web.request.RemoveDunningLevelRequest;
import com.kiga.accounting.dunning.web.request.UpdateDunningLevelRequest;
import com.kiga.security.services.SecurityService;
import com.kiga.shop.service.CountryService;
import com.kiga.shop.web.response.CountryViewModel;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 2/19/17.
 */
public class DunningLevelsControllerTest {
  private static final long ID = 1L;
  private SecurityService securityServiceMock = mock(SecurityService.class);
  private HttpServletRequest httpServletRequestMock = mock(HttpServletRequest.class);
  private DunningLevelsController dunningLevelsController;
  private DunningLevelService dunningLevelServiceMock = mock(DunningLevelService.class);
  private CountryService countryServiceMock = mock(CountryService.class);

  @Before
  public void prepareTests() {
    dunningLevelsController = new DunningLevelsController(dunningLevelServiceMock,
      countryServiceMock, securityServiceMock);
  }

  @Test
  public void testGetLevels() {
    dunningLevelsController.getLevels();
    verify(dunningLevelServiceMock).getAll();
  }

  @Test
  public void testAdd() {
    AddDunningLevelRequest addDunningLevelRequest = new AddDunningLevelRequest();
    dunningLevelsController.add(addDunningLevelRequest);
    verify(dunningLevelServiceMock).add(addDunningLevelRequest);
  }

  @Test
  public void testUpdate() {
    UpdateDunningLevelRequest updateDunningLevelRequest = new UpdateDunningLevelRequest();
    updateDunningLevelRequest.setId(ID);
    dunningLevelsController.update(updateDunningLevelRequest);
    verify(dunningLevelServiceMock).update(updateDunningLevelRequest);
  }

  @Test
  public void testRemove() {
    RemoveDunningLevelRequest removeDunningLevelRequest = new RemoveDunningLevelRequest();
    removeDunningLevelRequest.setIds(Collections.singletonList(ID));
    dunningLevelsController.remove(removeDunningLevelRequest);
    verify(dunningLevelServiceMock).remove(removeDunningLevelRequest);
  }

  @Test
  public void getOne() {
    GetDunningLevelRequest getDunningLevelRequest = new GetDunningLevelRequest();
    getDunningLevelRequest.setId(ID);
    dunningLevelsController.getOne(getDunningLevelRequest);
    verify(dunningLevelServiceMock).getOne(ID);
  }

  @Test
  public void getCountries() {
    CountryViewModel countryViewModel = new CountryViewModel();
    when(countryServiceMock.getAll()).thenReturn(Arrays.asList(countryViewModel, countryViewModel));
    List<CountryViewModel> countries = dunningLevelsController.getCountries();
    assertNotNull(countries);
    assertEquals(1, countries.size());
    assertSame(countryViewModel, countries.get(0));
  }
}
