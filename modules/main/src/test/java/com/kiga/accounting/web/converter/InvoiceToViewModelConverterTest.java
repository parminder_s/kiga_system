package com.kiga.accounting.web.converter;

import static org.exparity.hamcrest.date.DateMatchers.sameDay;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.dunning.web.converter.DunningLevelToViewModelConverter;
import com.kiga.accounting.web.response.InvoiceViewModel;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by rainerh on 14.03.17.
 */
public class InvoiceToViewModelConverterTest {
  @Test
  public void mapDefault() {
    Invoice invoice = new Invoice();
    invoice.setId(5L);
    invoice.setInvoiceNumber("AT023");
    invoice.setCustomerEmail("foo@bar.com");
    invoice.setCustomerName("Foo Bar");
    Date orderDate = DateTime.parse("2015-05-01").toDate();
    invoice.setOrderDate(orderDate);
    invoice.setOrderNumber("178");
    Date invoiceDate = DateTime.parse("2015-05-02").toDate();
    invoice.setInvoiceDate(invoiceDate);
    invoice.setDunningEmailSent(true);

    Date dueDate = DateTime.parse("2015-05-05").toDate();
    invoice.setDueDate(dueDate);
    invoice.setAmount(new BigDecimal(5.12));
    invoice.setCountryCode("de");
    invoice.setSettled(true);
    invoice.setPaymentTransactions(new ArrayList<>());

    DunningLevelToViewModelConverter dlvmConv = mock(DunningLevelToViewModelConverter.class);
    InvoiceViewModel invoiceViewModel =
      new InvoiceToViewModelConverter(dlvmConv).convertToResponse(invoice);

    assertEquals(5L, invoiceViewModel.getInvoiceId());
    assertEquals("AT023", invoiceViewModel.getInvoiceNumber());
    assertEquals("foo@bar.com", invoiceViewModel.getCustomerEmail());
    assertEquals("foo@bar.com", invoiceViewModel.getCustomerEmail());
    assertEquals("Foo Bar", invoiceViewModel.getCustomerName());

    assertThat(invoiceViewModel.getOrderDate(), sameDay(DateTime.parse("2015-05-01").toDate()));
    assertThat(invoiceViewModel.getInvoiceDate(), sameDay(DateTime.parse("2015-05-02").toDate()));
    assertThat(invoiceViewModel.getDueDate(), sameDay(DateTime.parse("2015-05-05").toDate()));
  }

}
