package com.kiga.accounting.transactions.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.service.InvoiceService;
import com.kiga.accounting.transactions.domain.PaymentTransaction;
import com.kiga.accounting.transactions.repository.PaymentTransactionRepository;
import com.kiga.accounting.transactions.web.converter.PaymentTransactionToViewModelConverter;
import com.kiga.accounting.transactions.web.request.AddPaymentTransactionRequest;
import com.kiga.accounting.transactions.web.request.FindPaymentTransactionRequest;
import com.kiga.accounting.transactions.web.request.UpdatePaymentTransactionRequest;
import com.kiga.accounting.transactions.web.response.PaymentTransactionViewModel;
import com.kiga.accounting.web.request.SingleInvoiceRequest;
import com.kiga.accounting.web.response.InvoiceViewModel;
import com.kiga.shop.service.CountryShopService;
import com.kiga.shop.web.response.CountryShopViewModel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author bbs
 * @since 2/9/17.
 */
public class PaymentTransactionServiceTest {
  private static final long VALID_ID = 1L;
  private static final long INVALID_ID = 0L;
  private static final String BANK = "BANK";
  private static final BigDecimal AMOUNT = BigDecimal.TEN;
  private static final String INVOICE = "Invoice";
  private static final Date TRANSFER_DATE = new Date();
  private static final String WRONG_INVOICE_NUMBER = "Wrong Invoice Number";
  private static final String COUNTRY_CODE = "de";

  private InvoiceService invoiceServiceMock = mock(InvoiceService.class);
  private PaymentTransactionRepository paymentTransactionRepositoryMock = mock(
    PaymentTransactionRepository.class);
  private PaymentTransactionToViewModelConverter paymentTransactionToViewModelConverterMock = mock(
    PaymentTransactionToViewModelConverter.class);
  private PaymentTransactionService paymentTransactionService;
  private PaymentTransaction paymentTransactionFromFindOne;
  private CountryShopService countryShopServiceMock = mock(CountryShopService.class);
  private InvoiceViewModel invoiceViewModel = new InvoiceViewModel();

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    paymentTransactionService = new PaymentTransactionService(invoiceServiceMock,
      paymentTransactionRepositoryMock,
      paymentTransactionToViewModelConverterMock, countryShopServiceMock);

    Invoice invoice = new Invoice();
    invoice.setInvoiceNumber(INVOICE);
    invoice.setId(VALID_ID);
    invoiceViewModel.setDueAmount(BigDecimal.ZERO);
    invoiceViewModel.setCountryCode(COUNTRY_CODE);
    when(invoiceServiceMock.getEntityByNumber(INVOICE)).thenReturn(invoice);
    when(invoiceServiceMock.getOne(anyLong())).thenReturn(invoiceViewModel);
    when(invoiceServiceMock.getOne(INVOICE)).thenReturn(invoiceViewModel);

    paymentTransactionFromFindOne = new PaymentTransaction();
    paymentTransactionFromFindOne.setInvoice(invoice);
    when(paymentTransactionRepositoryMock.findOne(VALID_ID)).thenReturn(
      paymentTransactionFromFindOne);
  }

  @Test
  public void testGetAll() {
    List<PaymentTransaction> paymentTransactionsMock = mock(List.class);
    when(paymentTransactionRepositoryMock.findAll()).thenReturn(paymentTransactionsMock);
    paymentTransactionService.getAll("all");
    verify(paymentTransactionRepositoryMock).findAll();
    verify(paymentTransactionToViewModelConverterMock).convertToResponse(paymentTransactionsMock);
  }

  @Test
  public void testGetAllByInvoice() {
    Invoice invoice = mock(Invoice.class);
    List<PaymentTransaction> paymentTransactionsMock = mock(List.class);
    when(invoice.getPaymentTransactions()).thenReturn(paymentTransactionsMock);
    when(invoiceServiceMock.getEntityByNumber(INVOICE)).thenReturn(invoice);
    paymentTransactionService.getAll(INVOICE);
    verify(paymentTransactionToViewModelConverterMock).convertToResponse(paymentTransactionsMock);
  }

  @Test
  public void testRemove() {
    paymentTransactionService.remove(Collections.singletonList(VALID_ID));
    verify(paymentTransactionRepositoryMock).delete(paymentTransactionFromFindOne);
  }

  @Test
  public void testAddWithProperValues() {
    AddPaymentTransactionRequest addPaymentTransactionRequest = new AddPaymentTransactionRequest();
    addPaymentTransactionRequest.setAmount(AMOUNT);
    addPaymentTransactionRequest.setBank(BANK);
    addPaymentTransactionRequest.setPaidInvoiceNumber(INVOICE);
    addPaymentTransactionRequest.setTransferDate(TRANSFER_DATE);

    paymentTransactionService.add(addPaymentTransactionRequest);

    ArgumentCaptor<PaymentTransaction> paymentTransactionArgumentCaptor = ArgumentCaptor
      .forClass(PaymentTransaction.class);
    verify(paymentTransactionRepositoryMock).save(paymentTransactionArgumentCaptor.capture());

    PaymentTransaction capturedPaymentTransaction = paymentTransactionArgumentCaptor.getValue();

    assertEquals(addPaymentTransactionRequest.getAmount(), capturedPaymentTransaction.getAmount());
    assertEquals(addPaymentTransactionRequest.getBank(), capturedPaymentTransaction.getBank());
    assertEquals(addPaymentTransactionRequest.getBankTransactionId(),
      capturedPaymentTransaction.getBankTransactionId());
    assertEquals(addPaymentTransactionRequest.getPaidInvoiceNumber(),
      capturedPaymentTransaction.getInvoice().getInvoiceNumber());
    assertEquals(addPaymentTransactionRequest.getTransferDate(),
      capturedPaymentTransaction.getTransferDate());
  }

  @Test
  public void testAddWithProperValuesAndSettled() {
    AddPaymentTransactionRequest addPaymentTransactionRequest = new AddPaymentTransactionRequest();
    addPaymentTransactionRequest.setAmount(AMOUNT);
    addPaymentTransactionRequest.setBank(BANK);
    addPaymentTransactionRequest.setPaidInvoiceNumber(INVOICE);
    addPaymentTransactionRequest.setTransferDate(TRANSFER_DATE);
    addPaymentTransactionRequest.setSettle(true);

    paymentTransactionService.add(addPaymentTransactionRequest);

    ArgumentCaptor<PaymentTransaction> paymentTransactionArgumentCaptor = ArgumentCaptor
      .forClass(PaymentTransaction.class);
    verify(paymentTransactionRepositoryMock).save(paymentTransactionArgumentCaptor.capture());

    PaymentTransaction capturedPaymentTransaction = paymentTransactionArgumentCaptor.getValue();

    Invoice invoice = capturedPaymentTransaction.getInvoice();
    assertTrue(invoice.isSettled());
    verify(invoiceServiceMock).save(invoice);
  }

  @Test
  public void testUpdateWithProperValuesAndSettled() {
    PaymentTransaction paymentTransaction = new PaymentTransaction();
    paymentTransaction.setId(VALID_ID);
    when(paymentTransactionRepositoryMock.findOne(VALID_ID)).thenReturn(paymentTransaction);

    UpdatePaymentTransactionRequest updatePaymentTransactionRequest = new
      UpdatePaymentTransactionRequest();
    updatePaymentTransactionRequest.setAmount(AMOUNT);
    updatePaymentTransactionRequest.setBank(BANK);
    updatePaymentTransactionRequest.setPaidInvoiceNumber(INVOICE);
    updatePaymentTransactionRequest.setTransferDate(TRANSFER_DATE);
    updatePaymentTransactionRequest.setId(VALID_ID);
    updatePaymentTransactionRequest.setSettle(true);
    paymentTransactionService.update(updatePaymentTransactionRequest);

    ArgumentCaptor<PaymentTransaction> paymentTransactionArgumentCaptor = ArgumentCaptor
      .forClass(PaymentTransaction.class);
    verify(paymentTransactionRepositoryMock).save(paymentTransactionArgumentCaptor.capture());

    PaymentTransaction capturedPaymentTransaction = paymentTransactionArgumentCaptor.getValue();

    Invoice invoice = capturedPaymentTransaction.getInvoice();
    assertTrue(invoice.isSettled());
    verify(invoiceServiceMock).save(invoice);
  }

  @Test
  public void testUpdateWithProperValues() {
    PaymentTransaction paymentTransaction = new PaymentTransaction();
    paymentTransaction.setId(VALID_ID);
    when(paymentTransactionRepositoryMock.findOne(VALID_ID)).thenReturn(paymentTransaction);

    UpdatePaymentTransactionRequest updatePaymentTransactionRequest = new
      UpdatePaymentTransactionRequest();
    updatePaymentTransactionRequest.setAmount(AMOUNT);
    updatePaymentTransactionRequest.setBank(BANK);
    updatePaymentTransactionRequest.setPaidInvoiceNumber(INVOICE);
    updatePaymentTransactionRequest.setTransferDate(TRANSFER_DATE);
    updatePaymentTransactionRequest.setId(VALID_ID);
    paymentTransactionService.update(updatePaymentTransactionRequest);

    verify(paymentTransactionRepositoryMock).findOne(VALID_ID);
    verify(paymentTransactionRepositoryMock).save(paymentTransaction);
  }

  @Test
  public void testGetOne() {
    FindPaymentTransactionRequest findPaymentTransactionRequest = new FindPaymentTransactionRequest(
      1L);
    paymentTransactionService.getOne(findPaymentTransactionRequest);
    verify(paymentTransactionToViewModelConverterMock)
      .convertToResponse(paymentTransactionFromFindOne);
  }

  @Test
  public void testGetModelByInvoice() {
    SingleInvoiceRequest singleInvoiceRequest = new SingleInvoiceRequest();
    singleInvoiceRequest.setInvoiceNumber(INVOICE);
    CountryShopViewModel countryShopViewModel = new CountryShopViewModel();
    countryShopViewModel.setBankName(BANK);
    when(countryShopServiceMock.getByCode(invoiceViewModel.getCountryCode()))
      .thenReturn(countryShopViewModel);

    PaymentTransactionViewModel modelByInvoice = paymentTransactionService
      .getModelByInvoice(singleInvoiceRequest);

    assertNotNull(modelByInvoice);
    assertEquals(invoiceViewModel.getDueAmount(), modelByInvoice.getAmount());
    assertEquals(invoiceViewModel.getInvoiceNumber(), modelByInvoice.getPaidInvoiceNumber());
    assertEquals(invoiceViewModel, modelByInvoice.getInvoiceModel());
    assertEquals(BANK, modelByInvoice.getBank());
    assertNull(modelByInvoice.getBankTransactionId());
  }

  @Test
  public void testGetModelByInvoiceFullAmountNoCountryShop() {
    InvoiceViewModel invoiceViewModel = new InvoiceViewModel();
    invoiceViewModel.setAmount(BigDecimal.ZERO);
    invoiceViewModel.setCountryCode(COUNTRY_CODE);
    when(invoiceServiceMock.getOne(INVOICE)).thenReturn(invoiceViewModel);

    SingleInvoiceRequest singleInvoiceRequest = new SingleInvoiceRequest();
    singleInvoiceRequest.setInvoiceNumber(INVOICE);

    PaymentTransactionViewModel modelByInvoice = paymentTransactionService
      .getModelByInvoice(singleInvoiceRequest);

    assertEquals(invoiceViewModel.getAmount(), modelByInvoice.getAmount());
    assertNull(modelByInvoice.getBank());
  }
}
