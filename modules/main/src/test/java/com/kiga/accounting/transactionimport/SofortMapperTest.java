package com.kiga.accounting.transactionimport;

import com.kiga.accounting.domain.AcquirerTransaction;
import com.kiga.accounting.formats.CsvTransactionImporter;
import com.kiga.accounting.formats.ImportFormat;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.InputStreamResource;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mfit on 05.12.17.
 */
public class SofortMapperTest {

  public static DateFormat dtf = new SimpleDateFormat("yyyyMMdd");

  @Test
  public void testImport() throws java.text.ParseException, UnsupportedEncodingException {
    String content =
      "Transaction.user_id;Transaction.project_id;Transaction.transaction;"
      + "Transaction.sender_holder;Transaction.sender_bank_holder;"
      + "Transaction.sender_account_number;Transaction.sender_bank_code;"
      + "Transaction.sender_bank_name;Transaction.sender_bank_bic;Transaction.sender_iban;"
      + "Transaction.sender_country_id;Transaction.recipient_holder;"
      + "Transaction.recipient_account_number;Transaction.recipient_bank_code;"
      + "Transaction.recipient_bank_name;Transaction.recipient_bank_bic;Transaction.recipient_iban;"
      + "Transaction.recipient_country_id;Transaction.amount;Transaction.amount_refunded;"
      + "Transaction.currency_id;Transaction.exchange_rate;Transaction.reason_1;"
      + "Transaction.reason_2;Transaction.test_transaction;Transaction.notification_status;"
      + "Transaction.notification_attempts;Transaction.notification_last_attempt;"
      + "Transaction.status;Transaction.status_modified;Transaction.user_costs;"
      + "Transaction.user_costs_currency_id;Transaction.user_costs_guarantee;Transaction.created;"
      + "Transaction.internationalization_fees\n"
      + "98641;203249;98641-203249-56F3C72F-A9B6;;Magdalena Leder;12983731301;20715;"
      + "SPARKASSE / ERSTE BANK;STSPAT2GXXX;AT262081512562301301;AT;"
      + "HOHE Medien OG;00040874216;20815;SPARKASSE / ERSTE BANK;STSPAT2GXXX;AT742081500040874216;"
      + "AT;39,90;0,00;EUR;1,0000;000065329340MPAY24;;;1;1;2016-03-24 11:58:14;untraceable;"
      + "2016-03-24 11:58:12;1,05;EUR;0.00;2016-03-24 11:58:12;0\n"
      + "98641;203249;98641-203249-56EE4CDF-F9A1;;Katharina Grugger;35201211111;42000;"
      + "VOLKSBANK BIEN-BAD;VBOEATWWXXX;AT214300032345232011;AT;HOHE Medien OG;00040874216;20815;"
      + "SPARKASSE / ERSTE BANK;STSPAT2GXXX;AT742081500040874216;AT;39,90;0,00;"
      + "EUR;1,0000;000065113896MPAY24;;;1;1;2016-03-20 08:13:38;untraceable;"
      + "2016-03-20 08:13:36;1,05;EUR;0.00;2016-03-20 08:13:36;0\n"
      + "98641;203249;98641-203249-56E83819-340F;;LABUCHER KARIN;0828559064632303242340;08185;"
      + "RAIFFEISENKASSE TAURERER-AHRNTAL / CASSA RAIFFEISEN TURES-AURINA;RZSBIT21440;"
      + "IT80L0828559064000303242340;IT;HOHE Medien OG;0603312001;10070024;Deutsche Bank;"
      + "DEUTDEDBBER;DE70100700240603312001;DE;39,90;0,00;EUR;1,0000;000064895362MPAY24;;;1;1;"
      + "2016-03-15 17:35:21;untraceable;2016-03-15 17:35:20;1,05;EUR;0.00;2016-03-15 17:35:20;0\n"
      + "98641;203249;98641-203249-56E54179-B133;;Sabine Meischberger;123459300;61410000;"
      + "Volksbank Vippingen-Schwenningen;GENODE61VS1;DE70621300000149059300;DE;HOHE Medien OG;"
      + "0603312001;10070024;Deutsche Bank;DEUTDEDBBER;DE70100700240603312001;DE;39,90;0,00;"
      + "EUR;1,0000;000064765620MPAY24;;;1;1;2016-03-13 11:33:56;untraceable;2016-03-13 11:33:55;"
      + "1,05;EUR;0.00;2016-03-13 11:33:55;0\n"
      + "98641;203249;98641-203249-56E1AA1B-EE96;;Luposcips Kathrin;103062830;25720001;"
      + "Sparkasse Pelle;NOLADE21CEL;DE71257500012152077830;DE;HOHE Medien OG;"
      + "0603312001;10070024;Deutsche Bank;DEUTDEDBBER;DE70100700240603312001;"
      + "DE;39,90;0,00;EUR;1,0000;000064637050MPAY24;;;1;1;2016-03-10 18:10:59;untraceable;"
      + "2016-03-10 18:10:58;1,05;EUR;0.00;2016-03-10 18:10:58;0\n"
      + "98641;203249;98641-203249-56D539D2-D43C;;Tatjana Posch;580352363;45850;"
      + "VOLKSBANK LANGECK;VBOEATWWLAN;AT284585000340370763;AT;HOHE Medien OG;00040874216;20815;"
      + "SPARKASSE / ERSTE BANK;STSPAT2GXXX;AT742081500040874216;AT;39,90;0,00;EUR;1,0000;"
      + "000064129862MPAY24;;;1;1;2016-03-01 07:43:50;untraceable;2016-03-01 07:43:48;1,05;"
      + "EUR;0.00;2016-03-01 07:43:48;0\n";

    List<AcquirerTransaction> items = CsvTransactionImporter.readItemsFromResource(
      new InputStreamResource(new ByteArrayInputStream(content.getBytes("UTF-8"))),
      ImportFormat.SOFORT, "sofort-test.csv");

    Assert.assertEquals(6, items.size());

    Assert.assertEquals(Arrays.asList("sofort-test.csv", "sofort-test.csv",
      "sofort-test.csv", "sofort-test.csv", "sofort-test.csv",
      "sofort-test.csv"),
      items.stream().map(AcquirerTransaction::getSourceFileName)
        .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(39.90, 39.90, 39.90, 39.90, 39.90, 39.90),
      items.stream().map(el -> el.getAmount().doubleValue()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("EUR", "EUR", "EUR", "EUR", "EUR", "EUR"),
      items.stream().map(AcquirerTransaction::getCurrency).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(39.90, 39.90, 39.90, 39.90, 39.90, 39.90),
      items.stream().map(el -> el.getAmountOrig().doubleValue()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("EUR", "EUR", "EUR", "EUR", "EUR", "EUR"),
      items.stream().map(AcquirerTransaction::getCurrencyOrig).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("65329340", "65113896", "64895362", "64765620", "64637050",
      "64129862"),
      items.stream().map(AcquirerTransaction::getPspTransactionId).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("sofort", "sofort", "sofort", "sofort", "sofort", "sofort"),
      items.stream().map(AcquirerTransaction::getAcquirerName).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("", "", "", "", "", ""),
      items.stream().map(AcquirerTransaction::getCardType).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(dtf.parse("20160324"), dtf.parse("20160320"),
      dtf.parse("20160315"), dtf.parse("20160313"),
      dtf.parse("20160310"), dtf.parse("20160301")),
      items.stream().map(AcquirerTransaction::getTransactionDate).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("98641-203249-56F3C72F-A9B6", "98641-203249-56EE4CDF-F9A1",
      "98641-203249-56E83819-340F", "98641-203249-56E54179-B133",
      "98641-203249-56E1AA1B-EE96", "98641-203249-56D539D2-D43C"),
      items.stream().map(AcquirerTransaction::getAcquirerTransactionId)
        .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("STSPAT2GXXX", "STSPAT2GXXX", "DEUTDEDBBER", "DEUTDEDBBER",
      "DEUTDEDBBER", "STSPAT2GXXX"),
      items.stream().map(AcquirerTransaction::getDestinationBankBic)
        .collect(Collectors.toList()));

  }

  @Test
  public void testImportWithAlternateDateFormat()
    throws java.text.ParseException, UnsupportedEncodingException {
    String content =
      "Transaction.user_id;Transaction.project_id;Transaction.transaction;"
      + "Transaction.sender_holder;Transaction.sender_bank_holder;"
      + "Transaction.sender_account_number;Transaction.sender_bank_code;"
      + "Transaction.sender_bank_name;Transaction.sender_bank_bic;Transaction.sender_iban;"
      + "Transaction.sender_country_id;Transaction.recipient_holder;"
      + "Transaction.recipient_account_number;Transaction.recipient_bank_code;"
      + "Transaction.recipient_bank_name;Transaction.recipient_bank_bic;"
      + "Transaction.recipient_iban;Transaction.recipient_country_id;"
      + "Transaction.amount;Transaction.amount_refunded;Transaction.currency_id;"
      + "Transaction.exchange_rate;Transaction.reason_1;Transaction.reason_2;"
      + "Transaction.test_transaction;Transaction.notification_status;"
      + "Transaction.notification_attempts;Transaction.notification_last_attempt;"
      + "Transaction.status;Transaction.status_modified;Transaction.user_costs;"
      + "Transaction.user_costs_currency_id;Transaction.user_costs_guarantee;"
      + "Transaction.created;Transaction.internationalization_fees\n"
      + "11245;252148;12641-201249-55DB8D4E-5F2X;;Jacqueline Muster;1407555547;50055551;"
      + "ING-DiBa;INGDDEFFXXX;DE12301205222405555547;DE;HOHE Medien OG;605512111;10055524;"
      + "Deutsche Bank;DEUTDEDBBER;DE70105550240605552001;DE;25,9;0;EUR;1;000098301524MPAY24;;;"
      + "1;1;09.10.2017 16:55;pending;09.10.2017 16:55;0,77;EUR;0.00;09.10.2017 16:55;0";

    List<AcquirerTransaction> items = CsvTransactionImporter.readItemsFromResource(
      new InputStreamResource(new ByteArrayInputStream(content.getBytes("UTF-8"))),
      ImportFormat.SOFORT, "sofort-test.csv");

    Assert.assertEquals(1, items.size());

    Assert.assertEquals(Arrays.asList("sofort-test.csv"),
      items.stream().map(AcquirerTransaction::getSourceFileName)
        .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(25.90),
      items.stream().map(el -> el.getAmount().doubleValue()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("EUR"),
      items.stream().map(AcquirerTransaction::getCurrency).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(25.90),
      items.stream().map(el -> el.getAmountOrig().doubleValue()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("EUR"),
      items.stream().map(AcquirerTransaction::getCurrencyOrig).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("98301524"),
      items.stream().map(AcquirerTransaction::getPspTransactionId).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("sofort"),
      items.stream().map(AcquirerTransaction::getAcquirerName).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(""),
      items.stream().map(AcquirerTransaction::getCardType).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(dtf.parse("20171009")),
      items.stream().map(AcquirerTransaction::getTransactionDate).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("12641-201249-55DB8D4E-5F2X"),
      items.stream().map(AcquirerTransaction::getAcquirerTransactionId)
        .collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("DEUTDEDBBER"),
      items.stream().map(AcquirerTransaction::getDestinationBankBic)
        .collect(Collectors.toList()));

  }

}
