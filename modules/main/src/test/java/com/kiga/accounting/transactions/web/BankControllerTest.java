package com.kiga.accounting.transactions.web;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.accounting.transactions.service.BankService;
import com.kiga.accounting.transactions.web.request.BanksByInvoiceRequest;
import com.kiga.accounting.transactions.web.request.BanksByTransactionRequest;
import org.junit.Before;
import org.junit.Test;

/**
 * @author bbs
 * @since 2/18/17.
 */
public class BankControllerTest {
  private BankService bankServiceMock = mock(BankService.class);
  private BankController bankController;

  @Before
  public void beforeTests() {
    bankController = new BankController(bankServiceMock);
  }

  @Test
  public void testGetAll() {
    bankController.getAll();
    verify(bankServiceMock).getAllBanks();
  }

  @Test
  public void testGetByInvoice() {
    BanksByInvoiceRequest banksByInvoiceRequest = new BanksByInvoiceRequest();
    banksByInvoiceRequest.setInvoiceNumber("123");
    bankController.getByInvoiceNumber(banksByInvoiceRequest);
    verify(bankServiceMock).getByInvoiceNumber(banksByInvoiceRequest.getInvoiceNumber());
  }

  @Test
  public void testGetByPaymentTransactionId() {
    BanksByTransactionRequest banksByTransactionRequest = new BanksByTransactionRequest();
    banksByTransactionRequest.setPaymentTransactionId(1L);
    bankController.getByPaymentTransactionId(banksByTransactionRequest);
    verify(bankServiceMock)
        .getByPaymentTransactionId(banksByTransactionRequest.getPaymentTransactionId());
  }
}
