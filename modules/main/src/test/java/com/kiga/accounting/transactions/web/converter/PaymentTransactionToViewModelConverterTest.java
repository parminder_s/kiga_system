package com.kiga.accounting.transactions.web.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.transactions.domain.PaymentTransaction;
import com.kiga.accounting.transactions.web.response.PaymentTransactionViewModel;
import com.kiga.accounting.web.converter.InvoiceToViewModelConverter;
import com.kiga.accounting.web.response.InvoiceViewModel;
import org.junit.Before;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author bbs
 * @since 2/10/17.
 */
public class PaymentTransactionToViewModelConverterTest {
  private static final String BANK_TRANSACTION_ID = "QWERTY";
  private static final Long VALID_ID = 1L;
  private static final Date TRANSFER_DATE = new Date();
  private static final String INVOICE = "1/2017";
  private static final BigDecimal AMOUNT = BigDecimal.TEN;
  private static final String BANK = "MOCKEDBANK";

  private PaymentTransactionToViewModelConverter paymentTransactionToViewModelConverter;
  private InvoiceToViewModelConverter invoiceToViewModelConverterMock = mock(
    InvoiceToViewModelConverter.class);

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    InvoiceViewModel invoiceViewModel = new InvoiceViewModel();
    invoiceViewModel.setInvoiceNumber(INVOICE);
    when(invoiceToViewModelConverterMock.convertToResponse(any(Invoice.class)))
      .thenReturn(invoiceViewModel);
    paymentTransactionToViewModelConverter = new PaymentTransactionToViewModelConverter(
      invoiceToViewModelConverterMock, new ModelMapper());
  }

  @Test
  public void testConvertEntity() {
    PaymentTransaction paymentTransaction = new PaymentTransaction();
    paymentTransaction.setId(VALID_ID);
    paymentTransaction.setTransferDate(TRANSFER_DATE);
    paymentTransaction.setAmount(AMOUNT);
    paymentTransaction.setBank(BANK);
    paymentTransaction.setBankTransactionId(BANK_TRANSACTION_ID);

    Invoice invoice = new Invoice();
    invoice.setInvoiceNumber(INVOICE);
    paymentTransaction.setInvoice(invoice);

    PaymentTransactionViewModel viewModel = paymentTransactionToViewModelConverter
      .convertToResponse(paymentTransaction);

    assertNotNull(viewModel);
    assertEquals(VALID_ID, viewModel.getId());
    assertEquals(TRANSFER_DATE, viewModel.getTransferDate());
    assertEquals(INVOICE, viewModel.getPaidInvoiceNumber());
    assertEquals(BANK, viewModel.getBank());
    assertEquals(AMOUNT, viewModel.getAmount());
    assertEquals(BANK_TRANSACTION_ID, viewModel.getBankTransactionId());
  }
}
