package com.kiga.accounting.transactionimport;

import com.kiga.accounting.domain.AcquirerTransaction;
import com.kiga.accounting.formats.CsvTransactionImporter;
import com.kiga.accounting.formats.ImportFormat;
import com.kiga.accounting.formats.TransactionImporterFactory;
import org.junit.Assert;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by mfit on 14.06.17.
 */
public class HobexMapperTest {

  public static DateFormat dtf = new SimpleDateFormat("yyyyMMdd");

  @Test
  public void hobexImportFromString() throws UnsupportedEncodingException, ParseException {
    String content =
      "TID;Paymuldate;Type;Clearingdate;Turnoverdate;Subtype;Receipt;Nativeamount;Nativecurrency;"
        + "Nativedisagio;Nativetxfee;Nativetax;Nativetransferamount;Referencenumber;Trans-Ref;\n"
        + "3518095;31.05.2017 00:00;ELV;09.05.2017 00:00;09.05.2017 04:32;DE;8591;7,9000;EUR;"
        + "0,1000;0,10;0,0400;7,6600;88588161;00351001151705310011A\n"
        + "3518095;31.05.2017 00:00;ELV;09.05.2017 00:00;09.05.2017 19:49;DE;8596;19,9000;EUR;"
        + "0,2600;0,10;0,0700;19,4700;88628580;00351001151705310011A\n"
        + "3518095;29.05.2017 00:00;ELV;06.05.2017 00:00;06.05.2017 20:04;DE;8562;59,9000;EUR;"
        + "0,7800;0,10;0,1800;58,8400;88452852;00351001151705290046A\n"
        + "3518095;02.05.2017 00:00;ELV;10.04.2017 00:00;10.04.2017 04:32;DE;8262;35,9000;EUR;"
        + "0,4700;0,10;0,1100;35,2200;86876068;00351001151705020048A\n";

    ArrayList<AcquirerTransaction> items =
      (ArrayList<AcquirerTransaction>) CsvTransactionImporter.readItemsFromString(content,
        ImportFormat.HOBEX);

    Assert.assertEquals(4, items.size());
    Assert.assertEquals(Arrays.asList(7.9, 19.9, 59.9, 35.9),
      items.stream().map(ta -> ta.getAmount().doubleValue()).collect(Collectors.toList()));
    Assert.assertEquals(Arrays.asList("88588161", "88628580", "88452852", "86876068"),
      items.stream().map(ta -> ta.getPspTransactionId()).collect(Collectors.toList()));
    Assert.assertEquals(Arrays.asList("EUR", "EUR", "EUR", "EUR"),
      items.stream().map(ta -> ta.getCurrency()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(dtf.parse("20170531"), dtf.parse("20170531"),
      dtf.parse("20170529"), dtf.parse("20170502")),
      items.stream().map(ta -> ta.getSettlementDate()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList(dtf.parse("20170509"), dtf.parse("20170509"),
      dtf.parse("20170506"), dtf.parse("20170410")),
      items.stream().map(ta -> ta.getTransactionDate()).collect(Collectors.toList()));

    Assert.assertEquals(Arrays.asList("hobex", "hobex", "hobex", "hobex"),
      items.stream().map(ta -> ta.getAcquirerName()).collect(Collectors.toList()));
    Assert.assertEquals(Arrays.asList("ELV", "ELV", "ELV", "ELV"),
      items.stream().map(ta -> ta.getCardType()).collect(Collectors.toList()));

    Assert.assertTrue(items.get(0).getSourceContent().length() > 2);
  }
}
