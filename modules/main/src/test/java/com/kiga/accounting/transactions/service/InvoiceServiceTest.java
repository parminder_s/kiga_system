package com.kiga.accounting.transactions.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.accounting.service.InvoiceService;
import com.kiga.accounting.web.converter.InvoiceToViewModelConverter;
import com.kiga.accounting.web.response.InvoiceViewModel;
import com.mysema.query.types.expr.BooleanExpression;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * @author bbs
 * @since 2/11/17.
 */
public class InvoiceServiceTest {
  public static final long ID = 1L;
  private static final String INVOICE_NUMBER = "1/2017";
  private InvoiceRepository invoiceRepositoryMock = mock(InvoiceRepository.class);
  private InvoiceToViewModelConverter invoiceToViewModelConverterMock = mock(
    InvoiceToViewModelConverter.class);
  private InvoiceService invoiceService;

  @Before
  public void prepareTests() {
    invoiceService = new InvoiceService(invoiceRepositoryMock, invoiceToViewModelConverterMock);
  }

  @Test
  public void testGetEntityByNumber() {
    invoiceService.getEntityByNumber(INVOICE_NUMBER);
    verify(invoiceRepositoryMock).findByInvoiceNumber(INVOICE_NUMBER);
  }

  @Test
  public void testGetOneByInvoiceNumber() {
    Invoice invoice = new Invoice();
    when(invoiceRepositoryMock.findByInvoiceNumber("ABC/123")).thenReturn(invoice);
    invoiceService.getOne("ABC/123");
    verify(invoiceRepositoryMock).findByInvoiceNumber("ABC/123");
    verify(invoiceToViewModelConverterMock).convertToResponse(invoice);
  }

  @Test
  public void testMarkDunningEmailSent() {
    Invoice invoice = new Invoice();
    when(invoiceRepositoryMock.findByInvoiceNumber(INVOICE_NUMBER)).thenReturn(invoice);
    invoiceService.markDunningEmailSent(INVOICE_NUMBER);
    ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
    verify(invoiceRepositoryMock).save(invoiceArgumentCaptor.capture());
    Invoice invoiceGot = invoiceArgumentCaptor.getValue();
    assertTrue(invoiceGot.isDunningEmailSent());
  }

  @Test
  public void testGetOneById() {
    Invoice invoice = new Invoice();
    when(invoiceRepositoryMock.findById(ID)).thenReturn(invoice);
    invoiceService.getOne(ID);
    verify(invoiceToViewModelConverterMock).convertToResponse(invoice);
  }

  @Test
  public void testSave() {
    Invoice invoice = new Invoice();
    invoiceService.save(invoice);
    verify(invoiceRepositoryMock).save(invoice);
  }

  @Test
  public void testGetEntity() {
    invoiceService.getEntity(ID);
    verify(invoiceRepositoryMock).findOne(ID);
  }

  @Test
  public void testGetAllOverdue() {
    InvoiceViewModel invoiceViewModelSettled = new InvoiceViewModel();
    invoiceViewModelSettled.setDueAmount(BigDecimal.ZERO);
    InvoiceViewModel invoiceViewModelNotSettled = new InvoiceViewModel();
    invoiceViewModelNotSettled.setDueAmount(BigDecimal.TEN);
    when(invoiceToViewModelConverterMock.convertToResponse(anyCollectionOf(Invoice.class)))
      .thenReturn(Arrays.asList(invoiceViewModelNotSettled, invoiceViewModelSettled));
    List<InvoiceViewModel> allOverdue = invoiceService.getAllOverdue();
    assertEquals(1, allOverdue.size());
    assertSame(invoiceViewModelNotSettled, allOverdue.get(0));
    ArgumentCaptor<BooleanExpression> invoiceIsNotSettledExpressionArgumentCaptor = ArgumentCaptor
      .forClass(BooleanExpression.class);
    verify(invoiceRepositoryMock).findAll(invoiceIsNotSettledExpressionArgumentCaptor.capture());
    BooleanExpression invoiceIsNotSettledExprGot = invoiceIsNotSettledExpressionArgumentCaptor
      .getValue();
    assertEquals("invoice.settled is null || invoice.settled = false",
      invoiceIsNotSettledExprGot.toString());
  }
}
