package com.kiga.accounting.dunning.domain;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanEqualsExcluding;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanHashCodeExcluding;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanToStringExcluding;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * @author bbs
 * @since 2/19/17.
 */
public class DunningLevelTest {
  @Test
  public void testBean() {
    assertThat(DunningLevel.class, allOf(hasValidGettersAndSetters(),
      hasValidBeanEqualsExcluding("className", "created", "lastEdited", "id"),
      hasValidBeanHashCodeExcluding("className", "created", "lastEdited", "id"),
      hasValidBeanToStringExcluding("className", "created", "lastEdited", "id")));
  }
}
