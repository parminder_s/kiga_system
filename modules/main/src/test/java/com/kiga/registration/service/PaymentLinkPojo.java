package com.kiga.registration.service;

/**
 * Created by rainerh on 11.04.16.
 */

class PaymentLinkPojo {
  private String url;
  private String email;
  private String bar;

  public PaymentLinkPojo(String url, String email, String bar) {
    this.url = url;
    this.email = email;
    this.bar = bar;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getBar() {
    return bar;
  }

  public void setBar(String bar) {
    this.bar = bar;
  }
}


