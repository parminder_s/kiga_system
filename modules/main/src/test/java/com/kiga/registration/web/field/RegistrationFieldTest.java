package com.kiga.registration.web.field;

import com.google.code.beanmatchers.BeanMatchers;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 12.10.16.
 */
public class RegistrationFieldTest {
  @Test
  public void testSelect() {
    Assert.assertThat(RegistrationFieldSelect.class, BeanMatchers.hasValidGettersAndSetters());
  }

  @Test
  public void testSelectOption() {
    Assert.assertThat(SelectOption.class, BeanMatchers.hasValidGettersAndSetters());
  }

}
