package com.kiga.registration.service;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.main.locale.Locale;
import com.kiga.web.service.UrlGenerator;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 10.04.16.
 */
public class RegistrationPaymentLinkGeneratorTest {
  @Test
  public void getLink() throws Exception {
    UrlGenerator urlGenerator = mock(UrlGenerator.class);
    when(urlGenerator.getNgUrl(anyString())).then(
      (answer) -> "http://www.kigaportal.com/#!/" + answer.getArguments()[0].toString()
    );

    PaymentLinkPojo pojo = new PaymentLinkPojo(
      "http://www.kigaportal.com/#!/ng/de/payment", "person@host.com", "asdf"
    );

    RegistrationPaymentLinkGenerator generator =
      new DefaultRegistrationPaymentLinkGenerator(urlGenerator);
    String link = generator.getLink(pojo, Locale.en);


    Assert.assertEquals(
      new StringBuilder("http://www.kigaportal.com/#!/en/payment/checkout?")
        .append("bar=asdf&email=person%40host.com&")
        .append("url=http%3A%2F%2Fwww.kigaportal.com%2F%23%21%2Fng%2Fde%2Fpayment")
        .toString() ,
      link
    );
  }

}
