package com.kiga.registration.web;

import com.kiga.registration.web.request.InitiateResponse;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 12.10.16.
 */
public class RegistrationMockControllerTest {
  @Test
  public void initiate() throws Exception {
    InitiateResponse response = new RegistrationMockController().initiate(null);
    Assert.assertEquals(1, response.getId());
    Assert.assertEquals("preSelectionFields", response.getStepName());
  }
}
