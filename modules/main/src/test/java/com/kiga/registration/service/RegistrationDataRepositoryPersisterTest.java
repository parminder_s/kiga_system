package com.kiga.registration.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.matches;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.registration.domain.RegistrationData;
import com.kiga.registration.web.request.RegistrationRequest;
import com.kiga.spec.Persister;
import com.kiga.test.MapFetcher;
import com.kiga.test.RegexMatcher;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 27.04.16.
 */

public class RegistrationDataRepositoryPersisterTest {
  @Test
  public void testPersister() throws JsonProcessingException {

    RegistrationRequest request = new RegistrationRequest();
    request.setRegistrationId(1L);
    request.setAgb(true);
    request.setSecurity(true);
    request.setProductGroup("standard");
    request.setGiftCode("giftCode");
    RegistrationData registrationData = new RegistrationData();
    registrationData.setId(1L);

    Persister<RegistrationData> persister = mock(Persister.class);
    ObjectMapper mapper = new ObjectMapper();
    DefaultRegistrationDataPersister registrationDataPersister =
      new DefaultRegistrationDataPersister(
        persister,
        new MapFetcher<RegistrationData>(registrationData),
        mapper);

    registrationDataPersister.persist(request);
    verify(persister).persist(any());
    Assert.assertThat(registrationData.getData(), RegexMatcher.matches(".*giftCode.*"));
  }
}
