package com.kiga.registration;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.registration.mapper.RequestNewSubscriptionMapper;
import com.kiga.registration.service.DefaultRegistrationDataPersister;
import com.kiga.registration.service.DefaultRegistrationPaymentLinkGenerator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.modelmapper.ModelMapper;

/** Created by rainerh on 29.05.16. */
public class RegistrationConfigTest {

  @Test
  public void registrationPaymentLinkGenerator() throws Exception {
    Assert.assertThat(
        new RegistrationConfig().getRegistrationPaymentLinkGenerator(null),
        instanceOf(DefaultRegistrationPaymentLinkGenerator.class));
  }

  @Test
  public void registrationDataPersister() throws Exception {
    Assert.assertThat(
        new RegistrationConfig().getRegistrationDataPersister(null, null),
        instanceOf(DefaultRegistrationDataPersister.class));
  }

  @Test
  public void modelMapper() throws Exception {
    ModelMapper modelMapper = mock(ModelMapper.class);
    RegistrationConfig registrationConfig = new RegistrationConfig();
    registrationConfig.setModelMapper(modelMapper);

    registrationConfig.onApplicationEvent(null);
    ArgumentCaptor<RequestNewSubscriptionMapper> captor =
        ArgumentCaptor.forClass(RequestNewSubscriptionMapper.class);
    verify(modelMapper).addMappings(captor.capture());
  }
}
