package com.kiga.registration;

import com.kiga.crm.messages.AboParameter;
import com.kiga.crm.messages.Address;
import com.kiga.crm.messages.requests.RequestNewSubscription;
import com.kiga.main.locale.Locale;
import com.kiga.registration.mapper.RequestNewSubscriptionMapper;
import com.kiga.registration.web.request.RegistrationRequest;
import org.junit.Assert;
import org.junit.Test;
import org.modelmapper.ModelMapper;

/**
 * Created by rainerh on 29.03.16.
 */
public class RegistrationRequestMapperTest {
  @Test
  public void mappingTest() {
    RegistrationRequest registrationRequest = new RegistrationRequest();
    registrationRequest.setRegistrationId(5L);
    registrationRequest.setProductGroup("standard");
    registrationRequest.setProductId(6);
    registrationRequest.setCountry("tr");
    registrationRequest.setGender("male");
    registrationRequest.setFirstname("Rainer");
    registrationRequest.setLastname("Hahnekamp");
    registrationRequest.setBirthday("1981-12-31");
    registrationRequest.setEmail("rainer.hahnekamp@k.tt4.at");
    registrationRequest.setPassword("test1234");
    registrationRequest.setPassportnumber("");
    registrationRequest.setPhone("+43 677 1234 0987");
    registrationRequest.setStreet("Am Graben");
    registrationRequest.setStreetNumber("5");
    registrationRequest.setFloorNumber("");
    registrationRequest.setDoorNumber("");
    registrationRequest.setZip("7000");
    registrationRequest.setCity("St. Georgen am Leithagebirge");
    registrationRequest.setBillOn(false);
    registrationRequest.setBillAddressName1("");
    registrationRequest.setBillAddressName1("");
    registrationRequest.setBillStreet("Hauptstraße");
    registrationRequest.setBillStreetNumber("12");
    registrationRequest.setBillFloorNumber("");
    registrationRequest.setBillDoorNumber("");
    registrationRequest.setBillZip(null);
    registrationRequest.setBillCity("");
    registrationRequest.setNewsletter(true);
    registrationRequest.setSecurity(true);
    registrationRequest.setPaymentMethod("GARANTI");
    registrationRequest.setInvoiceOption("bill_customer");
    registrationRequest.setLocale(Locale.tr);
    registrationRequest.setGiftCode("");
    registrationRequest.setTaxnumber("");
    registrationRequest.setPassportnumber("");
    ModelMapper modelMapper = new ModelMapper();
    modelMapper.addMappings(new RequestNewSubscriptionMapper());
    RequestNewSubscription requestNewSubscription =
      modelMapper.map(registrationRequest, RequestNewSubscription.class);

    //main data
    Assert.assertEquals("test1234", requestNewSubscription.getPassword());
    Assert.assertEquals("GARANTI", requestNewSubscription.getPaymentOption());
    Assert.assertEquals("bill_customer", requestNewSubscription.getInvoiceOption());
    Assert.assertEquals("", requestNewSubscription.getJob());
    Assert.assertEquals("", requestNewSubscription.getOrgType());
    Assert.assertEquals("", requestNewSubscription.getVatNumber());
    Assert.assertEquals("tr", requestNewSubscription.getUserLang());
    Assert.assertEquals(1, requestNewSubscription.getNewsletter());
    Assert.assertEquals(0, requestNewSubscription.getCreate());
    Assert.assertEquals("1981-12-31", requestNewSubscription.getDateOfBirth());
    Assert.assertNull(requestNewSubscription.getBillingAddress());

    Address shippingAddress = requestNewSubscription.getShippingAddress();
    Assert.assertEquals("Rainer", shippingAddress.getFirstname());
    Assert.assertEquals("Hahnekamp", shippingAddress.getLastname());
    Assert.assertEquals("m", shippingAddress.getSex());
    Assert.assertEquals("Am Graben", shippingAddress.getStreet());
    Assert.assertEquals("5", shippingAddress.getHousenr());
    Assert.assertEquals("", shippingAddress.getFloor());
    Assert.assertEquals("", shippingAddress.getDoornr());
    Assert.assertEquals("7000", shippingAddress.getZip());
    Assert.assertEquals("St. Georgen am Leithagebirge", shippingAddress.getCity());
    Assert.assertEquals("", shippingAddress.getOrganisation());
    Assert.assertEquals("", shippingAddress.getOrganisation2());

    AboParameter aboParameter = requestNewSubscription.getSubscription();
    Assert.assertEquals("tr", aboParameter.getCountry());
    Assert.assertEquals(-1, aboParameter.getLicenceType());
    Assert.assertEquals(6, aboParameter.getProductId());
    Assert.assertEquals(-1, aboParameter.getMaxNumberSubUsers());
    Assert.assertNull(requestNewSubscription.getBillingAddress());
  }

  @Test public void withBillingAddress() {
    RegistrationRequest registrationRequest = new RegistrationRequest();
    registrationRequest.setRegistrationId(5L);
    registrationRequest.setProductGroup("standard");
    registrationRequest.setProductId(6);
    registrationRequest.setCountry("tr");
    registrationRequest.setBirthday("1981-12-31");
    registrationRequest.setEmail("rainer.hahnekamp@k.tt4.at");
    registrationRequest.setPassword("test1234");
    registrationRequest.setPassportnumber("");
    registrationRequest.setPhone("+43 677 1234 0987");
    registrationRequest.setBillOn(true);
    registrationRequest.setBillAddressName1("Organisation 5");
    registrationRequest.setBillAddressName2("Area 51");
    registrationRequest.setBillStreet("Hauptstraße");
    registrationRequest.setBillStreetNumber("12");
    registrationRequest.setBillFloorNumber("");
    registrationRequest.setBillDoorNumber("");
    registrationRequest.setBillZip(null);
    registrationRequest.setBillCity("");
    registrationRequest.setNewsletter(true);
    registrationRequest.setSecurity(true);
    registrationRequest.setPaymentMethod("GARANTI");
    registrationRequest.setInvoiceOption("bill_customer");
    registrationRequest.setLocale(Locale.tr);
    registrationRequest.setGiftCode("");
    registrationRequest.setTaxnumber("");
    registrationRequest.setPassportnumber("");

    ModelMapper modelMapper = new ModelMapper();
    modelMapper.addMappings(new RequestNewSubscriptionMapper());
    RequestNewSubscription requestNewSubscription =
      modelMapper.map(registrationRequest, RequestNewSubscription.class);


    //billing address
    Assert.assertNotNull(requestNewSubscription.getBillingAddress());
    Address billingAddress = requestNewSubscription.getBillingAddress();
    Assert.assertEquals("Hauptstraße", billingAddress.getStreet());
    Assert.assertEquals("12", billingAddress.getHousenr());
    Assert.assertEquals("", billingAddress.getFloor());
    Assert.assertEquals("", billingAddress.getDoornr());
    Assert.assertEquals(null, billingAddress.getZip());
    Assert.assertEquals("", billingAddress.getCity());
    Assert.assertEquals("Organisation 5", billingAddress.getOrganisation());
    Assert.assertEquals("Area 51", billingAddress.getOrganisation2());
  }

  @Test
  public void testWithNullBillOn() {
    RegistrationRequest registrationRequest = new RegistrationRequest();
    ModelMapper modelMapper = new ModelMapper();
    modelMapper.addMappings(new RequestNewSubscriptionMapper());
    RequestNewSubscription requestNewSubscription =
      modelMapper.map(registrationRequest, RequestNewSubscription.class);

    Assert.assertNotNull(requestNewSubscription);
    Assert.assertNull(requestNewSubscription.getBillingAddress());
  }

  @Test
  public void testFemaleGender() {
    RegistrationRequest registrationRequest = new RegistrationRequest();
    registrationRequest.setGender("female");
    ModelMapper modelMapper = new ModelMapper();
    modelMapper.addMappings(new RequestNewSubscriptionMapper());
    RequestNewSubscription requestNewSubscription =
      modelMapper.map(registrationRequest, RequestNewSubscription.class);

    Assert.assertNotNull(requestNewSubscription);
    Assert.assertEquals("f", requestNewSubscription.getShippingAddress().getSex());
  }

  @Test
  public void testSpecialFields() {
    RegistrationRequest registrationRequest = new RegistrationRequest();

    registrationRequest.setLicences(1);
    registrationRequest.setTaxnumber("tr-12345");
    registrationRequest.setPassportnumber("TR-P-135");

    ModelMapper modelMapper = new ModelMapper();
    modelMapper.addMappings(new RequestNewSubscriptionMapper());
    RequestNewSubscription requestNewSubscription =
      modelMapper.map(registrationRequest, RequestNewSubscription.class);

    Assert.assertEquals("tr-12345", requestNewSubscription.getCountryContext().getTaxNr());
    Assert.assertEquals("TR-P-135", requestNewSubscription.getCountryContext().getPassportNr());
    Assert.assertEquals(1, requestNewSubscription.getSubscription().getMaxNumberSubUsers());
  }
}
