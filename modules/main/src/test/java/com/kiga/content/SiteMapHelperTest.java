package com.kiga.content;

import com.kiga.content.sitemap.service.SiteMapHelper;
import org.elasticsearch.common.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

/**
 * Created by peter on 10.05.16.
 */
public class SiteMapHelperTest {
  @Test
  public void getChangeFeq() {
    DateTime date = DateTime.parse("2016-12-01");

    String always = SiteMapHelper.getChangeFreq(date.toDate(), date.toDate());
    String hourly = SiteMapHelper.getChangeFreq(date.minusHours(1).toDate(), date.toDate());
    String daily = SiteMapHelper.getChangeFreq(date.minusDays(1).toDate(), date.toDate());
    String weekly = SiteMapHelper.getChangeFreq(date.minusWeeks(1).toDate(), date.toDate());
    String monthly = SiteMapHelper.getChangeFreq(date.minusMonths(1).toDate(), date.toDate());
    String yearly = SiteMapHelper.getChangeFreq(date.minusYears(1).toDate(), date.toDate());

    Assert.assertEquals("always", always);
    Assert.assertEquals("hourly", hourly);
    Assert.assertEquals("daily", daily);
    Assert.assertEquals("weekly", weekly);
    Assert.assertEquals("monthly", monthly);
    Assert.assertEquals("yearly", yearly);
  }

  @Test
  public void getPriority() {
    SiteMapHelper siteMapHelper = new SiteMapHelper();
    Assert.assertEquals("0.5", siteMapHelper.getPriority(null));
    Assert.assertEquals("1.0", SiteMapHelper.getPriority("1.0"));
  }

}
