package com.kiga.content.sorttree;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by peter on 25.07.17.
 */
public class SortTreeCalculatorTest {

  @Test
  public void calculateSortTreeTestNoChildren() {
    StaElement root =  StaElement.builder()
      .sortTree(new StElement(1, ""))
      .children(Collections.emptyList()).build();

    SortTreeCalculator sortTreeCalculator =
      new SortTreeCalculator(new SortTreeRelationCalculator());
    sortTreeCalculator.calculateSortTree("", Arrays.asList(root));
    Assert.assertEquals("AA", root.getSortTree().getSortTree());
  }

  @Test
  public void calculateSortTreeTestOneChildren() {
    StaElement child = new StaElement("");
    StaElement root =  StaElement.builder().sortTree(new StElement(1, ""))
      .children(Arrays.asList(child)).build();

    SortTreeCalculator sortTreeCalculator =
      new SortTreeCalculator(new SortTreeRelationCalculator());
    sortTreeCalculator.calculateSortTree("", Arrays.asList(root));
    Assert.assertEquals("AA", root.getSortTree().getSortTree());
    Assert.assertEquals("AAAA", child.getSortTree().getSortTree());
  }

  @Test
  public void calculateSortTreeTestTwoChildren() {
    StaElement child1 = new StaElement("");
    StaElement child2 = new StaElement("");
    StaElement root =  StaElement.builder().sortTree(new StElement(1, ""))
      .children(Arrays.asList(child1, child2)).build();

    SortTreeCalculator sortTreeCalculator =
      new SortTreeCalculator(new SortTreeRelationCalculator());
    sortTreeCalculator.calculateSortTree("", Arrays.asList(root));
    Assert.assertEquals("AA", root.getSortTree().getSortTree());
    Assert.assertEquals("AAAA", child1.getSortTree().getSortTree());
    Assert.assertEquals("AAAB", child2.getSortTree().getSortTree());
  }

  @Test
  public void calculateSortTreeTestThreeChildren() {
    StaElement child1 = new StaElement("");
    StaElement child2 = new StaElement("");
    StaElement child3 = new StaElement("");
    StaElement root =  StaElement.builder().sortTree(new StElement(1, ""))
      .children(Arrays.asList(child1, child2, child3)).build();

    SortTreeCalculator sortTreeCalculator =
      new SortTreeCalculator(new SortTreeRelationCalculator());
    sortTreeCalculator.calculateSortTree("", Arrays.asList(root));
    Assert.assertEquals("AA", root.getSortTree().getSortTree());
    Assert.assertEquals("AAAA", child1.getSortTree().getSortTree());
    Assert.assertEquals("AAAB", child2.getSortTree().getSortTree());
    Assert.assertEquals("AAAC", child3.getSortTree().getSortTree());
  }

  @Test
  public void updateBranchTest() {
    StaElement child1 = new StaElement("");
    StaElement child2 = new StaElement("");
    StaElement child3 = new StaElement("");
    StaElement root =  StaElement.builder().sortTree(new StElement(1, ""))
      .children(Arrays.asList(child1, child2, child3)).build();

    SortTreeCalculator sortTreeCalculator =
      new SortTreeCalculator(new SortTreeRelationCalculator());

    sortTreeCalculator.updateBranch(root);

    Assert.assertEquals("", root.getSortTree().getSortTree());
    Assert.assertEquals("AA", child1.getSortTree().getSortTree());
    Assert.assertEquals("AB", child2.getSortTree().getSortTree());
    Assert.assertEquals("AC", child3.getSortTree().getSortTree());

  }
}
