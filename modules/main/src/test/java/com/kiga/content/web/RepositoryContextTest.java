package com.kiga.content.web;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.RepositoryContexts;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.Answer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author bbs
 * @since 7/23/16.
 */
public class RepositoryContextTest {
  private RepositoryContexts repositoryContexts = null;
  private RepositoryContext context;
  private HttpSession session;

  private static final String SESSION_KEY = RepositoryContext.class
    .getCanonicalName() + "::context";

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    session = mock(HttpSession.class);
    when(session.getAttribute(SESSION_KEY)).thenAnswer(getContext());
    doAnswer(toggleContext()).when(session).setAttribute(eq(SESSION_KEY), any());

    HttpServletRequest request = mock(HttpServletRequest.class);
    when(request.getSession()).thenReturn(session);
    context = new RepositoryContext(request);
  }

  @Test
  public void testGetContextNullSessionAttribute() {
    RepositoryContexts repositoryContext = context.getContext();
    Assert.assertEquals(RepositoryContexts.LIVE, repositoryContext);
  }

  /**
   * Fake togglecontext method.
   *
   * @return void
   */
  Answer<Void> toggleContext() {
    return invocation -> {
      if (RepositoryContexts.LIVE.equals(repositoryContexts)) {
        repositoryContexts = RepositoryContexts.DRAFT;
      } else {
        repositoryContexts = RepositoryContexts.LIVE;
      }
      return null;
    };
  }

  /**
   * Fake getContext method.
   *
   * @return ERepositoryContexts
   */
  Answer<RepositoryContexts> getContext() {
    return invocation -> repositoryContexts;
  }
}
