package com.kiga.content.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.draft.SiteTreeDraft;
import com.kiga.content.domain.ss.draft.SiteTreeTranslationGroups;
import com.kiga.content.repository.ss.draft.SiteTreeTranslationGroupsRepository;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

/**
 * Created by robert on 03.09.17.
 */

public class SiteTreeTranslationGroupServiceTest {
  private static final long TRANSLATION_GROUP_ID = 123L;
  private static final long SITE_TREE_ID = 1L;
  private SiteTreeTranslationGroupsRepository siteTreeTranslationGroupsRepositoryMock =
    mock(SiteTreeTranslationGroupsRepository.class);
  private SiteTreeTranslationGroupService siteTreeTranslationGroupService =
    new SiteTreeTranslationGroupService(siteTreeTranslationGroupsRepositoryMock);

  @Test
  public void testCreateTranslationGroup() {
    when(siteTreeTranslationGroupsRepositoryMock.findNextAvailableTranslationGroupId()).thenReturn(
      TRANSLATION_GROUP_ID);

    SiteTreeDraft siteTree = new SiteTreeDraft();
    siteTree.setId(SITE_TREE_ID);
    siteTreeTranslationGroupService.createTranslationGroup(siteTree);

    ArgumentCaptor<SiteTreeTranslationGroups> siteTreeTranslationGroupsArgumentCaptor =
      ArgumentCaptor.forClass(SiteTreeTranslationGroups.class);
    verify(siteTreeTranslationGroupsRepositoryMock)
      .save(siteTreeTranslationGroupsArgumentCaptor.capture());

    SiteTreeTranslationGroups capturedTranslationGroups =
      siteTreeTranslationGroupsArgumentCaptor.getValue();

    assertEquals(TRANSLATION_GROUP_ID,
      capturedTranslationGroups.getTranslationGroupId().intValue());
    assertEquals(SITE_TREE_ID, capturedTranslationGroups.getOriginalId().intValue());
  }

  @Test
  public void testCreateTranslationGroupWithoutExistingTranslationGroup() {
    SiteTreeDraft siteTree = new SiteTreeDraft();
    siteTree.setId(SITE_TREE_ID);
    siteTreeTranslationGroupService.createTranslationGroup(siteTree);

    ArgumentCaptor<SiteTreeTranslationGroups> siteTreeTranslationGroupsArgumentCaptor =
      ArgumentCaptor.forClass(SiteTreeTranslationGroups.class);
    verify(siteTreeTranslationGroupsRepositoryMock)
      .save(siteTreeTranslationGroupsArgumentCaptor.capture());

    SiteTreeTranslationGroups capturedTranslationGroups =
      siteTreeTranslationGroupsArgumentCaptor.getValue();

    assertEquals(1L, capturedTranslationGroups.getTranslationGroupId().intValue());
    assertEquals(SITE_TREE_ID, capturedTranslationGroups.getOriginalId().intValue());
  }

  @Test
  public void testAddToTranslationGroup() {
    SiteTreeDraft siteTree = new SiteTreeDraft();
    siteTree.setId(SITE_TREE_ID);
    siteTreeTranslationGroupService.addToTranslationGroup(TRANSLATION_GROUP_ID, siteTree);

    ArgumentCaptor<SiteTreeTranslationGroups> siteTreeTranslationGroupsArgumentCaptor =
      ArgumentCaptor.forClass(SiteTreeTranslationGroups.class);
    verify(siteTreeTranslationGroupsRepositoryMock)
      .save(siteTreeTranslationGroupsArgumentCaptor.capture());

    SiteTreeTranslationGroups capturedTranslationGroups =
      siteTreeTranslationGroupsArgumentCaptor.getValue();

    assertEquals(TRANSLATION_GROUP_ID,
      capturedTranslationGroups.getTranslationGroupId().intValue());
    assertEquals(SITE_TREE_ID, capturedTranslationGroups.getOriginalId().intValue());
  }
}
