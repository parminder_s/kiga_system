package com.kiga.content.starrating.web;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import com.kiga.content.starrating.service.StarRatingService;
import com.kiga.content.starrating.web.request.UpdateRatingRequest;
import com.kiga.ideas.service.RatingCalculatorService;
import com.kiga.ideas.web.response.IdeasResponseRating;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by asv on 22.01.17.
 */
public class StarRatingControllerTest {
  private SecurityService securityService = mock(SecurityService.class);
  private HttpServletRequest request = mock(HttpServletRequest.class);
  private StarRatingService starRatingService = mock(StarRatingService.class);
  private StarRatingController starRatingController;
  private RatingCalculatorService ratingCalculatorService;

  @Before
  public void prepareTests() {
    starRatingController = new StarRatingController(
      starRatingService, securityService, ratingCalculatorService );
  }

  @Test
  public void getKigaPageRating() throws Exception {
    Member newMember = new Member();
    newMember.setId(1L);
    Optional<Member> member = Optional.of(newMember);
    when(securityService.getSessionMember()).thenReturn(member);
    IdeasResponseRating responseRating = new IdeasResponseRating();
    responseRating.setCount(3);
    when(starRatingService.getKigaPageRating(1L, 1L)).thenReturn(responseRating);
    IdeasResponseRating response = starRatingController.getKigaPageRating(1L);

    assertNotNull(response);
    Assert.assertEquals(response.getCount(), 3);
    verify(starRatingService).getKigaPageRating(1L, 1L);
  }

  @Test
  public void updateRating() throws Exception {
    UpdateRatingRequest updateRatingRequest = new UpdateRatingRequest();
    updateRatingRequest.setUserRating(1);
    updateRatingRequest.setKigaPageId(1L);
    Member newMember = new Member();
    newMember.setId(1L);
    Optional<Member> member = Optional.of(newMember);
    when(securityService.getSessionMember()).thenReturn(member);
    IdeasResponseRating responseRating = new IdeasResponseRating();
    responseRating.setCount(3);
    when(starRatingService.addRatingToKigaPage(1L, 1, 1L)).thenReturn(responseRating);
    IdeasResponseRating response = starRatingController.updateRating(updateRatingRequest);

    assertNotNull(response);
    Assert.assertEquals(response.getCount(), 3);
    verify(starRatingService).addRatingToKigaPage(1L, 1, 1L);
  }

  @Test
  public void clearRating() throws Exception {
    UpdateRatingRequest updateRatingRequest = new UpdateRatingRequest();
    updateRatingRequest.setUserRating(1);
    updateRatingRequest.setKigaPageId(1L);
    Member newMember = new Member();
    newMember.setId(1L);
    Optional<Member> member = Optional.of(newMember);
    when(securityService.getSessionMember()).thenReturn(member);
    IdeasResponseRating responseRating = new IdeasResponseRating();
    responseRating.setCount(3);
    when(starRatingService.clearUserRating(1L, 1L)).thenReturn(responseRating);
    IdeasResponseRating response = starRatingController.clearRating(updateRatingRequest);

    assertNotNull(response);
    Assert.assertEquals(response.getCount(), 3);
    verify(starRatingService).clearUserRating(1L, 1L);
  }

}
