package com.kiga.content.web;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.RepositoryContexts;
import com.kiga.content.web.response.ToggleContextResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.Answer;

/**
 * @author bbs
 * @since 7/23/16.
 */
public class RepositoryContextControllerTest {
  private RepositoryContexts repositoryContexts = RepositoryContexts.DRAFT;
  private RepositoryContext context;

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    context = mock(RepositoryContext.class);
    doAnswer(toggleContext()).when(context).toggleContext();
    doAnswer(getContext()).when(context).getContext();
  }

  @Test
  public void testToggleContext() {
    RepositoryContextController controller = new RepositoryContextController(context);
    ToggleContextResponse response = controller.toggleContext();
    Assert.assertEquals(RepositoryContexts.LIVE, response.getRepositoryContexts());
  }

  @Test
  public void testCurrentContext() {
    RepositoryContextController controller = new RepositoryContextController(context);
    ToggleContextResponse response = controller.currentContext();
    Assert.assertEquals(RepositoryContexts.DRAFT, response.getRepositoryContexts());
  }

  /**
   * Fake togglecontext method.
   *
   * @return void
   */
  Answer<Void> toggleContext() {
    return invocation -> {
      if (repositoryContexts.equals(RepositoryContexts.LIVE)) {
        repositoryContexts = RepositoryContexts.DRAFT;
      } else {
        repositoryContexts = RepositoryContexts.LIVE;
      }
      return null;
    };
  }

  /**
   * Fake getContext method.
   *
   * @return ERepositoryContexts
   */
  Answer<RepositoryContexts> getContext() {
    return invocation -> repositoryContexts;
  }
}
