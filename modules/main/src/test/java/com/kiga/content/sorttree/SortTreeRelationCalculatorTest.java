package com.kiga.content.sorttree;

import static org.junit.Assert.assertEquals;


import com.kiga.content.sorttree.SortTreeRelationCalculator;
import com.kiga.ideas.task.sorttree.SortTreeOverflowException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by faxxe on 5/16/17.
 */
public class SortTreeRelationCalculatorTest {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void getSortTreeForRightSiblingFirstSibling() {
    SortTreeRelationCalculator sortTreeRelationCalculator = new SortTreeRelationCalculator();
    String rightSibling = sortTreeRelationCalculator.getSortTreeForRightSibling("AA");
    assertEquals("AB", rightSibling);
  }

  @Test
  public void getSortTreeForRightSiblingDefault() {
    SortTreeRelationCalculator sortTreeRelationCalculator = new SortTreeRelationCalculator();
    String rightSibling = sortTreeRelationCalculator.getSortTreeForRightSibling("NX");
    assertEquals("NY", rightSibling);
  }

  @Test
  public void getSortTreeForRightSiblingOverflowTwoLetter() {
    SortTreeRelationCalculator sortTreeRelationCalculator = new SortTreeRelationCalculator();
    String rightSibling = sortTreeRelationCalculator.getSortTreeForRightSibling("AZ");
    assertEquals("BA", rightSibling);
  }

  @Test
  public void getSortTreeForRightSiblingOverflowFourLetter() {
    SortTreeRelationCalculator sortTreeRelationCalculator = new SortTreeRelationCalculator();
    String rightSibling = sortTreeRelationCalculator.getSortTreeForRightSibling("XYAZ");
    assertEquals("XYBA", rightSibling);
  }

  @Test
  public void getSortTreeForRightSiblingException()  throws Exception {
    SortTreeRelationCalculator sortTreeRelationCalculator = new SortTreeRelationCalculator();
    thrown.expect(SortTreeOverflowException.class);
    sortTreeRelationCalculator.getSortTreeForRightSibling("AAZZ");
  }

  @Test
  public void getSortTreeForFirstChildParentNull() {
    SortTreeRelationCalculator sortTreeRelationCalculator = new SortTreeRelationCalculator();
    String firstChild = sortTreeRelationCalculator.getSortTreeForFirstChild(null);
    assertEquals("AA", firstChild);
  }

  @Test
  public void getSortTreeForFirstChildParentEmpty() {
    SortTreeRelationCalculator sortTreeRelationCalculator = new SortTreeRelationCalculator();
    String firstChild = sortTreeRelationCalculator.getSortTreeForFirstChild("");
    assertEquals("AA", firstChild);
  }

  @Test
  public void getSortTreeForFirstChildParentDefaultOneLetter() {
    SortTreeRelationCalculator sortTreeRelationCalculator = new SortTreeRelationCalculator();
    String firstChild = sortTreeRelationCalculator.getSortTreeForFirstChild("A");
    assertEquals("AAA", firstChild);
  }

  @Test
  public void getSortTreeForFirstChildDefault() {
    SortTreeRelationCalculator sortTreeRelationCalculator = new SortTreeRelationCalculator();
    String firstChild = sortTreeRelationCalculator.getSortTreeForFirstChild("ZZ");
    assertEquals("ZZAA", firstChild);
  }
}