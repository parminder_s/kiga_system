package com.kiga.content.sorttree;

import static org.junit.Assert.assertEquals;

import com.kiga.content.domain.ss.live.IdeaGroupToIdeaLive;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import org.junit.Test;

public class SortTreeTest {
  @Test
  public void testSiteTreeLiveCompareTo() {
    SiteTreeLive stOne = new SiteTreeLive();
    SiteTreeLive stTwo = new SiteTreeLive();
    stOne.setSort(2);
    stTwo.setSort(1);
    assertEquals(1, stOne.compareTo(stTwo));
    assertEquals(-1, stTwo.compareTo(stOne));
  }

  @Test
  public void testIdeaGroupToIdeaCompareTo() {
    IdeaGroupToIdeaLive igOne = new IdeaGroupToIdeaLive();
    IdeaGroupToIdeaLive igTwo = new IdeaGroupToIdeaLive();
    igOne.setSort(2);
    igTwo.setSort(1);
    assertEquals(1, igOne.compareTo(igTwo));
    assertEquals(-1, igTwo.compareTo(igOne));
  }
}