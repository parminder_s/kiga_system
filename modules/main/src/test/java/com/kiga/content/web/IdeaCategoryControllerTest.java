package com.kiga.content.web;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.idea.member.provider.MemberIdeaCategoryService;
import com.kiga.main.locale.Locale;
import org.junit.Test;

/**
 * @author bbs
 * @since 9/3/17.
 */
public class IdeaCategoryControllerTest {
  private MemberIdeaCategoryService memberIdeaCategoryServiceMock =
    mock(MemberIdeaCategoryService.class);
  private IdeaCategoryController ideaCategoryController =
    new IdeaCategoryController(memberIdeaCategoryServiceMock);

  @Test
  public void testGetCategories() {
    ideaCategoryController.getCategories("en");
    ideaCategoryController.getCategories("de");
    ideaCategoryController.getCategories("it");
    ideaCategoryController.getCategories("tr");

    verify(memberIdeaCategoryServiceMock).listCategoriesHierarchyForDropdown(Locale.en);
    verify(memberIdeaCategoryServiceMock).listCategoriesHierarchyForDropdown(Locale.de);
    verify(memberIdeaCategoryServiceMock).listCategoriesHierarchyForDropdown(Locale.it);
    verify(memberIdeaCategoryServiceMock).listCategoriesHierarchyForDropdown(Locale.tr);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetCategoriesWithWrongLocale() {
    ideaCategoryController.getCategories("ch");
  }
}
