package com.kiga.content.sorttree;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.sorttree.helper.UnusedSortTreeNullSetter;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

/** Created by Florian Schneider on 07.11.17. */
public class SortTreeManagerTest {

  StaElement testSortTree;
  StaElement referenceSortTree;
  private UnusedSortTreeNullSetter unusedSortTreeNullSetterMock;

  @Before
  public void prepareTests() {
    unusedSortTreeNullSetterMock = mock(UnusedSortTreeNullSetter.class);
  }

  /**
   * Fill the testSortTree and the referenceSortTree with values to verify after calculation. Fill
   * with sort values for order verification
   */
  public void fillTestAndRefSortTreeWithOrder(String initialSortTree) {
    if (initialSortTree == null) {
      initialSortTree = "";
    }
    StaElement depthOneOne = new StaElement("test", 5);
    StaElement depthOneTwo = new StaElement("", 2);
    StaElement depthOne =
        StaElement.builder()
            .sortTree(StElement.builder().sortTree("test").sort(4).build())
            .children(Arrays.asList(depthOneOne, depthOneTwo))
            .build();

    StaElement depthTwoOne = new StaElement("test", 1);
    StaElement depthTwoTwo = new StaElement("", 2);
    StaElement depthTwo =
        StaElement.builder()
            .sortTree(StElement.builder().sortTree("test").sort(3).build())
            .children(Arrays.asList(depthTwoOne, depthTwoTwo))
            .build();

    testSortTree =
        StaElement.builder()
            .sortTree(StElement.builder().sortTree(initialSortTree).sort(0).build())
            .children(Arrays.asList(depthOne, depthTwo))
            .build();

    StaElement refDepthOneOne = new StaElement(initialSortTree + "AAAA", 1);
    StaElement refDepthOneTwo = new StaElement(initialSortTree + "AAAB", 2);
    StaElement refDepthOne =
        StaElement.builder()
            .sortTree(StElement.builder().sortTree(initialSortTree + "AA").sort(3).build())
            .children(Arrays.asList(refDepthOneOne, refDepthOneTwo))
            .build();

    StaElement refDepthTwoOne = new StaElement(initialSortTree + "ABAA", 2);
    StaElement refDepthTwoTwo = new StaElement(initialSortTree + "ABAB", 5);
    StaElement refDepthTwo =
        StaElement.builder()
            .sortTree(StElement.builder().sortTree(initialSortTree + "AB").sort(4).build())
            .children(Arrays.asList(refDepthTwoOne, refDepthTwoTwo))
            .build();

    referenceSortTree =
        StaElement.builder()
            .sortTree(StElement.builder().sortTree(initialSortTree).sort(0).build())
            .children(Arrays.asList(refDepthOne, refDepthTwo))
            .build();
  }

  /** Fill the testSortTree and the referenceSortTree with values to verify after calculation. */
  public void fillTestAndRefSortTreeNoOrder(String initialSortTree) {
    if (initialSortTree == null) {
      initialSortTree = "";
    }
    StaElement depthOneOne = new StaElement("test");
    StaElement depthOneTwo = new StaElement("");
    StaElement depthOne =
        StaElement.builder()
            .sortTree(StElement.builder().sortTree("test").build())
            .children(Arrays.asList(depthOneOne, depthOneTwo))
            .build();

    StaElement depthTwoOne = new StaElement("test");
    StaElement depthTwoTwo = new StaElement("");
    StaElement depthTwo =
        StaElement.builder()
            .sortTree(StElement.builder().sortTree("test").build())
            .children(Arrays.asList(depthTwoOne, depthTwoTwo))
            .build();

    testSortTree =
        StaElement.builder()
            .sortTree(StElement.builder().sortTree(initialSortTree).build())
            .children(Arrays.asList(depthOne, depthTwo))
            .build();

    StaElement refDepthOneOne = new StaElement(initialSortTree + "AAAA");
    StaElement refDepthOneTwo = new StaElement(initialSortTree + "AAAB");
    StaElement refDepthOne =
        StaElement.builder()
            .sortTree(StElement.builder().sortTree(initialSortTree + "AA").build())
            .children(Arrays.asList(refDepthOneOne, refDepthOneTwo))
            .build();

    StaElement refDepthTwoOne = new StaElement(initialSortTree + "ABAA");
    StaElement refDepthTwoTwo = new StaElement(initialSortTree + "ABAB");
    StaElement refDepthTwo =
        StaElement.builder()
            .sortTree(StElement.builder().sortTree(initialSortTree + "AB").build())
            .children(Arrays.asList(refDepthTwoOne, refDepthTwoTwo))
            .build();

    referenceSortTree =
        StaElement.builder()
            .sortTree(StElement.builder().sortTree(initialSortTree).build())
            .children(Arrays.asList(refDepthOne, refDepthTwo))
            .build();
  }

  @Test
  public void testUpdateSortTreeCorrectSortTreesNoOrderInitialEmpty() {
    fillTestAndRefSortTreeNoOrder("");
    SiteTreeLiveRepository liveRepo = mock(SiteTreeLiveRepository.class);
    SortTreeManager sortTreeManager =
        spy(new SortTreeManager(liveRepo, unusedSortTreeNullSetterMock));
    doNothing().when(sortTreeManager).retrieveRepositoryData();
    doNothing().when(sortTreeManager).mapSortTree();
    sortTreeManager.sortTreeHierarchy = testSortTree;
    sortTreeManager.recalculateSortTree();
    verify(sortTreeManager).updateSortTree();
    assertThat(testSortTree, sameBeanAs(referenceSortTree));
  }

  @Test
  public void testUpdateSortTreeCorrectSortTreesWithOrderInitialEmpty() {
    fillTestAndRefSortTreeWithOrder("");
    SiteTreeLiveRepository liveRepo = mock(SiteTreeLiveRepository.class);
    SortTreeManager sortTreeManager =
        spy(new SortTreeManager(liveRepo, unusedSortTreeNullSetterMock));
    doNothing().when(sortTreeManager).retrieveRepositoryData();
    doNothing().when(sortTreeManager).mapSortTree();
    sortTreeManager.sortTreeHierarchy = testSortTree;
    sortTreeManager.recalculateSortTree();
    verify(sortTreeManager).updateSortTree();
    assertThat(testSortTree, sameBeanAs(referenceSortTree));
  }

  @Test
  public void testUpdateSortTreeCorrectSortTreesNoOrderInitialFilled() {
    fillTestAndRefSortTreeNoOrder("TEST");
    SiteTreeLiveRepository liveRepo = mock(SiteTreeLiveRepository.class);
    SortTreeManager sortTreeManager =
        spy(new SortTreeManager(liveRepo, unusedSortTreeNullSetterMock));
    doNothing().when(sortTreeManager).retrieveRepositoryData();
    doNothing().when(sortTreeManager).mapSortTree();
    sortTreeManager.sortTreeHierarchy = testSortTree;
    sortTreeManager.recalculateSortTree();
    verify(sortTreeManager).updateSortTree();
    assertThat(testSortTree, sameBeanAs(referenceSortTree));
  }

  @Test
  public void testUpdateSortTreeCorrectSortTreesWithOrderInitialFilled() {
    fillTestAndRefSortTreeWithOrder("TEST");
    SiteTreeLiveRepository liveRepo = mock(SiteTreeLiveRepository.class);
    SortTreeManager sortTreeManager =
        spy(new SortTreeManager(liveRepo, unusedSortTreeNullSetterMock));
    doNothing().when(sortTreeManager).retrieveRepositoryData();
    doNothing().when(sortTreeManager).mapSortTree();
    sortTreeManager.sortTreeHierarchy = testSortTree;
    sortTreeManager.recalculateSortTree();
    verify(sortTreeManager).updateSortTree();
    assertThat(testSortTree, sameBeanAs(referenceSortTree));
  }

  @Test
  public void testUpdateSortTreeCorrectSortTreesNoOrderInitialNull() {
    fillTestAndRefSortTreeNoOrder(null);
    SiteTreeLiveRepository liveRepo = mock(SiteTreeLiveRepository.class);
    SortTreeManager sortTreeManager =
        spy(new SortTreeManager(liveRepo, unusedSortTreeNullSetterMock));
    doNothing().when(sortTreeManager).retrieveRepositoryData();
    doNothing().when(sortTreeManager).mapSortTree();
    sortTreeManager.sortTreeHierarchy = testSortTree;
    sortTreeManager.recalculateSortTree();
    verify(sortTreeManager).updateSortTree();
    assertThat(testSortTree, sameBeanAs(referenceSortTree));
  }

  @Test
  public void testUpdateSortTreeCorrectSortTreesWithOrderInitialNull() {
    fillTestAndRefSortTreeWithOrder(null);
    SiteTreeLiveRepository liveRepo = mock(SiteTreeLiveRepository.class);
    SortTreeManager sortTreeManager =
        spy(new SortTreeManager(liveRepo, unusedSortTreeNullSetterMock));
    doNothing().when(sortTreeManager).retrieveRepositoryData();
    doNothing().when(sortTreeManager).mapSortTree();
    sortTreeManager.sortTreeHierarchy = testSortTree;
    sortTreeManager.recalculateSortTree();
    verify(sortTreeManager).updateSortTree();
    assertThat(testSortTree, sameBeanAs(referenceSortTree));
  }

  @Test
  public void testRetrieveFromRepository() {
    IdeaGroupContainerLive ideaGroupContainerLiveMain = new IdeaGroupContainerLive();
    ideaGroupContainerLiveMain.setClassName("IdeaGroupMainContainer");

    SiteTreeLiveRepository liveRepo = mock(SiteTreeLiveRepository.class);

    SortTreeManager sortTreeManager =
        spy(new SortTreeManager(liveRepo, unusedSortTreeNullSetterMock));
    doNothing().when(sortTreeManager).updateSortTree();
    doNothing().when(sortTreeManager).mapSortTree();

    when(liveRepo.findByClassName("IdeaGroupMainContainer"))
        .thenReturn(Arrays.asList(ideaGroupContainerLiveMain));
    sortTreeManager.recalculateSortTree();

    verify(sortTreeManager).retrieveRepositoryData();
    assertEquals(
        Arrays.asList(ideaGroupContainerLiveMain), sortTreeManager.sortTreeParent.getChildren());
  }

  @Test
  public void testMapSortTree() {
    IdeaGroupContainerLive ideaGroupContainerLiveMain = new IdeaGroupContainerLive();
    ideaGroupContainerLiveMain.setClassName("IdeaGroupMainContainer");

    SiteTreeLiveRepository liveRepo = mock(SiteTreeLiveRepository.class);

    SortTreeManager sortTreeManager =
        spy(new SortTreeManager(liveRepo, unusedSortTreeNullSetterMock));
    doNothing().when(sortTreeManager).updateSortTree();
    doNothing().when(sortTreeManager).retrieveRepositoryData();

    sortTreeManager.sortTreeParent = new IdeaCategoryLive();
    sortTreeManager.sortTreeParent.setChildren(Arrays.asList(ideaGroupContainerLiveMain));
    sortTreeManager.recalculateSortTree();

    verify(sortTreeManager).mapSortTree();
  }

  @Test
  public void testRecalculateSortTreeOrder() {
    SiteTreeLiveRepository liveRepo = mock(SiteTreeLiveRepository.class);
    SortTreeManager sortTreeManager =
        spy(new SortTreeManager(liveRepo, unusedSortTreeNullSetterMock));
    doNothing().when(sortTreeManager).retrieveRepositoryData();
    doNothing().when(sortTreeManager).mapSortTree();
    doNothing().when(sortTreeManager).updateSortTree();
    sortTreeManager.recalculateSortTree();

    InOrder methodOrder = inOrder(sortTreeManager);
    methodOrder.verify(sortTreeManager).retrieveRepositoryData();
    methodOrder.verify(sortTreeManager).mapSortTree();
    methodOrder.verify(sortTreeManager).updateSortTree();
  }
}
