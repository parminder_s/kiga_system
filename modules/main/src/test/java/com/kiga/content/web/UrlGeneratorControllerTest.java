package com.kiga.content.web;

import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.content.web.response.UrlGeneratorResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by mfit on 16.08.17.
 */
public class UrlGeneratorControllerTest {
  SiteTreeUrlGenerator siteTreeUrlGenerator = Mockito.mock(SiteTreeUrlGenerator.class);
  SiteTreeLiveRepository siteTreeLiveRepository = Mockito.mock(SiteTreeLiveRepository.class);
  UrlGeneratorController urlGeneratorController;

  @Before
  public void setUpTests() {
    urlGeneratorController = new UrlGeneratorController(
      siteTreeUrlGenerator, siteTreeLiveRepository);
  }

  @Test
  public void testProducesUrl() {
    SiteTreeLive siteTree = new SiteTreeLive();
    siteTree.setUrlSegment("test-segment");
    Mockito.when(siteTreeLiveRepository.findOne(20L)).thenReturn(siteTree);
    Mockito.when(siteTreeUrlGenerator.getNgUrl(siteTree)).thenReturn("generated-test-url");

    UrlGeneratorResponse urlGeneratorResponse =
      urlGeneratorController.getUrlForCategoryId(20L);

    Assert.assertEquals("generated-test-url", urlGeneratorResponse.getUrl());
    Assert.assertEquals("test-segment", urlGeneratorResponse.getCategorySegment());
  }
}
