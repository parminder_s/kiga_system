package com.kiga.content.repository.ss;

import static com.kiga.ideas.service.DbSortValues.sortDate;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.repository.ss.abstraction.KigaIdeaRepository;
import com.kiga.content.repository.ss.draft.KigaIdeaRepositoryDraft;
import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive;
import com.kiga.main.locale.Locale;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

/**
 * @author bbs
 * @since 7/25/16.
 */
public class KigaIdeaRepositoryProxyTest {
  private static Long ID = 1L;
  private static Pageable PAGEABLE = new PageRequest(0, 50);
  private static String CLASS_NAME = "ABC";
  private static Locale LOCALE = Locale.tr;
  private static Integer LEVEL = 3;
  private Iterable<Long> longIterableMock = mock(Iterable.class);
  private Iterable<KigaIdeaDraft> kigaIdeaDraftIterableMock = mock(Iterable.class);
  private Iterable<KigaIdeaLive> kigaIdeaLiveIterableMock = mock(Iterable.class);
  private KigaIdeaDraft kigaIdeaDraftMock = mock(KigaIdeaDraft.class);
  private KigaIdeaLive kigaIdeaLiveMock = mock(KigaIdeaLive.class);

  @Test
  public void testDelegateMethods() {
    KigaIdeaRepositoryLive kigaIdeaRepositoryLive = mock(KigaIdeaRepositoryLive.class);
    KigaIdeaRepositoryDraft kigaIdeaRepositoryDraft = mock(KigaIdeaRepositoryDraft.class);
    RepositoryContext repositoryContext = mock(RepositoryContext.class);
    when(repositoryContext.getContext()).thenReturn(RepositoryContexts.DRAFT);
    KigaIdeaRepositoryProxy kigaIdeaModel = new KigaIdeaRepositoryProxy(kigaIdeaRepositoryLive,
      kigaIdeaRepositoryDraft,
      repositoryContext);

    kigaIdeaModel.findAll();
    kigaIdeaModel.findByClassName(CLASS_NAME);
    kigaIdeaModel.findOne(ID);
    kigaIdeaModel.findAllForGroup(ID, 0, sortDate);
    kigaIdeaModel.findPagedForGroup(null, ID, LEVEL, 0, PAGEABLE);

    kigaIdeaModel.findAll(longIterableMock);
    kigaIdeaModel.delete(kigaIdeaDraftIterableMock);
    kigaIdeaModel.delete(kigaIdeaDraftMock);
    kigaIdeaModel.delete(ID);
    kigaIdeaModel.deleteAll();
    kigaIdeaModel.count();
    kigaIdeaModel.save(kigaIdeaDraftIterableMock);
    kigaIdeaModel.save(kigaIdeaDraftMock);
    kigaIdeaModel.exists(ID);

    verify(kigaIdeaRepositoryDraft, times(1)).findAll();
    verify(kigaIdeaRepositoryDraft, times(1)).findByClassName(CLASS_NAME);
    verify((KigaIdeaRepository) kigaIdeaRepositoryDraft, times(1)).findOne(ID);
    verify(kigaIdeaRepositoryDraft, times(1)).findAllForGroup(ID, 0, sortDate);
    verify(kigaIdeaRepositoryDraft, times(1)).findPagedForGroup(null, ID, LEVEL, 0, PAGEABLE);

    verify(kigaIdeaRepositoryDraft, times(1)).findAll(longIterableMock);
    verify(kigaIdeaRepositoryDraft, times(1)).delete(kigaIdeaDraftIterableMock);
    verify(kigaIdeaRepositoryDraft, times(1)).delete(kigaIdeaDraftMock);
    verify(kigaIdeaRepositoryDraft, times(1)).delete(ID);
    verify(kigaIdeaRepositoryDraft, times(1)).deleteAll();
    verify(kigaIdeaRepositoryDraft, times(1)).count();
    verify(kigaIdeaRepositoryDraft, times(1)).save(kigaIdeaDraftIterableMock);
    verify(kigaIdeaRepositoryDraft, times(1)).save(kigaIdeaDraftMock);
    verify(kigaIdeaRepositoryDraft, times(1)).exists(ID);

    when(repositoryContext.getContext()).thenReturn(RepositoryContexts.LIVE);

    kigaIdeaModel.findAll();
    kigaIdeaModel.findByClassName(CLASS_NAME);
    kigaIdeaModel.findOne(ID);
    kigaIdeaModel.findAllForGroup(ID, 0, sortDate);
    kigaIdeaModel.findPagedForGroup(null, ID, LEVEL, 0, PAGEABLE);

    kigaIdeaModel.findAll(longIterableMock);
    kigaIdeaModel.delete(kigaIdeaLiveIterableMock);
    kigaIdeaModel.delete(kigaIdeaLiveMock);
    kigaIdeaModel.delete(ID);
    kigaIdeaModel.deleteAll();
    kigaIdeaModel.count();
    kigaIdeaModel.save(kigaIdeaLiveIterableMock);
    kigaIdeaModel.save(kigaIdeaLiveMock);
    kigaIdeaModel.exists(ID);

    verify(kigaIdeaRepositoryLive, times(1)).findAll();
    verify(kigaIdeaRepositoryLive, times(1)).findByClassName(CLASS_NAME);
    verify((KigaIdeaRepository) kigaIdeaRepositoryLive, times(1)).findOne(ID);
    verify(kigaIdeaRepositoryLive, times(1)).findAllForGroup(ID, 0, sortDate);
    verify(kigaIdeaRepositoryLive, times(1)).findPagedForGroup(null, ID, LEVEL, 0, PAGEABLE);

    verify(kigaIdeaRepositoryLive, times(1)).findAll(longIterableMock);
    verify(kigaIdeaRepositoryLive, times(1)).delete(kigaIdeaLiveIterableMock);
    verify(kigaIdeaRepositoryLive, times(1)).delete(kigaIdeaLiveMock);
    verify(kigaIdeaRepositoryLive, times(1)).delete(ID);
    verify(kigaIdeaRepositoryLive, times(1)).deleteAll();
    verify(kigaIdeaRepositoryLive, times(1)).count();
    verify(kigaIdeaRepositoryLive, times(1)).save(kigaIdeaLiveIterableMock);
    verify(kigaIdeaRepositoryLive, times(1)).save(kigaIdeaLiveMock);
    verify(kigaIdeaRepositoryLive, times(1)).exists(ID);
  }
}
