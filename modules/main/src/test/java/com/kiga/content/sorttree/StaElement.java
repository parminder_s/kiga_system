package com.kiga.content.sorttree;

import com.kiga.content.sorttree.spec.SortTree;
import com.kiga.content.sorttree.spec.SortTreeElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * Created by rainerh on 29.03.17.
 */
@Data
@Builder
@AllArgsConstructor
public class StaElement implements SortTreeElement {
  public StaElement(String sortTree) {
    this(sortTree, 0);
  }

  /**
   * easy constructor.
   */
  public StaElement(String sortTree, int sort) {
    StElement stElement = new StElement(sort, sortTree);
    this.children = Collections.emptyList();
    this.sortTree = stElement;
  }

  private List<SortTreeElement> children;
  private SortTree sortTree;
}
