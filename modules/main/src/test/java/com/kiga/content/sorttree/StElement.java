package com.kiga.content.sorttree;

import com.kiga.content.sorttree.spec.SortTree;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Created by rainerh on 29.03.17.
 */
@Data
@Builder
@AllArgsConstructor
public class StElement implements SortTree {
  private int sort;
  private String sortTree;
}
