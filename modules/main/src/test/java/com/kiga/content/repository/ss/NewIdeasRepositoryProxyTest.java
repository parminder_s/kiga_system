package com.kiga.content.repository.ss;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.repository.ss.live.NewIdeasRepositoryLive;
import com.kiga.main.locale.Locale;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Pageable;

/**
 * @author bbs
 * @since 7/25/16.
 */
public class NewIdeasRepositoryProxyTest {
  private NewIdeasRepositoryProxy newIdeasRepositoryProxy;
  private NewIdeasRepositoryLive newIdeasRepositoryLiveMock = mock(NewIdeasRepositoryLive.class);

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    RepositoryContext repositoryContextMock = mock(RepositoryContext.class);
    when(repositoryContextMock.getContext()).thenReturn(RepositoryContexts.LIVE);
    newIdeasRepositoryProxy = new NewIdeasRepositoryProxy(newIdeasRepositoryLiveMock, null,
      repositoryContextMock);
  }

  @Test
  public void testGetIdeasForAgeGroupByBitPosition() {
    int ageGroupCode = 0;
    Locale locale = mock(Locale.class);
    Pageable pageable = mock(Pageable.class);

    newIdeasRepositoryProxy.getIdeasForAgeGroupByBitPosition(ageGroupCode, locale, pageable);
    verify(newIdeasRepositoryLiveMock)
      .getIdeasForAgeGroupByBitPosition(ageGroupCode, locale, pageable);
  }
}
