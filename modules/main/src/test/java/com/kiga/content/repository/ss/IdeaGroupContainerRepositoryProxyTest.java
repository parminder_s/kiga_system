package com.kiga.content.repository.ss;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepository;
import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepositoryProxy;
import com.kiga.content.repository.ss.live.IdeaGroupContainerRepositoryLive;
import com.kiga.main.locale.Locale;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by rainerh on 14.08.16.
 */
public class IdeaGroupContainerRepositoryProxyTest {
  private IdeaGroupContainerRepositoryProxy proxy;
  private IdeaGroupContainerRepository repository = mock(IdeaGroupContainerRepositoryLive.class);

  @Before
  public void init() {
    proxy = new IdeaGroupContainerRepositoryProxy(repository, null, new RepositoryContext(null));
  }

  @Test
  public void testFindOne() {
    proxy.findOne(1L);
    verify(repository).findOne(1L);
  }

  @Test
  public void testFindByIdAndClassName() {
    proxy.findByIdAndClassName(1L, "ClassName");
    verify(repository).findByIdAndClassName(1L, "ClassName");
  }

  @Test
  public void testFindRootSubgroups() {
    proxy.findRootSubgroups(Locale.en);
    verify(repository).findRootSubgroups(Locale.en);
  }

  @Test
  public void testFindSubgroups() {
    proxy.findSubgroups(Locale.en, "UrlSegment", 1);
    verify(repository).findSubgroups(Locale.en, "UrlSegment", 1);
  }
}
