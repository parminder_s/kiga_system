package com.kiga.content.starrating.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.Rating;
import com.kiga.content.domain.ss.live.KigaPageLive;
import com.kiga.content.repository.ss.RatingRepository;
import com.kiga.content.repository.ss.live.KigaPageRepositoryLive;
import com.kiga.ideas.service.RatingCalculatorService;
import com.kiga.ideas.web.response.IdeasResponseRating;
import com.kiga.security.domain.Member;
import com.kiga.security.repository.MemberRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asv on 22.02.17.
 */
public class StarRatingServiceTest {
  private RatingCalculatorService ratingCalculatorService;
  private KigaPageRepositoryLive kigaPageModel;
  private RatingRepository ratingRepository;
  private MemberRepository memberRepository;

  /**
   * setup Test.
   */
  @Before
  public void init() {
    ratingCalculatorService = new RatingCalculatorService();
    kigaPageModel = mock(KigaPageRepositoryLive.class);
    ratingRepository = mock(RatingRepository.class);
    memberRepository = mock(MemberRepository.class);
  }

  @Test
  public void getKigaPageRating() throws Exception {
    KigaPageLive kigaPage = new KigaPageLive();
    kigaPage.setRating1(5);
    Rating rating = new Rating();
    rating.setRatingValue(4);
    when(ratingRepository.findByMemberIdAndKigaPageId(1L, 1L)).thenReturn(rating);

    StarRatingService starRatingService =
      new StarRatingService(ratingRepository, null, kigaPageModel, ratingCalculatorService);
    when(starRatingService.getKigaPage(1L)).thenReturn(kigaPage);

    IdeasResponseRating response = starRatingService.getKigaPageRating(1L, 1L);
    Assert.assertEquals(response.getCount(), 5);
    Assert.assertEquals(response.getUserRating(), 4);
    Assert.assertTrue(response.getRating() == 1);
  }

  @Test
  public void addRatingToKigaPage() throws Exception {
    KigaPageLive kigaPage = new KigaPageLive();
    kigaPage.setRating1(1);
    kigaPage.setId(1L);
    List<Rating> ratings = new ArrayList<>();
    kigaPage.setRatings(ratings);

    Member member = new Member();
    member.setId(1L);
    when(memberRepository.findOne(1L)).thenReturn(member);
    when(ratingRepository.findByMemberIdAndKigaPageId(1L, 1L)).thenReturn(null);
    when(ratingRepository.findByMemberIdAndKigaPageId(2L, 1L)).thenReturn(new Rating());

    StarRatingService starRatingService = new StarRatingService(ratingRepository, memberRepository,
      kigaPageModel, ratingCalculatorService);
    when(starRatingService.getKigaPage(1L)).thenReturn(kigaPage);

    IdeasResponseRating response = starRatingService.addRatingToKigaPage(1L, 5, 1L);
    Assert.assertEquals(response.getCount(), 2);
    Assert.assertTrue(response.getRating() == 3);

    IdeasResponseRating notNullRatingResponse = starRatingService.addRatingToKigaPage(1L, 4, 2L);
    Assert.assertEquals(notNullRatingResponse.getCount(), 2);
  }

  @Test
  public void clearUserRating() throws Exception {
    KigaPageLive kigaPage = new KigaPageLive();
    kigaPage.setRating1(2);
    kigaPage.setId(1L);
    List<Rating> ratings = new ArrayList<>();
    kigaPage.setRatings(ratings);
    Rating rating = new Rating();
    rating.setRatingValue(1);
    when(ratingRepository.findByMemberIdAndKigaPageId(1L, 1L)).thenReturn(rating);

    StarRatingService starRatingService = new StarRatingService(ratingRepository, memberRepository,
      kigaPageModel, ratingCalculatorService);
    when(starRatingService.getKigaPage(1L)).thenReturn(kigaPage);

    IdeasResponseRating response = starRatingService.clearUserRating(1L, 1L);
    Assert.assertEquals(response.getCount(), 1);
    Assert.assertTrue(response.getRating() == 1);
  }
}
