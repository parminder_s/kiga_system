package com.kiga.content;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.spy;

import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.domain.ss.draft.SiteTreeDraft;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.content.sitemap.service.SiteMapGenerator;
import com.kiga.main.locale.Locale;
import com.kiga.web.service.DefaultUrlGenerator;
import com.kiga.web.service.UrlGenerator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/** Created by peter on 10.05.16. */
public class SiteMapGeneratorTest {
  private SiteTreeDraft home;

  /** setup home. */
  @Before
  public void init() {
    home = new SiteTreeDraft();
    home.setClassName("HomePage");
    home.setUrlSegment("/en");
    home.setLocale(Locale.en);
  }

  @Test
  public void testXmlForIdea() {
    Date date = new Date();

    KigaIdeaDraft idea = new KigaIdeaDraft();
    idea.setPriority("1");
    idea.setLastEdited(date);
    idea.setUrlSegment("/test/url");
    idea.setParent(home);

    UrlGenerator urlGenerator = spy(new DefaultUrlGenerator("baseUrl", "url", "ngUrl"));
    doReturn("").when(urlGenerator).getNgUrl(any(String.class));
    SiteTreeUrlGenerator siteTreeUrlGeneratorSpy =
        spy(new SiteTreeUrlGenerator(urlGenerator, false));
    doReturn("").when(siteTreeUrlGeneratorSpy).getNgUrl(any(SiteTree.class));
    SiteMapGenerator siteMapGenerator = new SiteMapGenerator(siteTreeUrlGeneratorSpy);
    // set class name to something that gets the old link
    idea.setClassName(siteMapGenerator.getOldSiteTreeList().get(0));
    String result = siteMapGenerator.generateXmlForSiteTree(idea);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    String comparer =
        "<url>"
            + "\n<loc>"
            + "url/en/test/url"
            + "</loc>"
            + "\n<lastmod>"
            + simpleDateFormat.format(date)
            + ""
            + "</lastmod>"
            + "\n<changefreq>"
            + "always"
            + "</changefreq>"
            + "\n<priority>"
            + "1"
            + "</priority>"
            + "\n</url>\n";

    Assert.assertEquals(comparer, result);
  }

  @Test
  public void testXmlForIdeaNgUrl() {
    Date date = new Date();

    KigaIdeaDraft idea = new KigaIdeaDraft();
    idea.setPriority("1");
    idea.setLastEdited(date);
    idea.setUrlSegment("/test/url");
    idea.setParent(home);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    UrlGenerator urlGenerator = new DefaultUrlGenerator("baseUrl", "url", "ngUrl");
    SiteMapGenerator siteMapGenerator =
        new SiteMapGenerator(new SiteTreeUrlGenerator(urlGenerator, false));

    // rename sitetree to something that will have new link
    idea.setClassName(siteMapGenerator.getNgSiteTreeList().get(0));
    String result = siteMapGenerator.generateXmlForSiteTree(idea);

    String comparer =
        "<url>"
            + "\n<loc>"
            + "ngUrl/en/cms/test/url"
            + "</loc>"
            + "\n<lastmod>"
            + simpleDateFormat.format(date)
            + ""
            + "</lastmod>"
            + "\n<changefreq>"
            + "always"
            + "</changefreq>"
            + "\n<priority>"
            + "1"
            + "</priority>"
            + "\n</url>\n";

    Assert.assertEquals(comparer, result);
  }

  @Test
  public void testXmlForIdeaUrlSegmentNull() {
    Date date = new Date();

    KigaIdeaDraft idea = new KigaIdeaDraft();
    idea.setPriority("1");
    idea.setLastEdited(date);
    idea.setUrlSegment(null);
    idea.setParent(home);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    UrlGenerator urlGenerator = new DefaultUrlGenerator("baseUrl", "url", "ngUrl");
    SiteMapGenerator siteMapGenerator =
        new SiteMapGenerator(new SiteTreeUrlGenerator(urlGenerator, false));
    String result = siteMapGenerator.generateXmlForSiteTree(idea);

    String comparer = "";

    Assert.assertEquals(comparer, result);
  }

  @Test
  public void testXmlForTwoIdeas() {

    SiteTreeDraft idea1 = new SiteTreeDraft();
    idea1.setPriority("1");
    Date date1 = new Date();
    idea1.setLastEdited(date1);
    idea1.setUrlSegment("/test/url1");
    idea1.setParent(home);

    SiteTreeDraft idea2 = new SiteTreeDraft();
    idea2.setPriority("1");
    Date date2 = new Date();
    idea2.setLastEdited(date2);
    idea2.setUrlSegment("/test/url2");
    idea2.setParent(home);

    List<SiteTreeDraft> sites = new ArrayList<>();
    sites.add(idea1);
    sites.add(idea2);

    UrlGenerator urlGenerator = spy(new DefaultUrlGenerator("baseUrl", "url", "ngUrl"));
    doReturn("").when(urlGenerator).getNgUrl(any(String.class));
    SiteTreeUrlGenerator siteTreeUrlGeneratorSpy =
        spy(new SiteTreeUrlGenerator(urlGenerator, false));
    doReturn("").when(siteTreeUrlGeneratorSpy).getNgUrl(any(SiteTree.class));
    SiteMapGenerator siteMapGenerator = new SiteMapGenerator(siteTreeUrlGeneratorSpy);
    // set class names to something that gets the old link
    idea1.setClassName(siteMapGenerator.getOldSiteTreeList().get(0));
    idea2.setClassName(siteMapGenerator.getOldSiteTreeList().get(0));
    String result = siteMapGenerator.getSiteMap(sites);

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    String comparer =
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n"
            + "<url>"
            + "\n<loc>"
            + "url/en/test/url1"
            + "</loc>"
            + "\n<lastmod>"
            + simpleDateFormat.format(date1)
            + ""
            + "</lastmod>"
            + "\n<changefreq>"
            + "always"
            + "</changefreq>"
            + "\n<priority>"
            + "1"
            + "</priority>"
            + "\n</url>"
            + "\n<url>"
            + "\n<loc>"
            + "url/en/test/url2"
            + "</loc>"
            + "\n<lastmod>"
            + simpleDateFormat.format(date2)
            + ""
            + "</lastmod>"
            + "\n<changefreq>"
            + "always"
            + "</changefreq>"
            + "\n<priority>"
            + "1"
            + "</priority>"
            + "\n</url>\n"
            + "\n</urlset>";

    Assert.assertEquals(comparer, result);
  }
}
