package com.kiga.content.repository.ss;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.cms.url.resolver.exception.InvalidMappingClassException;
import com.kiga.content.repository.ss.draft.IdeaCategoryRepositoryDraft;
import com.kiga.content.repository.ss.live.IdeaCategoryRepositoryLive;
import com.kiga.main.locale.Locale;
import org.junit.Test;

import java.util.Collections;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * @author bbs
 * @since 7/24/16.
 */
public class IdeaCategoryRepositoryProxyTest {
  private static Locale LOCALE = Locale.en;
  private static String URL_SEGMENT = "ABC";
  private static Integer LEVEL = 1;
  private static String CLASS_NAME = "XYZ";
  private static Long ID = 2L;
  private static Long PARENT_ID = 3L;
  private static String CODE = "QWE";

  @Test
  public void testDelegateMethods() throws InvalidMappingClassException {
    IdeaCategoryRepositoryLive ideaCategoryRepositoryLive = mock(IdeaCategoryRepositoryLive.class);
    IdeaCategoryRepositoryDraft ideaCategoryRepositoryDraft = mock(
      IdeaCategoryRepositoryDraft.class);

    RepositoryContext repositoryContext = mock(RepositoryContext.class);
    when(repositoryContext.getContext()).thenReturn(RepositoryContexts.DRAFT);

    EntityManagerFactory entityManagerFactory = mock(EntityManagerFactory.class);
    EntityManager entityManager = mock(EntityManager.class);

    when(entityManagerFactory.createEntityManager()).thenReturn(entityManager);
    IdeaCategoryRepositoryProxy ideaCategoryModel = new IdeaCategoryRepositoryProxy(
      ideaCategoryRepositoryLive, ideaCategoryRepositoryDraft, repositoryContext);

    ideaCategoryModel.findByLocaleAndUrlSegment(LOCALE, URL_SEGMENT);
    ideaCategoryModel.findByLocaleAndUrlSegment(LOCALE, URL_SEGMENT, LEVEL);
    ideaCategoryModel.findByLocale(LOCALE);
    ideaCategoryModel.findLevel1(LOCALE);
    ideaCategoryModel.findByClassName(CLASS_NAME);
    ideaCategoryModel.findById(ID);
    ideaCategoryModel.findByIdAndClassName(ID, CLASS_NAME);
    ideaCategoryModel.findByIdIn(Collections.singletonList(ID));
    ideaCategoryModel.findByParentIdOrderBySortAsc(PARENT_ID);
    ideaCategoryModel.findByCodeAndLocale(CODE, LOCALE);
    ideaCategoryModel.findOne(ID);

    verify(ideaCategoryRepositoryDraft, times(1)).findByLocaleAndUrlSegment(LOCALE, URL_SEGMENT);
    verify(ideaCategoryRepositoryDraft, times(1))
      .findByLocaleAndUrlSegment(LOCALE, URL_SEGMENT, LEVEL);
    verify(ideaCategoryRepositoryDraft, times(1)).findByLocale(LOCALE);
    verify(ideaCategoryRepositoryDraft, times(1)).findLevel1(LOCALE);
    verify(ideaCategoryRepositoryDraft, times(1)).findByClassName(CLASS_NAME);
    verify(ideaCategoryRepositoryDraft, times(1)).findById(ID);
    verify(ideaCategoryRepositoryDraft, times(1)).findByIdAndClassName(ID, CLASS_NAME);
    verify(ideaCategoryRepositoryDraft, times(1)).findByIdIn(Collections.singletonList(ID));
    verify(ideaCategoryRepositoryDraft, times(1)).findByParentIdOrderBySortAsc(PARENT_ID);
    verify(ideaCategoryRepositoryDraft, times(1)).findByCodeAndLocale(CODE, LOCALE);
    verify(ideaCategoryRepositoryDraft, times(1)).findOne(ID.longValue());

    when(repositoryContext.getContext()).thenReturn(RepositoryContexts.LIVE);

    ideaCategoryModel.findByLocaleAndUrlSegment(LOCALE, URL_SEGMENT);
    ideaCategoryModel.findByLocaleAndUrlSegment(LOCALE, URL_SEGMENT, LEVEL);
    ideaCategoryModel.findByLocale(LOCALE);
    ideaCategoryModel.findLevel1(LOCALE);
    ideaCategoryModel.findByClassName(CLASS_NAME);
    ideaCategoryModel.findById(ID);
    ideaCategoryModel.findByIdAndClassName(ID, CLASS_NAME);
    ideaCategoryModel.findByIdIn(Collections.singletonList(ID));
    ideaCategoryModel.findByParentIdOrderBySortAsc(PARENT_ID);
    ideaCategoryModel.findByCodeAndLocale(CODE, LOCALE);
    ideaCategoryModel.findOne(ID);

    verify(ideaCategoryRepositoryLive, times(1)).findByLocaleAndUrlSegment(LOCALE, URL_SEGMENT);
    verify(ideaCategoryRepositoryLive, times(1))
      .findByLocaleAndUrlSegment(LOCALE, URL_SEGMENT, LEVEL);
    verify(ideaCategoryRepositoryLive, times(1)).findByLocale(LOCALE);
    verify(ideaCategoryRepositoryLive, times(1)).findLevel1(LOCALE);
    verify(ideaCategoryRepositoryLive, times(1)).findByClassName(CLASS_NAME);
    verify(ideaCategoryRepositoryLive, times(1)).findById(ID);
    verify(ideaCategoryRepositoryLive, times(1)).findByIdAndClassName(ID, CLASS_NAME);
    verify(ideaCategoryRepositoryLive, times(1)).findByIdIn(Collections.singletonList(ID));
    verify(ideaCategoryRepositoryLive, times(1)).findByParentIdOrderBySortAsc(PARENT_ID);
    verify(ideaCategoryRepositoryLive, times(1)).findByCodeAndLocale(CODE, LOCALE);
    verify(ideaCategoryRepositoryLive, times(1)).findOne(ID.longValue());
  }
}
