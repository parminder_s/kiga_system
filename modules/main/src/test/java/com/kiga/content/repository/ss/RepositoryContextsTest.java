package com.kiga.content.repository.ss;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author bbs
 * @since 7/24/16.
 */
public class RepositoryContextsTest {
  @Test
  public void testERepositoryContexts() {
    RepositoryContexts[] expectedValues = {RepositoryContexts.LIVE, RepositoryContexts.DRAFT};
    Assert.assertEquals(Arrays.asList(expectedValues), Arrays.asList(RepositoryContexts.values()));
    Assert.assertEquals(RepositoryContexts.LIVE, RepositoryContexts.valueOf("LIVE"));
    Assert.assertEquals(RepositoryContexts.DRAFT, RepositoryContexts.valueOf("DRAFT"));
  }
}
