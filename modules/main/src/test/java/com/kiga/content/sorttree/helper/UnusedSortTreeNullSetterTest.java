package com.kiga.content.sorttree.helper;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.matches;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.draft.IdeaGroupDraft;
import com.kiga.content.domain.ss.draft.IdeaGroupToIdeaDraft;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.repository.ss.draft.IdeaGroupToIdeaRepository;
import com.kiga.content.repository.ss.live.IdeaGroupLiveRepository;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;

public class UnusedSortTreeNullSetterTest {
  private UnusedSortTreeNullSetter unusedSortTreeNullSetter;
  private IdeaGroupToIdeaRepository ideaGroupToIdeaRepositoryMock;
  private IdeaGroupLiveRepository ideaGroupLiveRepositoryMock;

  @Before
  public void prepareTests() {
    IdeaGroupLive ideaGroupLive1 = new IdeaGroupLive();
    ideaGroupLive1.setId(1L);
    IdeaGroupLive ideaGroupLive2 = new IdeaGroupLive();
    ideaGroupLive2.setId(2L);
    IdeaGroupLive ideaGroupLive3 = new IdeaGroupLive();
    ideaGroupLive3.setId(3L);
    IdeaGroupDraft ideaGroupDraft2 = new IdeaGroupDraft();
    ideaGroupDraft2.setId(ideaGroupLive2.getId());

    IdeaGroupToIdeaDraft ideaGroupToIdeaDraftOther = new IdeaGroupToIdeaDraft();
    ideaGroupToIdeaDraftOther.setId(0L);
    IdeaGroupToIdeaDraft ideaGroupToIdeaDraft = new IdeaGroupToIdeaDraft();
    ideaGroupToIdeaDraft.setSortTree("AAAAAB");
    ideaGroupToIdeaDraft.setIdeaGroup(ideaGroupDraft2);
    ideaGroupLiveRepositoryMock = mock(IdeaGroupLiveRepository.class);
    when(ideaGroupLiveRepositoryMock.findByClassName(matches("IdeaGroup")))
        .thenReturn(Arrays.asList(ideaGroupLive1, ideaGroupLive3));
    ideaGroupToIdeaRepositoryMock = mock(IdeaGroupToIdeaRepository.class);
    when(ideaGroupToIdeaRepositoryMock.findAll())
        .thenReturn(
            Arrays.asList(
                ideaGroupToIdeaDraftOther, ideaGroupToIdeaDraftOther, ideaGroupToIdeaDraft));

    unusedSortTreeNullSetter =
        new UnusedSortTreeNullSetter(ideaGroupToIdeaRepositoryMock, ideaGroupLiveRepositoryMock);
  }

  @Test
  public void testResolveIdeaGroupToArticlesRelation() {

    unusedSortTreeNullSetter.setUnusedToNull();
    verify(ideaGroupToIdeaRepositoryMock, times(1)).save(any(IdeaGroupToIdeaDraft.class));
  }
}
