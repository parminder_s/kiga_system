package com.kiga.content;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.IdeaGroupToIdea;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.draft.IdeaGroupContainerDraft;
import com.kiga.content.domain.ss.draft.IdeaGroupDraft;
import com.kiga.content.domain.ss.draft.IdeaGroupToIdeaDraft;
import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepositoryProxy;
import com.kiga.content.service.KigaIdeaUrlGenerator;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.main.locale.Locale;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by peter on 29.03.17.
 */
public class KigaIdeaUrlGeneratorTest {
  @Test
  public void kigaIdeaUrlGeneratorTest() {
    Set<IdeaGroupToIdea> set = new HashSet<>();
    IdeaGroupToIdeaDraft ideaGroupToIdeaDraft = new IdeaGroupToIdeaDraft();
    IdeaGroupDraft ideaGroupDraft = new IdeaGroupDraft();
    ideaGroupToIdeaDraft.setIdeaGroup(ideaGroupDraft);
    set.add(ideaGroupToIdeaDraft);

    KigaIdea idea = new KigaIdeaDraft();
    idea.setUrlSegment("urlSegment");
    idea.setIdeaGroupToIdeaList(set);

    SiteTreeUrlGenerator siteTreeUrlGenerator = mock(SiteTreeUrlGenerator.class);
    when(siteTreeUrlGenerator.getRelativeNgUrl(ideaGroupDraft)).thenReturn("http://url.com/test");

    KigaIdeaUrlGenerator kigaIdeaUrlGenerator = new KigaIdeaUrlGenerator(siteTreeUrlGenerator,
      null);
    Assert.assertEquals("http://url.com/test?preview=urlSegment&previewCategory=0",
      kigaIdeaUrlGenerator.getRelativePreviewUrl(idea));
  }

  @Test
  public void kigaIdeaUrlGeneratorNullTest() {
    Set<IdeaGroupToIdea> set = new HashSet<>();

    KigaIdea idea = new KigaIdeaDraft();
    idea.setLocale(Locale.de);
    idea.setIdeaGroupToIdeaList(set);

    IdeaGroupContainerRepositoryProxy ideaGroupContainerRepositoryProxyMock = mock(
      IdeaGroupContainerRepositoryProxy.class);
    IdeaGroupContainer ideaGroupContainer = new IdeaGroupContainerDraft();
    when(ideaGroupContainerRepositoryProxyMock.findIdeaGroupMainContainer(eq(Locale.de)))
      .thenReturn(ideaGroupContainer);

    SiteTreeUrlGenerator siteTreeUrlGenerator = mock(SiteTreeUrlGenerator.class);
    when(siteTreeUrlGenerator.getRelativeNgUrl(eq(ideaGroupContainer))).thenReturn("main");

    KigaIdeaUrlGenerator kigaIdeaUrlGenerator = new KigaIdeaUrlGenerator(siteTreeUrlGenerator,
      ideaGroupContainerRepositoryProxyMock);
    Assert.assertEquals("main", kigaIdeaUrlGenerator.getRelativePreviewUrl(idea));
  }

  @Test
  public void kigaIdeaUrlGeneratorIdeaGroupNullTest() {
    Set<IdeaGroupToIdea> set = new HashSet<>();
    IdeaGroupToIdeaDraft ideaGroupToIdeaDraft = new IdeaGroupToIdeaDraft();
    ideaGroupToIdeaDraft.setIdeaGroup(null);
    set.add(ideaGroupToIdeaDraft);

    KigaIdea idea = new KigaIdeaDraft();
    idea.setLocale(Locale.de);
    idea.setIdeaGroupToIdeaList(set);

    IdeaGroupContainerRepositoryProxy ideaGroupContainerRepositoryProxyMock = mock(
      IdeaGroupContainerRepositoryProxy.class);
    IdeaGroupContainer ideaGroupContainer = new IdeaGroupContainerDraft();
    when(ideaGroupContainerRepositoryProxyMock.findIdeaGroupMainContainer(eq(Locale.de)))
      .thenReturn(ideaGroupContainer);

    SiteTreeUrlGenerator siteTreeUrlGenerator = mock(SiteTreeUrlGenerator.class);
    when(siteTreeUrlGenerator.getRelativeNgUrl(eq(ideaGroupContainer))).thenReturn("main");

    KigaIdeaUrlGenerator kigaIdeaUrlGenerator = new KigaIdeaUrlGenerator(siteTreeUrlGenerator,
      ideaGroupContainerRepositoryProxyMock);
    Assert.assertEquals("main", kigaIdeaUrlGenerator.getRelativePreviewUrl(idea));
  }
}
