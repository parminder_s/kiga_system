package com.kiga.content;

import static org.hamcrest.core.IsInstanceOf.instanceOf;

import com.kiga.FeaturesProperties;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.KigaIdeaRepositoryProxy;
import com.kiga.content.repository.ss.NewIdeasRepositoryProxy;
import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepositoryProxy;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.content.service.SiteTreeUrlGenerator;
import org.junit.Assert;
import org.junit.Test;

/** Created by peter on 18.05.16. */
public class ContentConfigTest {

  @Test
  public void testGetSiteTreeUrlGenerator() {
    ContentConfig contentConfig = new ContentConfig();
    SiteTreeUrlGenerator generator =
        contentConfig.getSiteTreeUrlGenerator(null, FeaturesProperties.builder().build());
    Assert.assertNotNull(generator);
    Assert.assertThat(generator, instanceOf(SiteTreeUrlGenerator.class));
  }

  @Test
  public void testGetKigaIdeaModel() {
    ContentConfig contentConfig = new ContentConfig();
    KigaIdeaRepositoryProxy bean = contentConfig.getKigaIdeaModel(null, null, null);
    Assert.assertNotNull(bean);
    Assert.assertThat(bean, instanceOf(KigaIdeaRepositoryProxy.class));
  }

  @Test
  public void testGetIdeaCategoryModel() {
    ContentConfig contentConfig = new ContentConfig();
    IdeaCategoryRepositoryProxy bean = contentConfig.getIdeaCategoryModel(null, null, null);
    Assert.assertNotNull(bean);
    Assert.assertThat(bean, instanceOf(IdeaCategoryRepositoryProxy.class));
  }

  @Test
  public void testCreateRepositoryContext() {
    ContentConfig contentConfig = new ContentConfig();
    RepositoryContext bean = contentConfig.createRepositoryContext(null);
    Assert.assertNotNull(bean);
    Assert.assertThat(bean, instanceOf(RepositoryContext.class));
  }

  @Test
  public void testCreateAgeGroupIdeasModel() {
    ContentConfig contentConfig = new ContentConfig();
    NewIdeasRepository bean = contentConfig.createAgeGroupIdeasModel(null, null, null);
    Assert.assertNotNull(bean);
    Assert.assertThat(bean, instanceOf(NewIdeasRepositoryProxy.class));
  }

  @Test
  public void testCreateIdeaGroupContainerRepositoryProxy() {
    ContentConfig contentConfig = new ContentConfig();
    IdeaGroupContainerRepositoryProxy bean =
        contentConfig.getIdeaGroupContainerRepositoryContext(null, null, null);
    Assert.assertNotNull(bean);
    Assert.assertThat(bean, instanceOf(IdeaGroupContainerRepositoryProxy.class));
  }

  @Test
  public void testGetNewIdeaRepositoryModel() {
    ContentConfig contentConfig = new ContentConfig();
    NewIdeasRepositoryProxy bean = contentConfig.getNewIdeaRepositoryModel(null, null, null);
    Assert.assertNotNull(bean);
    Assert.assertThat(bean, instanceOf(NewIdeasRepositoryProxy.class));
  }
}
