package com.kiga.content.web.response;

import com.kiga.content.repository.ss.RepositoryContexts;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author bbs
 * @since 7/23/16.
 */
public class ToggleContextViewModelTest {
  @Test
  public void testBasic() {
    ToggleContextResponse toggleContextResponse = new ToggleContextResponse(
      RepositoryContexts.LIVE);
    Assert.assertEquals(RepositoryContexts.LIVE, toggleContextResponse.getRepositoryContexts());
    toggleContextResponse.setRepositoryContexts(RepositoryContexts.DRAFT);
    Assert.assertEquals(RepositoryContexts.DRAFT, toggleContextResponse.getRepositoryContexts());
  }
}
