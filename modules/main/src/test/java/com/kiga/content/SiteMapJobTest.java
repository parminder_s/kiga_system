package com.kiga.content;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;

import com.amazonaws.services.s3.AmazonS3Client;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.sitemap.SiteMapJob;
import com.kiga.content.sitemap.service.SiteMapGenerator;
import com.kiga.s3.S3Properties;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by peter on 18.05.16.
 */
public class SiteMapJobTest {
  @Test
  public void testSiteMapJob() {
    AmazonS3Client amazonS3Client = mock(AmazonS3Client.class);
    SiteMapGenerator siteMapGenerator = mock(SiteMapGenerator.class);
    SiteTreeLiveRepository siteTreeLiveRepository = mock(SiteTreeLiveRepository.class);

    when(siteTreeLiveRepository.findAll()).thenReturn(new ArrayList<SiteTreeLive>());
    when(siteMapGenerator.getSiteMap(any())).thenReturn("sitemap content");

    S3Properties s3PropertiesMock = mock(S3Properties.class);

    when(s3PropertiesMock.getBucketName()).thenReturn("bucketName");

    SiteMapJob siteMapJob = new
      SiteMapJob(s3PropertiesMock, siteMapGenerator, siteTreeLiveRepository, amazonS3Client);
    siteMapJob.runJob();

    verify(amazonS3Client).putObject(eq("bucketName"), eq(SiteMapJob.SITEMAP_KEY), any(), any());
    verify(siteTreeLiveRepository).findAll();
    verify(siteMapGenerator).getSiteMap(any());
  }
}
