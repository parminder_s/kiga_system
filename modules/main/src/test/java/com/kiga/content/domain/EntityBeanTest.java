package com.kiga.content.domain;

import com.google.code.beanmatchers.BeanMatchers;

import com.kiga.content.domain.ss.draft.IdeaGroupContainerDraft;
import com.kiga.content.domain.ss.draft.IdeaGroupDraft;
import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.domain.ss.draft.LinkedKigaPageGroupDraft;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.live.LinkedKigaPageGroupLive;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 22.10.16.
 */
public class EntityBeanTest {
  @Test
  public void testBeans() {
    Assert.assertThat(LinkedKigaPageGroupDraft.class, BeanMatchers.hasValidGettersAndSetters());
    Assert.assertThat(LinkedKigaPageGroupLive.class, BeanMatchers.hasValidGettersAndSetters());

    Assert.assertThat(KigaIdeaDraft.class, BeanMatchers.hasValidGettersAndSetters());
    Assert.assertThat(KigaIdeaLive.class, BeanMatchers.hasValidGettersAndSetters());

    Assert.assertThat(IdeaGroupDraft.class, BeanMatchers.hasValidGettersAndSetters());
    Assert.assertThat(IdeaGroupLive.class, BeanMatchers.hasValidGettersAndSetters());

    Assert.assertThat(IdeaGroupContainerDraft.class, BeanMatchers.hasValidGettersAndSetters());
    Assert.assertThat(IdeaGroupContainerLive.class, BeanMatchers.hasValidGettersAndSetters());
  }
}
