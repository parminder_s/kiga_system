package com.kiga.content;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.sitemap.service.SiteMapGenerator;
import com.kiga.content.sitemap.web.SiteMapController;
import com.kiga.s3.S3Properties;
import com.kiga.security.services.SecurityService;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by peter on 24.05.16.
 */
public class SiteMapControllerTest {
  @Test
  public void siteMapXml() throws IOException {
    S3Properties s3Properties = mock(S3Properties.class);
    when(s3Properties.getAccessKey()).thenReturn("access");
    when(s3Properties.getBucketName()).thenReturn("bucket");
    when(s3Properties.getSecretKey()).thenReturn("secret");

    SiteMapGenerator siteMapGenerator = mock(SiteMapGenerator.class);
    when(siteMapGenerator.getSiteMap(any())).thenReturn("sitemap content");

    HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
    ServletOutputStream servletOutputStream = mock(ServletOutputStream.class);
    when(httpServletResponse.getOutputStream()).thenReturn(servletOutputStream);

    S3Object s3Object = mock(S3Object.class);
    InputStream inputStream = new ByteArrayInputStream("Test".getBytes(StandardCharsets.UTF_8));
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    S3ObjectInputStream s3ObjectInputStream = new S3ObjectInputStream(inputStream, null);
    when(s3Object.getObjectContent()).thenReturn(s3ObjectInputStream);
    AmazonS3Client amazonS3Client = mock(AmazonS3Client.class);
    when(amazonS3Client.getObject(anyString(), anyString())).thenReturn(s3Object);

    SiteTreeLiveRepository siteTreeLiveRepository = mock(SiteTreeLiveRepository.class);
    SiteMapController siteMapController = new SiteMapController(s3Properties, null,
      siteMapGenerator, siteTreeLiveRepository, amazonS3Client);

    siteMapController.siteMapXml(httpServletResponse);
    verify(httpServletResponse).setContentType(eq("text/xml"));
    verify(amazonS3Client).getObject(eq("bucket"), eq("sitemap.xml"));
  }

  @Test
  public void generateSiteMap() throws IOException {
    S3Properties s3Properties = mock(S3Properties.class);
    SiteMapGenerator siteMapGenerator = mock(SiteMapGenerator.class);
    HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
    PrintWriter printWriter = mock(PrintWriter.class);

    when(s3Properties.getAccessKey()).thenReturn("access");
    when(s3Properties.getSecretKey()).thenReturn("secret");
    when(siteMapGenerator.getSiteMap(any())).thenReturn("sitemap content");
    when(httpServletResponse.getWriter()).thenReturn(printWriter);

    SiteTreeLiveRepository siteTreeLiveRepository = mock(SiteTreeLiveRepository.class);
    SecurityService securityService = mock(SecurityService.class);
    AmazonS3Client amazonS3Client = mock(AmazonS3Client.class);

    SiteMapController siteMapController = new SiteMapController(s3Properties, securityService,
      siteMapGenerator, siteTreeLiveRepository, amazonS3Client);

    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    siteMapController.generateSiteMap(httpServletResponse);
    verify(siteMapGenerator).getSiteMap(any());
    verify(securityService).requiresContentAdmin();
    verify(httpServletResponse).setContentType(eq("text/xml"));
  }
}
