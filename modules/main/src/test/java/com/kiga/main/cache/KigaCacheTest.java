package com.kiga.main.cache;

import static org.mockito.Mockito.mock;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.data.redis.core.RedisOperations;
import scala.Array;

/**
 * Created by peter on 29.09.16.
 */
public class KigaCacheTest {
  @Test
  public void useCacheTest() {
    KigaCache kigaCache = new KigaCache("default", Array.emptyByteArray(),
      mock(RedisOperations.class), 1, true);
    Assert.assertEquals(kigaCache.isUseCache(), true);
  }

  @Test
  public void useCacheTestFalse() {
    KigaCache kigaCache = new KigaCache("default",  Array.emptyByteArray(),
      mock(RedisOperations.class), 1, false);
    Assert.assertEquals(kigaCache.isUseCache(), false);
  }
}
