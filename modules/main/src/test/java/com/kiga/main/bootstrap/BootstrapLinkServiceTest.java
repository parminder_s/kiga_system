package com.kiga.main.bootstrap;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.matches;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.MainController;
import com.kiga.bootstrap.api.model.BootstrapLink;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.main.bootstrap.services.BootstrapLinkService;
import com.kiga.main.locale.Locale;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BootstrapLinkServiceTest {
  private BootstrapLinkService bootstrapLinkService;
  private SiteTreeLiveRepository siteTreeLiveRepositoryMock;
  private SiteTreeUrlGenerator siteTreeUrlGeneratorMock;
  private Map translationsMock;

  @Before
  public void prepareTests() {
    siteTreeLiveRepositoryMock = mock(SiteTreeLiveRepository.class);
    siteTreeUrlGeneratorMock = mock(SiteTreeUrlGenerator.class);
    SiteTreeLive siteTreeLive = new SiteTreeLive();
    siteTreeLive.setTitle("testTitle");
    siteTreeLive.setChildren(Arrays.asList(siteTreeLive, siteTreeLive));
    when(siteTreeLiveRepositoryMock.findByClassNameAndLocale(anyString(), any(Locale.class)))
        .thenReturn(Collections.singletonList(siteTreeLive));
    when(siteTreeUrlGeneratorMock.getRelativeNgUrl(any(SiteTreeLive.class)))
        .thenReturn("testRelativeNgUrl");
    when(siteTreeUrlGeneratorMock.getRelativeUrl(any(SiteTreeLive.class)))
        .thenReturn("testRelativeUrl");
    translationsMock = mock(Map.class);
    when(translationsMock.get(anyString())).thenReturn("testTranslation");
    bootstrapLinkService =
        new BootstrapLinkService(siteTreeLiveRepositoryMock, siteTreeUrlGeneratorMock);
  }

  @Test
  public void getOldLinks() {
    List list = bootstrapLinkService.getOldLinks();
    Assert.assertEquals(6, list.size());
    Assert.assertEquals("My Account", ((MainController.LinkEntity) list.get(0)).getLabel());
    Assert.assertEquals("Search", ((MainController.LinkEntity) list.get(1)).getLabel());
    Assert.assertEquals("Forum", ((MainController.LinkEntity) list.get(2)).getLabel());
    Assert.assertEquals("Shop", ((MainController.LinkEntity) list.get(3)).getLabel());
    Assert.assertEquals(
        "ideaGroupMainContainer", ((MainController.LinkEntity) list.get(4)).getLabel());
    Assert.assertEquals("latestProductPage", ((MainController.LinkEntity) list.get(5)).getLabel());
  }

  @Test
  public void getOldFixedLinks() {
    List<BootstrapLink> list = bootstrapLinkService.getOldFixedLinks();

    Assert.assertEquals(6, list.size());
    Assert.assertEquals("My Account", (list.get(0)).getLabel());
    Assert.assertEquals("Search", (list.get(1)).getLabel());
    Assert.assertEquals("Forum", (list.get(2)).getLabel());
    Assert.assertEquals("Shop", (list.get(3)).getLabel());
    Assert.assertEquals("ideaGroupMainContainer", (list.get(4)).getLabel());
    Assert.assertEquals("latestProductPage", (list.get(5)).getLabel());
  }

  @Test
  public void getLinksMemberAdminTrue() {
    @SuppressWarnings("unchecked")
    List<BootstrapLink> list = bootstrapLinkService.getLinks("en", true, translationsMock);
    Assert.assertEquals("en/member/account", list.get(2).getUrl());
    testLinks(list);
  }

  @Test
  public void getLinksMemberAdminTrueLinkCacheNotNull() {
    bootstrapLinkService.getLinks("en", true, translationsMock);
    bootstrapLinkService.getLinks("en", true, translationsMock);

    verify(siteTreeLiveRepositoryMock, times(14))
        .findByClassNameAndLocale(anyString(), any(Locale.class));
  }

  @Test
  public void getLinksMemberAdminFalseListsEmpty() {

    when(siteTreeLiveRepositoryMock.findByClassNameAndLocale(
            matches("ShopMainContainer"), any(Locale.class)))
        .thenReturn(Collections.emptyList());

    when(siteTreeLiveRepositoryMock.findByClassNameAndLocale(
            matches("ShopArticlePage"), any(Locale.class)))
        .thenReturn(Collections.emptyList());

    @SuppressWarnings("unchecked")
    List<BootstrapLink> list = bootstrapLinkService.getLinks("en", false, translationsMock);
    Assert.assertEquals("testRelativeNgUrl", list.get(2).getUrl());
    testLinks(list);
  }

  @Test
  public void getLinksMemberAdminFalse() {

    @SuppressWarnings("unchecked")
    List<BootstrapLink> list = bootstrapLinkService.getLinks("en", false, translationsMock);
    Assert.assertEquals("testRelativeNgUrl", list.get(2).getUrl());
    testLinks(list);
  }

  private void testLinks(List<BootstrapLink> list) {
    Assert.assertEquals(17, list.size());
  }
}
