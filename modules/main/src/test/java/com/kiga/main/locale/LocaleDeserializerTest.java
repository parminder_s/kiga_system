package com.kiga.main.locale;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonParser;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;

/**
 * Created by rainerh on 30.10.16.
 */
public class LocaleDeserializerTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Test
  public void testLocale() throws IOException {
    LocaleDeserializer localeDeserializer = new LocaleDeserializer();
    JsonParser jsonParser = mock(JsonParser.class);
    when(jsonParser.getValueAsString()).thenReturn("de");
    Assert.assertEquals(Locale.de, localeDeserializer.deserialize(jsonParser, null));
  }

  @Test
  public void testUnknownLocale() throws IOException {
    LocaleDeserializer localeDeserializer = new LocaleDeserializer();
    JsonParser jsonParser = mock(JsonParser.class);
    when(jsonParser.getValueAsString()).thenReturn("fr");
    expectedException.expect(RuntimeException.class);
    localeDeserializer.deserialize(jsonParser, null);
  }

}
