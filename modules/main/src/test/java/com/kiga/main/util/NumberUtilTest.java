package com.kiga.main.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bbs
 * @since 2/10/17.
 */
public class NumberUtilTest {
  @Test
  public void testIsValidId() {
    assertTrue(NumberUtil.isValidId(1L));
    assertTrue(NumberUtil.isValidId(10L));
    assertTrue(NumberUtil.isValidId(10000000000L));

    assertFalse(NumberUtil.isValidId(0L));
    assertFalse(NumberUtil.isValidId(-1L));
    assertFalse(NumberUtil.isValidId(-10L));
    assertFalse(NumberUtil.isValidId(-10000000000L));
  }

  @Test
  public void testIsNotValidId() {
    assertFalse(NumberUtil.isNotValidId(1L));
    assertFalse(NumberUtil.isNotValidId(10L));
    assertFalse(NumberUtil.isNotValidId(10000000000L));

    assertTrue(NumberUtil.isNotValidId(0L));
    assertTrue(NumberUtil.isNotValidId(-1L));
    assertTrue(NumberUtil.isNotValidId(-10L));
    assertTrue(NumberUtil.isNotValidId(-10000000000L));
  }

  @Test
  public void testGetValidIds() {
    List<Long> ids = new ArrayList<>();
    for (long i = -10L; i <= 10L; ++i) {
      ids.add(i);
    }
    List<Long> validIds = NumberUtil.getValidIds(ids);
    assertNotNull(validIds);

    for (Long id : validIds) {
      assertNotNull(id);
      assertTrue(id > 0);
    }
  }
}
