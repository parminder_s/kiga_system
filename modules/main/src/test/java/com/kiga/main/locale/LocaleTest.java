package com.kiga.main.locale;

import com.google.code.beanmatchers.BeanMatchers;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by rainerh on 29.10.16.
 */
public class LocaleTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Test
  public void bean() {
    Assert.assertThat(Locale.class, BeanMatchers.hasValidBeanHashCodeExcluding("value"));
    Assert.assertThat(Locale.class, BeanMatchers.hasValidBeanEqualsExcluding("value"));
    Assert.assertEquals("de", Locale.de.toString());
    Assert.assertNull(Locale.valueOf("ch"));
  }
}
