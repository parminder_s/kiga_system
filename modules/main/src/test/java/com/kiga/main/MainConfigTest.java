package com.kiga.main;

import static org.hamcrest.core.IsInstanceOf.instanceOf;

import com.fasterxml.jackson.databind.Module;
import org.junit.Assert;
import org.junit.Test;
import org.modelmapper.ModelMapper;

/**
 * Created by rainerh on 30.10.16.
 */
public class MainConfigTest {
  @Test
  public void testBeans() {
    MainConfig mainConfig = new MainConfig();
    Assert.assertThat(mainConfig.getJacksonLocaleModule(), instanceOf(Module.class));
    Assert.assertThat(mainConfig.getModelMapper(), instanceOf(ModelMapper.class));
  }

}
