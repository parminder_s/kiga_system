package com.kiga.main.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.internal.util.collections.ArrayUtils;

import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 2/7/17.
 */
public class CookieServiceTest {
  private static final String CORRECT_COOKIE_NAME = "CORRECT_COOKIE_NAME";
  private static final String CORRECT_COOKIE_VALUE = "CORRECT_COOKIE_VALUE";
  private static final String NONEXISTENT_COOKIE_NAME = "NONEXISTENT_COOKIE_NAME";
  private static final String DEFAULT_VALUE = "DEFAULT_VALUE";
  private HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
  private HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
  private CookieService cookieService;

  @Before
  public void prepareTests() {
    cookieService = new CookieService(httpServletRequest, httpServletResponse);
  }

  @Test
  public void testFindingTheRightValueFromCoookie() {
    when(httpServletRequest.getCookies()).thenReturn(null);
    assertEquals(Optional.empty(), cookieService.getOptional(NONEXISTENT_COOKIE_NAME));

    when(httpServletRequest.getCookies()).thenReturn(new Cookie[0]);
    assertEquals(Optional.empty(), cookieService.getOptional(NONEXISTENT_COOKIE_NAME));

    when(httpServletRequest.getCookies()).thenReturn(
      new Cookie[]{new Cookie("ABC", "DEF"), new Cookie(CORRECT_COOKIE_NAME,
        CORRECT_COOKIE_VALUE)});
    assertEquals(CORRECT_COOKIE_VALUE, cookieService.get(CORRECT_COOKIE_NAME));
    assertEquals(CORRECT_COOKIE_VALUE, cookieService.get(CORRECT_COOKIE_NAME, DEFAULT_VALUE));
    assertEquals(DEFAULT_VALUE, cookieService.get(NONEXISTENT_COOKIE_NAME, DEFAULT_VALUE));
    assertEquals(Optional.of(CORRECT_COOKIE_VALUE),
      cookieService.getOptional(CORRECT_COOKIE_NAME));
    assertEquals(Optional.empty(), cookieService.getOptional(NONEXISTENT_COOKIE_NAME));
  }

  @Test
  public void testAddingTheCookie() {
    cookieService.add(CORRECT_COOKIE_NAME, CORRECT_COOKIE_VALUE);
    ArgumentCaptor<Cookie> cookieArgumentCaptor = ArgumentCaptor.forClass(Cookie.class);
    verify(httpServletResponse).addCookie(cookieArgumentCaptor.capture());
    Cookie capturedCookie = cookieArgumentCaptor.getValue();
    assertEquals(CORRECT_COOKIE_NAME, capturedCookie.getName());
    assertEquals(CORRECT_COOKIE_VALUE, capturedCookie.getValue());
    assertEquals("/", capturedCookie.getPath());
  }

  @Test
  public void testRemoval() {
    HttpServletResponse response = mock(HttpServletResponse.class);
    new CookieService(null, response).remove("foo");

    ArgumentCaptor<Cookie> cookieArgumentCaptor = ArgumentCaptor.forClass(Cookie.class);
    verify(response).addCookie(cookieArgumentCaptor.capture());
    Cookie capturedCookie = cookieArgumentCaptor.getValue();
    assertEquals("foo", capturedCookie.getName());
    assertNull(capturedCookie.getValue());
    assertEquals(0, capturedCookie.getMaxAge());
    assertEquals("/", capturedCookie.getPath());
  }
}
