package com.kiga.main.cache;

import static org.mockito.Mockito.mock;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.cache.CacheManager;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * Created by peter on 29.09.16.
 */
public class CacheConfigTest {

  @Test
  public void cacheManagerUseCacheTest() {
    CacheProperties cacheProperties = new CacheProperties();
    cacheProperties.setUseCache(true);
    cacheProperties.setDefaultTimeout(10);
    cacheProperties.setUrl("url");
    cacheProperties.setEnabled(true);

    CacheConfig cacheConfig = new CacheConfig();
    cacheConfig.setCacheProperties(cacheProperties);
    CacheManager cacheManager = cacheConfig.cacheManager(mock(RedisTemplate.class));

    KigaCache cache = (KigaCache) cacheManager.getCache("default");
    Assert.assertEquals(true, cache.isUseCache());
  }

  @Test
  public void cacheManagerUseCacheTestFalse() {
    CacheProperties cacheProperties = new CacheProperties();
    cacheProperties.setUseCache(false);
    cacheProperties.setDefaultTimeout(10);
    cacheProperties.setUrl("url");
    cacheProperties.setEnabled(true);

    CacheConfig cacheConfig = new CacheConfig();
    cacheConfig.setCacheProperties(cacheProperties);
    CacheManager cacheManager = cacheConfig.cacheManager(mock(RedisTemplate.class));

    KigaCache cache = (KigaCache) cacheManager.getCache("default");
    Assert.assertEquals(false, cache.isUseCache());
  }
}
