package com.kiga.main.locale;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.fasterxml.jackson.core.JsonGenerator;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by rainerh on 16.11.16.
 */
public class LocaleSerializerTest {
  @Test
  public void testSerializer() throws IOException {
    JsonGenerator jsonGenerator = mock(JsonGenerator.class);
    new LocaleSerializer().serialize(Locale.en, jsonGenerator, null);
    verify(jsonGenerator).writeString("en");
  }
}
