package com.kiga.main.bootstrap;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.FeaturesProperties;
import com.kiga.bootstrap.api.model.Config;
import com.kiga.main.bootstrap.services.BootstrapConfigService;
import com.kiga.main.bootstrap.services.BootstrapLinkService;
import com.kiga.main.bootstrap.services.BootstrapMetaService;
import com.kiga.main.bootstrap.services.BootstrapTranslationService;
import com.kiga.shop.service.CountryService;
import com.kiga.translation.provider.TranslationService;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import scala.collection.immutable.HashMap;

public class BootstrapControllerTest {
  private BootstrapController bootstrapController;
  private BootstrapLinkService bootstrapLinkServiceMock;
  private BootstrapConfigService bootstrapConfigServiceMock;
  private TranslationService translationServiceMock;
  private CountryService countryServiceMock;
  private FeaturesProperties featureProperties;
  private Config configMock;
  private BootstrapTranslationService bootstrapTranslationService;
  private BootstrapMetaService bootstrapMetaServiceMock;

  @Before
  public void prepareTests() throws IOException {
    bootstrapTranslationService = mock(BootstrapTranslationService.class);
    configMock = mock(Config.class);
    when(configMock.angularCmsEnabled(anyBoolean())).thenReturn(configMock);
    featureProperties = FeaturesProperties.builder().build();
    countryServiceMock = mock(CountryService.class);
    translationServiceMock = mock(TranslationService.class);
    when(translationServiceMock.getPlainJson(anyString())).thenReturn(new HashMap<>());
    bootstrapConfigServiceMock = mock(BootstrapConfigService.class);
    when(bootstrapConfigServiceMock.requestConfig()).thenReturn(configMock);
    when(bootstrapConfigServiceMock.requestFixedBootstrapConfig()).thenReturn(configMock);
    bootstrapLinkServiceMock = mock(BootstrapLinkService.class);
    when(bootstrapLinkServiceMock.getLinks(anyString(), anyBoolean(), anyMap()))
        .thenReturn(Collections.EMPTY_LIST);
    bootstrapMetaServiceMock = mock(BootstrapMetaService.class);
    Map<String, String> metaMock = new java.util.HashMap<>();
    metaMock.put("description", "TEST_META_DESCRIPTION");
    metaMock.put("title", "TEST_META_TITLE");
    metaMock.put("keywords", "TEST_META_KEYWORDS");
    when(bootstrapMetaServiceMock.getMetaData(anyString())).thenReturn(metaMock);

    bootstrapController =
        new BootstrapController(
            bootstrapConfigServiceMock,
            bootstrapLinkServiceMock,
            translationServiceMock,
            countryServiceMock,
            featureProperties,
            bootstrapTranslationService,
            bootstrapMetaServiceMock);
  }

  @Test
  public void testGetFixedLinks() {
    bootstrapController.getFixedLinks();
    verify(bootstrapLinkServiceMock).getOldLinks();
  }

  @Test
  public void testFixedConfig() {
    bootstrapController.getFixedConfig();
    verify(bootstrapConfigServiceMock).requestFixedConfig();
  }

  @Test
  public void testInitiateBootstrapFixturesTrue() {
    featureProperties.setBootstrapFixturesEnabled(true);
    bootstrapController.initiateBootstrap("at");
    verify(bootstrapConfigServiceMock).requestFixedBootstrapConfig();
    verify(bootstrapLinkServiceMock).getOldFixedLinks();
    verify(countryServiceMock).getAllforBootstrap(anyString());
  }

  @Test
  public void testInitiateBootstrapFixturesFalse() throws IOException {
    featureProperties.setBootstrapFixturesEnabled(false);
    featureProperties.setAngularCmsEnabled(true);
    featureProperties.setBootstrapFixturesEnabled(false);
    bootstrapController =
        new BootstrapController(
            bootstrapConfigServiceMock,
            bootstrapLinkServiceMock,
            translationServiceMock,
            countryServiceMock,
            featureProperties,
            bootstrapTranslationService,
            bootstrapMetaServiceMock);
    bootstrapController.initiateBootstrap("at");
    verify(countryServiceMock).getAllforBootstrap(anyString());
    verify(bootstrapConfigServiceMock).requestConfig();
  }
}
