package com.kiga.main.locale;

import static org.mockito.Mockito.mock;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by rainerh on 09.11.16.
 */
public class LocaleConverterTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Test
  public void constructorTest() {
    new LocaleConverter();
  }

  @Test
  public void toLocale() throws Exception {
    Assert.assertEquals(LocaleConverter.toLocale("de_DE"), Locale.de);
  }

  @Test
  public void toNullLocale() throws Exception {
    expectedException.expect(IllegalArgumentException.class);
    LocaleConverter.toLocale(null);
  }

  @Test
  public void toUknownLocale() throws Exception {
    expectedException.expect(IllegalArgumentException.class);
    LocaleConverter.toLocale("fr");
  }

  @Test
  public void fromLocale() throws Exception {
    Assert.assertEquals("de", LocaleConverter.toShort(Locale.de));
    Assert.assertEquals("de_DE", LocaleConverter.toLong(Locale.de));
  }

  @Test
  public void fromNullLocale() throws Exception {
    expectedException.expect(IllegalArgumentException.class);
    LocaleConverter.toShort(null);
  }

  @Test
  public void fromUnknownLocale() throws Exception {
    expectedException.expect(IllegalArgumentException.class);
    LocaleConverter.toShort(mock(Locale.class));
  }
}
