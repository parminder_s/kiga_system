package com.kiga.main.bootstrap;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiga.bootstrap.api.model.Config;
import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.RepositoryContexts;
import com.kiga.crm.service.HttpClientFactory;
import com.kiga.main.MainProperties;
import com.kiga.main.PermissionMode;
import com.kiga.main.bootstrap.services.BootstrapConfigService;
import com.kiga.web.service.GeoIpService;
import com.kiga.web.service.UrlGenerator;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BootstrapConfigServiceTest {
  private BootstrapConfigService bootstrapConfigService;

  private UrlGenerator urlGeneratorMock;
  private HttpClientFactory httpClientFactoryMock;
  private CloseableHttpClient closeableHttpClientMock;
  private CloseableHttpResponse closeableHttpResponseMock;
  private GeoIpService geoIpServiceMock;
  private RepositoryContext repositoryContextMock;
  private ObjectMapper objectMapper;
  private HttpEntity httpEntity;
  private MainProperties mainProperties;

  /** prepare tests. */
  @Before
  public void prepareTests() throws IOException {
    mainProperties = new MainProperties();
    mainProperties.setPermissionMode(PermissionMode.FullTest);
    repositoryContextMock = mock(RepositoryContext.class);
    RepositoryContexts repositoryContextsMock = RepositoryContexts.DRAFT;
    when(repositoryContextMock.getContext()).thenReturn(repositoryContextsMock);
    geoIpServiceMock = mock(GeoIpService.class);
    when(geoIpServiceMock.resolveCountryCode()).thenReturn("at");
    urlGeneratorMock = mock(UrlGenerator.class);
    when(urlGeneratorMock.getUrl(any(String.class)))
        .thenReturn("www.kigaportal.com/api/main/config");

    objectMapper = new ObjectMapper();
    httpClientFactoryMock = mock(HttpClientFactory.class);
    bootstrapConfigService =
        new BootstrapConfigService(
            urlGeneratorMock,
            geoIpServiceMock,
            repositoryContextMock,
            mainProperties,
            httpClientFactoryMock,
            objectMapper);

    closeableHttpClientMock = mock(CloseableHttpClient.class);
    bootstrapConfigService.setHttpClientFactory(httpClientFactoryMock);
    when(httpClientFactoryMock.getDefaultClient()).thenReturn(closeableHttpClientMock);

    closeableHttpResponseMock = mock(CloseableHttpResponse.class);
    when(closeableHttpClientMock.execute(any(HttpUriRequest.class)))
        .thenReturn(closeableHttpResponseMock);
    doNothing().when(closeableHttpResponseMock).close();

    httpEntity =
        EntityBuilder.create()
            .setText("{\"ngLiveUrl\":\"testValue\"}")
            .setContentType(ContentType.APPLICATION_JSON)
            .build();

    when(closeableHttpResponseMock.getEntity()).thenReturn(httpEntity);
  }

  /** test the config request to silverstripe. */
  @Test
  public void testRequestConfig() throws IOException {
    Config config = bootstrapConfigService.requestConfig();

    Assert.assertEquals("testValue", config.getNgLiveUrl());
    verify(urlGeneratorMock).getUrl(any(String.class));
    verify(httpClientFactoryMock).getDefaultClient();
    verify(closeableHttpClientMock).execute(any(HttpUriRequest.class));
    verify(closeableHttpResponseMock).getEntity();
  }

  @Test
  public void testRequestConfigEntityNull() throws IOException {
    when(closeableHttpResponseMock.getEntity()).thenReturn(null);
    Config config = bootstrapConfigService.requestConfig();

    Assert.assertNull(config);
  }

  @Test
  public void testRequestConfigCached() throws IOException {
    bootstrapConfigService.requestConfig();
    bootstrapConfigService.requestConfig();

    verify(urlGeneratorMock, times(1)).getUrl(anyString());
  }

  @Test
  public void testFixedBootstrapConfig() {
    Config config = bootstrapConfigService.requestFixedBootstrapConfig();

    Assert.assertEquals(false, config.getAllowCountrySelection());
    Assert.assertEquals(false, config.getFeedbackEnabled());
    Assert.assertEquals(false, config.getJsErrorLogEnabled());
    Assert.assertEquals(false, config.getRegShowInfo());
    Assert.assertEquals(false, config.getShowVersion());
    Assert.assertEquals(true, config.getShowMemberIdeas());
    Assert.assertEquals("at", config.getCurrentCountry());
    Assert.assertEquals(Arrays.asList("en", "de", "it", "tr"), config.getLocales());
    Assert.assertEquals(RepositoryContexts.DRAFT.name(), config.getCurrentRepositoryContext());
    Assert.assertEquals(Arrays.asList("LIVE", "DRAFT"), config.getRepositoryContexts());
    Assert.assertEquals(PermissionMode.FullTest.name(), config.getPermissionMode());
  }

  @Test
  public void testFixedConfig() {
    @SuppressWarnings("unchecked")
    Map<String, Object> config = bootstrapConfigService.requestFixedConfig();

    Assert.assertEquals(false, config.get("allowCountrySelection"));
    Assert.assertEquals(false, config.get("feedbackEnabled"));
    Assert.assertEquals(false, config.get("jsErrorLogEnabled"));
    Assert.assertEquals(false, config.get("regShowInfo"));
    Assert.assertEquals(false, config.get("showVersion"));
    Assert.assertEquals(true, config.get("showMemberIdeas"));
    Assert.assertEquals("at", config.get("currentCountry"));
    Assert.assertEquals(Arrays.asList("en", "de", "it", "tr"), config.get("locales"));
    Assert.assertEquals(RepositoryContexts.DRAFT.name(), config.get("currentRepositoryContext"));
    Assert.assertEquals(Arrays.asList("LIVE", "DRAFT"), config.get("repositoryContexts"));
    Assert.assertEquals(PermissionMode.FullTest.name(), config.get("permissionMode"));
  }
}
