package com.kiga;

import static com.kiga.util.NowService.KIGAZONE;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class DateTypesProvider {
  /** returns an Instant (UTC) from a year, month and day. */
  public static Instant getInstant(int year, int month, int dayOfMonth) {
    return LocalDate.of(year, month, dayOfMonth).atStartOfDay().toInstant(ZoneOffset.UTC);
  }

  public static ZonedDateTime getZoned(int year, int month, int dayOfMonth) {
    return getInstant(year, month, dayOfMonth).atZone(KIGAZONE);
  }
}
