package com.kiga.kigaupload.processors;

import org.junit.Test;

/**
 * @author bbs
 * @since 1/22/17.
 */
public class DummyPreUploadProcessorTest {
  @Test
  public void testProcess() {
    new DummyPreUploadProcessor().process(null, null);
  }
}
