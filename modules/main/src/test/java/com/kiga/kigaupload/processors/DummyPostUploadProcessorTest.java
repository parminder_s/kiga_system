package com.kiga.kigaupload.processors;

import org.junit.Test;

/**
 * @author bbs
 * @since 1/22/17.
 */
public class DummyPostUploadProcessorTest {
  @Test
  public void testProcess() {
    new DummyPostUploadProcessor().process(null, null, null);
  }
}
