package com.kiga.kigaupload.web.response;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanEquals;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanHashCode;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanToString;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.AllOf.allOf;

import org.junit.Test;

/**
 * @author bbs
 * @since 1/15/17.
 */
public class UploadResponseTest {
  @Test
  public void testProperties() {
    assertThat(UploadResponse.class,
      allOf(hasValidGettersAndSetters(), hasValidBeanEquals(), hasValidBeanHashCode(),
        hasValidBeanToString()));
  }
}
