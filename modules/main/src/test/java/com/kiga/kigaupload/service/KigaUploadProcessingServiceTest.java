package com.kiga.kigaupload.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.kigaupload.processors.PostUploadProcessor;
import com.kiga.kigaupload.processors.PreUploadProcessor;
import com.kiga.security.domain.Member;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

/**
 * @author bbs
 * @since 3/5/17.
 */
public class KigaUploadProcessingServiceTest {
  public static final String BLANK_CONTEXT = "";
  private static final String CONTEXT = "test";
  private UploadProcessorLocator uploadProcessorLocatorMock = mock(UploadProcessorLocator.class);
  private KigaUploadProcessingService kigaUploadProcessingService;

  @Before
  public void beforeTests() {
    kigaUploadProcessingService = new KigaUploadProcessingService(uploadProcessorLocatorMock);
  }

  @Test
  public void testPreProcess() {
    Member memberMock = mock(Member.class);
    PreUploadProcessor preUploadProcessorMock = mock(PreUploadProcessor.class);
    when(uploadProcessorLocatorMock.lookupProcessors(CONTEXT, PreUploadProcessor.class)).thenReturn(
      Arrays.asList(preUploadProcessorMock, preUploadProcessorMock, preUploadProcessorMock));
    List<MultipartFile> multipartFiles = Arrays
      .asList(mock(MultipartFile.class), mock(MultipartFile.class));
    kigaUploadProcessingService.preProcess(multipartFiles, CONTEXT, memberMock);
    verify(preUploadProcessorMock, times(3)).process(memberMock, multipartFiles);
  }

  @Test
  public void testPreProcessBlankContext() {
    Member memberMock = mock(Member.class);
    PreUploadProcessor preUploadProcessorMock = mock(PreUploadProcessor.class);
    when(uploadProcessorLocatorMock.lookupProcessors(BLANK_CONTEXT, PreUploadProcessor.class))
      .thenReturn(
        Arrays.asList(preUploadProcessorMock, preUploadProcessorMock, preUploadProcessorMock));
    List<MultipartFile> multipartFiles = Arrays
      .asList(mock(MultipartFile.class), mock(MultipartFile.class));
    kigaUploadProcessingService.preProcess(multipartFiles, BLANK_CONTEXT, memberMock);
    verify(preUploadProcessorMock, never()).process(memberMock, multipartFiles);
  }

  @Test
  public void testPostProcess() {
    Member memberMock = mock(Member.class);
    PostUploadProcessor postUploadProcessorMock = mock(PostUploadProcessor.class);
    when(uploadProcessorLocatorMock.lookupProcessors(CONTEXT, PostUploadProcessor.class))
      .thenReturn(
        Arrays.asList(postUploadProcessorMock, postUploadProcessorMock, postUploadProcessorMock));
    List<MultipartFile> multipartFiles = Arrays
      .asList(mock(MultipartFile.class), mock(MultipartFile.class));
    kigaUploadProcessingService.postProcess(multipartFiles, null, CONTEXT, memberMock);
    verify(postUploadProcessorMock, times(3)).process(eq(memberMock), eq(multipartFiles), any());
  }

  @Test
  public void testPostProcessBlankContext() {
    Member memberMock = mock(Member.class);
    PostUploadProcessor postUploadProcessorMock = mock(PostUploadProcessor.class);
    when(uploadProcessorLocatorMock.lookupProcessors(BLANK_CONTEXT, PostUploadProcessor.class))
      .thenReturn(
        Arrays.asList(postUploadProcessorMock, postUploadProcessorMock, postUploadProcessorMock));
    List<MultipartFile> multipartFiles = Arrays
      .asList(mock(MultipartFile.class), mock(MultipartFile.class));
    kigaUploadProcessingService.postProcess(multipartFiles, null, BLANK_CONTEXT, memberMock);
    verify(postUploadProcessorMock, never()).process(eq(memberMock), eq(multipartFiles), any());
  }
}
