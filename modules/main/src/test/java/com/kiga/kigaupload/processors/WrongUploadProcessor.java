package com.kiga.kigaupload.processors;

import com.kiga.kigaupload.annotation.UploadProcessor;

/**
 * @author bbs
 * @since 1/28/17.
 */
@UploadProcessor(context = "wrong")
public interface WrongUploadProcessor extends PreUploadProcessor {
}
