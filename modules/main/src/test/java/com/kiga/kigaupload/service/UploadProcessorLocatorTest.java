package com.kiga.kigaupload.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.kigaupload.annotation.UploadProcessor;
import com.kiga.util.AnnotatedClassFinder;
import com.kiga.util.InstanceProvider;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author bbs
 * @since 3/5/17.
 */
public class UploadProcessorLocatorTest {
  private AnnotatedClassFinder annotatedClassFinderMock = mock(AnnotatedClassFinder.class);
  private InstanceProvider instanceProviderMock = mock(InstanceProvider.class);
  private UploadProcessorLocator uploadProcessorLocator;

  /**
   * Prepare tests.
   */
  @Before
  public void beforeTests() {
    uploadProcessorLocator = new UploadProcessorLocator(annotatedClassFinderMock,
      instanceProviderMock);
    when(instanceProviderMock.getInstance(MockContextProcessor.class))
      .thenReturn(new MockContextProcessor());
    when(instanceProviderMock.getInstance(OtherContextProcessor.class))
      .thenReturn(new OtherContextProcessor());
  }

  @Test
  public void testGetLookupProcessors() {
    when(annotatedClassFinderMock.lookup(UploadProcessor.class, Processor.class))
      .thenReturn(Arrays.asList(MockContextProcessor.class, OtherContextProcessor.class));
    List<Processor> processors = uploadProcessorLocator
      .lookupProcessors("mock", Processor.class);
    assertEquals(1, processors.size());
    assertSame(MockContextProcessor.class, processors.get(0).getClass());
  }

  interface Processor {

  }

  @UploadProcessor(context = "mock")
  private static class MockContextProcessor implements Processor {

  }

  @UploadProcessor(context = "other")
  private static class OtherContextProcessor implements Processor {

  }
}
