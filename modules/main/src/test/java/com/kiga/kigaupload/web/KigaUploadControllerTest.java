package com.kiga.kigaupload.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.kiga.kigaupload.exception.ContentTypeMismatchException;
import com.kiga.kigaupload.exception.EmptyMultipartFilesListException;
import com.kiga.kigaupload.exception.KigaUploadException;
import com.kiga.kigaupload.service.KigaUploadService;
import com.kiga.kigaupload.web.response.UploadFileStatus;
import com.kiga.kigaupload.web.response.UploadResponse;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import com.kiga.upload.converter.MultipartFileUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author bbs
 * @since 1/15/17.
 */
public class KigaUploadControllerTest {
  private KigaUploadService kigaUploadServiceMock = mock(KigaUploadService.class);
  private SecurityService securityServiceMock = mock(SecurityService.class);
  private Member memberMock = mock(Member.class);
  private KigaUploadController kigaUploadController;
  private MultipartFileUtil multipartFileUtilMock = mock(MultipartFileUtil.class);

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    kigaUploadController = new KigaUploadController(kigaUploadServiceMock, securityServiceMock,
      multipartFileUtilMock);
    when(securityServiceMock.getSessionMember()).thenReturn(Optional.of(memberMock));
  }

  @Test(expected = ContentTypeMismatchException.class)
  public void testUploadNotAcceptedTypes()
    throws KigaUploadException, ContentTypeMismatchException {
    UploadFileStatus uploadFileStatus = mock(UploadFileStatus.class);
    MultipartFile multipartFileMock = mock(MultipartFile.class);
    List<MultipartFile> multipartFiles = Collections.singletonList(multipartFileMock);

    String[] contentTypes = new String[]{"pdf"};

    kigaUploadController.push(contentTypes, null, multipartFiles, "PublicRead");
  }

  @Test
  public void testUploadAcceptedTypes()
    throws Exception {
    UploadFileStatus uploadFileStatus = mock(UploadFileStatus.class);
    MultipartFile multipartFileMock = mock(MultipartFile.class);
    List<MultipartFile> multipartFiles = Collections.singletonList(multipartFileMock);
    when(multipartFileUtilMock.getContentType(multipartFileMock)).thenReturn("application/pdf");

    String[] contentTypes = new String[]{"pdf"};

    kigaUploadController.push(contentTypes, null, multipartFiles, "PublicRead");
  }

  @Test
  public void testUpload() throws KigaUploadException, ContentTypeMismatchException {
    UploadFileStatus uploadFileStatus = mock(UploadFileStatus.class);
    MultipartFile multipartFileMock = mock(MultipartFile.class);
    List<MultipartFile> multipartFiles = Collections.singletonList(multipartFileMock);
    when(kigaUploadServiceMock
      .uploadAndProcess(multipartFiles, null, CannedAccessControlList.PublicRead, memberMock))
      .thenReturn(Collections.singletonList(uploadFileStatus));

    UploadResponse uploadResponse =
      kigaUploadController.push(null, null, multipartFiles, "PublicRead");

    assertEquals(1, uploadResponse.getUploadedFiles().size());
    assertSame(uploadFileStatus, uploadResponse.getUploadedFiles().get(0));

    verify(securityServiceMock).requiresFullSubscription();
  }

  @Test
  public void testUploadEmptyCannedAccessControlList()
    throws KigaUploadException, ContentTypeMismatchException {
    UploadFileStatus uploadFileStatus = mock(UploadFileStatus.class);
    MultipartFile multipartFileMock = mock(MultipartFile.class);
    List<MultipartFile> multipartFiles = Collections.singletonList(multipartFileMock);
    when(kigaUploadServiceMock
      .uploadAndProcess(multipartFiles, null, CannedAccessControlList.Private, memberMock))
      .thenReturn(Collections.singletonList(uploadFileStatus));

    UploadResponse uploadResponse =
      kigaUploadController.push(null, null, multipartFiles, "");

    assertEquals(1, uploadResponse.getUploadedFiles().size());
    assertSame(uploadFileStatus, uploadResponse.getUploadedFiles().get(0));

    verify(securityServiceMock).requiresFullSubscription();
  }

  @Test
  public void testUploadNullCannedAccessControlList()
    throws KigaUploadException, ContentTypeMismatchException {
    UploadFileStatus uploadFileStatus = mock(UploadFileStatus.class);
    MultipartFile multipartFileMock = mock(MultipartFile.class);
    List<MultipartFile> multipartFiles = Collections.singletonList(multipartFileMock);
    when(kigaUploadServiceMock
      .uploadAndProcess(multipartFiles, null, CannedAccessControlList.Private, memberMock))
      .thenReturn(Collections.singletonList(uploadFileStatus));

    UploadResponse uploadResponse =
      kigaUploadController.push(null, null, multipartFiles, null);

    assertEquals(1, uploadResponse.getUploadedFiles().size());
    assertSame(uploadFileStatus, uploadResponse.getUploadedFiles().get(0));

    verify(securityServiceMock).requiresFullSubscription();
  }

  @Test
  public void testUploadWithoutContext() throws KigaUploadException,
    IllegalAccessException, InstantiationException, ContentTypeMismatchException {
    MultipartFile multipartFileMock = mock(MultipartFile.class);
    List<MultipartFile> multipartFileMocksList = Collections.singletonList(multipartFileMock);
    kigaUploadController.push(null, null, multipartFileMocksList, "PublicRead");
  }

  @Test(expected = EmptyMultipartFilesListException.class)
  public void testEmptyFilesList() throws KigaUploadException, ContentTypeMismatchException {
    kigaUploadController.push(null, null, Collections.emptyList(), "PublicRead");
  }

  @Test(expected = EmptyMultipartFilesListException.class)
  public void testNullFilesList()
    throws EmptyMultipartFilesListException, ContentTypeMismatchException {
    kigaUploadController.push(null, null, null, "PublicRead");
  }

  @Test
  public void testKigaUploadServiceReturningNull()
    throws EmptyMultipartFilesListException, ContentTypeMismatchException {
    UploadFileStatus uploadFileStatusMock = mock(UploadFileStatus.class);
    when(kigaUploadServiceMock.uploadAndProcess(any(), any(), any(), any()))
      .thenReturn(Collections.singletonList(uploadFileStatusMock));
    MultipartFile multipartFileMock = mock(MultipartFile.class);
    UploadResponse uploadResponse = kigaUploadController
      .push(null, null, Collections.singletonList(multipartFileMock), "PublicRead");
    assertNotNull(uploadResponse);
    assertTrue(CollectionUtils.isNotEmpty(uploadResponse.getUploadedFiles()));
  }
}
