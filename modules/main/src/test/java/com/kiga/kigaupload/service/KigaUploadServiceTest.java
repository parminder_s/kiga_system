package com.kiga.kigaupload.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.kiga.kigaupload.web.request.UniqueNameMultipartFileWrapper;
import com.kiga.kigaupload.web.response.UploadFileStatus;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.security.domain.Member;
import com.kiga.upload.UploadParameters;
import com.kiga.upload.factory.AbstractFileUploader;
import com.kiga.upload.factory.UploadFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.modelmapper.ModelMapper;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

/**
 * @author bbs
 * @since 1/19/17.
 */
public class KigaUploadServiceTest {
  private Member memberMock = mock(Member.class);
  private UploadFactory uploadFactoryMock = mock(UploadFactory.class);
  private KigaUploadProcessingService kigaUploadProcessingServiceMock = mock(
    KigaUploadProcessingService.class);
  private KigaUploadService kigaUploadService;
  private AbstractFileUploader abstractFileUploaderMock = mock(AbstractFileUploader.class);
  private AbstractFileUploader failingAbstractFileUploaderMock = mock(AbstractFileUploader.class);
  private MultipartFile multipartFile1 = mock(MultipartFile.class);
  private MultipartFile multipartFile2 = mock(MultipartFile.class);
  private MultipartFile emptyMultipartFile = mock(MultipartFile.class);

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() throws Exception {
    kigaUploadService =
      new KigaUploadService(new ModelMapper(), uploadFactoryMock, kigaUploadProcessingServiceMock);
    when(uploadFactoryMock.getUploaderInstance(any(), any()))
      .thenReturn(abstractFileUploaderMock).thenReturn(failingAbstractFileUploaderMock);
    when(emptyMultipartFile.isEmpty()).thenReturn(true);

    when(abstractFileUploaderMock.getUploadParameters()).thenReturn(new UploadParameters());
    when(failingAbstractFileUploaderMock.getUploadParameters()).thenReturn(new UploadParameters());

    KigaS3File kigaS3File = new KigaS3File();
    kigaS3File.setId(1L);
    when(abstractFileUploaderMock.upload()).thenReturn(kigaS3File);

    when(failingAbstractFileUploaderMock.upload()).thenReturn(null);
  }

  @Test
  public void testUploadAndProcess() {
    List<MultipartFile> multipartFiles = Arrays
      .asList(multipartFile1, multipartFile2, emptyMultipartFile);
    List<UploadFileStatus> uploadFileStatuses = kigaUploadService
      .uploadAndProcess(multipartFiles, "context", CannedAccessControlList.PublicRead, memberMock);

    assertEquals(multipartFiles.size(), uploadFileStatuses.size());
    assertFalse(uploadFileStatuses.get(0).isFailed());
    assertTrue(uploadFileStatuses.get(1).isFailed());
    assertTrue(uploadFileStatuses.get(2).isFailed());

    ArgumentCaptor<List> uniqueNameMultipartFileArgumentCaptor =
      ArgumentCaptor.forClass(List.class);
    verify(kigaUploadProcessingServiceMock)
      .preProcess(uniqueNameMultipartFileArgumentCaptor.capture(), eq("context"), eq(memberMock));
    List<UniqueNameMultipartFileWrapper> uniqueNameMultipartFileWrappers =
      uniqueNameMultipartFileArgumentCaptor.getValue();
    assertEquals(3, uniqueNameMultipartFileWrappers.size());
    assertEquals(multipartFile1, uniqueNameMultipartFileWrappers.get(0).getOriginalMultipartFile());
    assertEquals(multipartFile2, uniqueNameMultipartFileWrappers.get(1).getOriginalMultipartFile());
    assertEquals(emptyMultipartFile,
      uniqueNameMultipartFileWrappers.get(2).getOriginalMultipartFile());

    verify(kigaUploadProcessingServiceMock)
      .postProcess(uniqueNameMultipartFileArgumentCaptor.capture(), any(), eq("context"),
        eq(memberMock));
    uniqueNameMultipartFileWrappers = uniqueNameMultipartFileArgumentCaptor.getValue();
    assertEquals(3, uniqueNameMultipartFileWrappers.size());
    assertEquals(multipartFile1, uniqueNameMultipartFileWrappers.get(0).getOriginalMultipartFile());
    assertEquals(multipartFile2, uniqueNameMultipartFileWrappers.get(1).getOriginalMultipartFile());
    assertEquals(emptyMultipartFile,
      uniqueNameMultipartFileWrappers.get(2).getOriginalMultipartFile());
  }
}
