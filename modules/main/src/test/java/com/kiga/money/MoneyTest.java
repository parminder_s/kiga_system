package com.kiga.money;

import static com.kiga.money.MoneyBuilder.createMoney;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

public class MoneyTest {
  @Rule
  public final ExpectedException exception = ExpectedException.none();

  @Test
  public void testNormal() {
    Money money = new Money("15.25", "EUR");
    assertThat(money, allOf(
      hasProperty("amount", equalTo(new BigDecimal("15.25"))),
      hasProperty("currency", equalTo("EUR"))
    ));
  }

  @Test
  public void testRounding() {
    assertEquals(new BigDecimal("10.00"), createMoney("9.999").getAmount());
  }

  @Test
  public void testScale() {
    assertEquals(new BigDecimal("1.00"), createMoney("1.0").getAmount());
  }

  @Test
  public void testInvalid() {
    exception.expect(NumberFormatException.class);
    createMoney("12,23");
  }
}
