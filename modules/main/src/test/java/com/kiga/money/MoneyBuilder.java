package com.kiga.money;

public class MoneyBuilder {
  public static Money createMoney(String amount) {
    return new Money(amount, "EUR");
  }
}
