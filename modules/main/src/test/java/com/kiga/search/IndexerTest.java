package com.kiga.search;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaGroup;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.ideas.web.service.IdeasResponseIdeaConverter;
import com.kiga.search.models.KigaGroupSearchable;
import com.kiga.search.models.KigaIdeaSearchable;
import com.kiga.search.repositories.ElasticIdeaRepository;
import com.kiga.search.repositories.KigaGroupSearchableRepository;
import com.kiga.search.services.DefaultIndexer;
import com.kiga.search.services.GroupHierarchy;
import com.kiga.search.services.IdeaHierarchy;
import com.kiga.search.services.SiteTreeTraversalService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import java.util.ArrayList;

/**
 * Created by faxxe on 8/21/16.
 */
public class IndexerTest {


  DefaultIndexer jdefaultIndexer;
  String indexName = "kiga";
  private KigaIdeaRepositoryLive kigaIdeaRepositoryLive;
  private ElasticIdeaRepository elasticIdeaRepository;
  private KigaGroupSearchableRepository jkigaGroupSearchableRepository;
  private SiteTreeTraversalService siteTreeTraversalService;
  private IdeasResponseIdeaConverter responseIdeaConverter;
  private ElasticsearchTemplate elasticsearchTemplate;
  private IdeaSearchableConverter ideaSearchableConverter;

  /**
   * setup Test.
   */
  @Before
  public void setup() {
    this.kigaIdeaRepositoryLive = mock(KigaIdeaRepositoryLive.class);
    this.elasticIdeaRepository = mock(ElasticIdeaRepository.class);
    this.siteTreeTraversalService = mock(SiteTreeTraversalService.class);
    this.responseIdeaConverter = mock(IdeasResponseIdeaConverter.class);
    this.jkigaGroupSearchableRepository = mock(KigaGroupSearchableRepository.class);
    this.elasticsearchTemplate = mock(ElasticsearchTemplate.class);
    this.ideaSearchableConverter = mock(IdeaSearchableConverter.class);

    jdefaultIndexer = new DefaultIndexer(kigaIdeaRepositoryLive, elasticIdeaRepository,
      siteTreeTraversalService, responseIdeaConverter, jkigaGroupSearchableRepository,
      elasticsearchTemplate, ideaSearchableConverter, "kiga");
  }


  @Test
  public void deleteFromIndexTest() {
    jdefaultIndexer.deleteIdeaFromIndex(0L);
    verify(elasticIdeaRepository, times(1)).delete(0L);
  }

  @Test
  public void createExistingIndexTest() {
    when(elasticsearchTemplate.indexExists(indexName)).thenReturn(true);
    jdefaultIndexer.createIndex(indexName);
    verify(elasticsearchTemplate, times(1)).indexExists(indexName);
    verify(elasticsearchTemplate, never()).createIndex(anyString(), any());

  }

  @Test
  public void createIndexTest() {
    when(elasticsearchTemplate.indexExists(indexName)).thenReturn(false);
    jdefaultIndexer.createIndex(indexName);
    verify(elasticsearchTemplate, times(1)).indexExists(indexName);
    verify(elasticsearchTemplate, times(1)).createIndex(anyString(), any());

  }

  @Test
  public void resetIndexTest() {
    jdefaultIndexer.resetIndex(indexName);
    verify(elasticsearchTemplate, times(1)).deleteIndex(indexName);
    when(elasticsearchTemplate.indexExists(indexName)).thenReturn(true);

    verify(elasticsearchTemplate, times(1)).indexExists(indexName);
    verify(elasticsearchTemplate, times(1)).createIndex(anyString(), any());
  }

  @Test
  public void indexIdeasTest() {
    when(kigaIdeaRepositoryLive.findAll()).thenReturn(new ArrayList<KigaIdeaLive>());
    when(kigaIdeaRepositoryLive.findAll(anyCollection())).thenReturn(new ArrayList<KigaIdeaLive>());
    jdefaultIndexer.indexIdeas(null);
    jdefaultIndexer.indexIdeas(new ArrayList<>());
  }

  @Test
  public void indexIdeaTest() {
    IdeaHierarchy ideaHierarchy = new IdeaHierarchy();
    ideaHierarchy.setIdeaGroups(new ArrayList<>());
    when(siteTreeTraversalService.getIdeaHiearchy(any())).thenReturn(ideaHierarchy);

    IdeasViewModelIdea ideasViewModelIdea = new IdeasViewModelIdea();

    when(responseIdeaConverter.convertToResponse(any(KigaIdea.class)))
      .thenReturn(ideasViewModelIdea);

    KigaIdeaLive kigaIdea = new KigaIdeaLive();
    jdefaultIndexer.indexIdea(kigaIdea);
    verify(elasticIdeaRepository, times(1)).save(any(KigaIdeaSearchable.class));

  }

  @Test
  public void indexGroupTest() {
    GroupHierarchy groupHierarchy = new GroupHierarchy();
    IdeaGroup ideaGroup = mock(IdeaGroupLive.class);

    when(siteTreeTraversalService.getGroupHierarchy(any())).thenReturn(groupHierarchy);
    jdefaultIndexer.indexGroup(ideaGroup);
    verify(jkigaGroupSearchableRepository, times(1)).save(any(KigaGroupSearchable.class));
  }

}
