package com.kiga.search;

import static org.junit.Assert.assertNotNull;


import com.kiga.ideas.IdeasProperties;
import com.kiga.search.converter.SearchResponseConverter;
import com.kiga.search.job.CreateIndexJob;
import com.kiga.search.job.UpdateIndexJob;
import com.kiga.search.services.DefaultIndexer;
import com.kiga.search.services.ElasticGroupIndexer;
import com.kiga.search.services.GroupService;
import com.kiga.search.services.IndexerService;
import com.kiga.search.services.SearchService;
import com.kiga.search.services.SearchTermLogService;
import com.kiga.search.services.SiteTreeTraversalService;
import org.elasticsearch.client.Client;
import org.elasticsearch.node.NodeBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

/**
 * Created by faxxe on 11/16/16.
 */
public class SearcherConfigTest {

  SearcherConfig sc = new SearcherConfig();

  /**
   * comment.
   */
  @Before
  public void setUp() {
    SearchProperties searchProperties = new SearchProperties();
    sc.setSearchProperties(searchProperties);
    searchProperties.setHost("0.0.0.0");
    searchProperties.setPort(0);

  }

  @Test
  public void searchServiceConfigTest() {
    SearchService searchService =  sc.searchService(null, null, null, null, null);
    assertNotNull(searchService);
  }

  @Test
  public void searchTermLogServiceTest() {
    SearchTermLogService logService = sc.serchTermLogService(null);
    assertNotNull(logService);
  }

  @Test
  public void elasticgroupIndexer() {
    ElasticGroupIndexer elasticGroupIndexer = sc.elasticGroupIndexer(null, null);
    assertNotNull(elasticGroupIndexer);
  }

  @Test
  public void indexerTest() {
    DefaultIndexer indexer = sc.jdefaultIndexer(null, null, null, null, null, null, null);
    assertNotNull(indexer);
  }

  @Test
  public void searcherTest() {
    DefaultSearcher searcher = sc.searcher(null, null);
    assertNotNull(searcher);
  }

  @Test
  public void elasticSearchTemplateTest() {
    ElasticsearchTemplate est = sc.elasticsearchTemplate(null);
    assertNotNull(est);
    ElasticsearchOperations elasticsearchOperations = sc.elasticsearchOperations(null);
    assertNotNull(elasticsearchOperations);
  }

  @Test
  public void siteTraversalServiceTest() {
    SiteTreeTraversalService stts = sc.ideaTraversalService(null);
    assertNotNull(stts);
  }

  @Test
  public void searchResponseConverterTest() {
    IdeasProperties ideasProperties = new IdeasProperties();
    ideasProperties.setAmountPerCategory(14);
    SearchResponseConverter converter = sc.searchResponseConverter(null, null, ideasProperties);
    assertNotNull(converter);
    //    sc..setSearchOrderGroupsBy("ELASTICSEARCH");
    converter = sc.searchResponseConverter(null, null, ideasProperties);
    assertNotNull(converter);
  }

  @Test
  public void ideaSearchableConverterTest() {
    IdeaSearchableConverter converter = sc.ideaSearchableConverter();
    assertNotNull(converter);
  }

  @Test
  public void nodeBuilderTest() {
    NodeBuilder nodeBuilder = sc.nodeBuilder();
    assertNotNull(nodeBuilder);
  }

  @Test
  public void elasticsearchOperationsTest() {
    ElasticsearchOperations eo = sc.elasticsearchOperations(null);
    assertNotNull(eo);
  }

  @Test
  public void elasticClientTest() {
    Client client = sc.client();
    assertNotNull(client);
  }

  @Test
  public void groupServiceTest() {
    GroupService groupService = sc.groupService();
    assertNotNull(groupService);
  }

  @Test
  public void updateJobTest() {
    UpdateIndexJob updateIndexJob = sc.updateIndexJob(null);
    assertNotNull(updateIndexJob);
  }

  @Test
  public void createIndexJobTest() {
    CreateIndexJob createIndexJob = sc.createIndexJob(null);
    assertNotNull(createIndexJob);
  }

}
