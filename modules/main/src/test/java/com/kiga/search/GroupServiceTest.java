package com.kiga.search;

import com.kiga.search.models.KigaGroupSearchable;
import com.kiga.search.models.KigaIdeaSearchable;
import com.kiga.search.services.GroupService;
import org.junit.Assert;
import org.junit.Test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by faxxe on 8/21/16.
 */
public class GroupServiceTest {

  @Test
  public void getGrouphierarchyTest() {
    KigaGroupSearchable gs = new KigaGroupSearchable();
    gs.setPathIds(Arrays.asList(1L, 2L, 3L, 4L));
    gs.setPath(Arrays.asList("ans", "zwa", "drei", "vier"));

    GroupService groupService = new GroupService();
    List<KigaGroupSearchable> groups = groupService.groupGroups(Collections.singletonList(gs), 2L);

    KigaGroupSearchable groupSearchable = groups.get(0);
    Assert.assertEquals(3L, (long)groupSearchable.getId());
    Assert.assertEquals("drei", groupSearchable.getTitle());

  }

  @Test
  public void addIdeasToGroupTest() {
    KigaGroupSearchable gs1 = new KigaGroupSearchable();
    gs1.setId(1L);
    gs1.setKigaIdeas(new ArrayList<>());
    KigaGroupSearchable gs2 = new KigaGroupSearchable();
    gs2.setId(2L);
    gs2.setKigaIdeas(new ArrayList<>());
    KigaGroupSearchable gs3 = new KigaGroupSearchable();
    gs3.setId(3L);
    gs3.setKigaIdeas(new ArrayList<>());
    KigaGroupSearchable gs4 = new KigaGroupSearchable();
    gs4.setId(4L);
    gs4.setKigaIdeas(new ArrayList<>());

    KigaIdeaSearchable is1 = new KigaIdeaSearchable();
    is1.setId(1L);
    is1.setHierarchyIds(Arrays.asList(2L, 3L, 4L));
    KigaIdeaSearchable is2 = new KigaIdeaSearchable();
    is2.setId(2L);
    is2.setHierarchyIds(Arrays.asList(1L, 3L, 4L));
    KigaIdeaSearchable is3 = new KigaIdeaSearchable();
    is3.setId(3L);
    is3.setHierarchyIds(Arrays.asList(1L, 2L, 4L));
    KigaIdeaSearchable is4 = new KigaIdeaSearchable();
    is4.setId(4L);
    is4.setHierarchyIds(Arrays.asList(1L, 2L, 3L));

    List<KigaGroupSearchable> groups = Arrays.asList(gs1, gs2, gs3, gs4);
    List<KigaIdeaSearchable> ideas = Arrays.asList(is1, is2, is3, is4);

    GroupService groupService = new GroupService();
    groupService.addIdeasToGroups(groups, ideas);

    for (KigaGroupSearchable group: groups) {
      Assert.assertEquals(3, group.getKigaIdeas().size());
    }
  }

}
