package com.kiga.search;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import com.kiga.main.locale.Locale;
import com.kiga.search.converter.SearchResponseConverter;
import com.kiga.search.models.ElasticGroupSearchable;
import com.kiga.search.repositories.ElasticGroupRepository;
import com.kiga.search.requests.SearchRequest;
import com.kiga.search.services.DefaultSearchService;
import com.kiga.search.services.GroupService;
import com.kiga.search.services.SearchTermLogService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.elasticsearch.core.query.SearchQuery;


import java.util.stream.Stream;

import javax.persistence.EntityNotFoundException;

/**
 * Created by faxxe on 8/21/16.
 */
public class SearchServiceTest {

  private DefaultSearchService defaultSearchService;
  private DefaultSearcher searcher;
  private GroupService groupService;
  private ElasticGroupRepository elasticGroupRepository;
  private SearchTermLogService searchTermLogService;
  private SearchResponseConverter searchResponseConverter;

  /**
   * setup Test.
   */
  @Before
  public void setup() {
    this.searcher = mock(DefaultSearcher.class);
    this.groupService = mock(GroupService.class);
    this.elasticGroupRepository = mock(ElasticGroupRepository.class);
    this.searchTermLogService = mock(SearchTermLogService.class);
    this.searchResponseConverter = mock(SearchResponseConverter.class);

    defaultSearchService = new DefaultSearchService(searcher, groupService,
      elasticGroupRepository, searchTermLogService, searchResponseConverter, null);
  }

  @Test
  public void explainTest() {
    String explaination = defaultSearchService.explain(null, null, null);
    verify(searcher, times(1)).explainSync(null, null, null);
    Assert.assertNull(explaination);
  }

  @Test
  public void doEmptySearchTest() {
    SearchRequest searchRequest = new SearchRequest();
    searchRequest.setQuery(new String[0]);
    SearchResult searchResult = defaultSearchService.doSearch(searchRequest);

    Assert.assertEquals(searchResult, null);

  }

  @Test
  public void doSearchTest() {
    SearchRequest searchRequest = new SearchRequest();
    String[] query = {"igel"};
    searchRequest.setAgeGroup(0L);
    searchRequest.setParentId(3L);
    searchRequest.setQuery(query);
    searchRequest.setLocale(Locale.de);

    SearchQuery searchQuery = mock(SearchQuery.class);

    when(searcher.buildSearchQuery(any(), any())).thenReturn(searchQuery);

    ElasticGroupSearchable elasticGroupSearchable = new ElasticGroupSearchable();
    elasticGroupSearchable.setId(3L);
    elasticGroupSearchable.setClassName("IdeaGroupMainContainer");
    elasticGroupSearchable.setLanguageCode(Locale.de.toString());

    when(elasticGroupRepository.findOne(anyLong())).thenReturn(elasticGroupSearchable);

    SearchResult searchResult = new SearchResult();
    when(searcher.searchElastic(searchQuery)).thenReturn(searchResult);
    SearchResult result = defaultSearchService.doSearch(searchRequest);

    verify(searcher, times(1)).buildSearchQuery(any(), any());
    verify(searcher, times(1)).searchElastic(any());

    Assert.assertEquals(searchResult, result);

  }

  @Test
  public void doSearchLoopTest() {
    Stream.of("IdeaGroup", "IdeaGroupContainer", "IdeaGroupCategory", "IdeaGroupMainContainer")
      .forEach(this::doSearchNonRootTest);
  }

  private void doSearchNonRootTest(String className) {
    SearchRequest searchRequest = new SearchRequest();
    String[] query = {"igel"};
    searchRequest.setAgeGroup(0L);
    searchRequest.setParentId(3L);
    searchRequest.setQuery(query);
    searchRequest.setLocale(Locale.de);

    SearchQuery searchQuery = mock(SearchQuery.class);

    when(searcher.buildSearchQuery(any(), any())).thenReturn(searchQuery);

    ElasticGroupSearchable elasticGroupSearchable = new ElasticGroupSearchable();
    elasticGroupSearchable.setId(3L);
    elasticGroupSearchable.setClassName(className);
    elasticGroupSearchable.setLanguageCode(Locale.de.toString());

    when(elasticGroupRepository.findOne(anyLong())).thenReturn(elasticGroupSearchable);

    SearchResult searchResult = new SearchResult();
    when(searcher.searchElastic(searchQuery)).thenReturn(searchResult);
    SearchResult result = defaultSearchService.doSearch(searchRequest);

    verify(searcher, atLeastOnce()).buildSearchQuery(any(), any());
    verify(searcher, atLeastOnce()).searchElastic(any());

    Assert.assertEquals(searchResult, result);

  }


  @Test
  public void doSearchExceptionTest() {
    SearchRequest searchRequest = new SearchRequest();
    String[] query = {"igel"};
    searchRequest.setAgeGroup(0L);
    searchRequest.setParentId(3L);
    searchRequest.setQuery(query);
    searchRequest.setLocale(Locale.de);

    SearchQuery searchQuery = mock(SearchQuery.class);

    when(searcher.buildSearchQuery(any(), any())).thenReturn(searchQuery);

    ElasticGroupSearchable elasticGroupSearchable = new ElasticGroupSearchable();
    elasticGroupSearchable.setId(3L);
    elasticGroupSearchable.setClassName("IdeaGroup");
    elasticGroupSearchable.setLanguageCode(Locale.de.toString());

    when(elasticGroupRepository.findOne(anyLong())).thenThrow(mock(EntityNotFoundException.class));
    when(elasticGroupRepository.findTopByClassNameAndLanguageCode(any(), any()))
      .thenReturn(elasticGroupSearchable);

    SearchResult searchResult = new SearchResult();
    when(searcher.searchElastic(searchQuery)).thenReturn(searchResult);
    SearchResult result = defaultSearchService.doSearch(searchRequest);

    verify(searcher, times(1)).buildSearchQuery(any(), any());
    verify(searcher, times(1)).searchElastic(any());

    Assert.assertEquals(searchResult, result);

  }

}
