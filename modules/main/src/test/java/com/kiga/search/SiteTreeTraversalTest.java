package com.kiga.search;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


import com.kiga.content.domain.ss.IdeaGroup;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.domain.ss.live.IdeaGroupToIdeaLive;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.content.repository.ss.live.IdeaGroupToIdeaRepositoryLive;
import com.kiga.search.services.GroupHierarchy;
import com.kiga.search.services.IdeaHierarchy;
import com.kiga.search.services.SiteTreeTraversalService;
import org.junit.Before;
import org.junit.Test;


import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by faxxe on 8/21/16.
 */
public class SiteTreeTraversalTest {

  IdeaGroupToIdeaRepositoryLive ideaGroupToIdeaRepositoryLive =
    mock(IdeaGroupToIdeaRepositoryLive.class);
  SiteTreeTraversalService siteTreeTraversalService;

  DefaultSearcher jdefaultSearcher;

  /**
   * setup Test.
   */
  @Before
  public void setup() {
    siteTreeTraversalService = new SiteTreeTraversalService(ideaGroupToIdeaRepositoryLive);
  }

  @Test
  public void getGrouphierarchyTest() {
    String groupName = "group1";
    IdeaGroup group = new IdeaGroupLive();
    group.setId(1L);
    group.setTitle(groupName);

    String parentName = "groupP";
    IdeaGroup parentGroup = new IdeaGroupLive();
    parentGroup.setId(2L);
    parentGroup.setTitle(parentName);

    String rootName = "root";
    IdeaGroup root = new IdeaGroupLive();
    root.setId(3L);
    root.setTitle(rootName);

    group.setParent(parentGroup);
    parentGroup.setParent(root);

    GroupHierarchy groupHierarchy = siteTreeTraversalService.getGroupHierarchy(group);

    assertEquals(Arrays.asList(3L, 2L, 1L), groupHierarchy.getPathIds());
    assertEquals(Arrays.asList(rootName, parentName, groupName), groupHierarchy.getPathNames());

  }

  @Test
  public void ideaHierarchyTest() {
    String ideaName = "idea";
    KigaIdeaLive idea = new KigaIdeaLive();
    idea.setId(1L);
    idea.setTitle(ideaName);

    String groupName = "group";
    SiteTreeLive ideaGroup = new IdeaGroupLive();
    ideaGroup.setId(2L);
    ideaGroup.setTitle(groupName);

    String parentName = "groupP";
    IdeaGroup parentGroup = new IdeaGroupLive();
    parentGroup.setId(3L);
    parentGroup.setTitle(parentName);

    String rootName = "root";
    IdeaGroup root = new IdeaGroupLive();
    root.setId(4L);
    root.setTitle(rootName);

    idea.setParent(ideaGroup);
    ideaGroup.setParent((SiteTreeLive) parentGroup);
    parentGroup.setParent(root);

    IdeaGroupToIdeaLive ideaGroupToIdeaLive = new IdeaGroupToIdeaLive();
    ideaGroupToIdeaLive.setIdeaGroup((IdeaGroupLive) ideaGroup);

    List idg2id = Collections.singletonList(ideaGroupToIdeaLive);
    when(ideaGroupToIdeaRepositoryLive.findByArticleId(1L))
      .thenReturn(idg2id);

    IdeaHierarchy ideaHierarchy = siteTreeTraversalService.getIdeaHiearchy(idea);

    assertEquals((long)ideaHierarchy.getCategory1Id(), 2L);
    assertEquals(ideaHierarchy.getCategory1Title(), groupName);
    assertEquals((long)ideaHierarchy.getCategory2Id(), 3L);
    assertEquals(ideaHierarchy.getCategory2Title(), parentName);

    Set<Long> ids = new HashSet<>();
    ids.addAll(Arrays.asList(2L, 3L, 4L));

    assertEquals(ids, ideaHierarchy.getHierarchyIds());

    assertEquals(Collections.singletonList(ideaGroup.getId()), ideaHierarchy.getGroupIds());

    assertEquals(Collections.singletonList(ideaGroup), ideaHierarchy.getIdeaGroups());

  }

}
