package com.kiga.search;

import com.kiga.ideas.web.response.IdeasResponseCategory;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.search.comparators.IdeasResponseCategoryComparator;
import com.kiga.search.comparators.ResponseGroupComparator;
import org.junit.Assert;
import org.junit.Test;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by faxxe on 11/16/16.
 */
public class ComparatorTest {

  @Test
  public void comparatorTest() {

    List<IdeasResponseCategory> responses = new ArrayList<>();

    for (int i = 0; i < 5; i++) {
      IdeasResponseCategory ideasResponseCategory = new IdeasResponseCategory();
      ideasResponseCategory.setSort(7 - i);
      responses.add(ideasResponseCategory);
    }

    for (int i = 0; i < 5; i++) {
      IdeasResponseCategory ideasResponseCategory = new IdeasResponseCategory();
      ideasResponseCategory.setSort(i);
      responses.add(ideasResponseCategory);
    }

    responses.sort(new IdeasResponseCategoryComparator());

    for (int i = 0; i < 9; i++) {
      boolean tmp = responses.get(i).getSort() <= responses.get(i + 1).getSort();
      Assert.assertTrue(tmp);
    }

  }

  @Test
  public void comparator2Test() {

    List<IdeasResponseCategory> responses = new ArrayList<>();

    for (int i = 0; i < 5; i++) {
      IdeasResponseCategory ideasResponseCategory = new IdeasResponseCategory();
      IdeasViewModelIdea ideasViewModelIdea = new IdeasViewModelIdea();
      ideasViewModelIdea.setElasticScore((float)8 - i);
      ideasResponseCategory.setIdeas(Collections.singletonList(ideasViewModelIdea));
      responses.add(ideasResponseCategory);
    }
    for (int i = 0; i < 5; i++) {
      IdeasResponseCategory ideasResponseCategory = new IdeasResponseCategory();
      IdeasViewModelIdea ideasViewModelIdea = new IdeasViewModelIdea();
      ideasViewModelIdea.setElasticScore((float) i);
      ideasResponseCategory.setIdeas(Collections.singletonList(ideasViewModelIdea));
      responses.add(ideasResponseCategory);
    }

    responses.sort(new ResponseGroupComparator());

    for (int i = 0; i < 9; i++) {
      boolean tmp = ((List<IdeasViewModelIdea>) responses.get(i)
        .getIdeas()).get(0).getElasticScore()
        >= ((List<IdeasViewModelIdea>) responses.get(i + 1)
          .getIdeas()).get(0).getElasticScore();
      Assert.assertTrue(tmp);
    }

  }

}
