package com.kiga.search;

import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.ideas.web.response.IdeasResponseRating;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.main.locale.Locale;
import com.kiga.search.models.KigaIdeaSearchable;
import com.kiga.search.services.IdeaHierarchy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by faxxe on 8/21/16.
 */
public class IdeaSearchableConverterTest {

  private IdeaSearchableConverter ideaSearchableConverter;

  /**
   * setup Test.
   */
  @Before
  public void setup() {
    ideaSearchableConverter = new IdeaSearchableConverter();
  }

  @Test
  public void converterTest() {
    Stream.of(Locale.de, Locale.en, Locale.it, Locale.tr).forEach(this::converterTestHelper);

  }

  private void converterTestHelper(Locale locale) {

    List<Long> groupIds = Arrays.asList(1L, 2L, 3L, 4L);
    List<Long> hierarchyIds = Arrays.asList(1L, 2L, 3L, 4L);
    String cat1Title = "cat1Title";
    Long cat1Id = 11L;
    Long cat2Id = 12L;
    String cat2Title = "cat2Title";
    List<IdeaGroupLive> ideaGroupLives = Arrays.asList(new IdeaGroupLive());

    IdeaHierarchy ideaHierarchy = new IdeaHierarchy();
    ideaHierarchy.setGroupIds(groupIds);
    ideaHierarchy.setHierarchyIds(hierarchyIds);
    ideaHierarchy.setCategory1Id(cat1Id);
    ideaHierarchy.setCategory1Title(cat1Title);
    ideaHierarchy.setCategory2Id(cat2Id);
    ideaHierarchy.setCategory2Title(cat2Title);
    Date created = new Date();
    KigaIdeaLive kigaIdeaLive = new KigaIdeaLive();
    kigaIdeaLive.setId(1L);
    kigaIdeaLive.setCreated(created);
    kigaIdeaLive.setFacebookShare(0);
    kigaIdeaLive.setArticleType(ArticleType.DOWNLOAD);
    kigaIdeaLive.setInPool(0);
    kigaIdeaLive.setAgeGroup(2);
    kigaIdeaLive.setSortYear(2016);
    kigaIdeaLive.setSortYearMini(2016);
    kigaIdeaLive.setLocale(locale);
    kigaIdeaLive.setTitle("ideaTitle");
    kigaIdeaLive.setContent("some content");
    kigaIdeaLive.setDescription("some description");
    kigaIdeaLive.setMetaKeywords("some key words");

    IdeasViewModelIdea ideasViewModelIdea = new IdeasViewModelIdea();
    ideasViewModelIdea.setRating(new IdeasResponseRating(3f, 3));
    ideasViewModelIdea.setUrl("http://someurl");
    ideasViewModelIdea.setPreviewPages(new ArrayList<>());
    ideasViewModelIdea.setPreviewUrl("http://somepreviewurl");
    ideasViewModelIdea.setLargePreviewImageUrl("http://somelargepreviewurl");

    KigaIdeaSearchable searchable = ideaSearchableConverter.createKigaIdeaSearchable(kigaIdeaLive,
      ideasViewModelIdea, ideaHierarchy);

    Assert.assertEquals(1L, (long)searchable.getId());
    Assert.assertEquals(created, searchable.getCreated());
    Assert.assertEquals(false, searchable.getFacebookShare());
    Assert.assertEquals(false, searchable.getInPool());

    Assert.assertEquals(3f, searchable.getRating(), 0.1);
    Assert.assertEquals(3, (long)searchable.getRatingCount());
    Assert.assertEquals("http://someurl", searchable.getUrl());
    Assert.assertEquals("http://somepreviewurl", searchable.getPreviewUrl());
    Assert.assertEquals("http://somelargepreviewurl", searchable.getLargePreviewUrl());

  }

}
