package com.kiga.search;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.AllOf.allOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import com.kiga.main.locale.Locale;
import com.kiga.search.filters.AgeGroupFilter;
import com.kiga.search.filters.FilterHelper;
import com.kiga.search.filters.LanguageFilter;
import com.kiga.search.filters.NewIdeasFilter;
import com.kiga.search.filters.ParentFilter;
import com.kiga.search.filters.SearchFilter;
import com.kiga.search.models.ElasticGroupSearchable;
import com.kiga.search.models.KigaGroupSearchable;
import com.kiga.search.models.KigaIdeaSearchable;
import com.kiga.search.repositories.KigaGroupSearchableRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.SearchQuery;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Created by faxxe on 8/21/16.
 */
public class SearcherTest {


  private KigaGroupSearchableRepository jkigaGroupSearchableRepository;
  private ElasticsearchTemplate elasticsearchTemplate = mock(ElasticsearchTemplate.class);

  DefaultSearcher jdefaultSearcher;
  String indexName = "kiga";

  /**
   * setup Test.
   */
  @Before
  public void setup() {
    this.jkigaGroupSearchableRepository = mock(KigaGroupSearchableRepository.class);
    jdefaultSearcher = new DefaultSearcher(elasticsearchTemplate, jkigaGroupSearchableRepository);
  }

  @Test
  public void explainTest() {
    String explaination = jdefaultSearcher.explainSync(null, null, null);
    Assert.assertNull(explaination);
  }

  @Test
  public void searchElasticTest() {
    SearchQuery searchQuery = mock(SearchQuery.class);
    KigaIdeaSearchable jkigaIdeaSearchable = new KigaIdeaSearchable();
    KigaGroupSearchable jkigaGroupSearchable = new KigaGroupSearchable();

    List<Long> ids = Collections.emptyList();
    jkigaIdeaSearchable.setGroupIds(ids);

    when(elasticsearchTemplate.queryForList(searchQuery, KigaIdeaSearchable.class))
      .thenReturn(Collections.singletonList(jkigaIdeaSearchable));

    when(jkigaGroupSearchableRepository.findAll(ids))
      .thenReturn(Collections.singletonList(jkigaGroupSearchable));

    SearchResult searchResult = jdefaultSearcher.searchElastic(searchQuery);

    Assert.assertEquals(searchResult.getGroups().size(), 0);

    verify(jkigaGroupSearchableRepository, never()).findAll(new HashSet<Long>(ids));
  }

  @Test
  public void searchElastic2Test() {
    SearchQuery searchQuery = mock(SearchQuery.class);
    KigaIdeaSearchable jkigaIdeaSearchable = new KigaIdeaSearchable();
    KigaGroupSearchable jkigaGroupSearchable = new KigaGroupSearchable();

    List<Long> ids = Arrays.asList(1L, 2L, 3L);
    jkigaIdeaSearchable.setGroupIds(ids);

    when(elasticsearchTemplate.queryForList(searchQuery, KigaIdeaSearchable.class))
      .thenReturn(Collections.singletonList(jkigaIdeaSearchable));

    when(jkigaGroupSearchableRepository.findAll(ids))
      .thenReturn(Collections.singletonList(jkigaGroupSearchable));

    SearchResult searchResult = jdefaultSearcher.searchElastic(searchQuery);
    verify(jkigaGroupSearchableRepository, times(1)).findAll(new HashSet<Long>(ids));
  }

  private class TestFilter extends SearchFilter{}

  @Test
  public void queryBuilderTest() {
    String query = "someQuery";
    List<SearchFilter> searchFilters = new ArrayList<>();

    searchFilters.add(new AgeGroupFilter(3L));
    searchFilters.add(new ParentFilter(3L));
    searchFilters.add(new LanguageFilter(Locale.de));
    searchFilters.add(new NewIdeasFilter());
    searchFilters.add(new TestFilter());

    SearchQuery searchQuery = jdefaultSearcher.buildSearchQuery(query, searchFilters);
    Assert.assertNotNull(searchQuery);

  }

  @Test
  public void testBeans() {
    List<Class> classesToTest = Arrays.asList(
      SearchResult.class, ElasticGroupSearchable.class, KigaGroupSearchable.class,
      KigaIdeaSearchable.class, SearcherConfig.class, AgeGroupFilter.class, FilterHelper.class,
      LanguageFilter.class, NewIdeasFilter.class, ParentFilter.class
    );

    classesToTest.forEach(x ->
      assertThat(x, allOf(hasValidBeanConstructor(), hasValidGettersAndSetters())));

  }





}
