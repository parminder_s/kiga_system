package com.kiga.search;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import com.kiga.ideas.web.response.IdeaGroupViewModel;
import com.kiga.ideas.web.response.IdeasResponseCategory;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.search.comparators.IdeasResponseCategoryComparator;
import com.kiga.search.converter.KigaGroupSearchableConverter;
import com.kiga.search.converter.KigaIdeaSearchableConverter;
import com.kiga.search.converter.SearchResponseConverter;
import com.kiga.search.models.ElasticGroupSearchable;
import com.kiga.search.models.KigaGroupSearchable;
import com.kiga.search.models.KigaIdeaSearchable;
import com.kiga.search.repositories.ElasticGroupRepository;
import com.kiga.search.services.GroupService;
import org.junit.Before;
import org.junit.Test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by faxxe on 8/21/16.
 */
public class ConverterTest {

  private GroupService groupService;
  private ElasticGroupRepository elasticGroupRepository;
  private Comparator<IdeasResponseCategory> comparator;
  private KigaGroupSearchableConverter mockjkigaGroupSearchableConverter;
  private KigaGroupSearchableConverter jkigaGroupSearchableConverter;

  private KigaIdeaSearchableConverter jkigaIdeaSearchableConverter;

  private int maxIntPerRow = 3;

  private SearchResponseConverter searchResponseConverter;


  /**
   * setup Test.
   */
  @Before
  public void setup() {
    groupService = mock(GroupService.class);
    elasticGroupRepository = mock(ElasticGroupRepository.class);
    comparator = new IdeasResponseCategoryComparator();
    mockjkigaGroupSearchableConverter = mock(KigaGroupSearchableConverter.class);
    searchResponseConverter =
      new SearchResponseConverter(
        groupService,
        comparator, mockjkigaGroupSearchableConverter,
        maxIntPerRow
      );

    this.elasticGroupRepository = mock(ElasticGroupRepository.class);
    this.jkigaIdeaSearchableConverter = mock(KigaIdeaSearchableConverter.class);

    this.jkigaGroupSearchableConverter = new KigaGroupSearchableConverter(Collections
      .singletonList("hiddenGroup"), elasticGroupRepository, jkigaIdeaSearchableConverter);


  }

  @Test
  public void converterTest() {
    SearchResult searchResult = new SearchResult();
    searchResult.setRoot(true);
    searchResult.setParentId(2L);
    searchResult.setTotalHits(211L);

    List<KigaIdeaSearchable> ideas = getIdeas();
    List<KigaGroupSearchable> groups = getGroups();

    searchResult.setIdeas(ideas);
    searchResult.setGroups(groups);
    IdeasResponseCategory ideasResponseCategory = new IdeasResponseCategory();
    ideasResponseCategory.setIdeas(getIdeaModels());

    when(mockjkigaGroupSearchableConverter.convertToResponse(any()))
      .thenReturn(ideasResponseCategory);
    doCallRealMethod().when(groupService).groupGroups(any(), any());
    doCallRealMethod().when(groupService).addIdeasToGroups(any(), any());

    IdeaGroupViewModel ideasViewModel = searchResponseConverter.convertToResponse(searchResult);

    assertNotNull(ideasViewModel);

    verify(mockjkigaGroupSearchableConverter, times(1)).convertToResponse(any());
    verify(groupService, times(1)).groupGroups(any(), any());
    verify(groupService, times(1)).addIdeasToGroups(any(), any());

  }

  @Test
  public void ideaConverterTest() {

    KigaIdeaSearchable ideaSearchable = getIdeas().get(0);

    KigaIdeaSearchableConverter jkigaIdeaSearchableConverter = new KigaIdeaSearchableConverter();

    IdeasViewModelIdea ideasViewModelIdea = jkigaIdeaSearchableConverter
      .convertToResponse(ideaSearchable);

    assertEquals(ideasViewModelIdea.getId(), ideaSearchable.getId());
    assertEquals(ideasViewModelIdea.getTitle(), ideaSearchable.getTitle());
    assertEquals(ideasViewModelIdea.getName(), "" + ideaSearchable.getId());
    assertEquals(ideasViewModelIdea.getDescription(), ideaSearchable.getDescription());
    assertEquals(ideasViewModelIdea.getPreviewUrl(), ideaSearchable.getPreviewUrl());
    assertEquals(ideasViewModelIdea.getPreviewPages(), ideaSearchable.getPreviewPages());
    assertEquals(ideasViewModelIdea.getUrl(), ideaSearchable.getUrl());

    assertEquals(ideasViewModelIdea.getLevel1Title(), ideaSearchable.getCategory1Title());
    assertEquals(ideasViewModelIdea.getLevel2Title(), ideaSearchable.getCategory2Title());
    assertEquals(ideasViewModelIdea.getAgeRange(), ideaSearchable.getAgeGroupRange());
    assertEquals(ideasViewModelIdea.getRating().getRating(), ideaSearchable.getRating(), 0.1);
    assertEquals((double)ideasViewModelIdea.getElasticScore(), ideaSearchable.getScore(), 0.1);
    assertEquals(ideasViewModelIdea.getInPool(), ideaSearchable.getInPool());
    assertEquals(ideasViewModelIdea.getFacebookShare(), ideaSearchable.getFacebookShare());

  }

  @Test
  public void groupConverterTest() {
    ElasticGroupSearchable elasticGroupSearchable = new ElasticGroupSearchable();
    elasticGroupSearchable.setIconCode("iconcode");
    elasticGroupSearchable.setSort(1);

    when(elasticGroupRepository.findOne(anyLong())).thenReturn(elasticGroupSearchable);
    IdeasViewModelIdea ideasViewModelIdea = new IdeasViewModelIdea();
    ideasViewModelIdea.setId(1L);
    when(jkigaIdeaSearchableConverter.convertToResponse(any())).thenReturn(ideasViewModelIdea);

    KigaGroupSearchable groupSearchable = getGroups().get(0);
    groupSearchable.setKigaIdeas(getIdeas());

    IdeasResponseCategory ideaResponseCategory = jkigaGroupSearchableConverter
      .convertToResponse(groupSearchable);

    assertNotNull(ideaResponseCategory);
    verify(jkigaIdeaSearchableConverter, times(4)).convertToResponse(any());
    verify(elasticGroupRepository, times(1)).findOne(1L);
    assertEquals(4, ideaResponseCategory.getIdeas().size());

  }

  private List<IdeasViewModelIdea> getIdeaModels() {
    IdeasViewModelIdea ideasViewModelIdea = new IdeasViewModelIdea();
    ideasViewModelIdea.setId(0L);

    return Arrays.asList(ideasViewModelIdea);
  }

  private List<KigaGroupSearchable> getGroups() {

    KigaGroupSearchable jkgs1 = new KigaGroupSearchable();
    jkgs1.setId(1L);
    jkgs1.setTitle("Group1");
    jkgs1.setLanguageCode("de");
    jkgs1.setPath(Arrays.asList("g1", "g2", "g3", "g4"));
    jkgs1.setPathIds(Arrays.asList(1L, 2L, 3L, 4L));
    KigaGroupSearchable jkgs2 = new KigaGroupSearchable();
    jkgs2.setId(2L);
    jkgs2.setTitle("Group2");
    jkgs2.setLanguageCode("de");
    jkgs2.setPath(Arrays.asList("g1", "g2", "g3", "g4"));
    jkgs2.setPathIds(Arrays.asList(1L, 2L, 3L, 4L));
    KigaGroupSearchable jkgs3 = new KigaGroupSearchable();
    jkgs3.setId(3L);
    jkgs3.setTitle("Group3");
    jkgs3.setLanguageCode("de");
    jkgs3.setPath(Arrays.asList("g1", "g2", "g3", "g4"));
    jkgs3.setPathIds(Arrays.asList(1L, 2L, 3L, 4L));
    KigaGroupSearchable jkgs4 = new KigaGroupSearchable();
    jkgs4.setId(4L);
    jkgs4.setTitle("Group4");
    jkgs4.setLanguageCode("de");
    jkgs4.setPathIds(Arrays.asList(1L, 2L, 3L, 4L));
    jkgs4.setPath(Arrays.asList("g1", "g2", "g3", "g4"));

    return Arrays.asList(jkgs1, jkgs2, jkgs3, jkgs4);

  }

  private List<KigaIdeaSearchable> getIdeas() {
    KigaIdeaSearchable jkis1 = new KigaIdeaSearchable();
    jkis1.setId(11L);
    jkis1.setRating(1f);
    jkis1.setRatingCount(1);
    jkis1.setSortYear(2016);
    jkis1.setUrl("http:///");
    jkis1.setLanguageCode("de");
    jkis1.setPreviewPages(new ArrayList<>());
    jkis1.setPreviewUrl("http://");
    jkis1.setLargePreviewUrl("http://");
    jkis1.setCategory1Id(111L);
    jkis1.setCategory1Title("somecategory1Title");
    jkis1.setCategory2Id(111L);
    jkis1.setCategory2Title("somecategory2Title");
    jkis1.setContent("some Content!");
    jkis1.setDescription("description");
    jkis1.setKeywords("some keywords");
    jkis1.setTitle("some title");
    jkis1.setHierarchyIds(Arrays.asList(2L, 3L, 4L));
    jkis1.setScore(1.0f);

    KigaIdeaSearchable jkis2 = new KigaIdeaSearchable();
    jkis2.setId(12L);
    jkis2.setRating(1f);
    jkis2.setRatingCount(1);
    jkis2.setSortYear(2016);
    jkis2.setUrl("http:///");
    jkis2.setLanguageCode("de");
    jkis2.setPreviewPages(new ArrayList<>());
    jkis2.setPreviewUrl("http://");
    jkis2.setLargePreviewUrl("http://");
    jkis2.setCategory1Id(111L);
    jkis2.setCategory1Title("somecategory1Title");
    jkis2.setCategory2Id(111L);
    jkis2.setCategory2Title("somecategory2Title");
    jkis2.setContent("some Content!");
    jkis2.setDescription("description");
    jkis2.setKeywords("some keywords");
    jkis2.setTitle("some title");
    jkis2.setHierarchyIds(Arrays.asList(2L, 3L, 4L));
    jkis2.setScore(1.0f);

    KigaIdeaSearchable jkis3 = new KigaIdeaSearchable();
    jkis3.setId(13L);
    jkis3.setRating(1f);
    jkis3.setRatingCount(1);
    jkis3.setSortYear(2016);
    jkis3.setUrl("http:///");
    jkis3.setLanguageCode("de");
    jkis3.setPreviewPages(new ArrayList<>());
    jkis3.setPreviewUrl("http://");
    jkis3.setLargePreviewUrl("http://");
    jkis3.setCategory1Id(111L);
    jkis3.setCategory1Title("somecategory1Title");
    jkis3.setCategory2Id(111L);
    jkis3.setCategory2Title("somecategory2Title");
    jkis3.setContent("some Content!");
    jkis3.setDescription("description");
    jkis3.setKeywords("some keywords");
    jkis3.setTitle("some title");
    jkis3.setScore(1.0f);
    jkis3.setHierarchyIds(Arrays.asList(2L, 3L, 4L));

    KigaIdeaSearchable jkis4 = new KigaIdeaSearchable();
    jkis4.setId(14L);
    jkis4.setRating(1f);
    jkis4.setRatingCount(1);
    jkis4.setSortYear(2016);
    jkis4.setUrl("http:///");
    jkis4.setLanguageCode("de");
    jkis4.setPreviewPages(new ArrayList<>());
    jkis4.setPreviewUrl("http://");
    jkis4.setLargePreviewUrl("http://");
    jkis4.setCategory1Id(111L);
    jkis4.setCategory1Title("somecategory1Title");
    jkis4.setCategory2Id(111L);
    jkis4.setCategory2Title("somecategory2Title");
    jkis4.setContent("some Content!");
    jkis4.setDescription("description");
    jkis4.setKeywords("some keywords");
    jkis4.setTitle("some title");
    jkis4.setScore(1.0f);
    jkis4.setHierarchyIds(Arrays.asList(2L, 3L, 4L));

    return Arrays.asList(jkis1, jkis2, jkis3, jkis4);
  }


}
