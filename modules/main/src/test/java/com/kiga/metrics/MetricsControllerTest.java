package com.kiga.metrics;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


import org.junit.Test;
import org.springframework.boot.actuate.endpoint.MetricsEndpoint;
import org.springframework.boot.actuate.trace.TraceRepository;

/**
 * Created by faxxe on 2/14/17.
 */
public class MetricsControllerTest {

  @Test
  public void ctorTest() {

    MetricsEndpoint metricsEndpoint = mock(MetricsEndpoint.class);
    TraceRepository traceRepository = mock(TraceRepository.class);

    MetricsController controller = new MetricsController(metricsEndpoint, traceRepository);
    controller.getMetrics();
    verify(metricsEndpoint, times(1)).invoke();
    controller.trace();
    verify(traceRepository, times(1)).findAll();

  }

}
