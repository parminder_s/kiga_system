package com.kiga.metrics;

import static org.mockito.Mockito.mock;


import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.boot.actuate.metrics.Metric;
import org.springframework.boot.actuate.metrics.writer.Delta;
import org.springframework.data.influxdb.InfluxDBTemplate;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by faxxe on 2/14/17.
 */
public class ExporterTest {

  @Test
  public void constructorTest() {

  }

  @Test
  public void exporterIncrementTest() {

    InfluxDBTemplate influxTemplate = mock(InfluxDBTemplate.class);
    KigaExporter kigaExporter = new KigaExporter(influxTemplate, "test", "te");

    kigaExporter.increment(new Delta("prop", 5));
    kigaExporter.increment(new Delta("prop", 5));
    kigaExporter.increment(new Delta("prop", 5));

    kigaExporter.send();

    ArgumentCaptor<List> captor = ArgumentCaptor.forClass(List.class);

    Mockito.verify(influxTemplate, Mockito.times(1)).write(captor.capture());


    Assert.assertEquals(1, captor.getValue().size());

  }

  @Test
  public void exporterIncremtntGaugeTest() {

    InfluxDBTemplate influxTemplate = mock(InfluxDBTemplate.class);
    KigaExporter kigaExporter = new KigaExporter(influxTemplate, "test", "");

    kigaExporter.increment(new Delta("prop1", 5));
    kigaExporter.increment(new Delta("prop2", 5));
    kigaExporter.increment(new Delta("prop3", 5));
    kigaExporter.increment(new Delta("prop4", 5));

    kigaExporter.send();

    ArgumentCaptor<List> captor = ArgumentCaptor.forClass(List.class);
    Mockito.verify(influxTemplate, Mockito.times(1)).write(captor.capture());

    Assert.assertEquals(4, captor.getValue().size());

    kigaExporter.set(new Metric<Number>("wupp", 3));

    kigaExporter.send();

    captor = ArgumentCaptor.forClass(List.class);
    Mockito.verify(influxTemplate, Mockito.times(2)).write(captor.capture());

    Assert.assertEquals(1, captor.getValue().size());

    kigaExporter.increment(null);
    kigaExporter.reset(null);

    kigaExporter.set(null);
    Metric metric = new Metric("wupp", 3);
    kigaExporter.set(metric);

  }


  @Test
  public void exporterTestGaugeAverage() {
    InfluxDBTemplate influxTemplate = mock(InfluxDBTemplate.class);
    KigaExporter kigaExporter = new KigaExporter(influxTemplate, "test", "st");

    String name = "com.test.dings.bla";

    Metric metric = new Metric(name, 9f);

    Map<String, Float> average = kigaExporter.getAverage(Collections.singletonList(metric));

    Assert.assertEquals((Float) 9f, (Float)average.get(name));

    Metric metric21 = new Metric(name, 9f);
    Metric metric22 = new Metric(name, 18f);
    Metric metric33 = new Metric(name, 27f);

    average = kigaExporter.getAverage(Arrays.asList(metric21, metric22, metric33));
    Assert.assertEquals((Float) 18f, (Float)average.get(name));

  }

  @Test
  public void exporterExceptionAverage() {
    InfluxDBTemplate influxTemplate = mock(InfluxDBTemplate.class);
    KigaExporter kigaExporter = new KigaExporter(influxTemplate, "test", "st");
  }

}
