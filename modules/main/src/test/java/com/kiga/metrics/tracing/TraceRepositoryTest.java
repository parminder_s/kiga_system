package com.kiga.metrics.tracing;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.actuate.trace.Trace;


import java.util.List;

/**
 * Created by faxxe on 2/25/17.
 */
public class TraceRepositoryTest {


  @Test
  public void findAll() throws Exception {
    KigaTraceRepository kigaTraceRepository = new KigaTraceRepository(null, null);
    List<Trace> traces = kigaTraceRepository.findAll();
    Assert.assertNull(traces);
  }


  @Test
  public void findAllNoop() throws Exception {
    NoopTraceRepository kigaTraceRepository = new NoopTraceRepository();
    List<Trace> traces = kigaTraceRepository.findAll();
    Assert.assertNull(traces);
  }

  @Test
  public void addWithoutKafkaTemplateNoop() throws Exception {
    NoopTraceRepository kigaTraceRepository = new NoopTraceRepository();
    kigaTraceRepository.add(null);
  }


}
