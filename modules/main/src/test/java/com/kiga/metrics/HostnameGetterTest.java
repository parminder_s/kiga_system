package com.kiga.metrics;

import org.junit.Test;
import org.springframework.util.Assert;

/**
 * Created by faxxe on 2/14/17.
 */
public class HostnameGetterTest {

  @Test
  public void getterTest() {
    HostnameGetter hostnameGetter = new HostnameGetter();
    String hostname = hostnameGetter.getHostname();
    Assert.notNull(hostname);
  }

}
