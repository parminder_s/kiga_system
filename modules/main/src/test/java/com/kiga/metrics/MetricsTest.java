package com.kiga.metrics;

import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;


import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.actuate.metrics.writer.MetricWriter;
import org.springframework.data.influxdb.InfluxDBConnectionFactory;
import org.springframework.kafka.core.KafkaTemplate;

/**
 * Created by faxxe on 2/1/17.
 */
public class MetricsTest {


  @Test
  public void jobTest() {
    KigaExporter kigaExporter = mock(KigaExporter.class);

    MetricsReporterJob reporterJob = new MetricsReporterJob(kigaExporter);
    reporterJob.runJob();
    Mockito.verify(kigaExporter, Mockito.times(1)).send();

  }

  @Test
  public void repositoryTest() {
    MetricsProperties metricsProperties = new MetricsProperties();
    metricsProperties.setEnabled(true);
    metricsProperties.setTrace(true);
    metricsProperties.setTraceLogPath("/tmp/trace.log");
    KafkaTemplate kafkaTemplate = mock(KafkaTemplate.class);

    MetricsConfig metricsConfig = new MetricsConfig();
    metricsConfig.traceRepository(metricsProperties);
  }

  @Test
  public void repositoryDisabledTest() {
    MetricsProperties metricsProperties = new MetricsProperties();
    metricsProperties.setEnabled(true);
    metricsProperties.setTrace(false);

    MetricsConfig metricsConfig = new MetricsConfig();
    metricsConfig.traceRepository(metricsProperties);

  }

  @Test
  public void getterSetterTest() {
    assertThat(MetricsProperties.class, Matchers.allOf(hasValidGettersAndSetters()));
  }

  @Test
  public void filterTest() {
    MetricsConfig metricsConfig = new MetricsConfig();
    metricsConfig.webRequestTraceFilter(null, null, null);
  }

  @Test
  public void configTest() {

    MetricsConfig metricsConfig = new MetricsConfig();
    MetricsProperties metricsProperties = new MetricsProperties();
    HostnameGetter hng = mock(HostnameGetter.class);
    metricsProperties.setEnabled(true);
    MetricWriter writer = metricsConfig.kigaExporter(null, metricsProperties, hng);

    Assert.assertTrue(writer instanceof KigaExporter);
  }

  @Test
  public void configTest2() {

    MetricsConfig metricsConfig = new MetricsConfig();
    MetricsProperties metricsProperties = new MetricsProperties();
    HostnameGetter hng = mock(HostnameGetter.class);
    metricsProperties.setEnabled(false);
    MetricWriter writer = metricsConfig.kigaExporter(null, metricsProperties, hng);
    Assert.assertNull(writer);

  }

  @Test
  public void repositoryExporterTest() {
    MetricsConfig metricsConfig = new MetricsConfig();
    HostnameGetter hng = mock(HostnameGetter.class);
    MetricsProperties metricsProperties = new MetricsProperties();
    metricsProperties.setEnabled(true);
    MetricWriter writer = metricsConfig.kigaExporter(null, metricsProperties, hng);
    Assert.assertNotNull(writer);
    metricsProperties.setEnabled(false);
    writer = metricsConfig.kigaExporter(null, metricsProperties, hng);
    Assert.assertNull(writer);
  }

  @Test
  public void jobNullTest() {
    MetricsReporterJob reporterJob = new MetricsReporterJob(null);
    reporterJob.runJob();
  }

  @Test
  public void influxNullConfigTest() {
    InfluxConfig influxConfig = new InfluxConfig();

    InfluxDBConnectionFactory factory = influxConfig.connectionFactory(null);
    Assert.assertNull(factory);

  }

  @Test
  public void influxConfigFactoryTest() {
    InfluxConfig influxConfig = new InfluxConfig();
    MetricsProperties metricsProperties = new MetricsProperties();
    metricsProperties.setEnabled(true);
    InfluxDBConnectionFactory influxConnectionFactory = mock(InfluxDBConnectionFactory.class);
    influxConfig.defaultTemplate(influxConnectionFactory);

  }

  @Test
  public void influxConfigTest() {
    InfluxConfig influxConfig = new InfluxConfig();
    MetricsProperties metricsProperties = new MetricsProperties();
    metricsProperties.setEnabled(true);
    influxConfig.connectionFactory(metricsProperties);

  }

  @Test
  public void influxConfigDisabledTest() {
    InfluxConfig influxConfig = new InfluxConfig();
    MetricsProperties metricsProperties = new MetricsProperties();
    metricsProperties.setEnabled(false);
    InfluxDBConnectionFactory factory = influxConfig.connectionFactory(metricsProperties);
    Assert.assertNull(factory);

  }

  @Test
  public void influxTemplateTest() {
    InfluxConfig influxConfig = new InfluxConfig();
    influxConfig.defaultTemplate(null);
  }

  @Test
  public void metricsPropertiesTest() {

  }

}
