package com.kiga.metrics.tracing;

import static org.mockito.Mockito.mock;


import com.kiga.security.services.SecurityService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.actuate.trace.TraceProperties;
import org.springframework.boot.actuate.trace.TraceRepository;


import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by faxxe on 2/22/17.
 */
public class KigaTraceFilterTest {
  private KigaTraceFilter kigaTraceFilter;

  /**
   * some comment.
   */
  @Before
  public void setUp() {
    TraceRepository traceRepository = mock(TraceRepository.class);
    TraceProperties traceProperties = new TraceProperties();
    SecurityService securityService = mock(SecurityService.class);

    kigaTraceFilter = new KigaTraceFilter(
      traceRepository,
      traceProperties,
      securityService);
  }


  @Test
  public void ctorTest() {

    TraceRepository traceRepository = mock(TraceRepository.class);
    TraceProperties traceProperties = new TraceProperties();
    SecurityService securityService = mock(SecurityService.class);

    KigaTraceFilter kigaTraceFilter = new KigaTraceFilter(
      traceRepository,
      traceProperties,
      securityService);

    Assert.assertNotNull(kigaTraceFilter);

  }

  @Test
  public void doFilterInternal() throws Exception {

    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
    FilterChain filterChain = mock(FilterChain.class);
    //TODO: test stuff
  }

  @Test
  public void getTrace() throws Exception {

  }

}
