package com.kiga.favourites.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.repository.ss.live.KigaIdeaRepositoryLive;
import com.kiga.favourites.domain.IdeaFavourite;
import com.kiga.favourites.repository.IdeaFavouritesCustomQueriesRepository;
import com.kiga.favourites.repository.IdeaFavouritesRepository;
import com.kiga.favourites.web.converter.IdeaFavouriteToViewModelConverter;
import com.kiga.favourites.web.response.FavouriteResponse;
import com.kiga.security.domain.Member;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author bbs
 * @since 11/4/16.
 */
public class FavouritesServiceTest {
  private static final int KIGA_IDEA_LIVE_ID = 3;
  private FavouritesService favouritesService;
  private KigaIdeaRepositoryLive kigaIdeaRepositoryLive = mock(KigaIdeaRepositoryLive.class);
  private IdeaFavouritesRepository ideaFavouritesRepository = mock(IdeaFavouritesRepository.class);
  private IdeaFavouriteToViewModelConverter ideaFavouriteToViewModelConverter = mock(
    IdeaFavouriteToViewModelConverter.class);
  private IdeaFavouritesCustomQueriesRepository ideaFavouritesCustomQueriesRepository = mock(
    IdeaFavouritesCustomQueriesRepository.class);

  @Before
  public void prepareTests() {
    favouritesService = new FavouritesService(kigaIdeaRepositoryLive, ideaFavouritesRepository,
      ideaFavouriteToViewModelConverter, ideaFavouritesCustomQueriesRepository);
  }

  @Test
  public void testToggleNonExistingFavourite() {
    Member member = new Member();
    member.setId(0L);

    when(ideaFavouritesCustomQueriesRepository.findFavouriteWithKigaIdea(member, 1L))
      .thenReturn(null);

    KigaIdeaLive kigaIdeaLive = new KigaIdeaLive();
    kigaIdeaLive.setId(1L);
    when(kigaIdeaRepositoryLive.findOne(1L)).thenReturn(kigaIdeaLive);

    favouritesService.toggle(1L, member);
    ArgumentCaptor<IdeaFavourite> ideaFavouriteArgumentCaptor = ArgumentCaptor
      .forClass(IdeaFavourite.class);
    verify(ideaFavouritesCustomQueriesRepository).findFavouriteWithKigaIdea(member, 1L);
    verify(ideaFavouritesRepository).save(ideaFavouriteArgumentCaptor.capture());
    verify(kigaIdeaRepositoryLive).findOne(1L);
    IdeaFavourite savedIdeaFavourite = ideaFavouriteArgumentCaptor.getValue();
    assertNotNull(savedIdeaFavourite);
    assertEquals(1L, savedIdeaFavourite.getKigaIdeaLive().getId().longValue());
    assertEquals(member.getId().longValue(), savedIdeaFavourite.getMember().getId().longValue());
  }

  @Test
  public void testToggleExistingFavourite() {
    IdeaFavourite ideaFavourite = new IdeaFavourite();
    Member member = new Member();
    member.setId(0L);
    when(ideaFavouritesCustomQueriesRepository.findFavouriteWithKigaIdea(member, 1L))
      .thenReturn(ideaFavourite);
    favouritesService.toggle(1L, member);
    verify(ideaFavouritesCustomQueriesRepository).findFavouriteWithKigaIdea(member, 1L);
    verify(ideaFavouritesRepository).delete(ideaFavourite);
  }

  @Test
  public void testGetMemberFavourites() {
    List<IdeaFavourite> ideaFavourites = Collections.singletonList(new IdeaFavourite());
    List<FavouriteResponse> favouriteResponses = Collections.singletonList(new FavouriteResponse());

    Member member = new Member();
    member.setId(0L);
    when(ideaFavouritesRepository.findByMember(member)).thenReturn(ideaFavourites);
    when(ideaFavouriteToViewModelConverter.convertToResponse(ideaFavourites))
      .thenReturn(favouriteResponses);
    List<FavouriteResponse> returnedFavouriteResponses = favouritesService
      .getMemberFavourites(member);
    assertTrue(CollectionUtils.isNotEmpty(returnedFavouriteResponses));
    verify(ideaFavouriteToViewModelConverter).convertToResponse(ideaFavourites);
    verify(ideaFavouritesRepository).findByMember(member);
  }

  @Test
  public void testGetMemberFavourites2() {
    List<IdeaFavourite> ideaFavourites = Arrays.asList(new IdeaFavourite(), null);
    List<FavouriteResponse> favouriteResponses = Arrays.asList(new FavouriteResponse(), null);

    Member member = new Member();
    member.setId(0L);
    when(ideaFavouritesRepository.findByMember(member)).thenReturn(ideaFavourites);
    when(ideaFavouriteToViewModelConverter.convertToResponse(ideaFavourites))
      .thenReturn(favouriteResponses);
    List<FavouriteResponse> returnedFavouriteResponses = favouritesService
      .getMemberFavourites(member);
    assertTrue(CollectionUtils.isNotEmpty(returnedFavouriteResponses));
    verify(ideaFavouriteToViewModelConverter).convertToResponse(ideaFavourites);
    verify(ideaFavouritesRepository).findByMember(member);
  }

  @Test
  public void testGetForIdea() {
    Member memberMock = mock(Member.class);
    when(ideaFavouritesRepository.findByMemberAndKigaIdeaLiveId(memberMock, KIGA_IDEA_LIVE_ID))
      .thenReturn(new IdeaFavourite());
    IdeaFavourite forIdea = favouritesService.getForIdea(memberMock, KIGA_IDEA_LIVE_ID);
    assertNotNull(forIdea);
  }
}
