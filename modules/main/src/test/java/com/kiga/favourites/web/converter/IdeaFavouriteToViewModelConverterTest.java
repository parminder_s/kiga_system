package com.kiga.favourites.web.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.favourites.domain.IdeaFavourite;
import com.kiga.favourites.web.response.FavouriteResponse;
import com.kiga.security.domain.Member;
import org.junit.Test;

/**
 * @author bbs
 * @since 11/4/16.
 */
public class IdeaFavouriteToViewModelConverterTest {
  private static final String CLASS_NAME = "IdeaFavourite";
  private static final long ID = 1L;
  private static final long KIGA_IDEA_ID = 2L;
  private IdeaFavouriteToViewModelConverter ideaFavouriteToViewModelConverter =
      new IdeaFavouriteToViewModelConverter();

  @Test
  public void testConvertEntity() {
    IdeaFavourite ideaFavourite = new IdeaFavourite();

    ideaFavourite.setId(ID);
    ideaFavourite.setClassName(CLASS_NAME);

    KigaIdeaLive kigaIdeaLive = new KigaIdeaLive();
    kigaIdeaLive.setId(KIGA_IDEA_ID);
    ideaFavourite.setKigaIdeaLive(kigaIdeaLive);

    Member member = new Member();
    ideaFavourite.setMember(member);

    FavouriteResponse viewModel =
        ideaFavouriteToViewModelConverter.convertToResponse(ideaFavourite);

    assertEquals(ID, viewModel.getId().longValue());
    assertEquals(KIGA_IDEA_ID, viewModel.getIdeaId().longValue());
  }

  @Test
  public void testConvertNullIdea() {
    IdeaFavourite ideaFavourite = new IdeaFavourite();

    ideaFavourite.setId(ID);
    ideaFavourite.setClassName(CLASS_NAME);
    ideaFavourite.setKigaIdeaLive(null);

    Member member = new Member();
    ideaFavourite.setMember(member);

    FavouriteResponse viewModel =
        ideaFavouriteToViewModelConverter.convertToResponse(ideaFavourite);

    assertThat(viewModel).isNull();
  }
}
