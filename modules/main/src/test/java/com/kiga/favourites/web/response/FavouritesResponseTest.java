package com.kiga.favourites.web.response;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanEquals;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanHashCode;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanToString;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * @author bbs
 * @since 11/1/16.
 */
public class FavouritesResponseTest {
  @Test
  public void testProperties() {
    assertThat(FavouritesResponse.class,
      allOf(hasValidGettersAndSetters(), hasValidBeanHashCode(), hasValidBeanEquals(),
        hasValidBeanToString()));
  }
}
