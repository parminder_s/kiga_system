package com.kiga.favourites.service.parameters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.security.domain.Member;
import org.junit.Test;

import java.util.Optional;

/**
 * Created by rainerh on 04.01.17.
 */
public class QueryRequestParametersTest {
  @Test
  public void testEquals() {
    QueryRequestParameters parameters1 = QueryRequestParameters.builder().build();
    QueryRequestParameters parameters2 = QueryRequestParameters.builder().build();

    IdeaCategoryLive ideaCategoryLive = new IdeaCategoryLive();
    Member member1 = new Member();

    parameters1.setEntity(ideaCategoryLive);
    parameters1.setMember(Optional.of(member1));

    parameters2.setEntity(ideaCategoryLive);
    parameters2.setMember(Optional.of(member1));

    assertEquals(parameters1, parameters2);

    Member member2 = new Member();
    parameters2.setMember(Optional.of(member2));
    assertNotEquals(parameters1, parameters2);
  }
}
