package com.kiga.favourites.repository;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.security.domain.Member;
import com.mysema.query.types.expr.BooleanExpression;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

/**
 * @author bbs
 * @since 12/10/16.
 */
public class IdeaFavouritesCustomQueriesRepositoryTest {
  private IdeaFavouritesRepository ideaFavouritesRepositoryMock = mock(
    IdeaFavouritesRepository.class);
  private IdeaFavouritesCustomQueriesRepository ideaFavouritesCustomQueriesRepository = new
    IdeaFavouritesCustomQueriesRepository(
    ideaFavouritesRepositoryMock);

  @Test
  public void testFindFavouriteWithKigaIdea() {
    Member memberMock = mock(Member.class);
    when(memberMock.toString()).thenReturn("memberMock");
    ideaFavouritesCustomQueriesRepository.findFavouriteWithKigaIdea(memberMock, 1L);
    ArgumentCaptor<BooleanExpression> booleanExpressionArgumentCaptor = ArgumentCaptor
      .forClass(BooleanExpression.class);
    verify(ideaFavouritesRepositoryMock).findOne(booleanExpressionArgumentCaptor.capture());
    BooleanExpression booleanExpression = booleanExpressionArgumentCaptor.getValue();
    assertEquals("ideaFavourite.member = memberMock && ideaFavourite.kigaIdeaLive.id = 1",
      booleanExpression.toString());
  }
}
