package com.kiga.ideas.task.cache;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepositoryProxy;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by rainerh on 16.10.16.
 */
public class IdeasCacheExecutorTest {
  @Test
  public void test() {
    IdeaCategoryRepositoryProxy repositoryProxy =
      mock(IdeaCategoryRepositoryProxy.class);
    IdeasCacheActualizer actualizer = mock(IdeasCacheActualizer.class);
    IdeaGroupContainer ideaGroupContainer = new IdeaGroupContainerLive();
    when(repositoryProxy.findOne(anyLong())).thenReturn(ideaGroupContainer);

    IdeasCacheExecutor executor = new IdeasCacheExecutor(repositoryProxy, actualizer);
    executor.run(1L, new int[] {0, 1}, true);
    verify(actualizer, times(2)).run(eq(ideaGroupContainer), anyInt(), eq(true));
  }
}
