package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.handler;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.content.domain.ss.property.LayoutType;
import com.kiga.ideas.web.response.KigaIdeaViewModel;
import com.kiga.ideas.web.viewmodel.kigaidea.LinksReplacer;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;
import java.util.Arrays;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class KigaIdeaViewModelArticleTypeHandlerTest {
  private LinksReplacer linksReplacerMock;

  private KigaIdeaViewModelArticleTypeHandler kigaIdeaViewModelArticleTypeHandler;
  private KigaIdea kigaIdea;

  /** prepare tests. */
  @Before
  public void prepareTests() {
    linksReplacerMock = mock(LinksReplacer.class);
    when(linksReplacerMock.analyzeContentLinks(anyString())).thenReturn("TEST_IDEA_CONTENT");

    kigaIdeaViewModelArticleTypeHandler =
        new KigaIdeaViewModelArticleTypeHandler(linksReplacerMock);

    kigaIdea = new KigaIdeaLive();
    KigaS3File kigaS3File = new KigaS3File();
    kigaS3File.setName("TEST_S3_FILE_NAME");
    kigaS3File.setId(0L);
    kigaIdea.setUploadedFile(kigaS3File);
    S3Image s3Image = new S3Image();
    KigaPageImage kigaPageImage = new KigaPageImage();
    kigaPageImage.setS3Image(s3Image);
    kigaIdea.setKigaPageImages(Arrays.asList(kigaPageImage, kigaPageImage));
  }

  @Test
  public void testHandleArticleTypeDownload() {
    kigaIdea.setArticleType(ArticleType.DOWNLOAD);

    KigaIdeaViewModel kigaIdeaViewModel = new KigaIdeaViewModel();
    kigaIdeaViewModelArticleTypeHandler.handleArticleType(kigaIdeaViewModel, kigaIdea);

    Assert.assertEquals("TEST_S3_FILE_NAME", kigaIdeaViewModel.getUploadedFileName());
    Assert.assertEquals(0L, (long) kigaIdeaViewModel.getUploadedFileId());
    Assert.assertEquals(
        "<div class=\"container\">\n"
            + "<div class=\"rowDiv\">\n"
            + "<div class=\"span-4 last activeCell\" style=\"text-align:center;\">"
            + "<button  t=\"BUTTON_DOWNLOAD\" type=\"button\""
            + " ng-click=\"ctrl.downloadIdea()\">"
            + "</button>"
            + "</div></div></div>",
        kigaIdeaViewModel.getContent());
  }

  @Test
  public void testHandleArticleTypeNoDownloadDynamic() {
    kigaIdea.setLayoutType(LayoutType.DYNAMIC);

    KigaIdeaViewModel kigaIdeaViewModel = spy(new KigaIdeaViewModel());

    kigaIdeaViewModelArticleTypeHandler.handleArticleType(kigaIdeaViewModel, kigaIdea);

    verify(kigaIdeaViewModel).setContent(anyString());
    Assert.assertEquals("TEST_IDEA_CONTENT", kigaIdeaViewModel.getContent());
  }

  @Test
  public void testHandleArticleTypeNoDownloadNoDynamic() {
    kigaIdea.setLayoutType(LayoutType.STATIC_OLD);

    KigaIdeaViewModel kigaIdeaViewModel = spy(new KigaIdeaViewModel());

    kigaIdeaViewModelArticleTypeHandler.handleArticleType(kigaIdeaViewModel, kigaIdea);

    verify(kigaIdeaViewModel).setContent(anyString());
    Assert.assertEquals("TEST_IDEA_CONTENT", kigaIdeaViewModel.getContent());
  }
}
