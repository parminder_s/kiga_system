package com.kiga.ideas.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.property.PreviewImage;
import com.kiga.ideas.web.response.KigaIdeaViewModel;
import com.kiga.ideas.web.viewmodel.kigaidea.PreviewImagesSetter;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.S3ImageResizeService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by asv on 23.02.17.
 */
public class PreviewImagesSetterTest {
  private S3ImageResizeService s3ImageResizeService = mock(S3ImageResizeService.class);

  @Test
  public void setPreviewImagesTest() throws Exception {
    S3Image s3Image = new S3Image();
    KigaPageImage kigaPageImage = new KigaPageImage();
    kigaPageImage.setS3Image(s3Image);

    IdeasProperties ideasProperties = new IdeasProperties();
    PreviewImage previewImage = new PreviewImage();
    previewImage.setLarge(new ImageSize(50, 50));
    previewImage.setSmall(new ImageSize(20, 20));
    ideasProperties.setPreviewImage(previewImage);

    when(s3ImageResizeService
      .getResizedImageUrl(Mockito.any(), Mockito.any())).thenReturn("MOCK");

    KigaIdeaViewModel kigaIdeaViewModel = new KigaIdeaViewModel();
    PreviewImagesSetter previewImagesSetter = new PreviewImagesSetter(ideasProperties,
      s3ImageResizeService);
    previewImagesSetter.setPreviewImages(kigaIdeaViewModel, kigaPageImage);

    Assert.assertEquals(kigaIdeaViewModel.getPreviewUrl(), "MOCK");
    Assert.assertEquals(kigaIdeaViewModel.getLargePreviewImageUrl(), "MOCK");
  }

}



