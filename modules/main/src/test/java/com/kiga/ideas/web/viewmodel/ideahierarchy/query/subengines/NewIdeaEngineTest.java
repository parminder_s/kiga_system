package com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.favourites.repository.FavouriteNewIdeasRepository;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper.IdeaEngineHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import java.util.Collections;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Pageable;

public class NewIdeaEngineTest {

  private IdeasProperties ideasPropertiesMock;
  private FavouriteNewIdeasRepository favouriteNewIdeasRepositoryMock;
  private NewIdeasRepository newIdeasRepositoryMock;
  private IdeaEngineHelper ideaEngineHelperMock;
  private IdeaQueryParameter ideaQueryParameter;

  private NewIdeaEngine newIdeaEngine;

  /** Prepare Tests. */
  @Before
  public void prepareTests() {
    ideasPropertiesMock = mock(IdeasProperties.class);
    when(ideasPropertiesMock.getAmountPerCategory()).thenReturn(2);

    favouriteNewIdeasRepositoryMock = mock(FavouriteNewIdeasRepository.class);
    newIdeasRepositoryMock = mock(NewIdeasRepository.class);
    ideaEngineHelperMock = mock(IdeaEngineHelper.class);

    ideaQueryParameter = new IdeaQueryParameter();
    ideaQueryParameter.setLocale(Locale.en);
    ideaQueryParameter.setAgeGroupFilter(0);
    when(favouriteNewIdeasRepositoryMock.getIdeasForAllAgeGroups(
            any(Locale.class), anyInt(), any(Member.class), any(Pageable.class)))
        .thenReturn(Collections.emptyList());

    when(newIdeasRepositoryMock.getIdeasForAllAgeGroups(
            any(Locale.class), anyInt(), any(Pageable.class)))
        .thenReturn(Collections.emptyList());

    when(ideaEngineHelperMock.hasValidParent(any(KigaIdea.class))).thenReturn(true);

    newIdeaEngine =
        new NewIdeaEngine(
            ideasPropertiesMock, favouriteNewIdeasRepositoryMock,
            newIdeasRepositoryMock, ideaEngineHelperMock);
  }

  @Test
  public void getIdeasMember() {

    Member member = new Member();
    Optional optional = Optional.of(member);
    ideaQueryParameter.setMember(optional);

    Assert.assertEquals(Collections.emptyList(), newIdeaEngine.getIdeas(ideaQueryParameter));
    verify(ideasPropertiesMock).getAmountPerCategory();
    verify(favouriteNewIdeasRepositoryMock)
        .getIdeasForAllAgeGroups(
            any(Locale.class), anyInt(), any(Member.class), any(Pageable.class));
  }

  @Test
  public void getIdeasNoMember() {

    Optional optional = Optional.empty();
    ideaQueryParameter.setMember(optional);
    Assert.assertEquals(Collections.emptyList(), newIdeaEngine.getIdeas(ideaQueryParameter));
    verify(ideasPropertiesMock).getAmountPerCategory();
    verify(newIdeasRepositoryMock)
        .getIdeasForAllAgeGroups(any(Locale.class), anyInt(), any(Pageable.class));
  }
}
