package com.kiga.ideas.web.viewmodel.ideahierarchy.submanagers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.IdeaGroupViewModelMapper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.IdeaGroupQueryEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaGroupQueryResult;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import java.util.Collections;
import org.junit.Before;
import org.junit.Test;

public class IdeaGroupManagerTest {
  private IdeaGroupViewModelMapper ideaGroupViewModelMapperMock;
  private IdeaGroupQueryEngine ideaGroupQueryEngineMock;

  private IdeaGroupManager ideaGroupManager;
  private IdeaQueryParameter ideaQueryParameter;

  private IdeaCategory ideaCategory;
  private IdeaGroupQueryResult ideaGroupQueryResult;

  /** preparing tests. */
  @Before
  public void prepareTests() {
    ideaGroupQueryResult =
        IdeaGroupQueryResult.builder()
            .ideas(Collections.emptyList())
            .siblings(Collections.emptyList())
            .build();

    ideaGroupViewModelMapperMock = mock(IdeaGroupViewModelMapper.class);

    ideaGroupQueryEngineMock = mock(IdeaGroupQueryEngine.class);
    when(ideaGroupQueryEngineMock.query(any(IdeaQueryParameter.class)))
        .thenReturn(ideaGroupQueryResult);

    ideaGroupManager = new IdeaGroupManager(ideaGroupViewModelMapperMock, ideaGroupQueryEngineMock);

    ideaCategory = new IdeaCategoryLive();
    ideaQueryParameter = new IdeaQueryParameter();
    ideaQueryParameter.setIdeaCategory(ideaCategory);
  }

  @Test
  public void testGetViewModel() {
    ideaGroupManager.getViewModel(ideaQueryParameter);

    verify(ideaGroupViewModelMapperMock)
        .getViewModel(
            any(IdeaCategory.class), anyListOf(KigaIdea.class), anyListOf(IdeaCategory.class));
  }
}
