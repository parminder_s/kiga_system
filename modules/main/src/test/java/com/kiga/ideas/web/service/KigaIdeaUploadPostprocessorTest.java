package com.kiga.ideas.web.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.property.PreviewImage;
import com.kiga.kigaupload.web.response.UploadFileStatus;
import com.kiga.s3.repository.ss.S3ImageRepository;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.security.domain.Member;
import org.junit.Test;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Created by robert on 07.05.17.
 */
public class KigaIdeaUploadPostprocessorTest {
  private S3ImageResizeService s3ImageResizeServiceMock = mock(S3ImageResizeService.class);
  private S3ImageRepository s3ImageRepositoryMock = mock(S3ImageRepository.class);
  private IdeasProperties ideasPropertiesMock = mock(IdeasProperties.class);
  private KigaIdeaUploadPostprocessor kigaIdeaUploadPostprocessor =
    new KigaIdeaUploadPostprocessor(s3ImageResizeServiceMock, s3ImageRepositoryMock,
      ideasPropertiesMock);

  @Test
  public void testProcess() throws IOException {
    PreviewImage previewImageMock = mock(PreviewImage.class);
    ImageSize imageSize = new ImageSize(100, 100);
    when(previewImageMock.getLarge()).thenReturn(imageSize);
    when(ideasPropertiesMock.getPreviewImage()).thenReturn(previewImageMock);
    when(s3ImageResizeServiceMock.getResizedImageUrl(any(), any())).thenReturn("RESIZED_URL");
    UploadFileStatus uploadFileStatus = new UploadFileStatus();
    uploadFileStatus.setUrl("DEFAULT_URL");
    List<UploadFileStatus> uploadFileStatusListMock = Collections.singletonList(uploadFileStatus);
    List<MultipartFile> multipartFileListMock = mock(List.class);
    Member memberMock = mock(Member.class);
    kigaIdeaUploadPostprocessor
      .process(memberMock, multipartFileListMock, uploadFileStatusListMock);
    assertEquals("RESIZED_URL", uploadFileStatus.getUrl());
  }

  @Test
  public void testResizerThrowingException() throws IOException {
    PreviewImage previewImageMock = mock(PreviewImage.class);
    ImageSize imageSize = new ImageSize(100, 100);
    when(previewImageMock.getLarge()).thenReturn(imageSize);
    when(ideasPropertiesMock.getPreviewImage()).thenReturn(previewImageMock);
    when(s3ImageResizeServiceMock.getResizedImageUrl(any(), any())).thenThrow(IOException.class);
    UploadFileStatus uploadFileStatus = new UploadFileStatus();
    uploadFileStatus.setUrl("DIFFERENT_URL");
    List<UploadFileStatus> uploadFileStatusListMock = Collections.singletonList(uploadFileStatus);
    List<MultipartFile> multipartFileListMock = mock(List.class);
    Member memberMock = mock(Member.class);
    kigaIdeaUploadPostprocessor
      .process(memberMock, multipartFileListMock, uploadFileStatusListMock);
    assertEquals("DIFFERENT_URL", uploadFileStatus.getUrl());
  }
}
