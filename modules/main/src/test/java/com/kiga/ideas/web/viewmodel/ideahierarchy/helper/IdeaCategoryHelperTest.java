package com.kiga.ideas.web.viewmodel.ideahierarchy.helper;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.ideas.IdeasProperties;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IdeaCategoryHelperTest {
  private IdeasProperties ideasPropertiesMock;
  private IdeaCategory ideaCategory;

  private IdeaCategoryHelper ideaCategoryHelper;

  /** preparing tests. */
  @Before
  public void prepareTests() {
    ideasPropertiesMock = mock(IdeasProperties.class);
    ideaCategory = new IdeaCategoryLive();
    ideaCategoryHelper = new IdeaCategoryHelper(ideasPropertiesMock);
  }

  @Test
  public void testGetIconCode() {
    IdeaCategory ideaCategoryParent = new IdeaCategoryLive();
    ideaCategoryParent.setIconCode("TEST_PARENT_ICON_CODE");
    ideaCategoryParent.setClassName("IdeaGroupMainContainer");
    ideaCategory.setParent(ideaCategoryParent);

    Assert.assertEquals("icon-ideas", ideaCategoryHelper.getIconCode(ideaCategory));
  }

  @Test
  public void testGetIconCodeException() {
    IdeaCategory ideaCategoryParent = spy(new IdeaCategoryLive());
    when(ideaCategoryParent.getClassName()).thenThrow(new NullPointerException());
    ideaCategoryParent.setIconCode("TEST_PARENT_ICON_CODE");
    ideaCategoryParent.setClassName("IdeaGroupMainContainer");
    ideaCategory.setParent(ideaCategoryParent);

    Assert.assertEquals("icon-ideas", ideaCategoryHelper.getIconCode(ideaCategory));
  }

  @Test
  public void testGetIconCodeNotEmpty() {
    IdeaCategory ideaCategoryParent = new IdeaCategoryLive();
    ideaCategory.setIconCode("TEST_ICON_CODE");
    ideaCategoryParent.setIconCode("TEST_PARENT_ICON_CODE");
    ideaCategoryParent.setClassName("IdeaGroupMainContainer");
    ideaCategory.setParent(ideaCategoryParent);

    Assert.assertEquals("TEST_ICON_CODE", ideaCategoryHelper.getIconCode(ideaCategory));
  }

  @Test
  public void testGetDisableForFavourites() {
    IdeaCategory ideaCategoryParent = new IdeaCategoryLive();
    ideaCategoryParent.setClassName("IdeaGroupMainContainer");
    ideaCategoryParent.setDisableForFavourites(true);
    ideaCategory.setDisableForFavourites(true);
    ideaCategory.setParent(ideaCategoryParent);

    Assert.assertTrue(ideaCategoryHelper.getDisableForFavourites(ideaCategory));
  }

  @Test
  public void testGetDisableForFavouritesArticle() {
    IdeaCategory ideaCategoryParent = new IdeaCategoryLive();
    IdeaCategory ideaCategoryParentParent = new IdeaCategoryLive();
    ideaCategoryParentParent.setClassName("MainArticleCategory");
    ideaCategoryParentParent.setDisableForFavourites(true);
    ideaCategoryParent.setParent(ideaCategoryParentParent);
    ideaCategoryParent.setDisableForFavourites(true);
    ideaCategory.setDisableForFavourites(true);
    ideaCategory.setParent(ideaCategoryParent);

    Assert.assertTrue(ideaCategoryHelper.getDisableForFavourites(ideaCategory));
  }

  @Test
  public void testIsRedirect() {
    ideaCategory.setClassName("IdeaGroupMainContainer");
    when(ideasPropertiesMock.isSiblingsAsMenu()).thenReturn(true);
    Assert.assertTrue(ideaCategoryHelper.isRedirect(ideaCategory));
  }

  @Test
  public void testIsNewIdeaGroupCategory() {
    ideaCategory.setCode("new-ideas");
    Assert.assertTrue(ideaCategoryHelper.isNewIdeaGroupCategory(ideaCategory));
  }

  @Test
  public void testIsNewIdeaGroup() {
    ideaCategory.setCode("new-ideas-ig-41");
    Assert.assertTrue(ideaCategoryHelper.isNewIdeaGroup(ideaCategory));
  }

  @Test
  public void testIsIdeaGroup() {
    ideaCategory.setClassName("IdeaGroup");
    Assert.assertTrue(ideaCategoryHelper.isIdeaGroup(ideaCategory));
  }

  @Test
  public void testIsLastCategoryLevel() {
    ideaCategory.setClassName("IdeaGroup");
    Assert.assertTrue(ideaCategoryHelper.isLastCategoryLevel(ideaCategory));
  }
}
