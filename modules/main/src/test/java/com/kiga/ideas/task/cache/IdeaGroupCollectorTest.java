package com.kiga.ideas.task.cache;

import static org.junit.Assert.assertEquals;

import com.kiga.content.domain.ss.IdeaGroup;
import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by rainerh on 15.10.16.
 */
public class IdeaGroupCollectorTest {

  @Test
  public void testCollector() {
    IdeaGroupCollector collector = new IdeaGroupCollector();
    List<SiteTree> siteTrees = collector.collect(this.setupIdeaGroupHierarchy(), true);

    assertEquals(10, siteTrees.size());
    List<String> urlSegments = Arrays.asList(
      "ig1", "ig2", "category1", "category2", "subCat3Ig1",
      "subCat3Ig2", "subCategory3", "cat3Ig", "category3", "main");

    for (int i = 0; i < urlSegments.size(); i++) {
      assertEquals(
        "SiteTree at position " + i, urlSegments.get(i), siteTrees.get(i).getUrlSegment());
    }
  }

  @Test
  public void testWithoutRecursion() {
    IdeaGroupCollector collector = new IdeaGroupCollector();
    List<SiteTree> siteTrees = collector.collect(this.setupIdeaGroupHierarchy(), false);

    assertEquals(1, siteTrees.size());
    assertEquals("IdeaGroupMainContainer", siteTrees.get(0).getClassName());
  }

  /**
   * sets up a IdeaGroup Hierarchy with.
   * <pre>
   * main: IdeaGroupContainer
   *   - category1: IdeaGroupCategory
   *     - ig1: IdeaGrop
   *     - ig2: IdeaGroup
   *       - ig1Idea1: KigaIdea
   *       - igIdea2: KigaIdea
   *   - category2: IdeaGroupCategory
   *   - category3: IdeaGroupCategory
   *     - subCategory3: IdeaGroupCategory
   *       - subCat3Ig1: IdeaGroup
   *       - subCat3Ig2: IdeaGroup
   *     - cat3Ig: IdeaGroup
   *     - idea: KigaIdea
   * </pre>
   */
  public SiteTree setupIdeaGroupHierarchy() {
    SiteTree main = new IdeaGroupContainerLive();
    main.setClassName("IdeaGroupMainContainer");
    main.setUrlSegment("main");
    SiteTree category1 = new IdeaGroupContainerLive();
    category1.setClassName("IdeaGroupContainer");
    category1.setUrlSegment("category1");
    SiteTree ig1 = new IdeaGroupLive();
    ig1.setClassName("IdeaGroup");
    ig1.setUrlSegment("ig1");
    SiteTree ig2 = new IdeaGroupLive();
    ig2.setClassName("IdeaGroup");
    ig2.setUrlSegment("ig2");

    SiteTree ig1Idea1 = new KigaIdeaLive();
    ig1Idea1.setClassName("KigaIdea");
    SiteTree ig1Idea2 = new KigaIdeaLive();
    ig1Idea2.setClassName("KigaIdea");

    SiteTree category2 = new IdeaGroupContainerLive();
    category2.setClassName("IdeaGroupCategory");
    category2.setUrlSegment("category2");

    SiteTree category3 = new IdeaGroupContainerLive();
    category3.setClassName("IdeaGroupCategory");
    category3.setUrlSegment("category3");

    SiteTree subCategory3 = new IdeaGroupContainerLive();
    subCategory3.setClassName("IdeaGroupCategory");
    subCategory3.setUrlSegment("subCategory3");

    SiteTree subCat3Ig1 = new IdeaGroupLive();
    subCat3Ig1.setUrlSegment("subCat3Ig1");
    subCat3Ig1.setClassName("IdeaGroup");
    SiteTree subCat3Ig2 = new IdeaGroupLive();
    subCat3Ig2.setUrlSegment("subCat3Ig2");
    subCat3Ig2.setClassName("IdeaGroup");

    SiteTree cat3Ig = new IdeaGroupLive();
    cat3Ig.setUrlSegment("cat3Ig");
    cat3Ig.setClassName("IdeaGroup");

    SiteTree idea = new KigaIdeaLive();
    idea.setClassName("KigaIdea");

    main.setChildren(Arrays.asList(category1, category2, category3));
    category1.setChildren(Arrays.asList(ig1, ig2));
    ig2.setChildren(Arrays.asList(ig1Idea1, ig1Idea2));
    category3.setChildren(Arrays.asList(subCategory3, cat3Ig, idea));
    subCategory3.setChildren(Arrays.asList(subCat3Ig1, subCat3Ig2));

    return main;
  }
}
