package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.cms.web.response.RedirectViewModel;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.web.viewmodel.ideahierarchy.helper.IdeaCategoryHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaSet;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.Category;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.Idea;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.ParentCategoryViewModel;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Florian Schneider
 * @since 17.01.18
 */
public class IdeaGroupCategoryViewModelMapperTest {
  private IdeaGroupCategoryViewModelMapper ideaGroupCategoryViewModelMapper;
  private CategoryMapper categoryMapperMock;
  private IdeaMapper ideaMapperMock;
  private ParentCategoryViewModelMapper parentCategoryViewModelMapperMock;
  private IdeaCategoryRepositoryProxy ideaCategoryRepositoryMock;
  private SiteTreeUrlGenerator siteTreeUrlGeneratorMock;
  private IdeasProperties ideasProperties;
  private IdeaCategory activeGroup;
  private IdeaCategoryHelper ideaCategoryHelperMock;
  private List<IdeaSet> ideaSets;

  /** preparing tests. */
  @Before
  public void prepareTests() {

    Category groupViewModelMock = mock(Category.class);
    categoryMapperMock = mock(CategoryMapper.class);
    when(categoryMapperMock.convertIdeaCategory(any(IdeaCategory.class)))
        .thenReturn(groupViewModelMock);

    ideaMapperMock = mock(IdeaMapper.class);
    when(ideaMapperMock.convert(any(KigaIdea.class))).thenReturn(mock(Idea.class));

    parentCategoryViewModelMapperMock = mock(ParentCategoryViewModelMapper.class);
    when(parentCategoryViewModelMapperMock.convertSiblings(anyListOf(IdeaCategory.class)))
        .thenReturn(
            Arrays.asList(
                mock(ParentCategoryViewModel.class), mock(ParentCategoryViewModel.class)));

    ideaCategoryRepositoryMock = mock(IdeaCategoryRepositoryProxy.class);
    when(ideaCategoryRepositoryMock.findOne(anyInt())).thenReturn(mock(IdeaCategory.class));

    siteTreeUrlGeneratorMock = mock(SiteTreeUrlGenerator.class);
    when(siteTreeUrlGeneratorMock.getRelativeNgUrl(any(SiteTree.class)))
        .thenReturn("TEST_REDIRECT");

    ideaMapperMock = mock(IdeaMapper.class);
    ideaCategoryHelperMock = mock(IdeaCategoryHelper.class);

    ideasProperties = mock(IdeasProperties.class);
    when(ideasProperties.isSiblingsAsMenu()).thenReturn(true);

    ideaGroupCategoryViewModelMapper =
        new IdeaGroupCategoryViewModelMapper(
            ideaCategoryHelperMock,
            mock(ViewModelMetaDataProvider.class),
            siteTreeUrlGeneratorMock,
            ideaCategoryRepositoryMock,
            parentCategoryViewModelMapperMock,
            ideaMapperMock,
            categoryMapperMock);

    activeGroup = new IdeaGroupContainerLive();
    IdeaSet ideaSet =
        IdeaSet.builder()
            .ideaCategory(mock(IdeaCategory.class))
            .ideas(Arrays.asList(mock(KigaIdea.class), mock(KigaIdea.class)))
            .build();
    SiteTree mainContainer = new SiteTreeLive();
    mainContainer.setClassName("IdeaGroupMainContainer");
    activeGroup.setParent(mainContainer);
    ideaSets = Arrays.asList(ideaSet, ideaSet);
  }

  @Test
  public void testConvertIdeaCategory() {

    ideaGroupCategoryViewModelMapper.getViewModel(
        false,
        0,
        activeGroup,
        ideaSets,
        Arrays.asList(mock(IdeaCategory.class), mock(IdeaCategory.class)));

    // activeGroup + 2 mocks within ideaSet in stream
    verify(categoryMapperMock, times(3)).convertIdeaCategory(any(IdeaCategory.class));
    // 2 times per ideaSet
    verify(ideaMapperMock, times(4)).convert(any(KigaIdea.class));
    verify(parentCategoryViewModelMapperMock, times(1))
        .convertSiblings(anyListOf(IdeaCategory.class));
  }

  @Test
  public void testGetRedirectNoAgeGroupFilter() {
    when(ideaCategoryHelperMock.isRedirect(activeGroup)).thenReturn(true);
    activeGroup.setClassName("IdeaGroupMainContainer");
    ViewModel viewModel =
        ideaGroupCategoryViewModelMapper.getViewModel(
            false,
            0,
            activeGroup,
            ideaSets,
            Arrays.asList(mock(IdeaCategory.class), mock(IdeaCategory.class)));

    Assert.assertEquals(true, viewModel instanceof RedirectViewModel);
    Assert.assertEquals("TEST_REDIRECT", ((RedirectViewModel) viewModel).getRedirectTo());
  }

  @Test
  public void testGetRedirectWithAgeGroupFilter() {
    when(ideaCategoryHelperMock.isRedirect(activeGroup)).thenReturn(true);
    activeGroup.setClassName("IdeaGroupMainContainer");
    ViewModel viewModel =
        ideaGroupCategoryViewModelMapper.getViewModel(
            false,
            1,
            activeGroup,
            ideaSets,
            Arrays.asList(mock(IdeaCategory.class), mock(IdeaCategory.class)));

    Assert.assertEquals(true, viewModel instanceof RedirectViewModel);
    Assert.assertEquals(
        "TEST_REDIRECT?ageGroupFilter=1", ((RedirectViewModel) viewModel).getRedirectTo());
  }
}
