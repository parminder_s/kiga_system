package com.kiga.ideas.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.NewIdeasRepositoryProxy;
import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.RepositoryContexts;
import com.kiga.content.repository.ss.live.NewIdeasRepositoryLive;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.properties.IdeasRetrieveProperties;
import com.kiga.ideas.web.response.IdeasResponseGroup;
import com.kiga.ideas.web.service.IdeasResponseIdeaConverter;
import com.kiga.ideas.web.service.IdeasSeparateAgeGroupsConverter;
import com.kiga.main.locale.Locale;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.web.converter.ResponseConverter;
import com.kiga.web.service.DefaultUrlGenerator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * @author bbs
 * @since 7/16/16.
 */
public class NewIdeasGroupServiceImplTest {
  private IdeasProperties ideasProperties;
  private NewIdeasRepositoryLive newIdeasRepositoryLive;
  private NewIdeasGroupService newIdeasGroupService;
  private EntityManager entityManager;
  private IdeasSeparateAgeGroupsConverter ideasSeparateAgeGroupsConverterMock = mock(
    IdeasSeparateAgeGroupsConverter.class);

  /**
   * Prepare tests.
   */
  @Before
  public void prepare() {
    ideasProperties = new IdeasProperties();
    ideasProperties.setAmountPerCategory(50);
    newIdeasRepositoryLive = mock(NewIdeasRepositoryLive.class);

    RepositoryContext repositoryContext = mock(RepositoryContext.class);
    when(repositoryContext.getContext()).thenReturn(RepositoryContexts.LIVE);

    entityManager = mock(EntityManager.class);
    EntityManagerFactory entityManagerFactory = mock(EntityManagerFactory.class);
    when(entityManagerFactory.createEntityManager()).thenReturn(entityManager);

    NewIdeasRepositoryProxy ageGroupIdeasModel = new NewIdeasRepositoryProxy(newIdeasRepositoryLive,
      null,
      repositoryContext);

    SiteTreeUrlGenerator siteTreeUrlGenerator = mock(SiteTreeUrlGenerator.class);
    DefaultUrlGenerator urlGenerator = mock(DefaultUrlGenerator.class);
    when(siteTreeUrlGenerator.getUrl(any())).thenReturn("XYZ");

    ResponseConverter ideasResponseFactory = new IdeasResponseIdeaConverter(
      mock(S3ImageResizeService.class), ideasProperties, siteTreeUrlGenerator, urlGenerator, null,
      ideasSeparateAgeGroupsConverterMock);

    NewIdeasRepositoryProxy newIdeasModel = new NewIdeasRepositoryProxy(newIdeasRepositoryLive,
      null,
      repositoryContext);

    newIdeasGroupService = new NewIdeasGroupService(ideasResponseFactory, newIdeasModel,
      ideasProperties, null, null);
  }

  @Test
  public void testGetIdeasForAllAgeGroups() {
    IdeasRetrieveProperties ideasRetrieveProperties = new IdeasRetrieveProperties();
    ideasRetrieveProperties.setLocale(Locale.de);
    ideasRetrieveProperties.setAgeGroupFilter(0);
    newIdeasGroupService.getIdeasForAllAgeGroups(ideasRetrieveProperties);
    verify(newIdeasRepositoryLive, only())
      .getIdeasForAllAgeGroups(Locale.de, 0,
        new PageRequest(0, ideasProperties.getAmountPerCategory()));
  }

  @Test
  public void testGetAllIdeasByValue() {
    IdeasRetrieveProperties ideasRetrieveProperties = new IdeasRetrieveProperties();
    IdeasResponseGroup ideasResponseGroup = new IdeasResponseGroup();
    ideasRetrieveProperties.setGroup(ideasResponseGroup);
    ideasRetrieveProperties.setLocale(Locale.en);
    ideasRetrieveProperties.setAgeGroupFilter(0);

    ideasRetrieveProperties.getGroup().setCode("new-ideas-ig-1");
    newIdeasGroupService.getAllIdeasByValue(ideasRetrieveProperties);
    ideasRetrieveProperties.getGroup().setCode("new-ideas-ig-2");
    newIdeasGroupService.getAllIdeasByValue(ideasRetrieveProperties);
    ideasRetrieveProperties.getGroup().setCode("new-ideas-ig-4");
    newIdeasGroupService.getAllIdeasByValue(ideasRetrieveProperties);
    ideasRetrieveProperties.getGroup().setCode("new-ideas-ig-8");
    newIdeasGroupService.getAllIdeasByValue(ideasRetrieveProperties);
    ideasRetrieveProperties.getGroup().setCode("new-ideas-ig-16");
    newIdeasGroupService.getAllIdeasByValue(ideasRetrieveProperties);

    verify(
      newIdeasRepositoryLive, times(1))
      .getIdeasForAgeGroupByBitPosition(1, Locale.en, 0);
    verify(
      newIdeasRepositoryLive, times(1))
      .getIdeasForAgeGroupByBitPosition(2, Locale.en, 0);
    verify(
      newIdeasRepositoryLive, times(1))
      .getIdeasForAgeGroupByBitPosition(4, Locale.en, 0);
    verify(
      newIdeasRepositoryLive, times(1))
      .getIdeasForAgeGroupByBitPosition(8, Locale.en, 0);
    verify(
      newIdeasRepositoryLive, times(1))
      .getIdeasForAgeGroupByBitPosition(16, Locale.en, 0);
    verifyNoMoreInteractions(newIdeasRepositoryLive);
  }

  @Test
  public void getNewIdeasUrlTest() {
    IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxy = mock(
      IdeaCategoryRepositoryProxy.class);
    SiteTreeUrlGenerator siteTreeUrlGenerator = mock(SiteTreeUrlGenerator.class);
    IdeaCategory ideaCategory = new IdeaCategoryLive();
    ideaCategory.setUrlSegment("new-ideas");

    when(ideaCategoryRepositoryProxy.findByCodeAndLocale(eq("new-ideas"), eq(Locale.en)))
      .thenReturn(ideaCategory);
    when(siteTreeUrlGenerator.getRelativeNgUrl(any())).thenReturn("new-ideas-url");
    NewIdeasGroupService newIdeasGroupService = new NewIdeasGroupService(null, null, null,
      ideaCategoryRepositoryProxy, siteTreeUrlGenerator);
    Assert.assertEquals("new-ideas-url", newIdeasGroupService.getNewIdeasUrl(Locale.de));
  }
}
