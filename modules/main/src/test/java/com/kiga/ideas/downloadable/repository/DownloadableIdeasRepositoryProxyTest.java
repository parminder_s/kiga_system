package com.kiga.ideas.downloadable.repository;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.RepositoryContexts;
import com.kiga.main.locale.Locale;
import org.junit.Test;

/**
 * @author bbs
 * @since 4/7/17.
 */
public class DownloadableIdeasRepositoryProxyTest {
  private DownloadableIdeasRepositoryLive liveRepositoryMock =
    mock(DownloadableIdeasRepositoryLive.class);
  private DownloadableIdeasRepositoryDraft draftRepositoryMock =
    mock(DownloadableIdeasRepositoryDraft.class);
  private RepositoryContext repositoryContextMock = mock(RepositoryContext.class);
  private DownloadableIdeasRepositoryProxy downloadableIdeasRepositoryProxy =
    new DownloadableIdeasRepositoryProxy(liveRepositoryMock, draftRepositoryMock,
      repositoryContextMock);

  @Test
  public void findAllDownloadableIdeasLive() {
    Locale locale = mock(Locale.class);
    when(repositoryContextMock.getContext()).thenReturn(RepositoryContexts.LIVE);
    downloadableIdeasRepositoryProxy.findAllDownloadableIdeas(locale);
    verify(liveRepositoryMock).findAllDownloadableIdeas(locale);
    verify(draftRepositoryMock, never()).findAllDownloadableIdeas(locale);
  }

  @Test
  public void findAllDownloadableIdeasDraft() {
    Locale locale = mock(Locale.class);
    when(repositoryContextMock.getContext()).thenReturn(RepositoryContexts.DRAFT);
    downloadableIdeasRepositoryProxy.findAllDownloadableIdeas(locale);
    verify(draftRepositoryMock).findAllDownloadableIdeas(locale);
    verify(liveRepositoryMock, never()).findAllDownloadableIdeas(locale);
  }

  @Test
  public void getOneDownloadableLive() {
    Long id = 1L;
    when(repositoryContextMock.getContext()).thenReturn(RepositoryContexts.LIVE);
    downloadableIdeasRepositoryProxy.getOneDownloadable(id);
    verify(liveRepositoryMock).getOneDownloadable(id);
    verify(draftRepositoryMock, never()).getOneDownloadable(id);
  }

  @Test
  public void getOneDownloadableDraft() {
    Long id = 1L;
    when(repositoryContextMock.getContext()).thenReturn(RepositoryContexts.DRAFT);
    downloadableIdeasRepositoryProxy.getOneDownloadable(id);
    verify(draftRepositoryMock).getOneDownloadable(id);
    verify(liveRepositoryMock, never()).getOneDownloadable(id);
  }
}
