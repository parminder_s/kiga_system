package com.kiga.ideas.web;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.ideas.service.IdeaGroupService;
import com.kiga.main.locale.Locale;
import com.kiga.web.request.IdRequest;
import org.junit.Test;

/**
 * Created by robert on 08.06.17.
 */
public class IdeaGroupsControllerTest {
  private IdeaGroupService ideaGroupServiceMock = mock(IdeaGroupService.class);
  private IdeaGroupsController ideaGroupsController =
    new IdeaGroupsController(ideaGroupServiceMock);

  @Test
  public void testGetAssigned() {
    IdRequest idRequest = new IdRequest();
    idRequest.setId(123L);
    idRequest.setLocale(Locale.de);
    ideaGroupsController.getAssigned(idRequest);
    verify(ideaGroupServiceMock).getAssigned(123L, Locale.de);
  }

  @Test
  public void testGetUnassigned() {
    IdRequest idRequest = new IdRequest();
    idRequest.setId(123L);
    idRequest.setLocale(Locale.de);
    ideaGroupsController.getUnassigned(idRequest);
    verify(ideaGroupServiceMock).getUnassigned(123L, Locale.de);
  }

  @Test
  public void testGetUnassignedIdNull() {
    IdRequest idRequest = new IdRequest();
    idRequest.setLocale(Locale.de);
    ideaGroupsController.getUnassigned(idRequest);
    verify(ideaGroupServiceMock).getUnassigned(null, Locale.de);
  }

  @Test
  public void testGetAssignedTranslated() {
    IdRequest idRequest = new IdRequest();
    idRequest.setId(123L);
    idRequest.setLocale(Locale.de);
    ideaGroupsController.getTranslatedAssigned(idRequest);
    verify(ideaGroupServiceMock).getTranslatedAssigned(123L, Locale.de);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetAssignedTranslatedWithNullIdThrowingException() {
    IdRequest idRequest = new IdRequest();
    idRequest.setLocale(Locale.de);
    ideaGroupsController.getTranslatedAssigned(idRequest);
  }
}
