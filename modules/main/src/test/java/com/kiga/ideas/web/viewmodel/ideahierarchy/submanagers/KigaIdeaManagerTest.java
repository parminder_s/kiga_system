package com.kiga.ideas.web.viewmodel.ideahierarchy.submanagers;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.ideas.web.response.KigaIdeaViewModel;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.KigaIdeaViewModelMapper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import org.junit.Before;
import org.junit.Test;

public class KigaIdeaManagerTest {
  private KigaIdeaManager kigaIdeaManager;
  private KigaIdeaViewModelMapper kigaIdeaViewModelMapperMock;
  private IdeaQueryParameter ideaQueryParameterMock;

  /** prepare tests. */
  @Before
  public void prepareTests() throws Exception {
    ideaQueryParameterMock = mock(IdeaQueryParameter.class);
    when(ideaQueryParameterMock.getIdea()).thenReturn(mock(KigaIdea.class));
    kigaIdeaViewModelMapperMock = mock(KigaIdeaViewModelMapper.class);
    when(kigaIdeaViewModelMapperMock.convertToViewModel(any(KigaIdea.class)))
        .thenReturn(mock(KigaIdeaViewModel.class));
    kigaIdeaManager = new KigaIdeaManager(kigaIdeaViewModelMapperMock);
  }

  @Test
  public void testGetViewModel() throws Exception {

    kigaIdeaManager.getViewModel(ideaQueryParameterMock);
    verify(kigaIdeaViewModelMapperMock).convertToViewModel(any(KigaIdea.class));
  }
}
