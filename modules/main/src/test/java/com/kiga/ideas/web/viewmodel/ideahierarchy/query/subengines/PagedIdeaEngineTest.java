package com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.property.SortStrategy;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.KigaIdeaRepositoryProxy;
import com.kiga.favourites.repository.FavouriteKigaIdeaRepository;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Pageable;

public class PagedIdeaEngineTest {
  private IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxyMock;
  private IdeasProperties ideasPropertiesMock;
  private KigaIdeaRepositoryProxy kigaIdeaRepositoryProxyMock;
  private FavouriteKigaIdeaRepository favouriteKigaIdeaRepositoryMock;
  private IdeaQueryParameter ideaQueryParameter;
  private IdeaCategory ideaCategory;

  private PagedIdeaEngine pagedIdeaEngine;

  /** Preparing Tests. */
  @Before
  public void prepareTests() {

    ideaCategory = new IdeaCategoryLive();
    ideaCategory.setId(7L);
    ideaCategory.setSortTree("AAAA");
    ideaCategory.setSortStrategy(SortStrategy.SEQUENCE);

    ideaQueryParameter = new IdeaQueryParameter();
    ideaQueryParameter.setIdeaCategory(ideaCategory);
    ideaQueryParameter.setLocale(Locale.en);
    ideaQueryParameter.setAgeGroupFilter(0);

    ideasPropertiesMock = mock(IdeasProperties.class);
    when(ideasPropertiesMock.getAmountPerCategory()).thenReturn(2);
    ideaCategoryRepositoryProxyMock = mock(IdeaCategoryRepositoryProxy.class);
    when(ideaCategoryRepositoryProxyMock.findById(7L)).thenReturn(Arrays.asList(ideaCategory));

    kigaIdeaRepositoryProxyMock = mock(KigaIdeaRepositoryProxy.class);
    when(kigaIdeaRepositoryProxyMock.findPagedForGroup(
            any(Locale.class), anyLong(), anyInt(), anyInt(), any(Pageable.class)))
        .thenReturn(Collections.emptyList());
    when(kigaIdeaRepositoryProxyMock.findPagedForGroupSortTree(
            any(Locale.class), anyString(), anyInt(), any(Pageable.class)))
        .thenReturn(Collections.emptyList());
    favouriteKigaIdeaRepositoryMock = mock(FavouriteKigaIdeaRepository.class);
    when(favouriteKigaIdeaRepositoryMock.findPagedForGroupSortTree(
            any(Locale.class), anyString(), anyInt(), any(Member.class), any(Pageable.class)))
        .thenReturn(Collections.singletonList(new KigaIdeaLive()));
    when(favouriteKigaIdeaRepositoryMock.findPagedForGroup(
            any(Locale.class), anyLong(), anyInt(), anyInt(), any(Member.class)))
        .thenReturn(Collections.singletonList(new KigaIdeaLive()));

    pagedIdeaEngine =
        new PagedIdeaEngine(
            ideaCategoryRepositoryProxyMock,
            ideasPropertiesMock,
            kigaIdeaRepositoryProxyMock,
            favouriteKigaIdeaRepositoryMock);
  }

  @Test
  public void testGetIdeasUseSortTree() {
    when(ideasPropertiesMock.isUseSortTree()).thenReturn(true);

    Assert.assertEquals(Collections.emptyList(), pagedIdeaEngine.getIdeas(ideaQueryParameter));
    verify(ideasPropertiesMock).getAmountPerCategory();
    verify(ideasPropertiesMock).isUseSortTree();

    verify(kigaIdeaRepositoryProxyMock)
        .findPagedForGroupSortTree(any(Locale.class), anyString(), anyInt(), any(Pageable.class));
  }

  @Test
  public void testGetIdeasNoSortTree() {
    when(ideasPropertiesMock.isUseSortTree()).thenReturn(false);

    Assert.assertEquals(Collections.emptyList(), pagedIdeaEngine.getIdeas(ideaQueryParameter));
    verify(ideasPropertiesMock).getAmountPerCategory();
    verify(ideasPropertiesMock).isUseSortTree();

    verify(kigaIdeaRepositoryProxyMock)
        .findPagedForGroup(any(Locale.class), anyLong(), anyInt(), anyInt(), any(Pageable.class));
  }

  @Test
  public void testGetIdeasUseSortTreeFavourite() {
    when(ideasPropertiesMock.isUseSortTree()).thenReturn(true);
    ideaQueryParameter.setMember(Optional.of(mock(Member.class)));

    Assert.assertEquals(1, pagedIdeaEngine.getIdeas(ideaQueryParameter).size());
    verify(ideasPropertiesMock).getAmountPerCategory();
    verify(ideasPropertiesMock).isUseSortTree();

    verify(favouriteKigaIdeaRepositoryMock)
        .findPagedForGroupSortTree(
            any(Locale.class), anyString(), anyInt(), any(Member.class), any(Pageable.class));
  }

  @Test
  public void testGetIdeasNoSortTreeFavourite() {
    when(ideasPropertiesMock.isUseSortTree()).thenReturn(false);
    ideaQueryParameter.setMember(Optional.of(mock(Member.class)));

    Assert.assertEquals(1, pagedIdeaEngine.getIdeas(ideaQueryParameter).size());
    verify(ideasPropertiesMock).getAmountPerCategory();
    verify(ideasPropertiesMock).isUseSortTree();

    verify(favouriteKigaIdeaRepositoryMock)
        .findPagedForGroup(any(Locale.class), anyLong(), anyInt(), anyInt(), any(Member.class));
  }
}
