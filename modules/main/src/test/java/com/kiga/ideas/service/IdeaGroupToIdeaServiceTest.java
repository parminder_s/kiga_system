package com.kiga.ideas.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.property.SortStrategy;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.KigaIdeaRepositoryProxy;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.properties.IdeasRetrieveProperties;
import com.kiga.ideas.web.response.IdeasResponseGroup;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.main.locale.Locale;
import com.kiga.web.converter.ResponseConverter;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by rainerh on 12.09.16.
 */
public class IdeaGroupToIdeaServiceTest {
  @Test
  public void getAllIdeas() throws Exception {
    IdeaCategoryRepositoryProxy ideaCategoryRepository = mock(IdeaCategoryRepositoryProxy.class);
    IdeaCategory ideaCategory = new IdeaCategoryLive();
    ideaCategory.setId(5L);
    ideaCategory.setSortStrategy(SortStrategy.SEQUENCE);
    when(ideaCategoryRepository.findById(anyLong())).thenReturn(Arrays.asList(ideaCategory));

    KigaIdea kigaIdea = new KigaIdeaLive();

    KigaIdeaRepositoryProxy kigaIdeaRepository = mock(KigaIdeaRepositoryProxy.class);
    when(kigaIdeaRepository
      .findAllForGroup(eq(5L), eq(0), any())).thenReturn(Arrays.asList(kigaIdea));
    IdeasViewModelIdea ideasResponseIdea = new IdeasViewModelIdea();

    ResponseConverter converter = mock(ResponseConverter.class);
    when(converter.convertToResponse(any())).thenReturn(ideasResponseIdea);

    IdeasRetrieveProperties ideasRetrieveProperties = new IdeasRetrieveProperties();
    ideasRetrieveProperties.setGroup(new IdeasResponseGroup());
    ideasRetrieveProperties.getGroup().setId(5L);
    ideasRetrieveProperties.setAgeGroupFilter(0);

    List<IdeasViewModelIdea> ideasResponseIdeas = new IdeaGroupToIdeaService(
      kigaIdeaRepository, converter, null, new DefaultIdeaCategorySortToDbSort(),
      ideaCategoryRepository).getAllIdeas(ideasRetrieveProperties);

    Assert.assertEquals(1, ideasResponseIdeas.size());
    Assert.assertEquals(ideasResponseIdea, ideasResponseIdeas.get(0));
  }

  @Test
  public void getPagedIdeas() throws Exception {
    IdeaCategoryRepositoryProxy ideaCategoryRepository = mock(IdeaCategoryRepositoryProxy.class);
    IdeaCategory ideaCategory = new IdeaCategoryLive();
    ideaCategory.setId(5L);
    ideaCategory.setSortStrategy(SortStrategy.SEQUENCE);
    when(ideaCategoryRepository.findById(anyLong())).thenReturn(Arrays.asList(ideaCategory));

    KigaIdea kigaIdea = new KigaIdeaLive();

    KigaIdeaRepositoryProxy kigaIdeaRepository = mock(KigaIdeaRepositoryProxy.class);
    when(kigaIdeaRepository.findPagedForGroup(
      eq(Locale.en), eq(5L), eq(0), eq(0), any())).thenReturn(Arrays.asList(kigaIdea));
    IdeasViewModelIdea ideasResponseIdea = new IdeasViewModelIdea();

    ResponseConverter converter = mock(ResponseConverter.class);
    when(converter.convertToResponse(any())).thenReturn(ideasResponseIdea);

    IdeasProperties ideasProperties = new IdeasProperties();
    ideasProperties.setAmountPerCategory(10);

    IdeasRetrieveProperties ideasRetrieveProperties = new IdeasRetrieveProperties();
    ideasRetrieveProperties.setLocale(Locale.en);
    ideasRetrieveProperties.setGroup(new IdeasResponseGroup());
    ideasRetrieveProperties.getGroup().setId(5L);
    ideasRetrieveProperties.setLevel(0);
    ideasRetrieveProperties.setAgeGroupFilter(0);

    List<IdeasViewModelIdea> ideasResponseIdeas = new IdeaGroupToIdeaService(
      kigaIdeaRepository, converter, ideasProperties, new DefaultIdeaCategorySortToDbSort(),
      ideaCategoryRepository).getPagedIdeas(ideasRetrieveProperties);

    Assert.assertEquals(1, ideasResponseIdeas.size());
    Assert.assertEquals(ideasResponseIdea, ideasResponseIdeas.get(0));
  }

}
