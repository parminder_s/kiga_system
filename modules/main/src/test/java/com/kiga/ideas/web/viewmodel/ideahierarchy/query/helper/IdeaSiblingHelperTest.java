package com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.FeaturesProperties;
import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.ideas.web.viewmodel.ideahierarchy.helper.IdeaCategoryHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.IdeaQueryEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.NewIdeaBlockViewEngine;
import org.junit.Before;
import org.junit.Test;

public class IdeaSiblingHelperTest {

  private IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxyMock;
  private IdeaQueryParameter ideaQueryParameter;

  private IdeaCategory ideaCategoryParent;
  private IdeaCategory ideaCategory;
  private IdeaSiblingHelper ideaSiblingHelper;

  /** preparing tests. */
  @Before
  public void prepareTests() {
    ideaCategoryRepositoryProxyMock = mock(IdeaCategoryRepositoryProxy.class);
    ideaSiblingHelper =
        new IdeaSiblingHelper(
            ideaCategoryRepositoryProxyMock,
            mock(IdeaQueryEngine.class),
            mock(NewIdeaBlockViewEngine.class),
            mock(FeaturesProperties.class),
            mock(IdeaCategoryHelper.class));

    ideaCategoryParent = new IdeaCategoryLive();
    ideaCategoryParent.setId(0L);
    ideaCategory = new IdeaCategoryLive();
    ideaCategory.setParent(ideaCategoryParent);

    ideaQueryParameter = new IdeaQueryParameter();
    ideaQueryParameter.setIdeaCategory(ideaCategory);
  }

  @Test
  public void testGetSiblings() {
    ideaSiblingHelper.getSiblings(ideaQueryParameter);

    verify(ideaCategoryRepositoryProxyMock).findByParentIdOrderBySortAsc(anyLong());
  }
}
