package com.kiga.ideas.service;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.draft.IdeaCategoryDraft;
import com.kiga.content.domain.ss.draft.SiteTreeTranslationGroups;
import com.kiga.content.repository.ss.draft.IdeaCategoryRepository;
import com.kiga.content.repository.ss.draft.SiteTreeTranslationGroupsRepository;
import com.kiga.ideas.web.converter.IdeaCategoryToIdeasResponseGroupConverter;
import com.kiga.ideas.web.converter.IdeaCategoryToIdeasResponseGroupWithParentsConverter;
import com.kiga.main.locale.Locale;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by robert on 09.09.17.
 */
public class CategoryTranslationServiceTest {
  private IdeaCategoryRepository ideaCategoryRepositoryMock = mock(IdeaCategoryRepository.class);
  private IdeaCategoryToIdeasResponseGroupConverter ideaCategoryToIdeasResponseGroupConverterMock =
    mock(IdeaCategoryToIdeasResponseGroupConverter.class);
  private IdeaCategoryToIdeasResponseGroupWithParentsConverter
    ideaCategoryToIdeasResponseGroupConverterWithParentsMock =
    mock(IdeaCategoryToIdeasResponseGroupWithParentsConverter.class);
  private SiteTreeTranslationGroupsRepository translationGroupsRepositoryMock =
    mock(SiteTreeTranslationGroupsRepository.class);

  private CategoryTranslationService categoryTranslationService =
    new CategoryTranslationService(ideaCategoryRepositoryMock,
      ideaCategoryToIdeasResponseGroupConverterMock,
      ideaCategoryToIdeasResponseGroupConverterWithParentsMock, translationGroupsRepositoryMock);

  private SiteTreeTranslationGroups createSiteTreeTranslationGroups(long originalId) {
    SiteTreeTranslationGroups siteTreeTranslationGroups = new SiteTreeTranslationGroups();
    siteTreeTranslationGroups.setOriginalId(originalId);
    return siteTreeTranslationGroups;
  }

  @Test
  public void testTranslateSingle() {
    SiteTreeTranslationGroups siteTreeTranslationGroups = new SiteTreeTranslationGroups();
    siteTreeTranslationGroups.setTranslationGroupId(2L);
    when(translationGroupsRepositoryMock.findByOriginalId(1L))
      .thenReturn(siteTreeTranslationGroups);
    when(translationGroupsRepositoryMock.findAllByTranslationGroupId(2L)).thenReturn(Arrays
      .asList(createSiteTreeTranslationGroups(3L), createSiteTreeTranslationGroups(4L),
        createSiteTreeTranslationGroups(5L)));
    IdeaCategoryDraft ideaCategoryDraftMock = mock(IdeaCategoryDraft.class);
    when(ideaCategoryRepositoryMock.findByLocaleAndIdIn(Locale.de, Arrays.asList(3L, 4L, 5L)))
      .thenReturn(ideaCategoryDraftMock);
    categoryTranslationService.translate(1L, Locale.de);
    verify(ideaCategoryToIdeasResponseGroupConverterMock).convertToResponse(ideaCategoryDraftMock);
  }

  @Test
  public void testTranslateList() {
    SiteTreeTranslationGroups siteTreeTranslationGroups1 = new SiteTreeTranslationGroups();
    siteTreeTranslationGroups1.setTranslationGroupId(2L);
    when(translationGroupsRepositoryMock.findByOriginalId(1L))
      .thenReturn(siteTreeTranslationGroups1);

    SiteTreeTranslationGroups siteTreeTranslationGroups2 = new SiteTreeTranslationGroups();
    siteTreeTranslationGroups2.setTranslationGroupId(22L);
    when(translationGroupsRepositoryMock.findByOriginalId(11L))
      .thenReturn(siteTreeTranslationGroups2);

    SiteTreeTranslationGroups siteTreeTranslationGroups3 = new SiteTreeTranslationGroups();
    siteTreeTranslationGroups3.setTranslationGroupId(222L);
    when(translationGroupsRepositoryMock.findByOriginalId(111L))
      .thenReturn(siteTreeTranslationGroups3);

    when(translationGroupsRepositoryMock.findAllByTranslationGroupId(2L)).thenReturn(Arrays
      .asList(createSiteTreeTranslationGroups(3L), createSiteTreeTranslationGroups(4L),
        createSiteTreeTranslationGroups(5L)));
    when(translationGroupsRepositoryMock.findAllByTranslationGroupId(22L)).thenReturn(Arrays
      .asList(createSiteTreeTranslationGroups(33L), createSiteTreeTranslationGroups(44L),
        createSiteTreeTranslationGroups(55L)));
    when(translationGroupsRepositoryMock.findAllByTranslationGroupId(222L)).thenReturn(Arrays
      .asList(createSiteTreeTranslationGroups(333L), createSiteTreeTranslationGroups(444L),
        createSiteTreeTranslationGroups(555L)));

    IdeaCategoryDraft ideaCategoryDraftMock1 = mock(IdeaCategoryDraft.class);
    IdeaCategoryDraft ideaCategoryDraftMock2 = mock(IdeaCategoryDraft.class);
    IdeaCategoryDraft ideaCategoryDraftMock3 = mock(IdeaCategoryDraft.class);

    when(ideaCategoryRepositoryMock.findByLocaleAndIdIn(Locale.de, Arrays.asList(3L, 4L, 5L)))
      .thenReturn(ideaCategoryDraftMock1);
    when(ideaCategoryRepositoryMock.findByLocaleAndIdIn(Locale.de, Arrays.asList(33L, 44L, 55L)))
      .thenReturn(ideaCategoryDraftMock2);
    when(ideaCategoryRepositoryMock.findByLocaleAndIdIn(Locale.de, Arrays.asList(333L, 444L, 555L)))
      .thenReturn(ideaCategoryDraftMock3);

    categoryTranslationService.translate(Arrays.asList(1L, 11L, 111L), Locale.de);

    verify(ideaCategoryToIdeasResponseGroupConverterMock).convertToResponse(ideaCategoryDraftMock1);
    verify(ideaCategoryToIdeasResponseGroupConverterMock).convertToResponse(ideaCategoryDraftMock2);
    verify(ideaCategoryToIdeasResponseGroupConverterMock).convertToResponse(ideaCategoryDraftMock3);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTranslateListNullCategoryId() {
    categoryTranslationService.translate(null, Locale.de);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTranslateListNullLocale() {
    categoryTranslationService.translate(Collections.emptyList(), null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTranslateSingleNullLocale() {
    categoryTranslationService.translate(0, null);
  }

  @Test
  public void testTranslateWithParentsSingleNoEntity() {
    assertNull(categoryTranslationService.translateWithParents(1, Locale.de));
  }

  @Test
  public void testTranslateWithParentsSingle() {
    SiteTreeTranslationGroups siteTreeTranslationGroups = new SiteTreeTranslationGroups();
    siteTreeTranslationGroups.setTranslationGroupId(2L);
    when(translationGroupsRepositoryMock.findByOriginalId(1L))
      .thenReturn(siteTreeTranslationGroups);
    when(translationGroupsRepositoryMock.findAllByTranslationGroupId(2L)).thenReturn(Arrays
      .asList(createSiteTreeTranslationGroups(3L), createSiteTreeTranslationGroups(4L),
        createSiteTreeTranslationGroups(5L)));
    IdeaCategoryDraft ideaCategoryDraftMock = mock(IdeaCategoryDraft.class);
    when(ideaCategoryRepositoryMock.findByLocaleAndIdIn(Locale.de, Arrays.asList(3L, 4L, 5L)))
      .thenReturn(ideaCategoryDraftMock);
    categoryTranslationService.translateWithParents(1L, Locale.de);
    verify(ideaCategoryToIdeasResponseGroupConverterWithParentsMock)
      .convertToResponse(ideaCategoryDraftMock);
  }

  @Test
  public void testTranslateListWithParents() {
    SiteTreeTranslationGroups siteTreeTranslationGroups1 = new SiteTreeTranslationGroups();
    siteTreeTranslationGroups1.setTranslationGroupId(2L);
    when(translationGroupsRepositoryMock.findByOriginalId(1L))
      .thenReturn(siteTreeTranslationGroups1);

    SiteTreeTranslationGroups siteTreeTranslationGroups2 = new SiteTreeTranslationGroups();
    siteTreeTranslationGroups2.setTranslationGroupId(22L);
    when(translationGroupsRepositoryMock.findByOriginalId(11L))
      .thenReturn(siteTreeTranslationGroups2);

    SiteTreeTranslationGroups siteTreeTranslationGroups3 = new SiteTreeTranslationGroups();
    siteTreeTranslationGroups3.setTranslationGroupId(222L);
    when(translationGroupsRepositoryMock.findByOriginalId(111L))
      .thenReturn(siteTreeTranslationGroups3);

    when(translationGroupsRepositoryMock.findAllByTranslationGroupId(2L)).thenReturn(Arrays
      .asList(createSiteTreeTranslationGroups(3L), createSiteTreeTranslationGroups(4L),
        createSiteTreeTranslationGroups(5L)));
    when(translationGroupsRepositoryMock.findAllByTranslationGroupId(22L)).thenReturn(Arrays
      .asList(createSiteTreeTranslationGroups(33L), createSiteTreeTranslationGroups(44L),
        createSiteTreeTranslationGroups(55L)));
    when(translationGroupsRepositoryMock.findAllByTranslationGroupId(222L)).thenReturn(Arrays
      .asList(createSiteTreeTranslationGroups(333L), createSiteTreeTranslationGroups(444L),
        createSiteTreeTranslationGroups(555L)));

    IdeaCategoryDraft ideaCategoryDraftMock1 = mock(IdeaCategoryDraft.class);
    IdeaCategoryDraft ideaCategoryDraftMock2 = mock(IdeaCategoryDraft.class);
    IdeaCategoryDraft ideaCategoryDraftMock3 = mock(IdeaCategoryDraft.class);

    when(ideaCategoryRepositoryMock.findByLocaleAndIdIn(Locale.de, Arrays.asList(3L, 4L, 5L)))
      .thenReturn(ideaCategoryDraftMock1);
    when(ideaCategoryRepositoryMock.findByLocaleAndIdIn(Locale.de, Arrays.asList(33L, 44L, 55L)))
      .thenReturn(ideaCategoryDraftMock2);
    when(ideaCategoryRepositoryMock.findByLocaleAndIdIn(Locale.de, Arrays.asList(333L, 444L, 555L)))
      .thenReturn(ideaCategoryDraftMock3);

    categoryTranslationService.translateWithParents(Arrays.asList(1L, 11L, 111L), Locale.de);

    verify(ideaCategoryToIdeasResponseGroupConverterWithParentsMock)
      .convertToResponse(ideaCategoryDraftMock1);
    verify(ideaCategoryToIdeasResponseGroupConverterWithParentsMock)
      .convertToResponse(ideaCategoryDraftMock2);
    verify(ideaCategoryToIdeasResponseGroupConverterWithParentsMock)
      .convertToResponse(ideaCategoryDraftMock3);
  }
}
