package com.kiga.ideas.web.viewmodel.ideahierarchy;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.IdeaGroup;
import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.ideas.web.viewmodel.ideahierarchy.builder.IdeaQueryParameterBuilder;
import com.kiga.ideas.web.viewmodel.ideahierarchy.helper.IdeaCategoryHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.ideas.web.viewmodel.ideahierarchy.submanagers.IdeaGroupCategoryManager;
import com.kiga.ideas.web.viewmodel.ideahierarchy.submanagers.IdeaGroupManager;
import com.kiga.ideas.web.viewmodel.ideahierarchy.submanagers.KigaIdeaManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Test;

public class IdeaHierarchyManagerTest {
  private QueryRequestParameters queryRequestParameters;
  private IdeaQueryParameterBuilder ideaQueryParameterBuilderMock;
  private IdeaGroupManager ideaGroupManagerMock;
  private IdeaGroupCategoryManager ideaGroupCategoryManagerMock;
  private IdeaCategoryHelper ideaCategoryHelperMock;
  private KigaIdeaManager kigaIdeaManagerMock;
  private IdeaQueryParameter ideaQueryParameterMock;

  /** prepare tests. */
  @Before
  public void prepareTests() {
    ideaQueryParameterMock = mock(IdeaQueryParameter.class);
    kigaIdeaManagerMock = mock(KigaIdeaManager.class);
    ideaCategoryHelperMock = mock(IdeaCategoryHelper.class);
    ideaGroupManagerMock = mock(IdeaGroupManager.class);
    ideaGroupCategoryManagerMock = mock(IdeaGroupCategoryManager.class);
    ideaQueryParameterBuilderMock = mock(IdeaQueryParameterBuilder.class);
    when(ideaQueryParameterBuilderMock.buildQuery(any(QueryRequestParameters.class)))
        .thenReturn(ideaQueryParameterMock);
    queryRequestParameters = QueryRequestParameters.builder().ageGroup(0).build();
    queryRequestParameters.setAgeGroup(0);
  }

  @Test
  public void testBuildViewModelKigaIdea() throws Exception {

    when(ideaCategoryHelperMock.isLastCategoryLevel(any(IdeaCategory.class))).thenReturn(false);

    KigaIdea kigaIdeaLive = new KigaIdeaLive();
    kigaIdeaLive.setId(1L);
    kigaIdeaLive.setSortTree("AAAB");

    queryRequestParameters.setEntity(kigaIdeaLive);

    IdeaHierarchyManager ideaHierarchyManager =
        new IdeaHierarchyManager(
            kigaIdeaManagerMock,
            ideaQueryParameterBuilderMock,
            ideaGroupCategoryManagerMock,
            ideaGroupManagerMock,
            ideaCategoryHelperMock);

    when(ideaQueryParameterMock.getIdea()).thenReturn(kigaIdeaLive);

    ideaHierarchyManager.getViewModel(
        mock(HttpServletRequest.class), mock(HttpServletResponse.class), queryRequestParameters);

    verify(ideaQueryParameterBuilderMock).buildQuery(any(QueryRequestParameters.class));
    verify(kigaIdeaManagerMock).getViewModel(any(IdeaQueryParameter.class));
  }

  @Test
  public void testBuildViewModelIdeaGroup() throws Exception {

    when(ideaCategoryHelperMock.isLastCategoryLevel(any(IdeaCategory.class))).thenReturn(true);

    IdeaGroup ideaGroup = new IdeaGroupLive();
    ideaGroup.setId(1L);
    ideaGroup.setSortTree("AAAB");

    queryRequestParameters.setEntity(ideaGroup);

    IdeaHierarchyManager ideaHierarchyManager =
        new IdeaHierarchyManager(
            mock(KigaIdeaManager.class),
            ideaQueryParameterBuilderMock,
            ideaGroupCategoryManagerMock,
            ideaGroupManagerMock,
            ideaCategoryHelperMock);

    when(ideaQueryParameterMock.getIdea()).thenReturn(null);

    ideaHierarchyManager.getViewModel(
        mock(HttpServletRequest.class), mock(HttpServletResponse.class), queryRequestParameters);

    verify(ideaQueryParameterBuilderMock).buildQuery(any(QueryRequestParameters.class));
    verify(ideaGroupManagerMock).getViewModel(any(IdeaQueryParameter.class));
  }

  @Test
  public void testBuildViewModelIdeaGroupContainer() throws Exception {

    when(ideaCategoryHelperMock.isLastCategoryLevel(any(IdeaCategory.class))).thenReturn(false);

    IdeaGroupContainer ideaGroupContainer = new IdeaGroupContainerLive();
    ideaGroupContainer.setId(1L);

    queryRequestParameters.setEntity(ideaGroupContainer);

    IdeaHierarchyManager ideaHierarchyManager =
        new IdeaHierarchyManager(
            mock(KigaIdeaManager.class),
            ideaQueryParameterBuilderMock,
            ideaGroupCategoryManagerMock,
            ideaGroupManagerMock,
            ideaCategoryHelperMock);

    ideaHierarchyManager.getViewModel(
        mock(HttpServletRequest.class), mock(HttpServletResponse.class), queryRequestParameters);

    verify(ideaQueryParameterBuilderMock).buildQuery(any(QueryRequestParameters.class));
    verify(ideaGroupCategoryManagerMock).getViewModel(any(IdeaQueryParameter.class));
  }
}
