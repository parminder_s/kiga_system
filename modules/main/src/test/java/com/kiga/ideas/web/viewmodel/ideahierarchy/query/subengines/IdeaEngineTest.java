package com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.content.domain.ss.property.SortStrategy;
import com.kiga.content.repository.ss.KigaIdeaRepositoryProxy;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import java.util.Collections;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Sort;

public class IdeaEngineTest {

  private KigaIdeaRepositoryProxy kigaIdeaRepositoryProxyMock;
  private IdeaQueryParameter ideaQueryParameter;

  private IdeaCategory ideaCategory;
  private IdeaEngine ideaEngine;

  /** Preparing Tests. */
  @Before
  public void prepareTests() {

    kigaIdeaRepositoryProxyMock = mock(KigaIdeaRepositoryProxy.class);
    when(kigaIdeaRepositoryProxyMock.findAllForGroup(anyLong(), anyInt(), any(Sort.class)))
        .thenReturn(Collections.emptyList());

    ideaCategory = new IdeaCategoryLive();
    ideaCategory.setId(7L);
    ideaCategory.setSortStrategy(SortStrategy.SEQUENCE);

    ideaQueryParameter = new IdeaQueryParameter();
    ideaQueryParameter.setAgeGroupFilter(0);
    ideaQueryParameter.setIdeaCategory(ideaCategory);

    ideaEngine = new IdeaEngine(kigaIdeaRepositoryProxyMock);
  }

  @Test
  public void testGetIdeas() {

    Assert.assertEquals(Collections.emptyList(), ideaEngine.getIdeas(ideaQueryParameter));
    verify(kigaIdeaRepositoryProxyMock).findAllForGroup(anyLong(), anyInt(), any(Sort.class));
  }
}
