package com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import javax.persistence.EntityNotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IdeaEngineHelperTest {

  private IdeaEngineHelper ideaEngineHelper;
  private KigaIdea kigaIdea;
  private KigaIdea kigaIdeaParent;

  /** preparing tests. */
  @Before
  public void prepareTests() {
    ideaEngineHelper = new IdeaEngineHelper();
    kigaIdeaParent = new KigaIdeaLive();
    kigaIdeaParent.setTitle("TEST_PARENT_TITLE");
    kigaIdea = new KigaIdeaLive();
    kigaIdea.setId(0L);
    kigaIdea.setParent(kigaIdeaParent);
  }

  @Test
  public void testHasValidParent() {
    Assert.assertTrue(ideaEngineHelper.hasValidParent(kigaIdea));
  }

  @Test
  public void testHasValidParentNull() {
    kigaIdea.setParent(null);
    Assert.assertFalse(ideaEngineHelper.hasValidParent(kigaIdea));
  }

  @Test
  public void testHasValidParentEntityNotFound() {
    kigaIdea = spy(new KigaIdeaLive());
    kigaIdea.setParent(kigaIdeaParent);
    when(kigaIdea.getParent()).thenThrow(new EntityNotFoundException());

    Assert.assertFalse(ideaEngineHelper.hasValidParent(kigaIdea));
  }
}
