package com.kiga.ideas.web.request;

import com.google.code.beanmatchers.BeanMatchers;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 17.09.16.
 */
public class IdeasGroupListRequestTest {
  @Test
  public void testBean() {
    Assert.assertThat(IdeasGroupListRequest.class, BeanMatchers.hasValidGettersAndSetters());
  }
}
