package com.kiga.ideas.web.viewmodel.ideahierarchy.query;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.ideas.web.viewmodel.ideahierarchy.helper.IdeaCategoryHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper.IdeaEngineHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper.IdeaSiblingHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaGroupCategoryQueryResult;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.IdeaQueryEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.NewIdeaBlockViewEngine;
import com.kiga.main.locale.Locale;
import java.util.Collections;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IdeaGroupCategoryQueryEngineTest {
  private IdeaQueryEngine ideaQueryEngineMock;
  private IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxyMock;
  private IdeaGroupCategoryQueryEngine ideaGroupCategoryQueryEngine;
  private IdeaQueryParameter ideaQueryParameter;
  private IdeaGroupContainer ideaGroupContainer;
  private IdeaEngineHelper ideaEngineHelperMock;
  private IdeaCategoryHelper ideaCategoryHelperMock;
  private IdeaSiblingHelper ideaSiblingHelperMock;

  /** prepare tests. */
  @Before
  public void prepareTests() {
    ideaSiblingHelperMock = mock(IdeaSiblingHelper.class);
    ideaCategoryHelperMock = mock(IdeaCategoryHelper.class);
    when(ideaCategoryHelperMock.isNewIdeaGroupCategory(any(IdeaCategory.class))).thenReturn(true);
    ideaEngineHelperMock = mock(IdeaEngineHelper.class);
    ideaGroupContainer = new IdeaGroupContainerLive();
    ideaGroupContainer.setLocale(Locale.de);
    ideaGroupContainer.setSortTree("AA");
    ideaGroupContainer.setId(1L);
    ideaGroupContainer.setCode("test-123");
    ideaGroupContainer.setClassName("IdeaGroupContainer");
    ideaQueryEngineMock = mock(IdeaQueryEngine.class);
    ideaCategoryRepositoryProxyMock = mock(IdeaCategoryRepositoryProxy.class);
    when(ideaCategoryRepositoryProxyMock.findBySortTreeStartsWithAndLocaleLikeOrderBySortAsc(
            any(String.class), any(Locale.class)))
        .thenReturn(Collections.singletonList(ideaGroupContainer));
    ideaGroupCategoryQueryEngine =
        new IdeaGroupCategoryQueryEngine(
            mock(NewIdeaBlockViewEngine.class),
            ideaSiblingHelperMock,
            ideaCategoryHelperMock,
            ideaQueryEngineMock,
            ideaCategoryRepositoryProxyMock);
    KigaIdea ideaMock = mock(KigaIdea.class);
    when(ideaMock.getClassName()).thenReturn("KigaIdea");
    when(ideaQueryEngineMock.getIdeas(any(IdeaQueryParameter.class)))
        .thenReturn(Collections.singletonList(mock(KigaIdea.class)));

    ideaQueryParameter =
        IdeaQueryParameter.builder().ideaCategory(ideaGroupContainer).ageGroupFilter(0).build();
  }

  @Test
  public void testGetIdeaGroupCategoryResultNoBlockView() {
    IdeaGroupCategoryQueryResult ideaGroupCategoryQueryResult =
        ideaGroupCategoryQueryEngine.query(ideaQueryParameter);

    Assert.assertEquals(1, ideaGroupCategoryQueryResult.getIdeaSets().size());
    Assert.assertEquals(0, ideaGroupCategoryQueryResult.getSiblings().size());
  }

  @Test
  public void testGetIdeaGroupCategoryResultBlockView() {
    ideaGroupContainer.setSortTree("AAA");
    ideaGroupCategoryQueryEngine =
        new IdeaGroupCategoryQueryEngine(
            mock(NewIdeaBlockViewEngine.class),
            ideaSiblingHelperMock,
            ideaCategoryHelperMock,
            ideaQueryEngineMock,
            ideaCategoryRepositoryProxyMock);

    IdeaGroupCategoryQueryResult ideaGroupCategoryQueryResult =
        ideaGroupCategoryQueryEngine.query(ideaQueryParameter);

    Assert.assertEquals(1, ideaGroupCategoryQueryResult.getIdeaSets().size());
    Assert.assertEquals(0, ideaGroupCategoryQueryResult.getSiblings().size());
  }
}
