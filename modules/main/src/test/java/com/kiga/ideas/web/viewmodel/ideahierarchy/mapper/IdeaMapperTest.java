package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.KigaPrintPreviewImage;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.live.KigaPrintPreviewImageLive;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.content.domain.ss.property.LayoutType;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.property.PreviewImage;
import com.kiga.ideas.service.RatingCalculatorService;
import com.kiga.ideas.web.response.IdeasResponseRating;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class IdeaMapperTest {
  private static final String PROXY_DOWNLOAD_PATH_PREFIX = "/idea/downloadable/download/";
  @Rule public final ExpectedException exception = ExpectedException.none();
  private IdeaMapper ideaMapper;
  private S3ImageResizeService s3ImageResizeServiceMock;
  private IdeasProperties ideasPropertiesMock;
  private SiteTreeUrlGenerator siteTreeUrlGeneratorMock;
  private RatingCalculatorService ratingCalculatorServiceMock;
  private KigaIdea kigaIdea;
  private KigaIdea kigaIdeaParent;
  private KigaIdea kigaIdeaParentParent;
  private IdeasResponseRating ideaResponseRating;

  /** Fill s3file. */
  private void fills3File(KigaS3File kigaS3File) {
    kigaS3File.setId(4L);
    kigaS3File.setName("KIGA_S3_TEST_FILE");
    kigaS3File.setUrl("TEST_FILE_URL");
    kigaS3File.setSize(5);
    kigaIdea.setUploadedFile(kigaS3File);
  }

  /** fill KigaIdea with test content. */
  private void fillKigaIdea(KigaIdea kigaIdea) {
    kigaIdea.setArticleType(ArticleType.DOWNLOAD);
    kigaIdea.setFacebookShare(1);
    kigaIdea.setInPool(1);
    kigaIdea.setForParents(1);
    kigaIdea.setContent("TEST_CONTENT");

    kigaIdeaParent = new KigaIdeaLive();
    kigaIdeaParent.setUrlSegment("TEST_PARENT_URL_SEGMENT");
    kigaIdeaParent.setId(2L);
    kigaIdeaParent.setTitle("TEST_P_TITLE");
    kigaIdeaParentParent = new KigaIdeaLive();
    kigaIdeaParent.setParent(kigaIdeaParentParent);
    kigaIdeaParentParent.setUrlSegment("TEST_PP_URL_SEGMENT");
    kigaIdeaParentParent.setId(3L);
    kigaIdeaParentParent.setTitle("TEST_PP_TITLE");
    kigaIdea.setParent(kigaIdeaParent);
    S3Image kigaS3File = new S3Image();
    fills3File(kigaS3File);

    KigaPageImage previewImage = new KigaPageImage();
    S3Image s3Image = new S3Image();
    previewImage.setS3Image(s3Image);
    s3Image.setId(6L);
    s3Image.setName("TEST_S3_IMAGE");
    s3Image.setSize(7);
    kigaIdea.setPreviewImage(previewImage);
    KigaPrintPreviewImage printPreviewImage = new KigaPrintPreviewImageLive();
    printPreviewImage.setS3Image(s3Image);
    printPreviewImage.setPageNumber(8);
    kigaIdea.setKigaPrintPreviewImages(Arrays.asList(printPreviewImage));
    kigaIdea.setAgeGroup(0);
    kigaIdea.setLayoutType(LayoutType.DYNAMIC);
    kigaIdea.setSortMonth(8);
    kigaIdea.setSortYear(9);
    kigaIdea.setSortDayMini(10);
    kigaIdea.setSortMonthMini(11);
    kigaIdea.setSortYearMini(12);
  }

  /** preparing tests. */
  @Before
  public void prepareTests() throws Exception {

    ideaMapper = mock(IdeaMapper.class);
    s3ImageResizeServiceMock = mock(S3ImageResizeService.class);
    ideasPropertiesMock = mock(IdeasProperties.class);
    siteTreeUrlGeneratorMock = mock(SiteTreeUrlGenerator.class);
    ratingCalculatorServiceMock = mock(RatingCalculatorService.class);

    PreviewImage previewImageMock = mock(PreviewImage.class);
    when(previewImageMock.getSmall()).thenReturn(new ImageSize(1, 1));
    when(previewImageMock.getLarge()).thenReturn(new ImageSize(1, 1));
    when(ideasPropertiesMock.getPreviewImage()).thenReturn(previewImageMock);
    when(ideasPropertiesMock.getPrintPreviewImage()).thenReturn(previewImageMock);

    when(s3ImageResizeServiceMock.getResizedImageUrl(
            any(S3Image.class), any(PaddedImageStrategy.class)))
        .thenReturn("TEST_PREVIEW_STRING");

    when(ratingCalculatorServiceMock.getIdeaResponseRating(any(KigaIdea.class)))
        .thenReturn(ideaResponseRating);

    when(siteTreeUrlGeneratorMock.getRelativeUrl(any(SiteTree.class))).thenReturn("TEST_ST_URL");

    when(siteTreeUrlGeneratorMock.getRelativeNgUrl(any(SiteTree.class)))
        .thenReturn("TEST_STNG_URL");

    when(siteTreeUrlGeneratorMock.getUrl(any(SiteTree.class))).thenReturn("TEST_ST_URL");

    when(siteTreeUrlGeneratorMock.getNgUrl(any(SiteTree.class))).thenReturn("TEST_STNG_URL");

    ideaMapper =
        new IdeaMapper(
            s3ImageResizeServiceMock,
            ideasPropertiesMock,
            siteTreeUrlGeneratorMock,
            ratingCalculatorServiceMock);

    kigaIdea = new KigaIdeaLive();
    fillKigaIdea(kigaIdea);
  }
}
