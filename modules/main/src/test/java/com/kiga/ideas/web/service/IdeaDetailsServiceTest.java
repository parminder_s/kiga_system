package com.kiga.ideas.web.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.repository.ss.draft.KigaIdeaRepositoryDraft;
import com.kiga.content.repository.ss.live.KigaPageRepositoryLive;
import org.junit.Test;

/**
 * Created by rainerh on 04.09.16.
 */
public class IdeaDetailsServiceTest {
  private IdeasResponseIdeaDetailsConverter converterMock =
    mock(IdeasResponseIdeaDetailsConverter.class);
  private KigaIdeaRepositoryDraft kigaIdeaModelMock = mock(KigaIdeaRepositoryDraft.class);
  private KigaPageRepositoryLive kigaPageModelLiveMock = mock(KigaPageRepositoryLive.class);
  private IdeaDetailsService detailsService =
    new IdeaDetailsService(converterMock, kigaIdeaModelMock, kigaPageModelLiveMock);

  @Test
  public void getIdeaDetails() throws Exception {
    KigaIdeaDraft kigaIdea = new KigaIdeaDraft();
    when(kigaIdeaModelMock.findOne(1L)).thenReturn(kigaIdea);

    detailsService.getIdeaDetails(1L);
    verify(kigaIdeaModelMock).findOne(1L);
    verify(converterMock).convertToResponse(kigaIdea);
  }

  @Test
  public void testGetKigaPage() {
    detailsService.getKigaPage(1L);
    verify(kigaPageModelLiveMock).findOne(1L);
  }
}
