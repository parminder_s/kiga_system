package com.kiga.ideas.service;

import static org.junit.Assert.assertEquals;

import com.kiga.ideas.web.viewmodel.kigaidea.MediaLinksConverter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Test;

/**
 * Created by asv on 23.02.17.
 */
public class MediaLinksConverterTest {

  @Test
  public void convertMediaLinksTest() throws Exception {
    String content = "<div class='rowDiv'><div class='span-4 last'><a class='sm2_link' "
      + "href='https://kiga.s3.amazonaws.com/18429/Mama.mp3'>Mama Song</a>"
      + "</div></div><div class='rowDiv'><div class='span-4 last'><a class='videoplayer' "
      + "href='https://kiga.s3.amazonaws.com/18429/Video.mp4'>"
      + "<img class='rahmen1' src='https://kiga.s3.amazonaws.com/864.png'/>"
      + "</a></div></div></div>";
    MediaLinksConverter mediaLinksConverter = new MediaLinksConverter();
    String contentNew = mediaLinksConverter.convertMediaLinks(content);

    Document doc = Jsoup.parse(contentNew);
    Element audio = doc.select("audio-player").first();
    Element video = doc.select("video-player").first();

    assertEquals("https://kiga.s3.amazonaws.com/18429/Mama.mp3", audio.attr("href"));
    assertEquals("https://kiga.s3.amazonaws.com/18429/Video.mp4", video.attr("video-link"));
    assertEquals("https://kiga.s3.amazonaws.com/864.png", video.attr("preview-link"));
    assertEquals("Mama Song", audio.attr("label"));
  }

  @Test
  public void ideaWithoutMediaLinksTest() throws Exception {
    String content = "";
    MediaLinksConverter mediaLinksConverter = new MediaLinksConverter();
    String contentNew = mediaLinksConverter.convertMediaLinks(content);

    assertEquals(contentNew, content);
  }
}



