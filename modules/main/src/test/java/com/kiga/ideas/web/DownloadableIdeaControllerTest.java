package com.kiga.ideas.web;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.kiga.content.sorttree.SortTreeManager;
import com.kiga.ideas.web.request.ModifyDownloadableIdeaRequest;
import com.kiga.ideas.web.request.UrlSegmentValidationRequest;
import com.kiga.ideas.web.service.DownloadableIdeasService;
import com.kiga.integration.amazon.s3.config.AmazonS3Parameters;
import com.kiga.main.locale.Locale;
import com.kiga.s3.web.response.KigaS3FileViewModel;
import com.kiga.search.services.IndexerService;
import com.kiga.util.BrowserPusherService;
import com.kiga.web.request.IdListRequest;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 4/5/17.
 */
public class DownloadableIdeaControllerTest {
  private static final String FILE_NAME = "MOCK";
  private static final String BUCKET_NAME = "BUCKET";
  private static final String CONTENT_TYPE = "image/jpeg";
  private BrowserPusherService browserPusherServiceMock = mock(BrowserPusherService.class);
  private DownloadableIdeasService downloadableIdeasServiceMock =
    mock(DownloadableIdeasService.class);
  private AmazonS3Client amazonS3ClientMock = mock(AmazonS3Client.class);
  private AmazonS3Parameters amazonS3ParametersMock = mock(AmazonS3Parameters.class);
  private IndexerService indexerMock = mock(IndexerService.class);
  private SortTreeManager sortTreeManagerMock = mock(SortTreeManager.class);
  private DownloadableIdeaController downloadableIdeaController =
    new DownloadableIdeaController(browserPusherServiceMock, downloadableIdeasServiceMock,
      amazonS3ClientMock, amazonS3ParametersMock, indexerMock,
      sortTreeManagerMock);
  private DownloadableIdeaController downloadableIdeaControllerNewSiteTree =
    new DownloadableIdeaController(browserPusherServiceMock, downloadableIdeasServiceMock,
      amazonS3ClientMock, amazonS3ParametersMock, indexerMock,
      sortTreeManagerMock);
  private IdListRequest idsToRemoveMock;
  private List<Long> ids;

  /**
   * preparing tests.
   */
  @Before
  public void prepareTests() {
    idsToRemoveMock = mock(IdListRequest.class);
    ids = Collections.singletonList(1L);
    when(idsToRemoveMock.getIds()).thenReturn(ids);
  }

  @Test
  public void testAdd() {
    ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest =
      mock(ModifyDownloadableIdeaRequest.class);
    downloadableIdeaController.add(modifyDownloadableIdeaRequest);
    verify(downloadableIdeasServiceMock).add(modifyDownloadableIdeaRequest);
  }

  @Test
  public void testAddNewSiteTree() {
    ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest =
      mock(ModifyDownloadableIdeaRequest.class);
    downloadableIdeaControllerNewSiteTree.add(modifyDownloadableIdeaRequest);
    verify(downloadableIdeasServiceMock).add(modifyDownloadableIdeaRequest);
  }

  @Test
  public void testUpdate() {
    ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest =
      mock(ModifyDownloadableIdeaRequest.class);
    downloadableIdeaController.update(modifyDownloadableIdeaRequest);
    verify(downloadableIdeasServiceMock).update(modifyDownloadableIdeaRequest);
  }

  @Test
  public void testUpdateNewSiteTree() {
    ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest =
      mock(ModifyDownloadableIdeaRequest.class);
    downloadableIdeaControllerNewSiteTree.update(modifyDownloadableIdeaRequest);
    verify(downloadableIdeasServiceMock).update(modifyDownloadableIdeaRequest);
  }

  @Test
  public void testRemove() {
    downloadableIdeaController.remove(idsToRemoveMock);
    verify(downloadableIdeasServiceMock).remove(ids);
  }

  @Test
  public void testRemoveNewSiteTree() {
    downloadableIdeaControllerNewSiteTree.remove(idsToRemoveMock);
    verify(downloadableIdeasServiceMock).remove(ids);
  }


  @Test
  public void testGet() {
    long id = 1L;
    downloadableIdeaController.get(id);
    verify(downloadableIdeasServiceMock).findById(id);
  }

  @Test
  public void testGetNewSiteTree() {
    long id = 1L;
    downloadableIdeaControllerNewSiteTree.get(id);
    verify(downloadableIdeasServiceMock).findById(id);
  }

  private S3ObjectInputStream prepareStream() {
    KigaS3FileViewModel kigaS3FileViewModel = new KigaS3FileViewModel();
    kigaS3FileViewModel.setFileName(FILE_NAME);
    when(downloadableIdeasServiceMock.findUploadedFileByIdeaId(1L))
      .thenReturn(kigaS3FileViewModel);
    when(amazonS3ParametersMock.getBucketName()).thenReturn(BUCKET_NAME);
    S3Object s3ObjectMock = mock(S3Object.class);
    when(amazonS3ClientMock.getObject(any())).thenReturn(s3ObjectMock);

    ObjectMetadata objectMetadataMock = mock(ObjectMetadata.class);
    when(objectMetadataMock.getContentType()).thenReturn(CONTENT_TYPE);
    when(s3ObjectMock.getObjectMetadata()).thenReturn(objectMetadataMock);

    S3ObjectInputStream s3ObjectInputStream = mock(S3ObjectInputStream.class);
    when(s3ObjectMock.getObjectContent()).thenReturn(s3ObjectInputStream);

    return s3ObjectInputStream;
  }

  @Test
  public void testDownload() throws IOException {

    S3ObjectInputStream s3ObjectInputStream = prepareStream();
    HttpServletResponse responseMock = mock(HttpServletResponse.class);
    downloadableIdeaController.download(1L, responseMock);

    verify(browserPusherServiceMock)
      .pushFile(responseMock, s3ObjectInputStream, FILE_NAME, CONTENT_TYPE);
  }

  @Test
  public void testDownloadNewSiteTree() throws IOException {

    S3ObjectInputStream s3ObjectInputStream = prepareStream();
    HttpServletResponse responseMock = mock(HttpServletResponse.class);
    downloadableIdeaControllerNewSiteTree.download(1L, responseMock);

    verify(browserPusherServiceMock)
      .pushFile(responseMock, s3ObjectInputStream, FILE_NAME, CONTENT_TYPE);
  }

  @Test
  public void testList() {
    downloadableIdeaController.list(Locale.en.getValue());
    verify(downloadableIdeasServiceMock).getAll(Locale.en);
  }

  @Test
  public void testListNewSiteTree() {
    downloadableIdeaControllerNewSiteTree.list(Locale.en.getValue());
    verify(downloadableIdeasServiceMock).getAll(Locale.en);
  }

  @Test
  public void testPublish() {
    IdListRequest idsToPublishMock = mock(IdListRequest.class);
    downloadableIdeaController.publish(idsToPublishMock);
    verify(downloadableIdeasServiceMock).publish(idsToPublishMock);
  }

  @Test
  public void testPublishNewSiteTree() {
    IdListRequest idsToPublishMock = mock(IdListRequest.class);
    downloadableIdeaControllerNewSiteTree.publish(idsToPublishMock);
    verify(downloadableIdeasServiceMock).publish(idsToPublishMock);
  }

  @Test
  public void unpublish() {
    IdListRequest idsToUnpublishMock = mock(IdListRequest.class);
    downloadableIdeaController.unpublish(idsToUnpublishMock);
    verify(downloadableIdeasServiceMock).unpublish(idsToUnpublishMock);
  }

  @Test
  public void unpublishNewSiteTree() {
    IdListRequest idsToUnpublishMock = mock(IdListRequest.class);
    downloadableIdeaControllerNewSiteTree.unpublish(idsToUnpublishMock);
    verify(downloadableIdeasServiceMock).unpublish(idsToUnpublishMock);
  }

  @Test
  public void testValidate() {
    UrlSegmentValidationRequest urlSegmentValidationRequestMock =
      mock(UrlSegmentValidationRequest.class);
    downloadableIdeaController.validate(urlSegmentValidationRequestMock);
    verify(downloadableIdeasServiceMock).validate(urlSegmentValidationRequestMock);
  }

  @Test
  public void testValidateNewSiteTree() {
    UrlSegmentValidationRequest urlSegmentValidationRequestMock =
      mock(UrlSegmentValidationRequest.class);
    downloadableIdeaControllerNewSiteTree.validate(urlSegmentValidationRequestMock);
    verify(downloadableIdeasServiceMock).validate(urlSegmentValidationRequestMock);
  }
}
