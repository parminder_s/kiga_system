package com.kiga.ideas.web.converter;

import static org.junit.Assert.assertEquals;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.draft.IdeaCategoryDraft;
import com.kiga.content.domain.ss.draft.SiteTreeDraft;
import com.kiga.ideas.web.response.IdeaCategoryHierarchyViewModel;
import org.junit.Test;

/**
 * @author bbs
 * @since 5/13/16.
 */
public class IdeaCategoryHierarchyDropdownConverterTest {
  @Test
  public void convertToResponse() {
    IdeaCategory entity = new IdeaCategoryDraft();
    entity.setId(1L);
    entity.setUrlSegment("url-segment");
    entity.setTitle("title");
    entity.setSort(2);

    SiteTree parent = new SiteTreeDraft();
    parent.setId(2L);
    parent.setUrlSegment("other-url-segment");
    parent.setTitle("other title");
    parent.setSort(3);
    entity.setParent(parent);

    IdeaCategoryHierarchyDropdownConverter ideaCategoryHierarchyDropdownConverter =
      new IdeaCategoryHierarchyDropdownConverter();
    IdeaCategoryHierarchyViewModel converted =
      ideaCategoryHierarchyDropdownConverter.convertToResponse(entity);

    assertEquals(entity.getParent().getId(), converted.getCategoryId());
    assertEquals(entity.getParent().getUrlSegment(), converted.getCategoryName());
    assertEquals(entity.getParent().getTitle(), converted.getCategoryTitle());
    assertEquals(entity.getParent().getSort(), converted.getCategorySort().intValue());
    assertEquals(entity.getId(), converted.getSubCategoryId());
    assertEquals(entity.getUrlSegment(), converted.getSubCategoryName());
    assertEquals(entity.getTitle(), converted.getSubCategoryTitle());
    assertEquals(entity.getSort(), converted.getSubCategorySort().intValue());
  }
}
