package com.kiga.ideas.web.viewmodel.ideahierarchy.query;

import static org.mockito.Mockito.mock;

import com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper.IdeaSiblingHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaGroupQueryResult;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.IdeaQueryEngine;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IdeaGroupQueryEngineTest {
  private IdeaQueryEngine ideaQueryEngineMock;
  private IdeaSiblingHelper ideaSiblingHelperMock;

  /** preparing tests. */
  @Before
  public void prepareTests() {
    ideaQueryEngineMock = mock(IdeaQueryEngine.class);
    ideaSiblingHelperMock = mock(IdeaSiblingHelper.class);
  }

  @Test
  public void testGetIdeaGroupResult() {
    IdeaGroupQueryEngine ideaGroupQueryEngine =
        new IdeaGroupQueryEngine(ideaSiblingHelperMock, ideaQueryEngineMock);

    IdeaGroupQueryResult ideaGroupQueryResult =
        ideaGroupQueryEngine.query(mock(IdeaQueryParameter.class));

    Assert.assertEquals(0, ideaGroupQueryResult.getIdeas().size());
    Assert.assertEquals(0, ideaGroupQueryResult.getSiblings().size());
  }
}
