package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.ParentCategoryViewModel;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ParentCategoryViewModelMapperTest {
  private ParentCategoryViewModelMapper parentCategoryViewModelMapper;
  private List<IdeaCategory> siblings;

  /** prepare tests. */
  @Before
  public void prepareTests() {
    parentCategoryViewModelMapper = new ParentCategoryViewModelMapper();
    IdeaCategory ideaCategory = new IdeaGroupContainerLive();
    ideaCategory.setClassName("IdeaGroupMainContainer"); // to avoid parent climbing
    ideaCategory.setUrlSegment("TEST_URL_SEGMENT");
    ideaCategory.setTitle("TEST_TITLE");
    ideaCategory.setIconCode("TEST_ICON_CODE");
    ideaCategory.setDisableForFavourites(true);
    siblings = Arrays.asList(ideaCategory, ideaCategory);
  }

  @Test
  public void convertSiblingsTest() {

    List<ParentCategoryViewModel> convertedSiblings =
        parentCategoryViewModelMapper.convertSiblings(siblings);

    convertedSiblings.forEach(
        parentCategoryViewModel -> {
          Assert.assertEquals("TEST_URL_SEGMENT", parentCategoryViewModel.getName());
          Assert.assertEquals("TEST_TITLE", parentCategoryViewModel.getTitle());
          Assert.assertEquals("TEST_ICON_CODE", parentCategoryViewModel.getIcon());
          Assert.assertEquals(true, parentCategoryViewModel.isDisableForFavourites());
        });
  }
}
