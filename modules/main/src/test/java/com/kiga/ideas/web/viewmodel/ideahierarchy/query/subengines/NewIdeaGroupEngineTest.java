package com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.favourites.repository.FavouriteNewIdeasRepository;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import java.util.Collections;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NewIdeaGroupEngineTest {

  private NewIdeasRepository newIdeasRepositoryMock;
  private FavouriteNewIdeasRepository favouriteNewIdeasRepositoryMock;

  private IdeaQueryParameter ideaQueryParameter;
  private NewIdeaGroupEngine newIdeaGroupEngine;

  /** prepare tests. */
  @Before
  public void prepareTests() {
    newIdeasRepositoryMock = mock(NewIdeasRepository.class);
    when(newIdeasRepositoryMock.getIdeasForAgeGroupByBitPosition(
            anyInt(), any(Locale.class), anyInt()))
        .thenReturn(Collections.emptySet());

    favouriteNewIdeasRepositoryMock = mock(FavouriteNewIdeasRepository.class);
    when(favouriteNewIdeasRepositoryMock.getIdeasForAgeGroupByBitPosition(
            anyInt(), any(Locale.class), anyInt(), any(Member.class)))
        .thenReturn(Collections.emptySet());

    ideaQueryParameter = new IdeaQueryParameter();
    ideaQueryParameter.setLocale(Locale.en);
    ideaQueryParameter.setAgeGroupFilter(0);
    IdeaCategory ideaCategory = new IdeaCategoryLive();
    ideaCategory.setCode("testcode-345");
    ideaQueryParameter.setIdeaCategory(ideaCategory);

    newIdeaGroupEngine =
        new NewIdeaGroupEngine(newIdeasRepositoryMock, favouriteNewIdeasRepositoryMock);
  }

  @Test
  public void testGetIdeasMember() {

    Member member = new Member();
    Optional optional = Optional.of(member);
    ideaQueryParameter.setMember(optional);

    Assert.assertEquals(Collections.emptyList(), newIdeaGroupEngine.getIdeas(ideaQueryParameter));
    verify(favouriteNewIdeasRepositoryMock)
        .getIdeasForAgeGroupByBitPosition(anyInt(), any(Locale.class), anyInt(), any(Member.class));
  }

  @Test
  public void testGetIdeasNoMember() {

    ideaQueryParameter.setMember(Optional.empty());

    Assert.assertEquals(Collections.emptyList(), newIdeaGroupEngine.getIdeas(ideaQueryParameter));
    verify(newIdeasRepositoryMock)
        .getIdeasForAgeGroupByBitPosition(anyInt(), any(Locale.class), anyInt());
  }
}
