package com.kiga.ideas.web.viewmodel.ideahierarchy;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.property.SortStrategy;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.ideas.service.IdeaCategorySortToDbSort;
import com.kiga.ideas.web.viewmodel.ideahierarchy.helper.IdeaCategoryHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.IdeaEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.IdeaQueryEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.NewIdeaBlockViewEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.NewIdeaEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.NewIdeaGroupEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines.PagedIdeaEngine;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Sort;

public class IdeaQueryEngineTest {
  private Optional<Member> memberOptional = Optional.of(mock(Member.class));
  private IdeaCategory ideaGroupMock;
  private IdeaGroupContainer ideaGroupContainerMock;
  private PagedIdeaEngine pagedIdeaEngineMock;
  private IdeaEngine ideaEngineMock;
  private NewIdeaGroupEngine newIdeaGroupEngineMock;
  private NewIdeaEngine newIdeaEngineMock;
  private NewIdeaBlockViewEngine newIdeaBlockViewEngineMock;
  private IdeaCategoryHelper ideaCategoryHelperMock;

  /** prepare an IdeaCategory mock object. */
  @Before
  public void prepareTests() {
    ideaCategoryHelperMock = mock(IdeaCategoryHelper.class);
    when(ideaCategoryHelperMock.isNewIdeaGroupCategory(any(IdeaCategory.class))).thenReturn(true);
    when(ideaCategoryHelperMock.isNewIdeaGroup(any(IdeaCategory.class))).thenReturn(false);
    ideaGroupContainerMock = mock(IdeaGroupContainer.class);
    when(ideaGroupContainerMock.getId()).thenReturn(1L);
    when(ideaGroupContainerMock.getCode()).thenReturn("ideas");
    ideaGroupMock = mock(IdeaCategory.class);
    when(ideaGroupMock.getId()).thenReturn(1L);
    when(ideaGroupMock.getCode()).thenReturn("new-ideas");
    pagedIdeaEngineMock = mock(PagedIdeaEngine.class);
    ideaEngineMock = mock(IdeaEngine.class);
    newIdeaEngineMock = mock(NewIdeaEngine.class);
    newIdeaGroupEngineMock = mock(NewIdeaGroupEngine.class);
    newIdeaBlockViewEngineMock = mock(NewIdeaBlockViewEngine.class);
    IdeaGroupContainer ideaCategoryParentMock = mock(IdeaGroupContainer.class);
    when(ideaGroupContainerMock.getSortStrategy()).thenReturn(SortStrategy.INHERITED);
    when(ideaGroupMock.getSortStrategy()).thenReturn(SortStrategy.INHERITED);
    when(ideaCategoryParentMock.getSortStrategy()).thenReturn(SortStrategy.DATE); // break loop
    when(ideaGroupContainerMock.getParent()).thenReturn(ideaCategoryParentMock);
    when(ideaGroupMock.getParent()).thenReturn(ideaCategoryParentMock);
  }

  @Test
  public void testNewIdeasBlockViewWithShowAll() {
    IdeaQueryParameter ideaQueryParameter =
        IdeaQueryParameter.builder()
            .locale(Locale.en)
            .ageGroupFilter(0)
            .showAll(true)
            .ideaCategory(ideaGroupMock)
            .build();

    IdeaQueryEngine ideaQueryEngine =
        new IdeaQueryEngine(
            ideaCategoryHelperMock,
            pagedIdeaEngineMock,
            ideaEngineMock,
            newIdeaGroupEngineMock,
            newIdeaEngineMock,
            newIdeaBlockViewEngineMock);

    List<KigaIdea> ideas = ideaQueryEngine.getIdeas(ideaQueryParameter);

    verify(newIdeaBlockViewEngineMock).getIdeas(any(IdeaQueryParameter.class));

    Assert.assertEquals(0, ideas.size());
  }

  @Test
  public void testFavouriteNewIdeasBlockViewWithShowAll() {
    IdeaQueryParameter ideaQueryParameter =
        IdeaQueryParameter.builder()
            .locale(Locale.en)
            .ageGroupFilter(0)
            .showAll(true)
            .member(memberOptional)
            .ideaCategory(ideaGroupMock)
            .build();

    IdeaQueryEngine ideaQueryEngine =
        new IdeaQueryEngine(
            ideaCategoryHelperMock,
            pagedIdeaEngineMock,
            ideaEngineMock,
            newIdeaGroupEngineMock,
            newIdeaEngineMock,
            newIdeaBlockViewEngineMock);

    List<KigaIdea> list = ideaQueryEngine.getIdeas(ideaQueryParameter);

    verify(newIdeaBlockViewEngineMock).getIdeas(any(IdeaQueryParameter.class));

    Assert.assertEquals(0, list.size());
  }

  @Test
  public void testPagedUseSortTree() {
    IdeaQueryParameter ideaQueryParameter =
        IdeaQueryParameter.builder()
            .locale(Locale.en)
            .ageGroupFilter(0)
            .showAll(false)
            .ideaCategory(ideaGroupContainerMock)
            .build();

    when(ideaCategoryHelperMock.isNewIdeaGroupCategory(any(IdeaCategory.class))).thenReturn(false);

    IdeaQueryEngine ideaQueryEngine =
        new IdeaQueryEngine(
            ideaCategoryHelperMock,
            pagedIdeaEngineMock,
            ideaEngineMock,
            newIdeaGroupEngineMock,
            newIdeaEngineMock,
            newIdeaBlockViewEngineMock);

    List<KigaIdea> list = ideaQueryEngine.getIdeas(ideaQueryParameter);

    verify(pagedIdeaEngineMock).getIdeas(any(IdeaQueryParameter.class));
    Assert.assertEquals(0, list.size());
  }

  @Test
  public void testPagedNotUseSortTree() {
    IdeaQueryParameter ideaQueryParameter =
        IdeaQueryParameter.builder()
            .locale(Locale.en)
            .ageGroupFilter(0)
            .showAll(false)
            .ideaCategory(ideaGroupContainerMock)
            .build();

    when(ideaCategoryHelperMock.isNewIdeaGroupCategory(any(IdeaCategory.class))).thenReturn(false);

    IdeaCategorySortToDbSort ideaCategorySortToDbSort = mock(IdeaCategorySortToDbSort.class);
    when(ideaCategorySortToDbSort.mapSort(any(IdeaCategory.class))).thenReturn(mock(Sort.class));
    IdeaQueryEngine ideaQueryEngine =
        new IdeaQueryEngine(
            ideaCategoryHelperMock,
            pagedIdeaEngineMock,
            ideaEngineMock,
            newIdeaGroupEngineMock,
            newIdeaEngineMock,
            newIdeaBlockViewEngineMock);

    List<KigaIdea> list = ideaQueryEngine.getIdeas(ideaQueryParameter);

    verify(pagedIdeaEngineMock).getIdeas(any(IdeaQueryParameter.class));

    Assert.assertEquals(0, list.size());
  }

  @Test
  public void testNonAgeGroupWithShowAll() {
    IdeaQueryParameter ideaQueryParameter =
        IdeaQueryParameter.builder()
            .locale(Locale.en)
            .ageGroupFilter(0)
            .showAll(true)
            .ideaCategory(ideaGroupContainerMock)
            .build();

    when(ideaCategoryHelperMock.isNewIdeaGroupCategory(any(IdeaCategory.class))).thenReturn(false);

    IdeaQueryEngine ideaQueryEngine =
        new IdeaQueryEngine(
            ideaCategoryHelperMock,
            pagedIdeaEngineMock,
            ideaEngineMock,
            newIdeaGroupEngineMock,
            newIdeaEngineMock,
            newIdeaBlockViewEngineMock);

    List<KigaIdea> list = ideaQueryEngine.getIdeas(ideaQueryParameter);

    verify(ideaEngineMock).getIdeas(any(IdeaQueryParameter.class));

    Assert.assertEquals(0, list.size());
  }

  @Test
  public void testAgeGroupWithShowAll() {
    IdeaQueryParameter ideaQueryParameter =
        IdeaQueryParameter.builder()
            .locale(Locale.en)
            .ageGroupFilter(0)
            .showAll(true)
            .ideaCategory(ideaGroupMock)
            .build();

    when(ideaGroupMock.getCode()).thenReturn("1-3");
    when(ideaCategoryHelperMock.isNewIdeaGroupCategory(any(IdeaCategory.class))).thenReturn(false);
    when(ideaCategoryHelperMock.isNewIdeaGroup(any(IdeaCategory.class))).thenReturn(true);

    NewIdeasRepository newIdeasRepository = mock(NewIdeasRepository.class);

    when(newIdeasRepository.getIdeasForAgeGroupByBitPosition(anyInt(), eq(Locale.en), anyInt()))
        .thenReturn(Collections.emptySet());

    IdeaQueryEngine ideaQueryEngine =
        new IdeaQueryEngine(
            ideaCategoryHelperMock,
            pagedIdeaEngineMock,
            ideaEngineMock,
            newIdeaGroupEngineMock,
            newIdeaEngineMock,
            newIdeaBlockViewEngineMock);

    List<KigaIdea> list = ideaQueryEngine.getIdeas(ideaQueryParameter);

    verify(newIdeaGroupEngineMock).getIdeas(any(IdeaQueryParameter.class));

    Assert.assertEquals(0, list.size());
  }
}
