package com.kiga.ideas.web.response;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author bbs
 * @since 7/16/16.
 */
public class IdeaGroupViewModelGroupTest {
  @Test
  public void testAgeGroupContainer() {
    IdeasResponseGroup group = new IdeasResponseGroup();
    group.setCode("new-ideas");

    Assert.assertNull(group.calculateAgeGroupValue());
    Assert.assertEquals(false, group.isAgeGroup());
    Assert.assertEquals(true, group.isAgeGroupContainer());
    Assert.assertEquals("new-ideas", group.getCode());
  }

  @Test
  public void testAgeGroup() {
    IdeasResponseGroup group = new IdeasResponseGroup();
    group.setCode("new-ideas-ig-1");

    Assert.assertEquals(1, group.calculateAgeGroupValue().longValue());
    Assert.assertEquals(true, group.isAgeGroup());
    Assert.assertEquals(false, group.isAgeGroupContainer());
  }

  @Test
  public void testAgeGroupChangeToContainer() {
    IdeasResponseGroup group = new IdeasResponseGroup();
    group.setCode("new-ideas-ig-1");

    Assert.assertEquals(1, group.calculateAgeGroupValue().longValue());
    Assert.assertEquals(true, group.isAgeGroup());
    Assert.assertEquals(false, group.isAgeGroupContainer());

    group.setCode("new-ideas");

    Assert.assertNull(group.calculateAgeGroupValue());
    Assert.assertEquals(false, group.isAgeGroup());
    Assert.assertEquals(true, group.isAgeGroupContainer());
  }

  @Test
  public void testAgeGroupContainerChangeToGroup() {
    IdeasResponseGroup group = new IdeasResponseGroup();

    group.setCode("new-ideas");

    Assert.assertNull(group.calculateAgeGroupValue());
    Assert.assertEquals(false, group.isAgeGroup());
    Assert.assertEquals(true, group.isAgeGroupContainer());

    group.setCode("new-ideas-ig-1");

    Assert.assertEquals(1, group.calculateAgeGroupValue().longValue());
    Assert.assertEquals(true, group.isAgeGroup());
    Assert.assertEquals(false, group.isAgeGroupContainer());
  }

  @Test
  public void testAgeGroupToNonAgeGroup() {
    IdeasResponseGroup group = new IdeasResponseGroup();
    group.setCode("new-ideas-ig-1");

    Assert.assertEquals(1, group.calculateAgeGroupValue().longValue());
    Assert.assertEquals(true, group.isAgeGroup());
    Assert.assertEquals(false, group.isAgeGroupContainer());

    group.setCode(null);

    Assert.assertNull(group.calculateAgeGroupValue());
    Assert.assertEquals(false, group.isAgeGroup());
    Assert.assertEquals(false, group.isAgeGroupContainer());
  }

  @Test
  public void testNonAgeGroupToAgeGroup() {
    IdeasResponseGroup group = new IdeasResponseGroup();

    Assert.assertNull(group.calculateAgeGroupValue());
    Assert.assertEquals(false, group.isAgeGroup());
    Assert.assertEquals(false, group.isAgeGroupContainer());

    group.setCode("new-ideas-ig-1");

    Assert.assertEquals(1, group.calculateAgeGroupValue().longValue());
    Assert.assertEquals(true, group.isAgeGroup());
    Assert.assertEquals(false, group.isAgeGroupContainer());
  }

  @Test
  public void testNotAgeGroupNorContainer() {
    IdeasResponseGroup group = new IdeasResponseGroup();

    Assert.assertNull(group.calculateAgeGroupValue());
    Assert.assertEquals(false, group.isAgeGroup());
    Assert.assertEquals(false, group.isAgeGroupContainer());
  }
}
