package com.kiga.ideas.web;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.ideas.service.IdeaTranslationGroupService;
import com.kiga.main.locale.Locale;
import com.kiga.web.request.IdRequest;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by robert on 03.09.17.
 */
public class IdeaTranslationGroupControllerTest {
  private static final long IDEA_ID = 1L;
  private static final long TRANSLATION_GROUP_ID = 2L;
  public static final Locale LOCALE = Locale.en;
  private IdeaTranslationGroupService
    translationGroupServiceMock = mock(IdeaTranslationGroupService.class);
  private IdeaTranslationGroupController translationGroupController =
    new IdeaTranslationGroupController(translationGroupServiceMock);

  @Test
  public void testGetByIdea() {
    IdRequest idRequest = new IdRequest();
    idRequest.setId(IDEA_ID);
    translationGroupController.getByIdea(idRequest);
    verify(translationGroupServiceMock).getByIdeaId(IDEA_ID);
  }

  @Test
  public void testGetByTranslationGroup() {
    IdRequest idRequest = new IdRequest();
    idRequest.setId(TRANSLATION_GROUP_ID);
    translationGroupController.getByTranslationGroup(idRequest);
    verify(translationGroupServiceMock).getByTranslationGroupId(TRANSLATION_GROUP_ID);
  }

  @Test
  public void testPrepareIdeaForTranslation() throws IOException {
    IdRequest idRequest = new IdRequest();
    idRequest.setId(IDEA_ID);
    idRequest.setLocale(LOCALE);
    translationGroupController.prepareIdeaForTranslation(idRequest);
    verify(translationGroupServiceMock).prepareIdeaForTranslation(IDEA_ID, LOCALE);
  }
}
