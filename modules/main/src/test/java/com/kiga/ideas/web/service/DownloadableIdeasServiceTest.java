package com.kiga.ideas.web.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.draft.IdeaCategoryDraft;
import com.kiga.content.domain.ss.draft.IdeaGroupDraft;
import com.kiga.content.domain.ss.draft.IdeaGroupToIdeaDraft;
import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.domain.ss.draft.SiteTreeDraft;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.content.domain.ss.property.LayoutType;
import com.kiga.content.repository.ss.KigaPageImageRepository;
import com.kiga.content.repository.ss.draft.IdeaGroupRepository;
import com.kiga.content.repository.ss.draft.IdeaGroupToIdeaRepository;
import com.kiga.content.repository.ss.draft.SiteTreeRepository;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.service.SiteTreeTranslationGroupService;
import com.kiga.content.sorttree.SortTreeManager;
import com.kiga.idea.member.provider.MemberIdeaCategoryService;
import com.kiga.ideas.IdeaState;
import com.kiga.ideas.downloadable.repository.DownloadableIdeasRepositoryDraft;
import com.kiga.ideas.downloadable.repository.DownloadableIdeasRepositoryLive;
import com.kiga.ideas.web.request.ModifyDownloadableIdeaRequest;
import com.kiga.ideas.web.request.UrlSegmentValidationRequest;
import com.kiga.ideas.web.response.IdeasResponseGroup;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.ideas.web.response.UrlSegmentValidationResponse;
import com.kiga.main.locale.Locale;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.repository.ss.KigaS3FileRepository;
import com.kiga.s3.repository.ss.S3ImageRepository;
import com.kiga.s3.web.converter.KigaS3FileViewModelConverter;
import com.kiga.search.services.IndexerService;
import com.kiga.web.request.IdListRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author bbs
 * @since 4/7/17.
 */
public class DownloadableIdeasServiceTest {
  private static final Long ID1 = 1001L;
  private static final Long ID2 = 1002L;
  private static final Long ID3 = 1003L;
  private static final Long ID4 = 1004L;
  private static final Long SUBCATEGORY_ID = 2L;
  private static final String URL_SEGMENT = "URL-SEGMENT";
  private static final String TITLE = "TITLE";
  private static final String DESCRIPTION = "DESC";
  private static final Integer AGE_GROUP = 3;
  private static final Integer SORT_MONTH = 4;
  private static final Integer SORT_YEAR = 5;
  private static final Integer SORT_DAY_MINI = 6;
  private static final Integer SORT_MONTH_MINI = 7;
  private static final Integer SORT_YEAR_MINI = 8;
  private static final String INVALID_URL_SEGMENT = "INVALID@";
  private static final Long UPLOADED_FILE_ID = 14L;
  private static final long TRANSLATION_GROUP_ID = 999L;
  private SiteTreeRepository siteTreeRepositoryMock = mock(SiteTreeRepository.class);
  private SiteTreeLiveRepository siteTreeLiveRepositoryMock = mock(SiteTreeLiveRepository.class);
  private KigaS3FileViewModelConverter kigaS3FileViewModelConverterMock =
    mock(KigaS3FileViewModelConverter.class);
  private MemberIdeaCategoryService memberIdeaCategoryServiceMock =
    mock(MemberIdeaCategoryService.class);
  private DownloadableIdeasRepositoryLive downloadableIdeasRepositoryLiveMock =
    mock(DownloadableIdeasRepositoryLive.class);
  private DownloadableIdeasRepositoryDraft downloadableIdeasRepositoryDraftMock =
    mock(DownloadableIdeasRepositoryDraft.class);
  private IdeasResponseIdeaConverter ideasResponseIdeaConverterMock =
    mock(IdeasResponseIdeaConverter.class);
  private S3ImageRepository s3ImageRepositoryMock = mock(S3ImageRepository.class);
  private KigaS3FileRepository kigaS3FileRepositoryMock = mock(KigaS3FileRepository.class);
  private KigaPageImageRepository kigaPageImageRepositoryMock = mock(KigaPageImageRepository.class);
  private IdeaGroupRepository ideaGroupRepositoryDraftMock = mock(IdeaGroupRepository.class);
  private IdeaGroupToIdeaRepository ideaGroupToIdeaRepositoryMock =
    mock(IdeaGroupToIdeaRepository.class);
  private ModelMapper strictMatchingModelMapperSpy;
  private DownloadableIdeasService downloadableIdeasService;
  private SiteTreeTranslationGroupService siteTreeTranslationGroupServiceMock =
    mock(SiteTreeTranslationGroupService.class);
  private IndexerService indexer = mock(IndexerService.class);

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    ModelMapper strictMatchingModelMapper = new ModelMapper();
    strictMatchingModelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

    strictMatchingModelMapperSpy = spy(strictMatchingModelMapper);
    SortTreeManager sortTreeManagerMock = mock(SortTreeManager.class);

    downloadableIdeasService =
      new DownloadableIdeasService(siteTreeRepositoryMock, siteTreeLiveRepositoryMock,
        kigaS3FileViewModelConverterMock, memberIdeaCategoryServiceMock,
        downloadableIdeasRepositoryLiveMock, downloadableIdeasRepositoryDraftMock,
        ideasResponseIdeaConverterMock, s3ImageRepositoryMock, kigaS3FileRepositoryMock,
        kigaPageImageRepositoryMock, strictMatchingModelMapperSpy, ideaGroupRepositoryDraftMock,
        ideaGroupToIdeaRepositoryMock, siteTreeTranslationGroupServiceMock, indexer,
        sortTreeManagerMock);
  }

  @Test
  public void testGetAll() {
    Date lastEdited = new Date(100000);

    IdeasViewModelIdea ideasViewModelIdeaLive1 = new IdeasViewModelIdea();
    ideasViewModelIdeaLive1.setId(ID1);
    ideasViewModelIdeaLive1.setLastEdited(lastEdited);

    IdeasViewModelIdea ideasViewModelIdeaLive2 = new IdeasViewModelIdea();
    ideasViewModelIdeaLive2.setId(ID2);
    ideasViewModelIdeaLive2.setLastEdited(lastEdited);

    IdeasViewModelIdea ideasViewModelIdeaLive3 = new IdeasViewModelIdea();
    ideasViewModelIdeaLive3.setId(ID3);
    ideasViewModelIdeaLive3.setLastEdited(lastEdited);

    IdeasViewModelIdea ideasViewModelIdeaDraft1 = new IdeasViewModelIdea();
    ideasViewModelIdeaDraft1.setId(ID2);
    ideasViewModelIdeaDraft1.setLastEdited(lastEdited);

    Date anotherLastEdited = new Date(200000);

    IdeasViewModelIdea ideasViewModelIdeaDraft2 = new IdeasViewModelIdea();
    ideasViewModelIdeaDraft2.setId(ID3);
    ideasViewModelIdeaDraft2.setLastEdited(anotherLastEdited);

    IdeasViewModelIdea ideasViewModelIdeaDraft3 = new IdeasViewModelIdea();
    ideasViewModelIdeaDraft3.setId(ID4);
    ideasViewModelIdeaDraft3.setLastEdited(lastEdited);

    when(ideasResponseIdeaConverterMock.convertToResponse(anyListOf(KigaIdea.class)))
      .thenReturn(
        Arrays.asList(ideasViewModelIdeaLive1, ideasViewModelIdeaLive2, ideasViewModelIdeaLive3))
      .thenReturn(Arrays
        .asList(ideasViewModelIdeaDraft1, ideasViewModelIdeaDraft2, ideasViewModelIdeaDraft3));

    Locale locale = Locale.de;
    List<IdeasViewModelIdea> all = downloadableIdeasService.getAll(locale);
    assertEquals(4, all.size());

    assertEquals(ID2, all.get(0).getId());
    assertEquals(IdeaState.PUBLISHED, all.get(0).getState());
    assertEquals(ID3, all.get(1).getId());
    assertEquals(IdeaState.UPDATED, all.get(1).getState());
    assertEquals(ID4, all.get(2).getId());
    assertEquals(IdeaState.UNPUBLISHED, all.get(2).getState());
    assertEquals(ID1, all.get(3).getId());
    assertEquals(IdeaState.NOT_IN_DRAFT, all.get(3).getState());

    assertFalse(all.get(0).isDownload());
    assertFalse(all.get(1).isDownload());
    assertFalse(all.get(2).isDownload());
    assertFalse(all.get(3).isDownload());
  }

  @Test
  public void testFindByIdDraftEntityFound() {
    KigaIdea kigaIdeaMock = mock(KigaIdea.class);
    when(downloadableIdeasRepositoryDraftMock.getOneDownloadable(ID1)).thenReturn(kigaIdeaMock);
    downloadableIdeasService.findById(ID1);
    verify(ideasResponseIdeaConverterMock).convertToResponse(kigaIdeaMock);
  }

  @Test
  public void testFindByIdLiveEntityFound() {
    KigaIdea kigaIdeaMock = mock(KigaIdea.class);
    when(downloadableIdeasRepositoryLiveMock.getOneDownloadable(ID1)).thenReturn(kigaIdeaMock);
    downloadableIdeasService.findById(ID1);
    verify(ideasResponseIdeaConverterMock).convertToResponse(kigaIdeaMock);
  }

  @Test
  public void testFindUploadedFileByIdeaId() {
    KigaIdea kigaIdeaMock = mock(KigaIdea.class);
    KigaS3File kigaS3FileMock = mock(KigaS3File.class);
    when(kigaIdeaMock.getUploadedFile()).thenReturn(kigaS3FileMock);
    when(downloadableIdeasRepositoryLiveMock.getOneDownloadable(ID1)).thenReturn(kigaIdeaMock);
    downloadableIdeasService.findUploadedFileByIdeaId(ID1);
    verify(kigaS3FileViewModelConverterMock).convertToResponse(kigaS3FileMock);
  }

  @Test
  public void testAdd1() {
    ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest =
      new ModifyDownloadableIdeaRequest();
    modifyDownloadableIdeaRequest.setPreviewFileId(9L);
    modifyDownloadableIdeaRequest.setUploadedFileId(11L);
    modifyDownloadableIdeaRequest.setCategoryId(SUBCATEGORY_ID);
    modifyDownloadableIdeaRequest.setUrlSegment(URL_SEGMENT);
    modifyDownloadableIdeaRequest.setTitle(TITLE);
    modifyDownloadableIdeaRequest.setDescription(DESCRIPTION);
    modifyDownloadableIdeaRequest.setAgeGroup(AGE_GROUP);
    modifyDownloadableIdeaRequest.setInPool(true);
    modifyDownloadableIdeaRequest.setFacebookShare(true);
    modifyDownloadableIdeaRequest.setForParents(true);
    modifyDownloadableIdeaRequest.setSortMonth(SORT_MONTH);
    modifyDownloadableIdeaRequest.setSortYear(SORT_YEAR);
    modifyDownloadableIdeaRequest.setSortDayMini(SORT_DAY_MINI);
    modifyDownloadableIdeaRequest.setSortMonthMini(SORT_MONTH_MINI);
    modifyDownloadableIdeaRequest.setSortYearMini(SORT_YEAR_MINI);
    modifyDownloadableIdeaRequest.setLocale(Locale.de);
    modifyDownloadableIdeaRequest.setTranslationGroupId(TRANSLATION_GROUP_ID);

    KigaS3File kigaS3File = new KigaS3File();
    kigaS3File.setId(11L);
    when(kigaS3FileRepositoryMock.findOne(11L)).thenReturn(kigaS3File);

    IdeaCategoryDraft ideaCategoryDraft = new IdeaCategoryDraft();
    when(siteTreeRepositoryMock.findOne(SUBCATEGORY_ID)).thenReturn(ideaCategoryDraft);
    modifyDownloadableIdeaRequest.setPreviewFileId(10L);
    S3Image newS3Image = new S3Image();
    when(s3ImageRepositoryMock.findOne(10L)).thenReturn(newS3Image);

    KigaIdeaDraft returnedKigaIdea = new KigaIdeaDraft();
    when(downloadableIdeasRepositoryDraftMock.save(any(KigaIdeaDraft.class)))
      .thenReturn(returnedKigaIdea);

    downloadableIdeasService.add(modifyDownloadableIdeaRequest);

    verify(siteTreeTranslationGroupServiceMock)
      .addToTranslationGroup(TRANSLATION_GROUP_ID, returnedKigaIdea);

    ArgumentCaptor<KigaIdeaDraft> kigaIdeaDraftArgumentCaptor =
      ArgumentCaptor.forClass(KigaIdeaDraft.class);
    verify(downloadableIdeasRepositoryDraftMock).save(kigaIdeaDraftArgumentCaptor.capture());
    KigaIdeaDraft capturedIdea = kigaIdeaDraftArgumentCaptor.getValue();

    assertSame(ideaCategoryDraft, capturedIdea.getParent());

    KigaPageImage previewImage = capturedIdea.getPreviewImage();

    assertNull(capturedIdea.getId());
    assertEquals("KigaPageImage", previewImage.getClassName());
    assertSame(newS3Image, previewImage.getS3Image());
    assertSame(Locale.de.getValue(), previewImage.getLocale());

    verify(kigaPageImageRepositoryMock).save(previewImage);

    modifyDownloadableIdeaRequest.setUploadedFileId(12L);

    assertSame(kigaS3File, capturedIdea.getUploadedFile());

    assertEquals(URL_SEGMENT, capturedIdea.getUrlSegment());
    assertEquals(TITLE, capturedIdea.getTitle());
    assertEquals(DESCRIPTION, capturedIdea.getDescription());

    assertEquals(1, capturedIdea.getInPool().intValue());
    assertEquals(1, capturedIdea.getForParents().intValue());
    assertEquals(1, capturedIdea.getFacebookShare().intValue());
    assertEquals(SORT_MONTH, capturedIdea.getSortMonth());
    assertEquals(SORT_YEAR, capturedIdea.getSortYear());
    assertEquals(SORT_DAY_MINI, capturedIdea.getSortDayMini());
    assertEquals(SORT_MONTH_MINI, capturedIdea.getSortMonthMini());
    assertEquals(SORT_YEAR_MINI, capturedIdea.getSortYearMini());

    assertEquals(LayoutType.DYNAMIC, capturedIdea.getLayoutType());
    assertEquals(ArticleType.DOWNLOAD, capturedIdea.getArticleType());
    assertEquals("Article", capturedIdea.getClassName());
    assertEquals(new Double(1.0f), new Double(capturedIdea.getRatingComplete()));
    assertEquals(new Double(1.0f), new Double(capturedIdea.getRatingComplete()));
  }

  @Test
  public void testAdd2() {
    ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest =
      new ModifyDownloadableIdeaRequest();
    modifyDownloadableIdeaRequest.setCategoryId(SUBCATEGORY_ID);
    modifyDownloadableIdeaRequest.setUrlSegment(URL_SEGMENT);
    modifyDownloadableIdeaRequest.setTitle(TITLE);
    modifyDownloadableIdeaRequest.setDescription(DESCRIPTION);
    modifyDownloadableIdeaRequest.setAgeGroup(AGE_GROUP);
    modifyDownloadableIdeaRequest.setInPool(false);
    modifyDownloadableIdeaRequest.setFacebookShare(false);
    modifyDownloadableIdeaRequest.setForParents(false);
    modifyDownloadableIdeaRequest.setSortMonth(SORT_MONTH);
    modifyDownloadableIdeaRequest.setSortYear(SORT_YEAR);
    modifyDownloadableIdeaRequest.setSortDayMini(SORT_DAY_MINI);
    modifyDownloadableIdeaRequest.setSortMonthMini(SORT_MONTH_MINI);
    modifyDownloadableIdeaRequest.setSortYearMini(SORT_YEAR_MINI);
    modifyDownloadableIdeaRequest.setLocale(Locale.de);

    KigaIdeaDraft kigaIdeaDraft = new KigaIdeaDraft();
    kigaIdeaDraft.setId(ID1);
    KigaS3File kigaS3File = new KigaS3File();
    kigaS3File.setId(11L);
    kigaIdeaDraft.setUploadedFile(kigaS3File);

    KigaPageImage kigaPageImage = new KigaPageImage();
    S3Image s3Image = new S3Image();
    s3Image.setId(9L);
    kigaPageImage.setS3Image(s3Image);
    kigaIdeaDraft.setPreviewImage(kigaPageImage);

    when(downloadableIdeasRepositoryDraftMock.findOne(ID1)).thenReturn(kigaIdeaDraft);

    IdeaCategoryDraft ideaCategoryDraft = new IdeaCategoryDraft();
    when(siteTreeRepositoryMock.findOne(SUBCATEGORY_ID)).thenReturn(ideaCategoryDraft);
    modifyDownloadableIdeaRequest.setPreviewFileId(10L);
    S3Image newS3Image = new S3Image();
    when(s3ImageRepositoryMock.findOne(10L)).thenReturn(newS3Image);
    modifyDownloadableIdeaRequest.setUploadedFileId(12L);
    when(kigaS3FileRepositoryMock.findOne(12L)).thenReturn(kigaS3File);

    KigaIdeaDraft returnedKigaIdea = new KigaIdeaDraft();
    when(downloadableIdeasRepositoryDraftMock.save(any(KigaIdeaDraft.class)))
      .thenReturn(returnedKigaIdea);

    downloadableIdeasService.add(modifyDownloadableIdeaRequest);

    verify(siteTreeTranslationGroupServiceMock).createTranslationGroup(returnedKigaIdea);

    ArgumentCaptor<KigaIdeaDraft> kigaIdeaDraftArgumentCaptor =
      ArgumentCaptor.forClass(KigaIdeaDraft.class);
    verify(downloadableIdeasRepositoryDraftMock).save(kigaIdeaDraftArgumentCaptor.capture());
    KigaIdeaDraft capturedIdea = kigaIdeaDraftArgumentCaptor.getValue();

    assertSame(ideaCategoryDraft, capturedIdea.getParent());

    KigaPageImage previewImage = capturedIdea.getPreviewImage();
    verify(kigaPageImageRepositoryMock).save(previewImage);

    assertNull(capturedIdea.getId());
    assertEquals("KigaPageImage", previewImage.getClassName());
    assertSame(newS3Image, previewImage.getS3Image());
    assertSame(Locale.de.getValue(), previewImage.getLocale());
    assertSame(kigaS3File, capturedIdea.getUploadedFile());
    assertEquals(URL_SEGMENT, capturedIdea.getUrlSegment());
    assertEquals(TITLE, capturedIdea.getTitle());
    assertEquals(DESCRIPTION, capturedIdea.getDescription());
    assertEquals(0, capturedIdea.getInPool().intValue());
    assertEquals(0, capturedIdea.getForParents().intValue());
    assertEquals(0, capturedIdea.getFacebookShare().intValue());
    assertEquals(SORT_MONTH, capturedIdea.getSortMonth());
    assertEquals(SORT_YEAR, capturedIdea.getSortYear());
    assertEquals(SORT_DAY_MINI, capturedIdea.getSortDayMini());
    assertEquals(SORT_MONTH_MINI, capturedIdea.getSortMonthMini());
    assertEquals(SORT_YEAR_MINI, capturedIdea.getSortYearMini());
    assertEquals(LayoutType.DYNAMIC, capturedIdea.getLayoutType());
    assertEquals(ArticleType.DOWNLOAD, capturedIdea.getArticleType());
    assertEquals("Article", capturedIdea.getClassName());
    assertEquals(new Double(1.0f), new Double(capturedIdea.getRatingComplete()));
    assertEquals(new Double(1.0f), new Double(capturedIdea.getRatingComplete()));
  }

  @Test
  public void testUpdate1() {
    ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest =
      new ModifyDownloadableIdeaRequest();
    modifyDownloadableIdeaRequest.setId(ID1);
    modifyDownloadableIdeaRequest.setUploadedFileId(UPLOADED_FILE_ID);
    modifyDownloadableIdeaRequest.setCategoryId(SUBCATEGORY_ID);
    modifyDownloadableIdeaRequest.setUrlSegment(URL_SEGMENT);
    modifyDownloadableIdeaRequest.setTitle(TITLE);
    modifyDownloadableIdeaRequest.setDescription(DESCRIPTION);
    modifyDownloadableIdeaRequest.setAgeGroup(AGE_GROUP);
    modifyDownloadableIdeaRequest.setInPool(true);
    modifyDownloadableIdeaRequest.setFacebookShare(true);
    modifyDownloadableIdeaRequest.setForParents(true);
    modifyDownloadableIdeaRequest.setSortMonth(SORT_MONTH);
    modifyDownloadableIdeaRequest.setSortYear(SORT_YEAR);
    modifyDownloadableIdeaRequest.setSortDayMini(SORT_DAY_MINI);
    modifyDownloadableIdeaRequest.setSortMonthMini(SORT_MONTH_MINI);
    modifyDownloadableIdeaRequest.setSortYearMini(SORT_YEAR_MINI);
    modifyDownloadableIdeaRequest.setPreviewFileId(9L);

    IdeasResponseGroup ideasResponseGroup = new IdeasResponseGroup();
    ideasResponseGroup.setId(123L);
    modifyDownloadableIdeaRequest.setIdeaGroups(Collections.singletonList(ideasResponseGroup));

    KigaIdeaDraft kigaIdeaDraft = new KigaIdeaDraft();
    kigaIdeaDraft.setId(ID1);

    KigaPageImage kigaPageImage = new KigaPageImage();
    S3Image s3Image = new S3Image();
    s3Image.setId(9L);
    kigaPageImage.setS3Image(s3Image);
    kigaIdeaDraft.setPreviewImage(kigaPageImage);

    KigaS3File kigaS3File = new KigaS3File();
    kigaS3File.setId(UPLOADED_FILE_ID);
    kigaIdeaDraft.setUploadedFile(kigaS3File);

    kigaIdeaDraft.setIdeaGroupToIdeaList(new HashSet<>());


    IdeaGroupDraft ideaGroupDraft = new IdeaGroupDraft();
    ideaGroupDraft.setIdeaGroupToIdeaList(new ArrayList<>());
    when(ideaGroupRepositoryDraftMock.findOne(123L)).thenReturn(ideaGroupDraft);

    when(downloadableIdeasRepositoryDraftMock.findOne(ID1)).thenReturn(kigaIdeaDraft);

    IdeaCategoryDraft ideaCategoryDraft = new IdeaCategoryDraft();
    when(memberIdeaCategoryServiceMock.findOneDraft(SUBCATEGORY_ID)).thenReturn(ideaCategoryDraft);

    downloadableIdeasService.update(modifyDownloadableIdeaRequest);

    ArgumentCaptor<Set> ideaGroupToIdealistArgumentCaptor = ArgumentCaptor.forClass(Set.class);
    verify(ideaGroupToIdeaRepositoryMock).save(ideaGroupToIdealistArgumentCaptor.capture());
    Set<IdeaGroupToIdeaDraft> value = ideaGroupToIdealistArgumentCaptor.getValue();
    assertEquals(1, value.size());
    IdeaGroupToIdeaDraft next = value.iterator().next();
    IdeaGroupDraft ideaGroup = next.getIdeaGroup();
    assertSame(ideaGroup, ideaGroupDraft);
    assertSame(next.getArticle(), kigaIdeaDraft);

    ArgumentCaptor<KigaIdeaDraft> kigaIdeaDraftArgumentCaptor =
      ArgumentCaptor.forClass(KigaIdeaDraft.class);
    verify(downloadableIdeasRepositoryDraftMock).save(kigaIdeaDraftArgumentCaptor.capture());
    KigaIdeaDraft capturedIdea = kigaIdeaDraftArgumentCaptor.getValue();

    for (IdeaGroupToIdeaDraft ideaGroupToIdeaDraft : capturedIdea.getIdeaGroupToIdeaList()) {
      assertSame(ideaGroupToIdeaDraft.getIdeaGroup(), ideaGroupDraft);
    }

    assertSame(ideaCategoryDraft, capturedIdea.getParent());

    assertEquals(URL_SEGMENT, capturedIdea.getUrlSegment());
    assertEquals(TITLE, capturedIdea.getTitle());
    assertEquals(DESCRIPTION, capturedIdea.getDescription());

    assertEquals(1, capturedIdea.getInPool().intValue());
    assertEquals(1, capturedIdea.getForParents().intValue());
    assertEquals(1, capturedIdea.getFacebookShare().intValue());
    assertEquals(SORT_MONTH, capturedIdea.getSortMonth());
    assertEquals(SORT_YEAR, capturedIdea.getSortYear());
    assertEquals(SORT_DAY_MINI, capturedIdea.getSortDayMini());
    assertEquals(SORT_MONTH_MINI, capturedIdea.getSortMonthMini());
    assertEquals(SORT_YEAR_MINI, capturedIdea.getSortYearMini());
  }

  @Test
  public void testUpdate2() {
    ModifyDownloadableIdeaRequest modifyDownloadableIdeaRequest =
      new ModifyDownloadableIdeaRequest();
    modifyDownloadableIdeaRequest.setId(ID1);
    modifyDownloadableIdeaRequest.setCategoryId(SUBCATEGORY_ID);
    modifyDownloadableIdeaRequest.setUrlSegment(URL_SEGMENT);
    modifyDownloadableIdeaRequest.setTitle(TITLE);
    modifyDownloadableIdeaRequest.setDescription(DESCRIPTION);
    modifyDownloadableIdeaRequest.setAgeGroup(AGE_GROUP);
    modifyDownloadableIdeaRequest.setInPool(false);
    modifyDownloadableIdeaRequest.setFacebookShare(false);
    modifyDownloadableIdeaRequest.setForParents(false);
    modifyDownloadableIdeaRequest.setSortMonth(SORT_MONTH);
    modifyDownloadableIdeaRequest.setSortYear(SORT_YEAR);
    modifyDownloadableIdeaRequest.setSortDayMini(SORT_DAY_MINI);
    modifyDownloadableIdeaRequest.setSortMonthMini(SORT_MONTH_MINI);
    modifyDownloadableIdeaRequest.setSortYearMini(SORT_YEAR_MINI);
    modifyDownloadableIdeaRequest.setLocale(Locale.de);

    KigaIdeaDraft kigaIdeaDraft = new KigaIdeaDraft();
    kigaIdeaDraft.setId(ID1);
    KigaS3File kigaS3File = new KigaS3File();
    kigaS3File.setId(11L);
    kigaIdeaDraft.setUploadedFile(kigaS3File);

    KigaPageImage kigaPageImage = new KigaPageImage();
    S3Image s3Image = new S3Image();
    s3Image.setId(9L);
    kigaPageImage.setS3Image(s3Image);
    kigaPageImage.setClassName("KigaPageImage");
    kigaIdeaDraft.setPreviewImage(kigaPageImage);

    when(downloadableIdeasRepositoryDraftMock.findOne(ID1)).thenReturn(kigaIdeaDraft);

    IdeaCategoryDraft ideaCategoryDraft = new IdeaCategoryDraft();
    when(memberIdeaCategoryServiceMock.findOneDraft(SUBCATEGORY_ID)).thenReturn(ideaCategoryDraft);
    modifyDownloadableIdeaRequest.setPreviewFileId(10L);
    S3Image newS3Image = new S3Image();
    when(s3ImageRepositoryMock.findOne(10L)).thenReturn(newS3Image);
    modifyDownloadableIdeaRequest.setUploadedFileId(12L);
    KigaS3File kigaS3File1 = new KigaS3File();
    when(kigaS3FileRepositoryMock.findOne(12L)).thenReturn(kigaS3File1);

    downloadableIdeasService.update(modifyDownloadableIdeaRequest);

    ArgumentCaptor<KigaIdeaDraft> kigaIdeaDraftArgumentCaptor =
      ArgumentCaptor.forClass(KigaIdeaDraft.class);
    verify(downloadableIdeasRepositoryDraftMock).save(kigaIdeaDraftArgumentCaptor.capture());

    KigaIdeaDraft capturedIdea = kigaIdeaDraftArgumentCaptor.getValue();
    KigaPageImage previewImage = capturedIdea.getPreviewImage();
    assertSame(newS3Image, previewImage.getS3Image());
    assertSame(ideaCategoryDraft, capturedIdea.getParent());
    assertSame(Locale.de.getValue(), previewImage.getLocale());
    assertSame(kigaS3File1, capturedIdea.getUploadedFile());
    assertEquals("KigaPageImage", kigaPageImage.getClassName());
    assertEquals(URL_SEGMENT, capturedIdea.getUrlSegment());
    assertEquals(TITLE, capturedIdea.getTitle());
    assertEquals(DESCRIPTION, capturedIdea.getDescription());
    assertEquals(0, capturedIdea.getInPool().intValue());
    assertEquals(0, capturedIdea.getForParents().intValue());
    assertEquals(0, capturedIdea.getFacebookShare().intValue());
    assertEquals(SORT_MONTH, capturedIdea.getSortMonth());
    assertEquals(SORT_YEAR, capturedIdea.getSortYear());
    assertEquals(SORT_DAY_MINI, capturedIdea.getSortDayMini());
    assertEquals(SORT_MONTH_MINI, capturedIdea.getSortMonthMini());
    assertEquals(SORT_YEAR_MINI, capturedIdea.getSortYearMini());
    verify(kigaPageImageRepositoryMock).save(previewImage);
  }

  @Test
  public void testPublish() {
    IdListRequest idsToPublish = new IdListRequest();
    idsToPublish.setIds(Arrays.asList(ID1, SUBCATEGORY_ID, 3L));

    KigaIdeaDraft kigaIdeaDraftMock1 = mock(KigaIdeaDraft.class);
    when(kigaIdeaDraftMock1.getParent()).thenReturn(mock(SiteTreeDraft.class));
    KigaIdeaDraft kigaIdeaDraftMock2 = mock(KigaIdeaDraft.class);
    when(kigaIdeaDraftMock2.getParent()).thenReturn(mock(SiteTreeDraft.class));
    KigaIdeaDraft kigaIdeaDraftMock3 = mock(KigaIdeaDraft.class);
    when(kigaIdeaDraftMock3.getParent()).thenReturn(mock(SiteTreeDraft.class));
    when(downloadableIdeasRepositoryDraftMock.findOne(ID1)).thenReturn(kigaIdeaDraftMock1);
    when(downloadableIdeasRepositoryDraftMock.findOne(SUBCATEGORY_ID))
      .thenReturn(kigaIdeaDraftMock2);
    when(downloadableIdeasRepositoryDraftMock.findOne(3L)).thenReturn(kigaIdeaDraftMock3);

    KigaIdeaLive kigaIdeaLiveMock1 = mock(KigaIdeaLive.class);
    KigaIdeaLive kigaIdeaLiveMock2 = mock(KigaIdeaLive.class);
    KigaIdeaLive kigaIdeaLiveMock3 = mock(KigaIdeaLive.class);
    doReturn(kigaIdeaLiveMock1)
      .when(strictMatchingModelMapperSpy).map(kigaIdeaDraftMock1, KigaIdeaLive.class);
    doReturn(kigaIdeaLiveMock2)
      .when(strictMatchingModelMapperSpy).map(kigaIdeaDraftMock2, KigaIdeaLive.class);
    doReturn(kigaIdeaLiveMock3)
      .when(strictMatchingModelMapperSpy).map(kigaIdeaDraftMock3, KigaIdeaLive.class);

    downloadableIdeasService.publish(idsToPublish);

    verify(downloadableIdeasRepositoryLiveMock).save(kigaIdeaLiveMock1);
    verify(downloadableIdeasRepositoryLiveMock).save(kigaIdeaLiveMock2);
    verify(downloadableIdeasRepositoryLiveMock).save(kigaIdeaLiveMock3);
  }

  @Test
  public void testUnpublish() {
    IdListRequest idsToUnpublish = new IdListRequest();
    idsToUnpublish.setIds(Arrays.asList(ID1, SUBCATEGORY_ID, 3L));
    downloadableIdeasService.unpublish(idsToUnpublish);
    verify(downloadableIdeasRepositoryLiveMock).delete(ID1);
    verify(downloadableIdeasRepositoryLiveMock).delete(SUBCATEGORY_ID);
    verify(downloadableIdeasRepositoryLiveMock).delete(3L);
  }

  @Test
  public void testRemove() {
    downloadableIdeasService.remove(Arrays.asList(ID1, SUBCATEGORY_ID, 3L));
    verify(downloadableIdeasRepositoryDraftMock).delete(ID1);
    verify(downloadableIdeasRepositoryDraftMock).delete(SUBCATEGORY_ID);
    verify(downloadableIdeasRepositoryDraftMock).delete(3L);
  }

  @Test
  public void testValidateNullUrlSegment() {
    UrlSegmentValidationRequest urlSegmentValidationRequest = new UrlSegmentValidationRequest();
    UrlSegmentValidationResponse validationResponse =
      downloadableIdeasService.validate(urlSegmentValidationRequest);
    assertTrue(validationResponse.isValid());
    assertNull(validationResponse.getMessage());
  }

  @Test
  public void testValidateCategoryNotSelected() {
    UrlSegmentValidationRequest urlSegmentValidationRequest = new UrlSegmentValidationRequest();
    urlSegmentValidationRequest.setUrlSegment(URL_SEGMENT);
    urlSegmentValidationRequest.setCategoryId(0);
    UrlSegmentValidationResponse validationResponse =
      downloadableIdeasService.validate(urlSegmentValidationRequest);

    assertFalse(validationResponse.isValid());
    assertEquals("IDEAS_DOWNLOAD_IDEA_VALIDATION_URL_SEGMENT_MISSING_CATEGORY",
      validationResponse.getMessage());
  }

  @Test
  public void testValidateInvalidUrlSegmentFormat() {
    UrlSegmentValidationRequest urlSegmentValidationRequest = new UrlSegmentValidationRequest();
    urlSegmentValidationRequest.setCategoryId(SUBCATEGORY_ID);
    urlSegmentValidationRequest.setUrlSegment(INVALID_URL_SEGMENT);
    UrlSegmentValidationResponse validationResponse =
      downloadableIdeasService.validate(urlSegmentValidationRequest);

    assertFalse(validationResponse.isValid());
    assertEquals("IDEAS_DOWNLOAD_IDEA_VALIDATION_URL_SEGMENT_NOT_VALID",
      validationResponse.getMessage());
  }

  @Test
  public void testValidateNonUniquePerCategory() {
    UrlSegmentValidationRequest urlSegmentValidationRequest = new UrlSegmentValidationRequest();
    urlSegmentValidationRequest.setCategoryId(SUBCATEGORY_ID);
    urlSegmentValidationRequest.setUrlSegment(URL_SEGMENT);
    urlSegmentValidationRequest.setLocale(Locale.de);
    urlSegmentValidationRequest.setId(ID1);

    when(siteTreeRepositoryMock
      .findByLocaleAndUrlSegmentAndParentIdAndIdNot(Locale.de, URL_SEGMENT, SUBCATEGORY_ID, ID1))
      .thenReturn(new SiteTreeDraft());

    UrlSegmentValidationResponse validationResponse =
      downloadableIdeasService.validate(urlSegmentValidationRequest);

    assertFalse(validationResponse.isValid());
    assertEquals("IDEAS_DOWNLOAD_IDEA_VALIDATION_URL_SEGMENT_NOT_UNIQUE",
      validationResponse.getMessage());
  }

  @Test
  public void testValidateSuccess() {
    UrlSegmentValidationRequest urlSegmentValidationRequest = new UrlSegmentValidationRequest();
    urlSegmentValidationRequest.setCategoryId(SUBCATEGORY_ID);
    urlSegmentValidationRequest.setUrlSegment(URL_SEGMENT);
    urlSegmentValidationRequest.setLocale(Locale.de);
    urlSegmentValidationRequest.setId(ID1);

    UrlSegmentValidationResponse validationResponse =
      downloadableIdeasService.validate(urlSegmentValidationRequest);

    assertTrue(validationResponse.isValid());
    assertNull(validationResponse.getMessage());
  }
}
