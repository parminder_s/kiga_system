package com.kiga.ideas.task.cache;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.cms.web.request.ResolverRequest;
import com.kiga.content.domain.ss.IdeaGroup;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.ideas.web.viewmodel.ideahierarchy.IdeaHierarchyManager;
import org.junit.Test;

/** Created by rainerh on 16.10.16. */
public class IdeasCachePutterTest {
  @Test
  public void testViewModel() throws Exception {
    ResolverRequest request = new ResolverRequest();
    request.setAgeGroup(0);
    request.setPathSegments(new String[] {"ideas", "maths"});

    IdeaGroup ideaGroup = new IdeaGroupLive();
    IdeaHierarchyManager viewModelMapper = mock(IdeaHierarchyManager.class);

    new IdeasCachePutter(viewModelMapper).resolve(request, ideaGroup);
    verify(viewModelMapper).getViewModel(any(), any(), any());
  }
}
