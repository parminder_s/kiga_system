package com.kiga.ideas.web;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.ideas.web.response.IdeasResponseIdeaDetails;
import com.kiga.ideas.web.service.IdeaDetailsService;
import org.junit.Test;

public class IdeaDetailsControllerTest {
  @Test
  public void getPreviewUrl() {
    IdeaDetailsService ideaDetailsService = mock(IdeaDetailsService.class);
    IdeasResponseIdeaDetails detail = new IdeasResponseIdeaDetails();
    detail.setPreviewSiteUrl("foo");
    when(ideaDetailsService.getIdeaDetails(1L)).thenReturn(detail);
    IdeaDetailsController controller = new IdeaDetailsController(ideaDetailsService, null, null);

    assertEquals("foo", controller.getPreviewUrl(1L));
  }
}
