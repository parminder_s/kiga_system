package com.kiga.ideas.web.response;

import com.google.code.beanmatchers.BeanMatchers;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 17.09.16.
 */
public class ViewModelBeanTest {
  @Test
  public void testIdeasResponse() {
    Assert.assertThat(IdeaGroupViewModel.class, BeanMatchers.hasValidGettersAndSetters());
  }

  @Test
  public void testIdeasResponseCategory() {
    Assert.assertThat(IdeasResponseCategory.class, BeanMatchers.hasValidGettersAndSetters());
  }

  @Test
  public void testIdeasResponseGroup() {
    Assert.assertThat(IdeasResponseGroup.class, BeanMatchers.hasValidGettersAndSetters());
  }

  @Test
  public void testIdeasResponseIdea() {
    Assert.assertThat(IdeasViewModelIdea.class, BeanMatchers.hasValidGettersAndSetters());
  }

  @Test
  public void testIdeasResponseIdeaDetails() {
    Assert.assertThat(IdeasResponseIdeaDetails.class, BeanMatchers.hasValidGettersAndSetters());
  }

  @Test
  public void testIdeasResponseLinkedIdea() {
    Assert.assertThat(IdeasResponseLinkedIdea.class, BeanMatchers.hasValidGettersAndSetters());
  }

  @Test
  public void testPreviewPageResponse() {
    Assert.assertThat(PreviewPageResponse.class, BeanMatchers.hasValidGettersAndSetters());
  }

  @Test
  public void testIdeasResponseRating() {
    Assert.assertThat(IdeasResponseRating.class, BeanMatchers.hasValidGettersAndSetters());
  }
}
