package com.kiga.ideas.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import java.util.Optional;

/**
 * Created by rainerh on 01.12.16.
 */
public class IdeaPermissionServiceTest {
  private KigaIdea ideaFacebookShare;
  private KigaIdea ideaInPool;
  private KigaIdea ideaNotInPool;

  /**
   * setup the fixture ideas.
   */
  @Before
  public void setup() {
    ideaFacebookShare = new KigaIdeaLive();
    ideaFacebookShare.setFacebookShare(1);

    ideaInPool = new KigaIdeaLive();
    ideaInPool.setFacebookShare(0);
    ideaInPool.setInPool(1);

    ideaNotInPool = new KigaIdeaLive();
    ideaNotInPool.setFacebookShare(0);
    ideaNotInPool.setInPool(0);
  }

  @Test
  public void testAnonymous() {
    SecurityService securityService = mock(SecurityService.class);
    when(securityService.getSessionMember()).thenReturn(Optional.empty());

    assertGranted(ideaFacebookShare, securityService);
    assertDenied(ideaInPool, securityService);
    assertDenied(ideaNotInPool, securityService);
  }

  @Test
  public void testUserWithPool() {
    SecurityService securityService = constructSecurityService(1,"true", "true");

    assertGranted(ideaFacebookShare, securityService);
    assertGranted(ideaInPool, securityService);
    assertGranted(ideaNotInPool, securityService);
  }

  @Test
  public void testUserWithOnlyAccess() {
    SecurityService securityService = constructSecurityService(1,"false", "true");

    assertGranted(ideaFacebookShare, securityService);
    assertDenied(ideaInPool, securityService);
    assertGranted(ideaNotInPool, securityService);
  }

  @Test
  public void testUserWithoutAccess() {
    SecurityService securityService = constructSecurityService(1,"false", "false");

    assertGranted(ideaFacebookShare, securityService);
    assertDenied(ideaInPool, securityService);
    assertDenied(ideaNotInPool, securityService);
  }

  @Test
  public void testCmsUser() {
    SecurityService securityService = constructSecurityService(0,"false", "false");

    assertGranted(ideaFacebookShare, securityService);
    assertGranted(ideaInPool, securityService);
    assertGranted(ideaNotInPool, securityService);
  }

  private SecurityService constructSecurityService(
    Integer isCrm, String accessArticlesPool, String accessArticles) {
    SecurityService returner = mock(SecurityService.class);
    when(returner.getSessionMember()).thenReturn(Optional.of(new Member()));
    when(returner.getFromSession("is_crm")).thenReturn(isCrm.toString());
    when(returner.getFromSession("access_articles_pool")).thenReturn(accessArticlesPool);
    when(returner.getFromSession("access_articles")).thenReturn(accessArticles);
    return returner;
  }


  private void assertGranted(KigaIdea kigaIdea, SecurityService securityService) {
    assertPermission(kigaIdea,  securityService, true);
  }

  private void assertDenied(KigaIdea kigaIdea, SecurityService securityService) {
    assertPermission(kigaIdea, securityService, false);
  }

  private void assertPermission(
    KigaIdea kigaIdea, SecurityService securityService, boolean result) {
    Assert.assertEquals(result, new IdeaPermissionService(securityService)
      .permissionGranted(kigaIdea));
  }
}
