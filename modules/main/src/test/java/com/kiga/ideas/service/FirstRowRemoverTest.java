package com.kiga.ideas.service;

import com.kiga.ideas.web.viewmodel.kigaidea.FirstRowRemover;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;


import java.io.InputStream;

/**
 * Created by asv on 27.01.17.
 */
public class FirstRowRemoverTest {

  @Test
  public void rowRemoverFirstTest() throws Exception {

    InputStream stream = getClass()
      .getResourceAsStream("/com.kiga.ideas/ideaExample.html");
    String content = IOUtils.toString(stream);

    Document doc = Jsoup.parse(content);
    int rowsCount = doc.select(".container").first().children().size();

    FirstRowRemover rowRemover = new FirstRowRemover();
    String resultContent = rowRemover.analyzeContent(content);
    Document resultDoc = Jsoup.parse(resultContent);
    int resultRowsCount = resultDoc.select(".container").first().children().size();

    Assert.assertEquals(resultRowsCount, rowsCount - 2);
  }

  @Test
  public void rowRemoverSecondTest() throws Exception {
    // issue #983
    InputStream stream = getClass()
      .getResourceAsStream("/com.kiga.ideas/ideaNewTest.html");
    String content = IOUtils.toString(stream);

    Document doc = Jsoup.parse(content);
    int rowsCount = doc.select(".container").first().children().size();

    FirstRowRemover rowRemover = new FirstRowRemover();
    String resultContent = rowRemover.analyzeContent(content);
    Document resultDoc = Jsoup.parse(resultContent);
    int resultRowsCount = resultDoc.select(".container").first().children().size();

    Assert.assertEquals(resultRowsCount, rowsCount - 2);
  }

  @Test
  public void rowRemoverEmptyStringTest() throws Exception {
    String content = "";
    FirstRowRemover rowRemover = new FirstRowRemover();
    Assert.assertEquals(content, rowRemover.analyzeContent(content));
  }

}



