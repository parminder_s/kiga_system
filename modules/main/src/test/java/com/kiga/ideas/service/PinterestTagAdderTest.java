package com.kiga.ideas.service;

import com.kiga.ideas.web.viewmodel.kigaidea.PinterestTagAdder;
import com.kiga.s3.domain.S3Image;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by peter on 18.04.17.
 */
public class PinterestTagAdderTest {
  @Test
  public void addPinterestAttributes() {
    HashMap<String, String> cachedData = new HashMap<String, String>();
    cachedData.put("w650", "http://google.at");
    cachedData.put("wm2_w650", "http://watermarked");
    S3Image s3Image = new S3Image();
    s3Image.setCachedDataJson(cachedData);

    PinterestTagAdder pinterestTagAdder = new PinterestTagAdder();
    String html = "<img src=\"http://google.at\"/>";
    String result = pinterestTagAdder.analyzeContentImages(html, Arrays.asList(s3Image));
    Assert.assertEquals("<img src=\"http://google.at\" data-pin-media=\"http://watermarked\">", result);
  }

  @Test
  public void addPinterestAttributesClosingTag() {
    HashMap<String, String> cachedData = new HashMap<String, String>();
    cachedData.put("w650", "http://google.at");
    cachedData.put("wm2_w650", "http://watermarked");
    S3Image s3Image = new S3Image();
    s3Image.setCachedDataJson(cachedData);

    PinterestTagAdder pinterestTagAdder = new PinterestTagAdder();
    String html = "<img src=\"http://google.at\"></img>";
    String result = pinterestTagAdder.analyzeContentImages(html, Arrays.asList(s3Image));
    Assert.assertEquals("<img src=\"http://google.at\" data-pin-media=\"http://watermarked\">", result);
  }

  @Test
  public void addPinterestAttributesSelfclosing() {
    HashMap<String, String> cachedData = new HashMap<String, String>();
    cachedData.put("w650", "http://google.at");
    cachedData.put("wm2_w650", "http://watermarked");
    S3Image s3Image = new S3Image();
    s3Image.setCachedDataJson(cachedData);

    PinterestTagAdder pinterestTagAdder = new PinterestTagAdder();
    String html = "<img src=\"http://google.at\">";
    String result = pinterestTagAdder.analyzeContentImages(html, Arrays.asList(s3Image));
    Assert.assertEquals("<img src=\"http://google.at\" data-pin-media=\"http://watermarked\">", result);
  }

  @Test
  public void addPinterestAttributesSeveralImages() {
    HashMap<String, String> cachedData = new HashMap<String, String>();
    cachedData.put("w650", "http://google.at");
    cachedData.put("wm2_w650", "http://watermarked");
    S3Image s3Image = new S3Image();
    s3Image.setCachedDataJson(cachedData);

    String html = "<div><img src=\"http://google.at\"><p><img src=\"here\"></img></p></div>";
    PinterestTagAdder pinterestTagAdder = new PinterestTagAdder();
    String result = pinterestTagAdder.analyzeContentImages(html, Arrays.asList(s3Image));
    Assert.assertEquals(
      ("<div>\n <img src=\"http://google.at\" data-pin-media=\"http://watermarked\">\n"
        + " <p><img src=\"here\"></p>\n</div>"), result);

  }
}
