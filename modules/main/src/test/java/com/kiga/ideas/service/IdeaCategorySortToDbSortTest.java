package com.kiga.ideas.service;

import static com.kiga.ideas.service.DbSortValues.sortDate;
import static com.kiga.ideas.service.DbSortValues.sortSequence;

import com.kiga.content.domain.ss.IdeaGroup;
import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.domain.ss.property.SortStrategy;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 10.09.16.
 */
public class IdeaCategorySortToDbSortTest {
  private IdeaCategorySortToDbSort ideaCategorySortToDbSort = new DefaultIdeaCategorySortToDbSort();

  @Test
  public void ideaGroupSequence() throws Exception {
    IdeaGroup ideaGroup = new IdeaGroupLive();
    ideaGroup.setSortStrategy(SortStrategy.SEQUENCE);
    Assert.assertEquals(sortSequence, ideaCategorySortToDbSort.mapSort(ideaGroup));
  }

  @Test
  public void ideaGroupDate() throws Exception {
    IdeaGroup ideaGroup = new IdeaGroupLive();
    ideaGroup.setSortStrategy(SortStrategy.DATE);
    Assert.assertEquals(sortDate, ideaCategorySortToDbSort.mapSort(ideaGroup));
  }

  @Test
  public void ideaGroupInherited() {
    IdeaGroup ideaGroup = new IdeaGroupLive();
    ideaGroup.setSortStrategy(SortStrategy.INHERITED);
    IdeaGroupContainer container1 = new IdeaGroupContainerLive();
    container1.setSortStrategy(SortStrategy.INHERITED);
    IdeaGroupContainer container2 = new IdeaGroupContainerLive();
    container2.setSortStrategy(SortStrategy.SEQUENCE);
    ideaGroup.setParent(container1);
    container1.setParent(container2);

    Assert.assertEquals(sortSequence, ideaCategorySortToDbSort.mapSort(ideaGroup));
  }
}
