package com.kiga.ideas.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.draft.IdeaGroupDraft;
import com.kiga.content.domain.ss.draft.SiteTreeDraft;
import com.kiga.content.repository.ss.IdeaCategoryRepositoryProxy;
import com.kiga.content.repository.ss.draft.IdeaGroupRepository;
import com.kiga.ideas.web.converter.IdeaCategoryToIdeasResponseGroupConverter;
import com.kiga.ideas.web.converter.IdeaCategoryToIdeasResponseGroupWithParentsConverter;
import com.kiga.ideas.web.response.IdeasResponseGroup;
import com.kiga.ideas.web.response.IdeasResponseGroupWithParents;
import com.kiga.ideas.web.response.IdeasResponseIdeaDetails;
import com.kiga.ideas.web.service.IdeaDetailsService;
import com.kiga.main.locale.Locale;
import com.mysema.query.types.Predicate;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author bbs
 * @since 5/13/16.
 */
public class IdeaGroupServiceTest {
  private static final long IDEA_ID = 123L;
  private IdeaCategoryToIdeasResponseGroupConverter ideaCategoryToIdeasResponseGroupConverterMock =
    mock(IdeaCategoryToIdeasResponseGroupConverter.class);
  private IdeaCategoryToIdeasResponseGroupWithParentsConverter
    ideaCategoryToIdeasResponseGroupWithParentsConverterMock =
    mock(IdeaCategoryToIdeasResponseGroupWithParentsConverter.class);
  private IdeaCategoryRepositoryProxy ideaCategoryRepositoryProxyMock =
    mock(IdeaCategoryRepositoryProxy.class);
  private IdeaGroupRepository ideaGroupRepositoryMock = mock(IdeaGroupRepository.class);
  private CategoryTranslationService categoryTranslationServiceMock =
    mock(CategoryTranslationService.class);
  private IdeaDetailsService ideaDetailsServiceMock = mock(IdeaDetailsService.class);

  private IdeaGroupService ideaGroupService =
    new IdeaGroupService(ideaCategoryToIdeasResponseGroupConverterMock,
      ideaCategoryToIdeasResponseGroupWithParentsConverterMock, ideaCategoryRepositoryProxyMock,
      ideaGroupRepositoryMock, categoryTranslationServiceMock, ideaDetailsServiceMock);

  @Test
  public void testGetGroupsWithEntity() throws Exception {
    IdeaCategoryRepositoryProxy ideaCategoryModelMock = mock(IdeaCategoryRepositoryProxy.class);
    when(ideaCategoryModelMock.findByParentIdOrderBySortAsc(anyLong()))
      .thenReturn(Collections.emptyList());

    IdeasResponseGroup parentGroup = new IdeasResponseGroup();
    parentGroup.setId(5L);
    parentGroup.setSort(1);
    parentGroup.setSortTree("AA");

    IdeaGroupRepository ideaGroupRepositoryMock = mock(IdeaGroupRepository.class);
    IdeaCategoryToIdeasResponseGroupConverter groupConverterMock =
            mock(IdeaCategoryToIdeasResponseGroupConverter.class);

    List<IdeasResponseGroup> responseGroups =
      new IdeaGroupService(groupConverterMock,
        ideaCategoryToIdeasResponseGroupWithParentsConverterMock,
        ideaCategoryModelMock, ideaGroupRepositoryMock, categoryTranslationServiceMock,
        ideaDetailsServiceMock)
        .getGroupsSortTree(parentGroup);
    assertEquals(0, responseGroups.size());
  }

  @Test
  public void testGetUnassigned() {
    IdeaGroupDraft ideaGroupDraftMock = mock(IdeaGroupDraft.class);
    when(ideaGroupRepositoryMock.findAll(any(Predicate.class)))
      .thenReturn(Arrays.asList(ideaGroupDraftMock));

    when(
      ideaCategoryToIdeasResponseGroupWithParentsConverterMock
        .convertToResponse(any(Iterable.class)))
      .thenCallRealMethod();

    when(
      ideaCategoryToIdeasResponseGroupWithParentsConverterMock.convertToResponse(any(List.class)))
      .thenCallRealMethod();

    ideaGroupService.getUnassigned(123L, Locale.de);

    verify(ideaCategoryToIdeasResponseGroupWithParentsConverterMock)
      .convertToResponse(ideaGroupDraftMock);

    ArgumentCaptor<Predicate> predicateArgumentCaptor = ArgumentCaptor.forClass(Predicate.class);
    verify(ideaGroupRepositoryMock).findAll(predicateArgumentCaptor.capture());
    Predicate predicate = predicateArgumentCaptor.getValue();
    assertEquals("ideaGroupDraft.id not in com.mysema.query.DefaultQueryMetadata",
      predicate.toString().substring(0, predicate.toString().indexOf("@")));
  }

  @Test
  public void testGetUnassignedNullId() {
    IdeaGroupDraft ideaGroupDraft = new IdeaGroupDraft();
    SiteTreeDraft parent = new SiteTreeDraft();
    parent.setClassName("IdeaGroupMainContainer");
    ideaGroupDraft.setParent(parent);

    when(ideaGroupRepositoryMock.findAll(any(Predicate.class)))
      .thenReturn(Arrays.asList(ideaGroupDraft));

    when(
      ideaCategoryToIdeasResponseGroupWithParentsConverterMock
        .convertToResponse(any(Iterable.class)))
      .thenCallRealMethod();

    when(
      ideaCategoryToIdeasResponseGroupWithParentsConverterMock.convertToResponse(any(List.class)))
      .thenCallRealMethod();

    ideaGroupService.getUnassigned(null, Locale.de);

    verify(ideaCategoryToIdeasResponseGroupWithParentsConverterMock)
      .convertToResponse(ideaGroupDraft);

    ArgumentCaptor<Predicate> predicateArgumentCaptor = ArgumentCaptor.forClass(Predicate.class);
    verify(ideaGroupRepositoryMock).findAll(predicateArgumentCaptor.capture());
    Predicate predicate = predicateArgumentCaptor.getValue();
    assertEquals("ideaGroupDraft.className = IdeaGroup && ideaGroupDraft.locale = de",
      predicate.toString());
  }

  @Test
  public void testGetAssigned() {
    IdeaGroupDraft ideaGroupDraftMock = mock(IdeaGroupDraft.class);
    when(ideaGroupRepositoryMock.findAll(any(Predicate.class)))
      .thenReturn(Arrays.asList(ideaGroupDraftMock));

    when(
      ideaCategoryToIdeasResponseGroupWithParentsConverterMock
        .convertToResponse(any(Iterable.class)))
      .thenCallRealMethod();

    when(
      ideaCategoryToIdeasResponseGroupWithParentsConverterMock.convertToResponse(any(List.class)))
      .thenCallRealMethod();

    ideaGroupService.getAssigned(123L, Locale.de);

    verify(ideaCategoryToIdeasResponseGroupWithParentsConverterMock)
      .convertToResponse(ideaGroupDraftMock);

    ArgumentCaptor<Predicate> predicateArgumentCaptor = ArgumentCaptor.forClass(Predicate.class);
    verify(ideaGroupRepositoryMock).findAll(predicateArgumentCaptor.capture());
    Predicate predicate = predicateArgumentCaptor.getValue();
    assertEquals("ideaGroupDraft.id in com.mysema.query.DefaultQueryMetadata",
      predicate.toString().substring(0, predicate.toString().indexOf("@")));
  }

  @Test
  public void testGetTranslatedAssignedNoOriginalIdeaGroups() {
    IdeasResponseIdeaDetails ideasResponseIdeaDetails = new IdeasResponseIdeaDetails();
    ideasResponseIdeaDetails.setLocale(Locale.en);

    when(ideaDetailsServiceMock.getIdeaDetails(IDEA_ID)).thenReturn(ideasResponseIdeaDetails);

    IdeaGroupService ideaGroupServiceSpy = spy(ideaGroupService);

    doReturn(null).when(ideaGroupServiceSpy).getAssigned(IDEA_ID, Locale.en);

    List<IdeasResponseGroupWithParents> translatedAssigned =
      ideaGroupServiceSpy.getTranslatedAssigned(IDEA_ID, Locale.de);

    assertEquals(0, translatedAssigned.size());
  }

  @Test
  public void testGetTranslatedAssigned() {
    IdeasResponseIdeaDetails ideasResponseIdeaDetails = new IdeasResponseIdeaDetails();
    ideasResponseIdeaDetails.setLocale(Locale.en);

    when(ideaDetailsServiceMock.getIdeaDetails(IDEA_ID)).thenReturn(ideasResponseIdeaDetails);

    IdeaGroupService ideaGroupServiceSpy = spy(ideaGroupService);

    doReturn(Arrays
      .asList(createIdeasResponseGroupWithParents(100L), createIdeasResponseGroupWithParents(200L),
        createIdeasResponseGroupWithParents(300L))).when(ideaGroupServiceSpy)
      .getAssigned(IDEA_ID, Locale.en);

    @SuppressWarnings("unchecked")
    List<IdeasResponseGroupWithParents> mock = mock(List.class);

    when(categoryTranslationServiceMock
      .translateWithParents(Arrays.asList(100L, 200L, 300L), Locale.de))
      .thenReturn(mock);

    List<IdeasResponseGroupWithParents> translatedAssigned =
      ideaGroupServiceSpy.getTranslatedAssigned(IDEA_ID, Locale.de);

    assertSame(mock, translatedAssigned);
  }

  private IdeasResponseGroupWithParents createIdeasResponseGroupWithParents(long id) {
    IdeasResponseGroupWithParents ideasResponseGroupWithParents =
      new IdeasResponseGroupWithParents();
    ideasResponseGroupWithParents.setId(id);
    return ideasResponseGroupWithParents;
  }
}
