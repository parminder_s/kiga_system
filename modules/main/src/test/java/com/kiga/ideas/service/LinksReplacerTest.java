package com.kiga.ideas.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.content.repository.ss.live.SiteTreeLiveRepository;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.web.viewmodel.kigaidea.LinksReplacer;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by asv on 23.02.17.
 */
public class LinksReplacerTest {
  private SiteTreeLiveRepository siteTreeRepository = mock(SiteTreeLiveRepository.class);
  private SiteTreeUrlGenerator urlGenerator = mock(SiteTreeUrlGenerator.class);

  @Test
  public void analyzeContentLinksTest() throws Exception {
    String content = "<div class='rowDiv'><div class='span-4 last'><a "
      + "href='[sitetree_link id=1]'>Link 1</a></div></div>";
    SiteTreeLive siteTree = new SiteTreeLive();
    when(siteTreeRepository.findOne(1L)).thenReturn(siteTree);
    when(urlGenerator.getRelativeNgUrl(siteTree)).thenReturn("maths/counting/kitchen-sink");
    LinksReplacer linksReplacer = new LinksReplacer(siteTreeRepository, urlGenerator);
    String contentNew = linksReplacer.analyzeContentLinks(content);

    Document doc = Jsoup.parse(contentNew);
    String link = doc.select("a").first().attr("href");

    Assert.assertEquals(link, "maths/counting/kitchen-sink");
  }


  @Test
  public void ideaWithoutLinksTest() throws Exception {
    String content = "Test";
    LinksReplacer linksReplacer = new LinksReplacer(siteTreeRepository, urlGenerator);
    String contentNew = linksReplacer.analyzeContentLinks(content);

    Assert.assertEquals(contentNew, content);
  }
}



