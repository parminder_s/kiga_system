package com.kiga.ideas.web.converter;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.IdeaGroup;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.ideas.web.response.IdeasResponseGroup;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.modelmapper.ModelMapper;

/**
 * @author bbs
 * @since 5/15/16.
 */
public class IdeaCategoryToIdeaGroupViewModelGroupConverterTest {
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  private IdeaCategoryToIdeasResponseGroupConverter converter =
    new IdeaCategoryToIdeasResponseGroupConverter();

  @Test
  public void testNullEntityConversion() {
    Assert.assertNull(converter.convertToResponse((IdeaCategory) null));
  }

  @Test
  public void testNotNullEntityConversion() {
    IdeaGroupLive ideaGroup = new IdeaGroupLive();
    ideaGroup.setClassName("IdeaGroupMainContainer");
    IdeasResponseGroup group = converter.convertToResponse(ideaGroup);

    Assert.assertNotNull(group);

    Assert.assertEquals(ideaGroup.getId(), group.getId());
    Assert.assertEquals(ideaGroup.getUrlSegment(), group.getName());
    Assert.assertEquals(ideaGroup.getTitle(), group.getTitle());
    Assert.assertEquals(ideaGroup.getUrlSegment(), group.getUrlSegment());
    Assert.assertEquals(ideaGroup.getLocale(), group.getLocale());
  }

  @Test
  public void testInheritedIcon() {
    IdeaGroup ideaGroup = new IdeaGroupLive();
    ideaGroup.setIconCode("");
    IdeaGroup ideaGroupParent = new IdeaGroupLive();
    ideaGroupParent.setIconCode("something");
    ideaGroupParent.setClassName("IdeaGroupCategory");
    ideaGroup.setParent(ideaGroupParent);
    IdeaGroup main = new IdeaGroupLive();
    main.setClassName("IdeaGroupMainContainer");
    ideaGroupParent.setParent(main);

    IdeasResponseGroup ideasResponseGroup = converter.convertToResponse(ideaGroup);
    Assert.assertEquals("something", ideasResponseGroup.getIcon());
  }

  @Test
  public void testInheritedIconMain() {
    IdeaGroup ideaGroup = new IdeaGroupLive();
    ideaGroup.setIconCode("");
    IdeaGroup ideaGroupParent = new IdeaGroupLive();
    ideaGroupParent.setClassName("IdeaGroupMainContainer");
    ideaGroupParent.setIconCode("something");
    ideaGroup.setParent(ideaGroupParent);

    IdeasResponseGroup ideasResponseGroup = converter.convertToResponse(ideaGroup);
    Assert.assertEquals("icon-ideas", ideasResponseGroup.getIcon());
  }
}
