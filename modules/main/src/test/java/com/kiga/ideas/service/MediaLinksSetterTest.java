package com.kiga.ideas.service;

import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.ideas.web.service.MediaLinksSetter;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by asv on 19.02.17.
 */
public class MediaLinksSetterTest {

  @Test
  public void setMediaLinksTest() throws Exception {
    String content = "<div class='rowDiv'><div class='span-4 last'><a class='sm2_link' "
      + "href='https://kiga.s3.amazonaws.com/18429/Mama.mp3'>Mama.mp3</a>"
      + "</div></div><div class='rowDiv'><div class='span-4 last'><a class='videoplayer' "
      + "href='https://kiga.s3.amazonaws.com/18429/Video.mp4'>"
      + "<img class='rahmen1' src='https://kiga.s3.amazonaws.com/18251-18500/18429/864.png'/>"
      + "</a></div></div></div>";
    MediaLinksSetter mediaLinksSetter = new MediaLinksSetter();
    IdeasViewModelIdea returner = new IdeasViewModelIdea();
    mediaLinksSetter.setMediaLinks(returner, content);

    Assert.assertEquals(returner.getAudioLink(), "https://kiga.s3.amazonaws.com/18429/Mama.mp3");
    Assert.assertEquals(returner.getVideoLink(), "https://kiga.s3.amazonaws.com/18429/Video.mp4");
  }

  @Test
  public void ideaWithoutMediaLinksTest() throws Exception {
    String content = "";
    MediaLinksSetter mediaLinksSetter = new MediaLinksSetter();
    IdeasViewModelIdea returner = new IdeasViewModelIdea();
    mediaLinksSetter.setMediaLinks(returner, content);

    Assert.assertEquals(returner.getAudioLink(), null);
    Assert.assertEquals(returner.getVideoLink(), null);
  }
}



