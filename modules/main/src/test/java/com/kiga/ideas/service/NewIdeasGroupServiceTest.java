package com.kiga.ideas.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.properties.IdeasRetrieveProperties;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.main.locale.Locale;
import com.kiga.web.converter.ResponseConverter;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityNotFoundException;

/**
 * Created by rainerh on 11.09.16.
 */
public class NewIdeasGroupServiceTest {
  @Test
  public void testValidParent() throws Exception {
    KigaIdeaLive kigaIdeaWithoutParent = new KigaIdeaLive();
    kigaIdeaWithoutParent.setParent(null);

    KigaIdeaLive kigaNormal = new KigaIdeaLive();
    KigaIdea kigaParent = new KigaIdeaLive();
    kigaParent.setTitle("parent");
    kigaNormal.setParent(new KigaIdeaLive());

    KigaIdeaLive kigaEntityEx = mock(KigaIdeaLive.class);
    when(kigaEntityEx.getParent()).thenThrow(EntityNotFoundException.class);

    IdeasProperties ideasProperties = new IdeasProperties();
    ideasProperties.setAmountPerCategory(2);

    ResponseConverter responseFactory = mock(ResponseConverter.class);
    when(responseFactory.convertToResponse(any())).thenReturn(new IdeasViewModelIdea());

    NewIdeasRepository newIdeasRepository = mock(NewIdeasRepository.class);
    when(newIdeasRepository.getIdeasForAllAgeGroups(eq(Locale.en), eq(0), any()))
      .thenReturn(Arrays.asList(kigaIdeaWithoutParent, kigaNormal, kigaEntityEx));

    IdeasRetrieveProperties ideasRetrieveProperties = new IdeasRetrieveProperties();
    ideasRetrieveProperties.setLocale(Locale.en);
    ideasRetrieveProperties.setAgeGroupFilter(0);

    List<IdeasViewModelIdea> ideasResponseIdeas =
      new NewIdeasGroupService(responseFactory, newIdeasRepository, ideasProperties, null, null)
        .getIdeasForAllAgeGroups(ideasRetrieveProperties);

    Assert.assertEquals(1, ideasResponseIdeas.size());
  }
}
