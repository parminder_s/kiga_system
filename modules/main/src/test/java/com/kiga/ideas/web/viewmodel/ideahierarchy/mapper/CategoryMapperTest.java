package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.ideas.web.viewmodel.ideahierarchy.helper.IdeaCategoryHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.Category;
import com.kiga.main.locale.Locale;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CategoryMapperTest {
  private CategoryMapper categoryMapper;
  private IdeaCategory ideaCategory;
  private IdeaCategoryHelper ideaCategoryHelperMock;

  /** prepare Tests. */
  @Before
  public void prepareTests() {

    ideaCategory = new IdeaGroupLive();
    ideaCategory.setId(123L);
    ideaCategory.setUrlSegment("TEST_URL_SEGMENT");
    ideaCategory.setTitle("TEST_TITLE");
    ideaCategory.setLocale(Locale.de);
    ideaCategory.setCode("TEST_CODE");
    ideaCategory.setIconCode("TEST_ICON_CODE");
    ideaCategory.setDisableForFavourites(true);
    ideaCategory.setSortTree("ABBA");

    IdeaGroupContainer ideaGroupContainer = new IdeaGroupContainerLive();
    ideaGroupContainer.setClassName("IdeaGroupMainContainer");
    ideaCategory.setParent(ideaGroupContainer);

    ideaCategoryHelperMock = mock(IdeaCategoryHelper.class);
    when(ideaCategoryHelperMock.getIconCode(any(IdeaCategory.class))).thenReturn("TEST_ICON_CODE");
    when(ideaCategoryHelperMock.getDisableForFavourites(any(IdeaCategory.class))).thenReturn(true);

    categoryMapper = new CategoryMapper(ideaCategoryHelperMock);
  }

  @Test
  public void testConvertIdeaGroup() {
    Category groupViewModel = categoryMapper.convertIdeaCategory(ideaCategory);

    Assert.assertEquals("TEST_URL_SEGMENT", groupViewModel.getName());
    Assert.assertEquals("TEST_TITLE", groupViewModel.getTitle());
    Assert.assertEquals("TEST_ICON_CODE", groupViewModel.getIcon());
    Assert.assertEquals(true, groupViewModel.isDisableForFavourites());
  }

  @Test
  public void testConvertIdeaGroupIdeaCategoryNull() {

    ideaCategory = null;
    Category groupViewModel = categoryMapper.convertIdeaCategory(ideaCategory);

    Assert.assertNull(groupViewModel);
  }
}
