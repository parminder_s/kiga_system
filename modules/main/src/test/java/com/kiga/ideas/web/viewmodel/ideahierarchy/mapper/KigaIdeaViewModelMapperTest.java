package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.LinkedKigaPageGroup;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.property.ArticleType;
import com.kiga.content.domain.ss.property.LayoutType;
import com.kiga.content.service.KigaIdeaUrlGenerator;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.service.IdeaPermissionService;
import com.kiga.ideas.web.response.KigaIdeaViewModel;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.handler.KigaIdeaViewModelArticleTypeHandler;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.handler.KigaIdeaViewModelParentHandler;
import com.kiga.ideas.web.viewmodel.kigaidea.LinkedIdeasSetter;
import com.kiga.ideas.web.viewmodel.kigaidea.LinksReplacer;
import com.kiga.ideas.web.viewmodel.kigaidea.PreviewImagesSetter;
import com.kiga.main.MainProperties;
import com.kiga.main.PermissionMode;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class KigaIdeaViewModelMapperTest {

  private KigaIdeaViewModelMapper kigaIdeaViewModelMapper;
  private KigaIdea kigaIdea;
  private ViewModelMetaDataProvider viewModelMetaDataProviderMock;
  private IdeaPermissionService ideaPermissionServiceMock;
  private SiteTreeUrlGenerator siteTreeUrlGeneratorMock;
  private KigaIdeaUrlGenerator kigaIdeaUrlGeneratorMock;
  private LinkedIdeasSetter linkedIdeasSetterMock;
  private PreviewImagesSetter previewImagesSetterMock;
  private LinksReplacer linksReplacerMock;
  private MainProperties mainPropertiesMock;
  private Date testDate;
  private static List testImageList;
  private KigaIdeaViewModelArticleTypeHandler kigaIdeaViewModelArticleTypeHandlerMock;
  private KigaIdeaViewModelParentHandler kigaIdeaViewModelParentHandlerMock;

  /** prepare tests. */
  @Before
  public void prepareTests() {
    kigaIdea = new KigaIdeaLive();
    kigaIdea.setId(1L);
    kigaIdea.setTitle("TEST_TITLE");
    kigaIdea.setUrlSegment("TEST_URL_SEGMENT");
    kigaIdea.setDescription("TEST_DESCRIPTION");
    testDate = new Date();
    kigaIdea.setCreated(testDate);
    kigaIdea.setLayoutType(LayoutType.DYNAMIC);
    kigaIdea.setArticleType(ArticleType.DOWNLOAD);
    KigaS3File uploadedFileMock = mock(KigaS3File.class);
    when(uploadedFileMock.getName()).thenReturn("UPLOADED_FILE_TEST_NAME");
    when(uploadedFileMock.getId()).thenReturn(1L);
    kigaIdea.setUploadedFile(uploadedFileMock);
    kigaIdea.setContent("TEST_CONTENT");
    KigaPageImage kigaPageImageMock = mock(KigaPageImage.class);
    when(kigaPageImageMock.getS3Image()).thenReturn(mock(S3Image.class));
    testImageList = Collections.singletonList(kigaPageImageMock);
    kigaIdea.setKigaPageImages(testImageList);
    kigaIdea.setPreviewImage(kigaPageImageMock);
    kigaIdea.setLinkedKigaPageGroups(Collections.emptyList());
    kigaIdea.setAgeGroup(0);
    SiteTree parentMock = mock(SiteTree.class);
    when(parentMock.getTitle()).thenReturn("PARENT_TEST_TITLE");
    when(parentMock.getParent()).thenReturn(parentMock); // it's its own parent
    kigaIdea.setParent(parentMock);
    kigaIdea.setInPool(1);

    mainPropertiesMock = mock(MainProperties.class);
    when(mainPropertiesMock.getPermissionMode()).thenReturn(PermissionMode.FullTest);

    viewModelMetaDataProviderMock = mock(ViewModelMetaDataProvider.class);
    ideaPermissionServiceMock = mock(IdeaPermissionService.class);
    when(ideaPermissionServiceMock.permissionGranted(any(KigaIdea.class))).thenReturn(true);

    siteTreeUrlGeneratorMock = mock(SiteTreeUrlGenerator.class);
    when(siteTreeUrlGeneratorMock.getRelativeNgUrl(any(SiteTree.class)))
        .thenReturn("TEST_RELATIVE_URL");
    kigaIdeaUrlGeneratorMock = mock(KigaIdeaUrlGenerator.class);
    when(kigaIdeaUrlGeneratorMock.getRelativePreviewUrl(any(KigaIdea.class)))
        .thenReturn("RELATIVE_PREVIEW_TEST_URL");
    linkedIdeasSetterMock = mock(LinkedIdeasSetter.class);
    previewImagesSetterMock = mock(PreviewImagesSetter.class);
    linksReplacerMock = mock(LinksReplacer.class);
    when(linksReplacerMock.analyzeContentLinks(any(String.class)))
        .thenReturn("IDEA_CONTENT_TEST_STRING");
    kigaIdeaViewModelParentHandlerMock = mock(KigaIdeaViewModelParentHandler.class);
    kigaIdeaViewModelArticleTypeHandlerMock = mock(KigaIdeaViewModelArticleTypeHandler.class);

    kigaIdeaViewModelMapper =
        new KigaIdeaViewModelMapper(
            kigaIdeaViewModelParentHandlerMock,
            kigaIdeaViewModelArticleTypeHandlerMock,
            viewModelMetaDataProviderMock,
            ideaPermissionServiceMock,
            siteTreeUrlGeneratorMock,
            kigaIdeaUrlGeneratorMock,
            linkedIdeasSetterMock,
            previewImagesSetterMock,
            mainPropertiesMock);
  }

  private void checkViewModel(KigaIdeaViewModel kigaIdeaViewModel) {

    verify(ideaPermissionServiceMock).permissionGranted(any(KigaIdea.class));
    verify(kigaIdeaViewModelArticleTypeHandlerMock)
        .handleArticleType(any(KigaIdeaViewModel.class), any(KigaIdea.class));
    verify(viewModelMetaDataProviderMock)
        .fillWithMetaDataAndBreadcrumbs(any(KigaIdeaViewModel.class), any(KigaIdea.class));
    verify(kigaIdeaUrlGeneratorMock).getRelativePreviewUrl(any(KigaIdea.class));
    verify(previewImagesSetterMock)
        .setPreviewImages(any(KigaIdeaViewModel.class), any(KigaPageImage.class));
    verify(linkedIdeasSetterMock)
        .setLinkedIdeas(any(KigaIdeaViewModel.class), anyListOf(LinkedKigaPageGroup.class));
    verify(siteTreeUrlGeneratorMock).getRelativeUrl(any(SiteTree.class));
    verify(kigaIdeaViewModelParentHandlerMock)
        .handleParent(any(KigaIdeaViewModel.class), any(KigaIdea.class));
    verify(ideaPermissionServiceMock).permissionGranted(any(KigaIdea.class));
    long id = kigaIdeaViewModel.getId();
    Assert.assertEquals(id, 1L);
    Assert.assertEquals(kigaIdeaViewModel.getTitle(), "TEST_TITLE");
    Assert.assertEquals(kigaIdeaViewModel.getName(), "TEST_URL_SEGMENT");
    Assert.assertEquals(kigaIdeaViewModel.getDescription(), "TEST_DESCRIPTION");
    Assert.assertEquals(kigaIdeaViewModel.getCreateDate(), testDate);
    Assert.assertEquals(kigaIdeaViewModel.getLayoutType(), LayoutType.DYNAMIC);
    Assert.assertEquals(kigaIdeaViewModel.getArticleType(), ArticleType.DOWNLOAD);
    id = kigaIdeaViewModel.getUploadedFileId();
    Assert.assertEquals(id, 1L);
    Assert.assertEquals(kigaIdeaViewModel.getUploadedFileName(), "UPLOADED_FILE_TEST_NAME");
  }

  @Test
  public void testConvertViewModel() throws Exception {
    KigaIdeaViewModel kigaIdeaViewModel = kigaIdeaViewModelMapper.convertToViewModel(kigaIdea);
    checkViewModel(kigaIdeaViewModel);
  }

  @Test
  public void testConvertViewModelAgeGroupNull() throws Exception {
    kigaIdea.setAgeGroup(null);
    KigaIdeaViewModel kigaIdeaViewModel = kigaIdeaViewModelMapper.convertToViewModel(kigaIdea);
    checkViewModel(kigaIdeaViewModel);
  }

  @Test
  public void testConvertViewModelPermissionDenied() throws Exception {
    when(ideaPermissionServiceMock.permissionGranted(any(KigaIdea.class))).thenReturn(false);
    KigaIdeaViewModel kigaIdeaViewModel = kigaIdeaViewModelMapper.convertToViewModel(kigaIdea);
    Assert.assertEquals("TEST_RELATIVE_URL", kigaIdeaViewModel.getUrl());
  }

  @Test
  public void testConvertViewModelKigaIdeaNull() throws Exception {
    kigaIdea = null;
    Assert.assertNull(kigaIdeaViewModelMapper.convertToViewModel(kigaIdea));
  }

  @Test
  public void testConvertViewModelPermissionDeniedNotFullTest() throws Exception {
    when(ideaPermissionServiceMock.permissionGranted(any(KigaIdea.class))).thenReturn(false);
    when(mainPropertiesMock.getPermissionMode()).thenReturn(PermissionMode.NonFullTest);

    kigaIdeaViewModelMapper =
        new KigaIdeaViewModelMapper(
            kigaIdeaViewModelParentHandlerMock,
            kigaIdeaViewModelArticleTypeHandlerMock,
            viewModelMetaDataProviderMock,
            ideaPermissionServiceMock,
            siteTreeUrlGeneratorMock,
            kigaIdeaUrlGeneratorMock,
            linkedIdeasSetterMock,
            previewImagesSetterMock,
            mainPropertiesMock);

    KigaIdeaViewModel kigaIdeaViewModel = kigaIdeaViewModelMapper.convertToViewModel(kigaIdea);
    Assert.assertEquals("TEST_RELATIVE_URL", kigaIdeaViewModel.getUrl());
    Assert.assertEquals("fullSubscription", kigaIdeaViewModel.getPermission().getCode());
  }

  @Test
  public void testConvertViewModelPermissionDeniedNotFullTestNoPool() throws Exception {
    when(ideaPermissionServiceMock.permissionGranted(any(KigaIdea.class))).thenReturn(false);
    when(mainPropertiesMock.getPermissionMode()).thenReturn(PermissionMode.NonFullTest);
    kigaIdea.setInPool(0);

    kigaIdeaViewModelMapper =
        new KigaIdeaViewModelMapper(
            kigaIdeaViewModelParentHandlerMock,
            kigaIdeaViewModelArticleTypeHandlerMock,
            viewModelMetaDataProviderMock,
            ideaPermissionServiceMock,
            siteTreeUrlGeneratorMock,
            kigaIdeaUrlGeneratorMock,
            linkedIdeasSetterMock,
            previewImagesSetterMock,
            mainPropertiesMock);

    KigaIdeaViewModel kigaIdeaViewModel = kigaIdeaViewModelMapper.convertToViewModel(kigaIdea);
    Assert.assertEquals("TEST_RELATIVE_URL", kigaIdeaViewModel.getUrl());
    Assert.assertEquals("testSubscription", kigaIdeaViewModel.getPermission().getCode());
  }
}
