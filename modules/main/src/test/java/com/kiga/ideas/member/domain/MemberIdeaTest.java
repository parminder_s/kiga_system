package com.kiga.ideas.member.domain;

import com.google.code.beanmatchers.BeanMatchers;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 17.09.16.
 */
public class MemberIdeaTest {
  @Test
  public void beanTest() {
    Assert.assertThat(MemberIdea.class, BeanMatchers.hasValidGettersAndSetters());
  }
}
