package com.kiga.ideas.task.cache;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import com.kiga.cms.web.request.ResolverRequest;
import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.main.locale.Locale;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.slf4j.LoggerFactory;

import java.util.Collections;

/**
 * Created by rainerh on 15.10.16.
 */
public class IdeasCacheActualizerTest {
  IdeaCategory main;

  /**
   * setup SiteTree.
   */
  @Before
  public void setup() {
    main = new IdeaGroupContainerLive();
    main.setClassName("IdeaGroupMainContainer");
    main.setUrlSegment("ideas");
    main.setLocale(Locale.en);
  }

  @Test
  public void testResolving() throws Exception {
    IdeaGroupContainerLive main = new IdeaGroupContainerLive();
    main.setClassName("IdeaGroupMainContainer");
    main.setLocale(Locale.de);
    main.setUrlSegment("ideas");

    IdeaGroupContainerLive maths = new IdeaGroupContainerLive();
    maths.setLocale(Locale.de);
    maths.setUrlSegment("maths");
    maths.setParent(main);
    main.setChildren(Collections.singletonList(maths));

    IdeasCachePutter cachePutter = mock(IdeasCachePutter.class);
    ArgumentCaptor<ResolverRequest> requestCaptor = ArgumentCaptor.forClass(ResolverRequest.class);
    ArgumentCaptor<IdeaCategory> ideaGroupCaptor = ArgumentCaptor.forClass(IdeaCategory.class);
    IdeasCacheActualizer cacheActualizer = new IdeasCacheActualizer(cachePutter);
    cacheActualizer.run(maths, 1, true);

    verify(cachePutter, times(1)).resolve(requestCaptor.capture(), ideaGroupCaptor.capture());

    ResolverRequest resolverRequest = requestCaptor.getValue();
    assertEquals(new Integer(1), resolverRequest.getAgeGroup());
    String[] urlSegments = new String[] {"ideas", "maths"};
    LoggerFactory.getLogger(getClass())
      .info(StringUtils.join(resolverRequest.getPathSegments(), ","));
    assertArrayEquals(urlSegments, resolverRequest.getPathSegments());
    assertEquals(Locale.de, resolverRequest.getLocale());
  }

  @Test
  public void runWithException() throws Exception {
    IdeasCachePutter cachePutter = mock(IdeasCachePutter.class);

    IdeasCacheActualizer cacheActualizer = new IdeasCacheActualizer(cachePutter);
    when(cachePutter.resolve(any(), any())).thenThrow(Exception.class);
    cacheActualizer.run(main, 1, true);
    verify(cachePutter).resolve(any(), any());
  }

}
