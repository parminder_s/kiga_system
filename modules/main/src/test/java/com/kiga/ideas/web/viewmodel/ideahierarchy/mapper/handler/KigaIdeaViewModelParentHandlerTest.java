package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.handler;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.ideas.web.response.KigaIdeaViewModel;
import org.junit.Assert;
import org.junit.Test;

public class KigaIdeaViewModelParentHandlerTest {

  @Test
  public void testHandleParent() {
    KigaIdea kigaIdeaParentParent = new KigaIdeaLive();
    kigaIdeaParentParent.setTitle("PP_TITLE");
    kigaIdeaParentParent.setId(1L);
    kigaIdeaParentParent.setUrlSegment("PP_URL_SEGMENT");

    KigaIdea kigaIdeaParent = new KigaIdeaLive();
    kigaIdeaParent.setTitle("P_TITLE");
    kigaIdeaParent.setUrlSegment("P_URL_SEGMENT");
    kigaIdeaParent.setId(2L);
    kigaIdeaParent.setParent(kigaIdeaParentParent);

    KigaIdea kigaIdea = new KigaIdeaLive();
    kigaIdea.setParent(kigaIdeaParent);

    KigaIdeaViewModel kigaIdeaViewModel = new KigaIdeaViewModel();
    KigaIdeaViewModelParentHandler kigaIdeaViewModelParentHandler =
        new KigaIdeaViewModelParentHandler();
    kigaIdeaViewModelParentHandler.handleParent(kigaIdeaViewModel, kigaIdea);

    Assert.assertEquals("P_URL_SEGMENT", kigaIdeaViewModel.getCategoryName());
    long id = kigaIdeaViewModel.getCategoryId();
    Assert.assertEquals(2L, id);
    Assert.assertEquals("PP_URL_SEGMENT", kigaIdeaViewModel.getParentCategoryName());
    id = kigaIdeaViewModel.getParentCategoryId();
    Assert.assertEquals(1L, id);
  }

  @Test
  public void testHandleParentNull() {
    KigaIdea kigaIdea = new KigaIdeaLive();

    KigaIdeaViewModel kigaIdeaViewModel = new KigaIdeaViewModel();
    KigaIdeaViewModelParentHandler kigaIdeaViewModelParentHandler =
        new KigaIdeaViewModelParentHandler();
    kigaIdeaViewModelParentHandler.handleParent(kigaIdeaViewModel, kigaIdea);
  }
}
