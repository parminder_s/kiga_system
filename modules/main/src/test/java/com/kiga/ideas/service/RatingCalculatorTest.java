package com.kiga.ideas.service;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.ideas.web.response.IdeasResponseRating;
import org.junit.Assert;
import org.junit.Test;

/** Created by faxxe on 8/16/16. */
public class RatingCalculatorTest {

  RatingCalculatorService ratingCalculatorService = new RatingCalculatorService();

  /** test for calculatorservice. */
  @Test
  public void testHalfStar() {
    KigaIdea kigaIdea = new KigaIdeaLive();
    kigaIdea.setRating1(0);
    kigaIdea.setRating2(2);
    kigaIdea.setRating3(22);

    IdeasResponseRating ideasResponseRating =
        ratingCalculatorService.getIdeaResponseRating(kigaIdea);

    Assert.assertEquals((long) ideasResponseRating.getCount(), 24);
    Assert.assertEquals(3, ideasResponseRating.getRating(), 0.0);

    kigaIdea = new KigaIdeaLive();
    kigaIdea.setRating1(4);
    kigaIdea.setRating2(3);
    kigaIdea.setRating3(1);

    ideasResponseRating = ratingCalculatorService.getIdeaResponseRating(kigaIdea);

    Assert.assertEquals(8, (long) ideasResponseRating.getCount());
    Assert.assertEquals(2.5, ideasResponseRating.getRating(), 1.5);

    kigaIdea = new KigaIdeaLive();
    kigaIdea.setRating1(0);
    kigaIdea.setRating2(4);
    kigaIdea.setRating3(17);

    ideasResponseRating = ratingCalculatorService.getIdeaResponseRating(kigaIdea);

    Assert.assertEquals(21, (long) ideasResponseRating.getCount());
    Assert.assertEquals(4.5, ideasResponseRating.getRating(), 3.0);

    ideasResponseRating = ratingCalculatorService.getIdeaResponseRating(null);

    Assert.assertEquals(ideasResponseRating.getRating(), 0.0f, 0.0);
  }

  @Test
  public void ratingCalculatorTest() {
    KigaIdea kigaIdea = new KigaIdeaLive();
    kigaIdea.setRating1(1);
    kigaIdea.setRating2(1);
    kigaIdea.setRating3(1);
    kigaIdea.setRating4(1);
    kigaIdea.setRating5(1);

    IdeasResponseRating ideasResponseRating =
        ratingCalculatorService.getIdeaResponseRating(kigaIdea);

    Assert.assertEquals((long) ideasResponseRating.getCount(), 5);

    Assert.assertEquals(ideasResponseRating.getRating(), 3f, 0.0);

    ideasResponseRating = ratingCalculatorService.getIdeaResponseRating(null);

    Assert.assertEquals(ideasResponseRating.getRating(), 0.0f, 0.0);
  }

  @Test
  public void testWithNulls() {
    KigaIdea kigaIdea = new KigaIdeaLive();
    IdeasResponseRating rating = ratingCalculatorService.getIdeaResponseRating(kigaIdea);
    Assert.assertEquals(0, rating.getCount());
    Assert.assertEquals(0.0f, rating.getRating(), 0.0f);
  }
}
