package com.kiga.ideas;

import static org.hamcrest.core.IsInstanceOf.instanceOf;

import com.kiga.ideas.service.IdeaGroupToIdeaService;
import com.kiga.ideas.service.NewIdeasGroupService;
import com.kiga.s3.service.KigaAmazonS3Client;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 30.05.16.
 */
public class IdeasConfigTest {
  @Test
  public void getIdeaGroupToIdeaService() {
    IdeaGroupToIdeaService service = new IdeasConfig().getIdeaGroupToIdeaService(
      null, null, null, null);
    Assert.assertThat(service, instanceOf(IdeaGroupToIdeaService.class));
  }

  @Test
  public void getUploaderService() {
    KigaAmazonS3Client client = new IdeasConfig()
      .getUploaderService(null, new IdeasAmazonS3Parameters());
    Assert.assertThat(client, instanceOf(KigaAmazonS3Client.class));
  }

  @Test
  public void getAgeGroupIdeasService() {
    NewIdeasGroupService client = new IdeasConfig().getAgeGroupIdeasService(null, null, null,
      null, null);
    Assert.assertThat(client, instanceOf(NewIdeasGroupService.class));
  }
}
