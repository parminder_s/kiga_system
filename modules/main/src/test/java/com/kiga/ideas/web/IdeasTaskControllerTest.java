package com.kiga.ideas.web;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.ideas.task.cache.IdeasCacheExecutor;
import com.kiga.ideas.task.cache.IdeasCacheJob;
import com.kiga.ideas.web.request.IdeasCacheRequest;
import com.kiga.security.services.SecurityService;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by rainerh on 30.05.16.
 */
public class IdeasTaskControllerTest {
  private IdeasCacheExecutor ideasCacheExecutor = mock(IdeasCacheExecutor.class);
  private SecurityService securityService = mock(SecurityService.class);
  private IdeasCacheJob ideasCacheJob = mock(IdeasCacheJob.class);
  private IdeasTaskController controller;

  /**
   * initialize controller with mocks.
   */
  @Before
  public void initController() {
    controller = new IdeasTaskController(securityService, ideasCacheExecutor, ideasCacheJob);
  }

  @Test
  public void ideasCacheActualizer() {
    IdeasCacheRequest ideasCacheRequest = new IdeasCacheRequest();
    ideasCacheRequest.setAgeGroupFilters(new int[] {0});
    ideasCacheRequest.setSiteTreeId(1);
    controller.actualizeIdeasCache(ideasCacheRequest);
    verify(securityService).requiresContentAdmin();
    verify(ideasCacheExecutor).run(eq(1L), any(), eq(true));
  }

  @Test
  public void runJob() throws Exception {
    controller.runJob();
    verify(securityService).requiresContentAdmin();
    verify(ideasCacheJob).runJob();
  }
}
