package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.Category;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.IdeaGroupViewModel;
import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.ParentCategoryViewModel;
import java.util.Arrays;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IdeaGroupViewModelMapperTest {
  private IdeaGroupViewModelMapper ideaGroupViewModelMapper;
  private ParentCategoryViewModelMapper parentCategoryViewModelMapperMock;
  private IdeaMapper ideaMapperMock;
  private CategoryMapper categoryMapperMock;
  private IdeaCategory activeGroup;
  private Category groupViewModel;
  private SiteTree parent;

  /** prepare Tests. */
  @Before
  public void prepareTests() {

    activeGroup = new IdeaGroupLive();
    parent = new SiteTreeLive();
    activeGroup.setParent(parent);
    parent.setClassName("IdeaGroupMainContainer");
    parent.setTitle("ParentTitle");
    groupViewModel = new Category();

    parentCategoryViewModelMapperMock = mock(ParentCategoryViewModelMapper.class);
    when(parentCategoryViewModelMapperMock.convertSiblings(anyListOf(IdeaCategory.class)))
        .thenReturn(
            Arrays.asList(
                mock(ParentCategoryViewModel.class), mock(ParentCategoryViewModel.class)));

    ideaMapperMock = mock(IdeaMapper.class);

    categoryMapperMock = mock(CategoryMapper.class);
    when(categoryMapperMock.convertIdeaCategory(any(IdeaCategory.class)))
        .thenReturn(groupViewModel);

    ideaGroupViewModelMapper =
        new IdeaGroupViewModelMapper(
            mock(ViewModelMetaDataProvider.class),
            parentCategoryViewModelMapperMock,
            ideaMapperMock,
            categoryMapperMock);
  }

  @Test
  public void testConvertViewModel() {

    IdeaGroupViewModel viewModel =
        ideaGroupViewModelMapper.getViewModel(
            activeGroup,
            Arrays.asList(mock(KigaIdea.class), mock(KigaIdea.class)),
            Arrays.asList(mock(IdeaCategory.class), mock(IdeaCategory.class)));

    Assert.assertEquals(2, viewModel.getActiveCategory().getIdeas().size()); // Ideas
    Assert.assertEquals(2, viewModel.getParentCategories().size()); // Siblings
    Assert.assertEquals(true, viewModel.isShowBreadcrumbs());
    Assert.assertEquals(groupViewModel, viewModel.getActiveCategory()); // activeGroup converted?
  }
}
