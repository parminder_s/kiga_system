package com.kiga.ideas.task.sorttree;

import static org.junit.Assert.assertEquals;

import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.domain.ss.live.IdeaGroupLive;
import com.kiga.content.domain.ss.live.IdeaGroupToIdeaLive;
import com.kiga.content.sorttree.spec.SortTreeElement;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by rainerh on 29.03.17.
 */
public class IdeaHierarchySortTreeCalculatorMapperTest {
  @Test
  public void defaultTest() {
    IdeaGroupContainerLive travel = new IdeaGroupContainerLive();
    travel.setSort(1);

    IdeaGroupLive europe = new IdeaGroupLive();
    europe.setSort(1);
    IdeaGroupLive africa = new IdeaGroupLive();
    africa.setSort(2);
    travel.setChildren(Arrays.asList(africa, europe));

    IdeaGroupToIdeaLive igt1 = new IdeaGroupToIdeaLive();
    igt1.setSortOrder(1);
    igt1.setIdeaGroup(europe);

    IdeaGroupToIdeaLive igt2 = new IdeaGroupToIdeaLive();
    igt2.setIdeaGroup(europe);

    IdeaGroupToIdeaLive igt3 = new IdeaGroupToIdeaLive();
    igt3.setIdeaGroup(europe);

    europe.setIdeaGroupToIdeaList(Arrays.asList(igt1, igt2, igt3));
    africa.setIdeaGroupToIdeaList(Collections.emptyList());

    SortTreeElement root = new IdeaHierarchySortTreeCalculatorMapper().map(travel);

    assertEquals(travel, root.getSortTree());

    List<SortTreeElement> countries = root.getChildren();
    assertEquals(africa, countries.get(0).getSortTree());
    assertEquals(europe, countries.get(1).getSortTree());

    assertEquals(0, countries.get(0).getChildren().size());
    assertEquals(igt1, countries.get(1).getChildren().get(0).getSortTree());
    assertEquals(igt2, countries.get(1).getChildren().get(1).getSortTree());
    assertEquals(igt3, countries.get(1).getChildren().get(2).getSortTree());
  }
}
