package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.Page;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ViewModelMetaDataProviderTest {
  private Page entity;
  private Page entityParent;
  private Page entityParentParent;

  private ViewModelMetaDataProvider viewModelMetaDataProvider;

  @Rule public ExpectedException expectedException = ExpectedException.none();

  /** prepareTests. */
  @Before
  public void prepareTests() {
    entityParentParent = mock(Page.class);
    when(entityParentParent.getTitle()).thenReturn("TEST_TITLE");
    when(entityParentParent.getUrlSegment()).thenReturn("TEST_URL_SEGMENT");
    when(entityParentParent.getClassName()).thenReturn("TEST_CLASS_NAME");
    when(entityParentParent.getMetaDescription()).thenReturn("TEST_DESCRIPTION");
    when(entityParentParent.getMetaKeywords()).thenReturn("TEST_KEYWORDS");
    when(entityParentParent.getId()).thenReturn(0L);

    entityParent = mock(Page.class);
    when(entityParent.getTitle()).thenReturn("TEST_TITLE");
    when(entityParent.getUrlSegment()).thenReturn("TEST_URL_SEGMENT");
    when(entityParent.getClassName()).thenReturn("TEST_CLASS_NAME");
    when(entityParent.getMetaDescription()).thenReturn("TEST_DESCRIPTION");
    when(entityParent.getMetaKeywords()).thenReturn("TEST_KEYWORDS");
    when(entityParent.getId()).thenReturn(0L);
    when(entityParent.getParent()).thenReturn(entityParentParent);

    entity = mock(Page.class);
    when(entity.getTitle()).thenReturn("TEST_TITLE");
    when(entity.getUrlSegment()).thenReturn("TEST_URL_SEGMENT");
    when(entity.getClassName()).thenReturn("TEST_CLASS_NAME");
    when(entity.getMetaDescription()).thenReturn("TEST_DESCRIPTION");
    when(entity.getMetaKeywords()).thenReturn("TEST_KEYWORDS");
    when(entity.getId()).thenReturn(0L);
    when(entity.getParent()).thenReturn(entityParent);

    viewModelMetaDataProvider = new ViewModelMetaDataProvider();
  }

  @Test
  public void testFillWithMetaDataAndBreadcrumbsMoreThanTwo() {

    ViewModel viewModel = new ViewModel();
    viewModelMetaDataProvider.fillWithMetaDataAndBreadcrumbs(viewModel, entity);
    Assert.assertEquals(1, viewModel.getBreadcrumbs().size());
    Assert.assertEquals("TEST_TITLE", viewModel.getMetadata().getTitle());
    Assert.assertEquals("TEST_DESCRIPTION", viewModel.getMetadata().getDescription());
    Assert.assertEquals("TEST_URL_SEGMENT", viewModel.getBreadcrumbs().get(0).getUrlSegment());
    Assert.assertEquals("TEST_TITLE", viewModel.getBreadcrumbs().get(0).getLabel());
    Assert.assertEquals("TEST_KEYWORDS", viewModel.getMetadata().getKeywords());
    Assert.assertEquals("TEST_CLASS_NAME", viewModel.getResolvedType());
  }

  @Test
  public void testFillWithMetaDataAndBreadcrumbsTwoOrLess() {

    ViewModel viewModel = new ViewModel();
    viewModelMetaDataProvider.fillWithMetaDataAndBreadcrumbs(viewModel, entityParent);
    Assert.assertEquals(0, viewModel.getBreadcrumbs().size());
    Assert.assertEquals("TEST_TITLE", viewModel.getMetadata().getTitle());
    Assert.assertEquals("TEST_DESCRIPTION", viewModel.getMetadata().getDescription());
    Assert.assertEquals("TEST_KEYWORDS", viewModel.getMetadata().getKeywords());
    Assert.assertEquals("TEST_CLASS_NAME", viewModel.getResolvedType());
  }

  @Test
  public void testFillWithMetaDataAndBreadcrumbsNoEntity() {

    ViewModel viewModel = new ViewModel();
    expectedException.expect(NullPointerException.class);
    viewModelMetaDataProvider.fillWithMetaDataAndBreadcrumbs(viewModel, null);
    Assert.assertEquals(0, viewModel.getBreadcrumbs().size());
  }
}
