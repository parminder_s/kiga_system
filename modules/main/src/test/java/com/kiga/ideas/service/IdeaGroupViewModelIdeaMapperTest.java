package com.kiga.ideas.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.KigaPrintPreviewImage;
import com.kiga.content.domain.ss.draft.KigaCategoryDraft;
import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.domain.ss.live.KigaCategoryLive;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.content.domain.ss.live.KigaPrintPreviewImageLive;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.property.PreviewImage;
import com.kiga.ideas.property.PrintPreviewImage;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.ideas.web.service.IdeasResponseIdeaConverter;
import com.kiga.ideas.web.service.IdeasSeparateAgeGroupsConverter;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.web.converter.ResponseConverter;
import com.kiga.web.service.DefaultUrlGenerator;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

/**
 * @author bbs
 * @since 5/10/16.
 */
public class IdeaGroupViewModelIdeaMapperTest {
  @Rule public ExpectedException thrown = ExpectedException.none();
  private IdeasSeparateAgeGroupsConverter ideasSeparateAgeGroupsConverterMock =
      mock(IdeasSeparateAgeGroupsConverter.class);

  @Test
  public void testNotNullEntityConversion() throws IOException {
    KigaCategoryLive parent1 = new KigaCategoryLive();
    parent1.setUrlSegment("YYY");
    parent1.setTitle("UVW");

    KigaCategoryLive parent2 = new KigaCategoryLive();
    parent2.setId(2L);
    parent2.setParent(parent1);
    parent2.setUrlSegment("ZZZ");
    parent2.setTitle("XYZ");

    Date createDate1 = new Date();
    Date lastEdited = new Date();
    KigaIdeaLive idea = new KigaIdeaLive();
    idea.setId(1L);
    idea.setUrlSegment("ABC");
    idea.setTitle("DEF");
    idea.setDescription("DESC");
    idea.setCreated(createDate1);
    idea.setClassName("MAIN");
    idea.setRatingComplete(3.5f);
    idea.setPreviewImage(null);
    idea.setAgeGroup(2);
    idea.setParent(parent2);
    idea.setInPool(0);
    idea.setFacebookShare(0);
    idea.setForParents(0);
    idea.setLastEdited(lastEdited);
    idea.setUploadedFile(new KigaS3File());
    KigaPrintPreviewImageLive kigaPrintPreviewImage = new KigaPrintPreviewImageLive();
    idea.setKigaPrintPreviewImages(Arrays.asList(kigaPrintPreviewImage));

    KigaCategoryDraft parent3 = new KigaCategoryDraft();
    parent3.setUrlSegment("YYY2");

    KigaCategoryDraft parent4 = new KigaCategoryDraft();
    parent4.setParent(parent3);
    parent4.setUrlSegment("ZZZ2");

    KigaPageImage image = new KigaPageImage();
    S3Image s3Image = new S3Image();
    s3Image.setWidth(100);
    s3Image.setHeight(150);
    image.setS3Image(s3Image);

    Date createDate2 = new Date();
    KigaIdeaDraft idea2 = new KigaIdeaDraft();
    idea2.setId(2L);
    idea2.setUrlSegment("ABC2");
    idea2.setTitle("DEF2");
    idea2.setDescription("DESC2");
    idea2.setCreated(createDate2);
    idea2.setRatingComplete(4.5f);
    idea2.setClassName("MAIN2");
    idea2.setPreviewImage(image);
    idea2.setParent(parent4);
    idea2.setInPool(1);
    idea2.setFacebookShare(1);
    idea2.setForParents(1);
    idea2.setKigaPrintPreviewImages(null);

    S3Image uploadedFile = new S3Image();
    uploadedFile.setId(1L);
    uploadedFile.setUrl("url");
    uploadedFile.setName("Name");
    uploadedFile.setSize(123);
    idea2.setUploadedFile(uploadedFile);

    PrintPreviewImage printPreviewImage = new PrintPreviewImage();
    ImageSize imageSize = new ImageSize();
    imageSize.setHeight(50);
    imageSize.setWidth(50);
    printPreviewImage.setLarge(imageSize);
    printPreviewImage.setSmall(new ImageSize(50, 50));

    IdeasProperties ideasProperties = new IdeasProperties();
    ideasProperties.setPrintPreviewImage(printPreviewImage);

    PreviewImage previewImage = new PreviewImage();
    previewImage.setLarge(new ImageSize(50, 50));
    previewImage.setSmall(new ImageSize(20, 20));
    ideasProperties.setPreviewImage(previewImage);

    S3ImageResizeService s3ImageResizeServiceMock = mock(S3ImageResizeService.class);
    when(s3ImageResizeServiceMock.getResizedImageUrl(Mockito.any(), Mockito.any()))
        .thenReturn("MOCK");

    SiteTreeUrlGenerator siteTreeUrlGenerator = mock(SiteTreeUrlGenerator.class);
    DefaultUrlGenerator urlGenerator = mock(DefaultUrlGenerator.class);
    when(urlGenerator.getBaseUrl("/idea/downloadable/download/2")).thenReturn("proxy-url");
    when(siteTreeUrlGenerator.getRelativeUrl(any())).thenReturn("ideaUrl");
    when(siteTreeUrlGenerator.getRelativeNgUrl(any())).thenReturn("ideaNgUrl");

    RatingCalculatorService ratingCalculatorService = new RatingCalculatorService();

    ResponseConverter<KigaIdea, IdeasViewModelIdea> converter =
        new IdeasResponseIdeaConverter(
            s3ImageResizeServiceMock,
            ideasProperties,
            siteTreeUrlGenerator,
            urlGenerator,
            ratingCalculatorService,
            ideasSeparateAgeGroupsConverterMock);
    IdeasViewModelIdea response1 = converter.convertToResponse(idea);

    Assert.assertNotNull(response1);
    assertTrue(response1.getPublished());
    Assert.assertEquals(idea.getId(), response1.getId());
    Assert.assertEquals(idea.getUrlSegment(), response1.getName());
    Assert.assertEquals(idea.getTitle(), response1.getTitle());
    Assert.assertEquals(idea.getDescription(), response1.getDescription());
    Assert.assertTrue(response1.getPublished());
    Assert.assertEquals(false, response1.getInPool());
    Assert.assertEquals(false, response1.getFacebookShare());
    Assert.assertEquals(lastEdited, response1.getLastEdited());

    Assert.assertNotNull(response1.getPreviewPages());
    Assert.assertEquals(
        idea.getKigaPrintPreviewImages().size(), response1.getPreviewPages().size());

    Assert.assertEquals(idea.getParent().getTitle(), response1.getLevel2Title());
    Assert.assertEquals(idea.getParent().getParent().getTitle(), response1.getLevel1Title());

    Assert.assertEquals(response1.getCreateDate(), createDate1);
    Assert.assertEquals(idea.getParent().getUrlSegment(), response1.getCategoryName());
    Assert.assertEquals(idea.getParent().getId(), response1.getCategoryId());
    Assert.assertEquals(idea.getParent().getParent().getId(), response1.getParentCategoryId());
    Assert.assertEquals(
        idea.getParent().getParent().getUrlSegment(), response1.getParentCategoryName());
    Assert.assertEquals(response1.getPreviewUrl(), "");
    Assert.assertEquals(response1.getLargePreviewImageUrl(), "");
    Assert.assertEquals("ideaUrl", response1.getUrl());
    Assert.assertEquals("ideaNgUrl", response1.getNgUrl());

    IdeasViewModelIdea response2 = converter.convertToResponse(idea2);
    Assert.assertNotNull(response2);
    assertFalse(response2.getPublished());
    assertEquals("Name", response2.getFileName());
    assertEquals("proxy-url", response2.getFileProxyUrl());
    assertEquals("url", response2.getFileUrl());
    assertEquals(123, response2.getFileSize().intValue());
    assertEquals("MOCK", response2.getFilePreviewUrl());
    Assert.assertEquals(idea2.getId(), response2.getId());
    Assert.assertEquals(idea2.getUrlSegment(), response2.getName());
    Assert.assertEquals(idea2.getTitle(), response2.getTitle());
    Assert.assertEquals(true, response2.getFacebookShare());
    Assert.assertEquals(true, response2.getInPool());
    Assert.assertEquals(idea2.getDescription(), response2.getDescription());
    Assert.assertEquals(response2.getCreateDate(), createDate2);
    Assert.assertEquals(idea2.getParent().getUrlSegment(), response2.getCategoryName());
    Assert.assertEquals(
        idea2.getParent().getParent().getUrlSegment(), response2.getParentCategoryName());
    Assert.assertTrue(!response2.getPublished());
    Assert.assertEquals(response2.getPreviewUrl(), "MOCK");
    Assert.assertEquals(response2.getLargePreviewImageUrl(), "MOCK");
  }

  @Test
  public void testWithNullFacebookPoolAgeRange() {
    KigaIdea kigaIdea = new KigaIdeaLive();
    kigaIdea.setFacebookShare(null);
    kigaIdea.setInPool(null);
    kigaIdea.setKigaPrintPreviewImages(Collections.emptyList());

    S3ImageResizeService imageResizeService = mock(S3ImageResizeService.class);
    RatingCalculatorService ratingCalculatorService = mock(RatingCalculatorService.class);
    IdeasProperties ideasProperties = new IdeasProperties();
    SiteTreeUrlGenerator siteTreeUrlGenerator = mock(SiteTreeUrlGenerator.class);
    DefaultUrlGenerator urlGenerator = mock(DefaultUrlGenerator.class);

    IdeasViewModelIdea response =
        new IdeasResponseIdeaConverter(
                imageResizeService,
                ideasProperties,
                siteTreeUrlGenerator,
                urlGenerator,
                ratingCalculatorService,
                ideasSeparateAgeGroupsConverterMock)
            .convertToResponse(kigaIdea);

    Assert.assertEquals("", response.getAgeRange());
    Assert.assertEquals(false, response.getFacebookShare());
    Assert.assertEquals(false, response.getInPool());
  }

  @Test
  public void testPreviewWith2Exceptions() throws Exception {
    KigaIdea kigaIdea = new KigaIdeaLive();
    KigaPageImage kigaPageImage = new KigaPageImage();
    kigaIdea.setKigaPrintPreviewImages(Collections.emptyList());
    S3Image s3Imageioexception = new S3Image();
    kigaPageImage.setS3Image(s3Imageioexception);
    kigaIdea.setPreviewImage(kigaPageImage);
    IdeasProperties ideasProperties = new IdeasProperties();
    PreviewImage previewImage = new PreviewImage();
    previewImage.setLarge(new ImageSize(100, 100));
    previewImage.setSmall(new ImageSize(100, 100));
    ideasProperties.setPreviewImage(previewImage);

    S3ImageResizeService imageResizeService = mock(S3ImageResizeService.class);
    when(imageResizeService.getResizedImageUrl(eq(s3Imageioexception), any()))
        .thenThrow(IOException.class);
    RatingCalculatorService ratingCalculatorService = mock(RatingCalculatorService.class);
    SiteTreeUrlGenerator siteTreeUrlGenerator = mock(SiteTreeUrlGenerator.class);
    DefaultUrlGenerator urlGenerator = mock(DefaultUrlGenerator.class);
    ResponseConverter<KigaIdea, IdeasViewModelIdea> converter =
        new IdeasResponseIdeaConverter(
            imageResizeService,
            ideasProperties,
            siteTreeUrlGenerator,
            urlGenerator,
            ratingCalculatorService,
            ideasSeparateAgeGroupsConverterMock);
    IdeasViewModelIdea response = converter.convertToResponse(kigaIdea);

    Assert.assertThat(response.getLargePreviewImageUrl(), Matchers.isEmptyString());
    Assert.assertThat(response.getPreviewUrl(), Matchers.isEmptyString());

    S3Image s3Imageiae = new S3Image();
    when(imageResizeService.getResizedImageUrl(eq(s3Imageiae), any()))
        .thenThrow(IllegalArgumentException.class);
    kigaPageImage.setS3Image(s3Imageiae);
    response = converter.convertToResponse(kigaIdea);

    Assert.assertThat(response.getLargePreviewImageUrl(), Matchers.isEmptyString());
    Assert.assertThat(response.getPreviewUrl(), Matchers.isEmptyString());
  }

  @Test
  public void testPrintPreviewImageException() throws Exception {
    KigaIdea kigaIdea = new KigaIdeaLive();
    KigaPrintPreviewImage kigaPrintPreviewImage = new KigaPrintPreviewImageLive();
    kigaIdea.setKigaPrintPreviewImages(Collections.emptyList());
    S3Image s3Image = new S3Image();
    kigaPrintPreviewImage.setS3Image(s3Image);
    kigaIdea.setKigaPrintPreviewImages(Arrays.asList(kigaPrintPreviewImage));
    IdeasProperties ideasProperties = new IdeasProperties();
    PrintPreviewImage previewImage = new PrintPreviewImage();
    previewImage.setLarge(new ImageSize(100, 100));
    previewImage.setSmall(new ImageSize(100, 100));
    ideasProperties.setPrintPreviewImage(previewImage);

    S3ImageResizeService imageResizeService = mock(S3ImageResizeService.class);
    when(imageResizeService.getResizedImageUrl(eq(s3Image), any())).thenThrow(IOException.class);

    RatingCalculatorService ratingCalculatorService = mock(RatingCalculatorService.class);
    SiteTreeUrlGenerator siteTreeUrlGenerator = mock(SiteTreeUrlGenerator.class);
    DefaultUrlGenerator urlGenerator = mock(DefaultUrlGenerator.class);
    ResponseConverter<KigaIdea, IdeasViewModelIdea> converter =
        new IdeasResponseIdeaConverter(
            imageResizeService,
            ideasProperties,
            siteTreeUrlGenerator,
            urlGenerator,
            ratingCalculatorService,
            ideasSeparateAgeGroupsConverterMock);
    IdeasViewModelIdea response = converter.convertToResponse(kigaIdea);

    Assert.assertEquals(1, kigaIdea.getKigaPrintPreviewImages().size());
    Assert.assertEquals(0, response.getPreviewPages().size());
  }

  @Test
  public void testFilePreviewUrlCreationException() throws Exception {
    KigaCategoryLive parent1 = new KigaCategoryLive();
    parent1.setUrlSegment("YYY");
    parent1.setTitle("UVW");

    KigaCategoryLive parent2 = new KigaCategoryLive();
    parent2.setId(2L);
    parent2.setParent(parent1);
    parent2.setUrlSegment("ZZZ");
    parent2.setTitle("XYZ");

    KigaCategoryDraft parent3 = new KigaCategoryDraft();
    parent3.setUrlSegment("YYY2");

    KigaCategoryDraft parent4 = new KigaCategoryDraft();
    parent4.setParent(parent3);
    parent4.setUrlSegment("ZZZ2");

    KigaPageImage image = new KigaPageImage();
    S3Image s3Image = new S3Image();
    s3Image.setWidth(100);
    s3Image.setHeight(150);
    image.setS3Image(s3Image);

    Date createDate2 = new Date();
    KigaIdeaDraft idea = new KigaIdeaDraft();
    idea.setId(2L);
    idea.setUrlSegment("ABC2");
    idea.setTitle("DEF2");
    idea.setDescription("DESC2");
    idea.setCreated(createDate2);
    idea.setRatingComplete(4.5f);
    idea.setClassName("MAIN2");
    idea.setPreviewImage(image);
    idea.setParent(parent4);
    idea.setInPool(1);
    idea.setFacebookShare(1);
    idea.setForParents(1);
    idea.setKigaPrintPreviewImages(null);

    S3Image uploadedFile = new S3Image();
    uploadedFile.setId(1L);
    uploadedFile.setUrl("url");
    uploadedFile.setName("Name");
    uploadedFile.setSize(123);
    idea.setUploadedFile(uploadedFile);

    PrintPreviewImage printPreviewImage = new PrintPreviewImage();
    ImageSize imageSize = new ImageSize();
    imageSize.setHeight(50);
    imageSize.setWidth(50);
    printPreviewImage.setLarge(imageSize);
    printPreviewImage.setSmall(new ImageSize(50, 50));

    IdeasProperties ideasProperties = new IdeasProperties();
    ideasProperties.setPrintPreviewImage(printPreviewImage);

    PreviewImage previewImage = new PreviewImage();
    previewImage.setLarge(new ImageSize(50, 50));
    previewImage.setSmall(new ImageSize(20, 20));
    ideasProperties.setPreviewImage(previewImage);

    S3ImageResizeService s3ImageResizeServiceMock = mock(S3ImageResizeService.class);
    when(s3ImageResizeServiceMock.getResizedImageUrl(Mockito.any(), Mockito.any()))
        .thenThrow(IOException.class);

    SiteTreeUrlGenerator siteTreeUrlGenerator = mock(SiteTreeUrlGenerator.class);
    DefaultUrlGenerator urlGenerator = mock(DefaultUrlGenerator.class);
    when(urlGenerator.getBaseUrl("/idea/downloadable/download/2")).thenReturn("proxy-url");
    when(siteTreeUrlGenerator.getRelativeUrl(any())).thenReturn("ideaUrl");
    when(siteTreeUrlGenerator.getRelativeNgUrl(any())).thenReturn("ideaNgUrl");

    RatingCalculatorService ratingCalculatorService = new RatingCalculatorService();

    ResponseConverter<KigaIdea, IdeasViewModelIdea> converter =
        new IdeasResponseIdeaConverter(
            s3ImageResizeServiceMock,
            ideasProperties,
            siteTreeUrlGenerator,
            urlGenerator,
            ratingCalculatorService,
            ideasSeparateAgeGroupsConverterMock);

    IdeasViewModelIdea response = converter.convertToResponse(idea);
    Assert.assertNotNull(response);
    assertFalse(response.getPublished());
    assertEquals("Name", response.getFileName());
    assertEquals("proxy-url", response.getFileProxyUrl());
    assertEquals("url", response.getFileUrl());
    assertEquals(123, response.getFileSize().intValue());
    assertNull(response.getFilePreviewUrl());
    Assert.assertEquals(idea.getId(), response.getId());
    Assert.assertEquals(idea.getUrlSegment(), response.getName());
    Assert.assertEquals(idea.getTitle(), response.getTitle());
    Assert.assertEquals(true, response.getFacebookShare());
    Assert.assertEquals(true, response.getInPool());
    Assert.assertEquals(idea.getDescription(), response.getDescription());
    Assert.assertEquals(response.getCreateDate(), createDate2);
    Assert.assertEquals(idea.getParent().getUrlSegment(), response.getCategoryName());
    Assert.assertEquals(
        idea.getParent().getParent().getUrlSegment(), response.getParentCategoryName());
    Assert.assertTrue(!response.getPublished());
    Assert.assertTrue(StringUtils.isEmpty(response.getPreviewUrl()));
    Assert.assertTrue(StringUtils.isEmpty(response.getLargePreviewImageUrl()));
  }
}
