package com.kiga.ideas.task.cache;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import com.kiga.content.domain.ss.IdeaGroupContainer;
import com.kiga.content.domain.ss.live.IdeaGroupContainerLive;
import com.kiga.content.repository.ss.abstraction.IdeaGroupContainerRepositoryProxy;
import org.junit.Test;

/**
 * Created by rainerh on 16.10.16.
 */
public class IdeasCacheJobTest {
  @Test
  public void test() throws Exception {
    IdeaGroupContainerRepositoryProxy repositoryProxy =
      mock(IdeaGroupContainerRepositoryProxy.class);
    IdeasCacheActualizer actualizer = mock(IdeasCacheActualizer.class);
    IdeaGroupContainer ideaGroupContainer = new IdeaGroupContainerLive();
    when(repositoryProxy.findIdeaGroupMainContainer(any()))
      .thenReturn(ideaGroupContainer);

    IdeasCacheJob job = new IdeasCacheJob(actualizer, repositoryProxy);
    job.runJob();
    verify(actualizer, times(20)).run(eq(ideaGroupContainer), anyInt(), eq(true));
  }
}
