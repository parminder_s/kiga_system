package com.kiga.ideas.web.request;

import com.google.code.beanmatchers.BeanMatchers;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 17.09.16.
 */
public class IdeaDetailsRequestTest {
  @Test
  public void testBean() {
    Assert.assertThat(IdeaDetailsRequest.class, BeanMatchers.hasValidGettersAndSetters());
  }
}
