package com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.handler;

import com.kiga.ideas.web.viewmodel.ideahierarchy.viewmodel.Idea;
import org.junit.Assert;
import org.junit.Test;

public class MediaLinksHandlerTest {

  @Test
  public void testSetMediaLinksAll() {
    Idea returner = new Idea();
    String content =
        "<html><head><title>First parse</title></head>\"\n"
            + "  + \"<body><p>testContent</p></body>"
            + "<a class='sm2_link' href = 'test_href_audio'>testAudio</a>"
            + "<a class = 'videoplayer' href = 'test_href_video'>testVideo</a></html>";

    MediaLinksHandler mediaLinksHandler = new MediaLinksHandler();

    mediaLinksHandler.setMediaLinks(returner, content);
    Assert.assertEquals("test_href_audio", returner.getAudioLink());
    Assert.assertEquals("test_href_video", returner.getVideoLink());
  }

  @Test
  public void testSetMediaLinksNoContent() {
    Idea returner = new Idea();
    String content = "";

    MediaLinksHandler mediaLinksHandler = new MediaLinksHandler();

    mediaLinksHandler.setMediaLinks(returner, content);
    Assert.assertNull(returner.getAudioLink());
    Assert.assertNull(returner.getVideoLink());
  }

  @Test
  public void testSetMediaLinksContentNull() {
    Idea returner = new Idea();
    String content = null;

    MediaLinksHandler mediaLinksHandler = new MediaLinksHandler();

    mediaLinksHandler.setMediaLinks(returner, content);
    Assert.assertNull(returner.getAudioLink());
    Assert.assertNull(returner.getVideoLink());
  }

  @Test
  public void testSetMediaLinksNoVideo() {
    Idea returner = new Idea();
    String content =
        "<html><head><title>First parse</title></head>\"\n"
            + "  + \"<body><p>testContent</p></body>"
            + "<a class = 'sm2_link' href = 'test_href_audio'>testAudio</a></html>";

    MediaLinksHandler mediaLinksHandler = new MediaLinksHandler();

    mediaLinksHandler.setMediaLinks(returner, content);
    Assert.assertEquals("test_href_audio", returner.getAudioLink());
    Assert.assertNull(returner.getVideoLink());
  }

  @Test
  public void testSetMediaLinksNoAudio() {
    Idea returner = new Idea();
    String content =
        "<html><head><title>First parse</title></head>\"\n"
            + "  + \"<body><p>testContent</p></body>"
            + "<a class='videoplayer' href = 'test_href_video'>testVideo</a>";

    MediaLinksHandler mediaLinksHandler = new MediaLinksHandler();

    mediaLinksHandler.setMediaLinks(returner, content);
    Assert.assertNull(returner.getAudioLink());
    Assert.assertEquals("test_href_video", returner.getVideoLink());
  }
}
