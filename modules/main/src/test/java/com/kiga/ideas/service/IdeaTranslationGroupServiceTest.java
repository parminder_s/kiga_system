package com.kiga.ideas.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.domain.ss.draft.KigaCategoryDraft;
import com.kiga.content.domain.ss.draft.KigaIdeaDraft;
import com.kiga.content.domain.ss.draft.SiteTreeTranslationGroups;
import com.kiga.content.repository.ss.draft.KigaIdeaRepositoryDraft;
import com.kiga.content.repository.ss.draft.SiteTreeTranslationGroupsRepository;
import com.kiga.ideas.IdeasProperties;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.property.PreviewImage;
import com.kiga.ideas.web.response.IdeaTranslationGroup;
import com.kiga.ideas.web.response.IdeasResponseGroup;
import com.kiga.ideas.web.response.IdeasViewModelIdea;
import com.kiga.ideas.web.service.IdeasResponseIdeaConverter;
import com.kiga.main.locale.Locale;
import com.kiga.s3.domain.KigaS3File;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import org.apache.commons.lang3.BooleanUtils;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by robert on 03.09.17.
 */
@Service
public class IdeaTranslationGroupServiceTest {
  private static final long IDEA_ID = 100L;
  private static final long CATEGORY_ID = 101L;
  private static final long TRANSLATED_CATEGORY_ID = 102L;
  private static final long S3IMAGE_ID = 103L;
  private static final long KIGAS3FILE_ID = 104L;

  private static final Locale LOCALE = Locale.de;
  private static final int SORT_DAY_MINI = 9;
  private static final int SORT_MONTH_MINI = 10;
  private static final int SORT_YEAR_MINI = 2011;
  private static final int SORT_MONTH = 12;
  private static final int SORT_YEAR = 2013;

  private static final int EXPECTED_PREVIEW_IMAGE_WIDTH = 1000;
  private static final int EXPECTED_PREVIEW_IMAGE_HEIGHT = 2000;
  private static final int EXPECTED_PREVIEW_SIZE = 3000;
  private static final int EXPECTED_UPLOADED_SIZE = 4000;

  private static final int FACEBOOK_SHARE = 1;
  private static final int IN_POOL = 1;
  private static final int FOR_PARENTS = 1;

  private static final String UPLOADED_FILE_NAME = "uploaded-file";
  private static final String PREVIEW_IMAGE_NAME = "preview-image";
  private static final String PREVIEW_IMAGE_URL = "preview-image-url";

  private static final List<Integer> SEPARATE_AGE_GROUPS = Arrays.asList(1, 4, 16);
  private static final int AGE_GROUP_COMBINED = Long.valueOf(
    SEPARATE_AGE_GROUPS.stream().collect(Collectors.summarizingInt(Integer::intValue)).getSum())
    .intValue();

  private SiteTreeTranslationGroupsRepository siteTreeTranslationGroupsRepositoryMock =
    mock(SiteTreeTranslationGroupsRepository.class);
  private KigaIdeaRepositoryDraft kigaIdeaRepositoryDraftMock = mock(KigaIdeaRepositoryDraft.class);
  private IdeasResponseIdeaConverter ideasResponseIdeaConverterMock =
    mock(IdeasResponseIdeaConverter.class);
  private CategoryTranslationService categoryTranslationServiceMock =
    mock(CategoryTranslationService.class);
  private S3ImageResizeService s3ImageResizeServiceMock = mock(S3ImageResizeService.class);
  private IdeasProperties ideasPropertiesMock = mock(IdeasProperties.class);

  private IdeaTranslationGroupService translationGroupService =
    new IdeaTranslationGroupService(siteTreeTranslationGroupsRepositoryMock,
      kigaIdeaRepositoryDraftMock, ideasResponseIdeaConverterMock, categoryTranslationServiceMock,
      s3ImageResizeServiceMock, ideasPropertiesMock);

  @Test
  public void testGetById() {
    List<SiteTreeTranslationGroups> siteTreeTranslationGroupses = Arrays.asList(
      createSiteTreeTranslationGroup(2L),
      createSiteTreeTranslationGroup(3L),
      createSiteTreeTranslationGroup(4L)
    );
    when(siteTreeTranslationGroupsRepositoryMock.findAllByTranslationGroupId(1L))
      .thenReturn(siteTreeTranslationGroupses);

    List<KigaIdeaDraft> kigaIdeaDrafts = new ArrayList<>();
    when(kigaIdeaRepositoryDraftMock.findAll(eq(Arrays.asList(2L, 3L, 4L))))
      .thenReturn(kigaIdeaDrafts);

    List<IdeasViewModelIdea> ideasViewModelIdeas = new ArrayList<>();
    when(ideasResponseIdeaConverterMock.convertToResponse(kigaIdeaDrafts))
      .thenReturn(ideasViewModelIdeas);

    IdeaTranslationGroup translationGroup = translationGroupService.getByTranslationGroupId(1L);

    assertEquals(ideasViewModelIdeas, translationGroup.getIdeas());
    assertEquals(1L, translationGroup.getTranslationGroupId());
  }

  @Test
  public void testGetByIdeaId() {
    SiteTreeTranslationGroups siteTreeTranslationGroup = new SiteTreeTranslationGroups();
    siteTreeTranslationGroup.setTranslationGroupId(1L);

    when(siteTreeTranslationGroupsRepositoryMock.findByOriginalId(2L))
      .thenReturn(siteTreeTranslationGroup);

    List<SiteTreeTranslationGroups> siteTreeTranslationGroupses = Arrays.asList(
      createSiteTreeTranslationGroup(2L),
      createSiteTreeTranslationGroup(3L),
      createSiteTreeTranslationGroup(4L)
    );
    when(siteTreeTranslationGroupsRepositoryMock.findAllByTranslationGroupId(1L))
      .thenReturn(siteTreeTranslationGroupses);

    List<KigaIdeaDraft> kigaIdeaDrafts = new ArrayList<>();
    when(kigaIdeaRepositoryDraftMock.findAll(eq(Arrays.asList(2L, 3L, 4L))))
      .thenReturn(kigaIdeaDrafts);

    List<IdeasViewModelIdea> ideasViewModelIdeas = new ArrayList<>();
    when(ideasResponseIdeaConverterMock.convertToResponse(kigaIdeaDrafts))
      .thenReturn(ideasViewModelIdeas);

    IdeaTranslationGroup translationGroup = translationGroupService.getByIdeaId(2L);

    assertEquals(ideasViewModelIdeas, translationGroup.getIdeas());
    assertEquals(1L, translationGroup.getTranslationGroupId());
  }

  @Test
  public void testPrepareIdeaForTranslation() throws IOException {
    KigaIdeaDraft templateIdea = new KigaIdeaDraft();
    templateIdea.setId(IDEA_ID);

    templateIdea.setSortDayMini(SORT_DAY_MINI);
    templateIdea.setSortMonthMini(SORT_MONTH_MINI);
    templateIdea.setSortYearMini(SORT_YEAR_MINI);

    templateIdea.setSortMonth(SORT_MONTH);
    templateIdea.setSortYear(SORT_YEAR);

    templateIdea.setFacebookShare(FACEBOOK_SHARE);
    templateIdea.setInPool(IN_POOL);
    templateIdea.setForParents(FOR_PARENTS);

    templateIdea.setAgeGroup(AGE_GROUP_COMBINED);

    KigaCategoryDraft templateIdeaParentCategory = new KigaCategoryDraft();
    templateIdeaParentCategory.setId(CATEGORY_ID);
    templateIdea.setParent(templateIdeaParentCategory);

    S3Image s3Image = new S3Image();
    s3Image.setId(S3IMAGE_ID);
    s3Image.setName(PREVIEW_IMAGE_NAME);
    s3Image.setSize(EXPECTED_PREVIEW_SIZE);
    KigaPageImage kigaPageImage = new KigaPageImage();
    kigaPageImage.setS3Image(s3Image);
    templateIdea.setPreviewImage(kigaPageImage);

    KigaS3File kigaS3File = new S3Image();
    kigaS3File.setId(KIGAS3FILE_ID);
    kigaS3File.setName(UPLOADED_FILE_NAME);
    kigaS3File.setSize(EXPECTED_UPLOADED_SIZE);
    templateIdea.setUploadedFile(kigaS3File);

    KigaCategoryDraft expectedTranslatedParentCategory = new KigaCategoryDraft();
    expectedTranslatedParentCategory.setId(TRANSLATED_CATEGORY_ID);

    IdeasResponseGroup expectedTranslatedParentCategoryResponse = new IdeasResponseGroup();
    expectedTranslatedParentCategoryResponse.setId(TRANSLATED_CATEGORY_ID);

    PreviewImage previewImage = new PreviewImage();
    ImageSize large = new ImageSize(EXPECTED_PREVIEW_IMAGE_WIDTH, EXPECTED_PREVIEW_IMAGE_HEIGHT);
    previewImage.setLarge(large);
    when(ideasPropertiesMock.getPreviewImage()).thenReturn(previewImage);

    when(s3ImageResizeServiceMock.getResizedImageUrl(eq(s3Image), any(PaddedImageStrategy.class)))
      .thenReturn(PREVIEW_IMAGE_URL);

    when(kigaIdeaRepositoryDraftMock.findOne(IDEA_ID)).thenReturn(templateIdea);
    when(categoryTranslationServiceMock.translate(CATEGORY_ID, LOCALE))
      .thenReturn(expectedTranslatedParentCategoryResponse);

    IdeasViewModelIdea returned =
      translationGroupService.prepareIdeaForTranslation(IDEA_ID, LOCALE);

    assertEquals(FACEBOOK_SHARE, BooleanUtils.toInteger(returned.getFacebookShare()));
    assertEquals(FOR_PARENTS, BooleanUtils.toInteger(returned.getForParents()));
    assertEquals(IN_POOL, BooleanUtils.toInteger(returned.getInPool()));

    assertEquals(SORT_DAY_MINI, returned.getSortDayMini().intValue());
    assertEquals(SORT_MONTH_MINI, returned.getSortMonthMini().intValue());
    assertEquals(SORT_YEAR_MINI, returned.getSortYearMini().intValue());
    assertEquals(SORT_MONTH, returned.getSortMonth().intValue());
    assertEquals(SORT_YEAR, returned.getSortYear().intValue());

    assertEquals(SEPARATE_AGE_GROUPS, returned.getSeparatedAgeGroups());

    assertEquals(TRANSLATED_CATEGORY_ID, returned.getCategoryId().intValue());

    assertEquals(KIGAS3FILE_ID, returned.getUploadedFileId().longValue());
    assertEquals(UPLOADED_FILE_NAME, returned.getFileName());
    assertEquals(EXPECTED_UPLOADED_SIZE, returned.getFileSize().intValue());

    assertEquals(S3IMAGE_ID, returned.getPreviewFileId().longValue());
    assertEquals(PREVIEW_IMAGE_URL, returned.getLargePreviewImageUrl());
    assertEquals(PREVIEW_IMAGE_NAME, returned.getPreviewName());
    assertEquals(EXPECTED_PREVIEW_SIZE, returned.getPreviewSize().intValue());

    ArgumentCaptor<PaddedImageStrategy> paddedImageStrategyArgumentCaptor =
      ArgumentCaptor.forClass(PaddedImageStrategy.class);
    verify(s3ImageResizeServiceMock)
      .getResizedImageUrl(eq(s3Image), paddedImageStrategyArgumentCaptor.capture());
    PaddedImageStrategy paddedImageStrategy = paddedImageStrategyArgumentCaptor.getValue();
    assertEquals(EXPECTED_PREVIEW_IMAGE_WIDTH, paddedImageStrategy.getWidth());
    assertEquals(EXPECTED_PREVIEW_IMAGE_HEIGHT, paddedImageStrategy.getHeight());
  }

  @Test
  public void testPrepareIdeaForTranslationWithNullParent() throws IOException {
    KigaIdeaDraft templateIdea = new KigaIdeaDraft();
    templateIdea.setId(IDEA_ID);

    templateIdea.setSortDayMini(SORT_DAY_MINI);
    templateIdea.setSortMonthMini(SORT_MONTH_MINI);
    templateIdea.setSortYearMini(SORT_YEAR_MINI);

    templateIdea.setSortMonth(SORT_MONTH);
    templateIdea.setSortYear(SORT_YEAR);

    templateIdea.setFacebookShare(FACEBOOK_SHARE);
    templateIdea.setInPool(IN_POOL);
    templateIdea.setForParents(FOR_PARENTS);

    templateIdea.setAgeGroup(AGE_GROUP_COMBINED);

    S3Image s3Image = new S3Image();
    s3Image.setId(S3IMAGE_ID);
    s3Image.setName(PREVIEW_IMAGE_NAME);
    s3Image.setSize(EXPECTED_PREVIEW_SIZE);
    KigaPageImage kigaPageImage = new KigaPageImage();
    kigaPageImage.setS3Image(s3Image);
    templateIdea.setPreviewImage(kigaPageImage);

    KigaS3File kigaS3File = new S3Image();
    kigaS3File.setId(KIGAS3FILE_ID);
    kigaS3File.setName(UPLOADED_FILE_NAME);
    kigaS3File.setSize(EXPECTED_UPLOADED_SIZE);
    templateIdea.setUploadedFile(kigaS3File);

    PreviewImage previewImageProperty = new PreviewImage();
    ImageSize largePreviewSizeProperty = new ImageSize(EXPECTED_PREVIEW_IMAGE_WIDTH,
      EXPECTED_PREVIEW_IMAGE_HEIGHT);
    previewImageProperty.setLarge(largePreviewSizeProperty);
    when(ideasPropertiesMock.getPreviewImage()).thenReturn(previewImageProperty);
    when(s3ImageResizeServiceMock.getResizedImageUrl(eq(s3Image), any(PaddedImageStrategy.class)))
      .thenReturn(PREVIEW_IMAGE_URL);
    when(kigaIdeaRepositoryDraftMock.findOne(IDEA_ID)).thenReturn(templateIdea);

    IdeasViewModelIdea returned =
      translationGroupService.prepareIdeaForTranslation(IDEA_ID, LOCALE);

    assertEquals(FACEBOOK_SHARE, BooleanUtils.toInteger(returned.getFacebookShare()));
    assertEquals(FOR_PARENTS, BooleanUtils.toInteger(returned.getForParents()));
    assertEquals(IN_POOL, BooleanUtils.toInteger(returned.getInPool()));

    assertEquals(SORT_DAY_MINI, returned.getSortDayMini().intValue());
    assertEquals(SORT_MONTH_MINI, returned.getSortMonthMini().intValue());
    assertEquals(SORT_YEAR_MINI, returned.getSortYearMini().intValue());
    assertEquals(SORT_MONTH, returned.getSortMonth().intValue());
    assertEquals(SORT_YEAR, returned.getSortYear().intValue());

    assertEquals(SEPARATE_AGE_GROUPS, returned.getSeparatedAgeGroups());

    assertNull(returned.getCategoryId());

    assertEquals(KIGAS3FILE_ID, returned.getUploadedFileId().longValue());
    assertEquals(UPLOADED_FILE_NAME, returned.getFileName());
    assertEquals(EXPECTED_UPLOADED_SIZE, returned.getFileSize().intValue());

    assertEquals(S3IMAGE_ID, returned.getPreviewFileId().longValue());
    assertEquals(PREVIEW_IMAGE_URL, returned.getLargePreviewImageUrl());
    assertEquals(PREVIEW_IMAGE_NAME, returned.getPreviewName());
    assertEquals(EXPECTED_PREVIEW_SIZE, returned.getPreviewSize().intValue());

    ArgumentCaptor<PaddedImageStrategy> paddedImageStrategyArgumentCaptor =
      ArgumentCaptor.forClass(PaddedImageStrategy.class);
    verify(s3ImageResizeServiceMock)
      .getResizedImageUrl(eq(s3Image), paddedImageStrategyArgumentCaptor.capture());

    PaddedImageStrategy paddedImageStrategy = paddedImageStrategyArgumentCaptor.getValue();
    assertEquals(EXPECTED_PREVIEW_IMAGE_WIDTH, paddedImageStrategy.getWidth());
    assertEquals(EXPECTED_PREVIEW_IMAGE_HEIGHT, paddedImageStrategy.getHeight());
  }

  private SiteTreeTranslationGroups createSiteTreeTranslationGroup(long originalId) {
    SiteTreeTranslationGroups siteTreeTranslationGroup = new SiteTreeTranslationGroups();
    siteTreeTranslationGroup.setOriginalId(originalId);
    return siteTreeTranslationGroup;
  }
}
