package com.kiga.ideas.web.viewmodel.ideahierarchy.query.subengines;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.repository.ss.abstraction.NewIdeasRepository;
import com.kiga.favourites.repository.FavouriteNewIdeasRepository;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.helper.IdeaEngineHelper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import java.util.Collections;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NewIdeaBlockViewEngineTest {

  private FavouriteNewIdeasRepository favouriteNewIdeasRepositoryMock;
  private NewIdeasRepository newIdeasRepositoryMock;
  private IdeaEngineHelper ideaEngineHelperMock;

  private IdeaQueryParameter ideaQueryParameter;
  private NewIdeaBlockViewEngine newIdeaBlockViewEngine;

  /** prepare tests. */
  @Before
  public void prepareTests() {

    favouriteNewIdeasRepositoryMock = mock(FavouriteNewIdeasRepository.class);
    when(favouriteNewIdeasRepositoryMock.getAllIdeas(
            any(Locale.class), anyInt(), any(Member.class)))
        .thenReturn(Collections.emptyList());

    newIdeasRepositoryMock = mock(NewIdeasRepository.class);
    when(newIdeasRepositoryMock.getAllIdeas(any(Locale.class), anyInt()))
        .thenReturn(Collections.emptyList());
    ideaEngineHelperMock = mock(IdeaEngineHelper.class);
    when(ideaEngineHelperMock.hasValidParent(any(KigaIdea.class))).thenReturn(true);

    ideaQueryParameter = new IdeaQueryParameter();
    ideaQueryParameter.setAgeGroupFilter(0);
    ideaQueryParameter.setLocale(Locale.en);

    newIdeaBlockViewEngine =
        new NewIdeaBlockViewEngine(
            ideaEngineHelperMock, favouriteNewIdeasRepositoryMock, newIdeasRepositoryMock);
  }

  @Test
  public void testGetIdeasMember() {
    Member member = new Member();
    Optional optional = Optional.of(member);
    ideaQueryParameter.setMember(optional);

    Assert.assertEquals(
        Collections.emptyList(), newIdeaBlockViewEngine.getIdeas(ideaQueryParameter));
    verify(favouriteNewIdeasRepositoryMock)
        .getAllIdeas(any(Locale.class), anyInt(), any(Member.class));
  }

  @Test
  public void testGetIdeasNoMember() {
    ideaQueryParameter.setMember(Optional.empty());

    Assert.assertEquals(
        Collections.emptyList(), newIdeaBlockViewEngine.getIdeas(ideaQueryParameter));
    verify(newIdeasRepositoryMock).getAllIdeas(any(Locale.class), anyInt());
  }
}
