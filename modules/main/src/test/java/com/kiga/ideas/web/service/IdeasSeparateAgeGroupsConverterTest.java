package com.kiga.ideas.web.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

/**
 * @author bbs
 * @since 4/12/17.
 */
public class IdeasSeparateAgeGroupsConverterTest {
  @Test
  public void testConvert() {
    IdeasSeparateAgeGroupsConverter ideasSeparateAgeGroupsConverter =
      new IdeasSeparateAgeGroupsConverter();

    assertEquals(Collections.singletonList(1), ideasSeparateAgeGroupsConverter.convert(1));
    assertEquals(Collections.singletonList(2), ideasSeparateAgeGroupsConverter.convert(2));
    assertEquals(Collections.singletonList(4), ideasSeparateAgeGroupsConverter.convert(4));
    assertEquals(Collections.singletonList(8), ideasSeparateAgeGroupsConverter.convert(8));
    assertEquals(Collections.singletonList(16), ideasSeparateAgeGroupsConverter.convert(16));

    assertEquals(Arrays.asList(2, 8), ideasSeparateAgeGroupsConverter.convert(10));
    assertEquals(Arrays.asList(1, 4, 16), ideasSeparateAgeGroupsConverter.convert(21));
    assertEquals(Arrays.asList(1, 2, 4, 16), ideasSeparateAgeGroupsConverter.convert(23));
    assertEquals(Arrays.asList(1, 2, 4, 8, 16), ideasSeparateAgeGroupsConverter.convert(31));
  }
}
