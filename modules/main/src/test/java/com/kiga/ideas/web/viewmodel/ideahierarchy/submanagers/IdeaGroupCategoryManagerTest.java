package com.kiga.ideas.web.viewmodel.ideahierarchy.submanagers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.live.IdeaCategoryLive;
import com.kiga.ideas.web.viewmodel.ideahierarchy.mapper.IdeaGroupCategoryViewModelMapper;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.IdeaGroupCategoryQueryEngine;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaGroupCategoryQueryResult;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaSet;
import java.util.Collections;
import org.junit.Before;
import org.junit.Test;

public class IdeaGroupCategoryManagerTest {
  private IdeaGroupCategoryQueryEngine ideaGroupCategoryQueryEngineMock;
  private IdeaGroupCategoryViewModelMapper ideaGroupCategoryViewModelMapperMock;

  private IdeaGroupCategoryManager ideaGroupCategoryManager;
  private IdeaQueryParameter ideaQueryParameter;
  private IdeaCategory ideaCategory;
  private IdeaGroupCategoryQueryResult ideaGroupCategoryQueryResult;

  /** preparing tests. */
  @Before
  public void prepareTests() {
    ideaGroupCategoryQueryResult =
        IdeaGroupCategoryQueryResult.builder()
            .ideaSets(Collections.emptyList())
            .siblings(Collections.emptyList())
            .build();

    ideaGroupCategoryQueryEngineMock = mock(IdeaGroupCategoryQueryEngine.class);
    when(ideaGroupCategoryQueryEngineMock.query(any(IdeaQueryParameter.class)))
        .thenReturn(ideaGroupCategoryQueryResult);
    ideaGroupCategoryViewModelMapperMock = mock(IdeaGroupCategoryViewModelMapper.class);

    ideaGroupCategoryManager =
        new IdeaGroupCategoryManager(
            ideaGroupCategoryQueryEngineMock, ideaGroupCategoryViewModelMapperMock);
    ideaCategory = new IdeaCategoryLive();
    ideaQueryParameter = new IdeaQueryParameter();
    ideaQueryParameter.setIdeaCategory(ideaCategory);
  }

  @Test
  public void testGetViewModel() {
    ideaGroupCategoryManager.getViewModel(ideaQueryParameter);

    verify(ideaGroupCategoryViewModelMapperMock)
        .getViewModel(
            anyBoolean(),
            anyInt(),
            any(IdeaCategory.class),
            anyListOf(IdeaSet.class),
            anyListOf(IdeaCategory.class));
  }
}
