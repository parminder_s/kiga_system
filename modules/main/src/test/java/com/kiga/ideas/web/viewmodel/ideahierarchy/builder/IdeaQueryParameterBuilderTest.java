package com.kiga.ideas.web.viewmodel.ideahierarchy.builder;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.content.domain.ss.IdeaCategory;
import com.kiga.content.domain.ss.KigaIdea;
import com.kiga.content.domain.ss.live.KigaIdeaLive;
import com.kiga.ideas.web.viewmodel.ideahierarchy.query.model.IdeaQueryParameter;
import com.kiga.main.locale.Locale;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IdeaQueryParameterBuilderTest {

  private IdeaQueryParameterBuilder ideaQueryParameterBuilder;
  private QueryRequestParameters queryRequestParametersMock;
  private IdeaCategory ideaCategoryMock;

  /** Prepare Tests. */
  @Before
  public void prepareTests() {
    ideaQueryParameterBuilder = new IdeaQueryParameterBuilder();
    queryRequestParametersMock = mock(QueryRequestParameters.class);
    ideaCategoryMock = mock(IdeaCategory.class);
    when(ideaCategoryMock.getLocale()).thenReturn(Locale.de);
    when(queryRequestParametersMock.getMember()).thenReturn(Optional.empty());
    when(queryRequestParametersMock.getEntity()).thenReturn(ideaCategoryMock);
    when(queryRequestParametersMock.getAgeGroup()).thenReturn(0);
  }

  @Test
  public void testBuildQuery() {

    IdeaQueryParameter ideaQueryParameter =
        ideaQueryParameterBuilder.buildQuery(queryRequestParametersMock);

    Assert.assertEquals(Locale.de, ideaQueryParameter.getLocale());
    Assert.assertEquals(0, ideaQueryParameter.getAgeGroupFilter());
    Assert.assertEquals(ideaCategoryMock, ideaQueryParameter.getIdeaCategory());
  }

  @Test
  public void testBuildQueryKigaIdea() {

    KigaIdea kigaIdea = new KigaIdeaLive();
    kigaIdea.setLocale(Locale.de);
    when(queryRequestParametersMock.getEntity()).thenReturn(kigaIdea);

    IdeaQueryParameter ideaQueryParameter =
        ideaQueryParameterBuilder.buildQuery(queryRequestParametersMock);

    Assert.assertEquals(Locale.de, ideaQueryParameter.getLocale());
    Assert.assertEquals(0, ideaQueryParameter.getAgeGroupFilter());
    Assert.assertEquals(kigaIdea, ideaQueryParameter.getIdea());
  }

  @Test
  public void testBuildQueryInvalidEntity() {
    when(queryRequestParametersMock.getEntity()).thenReturn(Object.class);

    Assert.assertNull(ideaQueryParameterBuilder.buildQuery(queryRequestParametersMock));
  }
}
