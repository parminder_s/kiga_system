package com.kiga.security.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.security.domain.Member;
import com.kiga.security.spring.SecurityUtil;
import com.kiga.web.props.WebSecurityProperties;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

public class MemberControllerTest {
  @Test
  public void testMemberInfo() {
    SecurityUtil securityUtil = mock(SecurityUtil.class);
    Member member = new Member();
    member.setId(1L);
    member.setRoles(Collections.emptyList());

    when(securityUtil.getOptMember()).thenReturn(Optional.of(member));

    Map<String, Object> response =
        (Map<String, Object>) new MemberController(securityUtil, null, null).memberInfo();

    assertThat(response.get("id")).isEqualTo(1L);
  }

  @Test
  public void testMemberInfoWithAnonymous() {
    SecurityUtil securityUtil = mock(SecurityUtil.class);
    when(securityUtil.getOptMember()).thenReturn(Optional.ofNullable(null));
    String response = (String) new MemberController(securityUtil, null, null).memberInfo();

    assertThat(response).isEqualTo("No user logged in");
  }

  @Test
  public void testLoginUser() {
    WebSecurityProperties properties = new WebSecurityProperties();
    properties.setCookieName("foobar");
    HttpServletResponse response = mock(HttpServletResponse.class);
    ArgumentCaptor<Cookie> captor = ArgumentCaptor.forClass(Cookie.class);

    new MemberController(null, properties, response).loginUser("user");
    verify(response).addCookie(captor.capture());
    Cookie cookie = captor.getValue();
    assertThat(cookie).extracting("name", "value", "path").contains("foobar", "user", "/");
  }
}
