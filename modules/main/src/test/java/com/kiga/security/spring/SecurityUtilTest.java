package com.kiga.security.spring;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import com.kiga.security.domain.Member;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

@PrepareForTest(SecurityContextHolder.class)
@RunWith(PowerMockRunner.class)
public class SecurityUtilTest {
  @Test
  public void testGetMember() {
    mockStatic(SecurityContextHolder.class);
    SecurityContext context = mock(SecurityContext.class);
    when(SecurityContextHolder.getContext()).thenReturn(context);

    KigaAuthentication authentication = mock(KigaAuthentication.class);
    KigaUser kigaUser = new KigaUser();
    Member member = new Member();
    kigaUser.setMember(member);
    when(authentication.getUser()).thenReturn(kigaUser);
    when(context.getAuthentication()).thenReturn(authentication);

    SecurityUtil securityUtil = new SecurityUtil();

    assertThat(securityUtil.getMember()).isSameAs(member);
  }

  @Test
  public void testGetOptMember() {
    mockStatic(SecurityContextHolder.class);
    SecurityContext context = mock(SecurityContext.class);
    when(SecurityContextHolder.getContext()).thenReturn(context);

    KigaAuthentication authentication = mock(KigaAuthentication.class);
    when(context.getAuthentication()).thenReturn(authentication);

    SecurityUtil securityUtil = new SecurityUtil();

    assertThat(securityUtil.getOptMember().isPresent()).isFalse();
  }

  @Test
  public void testGetSessionId() {
    mockStatic(SecurityContextHolder.class);
    SecurityContext context = mock(SecurityContext.class);
    when(SecurityContextHolder.getContext()).thenReturn(context);

    KigaAuthentication authentication = mock(KigaAuthentication.class);
    when(authentication.getSessionId()).thenReturn("foo");
    when(context.getAuthentication()).thenReturn(authentication);

    SecurityUtil securityUtil = new SecurityUtil();

    assertThat(securityUtil.getSessionId()).isEqualToIgnoringWhitespace("foo");
  }

  @Test
  public void testHasRole() {
    mockStatic(SecurityContextHolder.class);
    SecurityContext context = mock(SecurityContext.class);
    when(SecurityContextHolder.getContext()).thenReturn(context);

    KigaAuthentication authentication = mock(KigaAuthentication.class);
    KigaUser kigaUser = mock(KigaUser.class);
    when(kigaUser.hasRole("admin")).thenReturn(true);
    when(authentication.getUser()).thenReturn(kigaUser);
    when(context.getAuthentication()).thenReturn(authentication);

    SecurityUtil securityUtil = new SecurityUtil();

    assertThat(securityUtil.hasRole("admin")).isTrue();
  }
}
