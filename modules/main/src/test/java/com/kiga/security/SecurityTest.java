package com.kiga.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


import com.kiga.security.domain.Member;
import com.kiga.security.domain.Role;
import com.kiga.security.exception.RoleException;
import com.kiga.security.repository.MemberRepository;
import com.kiga.security.services.DefaultSecurityService;
import com.kiga.web.security.session.Session;
import org.junit.Test;


import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by faxxe on 3/28/17.
 */
public class SecurityTest {


  @Test
  public void testSessionIdFromCookie() {
    MemberRepository memberRepository = mock(MemberRepository.class);
    Member member = new Member();

    when(memberRepository.findOne(any())).thenReturn(member);
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);

    Cookie[] cookies = {new Cookie("PHPSESSID", "testSession")};
    when(httpServletRequest.getCookies()).thenReturn(cookies);

    DefaultSecurityService securityService =
      new DefaultSecurityService(null, null, httpServletRequest);

    String sessionId = securityService.getSessionIdFromCookie();
    assertEquals(sessionId, "testSession");

    Cookie cookie1 = new Cookie("PHPSESSID", "fictionalSessionId");
    Cookie cookie2 = new Cookie("rembemberme", "sesss");
    Cookie cookie3 = new Cookie("rememberme", "anothersess");

    cookies = new Cookie[] {cookie1, cookie2, cookie3};

    httpServletRequest = mock(HttpServletRequest.class);
    securityService = new DefaultSecurityService(null, null, httpServletRequest);
    when(httpServletRequest.getCookies()).thenReturn(cookies);
    sessionId = securityService.getSessionIdFromCookie();
    assertEquals(sessionId, "fictionalSessionId");

    cookies = new Cookie[] {cookie2, cookie3};

    httpServletRequest = mock(HttpServletRequest.class);
    securityService = new DefaultSecurityService(null, null, httpServletRequest);
    when(httpServletRequest.getCookies()).thenReturn(cookies);
    sessionId = securityService.getSessionIdFromCookie();
    assertEquals(sessionId, "");

  }

  @Test(expected = RoleException.class)
  public void testRequireShopAdminException() {
    Member member = new Member();
    Session session = mock(Session.class);
    when(session.getMemberId()).thenReturn(Optional.of(12L));

    Role role = new Role();
    role.setCode("somerole");
    member.setRoles(Collections.singletonList(role));

    Cookie cookie1 = new Cookie("PHPSESSID", "123");
    Cookie[] cookies = new Cookie[] {cookie1};

    MemberRepository memberRepository = mock(MemberRepository.class);
    when(memberRepository.findOne(any())).thenReturn(member);
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    when(httpServletRequest.getCookies()).thenReturn(cookies);

    DefaultSecurityService securityService =
      new DefaultSecurityService(session, memberRepository, httpServletRequest);
    securityService.requireShopAdmin();

  }

  @Test
  public void testRequireShopAdmin() {
    Session session = mock(Session.class);
    when(session.getMemberId()).thenReturn(Optional.of(1234L));

    Role role = new Role();
    role.setCode("administrators");
    Member member = new Member();
    member.setRoles(Collections.singletonList(role));

    Cookie cookie1 = new Cookie("PHPSESSID", "123");
    Cookie[] cookies = new Cookie[] {cookie1};

    MemberRepository memberRepository = mock(MemberRepository.class);
    when(memberRepository.findOne(any())).thenReturn(member);
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    when(httpServletRequest.getCookies()).thenReturn(cookies);

    DefaultSecurityService securityService =
      new DefaultSecurityService(session, memberRepository, httpServletRequest);
    securityService.requireShopAdmin();

    role.setCode("shop-administrators");
    member.setRoles(Collections.singletonList(role));
    securityService.requireShopAdmin();

  }

  @Test
  public void testRequireContentAdmin() {
    Session session = mock(Session.class);
    when(session.getMemberId()).thenReturn(Optional.of(1234L));

    Role role = new Role();
    role.setCode("administrators");
    Member member = new Member();
    member.setRoles(Collections.singletonList(role));

    Cookie cookie1 = new Cookie("PHPSESSID", "123");
    Cookie[] cookies = new Cookie[] {cookie1};

    MemberRepository memberRepository = mock(MemberRepository.class);
    when(memberRepository.findOne(any())).thenReturn(member);
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    when(httpServletRequest.getCookies()).thenReturn(cookies);

    DefaultSecurityService securityService =
      new DefaultSecurityService(session, memberRepository, httpServletRequest);
    securityService.requiresContentAdmin();
  }

  @Test(expected = RoleException.class)
  public void testRequireContentAdminException() {
    Session session = mock(Session.class);
    when(session.getMemberId()).thenReturn(Optional.of(1234L));

    Role role = new Role();
    role.setCode("noAdmin ;)");
    Member member = new Member();
    member.setRoles(Collections.singletonList(role));

    Cookie cookie1 = new Cookie("PHPSESSID", "123");
    Cookie[] cookies = new Cookie[] {cookie1};

    MemberRepository memberRepository = mock(MemberRepository.class);
    when(memberRepository.findOne(any())).thenReturn(member);
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    when(httpServletRequest.getCookies()).thenReturn(cookies);

    DefaultSecurityService securityService =
      new DefaultSecurityService(session, memberRepository, httpServletRequest);
    securityService.requiresContentAdmin();
  }

  @Test
  public void testRequireCmsMember() {
    Session session = mock(Session.class);
    when(session.getMemberId()).thenReturn(Optional.of(1234L));

    Role role = new Role();
    Member member = new Member();
    member.setRoles(Collections.singletonList(role));

    Cookie cookie1 = new Cookie("PHPSESSID", "123");
    Cookie[] cookies = new Cookie[] {cookie1};

    MemberRepository memberRepository = mock(MemberRepository.class);
    when(memberRepository.findOne(any())).thenReturn(member);
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    when(httpServletRequest.getCookies()).thenReturn(cookies);

    DefaultSecurityService securityService =
      new DefaultSecurityService(session, memberRepository, httpServletRequest);
    securityService.requiresCmsMember();
  }

  @Test(expected = RoleException.class)
  public void testRequireCmsMemberException() {
    Session session = mock(Session.class);
    when(session.getMemberId()).thenReturn(Optional.of(1234L));

    Role role = new Role();
    role.setCode("noAdmin ;)");
    Member member = new Member();
    member.setRoles(Collections.singletonList(role));

    Cookie cookie1 = new Cookie("PHPSESSID", "123");
    Cookie[] cookies = new Cookie[] {cookie1};

    MemberRepository memberRepository = mock(MemberRepository.class);
    when(memberRepository.findOne(any())).thenReturn(member);
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    when(httpServletRequest.getCookies()).thenReturn(cookies);

    DefaultSecurityService securityService =
      new DefaultSecurityService(session, memberRepository, httpServletRequest);
    securityService.requiresContentAdmin();
  }

  @Test
  public void testHasRole() {
    Role testRole = new Role();
    testRole.setCode("testRole");

    Role someRole = new Role();
    someRole.setCode("someRole");

    Role anotherRole = new Role();
    anotherRole.setCode("anotherRole");

    Member member = new Member();

    DefaultSecurityService securityService = new DefaultSecurityService(null, null, null);

    member.setRoles(Arrays.asList(someRole, testRole, anotherRole));
    boolean hasRole = securityService.hasRole("testRole", member);
    assertTrue(hasRole);

    member.setRoles(Arrays.asList(testRole, anotherRole));
    hasRole = securityService.hasRole("testRole", member);
    assertTrue(hasRole);

    member.setRoles(Collections.singletonList(someRole));
    hasRole = securityService.hasRole("testRole", member);
    assertFalse(hasRole);

    member.setRoles(Collections.emptyList());
    assertFalse(securityService.hasRole("nix", member));
  }
}
