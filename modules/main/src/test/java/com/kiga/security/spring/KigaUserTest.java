package com.kiga.security.spring;

import static com.kiga.web.security.KigaRoles.ADMIN;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.kiga.security.domain.Member;
import com.kiga.security.domain.Role;
import org.junit.Test;

public class KigaUserTest {
  @Test
  public void testHasRole() {
    KigaUser kigaUser = new KigaUser();
    Member member = new Member();
    Role role = new Role();
    role.setCode(ADMIN);
    member.setRoles(singletonList(role));
    kigaUser.setMember(member);

    assertThat(kigaUser.hasRole(ADMIN)).isTrue();
  }

  @Test
  public void testHasRoleThrowsExceptionOnUnavailableRole() {
    assertThatThrownBy(() -> new KigaUser().hasRole("-"))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("unavailable role");
  }
}
