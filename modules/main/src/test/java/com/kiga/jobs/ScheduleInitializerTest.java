package com.kiga.jobs;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import com.kiga.spec.Jobable;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

import java.util.Collections;

/**
 * Created by rainerh on 18.10.16.
 */
public class ScheduleInitializerTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  class TestJob implements Jobable {
    @Override
    public void runJob() {}
  }

  private TestJob job;

  @Before
  public void setup() {
    this.job = new TestJob();
  }

  @Test
  public void onApplicationEvent() throws Exception {
    JobsRetriever jobsRetriever = mock(JobsRetriever.class);
    Environment environment = mock(Environment.class);
    ApplicationContext applicationContext = mock(ApplicationContext.class);

    JobWithCronProperty jobWithCronProperty = new JobWithCronProperty(job.getClass(), "cron");

    when(environment.getProperty("jobs.cron")).thenReturn("* * * * * *");
    when(jobsRetriever.getFromClassPath(any()))
      .thenReturn(Collections.singletonList(jobWithCronProperty));
    when(applicationContext.getBean(TestJob.class))
      .thenReturn(job);

    ThreadPoolTaskScheduler scheduler = mock(ThreadPoolTaskScheduler.class);
    new ScheduleInitializer(scheduler, environment, applicationContext, "", jobsRetriever)
      .onApplicationEvent(null);

    verify(scheduler, times(1)).schedule(any(), any(CronTrigger.class));
  }

  @Test
  public void testWithEmptyCron() throws Exception {
    ThreadPoolTaskScheduler scheduler = mock(ThreadPoolTaskScheduler.class);
    JobsRetriever jobsRetriever = mock(JobsRetriever.class);
    Environment environment = mock(Environment.class);

    JobWithCronProperty jobWithCronProperty = new JobWithCronProperty(job.getClass(), "cron");

    when(environment.getProperty("jobs.cron")).thenReturn("");
    when(jobsRetriever.getFromClassPath(any()))
      .thenReturn(Collections.singletonList(jobWithCronProperty));

    new ScheduleInitializer(scheduler, environment, null, "", jobsRetriever)
      .onApplicationEvent(null);

    verify(scheduler, never()).schedule(any(), any(CronTrigger.class));
  }

  @Test
  public void testWithException() {
    JobsRetriever jobsRetriever = mock(JobsRetriever.class);
    Environment environment = mock(Environment.class);
    JobWithCronProperty jobWithCronProperty = new JobWithCronProperty(job.getClass(), "cron");
    when(jobsRetriever.getFromClassPath(any()))
      .thenReturn(Collections.singletonList(jobWithCronProperty));

    expectedException.expect(RuntimeException.class);
    new ScheduleInitializer(null, environment, null, "", jobsRetriever)
      .onApplicationEvent(null);



  }
}
