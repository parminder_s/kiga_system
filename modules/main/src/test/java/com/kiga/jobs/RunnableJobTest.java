package com.kiga.jobs;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.spec.Jobable;
import org.junit.Test;

/**
 * Created by rainerh on 18.10.16.
 */
public class RunnableJobTest {
  @Test
  public void testRun() {
    Jobable jobable = mock(Jobable.class);
    Runnable runnable = new RunnableJob(jobable);
    runnable.run();
    verify(jobable).runJob();
  }

}
