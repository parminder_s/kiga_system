package com.kiga.jobs;

import static org.hamcrest.core.IsInstanceOf.instanceOf;


import org.junit.Assert;
import org.junit.Test;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * Created by rainerh on 18.10.16.
 */
public class JobsConfigTest {
  @Test
  public void testScheduler() {
    Assert.assertThat(new JobsConfig().getScheduler(),
      instanceOf(ThreadPoolTaskScheduler.class));
  }

  @Test
  public void testScheduleInitializer() {
    Assert.assertThat(new JobsConfig().getScheduleInitializer(null, null, null),
      instanceOf(ScheduleInitializer.class));
  }
}
