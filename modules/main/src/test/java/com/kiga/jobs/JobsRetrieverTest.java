package com.kiga.jobs;

import static org.junit.Assert.assertTrue;

import com.kiga.spec.Job;
import com.kiga.spec.Jobable;
import java.util.List;
import org.junit.Test;

/** Created by rainerh on 27.12.16. */
public class JobsRetrieverTest {
  public class JobWithoutAnnotation implements Jobable {
    @Override
    public void runJob() {}
  }

  @Job(cronProperty = "foo")
  public class JobWithAnnotation implements Jobable {
    @Override
    public void runJob() {}
  }

  @Test
  public void testRetriever() {
    List<JobWithCronProperty> jobWithCronPropertyList =
        new JobsRetriever().getFromClassPath("com.kiga.jobs");

    assertTrue(jobWithCronPropertyList.size() >= 2);
    assertTrue(
        jobWithCronPropertyList
            .stream()
            .anyMatch(
                jobWithCronProperty ->
                    jobWithCronProperty.getJobable() == JobWithAnnotation.class
                        && jobWithCronProperty.getCronProperty().equals("foo")));
    assertTrue(
        jobWithCronPropertyList
            .stream()
            .noneMatch(
                jobWithCronProperty ->
                    jobWithCronProperty.getJobable() == JobWithoutAnnotation.class));
  }
}
