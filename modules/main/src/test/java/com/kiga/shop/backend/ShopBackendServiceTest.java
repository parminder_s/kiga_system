package com.kiga.shop.backend;

import static org.junit.Assert.assertNotNull;
import static org.powermock.api.mockito.PowerMockito.when;


import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.shop.ShopBackendService;
import com.kiga.shop.domain.Purchase;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by faxxe on 1/4/17.
 */
public class ShopBackendServiceTest {

  ShopBackendService shopBackendService;

  @Test
  @Ignore
  public void getShippingInformationTest() {

    HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);

    shopBackendService = new ShopBackendService(null, null);

    List<Purchase> purchases = new ArrayList<>();
    purchases.add(getPaidPurchase());

    File information = shopBackendService.printSelectedOrders(purchases);
    assertNotNull(information);
  }



  private Purchase getPaidPurchase() {
    Purchase purchase = Mockito.mock(Purchase.class);
    when(purchase.getPaymentMethod()).thenReturn(PaymentMethod.PAYPAL);

    return purchase;
  }

  private Purchase getInvoicePurchase() {
    Purchase purchase = Mockito.mock(Purchase.class);
    when(purchase.getPaymentMethod()).thenReturn(PaymentMethod.INVOICE);

    return purchase;
  }


}
