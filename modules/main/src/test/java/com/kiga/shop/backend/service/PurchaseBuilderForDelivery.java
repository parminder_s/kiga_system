package com.kiga.shop.backend.service;

import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseDelivery;
import com.kiga.shop.domain.PurchaseDeliveryPurchaseProduct;
import com.kiga.shop.domain.PurchaseItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PurchaseBuilderForDelivery {
  private List<PurchaseItem> purchaseItems = new ArrayList<>();

  /**
   * add new purchaseProduct and amount.
   */
  public PurchaseBuilderForDelivery withPurchaseIdAmount(long purchaseProductId, int amount) {
    PurchaseItem purchaseItem =
        PurchaseItem.builder()
            .product(Product.builder().productTitle("p" + purchaseProductId).build())
            .amount(amount)
            .build();
    purchaseItem.setId(purchaseProductId);
    purchaseItems.add(purchaseItem);

    return this;
  }

  /**
   * returns the purchase as configured.
   */
  public Purchase build() {
    List<PurchaseDeliveryPurchaseProduct> deliveryProducts =
      purchaseItems
        .stream()
        .map(pi ->
          PurchaseDeliveryPurchaseProduct.builder()
            .purchaseItem(pi)
            .amount(pi.getAmount())
            .build())
        .collect(Collectors.toList());
    PurchaseDelivery delivery =
      PurchaseDelivery.builder().purchaseProducts(deliveryProducts).build();
    return Purchase.builder()
      .purchaseItems(purchaseItems)
      .purchaseDeliveries(Collections.singletonList(delivery))
      .build();
  }
}
