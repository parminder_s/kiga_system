package com.kiga.shop;


import static org.junit.Assert.assertEquals;

import com.kiga.shop.numbers.CreditVoucherOverflowException;
import com.kiga.shop.numbers.InvoiceNrOverflowException;
import com.kiga.shop.numbers.NumberGenerator;
import com.kiga.shop.numbers.ShippingNoteOverflowException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import scala.Option;

/**
 * Created by mfit on 05.10.17.
 */
public class NumberGeneratorTest {
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void testGenerateFirstInvoice() {
    assertEquals(
      "DE6000001",
      new NumberGenerator().generateInvoiceNr("", "DE", 2016)
    );
  }

  @Test
  public void testInvoiceDefault() {
    assertEquals(
      "DE6000003",
      new NumberGenerator().generateInvoiceNr("DE6000002", "DE", 2016)
    );
  }

  @Test
  public void testInvoiceAt999() {
    assertEquals(
      "DE7001000",
      new NumberGenerator().generateInvoiceNr("DE7000999", "DE", 2017));
  }

  @Test
  public void testInvoiceAt1000() {
    assertEquals(
      "DE7001001",
      new NumberGenerator().generateInvoiceNr("DE7001000", "DE", 2017));
  }

  @Test
  public void testMaxInvoice() {
    assertEquals(
      "DE6999999",
      new NumberGenerator().generateInvoiceNr("DE6999998", "DE", 2016));
  }

  @Test
  public void testOverflownInvoice() {
    thrown.expect(InvoiceNrOverflowException.class);
    assertEquals(
      "DE9999999",
      new NumberGenerator().generateInvoiceNr("DE6999999", "DE", 2016));
  }

  @Test
  public void testGenerateInvoiceNumberWithFormerYear() {
    // the invoice number will always be from the given year or an empty string
    assertEquals(
      "AT5000346",
      new NumberGenerator().generateInvoiceNr("AT5000345", "AT", 2015));
  }

  @Test
  public void testFirstVoucherNumber() {
    assertEquals("G0000001",
      new NumberGenerator().generateCreditVoucherNr(""));
  }

  @Test
  public void testFirstVoucherAt1000() {
    assertEquals("G0001000",
      new NumberGenerator().generateCreditVoucherNr("G0000999"));
  }

  @Test
  public void testLargestVoucherNumber() {
    assertEquals("G9999999",
      new NumberGenerator().generateCreditVoucherNr("G9999998"));
  }

  @Test
  public void testVoucherNumberOverflow() {
    thrown.expect(CreditVoucherOverflowException.class);
    assertEquals("G9999999",
      new NumberGenerator().generateCreditVoucherNr("G9999999"));
  }

  @Test
  public void testShippingAt99() {
    assertEquals(
      "001000",
      new NumberGenerator().generateShippingNr(Option.apply("000999"))
    );
  }

  @Test
  public void testShippingAt1000() {
    assertEquals(
      "001001",
      new NumberGenerator().generateShippingNr(Option.apply("001000"))
    );
  }

  @Test
  public void testShippingAtLargeNumber() {
    assertEquals(
      "010001",
      new NumberGenerator().generateShippingNr(Option.apply("010000"))
    );
  }

  @Test
  public void testNonIntegerShipping() {
    thrown.expect(NumberFormatException.class);
    new NumberGenerator().generateShippingNr(Option.apply("foobar"));
  }

  @Test
  public void testShippingOnMaxNumber() {
    thrown.expect(ShippingNoteOverflowException.class);
    new NumberGenerator().generateShippingNr(Option.apply("999999"));
  }

  @Test
  public void testShippingUnreachableNumber() {
    thrown.expect(ShippingNoteOverflowException.class);
    new NumberGenerator().generateShippingNr(Option.apply("1000000"));
  }

  @Test
  public void testShippingWithNull() {
    assertEquals(
      "000001",
      new NumberGenerator().generateShippingNr(Option.apply(null))
    );
  }

  @Test
  public void testShippingWithEmptyString() {
    assertEquals(
      "000001",
      new NumberGenerator().generateShippingNr(Option.apply(""))
    );
  }

  @Test
  public void testShippingWithEmpty() {
    assertEquals(
      "000001",
      new NumberGenerator().generateShippingNr(Option.empty())
    );
  }
}
