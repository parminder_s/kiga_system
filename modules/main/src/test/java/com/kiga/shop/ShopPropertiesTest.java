package com.kiga.shop;

import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.junit.Assert.assertThat;

import com.kiga.ideas.property.PreviewImage;
import org.junit.Test;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by rainerh on 02.04.16.
 */
public class ShopPropertiesTest {
  @Test
  public void testBean() {
    assertThat(ShopProperties.class, hasValidGettersAndSetters());
  }
}
