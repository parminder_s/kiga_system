package com.kiga.shop.backend.service.delivery;

import static com.kiga.shop.domain.ShippingStatus.ShippingInformationCreated;
import static org.assertj.core.api.Assertions.tuple;
import static org.assertj.core.api.Java6Assertions.assertThat;

import com.kiga.shop.backend.service.PurchaseBuilderForDelivery;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseDelivery;
import java.util.Arrays;
import java.util.Collections;
import org.junit.Test;

public class DeliverySplitterExecutorTest {
  @Test
  public void testDefaultCase() {
    Purchase purchase =
        new PurchaseBuilderForDelivery()
            .withPurchaseIdAmount(1, 5)
            .withPurchaseIdAmount(2, 4)
            .withPurchaseIdAmount(3, 8)
            .withPurchaseIdAmount(4, 1)
            .build();

    PurchaseDelivery oldDelivery = purchase.getPurchaseDeliveries().get(0);
    PurchaseDelivery newDelivery = PurchaseDeliveryMapper.INSTANCE.purchaseDelivery(oldDelivery);

    new DeliverySplitterExecutor()
        .split(
            oldDelivery,
            newDelivery,
            Arrays.asList(new PurchaseProductIdAmount(1, 2), new PurchaseProductIdAmount(3, 5)));

    assertThat(oldDelivery.getPurchaseProducts())
        .extracting(pp -> tuple(pp.getPurchaseItem().getId(), pp.getAmount()))
        .contains(tuple(1L, 3), tuple(2L, 4), tuple(3L, 3), tuple(4L, 1))
        .hasSize(4);

    assertThat(newDelivery.getPurchaseProducts())
        .extracting(pp -> tuple(pp.getPurchaseItem().getId(), pp.getAmount()))
        .contains(tuple(1L, 2), tuple(3L, 5))
        .hasSize(2);
  }

  @Test
  public void testForStatusSet() {
    Purchase purchase = new PurchaseBuilderForDelivery().withPurchaseIdAmount(1, 5).build();
    PurchaseDelivery newDelivery = new PurchaseDelivery();

    new DeliverySplitterExecutor()
        .split(
            purchase.getPurchaseDeliveries().get(0),
            newDelivery,
            Collections.singletonList(new PurchaseProductIdAmount(1, 2)));

    assertThat(newDelivery.getStatus()).isEqualByComparingTo(ShippingInformationCreated);
  }

  @Test
  public void testWithRemovingPurchaseProduct() {
    Purchase purchase =
        new PurchaseBuilderForDelivery()
            .withPurchaseIdAmount(1, 5)
            .withPurchaseIdAmount(2, 2)
            .build();

    PurchaseDelivery oldDelivery = purchase.getPurchaseDeliveries().get(0);
    PurchaseDelivery newDelivery = PurchaseDeliveryMapper.INSTANCE.purchaseDelivery(oldDelivery);

    new DeliverySplitterExecutor()
        .split(
            oldDelivery, newDelivery, Collections.singletonList(new PurchaseProductIdAmount(2, 2)));

    assertThat(oldDelivery.getPurchaseProducts())
        .extracting(pp -> tuple(pp.getPurchaseItem().getId(), pp.getAmount()))
        .contains(tuple(1L, 5))
        .contains(tuple(2L, 0))
        .hasSize(2);

    assertThat(newDelivery.getPurchaseProducts())
        .extracting(pp -> tuple(pp.getPurchaseItem().getId(), pp.getAmount()))
        .contains(tuple(2L, 2))
        .hasSize(1);
  }
}
