package com.kiga.shop.backend;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.security.services.SecurityService;
import com.kiga.shop.backend.exception.IllegalPaginationArgumentException;
import com.kiga.shop.backend.exception.NonexistentEntityUpdateException;
import com.kiga.shop.backend.web.request.ProductDetailRequest;
import com.kiga.shop.backend.web.request.ProductDetailsListRequest;
import com.kiga.shop.backend.web.request.ToggleActivateProductDetailRequest;
import com.kiga.shop.backend.web.response.ProductDetailViewModel;
import com.kiga.shop.service.ProductDetailService;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 10/8/16.
 */
public class ProductDetailsControllerTest {
  private ProductDetailsController productDetailsController;
  private ProductDetailService productDetailService = mock(
    ProductDetailService.class);
  private SecurityService securityService = mock(SecurityService.class);

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() throws IllegalPaginationArgumentException {
    ProductDetailViewModel productDetailViewModel = new ProductDetailViewModel();
    when(productDetailService.getList(anyLong(), anyInt(), anyInt()))
      .thenReturn(asList(productDetailViewModel));
    when(productDetailService.findOne(anyLong())).thenReturn(productDetailViewModel);
    productDetailsController = new ProductDetailsController(productDetailService, securityService);
  }

  @Test
  public void testGetList() throws IllegalPaginationArgumentException {
    ProductDetailsListRequest productDetailsListRequest = new ProductDetailsListRequest();
    productDetailsListRequest.setProductId(1L);
    productDetailsListRequest.setPage(0);
    productDetailsListRequest.setPageSize(50);
    List<ProductDetailViewModel> returned = productDetailsController
      .getList(productDetailsListRequest);
    assertNotNull(returned);
    assertEquals(1, returned.size());
  }

  @Test
  public void testGetListByNullRequest() throws IllegalPaginationArgumentException {
    List<ProductDetailViewModel> returned = productDetailsController.getList(null);
    assertNotNull(returned);
    assertEquals(1, returned.size());
    verify(productDetailService).getList(0, 0, 50);
  }

  @Test
  public void testGetListWrongPage() throws IllegalPaginationArgumentException {
    ProductDetailsListRequest productDetailsListRequest = new ProductDetailsListRequest();
    productDetailsListRequest.setProductId(1L);
    productDetailsListRequest.setPage(-1);
    productDetailsListRequest.setPageSize(50);
    productDetailsController.getList(productDetailsListRequest);
    verify(productDetailService).getList(productDetailsListRequest.getProductId(), 0, 50);
  }

  @Test
  public void testGetListWrongPageSize() throws IllegalPaginationArgumentException {
    ProductDetailsListRequest productDetailsListRequest = new ProductDetailsListRequest();
    productDetailsListRequest.setProductId(1L);
    productDetailsListRequest.setPage(0);
    productDetailsListRequest.setPageSize(0);
    productDetailsController.getList(productDetailsListRequest);
    verify(productDetailService).getList(productDetailsListRequest.getProductId(), 0, 50);
  }

  @Test
  public void testGetOne() {
    ProductDetailRequest productDetailRequest = new ProductDetailRequest();
    productDetailRequest.setProductDetailId(1L);
    ProductDetailViewModel returner =
      productDetailsController.getOne(productDetailRequest);
    assertNotNull(returner);
    verify(productDetailService).findOne(1L);
  }

  @Test
  public void testAdd() throws NonexistentEntityUpdateException {
    ProductDetailViewModel productDetailViewModel = new ProductDetailViewModel();
    productDetailsController.save(productDetailViewModel);
    verify(productDetailService).save(productDetailViewModel);
  }

  @Test
  public void testDeactivate() throws NonexistentEntityUpdateException {
    ToggleActivateProductDetailRequest toggleActivateProductDetailRequest = new
      ToggleActivateProductDetailRequest();
    toggleActivateProductDetailRequest.setId(1L);
    productDetailsController.deactivate(toggleActivateProductDetailRequest);
    verify(productDetailService)
      .toggleActivation(toggleActivateProductDetailRequest.getId(), false);
  }
}
