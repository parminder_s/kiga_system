package com.kiga.shop.backend.service.delivery;

import static com.kiga.shop.domain.ShippingPartner.DHL;
import static com.kiga.shop.domain.ShippingStatus.Shipped;
import static org.assertj.core.api.Assertions.assertThat;

import com.kiga.shop.backend.service.PurchaseBuilderForDelivery;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseDelivery;
import com.kiga.shop.domain.PurchaseDeliveryPurchaseProduct;
import org.junit.Test;

import java.util.Arrays;

public class PurchaseDeliveryMapperTest {
  @Test
  public void testPurchaseItemsNotMapped() {
    Purchase purchase = new Purchase();
    PurchaseDelivery delivery =
        PurchaseDelivery.builder()
            .purchase(purchase)
            .status(Shipped)
            .shippingNoteId(2)
            .shippingNoteNr("002")
            .shippingPartner(DHL)
            .purchaseProducts(
                Arrays.asList(
                    PurchaseDeliveryPurchaseProduct.builder().build(),
                    PurchaseDeliveryPurchaseProduct.builder().build()))
            .build();
    new PurchaseBuilderForDelivery()
        .withPurchaseIdAmount(1, 2)
        .withPurchaseIdAmount(2, 2)
        .build()
        .getPurchaseDeliveries()
        .get(0);
    PurchaseDelivery mapped = PurchaseDeliveryMapper.INSTANCE.purchaseDelivery(delivery);

    assertThat(mapped)
        .extracting("purchase", "shippingNoteId", "shippingNoteNr", "shippingPartner", "status")
        .contains(purchase, 2, "002", DHL, null);
    assertThat(mapped.getPurchaseProducts()).hasSize(0);
  }
}
