package com.kiga.shop.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.shopcontent.domain.ShopCategory;
import com.kiga.shopcontent.domain.draft.ShopCategoryDraft;
import com.kiga.shopcontent.domain.live.ShopCategoryLive;
import com.kiga.shopcontent.repository.ShopCategoryRepositoryProxy;
import com.kiga.shopcontent.service.ShopCategoryService;
import com.kiga.shopcontent.viewmodel.ShopCategoryViewModel;
import com.kiga.shopcontent.viewmodel.converter.ShopCategoryToResponseConverter;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bbs
 * @since 9/6/16.
 */
public class ShopCategoryServiceTest {
  private ShopCategoryService shopCategoryService;
  private ShopCategoryRepositoryProxy shopCategoryRepositoryProxy;
  private ShopCategoryToResponseConverter shopCategoryToResponseConverter;
  private List<ShopCategoryViewModel> shopCategoryResponseList = new ArrayList<>();
  private List<ShopCategory> shopCategoryLiveList = new ArrayList<>();
  private ShopCategoryLive shopCategory = new ShopCategoryLive();
  private ShopCategoryLive shopCategoryLive1 = new ShopCategoryLive();
  private ShopCategoryDraft shopCategoryLive2 = new ShopCategoryDraft();
  private ShopCategoryLive shopCategoryLive3 = new ShopCategoryLive();
  private ShopCategoryViewModel queriedCategory = new ShopCategoryViewModel();
  private ShopCategoryViewModel shopCategoryResponse1 = new ShopCategoryViewModel();
  private ShopCategoryViewModel shopCategoryResponse2 = new ShopCategoryViewModel();
  private ShopCategoryViewModel shopCategoryResponse3 = new ShopCategoryViewModel();

  /**
   * Just setup tests.
   */
  @Before
  public void setupTests() {
    queriedCategory.setId(4L);

    shopCategoryLiveList.add(shopCategoryLive1);
    shopCategoryLiveList.add(shopCategoryLive2);
    shopCategoryLiveList.add(shopCategoryLive3);

    shopCategoryResponseList.add(shopCategoryResponse1);
    shopCategoryResponseList.add(shopCategoryResponse2);
    shopCategoryResponseList.add(shopCategoryResponse3);

    shopCategoryRepositoryProxy = mock(ShopCategoryRepositoryProxy.class);
    when(shopCategoryRepositoryProxy.getSubcategories(queriedCategory.getId()))
      .thenReturn(shopCategoryLiveList);
    when(shopCategoryRepositoryProxy.findOne(queriedCategory.getId())).thenReturn(shopCategory);

    shopCategoryToResponseConverter = mock(ShopCategoryToResponseConverter.class);
    when(shopCategoryToResponseConverter.convertToResponse(shopCategoryLive1))
      .thenReturn(shopCategoryResponse1);
    when(shopCategoryToResponseConverter.convertToResponse(shopCategoryLive2))
      .thenReturn(shopCategoryResponse2);
    when(shopCategoryToResponseConverter.convertToResponse(shopCategoryLive3))
      .thenReturn(shopCategoryResponse3);

    shopCategoryService = new ShopCategoryService(shopCategoryRepositoryProxy,
      shopCategoryToResponseConverter);
  }

  @Test
  public void testGetCategory() {
    shopCategoryService.getCategory(queriedCategory.getId());
    verify(shopCategoryRepositoryProxy).findOne(queriedCategory.getId());
    verify(shopCategoryToResponseConverter).convertToResponse(shopCategory);
  }

  @Test
  public void testGetSubcategories() {
    shopCategoryService.getSubcategories(queriedCategory);
    verify(shopCategoryRepositoryProxy).getSubcategories(queriedCategory.getId());
    verify(shopCategoryToResponseConverter).convertToResponse(shopCategoryLive1);
    verify(shopCategoryToResponseConverter).convertToResponse(shopCategoryLive2);
    verify(shopCategoryToResponseConverter).convertToResponse(shopCategoryLive3);
  }
}
