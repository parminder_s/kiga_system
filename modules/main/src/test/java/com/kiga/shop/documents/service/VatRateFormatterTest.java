package com.kiga.shop.documents.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import org.junit.Test;

public class VatRateFormatterTest {
  @Test
  public void testFormatting_7_7() {
    assertThat(VatRateFormatter.format(new BigDecimal("0.077"))).isEqualTo("7,7");
  }

  @Test
  public void testFormatting_20() {
    assertThat(VatRateFormatter.format(new BigDecimal("0.200"))).isEqualTo("20");
  }
}
