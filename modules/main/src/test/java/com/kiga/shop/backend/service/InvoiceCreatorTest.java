package com.kiga.shop.backend.service;

import static com.kiga.DateTypesProvider.getZoned;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.numbers.NumberGenerator;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.shop.service.DocumentService;
import com.kiga.util.NowService;
import java.time.Instant;
import java.util.Date;
import org.joda.time.DateTime;
import org.junit.Test;

/** Created by rainerh on 22.02.17. */
public class InvoiceCreatorTest {
  @Test
  public void createInvoiceTest() {
    Purchase lastOrder = new Purchase();
    lastOrder.setInvoiceNr("Nr1");
    PurchaseRepository purchaseRepository = mock(PurchaseRepository.class);

    when(purchaseRepository.findTopByInvoiceNrStartingWithOrderByInvoiceNrDesc("GB8"))
        .thenReturn(lastOrder);
    NumberGenerator numberGenerator = mock(NumberGenerator.class);
    when(numberGenerator.generateInvoiceNr("Nr1", "GB", 2018)).thenReturn("Nr2");

    Purchase purchase = new Purchase();
    Country gb = new Country();
    gb.setCode("gb");
    purchase.setCreated(new DateTime(2018, 2, 1, 0, 0).toDate());
    purchase.setCountry(gb);
    purchase.setPaymentMethod(PaymentMethod.CREDITCARD);

    DocumentService documentService = mock(DocumentService.class);

    NowService nowService = mock(NowService.class);
    when(nowService.getZoned()).thenReturn(getZoned(2018, 2, 1));
    InvoiceCreator invoiceCreator =
        new InvoiceCreator(
            documentService, null, purchaseRepository, numberGenerator, null, nowService);

    invoiceCreator.createInvoice(purchase);
    verify(purchaseRepository).save(purchase);
    verify(documentService).addInvoiceToPurchase(purchase, "invoice/2018/GB/");

    assertEquals("Nr2", purchase.getInvoiceNr());
    assertTrue(purchase.getInvoiceDate().isBefore(Instant.now().plusSeconds(1)));
  }

  @Test
  public void createInvoiceWithInvoiceAccounting() {
    Purchase lastOrder = new Purchase();
    lastOrder.setInvoiceNr("Nr1");

    Purchase purchase = new Purchase();
    Country gb = new Country();
    gb.setCode("gb");
    purchase.setCreated(Date.from(Instant.parse("2018-01-01T00:00:00Z")));
    purchase.setCountry(gb);
    purchase.setPaymentMethod(PaymentMethod.INVOICE);

    NowService nowService = mock(NowService.class);
    when(nowService.getZoned()).thenReturn(getZoned(2018, 2, 1));

    PurchaseInvoiceMapper purchaseInvoiceMapper = mock(PurchaseInvoiceMapper.class);
    Invoice invoice = new Invoice();
    when(purchaseInvoiceMapper.map(purchase)).thenReturn(invoice);
    InvoiceRepository invoiceRepository = mock(InvoiceRepository.class);
    InvoiceCreator invoiceCreator =
        new InvoiceCreator(
            mock(DocumentService.class),
            invoiceRepository,
            mock(PurchaseRepository.class),
            mock(NumberGenerator.class),
            purchaseInvoiceMapper,
            nowService);

    invoiceCreator.createInvoice(purchase);

    verify(purchaseInvoiceMapper).map(purchase);
    verify(invoiceRepository).save(invoice);
  }
}
