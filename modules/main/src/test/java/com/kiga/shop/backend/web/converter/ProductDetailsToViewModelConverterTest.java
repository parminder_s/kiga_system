package com.kiga.shop.backend.web.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.kiga.shop.backend.web.response.ProductDetailViewModel;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryGroup;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.domain.ShippingDiscount;
import com.kiga.shop.service.VatRateCalculator;
import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;

/**
 * @author bbs
 * @since 10/8/16.
 */
public class ProductDetailsToViewModelConverterTest {
  private static final Integer THRESHOLD = 9;
  private static final BigDecimal DISCOUNT_VALUE = BigDecimal.valueOf(100000);
  private static final String TITLE = "TITLE";
  private static final String CODE = "ABC";
  private static final String CURRENCY = "EUR";
  private static final String CODE_3 = "DEF";
  private static final BigDecimal VAT = new BigDecimal("10.000");
  private static final long ID = 1L;
  private static final long PRODUCT_ID = 11L;
  private static final BigDecimal PRICE_NET = new BigDecimal("10.000");
  private static final BigDecimal SHIPPING_COSTS = BigDecimal.valueOf(100);
  private static final boolean ACTIVATED = true;
  private static final BigDecimal VAT_PRODUCT = new BigDecimal("10.000");
  private static final BigDecimal VAT_SHIPPING = BigDecimal.valueOf(10000);
  private static final int FREE_SHIPPING_AMOUNT = 3;
  private static final String SHIPPING_DISCOUNT_TYPE = "SHPTYPE";
  private static final Long MAX_DELIVERY_DAYS = 10L;
  private static final Long MIN_DELIVERY_DAYS = 5L;
  private static final long COUNTRY_GROUP_ID = 97L;
  private static final long COUNTRY_ID = 98L;
  private ProductDetailsToViewModelConverter productDetailsToViewModelConverter;

  /** Prepare tests. */
  @Before
  public void prepareTests() {
    productDetailsToViewModelConverter =
        new ProductDetailsToViewModelConverter(new VatRateCalculator());
  }

  @Test
  public void testConvertItemToResponse() {
    ProductDetailViewModel productDetailViewModel =
        productDetailsToViewModelConverter.convertToResponse(createProductDetail());

    executeAssertEquals(productDetailViewModel);
    assertNotNull(productDetailViewModel);
  }

  @Test
  public void checkPriceZero() {
    ProductDetail productDetail = new ProductDetail();
    productDetail.setVatProduct(BigDecimal.ZERO);
    productDetail.setPriceNet(new BigDecimal("12.5"));
    assertEquals("12.5", new BigDecimal("12.5").toString());
  }

  private void executeAssertEquals(ProductDetailViewModel productDetailViewModel) {
    assertEquals(ID, productDetailViewModel.getId());
    assertEquals(PRODUCT_ID, productDetailViewModel.getProductId());
    assertEquals(ACTIVATED, productDetailViewModel.isActivated());
    assertEquals(VAT_PRODUCT, productDetailViewModel.getVatProduct());
    assertEquals(SHIPPING_DISCOUNT_TYPE, productDetailViewModel.getShippingDiscountType());
    assertEquals(CURRENCY, productDetailViewModel.getCurrencyCode());
    assertEquals(COUNTRY_ID, productDetailViewModel.getCountryId());
  }

  private ProductDetail createProductDetail() {
    ShippingDiscount shippingDiscount = new ShippingDiscount();
    shippingDiscount.setThreshold(THRESHOLD);
    shippingDiscount.setDiscountValue(DISCOUNT_VALUE);
    CountryGroup countryGroup = new CountryGroup();
    countryGroup.setTitle(TITLE);
    countryGroup.setId(COUNTRY_GROUP_ID);
    Country country =
        Country.builder().code(CODE).code3(CODE_3).currency(CURRENCY).vat(VAT).build();
    country.setId(COUNTRY_ID);

    CountryShop countryShop = CountryShop.builder().country(country).build();
    country.setCountryShop(countryShop);

    Product product = new Product();
    product.setId(PRODUCT_ID);

    ProductDetail productDetail =
        ProductDetail.builder()
            .priceNet(PRICE_NET)
            .shippingCosts(SHIPPING_COSTS)
            .activated(ACTIVATED)
            .vatProduct(VAT_PRODUCT)
            .vatShipping(VAT_SHIPPING)
            .freeShippingAmount(FREE_SHIPPING_AMOUNT)
            .shippingDiscountType(SHIPPING_DISCOUNT_TYPE)
            .product(product)
            .countryGroup(countryGroup)
            .country(country)
            .shippingDiscounts(Arrays.asList(shippingDiscount, shippingDiscount))
            .build();
    productDetail.setId(ID);

    return productDetail;
  }
}
