package com.kiga.shop;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.mail.Mailer;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.security.domain.Member;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CreditVoucherItem;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.numbers.NumberGenerator;
import com.kiga.shop.repository.CreditVoucherItemRepository;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.shop.service.CreditVoucherService;
import com.kiga.shop.service.ProductHelper;
import com.kiga.shop.service.ShopS3Uploader;
import com.kiga.shop.service.ShopUrlGenerator;
import com.kiga.shop.web.response.PurchaseDetailResponse;
import com.kiga.shop.web.responses.PurchaseItemResponse;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.UnaryOperator;
import javax.mail.MessagingException;
import org.junit.Assert;
import org.junit.Test;

/** Created by peter on 31.05.16. */
public class CreditVoucherServiceTest {
  @Test
  public void generateCreditVoucherNumberEmpty() {

    NumberGenerator numberGenerator = mock(NumberGenerator.class);
    when(numberGenerator.generateCreditVoucherNr(eq(""))).thenReturn("empty");

    PurchaseRepository purchaseRepository = mock(PurchaseRepository.class);
    Purchase purchase = new Purchase();
    purchase.setCreditVoucherNr(null);
    when(purchaseRepository.findTopByOrderByCreditVoucherNrDesc()).thenReturn(purchase);

    CreditVoucherService creditVoucherService =
        CreditVoucherService.builder()
            .purchaseRepository(purchaseRepository)
            .generateShopNumbers(numberGenerator)
            .build();

    String empty = creditVoucherService.getNewCreditVoucherNumber();
    Assert.assertEquals("Test generateCreditVoucher with empty", "empty", empty);
  }

  @Test
  public void generateCreditVoucherNumberNoEmpty() {

    NumberGenerator numberGenerator = mock(NumberGenerator.class);
    when(numberGenerator.generateCreditVoucherNr(eq("123456"))).thenReturn("noempty");

    PurchaseRepository purchaseRepository = mock(PurchaseRepository.class);
    Purchase purchase = new Purchase();
    purchase.setCreditVoucherNr("123456");
    when(purchaseRepository.findTopByOrderByCreditVoucherNrDesc()).thenReturn(purchase);

    CreditVoucherService creditVoucherService =
        CreditVoucherService.builder()
            .purchaseRepository(purchaseRepository)
            .generateShopNumbers(numberGenerator)
            .build();

    String empty = creditVoucherService.getNewCreditVoucherNumber();
    Assert.assertEquals("Test generateCreditVoucher with no empty", "noempty", empty);
  }

  @Test
  public void emailCreditVoucherMale() throws MessagingException {
    ShopUrlGenerator shopUrlGenerator = mock(ShopUrlGenerator.class);
    when(shopUrlGenerator.generateCreditVoucherUrl(any())).thenReturn("12345");

    Purchase purchase = new Purchase();
    purchase.setId(2L);
    purchase.setGender("male");
    purchase.setFirstname("firstname");
    purchase.setEmail("email");

    Mailer mailer = mock(Mailer.class);
    CreditVoucherService creditVoucherService =
        CreditVoucherService.builder()
            .mailer(mailer)
            .shopEmail("shopEmail")
            .buckerName("bucketName")
            .shopUrlGenerator(shopUrlGenerator)
            .build();
    creditVoucherService.emailCreditVoucher(purchase);

    verify(mailer).send(eq("shop"), eq("creditVoucher"), eq("2"), any(UnaryOperator.class));
  }

  @Test
  public void emailCreditVoucherFemale() throws MessagingException {
    ShopUrlGenerator shopUrlGenerator = mock(ShopUrlGenerator.class);
    when(shopUrlGenerator.generateCreditVoucherUrl(any())).thenReturn("12345");

    Purchase purchase = new Purchase();
    purchase.setId(3L);
    purchase.setGender("female");
    purchase.setFirstname("firstname");
    purchase.setEmail("email");

    Mailer mailer = mock(Mailer.class);
    CreditVoucherService creditVoucherService =
        CreditVoucherService.builder()
            .mailer(mailer)
            .shopUrlGenerator(shopUrlGenerator)
            .shopEmail("shopEmail")
            .buckerName("bucketName")
            .build();
    creditVoucherService.emailCreditVoucher(purchase);

    verify(mailer).send(eq("shop"), eq("creditVoucher"), eq("3"), any(UnaryOperator.class));
  }

  @Test
  public void createCreditVoucherTest1() {
    PurchaseDetailResponse purchaseDto = mock(PurchaseDetailResponse.class);
    when(purchaseDto.getId()).thenReturn(123L);
    List<PurchaseItemResponse> items = new ArrayList<>();
    PurchaseItemResponse item = new PurchaseItemResponse();
    item.setCreditVoucherTotal(new BigDecimal(10));
    items.add(item);
    when(purchaseDto.getPurchaseItems()).thenReturn(items);

    Purchase purchase = mock(Purchase.class);
    Member member = new Member();
    member.setCustomerId("customerid");
    when(purchase.getCustomer()).thenReturn(member);
    when(purchase.getCurrency()).thenReturn("EUR");
    when(purchase.getPaymentMethod()).thenReturn(PaymentMethod.CREDITCARD);
    when(purchase.getInvoiceNr()).thenReturn("invoicenumber");
    Date date = new Date();
    when(purchase.getCreated()).thenReturn(date);
    when(purchase.getCreditVoucherDate()).thenReturn(date.toInstant());
    Country country = new Country();
    country.setCode("at");
    when(purchase.getCountry()).thenReturn(country);

    PurchaseRepository purchaseRepository = mock(PurchaseRepository.class);
    when(purchaseRepository.findTopByOrderByCreditVoucherNrDesc()).thenReturn(purchase);
    when(purchaseRepository.findById(eq(123L))).thenReturn(purchase);

    CreditVoucherItem creditVoucherItem = new CreditVoucherItem();
    creditVoucherItem.setPriceNetTotal(new BigDecimal(5));
    creditVoucherItem.setPriceGrossTotal(new BigDecimal(10));
    creditVoucherItem.setVatRate(new BigDecimal(5));
    creditVoucherItem.setVatTotal(new BigDecimal(5));
    creditVoucherItem.setAmount(5L);
    creditVoucherItem.setTitle("title");
    creditVoucherItem.setCode("code");

    List<CreditVoucherItem> creditVoucherItemList = new ArrayList<CreditVoucherItem>();
    creditVoucherItemList.add(creditVoucherItem);
    ProductHelper productHelper = mock(ProductHelper.class);
    doReturn(creditVoucherItemList)
        .when(productHelper)
        .getProductDetailsCreditVoucher(any(), any());

    CreditVoucherItemRepository creditVoucherItemRepository =
        mock(CreditVoucherItemRepository.class);

    ShopS3Uploader shopS3Uploader = mock(ShopS3Uploader.class);

    CreditVoucherService creditVoucherService =
        CreditVoucherService.builder()
            .purchaseRepository(purchaseRepository)
            .generateShopNumbers(new NumberGenerator())
            .productHelper(productHelper)
            .creditVoucherItemRepository(creditVoucherItemRepository)
            .shopS3Uploader(shopS3Uploader)
            .build();

    creditVoucherService.createCreditVoucher(purchaseDto);

    verify(shopS3Uploader).uploadToAmazon(any(), any());
    verify(purchaseRepository).findById(eq(123L));

    File file = new File(System.getProperty("java.io.tmpdir") + File.separator + "G0000001.pdf");
    Assert.assertTrue(file.exists());
    file.delete();
  }

  @Test
  public void createCreditVoucherTest2() {
    PurchaseDetailResponse purchaseDto = mock(PurchaseDetailResponse.class);
    when(purchaseDto.getId()).thenReturn(123L);
    List<PurchaseItemResponse> items = new ArrayList<>();
    PurchaseItemResponse item = new PurchaseItemResponse();
    item.setCreditVoucherTotal(new BigDecimal(10));
    items.add(item);
    when(purchaseDto.getPurchaseItems()).thenReturn(items);

    Purchase purchase = mock(Purchase.class);
    Member member = new Member();
    member.setCustomerId("customerid");
    when(purchase.getCustomer()).thenReturn(member);
    when(purchase.getCurrency()).thenReturn("EUR");
    when(purchase.getPaymentMethod()).thenReturn(PaymentMethod.CREDITCARD);
    when(purchase.getInvoiceNr()).thenReturn("invoicenumber");
    Date date = new Date();
    when(purchase.getCreated()).thenReturn(date);
    when(purchase.getCreditVoucherDate()).thenReturn(date.toInstant());
    Country country = new Country();
    country.setCode("at");
    when(purchase.getCountry()).thenReturn(country);

    PurchaseRepository purchaseRepository = mock(PurchaseRepository.class);
    when(purchaseRepository.findTopByOrderByCreditVoucherNrDesc()).thenReturn(purchase);
    when(purchaseRepository.findById(eq(123L))).thenReturn(purchase);

    CreditVoucherItem creditVoucherItem = new CreditVoucherItem();
    creditVoucherItem.setPriceNetTotal(new BigDecimal(5));
    creditVoucherItem.setPriceGrossTotal(new BigDecimal(10));
    creditVoucherItem.setVatRate(new BigDecimal(15));
    creditVoucherItem.setVatTotal(new BigDecimal(5));
    creditVoucherItem.setAmount(5L);
    creditVoucherItem.setTitle("title");
    creditVoucherItem.setCode("code");

    List<CreditVoucherItem> creditVoucherItemList = new ArrayList<CreditVoucherItem>();
    creditVoucherItemList.add(creditVoucherItem);
    ProductHelper productHelper = mock(ProductHelper.class);
    doReturn(creditVoucherItemList)
        .when(productHelper)
        .getProductDetailsCreditVoucher(any(), any());

    CreditVoucherItemRepository creditVoucherItemRepository =
        mock(CreditVoucherItemRepository.class);

    ShopS3Uploader shopS3Uploader = mock(ShopS3Uploader.class);

    CreditVoucherService creditVoucherService =
        CreditVoucherService.builder()
            .purchaseRepository(purchaseRepository)
            .generateShopNumbers(new NumberGenerator())
            .productHelper(productHelper)
            .creditVoucherItemRepository(creditVoucherItemRepository)
            .shopS3Uploader(shopS3Uploader)
            .build();

    creditVoucherService.createCreditVoucher(purchaseDto);

    verify(shopS3Uploader).uploadToAmazon(any(), any());
    verify(purchaseRepository).findById(eq(123L));

    File file = new File(System.getProperty("java.io.tmpdir") + File.separator + "G0000001.pdf");
    Assert.assertTrue(file.exists());
    file.delete();
  }

  @Test
  public void finalizeInvoice() {
    InvoiceRepository invoiceRepository = mock(InvoiceRepository.class);
    Invoice invoice = new Invoice();
    when(invoiceRepository.findByInvoiceNumber(eq("12345"))).thenReturn(invoice);
    CreditVoucherService creditVoucherService =
        CreditVoucherService.builder().invoiceRepository(invoiceRepository).build();

    creditVoucherService.finalizeCreditVoucher(
        Purchase.builder().paymentMethod(PaymentMethod.INVOICE).invoiceNr("12345").build());

    verify(invoiceRepository, times(1)).findByInvoiceNumber(eq("12345"));
    verify(invoiceRepository, times(1)).save(eq(invoice));
  }

  @Test
  public void finalizeCreditCard() {
    InvoiceRepository invoiceRepository = mock(InvoiceRepository.class);
    CreditVoucherService creditVoucherService =
        CreditVoucherService.builder().invoiceRepository(invoiceRepository).build();

    creditVoucherService.finalizeCreditVoucher(
        Purchase.builder().paymentMethod(PaymentMethod.CREDITCARD).invoiceNr("12345").build());

    verify(invoiceRepository, times(0)).findByInvoiceNumber(eq("12345"));
    verify(invoiceRepository, times(0)).save(anyList());
  }
}
