package com.kiga.shop.backend.service.delivery;

import static org.assertj.core.api.Assertions.assertThat;

import com.kiga.shop.backend.service.PurchaseBuilderForDelivery;
import com.kiga.shop.domain.Purchase;
import java.util.Arrays;
import java.util.Collections;
import org.junit.Test;

public class DeliverySplitterCheckerTest {
  @Test
  public void testNormal() {
    Purchase purchase =
        new PurchaseBuilderForDelivery()
            .withPurchaseIdAmount(1, 5)
            .withPurchaseIdAmount(2, 3)
            .withPurchaseIdAmount(3, 5)
            .build();

    assertThat(
            new DeliverySplitterChecker()
                .isValid(
                    purchase.getPurchaseDeliveries().get(0),
                    Arrays.asList(
                        new PurchaseProductIdAmount(1, 2), new PurchaseProductIdAmount(2, 3))))
        .isTrue();
  }

  @Test
  public void testAgainstInvalidProduct() {
    Purchase purchase = new PurchaseBuilderForDelivery().withPurchaseIdAmount(1, 5).build();

    assertThat(
            new DeliverySplitterChecker()
                .isValid(
                    purchase.getPurchaseDeliveries().get(0),
                    Collections.singletonList(new PurchaseProductIdAmount(2, 1))))
        .isFalse();
  }

  @Test
  public void testWithZeroAmount() {
    Purchase purchase =
        new PurchaseBuilderForDelivery()
            .withPurchaseIdAmount(1, 5)
            .withPurchaseIdAmount(2, 1)
            .build();

    assertThat(
            new DeliverySplitterChecker()
                .isValid(
                    purchase.getPurchaseDeliveries().get(0),
                    Arrays.asList(
                        new PurchaseProductIdAmount(1, 0), new PurchaseProductIdAmount(2, 0))))
        .isFalse();
  }

  @Test
  public void testWithNegativeAmount() {
    Purchase purchase = new PurchaseBuilderForDelivery().withPurchaseIdAmount(1, 2).build();

    assertThat(
            new DeliverySplitterChecker()
                .isValid(
                    purchase.getPurchaseDeliveries().get(0),
                    Collections.singletonList(new PurchaseProductIdAmount(1, -1))))
        .isFalse();
  }

  @Test
  public void testWithInvalidAmount() {
    Purchase purchase = new PurchaseBuilderForDelivery().withPurchaseIdAmount(1, 2).build();

    assertThat(
            new DeliverySplitterChecker()
                .isValid(
                    purchase.getPurchaseDeliveries().get(0),
                    Collections.singletonList(new PurchaseProductIdAmount(1, 3))))
        .isFalse();
  }

  @Test
  public void testWithCompletePurchase() {
    Purchase purchase =
        new PurchaseBuilderForDelivery()
            .withPurchaseIdAmount(1, 2)
            .withPurchaseIdAmount(2, 5)
            .withPurchaseIdAmount(3, 7)
            .build();

    assertThat(
            new DeliverySplitterChecker()
                .isValid(
                    purchase.getPurchaseDeliveries().get(0),
                    Arrays.asList(
                        new PurchaseProductIdAmount(3, 7),
                        new PurchaseProductIdAmount(1, 2),
                        new PurchaseProductIdAmount(2, 5))))
        .isFalse();
  }
}
