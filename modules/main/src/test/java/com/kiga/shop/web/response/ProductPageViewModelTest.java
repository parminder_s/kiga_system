package com.kiga.shop.web.response;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanHashCode;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanToString;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertThat;

import com.kiga.shopcontent.viewmodel.ProductPageViewModel;
import org.junit.Test;

/**
 * @author bbs
 * @since 9/4/16.
 */
public class ProductPageViewModelTest {
  @Test
  public void testBean() {
    assertThat(ProductPageViewModel.class,
      allOf(hasValidGettersAndSetters(), hasValidBeanHashCode(), hasValidBeanToString()));
  }
}
