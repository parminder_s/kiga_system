package com.kiga.shop.test;

import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.security.domain.Member;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.domain.PurchaseStatus;
import com.kiga.shop.domain.ShippingPartner;
import com.kiga.util.InstantCreator;
import java.math.BigDecimal;
import java.util.Collections;

public class PurchaseBuilder {
  /** returns a pre-filled Purchase Builder for unit tests. */
  public static Purchase.PurchaseBuilder get() {
    Member member = new Member();
    member.setCustomerId("5");

    return Purchase.builder()
        .firstname("Hugo")
        .lastname("Victor")
        .street("In der Tiefe")
        .streetNumber("5")
        .floorNumber("1")
        .doorNumber("26")
        .zip("10234")
        .city("The Shire")
        .countryTitle("Auenland")
        .addressName1("In der Tiefe")
        .addressName2(null)
        .priceNetTotal(new BigDecimal("5.00"))
        .priceGrossTotal(new BigDecimal("6.00"))
        .status(PurchaseStatus.Shipped)
        .shippingNoteNr("Ship01")
        .shippingPartner(ShippingPartner.DHL)
        .orderNr(45L)
        .orderDate(InstantCreator.create(2017, 12, 1))
        .invoiceNr("R18011")
        .invoiceDate(InstantCreator.create(2018, 1, 1))
        .paymentMethod(PaymentMethod.INVOICE)
        .customer(member)
        .email("hugo.victor@kigaportal.com")
        .phoneNr("+43506 1200 8421")
        .purchaseItems(Collections.singletonList(PurchaseItem.builder().build()))
        .billAddressName1("The Dark Tower")
        .billAddressName2("5-1")
        .billStreet("Dark Tower Street")
        .billStreetNumber("5")
        .billFloorNumber("5")
        .billDoorNumber("6")
        .billZip("12345")
        .billCity("Mordor")
        .billCountry(null);
  }
}
