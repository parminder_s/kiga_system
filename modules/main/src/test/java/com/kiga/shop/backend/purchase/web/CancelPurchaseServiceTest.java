package com.kiga.shop.backend.purchase.web;

import static com.kiga.shop.domain.PurchaseStatus.Cancelled;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.security.domain.Member;
import com.kiga.security.spring.SecurityUtil;
import com.kiga.shop.backend.api.model.PurchaseResponse;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.workflow.PurchaseWorkflowHistoryService;
import com.kiga.spec.Fetcher;
import org.junit.Test;

public class CancelPurchaseServiceTest {
  @Test
  public void test() {
    Fetcher fetcher = mock(Fetcher.class);
    PurchaseWorkflowHistoryService workflow = mock(PurchaseWorkflowHistoryService.class);
    Purchase purchase = new Purchase();
    when(fetcher.find(2L)).thenReturn(purchase);
    Member member = new Member();
    SecurityUtil securityUtil = mock(SecurityUtil.class);
    when(securityUtil.getMember()).thenReturn(member);
    PurchaseResponseConverter converter = mock(PurchaseResponseConverter.class);
    PurchaseResponse response = new PurchaseResponse();
    when(converter.convertToResponse(purchase)).thenReturn(response);

    assertThat(new CancelPurchaseService(fetcher, workflow, securityUtil, converter).cancel(2L))
        .isSameAs(response);
    verify(workflow).proceedToState(singletonList(purchase), Cancelled, member);
  }
}
