package com.kiga.shop.basket.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.shop.basket.service.price.DiscountSetter;
import com.kiga.shop.basket.service.price.PriceGrossWithoutShippingCalcer;
import com.kiga.shop.basket.service.price.PurchaseItemSetter;
import com.kiga.shop.basket.service.price.ShippingCostsSetter;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Arrays;

public class PricesSetterTest {
  @Test
  public void testDefault() {
    Purchase purchase = Purchase.builder()
      .country(Country.builder().currency("EUR").build())
      .purchaseItems(Arrays.asList(
        PurchaseItem.builder().priceNetSingle(new BigDecimal("50.00")).amount(2).build(),
        PurchaseItem.builder().priceNetSingle(new BigDecimal("25")).amount(4).build()
      ))
      .priceShippingNet(new BigDecimal("100")).vatShipping(new BigDecimal("20"))
      .build();

    PriceGrossWithoutShippingCalcer calcer = mock(PriceGrossWithoutShippingCalcer.class);
    when(calcer.calc(any())).thenReturn(new BigDecimal("360"));

    new PricesSetter(mock(ShippingCostsSetter.class), mock(PurchaseItemSetter.class),
      calcer, mock(DiscountSetter.class)).setPrices(purchase);

    assertEquals("EUR", purchase.getCurrency());
    assertEquals(new BigDecimal("480.00"), purchase.getPriceGrossTotal());
    assertEquals(new BigDecimal(("300.00")), purchase.getPriceNetTotal());
  }

  @Test
  public void testShippingAndItemsCalls() {
    Purchase purchase = Purchase.builder()
      .country(Country.builder().build())
      .priceShippingNet(BigDecimal.ZERO).vatShipping(BigDecimal.ONE)
      .purchaseItems(Arrays.asList())
      .build();

    PriceGrossWithoutShippingCalcer calcer = mock(PriceGrossWithoutShippingCalcer.class);
    when(calcer.calc(any())).thenReturn(BigDecimal.ZERO);

    ShippingCostsSetter shippingCostsSetter = mock(ShippingCostsSetter.class);
    PurchaseItemSetter purchaseItemSetter = mock(PurchaseItemSetter.class);
    DiscountSetter discountSetter = mock(DiscountSetter.class);

    PricesSetter pricesSetter =
      new PricesSetter(shippingCostsSetter, purchaseItemSetter, calcer, discountSetter);
    pricesSetter.setPrices(purchase);

    InOrder inOrder = inOrder(discountSetter, purchaseItemSetter, shippingCostsSetter);
    inOrder.verify(purchaseItemSetter).setPurchaseItems(purchase);
    inOrder.verify(discountSetter).applyDiscounts(any());
    inOrder.verify(shippingCostsSetter).setCosts(purchase);
  }

  @Test
  public void testWithEmptyBasket() {
    Purchase purchase = Purchase.builder()
      .country(Country.builder().build())
      .priceShippingNet(BigDecimal.ZERO).vatShipping(BigDecimal.ONE)
      .purchaseItems(Arrays.asList())
      .build();

    PriceGrossWithoutShippingCalcer calcer = mock(PriceGrossWithoutShippingCalcer.class);
    when(calcer.calc(any())).thenReturn(BigDecimal.ZERO);

    new PricesSetter(
      mock(ShippingCostsSetter.class), mock(PurchaseItemSetter.class),
      calcer, mock(DiscountSetter.class)).setPrices(purchase);

    assertEquals(new BigDecimal("1.00"), purchase.getPriceGrossTotal());
    assertEquals(BigDecimal.ZERO, purchase.getPriceNetTotal());
  }
}
