package com.kiga.shop.web;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.shop.service.CountryGroupService;
import com.kiga.shop.web.response.CountryGroupViewModel;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author bbs
 * @since 10/16/16.
 */
public class CountryGroupsControllerTest {
  private CountryGroupsController countryGroupsController;
  private CountryGroupService countryGroupService = mock(CountryGroupService.class);

  @Before
  public void prepareTests() {
    countryGroupsController = new CountryGroupsController(countryGroupService);
  }

  @Test
  public void testGetAll() {
    List<CountryGroupViewModel> countryGroupViewModelsIn = asList(new CountryGroupViewModel(),
      new CountryGroupViewModel());
    when(countryGroupService.getAll()).thenReturn(countryGroupViewModelsIn);
    List<CountryGroupViewModel> countryGroupViewModelsOut = countryGroupsController.list();
    verify(countryGroupService).getAll();
    assertNotNull(countryGroupViewModelsOut);
    assertEquals(countryGroupViewModelsIn, countryGroupViewModelsOut);
  }
}
