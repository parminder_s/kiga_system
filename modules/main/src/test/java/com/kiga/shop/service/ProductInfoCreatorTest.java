package com.kiga.shop.service;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.repository.ProductDetailRepository;
import com.kiga.shop.web.response.ProductInfo;
import com.kiga.web.service.GeoIpService;
import com.mysema.query.types.Predicate;
import java.math.BigDecimal;
import java.util.Optional;
import org.junit.Test;

/**
 * @author bbs
 * @since 3/19/17.
 */
public class ProductInfoCreatorTest {
  @Test
  public void testCreateForProductWithAllRelations() {
    Product productEntity =
        Product.builder()
            .productTitle("SuperCar")
            .code("SC")
            .volumeLitre(BigDecimal.TEN)
            .weightGram(3)
            .build();

    ProductDetail productDetail =
        ProductDetail.builder()
            .product(productEntity)
            .priceNet(new BigDecimal("100.000"))
            .vatProduct(new BigDecimal("0.100"))
            .country(
                Country.builder()
                    .code("at")
                    .currency("EUR")
                    .countryShop(
                        CountryShop.builder()
                            .minDeliveryDays(4L)
                            .maxDeliveryDays(5L)
                            .daysToPayInvoice(6)
                            .freeShippingLimit(new BigDecimal("100"))
                            .priceShippingNet(BigDecimal.TEN)
                            .vatRateShipping(BigDecimal.valueOf(0.16))
                            .build())
                    .build())
            .build();

    productEntity.setId(1L);
    productDetail.setId(2L);
    productDetail.getCountry().setId(3L);
    productDetail.getCountry().getCountryShop().setId(4L);

    GeoIpService ipService = mock(GeoIpService.class);
    when(ipService.resolveCountryCode()).thenReturn("at");
    ProductDetailRepository productDetailRepository = mock(ProductDetailRepository.class);
    when(productDetailRepository.findOne(any(Predicate.class))).thenReturn(productDetail);

    ProductInfo productInfo =
        new ProductInfoCreator(new VatRateCalculator(), ipService, productDetailRepository)
            .create(1)
            .get();

    ProductInfo expectedProductInfo =
        ProductInfo.builder()
            .id(1L)
            .productTitle("SuperCar")
            .productCode("SC")
            .volumeLitre(BigDecimal.TEN)
            .weightGram(3)
            .priceNet(new BigDecimal("100.000"))
            .vatRate(new BigDecimal("0.100"))
            .priceGross(new BigDecimal("110.00"))
            .vatProduct(new BigDecimal("10.00"))
            .minDeliveryDays(4L)
            .maxDeliveryDays(5L)
            .currency("EUR")
            .freeShippingLimit(BigDecimal.valueOf(100))
            .priceShippingNet(BigDecimal.TEN)
            .vatRateShipping(BigDecimal.valueOf(0.16))
            .daysToPayInvoice(6)
            .product(productEntity)
            .build();

    assertThat(productInfo, sameBeanAs(expectedProductInfo));
  }

  @Test
  public void testInvalidCountry() {
    ProductDetailRepository productDetailRepository = mock(ProductDetailRepository.class);
    when(productDetailRepository.findOne(any(Predicate.class))).thenReturn(null);

    Optional<ProductInfo> productInfo =
        new ProductInfoCreator(null, null, productDetailRepository).create(1, "fo");

    assertFalse(productInfo.isPresent());
  }
}
