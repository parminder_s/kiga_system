package com.kiga.shop.basket.voucher;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.shop.basket.service.PricesSetter;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.domain.Voucher;
import com.kiga.shop.repository.VoucherRepositoryExt;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

public class ApplyVoucherTest {
  @Test
  public void testVoucher() {
    Product product1 = new Product();
    product1.setId(1L);
    Product product2 = new Product();
    product2.setId(2L);

    Purchase purchase = Purchase.builder()
      .purchaseItems(Arrays.asList(
        PurchaseItem.builder().product(product1).build(),
        PurchaseItem.builder().product(product2).build()
      ))
      .build();

    PricesSetter pricesSetter = mock(PricesSetter.class);
    VoucherRepositoryExt repositoryExt = mock(VoucherRepositoryExt.class);
    Voucher voucher = Voucher.builder().product(product2).build();
    when(repositoryExt.findValidVoucher(anyString(), any()))
      .thenReturn(Optional.of(voucher));

    boolean successful =
      new ApplyVoucher(pricesSetter, repositoryExt).apply("discount", purchase);

    PurchaseItem purchaseItem = purchase.getPurchaseItems().get(1);
    assertEquals(voucher, purchaseItem.getVoucher());
    assertEquals("discount", purchaseItem.getVoucherCode());
    assertTrue(successful);
  }

  @Test
  public void testFindWithNoVoucher() {
    VoucherRepositoryExt repositoryExt = mock(VoucherRepositoryExt.class);
    when(repositoryExt.findValidVoucher(any(), any())).thenReturn(Optional.empty());

    assertFalse(new ApplyVoucher(null, repositoryExt).apply("code", null));
  }

  @Test
  public void testFindWithNoProduct() {
    VoucherRepositoryExt repositoryExt = mock(VoucherRepositoryExt.class);
    Voucher voucher = Voucher.builder().product(new Product()).build();
    when(repositoryExt.findValidVoucher(any(), any())).thenReturn(Optional.of(voucher));

    Product product = new Product();
    product.setId(1L);

    Purchase purchase = Purchase.builder()
      .purchaseItems(Collections.singletonList(
        PurchaseItem.builder().product(product).build()
      ))
      .build();

    assertFalse(new ApplyVoucher(null, repositoryExt).apply("code", purchase));
  }
}
