package com.kiga.shop.backend;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.security.services.SecurityService;
import com.kiga.shop.backend.exception.NonexistentEntityUpdateException;
import com.kiga.shop.backend.web.request.DeactivateProductRequest;
import com.kiga.shop.backend.web.request.ProductCloneRequest;
import com.kiga.shop.backend.web.request.ProductFormRequest;
import com.kiga.shop.backend.web.request.ProductsListRequest;
import com.kiga.shop.backend.web.response.ProductViewModel;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.repository.ProductDetailRepository;
import com.kiga.shop.repository.ProductRepository;
import com.kiga.shop.service.DefaultProductService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author bbs
 * @since 10/7/16.
 */
public class ProductsControllerTest {
  private static final long MOCK_ID = 1;
  private static final int PAGE_SIZE = 50;
  private static final int PAGE = 0;
  private ProductsController productsController;
  private DefaultProductService defaultProductService = mock(DefaultProductService.class);
  private SecurityService securityService = mock(SecurityService.class);

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    ProductViewModel productViewModel = new ProductViewModel();
    productViewModel.setId(MOCK_ID);
    when(defaultProductService.getProducts())
      .thenReturn(Collections.singletonList(productViewModel));
    when(defaultProductService.getProductById(MOCK_ID)).thenReturn(productViewModel);

    productsController = new ProductsController(defaultProductService, securityService,
      null, null);
  }

  @Test
  public void testGetProduct() {
    ProductFormRequest productFormRequest = new ProductFormRequest();
    productFormRequest.setProductId(MOCK_ID);
    ProductViewModel productViewModel = productsController.getProduct(productFormRequest);
    assertNotNull(productViewModel);
  }

  @Test
  public void testGetList() {
    ProductsListRequest productsListRequest = new ProductsListRequest();
    productsListRequest.setPage(PAGE);
    productsListRequest.setPageSize(PAGE_SIZE);
    List<ProductViewModel> products = productsController.getList();
    assertNotNull(products);
    assertEquals(1, products.size());
    verify(defaultProductService).getProducts();
  }

  @Test
  public void testGetListWrongRequest() {
    ProductsListRequest productsListRequest = new ProductsListRequest();
    productsListRequest.setPage(-1);
    productsListRequest.setPageSize(-1);
    List<ProductViewModel> products = productsController.getList();
    assertNotNull(products);
    assertEquals(1, products.size());
    verify(defaultProductService).getProducts();
  }

  @Test
  public void testGetListEmptyResults() {
    when(defaultProductService.getProducts()).thenReturn(null);
    List<ProductViewModel> products = productsController.getList();
    assertNotNull(products);
    assertEquals(0, products.size());
    verify(defaultProductService).getProducts();
  }

  @Test
  public void testGetListEmptyRequest() {
    List<ProductViewModel> products = productsController.getList();
    assertNotNull(products);
    assertEquals(1, products.size());
    verify(defaultProductService).getProducts();
  }

  @Test
  public void testSave() throws NonexistentEntityUpdateException {
    ProductViewModel productViewModel = new ProductViewModel();
    productViewModel.setId(1L);
    productsController.save(productViewModel);

    ArgumentCaptor<ProductViewModel> productViewModelArgumentCaptor = ArgumentCaptor.forClass(
      ProductViewModel.class);
    verify(defaultProductService).save(productViewModelArgumentCaptor.capture());

    ProductViewModel productViewModelOut = productViewModelArgumentCaptor.getValue();
    assertEquals(1L, productViewModelOut.getId().longValue());
    assertEquals(true, productViewModelOut.isActivated());
  }

  @Test
  public void testDeactivate() throws NonexistentEntityUpdateException {
    DeactivateProductRequest deactivateProductRequest = new DeactivateProductRequest();
    deactivateProductRequest.setId(1L);
    productsController.deactivate(deactivateProductRequest);
    verify(defaultProductService).deactivate(deactivateProductRequest.getId());
  }

  @Test
  public void testClone() throws NonexistentEntityUpdateException {
    Product product = Product.builder()
      .code("test-code")
      .productDetails(Arrays.asList(new ProductDetail(), new ProductDetail()))
      .build();

    product.setId(1L);

    ProductRepository productRepository = mock(ProductRepository.class);
    ProductDetailRepository productDetailRepository = mock(ProductDetailRepository.class);
    when(productRepository.findOne(1L)).thenReturn(product);
    when(productRepository.findByCode("test-code_C")).thenReturn(new Product());

    doAnswer(invocation -> {
      Object[] args = invocation.getArguments();
      ((Product) args[0]).setId(1L);
      return null;
    }).when(productRepository).save(any(Product.class));

    doAnswer(invocation -> {
      Object[] args = invocation.getArguments();
      ((ProductDetail) args[0]).setId(1L);
      return null;
    }).when(productDetailRepository).save(any(ProductDetail.class));

    ProductsController productsController = new ProductsController(defaultProductService,
      securityService, productRepository, productDetailRepository);

    productsController.cloneProduct(new ProductCloneRequest(1L));
    verify(productRepository).save(any(Product.class));
    verify(productDetailRepository, times(2)).save(any(ProductDetail.class));


  }
}
