package com.kiga.shop.repository;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.shop.domain.Voucher;
import com.kiga.util.InstantCreator;
import com.mysema.query.types.Predicate;
import org.junit.Test;

import java.time.Instant;

public class VoucherRepositoryExtTest {
  @Test
  public void testFindValidVoucher() {
    VoucherRepository repository = mock(VoucherRepository.class);
    VoucherRepositoryExt repositoryExt = new VoucherRepositoryExt(repository);
    Instant instant = InstantCreator.create(2017, 5, 1);
    Voucher voucher = new Voucher();
    when(repository.findOne(any(Predicate.class)))
      .thenReturn(voucher);

    Voucher voucherActual = repositoryExt.findValidVoucher("foo-voucher", instant).get();

    assertEquals(voucher, voucherActual);
  }
}
