package com.kiga.shop.backend;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;
import org.junit.Test;

public class ProductClonerTest {

  @Test
  public void testCloneProduct() {
    Product testProduct =
        Product.builder()
            .productTitle("TestProduct")
            .code("tp")
            .productDetails(
                Arrays.asList(
                    ProductDetail.builder()
                        .priceNet(BigDecimal.ONE)
                        .vatProduct(BigDecimal.valueOf(2))
                        .vatShipping(BigDecimal.valueOf(3))
                        .freeShippingAmount(4)
                        .shippingDiscountType("testType")
                        .oldPriceGross(BigDecimal.valueOf(5))
                        .shippingDiscounts(Collections.emptyList())
                        .build(),
                    ProductDetail.builder()
                        .priceNet(BigDecimal.valueOf(6))
                        .vatProduct(BigDecimal.valueOf(7))
                        .vatShipping(BigDecimal.valueOf(8))
                        .freeShippingAmount(9)
                        .shippingDiscountType("testType")
                        .oldPriceGross(BigDecimal.valueOf(10))
                        .shippingDiscounts(Collections.emptyList())
                        .build()))
            .build();

    testProduct
        .getProductDetails()
        .stream()
        .forEach(
            productDetail -> {
              productDetail.setProduct(testProduct);
              productDetail.setClassName("ProductDetail");
              productDetail.setId(2L);
            });
    testProduct.setClassName("Product");
    testProduct.setId(1L);

    ProductCloner productCloner = new ProductCloner();
    Product clonedProduct = productCloner.cloneProduct(testProduct);

    assertNotEquals(testProduct, clonedProduct);
    assertEquals("TestProduct_C", clonedProduct.getProductTitle());
    assertEquals("tp_C", clonedProduct.getCode());
    assertThat(
        testProduct,
        sameBeanAs(clonedProduct)
            .ignoring("0x1.productTitle")
            .ignoring("0x1.created")
            .ignoring("0x1.code")
            .ignoring("0x1.id")
            .ignoring(ProductDetail.class));
    assertNotEquals(testProduct.getProductDetails(), clonedProduct.getProductDetails());
    assertThat(
        testProduct.getProductDetails(),
        sameBeanAs(clonedProduct.getProductDetails()).ignoring("id").ignoring(Product.class));
    assertNull(clonedProduct.getId());
    assertTrue(
        clonedProduct
            .getProductDetails()
            .stream()
            .filter(productDetail -> productDetail.getId() != null)
            .collect(Collectors.toList())
            .equals(Collections.emptyList()));
  }
}
