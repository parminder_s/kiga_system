package com.kiga.shop.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import org.junit.Test;

/** Created by robert on 03.08.17. */
public class VatRateCalculatorTest {
  @Test
  public void testGross_12_5() {
    assertNet("10.416", "0.200", "12.50");
  }

  @Test
  public void testGross_12_49() {
    assertNet("10.408", "0.200", "12.49");
  }

  @Test
  public void testGross_12_48() {
    assertNet("10.400", "0.200", "12.48");
  }

  @Test
  public void testGross_39_9() {
    assertNet("36.272", "0.100", "39.90");
  }

  @Test
  public void testGross_9() {
    assertNet("8.181", "0.100", "9.00");
  }

  @Test
  public void testGross_69_90() {
    assertGross("69.90", "0.100", "63.545");
  }

  @Test
  public void testNet_10_416() {
    assertGross("12.50", "0.200", "10.416");
  }

  @Test
  public void testNet_12() {
    assertGross("12.00", "0.200", "10.000");
  }

  @Test
  public void testGrossFailsOnNon3DigitVatRate() {
    assertThatThrownBy(
            () ->
                new VatRateCalculator().calculateGrossPrice(BigDecimal.TEN, new BigDecimal("0.20")))
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  public void testNetFailsOnNon3DigitVatRate() {
    assertThatThrownBy(
            () -> new VatRateCalculator().calculateNetPrice(BigDecimal.TEN, new BigDecimal("0.20")))
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  public void testGrossFailsOnNon3DigitNetPrice() {
    assertThatThrownBy(
            () ->
                new VatRateCalculator()
                    .calculateGrossPrice(new BigDecimal("10.00"), new BigDecimal("0.200")))
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  public void testNetFailsOnNon2DigitNetPrice() {
    assertThatThrownBy(
            () ->
                new VatRateCalculator()
                    .calculateNetPrice(new BigDecimal("10.000"), new BigDecimal("0.200")))
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  public void testGrossWith3Digit() {}

  /** assert for Gross -> Net. */
  public void assertNet(String net, String vat, String gross) {
    assertEquals(
        new BigDecimal(net),
        new VatRateCalculator().calculateNetPrice(new BigDecimal(gross), new BigDecimal(vat)));
  }

  /** assert for Net -> Gross. */
  public void assertGross(String gross, String vat, String net) {
    assertEquals(
        new BigDecimal(gross),
        new VatRateCalculator().calculateGrossPrice(new BigDecimal(net), new BigDecimal(vat)));
  }
}
