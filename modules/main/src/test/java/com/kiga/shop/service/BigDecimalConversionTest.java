package com.kiga.shop.service;

import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Test;

public class BigDecimalConversionTest {

  @Test(expected = ArithmeticException.class)
  public void testScaleConversion3To2NoRounding() {
    BigDecimal scale3 = BigDecimal.valueOf(12.345);
    scale3.setScale(2);
  }

  @Test
  public void testScaleConversion2To3NoRounding() {
    BigDecimal scale2 = BigDecimal.valueOf(12.34);
    BigDecimal scale3 = scale2.setScale(3);

    Assert.assertEquals("12.340", scale3.toPlainString());
  }
}
