package com.kiga.shop.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.shop.backend.web.response.ProductDetailViewModel;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.repository.CountryRepository;
import com.kiga.shop.repository.ProductDetailRepository;
import com.kiga.shop.repository.ProductRepository;
import java.math.BigDecimal;
import org.junit.Test;

public class ProductDetailUpdaterTest {
  @Test
  public void testUpdate() {
    ProductDetailViewModel productDetailViewModel =
        ProductDetailViewModel.builder()
            .id(1L)
            .countryId(38L)
            .productId(11L)
            .currencyCode("EUR")
            .shippingDiscountType("DSCTP")
            .activated(true)
            .maxOrderAmount(10)
            .priceGross(new BigDecimal("12.00"))
            .vatProduct(new BigDecimal("0.200"))
            .build();
    ProductDetail productDetail = new ProductDetail();
    productDetail.setId(1L);
    CountryRepository countryRepository = mock(CountryRepository.class);
    ProductRepository productRepository = mock(ProductRepository.class);
    when(countryRepository.findOne(productDetailViewModel.getCountryId()))
        .thenReturn(new Country());
    Product product = new Product();
    product.setId(11L);
    when(productRepository.findByActivatedTrueAndId(productDetailViewModel.getProductId()))
        .thenReturn(product);
    ProductDetailRepository productDetailRepository = mock(ProductDetailRepository.class);

    new ProductDetailUpdater(
            productRepository, countryRepository, new VatRateCalculator(), productDetailRepository)
        .update(productDetailViewModel, productDetail);

    assertEquals(1L, productDetail.getId().longValue());
    assertEquals(11L, productDetail.getProduct().getId().longValue());
    assertEquals("DSCTP", productDetail.getShippingDiscountType());
    assertEquals(true, productDetail.getActivated());
    assertEquals(new BigDecimal("10.000"), productDetail.getPriceNet());
    assertEquals(new BigDecimal("0.200"), productDetail.getVatProduct());
  }
}
