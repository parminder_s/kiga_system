package com.kiga.shop.service;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.shop.backend.exception.IllegalPaginationArgumentException;
import com.kiga.shop.backend.exception.NonexistentEntityUpdateException;
import com.kiga.shop.backend.web.converter.ProductToViewModelConverter;
import com.kiga.shop.backend.web.converter.VoucherToViewModelConverter;
import com.kiga.shop.backend.web.response.VoucherViewModel;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Voucher;
import com.kiga.shop.repository.ProductRepository;
import com.kiga.shop.repository.VoucherRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Schneider Florian on 28.11.17.
 */
public class DefaultVoucherServiceTest {
  private static final long MOCK_ID = 1;
  private static final long PRODUCT_MOCK_ID = 2;
  private static final int PAGE = 0;
  private static final int PAGE_SIZE = 10;
  private static final int WRONG_PAGE = -1;
  private static final int WRONG_PAGE_SIZE = -1;
  private static final String TITLE = "MOCK_TITLE";
  private static final String CODE = "MOCK_CODE";

  private DefaultVoucherService defaultVoucherService;
  private DefaultProductService defaultProductService;
  private VoucherRepository voucherRepository = mock(VoucherRepository.class);
  private ProductRepository productRepository = mock(ProductRepository.class);
  private VoucherToViewModelConverter voucherToViewModelConverter
          = new VoucherToViewModelConverter();
  private ProductToViewModelConverter productToViewModelConverter
          = new ProductToViewModelConverter();

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    Voucher voucher = new Voucher();
    voucher.setId(MOCK_ID);
    voucher.setActivated(true);
    Product product = new Product();
    product.setId(PRODUCT_MOCK_ID);
    voucher.setProduct(product);
    when(voucherRepository.findByActivatedIsTrue(any(PageRequest.class)))
            .thenReturn(asList(voucher, null));
    when(voucherRepository.findByActivatedTrueAndId(MOCK_ID)).thenReturn(voucher);
    when(productRepository.findByActivatedTrueAndId(PRODUCT_MOCK_ID)).thenReturn(product);
    defaultProductService = new DefaultProductService(productRepository,
            productToViewModelConverter);
    defaultVoucherService = new DefaultVoucherService(voucherRepository,
            voucherToViewModelConverter, defaultProductService);
  }

  @Test
  public void testGetVouchers() throws IllegalPaginationArgumentException {
    List<VoucherViewModel> voucherViewModelList = defaultVoucherService
            .getVouchers(PAGE, PAGE_SIZE);
    assertNotNull(voucherViewModelList);
    assertTrue(CollectionUtils.isNotEmpty(voucherViewModelList));
    assertEquals(1, voucherViewModelList.size());
  }

  @Test
  public void testGetVouchersEmptyResults() throws IllegalPaginationArgumentException {
    when(voucherRepository.findByActivatedIsTrue(any(PageRequest.class)))
            .thenReturn(Collections.emptyList());
    List<VoucherViewModel> voucherViewModelList = defaultVoucherService
            .getVouchers(PAGE, PAGE_SIZE);
    assertNotNull(voucherViewModelList);
    assertTrue(CollectionUtils.isEmpty(voucherViewModelList));
    assertEquals(0, voucherViewModelList.size());
  }

  @Test
  public void testGetVouchersNullResult() throws IllegalPaginationArgumentException {
    when(voucherRepository.findByActivatedIsTrue(any(PageRequest.class))).thenReturn(null);
    List<VoucherViewModel> voucherViewModelList = defaultVoucherService
            .getVouchers(PAGE, PAGE_SIZE);
    assertNotNull(voucherViewModelList);
    assertTrue(CollectionUtils.isEmpty(voucherViewModelList));
    assertEquals(0, voucherViewModelList.size());
  }

  @Test(expected = IllegalPaginationArgumentException.class)
  public void testGetVouchersWrongPage() throws IllegalPaginationArgumentException {
    defaultVoucherService.getVouchers(WRONG_PAGE, PAGE_SIZE);
  }

  @Test(expected = IllegalPaginationArgumentException.class)
  public void testGetVouchersWrongPageSize() throws IllegalPaginationArgumentException {
    defaultVoucherService.getVouchers(PAGE, WRONG_PAGE_SIZE);
  }

  @Test
  public void testGetVoucher() {
    VoucherViewModel voucherViewModel = defaultVoucherService.getVoucher(MOCK_ID);
    assertNotNull(voucherViewModel);
    assertNotNull(voucherViewModel.getId());
    assertEquals(MOCK_ID, voucherViewModel.getId().intValue());
  }

  @Test
  public void testGetVoucherNoResult() {
    when(voucherRepository.findByActivatedTrueAndId(MOCK_ID)).thenReturn(null);
    VoucherViewModel voucherViewModel = defaultVoucherService.getVoucher(MOCK_ID);
    assertNull(voucherViewModel);
  }

  @Test
  public void testSave() throws NonexistentEntityUpdateException {
    VoucherViewModel voucherViewModel = new VoucherViewModel();
    voucherViewModel.setId(MOCK_ID);
    voucherViewModel.setProductId(PRODUCT_MOCK_ID);
    voucherViewModel.setDiscountRate(BigDecimal.valueOf(0.3));
    voucherViewModel.setCode(CODE);
    voucherViewModel.setValidFrom(new Date());
    voucherViewModel.setValidTo(new Date());
    Voucher voucher = new Voucher();
    voucher.setId(voucherViewModel.getId());
    defaultVoucherService.save(voucherViewModel);
    verify(voucherRepository).findByActivatedTrueAndId(voucherViewModel.getId());
    ArgumentCaptor<Voucher> voucherArgumentCaptor = ArgumentCaptor.forClass(Voucher.class);
    verify(voucherRepository).save(voucherArgumentCaptor.capture());

    Voucher capturedVoucher = voucherArgumentCaptor.getValue();
    assertEquals(CODE, capturedVoucher.getCode());
  }

  @Test(expected = NonexistentEntityUpdateException.class)
  public void testSaveProductNull() throws NonexistentEntityUpdateException {
    VoucherViewModel voucherViewModel = new VoucherViewModel();
    voucherViewModel.setId(MOCK_ID);
    voucherViewModel.setProductId(PRODUCT_MOCK_ID);
    voucherViewModel.setDiscountRate(BigDecimal.valueOf(0.3));
    voucherViewModel.setCode(CODE);
    voucherViewModel.setValidFrom(new Date());
    voucherViewModel.setValidTo(new Date());
    Voucher voucher = new Voucher();
    voucher.setId(voucherViewModel.getId());
    when(productRepository.findByActivatedTrueAndId(PRODUCT_MOCK_ID)).thenReturn(null);

    defaultVoucherService.save(voucherViewModel);
    verify(voucherRepository).findByActivatedTrueAndId(voucherViewModel.getId());
    ArgumentCaptor<Voucher> voucherArgumentCaptor = ArgumentCaptor.forClass(Voucher.class);
    verify(voucherRepository).save(voucherArgumentCaptor.capture());

    Voucher capturedVoucher = voucherArgumentCaptor.getValue();
    assertEquals(CODE, capturedVoucher.getCode());
    assertEquals(BigDecimal.valueOf(0.3), capturedVoucher.getDiscountRate());
    assertEquals(Long.valueOf(MOCK_ID), capturedVoucher.getId());
    assertEquals(true, capturedVoucher.getActivated());
  }

  @Test
  public void testSaveNewEntity() throws NonexistentEntityUpdateException {
    VoucherViewModel voucherViewModel = new VoucherViewModel();
    voucherViewModel.setProductId(PRODUCT_MOCK_ID);
    voucherViewModel.setCode(CODE);
    voucherViewModel.setDiscountRate(BigDecimal.valueOf(0.3));
    voucherViewModel.setValidFrom(new Date());
    voucherViewModel.setValidTo(new Date());

    defaultVoucherService.save(voucherViewModel);
    ArgumentCaptor<Voucher> voucherArgumentCaptor = ArgumentCaptor.forClass(Voucher.class);
    verify(voucherRepository).save(voucherArgumentCaptor.capture());

    Voucher capturedVoucher = voucherArgumentCaptor.getValue();
    assertEquals(CODE, capturedVoucher.getCode());
    assertEquals(BigDecimal.valueOf(0.3), capturedVoucher.getDiscountRate());
    assertNull(capturedVoucher.getId());
    assertEquals(true, capturedVoucher.getActivated());
  }

  @Test(expected = NonexistentEntityUpdateException.class)
  @SuppressWarnings("unchecked")
  public void testUpdateEntityThatDoesNotExists() throws NonexistentEntityUpdateException {
    VoucherViewModel voucherViewModel = new VoucherViewModel();
    voucherViewModel.setId(MOCK_ID);
    voucherViewModel.setDiscountRate(BigDecimal.valueOf(0.3));
    voucherViewModel.setCode(CODE);
    when(voucherRepository.findByActivatedTrueAndId(voucherViewModel.getId())).thenReturn(null);
    defaultVoucherService.save(voucherViewModel);
    verify(voucherRepository).findByActivatedTrueAndId(voucherViewModel.getId());
    ArgumentCaptor<Voucher> voucherArgumentCaptor = ArgumentCaptor.forClass(Voucher.class);
    verify(voucherRepository).save(voucherArgumentCaptor.capture());

    Voucher capturedVoucher = voucherArgumentCaptor.getValue();
    assertEquals(CODE, capturedVoucher.getCode());
    assertEquals(BigDecimal.valueOf(0.3), capturedVoucher.getDiscountRate());
    assertNull(capturedVoucher.getId());
  }

  @Test
  public void testSaveWithNullArgument() throws NonexistentEntityUpdateException {
    defaultVoucherService.save(null);
    verify(voucherRepository, never()).findByActivatedTrueAndId(anyLong());
  }

  @Test
  public void testDeactivate() throws NonexistentEntityUpdateException {
    long id = MOCK_ID;
    Voucher voucherIn = new Voucher();
    when(voucherRepository.findByActivatedTrueAndId(id)).thenReturn(voucherIn);
    defaultVoucherService.deactivate(id);
    verify(voucherRepository).findByActivatedTrueAndId(id);
    ArgumentCaptor<Voucher> voucherArgumentCaptor = ArgumentCaptor.forClass(Voucher.class);
    verify(voucherRepository).save(voucherArgumentCaptor.capture());

    Voucher voucherOut = voucherArgumentCaptor.getValue();
    assertEquals(voucherIn.getCode(), voucherOut.getCode());
    assertEquals(voucherIn.getDiscountRate(), voucherOut.getDiscountRate());
    assertEquals(false, voucherOut.getActivated());
  }

  @Test(expected = NonexistentEntityUpdateException.class)
  public void testDeactivateNonexistentEntity() throws NonexistentEntityUpdateException {
    long id = MOCK_ID;
    when(voucherRepository.findByActivatedTrueAndId(id)).thenReturn(null);
    defaultVoucherService.deactivate(id);
  }

}