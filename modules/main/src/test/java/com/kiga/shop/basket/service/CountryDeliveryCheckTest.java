package com.kiga.shop.basket.service;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.service.ProductInfoCreator;
import com.kiga.shop.web.response.ProductInfo;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class CountryDeliveryCheckTest {
  @Test
  public void testDeliveryOnNoPurchaseItems() {
    Purchase purchase = new Purchase();
    purchase.setPurchaseItems(new ArrayList<>());

    assertTrue(assertCountryDeliveryCheck(Optional.of(purchase), mock(ProductInfoCreator.class)));
  }

  @Test
  public void testOnNullBasket() {
    assertTrue(assertCountryDeliveryCheck(Optional.empty(), mock(ProductInfoCreator.class)));
  }

  @Test
  public void testOkOnMultiple() {
    ProductInfoCreator productInfoCreator = mock(ProductInfoCreator.class);
    Optional<ProductInfo> prodInfo = Optional.of(new ProductInfo());
    when(productInfoCreator.create(anyLong(), any())).thenReturn(prodInfo);
    Product product = new Product();
    product.setId(1L);

    Optional<Purchase> purchase = Optional.of(
      Purchase.builder().country(new Country()).purchaseItems(Arrays.asList(
        PurchaseItem.builder().product(product).build(),
        PurchaseItem.builder().product(product).build()
      )).build());

    assertTrue(assertCountryDeliveryCheck(purchase, productInfoCreator));
  }

  @Test
  public void testFailOnMultipleWithSingleFail() {
    ProductInfoCreator productInfoCreator = mock(ProductInfoCreator.class);
    Optional<ProductInfo> prodInfo = Optional.of(new ProductInfo());
    when(productInfoCreator.create(eq(1L), any())).thenReturn(prodInfo);
    when(productInfoCreator.create(eq(2L), any())).thenReturn(Optional.empty());

    Country country = new Country();
    Product product1 = new Product();
    product1.setId(1L);
    Product product2 = new Product();
    product2.setId(2L);

    Optional<Purchase> purchase = Optional.of(
      Purchase.builder().country(country).purchaseItems(Arrays.asList(
        PurchaseItem.builder().product(product1).build(),
        PurchaseItem.builder().product(product2).build(),
        PurchaseItem.builder().product(product1).build()
      )).build());

    assertFalse(assertCountryDeliveryCheck(purchase, productInfoCreator));
  }

  @Test
  public void testOnSingleFailed() {
    ProductInfoCreator productInfoCreator = mock(ProductInfoCreator.class);
    when(productInfoCreator.create(anyLong(), any())).thenReturn(Optional.empty());

    Product product = new Product();
    product.setId(1L);

    Optional<Purchase> purchase = Optional.of(
      Purchase.builder().country(new Country()).purchaseItems(Arrays.asList(
        PurchaseItem.builder().product(product).build()
      )).build());

    assertFalse(assertCountryDeliveryCheck(purchase, productInfoCreator));
  }

  @Test
  public void testOnSingleOk() {
    ProductInfoCreator productInfoCreator = mock(ProductInfoCreator.class);
    when(productInfoCreator.create(anyLong(), any())).thenReturn(Optional.of(new ProductInfo()));

    Product product = new Product();
    product.setId(1L);

    Optional<Purchase> purchase = Optional.of(
      Purchase.builder().country(new Country()).purchaseItems(Arrays.asList(
        PurchaseItem.builder().product(product).build()
      )).build());

    assertTrue(assertCountryDeliveryCheck(purchase, productInfoCreator));
  }

  /**
   * assertion method specificall for Class under test.
   */
  public boolean assertCountryDeliveryCheck(
    Optional<Purchase> purchase, ProductInfoCreator productInfoCreator) {
    CountryDeliveryCheck countryDeliveryCheck = new CountryDeliveryCheck(productInfoCreator);

    return countryDeliveryCheck.canDeliver(purchase);
  }
}
