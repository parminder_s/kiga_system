package com.kiga.shop.service;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.shop.domain.CountryGroup;
import com.kiga.shop.repository.CountryGroupRepository;
import com.kiga.shop.web.converter.CountryGroupToViewModelConverter;
import com.kiga.shop.web.response.CountryGroupViewModel;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author bbs
 * @since 10/16/16.
 */
public class CountryGroupServiceTest {
  private CountryGroupService countryGroupService;
  private CountryGroupRepository countryGroupRepository = mock(CountryGroupRepository.class);
  private CountryGroupToViewModelConverter countryGroupToViewModelConverter = mock(
    CountryGroupToViewModelConverter.class);

  @Before
  public void prepareTests() {
    countryGroupService = new CountryGroupService(countryGroupRepository,
      countryGroupToViewModelConverter);
  }

  @Test
  public void testGetAll() {
    List<CountryGroup> countryGroups = asList(new CountryGroup(), new CountryGroup());
    when(countryGroupRepository.findAll()).thenReturn(countryGroups);
    List<CountryGroupViewModel> countryGroupViewModels = countryGroupService.getAll();
    verify(countryGroupRepository).findAll();
    assertTrue(CollectionUtils.isNotEmpty(countryGroupViewModels));
  }

  @Test
  public void testGetEmptyResults() {
    when(countryGroupRepository.findAll()).thenReturn(Collections.emptyList());
    List<CountryGroupViewModel> countryGroupViewModels = countryGroupService.getAll();
    verify(countryGroupRepository).findAll();
    assertNotNull(countryGroupViewModels);
    assertFalse(CollectionUtils.isNotEmpty(countryGroupViewModels));
  }

  @Test
  public void testGetNullElementInNonEmptyResultSet() {
    List<CountryGroup> countryGroups = asList(new CountryGroup(), null);
    when(countryGroupRepository.findAll()).thenReturn(countryGroups);
    List<CountryGroupViewModel> countryGroupViewModels = countryGroupService.getAll();
    verify(countryGroupRepository).findAll();
    assertNotNull(countryGroupViewModels);
    assertTrue(CollectionUtils.isNotEmpty(countryGroupViewModels));
    assertEquals(1, countryGroupViewModels.size());
  }

  @Test
  public void testGetNullResult() {
    when(countryGroupRepository.findAll()).thenReturn(null);
    List<CountryGroupViewModel> countryGroupViewModels = countryGroupService.getAll();
    verify(countryGroupRepository).findAll();
    assertNotNull(countryGroupViewModels);
    assertFalse(CollectionUtils.isNotEmpty(countryGroupViewModels));
  }
}
