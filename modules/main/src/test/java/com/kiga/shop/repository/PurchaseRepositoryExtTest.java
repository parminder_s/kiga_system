package com.kiga.shop.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.shop.domain.Purchase;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

public class PurchaseRepositoryExtTest {
  @Test
  public void testByCustomerWithNoPurchase() {
    PurchaseRepository purchaseRepository = mock(PurchaseRepository.class);
    when(purchaseRepository.findAll(any(Predicate.class), any(OrderSpecifier.class)))
      .thenReturn(Collections.emptyList());
    Optional<Purchase> optPurchase =
      new PurchaseRepositoryExt(purchaseRepository).findNewByCustomer(5);

    assertFalse(optPurchase.isPresent());
  }

  @Test
  public void testByCustomerWithPurchases() {
    Purchase purchase1 = new Purchase();
    Purchase purchase2 = new Purchase();
    PurchaseRepository purchaseRepository = mock(PurchaseRepository.class);
    when(purchaseRepository.findAll(any(Predicate.class), any(OrderSpecifier.class)))
      .thenReturn(Arrays.asList(purchase1, purchase2));

    Optional<Purchase> optPurchase =
      new PurchaseRepositoryExt(purchaseRepository).findNewByCustomer(5);

    assertEquals(purchase1, optPurchase.get());
  }

  @Test
  public void testFindBySession() {
    Purchase purchase = new Purchase();
    PurchaseRepository purchaseRepository = mock(PurchaseRepository.class);
    when(purchaseRepository.findOne(any(Predicate.class))).thenReturn(purchase);

    PurchaseRepositoryExt repositoryExt = new PurchaseRepositoryExt(purchaseRepository);
    repositoryExt.findBySession("foo-sessionk");

    assertEquals(purchase, repositoryExt.findBySession("foo-session").get());
  }

}
