package com.kiga.shop.backend.purchase.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.shop.backend.purchase.PurchasesFinder;
import com.kiga.shop.domain.Purchase;
import com.mysema.query.types.expr.BooleanExpression;
import java.util.Collections;
import java.util.List;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public class PurchasesFinderTest {
  @Test
  public void testExecution() {
    QueryDslPredicateExecutor executor = mock(QueryDslPredicateExecutor.class);
    List<Purchase> purchases = Collections.emptyList();
    when(executor.findAll(any(BooleanExpression.class))).thenReturn(purchases);

    assertThat(new PurchasesFinder(executor).find(true)).isEqualTo(purchases);
  }

  @Test
  public void testExpressionForNotHideFinished() {
    assertExpression(
        false,
        "purchase.status in [Ordered, ShippingInformationCreated, CsvExported, CsvFinished, ReadyForShipping, Shipped, CreditVoucherPending, CreditVoucherFinalized, Cancelled]");
  }

  @Test
  public void testExpressionForHideFinished() {
    assertExpression(
        true,
        "purchase.status in [Ordered, ShippingInformationCreated, CsvExported, CsvFinished, ReadyForShipping, CreditVoucherPending]");
  }

  public void assertExpression(boolean hideFinished, String expression) {
    QueryDslPredicateExecutor executor = mock(QueryDslPredicateExecutor.class);
    ArgumentCaptor<BooleanExpression> captor = ArgumentCaptor.forClass(BooleanExpression.class);
    when(executor.findAll(captor.capture())).thenReturn(Collections.emptyList());

    new PurchasesFinder(executor).find(hideFinished);

    assertThat(captor.getValue().toString()).isEqualToIgnoringNewLines(expression);
  }
}
