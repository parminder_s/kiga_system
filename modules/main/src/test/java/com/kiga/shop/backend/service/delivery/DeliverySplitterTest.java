package com.kiga.shop.backend.service.delivery;

import static java.util.Collections.singletonList;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.shop.domain.PurchaseDelivery;
import com.kiga.spec.Fetcher;
import java.util.List;
import org.junit.Test;

public class DeliverySplitterTest {
  @Test
  public void testDefault() {
    PurchaseDeliveryMapper mapper = mock(PurchaseDeliveryMapper.class);
    DeliverySplitterChecker checker = mock(DeliverySplitterChecker.class);
    Fetcher fetcher = mock(Fetcher.class);

    PurchaseDelivery delivery = new PurchaseDelivery();
    when(fetcher.find(1L)).thenReturn(delivery);

    when(checker.isValid(eq(delivery), any())).thenReturn(true);
    PurchaseDelivery newDelivery = new PurchaseDelivery();
    when(mapper.purchaseDelivery(delivery)).thenReturn(newDelivery);

    List<PurchaseProductIdAmount> purchaseProductIdAmounts =
        singletonList(new PurchaseProductIdAmount(1, 1));
    DeliverySplitterExecutor splitter = mock(DeliverySplitterExecutor.class);
    DeliveryPersister persister = mock(DeliveryPersister.class);
    new DeliverySplitter(mapper, checker, splitter, fetcher, persister)
        .splitDelivery(1, purchaseProductIdAmounts);

    verify(splitter).split(delivery, newDelivery, purchaseProductIdAmounts);
    verify(persister).persist(delivery, newDelivery);
  }
}
