package com.kiga.shop.backend.purchase;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.mail.Mailer;
import com.kiga.shop.domain.Purchase;
import java.util.function.UnaryOperator;
import org.junit.Test;

public class PurchaseCancellerTest {
  @Test
  public void test() {
    Mailer mailer = mock(Mailer.class);
    PurchaseCanceller canceller = new PurchaseCanceller(mailer, "shop@kigaportal.com");
    Purchase purchase = Purchase.builder().gender("male").orderNr(1234L).build();
    purchase.setId(3L);

    canceller.cancel(purchase);

    verify(mailer).send(eq("shop"), eq("cancelled"), eq("3"), any(UnaryOperator.class));
  }
}
