package com.kiga.shop.backend;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


import com.kiga.shop.DocumentHelper;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;


import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by faxxe on 1/4/17.
 */
public class DocumentHelperTest {

  public DocumentHelper documentHelper;

  @Test
  @Ignore
  public void testSingleFile() {

    PDFMergerUtility pdfMergerUtility = Mockito.mock(PDFMergerUtility.class);
    documentHelper = new DocumentHelper();
    List<File> fileList = Arrays.asList(
      new File("test")
    );

    documentHelper.mergeFiles(fileList, "fileName");

    verify(pdfMergerUtility, times(1)).addSource(fileList.get(0));
    try {
      verify(pdfMergerUtility, times(1)).mergeDocuments();
    } catch (IOException | COSVisitorException exception) {
      exception.printStackTrace();
    }
  }

  @Test
  @Ignore
  public void testMultipleFiles() {

    PDFMergerUtility pdfMergerUtility = Mockito.mock(PDFMergerUtility.class);
    documentHelper = new DocumentHelper();
    List<File> fileList = Arrays.asList(
      new File("test"),
      new File("bla"),
      new File("wupp")
    );

    documentHelper.mergeFiles(fileList, "fileName");

    verify(pdfMergerUtility, times(1)).addSource(fileList.get(0));
    verify(pdfMergerUtility, times(1)).addSource(fileList.get(1));
    verify(pdfMergerUtility, times(1)).addSource(fileList.get(2));
    try {
      verify(pdfMergerUtility, times(1)).mergeDocuments();
    } catch (IOException | COSVisitorException exception) {
      exception.printStackTrace();
    }
  }


}
