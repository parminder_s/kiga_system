package com.kiga.shop.web.converter;

import static org.junit.Assert.assertEquals;

import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryGroup;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.web.response.CountryShopViewModel;
import org.junit.Before;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;

/**
 * @author bbs
 * @since 2/18/17.
 */
public class CountryShopToViewModelConverterTest {
  private CountryShopToViewModelConverter countryShopToViewModelConverter;

  /**
   * Prepare before tests.
   */
  @Before
  public void before() {
    countryShopToViewModelConverter = new
      CountryShopToViewModelConverter(
      new ModelMapper());
  }

  @Test
  public void testConvertEntity() {
    CountryShop countryShop = new CountryShop();
    countryShop.setId(1L);
    countryShop.setMaxPurchasePrice(BigDecimal.valueOf(1000));
    countryShop.setVatRateShipping(BigDecimal.valueOf(23));
    countryShop.setDaysToPayInvoice(7);
    countryShop.setAccountNr("123456");
    countryShop.setBankName("BANK");
    countryShop.setBic("QQQ");
    countryShop.setFreeShippingLimit(BigDecimal.valueOf(100));
    countryShop.setPriceShippingNet(BigDecimal.valueOf(15));

    Country country = new Country();
    country.setVat(BigDecimal.valueOf(0.23));
    country.setCode("ABC");
    country.setCode3("DEF");
    country.setCurrency("GHI");

    CountryGroup countryGroup = new CountryGroup();
    countryGroup.setTitle("TITLE");
    country.setCountryGroup(countryGroup);
    country.setCountryShop(countryShop);
    countryShop.setCountry(country);

    CountryShopViewModel viewModel = countryShopToViewModelConverter.convertToResponse(countryShop);

    assertEquals(countryShop.getId().longValue(), viewModel.getId());
    assertEquals(countryShop.getAccountNr(), viewModel.getAccountNr());
    assertEquals(countryShop.getBankName(), viewModel.getBankName());
    assertEquals(countryShop.getBic(), viewModel.getBic());
    assertEquals(countryShop.getDaysToPayInvoice(), viewModel.getDaysToPayInvoice());
    assertEquals(countryShop.getFreeShippingLimit(), viewModel.getFreeShippingLimit());
    assertEquals(countryShop.getIban(), viewModel.getIban());
    assertEquals(countryShop.getMaxPurchasePrice(), viewModel.getMaxPurchasePrice());
    assertEquals(countryShop.getPriceShippingNet(), viewModel.getPriceShippingNet());
    assertEquals(countryShop.getVatRateShipping(), viewModel.getVatRateShipping());
  }
}
