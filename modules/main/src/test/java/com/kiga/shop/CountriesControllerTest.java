package com.kiga.shop;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.shop.service.CountryService;
import com.kiga.shop.web.response.CountryViewModel;
import com.kiga.web.service.GeoIpService;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author bbs
 * @since 10/16/16.
 */
public class CountriesControllerTest {
  private CountryService countryService = mock(CountryService.class);
  private GeoIpService geoIpService = mock(GeoIpService.class);
  private CountriesController countriesController;

  @Before
  public void prepareTests() {
    countriesController = new CountriesController(countryService, geoIpService);
  }

  @Test
  public void testList() {
    when(countryService.getAll())
      .thenReturn(asList(new CountryViewModel(), new CountryViewModel()));
    List<CountryViewModel> countryViewModelList = countriesController.list();
    assertTrue(CollectionUtils.isNotEmpty(countryViewModelList));
    assertEquals(2, countryViewModelList.size());
    verify(countryService).getAll();
  }
}
