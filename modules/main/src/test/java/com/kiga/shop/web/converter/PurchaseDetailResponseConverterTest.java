package com.kiga.shop.web.converter;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.ShippingPartner;
import com.kiga.shop.repository.InvoiceVersionRepository;
import com.kiga.shop.repository.PurchaseWorkflowRepository;
import com.kiga.shop.service.ShopUrlGenerator;
import com.kiga.shop.test.PurchaseBuilder;
import com.kiga.shop.web.response.PurchaseDetailResponse;
import com.kiga.util.InstantCreator;
import org.junit.Test;

public class PurchaseDetailResponseConverterTest {
  @Test
  public void testDefault() {
    Purchase purchase = PurchaseBuilder.get().build();

    ShopUrlGenerator shopUrlGenerator = mock(ShopUrlGenerator.class);
    when(shopUrlGenerator.generateInvoiceUrl(purchase)).thenReturn("invoiceUrl");
    when(shopUrlGenerator.generateCreditVoucherUrl(purchase)).thenReturn("voucherUrl");

    InvoiceVersionRepository invoiceVersionRepository = mock(InvoiceVersionRepository.class);
    PurchaseWorkflowRepository purchaseWorkflowRepository = mock(PurchaseWorkflowRepository.class);

    PurchaseDetailResponse response =
        new PurchaseDetailResponseConverter(
                shopUrlGenerator, invoiceVersionRepository, purchaseWorkflowRepository)
            .convert(purchase);

    assertThat(
        response,
        sameBeanAs(
                PurchaseDetailResponse.builder()
                    .firstName("Hugo")
                    .lastName("Victor")
                    .street("In der Tiefe")
                    .streetNumber("5")
                    .floorNumber("1")
                    .doorNumber("26")
                    .zip("10234")
                    .city("The Shire")
                    .country("Auenland")
                    .addressName1("In der Tiefe")
                    .priceNetTotal("5.00")
                    .priceGrossTotal("6.00")
                    .status("Shipped")
                    .shippingNoteNumber("Ship01")
                    .shippingPartner(ShippingPartner.DHL)
                    .orderNr("45")
                    .orderDate("2017-12-01T00:00")
                    .invoiceNumber("R18011")
                    .invoiceDate(InstantCreator.create(2018, 1, 1))
                    .paymentMethod(PaymentMethod.INVOICE)
                    .memberId("5")
                    .memberEmail("hugo.victor@kigaportal.com")
                    .phoneNr("+43506 1200 8421")
                    .billAddresssName1("The Dark Tower")
                    .billAddresssName2("5-1")
                    .billStreet("Dark Tower Street")
                    .billStreetnumber("5")
                    .billFloornumber("5")
                    .billDoornumber("6")
                    .billZip("12345")
                    .billCity("Mordor")
                    .billCountry("")
                    .invoiceUrl("invoiceUrl")
                    .voucherUrl("voucherUrl")
                    .build())
            .ignoring("purchaseItems")
            .ignoring("invoices")
            .ignoring("workflow"));
    verify(invoiceVersionRepository).findAllByPurchase(purchase);
    verify(purchaseWorkflowRepository).findAllByPurchase(purchase);
  }
}
