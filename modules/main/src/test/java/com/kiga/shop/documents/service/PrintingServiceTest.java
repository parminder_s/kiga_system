package com.kiga.shop.documents.service;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneak;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.dunning.domain.DunningLevel;
import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.main.MainConfig;
import com.kiga.security.domain.Member;
import com.kiga.shop.documents.model.DunningDocumentModel;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.shop.service.ProductHelper;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;

/** Created by robert on 16.09.17. */
@Service
public class PrintingServiceTest {

  public static final String INVOICE_NO = "123456";

  @Test
  public void createDunningDocumentTest() {
    CountryShop countryShop = new CountryShop();
    countryShop.setDaysToPayInvoice(5);
    countryShop.setIban("AT12-4567-8888-9999");
    countryShop.setBic("STK1231");
    countryShop.setBankName("UTZBank");
    countryShop.setMinDeliveryDays(1L);
    countryShop.setMaxDeliveryDays(5L);

    Country country = new Country();
    country.setCountryShop(countryShop);

    Member member = new Member();
    member.setCustomerId("AT-1");

    Purchase purchase = new Purchase();
    purchase.setFirstname("Homer");
    purchase.setLastname("Simpson");
    purchase.setGender("male");
    purchase.setShippingAddressName1("Homer");
    purchase.setShippingAddressName1("Simpson");
    purchase.setPriceNetTotal(new BigDecimal("10"));
    purchase.setVatRateShipping(new BigDecimal("0.45"));
    purchase.setVatShipping(new BigDecimal("3"));
    purchase.setPriceShippingNet(new BigDecimal("7"));
    purchase.setCountry(country);
    purchase.setInvoiceDate(Instant.now());
    purchase.setOrderDate(Instant.now());
    purchase.setOrderNr(15L);
    purchase.setInvoiceNr(INVOICE_NO);
    Product pro1 = new Product();
    pro1.setId(1L);
    purchase.setCustomer(member);
    purchase.setPriceGrossTotal(new BigDecimal("20"));
    purchase.setCurrency("EUR");

    Product pro2 = new Product();
    pro2.setId(2L);

    PurchaseItem p1 = new PurchaseItem();
    p1.setTitle("Product1");
    p1.setAmount(2);
    p1.setCode("p-1");
    p1.setProduct(pro1);
    p1.setPriceNetSingle(new BigDecimal("10"));
    p1.setVatRate(new BigDecimal("0.2"));
    p1.setPriceGrossTotal(new BigDecimal("12"));
    p1.setWeightGramTotal(200);
    p1.setWeightGramSingle(100);
    p1.setVatTotal(new BigDecimal("10"));

    PurchaseItem p2 = new PurchaseItem();
    p2.setTitle("Product1");
    p2.setAmount(1);
    p2.setCode("p-1");
    p2.setProduct(pro2);
    p2.setPriceNetSingle(new BigDecimal("10"));
    p2.setVatRate(new BigDecimal("0.1"));
    p2.setPriceGrossTotal(new BigDecimal("12"));
    p2.setWeightGramTotal(100);
    p2.setWeightGramSingle(100);
    p2.setVatTotal(new BigDecimal("20"));

    purchase.setPurchaseItems(Arrays.asList(p1, p2));

    Map<Long, ProductDetail> productDetailMap = new HashMap<>();

    productDetailMap.put(
        1L,
        new ProductDetail(
            new BigDecimal("10"),
            new BigDecimal("1"),
            true,
            new BigDecimal("0.2"),
            new BigDecimal("0.2"),
            5,
            null,
            BigDecimal.ONE,
            null,
            country,
            pro1,
            null));

    productDetailMap.put(
        2L,
        new ProductDetail(
            new BigDecimal("10"),
            new BigDecimal("1"),
            true,
            new BigDecimal("0.2"),
            new BigDecimal("0.2"),
            5,
            null,
            BigDecimal.ONE,
            null,
            country,
            pro2,
            null));

    FreeMarkerConfigurationFactoryBean configuration = new FreeMarkerConfigurationFactoryBean();
    configuration.setTemplateLoaderPath(PurchaseItem.class.getResource("/templates").getPath());

    DunningLevel dunningLevel = new DunningLevel();
    dunningLevel.setDunningLevel(1);
    dunningLevel.setFeesAmount(BigDecimal.ONE);
    Invoice invoice = new Invoice();
    invoice.setDunningLevel(dunningLevel);
    invoice.setPaymentTransactions(Collections.emptyList());
    InvoiceRepository invoiceRepository = mock(InvoiceRepository.class);
    PurchaseRepository purchaseRepository = mock(PurchaseRepository.class);
    ProductHelper productHelper = mock(ProductHelper.class);

    when(invoiceRepository.findByInvoiceNumber(eq(INVOICE_NO))).thenReturn(invoice);
    when(purchaseRepository.findByInvoiceNr(eq(INVOICE_NO))).thenReturn(purchase);
    when(productHelper.getProductDetailsFromPurchaseAsJava(any())).thenReturn(productDetailMap);

    Map<String, Object> modelMap = new HashMap<>();
    modelMap.put(
        "model",
        new DunningDocumentModel(
            purchase, productHelper.getProductDetailsFromPurchaseAsJava(purchase), invoice));

    PrintingService printingService =
        sneak(
            () ->
                new PrintingService(
                    new MainConfig().freeMarkerConfiguration().createConfiguration()));
    assertNotNull(
        printingService.printDocument(
            "dunning-document-" + dunningLevel.getDunningLevel() + ".xml",
            modelMap,
            "dunning_document-" + INVOICE_NO));
  }
}
