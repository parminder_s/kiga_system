package com.kiga.shop.basket.web.service;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.main.locale.Locale;
import com.kiga.shop.basket.web.response.BasketProduct;
import com.kiga.shop.basket.web.response.BasketResponse;
import com.kiga.shop.basket.web.response.Shipping;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryLocale;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.domain.Voucher;
import com.kiga.shop.repository.CountryLocaleRepository;
import com.kiga.shop.service.ProductInfoCreator;
import com.kiga.shop.service.VatRateCalculator;
import com.kiga.shop.web.response.ProductInfo;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import org.junit.Test;

public class BasketMapperTest {
  @Test
  public void testtWithNormal() {
    Country country = new Country();
    country.setId(1L);
    country.setCode("at");
    country.setCountryShop(CountryShop.builder().minDeliveryDays(4L).maxDeliveryDays(7L).build());
    CountryLocale countryLocale = new CountryLocale();
    countryLocale.setTitle("Austria");

    CountryLocaleRepository countryLocaleRepository = mock(CountryLocaleRepository.class);
    when(countryLocaleRepository.findByCountryAndLocale(country, Locale.de))
        .thenReturn(countryLocale);

    Product product1 = new Product();
    product1.setId(23L);

    PurchaseItem purchaseItem =
        PurchaseItem.builder()
            .product(product1)
            .title("product1")
            .amount(3)
            .priceNetSingle(new BigDecimal("20.000"))
            .vatRate(new BigDecimal("0.100"))
            .priceGrossTotal(new BigDecimal("66.00"))
            .build();
    purchaseItem.setId(566L);

    Purchase purchase =
        Purchase.builder()
            .country(country)
            .locale(Locale.de)
            .priceShippingNet(new BigDecimal("50"))
            .vatShipping(new BigDecimal("10"))
            .priceGrossTotal(new BigDecimal("163.50"))
            .currency("EUR")
            .purchaseItems(Collections.singletonList(purchaseItem))
            .build();
    purchase.setId(2L);

    BasketResponse basketResponse =
        BasketResponse.builder()
            .id(2L)
            .totalWithoutDelivery(new BigDecimal("103.5"))
            .total(new BigDecimal("163.5"))
            .currency("EUR")
            .country("at")
            .products(
                Arrays.asList(
                    BasketProduct.builder()
                        .id(566L)
                        .productId(23L)
                        .title("product1")
                        .amount(3)
                        .price(new BigDecimal("22.00"))
                        .sum(new BigDecimal("66.00"))
                        .originalPrice(new BigDecimal("22.00"))
                        .originalSum(new BigDecimal("66.00"))
                        .hasDiscount(false)
                        .minDeliveryDays(4)
                        .maxDeliveryDays(7)
                        .build()))
            .shipping(Shipping.builder().cost(new BigDecimal("60.00")).to("Austria").build())
            .build();

    BasketMapper converter =
        new BasketMapper(countryLocaleRepository, new VatRateCalculator(), null);

    assertThat(basketResponse, sameBeanAs(converter.map(purchase)));
  }

  @Test
  public void testWithDiscounted() {
    Country country =
        Country.builder()
            .code("at")
            .countryShop(CountryShop.builder().minDeliveryDays(4L).maxDeliveryDays(7L).build())
            .build();
    CountryLocale countryLocale = new CountryLocale();
    countryLocale.setTitle("Austria");

    CountryLocaleRepository countryLocaleRepository = mock(CountryLocaleRepository.class);
    when(countryLocaleRepository.findByCountryAndLocale(country, Locale.de))
        .thenReturn(countryLocale);

    Product product = new Product();
    product.setId(23L);

    ProductInfoCreator productInfoCreator = mock(ProductInfoCreator.class);
    when(productInfoCreator.create(anyLong(), anyString()))
        .thenReturn(
            Optional.of(
                ProductInfo.builder()
                    .priceNet(new BigDecimal("40.910"))
                    .priceGross(new BigDecimal("45.00"))
                    .build()));

    PurchaseItem purchaseItem =
        PurchaseItem.builder()
            .product(product)
            .title("product1")
            .amount(3)
            .priceNetSingle(new BigDecimal("20.000"))
            .vatRate(new BigDecimal("0.100"))
            .priceGrossTotal(new BigDecimal("66.00"))
            .voucher(new Voucher())
            .build();
    purchaseItem.setId(566L);

    Purchase purchase =
        Purchase.builder()
            .country(country)
            .locale(Locale.de)
            .priceShippingNet(new BigDecimal("50"))
            .vatShipping(new BigDecimal("10"))
            .priceGrossTotal(new BigDecimal("163.50"))
            .currency("EUR")
            .purchaseItems(Collections.singletonList(purchaseItem))
            .build();
    purchase.setId(2L);

    BasketProduct basketProduct =
        BasketProduct.builder()
            .id(566L)
            .productId(23L)
            .title("product1")
            .amount(3)
            .price(new BigDecimal("22.00"))
            .sum(new BigDecimal("66.00"))
            .originalPrice(new BigDecimal("45.00"))
            .originalSum(new BigDecimal("135.00"))
            .hasDiscount(true)
            .minDeliveryDays(4)
            .maxDeliveryDays(7)
            .build();

    BasketMapper converter =
        new BasketMapper(countryLocaleRepository, new VatRateCalculator(), productInfoCreator);

    assertThat(converter.map(purchase).getProducts().get(0), sameBeanAs(basketProduct));
  }
}
