package com.kiga.shop.backend.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.main.exception.InvalidIdException;
import com.kiga.security.services.SecurityService;
import com.kiga.shop.backend.web.request.GetCountryRequest;
import com.kiga.shop.backend.web.request.UpdateCountryRequest;
import com.kiga.shop.service.CountryService;
import com.kiga.shop.web.response.CountryViewModel;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 * @author bbs
 * @since 3/14/17.
 */
public class CountryControllerTest {
  public static final long ID = 1L;
  private CountryService countryServiceMock = mock(CountryService.class);
  private CountryController countryController;
  private SecurityService securityServiceMock = mock(SecurityService.class);

  /**
   * Prepare tests.
   */
  @Before
  public void before() {
    countryController = new CountryController(countryServiceMock, securityServiceMock);
    CountryViewModel countryViewModel1 = new CountryViewModel();
    CountryViewModel countryViewModel = new CountryViewModel();
    countryViewModel.setVat(BigDecimal.ONE);
    when(countryServiceMock.getAll()).thenReturn(Collections.singletonList(countryViewModel));
    when(countryServiceMock.getAllImportant()).thenReturn(Collections.singletonList(
      countryViewModel1));
    when(countryServiceMock.getAllOther()).thenReturn(Collections.singletonList(
      countryViewModel));
  }

  @Test
  public void testList() {
    List<CountryViewModel> list = countryController.list();
    assertNotNull(list);
    assertEquals(1, list.size());
    assertEquals(BigDecimal.valueOf(100), list.get(0).getVat());
  }

  @Test
  public void testListImportant() {
    List<CountryViewModel> list = countryController.listImportant();
    assertNotNull(list);
    assertEquals(1, list.size());
    assertNull(list.get(0).getVat());
  }

  @Test
  public void testListOther() {
    List<CountryViewModel> list = countryController.listOther();
    assertNotNull(list);
    assertEquals(1, list.size());
    assertEquals(BigDecimal.valueOf(100), list.get(0).getVat());
  }

  @Test
  public void testGet() throws InvalidIdException {
    GetCountryRequest getCountryRequest = new GetCountryRequest();
    getCountryRequest.setId(ID);
    CountryViewModel expectedCountryViewModel = new CountryViewModel();
    when(countryServiceMock.getById(ID)).thenReturn(expectedCountryViewModel);
    CountryViewModel returnedCountryViewModel =
      countryController.get(getCountryRequest);
    assertSame(expectedCountryViewModel, returnedCountryViewModel);
  }

  @Test(expected = InvalidIdException.class)
  public void testGetWrongId() throws InvalidIdException {
    GetCountryRequest getCountryRequest = new GetCountryRequest();
    countryController.get(getCountryRequest);
  }

  @Test
  public void testUpdate() throws InvalidIdException {
    UpdateCountryRequest updateCountryRequest = new UpdateCountryRequest();
    updateCountryRequest.setId(ID);
    countryController.update(updateCountryRequest);
    verify(countryServiceMock).update(updateCountryRequest);
  }

  @Test(expected = InvalidIdException.class)
  public void testUpdateWrongId() throws InvalidIdException {
    UpdateCountryRequest updateCountryRequest = new UpdateCountryRequest();
    countryController.update(updateCountryRequest);
  }
}
