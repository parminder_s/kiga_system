package com.kiga.shop.basket.service.price;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.service.VatRateCalculator;
import java.math.BigDecimal;
import org.junit.Test;

public class ShippingCostsSetterTest {
  @Test
  public void calcWithLimit0() {
    CountryShop countryShop =
        CountryShop.builder()
            .vatRateShipping(new BigDecimal("0.200"))
            .priceShippingNet(new BigDecimal("10"))
            .freeShippingLimit(BigDecimal.ZERO)
            .build();

    assertShippingCosts(countryShop, "10.00", "2.00", "1000", "0.200");
  }

  @Test
  public void calcWithoutLimit() {
    CountryShop countryShop =
        CountryShop.builder()
            .vatRateShipping(new BigDecimal("0.200"))
            .priceShippingNet(new BigDecimal("10"))
            .build();

    assertShippingCosts(countryShop, "10.00", "2.00", "5000", "0.200");
  }

  @Test
  public void calcWithExactLimit() {
    CountryShop countryShop =
        CountryShop.builder()
            .vatRateShipping(new BigDecimal("0.200"))
            .priceShippingNet(new BigDecimal("10"))
            .freeShippingLimit(new BigDecimal("100.00"))
            .build();

    assertShippingCosts(countryShop, "10.00", "2.00", "100.00", "0.200");
  }

  @Test
  public void calcWithFreeShippingLimit() {
    CountryShop countryShop =
        CountryShop.builder()
            .vatRateShipping(new BigDecimal("0.200"))
            .priceShippingNet(new BigDecimal("10"))
            .freeShippingLimit(new BigDecimal("99.99"))
            .build();

    assertShippingCosts(countryShop, "0", "0", "100.00", "0.200");
  }

  @Test
  public void calcBelowFreeShippingLimit() {
    CountryShop countryShop =
        CountryShop.builder()
            .vatRateShipping(new BigDecimal("0.200"))
            .priceShippingNet(new BigDecimal("10"))
            .freeShippingLimit(new BigDecimal("100.00"))
            .build();

    assertShippingCosts(countryShop, "10.00", "2.00", "99.99", "0.200");
  }

  @Test
  public void calcWith3DigitVatRate() {
    CountryShop countryShop =
        CountryShop.builder()
            .vatRateShipping(new BigDecimal("0.077"))
            .priceShippingNet(new BigDecimal("10"))
            .freeShippingLimit(new BigDecimal("100.00"))
            .build();

    assertShippingCosts(countryShop, "10.00", "0.77", "0", "0.077");
  }

  private void assertShippingCosts(
      CountryShop countryShop,
      String net,
      String vat,
      String priceGrossTotal,
      String vatRateShipping) {
    Purchase purchase =
        Purchase.builder()
            .country(Country.builder().countryShop(countryShop).build())
            .purchaseItems(
                singletonList(
                    PurchaseItem.builder()
                        .priceGrossTotal(new BigDecimal(priceGrossTotal))
                        .build()))
            .build();

    new ShippingCostsSetter(new VatRateCalculator(), new PriceGrossWithoutShippingCalcer())
        .setCosts(purchase);

    assertEquals(new BigDecimal(net), purchase.getPriceShippingNet());
    assertEquals(new BigDecimal(vat), purchase.getVatShipping());
    assertEquals(new BigDecimal(vatRateShipping), purchase.getVatRateShipping());
  }
}
