package com.kiga.shop.converter;

import static com.kiga.shop.domain.ShippingStatus.CsvExported;
import static com.kiga.shop.domain.ShippingStatus.CsvFinished;
import static com.kiga.shop.domain.ShippingStatus.NONE;
import static com.kiga.shop.domain.ShippingStatus.ReadyForShipping;
import static com.kiga.shop.domain.ShippingStatus.Shipped;
import static com.kiga.shop.domain.ShippingStatus.ShippingInformationCreated;
import static org.assertj.core.api.Java6Assertions.assertThat;

import com.kiga.shop.domain.ShippingStatus;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;

public class ShippingStatusConverterTest {

  @Test
  public void convertToDatabaseColumnNull() {
    assertThat(new ShippingStatusConverter().convertToDatabaseColumn(null)).isNull();
  }

  @Test
  public void testAllValues() {
    List<ShippingStatus> status =
        Arrays.asList(
            NONE, ShippingInformationCreated, CsvExported, CsvFinished, ReadyForShipping, Shipped);
    List<String> strings =
        Arrays.asList(
            "none",
            "shippingInformationCreated",
            "csvExported",
            "csvFinished",
            "readyForShipping",
            "shipped");
    ShippingStatusConverter converter = new ShippingStatusConverter();

    for (int i = 0; i < status.size(); i++) {
      assertThat(converter.convertToDatabaseColumn(status.get(i))).isEqualTo(strings.get(i));
      assertThat(converter.convertToEntityAttribute(strings.get(i))).isEqualTo(status.get(i));
    }
  }

  @Test
  public void convertToEntityAttributeNul() {
    assertThat(new ShippingStatusConverter().convertToEntityAttribute(null)).isNull();
  }
}
