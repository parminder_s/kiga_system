package com.kiga.shop.web.response;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanHashCode;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanToString;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * @author bbs
 * @since 2/18/17.
 */
public class CountryShopViewModelTest {
  @Test
  public void testBean() {
    assertThat(CountryShopViewModel.class,
      allOf(hasValidGettersAndSetters(), hasValidBeanHashCode(), hasValidBeanToString()));
  }
}
