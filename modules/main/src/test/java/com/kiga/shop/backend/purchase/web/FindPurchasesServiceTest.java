package com.kiga.shop.backend.purchase.web;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.shop.backend.purchase.PurchasesFinder;
import com.kiga.shop.domain.Purchase;
import java.util.Collections;
import org.junit.Test;

public class FindPurchasesServiceTest {
  @Test
  public void test() {
    PurchasesFinder finder = mock(PurchasesFinder.class);
    Purchase purchase = new Purchase();
    when(finder.find(true)).thenReturn(Collections.singletonList(purchase));
    PurchaseResponseConverter converter = mock(PurchaseResponseConverter.class);

    new FindPurchasesService(finder, converter).findPurchases(true);

    verify(finder).find(true);
    verify(converter).convertToResponse(purchase);
  }
}
