package com.kiga.shop.service;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.shop.backend.web.request.UpdateCountryRequest;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.repository.CountryLocaleRepository;
import com.kiga.shop.repository.CountryRepository;
import com.kiga.shop.web.converter.CountryToDropdownViewModelConverter;
import com.kiga.shop.web.converter.CountryToViewModelConverter;
import com.kiga.shop.web.response.CountryDropdownViewModel;
import com.kiga.shop.web.response.CountryViewModel;
import com.mysema.query.types.Predicate;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

/**
 * @author bbs
 * @since 10/16/16.
 */
public class CountryServiceTest {
  private static final long COUNTRY_ID = 1L;
  private static final String COUNTRY_CODE = "ABC";
  private static final String CURRENCY = "USD";
  private static final String IBAN = "AT601250886507944314";
  private static final String BIC = "ALPEAT22";
  private static final String BANK_NAME = "ALPENBANK AG";
  private static final int DAYS_TO_PAY_INVOICE = 7;
  private static final String ACCOUNT_NR = "60 1250 8865 0794 4314";
  private static final BigDecimal MAX_PURCHASE_PRICE = BigDecimal.valueOf(1000);
  private static final BigDecimal PRICE_SHIPPING_NET = BigDecimal.valueOf(15);
  private static final BigDecimal VAT_RATE_SHIPPING = BigDecimal.valueOf(0.7);
  private static final BigDecimal FREE_SHIPPING_LIMIT = BigDecimal.valueOf(100);
  private CountryRepository countryRepository = mock(CountryRepository.class);
  private CountryToViewModelConverter countryToViewModelConverter = mock(
    CountryToViewModelConverter.class);
  private CountryToDropdownViewModelConverter countryToDropdownViewModelConverter = mock(
    CountryToDropdownViewModelConverter.class);
  private CountryService countryService;

  @Before
  public void prepareTests() {
    countryService = new CountryService(countryRepository, countryToViewModelConverter,
      countryToDropdownViewModelConverter, mock(CountryLocaleRepository.class));
  }

  @Test
  public void testGetAll() {
    when(countryRepository.findAll(any(Sort.class)))
      .thenReturn(asList(new Country(), new Country()));
    List<CountryViewModel> countryViewModels = countryService.getAll();
    assertTrue(CollectionUtils.isNotEmpty(countryViewModels));
    verify(countryRepository).findAll(any(Sort.class));
  }

  @Test
  public void testGetAllImportant() {
    countryService.getAllImportant();
    ArgumentCaptor<Predicate> predicateArgumentCaptor = ArgumentCaptor.forClass(Predicate.class);
    verify(countryRepository).findAll(predicateArgumentCaptor.capture(), any(Sort.class));
    Predicate value = predicateArgumentCaptor.getValue();
    assertTrue(value.toString().startsWith("country.id in com.mysema.query.DefaultQueryMetadata"));
  }

  @Test
  public void testGetAllOther() {
    countryService.getAllOther();
    ArgumentCaptor<Predicate> predicateArgumentCaptor = ArgumentCaptor.forClass(Predicate.class);
    verify(countryRepository).findAll(predicateArgumentCaptor.capture(), any(Sort.class));
    Predicate value = predicateArgumentCaptor.getValue();
    assertTrue(
      value.toString().startsWith("country.id not in com.mysema.query.DefaultQueryMetadata"));
  }

  @Test
  public void testGetEmptyResults() {
    when(countryRepository.findAll(any(Sort.class))).thenReturn(Collections.emptyList());
    List<CountryViewModel> countryViewModels = countryService.getAll();
    assertTrue(CollectionUtils.isEmpty(countryViewModels));
    assertNotNull(countryViewModels);
    verify(countryRepository).findAll(any(Sort.class));
  }

  @Test
  public void testGetNullResult() {
    when(countryRepository.findAll(any(Sort.class))).thenReturn(null);
    List<CountryViewModel> countryViewModels = countryService.getAll();
    assertTrue(CollectionUtils.isEmpty(countryViewModels));
    assertNotNull(countryViewModels);
    verify(countryRepository).findAll(any(Sort.class));
  }

  @Test
  public void testGetAllForDropdown() {
    when(countryRepository.findAll()).thenReturn(Collections.singletonList(new Country()));
    CountryDropdownViewModel countryDropdownViewModel = new CountryDropdownViewModel();
    when(countryToDropdownViewModelConverter.convertToResponse(any(Country.class)))
      .thenReturn(countryDropdownViewModel);
    List<CountryDropdownViewModel> allForDropdown = countryService.getAllForDropdown();
    assertNotNull(allForDropdown);
    assertEquals(1, allForDropdown.size());
    assertSame(countryDropdownViewModel, allForDropdown.get(0));
  }

  @Test
  public void testGetAllForDropdownNoCountriesFound() {
    when(countryRepository.findAll()).thenReturn(null);
    CountryDropdownViewModel countryDropdownViewModel = new CountryDropdownViewModel();
    when(countryToDropdownViewModelConverter.convertToResponse(any(Country.class)))
      .thenReturn(countryDropdownViewModel);
    List<CountryDropdownViewModel> allForDropdown = countryService.getAllForDropdown();
    assertNotNull(allForDropdown);
    assertTrue(CollectionUtils.isEmpty(allForDropdown));
  }

  @Test
  public void getEntityByCode() {
    countryService.getEntityByCode(COUNTRY_CODE);
    verify(countryRepository).findByCode(COUNTRY_CODE);
  }

  @Test
  public void getEntityById() {
    countryService.getEntityById(COUNTRY_ID);
    verify(countryRepository).findOne(COUNTRY_ID);
  }

  @Test
  public void testGetById() {
    Country country = new Country();
    when(countryRepository.findOne(COUNTRY_ID)).thenReturn(country);
    CountryViewModel expectedCountryViewModel = new CountryViewModel();
    when(countryToViewModelConverter.convertToResponse(country))
      .thenReturn(expectedCountryViewModel);
    CountryViewModel receivedCountryViewModel = countryService.getById(COUNTRY_ID);
    assertSame(expectedCountryViewModel, receivedCountryViewModel);
  }

  @Test
  public void testUpdate() {
    Country country = new Country();
    country.setCountryShop(new CountryShop());
    when(countryRepository.findOne(COUNTRY_ID)).thenReturn(country);

    UpdateCountryRequest updateCountryRequest = new UpdateCountryRequest();
    updateCountryRequest.setId(COUNTRY_ID);
    updateCountryRequest.setCode(COUNTRY_CODE);
    updateCountryRequest.setCode3(COUNTRY_CODE);
    updateCountryRequest.setCurrency(CURRENCY);
    updateCountryRequest.setVat(BigDecimal.valueOf(0.1));

    updateCountryRequest.setMaxPurchasePrice(MAX_PURCHASE_PRICE);
    updateCountryRequest.setPriceShippingNet(PRICE_SHIPPING_NET);
    updateCountryRequest.setVatRateShipping(VAT_RATE_SHIPPING);
    updateCountryRequest.setFreeShippingLimit(FREE_SHIPPING_LIMIT);
    updateCountryRequest.setIban(IBAN);
    updateCountryRequest.setBic(BIC);
    updateCountryRequest.setBankName(BANK_NAME);
    updateCountryRequest.setDaysToPayInvoice(DAYS_TO_PAY_INVOICE);
    updateCountryRequest.setAccountNr(ACCOUNT_NR);
    countryService.update(updateCountryRequest);

    ArgumentCaptor<Country> countryArgumentCaptor = ArgumentCaptor.forClass(Country.class);
    verify(countryRepository).save(countryArgumentCaptor.capture());

    Country capturedCountry = countryArgumentCaptor.getValue();
    assertEquals(updateCountryRequest.getCode(), capturedCountry.getCode());
    assertEquals(updateCountryRequest.getCode3(), capturedCountry.getCode3());
    assertEquals(updateCountryRequest.getCurrency(), capturedCountry.getCurrency());
    assertEquals(updateCountryRequest.getVat().multiply(BigDecimal.valueOf(100)),
      capturedCountry.getVat());

    assertEquals(updateCountryRequest.getMaxPurchasePrice(),
      capturedCountry.getCountryShop().getMaxPurchasePrice());
    assertEquals(updateCountryRequest.getPriceShippingNet(),
      capturedCountry.getCountryShop().getPriceShippingNet());
    assertEquals(updateCountryRequest.getVatRateShipping(),
      capturedCountry.getCountryShop().getVatRateShipping());
    assertEquals(updateCountryRequest.getFreeShippingLimit(),
      capturedCountry.getCountryShop().getFreeShippingLimit());
    assertEquals(updateCountryRequest.getIban(), capturedCountry.getCountryShop().getIban());
    assertEquals(updateCountryRequest.getBic(), capturedCountry.getCountryShop().getBic());
    assertEquals(updateCountryRequest.getBankName(),
      capturedCountry.getCountryShop().getBankName());
    assertEquals(updateCountryRequest.getDaysToPayInvoice(),
      capturedCountry.getCountryShop().getDaysToPayInvoice());
    assertEquals(updateCountryRequest.getAccountNr(),
      capturedCountry.getCountryShop().getAccountNr());
  }
}
