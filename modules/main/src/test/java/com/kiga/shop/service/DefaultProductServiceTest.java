package com.kiga.shop.service;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.shop.backend.exception.NonexistentEntityUpdateException;
import com.kiga.shop.backend.web.converter.ProductToViewModelConverter;
import com.kiga.shop.backend.web.response.ProductViewModel;
import com.kiga.shop.domain.Product;
import com.kiga.shop.repository.ProductRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;

import java.util.Collections;
import java.util.List;

/**
 * @author bbs
 * @since 10/7/16.
 */
public class DefaultProductServiceTest {
  private static final long MOCK_ID = 1;
  private static final int PAGE = 0;
  private static final int PAGE_SIZE = 10;
  private static final int WRONG_PAGE = -1;
  private static final int WRONG_PAGE_SIZE = -1;
  private static final String TITLE = "MOCK_TITLE";
  private static final String CODE = "MOCK_CODE";

  private DefaultProductService defaultProductService;
  private ProductRepository productRepository = mock(ProductRepository.class);
  private ProductToViewModelConverter productToViewModelConverter = new
    ProductToViewModelConverter();

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    Product product = new Product();
    product.setId(MOCK_ID);
    when(productRepository.findByActivatedIsTrue(any(PageRequest.class)))
      .thenReturn(asList(product, null));
    when(productRepository.findByActivatedTrueAndId(MOCK_ID)).thenReturn(product);
    when(productRepository.findByActivatedIsTrue()).thenReturn(Collections.singletonList(product));
    defaultProductService = new DefaultProductService(productRepository,
      productToViewModelConverter);
  }

  @Test
  public void testGetProducts() {
    List<ProductViewModel> productViewModelList = defaultProductService
      .getProducts();
    assertNotNull(productViewModelList);
    assertTrue(CollectionUtils.isNotEmpty(productViewModelList));
    assertEquals(1, productViewModelList.size());
  }

  @Test
  public void testGetProductsEmptyResults() {
    when(productRepository.findByActivatedIsTrue())
      .thenReturn(Collections.emptyList());
    List<ProductViewModel> productViewModelList = defaultProductService
      .getProducts();
    assertNotNull(productViewModelList);
    assertTrue(CollectionUtils.isEmpty(productViewModelList));
    assertEquals(0, productViewModelList.size());
  }

  @Test
  public void testGetProductsNullResult() {
    when(productRepository.findByActivatedIsTrue()).thenReturn(null);
    List<ProductViewModel> productViewModelList = defaultProductService
      .getProducts();
    assertNotNull(productViewModelList);
    assertTrue(CollectionUtils.isEmpty(productViewModelList));
    assertEquals(0, productViewModelList.size());
  }

  @Test
  public void testGetProduct() {
    ProductViewModel productViewModel = defaultProductService.getProductById(MOCK_ID);
    assertNotNull(productViewModel);
    assertNotNull(productViewModel.getId());
    assertEquals(MOCK_ID, productViewModel.getId().intValue());
  }

  @Test
  public void testGetProductNoResult() {
    when(productRepository.findByActivatedTrueAndId(MOCK_ID)).thenReturn(null);
    ProductViewModel productViewModel = defaultProductService.getProductById(MOCK_ID);
    assertNull(productViewModel);
  }

  @Test
  public void testSave() throws NonexistentEntityUpdateException {
    ProductViewModel productViewModel = new ProductViewModel();
    productViewModel.setId(7L);
    productViewModel.setVolumeLitre(BigDecimal.valueOf(9));
    productViewModel.setProductTitle(TITLE);
    productViewModel.setCode(CODE);
    productViewModel.setWeightGram(8);
    productViewModel.setActivated(true);
    Product product = new Product();
    product.setId(productViewModel.getId());
    when(productRepository.findByActivatedTrueAndId(productViewModel.getId())).thenReturn(product);
    defaultProductService.save(productViewModel);
    verify(productRepository).findByActivatedTrueAndId(productViewModel.getId());
    ArgumentCaptor<Product> productArgumentCaptor = ArgumentCaptor.forClass(Product.class);
    verify(productRepository).save(productArgumentCaptor.capture());

    Product capturedProduct = productArgumentCaptor.getValue();
    assertEquals(TITLE, capturedProduct.getProductTitle());
    assertEquals(CODE, capturedProduct.getCode());
    assertEquals(BigDecimal.valueOf(9), capturedProduct.getVolumeLitre());
    assertEquals(8, capturedProduct.getWeightGram());
    assertEquals(Long.valueOf(7), capturedProduct.getId());
    assertEquals(true, capturedProduct.getActivated());
  }

  @Test
  public void testSaveNewEntity() throws NonexistentEntityUpdateException {
    ProductViewModel productViewModel = new ProductViewModel();
    productViewModel.setVolumeLitre(BigDecimal.valueOf(9));
    productViewModel.setProductTitle(TITLE);
    productViewModel.setCode(CODE);
    productViewModel.setWeightGram(8);
    productViewModel.setActivated(true);

    defaultProductService.save(productViewModel);
    ArgumentCaptor<Product> productArgumentCaptor = ArgumentCaptor.forClass(Product.class);
    verify(productRepository).save(productArgumentCaptor.capture());

    Product capturedProduct = productArgumentCaptor.getValue();
    assertEquals(TITLE, capturedProduct.getProductTitle());
    assertEquals(CODE, capturedProduct.getCode());
    assertEquals(BigDecimal.valueOf(9), capturedProduct.getVolumeLitre());
    assertEquals(8, capturedProduct.getWeightGram());
    assertNull(capturedProduct.getId());
    assertEquals(true, capturedProduct.getActivated());
  }

  @Test(expected = NonexistentEntityUpdateException.class)
  @SuppressWarnings("unchecked")
  public void testUpdateEntityThatDoesNotExists() throws NonexistentEntityUpdateException {
    ProductViewModel productViewModel = new ProductViewModel();
    productViewModel.setId(7L);
    productViewModel.setVolumeLitre(BigDecimal.valueOf(9));
    productViewModel.setProductTitle(TITLE);
    productViewModel.setCode(CODE);
    productViewModel.setWeightGram(8);
    when(productRepository.findByActivatedTrueAndId(productViewModel.getId())).thenReturn(null);
    defaultProductService.save(productViewModel);
    verify(productRepository).findByActivatedTrueAndId(productViewModel.getId());
    ArgumentCaptor<Product> productArgumentCaptor = ArgumentCaptor.forClass(Product.class);
    verify(productRepository).save(productArgumentCaptor.capture());

    Product capturedProduct = productArgumentCaptor.getValue();
    assertEquals(TITLE, capturedProduct.getProductTitle());
    assertEquals(CODE, capturedProduct.getCode());
    assertEquals(BigDecimal.valueOf(9), capturedProduct.getVolumeLitre());
    assertEquals(8, capturedProduct.getWeightGram());
    assertNull(capturedProduct.getId());
  }

  @Test
  public void testSaveWithNullArgument() throws NonexistentEntityUpdateException {
    defaultProductService.save(null);
    verify(productRepository, never()).findByActivatedTrueAndId(anyLong());
  }

  @Test
  public void testDeactivate() throws NonexistentEntityUpdateException {
    long id = 11L;
    Product productIn = new Product();
    when(productRepository.findByActivatedTrueAndId(id)).thenReturn(productIn);
    defaultProductService.deactivate(id);
    verify(productRepository).findByActivatedTrueAndId(id);
    ArgumentCaptor<Product> productArgumentCaptor = ArgumentCaptor.forClass(Product.class);
    verify(productRepository).save(productArgumentCaptor.capture());

    Product productOut = productArgumentCaptor.getValue();
    assertEquals(productIn.getProductTitle(), productOut.getProductTitle());
    assertEquals(productIn.getCode(), productOut.getCode());
    assertEquals(productIn.getProductDetails(), productOut.getProductDetails());
    assertEquals(productIn.getVolumeLitre(), productOut.getVolumeLitre());
    assertEquals(productIn.getWeightGram(), productOut.getWeightGram());
    assertEquals(false, productOut.getActivated());
  }

  @Test(expected = NonexistentEntityUpdateException.class)
  public void testDeactivateNonexistentEntity() throws NonexistentEntityUpdateException {
    long id = 11L;
    when(productRepository.findByActivatedTrueAndId(id)).thenReturn(null);
    defaultProductService.deactivate(id);
  }
}
