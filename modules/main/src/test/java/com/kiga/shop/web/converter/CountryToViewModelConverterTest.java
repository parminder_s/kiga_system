package com.kiga.shop.web.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryLocale;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.repository.CountryLocaleRepository;
import com.kiga.shop.web.response.CountryViewModel;
import org.junit.Before;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;

/**
 * @author bbs
 * @since 10/16/16.
 */
public class CountryToViewModelConverterTest {
  private static final String CODE_3 = "ABC";
  private static final String CODE = "DEFG";
  private static final String CURRENCY = "USD";
  private static final String COUNTRY_LOCALE = "Austria";
  private static final long ID = 11L;
  public static final BigDecimal PRICE_SHIPPING_NET = BigDecimal.TEN;
  private static final BigDecimal VAT = PRICE_SHIPPING_NET;
  public static final BigDecimal MAX_PURCHASE_PRICE = BigDecimal.valueOf(1000);
  public static final BigDecimal VAT_RATE_SHIPPING = BigDecimal.valueOf(0.9);
  public static final BigDecimal FREE_SHIPPING_LIMIT = BigDecimal.valueOf(100);
  public static final String IBAN = "AT601250886507944314";
  public static final String BIC = "ALPEAT22";
  public static final String BANK_NAME = "ALPENBANK AG";
  public static final int DAYS_TO_PAY_INVOICE = 7;
  public static final String ACCOUNT_NR = "60 1250 8865 0794 4314";
  private CountryLocaleRepository repository = mock(CountryLocaleRepository.class);
  private CountryToViewModelConverter countryToViewModelConverter =
    new CountryToViewModelConverter(repository, new ModelMapper());

  /**
   * setup repository.
   */
  @Before
  public void setupRepository() {
    CountryLocale countryLocale = new CountryLocale();
    countryLocale.setTitle(COUNTRY_LOCALE);
    when(repository.findByCountryAndLocale(any(), any())).thenReturn(countryLocale);
  }

  @Test
  public void testConvertEntity() {
    Country country = new Country();
    country.setId(ID);
    country.setVat(VAT);
    country.setCode3(CODE_3);
    country.setCode(CODE);
    country.setCurrency(CURRENCY);
    country.setCountryShop(new CountryShop());
    country.getCountryShop().setPriceShippingNet(PRICE_SHIPPING_NET);

    country.getCountryShop().setMaxPurchasePrice(MAX_PURCHASE_PRICE);
    country.getCountryShop().setVatRateShipping(VAT_RATE_SHIPPING);
    country.getCountryShop().setFreeShippingLimit(FREE_SHIPPING_LIMIT);
    country.getCountryShop().setIban(IBAN);
    country.getCountryShop().setBic(BIC);
    country.getCountryShop().setBankName(BANK_NAME);
    country.getCountryShop().setDaysToPayInvoice(DAYS_TO_PAY_INVOICE);
    country.getCountryShop().setAccountNr(ACCOUNT_NR);

    CountryViewModel viewModel = countryToViewModelConverter.convertToResponse(country);

    assertNotNull(viewModel);
    assertEquals(ID, viewModel.getId());
    assertEquals(CODE, viewModel.getCode());
    assertEquals(CODE_3, viewModel.getCode3());
    assertEquals(CURRENCY, viewModel.getCurrency());
    assertEquals(VAT, viewModel.getVat());
    assertEquals(COUNTRY_LOCALE, viewModel.getTitle());
    assertEquals(PRICE_SHIPPING_NET, viewModel.getPriceShippingNet());

    assertEquals(MAX_PURCHASE_PRICE, viewModel.getMaxPurchasePrice());
    assertEquals(VAT_RATE_SHIPPING, viewModel.getVatRateShipping());
    assertEquals(FREE_SHIPPING_LIMIT, viewModel.getFreeShippingLimit());
    assertEquals(IBAN, viewModel.getIban());
    assertEquals(BIC, viewModel.getBic());
    assertEquals(BANK_NAME, viewModel.getBankName());
    assertEquals(DAYS_TO_PAY_INVOICE, viewModel.getDaysToPayInvoice().intValue());
    assertEquals(ACCOUNT_NR, viewModel.getAccountNr());
  }
}
