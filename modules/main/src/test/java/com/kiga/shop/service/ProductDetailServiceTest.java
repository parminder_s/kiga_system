package com.kiga.shop.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.shop.backend.exception.NonexistentEntityUpdateException;
import com.kiga.shop.backend.web.converter.ProductDetailsToViewModelConverter;
import com.kiga.shop.backend.web.response.ProductDetailViewModel;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shop.repository.CountryRepository;
import com.kiga.shop.repository.ProductDetailRepository;
import com.kiga.shop.repository.ProductRepository;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

/**
 * @author bbs
 * @since 10/8/16.
 */
public class ProductDetailServiceTest {
  private ProductDetailService productDetailService;
  private ProductDetailsToViewModelConverter productDetailsToViewModelConverter =
      mock(ProductDetailsToViewModelConverter.class);
  private ProductDetailRepository productDetailRepository = mock(ProductDetailRepository.class);
  private ProductRepository productRepository = mock(ProductRepository.class);
  private CountryRepository countryRepository = mock(CountryRepository.class);
  private ProductDetail productDetailMock = new ProductDetail();
  private List<ProductDetail> productDetailsListMock = Arrays.asList(productDetailMock);

  /** Prepare tests. */
  @Before
  public void prepareTests() {
    productDetailService =
        new ProductDetailService(
            productDetailsToViewModelConverter, productDetailRepository, productRepository, null);

    ProductDetailViewModel productDetailViewModel = new ProductDetailViewModel();

    when(productDetailRepository.findByProductId(anyLong(), any(Pageable.class)))
        .thenReturn(productDetailsListMock);
    when(productDetailRepository.findOne(1L)).thenReturn(productDetailMock);
    when(productDetailsToViewModelConverter.convertToResponse(productDetailMock))
        .thenReturn(productDetailViewModel);
    when(productDetailsToViewModelConverter.convertToResponse(productDetailMock))
        .thenReturn(productDetailViewModel);
  }

  @Test
  public void testGetList() {
    List<ProductDetailViewModel> productDetailViewModelList =
        productDetailService.getList(1, 0, 10);

    assertNotNull(productDetailViewModelList);
    assertEquals(1, productDetailViewModelList.size());
    assertNotNull(productDetailViewModelList.get(0));

    Pageable pageable = new PageRequest(0, 10);
    verify(productDetailRepository).findByProductId(1L, pageable);
    verify(productDetailsToViewModelConverter).convertToResponse(productDetailMock);
  }

  @Test
  public void testAddNewProductDetail() {
    ProductDetailUpdater productDetailUpdater = mock(ProductDetailUpdater.class);
    ProductDetailService service =
        ProductDetailService.builder().productDetailUpdater(productDetailUpdater).build();
    ProductDetailViewModel viewModel =
        ProductDetailViewModel.builder()
            .priceGross(BigDecimal.TEN)
            .vatProduct(BigDecimal.TEN)
            .build();

    service.save(viewModel);

    verify(productDetailUpdater).update(eq(viewModel), any());
  }

  @Test
  public void testUpdateExistingProductDetail() {
    ProductDetailUpdater productDetailUpdater = mock(ProductDetailUpdater.class);
    ProductDetailRepository repository = mock(ProductDetailRepository.class);
    ProductDetailService service =
        ProductDetailService.builder()
            .productDetailUpdater(productDetailUpdater)
            .productDetailRepository(repository)
            .build();
    ProductDetailViewModel viewModel =
        ProductDetailViewModel.builder()
            .id(1)
            .priceGross(BigDecimal.TEN)
            .vatProduct(BigDecimal.TEN)
            .build();
    ProductDetail productDetail = new ProductDetail();
    when(repository.findOne(anyLong())).thenReturn(productDetail);

    service.save(viewModel);

    verify(productDetailUpdater).update(viewModel, productDetail);
  }

  @Test
  public void testFindOne() {
    ProductDetailViewModel returner = productDetailService.findOne(1L);
    assertNotNull(returner);
    verify(productDetailRepository).findOne(1L);
    verify(productDetailsToViewModelConverter).convertToResponse(productDetailMock);
  }

  @Test
  public void testFindByProductId() {
    ProductRepository repository = mock(ProductRepository.class);
    Product product = new Product();
    product.setProductDetails(Arrays.asList());
    ProductDetailService service =
        ProductDetailService.builder().productRepository(repository).build();
    when(repository.findOne(any())).thenReturn(product);

    assertEquals(product.getProductDetails(), service.findByProductId(1));
  }

  @Test
  public void testDeactivate() throws NonexistentEntityUpdateException {
    ProductDetail productDetail = new ProductDetail();
    productDetail.setActivated(true);

    when(productDetailRepository.findOne(anyLong())).thenReturn(productDetail);
    productDetailService.toggleActivation(0, false);
    ArgumentCaptor<ProductDetail> productDetailArgumentCaptor =
        ArgumentCaptor.forClass(ProductDetail.class);
    verify(productDetailRepository).save(productDetailArgumentCaptor.capture());

    ProductDetail productDetailOut = productDetailArgumentCaptor.getValue();
    assertEquals(false, productDetailOut.getActivated());
  }
}
