package com.kiga.shop.basket.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.main.locale.Locale;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.repository.PurchaseItemRepository;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;

public class BasketServiceTest {
  @Test
  public void testAddProduct() {
    PurchaseItem purchaseItem = new PurchaseItem();
    purchaseItem.setAmount(5);
    Purchase purchase = new Purchase();
    BasketServiceExecutor executor = mockExecutor(purchase, purchaseItem);

    new BasketService(executor, null)
      .addProduct(Optional.empty(), 5, 10, Locale.en);

    assertEquals(15, purchaseItem.getAmount().longValue());
  }

  @Test
  public void testSetAmount() {
    PurchaseItem purchaseItem = new PurchaseItem();
    purchaseItem.setAmount(2);
    Purchase purchase = new Purchase();
    BasketServiceExecutor executor = mockExecutor(purchase, purchaseItem);

    new BasketService(executor, null)
      .setAmount(Optional.empty(), 5, 10, Locale.en);

    assertEquals(10, purchaseItem.getAmount().longValue());
  }

  @Test
  public void testRemoveProduct() {
    PurchaseItem purchaseItem1 = new PurchaseItem();
    Product product1 = new Product();
    product1.setId(1L);
    purchaseItem1.setProduct(product1);

    PurchaseItem purchaseItem2 = new PurchaseItem();
    Product product2 = new Product();
    product2.setId(2L);
    purchaseItem2.setProduct(product2);

    List<PurchaseItem> purchaseItems = new ArrayList<>();
    purchaseItems.add(purchaseItem1);
    purchaseItems.add(purchaseItem2);
    Purchase purchase = Purchase.builder().purchaseItems(purchaseItems).build();

    PurchaseItemRepository purchaseItemRepository = mock(PurchaseItemRepository.class);

    BasketServiceExecutor executor = mockExecutor(purchase, null);

    new BasketService(executor, purchaseItemRepository)
      .removeProduct(Optional.empty(), 2, Locale.en);

    assertEquals(1, purchase.getPurchaseItems().size());
    assertEquals(purchaseItem1, purchase.getPurchaseItems().get(0));
    verify(purchaseItemRepository).delete(purchaseItem2);
  }

  private BasketServiceExecutor mockExecutor(Purchase purchase, PurchaseItem purchaseItem) {
    BasketServiceExecutor executor = mock(BasketServiceExecutor.class);
    when(executor.execute(any(), anyLong(), any(), any())).thenAnswer(
      invocation -> {
        BiConsumer<Purchase, PurchaseItem> consumer = (BiConsumer) invocation.getArguments()[3];
        consumer.accept(purchase, purchaseItem);
        return purchase;
      }
    );
    return executor;
  }
}
