package com.kiga.shop.web.converter;

import static org.junit.Assert.assertEquals;

import com.kiga.shop.domain.CountryGroup;
import com.kiga.shop.web.response.CountryGroupViewModel;
import org.junit.Test;

/**
 * @author bbs
 * @since 10/16/16.
 */
public class CountryGroupToViewModelConverterTest {
  private CountryGroupToViewModelConverter countryGroupToViewModelConverter =
    new CountryGroupToViewModelConverter();

  @Test
  public void testConvertEntity() {
    CountryGroup entity = new CountryGroup();
    entity.setId(99L);
    entity.setTitle("MOCK_TITLE");
    CountryGroupViewModel viewModel = countryGroupToViewModelConverter.convertToResponse(entity);
    assertEquals(entity.getTitle(), viewModel.getTitle());
  }
}
