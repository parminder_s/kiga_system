package com.kiga.shop.document;

import com.kiga.crm.messages.response.Invoice;
import com.kiga.shop.documents.model.InvoiceVatRow;
import com.kiga.shop.documents.service.InvoiceVatTableCalcer;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.MathContext;

import java.util.Arrays;
import java.util.List;

/**
 * Created by peter on 14.02.17.
 */
public class InvoiceVatTableCalcerTest {
  @Test
  public void testThreeRates() {
    List<PurchaseItem> purchaseItems = Arrays.asList(
      createPurchaseItem(.16d, 22d, .2d, 5d),
      createPurchaseItem(.11d, 15d, .2d, 10d),
      createPurchaseItem(.2d, 12d, .2d, 1d)
    );

    Purchase purchase = createPurchase(purchaseItems, .2, 5d);

    InvoiceVatTableCalcer invoiceVatTableCalcer = new InvoiceVatTableCalcer();
    List<InvoiceVatRow> result = invoiceVatTableCalcer.calc(purchase);
    List<InvoiceVatRow> res = Arrays.asList(createIvr(.11, 15d), createIvr(.16, 22d),
      createIvr(.2, 17d));

    Assert.assertTrue(compareInvoiceVatRow(result.get(0),res.get(0)));
    Assert.assertTrue(compareInvoiceVatRow(result.get(1),res.get(1)));
    Assert.assertTrue(compareInvoiceVatRow(result.get(2),res.get(2)));


  }

  InvoiceVatRow createIvr(Double rate, Double total) {
    return new InvoiceVatRow(
      new BigDecimal(rate, new MathContext(2)),
      new BigDecimal(total, new MathContext(2))
    );
  }

  PurchaseItem createPurchaseItem(Double vatRate, Double vatTotal, Double vatShippingRate,
                                  Double vatShippingTotal) {
    PurchaseItem returner = new PurchaseItem();
    returner.setVatRate(new BigDecimal(vatRate, new MathContext(2)));
    returner.setVatTotal(new BigDecimal(vatTotal, new MathContext(2)));
    returner.setVatShippingRate(new BigDecimal(vatShippingRate, new MathContext(2)));
    returner.setVatShippingTotal(new BigDecimal(vatShippingTotal, new MathContext(2)));
    return returner;
  }

  Purchase createPurchase(List<PurchaseItem> purchaseItems, Double vatShippingRate,
                          Double vatShipping) {
    Purchase returner = new Purchase();
    returner.setPurchaseItems(purchaseItems);
    returner.setVatRateShipping(new BigDecimal(vatShippingRate, new MathContext(2)));
    returner.setVatShipping(new BigDecimal(vatShipping, new MathContext(2)));
    return returner;
  }

  @Test
  public void testSorting() {
    List<PurchaseItem> purchaseItems = Arrays.asList(
      createPurchaseItem(0.16d, 22d, 0.2d, 5d),
      createPurchaseItem(.11d, 15d, .2d, 10d),
      createPurchaseItem(.2d, 12d, .2d, 1d)
    );

    Purchase purchase = createPurchase(purchaseItems, .2, 5d);

    InvoiceVatTableCalcer invoiceVatTableCalcer = new InvoiceVatTableCalcer();
    List<InvoiceVatRow> result = invoiceVatTableCalcer.calc(purchase);
    List<InvoiceVatRow> res = Arrays.asList(createIvr(.11d, 15d), createIvr(.16d, 22d),
      createIvr(.2, 17d));

    Assert.assertTrue(compareInvoiceVatRow(result.get(0),res.get(0)));
    Assert.assertTrue(compareInvoiceVatRow(result.get(1),res.get(1)));
    Assert.assertTrue(compareInvoiceVatRow(result.get(2),res.get(2)));


  }


  @Test
  public void testSingleRate() {
    List<PurchaseItem> purchaseItems = Arrays.asList(
      createPurchaseItem(.2d, 22d, .2d, 5d),
      createPurchaseItem(.2d, 15d, .2d, 10d),
      createPurchaseItem(.2d, 12d, .2d, 1d)
    );
    Purchase purchase = createPurchase(purchaseItems, .2, 5d);

    InvoiceVatTableCalcer invoiceVatTableCalcer = new InvoiceVatTableCalcer();
    List<InvoiceVatRow> result = invoiceVatTableCalcer.calc(purchase);
    List<InvoiceVatRow> res = Arrays.asList(createIvr(.2, 54d));

    Assert.assertTrue(compareInvoiceVatRow(result.get(0),res.get(0)));
  }

  @Test
  public void testOneProduct() {
    List<PurchaseItem> purchaseItems = Arrays.asList(
      createPurchaseItem(.2d, 12d, .2d, 1d)
    );
    Purchase purchase = createPurchase(purchaseItems, .2, 13d);

    InvoiceVatTableCalcer invoiceVatTableCalcer = new InvoiceVatTableCalcer();
    List<InvoiceVatRow> result = invoiceVatTableCalcer.calc(purchase);
    List<InvoiceVatRow> res = Arrays.asList(createIvr(.2, 25d));

    Assert.assertTrue(compareInvoiceVatRow(result.get(0),res.get(0)));
  }

  boolean compareInvoiceVatRow(InvoiceVatRow first, InvoiceVatRow second) {
    return (first.getRate().compareTo(second.getRate()) == 0)
      && first.getTotal().compareTo(second.getTotal()) == 0;
  }
}
