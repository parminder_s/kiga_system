package com.kiga.shop.basket.service.price;

import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;

import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.domain.Voucher;
import com.kiga.shop.service.VatRateCalculator;
import java.math.BigDecimal;
import java.util.Arrays;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class DiscountSetterTest {
  @Test
  public void testWithoutDiscount() {
    PurchaseItem purchaseItem =
        PurchaseItem.builder()
            .priceNetSingle(new BigDecimal("12.50"))
            .priceGrossTotal(new BigDecimal("13.75"))
            .vatTotal(new BigDecimal("1.25"))
            .vatRate(new BigDecimal("0.100"))
            .build();

    new DiscountSetter(new VatRateCalculator()).applyDiscounts(singletonList(purchaseItem));

    assertThat(
        purchaseItem,
        allOf(
            hasProperty("priceNetSingle", equalTo(new BigDecimal("12.50"))),
            hasProperty("vatTotal", equalTo(new BigDecimal("1.25"))),
            hasProperty("priceGrossTotal", equalTo(new BigDecimal("13.75"))),
            hasProperty("voucherCode", equalTo("")),
            hasProperty("appliedDiscountGrossTotal", equalTo(BigDecimal.ZERO)),
            hasProperty("appliedDiscountRate", equalTo(BigDecimal.ZERO))));
  }

  @Test
  public void testWithSetVoucher() {
    Voucher voucher = Voucher.builder().code("foobar").discountRate(new BigDecimal("0.15")).build();
    PurchaseItem purchaseItem =
        PurchaseItem.builder()
            .priceNetSingle(new BigDecimal("12.50"))
            .priceGrossTotal(new BigDecimal("13.75"))
            .vatTotal(new BigDecimal("1.25"))
            .vatRate(new BigDecimal("0.100"))
            .amount(1)
            .voucherCode("foobar")
            .voucher(voucher)
            .build();

    new DiscountSetter(new VatRateCalculator()).applyDiscounts(singletonList(purchaseItem));

    assertThat(
        purchaseItem,
        allOf(
            hasProperty("priceGrossTotal", equalTo(new BigDecimal("11.96"))),
            hasProperty("priceNetSingle", equalTo(new BigDecimal("10.870"))),
            hasProperty("vatTotal", equalTo(new BigDecimal("1.09"))),
            hasProperty("voucherCode", equalTo("foobar")),
            hasProperty("appliedDiscountGrossTotal", equalTo(new BigDecimal("1.79"))),
            hasProperty("appliedDiscountRate", equalTo(new BigDecimal("0.15")))));
  }

  @Test
  public void testWithMultipleAmount() {
    Voucher voucher = Voucher.builder().code("foobar").discountRate(new BigDecimal("0.15")).build();
    PurchaseItem purchaseItem =
        PurchaseItem.builder()
            .priceNetSingle(new BigDecimal("25.00"))
            .priceGrossTotal(new BigDecimal("55"))
            .vatTotal(new BigDecimal("5"))
            .vatRate(new BigDecimal("0.100"))
            .voucherCode("foobar")
            .voucher(voucher)
            .amount(2)
            .build();

    new DiscountSetter(new VatRateCalculator()).applyDiscounts(singletonList(purchaseItem));

    assertThat(
        purchaseItem,
        allOf(
            hasProperty("priceGrossTotal", equalTo(new BigDecimal("47.82"))),
            hasProperty("priceNetSingle", equalTo(new BigDecimal("21.740"))),
            hasProperty("vatTotal", equalTo(new BigDecimal("4.34"))),
            hasProperty("voucherCode", equalTo("foobar")),
            hasProperty("appliedDiscountGrossTotal", equalTo(new BigDecimal("7.18"))),
            hasProperty("appliedDiscountRate", equalTo(new BigDecimal("0.15")))));
  }

  @Test
  public void testApplicationWith0InsteadOfZero() {
    Voucher voucher = Voucher.builder().discountRate(new BigDecimal("0.5")).build();
    PurchaseItem purchaseItem =
        PurchaseItem.builder()
            .priceNetSingle(new BigDecimal("10.00"))
            .priceGrossTotal(new BigDecimal("12"))
            .vatTotal(new BigDecimal("5"))
            .vatRate(new BigDecimal("0.200"))
            .voucher(voucher)
            .amount(1)
            .appliedDiscountRate(new BigDecimal("0.00"))
            .build();

    new DiscountSetter(new VatRateCalculator()).applyDiscounts(Arrays.asList(purchaseItem));

    assertThat(
        purchaseItem,
        allOf(
            hasProperty("appliedDiscountGrossTotal", equalTo(new BigDecimal("4.00"))),
            hasProperty("appliedDiscountRate", equalTo(new BigDecimal("0.5")))));
  }

  @Test
  public void testWith3DigitVatRate() {
    Voucher voucher = Voucher.builder().code("foobar").discountRate(new BigDecimal("0.15")).build();
    PurchaseItem purchaseItem =
        PurchaseItem.builder()
            .priceNetSingle(new BigDecimal("25.00"))
            .priceGrossTotal(new BigDecimal("53.86"))
            .vatTotal(new BigDecimal("3.85"))
            .vatRate(new BigDecimal("0.077"))
            .appliedDiscountRate(new BigDecimal("0.15"))
            .voucherCode("foobar")
            .voucher(voucher)
            .amount(2)
            .build();

    new DiscountSetter(new VatRateCalculator()).applyDiscounts(singletonList(purchaseItem));

    Assertions.assertThat(purchaseItem)
        .hasFieldOrPropertyWithValue("priceNetSingle", new BigDecimal("21.740"))
        .hasFieldOrPropertyWithValue("vatTotal", new BigDecimal("3.36"))
        .hasFieldOrPropertyWithValue("priceGrossTotal", new BigDecimal("46.84"))
        .hasFieldOrPropertyWithValue("voucherCode", "foobar")
        .hasFieldOrPropertyWithValue("appliedDiscountGrossTotal", new BigDecimal("7.02"))
        .hasFieldOrPropertyWithValue("appliedDiscountRate", new BigDecimal("0.15"));
  }

  /**
   * In this test we cover the use case where a voucher is already applied and the the basket gets
   * changed. Further more the original voucher and has now 50% instead of 15%. We should calculate
   * with 15%.
   */
  @Test
  public void testAmountChangeAndDifferentVoucher() {
    Voucher voucher = Voucher.builder().code("foobar").discountRate(new BigDecimal("0.5")).build();
    PurchaseItem purchaseItem =
        PurchaseItem.builder()
            .priceNetSingle(new BigDecimal("25.00"))
            .priceGrossTotal(new BigDecimal("55"))
            .vatTotal(new BigDecimal("5"))
            .vatRate(new BigDecimal("0.100"))
            .appliedDiscountRate(new BigDecimal("0.15"))
            .voucherCode("foobar")
            .voucher(voucher)
            .amount(2)
            .build();

    new DiscountSetter(new VatRateCalculator()).applyDiscounts(singletonList(purchaseItem));

    assertThat(
        purchaseItem,
        allOf(
            hasProperty("priceNetSingle", equalTo(new BigDecimal("21.740"))),
            hasProperty("vatTotal", equalTo(new BigDecimal("4.34"))),
            hasProperty("priceGrossTotal", equalTo(new BigDecimal("47.82"))),
            hasProperty("voucherCode", equalTo("foobar")),
            hasProperty("appliedDiscountGrossTotal", equalTo(new BigDecimal("7.18"))),
            hasProperty("appliedDiscountRate", equalTo(new BigDecimal("0.15")))));
  }
}
