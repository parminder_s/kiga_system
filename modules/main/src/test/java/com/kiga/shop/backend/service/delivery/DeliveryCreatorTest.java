package com.kiga.shop.backend.service.delivery;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseDelivery;
import com.kiga.shop.domain.PurchaseDeliveryPurchaseProduct;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.domain.ShippingStatus;
import com.kiga.spec.Persister;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

public class DeliveryCreatorTest {
  @Test
  public void testCreation() {
    Persister<PurchaseDelivery> deliveryPersister = mock(Persister.class);
    Persister<PurchaseDeliveryPurchaseProduct> productPersister = mock(Persister.class);
    Product p1 = new Product();
    PurchaseItem pi1 = PurchaseItem.builder().product(p1).amount(5).build();
    Product p2 = new Product();
    PurchaseItem pi2 = PurchaseItem.builder().product(p2).amount(3).build();
    Purchase purchase = Purchase.builder().purchaseItems(asList(pi1, pi2)).build();
    new DeliveryCreator(deliveryPersister, productPersister).create(purchase);
    ArgumentCaptor<PurchaseDelivery> captor = ArgumentCaptor.forClass(PurchaseDelivery.class);

    verify(deliveryPersister).persist(captor.capture());

    PurchaseDelivery purchaseDelivery = captor.getValue();
    assertThat(purchaseDelivery).extracting("status").contains(ShippingStatus.NONE);
    assertThat(purchaseDelivery.getPurchaseProducts())
        .extracting(pp -> tuple(pp.getPurchaseItem(), pp.getAmount()))
        .contains(tuple(pi1, 5))
        .contains(tuple(pi2, 3));
    verify(productPersister, times(2)).persist(any());
  }
}
