package com.kiga.shop.basket.web.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.main.service.CookieService;
import com.kiga.security.domain.Member;
import com.kiga.security.services.SecurityService;
import com.kiga.shop.basket.service.BasketRetriever;
import com.kiga.shop.basket.service.CountryDeliveryCheck;
import org.junit.Test;

import java.util.Optional;

public class BasketHttpRetrieverTest {
  @Test
  public void testDefault() {
    BasketRetriever basketRetriever = mock(BasketRetriever.class);
    when(basketRetriever.find(any(), anyString(), any())).thenReturn(Optional.empty());

    SecurityService securityService = mock(SecurityService.class);
    when(securityService.getSessionIdFromCookie()).thenReturn("foobar");
    Optional<Member> member = Optional.empty();
    when(securityService.getSessionMember()).thenReturn(member);

    CookieService cookieService = mock(CookieService.class);
    Optional<String> cookieValue = Optional.of("cookie");
    when(cookieService.getOptional(anyString())).thenReturn(cookieValue);

    BasketHttpRetriever basketHttpRetriever =
      new BasketHttpRetriever(securityService, cookieService, basketRetriever);
    basketHttpRetriever.getCurrentBasket();

    verify(basketRetriever).find(member, "foobar", cookieValue);
  }
}
