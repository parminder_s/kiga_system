package com.kiga.shop.web.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import com.kiga.shop.backend.web.converter.ProductToViewModelConverter;
import com.kiga.shop.backend.web.response.ProductViewModel;
import com.kiga.shop.domain.Product;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author bbs
 * @since 10/7/16.
 */
public final class ProductToViewModelConverterTest {
  private static final String TITLE = "TITLE";
  private static final String CODE = "CODE";
  private static final BigDecimal VOLUME_LITRE = BigDecimal.valueOf(3);
  private static final Integer WEIGHT_GRAM = 2;
  private static final Long ID = 1L;
  private static final Date CREATED = new Date();
  private static final Date LAST_EDITED = new Date();
  private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(
    "Y-M-dd HH:mm:ss");
  private ProductToViewModelConverter productToViewModelConverter;

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    productToViewModelConverter = new ProductToViewModelConverter();
  }

  @Test
  public void testConvertListWithNullElements() {
    List<ProductViewModel> productViewModels = productToViewModelConverter
      .convertToResponse(Collections.singletonList(null));
    assertNull(productViewModels.get(0));
  }

  @Test
  public void testConvertNullList() {
    List<ProductViewModel> productViewModels = productToViewModelConverter
      .convertToResponse((List<Product>) null);
    assertNull(productViewModels);
  }

  @Test
  public void testConvertList() {
    Product product = new Product();
    List<ProductViewModel> productViewModels = productToViewModelConverter
      .convertToResponse(Collections.singletonList(product));
    assertNotNull(productViewModels.get(0));
  }

  @Test
  public void testBothMethodsGivesSameResults() {
    Product product = new Product();
    List<ProductViewModel> productViewModels = productToViewModelConverter
      .convertToResponse(Collections.singletonList(product));
    ProductViewModel productViewModel = productToViewModelConverter.convertToResponse(product);
    assertNotNull(productViewModel);
    assertNotNull(productViewModels.get(0));
    assertEquals(productViewModel, productViewModels.get(0));
  }

  @Test
  public void testConvertNullProduct() {
    ProductViewModel productViewModel = productToViewModelConverter
      .convertToResponse((Product) null);
    assertNull(productViewModel);
  }

  @Test
  public void testConvertProduct() {
    Product product = new Product();
    product.setProductTitle(TITLE);
    product.setCode(CODE);
    product.setVolumeLitre(VOLUME_LITRE);
    product.setWeightGram(WEIGHT_GRAM);
    product.setId(ID);
    product.setCreated(CREATED);
    product.setLastEdited(LAST_EDITED);
    ProductViewModel productViewModel = productToViewModelConverter.convertToResponse(product);
    assertNotNull(productViewModel);
    assertEquals(ID, productViewModel.getId());
    assertEquals(TITLE, productViewModel.getProductTitle());
    assertEquals(CODE, productViewModel.getCode());
    assertEquals(VOLUME_LITRE, productViewModel.getVolumeLitre());
    assertEquals(WEIGHT_GRAM, productViewModel.getWeightGram());
    assertEquals(WEIGHT_GRAM, productViewModel.getWeightGram());
    assertEquals(SIMPLE_DATE_FORMAT.format(CREATED), productViewModel.getCreatedOn());
    assertEquals(SIMPLE_DATE_FORMAT.format(LAST_EDITED), productViewModel.getLastEditedOn());
  }
}
