package com.kiga.shop.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class SalutationTest {
  @Test
  public void testDefault() {
    assertThat(Salutation.getFromGender("foobar")).isEqualToIgnoringWhitespace("Frau");
  }

  @Test
  public void testMale() {
    assertThat(Salutation.getFromGender("male")).isEqualToIgnoringWhitespace("Herr");
  }

  @Test
  public void testFemale() {
    assertThat(Salutation.getFromGender("female")).isEqualToIgnoringWhitespace("Frau");
  }
}
