package com.kiga.shop.basket.web.service;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.main.locale.Locale;
import com.kiga.main.service.CookieService;
import com.kiga.security.domain.Member;
import com.kiga.security.spring.SecurityUtil;
import com.kiga.shop.basket.service.BasketFactoryExecutor;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.repository.CountryRepository;
import com.kiga.shop.repository.PurchaseRepository;
import com.kiga.web.service.GeoIpService;
import java.util.Optional;
import org.junit.Test;

public class BasketFactoryTest {
  @Test
  public void doTestWithMember() {
    Purchase purchase = new Purchase();
    BasketFactoryExecutor executor = mock(BasketFactoryExecutor.class);
    GeoIpService geoIpService = mock(GeoIpService.class);
    when(geoIpService.resolveCountryCode()).thenReturn("at");

    CountryRepository countryRepository = mock(CountryRepository.class);
    Country country = new Country();
    when(countryRepository.findByCode("at")).thenReturn(country);

    PurchaseRepository purchaseRepository = mock(PurchaseRepository.class);
    CookieService cookieService = mock(CookieService.class);

    SecurityUtil securityUtil = mock(SecurityUtil.class);
    Optional<Member> optMember = Optional.of(new Member());
    when(securityUtil.getOptMember()).thenReturn(optMember);
    when(securityUtil.getSessionId()).thenReturn("foobar");
    when(executor.create(country, optMember, Locale.en, "foobar")).thenReturn(purchase);

    BasketFactory basketFactory =
        new BasketFactory(
            executor,
            geoIpService,
            countryRepository,
            purchaseRepository,
            cookieService,
            securityUtil);
    basketFactory.create(Locale.en);

    verify(cookieService, never()).add(anyString(), anyString());
    verify(purchaseRepository).save(purchase);
  }

  @Test
  public void doTestWithAnonymous() {
    BasketFactoryExecutor executor = mock(BasketFactoryExecutor.class);
    CookieService cookieService = mock(CookieService.class);

    SecurityUtil securityUtil = mock(SecurityUtil.class);
    when(securityUtil.getSessionId()).thenReturn("foobar");
    when(securityUtil.getOptMember()).thenReturn(Optional.empty());

    BasketFactory basketFactory =
        new BasketFactory(
            executor,
            mock(GeoIpService.class),
            mock(CountryRepository.class),
            mock(PurchaseRepository.class),
            cookieService,
            securityUtil);
    basketFactory.create(Locale.en);

    verify(cookieService).add("PurchaseID", "foobar");
  }
}
