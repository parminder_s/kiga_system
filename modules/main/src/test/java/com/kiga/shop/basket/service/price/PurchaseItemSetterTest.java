package com.kiga.shop.basket.service.price;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.service.ProductInfoCreator;
import com.kiga.shop.web.response.ProductInfo;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

public class PurchaseItemSetterTest {
  @Rule
  public final ExpectedException exception = ExpectedException.none();

  @Test
  public void testNormal() {
    ProductInfo productInfo = ProductInfo.builder()
      .priceNet(new BigDecimal("25.00")).vatRate(new BigDecimal("0.2"))
      .vatProduct(new BigDecimal("5.00")).priceGross(new BigDecimal("30.00"))
      .volumeLitre(new BigDecimal("0.005")).weightGram(100)
      .build();
    ProductInfoCreator productInfoCreator = mock(ProductInfoCreator.class);
    when(productInfoCreator.create(anyLong(), anyString())).thenReturn(Optional.of(productInfo));
    Product product = new Product();
    product.setId(5L);
    Country country = new Country();
    country.setCode("at");

    Purchase purchase = Purchase.builder()
      .purchaseItems(Arrays.asList(
        PurchaseItem.builder()
          .amount(5).product(product)
          .build()
      ))
      .country(country).build();

    PurchaseItem purchaseItem = PurchaseItem.builder()
      .amount(5).product(product)
      .priceNetSingle(new BigDecimal("25.00")).vatRate(new BigDecimal("0.2"))
      .vatTotal(new BigDecimal("25.00")).priceGrossTotal(new BigDecimal("150.00"))
      .volumeLitreSingle(new BigDecimal("0.005")).volumeLitreTotal(new BigDecimal("0.025"))
      .weightGramSingle(100).weightGramTotal(500)
      .build();

    new PurchaseItemSetter(productInfoCreator).setPurchaseItems(purchase);

    assertThat(purchase.getPurchaseItems().get(0), sameBeanAs(purchaseItem));
  }

  @Test
  public void testWithInvalidCountry() {
    ProductInfoCreator productInfoCreator = mock(ProductInfoCreator.class);
    when(productInfoCreator.create(anyLong(), anyString())).thenReturn(Optional.empty());
    Product product = new Product();
    product.setId(1L);
    Purchase purchase = Purchase.builder()
      .purchaseItems(Arrays.asList(PurchaseItem.builder().product(product).build()))
      .country(Country.builder().code("at").build())
      .build();

    exception.expect(UnavailableCountryException.class);
    new PurchaseItemSetter(productInfoCreator).setPurchaseItems(purchase);
  }
}
