package com.kiga.shop.basket.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.shop.basket.web.service.BasketFactory;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import org.junit.Test;

import java.util.Optional;

public class BasketServiceExecutorTest {
  @Test
  public void testExecution() {
    Purchase purchase = new Purchase();
    PurchaseItem purchaseItem = PurchaseItem.builder().amount(1).build();
    PricesSetter pricesSetter = mock(PricesSetter.class);
    FindOrMakePurchaseItem findOrMakePurchaseItem = mock(FindOrMakePurchaseItem.class);
    when(findOrMakePurchaseItem.get(purchase, 5)).thenReturn(purchaseItem);

    new BasketServiceExecutor(null, findOrMakePurchaseItem, pricesSetter)
      .execute(Optional.of(purchase), 5, null, (purchase1, purchaseItem1) -> {
        purchase1.setFirstname("John");
        purchaseItem1.setTitle("Some Product");
      });

    assertEquals("John", purchase.getFirstname());
    assertEquals("Some Product", purchaseItem.getTitle());
  }

  @Test
  public void testExecutionWithNonExistingBasket() {
    Purchase purchase = new Purchase();
    PurchaseItem purchaseItem = PurchaseItem.builder().amount(0).build();
    PricesSetter pricesSetter = mock(PricesSetter.class);
    FindOrMakePurchaseItem findOrMakePurchaseItem = mock(FindOrMakePurchaseItem.class);
    when(findOrMakePurchaseItem.get(purchase, 5)).thenReturn(purchaseItem);
    BasketFactory basketFactory = mock(BasketFactory.class);
    when(basketFactory.create(null)).thenReturn(purchase);

    new BasketServiceExecutor(basketFactory, findOrMakePurchaseItem, pricesSetter)
      .execute(Optional.empty(), 5, null, (purchase1, purchaseItem1) -> {
        purchase1.setFirstname("John");
        purchaseItem1.setTitle("Some Product");
      });

    assertEquals("John", purchase.getFirstname());
    assertEquals("Some Product", purchaseItem.getTitle());
  }
}
