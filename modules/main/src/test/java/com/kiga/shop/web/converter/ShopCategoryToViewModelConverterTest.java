package com.kiga.shop.web.converter;

import com.kiga.shopcontent.domain.live.ShopCategoryLive;
import com.kiga.shopcontent.viewmodel.ShopCategoryViewModel;
import com.kiga.shopcontent.viewmodel.converter.ShopCategoryToResponseConverter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;

/**
 * @author bbs
 * @since 9/8/16.
 */
public class ShopCategoryToViewModelConverterTest {
  private ShopCategoryToResponseConverter shopCategoryToResponseConverter;

  /**
   * Prepare tests.
   *
   * @throws IOException won't really throw it
   */
  @Before
  public void prepareTests() throws IOException {
    shopCategoryToResponseConverter = new ShopCategoryToResponseConverter();
  }

  @Test
  public void testNullEntity() {
    ShopCategoryViewModel shopCategoryResponse = shopCategoryToResponseConverter
      .convertToResponse(null);
    Assert.assertNull(shopCategoryResponse);
  }

  @Test
  public void testNotLastCategoryLevelConversion() {
    ShopCategoryLive childShopCategoryLive = new ShopCategoryLive();
    ShopCategoryLive shopCategoryLive = new ShopCategoryLive();
    shopCategoryLive.setId(3L);
    shopCategoryLive.setClassName("ShopCategory");
    shopCategoryLive.setTitle("Mocked Shop Category");
    shopCategoryLive.setUrlSegment("mocked-product-page");
    shopCategoryLive.setChildren(Collections.singletonList(childShopCategoryLive));

    ShopCategoryViewModel shopCategoryResponse = shopCategoryToResponseConverter
      .convertToResponse(shopCategoryLive);

    Assert.assertEquals(Long.valueOf(3L), shopCategoryResponse.getId());
    Assert.assertEquals("ShopCategory", shopCategoryResponse.getClassName());
    Assert.assertEquals("Mocked Shop Category", shopCategoryResponse.getTitle());
    Assert.assertEquals("mocked-product-page", shopCategoryResponse.getUrlSegment());
    Assert.assertEquals("ShopCategoryLive", shopCategoryResponse.getResolvedType());
    Assert.assertFalse(shopCategoryResponse.isLastCategoryLevel());
  }

  @Test
  public void testLastCategoryLevelConversion() {
    ShopCategoryLive shopCategoryLive = new ShopCategoryLive();
    shopCategoryLive.setId(3L);
    shopCategoryLive.setClassName("ShopCategory");
    shopCategoryLive.setTitle("Mocked Shop Category");
    shopCategoryLive.setUrlSegment("mocked-product-page");

    ShopCategoryViewModel shopCategoryResponse = shopCategoryToResponseConverter
      .convertToResponse(shopCategoryLive);

    Assert.assertEquals(Long.valueOf(3L), shopCategoryResponse.getId());
    Assert.assertEquals("ShopCategory", shopCategoryResponse.getClassName());
    Assert.assertEquals("Mocked Shop Category", shopCategoryResponse.getTitle());
    Assert.assertEquals("mocked-product-page", shopCategoryResponse.getUrlSegment());
    Assert.assertEquals("ShopCategoryLive", shopCategoryResponse.getResolvedType());
    Assert.assertTrue(shopCategoryResponse.isLastCategoryLevel());
  }
}
