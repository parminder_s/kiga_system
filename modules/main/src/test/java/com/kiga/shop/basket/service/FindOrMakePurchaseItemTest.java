package com.kiga.shop.basket.service;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.repository.PurchaseItemRepository;
import com.kiga.shop.service.ProductInfoCreator;
import com.kiga.shop.web.response.ProductInfo;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class FindOrMakePurchaseItemTest {
  @Test
  public void testExisting() {
    Product product = new Product();
    product.setId(5L);

    PurchaseItem purchaseItem =
      PurchaseItem.builder().product(product).build();
    Purchase purchase = Purchase.builder()
      .purchaseItems(Arrays.asList(purchaseItem))
      .build();

    FindOrMakePurchaseItem findOrMakePurchaseItem =
      new FindOrMakePurchaseItem(null, null);

    assertEquals(purchaseItem, findOrMakePurchaseItem.get(purchase, 5L));
  }

  @Test
  public void testNonExisting() {
    ProductInfoCreator productInfoCreator = mock(ProductInfoCreator.class);
    Product product = new Product();
    ProductInfo productInfo = ProductInfo.builder()
      .productTitle("aProduct").productCode("a-p")
      .product(product).build();
    when(productInfoCreator.create(5L, "at")).thenReturn(Optional.of(productInfo));
    PurchaseItemRepository purchaseItemRepository = mock(PurchaseItemRepository.class);
    when(purchaseItemRepository.save(any(PurchaseItem.class)))
      .thenAnswer(invocation -> invocation.getArguments()[0]);

    Purchase purchase =
      Purchase.builder()
        .purchaseItems(new ArrayList<>())
        .country(Country.builder().code("at").build())
        .build();

    PurchaseItem purchaseItem = PurchaseItem.builder()
      .title("aProduct").code("a-p").amount(0)
      .product(product).purchase(purchase)
      .build();
    purchaseItem.setClassName("PurchaseItem");

    FindOrMakePurchaseItem findOrMakePurchaseItem =
      new FindOrMakePurchaseItem(productInfoCreator, purchaseItemRepository);
    PurchaseItem actual = findOrMakePurchaseItem.get(purchase, 5L);

    assertThat(purchaseItem, sameBeanAs(actual).ignoring(Purchase.class).ignoring("created"));
    assertEquals(product, actual.getProduct());
  }
}
