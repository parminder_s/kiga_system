package com.kiga.shop.basket.web.service;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.shop.basket.voucher.ApplyVoucher;
import com.kiga.shop.basket.web.request.ApplyVoucherRequest;
import com.kiga.shop.basket.web.response.ApplyVoucherResponse;
import com.kiga.shop.domain.Purchase;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ApplyVoucherEndpointTest {
  @Rule public ExpectedException thrown = ExpectedException.none();

  @Test
  public void testWithEmptyBasket() {
    BasketHttpRetriever basketHttpRetriever = mock(BasketHttpRetriever.class);
    when(basketHttpRetriever.getCurrentBasket()).thenReturn(Optional.empty());
    thrown.expect(NoSuchElementException.class);
    new ApplyVoucherEndpoint(basketHttpRetriever, null, null).apply(null);
  }

  @Test
  public void testWithFailedApply() {
    BasketHttpRetriever basketHttpRetriever = mock(BasketHttpRetriever.class);
    when(basketHttpRetriever.getCurrentBasket()).thenReturn(Optional.of(new Purchase()));

    ApplyVoucher applyVoucher = mock(ApplyVoucher.class);
    when(applyVoucher.apply(anyString(), any())).thenReturn(false);

    BasketMapper basketMapper = mock(BasketMapper.class);

    ApplyVoucherResponse response =
        new ApplyVoucherEndpoint(basketHttpRetriever, applyVoucher, basketMapper)
            .apply(ApplyVoucherRequest.builder().code("someCode").build());

    assertThat(
        response,
        sameBeanAs(ApplyVoucherResponse.builder().successful(false).basket(null).build()));
  }
}
