package com.kiga.shop.backend.purchase.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.DateTypesProvider;
import com.kiga.accounting.domain.Invoice;
import com.kiga.accounting.repository.InvoiceRepository;
import com.kiga.main.paymentmethod.PaymentMethod;
import com.kiga.security.domain.Member;
import com.kiga.shop.backend.api.model.PurchaseResponse;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.domain.PurchaseItem;
import com.kiga.shop.domain.PurchaseStatus;
import com.kiga.shop.domain.ShippingPartner;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.Arrays;
import org.junit.Test;

public class PurchaseResponseConverterTest {
  @Test
  public void testMapperWithDifferentMemberNames() {
    Member member = new Member();
    member.setCustomerId("5");
    member.setFirstname("Dingsbums");
    member.setLastname("Müller");
    member.setEmail("dingsbums.mueller@kigaportal.com");

    Invoice invoice = new Invoice();
    invoice.setId(12L);
    invoice.setSettled(true);
    InvoiceRepository invoiceRepository = mock(InvoiceRepository.class);
    when(invoiceRepository.findByInvoiceNumber("58")).thenReturn(invoice);

    Purchase purchase =
        Purchase.builder()
            .firstname("Arnolf Ludwig")
            .lastname("Mister")
            .email("arnolf.ludwig.mister@kigaportal.com")
            .orderNr(123L)
            .country(Country.builder().code("at").build())
            .priceNetTotal(new BigDecimal("10.00"))
            .priceGrossTotal(new BigDecimal("12.00"))
            .currency("EUR")
            .status(PurchaseStatus.Ordered)
            .purchaseItems(
                Arrays.asList(
                    PurchaseItem.builder().amount(2).build(),
                    PurchaseItem.builder().amount(1).build()))
            .customer(member)
            .orderDate(DateTypesProvider.getInstant(2018, 1, 1))
            .shippingNoteNr("SHIPGRAZ")
            .invoiceNr("58")
            .invoiceId(123)
            .paymentMethod(PaymentMethod.CREDITCARD)
            .paymentPspId("ATX")
            .shippingPartner(ShippingPartner.DHL)
            .build();
    purchase.setId(15L);

    assertThat(new PurchaseResponseConverter(invoiceRepository).convertToResponse(purchase))
        .isEqualToComparingFieldByField(
            new PurchaseResponse()
                .id(15)
                .name("Mister Arnolf Ludwig")
                .orderNr(123)
                .country("AT")
                .priceNetTotal("10.00")
                .priceGrossTotal("12.00")
                .currency("EUR")
                .status("Ordered")
                .amount(3)
                .shippingNoteNumber("SHIPGRAZ")
                .invoiceNumber("12")
                .invoiceId(12L)
                .invoiceNumber("58")
                .invoiceSettled(true)
                .memberId("5")
                .memberEmail("arnolf.ludwig.mister@kigaportal.com")
                .paymentPspId("ATX")
                .paymentMethod("CREDITCARD")
                .shippingPartner("DHL")
                .purchaseDate(
                    DateTypesProvider.getInstant(2018, 1, 1)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate()));
  }
}
