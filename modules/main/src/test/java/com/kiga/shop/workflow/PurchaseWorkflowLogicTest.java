package com.kiga.shop.workflow;

import static com.kiga.main.paymentmethod.PaymentMethod.INVOICE;
import static com.kiga.shop.domain.PurchaseStatus.Cancelled;
import static com.kiga.shop.domain.PurchaseStatus.CreditVoucherFinalized;
import static com.kiga.shop.domain.PurchaseStatus.Ordered;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.kiga.shop.domain.Purchase;
import com.kiga.shop.spec.WorkflowException;
import org.junit.Test;

public class PurchaseWorkflowLogicTest {
  @Test
  public void testCancellation() {
    Purchase purchase = new Purchase();
    purchase.setPaymentMethod(INVOICE);
    purchase.setStatus(Ordered);

    new PurchaseWorkflowLogic(null, null, null, null).getStageChanger(purchase, Cancelled);
  }

  @Test
  public void testInvalidCancellation() {
    Purchase purchase = new Purchase();
    purchase.setPaymentMethod(INVOICE);
    purchase.setStatus(CreditVoucherFinalized);

    assertThatThrownBy(
            () ->
                new PurchaseWorkflowLogic(null, null, null, null)
                    .getStageChanger(purchase, Cancelled))
        .isInstanceOf(WorkflowException.class);
  }
}
