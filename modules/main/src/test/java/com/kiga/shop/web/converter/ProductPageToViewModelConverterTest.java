package com.kiga.shop.web.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.content.domain.ss.KigaPageImage;
import com.kiga.content.service.SiteTreeUrlGenerator;
import com.kiga.ideas.property.ImageSize;
import com.kiga.ideas.property.PreviewImage;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.service.resize.PaddedImageStrategy;
import com.kiga.s3.service.resize.S3ImageResizeService;
import com.kiga.shop.ShopProperties;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shopcontent.domain.draft.ProductPageDraft;
import com.kiga.shopcontent.domain.live.ProductPageLive;
import com.kiga.shopcontent.viewmodel.ShopCategoryProductViewModel;
import com.kiga.shopcontent.viewmodel.converter.ShopCategoryProductToResponseConverter;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;

/**
 * @author bbs
 * @since 9/8/16.
 */
public class ProductPageToViewModelConverterTest {
  private ShopCategoryProductToResponseConverter shopCategoryProductToResponseConverter;
  private S3ImageResizeService s3ImageResizeService;
  private ShopProperties shopProperties;
  private SiteTreeUrlGenerator siteTreeUrlGenerator;
  private PreviewImage previewImage;

  /**
   * Prepare tests.
   *
   * @throws IOException won't really throw it
   */
  @Before
  public void prepareTests() throws IOException {
    s3ImageResizeService = mock(S3ImageResizeService.class);
    when(s3ImageResizeService.getResizedImageUrl(any(), any()))
      .thenReturn("http://MockedPreviewImage");

    previewImage = mock(PreviewImage.class);
    when(previewImage.getSmall()).thenReturn(new ImageSize(100, 100));
    when(previewImage.getLarge()).thenReturn(new ImageSize(200, 200));

    shopProperties = mock(ShopProperties.class);
    when(shopProperties.getPreviewImage()).thenReturn(previewImage);
    siteTreeUrlGenerator = mock(SiteTreeUrlGenerator.class);

    when(siteTreeUrlGenerator.getRelativeUrl(any())).thenReturn("http://MockedUrl");

    shopCategoryProductToResponseConverter = new ShopCategoryProductToResponseConverter(
      s3ImageResizeService, shopProperties, siteTreeUrlGenerator);
  }

  @Test
  public void testUnknownCountry() {
    ProductPageLive productPageLive = new ProductPageLive();
    productPageLive.setId(2L);
    productPageLive.setClassName("ProductPage");
    productPageLive.setTitle("Mocked Product Page");
    productPageLive.setUrlSegment("mocked-product-page");

    KigaPageImage kigaPageImage = new KigaPageImage();
    kigaPageImage.setS3Image(new S3Image());
    productPageLive.setPreviewImage(kigaPageImage);

    Product product = new Product();
    product.setId(1L);
    ProductDetail productDetail = new ProductDetail();
    Country greatBritain = new Country();
    greatBritain.setCode("gb");
    productDetail.setCountry(greatBritain);
    product.setProductDetails(Collections.singletonList(productDetail));
    productPageLive.setProduct(product);

    ShopCategoryProductViewModel productPageResponse = shopCategoryProductToResponseConverter
      .convertToViewModel(productPageLive, "de");

    assertFalse(productPageResponse.isAvailableInCountry());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testIoExceptionIsHandled() throws IOException {
    ProductPageLive productPageLive = new ProductPageLive();
    productPageLive.setId(2L);
    productPageLive.setClassName("ProductPage");
    productPageLive.setTitle("Mocked Product Page");
    productPageLive.setUrlSegment("mocked-product-page");

    S3Image s3Image = new S3Image();
    KigaPageImage kigaPageImage = new KigaPageImage();
    kigaPageImage.setS3Image(s3Image);
    productPageLive.setPreviewImage(kigaPageImage);

    Country country = new Country();
    country.setCurrency("EUR");
    country.setCode("DE");

    ProductDetail productDetail = new ProductDetail();
    productDetail.setPriceNet(BigDecimal.TEN);
    productDetail.setVatProduct(BigDecimal.ONE);
    productDetail.setCountry(country);

    Product product = new Product();
    product.setId(20L);
    product.setProductDetails(Collections.singletonList(productDetail));
    productPageLive.setProduct(product);

    PaddedImageStrategy paddedImageStrategy = new PaddedImageStrategy(100, 100);

    when(s3ImageResizeService.getResizedImageUrl(any(), any()))
      .thenThrow(IOException.class);
    shopCategoryProductToResponseConverter.convertToViewModel(productPageLive, "DE");
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testIllegalArgumentExceptionIsHandled() throws IOException {
    ProductPageDraft productPageDraft = new ProductPageDraft();
    productPageDraft.setId(2L);
    productPageDraft.setClassName("ProductPage");
    productPageDraft.setTitle("Mocked Product Page");
    productPageDraft.setUrlSegment("mocked-product-page");

    S3Image s3Image = new S3Image();
    KigaPageImage kigaPageImage = new KigaPageImage();
    kigaPageImage.setS3Image(s3Image);
    productPageDraft.setPreviewImage(kigaPageImage);

    Country country = new Country();
    country.setCurrency("EUR");
    country.setCode("DE");

    ProductDetail productDetail = new ProductDetail();
    productDetail.setPriceNet(BigDecimal.TEN);
    productDetail.setVatProduct(BigDecimal.ONE);
    productDetail.setCountry(country);

    Product product = new Product();
    product.setId(20L);
    product.setProductDetails(Collections.singletonList(productDetail));
    productPageDraft.setProduct(product);

    when(s3ImageResizeService.getResizedImageUrl(any(), any()))
      .thenThrow(IllegalArgumentException.class);
    shopCategoryProductToResponseConverter.convertToViewModel(productPageDraft, "DE");
  }

  @Test
  public void testConversion() {
    ProductPageLive productPageLive = new ProductPageLive();
    productPageLive.setId(2L);
    productPageLive.setClassName("ProductPage");
    productPageLive.setTitle("Mocked Product Page");
    productPageLive.setUrlSegment("mocked-product-page");

    S3Image s3Image = new S3Image();
    KigaPageImage kigaPageImage = new KigaPageImage();
    kigaPageImage.setS3Image(s3Image);
    productPageLive.setPreviewImage(kigaPageImage);

    Country country = new Country();
    country.setCurrency("EUR");
    country.setCode("DE");

    ProductDetail productDetail = new ProductDetail();
    productDetail.setPriceNet(BigDecimal.TEN);
    productDetail.setVatProduct(BigDecimal.ONE.divide(BigDecimal.TEN));
    productDetail.setCountry(country);

    Product product = new Product();
    product.setId(20L);
    product.setProductDetails(Collections.singletonList(productDetail));
    productPageLive.setProduct(product);

    ShopCategoryProductViewModel productPageResponse = shopCategoryProductToResponseConverter
      .convertToViewModel(productPageLive, "DE");

    assertEquals(Long.valueOf(2L), productPageResponse.getId());
    assertEquals("ProductPage", productPageResponse.getClassName());
    assertEquals("Mocked Product Page", productPageResponse.getTitle());
    assertEquals("mocked-product-page", productPageResponse.getUrlSegment());
    assertEquals("ProductPageLive", productPageResponse.getResolvedType());
    assertEquals("http://MockedUrl", productPageResponse.getUrl());
    assertEquals(BigDecimal.valueOf(11).setScale(2), productPageResponse.getGrossPrice());
  }
}
