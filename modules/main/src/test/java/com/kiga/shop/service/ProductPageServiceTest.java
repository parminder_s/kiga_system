package com.kiga.shop.service;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.shop.ShopProperties;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.ProductDetail;
import com.kiga.shopcontent.domain.ProductPage;
import com.kiga.shopcontent.domain.draft.ProductPageDraft;
import com.kiga.shopcontent.domain.live.ProductPageLive;
import com.kiga.shopcontent.repository.ProductPageRepositoryProxy;
import com.kiga.shopcontent.service.ProductPageService;
import com.kiga.shopcontent.viewmodel.ShopCategoryViewModel;
import com.kiga.shopcontent.viewmodel.converter.ShopCategoryProductToResponseConverter;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * @author bbs
 * @since 9/6/16.
 */
public class ProductPageServiceTest {
  private static final int AMT_PER_CATEGORY = 13;
  private static final String COUNTRY = "de";

  @Test
  public void testGetProductsByCategory() {
    Country country = new Country();
    country.setCode("DE");
    ProductDetail productDetail = new ProductDetail();
    productDetail.setCountry(country);
    Product product = new Product();
    product.setProductDetails(Collections.singletonList(productDetail));

    ProductPage productPage1 = new ProductPageLive();
    productPage1.setProduct(product);

    ProductPage productPage2 = new ProductPageDraft();
    productPage2.setProduct(product);

    ProductPage productPage3 = new ProductPageLive();
    productPage3.setProduct(product);

    List<ProductPage> productPageList = new ArrayList<>();
    productPageList.add(productPage1);
    productPageList.add(productPage2);
    productPageList.add(productPage3);

    ShopCategoryViewModel shopCategoryResponse = new ShopCategoryViewModel();
    shopCategoryResponse.setId(9L);

    Pageable pageable = new PageRequest(0, AMT_PER_CATEGORY);

    ProductPageRepositoryProxy productPageRepositoryProxy = mock(ProductPageRepositoryProxy.class);
    when(productPageRepositoryProxy
      .getProductsByCategoryId(eq(shopCategoryResponse.getId()), eq(COUNTRY), eq(pageable)))
      .thenReturn(productPageList);

    ShopProperties shopProperties = mock(ShopProperties.class);
    when(shopProperties.getAmountPerCategory()).thenReturn(AMT_PER_CATEGORY);

    ShopCategoryProductToResponseConverter shopCategoryProductToResponseConverter = mock(
      ShopCategoryProductToResponseConverter.class);
    ProductPageService productPageService = new ProductPageService(shopProperties,
      productPageRepositoryProxy, shopCategoryProductToResponseConverter);

    productPageService.getProductsByCategory(shopCategoryResponse, COUNTRY);

    verify(productPageRepositoryProxy)
      .getProductsByCategoryId(eq(shopCategoryResponse.getId()), eq(COUNTRY), eq(pageable));
    verify(shopCategoryProductToResponseConverter).convertToViewModel(productPage1, COUNTRY);
    verify(shopCategoryProductToResponseConverter).convertToViewModel(productPage2, COUNTRY);
    verify(shopCategoryProductToResponseConverter).convertToViewModel(productPage3, COUNTRY);
  }
}
