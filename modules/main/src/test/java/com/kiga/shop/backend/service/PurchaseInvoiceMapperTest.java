package com.kiga.shop.backend.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.accounting.domain.Invoice;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.repository.CountryShopRepository;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;

/**
 * Created by rainerh on 22.02.17.
 */
public class PurchaseInvoiceMapperTest {
  @Test
  public void doTest() {
    Country country = new Country();
    country.setId(1L);
    country.setCode("at");
    Purchase purchase = new Purchase();
    purchase.setCountry(country);
    purchase.setInvoiceNr("Nr1");
    purchase.setInvoiceDate(Instant.parse("2017-02-10T00:00:00Z"));
    purchase.setLastname("Harrison");
    purchase.setEmail("john.harrison@k.tt4.at");
    purchase.setPriceGrossTotal(new BigDecimal("50.23"));
    purchase.setCurrency("EUR");
    purchase.setOrderNr(423L);
    purchase.setOrderDate(Instant.parse("2017-02-03T00:00:00Z"));

    CountryShop countryShop = new CountryShop();
    countryShop.setDaysToPayInvoice(5);
    CountryShopRepository countryShopRepository = mock(CountryShopRepository.class);
    when(countryShopRepository.findByCountryId(1)).thenReturn(countryShop);
    PurchaseInvoiceMapper purchaseInvoiceMapper = new PurchaseInvoiceMapper(countryShopRepository);
    Invoice invoice = purchaseInvoiceMapper.map(purchase);

    assertEquals("Nr1", invoice.getInvoiceNumber());
    assertEquals(Instant.parse("2017-02-15T00:00:00Z"), invoice.getDueDate().toInstant());
    assertEquals(Instant.parse("2017-02-10T00:00:00Z"), invoice.getInvoiceDate().toInstant());
    assertEquals("Harrison", invoice.getCustomerName());
    assertEquals("john.harrison@k.tt4.at", invoice.getCustomerEmail());
    assertEquals(new BigDecimal("50.23"), invoice.getAmount());
    assertEquals("EUR", invoice.getCurrency());
    assertFalse(invoice.isSettled());
    assertEquals("at", invoice.getCountryCode());
    assertEquals("423", invoice.getOrderNumber());
    assertEquals(Instant.parse("2017-02-03T00:00:00Z"), invoice.getOrderDate().toInstant());
    assertFalse(invoice.isDunningEmailSent());
  }
}
