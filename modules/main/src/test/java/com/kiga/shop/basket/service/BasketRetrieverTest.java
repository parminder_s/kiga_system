package com.kiga.shop.basket.service;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.security.domain.Member;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Purchase;
import com.kiga.shop.repository.CountryRepository;
import com.kiga.shop.repository.PurchaseRepositoryExt;
import org.junit.Test;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

public class BasketRetrieverTest {
  @Test
  public void testWithNonCrmMember() {
    PurchaseRepositoryExt repository = mock(PurchaseRepositoryExt.class);
    when(repository.findBySession(anyString())).thenReturn(Optional.empty());
    Member member = new Member();
    member.setId(5L);

    Optional<Purchase> optPurchase =
      new BasketRetriever(repository, null).find(Optional.of(member), "foo", Optional.of("bar"));

    verify(repository, never()).findNewByCustomer(anyLong());
  }

  @Test
  public void testWithNoBasket() {
    PurchaseRepositoryExt repository = mock(PurchaseRepositoryExt.class);
    when(repository.findNewByCustomer(anyLong())).thenReturn(Optional.empty());
    when(repository.findBySession(anyString())).thenReturn(Optional.empty());
    Member member = new Member();
    member.setCrmId("foo");
    member.setId(5L);

    Optional<Purchase> optPurchase =
      new BasketRetriever(repository, null).find(Optional.of(member), "foo", Optional.of("bar"));

    assertFalse(optPurchase.isPresent());
  }

  @Test
  public void testWithMemberAndCookie() {
    PurchaseRepositoryExt repository = mock(PurchaseRepositoryExt.class);
    when(repository.findBySession("foo")).thenReturn(Optional.empty());

    Optional<Purchase> optPurchase =
      new BasketRetriever(repository, null).find(Optional.empty(), "foo", Optional.empty());

    assertFalse(optPurchase.isPresent());
  }

  @Test
  public void testOrderingWithMultipleBaskets() {
    Purchase purchaseCustomer = new Purchase();
    purchaseCustomer.setLastEdited(Date.from(Instant.now().minusSeconds(100)));
    Purchase purchaseSession = new Purchase();
    purchaseSession.setLastEdited(Date.from(Instant.now()));
    Purchase purchaseCookie = new Purchase();
    purchaseCookie.setLastEdited(Date.from(Instant.now().minusSeconds(200)));

    PurchaseRepositoryExt repository = mock(PurchaseRepositoryExt.class);
    when(repository.findNewByCustomer(anyLong())).thenReturn(Optional.of(purchaseCustomer));
    when(repository.findBySession("foo")).thenReturn(Optional.of(purchaseSession));
    when(repository.findBySession("bar")).thenReturn(Optional.of(purchaseCookie));
    CountryRepository countryRepository = mock(CountryRepository.class);

    Member member = new Member();
    member.setCrmId("foo");
    member.setId(5L);

    Optional<Purchase> optPurchase = new BasketRetriever(repository, countryRepository)
      .find(Optional.of(member), "foo", Optional.of("bar"));

    assertEquals(purchaseSession, optPurchase.get());
  }

  @Test
  public void testWithSingleBasket() {
    Purchase purchase = new Purchase();
    purchase.setLastEdited(Date.from(Instant.now()));

    PurchaseRepositoryExt repository = mock(PurchaseRepositoryExt.class);
    when(repository.findBySession("foo")).thenReturn(Optional.of(purchase));
    when(repository.findBySession("bar")).thenReturn(Optional.empty());
    CountryRepository countryRepository = mock(CountryRepository.class);

    Optional<Purchase> optPurchase = new BasketRetriever(repository, countryRepository)
      .find(Optional.empty(), "foo", Optional.of("bar"));

    assertEquals(purchase, optPurchase.get());
  }

  @Test
  public void testCustomerSet() {
    Purchase purchase = mock(Purchase.class);
    when(purchase.getLastEdited()).thenReturn(new Date());
    Member member = new Member();
    member.setId(5L);
    member.setCrmId("crmid");
    member.setCountry("at");

    PurchaseRepositoryExt repository = mock(PurchaseRepositoryExt.class);
    when(repository.findBySession("foo")).thenReturn(Optional.of(purchase));
    CountryRepository countryRepository = mock(CountryRepository.class);
    Country country = new Country();
    when(countryRepository.findByCode("at")).thenReturn(country);

    new BasketRetriever(repository, countryRepository)
      .find(Optional.of(member), "foo", Optional.empty());

    verify(purchase).setLastEdited(any());
    verify(purchase).setCountry(country);
  }

  @Test
  public void testNonCustomerSetWithAnonymous() {
    Purchase purchase = mock(Purchase.class);
    when(purchase.getLastEdited()).thenReturn(new Date());

    PurchaseRepositoryExt repository = mock(PurchaseRepositoryExt.class);
    when(repository.findBySession("foo")).thenReturn(Optional.of(purchase));
    when(repository.findBySession("bar")).thenReturn(Optional.empty());
    CountryRepository countryRepository = mock(CountryRepository.class);

    new BasketRetriever(repository, countryRepository)
      .find(Optional.empty(), "foo", Optional.of("bar"));

    verify(purchase, never()).setLastEdited(any());
    verify(purchase, never()).setCountry(any());
  }

}
