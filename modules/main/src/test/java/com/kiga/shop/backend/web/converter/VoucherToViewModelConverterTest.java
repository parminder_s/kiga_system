package com.kiga.shop.backend.web.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import com.kiga.shop.backend.web.response.VoucherViewModel;
import com.kiga.shop.domain.Product;
import com.kiga.shop.domain.Voucher;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Schneider Florian on 28.11.17.
 */
public class VoucherToViewModelConverterTest {
  private static final String CODE = "CODE";
  private static final BigDecimal DISCOUNT_RATE = BigDecimal.valueOf(0.5);
  private static final Long PRODUCT_ID = 2L;
  private static final Long ID = 1L;
  private static final Date CREATED = new Date();
  private static final Date LAST_EDITED = new Date();
  private static final Date VALID_FROM = new Date();
  private static final Date VALID_TO = new Date();
  private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(
          "Y-M-dd HH:mm:ss");
  private VoucherToViewModelConverter voucherToViewModelConverter;

  /**
   * Prepare tests.
   */
  @Before
  public void prepareTests() {
    voucherToViewModelConverter = new VoucherToViewModelConverter();
  }

  @Test
  public void testConvertListWithNullElements() {
    List<VoucherViewModel> voucherViewModels = voucherToViewModelConverter
            .convertToResponse(Collections.singletonList(null));
    assertNull(voucherViewModels.get(0));
  }

  @Test
  public void testConvertNullList() {
    List<VoucherViewModel> voucherViewModels = voucherToViewModelConverter
            .convertToResponse((List<Voucher>) null);
    assertNull(voucherViewModels);
  }

  @Test
  public void testConvertList() {
    Voucher voucher = new Voucher();
    List<VoucherViewModel> voucherViewModels = voucherToViewModelConverter
            .convertToResponse(Collections.singletonList(voucher));
    assertNotNull(voucherViewModels.get(0));
  }

  @Test
  public void testBothMethodsGivesSameResults() {
    Voucher voucher = new Voucher();
    List<VoucherViewModel> voucherViewModels = voucherToViewModelConverter
            .convertToResponse(Collections.singletonList(voucher));
    VoucherViewModel voucherViewModel = voucherToViewModelConverter.convertToResponse(voucher);
    assertNotNull(voucherViewModel);
    assertNotNull(voucherViewModels.get(0));
    assertEquals(voucherViewModel, voucherViewModels.get(0));
  }

  @Test
  public void testConvertNullProduct() {
    VoucherViewModel voucherViewModel = voucherToViewModelConverter
            .convertToResponse((Voucher) null);
    assertNull(voucherViewModel);
  }

  @Test
  public void testConvertProduct() {
    Voucher voucher = new Voucher();
    voucher.setCode(CODE);
    voucher.setDiscountRate(DISCOUNT_RATE);
    Product product = Product.builder().productTitle("TestProduct").code("testproduct01").build();
    product.setId(PRODUCT_ID);
    voucher.setProduct(product);
    voucher.setId(ID);
    voucher.setCreated(CREATED);
    voucher.setLastEdited(LAST_EDITED);
    voucher.setValidFromDate(VALID_FROM.toInstant());
    voucher.setValidToDate(VALID_FROM.toInstant());
    VoucherViewModel voucherViewModel = voucherToViewModelConverter.convertToResponse(voucher);
    assertNotNull(voucherViewModel);
    assertEquals(ID, voucherViewModel.getId());
    assertEquals(CODE, voucherViewModel.getCode());
    assertEquals(DISCOUNT_RATE, voucherViewModel.getDiscountRate());
    assertEquals("TestProduct (testproduct01)", voucherViewModel.getProductDisplayString());
    assertEquals(PRODUCT_ID, voucherViewModel.getProductId());
    assertEquals(SIMPLE_DATE_FORMAT.format(CREATED), voucherViewModel.getCreatedOn());
    assertEquals(SIMPLE_DATE_FORMAT.format(LAST_EDITED), voucherViewModel.getLastEditedOn());
    assertEquals(SIMPLE_DATE_FORMAT.format(VALID_FROM),
            SIMPLE_DATE_FORMAT.format(voucherViewModel.getValidFrom()));
    assertEquals(SIMPLE_DATE_FORMAT.format(VALID_TO),
            SIMPLE_DATE_FORMAT.format(voucherViewModel.getValidTo()));
  }
}