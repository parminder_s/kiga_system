package com.kiga.shop.basket.service;

import static com.kiga.shop.domain.PurchaseStatus.New;
import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import com.kiga.main.locale.Locale;
import com.kiga.security.domain.Member;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.Purchase;
import java.util.ArrayList;
import java.util.Optional;
import org.junit.Test;

public class BasketFactoryExecutorTest {
  @Test
  public void testDefault() {
    Country country = Country.builder().currency("EUR").build();
    Member member = new Member();
    Optional<Member> optMember = Optional.of(member);

    Purchase createdPurchase =
        new BasketFactoryExecutor(mock(PricesSetter.class))
            .create(country, optMember, Locale.de, "foobar");

    Purchase purchase =
        Purchase.builder()
            .purchaseItems(new ArrayList<>())
            .status(New)
            .customer(member)
            .country(country)
            .currency("EUR")
            .currentStep("new")
            .sessionId("foobar")
            .locale(Locale.de)
            .build();
    purchase.setClassName("Purchase");

    assertThat(purchase, sameBeanAs(createdPurchase));
  }

  @Test
  public void testAnonymous() {
    Country country = Country.builder().currency("USD").build();
    Optional<Member> optMember = Optional.empty();

    Purchase createdPurchase =
        new BasketFactoryExecutor(mock(PricesSetter.class))
            .create(country, optMember, Locale.en, "usa");

    Purchase purchase =
        Purchase.builder()
            .purchaseItems(new ArrayList<>())
            .status(New)
            .customer(null)
            .country(country)
            .currency("USD")
            .currentStep("new")
            .sessionId("usa")
            .locale(Locale.en)
            .build();
    purchase.setClassName("Purchase");

    assertThat(purchase, sameBeanAs(createdPurchase).ignoring("created"));
  }
}
