package com.kiga.shop.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryShop;
import com.kiga.shop.repository.CountryShopRepository;
import com.kiga.shop.web.converter.CountryShopToViewModelConverter;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

/**
 * @author bbs
 * @since 2/18/17.
 */
public class CountryShopServiceTest {
  public static final long ID = 1L;
  private static final String ACCOUNT_NR = "ABC";
  private static final String BANK_NAME = "DEF";
  private static final String BIC = "GHI";
  private static final String IBAN = "JKL";
  private static final int DAYS_TO_PAY_INVOICE = 2;
  private static final BigDecimal MAX_PURCHASE_PRICE = BigDecimal.ONE;
  private static final BigDecimal FREE_SHIPPING_LIMIT = BigDecimal.TEN;
  private static final BigDecimal VAT_RATE_SHIPPING = BigDecimal.ZERO;
  private static final BigDecimal PRICE_SHIPPING_NET = BigDecimal.valueOf(100);
  private CountryShopService countryShopService;
  private CountryService countryServiceMock = mock(CountryService.class);
  private CountryShopRepository countryShopRepositoryMock = mock(CountryShopRepository.class);
  private CountryShopToViewModelConverter countryShopToViewModelConverterMock = mock(
    CountryShopToViewModelConverter.class);

  @Before
  public void beforeTests() {
    countryShopService = new CountryShopService(countryServiceMock, countryShopRepositoryMock,
      countryShopToViewModelConverterMock);
  }

  @Test
  public void testGetAll() {
    List<CountryShop> countryShops = Collections.singletonList(new CountryShop());
    when(countryShopRepositoryMock.findAll()).thenReturn(countryShops);
    countryShopService.getAll();
    verify(countryShopToViewModelConverterMock).convertToResponse(countryShops);
  }

  @Test
  public void testGetByCode() {
    Country country = new Country();
    CountryShop countryShop = new CountryShop();
    country.setCountryShop(countryShop);
    when(countryServiceMock.getEntityByCode("DE")).thenReturn(country);
    countryShopService.getByCode("DE");
    verify(countryShopToViewModelConverterMock).convertToResponse(countryShop);
  }
}
