package com.kiga.shop.web.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryLocale;
import com.kiga.shop.repository.CountryLocaleRepository;
import com.kiga.shop.web.response.CountryDropdownViewModel;
import org.junit.Before;
import org.junit.Test;
import org.modelmapper.ModelMapper;


/**
 * Created by asv on 18.01.17.
 */
public class CountryToDropdownViewModelConverterTest {
  private static final String TITLE = "Austria";
  private static final String CODE = "DEFG";
  private static final String CURRENCY = "USD";
  private static final String COUNTRYLOCALE = "Austria";

  private CountryLocaleRepository repository = mock(CountryLocaleRepository.class);
  private CountryToDropdownViewModelConverter countryToDropdownViewModelConverter =
    new CountryToDropdownViewModelConverter(repository, new ModelMapper());


  /**
   * setup repository.
   */
  @Before
  public void setupRepository() {
    CountryLocale countryLocale = new CountryLocale();
    countryLocale.setTitle(COUNTRYLOCALE);
    when(repository.findByCountryAndLocale(any(), any())).thenReturn(countryLocale);
  }

  @Test
  public void testConvertEntity() {
    Country country = new Country();
    country.setClassName(TITLE);
    country.setCode(CODE);
    country.setCurrency(CURRENCY);

    CountryDropdownViewModel countryDropdownViewModel = countryToDropdownViewModelConverter
      .convertToResponse(country);

    assertNotNull(countryDropdownViewModel);
    assertEquals(CODE, countryDropdownViewModel.getCode());
    assertEquals(CURRENCY, countryDropdownViewModel.getCurrency());
    assertEquals(TITLE, countryDropdownViewModel.getTitle());
  }
}
