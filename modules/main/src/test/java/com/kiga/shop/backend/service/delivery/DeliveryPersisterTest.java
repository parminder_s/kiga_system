package com.kiga.shop.backend.service.delivery;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import com.kiga.shop.domain.PurchaseDelivery;
import com.kiga.shop.domain.PurchaseDeliveryPurchaseProduct;
import com.kiga.spec.Persister;
import com.kiga.spec.Remover;
import org.junit.Test;

public class DeliveryPersisterTest {
  @Test
  public void testPersisterWithZeroProducts() {
    PurchaseDeliveryPurchaseProduct zeroProducts =
        PurchaseDeliveryPurchaseProduct.builder().amount(0).build();
    PurchaseDeliveryPurchaseProduct existingProduct =
        PurchaseDeliveryPurchaseProduct.builder().amount(1).build();
    PurchaseDelivery existingDelivery =
        PurchaseDelivery.builder().purchaseProducts(asList(existingProduct, zeroProducts)).build();
    PurchaseDeliveryPurchaseProduct newProduct1 = PurchaseDeliveryPurchaseProduct.builder().build();
    PurchaseDeliveryPurchaseProduct newProduct2 = PurchaseDeliveryPurchaseProduct.builder().build();
    PurchaseDelivery newDelivery =
        PurchaseDelivery.builder().purchaseProducts(asList(newProduct1, newProduct2)).build();

    Persister<PurchaseDelivery> deliveryPersister = mock(Persister.class);
    Persister<PurchaseDeliveryPurchaseProduct> productPersister = mock(Persister.class);
    Remover remover = mock(Remover.class);

    new DeliveryPersister(deliveryPersister, productPersister, remover)
        .persist(existingDelivery, newDelivery);

    verify(remover).remove(zeroProducts);
    verify(remover, never()).remove(existingProduct);

    verify(deliveryPersister).persist(newDelivery);
    verify(productPersister).persist(newProduct1);
    verify(productPersister).persist(newProduct2);
  }
}
