package com.kiga.shop;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;

import com.kiga.shop.service.CountryGroupService;
import com.kiga.shop.web.converter.CountryGroupToViewModelConverter;
import com.kiga.shop.web.converter.CountryToViewModelConverter;
import org.junit.Test;

/**
 * Created by rainerh on 03.04.16.
 */
public class ShopConfigTest {
  private ShopConfig shopConfig = new ShopConfig();

  @Test
  public void testCreateCountryGroupService() {
    assertThat(shopConfig.createCountryGroupService(null, null),
      instanceOf(CountryGroupService.class));
  }

  @Test
  public void testCreateCountryToViewModelConverter() {
    assertThat(shopConfig.createCountryToViewModelConverter(null, null),
      instanceOf(CountryToViewModelConverter.class));
  }

  @Test
  public void testCreateCountryGroupToViewModelConverter() {
    assertThat(shopConfig.createCountryGroupToViewModelConverter(),
      instanceOf(CountryGroupToViewModelConverter.class));
  }
}
