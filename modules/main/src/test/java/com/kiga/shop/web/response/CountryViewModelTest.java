package com.kiga.shop.web.response;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanEquals;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanHashCode;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * @author bbs
 * @since 10/16/16.
 */
public class CountryViewModelTest {
  @Test
  public void testBean() {
    assertThat(CountryViewModel.class, hasValidGettersAndSetters());
  }
}
