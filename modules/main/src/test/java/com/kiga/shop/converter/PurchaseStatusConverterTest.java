package com.kiga.shop.converter;

import static com.kiga.shop.domain.PurchaseStatus.Cancelled;
import static com.kiga.shop.domain.PurchaseStatus.Checkout;
import static com.kiga.shop.domain.PurchaseStatus.CreditVoucherFinalized;
import static com.kiga.shop.domain.PurchaseStatus.CreditVoucherPending;
import static com.kiga.shop.domain.PurchaseStatus.CsvExported;
import static com.kiga.shop.domain.PurchaseStatus.CsvFinished;
import static com.kiga.shop.domain.PurchaseStatus.FailedCheckout;
import static com.kiga.shop.domain.PurchaseStatus.InPayment;
import static com.kiga.shop.domain.PurchaseStatus.New;
import static com.kiga.shop.domain.PurchaseStatus.Ordered;
import static com.kiga.shop.domain.PurchaseStatus.OrderedPending;
import static com.kiga.shop.domain.PurchaseStatus.ReadyForShipping;
import static com.kiga.shop.domain.PurchaseStatus.Resumed;
import static com.kiga.shop.domain.PurchaseStatus.Shipped;
import static com.kiga.shop.domain.PurchaseStatus.ShippingInformationCreated;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import com.kiga.shop.domain.PurchaseStatus;
import java.util.List;
import org.junit.Test;

public class PurchaseStatusConverterTest {
  @Test
  public void convertToDatabaseColumnNull() {
    assertThat(new PurchaseStatusEnumConverter().convertToDatabaseColumn(null)).isNull();
  }

  @Test
  public void testAllValues() {
    List<PurchaseStatus> status =
        asList(
            Cancelled,
            Checkout,
            CreditVoucherFinalized,
            CreditVoucherPending,
            CsvExported,
            CsvFinished,
            FailedCheckout,
            InPayment,
            Ordered,
            OrderedPending,
            ShippingInformationCreated,
            ReadyForShipping,
            Resumed,
            Shipped,
            New);

    List<String> strings =
        asList(
            "cancelled",
            "checkout",
            "creditVoucherFinalized",
            "creditVoucherPending",
            "csvExported",
            "csvFinished",
            "failedCheckout",
            "inPayment",
            "ordered",
            "orderedPending",
            "shippingInformationCreated",
            "readyForShipping",
            "resumed",
            "shipped",
            "new");
    PurchaseStatusEnumConverter converter = new PurchaseStatusEnumConverter();

    for (int i = 0; i < status.size(); i++) {
      assertThat(converter.convertToDatabaseColumn(status.get(i))).isEqualTo(strings.get(i));
      assertThat(converter.convertToEntityAttribute(strings.get(i))).isEqualTo(status.get(i));
    }
  }

  @Test
  public void convertToEntityAttributeNul() {
    assertThat(new PurchaseStatusEnumConverter().convertToEntityAttribute(null)).isNull();
  }
}
