package com.kiga.newsletter;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import com.kiga.newsletter.backend.domain.SelectionStats;
import com.kiga.newsletter.backend.domain.zend.Newsletter;
import com.kiga.newsletter.backend.domain.zend.RecipientSelection;
import com.kiga.newsletter.backend.repository.RecipientRepository;
import com.kiga.newsletter.backend.repository.RecipientSelectionRepository;
import com.kiga.newsletter.backend.service.SelectionService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by mfit on 07.03.17.
 */
public class SelectionServiceTest {

  private SelectionService selectionService;
  private RecipientRepository recipientRepository;

  /**
   * Instantiate selection service with mocks.
   */
  @Before
  public void setUpTests() {
    this.recipientRepository = Mockito.mock(RecipientRepository.class);
    this.selectionService = new SelectionService(
      this.recipientRepository,
      Mockito.mock(RecipientSelectionRepository.class));
  }

  @Test
  public void testCreatesCorrectDefaultSelection() {
    RecipientSelection recipientSelection;
    Newsletter newsletter = new Newsletter();
    newsletter.setLocale("de");
    recipientSelection = this.selectionService.buildDefaultSelection(newsletter);
    Assert.assertEquals(recipientSelection.getLocale(), "de");

    newsletter.setLocale("it");
    recipientSelection = this.selectionService.buildDefaultSelection(newsletter);
    Assert.assertEquals(recipientSelection.getLocale(), "it");
  }

  @Test
  public void testGetSendingStats() {
    when(this.recipientRepository.count(any())).thenReturn(1271L);
    RecipientSelection recipientSelection = new RecipientSelection();
    SelectionStats selectionStats = this.selectionService
      .getSendingStats(recipientSelection);
    Assert.assertEquals(selectionStats.getPending(), 1271);
  }
}
