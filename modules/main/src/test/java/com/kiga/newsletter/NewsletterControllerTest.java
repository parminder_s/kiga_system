package com.kiga.newsletter;

import com.kiga.newsletter.backend.domain.zend.Newsletter;
import com.kiga.newsletter.backend.domain.zend.Recipient;
import com.kiga.newsletter.backend.domain.zend.RecipientSelection;
import com.kiga.newsletter.backend.domain.zend.SendTask;
import com.kiga.newsletter.backend.repository.NewsletterRepository;
import com.kiga.newsletter.backend.repository.QueueEntryRepository;
import com.kiga.newsletter.backend.repository.RecipientRepository;
import com.kiga.newsletter.backend.repository.SendTaskRepository;
import com.kiga.newsletter.backend.service.SelectionService;
import com.kiga.newsletter.backend.service.SendService;
import com.kiga.newsletter.backend.web.NewsletterController;
import com.kiga.newsletter.backend.web.request.SendRequest;
import com.kiga.newsletter.backend.web.request.SendTestRequest;
import com.kiga.newsletter.backend.web.response.NewsletterPreviewResponse;
import com.kiga.newsletter.backend.web.response.SendTaskResponse;
import com.kiga.security.services.SecurityService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 * TODO: incomplete.
 * Created by mfit on 15.03.17.
 */
public class NewsletterControllerTest {
  SecurityService securityService = Mockito.mock(SecurityService.class);
  RecipientRepository recipientRepository = Mockito.mock(RecipientRepository.class);
  NewsletterRepository newsletterRepository = Mockito.mock(NewsletterRepository.class);
  SendTaskRepository sendTaskRepository = Mockito.mock(SendTaskRepository.class);
  SelectionService selectionService = Mockito.mock(SelectionService.class);
  SendService sendService = Mockito.mock(SendService.class);
  QueueEntryRepository queueEntryRepository = Mockito.mock(QueueEntryRepository.class);

  NewsletterController newsletterController;
  private HttpServletRequest request = Mockito.mock(HttpServletRequest.class);

  /**
   * Instantiate newsletter controller.
   */
  @Before
  public void setUpTests() {
    newsletterController = new NewsletterController(
     securityService, recipientRepository, newsletterRepository,
      sendTaskRepository, selectionService, sendService);
  }

  @Test
  public void testGetNewsletters() {
    newsletterController.getNewsletters();

    Mockito.verify(newsletterRepository, Mockito.times(1))
      .findTop10ByOrderByPublishingDateDesc();
  }

  @Test
  public void testSend() {
    Newsletter newsletter = new Newsletter();
    SendTask sendTask = new SendTask();
    sendTask.setId(987L);
    List<Recipient> recipients = Arrays.asList(
      new Recipient("test@kiga.de", "de"),
      new Recipient("dummy@test.com", "en"));

    Mockito.when(sendService.createSendTask(newsletter))
      .thenReturn(sendTask);
    Mockito.when(newsletterRepository.findOne(123L)).thenReturn(newsletter);
    Mockito.when(selectionService.getSelectedRecipients(
      Mockito.anyListOf(RecipientSelection.class))).thenReturn(recipients.stream());

    SendTaskResponse sendTaskResponse = newsletterController
      .sendNewsletter(123L, new SendRequest());

    Mockito.verify(selectionService, Mockito.times(1))
      .getSelections(newsletter);

    Mockito.verify(sendService, Mockito.times(1))
      .send(Mockito.eq(sendTask), Mockito.any());

    Assert.assertEquals(sendTask.getId(), sendTaskResponse.getTaskId());
  }

  @Test
  public void testSendSingleAddress() {
    Newsletter newsletter = new Newsletter();
    SendTestRequest sendTestRequest = new SendTestRequest("test@tt4.at");
    SendTaskResponse sendTaskResponse = newsletterController
      .sendTestNewsletter(123L, sendTestRequest);

    Mockito.when(newsletterRepository.findOne(123L)).thenReturn(newsletter);
    Mockito.verify(sendService, Mockito.times(1))
      .send(Mockito.any(), Mockito.any());
  }

  @Test
  public void testGetPreview() {
    Newsletter newsletter = new Newsletter();
    newsletter.setBody("<html><body>Test-Content1234</body></html>");
    Mockito.when(newsletterRepository.findOne(124L)).thenReturn(newsletter);

    NewsletterPreviewResponse previewResponse = newsletterController.getPreview(124L);

    Assert.assertEquals("<html><body>Test-Content1234</body></html>",
      previewResponse.getContent());
  }
}
