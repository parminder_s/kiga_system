package com.kiga.newsletter;

import com.kiga.newsletter.backend.domain.QueueEntryStatus;
import com.kiga.newsletter.backend.domain.QueueStatus;
import com.kiga.newsletter.backend.domain.SelectionStats;
import com.kiga.newsletter.backend.domain.zend.Newsletter;
import com.kiga.newsletter.backend.domain.zend.QueueEntry;
import com.kiga.newsletter.backend.domain.zend.Recipient;
import com.kiga.newsletter.backend.domain.zend.SendTask;
import com.kiga.newsletter.backend.repository.QueueEntryRepository;
import com.kiga.newsletter.backend.repository.RecipientRepository;
import com.kiga.newsletter.backend.repository.SendTaskRepository;
import com.kiga.newsletter.backend.service.SendService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Stream;

/**
 * Created by mfit on 14.03.17.
 */
public class SendServiceTest {
  SendService sendService;
  RecipientRepository recipientRepository;
  QueueEntryRepository queueEntryRepository;
  SendTaskRepository sendTaskRepository;

  /**
   * Instantiate send service with mocks.
   */
  @Before
  public void setUpTests() {
    this.recipientRepository = Mockito.mock(RecipientRepository.class);
    this.queueEntryRepository = Mockito.mock(QueueEntryRepository.class);
    this.sendTaskRepository = Mockito.mock(SendTaskRepository.class);
    this.sendService = new SendService(
      this.recipientRepository, this.queueEntryRepository, this.sendTaskRepository);
  }

  @Test
  public void testCreateSendTask() {
    Newsletter newsletter = new Newsletter();
    newsletter.setName("JustATest");
    SendTask sendTask = this.sendService.createSendTask(newsletter);
    Assert.assertEquals(sendTask.getNewsletter(), newsletter);
    Mockito.verify(this.sendTaskRepository, Mockito.times(1))
      .save(Mockito.any(SendTask.class));
  }

  @Test
  public void testGetQueueStatus() {
    Newsletter newsletter = new Newsletter();
    newsletter.setName("JustATest");
    SelectionStats selectionStats;

    selectionStats = this.sendService.getQueueStatus(newsletter);
    Mockito.verify(this.queueEntryRepository, Mockito.times(1))
      .getStatusForNewsletterName("JustATest");
    Assert.assertEquals(selectionStats.getPending(), 0);
    Assert.assertEquals(selectionStats.getDone(), 0);
    Assert.assertEquals(selectionStats.getSkipped(), 0);
    Assert.assertEquals(selectionStats.getTotal(), 0);
    Assert.assertEquals(selectionStats.getError(), 0);

    Mockito.when(this.queueEntryRepository.getStatusForNewsletterName("JustATest"))
      .thenReturn(Arrays.asList(
        new QueueStatus(QueueEntryStatus.done, "JustATest", 3L),
        new QueueStatus(QueueEntryStatus.error, "JustATest", 5L),
        new QueueStatus(QueueEntryStatus.pending, "JustATest", 11L)
        ));
    selectionStats = this.sendService.getQueueStatus(newsletter);
    Assert.assertEquals(11, selectionStats.getPending());
    Assert.assertEquals(3, selectionStats.getDone());
    Assert.assertEquals(0, selectionStats.getSkipped());
    Assert.assertEquals(19, selectionStats.getTotal());
    Assert.assertEquals(5, selectionStats.getError());
  }

  @Test
  public void testSend() {
    Newsletter newsletter = new Newsletter();
    SendTask sendTask = new SendTask(newsletter);
    sendTask.setSendDate(
      Date.from(LocalDateTime.of(2016, 10, 12, 14, 34).atZone(ZoneId.systemDefault()).toInstant()));

    Recipient recipient = new Recipient("test-nl-recipient@gmx.net", "de");

    this.sendService.send(sendTask, Stream.of(recipient.getEmail()));

    ArgumentCaptor<QueueEntry> argument = ArgumentCaptor.forClass(QueueEntry.class);
    Mockito.verify(this.queueEntryRepository, Mockito.times(1))
      .save(argument.capture());
    Assert.assertEquals("test-nl-recipient@gmx.net", argument.getValue().getEmail());

    LocalDateTime queueEntrySendTime =
      LocalDateTime.ofInstant(argument.getValue().getSendDate().toInstant(),
                              ZoneId.systemDefault());
    Assert.assertEquals(2016, queueEntrySendTime.getYear());
    Assert.assertEquals(10, queueEntrySendTime.getMonthValue());
    Assert.assertEquals(12, queueEntrySendTime.getDayOfMonth());
    Assert.assertEquals(14, queueEntrySendTime.getHour());

    ArgumentCaptor<SendTask> sendTaskArgument = ArgumentCaptor.forClass(SendTask.class);
    Mockito.verify(this.sendTaskRepository, Mockito.times(2))
      .save(sendTaskArgument.capture());

    Assert.assertEquals(true, sendTaskArgument.getAllValues().get(1).getIsFinished());
    Assert.assertEquals(1L, (long) sendTaskArgument.getAllValues().get(1).getDone());
  }

  @Test
  public void testCleanQueueEntry() {
    Newsletter newsletter = new Newsletter();
    newsletter.setName("To be cleared");

    Mockito
      .when(this.queueEntryRepository.findByEmailAndNewsletter("test@kigaportal.com",
        "To be cleared"))
      .thenReturn(Arrays.asList(QueueEntry.createPendingEntry("test@kigaportal.com",
        "To be cleared")));

    this.sendService.cleanQueueEntry(newsletter, "test@kigaportal.com");

    ArgumentCaptor<QueueEntry> queueEntryArgument = ArgumentCaptor.forClass(QueueEntry.class);
    Mockito
      .verify(this.queueEntryRepository, Mockito.times(1))
      .delete(queueEntryArgument.capture());

    Assert.assertEquals(queueEntryArgument.getValue().getNewsletter(), "To be cleared");
    Assert.assertEquals(queueEntryArgument.getValue().getEmail(), "test@kigaportal.com");
  }
}
