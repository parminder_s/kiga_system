package com.kiga.kga.mapper;

import static org.assertj.core.api.Java6Assertions.assertThat;

import com.kiga.kga.api.model.KgaParentModel;
import com.kiga.kga.api.model.ParentRoleEnum;
import com.kiga.kga.domain.KgaParentToChild;
import com.kiga.kga.domain.ParentRole;
import org.junit.Test;

public class KgaParentToChildMapperTest {

  @Test
  public void modelToEntity() {
    KgaParentToChild kgaParentToChild =
        KgaParentToChildMapper.INSTANCE.modelToEntity(
            new KgaParentModel().id(1).role(ParentRoleEnum.MOTHER));
    assertThat(kgaParentToChild.getRole()).isEqualTo(ParentRole.MOTHER);
    assertThat(kgaParentToChild.getId()).isEqualTo(1);
  }
}
