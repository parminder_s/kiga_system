package com.kiga.kga.service;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.kga.domain.KgaKindergarten;
import com.kiga.kga.repository.KgaKindergartenPermissionRepository;
import com.kiga.kga.repository.KindergartenRepository;
import com.kiga.security.domain.Member;
import com.kiga.security.spring.SecurityUtil;
import com.kiga.web.security.KigaRoles;
import java.util.Optional;
import org.junit.Test;

public class KindergartenListerTest {
  @Test
  public void testForAdmin() {
    SecurityUtil securityUtil = mock(SecurityUtil.class);
    when(securityUtil.hasRole(KigaRoles.ADMIN)).thenReturn(true);
    when(securityUtil.getOptMember()).thenReturn(Optional.of(new Member()));

    KgaKindergarten kindergarten = new KgaKindergarten();
    KindergartenRepository repo = mock(KindergartenRepository.class);
    when(repo.findAll()).thenReturn(singletonList(kindergarten));
    KindergartenLister lister = new KindergartenLister(securityUtil, null, repo);

    assertThat(lister.list()).containsExactly(kindergarten);
  }

  @Test
  public void testForMember() {
    SecurityUtil securityUtil = mock(SecurityUtil.class);
    when(securityUtil.hasRole(KigaRoles.ADMIN)).thenReturn(false);
    Member member = new Member();
    when(securityUtil.getOptMember()).thenReturn(Optional.of(member));
    KgaKindergartenPermissionRepository permissionRepository =
        mock(KgaKindergartenPermissionRepository.class);
    KindergartenForEmployeeLister employeeLister = mock(KindergartenForEmployeeLister.class);
    KgaKindergarten kindergarten = new KgaKindergarten();
    when(employeeLister.list()).thenReturn(singletonList(kindergarten));

    KindergartenLister lister = new KindergartenLister(securityUtil, employeeLister, null);

    assertThat(lister.list()).containsExactly(kindergarten);
  }

  @Test
  public void testForAnonymous() {
    SecurityUtil securityUtil = mock(SecurityUtil.class);
    when(securityUtil.getOptMember()).thenReturn(Optional.ofNullable(null));

    assertThat(new KindergartenLister(securityUtil, null, null).list()).isEmpty();
  }
}
