package com.kiga.kga.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import com.kiga.kga.api.model.ChildJournalEntry;
import com.kiga.kga.api.model.FileModel;
import com.kiga.kga.domain.KgaChild;
import com.kiga.s3.domain.S3File;
import java.util.Collections;
import org.joda.time.DateTime;
import org.junit.Test;

public class ChildJournalMapperTest {

  @Test
  public void toEntiy() {
    com.kiga.kga.domain.ChildJournalEntry entity =
        ChildJournalEntryMapper.INSTANCE.toEntity(
            new ChildJournalEntry()
                .title("title")
                // .date("2019-01-01")
                .files(Collections.singletonList(new FileModel().id(1).name("name")))
                .content("content"));

    assertThat(entity.getContent()).isEqualTo("content");
    assertThat(entity.getTitle()).isEqualTo("title");
    assertThat(entity.getFiles()).size().isEqualTo(1);
    assertThat(entity.getFiles().iterator().next().getId()).isEqualTo(1);
    assertThat(entity.getFiles().iterator().next().getName()).isNull();
    assertThat(entity.getFiles().iterator().next().getUrl()).isNull();
    // assertThat(entity.getDate()).isEqualTo("2019-01-01");
  }

  @Test
  public void toModel() {
    S3File file = new S3File();
    file.setId(15L);
    file.setName("testfile.png");

    KgaChild child = new KgaChild();
    child.setId(12L);

    ChildJournalEntry model =
        ChildJournalEntryMapper.INSTANCE.toModel(
            com.kiga.kga.domain.ChildJournalEntry.builder()
                .title("title")
                .child(child)
                .content("content")
                .date(DateTime.parse("2019-01-01").toDate())
                .files(Collections.singleton(file))
                .build());

    assertThat(model.getTitle()).isEqualTo("title");
    assertThat(model.getContent()).isEqualTo("content");
    assertThat(model.getFiles()).size().isEqualTo(1);
    assertThat(model.getFiles().get(0).getId()).isEqualTo(15);
    assertThat(model.getFiles().get(0).getName()).isEqualTo("testfile.png");
    assertThat(model.getChildId()).isEqualTo(12L);
    // assertThat(model.getDate()).is("2019-01-01");
  }
}
