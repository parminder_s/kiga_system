package com.kiga.kga.service;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.kga.domain.KgaEmployee;
import com.kiga.kga.repository.KgaEmployeeRepository;
import com.kiga.security.domain.Member;
import com.kiga.security.spring.SecurityUtil;
import java.util.Collections;
import org.junit.Test;

public class KgaPermissionServiceTest {
  @Test
  public void testFinishRegistration() {
    MockPersister<KgaEmployee> persister = new MockPersister<>();
    KgaEmployeeRepository kgaEmployeeRepository = mock(KgaEmployeeRepository.class);
    KgaPermissionService service =
        KgaPermissionService.builder()
            .kgaEmployeePersister(persister)
            .kgaEmployeeRepository(kgaEmployeeRepository)
            .build();
    KgaEmployee employee = new KgaEmployee();
    when(kgaEmployeeRepository.findByEmail(eq("email")))
        .thenReturn(Collections.singletonList(employee));
    Member member = new Member();
    member.setEmail("email");
    service.finishRegistration(member);
    assertThat(persister.getValue()).isEqualTo(employee);
    assertThat(persister.getValue().getMember()).isEqualTo(member);
  }

  @Test
  public void testConstructor() {
    assertThat(new KgaPermissionService(null, null))
        .isExactlyInstanceOf(KgaPermissionService.class);
  }

  @Test
  public void testGetCurrentEmployee() {
    SecurityUtil securityUtil = mock(SecurityUtil.class);
    Member member = new Member();
    when(securityUtil.getMember()).thenReturn(member);

    KgaEmployeeRepository employeeRepository = mock(KgaEmployeeRepository.class);
    KgaEmployee employee = new KgaEmployee();
    when(employeeRepository.findByMember(member)).thenReturn(employee);

    KgaPermissionService service = new KgaPermissionService(employeeRepository, securityUtil);

    assertThat(service.getCurrentEmployee()).isSameAs(employee);
  }
}
