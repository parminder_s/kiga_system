package com.kiga.kga.mapper;

import static org.assertj.core.api.Java6Assertions.assertThat;

import com.kiga.kga.api.model.LanguageViewModel;
import com.kiga.kga.domain.Language;
import org.junit.Test;

public class LangaugeMapperTest {

  @Test
  public void testToViewModel() {
    Language language = Language.builder().code("de").build();
    language.setId(3L);
    LanguageViewModel viewModel = LangaugeMapper.INSTANCE.toViewModel(language);
    assertThat(viewModel.getCode()).isEqualTo("de");
    assertThat(viewModel.getId()).isEqualTo("3");
  }

}
