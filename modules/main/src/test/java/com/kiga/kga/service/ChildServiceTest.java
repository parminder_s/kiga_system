package com.kiga.kga.service;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.kga.domain.*;
import com.kiga.kga.repository.*;
import java.util.Collections;
import java.util.List;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

public class ChildServiceTest {

  @Test
  public void addParentToChild() {
    KgaParentToChildRepository kgaParentToChildRepository = mock(KgaParentToChildRepository.class);
    KgaChildRepository kgaChildRepository = mock(KgaChildRepository.class);
    KgaChild child = new KgaChild();
    when(kgaChildRepository.findOne(eq(3L))).thenReturn(child);
    KgaParentRepository kgaParentRepository = mock(KgaParentRepository.class);
    KgaParent kgaParent = new KgaParent();
    when(kgaParentRepository.findOne(eq(5L))).thenReturn(kgaParent);
    MockPersister<KgaParentToChild> persister = new MockPersister<>();
    ChildService childService =
        ChildService.builder()
            .kgaChildRepository(kgaChildRepository)
            .kgaParentToChildPersister(persister)
            .parentRepository(kgaParentRepository)
            .kgaParentToChildRepository(kgaParentToChildRepository)
            .build();

    ArgumentCaptor<KgaParentToChild> captor = ArgumentCaptor.forClass(KgaParentToChild.class);
    childService.addParentToChild(3L, 5L, ParentRole.MOTHER);

    assertThat(persister.getValue().getRole()).isEqualTo(ParentRole.MOTHER);
    assertThat(persister.getValue().getChild()).isEqualTo(child);
    assertThat(persister.getValue().getParent()).isEqualTo(kgaParent);
  }

  @Test
  public void testSave() {
    KgaKindergarten kindergarten = new KgaKindergarten();
    KgaChild child = new KgaChild();
    MockPersister<KgaChild> persister = new MockPersister<>();
    VaccinationRepository vaccinationRepository = mock(VaccinationRepository.class);
    DiseasesRepository diseasesRepository = mock(DiseasesRepository.class);
    AllergyRepository allergyRepository = mock(AllergyRepository.class);
    assertThat(child.getKindergarten()).isNull();
    ChildService service =
        ChildService.builder()
            .childPersister(persister)
            .vaccinationRepository(vaccinationRepository)
            .allergyRepository(allergyRepository)
            .diseasesRepository(diseasesRepository)
            .build();
    service.save(child, kindergarten);
    assertThat(persister.getValue()).isEqualTo(child);
    assertThat(persister.getValue().getKindergarten()).isEqualTo(kindergarten);
  }

  @Test
  public void testFindOne() {
    KgaChildRepository repo = mock(KgaChildRepository.class);
    ChildService.builder().kgaChildRepository(repo).build().findOne(1L);
    verify(repo, times(1)).findOne(eq(1L));
  }

  @Test
  public void testFindByGroup() {
    ChildInKgaGroupRepository childInKgaGroupRepository = mock(ChildInKgaGroupRepository.class);
    KgaChild child = new KgaChild();
    ChildInKgaGroup childInKgaGroup = new ChildInKgaGroup();
    childInKgaGroup.setChild(child);
    when(childInKgaGroupRepository.findWithChildByGroup_Id(eq(1L)))
        .thenReturn(Collections.singletonList(childInKgaGroup));
    List<KgaChild> value =
        ChildService.builder()
            .childInKgaGroupRepository(childInKgaGroupRepository)
            .build()
            .findByGroup(1);
    assertThat(value).contains(child);
    assertThat(value).size().isOne();
  }

  @Test
  public void testFindOneWithParents() {
    KgaChildRepository repo = mock(KgaChildRepository.class);
    ChildService.builder().kgaChildRepository(repo).build().findWithParentsById(1L);
    verify(repo, times(1)).findWithParentsById(eq(1L));
  }

  @Test
  public void testFindByKindergarten() {
    KgaChildRepository repo = mock(KgaChildRepository.class);
    ChildService.builder().kgaChildRepository(repo).build().findByKindergarten(1);
    verify(repo, times(1)).findByKindergarten_Id(eq(1L));
  }

  @Test
  public void testRemoveParent() {
    KgaParentToChildRepository repo = mock(KgaParentToChildRepository.class);
    ChildService.builder().kgaParentToChildRepository(repo).build().removeParentFromChild(1L);
    verify(repo, times(1)).delete(eq(1L));
  }

  @Test
  public void testSetParentRole() {
    KgaParentToChildRepository repo = mock(KgaParentToChildRepository.class);
    when(repo.findOne(any())).thenReturn(new KgaParentToChild());
    MockPersister<KgaParentToChild> persister = new MockPersister<>();
    KgaParentToChild kgaParentToChild = new KgaParentToChild();
    kgaParentToChild.setRole(ParentRole.MOTHER);
    ChildService.builder()
        .kgaParentToChildPersister(persister)
        .kgaParentToChildRepository(repo)
        .build()
        .saveParentRole(kgaParentToChild);
    verify(repo, times(1)).findOne(any());
    assertThat(persister.getValue().getRole()).isEqualTo(ParentRole.MOTHER);
  }

  /*
  only for test coverage.
   */
  @Test
  public void testConstructor() {
    KgaChildRepository repo = mock(KgaChildRepository.class);
    KgaParentToChildRepository parentToChildRepository = mock(KgaParentToChildRepository.class);

    new ChildService(repo, null, null, parentToChildRepository, null, null, null, null, null);
  }
}
