package com.kiga.kga.mapper;

import static com.kiga.kga.domain.DiseaseEnum.DIABETES;
import static org.assertj.core.api.Java6Assertions.assertThat;

import com.kiga.kga.api.model.*;
import com.kiga.kga.api.model.VaccinationEnum;
import com.kiga.kga.domain.*;
import com.kiga.kga.domain.AllergyEnum;
import com.kiga.shop.domain.Country;
import java.math.BigDecimal;
import java.util.Collections;
import org.joda.time.DateTime;
import org.junit.Test;

public class ChildMapperTest {

  @Test
  public void childModelToChildTest() {
    ChildModel childModel =
        new ChildModel()
            .firstname("Peter")
            .lastname("Kohout")
            .gender(GenderEnum.M)
            .citizenshipCountryId(new BigDecimal("5"))
            .birthDate("1994-11-01")
            .withMeal(true)
            .careType(CareTypeEnum.ALLDAY)
            .diseases(Collections.singletonList(DiseasesEnum.DIABETES))
            .allergies(Collections.singletonList(com.kiga.kga.api.model.AllergyEnum.BEE))
            .vaccinations(Collections.singletonList(VaccinationEnum.FSME))
            .religion(ReligionEnum.CATHOLIC)
            .numberSiblings(new BigDecimal("2"))
            .nativeLanguageId(new BigDecimal("12"));

    KgaChild child = ChildMapper.INSTANCE.childModelToChild(childModel);
    assertThat(child.getFirstname()).isEqualTo("Peter");
    assertThat(child.getLastname()).isEqualTo("Kohout");
    assertThat(child.getGender()).isEqualByComparingTo(Gender.MALE);
    assertThat(child.getNativeLanguage().getId()).isEqualTo(12L);
    assertThat(child.getBirthDate()).hasYear(1994);
    assertThat(child.getBirthDate()).hasMonth(11);
    assertThat(child.getBirthDate()).hasDayOfMonth(1);
    assertThat(child.getWithMeal()).isTrue();
    assertThat(child.getCitizenship().getId()).isEqualByComparingTo(5L);
    assertThat(child.getClassName()).isEqualTo("KgaChild");
    assertThat(child.getAllergies().iterator().next().getAllergy()).isEqualTo(AllergyEnum.BEE);
    assertThat(child.getDiseases().iterator().next().getDisease()).isEqualTo(DIABETES);
    assertThat(child.getNumberSiblings()).isEqualTo(2L);
    assertThat(child.getReligion()).isEqualByComparingTo(Religion.CATHOLIC);
    assertThat(child.getCareType()).isEqualByComparingTo(CareType.ALLDAY);
  }

  @Test
  public void childToChildModelTest() {
    KgaYear year = new KgaYear();
    year.setId(4L);

    KgaGroup group = KgaGroup.builder().year(year).build();
    group.setId(1L);

    KgaParent parent = KgaParent.builder().firstname("Evelyn").lastname("Kohout").build();
    parent.setId(2L);

    Country country = new Country();
    country.setId(248L);

    KgaParentToChild parentToChild = new KgaParentToChild();
    parentToChild.setRole(ParentRole.MOTHER);
    parentToChild.setParent(parent);
    parentToChild.setId(3L);

    Language language = new Language();
    language.setId(12L);

    KgaChild child = new KgaChild();
    ChildInKgaGroup childInKgaGroup = new ChildInKgaGroup();
    childInKgaGroup.setGroup(group);
    childInKgaGroup.setChild(child);

    child.setFirstname("Peter");
    child.setLastname("Kohout");
    child.setWithMeal(true);
    child.setDiseases(Collections.singleton(new Disease(DIABETES)));
    child.setAllergies(Collections.singleton(new Allergy(AllergyEnum.EGG)));
    child.setReligion(Religion.ISLAM);
    child.setGender(Gender.MALE);
    child.setCitizenship(country);
    child.setNumberSiblings(2L);
    child.setNativeLanguage(language);
    child.setBirthDate(DateTime.parse("1994-11-01").toDate());
    child.setChildInKgaGroups(Collections.singletonList(childInKgaGroup));
    child.setParents(Collections.singletonList(parentToChild));
    child.setId(5L);

    ChildModel childModel = ChildMapper.INSTANCE.childToChildModel(child);
    assertThat(childModel.getFirstname()).isEqualTo("Peter");
    assertThat(childModel.getLastname()).isEqualTo("Kohout");
    assertThat(childModel.getGender()).isEqualByComparingTo(GenderEnum.M);
    assertThat(childModel.getNativeLanguageId()).isEqualTo("12");
    assertThat(childModel.getGroupId()).isOne();
    assertThat(childModel.getBirthDate()).isEqualTo("1994-11-01");
    assertThat(childModel.getReligion()).isEqualByComparingTo(ReligionEnum.ISLAM);
    assertThat(childModel.getParents().get(0).getFirstname()).isEqualTo("Evelyn");
    assertThat(childModel.getParents().get(0).getLastname()).isEqualTo("Kohout");
    assertThat(childModel.getParents().get(0).getRole()).isEqualTo(ParentRoleEnum.MOTHER);
    assertThat(childModel.getParents().get(0).getId()).isEqualTo(2);
    assertThat(childModel.getCitizenshipCountryId()).isEqualByComparingTo("248");

    assertThat(childModel.getWithMeal()).isTrue();

    assertThat(childModel.getDiseases()).contains(DiseasesEnum.DIABETES);
    assertThat(childModel.getAllergies()).contains(com.kiga.kga.api.model.AllergyEnum.EGG);
    assertThat(childModel.getNumberSiblings()).isEqualByComparingTo("2");
  }

  @Test
  public void childArchiveToChildModelTest() {
    KgaYear year = new KgaYear();
    year.setId(4L);

    KgaGroup group = KgaGroup.builder().year(year).build();
    group.setId(1L);

    KgaParent parent = KgaParent.builder().firstname("Evelyn").lastname("Kohout").build();
    parent.setId(2L);

    Country country = new Country();
    country.setId(248L);

    KgaParentToChild_Versions parentToChild = new KgaParentToChild_Versions();
    parentToChild.setRole(ParentRole.MOTHER);
    parentToChild.setId(3L);

    Language language = new Language();
    language.setId(12L);

    KgaChild_Versions child = new KgaChild_Versions();
    child.setFirstname("Peter");
    child.setLastname("Kohout");
    child.setWithMeal(true);
    child.setDiseases(Collections.singleton(new Disease_Versions(DIABETES)));
    child.setAllergies(Collections.singleton(new Allergy_Versions(AllergyEnum.POLLEN)));
    child.setReligion(Religion.OTHER);
    child.setGender(Gender.MALE);
    child.setCitizenship(country);
    child.setNumberSiblings(2L);
    child.setNativeLanguage(language);
    child.setBirthDate(DateTime.parse("1994-11-01").toDate());
    child.setId(5L);

    ChildModel childModel = ChildMapper.INSTANCE.childToChildModel(child);
    assertThat(childModel.getFirstname()).isEqualTo("Peter");
    assertThat(childModel.getLastname()).isEqualTo("Kohout");
    assertThat(childModel.getGender()).isEqualByComparingTo(GenderEnum.M);
    assertThat(childModel.getNativeLanguageId()).isEqualTo("12");
    assertThat(childModel.getGroupId()).isNull();
    assertThat(childModel.getParents()).isEmpty();
    assertThat(childModel.getBirthDate()).isEqualTo("1994-11-01");
    assertThat(childModel.getReligion()).isEqualByComparingTo(ReligionEnum.OTHER);
    assertThat(childModel.getCitizenshipCountryId()).isEqualByComparingTo("248");

    assertThat(childModel.getWithMeal()).isTrue();

    assertThat(childModel.getDiseases()).contains(DiseasesEnum.DIABETES);
    assertThat(childModel.getAllergies()).contains(com.kiga.kga.api.model.AllergyEnum.POLLEN);
    assertThat(childModel.getNumberSiblings()).isEqualByComparingTo("2");
  }

  @Test
  public void childToChildArchive() {
    KgaGroup group = new KgaGroup();
    KgaChild kgaChild = new KgaChild();
    Country country = new Country();
    Language language = new Language();

    ChildInKgaGroup childInKgaGroup = new ChildInKgaGroup();
    childInKgaGroup.setId(15L);
    childInKgaGroup.setGroup(group);

    KgaParentToChild kgaParentToChild = new KgaParentToChild();
    kgaParentToChild.setRole(ParentRole.OTHER);
    kgaParentToChild.setParent(KgaParent.builder().build());
    kgaParentToChild.setChild(kgaChild);

    kgaChild.setFirstname("Peter");
    kgaChild.setLastname("Kohout");
    kgaChild.setGender(Gender.MALE);
    kgaChild.setChildInKgaGroups(Collections.singletonList(childInKgaGroup));
    kgaChild.setAllergies(Collections.singleton(new Allergy(AllergyEnum.LATEX)));
    kgaChild.setDiseases(Collections.singleton(new Disease(DIABETES)));
    kgaChild.setParents(Collections.singletonList(kgaParentToChild));
    kgaChild.setNotes("notes");
    kgaChild.setLactoseIntolerant(true);
    kgaChild.setWithMeal(false);
    kgaChild.setBirthDate(DateTime.parse("1994-11-01").toDate());
    kgaChild.setCitizenship(country);
    kgaChild.setNativeLanguage(language);
    kgaChild.setNumberSiblings(2L);
    kgaChild.setReligion(Religion.PROTESTANT);

    KgaChild_Versions archive = ChildMapper.INSTANCE.childToChildArchive(kgaChild);

    assertThat(archive.getFirstname()).isEqualTo("Peter");
    assertThat(archive.getLastname()).isEqualTo("Kohout");
    assertThat(archive.getGender()).isEqualTo(Gender.MALE);
    assertThat(archive.getAllergies().iterator().next().getAllergy()).isEqualTo(AllergyEnum.LATEX);
    assertThat(archive.getDiseases().iterator().next().getDisease()).isEqualTo(DIABETES);
    assertThat(archive.getNotes()).isEqualTo("notes");
    assertThat(archive.getLactoseIntolerant()).isTrue();
    assertThat(archive.getWithMeal()).isFalse();
    assertThat(archive.getBirthDate()).isEqualTo("1994-11-01");
    assertThat(archive.getCitizenship()).isEqualTo(country);
    assertThat(archive.getNumberSiblings()).isEqualTo(2);
    assertThat(archive.getReligion()).isEqualTo(Religion.PROTESTANT);
    // assertThat(archive.getParents().size()).isEqualTo(1);
    // assertThat(archive.getParents().get(0).getRole()).isEqualByComparingTo(ParentRole.OTHER);
  }
}
