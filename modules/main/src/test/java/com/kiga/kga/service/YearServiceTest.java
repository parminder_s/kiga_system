package com.kiga.kga.service;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;

import com.kiga.kga.domain.KgaChild;
import com.kiga.kga.domain.KgaKindergarten;
import com.kiga.kga.domain.KgaYear;
import com.kiga.kga.repository.KgaYearRepository;
import com.kiga.kga.repository.KindergartenRepository;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;

public class YearServiceTest {

  @Test
  public void testArchive() {
    KgaKindergarten kgaKindergarten =
        KgaKindergarten.builder()
            .children(Arrays.asList(new KgaChild(), new KgaChild(), new KgaChild()))
            .build();
    KgaYear year = KgaYear.builder().kindergarten(kgaKindergarten).build();
    KgaYearRepository kgaYearRepository = mock(KgaYearRepository.class);
    when(kgaYearRepository.findWithGroupsById(eq(1L))).thenReturn(year);
    ChildService childService = mock(ChildService.class);
    YearService yearService =
        YearService.builder()
            .kgaYearRepository(kgaYearRepository)
            .childService(childService)
            .build();

    yearService.archive(1L);

    assertThat(year.isArchived()).isTrue();
    verify(kgaYearRepository, times(1)).save(eq(year));
  }

  @Test
  public void testFindOne() {
    KgaYearRepository kgaYearRepository = mock(KgaYearRepository.class);
    YearService yearService = YearService.builder().kgaYearRepository(kgaYearRepository).build();
    yearService.findOne(1L);
    verify(kgaYearRepository, times(1)).findOne(eq(1L));
  }

  @Test
  public void testFindByKindergarten() {
    KgaYearRepository kgaYearRepository = mock(KgaYearRepository.class);
    YearService yearService = YearService.builder().kgaYearRepository(kgaYearRepository).build();
    yearService.findByKindergarten(1L);
    verify(kgaYearRepository, times(1)).findByKindergarten_Id(eq(1L));
  }

  @Test
  public void testSave() {
    MockPersister<KgaYear> persister = new MockPersister<>();
    KindergartenRepository kindergartenRepository = mock(KindergartenRepository.class);

    KgaKindergarten kindergarten = KgaKindergarten.builder().build();
    when(kindergartenRepository.findOne(eq(1L))).thenReturn(kindergarten);
    YearService yearService =
        YearService.builder()
            .yearPersister(persister)
            .kindergartenRepository(kindergartenRepository)
            .build();

    KgaYear toSave = KgaYear.builder().build();
    yearService.save(toSave, 1L);
    assertThat(persister.getValue().getKindergarten()).isEqualTo(kindergarten);
    assertThat(persister.getValue()).isEqualTo(toSave);
  }

  @Test
  public void testImport() {
    KgaYear year = KgaYear.builder().build();
    KgaYearRepository kgaYearRepository = mock(KgaYearRepository.class);
    when(kgaYearRepository.findOne(eq(1L))).thenReturn(year);
    KgaGroupService kgaGroupService = mock(KgaGroupService.class);
    List<Long> ids = Arrays.asList(1L, 2L, 3L, 4L, 5L);
    YearService yearService =
        YearService.builder()
            .kgaYearRepository(kgaYearRepository)
            .kgaGroupService(kgaGroupService)
            .build();
    yearService.importFromOtherYear(1L, ids);
    verify(kgaGroupService, times(1)).importToYear(eq(year), eq(ids));
  }

  /*
  only for test coverage.
   */
  @Test
  public void testConstructor() {
    new YearService(null, null, null, null);
  }
}
