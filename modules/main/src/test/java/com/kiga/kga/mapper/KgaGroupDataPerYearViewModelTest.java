package com.kiga.kga.mapper;

import static org.assertj.core.api.Java6Assertions.assertThat;

import com.kiga.kga.api.model.KgaGroupViewModel;
import com.kiga.kga.domain.KgaGroup;
import com.kiga.kga.domain.KgaYear;
import org.junit.Test;

public class KgaGroupDataPerYearViewModelTest {
  @Test
  public void entityToModel() {
    KgaYear year = new KgaYear();
    year.setId(2L);
    KgaGroup kgaGroupDataPerYear = KgaGroup.builder()
      .name("group")
      .build();

    kgaGroupDataPerYear.setId(1L);

    KgaGroupViewModel model = KgaGroupViewModelMapper.INSTANCE
      .kgaGroupToViewModel(kgaGroupDataPerYear);
    assertThat(model.getId()).isEqualTo(1);
    assertThat(model.getName()).isEqualTo("group");
  }
}
