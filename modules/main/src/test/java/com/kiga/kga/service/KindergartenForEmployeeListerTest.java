package com.kiga.kga.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.kga.domain.KgaEmployee;
import com.kiga.kga.domain.KgaKindergarten;
import com.kiga.kga.domain.KgaKindergartenPermission;
import com.kiga.kga.repository.KgaKindergartenPermissionRepository;
import java.util.Collections;
import org.junit.Test;

public class KindergartenForEmployeeListerTest {
  @Test
  public void testList() {
    KgaEmployee employee = new KgaEmployee();
    KgaKindergarten kindergarten = new KgaKindergarten();
    KgaKindergartenPermission permission = new KgaKindergartenPermission();
    permission.setKindergarten(kindergarten);
    permission.setEmployee(employee);

    KgaKindergartenPermissionRepository repository =
        mock(KgaKindergartenPermissionRepository.class);
    when(repository.findByEmployee(employee)).thenReturn(Collections.singletonList(permission));
    KgaPermissionService service = mock(KgaPermissionService.class);
    when(service.getCurrentEmployee()).thenReturn(employee);

    KindergartenForEmployeeLister getter = new KindergartenForEmployeeLister(repository, service);

    assertThat(getter.list()).containsExactly(kindergarten);
  }
}
