package com.kiga.kga.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import com.kiga.kga.api.model.EmployeeModel;
import com.kiga.kga.api.model.EmploymentContractEnum;
import com.kiga.kga.api.model.EmploymentTypeEnum;
import com.kiga.kga.api.model.GenderEnum;
import com.kiga.kga.api.model.KigaAddress;
import com.kiga.kga.domain.EmploymentContract;
import com.kiga.kga.domain.EmploymentType;
import com.kiga.kga.domain.Gender;
import com.kiga.kga.domain.KgaEmployee;
import com.kiga.shop.domain.Country;
import java.math.BigDecimal;
import org.joda.time.DateTime;
import org.junit.Test;

public class KgaEmployeeMapperTest {

  @Test
  public void modelToEntiyt() {
    KgaEmployee employee =
        KgaEmployeeMapper.INSTANCE.modelToEntiyt(
            new EmployeeModel()
                .email("mail")
                .firstname("peter")
                .lastname("kohout")
                .employmentType(EmploymentTypeEnum.DAY_DADDY)
                .parentalLeave(true)
                .phoneNumber("12345")
                .socialSecurityNumber("55555")
                .gender(GenderEnum.M)
                .employmentContract(EmploymentContractEnum.PARTTIME)
                .employmentContractNote("50%")
                .address(
                    new KigaAddress()
                        .street("street")
                        .zipCode(new BigDecimal("9120"))
                        .floor(new BigDecimal("1"))
                        .doorNumber(new BigDecimal("2"))
                        .city("Klagenfurt")
                        .countryId(new BigDecimal("123")))
                .birthDate("2018-01-01"));

    assertThat(employee.getEmail()).isEqualTo("mail");
    assertThat(employee.getFirstname()).isEqualTo("peter");
    assertThat(employee.getLastname()).isEqualTo("kohout");
    assertThat(employee.getEmploymentType()).isEqualTo(EmploymentType.DAY_DADDY);
    assertThat(employee.isParentalLeave()).isTrue();
    assertThat(employee.getSocialSecurityNumber()).isEqualTo("55555");
    assertThat(employee.getPhoneNumber()).isEqualTo("12345");
    assertThat(employee.getBirthDate()).isEqualTo("2018-01-01");
    assertThat(employee.getEmploymentContract()).isEqualByComparingTo(EmploymentContract.PARTTIME);
    assertThat(employee.getEmploymentContractNote()).isEqualTo("50%");
    assertThat(employee.getStreet()).isEqualTo("street");
    assertThat(employee.getZip()).isEqualTo("9120");
    assertThat(employee.getFloor()).isEqualTo("1");
    assertThat(employee.getDoorNumber()).isEqualTo("2");
    assertThat(employee.getCity()).isEqualTo("Klagenfurt");
    assertThat(employee.getCountry().getId()).isEqualTo(123L);
  }

  @Test
  public void entityToModel() {
    Country country = new Country();
    country.setId(123L);
    EmployeeModel model =
        KgaEmployeeMapper.INSTANCE.entityToModel(
            KgaEmployee.builder()
                .email("mail")
                .firstname("peter")
                .lastname("kohout")
                .employmentType(EmploymentType.DAY_DADDY)
                .parentalLeave(true)
                .phoneNumber("12345")
                .socialSecurityNumber("55555")
                .gender(Gender.MALE)
                .birthDate(DateTime.parse("2018-01-01").toDate())
                .employmentContract(EmploymentContract.PARTTIME)
                .employmentContractNote("50%")
                .street("street")
                .zip("9120")
                .floor("1")
                .doorNumber("2")
                .city("Klagenfurt")
                .country(country)
                .build());

    assertThat(model.getEmail()).isEqualTo("mail");
    assertThat(model.getFirstname()).isEqualTo("peter");
    assertThat(model.getLastname()).isEqualTo("kohout");
    assertThat(model.getEmploymentType()).isEqualTo(EmploymentTypeEnum.DAY_DADDY);
    assertThat(model.getParentalLeave()).isTrue();
    assertThat(model.getSocialSecurityNumber()).isEqualTo("55555");
    assertThat(model.getPhoneNumber()).isEqualTo("12345");
    assertThat(model.getBirthDate()).isEqualTo("2018-01-01");
    assertThat(model.getEmploymentContract()).isEqualByComparingTo(EmploymentContractEnum.PARTTIME);
    assertThat(model.getEmploymentContractNote()).isEqualTo("50%");
    assertThat(model.getAddress().getStreet()).isEqualTo("street");
    assertThat(model.getAddress().getZipCode()).isEqualTo("9120");
    assertThat(model.getAddress().getFloor()).isEqualTo("1");
    assertThat(model.getAddress().getDoorNumber()).isEqualTo("2");
    assertThat(model.getAddress().getCity()).isEqualTo("Klagenfurt");
    assertThat(model.getAddress().getCountryId()).isEqualTo("123");
  }
}
