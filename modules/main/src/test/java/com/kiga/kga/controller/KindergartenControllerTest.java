package com.kiga.kga.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.kga.api.model.KindergartenModel;
import com.kiga.kga.domain.KgaKindergarten;
import com.kiga.kga.mapper.KindergartenMapper;
import com.kiga.kga.service.KindergartenLister;
import java.util.Collections;
import org.junit.Test;

public class KindergartenControllerTest {

  @Test
  public void listKindergarten() {
    KindergartenLister lister = mock(KindergartenLister.class);
    lister.list();
    KgaKindergarten kindergarten = new KgaKindergarten();
    kindergarten.setName("Kindergarten St. Georgen");
    when(lister.list()).thenReturn(Collections.singletonList(kindergarten));
    KindergartenMapper mapper = mock(KindergartenMapper.class);
    KindergartenModel model = new KindergartenModel();
    when(mapper.kindergartenToKindergartenModel(kindergarten)).thenReturn(model);

    KindergartenController controller = new KindergartenController(null, lister, mapper);

    assertThat(controller.listKindergarten().getBody()).containsExactly(model);
  }
}
