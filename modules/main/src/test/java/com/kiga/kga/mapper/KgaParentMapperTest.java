package com.kiga.kga.mapper;

import static org.assertj.core.api.Java6Assertions.assertThat;

import com.kiga.kga.api.model.KgaParentModel;
import com.kiga.kga.domain.KgaParent;
import org.junit.Test;

public class KgaParentMapperTest {

  @Test
  public void kgaParentModelToKgaParent() {
    KgaParentModel kgaParentModel =
        KgaParentMapper.INSTANCE.kgaParentToKgaParentModel(
            KgaParent.builder()
                .firstname("Evelyn")
                .lastname("Kohout")
                .email("evi@kohout.at")
                .phoneNumber("0660123123")
                .legalGuardian(true)
                .emergencyAuthorized(true)
                .pickupAuthorized(true)
                .build());

    assertThat(kgaParentModel.getFirstname()).isEqualTo("Evelyn");
    assertThat(kgaParentModel.getLastname()).isEqualTo("Kohout");
    assertThat(kgaParentModel.getEmail()).isEqualTo("evi@kohout.at");
    assertThat(kgaParentModel.getPhoneNumber()).isEqualTo("0660123123");
    assertThat(kgaParentModel.getLegalGuardian()).isTrue();
    assertThat(kgaParentModel.getPickupAuthorized()).isTrue();
    assertThat(kgaParentModel.getEmergencyAuthorized()).isTrue();
  }

  @Test
  public void kgaParentToKgaParentModel() {
    KgaParent kgaParent =
        KgaParentMapper.INSTANCE.kgaParentModelToKgaParent(
            new KgaParentModel()
                .firstname("Evelyn")
                .lastname("Kohout")
                .email("evi@kohout.at")
                .phoneNumber("0660123123")
                .legalGuardian(true)
                .emergencyAuthorized(true)
                .pickupAuthorized(true));

    assertThat(kgaParent.getFirstname()).isEqualTo("Evelyn");
    assertThat(kgaParent.getLastname()).isEqualTo("Kohout");
    assertThat(kgaParent.getEmail()).isEqualTo("evi@kohout.at");
    assertThat(kgaParent.getPhoneNumber()).isEqualTo("0660123123");
    assertThat(kgaParent.isLegalGuardian()).isTrue();
    assertThat(kgaParent.isPickupAuthorized()).isTrue();
    assertThat(kgaParent.isEmergencyAuthorized()).isTrue();
  }
}
