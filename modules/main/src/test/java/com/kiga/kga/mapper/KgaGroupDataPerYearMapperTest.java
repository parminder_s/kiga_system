package com.kiga.kga.mapper;

import static org.assertj.core.api.Java6Assertions.assertThat;

import com.kiga.kga.api.model.KgaGroupModel;
import com.kiga.kga.domain.KgaGroup;
import com.kiga.kga.domain.KgaYear;
import java.util.Collections;
import org.junit.Test;

public class KgaGroupDataPerYearMapperTest {

  @Test
  public void entityToModel() {
    KgaYear year = new KgaYear();
    year.setId(2L);
    KgaGroup kgaGroupDataPerYear =
        KgaGroup.builder().year(year).email("email").name("name").room("room").build();
    kgaGroupDataPerYear.setId(1L);
    kgaGroupDataPerYear.setChildInKgaGroup(Collections.singletonList(null));
    KgaGroupModel model = KgaGroupMapper.INSTANCE.kgaGroupToKgaGroupModel(kgaGroupDataPerYear);
    assertThat(model.getId()).isOne();
    assertThat(model.getName()).isEqualTo("name");
    assertThat(model.getEmail()).isEqualTo("email");
    assertThat(model.getRoom()).isEqualTo("room");
    assertThat(model.getYearId()).isEqualTo(2);
    assertThat(model.getCountChildren()).isEqualTo(1);
  }

  @Test
  public void modelToEntity() {
    KgaGroupModel model = new KgaGroupModel().id(1).email("email").name("name").room("room");
    KgaGroup kgaGroupDataPerYear = KgaGroupMapper.INSTANCE.kgaGroupModelToKgaGroup(model);
    assertThat(kgaGroupDataPerYear.getId()).isOne();
    assertThat(kgaGroupDataPerYear.getName()).isEqualTo("name");
    assertThat(kgaGroupDataPerYear.getEmail()).isEqualTo("email");
    assertThat(kgaGroupDataPerYear.getRoom()).isEqualTo("room");
  }
}
