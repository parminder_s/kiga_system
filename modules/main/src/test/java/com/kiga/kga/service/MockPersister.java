package com.kiga.kga.service;

import com.kiga.spec.Persister;
import lombok.Getter;

@Getter
public class MockPersister<T> implements Persister<T> {
  T value;

  @Override
  public T persist(T entity) {
    this.value = entity;
    return entity;
  }
}
