package com.kiga.kga.mapper;

import static org.assertj.core.api.Java6Assertions.assertThat;

import com.kiga.kga.api.model.CountryViewModel;
import com.kiga.main.locale.Locale;
import com.kiga.shop.domain.Country;
import com.kiga.shop.domain.CountryLocale;
import org.junit.Test;

public class CountryMapperTest {

  @Test
  public void toViewModel() {
    CountryLocale country =
        CountryLocale.builder()
            .country(Country.builder().code("at").build())
            .title("Österreich")
            .locale(Locale.de)
            .build();
    country.getCountry().setId(3L);
    country.setId(5L);
    CountryViewModel viewModel = CountryMapper.INSTANCE.toViewModel(country);
    assertThat(viewModel.getCode()).isEqualTo("at");
    assertThat(viewModel.getId()).isEqualTo("3");
    assertThat(viewModel.getTitle()).isEqualTo("Österreich");
  }
}
