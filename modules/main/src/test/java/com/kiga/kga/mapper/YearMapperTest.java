package com.kiga.kga.mapper;

import static org.assertj.core.api.Java6Assertions.assertThat;

import com.kiga.kga.api.model.YearModel;
import com.kiga.kga.domain.KgaKindergarten;
import com.kiga.kga.domain.KgaYear;
import java.math.BigDecimal;
import org.joda.time.DateTime;
import org.junit.Test;

public class YearMapperTest {
  @Test
  public void entityToModel() {
    KgaKindergarten kindergarten = new KgaKindergarten();
    kindergarten.setId(2L);
    KgaYear year =
        KgaYear.builder()
            .archived(true)
            .startDate(DateTime.parse("2018-01-01").toDate())
            .endDate(DateTime.parse("2018-12-31").toDate())
            .kindergarten(kindergarten)
            .build();
    year.setId(1L);
    YearModel model = YearMapper.INSTANCE.yearToYearModel(year);
    assertThat(model.getId()).isEqualTo(1);
    assertThat(model.getStartDate()).isEqualTo("2018-01-01");
    assertThat(model.getEndDate()).isEqualTo("2018-12-31");
    assertThat(model.getKindergartenId()).isEqualTo("2");
    assertThat(model.getArchived()).isTrue();
  }

  @Test
  public void entityToModelArchiveNull() {
    assertThat(
            YearMapper.INSTANCE
                .yearToYearModel(KgaYear.builder().archived(false).build())
                .getArchived())
        .isFalse();
  }

  @Test
  public void modelToEntity() {
    YearModel yearModel =
        new YearModel()
            .id(1)
            .startDate("2018-01-01")
            .endDate("2018-12-31")
            .kindergartenId(new BigDecimal("2"))
            .archived(true);
    KgaYear year = YearMapper.INSTANCE.yearModelToYear(yearModel);
    assertThat(year.getId()).isEqualTo(1L);
    assertThat(year.getStartDate()).isEqualTo("2018-01-01");
    assertThat(year.getEndDate()).isEqualTo("2018-12-31");
    assertThat(year.isArchived()).isTrue();
  }

  @Test
  public void modelToEntityArchiveNull() {
    assertThat(YearMapper.INSTANCE.yearModelToYear(new YearModel().archived(null)).isArchived())
        .isFalse();
  }
}
