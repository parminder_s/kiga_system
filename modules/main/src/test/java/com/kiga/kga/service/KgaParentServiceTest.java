package com.kiga.kga.service;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.kga.api.model.ParentRoleEnum;
import com.kiga.kga.domain.KgaChild;
import com.kiga.kga.domain.KgaKindergarten;
import com.kiga.kga.domain.KgaParent;
import com.kiga.kga.repository.KgaParentRepository;
import org.junit.Test;

public class KgaParentServiceTest {

  @Test
  public void testSave() {
    MockPersister<KgaParent> persister = new MockPersister<>();
    KgaParentService service = KgaParentService.builder().parentPerister(persister).build();
    KgaParent parent = new KgaParent();
    service.save(parent);
    assertThat(persister.getValue()).isEqualTo(parent);
  }

  @Test
  public void testCreate() {
    MockPersister<KgaParent> persister = new MockPersister<>();
    ChildService childService = mock(ChildService.class);
    KgaKindergarten kgaKindergarten = KgaKindergarten.builder().build();
    KgaChild child = new KgaChild();
    child.setKindergarten(kgaKindergarten);
    when(childService.findOne(eq(1L))).thenReturn(child);
    KgaParentService service =
        KgaParentService.builder().childService(childService).parentPerister(persister).build();

    KgaParent parent = new KgaParent();
    service.saveOrCreate(parent, 1L, ParentRoleEnum.FATHER);

    assertThat(persister.getValue()).isEqualTo(parent);
    assertThat(persister.getValue().getKindergarten()).isEqualTo(kgaKindergarten);
  }

  @Test
  public void testFindByKindergarten() {
    KgaParentRepository parentRepository = mock(KgaParentRepository.class);
    KgaParentService service =
        KgaParentService.builder().parentRepository(parentRepository).build();
    service.findByKindergarten(1L);
    verify(parentRepository, times(1)).findByKindergarten_Id(eq(1L));
  }

  @Test
  public void testFindOne() {
    KgaParentRepository parentRepository = mock(KgaParentRepository.class);
    KgaParentService service =
        KgaParentService.builder().parentRepository(parentRepository).build();
    KgaParent parent = new KgaParent();
    when(parentRepository.findOne(eq(5L))).thenReturn(parent);
    assertThat(service.findOne(5L)).isEqualTo(parent);
  }

  /*
  only for coverage.
  */
  @Test
  public void testConstructor() {
    new KgaParentService(null, null, null);
  }
}
