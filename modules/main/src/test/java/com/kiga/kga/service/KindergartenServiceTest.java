package com.kiga.kga.service;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.kga.domain.KgaChild;
import com.kiga.kga.domain.KgaKindergarten;
import com.kiga.kga.repository.KindergartenRepository;
import com.kiga.shop.domain.Country;
import com.kiga.shop.repository.CountryRepository;
import java.util.Collections;
import java.util.List;
import org.junit.Ignore;
import org.junit.Test;

public class KindergartenServiceTest {

  @Test
  @Ignore
  public void testSave() {
    CountryRepository countryRepository = mock(CountryRepository.class);
    when(countryRepository.findOne(eq(1L))).thenReturn(Country.builder().code("at").build());
    MockPersister<KgaKindergarten> persister = new MockPersister<>();
    KindergartenService service =
        KindergartenService.builder()
            .countryRepository(countryRepository)
            .kindergartenPersister(persister)
            .build();
    KgaKindergarten kgaKindergarten = new KgaKindergarten();
    kgaKindergarten.setId(1L);
    service.save(kgaKindergarten, 1L);
    assertThat(persister.getValue().getCountry().getCode()).isEqualTo("at");
  }

  @Test
  public void testSaveChild() {
    KindergartenRepository kindergartenRepository = mock(KindergartenRepository.class);
    KgaKindergarten kindergarten = KgaKindergarten.builder().build();
    when(kindergartenRepository.findOne(eq(1L))).thenReturn(kindergarten);

    ChildService childService = mock(ChildService.class);
    KgaChild kgaChild = new KgaChild();

    KindergartenService kindergartenService =
        KindergartenService.builder()
            .kindergartenRepository(kindergartenRepository)
            .childService(childService)
            .build();
    kindergartenService.saveChild(kgaChild, 1);
    verify(childService, times(1)).save(kgaChild, kindergarten);
  }

  @Test
  public void testFindOne() {
    KindergartenRepository kindergartenRepository = mock(KindergartenRepository.class);
    KgaKindergarten kindergarten = KgaKindergarten.builder().build();
    when(kindergartenRepository.findOne(eq(1L))).thenReturn(kindergarten);
    assertThat(
            KindergartenService.builder()
                .kindergartenRepository(kindergartenRepository)
                .build()
                .findOne(1))
        .isEqualTo(kindergarten);
  }

  @Test
  public void testFindAll() {
    KindergartenRepository kindergartenRepository = mock(KindergartenRepository.class);
    KgaKindergarten kindergarten = KgaKindergarten.builder().build();
    when(kindergartenRepository.findAll()).thenReturn(Collections.singletonList(kindergarten));
    List<KgaKindergarten> value =
        KindergartenService.builder()
            .kindergartenRepository(kindergartenRepository)
            .build()
            .findAll();
    assertThat(value.size()).isOne();
    assertThat(value).contains(kindergarten);
  }

  /*
  only for coverage.
  */
  @Test
  public void testConstructor() {
    KindergartenRepository repo = mock(KindergartenRepository.class);
    new KindergartenService(repo, null, null, null, null, null, null, null);
  }
}
