package com.kiga.kga.mapper;

import static org.assertj.core.api.Java6Assertions.assertThat;

import com.kiga.kga.api.model.KigaAddress;
import com.kiga.kga.api.model.KindergartenModel;
import com.kiga.kga.domain.KgaKindergarten;
import com.kiga.kga.domain.KgaYear;
import com.kiga.shop.domain.Country;
import java.math.BigDecimal;
import java.util.Collections;
import org.junit.Test;

public class KindergartenMapperTest {

  @Test
  public void kindergaretnModelToKindergarten() {
    KgaKindergarten kindergarten =
        KindergartenMapper.INSTANCE.kindergartenModelToKindergarten(
            new KindergartenModel()
                .id(1)
                .name("name")
                .address(
                    new KigaAddress()
                        .city("Poggersdorf")
                        .doorNumber(new BigDecimal("2"))
                        .floor(new BigDecimal("3"))
                        .houseNumber("4")
                        .zipCode(new BigDecimal("9130"))
                        .countryId(new BigDecimal("248"))
                        .street("Schleusenweg"))
                .website("website.com")
                .email("email@website.com"));
    assertThat(kindergarten.getId()).isEqualTo(1);
    assertThat(kindergarten.getName()).isEqualTo("name");
    assertThat(kindergarten.getCity()).isEqualTo("Poggersdorf");
    assertThat(kindergarten.getDoorNumber()).isEqualTo("2");
    assertThat(kindergarten.getFloor()).isEqualTo("3");
    assertThat(kindergarten.getHouseNumber()).isEqualTo("4");
    assertThat(kindergarten.getZip()).isEqualTo("9130");
    assertThat(kindergarten.getStreet()).isEqualTo("Schleusenweg");
    assertThat(kindergarten.getWebsite()).isEqualTo("website.com");
    assertThat(kindergarten.getCountry().getId()).isEqualByComparingTo(248L);
    assertThat(kindergarten.getEmail()).isEqualTo("email@website.com");
  }

  @Test
  public void kindergartenToKindergartenModel() {
    KgaYear year = new KgaYear();
    year.setId(3L);

    Country country = new Country();
    country.setId(248L);

    KgaKindergarten kindergarten = new KgaKindergarten();
    kindergarten.setId(1L);
    kindergarten.setName("name");
    kindergarten.setCity("Poggersdorf");
    kindergarten.setDoorNumber("2");
    kindergarten.setFloor("3");
    kindergarten.setHouseNumber("4");
    kindergarten.setZip("9130");
    kindergarten.setStreet("Schleusenweg");
    kindergarten.setCountry(country);
    kindergarten.setWebsite("website.com");
    kindergarten.setEmail("email@website.com");
    kindergarten.setYears(Collections.singletonList(year));
    KindergartenModel kindergartenModel =
        KindergartenMapper.INSTANCE.kindergartenToKindergartenModel(kindergarten);
    assertThat(kindergartenModel.getId()).isEqualTo(1);
    assertThat(kindergartenModel.getName()).isEqualTo("name");
    assertThat(kindergartenModel.getAddress().getCity()).isEqualTo("Poggersdorf");
    assertThat(kindergartenModel.getAddress().getDoorNumber()).isEqualByComparingTo("2");
    assertThat(kindergartenModel.getAddress().getFloor()).isEqualByComparingTo("3");
    assertThat(kindergartenModel.getAddress().getHouseNumber()).isEqualTo("4");
    assertThat(kindergartenModel.getAddress().getZipCode()).isEqualByComparingTo("9130");
    assertThat(kindergartenModel.getAddress().getStreet()).isEqualTo("Schleusenweg");
    assertThat(kindergartenModel.getAddress().getCountryId()).isEqualByComparingTo("248");
    assertThat(kindergartenModel.getWebsite()).isEqualTo("website.com");
    assertThat(kindergartenModel.getEmail()).isEqualTo("email@website.com");
    assertThat(kindergartenModel.getCurrentYearId()).isEqualTo(3);
  }
}
