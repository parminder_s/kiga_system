package com.kiga.misc;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.security.crypto.codec.Base64;

/**
 * Created by faxxe on 4/6/16.
 */
public class Base64Test {

  @Test
  public void base64EncodeTest() {
    String teststring = "teststring";
    byte[] encoded = Base64.encode(teststring.getBytes());
    Assert.assertEquals("dGVzdHN0cmluZw==", new String(encoded));
    String decoded = new String(Base64.decode(encoded));
    Assert.assertEquals(teststring, new String(decoded));
  }

}
