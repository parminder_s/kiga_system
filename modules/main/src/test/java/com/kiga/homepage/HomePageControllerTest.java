package com.kiga.homepage;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.cms.url.resolver.exception.InvalidMappingClassException;
import com.kiga.homepage.service.HomePageFacade;
import com.kiga.homepage.web.HomePageController;
import com.kiga.main.locale.Locale;
import com.kiga.web.message.EndpointRequest;
import org.junit.Test;

/**
 * Created by peter on 21.09.16.
 */
public class HomePageControllerTest {
  @Test
  public void getHomeTest() throws InvalidMappingClassException {
    HomePageFacade homePageFacade = mock(HomePageFacade.class);
    HomePageController homePageController = new HomePageController(homePageFacade);
    homePageController.getHome(new EndpointRequest(Locale.en));
    verify(homePageFacade).getHome(any());
  }
}
