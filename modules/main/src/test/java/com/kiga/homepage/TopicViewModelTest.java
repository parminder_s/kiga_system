package com.kiga.homepage;

import com.kiga.homepage.domain.HomePageElement;
import com.kiga.homepage.domain.draft.HomePageElementDraft;
import com.kiga.homepage.web.responses.TopicResponse;
import com.kiga.s3.domain.S3Image;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by peter on 22.09.16.
 */
public class TopicViewModelTest {
  @Test
  public void testDefaultConstructor() {
    TopicResponse topicResponse = new TopicResponse();
    Assert.assertNotNull(topicResponse);
  }

  @Test
  public void testConstructor() {
    HomePageElement homePageElement = new HomePageElementDraft();
    S3Image big = new S3Image();
    big.setUrl("bigImageUrl");
    S3Image small = new S3Image();
    small.setUrl("smallImageUrl");
    homePageElement.setUrl("linkUrl");
    homePageElement.setTitle("title");
    homePageElement.setTitleShort("titleShort");
    homePageElement.setBig(big);
    homePageElement.setSmall(small);
    homePageElement.setColor("color");
    TopicResponse topicResponse = new TopicResponse(homePageElement);
    Assert.assertEquals("bigImageUrl", topicResponse.getBigImageUrl());
    Assert.assertEquals("smallImageUrl", topicResponse.getSmallImageUrl());
    Assert.assertEquals("linkUrl", topicResponse.getLinkUrl());
    Assert.assertEquals("title", topicResponse.getTitle());
    Assert.assertEquals("titleShort", topicResponse.getTitleShort());
    Assert.assertEquals("color", topicResponse.getColor());
  }
}
