package com.kiga.homepage;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.homepage.domain.HomePageElement;
import com.kiga.homepage.domain.HomePageElementContext;
import com.kiga.homepage.domain.HomePageElementType;
import com.kiga.homepage.domain.draft.HomePageElementDraft;
import com.kiga.homepage.domain.live.HomePageElementLive;
import com.kiga.homepage.repository.draft.HomePageElementRepositoryDraft;
import com.kiga.homepage.repository.live.HomePageElementRepositoryLive;
import com.kiga.homepage.service.HomePageEditService;
import com.kiga.main.locale.Locale;
import com.kiga.s3.domain.S3Image;
import com.kiga.s3.repository.ss.S3ImageRepository;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by peter on 21.09.16.
 */
public class HomePageEditServiceTest {
  @Test
  public void addHomePageServiceTest() {
    HomePageElementDraft homePageElement = new HomePageElementDraft();
    homePageElement.setId(1L);
    homePageElement.setLocale(Locale.de);
    homePageElement.setPosition(2);
    homePageElement.setType(HomePageElementType.slider);
    homePageElement.setUrl("url");
    homePageElement.setTitle("title");
    homePageElement.setColor("white");
    S3Image s1 = new S3Image();
    s1.setId(1L);
    S3Image s2 = new S3Image();
    s2.setId(3L);
    homePageElement.setBig(s1);
    homePageElement.setSmall(s2);
    S3ImageRepository s3ImageRepository = mock(S3ImageRepository.class);
    when(s3ImageRepository.findOne(1L)).thenReturn(s1);
    when(s3ImageRepository.findOne(3L)).thenReturn(s2);
    when(s3ImageRepository.exists(any())).thenReturn(true);
    HomePageElementRepositoryDraft homePagePreviewRepositoryDraft = mock(
      HomePageElementRepositoryDraft.class);
    when(homePagePreviewRepositoryDraft.findAll()).thenReturn(Collections.emptyList());
    when(homePagePreviewRepositoryDraft.findOne(eq(1L))).thenReturn(homePageElement);
    HomePageEditService homePageEditService = new HomePageEditService(
      homePagePreviewRepositoryDraft, null, null);
    homePageEditService.add(homePageElement);
    ArgumentCaptor<HomePageElementDraft> captor = ArgumentCaptor.forClass(
      HomePageElementDraft.class);
    verify(homePagePreviewRepositoryDraft, times(2)).save(captor.capture());
    HomePageElementDraft result =  captor.getValue();
    Assert.assertEquals(result.getBig().getId(), new Long(1));
    Assert.assertEquals(result.getPosition(), new Integer(2));
    Assert.assertEquals(result.getSmall().getId(), new Long(3));
    Assert.assertEquals(result.getType(), HomePageElementType.slider);
    Assert.assertEquals(result.getUrl(), "url");
    Assert.assertEquals(result.getLocale(), Locale.de);
    Assert.assertEquals(result.getTitle(), "title");
    Assert.assertEquals(result.getColor(), "white");
  }

  @Test
  public void addNullHomePageServiceTest() {
    HomePageElementDraft homePageElement = new HomePageElementDraft();
    homePageElement.setLocale(null);
    HomePageEditService homePageEditService = new HomePageEditService(null, null, null);
    Assert.assertEquals(false, homePageEditService.add(homePageElement));
  }


  @Test
  public void updateHomePageServiceTest() {
    S3Image s1 = new S3Image();
    s1.setId(1L);
    S3Image s2 = new S3Image();
    s2.setId(2L);
    HomePageElementDraft homePageElementDraft = new HomePageElementDraft();
    homePageElementDraft.setLocale(Locale.en);
    homePageElementDraft.setSmall(s1);
    homePageElementDraft.setPosition(2);
    homePageElementDraft.setBig(s2);
    homePageElementDraft.setType(HomePageElementType.slider);
    homePageElementDraft.setUrl("url");
    homePageElementDraft.setTitle("title");
    homePageElementDraft.setColor("white");
    S3Image s3 = new S3Image();
    s3.setId(3L);
    S3Image s4 = new S3Image();
    s4.setId(4L);
    S3ImageRepository s3ImageRepository = mock(S3ImageRepository.class);
    when(s3ImageRepository.findOne(3L)).thenReturn(s3);
    when(s3ImageRepository.findOne(4L)).thenReturn(s4);
    when(s3ImageRepository.exists(any())).thenReturn(true);
    HomePageElementDraft homePageElementParam = new HomePageElementDraft();
    homePageElementParam.setId(1L);
    homePageElementParam.setLocale(Locale.en);
    homePageElementParam.setBig(s3);
    homePageElementParam.setPosition(5);
    homePageElementParam.setSmall(s4);
    homePageElementParam.setType(HomePageElementType.theme);
    homePageElementParam.setUrl("url");
    homePageElementParam.setTitle("new");
    homePageElementParam.setColor("black");
    HomePageElementRepositoryDraft homePageElementRepositoryDraft = mock(
      HomePageElementRepositoryDraft.class);
    when(homePageElementRepositoryDraft.findOne(any())).thenReturn(homePageElementParam);
    HomePageEditService homePagePreviewService = new HomePageEditService(
      homePageElementRepositoryDraft, null, null);
    homePagePreviewService.update(homePageElementParam);
    ArgumentCaptor<HomePageElementDraft> captor = ArgumentCaptor.forClass(
      HomePageElementDraft.class);
    verify(homePageElementRepositoryDraft).save(captor.capture());
    HomePageElementDraft homePagePreviewRef =  captor.getValue();

    Assert.assertEquals(new Long(3), homePagePreviewRef.getBig().getId());
    Assert.assertEquals(new Integer(5), homePagePreviewRef.getPosition());
    Assert.assertEquals(new Long(4),homePagePreviewRef.getSmall().getId());
    Assert.assertEquals(homePagePreviewRef.getType(), HomePageElementType.theme);
    Assert.assertEquals(homePagePreviewRef.getUrl(), "url");
    Assert.assertEquals(homePagePreviewRef.getLocale(), Locale.en);
  }

  @Test
  public void deleteHomePageServiceTest() {
    HomePageElementRepositoryDraft homePageElementRepositoryDraft = mock(
      HomePageElementRepositoryDraft.class);
    when(homePageElementRepositoryDraft.exists((any()))).thenReturn(true);
    HomePageEditService homePageEditService = new HomePageEditService(
      homePageElementRepositoryDraft, null, null);
    homePageEditService.delete(1L);
    verify(homePageElementRepositoryDraft).delete(1L);
  }

  @Test
  public void deleteHomePageServiceNullTest() {
    HomePageElementRepositoryDraft homePageElementRepositoryDraft = mock(
      HomePageElementRepositoryDraft.class);
    when(homePageElementRepositoryDraft.exists((any()))).thenReturn(false);
    HomePageEditService homePageEditService = new HomePageEditService(
      homePageElementRepositoryDraft, null,null);
    homePageEditService.delete(1L);
    verify(homePageElementRepositoryDraft, never()).delete(eq(1L));
  }

  @Test
  public void publishHomePageServiceTest() {
    HomePageElementDraft homePageElement = new HomePageElementDraft();
    homePageElement.setId(1L);
    homePageElement.setLocale(Locale.de);
    homePageElement.setPosition(2);
    homePageElement.setType(HomePageElementType.slider);
    homePageElement.setUrl("url");
    homePageElement.setTitle("title");
    homePageElement.setTitleShort("short");
    homePageElement.setColor("white");
    S3Image big = new S3Image();
    S3Image small = new S3Image();
    homePageElement.setBig(big);
    homePageElement.setSmall(small);
    HomePageElementRepositoryDraft homePageElementRepositoryDraft = mock(
      HomePageElementRepositoryDraft.class);
    when(homePageElementRepositoryDraft.findOne(eq(1L))).thenReturn(homePageElement);
    HomePageElementRepositoryLive homePageElementRepositoryLive = mock(
      HomePageElementRepositoryLive.class);
    when(homePageElementRepositoryLive.findOne(eq(1L))).thenReturn(null);
    HomePageEditService homePageEditService = new HomePageEditService(
      homePageElementRepositoryDraft, homePageElementRepositoryLive,null);
    homePageEditService.publish(1L);

    ArgumentCaptor<HomePageElementLive> captor = ArgumentCaptor.forClass(
      HomePageElementLive.class);
    verify(homePageElementRepositoryLive).save(captor.capture());
    HomePageElementLive result = captor.getValue();
    Assert.assertEquals(homePageElement.getId(), result.getId());
    Assert.assertEquals(homePageElement.getLocale(), result.getLocale());
    Assert.assertEquals(homePageElement.getPosition(), result.getPosition());
    Assert.assertEquals(homePageElement.getType(), result.getType());
    Assert.assertEquals(homePageElement.getUrl(), result.getUrl());
    Assert.assertEquals(homePageElement.getTitle(), result.getTitle());
    Assert.assertEquals(homePageElement.getTitleShort(), result.getTitleShort());
    Assert.assertEquals(homePageElement.getColor(), result.getColor());
    Assert.assertEquals(homePageElement.getBig(), result.getBig());
    Assert.assertEquals(homePageElement.getSmall(), result.getSmall());
  }

  @Test
  public void unpublishomePageServiceTest() {
    HomePageElementRepositoryLive homePageElementRepositoryLive = mock(
      HomePageElementRepositoryLive.class);
    when(homePageElementRepositoryLive.exists((any()))).thenReturn(true);
    HomePageEditService homePageEditService = new HomePageEditService(
      null, homePageElementRepositoryLive, null);
    homePageEditService.unpublish(1L);
    verify(homePageElementRepositoryLive).delete(1L);
  }

  @Test
  public void publishAllTest() {
    HomePageElementDraft draft1 = new HomePageElementDraft();
    draft1.setId(1L);
    draft1.setContext(HomePageElementContext.homepage);
    HomePageElementDraft draft2 = new HomePageElementDraft();
    draft2.setId(2L);
    draft2.setContext(HomePageElementContext.homepage);
    HomePageElementDraft draft4 = new HomePageElementDraft();
    draft4.setId(4L);
    draft4.setContext(HomePageElementContext.homepage);

    List<HomePageElement> drafts = new ArrayList<>();
    drafts.add(draft1);
    drafts.add(draft2);
    drafts.add(draft4);

    S3Image big = new S3Image();
    drafts.forEach((draft) -> draft.setBig(big));
    S3Image small = new S3Image();
    drafts.forEach((draft) -> draft.setSmall(small));



    HomePageElementLive live1 = new HomePageElementLive();
    live1.setId(1L);
    live1.setContext(HomePageElementContext.homepage);
    HomePageElementLive live2 = new HomePageElementLive();
    live2.setId(2L);
    live2.setContext(HomePageElementContext.homepage);
    HomePageElementLive live3 = new HomePageElementLive();
    live3.setId(3L);
    live3.setContext(HomePageElementContext.homepage);
    HomePageElementLive live4 = new HomePageElementLive();
    live4.setId(4L);
    live4.setContext(HomePageElementContext.homepage);
    List<HomePageElement> lives = new ArrayList<>();
    lives.add(live1);
    lives.add(live2);
    lives.add(live3);
    lives.add(live4);

    HomePageElementRepositoryDraft homePageElementRepositoryDraft = mock(
      HomePageElementRepositoryDraft.class);
    when(homePageElementRepositoryDraft.findByContext(eq(HomePageElementContext.homepage)))
      .thenReturn(drafts);
    drafts.forEach((draft) ->
      when(homePageElementRepositoryDraft.findOne(eq(draft.getId())))
        .thenReturn((HomePageElementDraft)draft));
    HomePageElementRepositoryLive homePageElementRepositoryLive = mock(
      HomePageElementRepositoryLive.class);
    when(homePageElementRepositoryLive.findByContext(eq(HomePageElementContext.homepage)))
      .thenReturn(lives);
    lives.forEach((live) ->
      when(homePageElementRepositoryLive.findOne(eq(live.getId())))
        .thenReturn((HomePageElementLive)live));

    HomePageEditService homePageEditService = new HomePageEditService(
      homePageElementRepositoryDraft, homePageElementRepositoryLive, null);

    homePageEditService.publishAll(HomePageElementContext.homepage);

    verify(homePageElementRepositoryLive).delete(eq(3L));
    verify(homePageElementRepositoryLive, times(3)).save((HomePageElementLive) anyObject());
  }

  @Test
  public void listTest() {
    HomePageElementRepositoryDraft homePageElementRepositoryDraft = mock(
      HomePageElementRepositoryDraft.class);
    HomePageEditService homePageEditService = new HomePageEditService(
      homePageElementRepositoryDraft, null, null);
    List<HomePageElement> list = Collections.emptyList();
    when(homePageElementRepositoryDraft.findByTypeAndContextAndLocaleOrderByPositionAsc(
      eq(HomePageElementType.slider), eq(HomePageElementContext.homepage),eq(Locale.en)))
      .thenReturn(list);
    List<HomePageElement> result = homePageEditService.list(Locale.en,
      HomePageElementType.slider, HomePageElementContext.homepage);
    verify(homePageElementRepositoryDraft).findByTypeAndContextAndLocaleOrderByPositionAsc(
      eq(HomePageElementType.slider), eq(HomePageElementContext.homepage), eq(Locale.en));
    Assert.assertEquals(result, list);
  }

  List<HomePageElement> moveTestData() {
    List<HomePageElement> returner = new ArrayList<>();
    for (int i = 1; i < 6; i++) {
      HomePageElement preview = new HomePageElementDraft();
      preview.setId(new Long(i));
      preview.setTitle("preview-" + i);
      preview.setPosition(i);
      preview.setLocale(Locale.en);
      preview.setType(HomePageElementType.slider);
      preview.setContext(HomePageElementContext.shop);
      returner.add(preview);
    }
    return returner;
  }

  HomePageEditService moveTestService(List<HomePageElement> drafts) {
    HomePageElementRepositoryDraft homePageElementRepositoryDraft = mock(
      HomePageElementRepositoryDraft.class);
    when(homePageElementRepositoryDraft.findByTypeAndContextAndLocaleOrderByPositionAsc(
      eq(HomePageElementType.slider), eq(HomePageElementContext.shop), eq(Locale.en)))
      .thenReturn(drafts);
    drafts.forEach((draft) -> when(homePageElementRepositoryDraft
      .findOne(new Long(draft.getId()))).thenReturn((HomePageElementDraft) draft));
    return new HomePageEditService(homePageElementRepositoryDraft, null, null);
  }

  @Test
  public void moveLeftTest() {
    HomePageElementDraft update = new HomePageElementDraft();
    update.setId(4L);
    update.setPosition(2);
    update.setLocale(Locale.en);
    update.setContext(HomePageElementContext.shop);
    update.setType(HomePageElementType.slider);

    List<HomePageElement> drafts = moveTestData();
    HomePageEditService homePageEditService = moveTestService(drafts);

    homePageEditService.update(update);

    Assert.assertEquals(drafts.get(0).getPosition().toString(), "1");
    Assert.assertEquals(drafts.get(3).getPosition().toString(), "2");
    Assert.assertEquals(drafts.get(1).getPosition().toString(), "3");
    Assert.assertEquals(drafts.get(2).getPosition().toString(), "4");
    Assert.assertEquals(drafts.get(4).getPosition().toString(), "5");
  }

  @Test
  public void moveLeftRight() {
    HomePageElementDraft update = new HomePageElementDraft();
    update.setId(1L);
    update.setPosition(3);
    update.setLocale(Locale.en);
    update.setType(HomePageElementType.slider);
    update.setContext(HomePageElementContext.shop);

    List<HomePageElement> drafts = moveTestData();
    HomePageEditService homePageEditService = moveTestService(drafts);

    homePageEditService.update(update);

    Assert.assertEquals(drafts.get(0).getPosition().toString(), "3");
    Assert.assertEquals(drafts.get(1).getPosition().toString(), "1");
    Assert.assertEquals(drafts.get(2).getPosition().toString(), "2");
    Assert.assertEquals(drafts.get(3).getPosition().toString(), "4");
    Assert.assertEquals(drafts.get(4).getPosition().toString(), "5");
  }

}
