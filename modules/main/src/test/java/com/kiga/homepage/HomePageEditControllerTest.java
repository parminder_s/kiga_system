package com.kiga.homepage;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.homepage.domain.HomePageElementContext;
import com.kiga.homepage.domain.HomePageElementType;
import com.kiga.homepage.domain.draft.HomePageElementDraft;
import com.kiga.homepage.service.HomePageEditService;
import com.kiga.homepage.web.HomePageEditController;
import com.kiga.homepage.web.requests.HomePageElementRequest;
import com.kiga.homepage.web.requests.PublishAllReqTest;
import com.kiga.main.locale.Locale;
import com.kiga.security.services.SecurityService;
import com.kiga.web.message.EndpointRequest;
import org.junit.Test;

/**
 * Created by peter on 21.09.16.
 */
public class HomePageEditControllerTest {

  @Test
  public void deleteTest() {
    SecurityService securityService = mock(SecurityService.class);
    HomePageEditService homePageEditService = mock(HomePageEditService.class);
    HomePageEditController homePageEditController = new HomePageEditController(homePageEditService,
      securityService);
    EndpointRequest endpointRequest = new EndpointRequest();
    endpointRequest.setLocale(Locale.de);
    homePageEditController.delete(1L, endpointRequest);
    verify(securityService).requiresPublisher(any());
    verify(homePageEditService).delete(eq(1L));
  }

  @Test
  public void publishTest() {
    SecurityService securityService = mock(SecurityService.class);
    HomePageEditService homePageEditService = mock(HomePageEditService.class);
    HomePageEditController homePageEditController = new HomePageEditController(homePageEditService,
      securityService);
    homePageEditController.publish(1L,new EndpointRequest());
    verify(securityService).requiresPublisher(any());
    verify(homePageEditService).publish(eq(1L));
  }

  @Test
  public void publishAllTest() {
    SecurityService securityService = mock(SecurityService.class);
    HomePageEditService homePageEditService = mock(HomePageEditService.class);
    HomePageEditController homePageEditController = new HomePageEditController(homePageEditService,
      securityService);
    homePageEditController.publish(new PublishAllReqTest(HomePageElementContext.homepage,
      Locale.de));
    verify(securityService).requiresPublisher(any());
    verify(homePageEditService).publishAll(eq(HomePageElementContext.homepage));
  }

  @Test
  public void unpublishTest() {
    SecurityService securityService = mock(SecurityService.class);
    HomePageEditService homePageEditService = mock(HomePageEditService.class);
    HomePageEditController homePageEditController = new HomePageEditController(homePageEditService,
      securityService);
    homePageEditController.unpublish(1L,new EndpointRequest());
    verify(securityService).requiresPublisher(any());
    verify(homePageEditService).unpublish(eq(1L));
  }

  @Test
  public void update() {
    HomePageElementRequest homePageElementRequest = new HomePageElementRequest();
    HomePageElementDraft homePageElementDraft = new HomePageElementDraft();
    homePageElementRequest.setHomePageElement(homePageElementDraft);
    SecurityService securityService = mock(SecurityService.class);
    HomePageEditService homePageEditService = mock(HomePageEditService.class);
    HomePageEditController homePageEditController = new HomePageEditController(homePageEditService,
      securityService);
    homePageEditController.update(homePageElementRequest);
    verify(securityService).requiresPublisher(any());
    verify(homePageEditService).update(eq(homePageElementDraft));
  }

  @Test
  public void add() {
    HomePageElementRequest homePageElementRequest = new HomePageElementRequest();
    HomePageElementDraft homePageElementDraft = new HomePageElementDraft();
    homePageElementRequest.setHomePageElement(homePageElementDraft);
    SecurityService securityService = mock(SecurityService.class);
    HomePageEditService homePageEditService = mock(HomePageEditService.class);
    HomePageEditController homePageEditController = new HomePageEditController(homePageEditService,
      securityService);
    homePageElementRequest.setLocale(Locale.en);
    homePageEditController.add(homePageElementRequest);
    verify(securityService).requiresPublisher(any());
    verify(homePageEditService).add(eq(homePageElementDraft));
  }

  @Test
  public void list() {
    SecurityService securityService = mock(SecurityService.class);
    HomePageEditService homePageEditService = mock(HomePageEditService.class);
    HomePageEditController homePageEditController = new HomePageEditController(homePageEditService,
      securityService);
    EndpointRequest endpointRequest = new EndpointRequest();
    endpointRequest.setLocale(Locale.de);
    homePageEditController.list(endpointRequest, "slider", "homepage");
    verify(securityService).requiresPublisher(any());
    verify(homePageEditService).list(eq(Locale.de), eq(HomePageElementType.slider),
      eq(HomePageElementContext.homepage));
  }
}
