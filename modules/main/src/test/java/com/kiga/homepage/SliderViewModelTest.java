package com.kiga.homepage;

import com.kiga.homepage.domain.HomePageElement;
import com.kiga.homepage.domain.draft.HomePageElementDraft;
import com.kiga.homepage.web.responses.SliderResponse;
import com.kiga.s3.domain.S3Image;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by peter on 22.09.16.
 */
public class SliderViewModelTest {
  @Test
  public void testDefaultConstructor() {
    SliderResponse sliderResponse = new SliderResponse();
    Assert.assertNotNull(sliderResponse);
  }

  @Test
  public void testConstructor() {
    HomePageElement homePageElement = new HomePageElementDraft();
    S3Image big = new S3Image();
    big.setUrl("bigImageUrl");
    S3Image small = new S3Image();
    small.setUrl("smallImageUrl");
    homePageElement.setUrl("linkUrl");
    homePageElement.setTitle("altText");
    homePageElement.setBig(big);
    homePageElement.setSmall(small);
    SliderResponse sliderResponse = new SliderResponse(homePageElement);
    Assert.assertEquals("bigImageUrl", sliderResponse.getBigImageUrl());
    Assert.assertEquals("smallImageUrl", sliderResponse.getSmallImageUrl());
    Assert.assertEquals("linkUrl", sliderResponse.getLinkUrl());
    Assert.assertEquals("altText", sliderResponse.getAlternateText());
  }
}
