package com.kiga.homepage;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.kiga.cms.url.resolver.exception.InvalidMappingClassException;
import com.kiga.forum.service.NewestForumService;
import com.kiga.homepage.domain.HomePageElementContext;
import com.kiga.homepage.service.HomePageElementService;
import com.kiga.homepage.service.HomePageFacade;
import com.kiga.homepage.web.responses.HomeResponse;
import com.kiga.ideas.properties.IdeasRetrieveProperties;
import com.kiga.ideas.service.NewIdeasGroupService;
import com.kiga.main.locale.Locale;
import org.junit.Test;

/**
 * Created by peter on 22.09.16.
 */
public class HomePageFacadeTest {
  @Test
  public void getHomeTest() throws InvalidMappingClassException {
    NewIdeasGroupService newIdeasGroupService = mock(NewIdeasGroupService.class);
    HomePageElementService homePageElementService = mock(HomePageElementService.class);
    NewestForumService newestForumService = mock(NewestForumService.class);
    HomePageFacade homePageFacade = new HomePageFacade(newIdeasGroupService,
      homePageElementService, newestForumService);
    HomeResponse homeResponse = homePageFacade.getHome(Locale.en);
    verify(newIdeasGroupService).getNewIdeasUrl(eq(Locale.en));
    verify(newestForumService).getNewestForum(eq("idea"), eq(Locale.en));
    verify(newestForumService).getNewestForum(eq("article"), eq(Locale.en));
    IdeasRetrieveProperties ideasRetrieveProperties = new IdeasRetrieveProperties();
    ideasRetrieveProperties.setLocale(Locale.en);
    ideasRetrieveProperties.setAgeGroupFilter(0);
    verify(newIdeasGroupService).getIdeasForAllAgeGroups(eq(ideasRetrieveProperties));
    verify(homePageElementService).getSlider(eq(Locale.en), eq(HomePageElementContext.homepage));
    verify(homePageElementService).getTopic(eq(Locale.en), eq(HomePageElementContext.homepage));
  }
}
