package com.kiga.homepage;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.homepage.domain.HomePageElement;
import com.kiga.homepage.domain.HomePageElementContext;
import com.kiga.homepage.domain.HomePageElementType;
import com.kiga.homepage.repository.HomePageElementRepositoryProxy;
import com.kiga.homepage.service.HomePageElementService;
import com.kiga.main.locale.Locale;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * Created by peter on 22.09.16.
 */
public class HomePageElementServiceTest {

  @Test
  public void sliderTest() {
    HomePageElementRepositoryProxy elementRepositoryProxy = mock(
      HomePageElementRepositoryProxy.class);
    HomePageElementService homePageElementService = new HomePageElementService(
      elementRepositoryProxy);
    List<HomePageElement> list = Collections.emptyList();
    when(elementRepositoryProxy.findByTypeAndContextAndLocaleOrderByPositionAsc(
      eq(HomePageElementType.slider), eq(HomePageElementContext.homepage), eq(Locale.en)))
      .thenReturn(list);
    homePageElementService.getSlider(Locale.en, HomePageElementContext.homepage);
    verify(elementRepositoryProxy).findByTypeAndContextAndLocaleOrderByPositionAsc(
      eq(HomePageElementType.slider),  eq(HomePageElementContext.homepage), eq(Locale.en));
    Assert.assertEquals(list, homePageElementService.getSlider(Locale.en,
      HomePageElementContext.homepage));
  }

  @Test
  public void themeTest() {
    HomePageElementRepositoryProxy elementRepositoryProxy = mock(
      HomePageElementRepositoryProxy.class);
    HomePageElementService homePageElementService = new HomePageElementService(
      elementRepositoryProxy);
    List<HomePageElement> list = Collections.emptyList();
    when(elementRepositoryProxy.findByTypeAndContextAndLocaleOrderByPositionAsc(
      eq(HomePageElementType.theme), eq(HomePageElementContext.homepage), eq(Locale.en)))
      .thenReturn(list);
    homePageElementService.getTopic(Locale.en, HomePageElementContext.homepage);
    verify(elementRepositoryProxy).findByTypeAndContextAndLocaleOrderByPositionAsc(
      eq(HomePageElementType.theme), eq(HomePageElementContext.homepage), eq(Locale.en));
    Assert.assertEquals(list, homePageElementService.getSlider(Locale.en,
      HomePageElementContext.homepage));
  }
}
