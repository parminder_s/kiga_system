package com.kiga.homepage;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kiga.content.repository.ss.RepositoryContext;
import com.kiga.content.repository.ss.RepositoryContexts;
import com.kiga.homepage.domain.HomePageElementContext;
import com.kiga.homepage.domain.HomePageElementType;
import com.kiga.homepage.repository.HomePageElementRepositoryProxy;
import com.kiga.homepage.repository.draft.HomePageElementRepositoryDraft;
import com.kiga.homepage.repository.live.HomePageElementRepositoryLive;
import com.kiga.main.locale.Locale;
import org.junit.Test;

/**
 * Created by peter on 22.09.16.
 */
public class HomePageElementRepositoryProxyTest {
  @Test
  public void livefindByTypeAndLocaleOrderByPositionAscTest() {
    HomePageElementRepositoryLive live = mock(HomePageElementRepositoryLive.class);
    HomePageElementRepositoryDraft draft = mock(HomePageElementRepositoryDraft.class);
    RepositoryContext context = mock(RepositoryContext.class);
    HomePageElementRepositoryProxy proxy = new HomePageElementRepositoryProxy(live, draft, context);
    when(context.getContext()).thenReturn(RepositoryContexts.LIVE);
    proxy.findByTypeAndContextAndLocaleOrderByPositionAsc(
      HomePageElementType.slider, HomePageElementContext.homepage, Locale.en);
    verify(live).findByTypeAndContextAndLocaleOrderByPositionAsc(eq(HomePageElementType.slider),
      eq(HomePageElementContext.homepage), eq(Locale.en));
    verify(draft, never()).findByTypeAndContextAndLocaleOrderByPositionAsc(
      eq(HomePageElementType.slider), eq(HomePageElementContext.homepage), eq(Locale.en));
  }

  @Test
  public void draftfindByTypeAndLocaleOrderByPositionAscTest() {
    HomePageElementRepositoryLive live = mock(HomePageElementRepositoryLive.class);
    HomePageElementRepositoryDraft draft = mock(HomePageElementRepositoryDraft.class);
    RepositoryContext context = mock(RepositoryContext.class);
    HomePageElementRepositoryProxy proxy = new HomePageElementRepositoryProxy(live, draft, context);
    when(context.getContext()).thenReturn(RepositoryContexts.DRAFT);
    proxy.findByTypeAndContextAndLocaleOrderByPositionAsc(
      HomePageElementType.slider,HomePageElementContext.homepage, Locale.en);
    verify(draft).findByTypeAndContextAndLocaleOrderByPositionAsc(
      eq(HomePageElementType.slider), eq(HomePageElementContext.homepage), eq(Locale.en));
    verify(live, never()).findByTypeAndContextAndLocaleOrderByPositionAsc(
      eq(HomePageElementType.slider), eq(HomePageElementContext.homepage), eq(Locale.en));
  }

  @Test
  public void livefindByPositionGreaterThanAndType() {
    HomePageElementRepositoryLive live = mock(HomePageElementRepositoryLive.class);
    HomePageElementRepositoryDraft draft = mock(HomePageElementRepositoryDraft.class);
    RepositoryContext context = mock(RepositoryContext.class);
    HomePageElementRepositoryProxy proxy = new HomePageElementRepositoryProxy(live, draft, context);
    when(context.getContext()).thenReturn(RepositoryContexts.LIVE);
    proxy.findByPositionGreaterThanAndTypeAndContext(
      2, HomePageElementType.slider, HomePageElementContext.homepage);
    verify(live).findByPositionGreaterThanAndTypeAndContext(
      eq(2), eq(HomePageElementType.slider), eq(HomePageElementContext.homepage));
    verify(draft, never()).findByPositionGreaterThanAndTypeAndContext(
      eq(2), eq(HomePageElementType.slider), eq(HomePageElementContext.homepage));
  }

  @Test
  public void draftfindByPositionGreaterThanAndType() {
    HomePageElementRepositoryLive live = mock(HomePageElementRepositoryLive.class);
    HomePageElementRepositoryDraft draft = mock(HomePageElementRepositoryDraft.class);
    RepositoryContext context = mock(RepositoryContext.class);
    HomePageElementRepositoryProxy proxy = new HomePageElementRepositoryProxy(
      live, draft, context);
    when(context.getContext()).thenReturn(RepositoryContexts.DRAFT);
    proxy.findByPositionGreaterThanAndTypeAndContext(
      2, HomePageElementType.slider, HomePageElementContext.homepage);
    verify(draft).findByPositionGreaterThanAndTypeAndContext(
      eq(2), eq(HomePageElementType.slider), eq(HomePageElementContext.homepage));
    verify(live, never()).findByPositionGreaterThanAndTypeAndContext(
      eq(2), eq(HomePageElementType.slider), eq(HomePageElementContext.homepage));
  }


}
