package com.kiga.homepage;

import static org.hamcrest.core.IsInstanceOf.instanceOf;

import com.kiga.homepage.repository.HomePageElementRepositoryProxy;
import com.kiga.homepage.service.HomePageEditService;
import com.kiga.homepage.service.HomePageElementService;
import com.kiga.homepage.service.HomePageFacade;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by peter on 03.08.16.
 */
public class HomePageConfigTest {

  @Test
  public void homePageFacadeTest() {
    HomePageFacade homePageFacade = new HomePageConfig().homePageFacade(null, null, null);
    Assert.assertThat(homePageFacade, instanceOf(HomePageFacade.class));
  }

  @Test
  public void homePageServiceTest() {
    HomePageEditService homePageEditService = new HomePageConfig().homePageEditService(null, null,
      null);
    Assert.assertThat(homePageEditService, instanceOf(HomePageEditService.class));
  }

  @Test
  public void homePageElementTest() {
    HomePageElementService homePageEditService = new HomePageConfig().homePageElementService(null);
    Assert.assertThat(homePageEditService, instanceOf(HomePageElementService.class));
  }

  @Test
  public void homePageEditServiceTest() {
    HomePageEditService homePageEditService = new HomePageConfig().homePageEditService(null,null,
      null);
    Assert.assertThat(homePageEditService, instanceOf(HomePageEditService.class));
  }

  @Test
  public void homePageElementRepositoryProxyTest() {
    HomePageElementRepositoryProxy homePageElementRepositoryProxy = new HomePageConfig()
      .homePageElementRepositoryProxy(null,null, null);
    Assert.assertThat(homePageElementRepositoryProxy, instanceOf(
      HomePageElementRepositoryProxy.class));
  }
}
