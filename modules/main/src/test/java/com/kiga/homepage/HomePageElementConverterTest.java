package com.kiga.homepage;

import com.kiga.homepage.domain.HomePageElement;
import com.kiga.homepage.domain.HomePageElementConverter;
import com.kiga.homepage.domain.HomePageElementType;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by peter on 16.08.16.
 */
public class HomePageElementConverterTest {

  @Test
  public void fromString() {
    HomePageElementConverter converter = new HomePageElementConverter();
    HomePageElementType e1 = converter.convertToEntityAttribute("slider");
    HomePageElementType e2 = converter.convertToEntityAttribute("thema");

    Assert.assertEquals(e1, HomePageElementType.slider);
    Assert.assertEquals(e2, HomePageElementType.theme);

  }

  @Test
  public void fromEnumTest() {
    HomePageElementConverter converter = new HomePageElementConverter();
    String e1 = converter.convertToDatabaseColumn(HomePageElementType.slider);
    String e2 = converter.convertToDatabaseColumn(HomePageElementType.theme);

    Assert.assertEquals(e1, "slider");
    Assert.assertEquals(e2, "thema");
  }

  @Test(expected = IllegalArgumentException.class)
  public void fromEnumIllegal() {
    HomePageElementConverter converter = new HomePageElementConverter();
    converter.convertToDatabaseColumn(HomePageElementType.valueOf("asdf"));
  }

  @Test(expected = IllegalArgumentException.class)
  public void fromStringIlligal() {
    HomePageElementConverter converter = new HomePageElementConverter();
    converter.convertToEntityAttribute("asdf");
  }

  @Test
  public void testAllEnumValues() {
    HomePageElementConverter converter = new HomePageElementConverter();
    for (HomePageElementType type : HomePageElementType.values()) {
      converter.convertToDatabaseColumn(type);
    }
  }

}
