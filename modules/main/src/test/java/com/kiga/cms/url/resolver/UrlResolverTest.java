package com.kiga.cms.url.resolver;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

import com.kiga.cms.url.resolver.exception.InvalidMappingClassException;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.main.locale.Locale;
import com.mysema.query.types.expr.BooleanExpression;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.Arrays;

/**
 * @author bbs
 * @since 6/15/16.
 */
public class UrlResolverTest {
  @Test(expected = InvalidMappingClassException.class)
  public void testDummyExceptionThrow2() throws InvalidMappingClassException {
    new UrlResolver(null, null).resolve(new String[] {}, Locale.en, SiteTreeLive.class);
  }

  @Test
  public void defaultCase() throws Exception {
    String[] urlSegments = (String[])Arrays.asList("segment1", "segment2").toArray();
    QueryDslPredicateExecutor executor = mock(QueryDslPredicateExecutor.class);
    UrlResolverBooleanExpressionCreator creator = mock(UrlResolverBooleanExpressionCreator.class);
    BooleanExpression expression1 = mock(BooleanExpression.class);
    BooleanExpression expression2 = mock(BooleanExpression.class);

    when(creator.getBooleanExpressions(
      any(), eq(Locale.en), eq(SiteTreeLive.class), eq("siteTreeLive")))
      .thenReturn(Arrays.asList(expression1, expression2));
    ResolvedUrlHierarchyItem item = new UrlResolver<>(executor, creator)
      .resolve(urlSegments, Locale.en, SiteTreeLive.class);
    Assert.assertThat(item, instanceOf(ResolvedUrlHierarchyItem.class));

    verify(expression1).and(expression2);
    verify(executor).findOne(any());
  }
}
