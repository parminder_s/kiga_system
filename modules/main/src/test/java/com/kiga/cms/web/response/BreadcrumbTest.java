package com.kiga.cms.web.response;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanEquals;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanHashCode;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * @author bbs
 * @since 9/10/16.
 */
public class BreadcrumbTest {
  @Test
  public void testBean() {
    assertThat(Breadcrumb.class,
      allOf(hasValidGettersAndSetters(), hasValidBeanEquals(), hasValidBeanHashCode()));
  }
}
