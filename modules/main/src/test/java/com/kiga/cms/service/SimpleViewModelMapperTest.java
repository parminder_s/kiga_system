package com.kiga.cms.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.cms.web.response.Breadcrumb;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.SiteTree;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by rainerh on 21.12.16.
 */
public class SimpleViewModelMapperTest {
  private static final String TITLE = "an idea";
  private static final String META_DESCRIPTION = "meta description for idea";
  private static final String META_KEYWORDS = "ideas, kindergarten";
  private static final String CLASS_NAME = "SiteTree";
  private static final String SITETREE_TITLE = "siteTree";
  private static final String PARENT_L1_TITLE = "l1";
  private static final String PARENT_L2_TITLE = "l2";
  private static final String MAIN_TITLE = "main";

  SimpleViewModelMapper mapper = new SimpleViewModelMapper<SiteTreeLive, ViewModel>() {
    @Override
    protected ViewModel buildViewModel(HttpServletRequest httpServletRequest,
        HttpServletResponse httpServletResponse, QueryRequestParameters queryRequestParameters)
        throws Exception {
      return new ViewModel();
    }
  };

  @Test
  public void testBreadcrumbs() {
    SiteTree siteTree = new SiteTreeLive();
    siteTree.setTitle(SITETREE_TITLE);
    SiteTree parentL1 = new SiteTreeLive();
    parentL1.setTitle(PARENT_L1_TITLE);
    SiteTree parentL2 = new SiteTreeLive();
    parentL2.setTitle(PARENT_L2_TITLE);
    SiteTree main = new SiteTreeLive();
    main.setTitle(MAIN_TITLE);

    siteTree.setParent(parentL2);
    parentL2.setParent(parentL1);
    parentL1.setParent(main);

    List<Breadcrumb> breadcrumbs = mapper.buildBreadcrumbs(siteTree);
    List<String> labels =
        breadcrumbs.stream().map(Breadcrumb::getLabel).collect(Collectors.toList());
    assertArrayEquals(Arrays.asList(PARENT_L2_TITLE, SITETREE_TITLE).toArray(), labels.toArray());
  }

  @Test
  public void testDefaultProperties() throws Exception {
    SiteTreeLive siteTree = new SiteTreeLive();
    siteTree.setTitle(TITLE);
    siteTree.setMetaDescription(META_DESCRIPTION);
    siteTree.setMetaKeywords(META_KEYWORDS);
    siteTree.setClassName(CLASS_NAME);

    QueryRequestParameters queryRequestParameters =
      QueryRequestParameters.builder()
        .entity(siteTree)
        .build();

    ViewModel viewModel = mapper.getViewModel(null, null, queryRequestParameters);
    assertEquals(TITLE, viewModel.getMetadata().getTitle());
    assertEquals(META_DESCRIPTION, viewModel.getMetadata().getDescription());
    assertEquals(META_KEYWORDS, viewModel.getMetadata().getKeywords());
    assertEquals(CLASS_NAME, viewModel.getResolvedType());
  }
}
