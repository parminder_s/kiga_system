package com.kiga.cms.service;

import static org.mockito.Mockito.mock;

import com.kiga.cms.service.exception.ViewModelMapperNotRegisteredException;
import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.draft.SiteTreeDraft;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import org.junit.Assert;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 7/3/16.
 */
public class ViewModelMapperFactoryTest {
  ViewModelMapperFactory viewModelMapperFactory = new ViewModelMapperFactory();
  HttpServletRequest httpServletRequestMock = mock(HttpServletRequest.class);
  HttpServletResponse httpServletResponseMock = mock(HttpServletResponse.class);

  @Test(expected = IllegalArgumentException.class)
  public void testSearchFacadeServiceWhenTypeIsNull() throws ViewModelMapperNotRegisteredException {
    viewModelMapperFactory.forType(null);
  }

  @Test(expected = ViewModelMapperNotRegisteredException.class)
  public void testFacadeServiceNotRegisteredForType() throws ViewModelMapperNotRegisteredException {
    viewModelMapperFactory.forType(SiteTreeDraft.class);
  }

  @Test
  public void testFindingProperFacadeService() throws Exception {
    SiteTreeLive siteTreeLive = new SiteTreeLive();
    siteTreeLive.setId(123L);

    viewModelMapperFactory.register(new SimpleViewModelMapper<SiteTreeLive, ViewModel>() {
      @Override
      protected ViewModel buildViewModel(
        HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
        QueryRequestParameters<SiteTreeLive> queryRequestParameters) {
        return new ViewModel();
      }
    }, SiteTreeLive.class);

    QueryRequestParameters queryRequestParameters =
      QueryRequestParameters.builder()
        .entity(siteTreeLive)
        .build();

    ViewModel resolvedViewModel = viewModelMapperFactory.forType(SiteTreeLive.class)
        .getViewModel(httpServletRequestMock, httpServletResponseMock, queryRequestParameters);

    Assert.assertEquals(resolvedViewModel.getId(), siteTreeLive.getId());
  }
}
