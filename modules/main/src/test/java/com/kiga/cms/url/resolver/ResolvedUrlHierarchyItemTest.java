package com.kiga.cms.url.resolver;

import com.google.code.beanmatchers.BeanMatchers;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 13.11.16.
 */
public class ResolvedUrlHierarchyItemTest {
  @Test
  public void testBean() {
    Assert.assertThat(ResolvedUrlHierarchyItem.class, BeanMatchers.hasValidGettersAndSetters());
  }

}
