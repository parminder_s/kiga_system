package com.kiga.cms;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kiga.cms.service.SimpleViewModelMapper;
import com.kiga.cms.service.ViewModelMapperFactory;
import com.kiga.cms.service.parameters.QueryRequestParameters;
import com.kiga.cms.url.resolver.ResolvedUrlHierarchyItem;
import com.kiga.cms.url.resolver.UrlResolver;
import com.kiga.cms.url.resolver.exception.UrlResolverNotRegisteredException;
import com.kiga.cms.web.CmsController;
import com.kiga.cms.web.request.ResolverRequest;
import com.kiga.cms.web.response.ViewModel;
import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.main.locale.Locale;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bbs
 * @since 7/3/16.
 */
public class CmsControllerTest {
  HttpServletRequest httpServletRequestMock = mock(HttpServletRequest.class);
  HttpServletResponse httpServletResponseMock = mock(HttpServletResponse.class);

  @Test
  public void testResolve() throws Exception {
    SiteTreeLive siteTreeLive = new SiteTreeLive();
    ResolvedUrlHierarchyItem<SiteTreeLive> resolvedItemHolder =
        new ResolvedUrlHierarchyItem<>(siteTreeLive, 1);

    UrlResolver urlResolver = mock(UrlResolver.class);
    when(urlResolver.resolve(any(), any(), any())).thenReturn(resolvedItemHolder);

    ViewModelMapperFactory viewModelMapperFactory = new ViewModelMapperFactory();
    viewModelMapperFactory.register(new SimpleViewModelMapper<SiteTreeLive, ViewModel>() {
      @Override
      protected ViewModel buildViewModel(
        HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
        QueryRequestParameters<SiteTreeLive> queryRequestParameters) {
        return new ViewModel();
      }
    }, SiteTreeLive.class);

    CmsController cmsController = new CmsController(urlResolver, viewModelMapperFactory);

    String[] urlSegments = Arrays.asList("a", "b", "c").toArray(new String[0]);

    ResolverRequest resolverRequest = new ResolverRequest();
    resolverRequest.setPathSegments(urlSegments);
    resolverRequest.setAgeGroup(0);
    resolverRequest.setLocale(Locale.en);
    ViewModel viewModel = cmsController
        .resolve(httpServletRequestMock, httpServletResponseMock, resolverRequest);

    Assert.assertNotNull(viewModel);
    Assert.assertEquals(siteTreeLive.getClassName(), viewModel.getResolvedType());
  }

  @Test(expected = UrlResolverNotRegisteredException.class)
  public void testDummyUrlResolverNotRegisteredException() throws
      UrlResolverNotRegisteredException {
    throw new UrlResolverNotRegisteredException();
  }
}
