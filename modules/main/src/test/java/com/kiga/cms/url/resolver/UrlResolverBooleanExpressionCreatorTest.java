package com.kiga.cms.url.resolver;

import com.kiga.content.domain.ss.live.SiteTreeLive;
import com.kiga.main.locale.Locale;
import com.mysema.query.types.expr.BooleanExpression;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by rainerh on 13.11.16.
 */
public class UrlResolverBooleanExpressionCreatorTest {
  @Test
  public void test() {
    List<String> conditions = Arrays.asList(
      "siteTreeLive.parent.parent.parent.className like HomePage",
      "siteTreeLive.parent.parent.parent.locale = en",
      "siteTreeLive.parent.parent.urlSegment like ideas",
      "siteTreeLive.parent.urlSegment like ideaCategory",
      "siteTreeLive.urlSegment like idea");

    List<BooleanExpression> expressions =
      new UrlResolverBooleanExpressionCreator<SiteTreeLive>().getBooleanExpressions(
        new LinkedList<String>(Arrays.asList("idea", "ideaCategory", "ideas")),
        Locale.en, SiteTreeLive.class, "siteTreeLive");

    for (int i = 0; i < conditions.size(); i++) {
      Assert.assertEquals(conditions.get(i), expressions.get(i).toString());
    }
  }
}
