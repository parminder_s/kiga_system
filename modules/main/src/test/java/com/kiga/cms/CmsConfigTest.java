package com.kiga.cms;

import static org.hamcrest.core.IsInstanceOf.instanceOf;

import com.kiga.cms.service.ViewModelMapperFactory;
import com.kiga.cms.url.resolver.UrlResolver;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author bbs
 * @since 7/6/16.
 */
public class CmsConfigTest {
  @Test
  public void defaultUrlResolverTest() {
    UrlResolver resolver = new CmsConfig().defaultUrlResolver(null);
    Assert.assertThat(resolver, instanceOf(UrlResolver.class));
  }

  @Test
  public void facadeServiceFactoryTest() {
    ViewModelMapperFactory factory = new CmsConfig().facadeServiceFactory();
    Assert.assertThat(factory, instanceOf(ViewModelMapperFactory.class));
  }
}
