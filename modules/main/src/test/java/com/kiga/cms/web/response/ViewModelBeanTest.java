package com.kiga.cms.web.response;

import com.google.code.beanmatchers.BeanMatchers;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rainerh on 29.11.16.
 */
public class ViewModelBeanTest {
  @Test
  public void testViewModel() {
    Assert.assertThat(ViewModel.class, BeanMatchers.hasValidGettersAndSetters());
  }

  @Test
  public void testEqual() {
    Assert.assertThat(ViewModel.class, BeanMatchers.hasValidBeanEquals());
  }

  @Test
  public void testMetadata() {
    Assert.assertThat(Metadata.class, BeanMatchers.hasValidGettersAndSetters());
  }

  @Test
  public void testViewModelPermission() {
    Assert.assertThat(Permission.class, BeanMatchers.hasValidGettersAndSetters());
  }

}
