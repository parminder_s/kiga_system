insert into Manufacturer(ID, Title) values(1, 'Ford');
insert into Manufacturer(ID, Title) values(2, 'Toyota');
insert into Manufacturer(ID, Title) values(3, 'VW');

insert into Car(ID, manufacturer_id, Title) values(1, 1, 'C-Max');
insert into Car(ID, manufacturer_id, Title) values(2, 1, 'Mustang');
insert into Car(ID, manufacturer_id, Title) values(3, 2, 'Auris');
insert into Car(ID, manufacturer_id, Title) values(4, 3, 'Polo');
insert into Car(ID, manufacturer_id, Title) values(5, 3, 'Passat');
