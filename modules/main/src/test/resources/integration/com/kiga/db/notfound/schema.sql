create table Employee (
  id IDENTITY,
  firstname VARCHAR(50),
  lastname VARCHAR(50),
  company_id int,
  car_id int
);

create table Company (
  id IDENTITY,
  title VARCHAR(50)
);

create table Car (
  id IDENTITY,
  name varchar(50)
);
