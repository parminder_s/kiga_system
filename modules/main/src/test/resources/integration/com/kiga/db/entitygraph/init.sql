insert into Manufacturer(id, name) values(1, 'Daimler'), (2, 'Toyota'), (3, 'Ford');
insert into Car(id, name, manufacturer_id) VALUES
  (1, 'C-Class', 1), (2, 'E-Class', 1),
  (3, 'Auris', 2), (4, 'Prius', 2),
  (5, 'Focus', 3), (6, 'Mustang', 3);

insert into Car_Variety(id, name, car_id) VALUES
  (1, 'W-205', 1), (2, 'W-204', 1), (3, 'W-203', 1),
  (4, 'E-150', 3), (5, 'E-180', 3),
  (6, 'ST', 5), (7, 'C-MAX', 5), (8, 'RS500', 5);
