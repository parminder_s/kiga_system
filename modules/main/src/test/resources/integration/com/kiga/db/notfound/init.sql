insert into Company(id, title) values(1, 'Mars Austria');
insert into Company(id, title) values(2, 'KiGaPortal');
insert into Car(id, name) values(1, 'Mercedes E-Klasse (1997)');
insert into Employee(id, firstname, lastname, company_id, car_id) values(1, 'Otto', 'Touzil', 1, 1);
insert into Employee(id, firstname, lastname, company_id, car_id) values(2, 'Peter', 'Kohout', 1, 0);
insert into Employee(id, firstname, lastname, company_id, car_id) values(3, 'Christian', 'Fuchs', 2, 0);

