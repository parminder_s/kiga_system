FROM openjdk:8-alpine as builder

WORKDIR /src

COPY gradlew gradlew
COPY .gradle .gradle
COPY gradle gradle

RUN ./gradlew wrapper

COPY build.gradle build.gradle
COPY settings.gradle settings.gradle
COPY modules/main/build.gradle modules/main/build.gradle
COPY modules/main/gradle.properties modules/main/gradle.properties
COPY modules/main/tasks modules/main/tasks
COPY modules/main/src/main/java/com/kiga/Application.java modules/main/src/main/java/com/kiga/Application.java
RUN ./gradlew :modules:main:test

COPY modules modules
COPY doc doc
RUN ./gradlew :modules:main:build



FROM openjdk:8-alpine
MAINTAINER rainer.hahnekamp@gmail.com

COPY --from=builder /src/modules/main/build/libs/main.jar /kiga-system.jar

EXPOSE 8080

ENTRYPOINT java -jar /kiga-system.jar
