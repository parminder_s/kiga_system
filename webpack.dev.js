const config = require('./webpack.config.js'),
  path = require('path'),
  webpackMerge = require('webpack-merge'),
  WebpackNotifier = require('webpack-notifier'),
  WebpackShellPlugin = require('webpack-shell-plugin');

module.exports = webpackMerge(config, {
  devtool: '#inline-source-map',
  watch: true,
  module: {
    loaders: [
      {
        test: /\.js$/,
        enforce: 'pre',
        loader: 'source-map-loader'
      },
      {
        test: /\.css/,
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' }]
      },
      {
        test: /\.less$/,
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' }, { loader: 'less-loader' }]
      }
    ]
  },
  plugins: [
    new WebpackNotifier({
      title: 'KiGaPortal',
      contentImage: path.join(__dirname, 'logo.png')
    }),
    new WebpackShellPlugin({
      onBuildEnd: ['npm run serve']
    })
  ]
});
