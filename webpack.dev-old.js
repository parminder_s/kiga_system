const config = require('./webpack.dev.js'),
  webpackMerge = require('webpack-merge'),
  WebpackShellPlugin = require('webpack-shell-plugin'),
  webpackConfig = webpackMerge(config, {});

webpackConfig.plugins = webpackConfig.plugins.filter(
  plugin => !(plugin instanceof WebpackShellPlugin)
);
webpackConfig.watch = false;
webpackConfig.devServer = {
  proxy: {
    '/api/*': 'http://localhost:8080'
  },
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
    'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization'
  }
};

module.exports = webpackConfig;
