/**
 * Created by rainerh on 08.04.15.
 *
 * Gulp is used for creating the final build. For development mode, webpack-dev-server is enough.
 * For further information consult the Installation.md.
 */

'use strict';

const gulp = require('gulp'),
  del = require('del'),
  changed = require('gulp-changed'),
  copy = require('gulp-copy'),
  copyDir = require('copy-dir'),
  header = require('gulp-header'),
  htmlmin = require('gulp-htmlmin'),
  less = require('gulp-less'),
  minifyCss = require('gulp-minify-css'),
  plumber = require('gulp-plumber'),
  runSequence = require('run-sequence'),
  webpack = require('webpack');

const paths = {
  app: 'app/src',
  test: 'app/test',
  distRoot: 'bundle',
  testJs: 'bundle/test',
  dist: 'bundle/src'
};

const copyPrefix = 2;

gulp.task('default', cb =>
  runSequence('clean', 'assets', ['webpack', 'minimizeHtml', 'less:tinymce'], cb)
);

gulp.task('old-build', ['default'], cb => {
  copyDir('bundle', 'dist', cb);
});

gulp.task('dev', cb => runSequence('clean', 'assets', ['webpack-dev', 'assets-watch'], cb));

// removes complete build directory
gulp.task('clean', cb => del([paths.dist + '/**'], cb));

//compiles and minifies the css for the tinymce's content
gulp.task('less:tinymce', () =>
  gulp
    .src(paths.app + '/tinymce.less')
    .pipe(less({ relativeUrls: true }))
    .pipe(minifyCss())
    .pipe(header('/* Copyright KiGaPortal ' + new Date().getFullYear() + ' */\n'))
    .pipe(gulp.dest(paths.dist))
);

//compiles the less file for the docs
gulp.task('less:docs', () =>
  gulp
    .src(paths.app + '/docs/doc.less')
    .pipe(plumber())
    .pipe(less({ relativeUrls: true }))
    .pipe(gulp.dest(paths.dist + '/docs'))
);

//copies and watches the asset files for changes
gulp.task('assets', () =>
  gulp
    .src([paths.app + '/**/*.{html,json,jpg,png,svg,gif}', '!' + paths.app + '/index.html'])
    .pipe(changed(paths.dist))
    .pipe(gulp.dest(paths.dist))
);

//minimizes all html files in the dist directory
gulp.task('minimizeHtml', () =>
  gulp
    .src(paths.dist + '/**/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest(paths.dist))
);

gulp.task('assets-watch', () =>
  gulp.watch(
    [paths.app + '/**/*.{html,json,jpg,png,svg,gif}', '!' + paths.app + '/index.html'],
    ['assets']
  )
);

gulp.task('webpack', cb => webpack(require('./webpack.build'), webpackCallback(cb)));

gulp.task('webpack-dev', cb => webpack(require('./webpack.dev'), webpackCallback(cb)));

const webpackCallback = cb => (err, stats) => {
  const info = stats.toJson();
  if (err) {
    cb(err);
  } else if (stats.hasErrors()) {
    cb(info.errors);
  } else if (stats.hasWarnings()) {
    cb(info.warnings);
  } else {
    cb();
  }
};
